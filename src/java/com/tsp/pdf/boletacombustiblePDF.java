
/*****************************************************************************
 * Nombre clase :                   boletacombustiblePDF                     *
 * Descripcion :                    Clase auxiliar que maneja los eventos    *
 *                                  relacionados con la generacion de los    *
 *                                  archivos XML y PDF                       *
 * Autor :                          Ing. Andres Martinez                     *
 * Fecha :                          06 julio del 2006, 7:31 PM               *
 * Version :                        1.0                                      *
 * Copyright :                      Fintravalores S.A.                  *
 *****************************************************************************/

package com.tsp.pdf;
//dom4j
import org.dom4j.Element;
import org.dom4j.DocumentHelper;
import org.dom4j.Document;

//java
import java.io.*;
import java.util.*;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamSource;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.sax.SAXSource;
import org.xml.sax.InputSource;

//Fop
import org.apache.fop.apps.Driver;
import org.apache.fop.messaging.MessageHandler;
import org.apache.avalon.framework.logger.ConsoleLogger;
import org.apache.avalon.framework.logger.Logger;

import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.util.Util;
/**
 *
 * @author  igomez
 */
public class boletacombustiblePDF {
     Model model;    
    //private  HttpServletRequest request;
    
    /** Creates a new instance of HojaControlViaje */
    public boletacombustiblePDF() {
        
    }
    
    public Element boleta(String foto) {
        
        
        Element raiz = DocumentHelper.createElement("raiz");
        
        List	listado = model.DescuentoTercmWebSvc.getListaImpresion();
        List lisNombre = model.DescuentoTercmWebSvc.getListado();
        String Nombre="",OC="";
        Iterator Ite = lisNombre.iterator();
        if(Ite.hasNext()){
            DescuentoTercm  desc   = (DescuentoTercm)Ite.next();
            Nombre = desc.getNombre();
            OC     = desc.getOC();
        }
        
        Iterator It = listado.iterator();
        int i = 1;
        while(It.hasNext()){
            DescuentoTercm  tercm   = (DescuentoTercm)It.next(); 

            // if( hvc != null ){
            Element data        = raiz.addElement("data");
            Element cabecera    = data.addElement("cabecera");
            Element tabla1      = data.addElement("tabla1");
            Element tabla2      = data.addElement("tabla2");

            Element boleta      = cabecera.addElement("boleta").addText(tercm.getBoleta());
            tabla1.addElement("imagen").addText("url("+foto+")");
            //tabla1.addAttribute("imagen","url(documentos/imagenes/KDIAZG/0079752335.JPG)"); 
            Element planilla    = tabla1.addElement("planilla").addText("GGGDGGFGFGG");


            String leyenda      =   " Fintravalores S.A. S.A autoriza a "+Nombre+" con \n "+
                                    " C.C : "+tercm.getCedula()+" el suministro de combustible al equipo \n "+
                                    " con placa "+tercm.getPlaca()+" y planilla "+tercm.getOC()+"    \n "+
                                    "							\n ";
            String leyenda2      =  " por valor de : $ "+(""+Util.customFormat(Double.parseDouble(tercm.getValorPost())))+"";                              
            Element ley         =  tabla1.addElement("leyenda").addText(leyenda);
            Element ley2         =  tabla1.addElement("leyenda2").addText(leyenda2);  
        }
        return raiz;
    }
    
    public void generarPDF ( String ruta, Model a,String foto) throws Exception {
        try{
            model =a;
           

            File xslt = new File ( ruta + "Templates/boletacombustible.xsl" );
            File pdf = new File ( ruta + "pdf/boletacombustible.pdf" );
            Element raiz = this.boleta(foto);

            //System.out.println("ok pdf 1");
            this.PDF(xslt, pdf, raiz);
            
           
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }
    
    public void PDF(File xslt, File pdf, Element raiz){
        
        
        try {
            org.apache.fop.configuration.Configuration.put( "baseDir", "file:///" + xslt.getParent() );
            Document documento = DocumentHelper.createDocument ( raiz );
            ProformaXMLReader proformaXMLReader = new ProformaXMLReader ( documento );
            Driver driver = new Driver();
            Logger logger = new ConsoleLogger ( ConsoleLogger.LEVEL_INFO );
            driver.setLogger ( logger );
            driver.setRenderer ( driver.RENDER_PDF );
            FileOutputStream salida = new FileOutputStream ( pdf );
            driver.setOutputStream ( salida );
            TransformerFactory factory = TransformerFactory.newInstance();
            Transformer transformer = factory.newTransformer( new StreamSource ( xslt ) );
            InputSource inputSource = new InputSource();
            inputSource.setEncoding( "windows-1252" );
            SAXSource source = new SAXSource( proformaXMLReader, inputSource );
            SAXResult res = new SAXResult( driver.getContentHandler () );
            transformer.transform( source, res );
            String pageCount = "1";
            driver = new Driver();
            Logger logger1 = new ConsoleLogger ( ConsoleLogger.LEVEL_INFO );
            driver.setLogger ( logger1 );
            MessageHandler.setScreenLogger ( logger1 );
            driver.setRenderer ( driver.RENDER_PDF );
            salida = new FileOutputStream ( pdf );
            driver.setOutputStream ( salida );
            factory = TransformerFactory.newInstance();
            transformer = factory.newTransformer(new StreamSource ( xslt ) );
            inputSource = new InputSource();
            inputSource.setEncoding ( "windows-1252" );
            res = new SAXResult ( driver.getContentHandler () );
            transformer.setParameter ( "page-count", pageCount );
            transformer.transform ( source, res ); 
             salida.close(); 
        }catch(Exception e){
            e.printStackTrace();
        } 
    }
    
    protected String obtenerTexto( Object obj ) {
        
        if ( ( obj == null ) || ( obj.toString().toLowerCase().trim().equals( "ninguno" ) ) ) {
            
            return "";
            
        } else {
            
            return obj.toString();
            
        }
    }    
   
}
