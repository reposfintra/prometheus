/*****************************************************************************
 * Nombre clase :                   FacturaPDF                               *
 * Descripcion :                    Clase auxiliar que maneja los eventos    *
 *                                  relacionados con la generacion de los    *
 *                                  archivos XML y PDF                       *
 * Autor :                          AMARTINEZ                                *
 * Fecha :                          20 junio del 2006, 2:27 PM              *
 * Version :                        1.0                                      *
 * Copyright :                      Fintravalores S.A.                  *
 *****************************************************************************/

package com.tsp.pdf;

import org.dom4j.Element;
import org.dom4j.DocumentHelper;
import org.dom4j.Document;
import java.io.*;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamSource;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.sax.SAXSource;
import org.xml.sax.InputSource;
import org.apache.fop.apps.Driver;
import org.apache.fop.messaging.MessageHandler;
import org.apache.avalon.framework.logger.ConsoleLogger;
import org.apache.avalon.framework.logger.Logger;
import java.lang.*;
import java.util.*;
import com.tsp.util.UtilFinanzas;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.pdf.*;
import com.tsp.util.*;

public class FacturaPDF {
    boolean sw = false;
    
    Model model;
    Element raiz2;
    Element tabla5;
    String fac;
    private String dstrct;
    private String res_dian;
    private String res_gran;
    private String estado = "";
    private String idusuario = "";
    private int lineas = 0;
    private int lineas_insertadas = 0;
    private int limite = 0;
    
    Element objeto;
    /** Creates a new instance  */
    public FacturaPDF( String f, String distrito, String est, String idusu ){
        fac             =   f;
        dstrct          =   distrito;
        this.estado     =   est;
        this.idusuario  =   idusu;
    }
    
    public Element FilasFactura() {
        String strtasa = "";
        Vector copia = (Vector)model.facturaService.getVFacturas().clone();
        Element raiz = DocumentHelper.createElement("raiz");
        if(!fac.equals("")){
            factura fact = new factura();
            for (int kk = 0 ; kk <copia.size() ; kk++ ){
                fact = (factura) ((factura)copia.get(kk));
                if(fac.equals(fact.getFactura())){
                    break;
                }
                
            }
            copia.removeAllElements();
            copia.add(fact);
        }
        
        /*Modificacion 05-02-07*/
        if(this.estado.equals("chk")){
            Vector vpdf = (Vector)copia.clone();
            copia.removeAllElements();
            for (int ind = 0 ; ind < vpdf.size() ; ind++ ){
                factura fact = (factura) ((factura)vpdf.get(ind));
                
                if(fact.isEstadoImpresion()){
                    copia.add(fact);
                }
                
            }
        }
        
        
        
        for (int kk = 0 ; kk < copia.size() ; kk++ ){
            
            factura factu = (factura) ((factura)copia.get(kk));
            
            model.facturaService.updateFechaImpresion( factu.getFactura() , this.dstrct );
            
            Element data    = raiz.addElement("data");
            Element tabla1  = data.addElement("datos_clientes");
            
            
            
            String c_cliente = (factu.getNomcli()!=null)?factu.getNomcli() :"";
            
            if( c_cliente.length() > 43 ){
                c_cliente = c_cliente.substring(0, 44);
            }
            
            Element cliente = tabla1.addElement( "cliente" ).addText( c_cliente );
            
            /*Validacion Cliente*/
            
            String c_nit    = ( factu.getNit()!=null )? factu.getNit() : "";
            
            String c_rif    = ( factu.getRif()!=null )? factu.getRif() : "";
            
            String aux_nit  = c_nit;
            
            if( !c_rif.equals("") ){
                aux_nit += " RIF : " + c_rif;
            }
            
            Element nit  = tabla1.addElement( "nit" ).addText( aux_nit );
            
            /*Validacion nit*/
            
            String c_direccion = (factu.getDireccion()!=null)?factu.getDireccion():"";
            
            if( c_direccion.length() > 43 ){
                
                c_direccion = c_direccion.substring(0,44);
                
            }
            
            Element direccion  = tabla1.addElement( "direccion" ).addText(c_direccion );
            
            /*Validacion Direccion*/
            
            Element telefono  = tabla1.addElement( "telefono" ).addText( (factu.getTelefono()!=null)?factu.getTelefono():"" );
            
            String c_lugar   = "";
            try{
                c_lugar   = model.facturaService.buscarCiudad_Cliente( this.idusuario );
            }catch( Exception e ){e.printStackTrace();}
            
            Element lugar  = tabla1.addElement( "lugar" ).addText( c_lugar );
            
            
            String c_ciudad = ( factu.getCiudad()!=null)?factu.getCiudad():"";
            
            c_ciudad        = ( c_ciudad.toUpperCase().equals("OFICINA PRINCIPAL"))?"BARRANQUILLA":c_ciudad;
            
            Element ciudad  = tabla1.addElement( "ciudad" ).addText( c_ciudad );
            
            
            String actual = Util.getFechaActual_Letras();
            Element fecha  = tabla1.addElement( "fecha" ).addText(actual);
            
            
            
            Element factura  = tabla1.addElement( "factura" ).addText( (factu.getFactura()!=null)?factu.getFactura():"" );
            
            
            
            Element tabla6  = data.addElement("barracolumna");
            
            Element resoluciones  = data.addElement("resoluciones");
            
            
            this.res_dian = (factu.getResol_dian()!=null)?factu.getResol_dian():"    ";//Resolucion DIAN
            this.res_gran = (factu.getResol_gran()!=null)?factu.getResol_gran():"    ";//Resolucion Gran Contribuyente;
            
            Element resol_dian  = resoluciones.addElement("resolucion_dian").addText(res_dian);
            
            Element resol_gran  = resoluciones.addElement("resolucion_gran").addText(res_gran);
            
            
            int cont=0 , h=0;
            
            Vector info = new Vector();
            try{
                
                /**Modificacion carbon*/                
                if ( factu.getHc().equals("FC") ){
                    model.facturaService.buscarItemsCarbon( factu.getFactura(), this.dstrct );
                    factu = model.facturaService.buscarDescripcionCarbon(this.dstrct, factu );
                    info = model.facturaService.getRemesasFacturar();
                }//Fin de validacion Carbon
                else{
                    model.facturaService.buscarItemPdf( factu.getFactura(), this.dstrct );
                    info = model.facturaService.getRemesasFacturar();
                }
                
                
            }
            catch(Exception r){
                r.printStackTrace();
            }
            
            
            //Items
            int cont_items      = 0;
            boolean flag        = true;
            int max             = 0;
            String  descripcion_ruta = "EN LA RUTA ";
            //Descripcion_ruta
            descripcion_ruta    +=  ( factu.getDescripcion_ruta() != null )?factu.getDescripcion_ruta():"";
            double  tons        = 0;
            String inicio       = "";
            String fin          = "";
            String vlr_descrip  = "";
            
            vlr_descrip         =   factu.getSimbolo() + " " + factu.getVlr_freight();
            
            for( h = 0; h < info.size(); h++ ){
                factura_detalle factu_de =(factura_detalle)info.get(h);
                                                
                tons                +=  factu_de.getCantidad().doubleValue();
                
                if( h == 0 ){ inicio = ( factu_de.getFecrem() != null )?factu_de.getFecrem():"" ; }
                if( h == info.size() - 1 ){ fin = ( factu_de.getFecrem() != null )?factu_de.getFecrem():"" ; }                                
                
                String s_descripcion = ( factu_de.getDescripcion() != null )?factu_de.getDescripcion():"";
                
                if ( !factu.getHc().equals("FC") ){                
                    this.lineas        += this.Lineas(s_descripcion);
                }
                
                else{
                    this.lineas++;
                }
                //Validacion Teniendo encuenta el HC
                
                System.out.println("LINEAS " + this.lineas);
                /*Limite de Lineas*/
                if( this.lineas > 14 ){
                    flag = false;
                    System.out.println("SI " + this.lineas + " > 14 ");
                }
                else{
                    System.out.println("NO " + this.lineas + " > 14 " );
                }
                
                
                
                if( this.lineas <= 14 ){
                    
                    System.out.println("SI " + this.lineas + "<= 14");
                    tabla5  =   data.addElement("datos_items");
                    Element remesa  =   tabla5.addElement( "remesa" ).addText("ORDEN DE TRABAJO: "+((factu_de.getNumero_remesa()!=null)?factu_de.getNumero_remesa():"") );
                    
                    Element fec = tabla5.addElement( "fecha" ).addText( "FECHA: "+((factu_de.getFecrem()!=null)?factu_de.getFecrem():"" ));
                    
                    if ( !factu.getHc().equals("FC") ){
                        
                        Element placa = tabla5.addElement( "placa" ).addText( "PLACA: "+((factu_de.getPlaveh()!=null)?factu_de.getPlaveh():"" ));
                        
                        Element origen = tabla5.addElement( "origen" ).addText( (factu_de.getOrigen()!=null)?UtilFinanzas.Trunc(factu_de.getOrigen(),12):"" );
                        
                        Element destino = tabla5.addElement( "destino" ).addText( (factu_de.getDestino()!=null)?UtilFinanzas.Trunc(factu_de.getDestino(),12):"" );
                        
                        int lineas_descripcion  = this.Lineas(s_descripcion);
                        
                      /*  s_descripcion = "";
                        if( h == 0 ){
                       
                            for( int j = 0 ; j < 40 ; j++ ){
                                s_descripcion += "M";
                            }
                        }
                        else{
                            for( int j = 0 ; j < 40 ; j++ ){
                                s_descripcion += "I";
                            }
                        }*/
                        
                        Vector vec  = this.items( s_descripcion.trim(), 80 );
                        
                        for( int i = 0; i < vec.size() ; i++ ){
                            
                            String aux_descrip      = ( (String)vec.get(i) != null )?(String)vec.get(i):"";
                            Element descripcion     =   tabla5.addElement( "descripcion" );
                            descripcion.addElement("s_descripcion").addText( aux_descrip.toUpperCase() );
                            
                            System.out.println("DESCRIPCION  " + aux_descrip.toUpperCase() );
                            this.objeto                  =   descripcion;
                            
                        }//Validacion de Descripcion
                        
                        
                    }
                    
                    if( factu_de.getCantidad()  !=  null ){
                        Element cantidad    =   tabla5.addElement( "cantidad" ).addText(""+factu_de.getCantidad());
                    }
                    
                    Element unidad          =   tabla5.addElement( "unidad" ).addText( (factu_de.getUnidad()!=null)?factu_de.getUnidad():"" );
                    
                    Element valor           =   tabla5.addElement( "valor" ).addText( UtilFinanzas.customFormat("#,##0.00",Double.parseDouble(""+factu_de.getValor_itemme()),2) );
                    
                    
                    if ( !factu.getHc().equals("FC") ){
                        this.lineas_insertadas += this.Lineas(s_descripcion);
                    }
                    
                    else{
                        this.lineas_insertadas++;
                    }
                    //Validacion Teniendo encuenta el HC
                    
                    
                }//Fin del For de Validacion 14 Lineas //Agrega Items
                
                if( !flag ){
                    this.objeto.addElement("anexo").addText("Ver anexos");
                    break;
                }//Agrega Linea de Anexos
                
            }//Generacion de PDF Factura
            
            
            /*for( int j = this.lineas_insertadas ; j < 14 ; j+=2 ){
                Element tabla5          =   data.addElement("datos_items
                int aux 		=  j+2;
                if( aux > 14 ){
                    Element remesa          =   tabla5.addElement( "remesa" ).addText(".");
                }
                else{
                    Element remesa          =   tabla5.addElement( "remesa" ).addText(".");
                    Element descripcion     =   tabla5.addElement( "descripcion" );
                    descripcion.addElement("s_descripcion").addText(".");
                }
            }*/
            
            for( int j = this.lineas_insertadas ; j < 14 ; j++ ){
                Element tabla5          =   data.addElement("datos_items");
                Element remesa          =   tabla5.addElement( "remesa" ).addText(".");
            }//Puntos Faltantes
            
            
            
            Element tabla7          =   data.addElement("total");
            
            String simbolo = factu.getSimbolo()+" "+UtilFinanzas.customFormat("#,##0.00",Double.parseDouble(""+factu.getValor_facturame()),2);
            Element valortotal      =   tabla7.addElement( "valortotal" ).addText( simbolo );
            
            
            String [] vector        =   RMCantidadEnLetras.getTexto(factu.getValor_facturame(), factu.getMoneda());
            //String [] vector        =   RMCantidadEnLetras.getTexto( 2412545896.00 , factu.getMoneda());
            String a                =   "";
            for( int j = 0 ; j < vector.length; j++ ){
                a +=vector[j];
            }
            
            
            Element valorletras     = tabla7.addElement( "valorletras" ).addText(a);
            
            String  cplazo          = ( factu.getCplazo()!=null )? factu.getCplazo() : "";
            
            if( cplazo.equals("1")){
                cplazo              = cplazo + " Dia";
            }
            else{
                cplazo                  = ( !cplazo.equals("") )?cplazo + " Dias": "";
            }
            
            Element formapago       = tabla7.addElement( "formapago" ).addText(cplazo);
            Element observaciones   =  tabla7.addElement( "observaciones" ).addText((""+factu.getObservacion()!=null)?""+factu.getObservacion().toUpperCase():"" );
            
            
            /*Inicio Descripcion Factura*/
            
            String descrip_fac1      = "";
            String descrip_fac2      = "";
            String descrip_fac3      = "";
            String descrip_fac4      = "";
            String s_tons            = "TRANSP " + tons + " TON DE CARBON A " + vlr_descrip;
            
            if( factu.getHc().equals("FC") ){
                String  tasa_cambio      = "TASA DE CAMBIO : " + factu.getValor_tasa();
                String s_inicio          = "";
                String s_fin             = "";
                String s_mesi            = "";
                String s_mesf            = "";
                String mesi              = "";
                String mesf              = "";
                String s_a�o             = "";
                
                if( inicio.split("-").length > 1 && fin.split("-").length > 1 ){
                    s_mesi           = inicio.split("-")[1];
                    s_mesf           = fin.split("-")[1];
                    s_inicio         = inicio.split("-")[2];
                    s_fin            = fin.split("-")[2];
                    
                    mesi             = Util.NombreMes( Integer.parseInt( s_mesi  ) );
                    mesf             = Util.NombreMes( Integer.parseInt( s_mesf  ) );
                    
                    s_a�o            = fin.split("-")[0];
                }
                
                String  periodo          = "PERIODO : DEL " + s_inicio + " " + mesi.toUpperCase() + " AL " + s_fin + " " + mesf.toUpperCase() + " DEL " + s_a�o;
                descrip_fac1             = s_tons;
                descrip_fac2             = tasa_cambio;
                descrip_fac3             = descripcion_ruta;                
                descrip_fac4             = periodo;                                                
            }
            else{
                descrip_fac1             = (factu.getDescripcion()!=null)?factu.getDescripcion():"";
            }
            
            Element tabla4  = data.addElement("descripcionfac");
            
            Element descripcionfac1  = tabla4.addElement( "descripcion_fac1" ).addText( descrip_fac1 );
            Element descripcionfac2  = tabla4.addElement( "descripcion_fac2" ).addText( descrip_fac2 );
            Element descripcionfac3  = tabla4.addElement( "descripcion_fac3" ).addText( descrip_fac3 );
            Element descripcionfac4  = tabla4.addElement( "descripcion_fac4" ).addText( descrip_fac4 );
            
            /*Fin de Descripion de Factura*/
            
            
                        
            
            if( !flag ){
                
                sw                  =   true;
                
                int pagina          =   0;
                
                double decimales    =   (double)(info.size()-6)/24;
                
                int enteros         =   (int)((info.size()-6)/24);
                
                
                if((  decimales   -   enteros ) > 0 ){
                    enteros += 1;
                }
                
                
                while( h < info.size() ){
                    
                    pagina++;
                    Element data2       =   raiz2.addElement("data");
                    Element tabla11     =   data2.addElement("datos_clientes");
                    
                    Element cliente2    =   tabla11.addElement( "cliente" ).addText( (factu.getNomcli()!=null)?factu.getNomcli():"" );
                    Element nit2        =   tabla11.addElement( "nit" ).addText( (factu.getNit()!=null)?factu.getNit():"");
                    Element direccion2  =   tabla11.addElement( "direccion" ).addText( (factu.getDireccion()!=null)?factu.getDireccion():"");
                    Element telefono2   =   tabla11.addElement( "telefono" ).addText( (factu.getTelefono()!=null)?factu.getTelefono():"" );
                    Element ciudad2     =   tabla11.addElement( "ciudad" ).addText(""+pagina+"/"+enteros );
                    Element lugar2      =   tabla11.addElement( "lugar" ).addText( c_lugar );
                    Element fecha2      =   tabla11.addElement( "fecha" ).addText(actual);
                    Element factura2    =   tabla11.addElement( "factura" ).addText( (factu.getFactura()!=null)?factu.getFactura():"" );
                    
                    Element tabla66  = data2.addElement("barracolumna");
                    
                    int contt           =  0;
                    
                    for( int j = h  ; j < info.size() ; j++ ){
                        h = j;
                        if( contt < 24 ){
                            
                            factura_detalle factu_de    =   (factura_detalle)info.get(j);
                            Element tabla5              = data2.addElement("datos_items");
                            Element remesa              = tabla5.addElement( "remesa" ).addText("ORDEN DE TRABAJO: "+((factu_de.getNumero_remesa()!=null)?factu_de.getNumero_remesa():"") );
                            Element fec                 = tabla5.addElement( "fecha" ).addText( "FECHA: "+((factu_de.getFecrem()!=null)?factu_de.getFecrem():"" ));
                            if ( !factu.getHc().equals("FC") ){
                                Element placa           = tabla5.addElement( "placa" ).addText( "PLACA: "+((factu_de.getPlaveh()!=null)?factu_de.getPlaveh():"" ));
                                Element origen          = tabla5.addElement( "origen" ).addText( (factu_de.getOrigen()!=null)?UtilFinanzas.Trunc(factu_de.getOrigen(),12):"" );
                                Element destino         = tabla5.addElement( "destino" ).addText( (factu_de.getDestino()!=null)?UtilFinanzas.Trunc(factu_de.getDestino(),12):"" );
                                String  s_descripcion   = factu_de.getDescripcion()!=null?factu_de.getDescripcion():"" ;
                                Element descripcion     = tabla5.addElement( "descripcion" );
                                descripcion.addElement("s_descripcion").addText(s_descripcion.toUpperCase());
                            }
                            if(factu_de.getCantidad()!=null){
                                Element cantidad = tabla5.addElement( "cantidad" ).addText(""+factu_de.getCantidad());
                            }
                            Element unidad              = tabla5.addElement( "unidad" ).addText( (factu_de.getUnidad()!=null)?factu_de.getUnidad():"" );
                            Element valor               = tabla5.addElement( "valor" ).addText( UtilFinanzas.customFormat("#,##0.00",Double.parseDouble(""+factu_de.getValor_itemme()),2) );
                            contt++;
                        }
                        else{
                            
                            break;
                        }
                    }
                    if((    info.size()-h)  ==1 ){
                        h   =   info.size();
                    }
                    if( contt   <  24   ){
                        for(int y=contt;y<25;y++){
                            Element tabla5 = data2.addElement("datos_items");
                            Element remesa = tabla5.addElement( "remesa" ).addText("");
                            
                        }
                        
                    }
                    
                }
                
            }//Genera Anexos
            
            //Contadores para validacion inicializados
            this.lineas             = 0;
            this.lineas_insertadas  = 0;
            this.limite             = 0 ;
            
        }
        return raiz;
    }
    
    public void generarPDF( String ruta, Model a, String usuario) throws Exception {
        try{
            model = a;
            raiz2 = DocumentHelper.createElement("raiz");
            File xslt = new File( ruta + "Templates/FacturaPDF.xsl" );
            File pdf = new File( ruta + "pdf/FacturaPDF.pdf" );
            Element raiz = this.FilasFactura();
            
            this.PDF(xslt, pdf, raiz);
            if(sw){
                File xslt2 = new File( ruta + "Templates/AnexosFacturaPDF.xsl" );
                File pdf2 = new File( ruta + "exportar/migracion/"+usuario+"/Anexos"+Util.getFechaActual_String(6).replaceAll("/","").replaceAll(":","").replaceAll(" ","_")+".pdf" );
                this.PDF(xslt2, pdf2, raiz2);
            }
            
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }
    
    public void PDF(File xslt, File pdf, Element raiz){
        
        try {
            org.apache.fop.configuration.Configuration.put( "baseDir", "file:///" + xslt.getParent() );
            Document documento = DocumentHelper.createDocument( raiz );
            ProformaXMLReader proformaXMLReader = new ProformaXMLReader( documento );
            Driver driver = new Driver();
            Logger logger = new ConsoleLogger( ConsoleLogger.LEVEL_INFO );
            driver.setLogger( logger );
            driver.setRenderer( driver.RENDER_PDF );
            FileOutputStream salida = new FileOutputStream( pdf );
            driver.setOutputStream( salida );
            TransformerFactory factory = TransformerFactory.newInstance();
            Transformer transformer = factory.newTransformer( new StreamSource( xslt ) );
            InputSource inputSource = new InputSource();
            inputSource.setEncoding( "windows-1252" );
            SAXSource source = new SAXSource( proformaXMLReader, inputSource );
            SAXResult res = new SAXResult( driver.getContentHandler() );
            transformer.transform( source, res );
            String pageCount = "1";
            driver = new Driver();
            Logger logger1 = new ConsoleLogger( ConsoleLogger.LEVEL_INFO );
            driver.setLogger( logger1 );
            MessageHandler.setScreenLogger( logger1 );
            
            driver.setRenderer( driver.RENDER_PDF );
            salida = new FileOutputStream( pdf );
            
            driver.setOutputStream( salida );
            
            factory = TransformerFactory.newInstance();
            transformer = factory.newTransformer(new StreamSource( xslt ) );
            
            inputSource = new InputSource();
            inputSource.setEncoding( "windows-1252" );
            res = new SAXResult( driver.getContentHandler() );
            transformer.setParameter( "page-count", pageCount );
            transformer.transform( source, res );
            salida.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
    protected String obtenerTexto( Object obj ) {
        
        if ( ( obj == null ) || ( obj.toString().toLowerCase().trim().equals( "ninguno" ) ) ) {
            
            return "";
            
        } else {
            
            return obj.toString();
            
        }
    }
    
    private int Lineas( String descripcion ){
        int cont_items  = descripcion.length();
        int lineas_aux  = 0;
        if(  cont_items > 80 ){
            int c_aux = ( cont_items / 80 );
            lineas_aux += c_aux;
            int c_mod = ( cont_items % 80 );
            if( c_mod > 0 ){
                lineas_aux ++;
            }
            
        }
        else{
            lineas_aux ++;
        }
        lineas_aux ++;
        return lineas_aux;
    }
        
    
    private Vector items( String d, int lon ){
        
        Vector part = new Vector();
        if( d.length() >= lon ){
            while( d.length()>= lon ){
                part.add( d.substring( 0, lon ) );
                d = d.substring( lon , d.length() );
            }
            
        }
        
        if( d.length() > 0 ){
            part.add( d );
        }
        
        return part;
        
    }
    
    
    /**
     * Getter for property objeto.
     * @return Value of property objeto.
     */
    public org.dom4j.Element getObjeto() {
        return objeto;
    }
    
    /**
     * Setter for property objeto.
     * @param objeto New value of property objeto.
     */
    public void setObjeto(org.dom4j.Element objeto) {
        this.objeto = objeto;
    }
    
    public static void main(String[] args) {
        Model model = new Model();
        FacturaPDF factura = new FacturaPDF( "R0000409", "FINV", "", "HOSORIO" );
        try{
            factura.generarPDF("c:/",model,"HOSORIO");
        }catch(Exception e){ e.printStackTrace(); }
        ///String f, String distrito, String est, String idusu
    }
    
    //Fin del metodo
    
}
