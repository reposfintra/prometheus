
/*****************************************************************************
 * Nombre clase :                   HojaControlViajePDF                      *
 * Descripcion :                    Clase auxiliar que maneja los eventos    *
 *                                  relacionados con la generacion de los    *
 *                                  archivos XML y PDF para la Hoja de       *
 *                                  Control de Viaje                         *
 * Autor :                          Ing. Juan Manuel Escandon Perez          *
 * Fecha :                          16 de diciembre de 2005, 11:31 AM        *
 * Version :                        1.0                                      *
 * Copyright :                      Fintravalores S.A.                  *
 *****************************************************************************/

package com.tsp.pdf;
//dom4j
import org.dom4j.Element;
import org.dom4j.DocumentHelper;
import org.dom4j.Document;

//java
import java.io.*;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamSource;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.sax.SAXSource;
import org.xml.sax.InputSource;

//Fop
import org.apache.fop.apps.Driver;
import org.apache.fop.messaging.MessageHandler;
import org.apache.avalon.framework.logger.ConsoleLogger;
import org.apache.avalon.framework.logger.Logger;

import com.tsp.operation.model.beans.*;

/**
 *
 * @author  JESCANDON
 */
public class HojaControlViajePDF {
    
    /** Creates a new instance of HojaControlViaje */
    public HojaControlViajePDF() {
    }
    
    public Element HojaControlViaje( HojaControlViaje hvc ) {
        
        
        Element raiz = DocumentHelper.createElement("raiz");
        if( hvc != null ){
            Element data = raiz.addElement("data");
            Element tabla1 = data.addElement("tabla1");
            Element tabla2 = data.addElement("tabla2");
            Element tabla3 = data.addElement("tabla3");
            Element codigo = tabla1.addElement("codigor").addText((hvc.getCliente_remitente()!=null)?hvc.getCliente_remitente():"");
            Element remitente = tabla2.addElement("cliente").addText(((hvc.getCliente_remitente()!=null)?hvc.getCliente_remitente():""));
            Element ot = tabla3.addElement("ot").addText((hvc.getOt()!=null)?hvc.getOt():"");
            Element origen = tabla3.addElement("origen").addText((hvc.getOrigen_carga()!=null)?hvc.getOrigen_carga():"");
            Element oc = tabla3.addElement("oc").addText((hvc.getOc()!=null)?hvc.getOc():"");
            Element destino = tabla3.addElement("destino").addText((hvc.getDestino_final()!=null)?hvc.getDestino_final():"");
            
            Element valores = data.addElement("valores");
            Element dia = valores.addElement("dd").addText((hvc.getDia()!=null)?hvc.getDia():"");
            Element mes = valores.addElement("mm").addText((hvc.getMes()!=null)?hvc.getMes():"");
            Element ano = valores.addElement("aa").addText((hvc.getAno()!=null)?hvc.getAno():"");
            Element llegada = valores.addElement("llegada").addText("");
            Element estadol = valores.addElement("estadol").addText("");
            String precintos = (hvc.getPrecintos()!=null)?hvc.getPrecintos():"";
            Element salida = valores.addElement("salida").addText(precintos.equals(",,")?"":precintos);
            Element estados = valores.addElement("estados").addText(hvc.getPrecintos().equals(",,")?"":"OK");
            Element si = valores.addElement("si").addText("");
            Element no = valores.addElement("no").addText("");
            Element cabezote = valores.addElement("cabezote").addText((hvc.getCabezote()!=null)?hvc.getCabezote():"");
            Element trailer = valores.addElement("trailer").addText((hvc.getTrailer()!=null)?hvc.getTrailer():"");
            
            Element tipotrailer = valores.addElement("tipotrailer").addText((hvc.getTipo_trailer()!=null && !hvc.getTipo_trailer().equals(""))?hvc.getTipo_trailer():"");
            Element firma = valores.addElement("firma").addText("");
        }
        return raiz;
    }
    
    public void generarPDF(File xslt, File pdf, HojaControlViaje hvc ) throws
    Exception {
        org.apache.fop.configuration.Configuration.put("baseDir",
        "file:///" + xslt.getParent());
        //Crear Documento
        Document documento = DocumentHelper.createDocument(HojaControlViaje(hvc));
        ProformaXMLReader proformaXMLReader = new ProformaXMLReader(documento);
        
        //Establecer el formato de salida a PDF
        Driver driver = new Driver();
        //Setup logger
        Logger logger = new ConsoleLogger(ConsoleLogger.LEVEL_INFO);
        driver.setLogger(logger);
        //MessageHandler.setScreenLogger(logger);
        
        driver.setRenderer(driver.RENDER_PDF);
        
        //Cofigurar la salida
        FileOutputStream salida = new FileOutputStream(pdf);
        
        try {
            
            driver.setOutputStream(salida);
            
            //Configurar el XSLT
            TransformerFactory factory = TransformerFactory.newInstance();
            Transformer transformer = factory.newTransformer(new StreamSource(
            xslt));
            //Configurar la entrada del XSLT para la transformación
            InputSource inputSource = new InputSource();
            //inputSource.setEncoding("ISO-8859-1");
            inputSource.setEncoding("windows-1252");
            SAXSource source = new SAXSource(proformaXMLReader, inputSource);
            
            SAXResult res = new SAXResult(driver.getContentHandler());
            
            //Iniciar la transformación XSLT y el procesamiento FOP
            transformer.transform(source, res);
            
            //Obtenemos el número de páginas
            String pageCount = "1";
            //String pageCount = Integer.toString(driver.getResults().getPageCount());
            System.err.println("Número de págs:" + pageCount);
            
            //Inicializamos todo para la segunda transformación
            driver = new Driver();
            
            //Setup logger
            Logger logger1 = new ConsoleLogger(ConsoleLogger.LEVEL_INFO);
            driver.setLogger(logger1);
            MessageHandler.setScreenLogger(logger1);
            
            driver.setRenderer(driver.RENDER_PDF);
            salida = new FileOutputStream(pdf);
            driver.setOutputStream(salida);
            factory = TransformerFactory.newInstance();
            transformer = factory.newTransformer(new StreamSource(xslt));
            inputSource = new InputSource();
            //inputSource.setEncoding("ISO-8859-1");
            inputSource.setEncoding("windows-1252");
            res = new SAXResult(driver.getContentHandler());
            
            //Enviamos el parámetro
            transformer.setParameter("page-count", pageCount);
            
            //... y realizamos de nuevo la transformación
            transformer.transform(source, res);
        }
        finally {
            salida.close();
        }
    }
    
    protected String obtenerTexto(Object obj) {
        if ( (obj == null) || (obj.toString().toLowerCase().trim().equals("ninguno"))) {
            return "";
        }
        else {
            return obj.toString();
        }
    }
    
}
