package com.tsp.pdf;

import java.io.File;
import java.util.Vector;
import java.util.*;
import java.text.*;

public class Sistema {

  public Sistema() throws Exception {
  }

  public Proforma obtenerProforma(
      String leyenda1,
      String oorigen1,
      String dia1,
      String mes1,
      String ano1,
      String cargan1,
      String corigen1,
      String remitente1,
      String consignatario1,
      String destinatario1,
      String direccion1,
      String ciudad1,
      String planillas1,
      String placas1,
      String conductor1,

      Vector detalle,
      String leyenda2,

      String flete1,
      String pagadero1,
      String nit1,
      String ciudad11,
      String manifiesto1,
      String aduana1,
      String jefe1,

      Vector copias

      ) throws Exception {
    //Guardar los objetos en el objeto proforma
    Proforma proforma = new Proforma(
        leyenda1,
        oorigen1,
        dia1,
        mes1,
        ano1,
        cargan1,
        corigen1,
        remitente1,
        consignatario1,
        destinatario1,
        direccion1,
        ciudad1,
        planillas1,
        placas1,
        conductor1,

        detalle,
        leyenda2,

        flete1,
        pagadero1,
        nit1,
        ciudad11,
        manifiesto1,
        aduana1,
        jefe1,

        copias
        );
    return proforma;
  }

///////////////////////////////////////////////////////////////////////
  public Proforma2 obtenerProforma2(
      String codigor1,
      String codigoe1,
      String codigoc1,

      String rango1,
      String res1,
      String fechae1,
      String origen1,
      String destino1,

      String placa1,
      String marca1,
      String linea1,
      String modelo1,
      String modelor1,
      String serie1,
      String color1,
      String tipoc1,
      String registro1,
      String configuracion1,
      String peso1,
      String numerop1,
      String compas1,
      String vencimiento1,
      String placas11,
      String propietario1,
      String documentop1,
      String direccionp1,
      String telefonop1,
      String ciudadp1,
      String tenedor1,
      String documentot1,
      String direcciont1,
      String telefonot1,
      String ciudadt1,
      String conductor11,
      String documentoc1,
      String direccionc1,
      String telefonoc1,
      String catc1,
      String ciudadc1,

      Vector detalle,
      String leyenda2,

      String valor1,
      String retencion1,
      String descuento1,
      String flete11,
      String valora1,
      String neto1,
      String lugar1,
      String fecha1,
      String cargue1,
      String descargue1,
      String valort1,
      String seguro1,
      String poliza1,
      String vigencia1,

      String observacion1,

      Vector copias
      ) throws
      Exception {
    //Guardar los objetos en el objeto proforma
    Proforma2 proforma = new Proforma2(
        codigor1,
        codigoe1,
        codigoc1,

        rango1,
        res1,
        fechae1,
        origen1,
        destino1,

        placa1,
        marca1,
        linea1,
        modelo1,
        modelor1,
        serie1,
        color1,
        tipoc1,
        registro1,
        configuracion1,
        peso1,
        numerop1,
        compas1,
        vencimiento1,
        placas11,
        propietario1,
        documentop1,
        direccionp1,
        telefonop1,
        ciudadp1,
        tenedor1,
        documentot1,
        direcciont1,
        telefonot1,
        ciudadt1,
        conductor11,
        documentoc1,
        direccionc1,
        telefonoc1,
        catc1,
        ciudadc1,

        detalle,
        leyenda2,

        valor1,
        retencion1,
        descuento1,
        flete11,
        valora1,
        neto1,
        lugar1,
        fecha1,
        cargue1,
        descargue1,
        valort1,
        seguro1,
        poliza1,
        vigencia1,

        observacion1, copias
        );
    return proforma;
  }

  ///////////////////////////////////////////////////////////////////
  public Proforma3 obtenerProforma3(
      String ley, String or, String d, String m, String a, String ca, String co,
      String re, String con, String des, String dir, String ciu,
      String plan, String pla, String cond, Vector de, String leyenda2,
      String fle, String pa,
      String ni, String ciud,
      String man, String ad, String je, Vector copias,
      ////////////////////////
      String codigor1,
      String codigoe1,
      String codigoc1,

      String rango1,
      String res1,
      String fechae1,
      String origen1,
      String destino1,

      String placa1,
      String marca1,
      String linea1,
      String modelo1,
      String modelor1,
      String serie1,
      String color1,
      String tipoc1,
      String registro1,
      String configuracion1,
      String peso1,
      String numerop1,
      String compas1,
      String vencimiento1,
      String placas11,
      String propietario1,
      String documentop1,
      String direccionp1,
      String telefonop1,
      String ciudadp1,
      String tenedor1,
      String documentot1,
      String direcciont1,
      String telefonot1,
      String ciudadt1,
      String conductor11,
      String documentoc1,
      String direccionc1,
      String telefonoc1,
      String catc1,
      String ciudadc1,

      Vector de1,
      String leyenda21,

      String valor1,
      String retencion1,
      String descuento1,
      String flete11,
      String valora1,
      String neto1,
      String lugar1,
      String fecha1,
      String cargue1,
      String descargue1,
      String valort1,
      String seguro1,
      String poliza1,
      String vigencia1,
      String observacion1,

      Vector copias1

      ) throws Exception {

    System.err.println("detalle metodo" + de.elementAt(0));
    System.err.println("detalle metodo" + de.elementAt(1));
    //Guardar los objetos en el objeto proforma
    Proforma3 proforma3 = new Proforma3(

        ley, or, d, m, a, ca, co,
        re, con, des, dir, ciu,
        plan, pla, cond, de, leyenda2, fle, pa,
        ni, ciud,
        man, ad, je, copias,
        /////////////////////////////
        codigor1,
        codigoe1,
        codigoc1,

        rango1,
        res1,
        fechae1,
        origen1,
        destino1,

        placa1,
        marca1,
        linea1,
        modelo1,
        modelor1,
        serie1,
        color1,
        tipoc1,
        registro1,
        configuracion1,
        peso1,
        numerop1,
        compas1,
        vencimiento1,
        placas11,
        propietario1,
        documentop1,
        direccionp1,
        telefonop1,
        ciudadp1,
        tenedor1,
        documentot1,
        direcciont1,
        telefonot1,
        ciudadt1,
        conductor11,
        documentoc1,
        direccionc1,
        telefonoc1,
        catc1,
        ciudadc1,

        de1, leyenda21,

        valor1,
        retencion1,
        descuento1,
        flete11,
        valora1,
        neto1,
        lugar1,
        fecha1,
        cargue1,
        descargue1,
        valort1,
        seguro1,
        poliza1,
        vigencia1,

        observacion1,

        copias1

        );
    return proforma3;
  }

}
