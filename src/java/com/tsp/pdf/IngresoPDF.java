/*****************************************************************************
 * Nombre clase :                   RemisionMercanciaPDF.java                    *
 * Descripcion :                    Clase auxiliar que maneja los eventos    *
 *                                  relacionados con la generacion de los    *
 *                                  archivos XML y PDF para la impresion     *
 *                                  de las Planilla                          *
 * Autor :                          Ing. Juan Manuel Escandon Perez          *
 * Fecha :                          5 de enero de 2006, 02:31 PM             *
 * Version :                        1.0                                      *
 * Copyright :                      Fintravalores S.A.                  *
 *****************************************************************************/
package com.tsp.pdf;

//dom4j
import org.dom4j.Element;
import org.dom4j.DocumentHelper;
import org.dom4j.Document;

//Java
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Vector;
import java.util.Currency;
import java.text.DecimalFormat;
import java.util.Locale;
import java.io.*;

//JAXP
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerException;
import javax.xml.transform.Source;
import javax.xml.transform.Result;
import javax.xml.transform.stream.StreamSource;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.sax.SAXSource;
import org.xml.sax.InputSource;

//Fop
import org.apache.fop.apps.Driver;
import org.apache.fop.layout.Page;
import org.apache.fop.apps.XSLTInputHandler;
import org.apache.fop.messaging.MessageHandler;
import org.apache.fop.apps.Fop;

//Avalon
import org.apache.avalon.framework.ExceptionUtil;
import org.apache.avalon.framework.logger.ConsoleLogger;
import org.apache.avalon.framework.logger.Logger;

import com.tsp.operation.model.beans.*;
import java.util.*;

import com.tsp.util.*;

import com.tsp.operation.model.*;


public class IngresoPDF {
    
    
    String fechaactual;
    String hora;
    String usuario;
    String sw;
    String creation_user;
    
    Element raiz;
    Element data;
    
    Element cabecera;
    Element detalle;
    Element items;
    File xslt;
    File pdf;
    
    private UtilFinanzas util;
    
    /** Creates a new instance of PlanillaPDF */
    public IngresoPDF() {
    }
    
    public void RemisionPlantilla( ) {
        
        ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
        String fecha = "";
        String ruta = "";
        ruta = rb.getString("ruta");
        
        
        this.pdf  = new File(ruta + "/pdf/IngresoPDF.pdf");
        this.xslt = new File(ruta + "/Templates/IngresoPDF.xsl");
        
    }
    
    public void crearCabecera() {
        this.raiz        = DocumentHelper.createElement("raiz");
        this.data        = raiz.addElement("data");
        this.cabecera    = data.addElement("cabecera");
    }
    
    public void crearRaiz() {
        this.raiz        = DocumentHelper.createElement("raiz");
    }
    
    
    public void crearRemision( Model model, String usr, String numero_ingreso, String tipo, String sw, String user_creation ){
        try{
            
            this.sw = sw;
            
            this.creation_user = user_creation;
            
            model.ingreso_detalleService.datosCabecera( usr, numero_ingreso, tipo, "C");
            
            Ingreso_detalle objcab = model.ingreso_detalleService.getCabecera();
            
            this.fechaactual    =   com.tsp.util.Util.getFechaActual_String(7);
            
            this.hora           =   com.tsp.util.Util.getCurrentTime12();
            
            this.usuario        =   usr;
            
            
            model.tablaGenService.buscarDatos("TIPOING", tipo);
            
            TablaGen tg                 = model.tablaGenService.getTblgen();
            String destipo              = "";
            if( tg != null ){
                destipo = tg.getDescripcion();
            }
            
            
            String c_encabezado         = numero_ingreso + " " +destipo ;
            Element encabezado          = cabecera.addElement("encabezado").addText(c_encabezado);
            
            String ccliente             = this.validar(objcab.getCliente()) + "-" +this.validar(objcab.getNom_cliente());
            Element cliente             = cabecera.addElement("cliente").addText(ccliente);
            
            Element nomcli              = cabecera.addElement("nomcli").addText(this.validar(objcab.getNit_cliente()));
            
            String vlr                  = this.validar( util.customFormat2(objcab.getValor_ingreso_me())) + " " + this.validar(objcab.getMoneda());
            
            Element vlrconsignacion     = cabecera.addElement("vlrconsignacion").addText(vlr);
            
            Element fechaconsg          = cabecera.addElement("fechaconsg").addText(this.validar(objcab.getFecha_consignacion()));
            
            Element fechaing            = cabecera.addElement("fechaing").addText(this.validar(objcab.getCreation_date()));
            
            
            if( destipo.toUpperCase().equals("INGRESO")){
                
                Element ingreso             = cabecera.addElement("ingreso");
                
                Element banco               = ingreso.addElement("banco").addText(this.validar(objcab.getBanco()));
                
                Element sucursal            = ingreso.addElement("sucursal").addText(this.validar(objcab.getSucursal()));
                
                Element cuenta_b            = ingreso.addElement("cuenta_b").addText(this.validar(objcab.getCuenta_banco()));
                
            }
            else{
                
                Element nota                = cabecera.addElement("nota");
                
                Element cuenta              = nota.addElement("cuenta").addText(this.validar(objcab.getCuenta()));
                Element tipoc               = nota.addElement("tipo").addText(this.validar(objcab.getTipo_aux()));
                Element auxiliar            = nota.addElement("auxiliar").addText(this.validar(objcab.getAuxiliar()));
                
            }
            
            String  cfechareg           = this.validar(objcab.getCreation_date());//Fecha de Registro
            cfechareg                   = cfechareg.replaceAll("-", "");
            cfechareg                   = cfechareg.trim();
            
            Element fechareg            = cabecera.addElement("fechareg").addText(cfechareg);
            
            String cperiodo             = cfechareg;
            
            if( cperiodo.length() > 6){
                cperiodo = cperiodo.substring(0,6);
            }
            
            Element periodo             = cabecera.addElement("periodo").addText(cperiodo);
            
            String c_estado             = "";
            Element estado_1              = cabecera.addElement("estado_c1").addText("Estado");
            Element estado_2              = cabecera.addElement("estado_c2").addText(":");
            if( objcab.getReg_status().equals("ANULADO") ){
                Element estado_3              = cabecera.addElement("estado_c3").addText(objcab.getReg_status());
            }
            else{
                Element estado_3              = cabecera.addElement("estado_c3").addText("ACTIVO");
            }
            /*Tabla 2*/
            
            String vtasa                = "";
            
            
            Vector vec                 = new Vector();
            
            if( this.sw.equals("items")){
                
                vec = model.ingreso_detalleService.datosListadoItems( numero_ingreso, tipo, usr );
                objcab.setValor_tasa(0);
                for(int i = 0; i < vec.size(); i++){
                    Ingreso_detalle in = (Ingreso_detalle) vec.get(i);
                    in.setValor_saldo_factura( in.getValor_saldo_factura_me() );
                    if( !in.getMoneda_factura().equals(objcab.getMoneda()) ){
                        if( in.getValor_tasa() !=0 ){
                            vtasa               = in.getValor_tasa()!=0? "Valor Tasa : " + UtilFinanzas.customFormat("#,###.##########", in.getValor_tasa(),10 ):"";
                            objcab.setValor_tasa(  Double.parseDouble( UtilFinanzas.customFormat("#,###.##########",  (in.getValor_tasa()) ,10 ).replaceAll(",","") ) );
                            in.setValor_saldo_factura( in.getValor_saldo_factura_me() * objcab.getValor_tasa() );
                        }
                    }
                    in.setValor_total_factura( in.getValor_saldo_fing() - in.getValor_abono() );
                }
                
                model.ingreso_detalleService.setVectorIngreso_detalle( vec );
                
                
                vec                     = model.ingreso_detalleService.getVecIngreso_detalle();
            }//Fin Validacion Items
            
            Iterator it = vec.iterator();
            
            if( vec.size() > 0  ){
                
                /*Actualizar fecha Impresion*/
                model.ingresoService.update_Fecimp_Ingresoo(tipo, numero_ingreso);
                
                detalle     = data.addElement("detalle");
                
                int i = 0;
                double valor_t          = 0;
                double t_vlrfacturamf   = 0;
                double t_vlrsaldomf     = 0;
                double t_vlrsaldomi     = 0;
                double t_vlrsaldofmi    = 0;
                double t_vlrabono       = 0;
                double t_saldo          = 0;
                
                double t_rfte           = 0;
                double t_rica           = 0;
                
                String monedaFac        = "";
                String moneda           = "";
                
                String monTasa      = "";
                while(it.hasNext()){
                    
                    i++;
                    
                    Ingreso_detalle obj      = (Ingreso_detalle)it.next();
                    items       = detalle.addElement("items");
                    
                    monedaFac        = this.validar(obj.getMoneda_factura());
                    moneda           = this.validar(objcab.getMoneda());
                    
                    if( !obj.getMoneda_factura().equals( objcab.getMoneda() ) )
                        monTasa = obj.getMoneda_factura();
                    
                    Element consc            = items.addElement("consc").addText(String.valueOf(i));
                    Element factura          = items.addElement("factura").addText(this.validar(obj.getFactura()));
                    Element descripcion      = items.addElement("descripcion").addText(this.validar(obj.getDescripcion_factura()));
                    Element fechafac         = items.addElement("fechafac").addText(this.validar(obj.getFecha_factura()));
                    
                    Element cuentai          = items.addElement("cuentai").addText(this.validar(obj.getCuenta()));
                    Element auxiliari        = items.addElement("auxiliari").addText(this.validar(obj.getTipo_aux()  + "-" + this.validar(obj.getAuxiliar())));
                    
                    
                    String vlrmf             = this.validar( util.customFormat2(obj.getValor_factura_me())) + " " + this.validar(obj.getMoneda_factura());
                    Element vlrfacturamf     = items.addElement("vlrfacturamf").addText(vlrmf);
                    
                    t_vlrfacturamf           += obj.getValor_factura_me();
                    
                    String vlrsmf            = this.validar( util.customFormat2(obj.getValor_saldo_factura_me())) + " " +  this.validar(obj.getMoneda_factura());
                    Element vlrsaldomf       = items.addElement("vlrsaldomf").addText(vlrsmf);
                    
                    t_vlrsaldomf             += obj.getValor_saldo_factura_me();
                    
                    String vlrsmi            = this.validar( util.customFormat2(obj.getValor_saldo_fing())) + " " + this.validar(objcab.getMoneda());
                    Element vlrsaldomi       = items.addElement("vlrsaldomi").addText(vlrsmi);
                    
                    t_vlrsaldomi             += obj.getValor_saldo_fing();
                    
                    
                    String vrfte             = this.validar( util.customFormat2(obj.getValor_retefuente()));
                    vrfte                    = (!vrfte.equals("0.00"))?vrfte:"";
                    
                    t_rfte                   += obj.getValor_retefuente();
                    
                    vrfte                    = this.validar(obj.getCodigo_retefuente()) + " - " +  vrfte;
                    
                    Element rfte             = items.addElement("rfte").addText(vrfte);
                    
                    
                    
                    String vrica             = this.validar( util.customFormat2(obj.getValor_reteica()));
                    vrica                    = (!vrica.equals("0.00"))?vrica:"";
                    
                    t_rica                  +=  obj.getValor_reteica();
                    
                    vrica                    = this.validar(obj.getCodigo_reteica()) + " - " +  vrica;
                    
                    Element vlrrica          = items.addElement("rica").addText(vrica);
                    
                    
                    String vsfmi             = this.validar( util.customFormat2(obj.getValor_saldo_factura()));
                    Element vlrsaldofmi      = items.addElement("vlrsaldofmi").addText(vsfmi);
                    
                    t_vlrsaldofmi            += obj.getValor_saldo_factura();
                    
                    String vabono            = this.validar( util.customFormat2(obj.getValor_abono()));
                    Element vlrabono         = items.addElement("vlrabono").addText(vabono);
                    
                    t_vlrabono               += obj.getValor_abono();
                    
                    valor_t                  = obj.getValor_total_factura();
                    
                    String vsaldo            = util.customFormat2(valor_t);
                    
                    Element saldo            = items.addElement("saldo").addText(vsaldo);
                    
                    t_saldo                  += valor_t;
                    
                }//Fin de Iterator
                
                Element tasa             = cabecera.addElement("tasaconv").addText(vtasa + " " + monTasa);
                
                /*Element totales          = detalle.addElement("total");
                 
                String  vt_vlrfacturamf         = this.validar(util.customFormat2(t_vlrfacturamf));
                Element c_vlrfacturamf          = totales.addElement("t_vlrfacturamf").addText(vt_vlrfacturamf + " " + monedaFac );
                 
                String  vt_vlrsaldomf           = this.validar(util.customFormat2(t_vlrsaldomf));
                Element c_vlrsaldomf            = totales.addElement("t_vlrsaldomf").addText(vt_vlrsaldomf + " " + monedaFac );
                 
                String  vt_vlrsaldomi           = this.validar(util.customFormat2(t_vlrsaldomi));
                Element c_vlrsaldomi            = totales.addElement("t_vlrsaldomi").addText(vt_vlrsaldomi + " " + moneda );
                 
                 
                String  vt_vlrsaldofmi          = this.validar(util.customFormat2(t_vlrsaldofmi));
                Element c_vlrsaldofmi           = totales.addElement("t_vlrsaldofmi").addText( vt_vlrsaldofmi );
                 
                String  vt_vlrabono             = this.validar(util.customFormat2(t_vlrabono));
                Element c_vlrabono              = totales.addElement("t_vlrabono").addText( vt_vlrabono );
                 
                String  vt_saldo                = this.validar(util.customFormat2(t_saldo));
                Element c_saldo                 = totales.addElement("t_saldo").addText( vt_saldo );
                 
                String  vt_rfte                 = this.validar(util.customFormat2(t_rfte ));
                vt_rfte                         = (!vt_rfte.equals("0.00"))?vt_rfte:"";
                Element c_rfte                  = totales.addElement("t_rfte").addText( "     " + vt_rfte );
                 
                String  vt_rica                 = this.validar(util.customFormat2(t_rica));
                vt_rica                           = (!vt_rica.equals("0.00"))?vt_rica:"";
                Element c_rica                  = totales.addElement("t_rica").addText( "     " + vt_rica );*/
                
                double total                    = t_vlrabono - ( t_rfte + t_rica );
                
                String  vtotal                   = this.validar(util.customFormat2(total));
                vtotal                           = (!vtotal.equals("0.00"))?vtotal:"";
                //System.out.println(" TOTAL  " +  vtotal );
                Element c_total                  = cabecera.addElement("acumulado").addText( vtotal );
                
                
                
                
            }//Fin de Validacion
            
        }catch(Exception e){}
        
    }
    
    /*Jescandon */
    public void crearRemision( Model model, String datos[] , String sw, String user_creation ){
        try{
            
            for ( int l  = 0; datos!=null && l < datos.length; l++ ){
                
                String s_obj        = datos[l];
                
                if( s_obj.split(",").length > 2  ){
                    
                    Element data_aux        = this.raiz.addElement("data");
                    
                    Element cabecera_aux    = data_aux.addElement("cabecera");
                    
                    String usr              = s_obj.split(",")[0];//Distrito
                    
                    String tipo             = s_obj.split(",")[1];//Tipo
                    
                    String numero_ingreso   = s_obj.split(",")[2];//Ingreso
                    
                    this.sw = sw;
                    
                    this.creation_user = user_creation;
                    
                    model.ingreso_detalleService.datosCabecera( usr, numero_ingreso, tipo, "C");
                    
                    Ingreso_detalle objcab = model.ingreso_detalleService.getCabecera();
                    
                    this.fechaactual    =   com.tsp.util.Util.getFechaActual_String(7);
                    
                    this.hora           =   com.tsp.util.Util.getCurrentTime12();
                    
                    this.usuario        =   usr;
                    
                    
                    model.tablaGenService.buscarDatos("TIPOING", tipo);
                    
                    TablaGen tg                 = model.tablaGenService.getTblgen();
                    String destipo              = "";
                    if( tg != null ){
                        destipo = tg.getDescripcion();
                    }
                    
                    
                    String c_encabezado         = numero_ingreso + " " +destipo ;
                    Element encabezado          = cabecera_aux.addElement("encabezado").addText(c_encabezado);
                    
                    String ccliente             = this.validar(objcab.getCliente()) + "-" +this.validar(objcab.getNom_cliente());
                    Element cliente             = cabecera_aux.addElement("cliente").addText(ccliente);
                    
                    Element nomcli              = cabecera_aux.addElement("nomcli").addText(this.validar(objcab.getNit_cliente()));
                    
                    String vlr                  = this.validar( util.customFormat2(objcab.getValor_ingreso_me())) + " " + this.validar(objcab.getMoneda());
                    
                    Element vlrconsignacion     = cabecera_aux.addElement("vlrconsignacion").addText(vlr);
                    
                    Element fechaconsg          = cabecera_aux.addElement("fechaconsg").addText(this.validar(objcab.getFecha_consignacion()));
                    
                    Element fechaing            = cabecera_aux.addElement("fechaing").addText(this.validar(objcab.getCreation_date()));
                    
                    
                    if( destipo.toUpperCase().equals("INGRESO")){
                        
                        Element ingreso             = cabecera_aux.addElement("ingreso");
                        
                        Element banco               = ingreso.addElement("banco").addText(this.validar(objcab.getBanco()));
                        
                        Element sucursal            = ingreso.addElement("sucursal").addText(this.validar(objcab.getSucursal()));
                        
                        Element cuenta_b            = ingreso.addElement("cuenta_b").addText(this.validar(objcab.getCuenta_banco()));
                        
                    }
                    else{
                        
                        Element nota                = cabecera_aux.addElement("nota");
                        
                        Element cuenta              = nota.addElement("cuenta").addText(this.validar(objcab.getCuenta()));
                        Element tipoc               = nota.addElement("tipo").addText(this.validar(objcab.getTipo_aux()));
                        Element auxiliar            = nota.addElement("auxiliar").addText(this.validar(objcab.getAuxiliar()));
                        
                    }
                    
                    String  cfechareg           = this.validar(objcab.getCreation_date());//Fecha de Registro
                    cfechareg                   = cfechareg.replaceAll("-", "");
                    cfechareg                   = cfechareg.trim();
                    
                    Element fechareg            = cabecera_aux.addElement("fechareg").addText(cfechareg);
                    
                    String cperiodo             = cfechareg;
                    
                    if( cperiodo.length() > 6){
                        cperiodo = cperiodo.substring(0,6);
                    }
                    
                    Element periodo             = cabecera_aux.addElement("periodo").addText(cperiodo);
                    
                    String c_estado             = "";
                    Element estado_1              = cabecera_aux.addElement("estado_c1").addText("Estado");
                    Element estado_2              = cabecera_aux.addElement("estado_c2").addText(":");
                    if( objcab.getReg_status().equals("ANULADO") ){
                        Element estado_3              = cabecera_aux.addElement("estado_c3").addText(objcab.getReg_status());
                    }
                    else{
                        Element estado_3              = cabecera_aux.addElement("estado_c3").addText("ACTIVO");
                    }
                    /*Tabla 2*/
                    
                    String vtasa                = "";
                    
                    
                    Vector vec                 = new Vector();
                    
                    if( this.sw.equals("items")){
                        
                        vec = model.ingreso_detalleService.datosListadoItems( numero_ingreso, tipo, usr );
                        objcab.setValor_tasa(0);
                        for(int i = 0; i < vec.size(); i++){
                            Ingreso_detalle in = (Ingreso_detalle) vec.get(i);
                            in.setValor_saldo_factura( in.getValor_saldo_factura_me() );
                            if( !in.getMoneda_factura().equals(objcab.getMoneda()) ){
                                if( in.getValor_tasa() !=0 ){
                                    vtasa               = in.getValor_tasa()!=0? "Valor Tasa : " + UtilFinanzas.customFormat("#,###.##########", in.getValor_tasa(),10 ):"";
                                    objcab.setValor_tasa(  Double.parseDouble( UtilFinanzas.customFormat("#,###.##########",  (in.getValor_tasa()) ,10 ).replaceAll(",","") ) );
                                    in.setValor_saldo_factura( in.getValor_saldo_factura_me() * objcab.getValor_tasa() );
                                }
                            }
                            in.setValor_total_factura( in.getValor_saldo_fing() - in.getValor_abono() );
                        }
                        
                        model.ingreso_detalleService.setVectorIngreso_detalle( vec );
                        
                        
                        vec                     = model.ingreso_detalleService.getVecIngreso_detalle();
                    }//Fin Validacion Items
                    
                    Iterator it = vec.iterator();
                    
                    if( vec.size() > 0  ){
                        
                        /*Actualizar fecha Impresion*/
                        model.ingresoService.update_Fecimp_Ingresoo(tipo, numero_ingreso);
                        
                        Element detalle_aux     = data_aux.addElement("detalle");
                        
                        int i = 0;
                        double valor_t          = 0;
                        double t_vlrfacturamf   = 0;
                        double t_vlrsaldomf     = 0;
                        double t_vlrsaldomi     = 0;
                        double t_vlrsaldofmi    = 0;
                        double t_vlrabono       = 0;
                        double t_saldo          = 0;
                        
                        double t_rfte           = 0;
                        double t_rica           = 0;
                        
                        String monedaFac        = "";
                        String moneda           = "";
                        
                        String monTasa      = "";
                        while(it.hasNext()){
                            
                            i++;
                            
                            Ingreso_detalle obj      = (Ingreso_detalle)it.next();
                            Element items_aux        = detalle_aux.addElement("items");
                            
                            monedaFac        = this.validar(obj.getMoneda_factura());
                            moneda           = this.validar(objcab.getMoneda());
                            
                            if( !obj.getMoneda_factura().equals( objcab.getMoneda() ) )
                                monTasa = obj.getMoneda_factura();
                            
                            Element consc            = items_aux.addElement("consc").addText(String.valueOf(i));
                            Element factura          = items_aux.addElement("factura").addText(this.validar(obj.getFactura()));
                            Element descripcion      = items_aux.addElement("descripcion").addText(this.validar(obj.getDescripcion_factura()));
                            Element fechafac         = items_aux.addElement("fechafac").addText(this.validar(obj.getFecha_factura()));
                            
                            Element cuentai          = items_aux.addElement("cuentai").addText(this.validar(obj.getCuenta()));
                            Element auxiliari        = items_aux.addElement("auxiliari").addText(this.validar(obj.getTipo_aux()  + "-" + this.validar(obj.getAuxiliar())));
                            
                            
                            String vlrmf             = this.validar( util.customFormat2(obj.getValor_factura_me())) + " " + this.validar(obj.getMoneda_factura());
                            Element vlrfacturamf     = items_aux.addElement("vlrfacturamf").addText(vlrmf);
                            
                            t_vlrfacturamf           += obj.getValor_factura_me();
                            
                            String vlrsmf            = this.validar( util.customFormat2(obj.getValor_saldo_factura_me())) + " " +  this.validar(obj.getMoneda_factura());
                            Element vlrsaldomf       = items_aux.addElement("vlrsaldomf").addText(vlrsmf);
                            
                            t_vlrsaldomf             += obj.getValor_saldo_factura_me();
                            
                            String vlrsmi            = this.validar( util.customFormat2(obj.getValor_saldo_fing())) + " " + this.validar(objcab.getMoneda());
                            Element vlrsaldomi       = items_aux.addElement("vlrsaldomi").addText(vlrsmi);
                            
                            t_vlrsaldomi             += obj.getValor_saldo_fing();
                            
                            
                            String vrfte             = this.validar( util.customFormat2(obj.getValor_retefuente()));
                            vrfte                    = (!vrfte.equals("0.00"))?vrfte:"";
                            
                            t_rfte                   += obj.getValor_retefuente();
                            
                            vrfte                    = this.validar(obj.getCodigo_retefuente()) + " - " +  vrfte;
                            
                            Element rfte             = items_aux.addElement("rfte").addText(vrfte);
                            
                            
                            
                            String vrica             = this.validar( util.customFormat2(obj.getValor_reteica()));
                            vrica                    = (!vrica.equals("0.00"))?vrica:"";
                            
                            t_rica                  +=  obj.getValor_reteica();
                            
                            vrica                    = this.validar(obj.getCodigo_reteica()) + " - " +  vrica;
                            
                            Element vlrrica          = items_aux.addElement("rica").addText(vrica);
                            
                            
                            String vsfmi             = this.validar( util.customFormat2(obj.getValor_saldo_factura()));
                            Element vlrsaldofmi      = items_aux.addElement("vlrsaldofmi").addText(vsfmi);
                            
                            t_vlrsaldofmi            += obj.getValor_saldo_factura();
                            
                            String vabono            = this.validar( util.customFormat2(obj.getValor_abono()));
                            Element vlrabono         = items_aux.addElement("vlrabono").addText(vabono);
                            
                            t_vlrabono               += obj.getValor_abono();
                            
                            valor_t                  = obj.getValor_total_factura();
                            
                            String vsaldo            = util.customFormat2(valor_t);
                            
                            Element saldo            = items_aux.addElement("saldo").addText(vsaldo);
                            
                            t_saldo                  += valor_t;
                            
                        }//Fin de Iterator
                        
                        Element tasa             = cabecera_aux.addElement("tasaconv").addText(vtasa + " " + monTasa);
                        
                /*Element totales          = detalle.addElement("total");
                 
                String  vt_vlrfacturamf         = this.validar(util.customFormat2(t_vlrfacturamf));
                Element c_vlrfacturamf          = totales.addElement("t_vlrfacturamf").addText(vt_vlrfacturamf + " " + monedaFac );
                 
                String  vt_vlrsaldomf           = this.validar(util.customFormat2(t_vlrsaldomf));
                Element c_vlrsaldomf            = totales.addElement("t_vlrsaldomf").addText(vt_vlrsaldomf + " " + monedaFac );
                 
                String  vt_vlrsaldomi           = this.validar(util.customFormat2(t_vlrsaldomi));
                Element c_vlrsaldomi            = totales.addElement("t_vlrsaldomi").addText(vt_vlrsaldomi + " " + moneda );
                 
                 
                String  vt_vlrsaldofmi          = this.validar(util.customFormat2(t_vlrsaldofmi));
                Element c_vlrsaldofmi           = totales.addElement("t_vlrsaldofmi").addText( vt_vlrsaldofmi );
                 
                String  vt_vlrabono             = this.validar(util.customFormat2(t_vlrabono));
                Element c_vlrabono              = totales.addElement("t_vlrabono").addText( vt_vlrabono );
                 
                String  vt_saldo                = this.validar(util.customFormat2(t_saldo));
                Element c_saldo                 = totales.addElement("t_saldo").addText( vt_saldo );
                 
                String  vt_rfte                 = this.validar(util.customFormat2(t_rfte ));
                vt_rfte                         = (!vt_rfte.equals("0.00"))?vt_rfte:"";
                Element c_rfte                  = totales.addElement("t_rfte").addText( "     " + vt_rfte );
                 
                String  vt_rica                 = this.validar(util.customFormat2(t_rica));
                vt_rica                           = (!vt_rica.equals("0.00"))?vt_rica:"";
                Element c_rica                  = totales.addElement("t_rica").addText( "     " + vt_rica );*/
                        
                        double total                    = t_vlrabono - ( t_rfte + t_rica );
                        
                        String  vtotal                   = this.validar(util.customFormat2(total));
                        vtotal                           = (!vtotal.equals("0.00"))?vtotal:"";
                        //System.out.println(" TOTAL  " +  vtotal );
                        Element c_total                  = cabecera_aux.addElement("acumulado").addText( vtotal );
                        
                        
                        
                        
                    }//Fin de Validacion
                    
                }//Fin de Validacion del Objeto s_obj
                
            }//Fin del for
            
        }catch(Exception e){  e.printStackTrace();  }
        
    }
    
    
    public void generarPDF() throws Exception {
        org.apache.fop.configuration.Configuration.put("baseDir",
        "file:///" + xslt.getParent());
        //Crear Documento
        Document documento = DocumentHelper.createDocument(raiz);
        ProformaXMLReader proformaXMLReader = new ProformaXMLReader(documento);
        
        //Establecer el formato de salida a PDF
        Driver driver = new Driver();
        //Setup logger
        Logger logger = new ConsoleLogger(ConsoleLogger.LEVEL_INFO);
        driver.setLogger(logger);
        //MessageHandler.setScreenLogger(logger);
        
        driver.setRenderer(driver.RENDER_PDF);
        
        //Cofigurar la salida
        FileOutputStream salida = new FileOutputStream(pdf);
        
        try {
            
            driver.setOutputStream(salida);
            
            //Configurar el XSLT
            TransformerFactory factory = TransformerFactory.newInstance();
            Transformer transformer = factory.newTransformer(new StreamSource(
            xslt));
            //Configurar la entrada del XSLT para la transformación
            InputSource inputSource = new InputSource();
            //inputSource.setEncoding("ISO-8859-1");
            inputSource.setEncoding("windows-1252");
            SAXSource source = new SAXSource(proformaXMLReader, inputSource);
            
            SAXResult res = new SAXResult(driver.getContentHandler());
            
            //Iniciar la transformación XSLT y el procesamiento FOP
            transformer.transform(source, res);
            
            //Obtenemos el número de páginas
            String pageCount = Integer.toString(driver.getResults().getPageCount());
            
            //Inicializamos todo para la segunda transformación
            driver = new Driver();
            
            //Setup logger
            Logger logger1 = new ConsoleLogger(ConsoleLogger.LEVEL_INFO);
            driver.setLogger(logger1);
            MessageHandler.setScreenLogger(logger1);
            
            driver.setRenderer(driver.RENDER_PDF);
            salida = new FileOutputStream(pdf);
            driver.setOutputStream(salida);
            factory = TransformerFactory.newInstance();
            transformer = factory.newTransformer(new StreamSource(xslt));
            inputSource = new InputSource();
            //inputSource.setEncoding("ISO-8859-1");
            inputSource.setEncoding("windows-1252");
            res = new SAXResult(driver.getContentHandler());
            
            //Enviamos el parámetro
            transformer.setParameter("page-count", pageCount);
            transformer.setParameter("fecha", this.fechaactual);
            transformer.setParameter("hora", this.hora);
            transformer.setParameter("usuario", this.creation_user);
            
            //... y realizamos de nuevo la transformación
            transformer.transform(source, res);
        }
        finally {
            salida.close();
        }
    }
    
    protected String obtenerTexto(Object obj) {
        if ( (obj == null) || (obj.toString().toLowerCase().trim().equals("ninguno"))) {
            return "";
        }
        else {
            return obj.toString();
        }
    }
    
    public String validar( String obj ){
        return ( obj!=null )?obj:"";
    }
    
    public static void main(String[] arg ) throws Exception{
        
        File pdf   = new File("C:/Ingreso.pdf");
        File xslt  = new File("C:/20062701/fintravalores/Templates/IngresoPDF.xsl");
        
        IngresoPDF remision = new IngresoPDF();
        
        remision.RemisionPlantilla();
        
        remision.crearCabecera();
        
        Model model = new Model();
        
        // remision.crearRemision(model, "FINV", "IC000044", "ING");
        
        
        remision.generarPDF();
        
        
    }
    
}
