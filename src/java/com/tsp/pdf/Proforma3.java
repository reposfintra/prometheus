package com.tsp.pdf;

//dom4j
import org.dom4j.Element;
import org.dom4j.DocumentHelper;
import org.dom4j.Document;

//Java
import java.io.File;
import java.util.Vector;
import java.io.*;

//JAXP
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamSource;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.sax.SAXSource;
import org.xml.sax.InputSource;

//Fop
import org.apache.fop.apps.Driver;
import org.apache.fop.messaging.MessageHandler;
import org.apache.avalon.framework.logger.ConsoleLogger;
import org.apache.avalon.framework.logger.Logger;

public class Proforma3 {
  public Proforma3() {
  }

  private String leyenda1 = "";
  private String oorigen1 = "";
  private String dia1 = "";
  private String mes1 = "";
  private String ano1 = "";
  private String cargan1 = "";
  private String corigen1 = "";
  private String remitente1 = "";
  private String consignatario1 = "";
  private String destinatario1 = "";
  private String direccion1 = "";
  private String ciudad1 = "";
  private String planillas1 = "";
  private String placas1 = "";
  private String conductor1 = "";

  private Vector detalle;

  public void setMercancia(Vector detalle) {
    this.detalle = detalle;
  }

  public Vector getMercancia() {
    return detalle;
  }

  private String leyenda2 = "";

  private String flete1 = "";
  private String pagadero1 = "";
  private String nit1 = "";
  private String ciudad11 = "";
  private String manifiesto1 = "";
  private String aduana1 = "";
  private String jefe1 = "";

  private Vector copias;

  public void setCopias(Vector copias) {
    this.copias = copias;
  }

  public Vector getCopias() {
    return copias;
  }

  /////PARAMETROS DE LA SEGUNA TABLA//////////////////////////////
  private String codigor1 = "";
  private String codigoe1 = "";
  private String codigoc1 = "";

  private String rango1 = "";
  private String res1 = "";
  private String fechae1 = "";
  private String origen1 = "";
  private String destino1 = "";

  private String placa1 = "";
  private String marca1 = "";
  private String linea1 = "";
  private String modelo1 = "";
  private String modelor1 = "";
  private String serie1 = "";
  private String color1 = "";
  private String tipoc1 = "";
  private String registro1 = "";
  private String configuracion1 = "";
  private String peso1 = "";
  private String numerop1 = "";
  private String compas1 = "";
  private String vencimiento1 = "";
  private String placas11 = "";
  private String propietario1 = "";
  private String documentop1 = "";
  private String direccionp1 = "";
  private String telefonop1 = "";
  private String ciudadp1 = "";
  private String tenedor1 = "";
  private String documentot1 = "";
  private String direcciont1 = "";
  private String telefonot1 = "";
  private String ciudadt1 = "";
  private String conductor11 = "";
  private String documentoc1 = "";
  private String direccionc1 = "";
  private String telefonoc1 = "";
  private String catc1 = "";
  private String ciudadc1 = "";

  private Vector detalle1;

  public void setMercancia1(Vector detalle1) {
    this.detalle1 = detalle1;
  }

  public Vector getMercancia1() {
    return detalle1;
  }

  private String leyenda21 = "";

  private String valor1 = "";
  private String retencion1 = "";
  private String descuento1 = "";
  private String flete11 = "";
  private String valora1 = "";
  private String neto1 = "";
  private String lugar1 = "";
  private String fecha1 = "";
  private String cargue1 = "";
  private String descargue1 = "";
  private String valort1 = "";
  private String seguro1 = "";
  private String poliza1 = "";
  private String vigencia1 = "";

  private String observacion1 = "";

  private Vector copias1;

  public Proforma3(String ley, String or, String d, String m, String a,
                   String ca, String co,
                   String re, String con, String des, String dir, String ciu,
                   String plan, String pla, String cond, Vector de,
                   String leyenda2, String fle, String pa,
                   String ni, String ciud,
                   String man, String ad, String je, Vector copias,
                   //PArametros de la segunda tabla

                   String codigor1,
                   String codigoe1,
                   String codigoc1,

                   String rango1,
                   String res1,
                   String fechae1,
                   String origen1,
                   String destino1,

                   String placa1,
                   String marca1,
                   String linea1,
                   String modelo1,
                   String modelor1,
                   String serie1,
                   String color1,
                   String tipoc1,
                   String registro1,
                   String configuracion1,
                   String peso1,
                   String numerop1,
                   String compas1,
                   String vencimiento1,
                   String placas11,
                   String propietario1,
                   String documentop1,
                   String direccionp1,
                   String telefonop1,
                   String ciudadp1,
                   String tenedor1,
                   String documentot1,
                   String direcciont1,
                   String telefonot1,
                   String ciudadt1,
                   String conductor11,
                   String documentoc1,
                   String direccionc1,
                   String telefonoc1,
                   String catc1,
                   String ciudadc1,

                   Vector de1, String leyenda21,

                   String valor1,
                   String retencion1,
                   String descuento1,
                   String flete11,
                   String valora1,
                   String neto1,
                   String lugar1,
                   String fecha1,
                   String cargue1,
                   String descargue1,
                   String valort1,
                   String seguro1,
                   String poliza1,
                   String vigencia1,

                   String observacion1,

                   Vector copias1

                   ) {

    leyenda1 = ley;
    oorigen1 = or;
    dia1 = d;
    mes1 = m;
    ano1 = a;
    cargan1 = ca;
    corigen1 = co;
    remitente1 = re;
    consignatario1 = con;
    destinatario1 = des;
    direccion1 = dir;
    ciudad1 = ciu;
    planillas1 = plan;
    placas1 = pla;
    conductor1 = cond;

    detalle = de;
    System.err.println("detalle constructor" + detalle.elementAt(0));
    System.err.println("detalle constructor" + detalle.elementAt(1));
    this.leyenda2 = leyenda2;

    flete1 = fle;
    pagadero1 = pa;
    nit1 = ni;
    ciudad11 = ciud;
    manifiesto1 = man;
    aduana1 = ad;
    jefe1 = je;

    this.copias = copias;

    //Parametros de la sugunda tabla
    this.codigor1 = codigor1;
    this.codigoe1 = codigoe1;
    this.codigoc1 = codigoc1;

    this.rango1 = rango1;
    this.res1 = res1;
    this.fechae1 = fechae1;
    this.origen1 = origen1;
    this.destino1 = destino1;

    this.placa1 = placa1;
    this.marca1 = marca1;
    this.linea1 = linea1;
    this.modelo1 = modelo1;
    this.modelor1 = modelor1;
    this.serie1 = serie1;
    this.color1 = color1;
    this.tipoc1 = tipoc1;
    this.registro1 = registro1;
    this.configuracion1 = configuracion1;
    this.peso1 = peso1;
    this.numerop1 = numerop1;
    this.compas1 = compas1;
    this.vencimiento1 = vencimiento1;
    this.placas11 = placas11;
    this.propietario1 = propietario1;
    this.documentop1 = documentop1;
    this.direccionp1 = direccionp1;
    this.telefonop1 = telefonop1;
    this.ciudadp1 = ciudadp1;
    this.tenedor1 = tenedor1;
    this.documentot1 = documentot1;
    this.direcciont1 = direcciont1;
    this.telefonot1 = telefonot1;
    this.ciudadt1 = ciudadt1;
    this.conductor11 = conductor11;
    this.documentoc1 = documentoc1;
    this.direccionc1 = direccionc1;
    this.telefonoc1 = telefonoc1;
    this.catc1 = catc1;
    this.ciudadc1 = ciudadc1;

    detalle1 = de1;
    this.leyenda21 = leyenda21;

    this.valor1 = valor1;
    this.retencion1 = retencion1;
    this.descuento1 = descuento1;
    this.flete11 = flete11;
    this.valora1 = valora1;
    this.neto1 = neto1;
    this.lugar1 = lugar1;
    this.fecha1 = fecha1;
    this.cargue1 = cargue1;
    this.descargue1 = descargue1;
    this.valort1 = valort1;
    this.seguro1 = seguro1;
    this.poliza1 = poliza1;
    this.vigencia1 = vigencia1;

    this.observacion1 = observacion1;
    this.copias1 = copias1;

  }

  public Element obtenerElemento() {
    Element raiz = DocumentHelper.createElement("raiz");
    System.err.println("raiz");

    Element data = null;

    System.err.println("copias");
    for (int ii = 0; ii < copias.size(); ii++) {
      data = raiz.addElement("data");
      data.addElement("trafico").addText(copias.elementAt(ii) + "");

      Element tabla1 = data.addElement("tabla1");
      //  Element tabla2 = data.addElement("tabla2");
      Element tabla3 = data.addElement("tabla3");

      //Creamos un hijos para el tabla1
      Element leyenda = tabla1.addElement("leyenda").addText(leyenda1);
      Element oorigen = tabla1.addElement("oorigen").addText(oorigen1);
      Element dia = tabla1.addElement("dia").addText(dia1);
      Element mes = tabla1.addElement("mes").addText(mes1);
      Element ano = tabla1.addElement("ano").addText(ano1);
      Element cargan = tabla1.addElement("cargan").addText(cargan1);
      Element corigen = tabla1.addElement("corigen").addText(corigen1);
      Element remitente = tabla1.addElement("remitente").addText(remitente1);
      Element consignatario = tabla1.addElement("consignatario").addText(
          consignatario1);
      Element destinatario = tabla1.addElement("destinatario").addText(
          destinatario1);
      Element direccion = tabla1.addElement("direccion").addText(direccion1);
      Element ciudad = tabla1.addElement("ciudad").addText(ciudad1);
      Element planillas = tabla1.addElement("planillas").addText(planillas1);
      Element placas = tabla1.addElement("placas").addText(placas1);
      Element conductor = tabla1.addElement("conductor").addText(conductor1);

      /*
            //Creamos un hijos para el tabla2
            Element marcas = tabla2.addElement("marcas").addText("15");
            Element bultos = tabla2.addElement("bultos").addText("16");
            Element kilos = tabla2.addElement("kilos").addText("17");
            Element mercancia = tabla2.addElement("mercancia").addText("18");*/

      Element valores = data.addElement("valores");
      Element ley = valores.addElement("leyenda2").addText(leyenda2);
      System.err.println("valores");
      Element item = null;

      for (int i = 0; i < detalle.size(); i++) {
        System.err.println("entra en detalle 1  " + detalle.size() + " " +
                           detalle.elementAt(i));
        Item1 producto = (Item1) detalle.elementAt(i);
        //elemento.add(producto.obtenerElemento());
        item = valores.addElement("item");
        item.addElement("marcas").addText(obtenerTexto(producto.getMarca()));
        item.addElement("bultos").addText(obtenerTexto(producto.getBultos()));
        item.addElement("kilos").addText(obtenerTexto(producto.getKilos()));
        item.addElement("mercancia").addText(obtenerTexto(producto.getMercancia()));
        System.err.println("items" + i + " de plantilla1");
      }

      //Creamos un hijos para el tabla3/*
       Element flete = tabla3.addElement("flete").addText(flete1);
      Element pagadero = tabla3.addElement("pagadero").addText(pagadero1);
      Element nit = tabla3.addElement("nit").addText(nit1);
      Element ciudad1 = tabla3.addElement("ciudad").addText(ciudad11);
      Element manifiesto = tabla3.addElement("manifiesto").addText(manifiesto1);
      Element aduana = tabla3.addElement("aduana").addText(aduana1);
      Element jefe = tabla3.addElement("jefe").addText(jefe1);

      System.err.println("copia" + ii);
    }

    for (int ii = 0; ii < copias1.size(); ii++) {
      data = raiz.addElement("data1");
      data.addElement("trafico1").addText(copias.elementAt(ii) + "");

//Creamos un hijo para el root
      Element tabla11 = data.addElement("tabla11");
      Element tabla21 = data.addElement("tabla21");
      Element tabla31 = data.addElement("tabla31");
      Element tabla41 = data.addElement("tabla41");
      Element tabla51 = data.addElement("tabla51");
      Element tabla61 = data.addElement("tabla61");

//Creamos un hijos para el tabla1
      Element codigor = tabla11.addElement("codigor").addText(codigor1);
      Element codigoe = tabla11.addElement("codigoe").addText(codigoe1);
      Element codigoc = tabla11.addElement("codigoc").addText(codigoc1);

//Creamos un hijos para el tabla2
      Element rango = tabla21.addElement("rango").addText(rango1);
      Element res = tabla21.addElement("res").addText(res1);
      Element fechae = tabla21.addElement("fechae").addText(fechae1);
      Element origen = tabla21.addElement("origen").addText(origen1);
      Element destino = tabla21.addElement("destino").addText(destino1);

//Creamos un hijos para el tabla3
      Element placa = tabla31.addElement("placa").addText(placa1);
      Element marca = tabla31.addElement("marca").addText(marca1);
      Element linea = tabla31.addElement("linea").addText(linea1);
      Element modelo = tabla31.addElement("modelo").addText(modelo1);
      Element modelor = tabla31.addElement("modelor").addText(modelor1);
      Element serie = tabla31.addElement("serie").addText(serie1);
      Element color = tabla31.addElement("color").addText(color1);
      Element tipoc = tabla31.addElement("tipoc").addText(tipoc1);
      Element registro = tabla31.addElement("registro").addText(registro1);
      Element configuracion = tabla31.addElement("configuracion").addText(
          configuracion1);
      Element peso = tabla31.addElement("peso").addText(peso1);
      Element numerop = tabla31.addElement("numerop").addText(numerop1);
      Element compas = tabla31.addElement("compas").addText(compas1);
      Element vencimiento = tabla31.addElement("vencimiento").addText(
          vencimiento1);
      Element placas1 = tabla31.addElement("placas").addText(placas11);
      Element propietario = tabla31.addElement("propiestario").addText(
          propietario1);
      Element documentop = tabla31.addElement("documentop").addText(documentop1);
      Element direccionp = tabla31.addElement("direccionp").addText(direccionp1);
      Element telefonop = tabla31.addElement("telefonop").addText(telefonop1);
      Element ciudadp = tabla31.addElement("ciudadp").addText(ciudadp1);
      Element tenedor = tabla31.addElement("tenedor").addText(tenedor1);
      Element documentot = tabla31.addElement("documentot").addText(documentot1);
      Element direcciont = tabla31.addElement("direcciont").addText(direcciont1);
      Element telefonot = tabla31.addElement("telefonot").addText(telefonot1);
      Element ciudadt = tabla31.addElement("ciudadt").addText(ciudadt1);
      Element conductor1 = tabla31.addElement("conductor").addText(conductor11);
      Element documentoc = tabla31.addElement("documentoc").addText(documentoc1);
      Element direccionc = tabla31.addElement("direccionc").addText(direccionc1);
      Element catc = tabla31.addElement("catc").addText(catc1);
      Element ciudadc = tabla31.addElement("ciudadc").addText(ciudadc1);

      Element valores = data.addElement("valores1");
      Element ley = valores.addElement("leyenda21").addText(leyenda2);
      System.err.println("valores1");
      Element item1 = null;

      for (int i = 0; i < detalle1.size(); i++) {
        Item2 mercancia = (Item2) detalle1.elementAt(i);
        //elemento.add(producto.obtenerElemento());
        item1 = valores.addElement("item1");
        item1.addElement("numero").addText(obtenerTexto(mercancia.getNumero()));
        System.err.println(obtenerTexto(mercancia.getNumero()));
        item1.addElement("unidad").addText(obtenerTexto(mercancia.getUnidad()));
        item1.addElement("cantidad").addText(obtenerTexto(mercancia.getCantidad()));
        item1.addElement("peso").addText(obtenerTexto(mercancia.getPeso()));
        item1.addElement("codigon").addText(obtenerTexto(mercancia.getCodigoN()));
        item1.addElement("codigoe").addText(obtenerTexto(mercancia.getCodigoE()));
        item1.addElement("codigop").addText(obtenerTexto(mercancia.getCodigoP()));
        item1.addElement("producto").addText(obtenerTexto(mercancia.getProducto()));
        item1.addElement("remitente").addText(obtenerTexto(mercancia.
            getRemitente()));
        item1.addElement("destinatario").addText(obtenerTexto(mercancia.
            getDestinatario()));
        item1.addElement("destino").addText(obtenerTexto(mercancia.getDestino()));
        System.err.println("item" + i + " de plantilla2");
      }

//Creamos un hijos para el tabla5
      Element valor = tabla51.addElement("valor").addText(valor1);
      Element retencion = tabla51.addElement("retencion").addText(retencion1);
      Element descuento = tabla51.addElement("descuento").addText(descuento1);
      Element flete1 = tabla51.addElement("flete").addText(flete11);
      Element valora = tabla51.addElement("valora").addText(valora1);
      Element neto = tabla51.addElement("neto").addText(neto1);
      Element lugar = tabla51.addElement("lugar").addText(lugar1);
      Element fecha = tabla51.addElement("fecha").addText(fecha1);
      Element cargue = tabla51.addElement("cargue").addText(cargue1);
      Element descargue = tabla51.addElement("descargue").addText(descargue1);
      Element valort = tabla51.addElement("valort").addText(valort1);
      Element seguro = tabla51.addElement("seguro").addText(seguro1);
      Element poliza = tabla51.addElement("poliza").addText(poliza1);
      Element vigencia = tabla51.addElement("vigencia").addText(vigencia1);

//Creamos un hijos para el tabla6
      Element observacion = tabla61.addElement("observacion").addText(
          observacion1);
      System.err.println("copia" + ii);
    }

    return raiz;
  }

  public void generarPDF(File xslt, File pdf) throws
      Exception {
    org.apache.fop.configuration.Configuration.put("baseDir",
        "file:///" + xslt.getParent());
    System.err.println("antes");
    //Crear Documento
    Document documento = DocumentHelper.createDocument(obtenerElemento());
    ProformaXMLReader proformaXMLReader = new ProformaXMLReader(documento);
    System.err.println("despues");

    //Establecer el formato de salida a PDF
    Driver driver = new Driver();
    //Setup logger
    Logger logger = new ConsoleLogger(ConsoleLogger.LEVEL_INFO);
    driver.setLogger(logger);
    MessageHandler.setScreenLogger(logger);

    driver.setRenderer(driver.RENDER_PDF);

    //Cofigurar la salida
    FileOutputStream salida = new FileOutputStream(pdf);

    try {

      driver.setOutputStream(salida);

      //Configurar el XSLT
      TransformerFactory factory = TransformerFactory.newInstance();
      Transformer transformer = factory.newTransformer(new StreamSource(
          xslt));
      //Configurar la entrada del XSLT para la transformación
      InputSource inputSource = new InputSource();
      //inputSource.setEncoding("ISO-8859-1");
      inputSource.setEncoding("windows-1252");
      SAXSource source = new SAXSource(proformaXMLReader, inputSource);

      SAXResult res = new SAXResult(driver.getContentHandler());

      //Iniciar la transformación XSLT y el procesamiento FOP
      transformer.transform(source, res);

      //Obtenemos el número de páginas
      String pageCount = Integer.toString(driver.getResults().getPageCount());
      System.err.println("Número de págs:" + pageCount);

      //Inicializamos todo para la segunda transformación
      driver = new Driver();

      //Setup logger
      Logger logger1 = new ConsoleLogger(ConsoleLogger.LEVEL_INFO);
      driver.setLogger(logger1);
      MessageHandler.setScreenLogger(logger1);

      driver.setRenderer(driver.RENDER_PDF);
      salida = new FileOutputStream(pdf);
      driver.setOutputStream(salida);
      factory = TransformerFactory.newInstance();
      transformer = factory.newTransformer(new StreamSource(xslt));
      inputSource = new InputSource();
      //inputSource.setEncoding("ISO-8859-1");
      inputSource.setEncoding("windows-1252");
      res = new SAXResult(driver.getContentHandler());

      //Enviamos el parámetro
      transformer.setParameter("page-count", pageCount);

      //... y realizamos de nuevo la transformación
      transformer.transform(source, res);
    }
    finally {
      salida.close();
    }
  }

  protected String obtenerTexto(Object obj) {
    if ( (obj == null) || (obj.toString().toLowerCase().trim().equals("ninguno"))) {
      return "";
    }
    else {
      return obj.toString();
    }
  }

}
