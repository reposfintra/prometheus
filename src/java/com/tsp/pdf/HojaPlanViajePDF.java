
/*****************************************************************************
 * Nombre clase :                   HojaPlanViajePDF                      *
 * Descripcion :                    Clase auxiliar que maneja los eventos    *
 *                                  relacionados con la generacion de los    *
 *                                  archivos XML y PDF para la Hoja de       *
 *                                  Control de Viaje                         *
 * Autor :                          Ing. Ivan Dario Gomez Vanegas          *
 * Fecha :                          15 marzo del 2006, 11:31 AM        *
 * Version :                        1.0                                      *
 * Copyright :                      Fintravalores S.A.                  *
 *****************************************************************************/

package com.tsp.pdf;
//dom4j
import org.dom4j.Element;
import org.dom4j.DocumentHelper;
import org.dom4j.Document;

//java
import java.io.*;
import java.util.*;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamSource;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.sax.SAXSource;
import org.xml.sax.InputSource;

//Fop
import org.apache.fop.apps.Driver;
import org.apache.fop.messaging.MessageHandler;
import org.apache.avalon.framework.logger.ConsoleLogger;
import org.apache.avalon.framework.logger.Logger;

import com.tsp.operation.model.beans.*;
/**
 *
 * @author  igomez
 */
public class HojaPlanViajePDF {
    
    //private  HttpServletRequest request;
    
    /** Creates a new instance of HojaControlViaje */
    public HojaPlanViajePDF() {
        
    }
    
    public Element HojaControlViaje( PlanDeViaje planViaje ) {
        
        
        Element raiz = DocumentHelper.createElement("raiz");
        // if( hvc != null ){
        Element data    = raiz.addElement("data");
        Element tabla1  = data.addElement("cabecera");
        Element tabla2  = data.addElement("datos_viajes");
        Element tabla3  = data.addElement("datos_conductor");
        Element tabla4  = data.addElement("equipos_comunicacion");
        Element tabla5  = data.addElement("datos_familiar");
        Element tabla6  = data.addElement("propietario");
        Element tabla7  = data.addElement("alimentacion");
        Element tabla8  = data.addElement("pernotacion");
        Element tabla9  = data.addElement("parqueadero");
        Element tabla10 = data.addElement("puestoControl");
        Element tabla11 = data.addElement("comentario");
        
        
        //            PlanDeViaje planViaje        = (PlanDeViaje)request.getAttribute("planViajeObject");
        List  listAlimentacion       = planViaje.getListAlimentacion();
        List  listPernotacion        = planViaje.getListPernotacion() ;
        List  listParqueadero        = planViaje.getListParqueadero() ;
        List  listTanqueo            = planViaje.getListTanqueo()     ;
        List  listAlimentacionPlan   = planViaje.getListAlimentacionPlan();
        List  listPernotacionPlan    = planViaje.getListPernotacionPlan() ;
        List  listParqueaderoPlan    = planViaje.getListParqueaderoPlan() ;
        List  listTanqueoPlan        = planViaje.getListTanqueoPlan()     ;
        
        
        
        /*DATOS DEL VIAJE*/
        Element planilla        = tabla2.addElement("planilla"      ).addText((planViaje.getPlanilla() != null)?planViaje.getPlanilla():"");
        Element distrito        = tabla2.addElement("distrito"      ).addText((planViaje.getDistrito() != null)?planViaje.getDistrito():"");
        Element placa           = tabla2.addElement("placa"         ).addText((planViaje.getPlaca()    != null)?planViaje.getPlaca()   :"");
        Element fechaDespacho   = tabla2.addElement("fecha_despacho").addText((planViaje.getFechaDespacho()!=null)?planViaje.getFechaDespacho():"");
        Element origen          = tabla2.addElement("origen"        ).addText((planViaje.getOrigen()!=null)?planViaje.getOrigen():"");
        Element destino         = tabla2.addElement("destino"       ).addText((planViaje.getDestino()!=null)?planViaje.getDestino():"");
        Element destinatario    = tabla2.addElement("destinatario"  ).addText((planViaje.getDestinatario()!=null)?planViaje.getDestinatario():"");
        Element tipo_carga      = tabla2.addElement("tipo_carga"    ).addText((planViaje.getTipoCarga()!=null)?planViaje.getTipoCarga():"");
        Element productos       = tabla2.addElement("productos"     ).addText((planViaje.getProducto()!=null)?planViaje.getProducto():"");
        Element placa_carga     = tabla2.addElement("placa_carga"   ).addText((planViaje.getPlacaUnidad()!=null)?planViaje.getPlacaUnidad():"");
        Element contendores     = tabla2.addElement("contenedores").addText((planViaje.getContenedores()!=null)?planViaje.getContenedores():"");
        Element trailer         = tabla2.addElement("trailer").addText((planViaje.getTrailer()!=null)?planViaje.getTrailer():"");
        
        String rutas = "";
        String Pcontrol ="";
        List lisVia = planViaje.getVias();
        if(lisVia!=null && lisVia.size()>0){
            Iterator it = lisVia.iterator();
            while(it.hasNext()){
                Via  via = (Via) it.next();
                List pc = via.getPuestos();
                boolean flag = (via.getCodigo().equals(planViaje.getRuta()))?true:false;
                if( flag ){
                    rutas = "[" + via.getCodigo() + "]" + " - " + via.getDescripcion();
                    
                    for(int i=0; i<pc.size();i++){
                            String   dato   = (String) pc.get(i);
                            String[] vec    = dato.split(":");      
                            String   pcon     = vec[0]  + ":" + vec[1];
                            
                           Pcontrol += pcon+"\n";
                    }
                }
            }
        }
        Element ruta            = tabla2.addElement("ruta").addText(rutas);
        Element fechaPlanViaje  = tabla2.addElement("fecha_planviaje").addText((planViaje.getFechaPlanViaje()!=null)?planViaje.getFechaPlanViaje():"");
        String  PuestoControl   = planViaje.getPCTrafico() + ":" + planViaje.getNombrePCTrafico();
        Element proxPC          = tabla2.addElement("proxPC").addText(PuestoControl);
        String  zonaPc          = planViaje.getZona() + ":" + planViaje.getNombreZona();
        Element zonaPC          = tabla2.addElement("zonaPC").addText(zonaPc);
        Element salida          = tabla2.addElement("salida").addText((planViaje.getCaravana()!=null)?planViaje.getCaravana():"");
        Element escolta         = tabla2.addElement("escolta").addText((planViaje.getEscolta()!=null)?planViaje.getEscolta():"");
        Element usuarioCreacion = tabla2.addElement("usuario_creacion").addText((planViaje.getUsuario()!=null)?planViaje.getUsuario():"");
        Element fechaCreacion   = tabla2.addElement("fecha_creacion").addText((planViaje.getFechaCreacion()!=null)?planViaje.getFechaCreacion():"");
        Element compromisoRet   = tabla2.addElement("compromiso_retono").addText((planViaje.getRetorno()!=null)?planViaje.getRetorno():"");
        
        /*DATOS CONDUCTOR*/
        Element ced_conductor       = tabla3.addElement("ced_conductor").addText((planViaje.getCedulaConductor()!=null)?planViaje.getCedulaConductor():"");
        Element nombre_conductor    = tabla3.addElement("nom_conductor").addText((planViaje.getNameConductor()!=null)?planViaje.getNameConductor():"");
        Element ciudad_conductor   = tabla3.addElement("ciudad_conductor").addText((planViaje.getCiudadConductor()!=null)?planViaje.getCiudadConductor():"");
        Element dir_conductor   = tabla3.addElement("dir_conductor").addText((planViaje.getDirConductor()!=null)?planViaje.getDirConductor():"");
        Element tel_conductor   = tabla3.addElement("tel_conductor").addText((planViaje.getTelConductor()!=null)?planViaje.getTelConductor():"");
        
        
        /*EQUIPOS DE COMUNICACION*/
        Element radio                  = tabla4.addElement("radio").addText((planViaje.getRadio()!=null)?planViaje.getRadio():"");
        Element avantel                = tabla4.addElement("avantel").addText((planViaje.getAvantel()!=null)?planViaje.getAvantel():"");
        Element cazador                = tabla4.addElement("cazador").addText((planViaje.getCazador()!=null)?planViaje.getCazador():"");
        Element celular_comunicacion   = tabla4.addElement("celular_comunicacion").addText((planViaje.getCelular()!=null)?planViaje.getCelular():"");
        Element telefono_comunicacion  = tabla4.addElement("telefono_comunicacion").addText((planViaje.getTelefono()!=null)?planViaje.getTelefono():"");
        Element movil                  = tabla4.addElement("movil").addText((planViaje.getMovil()!=null)?planViaje.getMovil():"");
        Element otro_comunicacion      = tabla4.addElement("otro_comunicacion").addText((planViaje.getOtros()!=null)?planViaje.getOtros():"");
        
        
        /*DATOS DE UN FAMILIAR*/
        Element nombre_familiar  = tabla5.addElement("nombre_familiar").addText((planViaje.getNameFamiliar()!=null)?planViaje.getNameFamiliar():"");
        Element tel_familiar     = tabla5.addElement("tel_familiar").addText((planViaje.getTelFamiliar()!=null)?planViaje.getTelFamiliar():"");
        
        
        /*DATOS PROPIETARIO*/
        Element ced_propietario       = tabla6.addElement("ced_propietario").addText((planViaje.getCedulaPropietario()!=null)?planViaje.getCedulaPropietario():"" );
        Element nombre_propietario    = tabla6.addElement("nom_propietario").addText((planViaje.getNamePropietario()!=null)?planViaje.getNamePropietario():"");
        Element ciudad_propietario    = tabla6.addElement("ciudad_propietario").addText((planViaje.getCiudadPropietario()!=null)?planViaje.getCiudadPropietario():"");
        Element dir_propietario       = tabla6.addElement("dir_propietario").addText((planViaje.getDirPropietario()!=null)?planViaje.getDirPropietario():"");
        Element tel_propietario       = tabla6.addElement("tel_propietario").addText((planViaje.getTelPropietario()!=null)?planViaje.getTelPropietario():"");
        
        /*ALIMENTACION*/
        String restaurantes = "";
        if(listAlimentacionPlan != null){
            for(int i=0; i<listAlimentacionPlan.size();i++){
                Ubicacion  ub = ( Ubicacion)listAlimentacionPlan.get(i);
                restaurantes += ub.getDescripcion()+"\n";
            }
        }
        //System.out.println("RR " +  restaurantes );
        Element restaurantes_viaje  = tabla7.addElement("restaurantes_viaje").addText(restaurantes);
        
        /*PERNOTACION*/
        String s_pernotacion = "" ;
        if(listPernotacionPlan != null){
            for(int i=0; i<listPernotacionPlan.size();i++){
                Ubicacion  ub = ( Ubicacion)listPernotacionPlan.get(i);
                s_pernotacion += ub.getDescripcion()+"\n";
            }
        }
        Element pernotacion  = tabla7.addElement("pernotaderos_viaje").addText(s_pernotacion);
        
        
        /*PARQUEADERO*/
        String s_parqueadero = "" ;
        if(listParqueaderoPlan != null){
            for(int i=0; i<listParqueaderoPlan.size();i++){
                Ubicacion  ub = ( Ubicacion)listParqueaderoPlan.get(i);
                s_parqueadero += ub.getDescripcion()+"\n";
            }
        }
        Element parqueadero  = tabla9.addElement("parqueaderos_viaje").addText(s_parqueadero);
        
        
        /*TANQUEO*/
        String s_tanqueo = "" ;
        if( listTanqueoPlan != null  ){
            for(int i=0; i<listTanqueoPlan.size();i++){
                Ubicacion  ub = ( Ubicacion)listTanqueoPlan.get(i);
                s_tanqueo += ub.getDescripcion()+"\n";
            }
        }
        Element tanqueo  = tabla9.addElement("tanqueos_viaje").addText(s_tanqueo);
        
        /*PUESTOS DE CONTROL*/
        Element puestoControl  = tabla10.addElement("pcontrol").addText( Pcontrol);
        
        /*COMENTARIOS*/
        Element comentario  = tabla11.addElement("comentario").addText( planViaje.getComentario1());
        // }
        return raiz;
    }
    
    public void generarPDF(File xslt, File pdf, PlanDeViaje planViaje ) throws
    Exception {
        org.apache.fop.configuration.Configuration.put("baseDir",
        "file:///" + xslt.getParent());
        //Crear Documento
        Document documento = DocumentHelper.createDocument(HojaControlViaje(planViaje));
        ProformaXMLReader proformaXMLReader = new ProformaXMLReader(documento);
        
        //Establecer el formato de salida a PDF
        Driver driver = new Driver();
        //Setup logger
        Logger logger = new ConsoleLogger(ConsoleLogger.LEVEL_INFO);
        driver.setLogger(logger);
        //MessageHandler.setScreenLogger(logger);
        
        driver.setRenderer(driver.RENDER_PDF);
        
        //Cofigurar la salida
        FileOutputStream salida = new FileOutputStream(pdf);
        
        try {
            
            driver.setOutputStream(salida);
            
            //Configurar el XSLT
            TransformerFactory factory = TransformerFactory.newInstance();
            Transformer transformer = factory.newTransformer(new StreamSource(
            xslt));
            //Configurar la entrada del XSLT para la transformación
            InputSource inputSource = new InputSource();
            //inputSource.setEncoding("ISO-8859-1");
            inputSource.setEncoding("windows-1252");
            SAXSource source = new SAXSource(proformaXMLReader, inputSource);
            
            SAXResult res = new SAXResult(driver.getContentHandler());
            
            //Iniciar la transformación XSLT y el procesamiento FOP
            transformer.transform(source, res);
            
            //Obtenemos el número de páginas
            String pageCount = "1";
            //String pageCount = Integer.toString(driver.getResults().getPageCount());
            System.err.println("Número de págs:" + pageCount);
            
            //Inicializamos todo para la segunda transformación
            driver = new Driver();
            
            //Setup logger
            Logger logger1 = new ConsoleLogger(ConsoleLogger.LEVEL_INFO);
            driver.setLogger(logger1);
            MessageHandler.setScreenLogger(logger1);
            
            driver.setRenderer(driver.RENDER_PDF);
            salida = new FileOutputStream(pdf);
            driver.setOutputStream(salida);
            factory = TransformerFactory.newInstance();
            transformer = factory.newTransformer(new StreamSource(xslt));
            inputSource = new InputSource();
            //inputSource.setEncoding("ISO-8859-1");
            inputSource.setEncoding("windows-1252");
            res = new SAXResult(driver.getContentHandler());
            
            //Enviamos el parámetro
            transformer.setParameter("page-count", pageCount);
            
            //... y realizamos de nuevo la transformación
            transformer.transform(source, res);
        }
        finally {
            salida.close();
        }
    }
    
    protected String obtenerTexto(Object obj) {
        if ( (obj == null) || (obj.toString().toLowerCase().trim().equals("ninguno"))) {
            return "";
        }
        else {
            return obj.toString();
        }
    }
    
}
