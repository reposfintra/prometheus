package com.tsp.pdf;

//dom4j
import org.dom4j.Element;
import org.dom4j.DocumentHelper;
import org.dom4j.Document;

//Java
import java.io.File;
import java.util.Vector;
import java.io.*;

//JAXP
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamSource;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.sax.SAXSource;
import org.xml.sax.InputSource;

//Fop
import org.apache.fop.apps.Driver;
import org.apache.fop.messaging.MessageHandler;
import org.apache.avalon.framework.logger.ConsoleLogger;
import org.apache.avalon.framework.logger.Logger;

public class Proforma {
  public Proforma() {
  }

  private String leyenda1 = "";
  private String oorigen1 = "";
  private String dia1 = "";
  private String mes1 = "";
  private String ano1 = "";
  private String cargan1 = "";
  private String corigen1 = "";
  private String remitente1 = "";
  private String consignatario1 = "";
  private String destinatario1 = "";
  private String direccion1 = "";
  private String ciudad1 = "";
  private String planillas1 = "";
  private String placas1 = "";
  private String conductor1 = "";

  private Vector detalle;

  public void setMercancia(Vector detalle) {
    this.detalle = detalle;
  }

  public Vector getMercancia() {
    return detalle;
  }

  private String leyenda2 = "";

  private String flete1 = "";
  private String pagadero1 = "";
  private String nit1 = "";
  private String ciudad11 = "";
  private String manifiesto1 = "";
  private String aduana1 = "";
  private String jefe1 = "";

  private Vector copias;

  public void setCopias(Vector copias) {
    this.copias = copias;
  }

  public Vector getCopias() {
    return copias;
  }

  public Proforma(String ley, String or, String d, String m, String a,
                  String ca, String co,
                  String re, String con, String des, String dir, String ciu,
                  String plan, String pla, String cond, Vector de,
                  String leyenda2, String fle, String pa,
                  String ni, String ciud,
                  String man, String ad, String je, Vector copias) {
    leyenda1 = ley;
    oorigen1 = or;
    dia1 = d;
    mes1 = m;
    ano1 = a;
    cargan1 = ca;
    corigen1 = co;
    remitente1 = re;
    consignatario1 = con;
    destinatario1 = des;
    direccion1 = dir;
    ciudad1 = ciu;
    planillas1 = plan;
    placas1 = pla;
    conductor1 = cond;

    detalle = de;
    this.leyenda2 = leyenda2;

    flete1 = fle;
    pagadero1 = pa;
    nit1 = ni;
    ciudad11 = ciud;
    manifiesto1 = man;
    aduana1 = ad;
    jefe1 = je;

    this.copias = copias;

  }

  public Element obtenerElemento() {
    Element raiz = DocumentHelper.createElement("raiz");
    System.err.println("raiz");
    // Element data =  DocumentHelper.createElement("data");System.err.println("data");
    //Creamos un hijo para el root

    //Element cop = data.addElement("copias");

    // Element nc = null;
    Element data = null;

    System.err.println("copias");
    for (int ii = 0; ii < copias.size(); ii++) {
      data = raiz.addElement("data");
      data.addElement("trafico").addText(copias.elementAt(ii) + "");

      Element tabla1 = data.addElement("tabla1");
      //  Element tabla2 = data.addElement("tabla2");
      Element tabla3 = data.addElement("tabla3");

      //Creamos un hijos para el tabla1
      Element leyenda = tabla1.addElement("leyenda").addText(leyenda1);
      Element oorigen = tabla1.addElement("oorigen").addText(oorigen1);
      Element dia = tabla1.addElement("dia").addText(dia1);
      Element mes = tabla1.addElement("mes").addText(mes1);
      Element ano = tabla1.addElement("ano").addText(ano1);
      Element cargan = tabla1.addElement("cargan").addText(cargan1);
      Element corigen = tabla1.addElement("corigen").addText(corigen1);
      Element remitente = tabla1.addElement("remitente").addText(remitente1);
      Element consignatario = tabla1.addElement("consignatario").addText(
          consignatario1);
      Element destinatario = tabla1.addElement("destinatario").addText(
          destinatario1);
      Element direccion = tabla1.addElement("direccion").addText(direccion1);
      Element ciudad = tabla1.addElement("ciudad").addText(ciudad1);
      Element planillas = tabla1.addElement("planillas").addText(planillas1);
      Element placas = tabla1.addElement("placas").addText(placas1);
      Element conductor = tabla1.addElement("conductor").addText(conductor1);

      /*
            //Creamos un hijos para el tabla2
            Element marcas = tabla2.addElement("marcas").addText("15");
            Element bultos = tabla2.addElement("bultos").addText("16");
            Element kilos = tabla2.addElement("kilos").addText("17");
            Element mercancia = tabla2.addElement("mercancia").addText("18");*/

      Element valores = data.addElement("valores");
      Element ley = valores.addElement("leyenda2").addText(leyenda2);
      System.err.println("valores");
      Element item = null;

      for (int i = 0; i < detalle.size(); i++) {
        Item1 producto = (Item1) detalle.elementAt(i);
        //elemento.add(producto.obtenerElemento());
        item = valores.addElement("item");
        item.addElement("marcas").addText(obtenerTexto(producto.getMarca()));
        item.addElement("bultos").addText(obtenerTexto(producto.getBultos()));
        item.addElement("kilos").addText(obtenerTexto(producto.getKilos()));
        item.addElement("mercancia").addText(obtenerTexto(producto.getMercancia()));
        System.err.println("item" + i);
      }

      //Creamos un hijos para el tabla3/*
       Element flete = tabla3.addElement("flete").addText(flete1);
      Element pagadero = tabla3.addElement("pagadero").addText(pagadero1);
      Element nit = tabla3.addElement("nit").addText(nit1);
      Element ciudad1 = tabla3.addElement("ciudad").addText(ciudad11);
      Element manifiesto = tabla3.addElement("manifiesto").addText(manifiesto1);
      Element aduana = tabla3.addElement("aduana").addText(aduana1);
      Element jefe = tabla3.addElement("jefe").addText(jefe1);

      System.err.println("copia" + ii);
    }

    return raiz;
  }

  public void generarPDF(File xslt, File pdf) throws
      Exception {
    org.apache.fop.configuration.Configuration.put("baseDir",
        "file:///" + xslt.getParent());
    System.err.println("antes");
    //Crear Documento
    Document documento = DocumentHelper.createDocument(obtenerElemento());
    ProformaXMLReader proformaXMLReader = new ProformaXMLReader(documento);
    System.err.println("despues");

    //Establecer el formato de salida a PDF
    Driver driver = new Driver();
    //Setup logger
    Logger logger = new ConsoleLogger(ConsoleLogger.LEVEL_INFO);
    driver.setLogger(logger);
    MessageHandler.setScreenLogger(logger);

    driver.setRenderer(driver.RENDER_PDF);

    //Cofigurar la salida
    FileOutputStream salida = new FileOutputStream(pdf);

    try {

      driver.setOutputStream(salida);

      //Configurar el XSLT
      TransformerFactory factory = TransformerFactory.newInstance();
      Transformer transformer = factory.newTransformer(new StreamSource(
          xslt));
      //Configurar la entrada del XSLT para la transformación
      InputSource inputSource = new InputSource();
      //inputSource.setEncoding("ISO-8859-1");
      inputSource.setEncoding("windows-1252");
      SAXSource source = new SAXSource(proformaXMLReader, inputSource);

      SAXResult res = new SAXResult(driver.getContentHandler());

      //Iniciar la transformación XSLT y el procesamiento FOP
      transformer.transform(source, res);

      //Obtenemos el número de páginas
      String pageCount = Integer.toString(driver.getResults().getPageCount());
      System.err.println("Número de págs:" + pageCount);

      //Inicializamos todo para la segunda transformación
      driver = new Driver();

      //Setup logger
      Logger logger1 = new ConsoleLogger(ConsoleLogger.LEVEL_INFO);
      driver.setLogger(logger1);
      MessageHandler.setScreenLogger(logger1);

      driver.setRenderer(driver.RENDER_PDF);
      salida = new FileOutputStream(pdf);
      driver.setOutputStream(salida);
      factory = TransformerFactory.newInstance();
      transformer = factory.newTransformer(new StreamSource(xslt));
      inputSource = new InputSource();
      //inputSource.setEncoding("ISO-8859-1");
      inputSource.setEncoding("windows-1252");
      res = new SAXResult(driver.getContentHandler());

      //Enviamos el parámetro
      transformer.setParameter("page-count", pageCount);

      //... y realizamos de nuevo la transformación
      transformer.transform(source, res);
    }
    finally {
      salida.close();
    }
  }

  protected String obtenerTexto(Object obj) {
    if ( (obj == null) || (obj.toString().toLowerCase().trim().equals("ninguno"))) {
      return "";
    }
    else {
      return obj.toString();
    }
  }

}