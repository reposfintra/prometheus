/********************************************************************
 *      Nombre Clase.................   LibroMayorPDF.java
 *      Descripción..................   Generacion de Libro Mayor en PDF
 *      Autor........................   Osvaldo Pérez Ferrer
 *      Fecha........................   28 de Junio de 2006
 *      Versión......................   1.0
 *      Copyright....................   Transportes Sánchez Polo S.A.
 *******************************************************************/

/*
 * LibroMayorPDF.java
 *
 * Created on 28 de junio de 2006, 07:47 PM
 */

package com.tsp.pdf;

/**
 *
 * @author  Osvaldo
 */
import com.google.gson.JsonObject;
import org.dom4j.Element;
import org.dom4j.DocumentHelper;
import org.dom4j.Document;
import java.io.*;
import java.util.*;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamSource;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.sax.SAXSource;
import org.xml.sax.InputSource;
import org.apache.fop.apps.Driver;
import org.apache.fop.messaging.MessageHandler;
import org.apache.avalon.framework.logger.ConsoleLogger;
import org.apache.avalon.framework.logger.Logger;
import java.lang.*;
import com.tsp.util.*;
import com.tsp.finanzas.contab.model.DAO.*;
import com.tsp.finanzas.contab.model.beans.*;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.services.MenuService;


public class LibroMayorPDF {
    
    MayorizacionDAO mayor;
    ComprobantesDAO comp;
    String dstrct;
    com.tsp.finanzas.contab.model.Model model;
    Element raiz;
    Element data;
    File xslt;
    File pdf;
    String periodo;
    Usuario usuario = null;
    
    
    /** Creates a new instance of LibroMayorPDF */
    public LibroMayorPDF(File xslt, File pdf, Usuario u) {
        this.xslt = xslt;
        this.pdf = pdf;
        this.comp = new ComprobantesDAO(u.getBd());
        this.mayor = new MayorizacionDAO(u.getBd());
        dstrct = "";
        this.usuario = u;
    }
    
    public void crearCabecera(String periodo) throws Exception{
        
        periodo = periodo.substring(0,4)+"/"+periodo.substring(4,6);
        this.periodo = periodo;
        raiz = DocumentHelper.createElement("raiz");
        data = raiz.addElement("data");
        Element cabecera = data.addElement("cabecera");
        cabecera.addElement("periodo").setText(periodo);
        JsonObject empresa = null;
        if (this.usuario != null ) {
            empresa = (new Util()).cargarEmpresaSession(usuario.getEmpresa());
        }
        
        cabecera.addElement("empresa").setText(empresa != null ? 
                            empresa.get("descripcion").getAsString().toUpperCase() : "");
        cabecera.addElement("nit").setText(empresa != null ? 
                            empresa.get("nit").getAsString().toUpperCase() : "");
    }
        
    public void agregarElementos( Vector v, String periodo, String table ){
        
        String anio = periodo.substring(0,4);
        String m = periodo.substring(4,6);
        
        try{
            int mes = Integer.parseInt(m);
            Comprobantes c = new Comprobantes();
            
            Element tabla = data.addElement("tabla");
            
            double debito = 0;
            double credito = 0;
            double anterior = 0;
            double actual = 0;
            
            double tanterior = 0;
            double tactual = 0;
            
            String desc = "";
            
            for(int i=0;i<v.size();i++){
                
                c = (Comprobantes) v.elementAt(i);
                
                debito += c.getTotal_debito();
                credito += c.getTotal_credito();
                
                anterior = c.getValor_unit();
                actual = anterior + c.getTotal_debito() - c.getTotal_credito();
                
                tanterior += anterior;
                tactual += actual;
                Element cuenta = tabla.addElement("cuenta");
                
                cuenta.addElement("ccuenta").addText( c.getCuenta() );
                cuenta.addElement("cdescripcion").addText(c.getDetalle());
                cuenta.addElement("anterior").addText(UtilFinanzas.customFormat( "#,##0.00",anterior, 2));
                cuenta.addElement("cdebito").addText( UtilFinanzas.customFormat( "#,##0.00",c.getTotal_debito(),2));
                cuenta.addElement("ccredito").addText( UtilFinanzas.customFormat( "#,##0.00", c.getTotal_credito(), 2));
                cuenta.addElement("actual").addText(UtilFinanzas.customFormat( "#,##0.00", actual, 2));                
            }
            
            tabla.addElement( "totalanterior" ).addText( UtilFinanzas.customFormat( "#,##0.00", tanterior, 2 ));
            tabla.addElement( "totaldebito" ).addText( UtilFinanzas.customFormat( "#,##0.00", debito, 2 ));
            tabla.addElement( "totalcredito" ).addText( UtilFinanzas.customFormat( "#,##0.00", credito, 2 ));            
            tabla.addElement( "totalactual" ).addText( UtilFinanzas.customFormat( "#,##0.00", tactual, 2 ));
            
        }catch(Exception ex){
            
        }
        
    }
    
    
    public void generarPDF() throws Exception {
        
        org.apache.fop.configuration.Configuration.put( "baseDir", "file:///" + xslt.getParent() );
        
        //Crear Documento
        //Element raiz = this.HojaControlReporte(info);
        Document documento = DocumentHelper.createDocument( raiz );
        ProformaXMLReader proformaXMLReader = new ProformaXMLReader( documento );
        
        //Establecer el formato de salida a PDF
        Driver driver = new Driver();
        
        //Setup logger
        Logger logger = new ConsoleLogger( ConsoleLogger.LEVEL_INFO );
        driver.setLogger( logger );
        
        driver.setRenderer( driver.RENDER_PDF );
        
        //Cofigurar la salida
        FileOutputStream salida = new FileOutputStream( pdf );
        
        try {
            
            driver.setOutputStream( salida );
            
            //Configurar el XSLT
            TransformerFactory factory = TransformerFactory.newInstance();
            Transformer transformer = factory.newTransformer( new StreamSource( xslt ) );
            
            //Configurar la entrada del XSLT para la transformación
            InputSource inputSource = new InputSource();
            inputSource.setEncoding( "windows-1252" );
            SAXSource source = new SAXSource( proformaXMLReader, inputSource );
            SAXResult res = new SAXResult( driver.getContentHandler() );
            
            //Iniciar la transformación XSLT y el procesamiento FOP
            transformer.transform( source, res );
            
            //Obtenemos el número de páginas
            String pageCount = Integer.toString(driver.getResults().getPageCount());
            
            //Inicializamos todo para la segunda transformación
            driver = new Driver();
            
            //Setup logger
            Logger logger1 = new ConsoleLogger( ConsoleLogger.LEVEL_INFO );
            driver.setLogger( logger1 );
            MessageHandler.setScreenLogger( logger1 );
            
            driver.setRenderer( driver.RENDER_PDF );
            salida = new FileOutputStream( pdf );
            driver.setOutputStream( salida );
            factory = TransformerFactory.newInstance();
            transformer = factory.newTransformer(new StreamSource( xslt ) );
            inputSource = new InputSource();
            inputSource.setEncoding( "windows-1252" );
            res = new SAXResult( driver.getContentHandler() );
            
            //Enviamos el parámetro
            transformer.setParameter( "page-count", pageCount );
            
            //... y realizamos de nuevo la transformación
            transformer.transform( source, res );
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            salida.close();
        }
    }
    
    public void generarLibroMayor(String dstrct, String periodo, String table){
        
        this.dstrct = dstrct;
        String anio = periodo.substring(0,4);
        String m    = periodo.substring(4,6);
        try{
            int mes = Integer.parseInt(m);
            //comp.crearTablaTemporal(table);
            //System.out.println("temporal creada: "+table);
            try{
                //comp.pasarATemporal(dstrct, periodo, table);
                //System.out.println("pasado a temporal");
                this.crearCabecera(periodo);
                //ComprobantesDAO compa = new ComprobantesDAO();
                
                //Vector res = compa.resumenLibroDiario(table);
                Vector v = mayor.obtenerCuentasConSaldo(dstrct, anio, mes);
                Vector res = mayor.obtenerValoresPeriodo(v, anio, periodo.substring(4,6) );

                this.agregarElementos(res,periodo,table);
                
                //this.agregarElementos(v);
                //this.crearTotales();
                                
                this.generarPDF();
            }catch (Exception exc){
                exc.printStackTrace();
                //try{
                   // comp.dropTablaTemporal(table);
                //}catch(Exception er){}
            }
            //comp.dropTablaTemporal(table);
            //System.out.println("Temporal borrada: "+table);
            
            
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }
    
}