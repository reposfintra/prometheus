/*****************************************************************************
 * Nombre clase :                   RemisionMercanciaPDF.java                    *
 * Descripcion :                    Clase auxiliar que maneja los eventos    *
 *                                  relacionados con la generacion de los    *
 *                                  archivos XML y PDF para la impresion     *
 *                                  de las Planilla                          *
 * Autor :                          Ing. Juan Manuel Escandon Perez          *
 * Fecha :                          5 de enero de 2006, 02:31 PM             *
 * Version :                        1.0                                      *
 * Copyright :                      Fintravalores S.A.                  *
 *****************************************************************************/
package com.tsp.pdf;

//dom4j
import org.dom4j.Element;
import org.dom4j.DocumentHelper;
import org.dom4j.Document;

//Java
import java.io.File;
import java.util.Vector;
import java.io.*;

//JAXP
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamSource;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.sax.SAXSource;
import org.xml.sax.InputSource;

//Fop
import org.apache.fop.apps.Driver;
import org.apache.fop.messaging.MessageHandler;
import org.apache.avalon.framework.logger.ConsoleLogger;
import org.apache.avalon.framework.logger.Logger;

import com.tsp.operation.model.beans.*;
import java.util.*;

import com.tsp.util.*;

import com.tsp.operation.model.*;


public class IngresoMPDF {
    
    String fechaactual;
    String hora;
    String usuario;
    
    Element raiz;
    Element data;
    
    Element cabecera;
    Element detalle;
    Element items;
    File xslt;
    File pdf;
    
    private UtilFinanzas util;
    
    /** Creates a new instance of PlanillaPDF */
    public IngresoMPDF() {
    }
    
    public void RemisionPlantilla() {
        
        ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
        String fecha = "";
        String ruta = "";
        ruta = rb.getString("ruta");
        
        this.pdf  = new File(ruta + "/pdf/IngresoMPDF.pdf");
        this.xslt = new File(ruta + "/Templates/IngresoMPDF.xsl");
        
    }
    
    public void crearCabecera() {
        
        this.raiz        = DocumentHelper.createElement("raiz");
        this.data        = raiz.addElement("data");
        this.cabecera    = data.addElement("cabecera");
        //items       = detalle.addElement("items");
        
    }
    
    public void crearRaiz() {
        this.raiz = DocumentHelper.createElement("raiz");
    }
    
    
    public void crearRemision( Model model, String dstrct, String stipo , String numero_ingreso, String op, String user ){
        try{
            this.usuario        =   user;
            
            this.fechaactual    =   com.tsp.util.Util.getFechaActual_String(7);
            
            this.hora           =   com.tsp.util.Util.getCurrentTime12();
            
            model.ingresoService.buscarIngresoMiscelaneo( dstrct, stipo, numero_ingreso);
            
            /*Actualizar fecha Impresion*/
            model.ingresoService.update_Fecimp_Ingresoo(stipo, numero_ingreso);
            
            Ingreso objcab = model.ingresoService.getIngreso();
            
            model.tablaGenService.buscarDatos("TIPOING5w7", stipo);
            
            TablaGen tg                 = model.tablaGenService.getTblgen();
            String destipo              = "";
            if( tg != null ){
                destipo = tg.getDescripcion();
            }
            
            
            if( objcab != null ){
                
                String c_encabezado         = destipo + " " + numero_ingreso;
                Element encabezado          = this.cabecera.addElement("encabezado").addText(c_encabezado);
                
                String ccliente             = this.validar(objcab.getNitcli());
                
                Element cliente             = this.cabecera.addElement("cliente").addText(ccliente);
                
                Element nroingreso          = this.cabecera.addElement("nroingreso").addText(this.validar(objcab.getNum_ingreso()));
                
                Element nroconsig           = this.cabecera.addElement("nroconsig").addText(this.validar(objcab.getNro_consignacion()));
                
                Element nombre              = this.cabecera.addElement("nombre").addText(this.validar(objcab.getNomCliente()));
                
                String vlr                  = this.validar( util.customFormat2(objcab.getVlr_ingreso_me())) ;
                String moneda               = this.validar( objcab.getCodmoneda() );
                
                String  cfechareg           = this.validar(objcab.getFecha_ingreso());
                
                Element fechareg            = this.cabecera.addElement("fechareg").addText(cfechareg);
                
                String cperiodo             = this.validar(objcab.getFecha_ingreso());
                
                cperiodo                   = cperiodo.replaceAll("-", "");
                cperiodo                   = cperiodo.trim();
                
                if( cperiodo.length() > 6){
                    cperiodo = cperiodo.substring(0,6);
                }
                
                Element periodo             = this.cabecera.addElement("periodo").addText(cperiodo);
                
                Element vlrconsignacion     = this.cabecera.addElement("vlrconsignacion").addText( vlr + " " + moneda );
                
                Element fechaconsg          = this.cabecera.addElement("fechaconsg").addText(this.validar(objcab.getFecha_consignacion()));
                
                Element banco               = this.cabecera.addElement("banco").addText(this.validar(objcab.getBranch_code()));
                
                String  v_suc               = this.validar(objcab.getBank_account_no());
                
                String s_suc                = "";
                String s_cuenta             = "";
                
                String aux[] = v_suc.split("/");
                if( aux != null && aux.length > 1 ){
                    s_suc       = aux[0];
                    s_cuenta    = aux[1];
                }
                
                Element sucursal            = this.cabecera.addElement("sucursal").addText(s_suc);
                
                Element cuenta              = this.cabecera.addElement("cuenta").addText(s_cuenta);
                
                Element referencia          = this.cabecera.addElement("referencia").addText(this.validar(objcab.getNro_consignacion()));
                /*Tabla 2*/
                
                String c_estado             = "";
                Element estado_1              = this.cabecera.addElement("estado_c1").addText("Estado");
                Element estado_2              = this.cabecera.addElement("estado_c2").addText(":");
                if( objcab.getReg_status().equals("A") ){
                    Element estado_3              = this.cabecera.addElement("estado_c3").addText("ANULADO");
                }
                else{
                    Element estado_3              = this.cabecera.addElement("estado_c3").addText("ACTIVO");
                }
                
                
                if( op.equals("ALL")){
                    
                    Vector vec               = model.ingreso_detalleService.getItemsMiscelaneo();
                    
                    
                    if( vec.size() > 0 ){
                        
                        model.ingresoService.tieneItemsIngreso( objcab.getDstrct(), stipo, objcab.getNum_ingreso() );
                        
                        detalle     = data.addElement("detalle");
                        
                        double sumtotal    = 0;
                        
                        Iterator it = vec.iterator();
                        
                        int i = 0;
                        
                        while(it.hasNext()){
                            
                            i++;
                            
                            Ingreso_detalle obj         = (Ingreso_detalle)it.next();
                            
                            items                       = detalle.addElement("items");
                            
                            Element cont                = items.addElement("cont").addText(""+i);
                            
                            Element nrocuenta           = items.addElement("cuentai").addText(this.validar(obj.getCuenta()));
                            
                            Element tipo                = items.addElement("tipo").addText(this.validar(obj.getTipo_aux()));
                            
                            Element auxiliar            = items.addElement("auxiliar").addText(this.validar(obj.getAuxiliar()));
                            
                            Element descripcion         = items.addElement("descripcion").addText(this.validar(obj.getDescripcion()));
                            
                            Element tipodoc             = items.addElement("tipodoc").addText(this.validar(obj.getTipo_doc()));
                            
                            Element documento           = items.addElement("documeto").addText(this.validar(obj.getDocumento()));
                            
                            double vlring               = obj.getValor_ingreso_me();
                            
                            String svalor               = this.validar( util.customFormat2(vlring));
                            svalor                      = (!svalor.equals("0.00"))?svalor:"";
                            
                            Element valor               = items.addElement("valor").addText(svalor);
                            
                            sumtotal                    += vlring;
                            
                        }//Fin de Iterator
                        
                        Element totales                 = detalle.addElement("totales");
                        
                        String  vcumulado               = this.validar(util.customFormat2(sumtotal));
                        vcumulado                       = (!vcumulado.equals("0.0"))?vcumulado:"";
                        Element total                   = totales.addElement("total").addText(vcumulado);
                        
                    }
                }
                
            }
            
        }catch(Exception e){}
        
    }
    
    
    
    /*Metodo para la Impresion por lotes*/
    public void crearRemision( Model model, String datos[], String op, String user ){
        try{
            
            
            for ( int l  = 0; datos!=null && l < datos.length; l++ ){
                
                String s_obj        = datos[l];
                               
                if( s_obj.split(",").length > 2  ){
                    
                    Element data_aux        = this.raiz.addElement("data");
                    
                    Element cabecera_aux    = data_aux.addElement("cabecera");
                    
                    String dstrct           = s_obj.split(",")[0];
                    
                    String stipo            = s_obj.split(",")[1];
                    
                    String numero_ingreso   = s_obj.split(",")[2];
                    
                    this.usuario        =   user;
                    
                    this.fechaactual    =   com.tsp.util.Util.getFechaActual_String(7);
                    
                    this.hora           =   com.tsp.util.Util.getCurrentTime12();
                    
                    model.ingresoService.buscarIngresoMiscelaneo( dstrct, stipo, numero_ingreso);
                    
                    /*Actualizar fecha Impresion*/
                    model.ingresoService.update_Fecimp_Ingresoo(stipo, numero_ingreso);
                    
                    Ingreso objcab = model.ingresoService.getIngreso();
                    
                    model.tablaGenService.buscarDatos("TIPOING5w7", stipo);
                    
                    TablaGen tg                 = model.tablaGenService.getTblgen();
                    String destipo              = "";
                    if( tg != null ){
                        destipo = tg.getDescripcion();
                    }
                    
                    
                    
                    if( objcab != null ){
                        
                        String c_encabezado         = destipo + " " + numero_ingreso;
                        Element encabezado          = cabecera_aux.addElement("encabezado").addText(c_encabezado);
                        
                        String ccliente             = this.validar(objcab.getNitcli());
                        
                        Element cliente             = cabecera_aux.addElement("cliente").addText(ccliente);
                        
                        Element nroingreso          = cabecera_aux.addElement("nroingreso").addText(this.validar(objcab.getNum_ingreso()));
                        
                        Element nroconsig           = cabecera_aux.addElement("nroconsig").addText(this.validar(objcab.getNro_consignacion()));
                        
                        Element nombre              = cabecera_aux.addElement("nombre").addText(this.validar(objcab.getNomCliente()));
                        
                        String vlr                  = this.validar( util.customFormat2(objcab.getVlr_ingreso_me())) ;
                        String moneda               = this.validar( objcab.getCodmoneda() );
                        
                        String  cfechareg           = this.validar(objcab.getFecha_ingreso());
                        
                        Element fechareg            = cabecera_aux.addElement("fechareg").addText(cfechareg);
                        
                        String cperiodo             = this.validar(objcab.getFecha_ingreso());
                        
                        cperiodo                   = cperiodo.replaceAll("-", "");
                        cperiodo                   = cperiodo.trim();
                        
                        if( cperiodo.length() > 6){
                            cperiodo = cperiodo.substring(0,6);
                        }
                        
                        Element periodo             = cabecera_aux.addElement("periodo").addText(cperiodo);
                        
                        Element vlrconsignacion     = cabecera_aux.addElement("vlrconsignacion").addText( vlr + " " + moneda );
                        
                        Element fechaconsg          = cabecera_aux.addElement("fechaconsg").addText(this.validar(objcab.getFecha_consignacion()));
                        
                        Element banco               = cabecera_aux.addElement("banco").addText(this.validar(objcab.getBranch_code()));
                        
                        String  v_suc               = this.validar(objcab.getBank_account_no());
                        
                        String s_suc                = "";
                        String s_cuenta             = "";
                        
                        String aux[] = v_suc.split("/");
                        if( aux != null && aux.length > 1 ){
                            s_suc       = aux[0];
                            s_cuenta    = aux[1];
                        }
                        
                        Element sucursal            = cabecera_aux.addElement("sucursal").addText(s_suc);
                        
                        Element cuenta              = cabecera_aux.addElement("cuenta").addText(s_cuenta);
                        
                        Element referencia          = cabecera_aux.addElement("referencia").addText(this.validar(objcab.getNro_consignacion()));
                        /*Tabla 2*/
                        
                        String c_estado             = "";
                        Element estado_1            = cabecera_aux.addElement("estado_c1").addText("Estado");
                        Element estado_2            = cabecera_aux.addElement("estado_c2").addText(":");
                        if( objcab.getReg_status().equals("A") ){
                            Element estado_3              = cabecera_aux.addElement("estado_c3").addText("ANULADO");
                        }
                        else{
                            Element estado_3              = cabecera_aux.addElement("estado_c3").addText("ACTIVO");
                        }
                        
                        
                        if( op.equals("ALL")){
                            
                            model.ingreso_detalleService.itemsMiscelaneo( numero_ingreso, stipo, dstrct );
                            
                            Vector vec               = model.ingreso_detalleService.getItemsMiscelaneo();
                            
                            if( vec != null  ){
                                
                                if( vec.size() > 0 ){
                                    
                                    //model.ingresoService.tieneItemsIngreso( objcab.getDstrct(), stipo, objcab.getNum_ingreso() );
                                    
                                    Element detalle_aux     = data_aux.addElement("detalle");
                                    
                                    double sumtotal    = 0;
                                    
                                    Iterator it = vec.iterator();
                                    
                                    int i = 0;
                                    
                                    while(it.hasNext()){
                                        
                                        i++;
                                        
                                        Ingreso_detalle obj         = (Ingreso_detalle)it.next();
                                        
                                        Element items_aux           = detalle_aux.addElement("items");
                                        
                                        Element cont                = items_aux.addElement("cont").addText(""+i);
                                        
                                        Element nrocuenta           = items_aux.addElement("cuentai").addText(this.validar(obj.getCuenta()));
                                        
                                        Element tipo                = items_aux.addElement("tipo").addText(this.validar(obj.getTipo_aux()));
                                        
                                        Element auxiliar            = items_aux.addElement("auxiliar").addText(this.validar(obj.getAuxiliar()));
                                        
                                        Element descripcion         = items_aux.addElement("descripcion").addText(this.validar(obj.getDescripcion()));
                                        
                                        Element tipodoc             = items_aux.addElement("tipodoc").addText(this.validar(obj.getTipo_doc()));
                                        
                                        Element documento           = items_aux.addElement("documeto").addText(this.validar(obj.getDocumento()));
                                        
                                        double vlring               = obj.getValor_ingreso_me();
                                        
                                        String svalor               = this.validar( util.customFormat2(vlring));
                                        svalor                      = (!svalor.equals("0.00"))?svalor:"";
                                        
                                        Element valor               = items_aux.addElement("valor").addText(svalor);
                                        
                                        sumtotal                    += vlring;
                                        
                                    }//Fin de Iterator
                                    
                                    Element totales                 = detalle_aux.addElement("totales");
                                    
                                    String  vcumulado               = this.validar(util.customFormat2(sumtotal));
                                    vcumulado                       = (!vcumulado.equals("0.0"))?vcumulado:"";
                                    Element total                   = totales.addElement("total").addText(vcumulado);
                                    
                                }
                            }//Fin de Validacion del Vector
                        }
                        
                    }
                    
                }//Fin de Validacion del Objeto s_obj
                
            }//Fin del for del Recorrido de la Lista de los ingresos
            
        }catch(Exception e){ e.printStackTrace(); }
                
        
    }//Fin del Metodo
    
    
    public void generarPDF() throws Exception {
        org.apache.fop.configuration.Configuration.put("baseDir",
        "file:///" + xslt.getParent());
        //Crear Documento
        Document documento = DocumentHelper.createDocument(this.raiz);
        ProformaXMLReader proformaXMLReader = new ProformaXMLReader(documento);
        
        //Establecer el formato de salida a PDF
        Driver driver = new Driver();
        //Setup logger
        Logger logger = new ConsoleLogger(ConsoleLogger.LEVEL_INFO);
        driver.setLogger(logger);
        //MessageHandler.setScreenLogger(logger);
        
        driver.setRenderer(driver.RENDER_PDF);
        
        //Cofigurar la salida
        FileOutputStream salida = new FileOutputStream(pdf);
        
        try {
            
            driver.setOutputStream(salida);
            
            //Configurar el XSLT
            TransformerFactory factory = TransformerFactory.newInstance();
            Transformer transformer = factory.newTransformer(new StreamSource(
            xslt));
            //Configurar la entrada del XSLT para la transformación
            InputSource inputSource = new InputSource();
            //inputSource.setEncoding("ISO-8859-1");
            inputSource.setEncoding("windows-1252");
            SAXSource source = new SAXSource(proformaXMLReader, inputSource);
            
            SAXResult res = new SAXResult(driver.getContentHandler());
            
            //Iniciar la transformación XSLT y el procesamiento FOP
            transformer.transform(source, res);
            
            //Obtenemos el número de páginas
            String pageCount = Integer.toString(driver.getResults().getPageCount());
            
            //Inicializamos todo para la segunda transformación
            driver = new Driver();
            
            //Setup logger
            Logger logger1 = new ConsoleLogger(ConsoleLogger.LEVEL_INFO);
            driver.setLogger(logger1);
            MessageHandler.setScreenLogger(logger1);
            
            driver.setRenderer(driver.RENDER_PDF);
            salida = new FileOutputStream(pdf);
            driver.setOutputStream(salida);
            factory = TransformerFactory.newInstance();
            transformer = factory.newTransformer(new StreamSource(xslt));
            inputSource = new InputSource();
            //inputSource.setEncoding("ISO-8859-1");
            inputSource.setEncoding("windows-1252");
            res = new SAXResult(driver.getContentHandler());
            
            //Enviamos el parámetro
            transformer.setParameter("page-count", pageCount);
            transformer.setParameter("fecha", this.fechaactual);
            transformer.setParameter("hora", this.hora);
            transformer.setParameter("usuario", this.usuario);
            //... y realizamos de nuevo la transformación
            transformer.transform(source, res);
        }
        finally {
            salida.close();
        }
    }
    
    protected String obtenerTexto(Object obj) {
        if ( (obj == null) || (obj.toString().toLowerCase().trim().equals("ninguno"))) {
            return "";
        }
        else {
            return obj.toString();
        }
    }
    
    public String validar( String obj ){
        return ( obj!=null )?obj:"";
    }
    
    public static void main(String[] arg ) throws Exception{
        
        IngresoMPDF remision = new IngresoMPDF();
        
        remision.RemisionPlantilla();
        
        remision.crearCabecera();
        
        Model model = new Model();
        
        remision.generarPDF();
        
        
    }
    
}
