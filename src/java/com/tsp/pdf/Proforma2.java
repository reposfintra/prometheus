package com.tsp.pdf;

//dom4j
import org.dom4j.Element;
import org.dom4j.DocumentHelper;
import org.dom4j.Document;

//Java
import java.io.File;
import java.util.Vector;
import java.io.*;

//JAXP
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamSource;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.sax.SAXSource;
import org.xml.sax.InputSource;

//Fop
import org.apache.fop.apps.Driver;
import org.apache.fop.messaging.MessageHandler;
import org.apache.avalon.framework.logger.ConsoleLogger;
import org.apache.avalon.framework.logger.Logger;

public class Proforma2 {

  public Proforma2() {
  }

  private String codigor1 = "";
  private String codigoe1 = "";
  private String codigoc1 = "";

  private String rango1 = "";
  private String res1 = "";
  private String fechae1 = "";
  private String origen1 = "";
  private String destino1 = "";

  private String placa1 = "";
  private String marca1 = "";
  private String linea1 = "";
  private String modelo1 = "";
  private String modelor1 = "";
  private String serie1 = "";
  private String color1 = "";
  private String tipoc1 = "";
  private String registro1 = "";
  private String configuracion1 = "";
  private String peso1 = "";
  private String numerop1 = "";
  private String compas1 = "";
  private String vencimiento1 = "";
  private String placas11 = "";
  private String propietario1 = "";
  private String documentop1 = "";
  private String direccionp1 = "";
  private String telefonop1 = "";
  private String ciudadp1 = "";
  private String tenedor1 = "";
  private String documentot1 = "";
  private String direcciont1 = "";
  private String telefonot1 = "";
  private String ciudadt1 = "";
  private String conductor11 = "";
  private String documentoc1 = "";
  private String direccionc1 = "";
  private String telefonoc1 = "";
  private String catc1 = "";
  private String ciudadc1 = "";

  private Vector detalle;

  public void setMercancia(Vector detalle) {
    this.detalle = detalle;
  }

  public Vector getMercancia() {
    return detalle;
  }

  private String leyenda2 = "";

  private String valor1 = "";
  private String retencion1 = "";
  private String descuento1 = "";
  private String flete11 = "";
  private String valora1 = "";
  private String neto1 = "";
  private String lugar1 = "";
  private String fecha1 = "";
  private String cargue1 = "";
  private String descargue1 = "";
  private String valort1 = "";
  private String seguro1 = "";
  private String poliza1 = "";
  private String vigencia1 = "";

  private String observacion1 = "";

  private Vector copias;

  public Proforma2(
      String codigor1,
      String codigoe1,
      String codigoc1,

      String rango1,
      String res1,
      String fechae1,
      String origen1,
      String destino1,

      String placa1,
      String marca1,
      String linea1,
      String modelo1,
      String modelor1,
      String serie1,
      String color1,
      String tipoc1,
      String registro1,
      String configuracion1,
      String peso1,
      String numerop1,
      String compas1,
      String vencimiento1,
      String placas11,
      String propietario1,
      String documentop1,
      String direccionp1,
      String telefonop1,
      String ciudadp1,
      String tenedor1,
      String documentot1,
      String direcciont1,
      String telefonot1,
      String ciudadt1,
      String conductor11,
      String documentoc1,
      String direccionc1,
      String telefonoc1,
      String catc1,
      String ciudadc1,

      Vector de, String leyenda2,

      String valor1,
      String retencion1,
      String descuento1,
      String flete11,
      String valora1,
      String neto1,
      String lugar1,
      String fecha1,
      String cargue1,
      String descargue1,
      String valort1,
      String seguro1,
      String poliza1,
      String vigencia1,

      String observacion1,

      Vector copias
      ) {
    this.codigor1 = codigor1;
    this.codigoe1 = codigoe1;
    this.codigoc1 = codigoc1;

    this.rango1 = rango1;
    this.res1 = res1;
    this.fechae1 = fechae1;
    this.origen1 = origen1;
    this.destino1 = destino1;

    this.placa1 = placa1;
    this.marca1 = marca1;
    this.linea1 = linea1;
    this.modelo1 = modelo1;
    this.modelor1 = modelor1;
    this.serie1 = serie1;
    this.color1 = color1;
    this.tipoc1 = tipoc1;
    this.registro1 = registro1;
    this.configuracion1 = configuracion1;
    this.peso1 = peso1;
    this.numerop1 = numerop1;
    this.compas1 = compas1;
    this.vencimiento1 = vencimiento1;
    this.placas11 = placas11;
    this.propietario1 = propietario1;
    this.documentop1 = documentop1;
    this.direccionp1 = direccionp1;
    this.telefonop1 = telefonop1;
    this.ciudadp1 = ciudadp1;
    this.tenedor1 = tenedor1;
    this.documentot1 = documentot1;
    this.direcciont1 = direcciont1;
    this.telefonot1 = telefonot1;
    this.ciudadt1 = ciudadt1;
    this.conductor11 = conductor11;
    this.documentoc1 = documentoc1;
    this.direccionc1 = direccionc1;
    this.telefonoc1 = telefonoc1;
    this.catc1 = catc1;
    this.ciudadc1 = ciudadc1;

    detalle = de;
    this.leyenda2 = leyenda2;

    this.valor1 = valor1;
    this.retencion1 = retencion1;
    this.descuento1 = descuento1;
    this.flete11 = flete11;
    this.valora1 = valora1;
    this.neto1 = neto1;
    this.lugar1 = lugar1;
    this.fecha1 = fecha1;
    this.cargue1 = cargue1;
    this.descargue1 = descargue1;
    this.valort1 = valort1;
    this.seguro1 = seguro1;
    this.poliza1 = poliza1;
    this.vigencia1 = vigencia1;

    this.observacion1 = observacion1;
    this.copias = copias;

  }

  public Element obtenerElemento2() {
    Element raiz = DocumentHelper.createElement("raiz");
    System.err.println("raiz");
    //Element data = DocumentHelper.createElement("data");
    //System.err.println("data");

    Element data = null;

    System.err.println("copias");
    for (int ii = 0; ii < copias.size(); ii++) {
      data = raiz.addElement("data");
      data.addElement("trafico").addText(copias.elementAt(ii) + "");

//Creamos un hijo para el root
      Element tabla1 = data.addElement("tabla1");
      Element tabla2 = data.addElement("tabla2");
      Element tabla3 = data.addElement("tabla3");
      Element tabla4 = data.addElement("tabla4");
      Element tabla5 = data.addElement("tabla5");
      Element tabla6 = data.addElement("tabla6");

//Creamos un hijos para el tabla1
      Element codigor = tabla1.addElement("codigor").addText(codigor1);
      Element codigoe = tabla1.addElement("codigoe").addText(codigoe1);
      Element codigoc = tabla1.addElement("codigoc").addText(codigoc1);

//Creamos un hijos para el tabla2
      Element rango = tabla2.addElement("rango").addText(rango1);
      Element res = tabla2.addElement("res").addText(res1);
      Element fechae = tabla2.addElement("fechae").addText(fechae1);
      Element origen = tabla2.addElement("origen").addText(origen1);
      Element destino = tabla2.addElement("destino").addText(destino1);

//Creamos un hijos para el tabla3
      Element placa = tabla3.addElement("placa").addText(placa1);
      Element marca = tabla3.addElement("marca").addText(marca1);
      Element linea = tabla3.addElement("linea").addText(linea1);
      Element modelo = tabla3.addElement("modelo").addText(modelo1);
      Element modelor = tabla3.addElement("modelor").addText(modelor1);
      Element serie = tabla3.addElement("serie").addText(serie1);
      Element color = tabla3.addElement("color").addText(color1);
      Element tipoc = tabla3.addElement("tipoc").addText(tipoc1);
      Element registro = tabla3.addElement("registro").addText(registro1);
      Element configuracion = tabla3.addElement("configuracion").addText(
          configuracion1);
      Element peso = tabla3.addElement("peso").addText(peso1);
      Element numerop = tabla3.addElement("numerop").addText(numerop1);
      Element compas = tabla3.addElement("compas").addText(compas1);
      Element vencimiento = tabla3.addElement("vencimiento").addText(
          vencimiento1);
      Element placas1 = tabla3.addElement("placas").addText(placas11);
      Element propietario = tabla3.addElement("propiestario").addText(
          propietario1);
      Element documentop = tabla3.addElement("documentop").addText(documentop1);
      Element direccionp = tabla3.addElement("direccionp").addText(direccionp1);
      Element telefonop = tabla3.addElement("telefonop").addText(telefonop1);
      Element ciudadp = tabla3.addElement("ciudadp").addText(ciudadp1);
      Element tenedor = tabla3.addElement("tenedor").addText(tenedor1);
      Element documentot = tabla3.addElement("documentot").addText(documentot1);
      Element direcciont = tabla3.addElement("direcciont").addText(direcciont1);
      Element telefonot = tabla3.addElement("telefonot").addText(telefonot1);
      Element ciudadt = tabla3.addElement("ciudadt").addText(ciudadt1);
      Element conductor1 = tabla3.addElement("conductor").addText(conductor11);
      Element documentoc = tabla3.addElement("documentoc").addText(documentoc1);
      Element direccionc = tabla3.addElement("direccionc").addText(direccionc1);
      Element catc = tabla3.addElement("catc").addText(catc1);
      Element ciudadc = tabla3.addElement("ciudadc").addText(ciudadc1);
      /*
//Creamos un hijos para el tabla4/*
           Element numero = tabla4.addElement("numero").addText("37");
           Element unidad = tabla4.addElement("unidad").addText("38");
           Element cantidad = tabla4.addElement("cantidad").addText("39");
           Element peso1 = tabla4.addElement("peso").addText("40");
           Element codigon = tabla4.addElement("codigon").addText("41");
           Element codigoe1 = tabla4.addElement("codigoe").addText("42");
           Element codigop = tabla4.addElement("codigop").addText("43");
           Element producto = tabla4.addElement("producto").addText("44");
           Element remitente1 = tabla4.addElement("remitente").addText("45");
            Element destinatario1 = tabla4.addElement("destinatario").addText("46");
           Element destino1 = tabla4.addElement("destino").addText("47");
        */
      Element valores = data.addElement("valores");
      Element ley = valores.addElement("leyenda2").addText(leyenda2);
      System.err.println("valores");
      Element item = null;

      for (int i = 0; i < detalle.size(); i++) {
        Item2 mercancia = (Item2) detalle.elementAt(i);
        //elemento.add(producto.obtenerElemento());
        item = valores.addElement("item");
        item.addElement("numero").addText(obtenerTexto(mercancia.getNumero()));
        item.addElement("unidad").addText(obtenerTexto(mercancia.getUnidad()));
        item.addElement("cantidad").addText(obtenerTexto(mercancia.getCantidad()));
        item.addElement("peso").addText(obtenerTexto(mercancia.getPeso()));
        item.addElement("codigon").addText(obtenerTexto(mercancia.getCodigoN()));
        item.addElement("codigoe").addText(obtenerTexto(mercancia.getCodigoE()));
        item.addElement("codigop").addText(obtenerTexto(mercancia.getCodigoP()));
        item.addElement("producto").addText(obtenerTexto(mercancia.getProducto()));
        item.addElement("remitente").addText(obtenerTexto(mercancia.
            getRemitente()));
        item.addElement("destinatario").addText(obtenerTexto(mercancia.
            getDestinatario()));
        item.addElement("destino").addText(obtenerTexto(mercancia.getDestino()));
        System.err.println("item" + i);
      }

//Creamos un hijos para el tabla5
      Element valor = tabla5.addElement("valor").addText(valor1);
      Element retencion = tabla5.addElement("retencion").addText(retencion1);
      Element descuento = tabla5.addElement("descuento").addText(descuento1);
      Element flete1 = tabla5.addElement("flete").addText(flete11);
      Element valora = tabla5.addElement("valora").addText(valora1);
      Element neto = tabla5.addElement("neto").addText(neto1);
      Element lugar = tabla5.addElement("lugar").addText(lugar1);
      Element fecha = tabla5.addElement("fecha").addText(fecha1);
      Element cargue = tabla5.addElement("cargue").addText(cargue1);
      Element descargue = tabla5.addElement("descargue").addText(descargue1);
      Element valort = tabla5.addElement("valort").addText(valort1);
      Element seguro = tabla5.addElement("seguro").addText(seguro1);
      Element poliza = tabla5.addElement("poliza").addText(poliza1);
      Element vigencia = tabla5.addElement("vigencia").addText(vigencia1);

//Creamos un hijos para el tabla6
      Element observacion = tabla6.addElement("observacion").addText(
          observacion1);
      System.err.println("copia" + ii);
    }
    return raiz;
  }

  public void generarPDF(File xslt, File pdf) throws
      Exception {
    org.apache.fop.configuration.Configuration.put("baseDir",
        "file:///" + xslt.getParent());
    System.err.println("antes");
    //Crear Documento
    Document documento = DocumentHelper.createDocument(obtenerElemento2());
    ProformaXMLReader proformaXMLReader = new ProformaXMLReader(documento);
    System.err.println("despues");

    //Establecer el formato de salida a PDF
    Driver driver = new Driver();
    //Setup logger
    Logger logger = new ConsoleLogger(ConsoleLogger.LEVEL_INFO);
    driver.setLogger(logger);
    //MessageHandler.setScreenLogger(logger);
    System.err.println("hola");

    driver.setRenderer(driver.RENDER_PDF);

    //Cofigurar la salida
    FileOutputStream salida = new FileOutputStream(pdf);

    try {

      driver.setOutputStream(salida);

      //Configurar el XSLT
      TransformerFactory factory = TransformerFactory.newInstance();
      Transformer transformer = factory.newTransformer(new StreamSource(
          xslt));
      //Configurar la entrada del XSLT para la transformación
      InputSource inputSource = new InputSource();
      //inputSource.setEncoding("ISO-8859-1");
      inputSource.setEncoding("windows-1252");
      SAXSource source = new SAXSource(proformaXMLReader, inputSource);

      SAXResult res = new SAXResult(driver.getContentHandler());

      //Iniciar la transformación XSLT y el procesamiento FOP
      transformer.transform(source, res);

      //Obtenemos el número de páginas
      String pageCount = Integer.toString(driver.getResults().getPageCount());
      System.err.println("Número de págs:" + pageCount);

      //Inicializamos todo para la segunda transformación
      driver = new Driver();

      //Setup logger
      Logger logger1 = new ConsoleLogger(ConsoleLogger.LEVEL_INFO);
      driver.setLogger(logger1);
      MessageHandler.setScreenLogger(logger1);

      driver.setRenderer(driver.RENDER_PDF);
      salida = new FileOutputStream(pdf);
      driver.setOutputStream(salida);
      factory = TransformerFactory.newInstance();
      transformer = factory.newTransformer(new StreamSource(xslt));
      inputSource = new InputSource();
      //inputSource.setEncoding("ISO-8859-1");
      inputSource.setEncoding("windows-1252");
      res = new SAXResult(driver.getContentHandler());

      //Enviamos el parámetro
      transformer.setParameter("page-count", pageCount);

      //... y realizamos de nuevo la transformación
      transformer.transform(source, res);
    }
    finally {
      salida.close();
    }
  }

  protected String obtenerTexto(Object obj) {
    if ( (obj == null) || (obj.toString().toLowerCase().trim().equals("ninguno"))) {
      return "";
    }
    else {
      return obj.toString();
    }
  }

}
