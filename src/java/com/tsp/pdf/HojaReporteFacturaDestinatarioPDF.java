/*****************************************************************************
 * Nombre clase :                   HojaReporteFacturaDestinatarioPDF        *
 * Descripcion :                    Clase auxiliar que maneja los eventos    *
 *                                  relacionados con la generacion de los    *
 *                                  archivos XML y PDF para la Hoja de       *
 *                                  Reporte Factura Destinatario             *
 * Autor :                          LREALES                                  *
 * Fecha :                          03 abril del 2006, 01:27 PM              *
 * Version :                        1.0                                      *
 * Copyright :                      Fintravalores S.A.                  *
 *****************************************************************************/

package com.tsp.pdf;

import org.dom4j.Element;
import org.dom4j.DocumentHelper;
import org.dom4j.Document;
import java.io.*;
import java.util.*;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamSource;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.sax.SAXSource;
import org.xml.sax.InputSource;
import org.apache.fop.apps.Driver;
import org.apache.fop.messaging.MessageHandler;
import org.apache.avalon.framework.logger.ConsoleLogger;
import org.apache.avalon.framework.logger.Logger;
import com.tsp.operation.model.beans.*;
import java.lang.*;

public class HojaReporteFacturaDestinatarioPDF {
        
    /** Creates a new instance of HojaReportes */
    public HojaReporteFacturaDestinatarioPDF() {        
    }
    
    public Element HojaControlReporte( Vector info ) {
                
        Element raiz = DocumentHelper.createElement("raiz");        
        Element data    = raiz.addElement("data");
        Element tabla1  = data.addElement("cabecera");
        
        Element tabla12 = data.addElement("titulo");        
        HojaReportes hojaReportes12 = ( HojaReportes ) info.elementAt( 0 );
        Element tipo = tabla12.addElement( "tipo" ).addText( (hojaReportes12.getTipo()!=null)?hojaReportes12.getTipo():"" );        
        Element numero  = tabla12.addElement( "numero" ).addText( (hojaReportes12.getNumero()!=null)?hojaReportes12.getNumero():"" );
        
        for( int i = 0; i < info.size(); i++ ){
            HojaReportes hojaReportes = ( HojaReportes ) info.elementAt( i );
            /*DATOS DEL VIAJE*/
            Element tabla2 = data.addElement("datos_viajes");
            //Element tipo = tabla2.addElement( "tipo" ).addText( (hojaReportes.getTipo()!=null)?hojaReportes.getTipo():"" );        
            //Element numero  = tabla2.addElement( "numero" ).addText( (hojaReportes.getNumero()!=null)?hojaReportes.getNumero():"" );
            Element destinatario = tabla2.addElement( "destinatario" ).addText( (hojaReportes.getDestinatario()!=null)?hojaReportes.getDestinatario():"" );
            Element tipo_documento = tabla2.addElement( "tipo_documento" ).addText( (hojaReportes.getTipo_documento()!=null)?hojaReportes.getTipo_documento():"" );
            Element documento = tabla2.addElement( "documento" ).addText( (hojaReportes.getDocumento()!=null)?hojaReportes.getDocumento():"" );
            Element tipo_doc_rel = tabla2.addElement( "tipo_doc_rel" ).addText( (hojaReportes.getTipo_doc_rel()!=null)?hojaReportes.getTipo_doc_rel():"" );
            Element doc_rel = tabla2.addElement( "doc_rel" ).addText( (hojaReportes.getDoc_rel()!=null)?hojaReportes.getDoc_rel():"" );
            Element fecha_cum = tabla2.addElement( "fecha_cum" ).addText( (hojaReportes.getFecha_cum()!=null)?hojaReportes.getFecha_cum():"" );
            Element cantidad_cum = tabla2.addElement( "cantidad_cum" ).addText( (hojaReportes.getCantidad_cum()!=null)?hojaReportes.getCantidad_cum():"" );
            Element discrepancia = tabla2.addElement( "discrepancia" ).addText( (hojaReportes.getDiscrepancia()!=null)?hojaReportes.getDiscrepancia():"" );
            Element observacion = tabla2.addElement( "observacion" ).addText( (hojaReportes.getObservacion()!=null)?hojaReportes.getObservacion():"" );
        }
        
        return raiz;
    }
    
    public void generarPDF ( File xslt, File pdf, Vector info ) throws Exception {
        
        org.apache.fop.configuration.Configuration.put( "baseDir", "file:///" + xslt.getParent() );
        
        //Crear Documento
        Element raiz = this.HojaControlReporte(info);
        Document documento = DocumentHelper.createDocument ( raiz );
        ProformaXMLReader proformaXMLReader = new ProformaXMLReader ( documento );
        
        //Establecer el formato de salida a PDF
        Driver driver = new Driver();
        
        //Setup logger
        Logger logger = new ConsoleLogger ( ConsoleLogger.LEVEL_INFO );
        driver.setLogger ( logger );
        
        driver.setRenderer ( driver.RENDER_PDF );
        
        //Cofigurar la salida
        FileOutputStream salida = new FileOutputStream ( pdf );
        
        try {
            
            driver.setOutputStream ( salida );
            
            //Configurar el XSLT
            TransformerFactory factory = TransformerFactory.newInstance();
            Transformer transformer = factory.newTransformer( new StreamSource ( xslt ) );
            
            //Configurar la entrada del XSLT para la transformación
            InputSource inputSource = new InputSource();
            inputSource.setEncoding( "windows-1252" );
            SAXSource source = new SAXSource( proformaXMLReader, inputSource );
            SAXResult res = new SAXResult( driver.getContentHandler () );
            
            //Iniciar la transformación XSLT y el procesamiento FOP
            transformer.transform( source, res );
            
            //Obtenemos el número de páginas
            String pageCount = "1";
            
            //Inicializamos todo para la segunda transformación
            driver = new Driver();
            
            //Setup logger
            Logger logger1 = new ConsoleLogger ( ConsoleLogger.LEVEL_INFO );
            driver.setLogger ( logger1 );
            MessageHandler.setScreenLogger ( logger1 );
            
            driver.setRenderer ( driver.RENDER_PDF );
            salida = new FileOutputStream ( pdf );
            driver.setOutputStream ( salida );
            factory = TransformerFactory.newInstance();
            transformer = factory.newTransformer(new StreamSource ( xslt ) );
            inputSource = new InputSource();
            inputSource.setEncoding ( "windows-1252" );
            res = new SAXResult ( driver.getContentHandler () );
            
            //Enviamos el parámetro
            transformer.setParameter ( "page-count", pageCount );
            
            //... y realizamos de nuevo la transformación
            transformer.transform ( source, res );
            
        } finally {
            
            salida.close();
            
        }
    }
    
    protected String obtenerTexto( Object obj ) {
        
        if ( ( obj == null ) || ( obj.toString().toLowerCase().trim().equals( "ninguno" ) ) ) {
            
            return "";
            
        } else {
            
            return obj.toString();
            
        }
    }    
    
}