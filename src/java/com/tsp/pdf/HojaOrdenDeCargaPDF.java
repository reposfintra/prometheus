/*****************************************************************************
 * Nombre clase :                   HojaOrdenDeCargaPDF                      *
 * Descripcion :                    Clase auxiliar que maneja los eventos    *
 *                                  relacionados con la generacion de los    *
 *                                  archivos XML y PDF para la Hoja de       *
 *                                  Orden De Carga                           *
 * Autor :                          LREALES                                  *
 * Fecha :                          11 mayo del 2006, 02:47 PM               *
 * Version :                        1.0                                      *
 * Copyright :                      Fintravalores S.A.                  *
 *****************************************************************************/

package com.tsp.pdf;

import org.dom4j.Element;
import org.dom4j.DocumentHelper;
import org.dom4j.Document;
import java.io.*;
import java.util.*;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamSource;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.sax.SAXSource;
import org.xml.sax.InputSource;
import org.apache.fop.apps.Driver;
import org.apache.fop.messaging.MessageHandler;
import org.apache.avalon.framework.logger.ConsoleLogger;
import org.apache.avalon.framework.logger.Logger;
import com.tsp.operation.model.beans.*;
import java.lang.*;
import com.tsp.operation.model.*;

public class HojaOrdenDeCargaPDF {
    
    private Model model;
    
    /** Creates a new instance of HojaOrdenDeCargaPDF */
    public HojaOrdenDeCargaPDF(Model model) {
        
        this.model = model;
        
    }
    
    public Element HojaControlOrdenDeCarga( Vector info_orden ) {
        
        
        HojaOrdenDeCarga hojaOrden = ( HojaOrdenDeCarga ) info_orden.elementAt( 0 );
        
        Element raiz = DocumentHelper.createElement("raiz");
        
        Element data = raiz.addElement("data");
        
        String p1 = hojaOrden.getPrecinto1();
        String p2 = hojaOrden.getPrecinto2();
        String p3 = hojaOrden.getPrecinto3();
        String p4 = hojaOrden.getPrecinto4();
        String p5 = hojaOrden.getPrecinto5();
        String pc1 = hojaOrden.getPrecintoc1();
        String pc2 = hojaOrden.getPrecintoc2();
        
        try {
            String precintos = "";
            
            if ( !p1.equals("") && precintos.equals("") )
                precintos = p1;
            if ( !p2.equals("") && precintos.equals("") )
                precintos = p2;
            if ( !p2.equals("") && !precintos.equals("") )
                precintos = precintos + " - " + p2;
            if ( !p3.equals("") && precintos.equals("") )
                precintos = p3;
            if ( !p3.equals("") && !precintos.equals("") )
                precintos = precintos + " - " + p3;
            if ( !p4.equals("") && precintos.equals("") )
                precintos = p4;
            if ( !p4.equals("") && !precintos.equals("") )
                precintos = precintos + " - " + p4;
            if ( !p5.equals("") && precintos.equals("") )
                precintos = p5;
            if ( !p5.equals("") && !precintos.equals("") )
                precintos = precintos + " - " + p5;
            if ( !pc1.equals("") && precintos.equals("") )
                precintos = pc1;
            if ( !pc1.equals("") && !precintos.equals("") )
                precintos = precintos + " - " + pc1;
            if ( !pc2.equals("") && precintos.equals("") )
                precintos = pc2;
            if ( !pc2.equals("") && !precintos.equals("") )
                precintos = precintos + " - " + pc2;
            
            String duplicado = "";
            String get_duplicado = model.imprimirOrdenService.getDuplicado();
            
            if ( get_duplicado.equals("si") )
                duplicado = "DUPLICADO";
            if ( get_duplicado.equals("no") )
                duplicado = "";
            
            /*DATOS DE LA ORDEN DE CARGA*/
            Element tabla2 = data.addElement("datos_orden");
            
            Element creation_date = tabla2.addElement( "creation_date" ).addText( (hojaOrden.getCreation_date() != null)?hojaOrden.getCreation_date():"" );
            Element dia = tabla2.addElement( "dia" ).addText( (hojaOrden.getDia() != null)?hojaOrden.getDia():"" );
            Element mes = tabla2.addElement( "mes" ).addText( (hojaOrden.getMes() != null)?hojaOrden.getMes():"" );
            Element ano = tabla2.addElement( "ano" ).addText( (hojaOrden.getAno() != null)?hojaOrden.getAno():"" );
            Element orden  = tabla2.addElement( "orden" ).addText( (hojaOrden.getOrden() != null)?hojaOrden.getOrden():"" );
            Element cedula = tabla2.addElement( "cedula" ).addText( (hojaOrden.getConductor() != null)?hojaOrden.getConductor():"" );
            Element placa = tabla2.addElement( "placa" ).addText( (hojaOrden.getPlaca() != null)?hojaOrden.getPlaca():"" );
            Element trailer = tabla2.addElement( "trailer" ).addText( (hojaOrden.getTrailer() != null)?hojaOrden.getTrailer():"" );
            Element contenido = tabla2.addElement( "contenido" ).addText( (hojaOrden.getContenido() != null)?hojaOrden.getContenido():"" );
            Element contenedores = tabla2.addElement( "contenedores" ).addText( (hojaOrden.getContenedores() != null)?hojaOrden.getContenedores():"" );
            Element entregar = tabla2.addElement( "entregar" ).addText( (hojaOrden.getEntregar() != null)?hojaOrden.getEntregar():"" );
            Element observacion = tabla2.addElement( "observacion" ).addText( (hojaOrden.getObservacion() != null)?hojaOrden.getObservacion():"" );
            Element creation_user = tabla2.addElement( "creation_user" ).addText( (hojaOrden.getCreation_user() != null)?hojaOrden.getCreation_user():"" );
            Element todosprecintos = tabla2.addElement( "todosprecintos" ).addText( precintos );
            Element esduplicado = tabla2.addElement( "esduplicado" ).addText( duplicado );
            Element remitente = tabla2.addElement( "remitente" ).addText( (hojaOrden.getRemitente() != null)?hojaOrden.getRemitente():"" );
            Element empresa = tabla2.addElement( "empresa" ).addText( (hojaOrden.getEmpresa() != null)?hojaOrden.getEmpresa():"" );
            Element direccion = tabla2.addElement( "direccion" ).addText( (hojaOrden.getDireccion() != null)?hojaOrden.getDireccion():"" );
            Element nomconductor = tabla2.addElement( "nomconductor" ).addText( (hojaOrden.getNomconductor() != null)?hojaOrden.getNomconductor():"" );
            Element expced = tabla2.addElement( "expced" ).addText( (hojaOrden.getExpced() != null)?hojaOrden.getExpced():"" );
            Element pase = tabla2.addElement( "pase" ).addText( (hojaOrden.getPase() != null)?hojaOrden.getPase():"" );
            Element marca = tabla2.addElement( "marca" ).addText( (hojaOrden.getMarca() != null)?hojaOrden.getMarca():"" );
            Element modelo = tabla2.addElement( "modelo" ).addText( (hojaOrden.getModelo() != null)?hojaOrden.getModelo():"" );
            Element motor = tabla2.addElement( "motor" ).addText( (hojaOrden.getMotor() != null)?hojaOrden.getMotor():"" );
            Element chasis = tabla2.addElement( "chasis" ).addText( (hojaOrden.getChasis() != null)?hojaOrden.getChasis():"" );
            Element empresaafil = tabla2.addElement( "empresaafil" ).addText( (hojaOrden.getEmpresaafil() != null)?hojaOrden.getEmpresaafil():"" );
            Element tarjetaoper = tabla2.addElement( "tarjetaoper" ).addText( (hojaOrden.getTarjetaoper() != null)?hojaOrden.getTarjetaoper():"" );
            Element color = tabla2.addElement( "color" ).addText( (hojaOrden.getColor() != null)?hojaOrden.getColor():"" );
            Element ciudad = tabla2.addElement( "ciudad" ).addText( (hojaOrden.getCiudad() != null)?hojaOrden.getCiudad():"" );
            Element destino = tabla2.addElement( "destino" ).addText( (hojaOrden.getDestino() != null)?hojaOrden.getDestino():"" );
                        
        } catch ( Exception e ) {
            
            e.printStackTrace();
            
        }
        
        return raiz;
        
    }
    
    public void generarPDF( File xslt, File pdf, Vector info_orden ) throws Exception {
        
        org.apache.fop.configuration.Configuration.put( "baseDir", "file:///" + xslt.getParent() );
        
        //Crear Documento
        Element raiz = this.HojaControlOrdenDeCarga( info_orden );
        Document documento = DocumentHelper.createDocument( raiz );
        ProformaXMLReader proformaXMLReader = new ProformaXMLReader( documento );
        
        //Establecer el formato de salida a PDF
        Driver driver = new Driver();
        
        //Setup logger
        Logger logger = new ConsoleLogger( ConsoleLogger.LEVEL_INFO );
        driver.setLogger( logger );
        
        driver.setRenderer( driver.RENDER_PDF );
        
        //Cofigurar la salida
        FileOutputStream salida = new FileOutputStream( pdf );
        
        try {
            
            driver.setOutputStream( salida );
            
            //Configurar el XSLT
            TransformerFactory factory = TransformerFactory.newInstance();
            Transformer transformer = factory.newTransformer( new StreamSource( xslt ) );
            
            //Configurar la entrada del XSLT para la transformación
            InputSource inputSource = new InputSource();
            inputSource.setEncoding( "windows-1252" );
            SAXSource source = new SAXSource( proformaXMLReader, inputSource );
            SAXResult res = new SAXResult( driver.getContentHandler() );
            
            //Iniciar la transformación XSLT y el procesamiento FOP
            transformer.transform( source, res );
            
            //Obtenemos el número de páginas
            String pageCount = "1";
            
            //Inicializamos todo para la segunda transformación
            driver = new Driver();
            
            //Setup logger
            Logger logger1 = new ConsoleLogger( ConsoleLogger.LEVEL_INFO );
            driver.setLogger( logger1 );
            MessageHandler.setScreenLogger( logger1 );
            
            driver.setRenderer( driver.RENDER_PDF );
            salida = new FileOutputStream( pdf );
            driver.setOutputStream( salida );
            factory = TransformerFactory.newInstance();
            transformer = factory.newTransformer(new StreamSource( xslt ) );
            inputSource = new InputSource();
            inputSource.setEncoding( "windows-1252" );
            res = new SAXResult( driver.getContentHandler() );
            
            //Enviamos el parámetro
            transformer.setParameter( "page-count", pageCount );
            
            //... y realizamos de nuevo la transformación
            transformer.transform( source, res );
            
        } finally {
            
            salida.close();
            
        }
        
    }
    
    protected String obtenerTexto( Object obj ) {
        
        if ( ( obj == null ) || ( obj.toString().toLowerCase().trim().equals( "ninguno" ) ) ) {
            
            return "";
            
        } else {
            
            return obj.toString();
            
        }
        
    }
    
}