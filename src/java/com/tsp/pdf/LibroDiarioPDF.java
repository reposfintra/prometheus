/*
 * LibroDiarioPDF.java
 *
 * Created on 24 de junio de 2006, 03:08 PM
 */

package com.tsp.pdf;

/**
 *
 * @author  Osvaldo
 */

import org.dom4j.Element;
import org.dom4j.DocumentHelper;
import org.dom4j.Document;
import java.io.*;
import java.util.*;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamSource;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.sax.SAXSource;
import org.xml.sax.InputSource;
import org.apache.fop.apps.Driver;
import org.apache.fop.messaging.MessageHandler;
import org.apache.avalon.framework.logger.ConsoleLogger;
import org.apache.avalon.framework.logger.Logger;
import java.lang.*;
import com.tsp.util.*;

import com.tsp.finanzas.contab.model.DAO.*;
import com.tsp.finanzas.contab.model.beans.*;

public class LibroDiarioPDF {
    
    
    Tipo_DoctoDAO tp;
    ComprobantesDAO comp;
    String dstrct;
    com.tsp.finanzas.contab.model.Model model;
    Element raiz;
    Element data;
    File xslt;
    File pdf;
    String periodo;
    
    double tdebito = 0;
    double tcredito = 0;
    
    // Creates a new instance of LibroDiarioPDF
    public LibroDiarioPDF(File xslt, File pdf) {
        this.xslt = xslt;
        this.pdf = pdf;
        this.comp = new ComprobantesDAO();
        this.tp = new Tipo_DoctoDAO();
        dstrct = "";
    }
    
    public void crearCabecera(String periodo) {
        
        periodo = periodo.substring(0,4)+"/"+periodo.substring(4,6);
        this.periodo = periodo;
        raiz = DocumentHelper.createElement("raiz");
        data = raiz.addElement("data");
        Element cabecera = data.addElement("cabecera");
        cabecera.addElement("periodo").setText(periodo);
    }
    
    public void crearCabeceraResumen(String periodo, Element resumen) {
        
        Element cabecera = resumen.addElement("cabecera");
        cabecera.addElement("periodo").setText(periodo);
        cabecera.addElement("res").setText("RESUMEN - ");
    }
    
    public void crearTotales(){
        Element totales = data.addElement("totales");
        totales.addElement("totaldebito2").setText( Util.customFormat(tdebito)) ;
        totales.addElement("totalcredito2").setText( Util.customFormat(tcredito)) ;
    }
    
    public void agregarResumen(Vector v){
        try{
            double deb = 0;
            double cre = 0;
            Comprobantes c = new Comprobantes();
            
            Element resumen = data.addElement("resumen");
            
            double debito = 0;
            double credito = 0;
            
            this.crearCabeceraResumen(periodo, resumen);
            for(int i=0;i<v.size();i++){
                
                c = (Comprobantes) v.elementAt(i);
                
                deb += c.getTotal_debito();
                cre += c.getTotal_credito();
                
                Element cuenta = resumen.addElement("cuenta");
                
                
                cuenta.addElement("ccuenta").addText( c.getCuenta() );
                cuenta.addElement("cdescripcion").addText(c.getDetalle());
                cuenta.addElement("cdebito").addText( Util.customFormat( c.getTotal_debito()));
                cuenta.addElement("ccredito").addText( Util.customFormat( c.getTotal_credito()));
            }
            
            
            resumen.addElement("totaldebitoresumen").addText(UtilFinanzas.customFormat( "#,##0.00", deb, 2));
            resumen.addElement("totalcreditoresumen").addText(UtilFinanzas.customFormat( "#,##0.00", cre, 2));
            
        }catch(Exception ex){
            
        }
    }
    public void agregarElementos( Vector v ){
        
        try{
            
            Comprobantes c = new Comprobantes();
            
            Element tabla = data.addElement("tabla");
            
            double debito = 0;
            double credito = 0;
            String desc = "";
            for(int i=0;i<v.size();i++){
                
                c = (Comprobantes) v.elementAt(i);
                
                debito += c.getTotal_debito();
                credito += c.getTotal_credito();
                try{
                    tp.obtener(dstrct, c.getTipodoc());
                    desc = (tp.getDocto().getDescripcion()!= null)?tp.getDocto().getDescripcion():"";
                }catch(Exception e){
                    desc="";
                }
                tabla.addElement("tipodoc").addText("  "+c.getTipodoc());
                tabla.addElement("desctipodoc").addText("  "+ desc );
                
                Element cuenta = tabla.addElement("cuenta");
                
                String cta = (c.getCuenta().substring(0,1).matches("C|G|I"))? c.getCuenta()+"*" : c.getCuenta();
                cuenta.addElement("ccuenta").addText( cta );
                cuenta.addElement("cdescripcion").addText(c.getDetalle());
                cuenta.addElement("cdebito").addText( UtilFinanzas.customFormat( "#,##0.00", c.getTotal_debito(), 2));
                cuenta.addElement("ccredito").addText( UtilFinanzas.customFormat( "#,##0.00", c.getTotal_credito(), 2));
            }
            
            tabla.addElement( "totaldebito" ).addText( UtilFinanzas.customFormat( "#,##0.00", debito, 2 ));
            tabla.addElement( "totalcredito" ).addText( UtilFinanzas.customFormat( "#,##0.00", credito, 2 ));
            
            this.tdebito += debito;
            this.tcredito += credito;
        }catch(Exception ex){
            
        }
        
    }
    
    public void generarPDF() throws Exception {
        
        org.apache.fop.configuration.Configuration.put( "baseDir", "file:///" + xslt.getParent() );
        
        //Crear Documento
        //Element raiz = this.HojaControlReporte(info);
        Document documento = DocumentHelper.createDocument( raiz );
        ProformaXMLReader proformaXMLReader = new ProformaXMLReader( documento );
        
        //Establecer el formato de salida a PDF
        Driver driver = new Driver();
        
        //Setup logger
        Logger logger = new ConsoleLogger( ConsoleLogger.LEVEL_INFO );
        driver.setLogger( logger );
        
        driver.setRenderer( driver.RENDER_PDF );
        
        //Cofigurar la salida
        FileOutputStream salida = new FileOutputStream( pdf );
        
        try {
            
            driver.setOutputStream( salida );
            
            //Configurar el XSLT
            TransformerFactory factory = TransformerFactory.newInstance();
            Transformer transformer = factory.newTransformer( new StreamSource( xslt ) );
            
            //Configurar la entrada del XSLT para la transformación
            InputSource inputSource = new InputSource();
            inputSource.setEncoding( "windows-1252" );
            SAXSource source = new SAXSource( proformaXMLReader, inputSource );
            SAXResult res = new SAXResult( driver.getContentHandler() );
            
            //Iniciar la transformación XSLT y el procesamiento FOP
            transformer.transform( source, res );
            
            //Obtenemos el número de páginas
            String pageCount = Integer.toString(driver.getResults().getPageCount());
            
            //Inicializamos todo para la segunda transformación
            driver = new Driver();
            
            //Setup logger
            Logger logger1 = new ConsoleLogger( ConsoleLogger.LEVEL_INFO );
            driver.setLogger( logger1 );
            MessageHandler.setScreenLogger( logger1 );
            
            driver.setRenderer( driver.RENDER_PDF );
            salida = new FileOutputStream( pdf );
            driver.setOutputStream( salida );
            factory = TransformerFactory.newInstance();
            transformer = factory.newTransformer(new StreamSource( xslt ) );
            inputSource = new InputSource();
            inputSource.setEncoding( "windows-1252" );
            res = new SAXResult( driver.getContentHandler() );
            
            //Enviamos el parámetro
            transformer.setParameter( "page-count", pageCount );
            
            //... y realizamos de nuevo la transformación
            transformer.transform( source, res );
            
        } finally {
            
            salida.close();
            
        }
    }
    
    public void generarLibroDiario(String dstrct, String periodo, String table){
        
        this.dstrct = dstrct;
        try{
            
            comp.crearTablaTemporal(table);
            //System.out.println("temporal  creada");
            try{
                comp.pasarATemporal(dstrct, periodo, table);
                //System.out.println("pasado a temporal");
                this.crearCabecera(periodo);
                ComprobantesDAO compa = new ComprobantesDAO();
                
                Vector td = compa.obtenerTiposDoc(table);
                
                for(int i=0;i<td.size();i++){
                    Vector v = compa.obtenerCreditoDebitoTipodoc((String)td.elementAt(i), table);
                    
                    this.agregarElementos(v);
                }
                
                Vector res = compa.resumenLibroDiario(table);
                
                
                this.agregarResumen(res);
                
                this.crearTotales();
                
                
                this.generarPDF();
            }catch (Exception exc){
                try{
                    comp.dropTablaTemporal(table);
                }catch(Exception er){}
            }
            comp.dropTablaTemporal(table);
            //System.out.println("Temporal borrada");
            
            
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }
    
}
