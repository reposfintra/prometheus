package com.tsp.pdf;

public class Item2 {
  public Item2() {
  }
  private String numero="";
  private String unidad="";
  private String cantidad="";
  private String peso="";
  private String codigon="";
  private String codigoe="";
  private String codigop="";
  private String producto="";
  private String remitente="";
  private String destinatario="";
  private String destino="";

  public Item2(String nu,String un,String can,String pe,String con,String coe,String cop,String p,String r,String d,String des) {
    numero=nu;
    unidad=un;
    cantidad=can;
    peso=pe;
    codigon=con;
    codigoe=coe;
    codigop=cop;
    producto=p;
    remitente=r;
    destinatario=d;
    destino=des;
  }

  public String getNumero(){
    return numero;
  }

  public String getUnidad(){
    return unidad;
  }

  public String getCantidad(){
    return cantidad;
  }

  public String getPeso(){
    return peso;
  }

  public String getCodigoN(){
    return codigon;
  }

  public String getCodigoE(){
    return codigoe;
  }

  public String getCodigoP(){
    return codigop;
  }

  public String getProducto(){
    return producto;
  }

  public String getRemitente(){
    return remitente;
  }

  public String getDestinatario(){
    return destinatario;
  }

  public String getDestino(){
    return destino;
  }

  public void setNumero(String numero){
    this.numero=numero;
  }

  public void setUnidad(String unidad){
    this.unidad=unidad;
  }

  public void setCantidad(String cantidad){
    this.cantidad=cantidad;
  }

  public void setPeso(String peso){
    this.peso=peso;
  }

  public void setCodigoN(String codigon){
    this.codigon=codigon;
  }

  public void setCodigoE(String codigoe){
    this.codigoe=codigoe;
  }

  public void setCodigoP(String codigop){
    this.codigop=codigop;
  }

  public void setProducto(String producto){
    this.producto=producto;
  }

  public void setRemitente(String remitente){
    this.remitente=remitente;
  }

  public void setDestinatario(String destinatario){
    this.destinatario=destinatario;
  }

  public void setDestino(String destino){
    this.destino=destino;
  }

}//Fin de la Clase