/*
 * BalancePruebaPDF2.java
 *
 * Created on 18 de abril de 2007, 10:16 AM
 */

package com.tsp.pdf;

import java.io.FileOutputStream;
import java.util.Vector;
import java.text.NumberFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.Date;

import com.lowagie.text.Rectangle;
import com.lowagie.text.Document;
import com.lowagie.text.ExceptionConverter;
import com.lowagie.text.PageSize;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import com.lowagie.text.pdf.PdfPageEventHelper;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.Font;

import com.tsp.operation.model.beans.BPCuentas2;
import com.tsp.operation.model.DAOS.BalancePruebaDAO;
import com.tsp.operation.model.beans.Usuario;
/**
 *
 * @author  Osvaldo P�rez Ferrer
 */
public class BalancePruebaPDF2  extends PdfPageEventHelper{
    
    //Variables de PDF
    Document document;
    
    PdfPTable table;
    
    float[] widths = {0.13f, 0.19f, 0.17f, 0.17f, 0.17f, 0.17f};
    
    BaseFont bf;
    
    Font fuente_normal   = new  Font(bf, 8);
    Font fuente_normal6  = new Font(bf, 6);
    Font fuente_negrita  = new Font(bf, 8);
    Font fuente_negrita7 = new Font(bf, 8.5f);
    Font fuente_negrita8 = new Font(bf, 9.5f);
    
    PdfWriter writer;
    Rectangle page;
    
    PdfPCell linea_blanco;
    SimpleDateFormat fmt = null;
    String ruta = "";
    
    //Variables de datos
    BalancePruebaDAO b;
    Vector datos = new Vector();
    BPCuentas2 total = new BPCuentas2();
    String distrito = "";
    String anio = "";
    int    mes;
    
    boolean tipo_balance = false;
    Usuario usuario;
    
    /** Creates a new instance of BalancePruebaPDF2 */
    public BalancePruebaPDF2( String ruta, String distrito, String anio, int mes) throws Exception{
        
        this.anio = anio;
        this.mes  = mes;
        this.distrito = distrito;
        this.ruta = ruta;
        document = new Document( PageSize.LETTER, 30, 30, 90, 70 );
        fmt = new SimpleDateFormat("yyyy/MM/dd kk:mm:ss");
        //table = new PdfPTable(widths);
        
        bf = BaseFont.createFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
        fuente_negrita.setStyle(com.lowagie.text.Font.BOLD);
        fuente_negrita7.setStyle(com.lowagie.text.Font.BOLD);
        fuente_negrita8.setStyle(com.lowagie.text.Font.BOLD);
        
        b = new BalancePruebaDAO();
        
        writer = PdfWriter.getInstance(document, new FileOutputStream( ruta ));
        
        page = document.getPageSize();
        
        document.open();
        
        //table.setWidthPercentage(100);
    }
    public BalancePruebaPDF2( String ruta, String distrito, String anio, int mes, Usuario usuario) throws Exception{
        this.usuario = usuario;
        this.anio = anio;
        this.mes  = mes;
        this.distrito = distrito;
        this.ruta = ruta;
        document = new Document( PageSize.LETTER, 30, 30, 90, 70 );
        fmt = new SimpleDateFormat("yyyy/MM/dd kk:mm:ss");
        
        bf = BaseFont.createFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
        fuente_negrita.setStyle(com.lowagie.text.Font.BOLD);
        fuente_negrita7.setStyle(com.lowagie.text.Font.BOLD);
        fuente_negrita8.setStyle(com.lowagie.text.Font.BOLD);
        
        b = new BalancePruebaDAO(usuario.getBd());
        writer = PdfWriter.getInstance(document, new FileOutputStream( ruta ));
        page = document.getPageSize();
        document.open();
    }
    
    public void reporte() throws Exception{
        
        try{
            writer.setPageEvent(new BalancePruebaPDF2( this.ruta, this.distrito, this.anio, this.mes, usuario ));
            
            //Cuentas Balance
            this.datos = b.cuentasBalance( distrito, anio, mes);
            this.escribir("CUENTAS DE BALANCE");
            document.newPage();
            
            
            //Cuentas Ingresos
            this.datos = new Vector();
            this.datos = b.cuentasIngreso("FINV", anio, mes, "I");
            this.escribir("INGRESOS");
            document.newPage();
            
            //Cuentas Costos
            this.datos = new Vector();
            this.datos = b.cuentasIngreso("FINV", anio, mes, "C");
            this.escribir("COSTOS");
            document.newPage();
            
            //Cuentas Gastos
            this.datos = new Vector();
            this.datos = b.cuentasIngreso("FINV", anio, mes, "G");
            this.escribir("GASTOS");
            
            table = new PdfPTable(widths);
            table.setWidthPercentage(100);
            
            table.addCell( linea_blanco );
            
            this.totalesUnidad("TOTAL GENERAL", this.total, table, fuente_negrita8 );
            
            document.add(table);
            
            document.close();
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }
    
    public void escribir(String tipo) throws Exception{
        
        try {
            
            table = new PdfPTable(widths);
            table.setWidthPercentage(100);
            
            BPCuentas2 c;
            
            //Color blanco = java.awt.Color.decode("ffffff");
            
            PdfPCell cell         = new PdfPCell();
            cell.setHorizontalAlignment( PdfPCell.ALIGN_LEFT );
            cell.setBorderColor( java.awt.Color.WHITE );
            cell.setVerticalAlignment( PdfPCell.ALIGN_MIDDLE );
            
            
            linea_blanco = new PdfPCell();
            linea_blanco.setBorderColor( java.awt.Color.WHITE );
            linea_blanco.setColspan(6);
            
            PdfPCell linea_negra = new PdfPCell();
            linea_negra.setBorderColor( java.awt.Color.WHITE );
            linea_negra.setUseVariableBorders(true);
            linea_negra.setBorderWidthBottom( 2 );
            linea_negra.setColspan(6);
            linea_negra.setBorderColorBottom( java.awt.Color.BLACK );
            
            PdfPCell linea_negra_bot = new PdfPCell();
            linea_negra_bot.setBorderColor( java.awt.Color.WHITE );
            linea_negra_bot.setUseVariableBorders(true);
            linea_negra_bot.setBorderWidthBottom( 1 );
            linea_negra_bot.setColspan(6);
            linea_negra_bot.setBorderColorBottom( java.awt.Color.BLACK );
            
            PdfPCell linea_negra_top = new PdfPCell();
            linea_negra_top.setBorderColor( java.awt.Color.WHITE );
            linea_negra_top.setUseVariableBorders(true);
            linea_negra_top.setBorderWidthBottom( 1 );
            linea_negra_top.setColspan(6);
            linea_negra_top.setBorderColorBottom( java.awt.Color.BLACK );
            
            String elemento_act = "";
            String unidad_act   = "";
            String grupo_act    = "";
            
            String elemento = "";
            String unidad   = "";
            String grupo    = "";
            
            cell.setColspan(6);
            cell.setHorizontalAlignment( PdfPCell.ALIGN_LEFT );
            cell.setPhrase( new Phrase("Tipo "+tipo, fuente_negrita8) );
            table.addCell(cell);
            
            table.addCell( linea_blanco );
            
            int cont = 0;
            
            boolean puc = false;
            
            while( this.datos.size() > 0 ){
                
                cont++;
                
                c = (BPCuentas2)this.datos.remove(0);
                
                elemento = c.getElemento();
                unidad   = c.getUnidad();
                grupo    = c.getGrupo();
                
                puc = !tipo.matches("INGRESOS|COSTOS|GASTOS");
                
                if( !unidad.matches("SUBTOTAL_UNIDAD|SUBTOTAL_ELEMENTO|SUBTOTAL_TIPO|SUBTOTAL_GRUPO") ){
                    
                    if( grupo !=null && !grupo_act.equals(grupo) ){
                        
                        if( puc ){
                            cell.setColspan(6);
                            cell.setHorizontalAlignment( PdfPCell.ALIGN_LEFT );
                            cell.setPhrase( new Phrase( grupo + " " + b.getDescElemento_Unidad( c.getCuenta(), "grupo" ), fuente_negrita8) );
                            table.addCell(cell);
                        }
                        
                        grupo_act = grupo;
                        //elemento_act = elemento;
                        //unidad_act = "";
                        
                        cell.setColspan(1);
                    }
                    
                    if( elemento !=null && !elemento_act.equals(elemento) ){
                        
                        if( puc ){
                            cell.setColspan(6);
                            cell.setHorizontalAlignment( PdfPCell.ALIGN_LEFT );
                            cell.setPhrase( new Phrase( "   "+elemento +" "+  b.getDescElemento_Unidad( c.getCuenta(), "elemento" ), fuente_negrita8) );
                            table.addCell(cell);
                        }else{
                            cell.setColspan(2);
                            cell.setHorizontalAlignment( PdfPCell.ALIGN_LEFT );
                            cell.setPhrase( new Phrase( "ELEMENTO DEL GASTO "+elemento, fuente_negrita8) );
                            table.addCell(cell);
                            
                            cell.setColspan(4);
                            cell.setPhrase( new Phrase( c.getDesc_elemento(), fuente_negrita8) );
                            table.addCell(cell);
                        }
                        
                        elemento_act = elemento;
                        unidad_act = "";
                        
                        cell.setColspan(1);
                    }
                    
                    if( ! unidad_act.equals(unidad) ){
                        
                        if( puc ){
                            
                            cell.setColspan(6);
                            cell.setHorizontalAlignment( PdfPCell.ALIGN_LEFT );
                            cell.setPhrase( new Phrase( "         "+unidad +" "+ b.getDescElemento_Unidad( c.getCuenta(), "unidad" ), fuente_negrita7 ) );
                            table.addCell(cell);
                            
                        }else{
                            cell.setColspan(2);
                            cell.setHorizontalAlignment( PdfPCell.ALIGN_LEFT );
                            cell.setPhrase( new Phrase( "        UNIDAD DE NEGOCIO "+unidad, fuente_negrita7) );
                            table.addCell(cell);
                            
                            cell.setColspan(4);
                            cell.setPhrase( new Phrase( c.getDesc_unidad(), fuente_negrita7) );
                            table.addCell(cell);
                        }
                        
                        unidad_act = unidad;
                        
                        cell.setColspan(1);
                        
                    }
                    
                    cell.setHorizontalAlignment( PdfPCell.ALIGN_LEFT );
                    //Cuenta
                    
                    cell.setPhrase( new Phrase(c.getCuenta(), fuente_normal) );
                    table.addCell(cell);
                    
                    //Descripcion
                    cell.setNoWrap(true);
                    cell.setPhrase( new Phrase( c.getDesc_cuenta().length()<=32? c.getDesc_cuenta():c.getDesc_cuenta().substring(0,25) , fuente_normal6) );
                    table.addCell(cell);
                    cell.setNoWrap(false);
                    
                    
                    cell.setHorizontalAlignment( PdfPCell.ALIGN_RIGHT );
                    //Saldo anterior
                    cell.setPhrase( new Phrase(format( c.getVlr_saldo_anterior() ), fuente_normal) );
                    table.addCell(cell);
                    
                    //Debito
                    cell.setPhrase( new Phrase(format( c.getVlr_debito() ), fuente_normal) );
                    table.addCell(cell);
                    
                    //Credito
                    cell.setPhrase( new Phrase(format( c.getVlr_credito() ), fuente_normal) );
                    table.addCell(cell);
                    
                    //Saldo actual
                    cell.setPhrase( new Phrase(format( c.getVlr_saldo_actual() ), fuente_normal) );
                    table.addCell(cell);
                    
                }else{
                    
                    if( unidad.equals("SUBTOTAL_UNIDAD") ){
                        
                        if( puc ){
                            this.totalesUnidad("        SUBTOTAL "+b.getDescElemento_Unidad( elemento_act+unidad_act, "unidad" ), c, table, fuente_negrita7 );
                        }else{
                            this.totalesUnidad("        SUBTOTAL UNIDAD DE NEGOCIO", c, table, fuente_negrita7 );
                        }
                        table.addCell( linea_negra_bot );//Mod Tmolina 24-Dic-2008
                        
                    }else if( unidad.equals("SUBTOTAL_ELEMENTO") ){
                        
                        table.addCell( linea_negra_top );
                        //this.totalesUnidad("SUBTOTAL ELEMENTO ", c, table, fuente_negrita8 );
                        if( puc ){
                            this.totalesUnidad("    SUBTOTAL "+b.getDescElemento_Unidad( elemento_act, "elemento" ), c, table, fuente_negrita7 );
                        }else{
                            this.totalesUnidad("SUBTOTAL ELEMENTO ", c, table, fuente_negrita8 );
                        }
                        table.addCell( linea_negra_bot );
                        table.addCell( linea_blanco );
                        
                    }else if( unidad.equals("SUBTOTAL_GRUPO") ){
                        
                        this.totalesUnidad("SUBTOTAL "+b.getDescElemento_Unidad( grupo_act, "grupo"), c, table, fuente_negrita8 );
                        table.addCell( linea_negra );
                        
                    }else{
                        
                        this.totalesUnidad("SUBTOTAL TIPO ", c, table, fuente_negrita8 );
                        table.addCell( linea_negra );
                        
                        this.total.setVlr_saldo_anterior(   total.getVlr_saldo_anterior() + c.getVlr_saldo_anterior() );
                        this.total.setVlr_debito(           total.getVlr_debito()         + c.getVlr_debito() );
                        this.total.setVlr_credito(          total.getVlr_credito()        + c.getVlr_credito() );
                        this.total.setVlr_saldo_actual(     total.getVlr_saldo_actual()   + c.getVlr_saldo_actual() );
                        
                        document.newPage();
                        
                    }
                    
                    table.addCell( linea_blanco );
                    
                }
                
            }
            
            document.add(table);
            
        }catch (java.lang.OutOfMemoryError mem) {
            mem.printStackTrace();
            throw new Exception("Sin Memoria en BalancePruebaPDF2.reporte "+mem.getMessage());
        }catch (Exception ex ){
            ex.printStackTrace();
            throw new Exception("Error BalancePruebaPDF2.reporte "+ex.getMessage());
        }
        
        
    }
    
    public void onEndPage(PdfWriter writer, Document document) {
        try {
            
            Rectangle page = document.getPageSize();
            PdfContentByte cb = writer.getDirectContent();
            
            cb.beginText();
            BaseFont bf = BaseFont.createFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
            cb.setFontAndSize( bf, 12);
            cb.showTextAligned(PdfContentByte.ALIGN_CENTER, this.usuario.getBd().toUpperCase(), page.getWidth()/2, page.getHeight()-25 , 0);
            cb.showTextAligned(PdfContentByte.ALIGN_CENTER, "BALANCE DE PRUEBA", page.getWidth()/2, page.getHeight() - 40, 0);
            cb.endText();
            
            
            PdfPTable table = new PdfPTable(widths);
            table.setTotalWidth(page.getWidth() - document.leftMargin() - document.rightMargin());
            
            Font fuente_normal = new Font(bf, 8);
            PdfPCell cell = new PdfPCell();
            
            cell.setBorderColor( java.awt.Color.WHITE );
            
            cell.setPhrase( new Phrase( fmt.format(new Date()) , fuente_normal) );
            cell.setHorizontalAlignment( PdfPCell.ALIGN_LEFT );
            cell.setColspan(2);
            table.addCell(cell);
            
            cell.setPhrase( new Phrase("Periodo :"+anio+(mes<10? "0"+mes: ""+mes), fuente_normal) );
            cell.setHorizontalAlignment( PdfPCell.ALIGN_CENTER );
            cell.setColspan(2);
            table.addCell(cell);
            
            cell.setPhrase( new Phrase("Pagina "+ document.getPageNumber() , fuente_normal) );
            cell.setHorizontalAlignment( PdfPCell.ALIGN_RIGHT );
            cell.setColspan(2);
            table.addCell(cell);
            
            cell.setUseVariableBorders(true);
            cell.setBorderWidthBottom( 1 );
            cell.setBorderColorBottom( java.awt.Color.BLACK );
            
            cell.setPhrase( new Phrase("CUENTA", fuente_normal) );
            cell.setHorizontalAlignment( PdfPCell.ALIGN_LEFT );
            cell.setColspan(1);
            table.addCell(cell);
            
            cell.setPhrase( new Phrase("DESCRIPCI�N", fuente_normal) );
            cell.setHorizontalAlignment( PdfPCell.ALIGN_LEFT );
            table.addCell(cell);
            
            cell.setPhrase( new Phrase("SALDO ANTERIOR", fuente_normal) );
            cell.setHorizontalAlignment( PdfPCell.ALIGN_RIGHT );
            table.addCell(cell);
            
            cell.setPhrase( new Phrase("MVTO. DEBITO", fuente_normal) );
            cell.setHorizontalAlignment( PdfPCell.ALIGN_RIGHT );
            table.addCell(cell);
            
            cell.setPhrase( new Phrase("MVTO. CREDITO", fuente_normal) );
            cell.setHorizontalAlignment( PdfPCell.ALIGN_RIGHT );
            table.addCell(cell);
            
            cell.setPhrase( new Phrase("SALDO ACTUAL", fuente_normal) );
            cell.setHorizontalAlignment( PdfPCell.ALIGN_RIGHT );
            table.addCell(cell);
            
            table.writeSelectedRows(0, -1, document.leftMargin(), page.getHeight() - 45, writer.getDirectContent());
            
        }
        catch (Exception e) {
            throw new ExceptionConverter(e);
        }
    }
    
    public void totalesUnidad( String label, BPCuentas2 c, PdfPTable table, Font fuente ){
        
        PdfPCell cell = new PdfPCell();
        
        cell.setBorderColor( java.awt.Color.WHITE );
        
        cell.setHorizontalAlignment( PdfPCell.ALIGN_LEFT );
        
        cell.setColspan(2);
        cell.setPhrase( new Phrase(label, fuente) );
        table.addCell(cell);
        
        cell.setHorizontalAlignment( PdfPCell.ALIGN_RIGHT );
        cell.setColspan(1);
        //Saldo anterior
        cell.setPhrase( new Phrase(format( c.getVlr_saldo_anterior() ), fuente) );
        table.addCell(cell);
        
        //Debito
        cell.setPhrase( new Phrase(format( c.getVlr_debito() ), fuente) );
        table.addCell(cell);
        
        //Credito
        cell.setPhrase( new Phrase(format( c.getVlr_credito() ), fuente) );
        table.addCell(cell);
        
        //Saldo actual
        cell.setPhrase( new Phrase(format( c.getVlr_saldo_actual() ), fuente) );
        table.addCell(cell);
        
    }
    
    public String format(double value) {
        DecimalFormat df = (DecimalFormat) NumberFormat.getNumberInstance(new Locale("en", "US"));
        df.applyPattern("#,###.##");
        df.setMaximumFractionDigits(2);
        df.setMinimumFractionDigits(2);
        return df.format(value);
    }
    public static void main(String[]saf)throws Exception{
        
        BalancePruebaPDF2 p = new BalancePruebaPDF2( "C:/Balance_Prueba.pdf","TSP", "2007", 3 );
        p.reporte();
        
    }
}