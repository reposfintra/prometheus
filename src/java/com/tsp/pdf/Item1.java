package com.tsp.pdf;

public class Item1 {

  private String marcas="";
  private String bultos="";
  private String kilos="";
  private String mercancia="";

  public Item1() {
  }

  public Item1(String m,String b,String k,String me) {
    marcas=m;
    bultos=b;
    kilos=k;
    mercancia=me;
  }

  public String getMarca(){
    return mercancia;
  }

  public String getBultos(){
    return bultos;
  }

  public String getKilos(){
      return kilos;
  }

  public String getMercancia(){
    return mercancia;
  }

  public void setMarcas(String marcas){
    this.marcas=marcas;
  }

  public void setBultos(String bultos){
    this.bultos=bultos;
  }

  public void setKilos(String kilos){
    this.kilos=kilos;
  }

  public void setMercancia(String mercancia){
     this.mercancia=mercancia;
  }

}