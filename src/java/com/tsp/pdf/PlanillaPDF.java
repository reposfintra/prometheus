/*****************************************************************************
 * Nombre clase :                   PlanillaPDF.java                         *
 * Descripcion :                    Clase auxiliar que maneja los eventos    *
 *                                  relacionados con la generacion de los    *
 *                                  archivos XML y PDF para la impresion     *
 *                                  de las Planilla                          *
 * Autor :                          Ing. Juan Manuel Escandon Perez          *
 * Fecha :                          5 de enero de 2006, 02:31 PM             *
 * Version :                        1.0                                      *
 * Copyright :                      Fintravalores S.A.                  *
 *****************************************************************************/
package com.tsp.pdf;

//dom4j
import org.dom4j.Element;
import org.dom4j.DocumentHelper;
import org.dom4j.Document;

//Java
import java.io.File;
import java.util.Vector;
import java.io.*;

//JAXP
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamSource;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.sax.SAXSource;
import org.xml.sax.InputSource;

//Fop
import org.apache.fop.apps.Driver;
import org.apache.fop.messaging.MessageHandler;
import org.apache.avalon.framework.logger.ConsoleLogger;
import org.apache.avalon.framework.logger.Logger;

import com.tsp.operation.model.beans.*;
import java.util.*;


public class PlanillaPDF {
    
    /** Creates a new instance of PlanillaPDF */
    public PlanillaPDF() {
    }
    public Element CreacionPlantillaPlanilla( List lista ) {
        com.tsp.util.UtilFinanzas u = new com.tsp.util.UtilFinanzas();
        
        Element raiz = DocumentHelper.createElement("raiz");
        //Iterator it = lista.iterator();
        //while(it.hasNext()){
        for( int k = 0 ; k < lista.size(); k++ ){
            DatosPlanillaImpPDF datos = (DatosPlanillaImpPDF)lista.get(k);
            Element data = null;
            Vector copias = datos.getCopias();
            for (int i = 0; i < copias.size(); i++) {
                data = raiz.addElement("data");
                data.addElement("trafico").addText(copias.elementAt(i) + "");
                
                //Creamos un hijo para el root
                Element tabla1 = data.addElement("tabla1");
                Element tabla2 = data.addElement("tabla2");
                Element tabla3 = data.addElement("tabla3");
                Element tabla4 = data.addElement("tabla4");
                Element tabla5 = data.addElement("tabla5");
                Element tabla6 = data.addElement("tabla6");
                
                //Creamos un hijos para el tabla1
                Element codigor = tabla1.addElement("codigor").addText((datos.getCodigoRegional()!= null )?datos.getCodigoRegional():"");
                Element codigoe = tabla1.addElement("codigoe").addText((datos.getCodigoEmpresa()!= null )?datos.getCodigoEmpresa():"");
                Element codigoc = tabla1.addElement("codigoc").addText((datos.getNumeroPlanilla()!= null )?datos.getNumeroPlanilla():"");
                
                //Creamos un hijos para el tabla2
                String rangoInicial = ( datos.getRangoInicial()!= null )?datos.getRangoInicial():"";
                String rangoFinal = ( datos.getRangoFinal() != null )?datos.getRangoFinal():"";
                Element rango = tabla2.addElement("rango").addText(rangoInicial + " - " + rangoFinal);
                Element res = tabla2.addElement("res").addText(( datos.getResolucion()!= null )?datos.getResolucion():"");
                Element fechae = tabla2.addElement("fechae").addText(( datos.getFechaPlanilla()!= null)?datos.getFechaPlanilla():"");
                Element origen = tabla2.addElement("origen").addText(( datos.getDesOrigen()!= null)?datos.getDesOrigen():"");
                Element destino = tabla2.addElement("destino").addText(( datos.getDesDestino()!=null)?datos.getDesDestino():"");
                
                //Creamos un hijos para el tabla3
                Element placa = tabla3.addElement("placa").addText(( datos.getPlaca()!= null)?datos.getPlaca():"");
                Element marca = tabla3.addElement("marca").addText(( datos.getMarca()!= null)?datos.getMarca():"");
                Element linea = tabla3.addElement("linea").addText("");
                Element modelo = tabla3.addElement("modelo").addText(( datos.getModelo()!= null)?datos.getModelo():"");
                Element modelor = tabla3.addElement("modelor").addText("");
                Element serie = tabla3.addElement("serie").addText(( datos.getSerial()!= null)?datos.getSerial():"");
                Element color = tabla3.addElement("color").addText(( datos.getColor()!= null)?datos.getColor():"");
                Element tipoc = tabla3.addElement("tipoc").addText(( datos.getCarroceria()!=null)?datos.getCarroceria():"");
                Element registro = tabla3.addElement("registro").addText((datos.getRegistroNacionalCarga()!=null)?datos.getRegistroNacionalCarga():"");
                Element configuracion = tabla3.addElement("configuracion").addText((datos.getNumeroEjes()!=null)?datos.getNumeroEjes():"");
                Element peso = tabla3.addElement("peso").addText((datos.getPesoVacio()!= null)?datos.getPesoVacio():"");
                Element numerop = tabla3.addElement("numerop").addText((datos.getNumeroPolizaSoat()!=null)?datos.getNumeroPolizaSoat():"");
                Element compas = tabla3.addElement("compas").addText((datos.getCompa�iaSoat()!=null)?datos.getCompa�iaSoat():"");
                String soat = (datos.getVecimientoSoat()!= null)?datos.getVecimientoSoat():"";
                soat = (!soat.equals("0099-01-01"))?soat:"";
                Element vencimiento = tabla3.addElement("vencimiento").addText(soat);
                Element placas1 = tabla3.addElement("placas").addText((datos.getPlacaTrailer()!= null)?datos.getPlacaTrailer():"");
                Element propietario = tabla3.addElement("propietario").addText((datos.getNombrePropietario()!= null)?datos.getNombrePropietario():"");
                Element documentop = tabla3.addElement("documentop").addText((datos.getCedulaPropietario()!= null)?datos.getCedulaPropietario():"");
                Element direccionp = tabla3.addElement("direccionp").addText((datos.getDireccionPropietario()!= null)?datos.getDireccionPropietario():"");
                Element telefonop = tabla3.addElement("telefonop").addText((datos.getTelefonoPropietario()!= null)?datos.getTelefonoPropietario():"");
                Element ciudadp = tabla3.addElement("ciudadp").addText((datos.getCiudadPropietario()!= null)?datos.getCiudadPropietario():"");
                Element tenedor = tabla3.addElement("tenedor").addText((datos.getNombreTenedor()!= null)?datos.getNombreTenedor():"");
                Element documentot = tabla3.addElement("documentot").addText((datos.getCedulaTenedor()!= null)?datos.getCedulaTenedor():"");
                Element direcciont = tabla3.addElement("direcciont").addText((datos.getDireccionTenedor()!= null)?datos.getDireccionTenedor():"");
                Element telefonot = tabla3.addElement("telefonot").addText((datos.getTelefonoTenedor()!= null)?datos.getTelefonoTenedor():"");
                Element ciudadt = tabla3.addElement("ciudadt").addText((datos.getCiudadTenedor()!= null)?datos.getCiudadTenedor():"");
                Element conductor1 = tabla3.addElement("conductor").addText((datos.getNombreConductor()!= null)?datos.getNombreConductor():"");
                Element documentoc = tabla3.addElement("documentoc").addText((datos.getCedulaConductor()!= null)?datos.getCedulaConductor():"");
                Element direccionc = tabla3.addElement("direccionc").addText((datos.getDireccionConductor()!= null)?datos.getDireccionConductor():"");
                Element catc = tabla3.addElement("catc").addText((datos.getCatLicConductor()!= null)?datos.getCatLicConductor():"");
                Element ciudadc = tabla3.addElement("ciudadc").addText((datos.getCiudadConductor()!= null)?datos.getCiudadConductor():"");
                
                Element despachador = data.addElement("despachador").addText((datos.getDespachador()!= null)?datos.getDespachador():"");
                
                Element valores = data.addElement("valores");
                
                String precinto = "Sticker : ";
                String numprecinto = ( datos.getPrecinto()!=null )?datos.getPrecinto():"";
                
                precinto = (!numprecinto.equals(""))?precinto+ "" + numprecinto: "";
                
                Element pre = valores.addElement("precinto").addText(precinto);
                Element ley = valores.addElement("leyenda2").addText((datos.getLeyenda()!= null)?datos.getLeyenda():"");
                Element item = null;
                
                Vector detalle = datos.getDetalle();
                
                for (int j = 0; j < detalle.size(); j++) {
                    Item2 mercancia = (Item2) detalle.elementAt(j);
                    //elemento.add(producto.obtenerElemento());
                    item = valores.addElement("item");
                    item.addElement("numero").addText(obtenerTexto(mercancia.getNumero()));
                    item.addElement("unidad").addText(obtenerTexto(mercancia.getUnidad()));
                    item.addElement("cantidad").addText(obtenerTexto(mercancia.getCantidad()));
                    item.addElement("peso").addText(obtenerTexto(mercancia.getPeso()));
                    item.addElement("codigon").addText(obtenerTexto(mercancia.getCodigoN()));
                    item.addElement("codigoe").addText(obtenerTexto(mercancia.getCodigoE()));
                    item.addElement("codigop").addText(obtenerTexto(mercancia.getCodigoP()));
                    item.addElement("producto").addText(obtenerTexto(mercancia.getProducto()));
                    item.addElement("remitente").addText(obtenerTexto(mercancia.
                    getRemitente()));
                    item.addElement("destinatario").addText(obtenerTexto(mercancia.
                    getDestinatario()));
                    item.addElement("destino").addText(obtenerTexto(mercancia.getDestino()));
                }
                
                //Creamos un hijos para el tabla5
                Element valor = tabla5.addElement("valor").addText("S.CONTRATO");
                Element retencion = tabla5.addElement("retencion").addText("---");
                Element descuento = tabla5.addElement("descuento").addText("---");
                Element flete1 = tabla5.addElement("flete").addText("---");
                String moneda =(datos.getMoneda()!=null)?datos.getMoneda():"PES";       
                
                String ant = u.customFormat(datos.getValorAnticipo());
                
                Element valora = tabla5.addElement("valora").addText(ant + " " + moneda);
                
                Element neto = tabla5.addElement("neto").addText("");
                Element lugar = tabla5.addElement("lugar").addText("S.CONTRATO");
                Element fecha = tabla5.addElement("fecha").addText("S.CONTRATO");
                Element cargue = tabla5.addElement("cargue").addText("GENERADOR DE CARGA");
                Element descargue = tabla5.addElement("descargue").addText("GENERADOR DE CARGA");
                Element valort = tabla5.addElement("valort").addText("SEGUN CONTRATO");
                Element seguro = tabla5.addElement("seguro").addText((datos.getCiaSeguros()!= null)?datos.getCiaSeguros():"");
                Element poliza = tabla5.addElement("poliza").addText((datos.getNumPoliza()!= null)?datos.getNumPoliza():"");
                Element vigencia = tabla5.addElement("vigencia").addText((datos.getVigenciaPoliza()!= null)?datos.getVigenciaPoliza():"");
                
                //Creamos un hijos para el tabla6
                Element soporte = tabla6.addElement("soporte").addText(datos.getSoportes());
                Element observacion = tabla6.addElement("observacion").addText(datos.getObservaciones());
                Element fcargue = tabla6.addElement("fecha_cargue").addText((datos.getFechacargue()!= null)?"Fecha cargue : " + datos.getFechacargue():"");
                
                
            }
        }
        
        
        
        
        return raiz;
    }
    
    public void generarPDF(File xslt, File pdf, List lista ) throws Exception {
        org.apache.fop.configuration.Configuration.put("baseDir",
        "file:///" + xslt.getParent());
        //Crear Documento
        Document documento = DocumentHelper.createDocument(CreacionPlantillaPlanilla(lista));
        ProformaXMLReader proformaXMLReader = new ProformaXMLReader(documento);
        
        //Establecer el formato de salida a PDF
        Driver driver = new Driver();
        //Setup logger
        Logger logger = new ConsoleLogger(ConsoleLogger.LEVEL_INFO);
        driver.setLogger(logger);
        //MessageHandler.setScreenLogger(logger);
        
        driver.setRenderer(driver.RENDER_PDF);
        
        //Cofigurar la salida
        FileOutputStream salida = new FileOutputStream(pdf);
        
        try {
            
            driver.setOutputStream(salida);
            
            //Configurar el XSLT
            TransformerFactory factory = TransformerFactory.newInstance();
            Transformer transformer = factory.newTransformer(new StreamSource(
            xslt));
            //Configurar la entrada del XSLT para la transformaci�n
            InputSource inputSource = new InputSource();
            //inputSource.setEncoding("ISO-8859-1");
            inputSource.setEncoding("windows-1252");
            SAXSource source = new SAXSource(proformaXMLReader, inputSource);
            
            SAXResult res = new SAXResult(driver.getContentHandler());
            
            //Iniciar la transformaci�n XSLT y el procesamiento FOP
            transformer.transform(source, res);
            
            //Obtenemos el n�mero de p�ginas
            String pageCount = Integer.toString(driver.getResults().getPageCount());
            
            //Inicializamos todo para la segunda transformaci�n
            driver = new Driver();
            
            //Setup logger
            Logger logger1 = new ConsoleLogger(ConsoleLogger.LEVEL_INFO);
            driver.setLogger(logger1);
            MessageHandler.setScreenLogger(logger1);
            
            driver.setRenderer(driver.RENDER_PDF);
            salida = new FileOutputStream(pdf);
            driver.setOutputStream(salida);
            factory = TransformerFactory.newInstance();
            transformer = factory.newTransformer(new StreamSource(xslt));
            inputSource = new InputSource();
            //inputSource.setEncoding("ISO-8859-1");
            inputSource.setEncoding("windows-1252");
            res = new SAXResult(driver.getContentHandler());
            
            //Enviamos el par�metro
            transformer.setParameter("page-count", pageCount);
            
            //... y realizamos de nuevo la transformaci�n
            transformer.transform(source, res);
        }
        finally {
            salida.close();
        }
    }
    
    protected String obtenerTexto(Object obj) {
        if ( (obj == null) || (obj.toString().toLowerCase().trim().equals("ninguno"))) {
            return "";
        }
        else {
            return obj.toString();
        }
    }
    
}
