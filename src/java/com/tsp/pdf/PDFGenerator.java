/********************************************************************
 *      Nombre Clase.................   HojaVidaPDF.java
 *      Descripci�n..................   Generarcion de documentos en formato PDF
 *      Autor........................   Ing. Andr�s Maturana De La Cruz
 *      Fecha........................   21 Abril del 2006
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes S�nchez Polo S.A.
 *******************************************************************/

package com.tsp.pdf;
//dom4j
import org.dom4j.Document;

//java
import java.io.*;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamSource;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.sax.SAXSource;
import org.xml.sax.InputSource;

//Fop
import org.apache.fop.apps.Driver;
import org.apache.avalon.framework.logger.ConsoleLogger;
import org.apache.avalon.framework.logger.Logger;

import com.tsp.operation.model.*;

public class PDFGenerator {
    
    //Log Procesos
    private String procesoName;
    private String des;
    
    /** Creates a new instance of HojaControlViaje */
    public PDFGenerator() {
    }
    
    public static void generarPDF(File xslt, File pdf, Document documento, Model model, String procesoName, String des, String login) 
            throws Exception {
            
        //INSERTO EN EL LOG DE PROCESO
        model.LogProcesosSvc.InsertProceso(procesoName, PDFGenerator.class.hashCode(), des, login);
            
        org.apache.fop.configuration.Configuration.put("baseDir",
        "file:///" + xslt.getParent());
        //Crear Documento        
        ProformaXMLReader proformaXMLReader = new ProformaXMLReader(documento);
        
        //Establecer el formato de salida a PDF
        Driver driver = new Driver();
        //Setup logger
        Logger logger = new ConsoleLogger(ConsoleLogger.LEVEL_INFO);
        driver.setLogger(logger);
        //MessageHandler.setScreenLogger(logger);
        
        driver.setRenderer(driver.RENDER_PDF);
        
        //Cofigurar la salida
        FileOutputStream salida = new FileOutputStream(pdf);
        
        try {
            
            driver.setOutputStream(salida);
            
            //Configurar el XSLT
            TransformerFactory factory = TransformerFactory.newInstance();
            Transformer transformer = factory.newTransformer(new StreamSource(xslt));
            //Configurar la entrada del XSLT para la transformaci�n
            InputSource inputSource = new InputSource();
            //inputSource.setEncoding("ISO-8859-1");
            inputSource.setEncoding("windows-1252");
            SAXSource source = new SAXSource(proformaXMLReader, inputSource);
            
            SAXResult res = new SAXResult(driver.getContentHandler());
            
            //Iniciar la transformaci�n XSLT y el procesamiento FOP
            transformer.transform(source, res);
            
            //Obtenemos el n�mero de p�ginas
            String pageCount = "1";
            //String pageCount = Integer.toString(driver.getResults().getPageCount());
            //System.err.println("N�mero de p�gs:" + pageCount);
            
            //Inicializamos todo para la segunda transformaci�n
            /*driver = new Driver();
            
            //Setup logger
            Logger logger1 = new ConsoleLogger(ConsoleLogger.LEVEL_INFO);
            driver.setLogger(logger1);
            MessageHandler.setScreenLogger(logger1);
            
            driver.setRenderer(driver.RENDER_PDF);
            salida = new FileOutputStream(pdf);
            driver.setOutputStream(salida);
            factory = TransformerFactory.newInstance();
            transformer = factory.newTransformer(new StreamSource(xslt));
            inputSource = new InputSource();
            //inputSource.setEncoding("ISO-8859-1");
            inputSource.setEncoding("windows-1252");
            res = new SAXResult(driver.getContentHandler());
            
            //Enviamos el par�metro
            transformer.setParameter("page-count", pageCount);
            
            //... y realizamos de nuevo la transformaci�n
            transformer.transform(source, res);*/
            
            model.LogProcesosSvc.finallyProceso(procesoName, PDFGenerator.class.hashCode(), login, "PROCESO EXITOSO");
        } catch (Exception ex){
            ex.printStackTrace();
            //System.out.println("ERROR AL GENERAR EL ARCHIVO XLS : " + ex.getMessage());
            try{
                model.LogProcesosSvc.finallyProceso(procesoName, PDFGenerator.class.hashCode(), login, "ERROR :" + ex.getMessage());
            }
            catch(Exception f){
                try{
                    model.LogProcesosSvc.finallyProceso(procesoName, PDFGenerator.class.hashCode(), login, "ERROR :");
                }catch(Exception p){    }
            }
        } finally {
            salida.close();
        } 
    }
    
    protected String obtenerTexto(Object obj) {
        if ( obj == null ) {
            return "";
        }
        else {
            return obj.toString();
        }
    }
    
}
