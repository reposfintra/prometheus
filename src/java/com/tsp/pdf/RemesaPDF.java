/*****************************************************************************
 * Nombre clase :                   RemesaPDF.java                           *
 * Descripcion :                    Clase auxiliar que maneja los eventos    *
 *                                  relacionados con la generacion de los    *
 *                                  archivos XML y PDF para la impresion     *
 *                                  de las Planilla                          *
 * Autor :                          Ing. Juan Manuel Escandon Perez          *
 * Fecha :                          6 de enero de 2006, 04:56 PM             *
 * Version :                        1.0                                      *
 * Copyright :                      Fintravalores S.A.                  *
 *****************************************************************************/
package com.tsp.pdf;

//dom4j
import org.dom4j.Element;
import org.dom4j.DocumentHelper;
import org.dom4j.Document;

//Java
import java.io.File;
import java.util.Vector;
import java.io.*;

//JAXP
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamSource;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.sax.SAXSource;
import org.xml.sax.InputSource;

//Fop
import org.apache.fop.apps.Driver;
import org.apache.fop.messaging.MessageHandler;
import org.apache.avalon.framework.logger.ConsoleLogger;
import org.apache.avalon.framework.logger.Logger;

import com.tsp.operation.model.beans.*;
import java.util.*;


public class RemesaPDF {
    
    /** Creates a new instance of RemesaPDF */
    public RemesaPDF() {
    }
    
    public Element CreacionPlantillaRemesa(List lista) {
        Element raiz = DocumentHelper.createElement("raiz");
        Iterator it = lista.iterator();
        
        while(it.hasNext()){
            DatosRemesaImpPDF datos = (DatosRemesaImpPDF)it.next();           
            Element data = null;
            Vector copias = datos.getCopias();
            for (int i = 0; i < copias.size(); i++) {
                data = raiz.addElement("data");
                String trafico = (String)copias.elementAt(i);
                data.addElement("trafico").addText(trafico.split("/")[0]);
                data.addElement("trafico2").addText(trafico.split("/")[1]);
                Element tabla1 = data.addElement("tabla1");
                //  Element tabla2 = data.addElement("tabla2");
                Element tabla3 = data.addElement("tabla3");
                
                //Creamos un hijos para el tabla1
                // Element leyenda = tabla1.addElement("leyenda").addText("");
                String Oorigen = (datos.getOficina_origen()!= null )?datos.getOficina_origen():"";
                Oorigen = (Oorigen.length() > 21 )?Oorigen.substring(0,21):Oorigen;
                Element oorigen = tabla1.addElement("oorigen").addText(Oorigen);
                
                Element dia = tabla1.addElement("dia").addText(datos.getFecha().substring(0,4));
                Element mes = tabla1.addElement("mes").addText(datos.getFecha().substring(5,7));
                Element ano = tabla1.addElement("ano").addText(datos.getFecha().substring(8,10));
                Element cargan = tabla1.addElement("cargan").addText((datos.getNumero_remesa()!= null )?datos.getNumero_remesa():"");                
                
                String Corigen = (datos.getOrigen()!= null )?datos.getOrigen():"";
                Corigen = (Corigen.length() > 21)?Corigen.substring(0,21):Corigen;
                Element corigen = tabla1.addElement("corigen").addText(Corigen);
                
                String Remitente = (datos.getRemitente()!= null )?datos.getRemitente():"";
                Remitente = (Remitente.length() > 40)?Remitente.substring(0,40):Remitente;
                Element remitente = tabla1.addElement("remitente").addText(Remitente);
                
                Element consignatario = tabla1.addElement("consignatario").addText("");
                
                String Destinatario = (datos.getDestinatario()!= null )?datos.getDestinatario():"";                
                Destinatario = (Destinatario.length() > 40)?Destinatario.substring(0,40):Destinatario;
                Element destinatario = tabla1.addElement("destinatario").addText(Destinatario);
                
                String Direccion = (datos.getDireccion()!= null )?datos.getDireccion():"";
                Direccion = (Direccion.length() > 30)?Direccion.substring(0,30):Direccion;
                Element direccion = tabla1.addElement("direccion").addText(Direccion); 
                
                String Ciudad = (datos.getCiudad()!= null )?datos.getCiudad():"";
                Ciudad = (Ciudad.length() > 21)?Ciudad.substring(0,21):Ciudad;
                Element ciudad = tabla1.addElement("ciudad").addText(Ciudad);
                
                Element planillas = tabla1.addElement("planilla").addText((datos.getNumpla()!= null )?datos.getNumpla():"");
                Element placas = tabla1.addElement("placas").addText((datos.getPlacas()!= null )?datos.getPlacas():"");
                String cond     =  (datos.getConductor()!= null )?datos.getConductor():"";
                String conduaux =  ( !cond.equals("null - null"))?cond:"";
                Element conductor = tabla1.addElement("conductor").addText(conduaux);
                
                
                
                Element valores = data.addElement("valores");
                String doc =  datos.getDocinternos().length()>579?datos.getDocinternos().substring(0,580):datos.getDocinternos();//jose 2007-04-09
                Element ley = valores.addElement("leyenda2").addText(doc);
                String contenedores = "";
                if( datos.getContenedores() != null  ){
                    contenedores = (!datos.getContenedores().equals(""))?" Contenedore(s) :  " + datos.getContenedores():"";
                }
                
                Element contendores = valores.addElement("contenedores").addText(contenedores);
                String saux = (datos.getSellos()!=null)?datos.getSellos():"";
                String s = (!saux.equals(",,"))?saux:"";
                String sfinal = (!s.equals("null"))?s:"";
                Element sellos = valores.addElement("sellos").addText("SELLOS : " + s );
                //Element sellos = valores.addElement("sellos").addText("SELLOS : " + sfinal);
                String Texto = (datos.getFiduciaria().equals("S"))?datos.getTextooc():"";
                Element textooc = valores.addElement("textooc").addText(Texto);
                Element item = null;
                
            /* for (int i = 0; i < detalle.size(); i++) {
                 System.err.println("entra en detalle 1  " + detalle.size() + " " +
                 detalle.elementAt(i));
                 Item1 producto = (Item1) detalle.elementAt(i);
                 //elemento.add(producto.obtenerElemento());*/
                item = valores.addElement("item");
                item.addElement("marcas").addText(" Segun documento(s) internos del Cliente : ");
                 /*item.addElement("bultos").addText(obtenerTexto(producto.getBultos()));
                 item.addElement("kilos").addText(obtenerTexto(producto.getKilos()));*/
                item.addElement("mercancia").addText("");
                //}
                
                //Creamos un hijos para el tabla3/*
                Element flete = tabla3.addElement("flete").addText("XXX");
                Element pagadero = tabla3.addElement("pagadero").addText("XXX");
                Element nit = tabla3.addElement("nit").addText("XXX");
                Element ciudad1 = tabla3.addElement("ciudad").addText("XXX");
                Element manifiesto = tabla3.addElement("manifiesto").addText("XXX");
                Element aduana = tabla3.addElement("aduana").addText("XXX");
                Element jefe = tabla3.addElement("jefe").addText((datos.getDespachador()!= null )?datos.getDespachador().toUpperCase():"");
                
            }
        }
        return raiz;
    }
    
    public void generarPDF(File xslt, File pdf, List lista) throws
    Exception {
        org.apache.fop.configuration.Configuration.put("baseDir",
        "file:///" + xslt.getParent());
        //Crear Documento
        Document documento = DocumentHelper.createDocument(CreacionPlantillaRemesa(lista));
        ProformaXMLReader proformaXMLReader = new ProformaXMLReader(documento);
        
        //Establecer el formato de salida a PDF
        Driver driver = new Driver();
        //Setup logger
        Logger logger = new ConsoleLogger(ConsoleLogger.LEVEL_INFO);
        driver.setLogger(logger);
        //MessageHandler.setScreenLogger(logger);
        
        driver.setRenderer(driver.RENDER_PDF);
        
        //Cofigurar la salida
        FileOutputStream salida = new FileOutputStream(pdf);
        
        try {
            
            driver.setOutputStream(salida);
            
            //Configurar el XSLT
            TransformerFactory factory = TransformerFactory.newInstance();
            Transformer transformer = factory.newTransformer(new StreamSource(
            xslt));
            //Configurar la entrada del XSLT para la transformación
            InputSource inputSource = new InputSource();
            //inputSource.setEncoding("ISO-8859-1");
            inputSource.setEncoding("windows-1252");
            SAXSource source = new SAXSource(proformaXMLReader, inputSource);
            
            SAXResult res = new SAXResult(driver.getContentHandler());
            
            //Iniciar la transformación XSLT y el procesamiento FOP
            transformer.transform(source, res);
            
            //Obtenemos el número de páginas
            String pageCount = Integer.toString(driver.getResults().getPageCount());
            
            //Inicializamos todo para la segunda transformación
            driver = new Driver();
            
            //Setup logger
            Logger logger1 = new ConsoleLogger(ConsoleLogger.LEVEL_INFO);
            driver.setLogger(logger1);
            MessageHandler.setScreenLogger(logger1);
            
            driver.setRenderer(driver.RENDER_PDF);
            salida = new FileOutputStream(pdf);
            driver.setOutputStream(salida);
            factory = TransformerFactory.newInstance();
            transformer = factory.newTransformer(new StreamSource(xslt));
            inputSource = new InputSource();
            //inputSource.setEncoding("ISO-8859-1");
            inputSource.setEncoding("windows-1252");
            res = new SAXResult(driver.getContentHandler());
            
            //Enviamos el parámetro
            transformer.setParameter("page-count", pageCount);
            
            //... y realizamos de nuevo la transformación
            transformer.transform(source, res);
        }
        finally {
            salida.close();
        }
    }
    
    protected String obtenerTexto(Object obj) {
        if ( (obj == null) || (obj.toString().toLowerCase().trim().equals("ninguno"))) {
            return "";
        }
        else {
            return obj.toString();
        }
    }
    
}
