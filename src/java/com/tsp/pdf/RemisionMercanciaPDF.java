
/*****************************************************************************
 * Nombre clase :                   RemisionMercanciaPDF.java                    *
 * Descripcion :                    Clase auxiliar que maneja los eventos    *
 *                                  relacionados con la generacion de los    *
 *                                  archivos XML y PDF para la impresion     *
 *                                  de las Planilla                          *
 * Autor :                          Ing. Juan Manuel Escandon Perez          *
 * Fecha :                          5 de enero de 2006, 02:31 PM             *
 * Version :                        1.0                                      *
 * Copyright :                      Fintravalores S.A.                  *
 *****************************************************************************/
package com.tsp.pdf;

//dom4j
import org.dom4j.Element;
import org.dom4j.DocumentHelper;
import org.dom4j.Document;

//Java
import java.io.File;
import java.io.*;

//JAXP
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamSource;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.sax.SAXSource;
import org.xml.sax.InputSource;

//Fop
import org.apache.fop.apps.Driver;
import org.apache.fop.messaging.MessageHandler;
import org.apache.avalon.framework.logger.ConsoleLogger;
import org.apache.avalon.framework.logger.Logger;

import com.tsp.operation.model.beans.*;
import java.util.*;

import com.tsp.util.*;


public class RemisionMercanciaPDF {
    
    private String periodo;
    private String fechaactual;
    
    Element raiz;
    Element data;
    
    Element tabla1;
    Element tabla2;
    Element tabla3;
    Element tabla4;
    Element tabla5;
    Element tabla6;
    Element tabla7;
    Element tabla8;
    Element tabla9;
    Element tabla10;
    File xslt;
    File pdf;
    
    
    
    /** Creates a new instance of PlanillaPDF */
    public RemisionMercanciaPDF() {
    }
    
    public void RemisionPlantilla( File xslt, File pdf ) {
        
        this.xslt = xslt;
        this.pdf = pdf;
        
        ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
        String fecha = "";
        String ruta = "";
        ruta = rb.getString("ruta");
        
        
        pdf  = new File(ruta + "/pdf/remision.pdf");
        xslt = new File(ruta + "/Templates/RemisionMercancia.xsl");
        
    }
    
    public void crearCabecera() {
        
        raiz = DocumentHelper.createElement("raiz");
        tabla1  = raiz.addElement("tabla1");
        tabla2  = raiz.addElement("tabla2");
        tabla3  = raiz.addElement("tabla3");
        tabla4  = raiz.addElement("tabla4");
        tabla5  = raiz.addElement("tabla5");
        tabla6  = raiz.addElement("tabla6");
        tabla7  = raiz.addElement("tabla7");
        tabla8  = raiz.addElement("tabla8");
        tabla9  = raiz.addElement("tabla9");
        tabla10 = raiz.addElement("tabla10");
        
    }
    
    
    public void crearRemision( DatoFormato dat ){
        Util u = new Util();
        String fechaactual = u.getFechaActual_String(4);
        
        Element transportista   = tabla1.addElement("transportista").addText("TRANSPORTE SANCHEZ POLO");
        
        String datosagencia     = (dat.getCadena_datos()!=null)?dat.getCadena_datos():"";
        
        String suc      = "";
        String dirsuc   = "";
        String telsuc   = "";
        
        if( datosagencia.length() > 2 ){
            if( datosagencia.split("|").length > 2 ){
                suc              = datosagencia.split("|")[0];
                dirsuc           = datosagencia.split("|")[1];
                telsuc           = datosagencia.split("|")[2];
            }
        }
        
        Element esuc            = tabla1.addElement("sucursal").addText(suc);
        Element edirsuc         = tabla1.addElement("direccion").addText(dirsuc);
        Element etelsuc         = tabla1.addElement("telefono").addText(telsuc);
        
        Element remesa          = tabla1.addElement("remesa").addText((dat.getOt()!=null)?dat.getOt():"");
        Element remision        = tabla1.addElement("remision").addText((dat.getRemesion()!=null)?dat.getRemesion():"");
        
        Element fechaa          = tabla1.addElement("fechaa").addText(fechaactual);
        Element fechadsp        = tabla1.addElement("fechadsp").addText((dat.getFechadespacho()!=null)?dat.getFechadespacho():"");
        Element horadsp         = tabla1.addElement("horadsp").addText((dat.getHoradespacho()!=null)?dat.getHoradespacho():"");
        
        
        String placa            = dat.getPlaca();
        
        Element letra1          = tabla2.addElement("letra1").addText(placa.substring(0,1));
        Element letra2          = tabla2.addElement("letra2").addText(placa.substring(1,2));
        Element letra3          = tabla2.addElement("letra3").addText(placa.substring(2,3));
        /*Letras Placa*/
        
        Element numero1          = tabla2.addElement("numero1").addText(placa.substring(3,4));
        Element numero2          = tabla2.addElement("numero2").addText(placa.substring(4,5));
        Element numero3          = tabla2.addElement("numero3").addText(placa.substring(5,6));
        /*Numeros Placa*/
        
        /*Tabla 2*/
        Element conductor        = tabla2.addElement("conductor").addText((dat.getNomcond()!=null)?dat.getNomcond():"");
        Element cedula           = tabla2.addElement("cedula").addText((dat.getCedcond()!=null)?dat.getCedcond():"");
        Element celular          = tabla2.addElement("celular").addText((dat.getCelularcond()!=null)?dat.getCelularcond():"");
        
        /*Tabla 3*/
        Element cant_piezas      = tabla3.addElement("cant_piezas").addText((dat.getPiezas()!=null)?dat.getPiezas():"");
        Element peso             = tabla3.addElement("peso").addText((dat.getPeso()!=null)?dat.getPeso():"");
        Element tipo_veh         = tabla3.addElement("tipo_veh").addText((dat.getTipo()!=null)?dat.getTipo():"");
        Element DesRemision     = tabla3.addElement("DesRemision").addText((dat.getDesRemision()!=null)?dat.getDesRemision():"");
      
        String ruta              =(dat.getRuta()!=null)?dat.getRuta():"";
                    
        
        String sorigen           = "";
        String sdestino          = "";
        
        if( ruta.split("-").length > 1 ){
            sorigen                 = ruta.split("-")[0];
            sdestino                = ruta.split("-")[1];
        }
        
        Element origen           = tabla3.addElement("origen").addText(sorigen);
        Element destino          = tabla3.addElement("destino").addText(sdestino);
        
        Element centro           = tabla3.addElement("centro").addText((dat.getCentrocosto()!=null)?dat.getCentrocosto():"");
        Element compensac        = tabla3.addElement("compensac").addText((dat.getCompensac()!=null)?dat.getCompensac():"");
        
        /*Tabla4*/
        Element origen1         = tabla4.addElement("origen1").addText((dat.getOrigen()!=null)?dat.getOrigen():"");
        Element direccion1      = tabla4.addElement("direccion1").addText((dat.getDirorigen()!=null)?dat.getDirorigen():"");
        Element tel1            = tabla4.addElement("tel1").addText((dat.getTelorigen()!=null)?dat.getTelorigen():"");
        Element contacto1       = tabla4.addElement("contacto1").addText((dat.getContactoorigen()!=null)?dat.getContactoorigen():"");
        
        Element origen2         = tabla4.addElement("origen2").addText("-");
        Element direccion2      = tabla4.addElement("direccion2").addText("");
        Element tel2            = tabla4.addElement("tel2").addText("");
        Element contacto2       = tabla4.addElement("contacto2").addText("");
        
        Element origen3         = tabla4.addElement("origen3").addText("-");
        Element direccion3      = tabla4.addElement("direccion3").addText("");
        Element tel3            = tabla4.addElement("tel3").addText("");
        Element contacto3       = tabla4.addElement("contacto3").addText("");
        
        Element origen4         = tabla4.addElement("origen4").addText("-");
        Element direccion4      = tabla4.addElement("direccion4").addText("");
        Element tel4            = tabla4.addElement("tel4").addText("");
        Element contacto4       = tabla4.addElement("contacto4").addText("");
        
        Element origen5         = tabla4.addElement("origen5").addText("-");
        Element direccion5      = tabla4.addElement("direccion5").addText("");
        Element tel5            = tabla4.addElement("tel5").addText("");
        Element contacto5       = tabla4.addElement("contacto5").addText("");
        
        /*Tabla5*/
        
        Element destino1        = tabla5.addElement("destino1").addText((dat.getDestino()!=null)?dat.getDestino():"");
        Element direcciond1     = tabla5.addElement("direcciond1").addText((dat.getDirdestino()!=null)?dat.getDirdestino():"");
        Element teld1           = tabla5.addElement("teld1").addText((dat.getTeldestino()!=null)?dat.getTeldestino():"");
        Element contactod1      = tabla5.addElement("contactod1").addText((dat.getContactodestino()!=null)?dat.getContactodestino():"");
        
        Element destino2         = tabla5.addElement("destino2").addText("-");
        Element direcciond2      = tabla5.addElement("direcciond2").addText("");
        Element teld2            = tabla5.addElement("teld2").addText("");
        Element contactod2       = tabla5.addElement("contactod2").addText("");
        
        Element destino3         = tabla5.addElement("destino3").addText("-");
        Element direcciond3      = tabla5.addElement("direcciond3").addText("");
        Element teld3            = tabla5.addElement("teld3").addText("");
        Element contactod3       = tabla5.addElement("contactod3").addText("");
        
        Element destino4         = tabla5.addElement("destino4").addText("-");
        Element direcciond4      = tabla5.addElement("direcciond4").addText("");
        Element teld4            = tabla5.addElement("teld4").addText("");
        Element contactod4       = tabla5.addElement("contactod4").addText("");
        
        Element destino5         = tabla5.addElement("destino5").addText("-");
        Element direcciond5      = tabla5.addElement("direcciond5").addText("");
        Element teld5            = tabla5.addElement("teld5").addText("");
        Element contactod5       = tabla5.addElement("contactod5").addText("");
        
        
        /*Tabla 6*/
        Element elementodo        = tabla6.addElement("do").addText((dat.getElementodo()!=null)?dat.getElementodo():"");
        
        /*Tabla 7*/
        Element elementopo        = tabla7.addElement("po").addText((dat.getPo()!=null)?dat.getPo():"");
        
        /*Tabla 8*/
        Element contenido         = tabla8.addElement("contenido").addText((dat.getContenido()!=null)?dat.getContenido():"");
        
        /*Tabla 9*/
        Element observaciones     = tabla9.addElement("observaciones").addText((dat.getObservaciones()!=null)?dat.getObservaciones():"");
        
        /*Tabla 10*/
        Element despachador     = tabla10.addElement("despachador").addText((dat.getNombreenvio()!=null)?dat.getNombreenvio().toUpperCase():"");
        
        Element fecha     = tabla10.addElement("fechaactual").addText(fechaactual);
        
    }
    
    
    
    public void generarPDF() throws Exception {
        org.apache.fop.configuration.Configuration.put("baseDir",
        "file:///" + xslt.getParent());
        //Crear Documento
        Document documento = DocumentHelper.createDocument(raiz);
        ProformaXMLReader proformaXMLReader = new ProformaXMLReader(documento);
        
        //Establecer el formato de salida a PDF
        Driver driver = new Driver();
        //Setup logger
        Logger logger = new ConsoleLogger(ConsoleLogger.LEVEL_INFO);
        driver.setLogger(logger);
        //MessageHandler.setScreenLogger(logger);
        
        driver.setRenderer(driver.RENDER_PDF);
        
        //Cofigurar la salida
        FileOutputStream salida = new FileOutputStream(pdf);
        
        try {
            
            driver.setOutputStream(salida);
            
            //Configurar el XSLT
            TransformerFactory factory = TransformerFactory.newInstance();
            Transformer transformer = factory.newTransformer(new StreamSource(
            xslt));
            //Configurar la entrada del XSLT para la transformación
            InputSource inputSource = new InputSource();
            //inputSource.setEncoding("ISO-8859-1");
            inputSource.setEncoding("windows-1252");
            SAXSource source = new SAXSource(proformaXMLReader, inputSource);
            
            SAXResult res = new SAXResult(driver.getContentHandler());
            
            //Iniciar la transformación XSLT y el procesamiento FOP
            transformer.transform(source, res);
            
            //Obtenemos el número de páginas
            String pageCount = Integer.toString(driver.getResults().getPageCount());
            
            //Inicializamos todo para la segunda transformación
            driver = new Driver();
            
            //Setup logger
            Logger logger1 = new ConsoleLogger(ConsoleLogger.LEVEL_INFO);
            driver.setLogger(logger1);
            MessageHandler.setScreenLogger(logger1);
            
            driver.setRenderer(driver.RENDER_PDF);
            salida = new FileOutputStream(pdf);
            driver.setOutputStream(salida);
            factory = TransformerFactory.newInstance();
            transformer = factory.newTransformer(new StreamSource(xslt));
            inputSource = new InputSource();
            //inputSource.setEncoding("ISO-8859-1");
            inputSource.setEncoding("windows-1252");
            res = new SAXResult(driver.getContentHandler());
            
            //Enviamos el parámetro
            transformer.setParameter("periodo", this.periodo);
            transformer.setParameter("page-count", pageCount);
            transformer.setParameter("fechaactual", this.fechaactual);
            
            //... y realizamos de nuevo la transformación
            transformer.transform(source, res);
        }
        finally {
            salida.close();
        }
    }
    
    protected String obtenerTexto(Object obj) {
        if ( (obj == null) || (obj.toString().toLowerCase().trim().equals("ninguno"))) {
            return "";
        }
        else {
            return obj.toString();
        }
    }
    
    
    public static void main(String[] arg ) throws Exception{
        
        File pdf   = new File("C:/fintravalores/remision.pdf");
        File xslt  = new File("C:/fintravalores/Templates/RemisionMercancia.xsl");
        
        RemisionMercanciaPDF remision = new RemisionMercanciaPDF();
        
        remision.crearCabecera();
        
        // remision.crearRemision( dato );
        
        remision.RemisionPlantilla(xslt, pdf);
        
        remision.generarPDF();
        
        
    }
    
}
