package com.tsp.pdf;

import org.dom4j.*;
import org.xml.sax.*;
//import aplicacionwebfinal.tools.*;
import java.util.List;

public class ProformaXMLReader extends AbstractObjectReader {

    private Document proforma;

    public ProformaXMLReader(Document doc) {
        proforma = doc;
    }

    public void parse(InputSource input) throws SAXException {
        handler.startDocument();
        recorrerArbol(proforma.getRootElement());
        handler.endDocument();
    }

    private void recorrerArbol(Element elemento) throws SAXException {
        List elementos = elemento.elements();

        if ( elementos.isEmpty() ) {
            handler.element(elemento.getName(), elemento.getText());
        }
        else {
            handler.startElement(elemento.getName());
            for (int i = 0; i < elementos.size(); i++) {
                recorrerArbol((Element)elementos.get(i));
            }
            handler.endElement(elemento.getName());
        }
    }
}

