/********************************************************************
 *      Nombre Clase.................   HojaVidaPDF.java
 *      Descripci�n..................   Generarcion de documentos en formato PDF
 *      Autor........................   Ing. Andr�s Maturana De La Cruz
 *      Fecha........................   21 Abril del 2006
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes S�nchez Polo S.A.
 *******************************************************************/

package com.tsp.pdf;
//dom4j
import org.dom4j.Element;
import org.dom4j.DocumentHelper;
import org.dom4j.Document;

//java
import java.io.*;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamSource;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.sax.SAXSource;
import org.xml.sax.InputSource;

//Fop
import org.apache.fop.apps.Driver;
import org.apache.fop.messaging.MessageHandler;
import org.apache.avalon.framework.logger.ConsoleLogger;
import org.apache.avalon.framework.logger.Logger;

import com.tsp.operation.model.beans.*;

public class HojaVidaPDF {
    
    /** Creates a new instance of HojaControlViaje */
    public HojaVidaPDF() {
    }
    
    public Element HojaVida( HojaVida hv ) {
        
        
        Element raiz = DocumentHelper.createElement("raiz");
        if( hv != null ){
            Element veh = raiz.addElement("vehiculo");
            Element cond = raiz.addElement("conductor");
            Element prop = raiz.addElement("propietario");
            Element reg = raiz.addElement("ctrl_reg");
            
            /* Informaci�n de la Placa */
            Placa placa = hv.getPlaca();
            if( placa!=null ){
                veh.addElement("placab").addText(obtenerTexto(placa.getPlaca()));
                veh.addElement("ejes").addText(obtenerTexto(placa.getNoejes()));
                veh.addElement("tcarroceria").addText(obtenerTexto(placa.getCarroceria()));
                veh.addElement("tipo").addText(obtenerTexto(placa.getTipo()));
                veh.addElement("reg_nal").addText(obtenerTexto(placa.getReg_nal_carga()));
                veh.addElement("venreg").addText(obtenerTexto(placa.getFecvenreg()));
                veh.addElement("chasis").addText(obtenerTexto(placa.getNochasis()));
                veh.addElement("platlr").addText(obtenerTexto(placa.getPlaca_trailer()));
                veh.addElement("nrin").addText(obtenerTexto(placa.getNumero_rin()));
                veh.addElement("motor").addText(obtenerTexto(placa.getNomotor()));
                veh.addElement("captlr").addText(obtenerTexto(placa.getCapacidad_trailer()));
                veh.addElement("riesgo").addText(obtenerTexto(placa.getGrado_riesgo()!=0? String.valueOf(placa.getGrado_riesgo()) : ""));
                veh.addElement("color").addText(obtenerTexto(placa.getColor()));
                veh.addElement("gases").addText(obtenerTexto(placa.getCertgases()));
                veh.addElement("vengases").addText(obtenerTexto(placa.getFecvengases()));
                veh.addElement("marca").addText(obtenerTexto(placa.getMarca()));
                veh.addElement("polandina").addText(obtenerTexto(placa.getPoliza_andina()));
                veh.addElement("venpolandina").addText(obtenerTexto(placa.getFecvenandina()));
                veh.addElement("modelo").addText(obtenerTexto(placa.getModelo()));
                veh.addElement("soat").addText(obtenerTexto(placa.getPolizasoat()));
                veh.addElement("vensoat").addText(obtenerTexto(placa.getVenseguroobliga()));
                veh.addElement("capacidad").addText(obtenerTexto(placa.getCapacidad()));
                veh.addElement("empresa").addText(obtenerTexto(placa.getTarempresa()));
                veh.addElement("venempresa").addText(obtenerTexto(placa.getFecvenempresa()));
                veh.addElement("afiliacion").addText(obtenerTexto(placa.getEmpresaafil()));
                veh.addElement("tarhabil").addText(obtenerTexto(placa.getTarhabil()));
                veh.addElement("ventarhabil").addText(obtenerTexto(placa.getFecvenhabil()));
                veh.addElement("ciudad").addText(obtenerTexto(placa.getCiudadAfiliada()));
                veh.addElement("tarprop").addText(obtenerTexto(placa.getTarprop()));
                veh.addElement("ventarprop").addText(obtenerTexto(placa.getFecvenprop()));
                //veh.addElement("vehiculo_img").addText(obtenerTexto("url(documentos/imagenes/" + ((!hv.getFotoVehiculo().equals("")) ? hv.getFotoVehiculo() : "prede.jpg") + ")"));
                veh.addElement("vehiculo_img").addText(obtenerTexto("documentos/imagenes/" + ((!hv.getFotoVehiculo().equals("")) ? hv.getFotoVehiculo() : "prede.jpg") + ""));
            }
            
            /* Datos del conductor */
            Identidad iden = hv.getIdentidad();
            Conductor conductor = hv.getConductor();
            String estcivil =(iden!=null && iden.getEst_civil()!=null)?iden.getEst_civil():"";
            if (estcivil.equals("S")){
                estcivil ="SOLTERO";
            }
            else if (estcivil.equals("C")){
                estcivil="CASADO";
            }
            else if (estcivil.equals("V")){
                estcivil = "VIUDO";
            }
            else if (estcivil.equals("E")){
                estcivil = "SEPARADO";
            }
            else if (estcivil.equals("U")){
                estcivil = "UNION LIBRE";
            }
            String t11 =(iden!=null && iden.getPais1()!=null)?iden.getPais1():"";
            String t12 =(iden!=null && iden.getArea1()!=null)?iden.getArea1():"";
            String t13 =(iden!=null && iden.getnum1()!=null)?iden.getnum1():"";
            t11+=" "+t12+" "+t13;
            if( iden!=null ){
                cond.addElement("nombres").addText(obtenerTexto(iden.getNom1() + " " + iden.getNom2()));
                cond.addElement("celular").addText(obtenerTexto(iden.getCelular()));
                cond.addElement("cedula").addText(obtenerTexto(iden.getCedula()));
                cond.addElement("militar").addText(obtenerTexto(iden.getLibmilitar()));
                cond.addElement("apellidos").addText(obtenerTexto(iden.getApe1() + " " + iden.getApe2()));
                cond.addElement("ciudad").addText(obtenerTexto((iden.getCodciu()!=null? iden.getCodciu():"") + " " + ((iden.getCodpais()!=null)?iden.getCodpais().substring(0,3):"")));
                cond.addElement("expced").addText(obtenerTexto(iden.getExpced()));
                cond.addElement("lugarnac").addText(obtenerTexto((iden!=null && iden.getLugarnac()!=null)?iden.getLugarnac():""));
                cond.addElement("fechanac").addText(obtenerTexto((iden.getFechanac()!=null && !iden.getFechanac().equals("0099-01-01"))?iden.getFechanac():""));
                cond.addElement("direccion").addText(obtenerTexto(iden.getDireccion()));
                cond.addElement("barrio").addText(obtenerTexto(iden.getBarrio()));
            }
            
            if( conductor!=null){
                cond.addElement("rh").addText(obtenerTexto(conductor.getRh()));
                cond.addElement("riesgo").addText(obtenerTexto(conductor.getGradoriesgo().compareTo("0")==0? "" : conductor.getGradoriesgo()));
                cond.addElement("tripulante").addText(obtenerTexto(conductor.getNrolibtripulante()));
                cond.addElement("pase").addText(obtenerTexto(conductor.getPassNo()));
                cond.addElement("eps").addText(obtenerTexto(conductor.getNomeps()));
                cond.addElement("restrpase").addText(obtenerTexto(conductor.getDesrespase()));
                cond.addElement("neps").addText(obtenerTexto(conductor.getNroeps()));
                cond.addElement("catpase").addText(obtenerTexto(conductor.getPassCat()));
                cond.addElement("feceps").addText(obtenerTexto((conductor.getFecafieps()!=null && !conductor.getFecafieps().equals("0099-01-01"))?conductor.getFecafieps():""));
                cond.addElement("venpase").addText(obtenerTexto((conductor.getPassExpiryDate()!=null && !conductor.getPassExpiryDate().equals("0099-01-01"))?conductor.getPassExpiryDate():" "));
                cond.addElement("narp").addText(obtenerTexto((conductor.getNomarp()!=null && !conductor.getNomarp().equals("0099-01-01")?conductor.getNomarp():"")));
                cond.addElement("njudicial").addText(obtenerTexto(conductor.getNrojudicial()));
                cond.addElement("fecarp").addText(obtenerTexto((conductor.getFecafiarp()!=null && !conductor.getFecafiarp().equals("0099-01-01"))?conductor.getFecafiarp():""));
                cond.addElement("pasaporte").addText(obtenerTexto(conductor.getPassport()));
                cond.addElement("venpasaporte").addText(obtenerTexto((conductor.getPassportExpiryDate()!=null && !conductor.getPassportExpiryDate().equals("0099-01-01"))?conductor.getPassportExpiryDate():""));
                cond.addElement("nvisa").addText(obtenerTexto(conductor.getNrovisa()));
                cond.addElement("venvisa").addText(obtenerTexto((conductor.getVencevisa()!=null && !conductor.getVencevisa().equals("0099-01-01"))?conductor.getVencevisa():""));
                //cond.addElement("conductor_img").addText(obtenerTexto("url(documentos/imagenes/" + ((hv.getFotoConductor()!=null && !hv.getFotoConductor().equals("")) ? hv.getFotoConductor() : "prede.jpg") + ")"));
                cond.addElement("conductor_img").addText(obtenerTexto("documentos/imagenes/" + ((hv.getFotoConductor()!=null && !hv.getFotoConductor().equals("")) ? hv.getFotoConductor() : "prede.jpg") + ""));
                cond.addElement("estcivil").addText(obtenerTexto(estcivil));
                cond.addElement("telefono").addText(obtenerTexto(t11));
            }
            
            /* Datos del propietario y tenedor  */            
            Proveedor propietario = hv.getPropietario();
            Tenedor   tenedor = hv.getTenedor(); 
            
            if( propietario!=null ){
                prop.addElement("prop_nombre").addText(obtenerTexto(propietario.getNom1() + " " + propietario.getNom2()));
                prop.addElement("prop_ciudad").addText(obtenerTexto(propietario.getCiudad() + " " + propietario.getPais().substring(0,3)));
                prop.addElement("prop_apellido").addText(obtenerTexto(propietario.getApe1() + " " + propietario.getApe2()));
                prop.addElement("prop_tel").addText(obtenerTexto(propietario.getTelefono()));
                prop.addElement("prop_nit").addText(obtenerTexto(propietario.getC_nit()));
                prop.addElement("prop_movil").addText(obtenerTexto(propietario.getCelular()));
                prop.addElement("prop_expced").addText(obtenerTexto(propietario.getExpced()));
                prop.addElement("prop_sede").addText(obtenerTexto(propietario.getC_agency_id()));
                prop.addElement("prop_direccion").addText(obtenerTexto(propietario.getDireccion()));
            }
            
            if( tenedor!=null ){
                prop.addElement("ten_nombre").addText(obtenerTexto(tenedor.getNom1() + " " + tenedor.getNom2()));
                prop.addElement("ten_ciudad").addText(obtenerTexto(tenedor.getCiudad() + " " + tenedor.getPais().substring(0,3)));
                prop.addElement("ten_apellido").addText(obtenerTexto(tenedor.getApe1()+"  "+tenedor.getApe2()));
                prop.addElement("ten_tel").addText(obtenerTexto(tenedor.getTelefono()));
                prop.addElement("ten_cedula").addText(obtenerTexto(tenedor.getCedula()));
                prop.addElement("ten_movil").addText(obtenerTexto(tenedor.getCelular()));
                prop.addElement("ten_expced").addText(obtenerTexto(tenedor.getExpedicionced()));
                prop.addElement("ten_direccion").addText(obtenerTexto(tenedor.getDireccion()));
            }
            
            /* Control de Registro */
            if( placa!=null ){
                reg.addElement("creation_date").addText(obtenerTexto(placa.getFechacrea()!=null && placa.getFechacrea().length()>=11 ? placa.getFechacrea().substring(0,11): placa.getFechacrea() ));
                reg.addElement("creation_user").addText(obtenerTexto(placa.getUsuariocrea()));
                reg.addElement("last_update").addText(obtenerTexto(placa.getFechaultact()!=null && placa.getFechaultact().length()>=11 ? placa.getFechaultact().substring(0,11) : placa.getFechaultact()));
                reg.addElement("user_update").addText(obtenerTexto(placa.getUsuario()));
            }
            
            reg.addElement("creation_agency").addText(obtenerTexto((hv.getAgenUsuarioCrea()!=null && !hv.getAgenUsuarioCrea().equals(""))? hv.getAgenUsuarioCrea() :""));
            reg.addElement("leyenda_1").addText(obtenerTexto(hv.getLeyenda1()));
            reg.addElement("leyenda_2").addText(obtenerTexto(hv.getLeyenda2()));
            reg.addElement("agency_update").addText(obtenerTexto(hv.getAgenUsuarioMod()));
            /*reg.addElement("huella1_img").addText(obtenerTexto("url(documentos/imagenes/" + hv.getHuella_der() + ")"));
            reg.addElement("huella2_img").addText(obtenerTexto("url(documentos/imagenes/" + hv.getHuella_izq() + ")"));
            reg.addElement("firma_img").addText(obtenerTexto("url(documentos/imagenes/" + hv.getFirma() + ")"));*/
            reg.addElement("huella1_img").addText(obtenerTexto("documentos/imagenes/" + hv.getHuella_der() + ""));
            reg.addElement("huella2_img").addText(obtenerTexto("documentos/imagenes/" + hv.getHuella_izq() + ""));
            reg.addElement("firma_img").addText(obtenerTexto("documentos/imagenes/" + hv.getFirma() + ""));
            
            /* Informaci�n del Veto*/
            String veto = "";           
            if( placa!=null )
                veto += (placa.getVeto()!=null && placa.getVeto().equals("V"))? "VEHICULO REPORTADO" : "";
            
            if( propietario!=null )
                veto += ((propietario.getVeto()!=null && propietario.getVeto().equals("V"))? (veto.length()==0? "" : " - ") + "PROPIETARIO REPORTADO" : "");
            
            if( iden!=null )    
                veto += ((iden.getVeto()!=null && iden.getVeto().equals("V"))? (veto.length()==0? "" : " - ") +  "CONDUCTOR REPORTADO" : "");
            raiz.addElement("veto").addText(obtenerTexto(veto));
        }
        
        return raiz;
    }
    
    public void generarPDF(File xslt, File pdf, HojaVida hv) throws
    Exception {
        org.apache.fop.configuration.Configuration.put("baseDir",
        "file:///" + xslt.getParent());
        //Crear Documento
        Document documento = DocumentHelper.createDocument(HojaVida(hv));
        ProformaXMLReader proformaXMLReader = new ProformaXMLReader(documento);
        
        //Establecer el formato de salida a PDF
        Driver driver = new Driver();
        //Setup logger
        Logger logger = new ConsoleLogger(ConsoleLogger.LEVEL_INFO);
        driver.setLogger(logger);
        //MessageHandler.setScreenLogger(logger);
        
        driver.setRenderer(driver.RENDER_PDF);
        
        //Cofigurar la salida
        FileOutputStream salida = new FileOutputStream(pdf);
        
        try {
            
            driver.setOutputStream(salida);
            
            //Configurar el XSLT
            TransformerFactory factory = TransformerFactory.newInstance();
            Transformer transformer = factory.newTransformer(new StreamSource(xslt));
            //Configurar la entrada del XSLT para la transformaci�n
            InputSource inputSource = new InputSource();
            //inputSource.setEncoding("ISO-8859-1");
            inputSource.setEncoding("windows-1252");
            SAXSource source = new SAXSource(proformaXMLReader, inputSource);
            
            SAXResult res = new SAXResult(driver.getContentHandler());
            
            //Iniciar la transformaci�n XSLT y el procesamiento FOP
            transformer.transform(source, res);
            
            //Obtenemos el n�mero de p�ginas
            String pageCount = "1";
            //String pageCount = Integer.toString(driver.getResults().getPageCount());
            //System.err.println("N�mero de p�gs:" + pageCount);
            
            //Inicializamos todo para la segunda transformaci�n
            driver = new Driver();
            
            //Setup logger
            Logger logger1 = new ConsoleLogger(ConsoleLogger.LEVEL_INFO);
            driver.setLogger(logger1);
            MessageHandler.setScreenLogger(logger1);
            
            driver.setRenderer(driver.RENDER_PDF);
            salida = new FileOutputStream(pdf);
            driver.setOutputStream(salida);
            factory = TransformerFactory.newInstance();
            transformer = factory.newTransformer(new StreamSource(xslt));
            inputSource = new InputSource();
            //inputSource.setEncoding("ISO-8859-1");
            inputSource.setEncoding("windows-1252");
            res = new SAXResult(driver.getContentHandler());
            
            //Enviamos el par�metro
            transformer.setParameter("page-count", pageCount);
            
            //... y realizamos de nuevo la transformaci�n
            transformer.transform(source, res);
        }
        finally {
            salida.close();
        }
    }
    
    protected String obtenerTexto(Object obj) {
        if ( obj == null ) {
            return "";
        }
        else {
            return obj.toString();
        }
    }
    
}
