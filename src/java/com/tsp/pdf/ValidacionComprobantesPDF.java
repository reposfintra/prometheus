/*
 * Nombre        ValidacionComprobantesPDF.java
 * Descripci�n   Utilidad para la generaci�n del PDF
 * Autor         David Pi�a L�pez
 * Fecha         20 de junio de 2006, 10:53 AM
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */
package com.tsp.pdf;

import org.dom4j.Element;
import org.dom4j.DocumentHelper;
import org.dom4j.Document;
import java.io.*;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamSource;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.sax.SAXSource;
import org.xml.sax.InputSource;
import org.apache.fop.apps.Driver;
import org.apache.fop.messaging.MessageHandler;
import org.apache.avalon.framework.logger.ConsoleLogger;
import org.apache.avalon.framework.logger.Logger;
import java.lang.*;

public class ValidacionComprobantesPDF {
    Element raiz;
    Element data;
    File xslt;
    File pdf;
    /** Creates a new instance of ValidacionComprobantesPDF */
    public ValidacionComprobantesPDF( File xslt, File pdf ) {
        this.xslt = xslt;
        this.pdf = pdf;
    }    
    
    public void crearCabecera() {
        raiz = DocumentHelper.createElement("raiz");        
        data = raiz.addElement("data");
        data.addElement("cabecera");
    }
    public void agregarElementos( String info ){
        Element tabla = data.addElement("contenido_log");        
        tabla.addElement( "mensajelog" ).addText( info );
    }
    
    public void generarPDF() throws Exception {
        
        org.apache.fop.configuration.Configuration.put( "baseDir", "file:///" + xslt.getParent() );
        
        //Crear Documento
        //Element raiz = this.HojaControlReporte(info);
        Document documento = DocumentHelper.createDocument ( raiz );
        ProformaXMLReader proformaXMLReader = new ProformaXMLReader ( documento );
        
        //Establecer el formato de salida a PDF
        Driver driver = new Driver();
        
        //Setup logger
        Logger logger = new ConsoleLogger ( ConsoleLogger.LEVEL_INFO );
        driver.setLogger ( logger );
        
        driver.setRenderer ( driver.RENDER_PDF );
        
        //Cofigurar la salida
        FileOutputStream salida = new FileOutputStream ( pdf );
        
        try {
            
            driver.setOutputStream ( salida );
            
            //Configurar el XSLT
            TransformerFactory factory = TransformerFactory.newInstance();
            Transformer transformer = factory.newTransformer( new StreamSource ( xslt ) );
            
            //Configurar la entrada del XSLT para la transformaci�n
            InputSource inputSource = new InputSource();
            inputSource.setEncoding( "windows-1252" );
            SAXSource source = new SAXSource( proformaXMLReader, inputSource );
            SAXResult res = new SAXResult( driver.getContentHandler () );
            
            //Iniciar la transformaci�n XSLT y el procesamiento FOP
            transformer.transform( source, res );
            
            //Obtenemos el n�mero de p�ginas
            String pageCount = "1";
            
            //Inicializamos todo para la segunda transformaci�n
            driver = new Driver();
            
            //Setup logger
            Logger logger1 = new ConsoleLogger ( ConsoleLogger.LEVEL_INFO );
            driver.setLogger ( logger1 );
            MessageHandler.setScreenLogger ( logger1 );
            
            driver.setRenderer ( driver.RENDER_PDF );
            salida = new FileOutputStream ( pdf );
            driver.setOutputStream ( salida );
            factory = TransformerFactory.newInstance();
            transformer = factory.newTransformer(new StreamSource ( xslt ) );
            inputSource = new InputSource();
            inputSource.setEncoding ( "windows-1252" );
            res = new SAXResult ( driver.getContentHandler () );
            
            //Enviamos el par�metro
            transformer.setParameter ( "page-count", pageCount );
            
            //... y realizamos de nuevo la transformaci�n
            transformer.transform ( source, res );
            
        } finally {
            
            salida.close();
            
        }
    }
    
}
