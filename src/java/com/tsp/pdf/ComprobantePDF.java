/*
 * Comprobante.java
 *
 * Created on 3 de julio de 2006, 11:37 PM
 */

package com.tsp.pdf;


import org.dom4j.Element;
import org.dom4j.DocumentHelper;
import org.dom4j.Document;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamSource;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.sax.SAXSource;
import org.xml.sax.InputSource;
import org.apache.fop.apps.Driver;
import org.apache.fop.messaging.MessageHandler;
import org.apache.avalon.framework.logger.ConsoleLogger;
import org.apache.avalon.framework.logger.Logger;


import java.io.*;
import java.util.*;

import com.tsp.util.*;
import com.tsp.finanzas.contab.model.Model;
import com.tsp.finanzas.contab.model.beans.Comprobantes;


/**
 *
 * @author  Mario
 */
public class ComprobantePDF {
    
    Element raiz;
    File xslt;
    File pdf;
    Model model;
    
    
    /** Creates a new instance of Comprobante */
    public ComprobantePDF() {
    }
    
    
    /** Creates a new instance of PDF */
    public ComprobantePDF(File template, File output, Model model) {
        this.xslt = template;
        this.pdf = output;
        raiz = DocumentHelper.createElement("raiz");
        this.model = model;
    }
    
    
    
    public void generarPDF() throws Exception {
        
        org.apache.fop.configuration.Configuration.put( "baseDir", "file:///" + xslt.getParent() );
        
        //Crear Documento
        Document documento = DocumentHelper.createDocument( raiz );
        ProformaXMLReader proformaXMLReader = new ProformaXMLReader( documento );
        
        //Establecer el formato de salida a PDF
        Driver driver = new Driver();
        
        //Setup logger
        Logger logger = new ConsoleLogger( ConsoleLogger.LEVEL_INFO );
        driver.setLogger( logger );
        
        driver.setRenderer( driver.RENDER_PDF );
        
        //Cofigurar la salida
        FileOutputStream salida = new FileOutputStream( pdf );
        try {
            
            driver.setOutputStream( salida );
            
            //Configurar el XSLT
            TransformerFactory factory = TransformerFactory.newInstance();
            Transformer transformer = factory.newTransformer( new StreamSource( xslt ) );
            
            //Configurar la entrada del XSLT para la transformación
            InputSource inputSource = new InputSource();
            inputSource.setEncoding( "windows-1252" );
            SAXSource source = new SAXSource( proformaXMLReader, inputSource );
            SAXResult res = new SAXResult( driver.getContentHandler() );
            
            //Iniciar la transformación XSLT y el procesamiento FOP
            transformer.transform( source, res );
            
            //Obtenemos el número de páginas
            String pageCount = Integer.toString(driver.getResults().getPageCount());
            
            //Inicializamos todo para la segunda transformación
            driver = new Driver();
            
            //Setup logger
            Logger logger1 = new ConsoleLogger( ConsoleLogger.LEVEL_INFO );
            driver.setLogger( logger1 );
            MessageHandler.setScreenLogger( logger1 );
            
            driver.setRenderer( driver.RENDER_PDF );
            salida = new FileOutputStream( pdf );
            driver.setOutputStream( salida );
            factory = TransformerFactory.newInstance();
            transformer = factory.newTransformer(new StreamSource( xslt ) );
            inputSource = new InputSource();
            inputSource.setEncoding( "windows-1252" );
            res = new SAXResult( driver.getContentHandler() );
            
            //Enviamos el parámetro
            transformer.setParameter( "page-count", pageCount );
            
            //... y realizamos de nuevo la transformación
            transformer.transform( source, res );
            
        } finally {
            salida.close();
        }
    }
    
    
    public void prepararPDF(Vector vector) throws Exception{        
        try{
            if (vector!=null && !vector.isEmpty()){
                for (int i=0;i<vector.size();i++){
                    Element data = raiz.addElement("data");
                    Element cabecera = data.addElement("comprobante");
                    Comprobantes c = (Comprobantes) vector.get(i);

                    cabecera.addElement("distrito"        ).addText( c.getDstrct   () );
                    cabecera.addElement("tipodoc"         ).addText( c.getTipodoc  () );
                    cabecera.addElement("numdoc"          ).addText( c.getNumdoc   () );
                    cabecera.addElement("numtransaccion"  ).addText( c.getGrupo_transaccion()+"" );
                    cabecera.addElement("periodo"         ).addText( c.getPeriodo()  );
                    cabecera.addElement("fecha"           ).addText( formatearFecha(c.getFechadoc()) );
                    cabecera.addElement("detalle"         ).addText( c.getDetalle()  );
                    cabecera.addElement("tercero"         ).addText( c.getTercero() );
                    cabecera.addElement("application_date").addText( formatearFecha(c.getFecha_aplicacion()) );
                    cabecera.addElement("creation_user"   ).addText( c.getUsuario() );
                    cabecera.addElement("creation_date"   ).addText( formatearFecha(c.getFecha_creacion()) );


                    model.comprobanteService.getDetallesComprobante(c);
                    Vector detalle  = model.comprobanteService.getVector();
                    double tdebito  = 0;
                    double tcredito = 0;
                    for (int j = 0; j< detalle.size(); j++){
                        Comprobantes d = (Comprobantes) detalle.get(j);
                                           
                        Element item = cabecera.addElement("item");
                        item.addElement("cuenta"  ).addText( d.getCuenta() );
                        item.addElement("auxiliar").addText( d.getAuxiliar() );
                        item.addElement("detalle" ).addText( d.getDetalle() );
                        item.addElement("debito"  ).addText( (d.getTotal_debito() !=0?UtilFinanzas.customFormat("#,##0.00",d.getTotal_debito() , 2):"") );
                        item.addElement("credito" ).addText( (d.getTotal_credito()!=0?UtilFinanzas.customFormat("#,##0.00",d.getTotal_credito(), 2):"") );
                        item.addElement("tercero" ).addText( d.getTercero()     );
                        item.addElement("tdocrel" ).addText( d.getTipodoc_rel() );
                        item.addElement("docrel"  ).addText( d.getNumdoc_rel()  );
                        
                        tdebito  += d.getTotal_debito();
                        tcredito += d.getTotal_credito();
                    }
                    cabecera.addElement("tdebito" ).addText( UtilFinanzas.customFormat("#,##0.00", tdebito  , 2) );
                    cabecera.addElement("tcredito").addText( UtilFinanzas.customFormat("#,##0.00", tcredito , 2) );
                    
                    if ((i+1)<vector.size()) data.addElement("newpage");
                }
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }
    
    public String formatearFecha (String date) throws Exception{
        if(date.indexOf(".")!=-1)
            date = date.split("\\.")[0];
        if (date.equals("0099-01-01") || date.equals("0099-01-01 00:00:00"))
            date = "";
        return date;
    }    
}