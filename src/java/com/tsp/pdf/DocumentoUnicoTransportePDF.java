/********************************************************************
 *      Nombre Clase.................   DocumentoUnicoTransportePDF.java
 *      Descripción..................   Generacion del Documento Unico de Transporte
 *      Autor........................   Osvaldo Pérez Ferrer
 *      Fecha........................   22 de Julio de 2006
 *      Versión......................   1.0
 *      Copyright....................   Transportes Sánchez Polo S.A.
 *******************************************************************/

/*
 * DocumentoUnicoTransporte.java
 *
 * Created on 22 de julio de 2006, 09:40 AM
 */

package com.tsp.pdf;

/**
 *
 * @author  Osvaldo
 */

import org.dom4j.Element;
import org.dom4j.DocumentHelper;
import org.dom4j.Document;
import java.io.*;
import java.util.*;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamSource;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.sax.SAXSource;
import org.xml.sax.InputSource;
import org.apache.fop.apps.Driver;
import org.apache.fop.messaging.MessageHandler;
import org.apache.avalon.framework.logger.ConsoleLogger;
import org.apache.avalon.framework.logger.Logger;
import com.tsp.operation.model.beans.*;
import java.lang.*;

import com.tsp.util.*;


public class DocumentoUnicoTransportePDF {
    
    
    
    
    Element raiz;
    Element data;
    Element tabla;
    File xslt;
    File pdf;
    Remision remision;
    
    
    /** Creates a new instance of DocumentoUnicoTransporte */
    public DocumentoUnicoTransportePDF() {
        this.xslt = xslt;
        this.pdf = pdf;
        this.remision = remision;
        
        
        ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
        String fecha = "";
        String ruta = "";
        ruta = rb.getString("ruta");
        
        
        pdf = new File(ruta +"/pdf/dut.pdf");
        xslt = new File(ruta + "/Templates/documentoUnicoTransporte.xsl");
        
    }
    
    public void crearCabecera() {
        
        raiz = DocumentHelper.createElement("raiz");
        
    }
    
    public void agregarElementos(Remision remision, String copia){
        
        
        data = raiz.addElement("data");
        
        try{
            
            
            Element tabla = data.addElement("tabla");
            
            tabla.addElement("remision").addText( (remision.getNoRemision() != null)? remision.getNoRemision() : "" );
            tabla.addElement("egreso").addText( (remision.getEgreso() != null)? remision.getEgreso() : "" );
            tabla.addElement("planilla").addText( (remision.getRemision() != null)? remision.getRemision() : "" );
            tabla.addElement("remesa").addText( (remision.getRemesa() != null)? remision.getRemesa() : "" );
            tabla.addElement("fecha").addText( (remision.getFechaCumplido() != null)? remision.getFechaCumplido() : "" );
            tabla.addElement("placa").addText( (remision.getPlaca() != null)? remision.getPlaca() : "" );
            
            tabla.addElement("contrato").addText( (remision.getcontrato() != null)? remision.getcontrato() : "" );
            tabla.addElement("propietario").addText( (remision.getPropietario() != null)? remision.getPropietario() : "" );
            
            tabla.addElement("cumplido").addText( (remision.getCantidadCarga() + ((remision.getUnidadValorizacion() != null )? remision.getUnidadValorizacion() : "" ) ) );
            tabla.addElement("efectivo").addText( (Util.customFormat(remision.getAnticipo()).equals(".00"))? "0.0" : Util.customFormat(remision.getAnticipo()) );
            
            tabla.addElement("acpm").addText( (remision.getcantidadACPM()+" GAL. "+ ((remision.getProveedorACPM() != null)? remision.getProveedorACPM() : "" ) ) );
            tabla.addElement("vlracpm").addText( (Util.customFormat(remision.getValorACPM()).equals(".00"))? "0.0" : Util.customFormat(remision.getValorACPM()) );
            
            tabla.addElement("peajes").addText( remision.getCantidadPeajes() + remision.getSucursalPeaje() );
            tabla.addElement("vlrpeajes").addText( (Util.customFormat(remision.getValorPeajes()).equals(".00"))? "0.0" : Util.customFormat(remision.getValorPeajes()) );
            
            tabla.addElement("carga").addText( (remision.getCarga() != null)? remision.getCarga() : "" );
            tabla.addElement("total").addText( (Util.customFormat(remision.getTotal()).equals(".00"))? "0.0" : Util.customFormat(remision.getTotal()) );
            
            tabla.addElement("ruta").addText( ( (remision.getOrigen()!=null)? remision.getOrigen() : "" ) +"-->"+ ((remision.getDestino()!=null)? remision.getDestino() : "") );
            
            tabla.addElement("firma").addText( (remision.getConductor()!=null)? remision.getConductor() : "" );
            tabla.addElement("telefono").addText( (remision.getNit()!=null)? remision.getNit() : "" );
            
            tabla.addElement("leyenda").addText( (remision.getTextoOc()!=null)? remision.getTextoOc() : "" );
            
            tabla.addElement("preparado").addText( (remision.getUsuario()!=null)? remision.getUsuario() : "" );
            
            tabla.addElement("copia").addText(copia);
        }catch(Exception ex){
            //System.out.println(" error en DocumentoUnicoTransportePDF, agregarElementos -->"+ex.getMessage());
        }
        
    }
    
    
    public void generarPDF() throws Exception {
        
        org.apache.fop.configuration.Configuration.put( "baseDir", "file:///" + xslt.getParent() );
        
        //Crear Documento
        //Element raiz = this.HojaControlReporte(info);
        Document documento = DocumentHelper.createDocument( raiz );
        ProformaXMLReader proformaXMLReader = new ProformaXMLReader( documento );
        
        //Establecer el formato de salida a PDF
        Driver driver = new Driver();
        
        //Setup logger
        Logger logger = new ConsoleLogger( ConsoleLogger.LEVEL_INFO );
        driver.setLogger( logger );
        
        driver.setRenderer( driver.RENDER_PDF );
        
        //Cofigurar la salida
        FileOutputStream salida = new FileOutputStream( pdf );
        
        try {
            
            driver.setOutputStream( salida );
            
            //Configurar el XSLT
            TransformerFactory factory = TransformerFactory.newInstance();
            Transformer transformer = factory.newTransformer( new StreamSource( xslt ) );
            
            //Configurar la entrada del XSLT para la transformación
            InputSource inputSource = new InputSource();
            inputSource.setEncoding( "windows-1252" );
            SAXSource source = new SAXSource( proformaXMLReader, inputSource );
            SAXResult res = new SAXResult( driver.getContentHandler() );
            
            //Iniciar la transformación XSLT y el procesamiento FOP
            transformer.transform( source, res );
            
            //Obtenemos el número de páginas
            String pageCount = Integer.toString(driver.getResults().getPageCount());
            
            //Inicializamos todo para la segunda transformación
            driver = new Driver();
            
            //Setup logger
            Logger logger1 = new ConsoleLogger( ConsoleLogger.LEVEL_INFO );
            driver.setLogger( logger1 );
            MessageHandler.setScreenLogger( logger1 );
            
            driver.setRenderer( driver.RENDER_PDF );
            salida = new FileOutputStream( pdf );
            driver.setOutputStream( salida );
            factory = TransformerFactory.newInstance();
            transformer = factory.newTransformer(new StreamSource( xslt ) );
            inputSource = new InputSource();
            inputSource.setEncoding( "windows-1252" );
            res = new SAXResult( driver.getContentHandler() );
            
            //Enviamos el parámetro
            transformer.setParameter( "page-count", pageCount );
            
            //... y realizamos de nuevo la transformación
            transformer.transform( source, res );
            
        } finally {
            
            salida.close();
            
        }
    }
    
    public static void main(String[]eakiuawbe) throws Exception{
        
        File pdf = new File("C:/dut.pdf");
        File xslt = new File("C:/documentoUnicoTransporte.xsl");
        
        DocumentoUnicoTransportePDF dut = new DocumentoUnicoTransportePDF();
        
        dut.crearCabecera();
        
        
        dut.generarPDF();
    }
    
    public void imprimirRemisiones(Vector v){
        
        try{
            if( v != null && v.size()>0){
                this.crearCabecera();
                for( int i=0; i<v.size(); i++){
                    Remision rem = (Remision) v.get(i);
                    //System.out.println(" to Print... DocumentoUnicoTransportePDF --> "+rem.getRemesa());
                    
                    
                    
                    this.agregarElementos(rem, "CONDUCTOR");
                    this.agregarElementos(rem, "CONDUCTOR COPIA 1");
                    this.agregarElementos(rem, "CONDUCTOR COPIA 2");
                    this.agregarElementos(rem, "FINV");
                                        
                }
                this.generarPDF();
                //System.out.println(" Printed!!!... DocumentoUnicoTransportePDF --> "+v.size()+" remisiones impresas");
            }
        }catch(Exception e){
            //System.out.println(" error en DocumentoUnicoTransportePDF -->"+e.getMessage());
        }
        
    }
    
}


