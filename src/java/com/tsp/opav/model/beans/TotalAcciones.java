/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.opav.model.beans;




/**
 *
 * @author Alvaro
 */
public class TotalAcciones {

    private String id_solicitud;
    private int    parcial;
    private double material;
    private double mano_obra;
    private double otros;
    private double administracion;
    private double imprevisto;
    private double utilidad;
    private double costo_contratista;
    private double iva_contratista;
    private double bonificacion;
    private double iva_bonificacion;
    private double comision_opav;
    private double comision_fintra;
    private double comision_interventoria;
    private double comision_provintegral;
    private double comision_eca;
    private double iva_comision_opav;
    private double iva_comision_fintra;
    private double iva_comision_interventoria;
    private double iva_comision_provintegral;
    private double iva_comision_eca;
    private double valor_a_financiar;
    private double subtotal_iva;
    private double valor_a_financiar_sin_iva;




    /** Creates a new instance of AccionPorSolicitud */
    public TotalAcciones() {
    }


    /** Extrae un registro de la BD correspondientes a las acciones de una solicitudes que no esta facturada */
    public static TotalAcciones load(java.sql.ResultSet rs)throws java.sql.SQLException{

        TotalAcciones totalAcciones = new TotalAcciones();

        totalAcciones.setId_solicitud( rs.getString("id_solicitud") );
        totalAcciones.setParcial( rs.getInt("parcial") );
        totalAcciones.setMaterial( rs.getDouble("material") );
        totalAcciones.setMano_obra( rs.getDouble("mano_obra") );
        totalAcciones.setOtros( rs.getDouble("otros") );
        totalAcciones.setAdministracion( rs.getDouble("administracion") );
        totalAcciones.setImprevisto( rs.getDouble("imprevisto") );
        totalAcciones.setUtilidad( rs.getDouble("utilidad") );
        totalAcciones.setCosto_contratista( rs.getDouble("costo_contratista") );
        totalAcciones.setIva_contratista( rs.getDouble("iva_contratista") );
        totalAcciones.setBonificacion( rs.getDouble("bonificacion") );
        totalAcciones.setIva_bonificacion( rs.getDouble("iva_bonificacion") );
        totalAcciones.setComision_opav( rs.getDouble("comision_opav") );
        totalAcciones.setComision_fintra( rs.getDouble("comision_fintra") );
        totalAcciones.setComision_interventoria( rs.getDouble("comision_interventoria") );
        totalAcciones.setComision_provintegral( rs.getDouble("comision_provintegral") );
        totalAcciones.setComision_eca( rs.getDouble("comision_eca") );
        totalAcciones.setIva_comision_opav( rs.getDouble("iva_comision_opav") );
        totalAcciones.setIva_comision_fintra( rs.getDouble("iva_comision_fintra") );
        totalAcciones.setIva_comision_interventoria( rs.getDouble("iva_comision_interventoria") );
        totalAcciones.setIva_comision_provintegral( rs.getDouble("iva_comision_provintegral") );
        totalAcciones.setIva_comision_eca( rs.getDouble("iva_comision_eca") );

        double valor =  totalAcciones.getCosto_contratista() +
                        // totalAcciones.getBonificacion() + //20100207
                        totalAcciones.getComision_eca() +
                        totalAcciones.getComision_fintra() +
                        totalAcciones.getComision_interventoria() +
                        totalAcciones.getComision_opav() +
                        totalAcciones.getComision_provintegral() +
                        totalAcciones.getIva_bonificacion() + //20100207 sr
                        totalAcciones.getIva_comision_eca() +
                        totalAcciones.getIva_comision_fintra() +
                        totalAcciones.getIva_comision_interventoria() +
                        totalAcciones.getIva_comision_opav() +
                        totalAcciones.getIva_contratista() +
                        totalAcciones.getIva_comision_provintegral() ;

        totalAcciones.setValor_a_financiar(valor);

        valor =  totalAcciones.getIva_bonificacion() + //20100207 sr
                 totalAcciones.getIva_comision_eca() +
                 totalAcciones.getIva_comision_opav() +
                 totalAcciones.getIva_comision_fintra() +
                 totalAcciones.getIva_comision_interventoria() +
                 totalAcciones.getIva_contratista() +
                 totalAcciones.getIva_comision_provintegral();

        totalAcciones.setSubtotal_iva(valor);

        valor =  totalAcciones.getCosto_contratista() +
                 // totalAcciones.getBonificacion() + //20100207
                 totalAcciones.getComision_eca() +
                 totalAcciones.getComision_fintra() +
                 totalAcciones.getComision_interventoria() +
                 totalAcciones.getComision_opav() +
                 totalAcciones.getComision_provintegral()  ;

        totalAcciones.setValor_a_financiar_sin_iva(valor);

        return totalAcciones;

    }



    /**
     * @return the id_solicitud
     */
    public String getId_solicitud() {
        return id_solicitud;
    }

    /**
     * @param id_solicitud the id_solicitud to set
     */
    public void setId_solicitud(String id_solicitud) {
        this.id_solicitud = id_solicitud;
    }



    /**
     * @return the material
     */
    public double getMaterial() {
        return material;
    }

    /**
     * @param material the material to set
     */
    public void setMaterial(double material) {
        this.material = material;
    }

    /**
     * @return the mano_obra
     */
    public double getMano_obra() {
        return mano_obra;
    }

    /**
     * @param mano_obra the mano_obra to set
     */
    public void setMano_obra(double mano_obra) {
        this.mano_obra = mano_obra;
    }

    /**
     * @return the otros
     */
    public double getOtros() {
        return otros;
    }

    /**
     * @param otros the otros to set
     */
    public void setOtros(double otros) {
        this.otros = otros;
    }

    /**
     * @return the administracion
     */
    public double getAdministracion() {
        return administracion;
    }

    /**
     * @param administracion the administracion to set
     */
    public void setAdministracion(double administracion) {
        this.administracion = administracion;
    }

    /**
     * @return the imprevisto
     */
    public double getImprevisto() {
        return imprevisto;
    }

    /**
     * @param imprevisto the imprevisto to set
     */
    public void setImprevisto(double imprevisto) {
        this.imprevisto = imprevisto;
    }

    /**
     * @return the utilidad
     */
    public double getUtilidad() {
        return utilidad;
    }

    /**
     * @param utilidad the utilidad to set
     */
    public void setUtilidad(double utilidad) {
        this.utilidad = utilidad;
    }

    /**
     * @return the costo_contratista
     */
    public double getCosto_contratista() {
        return costo_contratista;
    }

    /**
     * @param costo_contratista the costo_contratista to set
     */
    public void setCosto_contratista(double costo_contratista) {
        this.costo_contratista = costo_contratista;
    }

    /**
     * @return the iva_contratista
     */
    public double getIva_contratista() {
        return iva_contratista;
    }

    /**
     * @param iva_contratista the iva_contratista to set
     */
    public void setIva_contratista(double iva_contratista) {
        this.iva_contratista = iva_contratista;
    }

    /**
     * @return the bonificacion
     */
    public double getBonificacion() {
        return bonificacion;
    }

    /**
     * @param bonificacion the bonificacion to set
     */
    public void setBonificacion(double bonificacion) {
        this.bonificacion = bonificacion;
    }

    /**
     * @return the iva_bonificacion
     */
    public double getIva_bonificacion() {
        return iva_bonificacion;
    }

    /**
     * @param iva_bonificacion the iva_bonificacion to set
     */
    public void setIva_bonificacion(double iva_bonificacion) {
        this.iva_bonificacion = iva_bonificacion;
    }

    /**
     * @return the comision_opav
     */
    public double getComision_opav() {
        return comision_opav;
    }

    /**
     * @param comision_opav the comision_opav to set
     */
    public void setComision_opav(double comision_opav) {
        this.comision_opav = comision_opav;
    }

    /**
     * @return the comision_fintra
     */
    public double getComision_fintra() {
        return comision_fintra;
    }

    /**
     * @param comision_fintra the comision_fintra to set
     */
    public void setComision_fintra(double comision_fintra) {
        this.comision_fintra = comision_fintra;
    }

    /**
     * @return the comision_interventoria
     */
    public double getComision_interventoria() {
        return comision_interventoria;
    }

    /**
     * @param comision_interventoria the comision_interventoria to set
     */
    public void setComision_interventoria(double comision_interventoria) {
        this.comision_interventoria = comision_interventoria;
    }

    /**
     * @return the comision_provintegral
     */
    public double getComision_provintegral() {
        return comision_provintegral;
    }

    /**
     * @param comision_provintegral the comision_provintegral to set
     */
    public void setComision_provintegral(double comision_provintegral) {
        this.comision_provintegral = comision_provintegral;
    }

    /**
     * @return the comision_eca
     */
    public double getComision_eca() {
        return comision_eca;
    }

    /**
     * @param comision_eca the comision_eca to set
     */
    public void setComision_eca(double comision_eca) {
        this.comision_eca = comision_eca;
    }

    /**
     * @return the iva_comision_opav
     */
    public double getIva_comision_opav() {
        return iva_comision_opav;
    }

    /**
     * @param iva_comision_opav the iva_comision_opav to set
     */
    public void setIva_comision_opav(double iva_comision_opav) {
        this.iva_comision_opav = iva_comision_opav;
    }

    /**
     * @return the iva_comision_fintra
     */
    public double getIva_comision_fintra() {
        return iva_comision_fintra;
    }

    /**
     * @param iva_comision_fintra the iva_comision_fintra to set
     */
    public void setIva_comision_fintra(double iva_comision_fintra) {
        this.iva_comision_fintra = iva_comision_fintra;
    }

    /**
     * @return the iva_comision_interventoria
     */
    public double getIva_comision_interventoria() {
        return iva_comision_interventoria;
    }

    /**
     * @param iva_comision_interventoria the iva_comision_interventoria to set
     */
    public void setIva_comision_interventoria(double iva_comision_interventoria) {
        this.iva_comision_interventoria = iva_comision_interventoria;
    }

    /**
     * @return the iva_comision_proventegral
     */
    public double getIva_comision_provintegral() {
        return iva_comision_provintegral;
    }

    /**
     * @param iva_comision_proventegral the iva_comision_proventegral to set
     */
    public void setIva_comision_provintegral(double iva_comision_proventegral) {
        this.iva_comision_provintegral = iva_comision_proventegral;
    }

    /**
     * @return the iva_comision_eca
     */
    public double getIva_comision_eca() {
        return iva_comision_eca;
    }

    /**
     * @param iva_comision_eca the iva_comision_eca to set
     */
    public void setIva_comision_eca(double iva_comision_eca) {
        this.iva_comision_eca = iva_comision_eca;
    }


    /**
     * @return the valor_a_financiar
     */
    public double getValor_a_financiar() {
        return valor_a_financiar;
    }

    /**
     * @param valor_a_financiar the valor_a_financiar to set
     */
    public void setValor_a_financiar(double valor_a_financiar) {
        this.valor_a_financiar = valor_a_financiar;
    }

    /**
     * @return the parcial
     */
    public int getParcial() {
        return parcial;
    }

    /**
     * @param parcial the parcial to set
     */
    public void setParcial(int parcial) {
        this.parcial = parcial;
    }

    /**
     * @return the subtotal_iva
     */
    public double getSubtotal_iva() {
        return subtotal_iva;
    }

    /**
     * @param subtotal_iva the subtotal_iva to set
     */
    public void setSubtotal_iva(double subtotal_iva) {
        this.subtotal_iva = subtotal_iva;
    }

    /**
     * @return the valor_a_financiar_sin_iva
     */
    public double getValor_a_financiar_sin_iva() {
        return valor_a_financiar_sin_iva;
    }

    /**
     * @param valor_a_financiar_sin_iva the valor_a_financiar_sin_iva to set
     */
    public void setValor_a_financiar_sin_iva(double valor_a_financiar_sin_iva) {
        this.valor_a_financiar_sin_iva = valor_a_financiar_sin_iva;
    }




}