/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.opav.model.beans;


import java.sql.*;



/**
 *
 * @author Alvaro
 */
public class FacturaDetalleCxP {


        private String reg_status;
        private String dstrct;
	private String proveedor;
	private String tipo_documento;
	private String documento;
        private String item;
	private String descripcion;
        private double vlr;
        private double vlr_me;
        private String codigo_cuenta;
        private String codigo_abc;
        private String planilla;
	private String last_update;
	private String user_update;
	private String creation_date;
	private String creation_user;
	private String base;
        private String codcliarea;
        private String tipcliarea;
        private String concepto;
        private String auxiliar;
        private String tipo_referencia_1;
        private String referencia_1;
        private String tipo_referencia_2;
        private String referencia_2;
        private String tipo_referencia_3;
        private String referencia_3;




        public static FacturaDetalleCxP load(ResultSet rs)throws SQLException {
            FacturaDetalleCxP facturaDetalleCxP = new FacturaDetalleCxP();

            facturaDetalleCxP.setReg_status( rs.getString("reg_status") );
            facturaDetalleCxP.setDstrct( rs.getString("dstrct") );
            facturaDetalleCxP.setProveedor(rs.getString("proveedor") );
            facturaDetalleCxP.setTipo_documento(rs.getString("tipo_documento") );
            facturaDetalleCxP.setDocumento(rs.getString("documento") );
            facturaDetalleCxP.setItem(rs.getString("item") );
            facturaDetalleCxP.setDescripcion(rs.getString("descripcion") );
            facturaDetalleCxP.setVlr(rs.getDouble("vlr"));
            facturaDetalleCxP.setVlr_me(rs.getDouble("vlr_me"));
            facturaDetalleCxP.setCodigo_cuenta(rs.getString("codigo_cuenta"));
            facturaDetalleCxP.setCodigo_abc(rs.getString("codigo_abc"));
            facturaDetalleCxP.setPlanilla(rs.getString("planilla"));
            facturaDetalleCxP.setLast_update( rs.getString("last_update") );
            facturaDetalleCxP.setUser_update( rs.getString("user_update") );
            facturaDetalleCxP.setCreation_date(rs.getString("creation_date") );
            facturaDetalleCxP.setCreation_user(rs.getString("creation_user") );
            facturaDetalleCxP.setBase( rs.getString("base") );
            facturaDetalleCxP.setCodcliarea( rs.getString("codcliarea") );
            facturaDetalleCxP.setTipcliarea( rs.getString("tipcliarea") );
            facturaDetalleCxP.setConcepto( rs.getString("concepto") );
            facturaDetalleCxP.setAuxiliar( rs.getString("auxiliar") );
            facturaDetalleCxP.setTipo_referencia_1( rs.getString("tipo_referencia_1") );
            facturaDetalleCxP.setReferencia_1( rs.getString("referencia_1") );
            facturaDetalleCxP.setTipo_referencia_2( rs.getString("tipo_referencia_2") );
            facturaDetalleCxP.setReferencia_2( rs.getString("referencia_2") );
            facturaDetalleCxP.setTipo_referencia_3( rs.getString("tipo_referencia_3") );
            facturaDetalleCxP.setReferencia_3( rs.getString("referencia_3") );

            return facturaDetalleCxP;
        }



    /**
     * @return the reg_status
     */
    public String getReg_status() {
        return reg_status;
    }

    /**
     * @param reg_status the reg_status to set
     */
    public void setReg_status(String reg_status) {
        this.reg_status = reg_status;
    }

    /**
     * @return the dstrct
     */
    public String getDstrct() {
        return dstrct;
    }

    /**
     * @param dstrct the dstrct to set
     */
    public void setDstrct(String dstrct) {
        this.dstrct = dstrct;
    }

    /**
     * @return the proveedor
     */
    public String getProveedor() {
        return proveedor;
    }

    /**
     * @param proveedor the proveedor to set
     */
    public void setProveedor(String proveedor) {
        this.proveedor = proveedor;
    }

    /**
     * @return the tipo_documento
     */
    public String getTipo_documento() {
        return tipo_documento;
    }

    /**
     * @param tipo_documento the tipo_documento to set
     */
    public void setTipo_documento(String tipo_documento) {
        this.tipo_documento = tipo_documento;
    }

    /**
     * @return the documento
     */
    public String getDocumento() {
        return documento;
    }

    /**
     * @param documento the documento to set
     */
    public void setDocumento(String documento) {
        this.documento = documento;
    }

    /**
     * @return the item
     */
    public String getItem() {
        return item;
    }

    /**
     * @param item the item to set
     */
    public void setItem(String item) {
        this.item = item;
    }

    /**
     * @return the descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * @param descripcion the descripcion to set
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * @return the vlr
     */
    public double getVlr() {
        return vlr;
    }

    /**
     * @param vlr the vlr to set
     */
    public void setVlr(double vlr) {
        this.vlr = vlr;
    }

    /**
     * @return the vlr_me
     */
    public double getVlr_me() {
        return vlr_me;
    }

    /**
     * @param vlr_me the vlr_me to set
     */
    public void setVlr_me(double vlr_me) {
        this.vlr_me = vlr_me;
    }

    /**
     * @return the codigo_cuenta
     */
    public String getCodigo_cuenta() {
        return codigo_cuenta;
    }

    /**
     * @param codigo_cuenta the codigo_cuenta to set
     */
    public void setCodigo_cuenta(String codigo_cuenta) {
        this.codigo_cuenta = codigo_cuenta;
    }

    /**
     * @return the codigo_abc
     */
    public String getCodigo_abc() {
        return codigo_abc;
    }

    /**
     * @param codigo_abc the codigo_abc to set
     */
    public void setCodigo_abc(String codigo_abc) {
        this.codigo_abc = codigo_abc;
    }

    /**
     * @return the planilla
     */
    public String getPlanilla() {
        return planilla;
    }

    /**
     * @param planilla the planilla to set
     */
    public void setPlanilla(String planilla) {
        this.planilla = planilla;
    }

    /**
     * @return the last_update
     */
    public String getLast_update() {
        return last_update;
    }

    /**
     * @param last_update the last_update to set
     */
    public void setLast_update(String last_update) {
        this.last_update = last_update;
    }

    /**
     * @return the user_update
     */
    public String getUser_update() {
        return user_update;
    }

    /**
     * @param user_update the user_update to set
     */
    public void setUser_update(String user_update) {
        this.user_update = user_update;
    }

    /**
     * @return the creation_date
     */
    public String getCreation_date() {
        return creation_date;
    }

    /**
     * @param creation_date the creation_date to set
     */
    public void setCreation_date(String creation_date) {
        this.creation_date = creation_date;
    }

    /**
     * @return the creation_user
     */
    public String getCreation_user() {
        return creation_user;
    }

    /**
     * @param creation_user the creation_user to set
     */
    public void setCreation_user(String creation_user) {
        this.creation_user = creation_user;
    }

    /**
     * @return the base
     */
    public String getBase() {
        return base;
    }

    /**
     * @param base the base to set
     */
    public void setBase(String base) {
        this.base = base;
    }

    /**
     * @return the codcliarea
     */
    public String getCodcliarea() {
        return codcliarea;
    }

    /**
     * @param codcliarea the codcliarea to set
     */
    public void setCodcliarea(String codcliarea) {
        this.codcliarea = codcliarea;
    }

    /**
     * @return the tipcliarea
     */
    public String getTipcliarea() {
        return tipcliarea;
    }

    /**
     * @param tipcliarea the tipcliarea to set
     */
    public void setTipcliarea(String tipcliarea) {
        this.tipcliarea = tipcliarea;
    }

    /**
     * @return the concepto
     */
    public String getConcepto() {
        return concepto;
    }

    /**
     * @param concepto the concepto to set
     */
    public void setConcepto(String concepto) {
        this.concepto = concepto;
    }

    /**
     * @return the auxiliar
     */
    public String getAuxiliar() {
        return auxiliar;
    }

    /**
     * @param auxiliar the auxiliar to set
     */
    public void setAuxiliar(String auxiliar) {
        this.auxiliar = auxiliar;
    }

    /**
     * @return the tipo_referencia_1
     */
    public String getTipo_referencia_1() {
        return tipo_referencia_1;
    }

    /**
     * @param tipo_referencia_1 the tipo_referencia_1 to set
     */
    public void setTipo_referencia_1(String tipo_referencia_1) {
        this.tipo_referencia_1 = tipo_referencia_1;
    }

    /**
     * @return the referencia_1
     */
    public String getReferencia_1() {
        return referencia_1;
    }

    /**
     * @param referencia_1 the referencia_1 to set
     */
    public void setReferencia_1(String referencia_1) {
        this.referencia_1 = referencia_1;
    }

    /**
     * @return the tipo_referencia_2
     */
    public String getTipo_referencia_2() {
        return tipo_referencia_2;
    }

    /**
     * @param tipo_referencia_2 the tipo_referencia_2 to set
     */
    public void setTipo_referencia_2(String tipo_referencia_2) {
        this.tipo_referencia_2 = tipo_referencia_2;
    }

    /**
     * @return the referencia_2
     */
    public String getReferencia_2() {
        return referencia_2;
    }

    /**
     * @param referencia_2 the referencia_2 to set
     */
    public void setReferencia_2(String referencia_2) {
        this.referencia_2 = referencia_2;
    }

    /**
     * @return the tipo_referencia_3
     */
    public String getTipo_referencia_3() {
        return tipo_referencia_3;
    }

    /**
     * @param tipo_referencia_3 the tipo_referencia_3 to set
     */
    public void setTipo_referencia_3(String tipo_referencia_3) {
        this.tipo_referencia_3 = tipo_referencia_3;
    }

    /**
     * @return the referencia_3
     */
    public String getReferencia_3() {
        return referencia_3;
    }

    /**
     * @param referencia_3 the referencia_3 to set
     */
    public void setReferencia_3(String referencia_3) {
        this.referencia_3 = referencia_3;
    }


}