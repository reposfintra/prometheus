/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.opav.model.beans;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.io.Serializable;

/**
 *
 * @author Alvaro
 */
public class FacturaCabeceraCxP {

        private String reg_status;
        private String dstrct;
	private String proveedor;
	private String tipo_documento;
	private String documento;
	private String descripcion;
        private String agencia;
	private String handle_code;
	private String id_mims;
	private String tipo_documento_rel;
	private String documento_relacionado;
	private String fecha_aprobacion;
	private String aprobador;
	private String usuario_aprobacion;
	private String banco;
	private String sucursal;
	private String moneda;
	private double vlr_neto;
	private double vlr_total_abonos;
	private double vlr_saldo;
	private double vlr_neto_me;
	private double vlr_total_abonos_me;
	private double vlr_saldo_me;
	private double tasa;
	private String usuario_contabilizo;
	private String fecha_contabilizacion;
	private String usuario_anulo;
	private String fecha_anulacion;
	private String fecha_contabilizacion_anulacion;
	private String observacion;
        private double num_obs_autorizador;
        private double num_obs_pagador ;
        private double num_obs_registra ;
	private String last_update;
	private String user_update;
	private String creation_date;
	private String creation_user;
	private String base;
        private String corrida ;
        private String cheque ;
        private String periodo;
        private String fecha_procesado;
        private String fecha_contabilizacion_ajc;
        private String fecha_contabilizacion_ajv;
        private String periodo_ajc;
        private String periodo_ajv;
        private String usuario_contabilizo_ajc;
        private String usuario_contabilizo_ajv;
        private int    transaccion_ajc;
        private int    transaccion_ajv;
        private String clase_documento;
        private int    transaccion;
        private String moneda_banco;
	private String fecha_documento;
        private String fecha_vencimiento;
        private String ultima_fecha_pago;
        private String flujo;
        private int    transaccion_anulacion;
        private String ret_pago;
        private String clase_documento_rel;
        private String tipo_referencia_1;
        private String referencia_1;
        private String tipo_referencia_2;
        private String referencia_2;
        private String tipo_referencia_3;
        private String referencia_3;
        private String indicador_traslado_fintra;
        private String factoring_formula_aplicada;



        public void clear(){

            reg_status = "";
            dstrct = "";
            proveedor = "" ;
            tipo_documento = "" ;
            documento = "" ;
            descripcion = "" ;
            agencia = "" ;
            handle_code = "" ;
            id_mims = "" ;
            tipo_documento_rel = "" ;
            documento_relacionado = "" ;
            fecha_aprobacion = "0099-01-01 00:00:00" ;
            aprobador = "" ;
            usuario_aprobacion = "" ;
            banco = "" ;
            sucursal = "" ;
            moneda = "" ;
            vlr_neto = 0.00 ;
            vlr_total_abonos = 0.00 ;
            vlr_saldo = 0.00 ;
            vlr_neto_me = 0.00 ;
            vlr_total_abonos_me = 0.00 ;
            vlr_saldo_me = 0.00 ;
            tasa = 0.00 ;
            usuario_contabilizo = "" ;
            fecha_contabilizacion = "0099-01-01 00:00:00" ;
            usuario_anulo = "" ;
            fecha_anulacion = "0099-01-01 00:00:00" ;
            fecha_contabilizacion_anulacion = "0099-01-01 00:00:00" ;
            observacion = "" ;
            num_obs_autorizador = 0 ;
            num_obs_pagador  = 0 ;
            num_obs_registra  = 0 ;
            last_update = "" ;
            user_update = "" ;
            creation_date = "" ;
            creation_user = "" ;
            base = "" ;
            corrida  = "" ;
            cheque  = "" ;
            periodo = "" ;
            fecha_procesado = "0099-01-01 00:00:00" ;
            fecha_contabilizacion_ajc = "0099-01-01 00:00:00" ;
            fecha_contabilizacion_ajv = "0099-01-01 00:00:00" ;
            periodo_ajc = "" ;
            periodo_ajv = "" ;
            usuario_contabilizo_ajc = "" ;
            usuario_contabilizo_ajv = "" ;
            transaccion_ajc = 0 ;
            transaccion_ajv = 0 ;
            clase_documento = "" ;
            transaccion = 0 ;
            moneda_banco = "" ;
            fecha_documento = "0099-01-01" ;
            fecha_vencimiento = "0099-01-01" ;
            ultima_fecha_pago = "0099-01-01" ;
            flujo = "S" ;
            transaccion_anulacion = 0 ;
            ret_pago = "N" ;
            clase_documento_rel = "" ;
            tipo_referencia_1 = "" ;
            referencia_1 = "" ;
            tipo_referencia_2 = "" ;
            referencia_2 = "" ;
            tipo_referencia_3 = "" ;
            referencia_3 = "" ;
            indicador_traslado_fintra = "N" ;
            factoring_formula_aplicada = "N" ;
        }


        public static FacturaCabeceraCxP load(ResultSet rs)throws SQLException {
            FacturaCabeceraCxP facturaCabeceraCxP = new FacturaCabeceraCxP();


            facturaCabeceraCxP.setReg_status( rs.getString("reg_status") );
            facturaCabeceraCxP.setDstrct( rs.getString("dstrct") );
            facturaCabeceraCxP.setProveedor(rs.getString("proveedor") );
            facturaCabeceraCxP.setTipo_documento(rs.getString("tipo_documento") );
            facturaCabeceraCxP.setDocumento(rs.getString("documento") );
            facturaCabeceraCxP.setDescripcion(rs.getString("descripcion") );
            facturaCabeceraCxP.setAgencia(rs.getString("agencia"));
            facturaCabeceraCxP.setHandle_code(rs.getString("handle_code"));
            facturaCabeceraCxP.setId_mims(rs.getString("id_mims"));
            facturaCabeceraCxP.setTipo_documento_rel(rs.getString("tipo_documento_rel"));
            facturaCabeceraCxP.setDocumento_relacionado(rs.getString("documento_relacionado"));
            facturaCabeceraCxP.setFecha_aprobacion( ( rs.getString("fecha_aprobacion") != null )?rs.getString("fecha_aprobacion"):"" );
            facturaCabeceraCxP.setAprobador(rs.getString("aprobador"));
            facturaCabeceraCxP.setUsuario_aprobacion(rs.getString("usuario_aprobacion"));
            facturaCabeceraCxP.setBanco(rs.getString("banco"));
            facturaCabeceraCxP.setSucursal( rs.getString("sucursal") );
            facturaCabeceraCxP.setMoneda( rs.getString("moneda") );
            facturaCabeceraCxP.setVlr_neto(rs.getDouble("vlr_neto"));
            facturaCabeceraCxP.setVlr_total_abonos( rs.getDouble("vlr_total_abonos") );
            facturaCabeceraCxP.setVlr_saldo( rs.getDouble("vlr_saldo") ) ;
            facturaCabeceraCxP.setVlr_neto_me( rs.getDouble("vlr_neto_me") );
            facturaCabeceraCxP.setVlr_total_abonos_me( rs.getDouble("vlr_total_abonos_me") );
            facturaCabeceraCxP.setVlr_saldo_me( rs.getDouble("vlr_saldo_me") );
            facturaCabeceraCxP.setTasa( rs.getDouble("tasa") );
            facturaCabeceraCxP.setUsuario_contabilizo( rs.getString("usuario_contabilizo") );
            facturaCabeceraCxP.setFecha_contabilizacion( ( rs.getString("fecha_contabilizacion") != null)?rs.getString("fecha_contabilizacion"):"");
            facturaCabeceraCxP.setUsuario_anulo( rs.getString("usuario_anulo") );
            facturaCabeceraCxP.setFecha_anulacion( rs.getString("fecha_anulacion") );
            facturaCabeceraCxP.setFecha_contabilizacion_anulacion( rs.getString("fecha_contabilizacion_anulacion") );
            facturaCabeceraCxP.setObservacion( rs.getString("observacion") );
            facturaCabeceraCxP.setNum_obs_autorizador( rs.getDouble("num_obs_autorizador") );
            facturaCabeceraCxP.setNum_obs_pagador( rs.getDouble("num_obs_pagador") );
            facturaCabeceraCxP.setNum_obs_registra( rs.getDouble("num_obs_registra") );
            facturaCabeceraCxP.setLast_update( rs.getString("last_update") );
            facturaCabeceraCxP.setUser_update( rs.getString("user_update") );
            facturaCabeceraCxP.setCreation_date(rs.getString("creation_date") );
            facturaCabeceraCxP.setCreation_user(rs.getString("creation_user") );
            facturaCabeceraCxP.setBase( rs.getString("base") );
            facturaCabeceraCxP.setCorrida( rs.getString("corrida") ) ;
            facturaCabeceraCxP.setCheque( rs.getString("cheque") );
            facturaCabeceraCxP.setPeriodo( rs.getString("periodo") );
            facturaCabeceraCxP.setFecha_procesado( rs.getString("fecha_procesado") );
            facturaCabeceraCxP.setFecha_contabilizacion_ajc( rs.getString("fecha_contabilizacion_ajc") );
            facturaCabeceraCxP.setFecha_contabilizacion_ajv( rs.getString("fecha_contabilizacion_ajv") );
            facturaCabeceraCxP.setPeriodo_ajc( rs.getString("periodo_ajc") );
            facturaCabeceraCxP.setPeriodo_ajv( rs.getString("periodo_ajv") );
            facturaCabeceraCxP.setUsuario_contabilizo_ajc( rs.getString("usuario_contabilizo_ajc") );
            facturaCabeceraCxP.setUsuario_contabilizo_ajv( rs.getString("usuario_contabilizo_ajv") );
            facturaCabeceraCxP.setTransaccion_ajv( rs.getInt("transaccion_ajc") );
            facturaCabeceraCxP.setTransaccion_ajv( rs.getInt("transaccion_ajv") );
            facturaCabeceraCxP.setClase_documento( rs.getString("clase_documento") );
            facturaCabeceraCxP.setTransaccion( rs.getInt("transaccion") );
            facturaCabeceraCxP.setMoneda_banco( rs.getString("moneda_banco") );
            facturaCabeceraCxP.setFecha_documento( ( rs.getString("fecha_documento") != null )?rs.getString("fecha_documento"):"" );
            facturaCabeceraCxP.setFecha_vencimiento( rs.getString("fecha_vencimiento") );
            facturaCabeceraCxP.setUltima_fecha_pago( rs.getString("ultima_fecha_pago") );
            facturaCabeceraCxP.setFlujo( rs.getString("flujo") );
            facturaCabeceraCxP.setTransaccion_anulacion( rs.getInt("transaccion_anulacion") );
            facturaCabeceraCxP.setRet_pago( rs.getString("ret_pago") );
            facturaCabeceraCxP.setClase_documento_rel( rs.getString("clase_documento_rel") );
            facturaCabeceraCxP.setTipo_referencia_1( rs.getString("tipo_referencia_1") );
            facturaCabeceraCxP.setReferencia_1( rs.getString("referencia_1") );
            facturaCabeceraCxP.setTipo_referencia_2( rs.getString("tipo_referencia_2") );
            facturaCabeceraCxP.setReferencia_2( rs.getString("referencia_2") );
            facturaCabeceraCxP.setTipo_referencia_3( rs.getString("tipo_referencia_3") );
            facturaCabeceraCxP.setReferencia_3( rs.getString("referencia_3") );
            facturaCabeceraCxP.setReferencia_3( rs.getString("indicador_traslado_fintra") );
            facturaCabeceraCxP.setFactoring_formula_aplicada( rs.getString("factoring_formula_aplicada") );

            return facturaCabeceraCxP;
        }



        public static FacturaCabeceraCxP loadCxPContratista(ResultSet rs)throws SQLException {
            FacturaCabeceraCxP facturaCabeceraCxP = new FacturaCabeceraCxP();

            facturaCabeceraCxP.setReg_status( rs.getString("reg_status") );
            facturaCabeceraCxP.setDstrct( rs.getString("dstrct") );
            facturaCabeceraCxP.setProveedor(rs.getString("proveedor") );
            facturaCabeceraCxP.setTipo_documento(rs.getString("tipo_documento") );
            facturaCabeceraCxP.setDocumento(rs.getString("documento") );
            facturaCabeceraCxP.setDescripcion(rs.getString("descripcion") );
            facturaCabeceraCxP.setAgencia(rs.getString("agencia"));
            facturaCabeceraCxP.setHandle_code(rs.getString("handle_code"));
            facturaCabeceraCxP.setId_mims(rs.getString("id_mims"));
            facturaCabeceraCxP.setTipo_documento_rel(rs.getString("tipo_documento_rel"));
            facturaCabeceraCxP.setDocumento_relacionado(rs.getString("documento_relacionado"));
            facturaCabeceraCxP.setFecha_aprobacion( ( rs.getString("fecha_aprobacion") != null )?rs.getString("fecha_aprobacion"):"" );
            facturaCabeceraCxP.setAprobador(rs.getString("aprobador"));
            facturaCabeceraCxP.setUsuario_aprobacion(rs.getString("usuario_aprobacion"));
            facturaCabeceraCxP.setBanco(rs.getString("banco"));
            facturaCabeceraCxP.setSucursal( rs.getString("sucursal") );
            facturaCabeceraCxP.setMoneda( rs.getString("moneda") );
            facturaCabeceraCxP.setVlr_neto(rs.getDouble("vlr_neto"));
            facturaCabeceraCxP.setVlr_total_abonos( rs.getDouble("vlr_total_abonos") );
            facturaCabeceraCxP.setVlr_saldo( rs.getDouble("vlr_saldo") ) ;
            facturaCabeceraCxP.setVlr_neto_me( rs.getDouble("vlr_neto_me") );
            facturaCabeceraCxP.setVlr_total_abonos_me( rs.getDouble("vlr_total_abonos_me") );
            facturaCabeceraCxP.setVlr_saldo_me( rs.getDouble("vlr_saldo_me") );
            facturaCabeceraCxP.setTasa( rs.getDouble("tasa") );
            facturaCabeceraCxP.setUsuario_contabilizo( rs.getString("usuario_contabilizo") );
            facturaCabeceraCxP.setFecha_contabilizacion( ( rs.getString("fecha_contabilizacion") != null)?rs.getString("fecha_contabilizacion"):"");
            facturaCabeceraCxP.setUsuario_anulo( rs.getString("usuario_anulo") );
            facturaCabeceraCxP.setFecha_anulacion( rs.getString("fecha_anulacion") );
            facturaCabeceraCxP.setFecha_contabilizacion_anulacion( rs.getString("fecha_contabilizacion_anulacion") );
            facturaCabeceraCxP.setObservacion( rs.getString("observacion") );
            facturaCabeceraCxP.setNum_obs_autorizador( rs.getDouble("num_obs_autorizador") );
            facturaCabeceraCxP.setNum_obs_pagador( rs.getDouble("num_obs_pagador") );
            facturaCabeceraCxP.setNum_obs_registra( rs.getDouble("num_obs_registra") );
            facturaCabeceraCxP.setLast_update( rs.getString("last_update") );
            facturaCabeceraCxP.setUser_update( rs.getString("user_update") );
            facturaCabeceraCxP.setCreation_date(rs.getString("creation_date") );
            facturaCabeceraCxP.setCreation_user(rs.getString("creation_user") );
            facturaCabeceraCxP.setBase( rs.getString("base") );
            facturaCabeceraCxP.setCorrida( rs.getString("corrida") ) ;
            facturaCabeceraCxP.setCheque( rs.getString("cheque") );
            facturaCabeceraCxP.setPeriodo( rs.getString("periodo") );
            facturaCabeceraCxP.setFecha_procesado( rs.getString("fecha_procesado") );
            facturaCabeceraCxP.setFecha_contabilizacion_ajc( rs.getString("fecha_contabilizacion_ajc") );
            facturaCabeceraCxP.setFecha_contabilizacion_ajv( rs.getString("fecha_contabilizacion_ajv") );
            facturaCabeceraCxP.setPeriodo_ajc( rs.getString("periodo_ajc") );
            facturaCabeceraCxP.setPeriodo_ajv( rs.getString("periodo_ajv") );
            facturaCabeceraCxP.setUsuario_contabilizo_ajc( rs.getString("usuario_contabilizo_ajc") );
            facturaCabeceraCxP.setUsuario_contabilizo_ajv( rs.getString("usuario_contabilizo_ajv") );
            facturaCabeceraCxP.setTransaccion_ajv( rs.getInt("transaccion_ajc") );
            facturaCabeceraCxP.setTransaccion_ajv( rs.getInt("transaccion_ajv") );
            facturaCabeceraCxP.setClase_documento( rs.getString("clase_documento") );
            facturaCabeceraCxP.setTransaccion( rs.getInt("transaccion") );
            facturaCabeceraCxP.setMoneda_banco( rs.getString("moneda_banco") );
            facturaCabeceraCxP.setFecha_documento( ( rs.getString("fecha_documento") != null )?rs.getString("fecha_documento"):"" );
            facturaCabeceraCxP.setFecha_vencimiento( rs.getString("fecha_vencimiento") );
            facturaCabeceraCxP.setUltima_fecha_pago( rs.getString("ultima_fecha_pago") );
            facturaCabeceraCxP.setFlujo( rs.getString("flujo") );
            facturaCabeceraCxP.setTransaccion_anulacion( rs.getInt("transaccion_anulacion") );
            facturaCabeceraCxP.setRet_pago( rs.getString("ret_pago") );
            facturaCabeceraCxP.setClase_documento_rel( rs.getString("clase_documento_rel") );
            facturaCabeceraCxP.setTipo_referencia_1( rs.getString("tipo_referencia_1") );
            facturaCabeceraCxP.setReferencia_1( rs.getString("referencia_1") );
            facturaCabeceraCxP.setTipo_referencia_2( rs.getString("tipo_referencia_2") );
            facturaCabeceraCxP.setReferencia_2( rs.getString("referencia_2") );
            facturaCabeceraCxP.setTipo_referencia_3( rs.getString("tipo_referencia_3") );
            facturaCabeceraCxP.setReferencia_3( rs.getString("referencia_3") );

            return facturaCabeceraCxP;
        }













    /**
     * @return the reg_status
     */
    public String getReg_status() {
        return reg_status;
    }

    /**
     * @param reg_status the reg_status to set
     */
    public void setReg_status(String reg_status) {
        this.reg_status = reg_status;
    }

    /**
     * @return the dstrct
     */
    public String getDstrct() {
        return dstrct;
    }

    /**
     * @param dstrct the dstrct to set
     */
    public void setDstrct(String dstrct) {
        this.dstrct = dstrct;
    }

    /**
     * @return the proveedor
     */
    public String getProveedor() {
        return proveedor;
    }

    /**
     * @param proveedor the proveedor to set
     */
    public void setProveedor(String proveedor) {
        this.proveedor = proveedor;
    }

    /**
     * @return the tipo_documento
     */
    public String getTipo_documento() {
        return tipo_documento;
    }

    /**
     * @param tipo_documento the tipo_documento to set
     */
    public void setTipo_documento(String tipo_documento) {
        this.tipo_documento = tipo_documento;
    }

    /**
     * @return the documento
     */
    public String getDocumento() {
        return documento;
    }

    /**
     * @param documento the documento to set
     */
    public void setDocumento(String documento) {
        this.documento = documento;
    }

    /**
     * @return the descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * @param descripcion the descripcion to set
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * @return the agencia
     */
    public String getAgencia() {
        return agencia;
    }

    /**
     * @param agencia the agencia to set
     */
    public void setAgencia(String agencia) {
        this.agencia = agencia;
    }

    /**
     * @return the handle_code
     */
    public String getHandle_code() {
        return handle_code;
    }

    /**
     * @param handle_code the handle_code to set
     */
    public void setHandle_code(String handle_code) {
        this.handle_code = handle_code;
    }

    /**
     * @return the id_mims
     */
    public String getId_mims() {
        return id_mims;
    }

    /**
     * @param id_mims the id_mims to set
     */
    public void setId_mims(String id_mims) {
        this.id_mims = id_mims;
    }

    /**
     * @return the tipo_documento_rel
     */
    public String getTipo_documento_rel() {
        return tipo_documento_rel;
    }

    /**
     * @param tipo_documento_rel the tipo_documento_rel to set
     */
    public void setTipo_documento_rel(String tipo_documento_rel) {
        this.tipo_documento_rel = tipo_documento_rel;
    }

    /**
     * @return the documento_relacionado
     */
    public String getDocumento_relacionado() {
        return documento_relacionado;
    }

    /**
     * @param documento_relacionado the documento_relacionado to set
     */
    public void setDocumento_relacionado(String documento_relacionado) {
        this.documento_relacionado = documento_relacionado;
    }

    /**
     * @return the fecha_aprobacion
     */
    public String getFecha_aprobacion() {
        return fecha_aprobacion;
    }

    /**
     * @param fecha_aprobacion the fecha_aprobacion to set
     */
    public void setFecha_aprobacion(String fecha_aprobacion) {
        this.fecha_aprobacion = fecha_aprobacion;
    }

    /**
     * @return the aprobador
     */
    public String getAprobador() {
        return aprobador;
    }

    /**
     * @param aprobador the aprobador to set
     */
    public void setAprobador(String aprobador) {
        this.aprobador = aprobador;
    }

    /**
     * @return the usuario_aprobacion
     */
    public String getUsuario_aprobacion() {
        return usuario_aprobacion;
    }

    /**
     * @param usuario_aprobacion the usuario_aprobacion to set
     */
    public void setUsuario_aprobacion(String usuario_aprobacion) {
        this.usuario_aprobacion = usuario_aprobacion;
    }

    /**
     * @return the banco
     */
    public String getBanco() {
        return banco;
    }

    /**
     * @param banco the banco to set
     */
    public void setBanco(String banco) {
        this.banco = banco;
    }

    /**
     * @return the sucursal
     */
    public String getSucursal() {
        return sucursal;
    }

    /**
     * @param sucursal the sucursal to set
     */
    public void setSucursal(String sucursal) {
        this.sucursal = sucursal;
    }

    /**
     * @return the moneda
     */
    public String getMoneda() {
        return moneda;
    }

    /**
     * @param moneda the moneda to set
     */
    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    /**
     * @return the vlr_neto
     */
    public double getVlr_neto() {
        return vlr_neto;
    }

    /**
     * @param vlr_neto the vlr_neto to set
     */
    public void setVlr_neto(double vlr_neto) {
        this.vlr_neto = vlr_neto;
    }

    /**
     * @return the vlr_total_abonos
     */
    public double getVlr_total_abonos() {
        return vlr_total_abonos;
    }

    /**
     * @param vlr_total_abonos the vlr_total_abonos to set
     */
    public void setVlr_total_abonos(double vlr_total_abonos) {
        this.vlr_total_abonos = vlr_total_abonos;
    }

    /**
     * @return the vlr_saldo
     */
    public double getVlr_saldo() {
        return vlr_saldo;
    }

    /**
     * @param vlr_saldo the vlr_saldo to set
     */
    public void setVlr_saldo(double vlr_saldo) {
        this.vlr_saldo = vlr_saldo;
    }

    /**
     * @return the vlr_neto_me
     */
    public double getVlr_neto_me() {
        return vlr_neto_me;
    }

    /**
     * @param vlr_neto_me the vlr_neto_me to set
     */
    public void setVlr_neto_me(double vlr_neto_me) {
        this.vlr_neto_me = vlr_neto_me;
    }

    /**
     * @return the vlr_total_abonos_me
     */
    public double getVlr_total_abonos_me() {
        return vlr_total_abonos_me;
    }

    /**
     * @param vlr_total_abonos_me the vlr_total_abonos_me to set
     */
    public void setVlr_total_abonos_me(double vlr_total_abonos_me) {
        this.vlr_total_abonos_me = vlr_total_abonos_me;
    }

    /**
     * @return the vlr_saldo_me
     */
    public double getVlr_saldo_me() {
        return vlr_saldo_me;
    }

    /**
     * @param vlr_saldo_me the vlr_saldo_me to set
     */
    public void setVlr_saldo_me(double vlr_saldo_me) {
        this.vlr_saldo_me = vlr_saldo_me;
    }

    /**
     * @return the tasa
     */
    public double getTasa() {
        return tasa;
    }

    /**
     * @param tasa the tasa to set
     */
    public void setTasa(double tasa) {
        this.tasa = tasa;
    }

    /**
     * @return the usuario_contabilizo
     */
    public String getUsuario_contabilizo() {
        return usuario_contabilizo;
    }

    /**
     * @param usuario_contabilizo the usuario_contabilizo to set
     */
    public void setUsuario_contabilizo(String usuario_contabilizo) {
        this.usuario_contabilizo = usuario_contabilizo;
    }

    /**
     * @return the fecha_contabilizacion
     */
    public String getFecha_contabilizacion() {
        return fecha_contabilizacion;
    }

    /**
     * @param fecha_contabilizacion the fecha_contabilizacion to set
     */
    public void setFecha_contabilizacion(String fecha_contabilizacion) {
        this.fecha_contabilizacion = fecha_contabilizacion;
    }

    /**
     * @return the usuario_anulo
     */
    public String getUsuario_anulo() {
        return usuario_anulo;
    }

    /**
     * @param usuario_anulo the usuario_anulo to set
     */
    public void setUsuario_anulo(String usuario_anulo) {
        this.usuario_anulo = usuario_anulo;
    }

    /**
     * @return the fecha_anulacion
     */
    public String getFecha_anulacion() {
        return fecha_anulacion;
    }

    /**
     * @param fecha_anulacion the fecha_anulacion to set
     */
    public void setFecha_anulacion(String fecha_anulacion) {
        this.fecha_anulacion = fecha_anulacion;
    }

    /**
     * @return the fecha_contabilizacion_anulacion
     */
    public String getFecha_contabilizacion_anulacion() {
        return fecha_contabilizacion_anulacion;
    }

    /**
     * @param fecha_contabilizacion_anulacion the fecha_contabilizacion_anulacion to set
     */
    public void setFecha_contabilizacion_anulacion(String fecha_contabilizacion_anulacion) {
        this.fecha_contabilizacion_anulacion = fecha_contabilizacion_anulacion;
    }

    /**
     * @return the observacion
     */
    public String getObservacion() {
        return observacion;
    }

    /**
     * @param observacion the observacion to set
     */
    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    /**
     * @return the num_obs_autorizador
     */
    public double getNum_obs_autorizador() {
        return num_obs_autorizador;
    }

    /**
     * @param num_obs_autorizador the num_obs_autorizador to set
     */
    public void setNum_obs_autorizador(double num_obs_autorizador) {
        this.num_obs_autorizador = num_obs_autorizador;
    }

    /**
     * @return the num_obs_pagador
     */
    public double getNum_obs_pagador() {
        return num_obs_pagador;
    }

    /**
     * @param num_obs_pagador the num_obs_pagador to set
     */
    public void setNum_obs_pagador(double num_obs_pagador) {
        this.num_obs_pagador = num_obs_pagador;
    }

    /**
     * @return the num_obs_registra
     */
    public double getNum_obs_registra() {
        return num_obs_registra;
    }

    /**
     * @param num_obs_registra the num_obs_registra to set
     */
    public void setNum_obs_registra(double num_obs_registra) {
        this.num_obs_registra = num_obs_registra;
    }

    /**
     * @return the last_update
     */
    public String getLast_update() {
        return last_update;
    }

    /**
     * @param last_update the last_update to set
     */
    public void setLast_update(String last_update) {
        this.last_update = last_update;
    }

    /**
     * @return the user_update
     */
    public String getUser_update() {
        return user_update;
    }

    /**
     * @param user_update the user_update to set
     */
    public void setUser_update(String user_update) {
        this.user_update = user_update;
    }

    /**
     * @return the creation_date
     */
    public String getCreation_date() {
        return creation_date;
    }

    /**
     * @param creation_date the creation_date to set
     */
    public void setCreation_date(String creation_date) {
        this.creation_date = creation_date;
    }

    /**
     * @return the creation_user
     */
    public String getCreation_user() {
        return creation_user;
    }

    /**
     * @param creation_user the creation_user to set
     */
    public void setCreation_user(String creation_user) {
        this.creation_user = creation_user;
    }

    /**
     * @return the base
     */
    public String getBase() {
        return base;
    }

    /**
     * @param base the base to set
     */
    public void setBase(String base) {
        this.base = base;
    }

    /**
     * @return the corrida
     */
    public String getCorrida() {
        return corrida;
    }

    /**
     * @param corrida the corrida to set
     */
    public void setCorrida(String corrida) {
        this.corrida = corrida;
    }

    /**
     * @return the cheque
     */
    public String getCheque() {
        return cheque;
    }

    /**
     * @param cheque the cheque to set
     */
    public void setCheque(String cheque) {
        this.cheque = cheque;
    }

    /**
     * @return the periodo
     */
    public String getPeriodo() {
        return periodo;
    }

    /**
     * @param periodo the periodo to set
     */
    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }

    /**
     * @return the fecha_procesado
     */
    public String getFecha_procesado() {
        return fecha_procesado;
    }

    /**
     * @param fecha_procesado the fecha_procesado to set
     */
    public void setFecha_procesado(String fecha_procesado) {
        this.fecha_procesado = fecha_procesado;
    }

    /**
     * @return the fecha_contabilizacion_ajc
     */
    public String getFecha_contabilizacion_ajc() {
        return fecha_contabilizacion_ajc;
    }

    /**
     * @param fecha_contabilizacion_ajc the fecha_contabilizacion_ajc to set
     */
    public void setFecha_contabilizacion_ajc(String fecha_contabilizacion_ajc) {
        this.fecha_contabilizacion_ajc = fecha_contabilizacion_ajc;
    }

    /**
     * @return the fecha_contabilizacion_ajv
     */
    public String getFecha_contabilizacion_ajv() {
        return fecha_contabilizacion_ajv;
    }

    /**
     * @param fecha_contabilizacion_ajv the fecha_contabilizacion_ajv to set
     */
    public void setFecha_contabilizacion_ajv(String fecha_contabilizacion_ajv) {
        this.fecha_contabilizacion_ajv = fecha_contabilizacion_ajv;
    }

    /**
     * @return the periodo_ajc
     */
    public String getPeriodo_ajc() {
        return periodo_ajc;
    }

    /**
     * @param periodo_ajc the periodo_ajc to set
     */
    public void setPeriodo_ajc(String periodo_ajc) {
        this.periodo_ajc = periodo_ajc;
    }

    /**
     * @return the periodo_ajv
     */
    public String getPeriodo_ajv() {
        return periodo_ajv;
    }

    /**
     * @param periodo_ajv the periodo_ajv to set
     */
    public void setPeriodo_ajv(String periodo_ajv) {
        this.periodo_ajv = periodo_ajv;
    }

    /**
     * @return the usuario_contabilizo_ajc
     */
    public String getUsuario_contabilizo_ajc() {
        return usuario_contabilizo_ajc;
    }

    /**
     * @param usuario_contabilizo_ajc the usuario_contabilizo_ajc to set
     */
    public void setUsuario_contabilizo_ajc(String usuario_contabilizo_ajc) {
        this.usuario_contabilizo_ajc = usuario_contabilizo_ajc;
    }

    /**
     * @return the usuario_contabilizo_ajv
     */
    public String getUsuario_contabilizo_ajv() {
        return usuario_contabilizo_ajv;
    }

    /**
     * @param usuario_contabilizo_ajv the usuario_contabilizo_ajv to set
     */
    public void setUsuario_contabilizo_ajv(String usuario_contabilizo_ajv) {
        this.usuario_contabilizo_ajv = usuario_contabilizo_ajv;
    }

    /**
     * @return the transaccion_ajc
     */
    public int getTransaccion_ajc() {
        return transaccion_ajc;
    }

    /**
     * @param transaccion_ajc the transaccion_ajc to set
     */
    public void setTransaccion_ajc(int transaccion_ajc) {
        this.transaccion_ajc = transaccion_ajc;
    }

    /**
     * @return the transaccion_ajv
     */
    public int getTransaccion_ajv() {
        return transaccion_ajv;
    }

    /**
     * @param transaccion_ajv the transaccion_ajv to set
     */
    public void setTransaccion_ajv(int transaccion_ajv) {
        this.transaccion_ajv = transaccion_ajv;
    }

    /**
     * @return the clase_documento
     */
    public String getClase_documento() {
        return clase_documento;
    }

    /**
     * @param clase_documento the clase_documento to set
     */
    public void setClase_documento(String clase_documento) {
        this.clase_documento = clase_documento;
    }

    /**
     * @return the transaccion
     */
    public int getTransaccion() {
        return transaccion;
    }

    /**
     * @param transaccion the transaccion to set
     */
    public void setTransaccion(int transaccion) {
        this.transaccion = transaccion;
    }

    /**
     * @return the moneda_banco
     */
    public String getMoneda_banco() {
        return moneda_banco;
    }

    /**
     * @param moneda_banco the moneda_banco to set
     */
    public void setMoneda_banco(String moneda_banco) {
        this.moneda_banco = moneda_banco;
    }

    /**
     * @return the fecha_documento
     */
    public String getFecha_documento() {
        return fecha_documento;
    }

    /**
     * @param fecha_documento the fecha_documento to set
     */
    public void setFecha_documento(String fecha_documento) {
        this.fecha_documento = fecha_documento;
    }

    /**
     * @return the fecha_vencimiento
     */
    public String getFecha_vencimiento() {
        return fecha_vencimiento;
    }

    /**
     * @param fecha_vencimiento the fecha_vencimiento to set
     */
    public void setFecha_vencimiento(String fecha_vencimiento) {
        this.fecha_vencimiento = fecha_vencimiento;
    }

    /**
     * @return the ultima_fecha_pago
     */
    public String getUltima_fecha_pago() {
        return ultima_fecha_pago;
    }

    /**
     * @param ultima_fecha_pago the ultima_fecha_pago to set
     */
    public void setUltima_fecha_pago(String ultima_fecha_pago) {
        this.ultima_fecha_pago = ultima_fecha_pago;
    }

    /**
     * @return the flujo
     */
    public String getFlujo() {
        return flujo;
    }

    /**
     * @param flujo the flujo to set
     */
    public void setFlujo(String flujo) {
        this.flujo = flujo;
    }

    /**
     * @return the transaccion_anulacion
     */
    public int getTransaccion_anulacion() {
        return transaccion_anulacion;
    }

    /**
     * @param transaccion_anulacion the transaccion_anulacion to set
     */
    public void setTransaccion_anulacion(int transaccion_anulacion) {
        this.transaccion_anulacion = transaccion_anulacion;
    }

    /**
     * @return the ret_pago
     */
    public String getRet_pago() {
        return ret_pago;
    }

    /**
     * @param ret_pago the ret_pago to set
     */
    public void setRet_pago(String ret_pago) {
        this.ret_pago = ret_pago;
    }

    /**
     * @return the clase_documento_rel
     */
    public String getClase_documento_rel() {
        return clase_documento_rel;
    }

    /**
     * @param clase_documento_rel the clase_documento_rel to set
     */
    public void setClase_documento_rel(String clase_documento_rel) {
        this.clase_documento_rel = clase_documento_rel;
    }

    /**
     * @return the tipo_referencia_1
     */
    public String getTipo_referencia_1() {
        return tipo_referencia_1;
    }

    /**
     * @param tipo_referencia_1 the tipo_referencia_1 to set
     */
    public void setTipo_referencia_1(String tipo_referencia_1) {
        this.tipo_referencia_1 = tipo_referencia_1;
    }

    /**
     * @return the referencia_1
     */
    public String getReferencia_1() {
        return referencia_1;
    }

    /**
     * @param referencia_1 the referencia_1 to set
     */
    public void setReferencia_1(String referencia_1) {
        this.referencia_1 = referencia_1;
    }

    /**
     * @return the tipo_referencia_2
     */
    public String getTipo_referencia_2() {
        return tipo_referencia_2;
    }

    /**
     * @param tipo_referencia_2 the tipo_referencia_2 to set
     */
    public void setTipo_referencia_2(String tipo_referencia_2) {
        this.tipo_referencia_2 = tipo_referencia_2;
    }

    /**
     * @return the referencia_2
     */
    public String getReferencia_2() {
        return referencia_2;
    }

    /**
     * @param referencia_2 the referencia_2 to set
     */
    public void setReferencia_2(String referencia_2) {
        this.referencia_2 = referencia_2;
    }

    /**
     * @return the tipo_referencia_3
     */
    public String getTipo_referencia_3() {
        return tipo_referencia_3;
    }

    /**
     * @param tipo_referencia_3 the tipo_referencia_3 to set
     */
    public void setTipo_referencia_3(String tipo_referencia_3) {
        this.tipo_referencia_3 = tipo_referencia_3;
    }

    /**
     * @return the referencia_3
     */
    public String getReferencia_3() {
        return referencia_3;
    }

    /**
     * @param referencia_3 the referencia_3 to set
     */
    public void setReferencia_3(String referencia_3) {
        this.referencia_3 = referencia_3;
    }

    /**
     * @return the indicador_traslado_fintra
     */
    public String getIndicador_traslado_fintra() {
        return indicador_traslado_fintra;
    }

    /**
     * @param indicador_traslado_fintra the indicador_traslado_fintra to set
     */
    public void setIndicador_traslado_fintra(String indicador_traslado_fintra) {
        this.indicador_traslado_fintra = indicador_traslado_fintra;
    }

    /**
     * @return the factoring_formula_aplicada
     */
    public String getFactoring_formula_aplicada() {
        return factoring_formula_aplicada;
    }

    /**
     * @param factoring_formula_aplicada the factoring_formula_aplicada to set
     */
    public void setFactoring_formula_aplicada(String factoring_formula_aplicada) {
        this.factoring_formula_aplicada = factoring_formula_aplicada;
    }


}