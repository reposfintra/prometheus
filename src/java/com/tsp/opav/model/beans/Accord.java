/* * Accord.java  * Created on 1 de junio de 2009, 8:08 */
package com.tsp.opav.model.beans;
/** * @author  Fintra */
public class Accord {
    private String id_accion, id_orden, consecutivo, id_contratista, acciones, total_prev1,
       last_update_finv, fecha_envio_ws, fecha_registro, por_actualizar,
       valor_materiales, valor_mano_obra, valor_otros, simbolo_variable,
       fact_conformada, eca_oferta, oferta, f_facturado_cliente, exf_facturado_cliente,
       aiu_administracion, aiu_imprevistos, aiu_utilidad, factura_contratista;
    private String nombre_contratista;
    public Accord() {    }

    public String getIdAccion() {
        return id_accion;
    }
    public void setIdAccion(String x) {
        this.id_accion= x;
    }
    public String getIdOrden(){
        return id_orden;
    }
    public void setIdOrden(String x) {
        this.id_orden= x;
    }
    public String getConsecutivo(){
        return consecutivo;
    }
    public void setConsecutivo(String x) {
        this.consecutivo= x;
    }
    public String getIdContratista(){//sale de jsp
        return id_contratista;
    }
    public void setIdContratista(String x) {
        this.id_contratista= x;
    }
    public String getAcciones(){
        return acciones;
    }
    public void setAcciones(String x) {//sale de jsp y es full texto
        this.acciones= x;
    }
    public String getTotalPrev1(){
        return total_prev1;
    }
    public void setTotalPrev1(String x) {
        this.total_prev1= x;
    }
    public String getLastUpdate(){
        return last_update_finv;
    }
    public void setLastUpdate(String x) {
        this.last_update_finv= x;
    }
    public String getFechaEnvioWs(){
        return fecha_envio_ws;
    }
    public void setFechaEnvioWs(String x) {
        this.fecha_envio_ws= x;
    }
    public String getFechaRegistro(){
        return fecha_registro;
    }
    public void setFechaRegistro(String x) {
        this.fecha_registro= x;
    }
    public String getPorActualizar(){
        return por_actualizar;
    }
    public void setPorActualizar(String x) {
        this.por_actualizar= x;
    }
    public String getValorMateriales(){//sale de jsp
        return valor_materiales;
    }
    public void setValorMateriales(String x) {
        this.valor_materiales= x;
    }
    public String getValorManoObra(){//sale de jsp
        return valor_mano_obra;
    }
    public void setValorManoObra(String x) {
        this.valor_mano_obra= x;
    }
    public String getValorOtros(){//sale de jsp
        return valor_otros;
    }
    public void setValorOtros(String x) {
        this.valor_otros= x;
    }
    public String getSimboloVariable(){//sale de jsp  quitar
        return simbolo_variable;
    }
    public void setSimboloVariable(String x) {
        this.simbolo_variable= x;
    }
    public String getFactConformada(){//sale de jsp*  quitar
        return fact_conformada;
    }
    public void setFactConformada(String x) {
        this.fact_conformada= x;
    }
    public String getEcaOferta(){
        return eca_oferta;
    }
    public void setEcaOferta(String x) {
        this.eca_oferta= x;
    }
    public String getOferta(){
        return oferta;
    }
    public void setOferta(String x) {
        this.oferta= x;
    }
    public String getFFacturadoCliente(){
        return f_facturado_cliente;
    }
    public void setFFacturadoCliente(String x) {
        this.f_facturado_cliente= x;
    }
    public String getAdministracion(){//sale de jsp
        return aiu_administracion;
    }
    public void setAdministracion(String x) {
        this.aiu_administracion= x;
    }
    public String getImprevistos(){//sale de jsp
        return aiu_imprevistos;
    }
    public void setImprevistos(String x) {
        this.aiu_imprevistos= x;
    }
    public String getUtilidad(){//sale de jsp
        return aiu_utilidad;
    }
    public void setUtilidad(String x) {
        this.aiu_utilidad= x;
    }
    public String getFacturaContratista(){
        return factura_contratista;
    }
    public void setFacturaContratista(String x) {
        this.factura_contratista= x;
    }
    public String getNombreContratista(){//sale de id_contratista
        return nombre_contratista;
    }
    public void setNombreContratista(String x) {
        this.nombre_contratista= x;
    }

}