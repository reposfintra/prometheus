/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.opav.model.beans;

import java.sql.*;

/**
 *
 * @author Alvaro
 */
public class FacturaFactoringFormula {


   private String dstrct;
   private String tipo_documento;
   private String documento;
   private String proveedor;
   private double val_factoring_contratista;
   private double val_formula_contratista;
   private double total_nota_credito;




    public static FacturaFactoringFormula load(ResultSet rs)throws SQLException {
        FacturaFactoringFormula facturaFactoringFormula = new FacturaFactoringFormula();


        facturaFactoringFormula.setDstrct( rs.getString("dstrct") );
        facturaFactoringFormula.setTipo_documento(rs.getString("tipo_documento") );
        facturaFactoringFormula.setDocumento(rs.getString("documento") );
        facturaFactoringFormula.setProveedor(rs.getString("proveedor") );
        facturaFactoringFormula.setVal_factoring_contratista(rs.getDouble("val_factoring_contratista") );
        facturaFactoringFormula.setVal_formula_contratista(rs.getDouble("val_formula_contratista"));
        facturaFactoringFormula.setTotal_nota_credito(rs.getDouble("total_nota_credito"));

        return facturaFactoringFormula;
    }




    /**
     * @return the dstrct
     */
    public String getDstrct() {
        return dstrct;
    }

    /**
     * @param dstrct the dstrct to set
     */
    public void setDstrct(String dstrct) {
        this.dstrct = dstrct;
    }

    /**
     * @return the tipo_documento
     */
    public String getTipo_documento() {
        return tipo_documento;
    }

    /**
     * @param tipo_documento the tipo_documento to set
     */
    public void setTipo_documento(String tipo_documento) {
        this.tipo_documento = tipo_documento;
    }

    /**
     * @return the documento
     */
    public String getDocumento() {
        return documento;
    }

    /**
     * @param documento the documento to set
     */
    public void setDocumento(String documento) {
        this.documento = documento;
    }

    /**
     * @return the factura_traslado
     */
    public String getProveedor() {
        return proveedor;
    }

    /**
     * @param factura_traslado the factura_traslado to set
     */
    public void setProveedor(String proveedor) {
        this.proveedor = proveedor;
    }

    /**
     * @return the val_factoring_contratista
     */
    public double getVal_factoring_contratista() {
        return val_factoring_contratista;
    }

    /**
     * @param val_factoring_contratista the val_factoring_contratista to set
     */
    public void setVal_factoring_contratista(double val_factoring_contratista) {
        this.val_factoring_contratista = val_factoring_contratista;
    }

    /**
     * @return the val_formula_contratista
     */
    public double getVal_formula_contratista() {
        return val_formula_contratista;
    }

    /**
     * @param val_formula_contratista the val_formula_contratista to set
     */
    public void setVal_formula_contratista(double val_formula_contratista) {
        this.val_formula_contratista = val_formula_contratista;
    }

    /**
     * @return the total_nota_credito
     */
    public double getTotal_nota_credito() {
        return total_nota_credito;
    }

    /**
     * @param total_nota_credito the total_nota_credito to set
     */
    public void setTotal_nota_credito(double total_nota_credito) {
        this.total_nota_credito = total_nota_credito;
    }


}