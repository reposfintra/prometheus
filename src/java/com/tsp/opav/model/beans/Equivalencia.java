/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.opav.model.beans;

/**
 *
 * @author Alvaro
 */
public class Equivalencia {

  private String reg_status;
  private String cia_inicial;
  private String cia_final;
  private String clase_inicial;
  private String codigo_inicial;
  private String clase_final;
  private String codigo_final;
  private String last_update;
  private String user_update;
  private String creation_date;
  private String creation_user;


    /** Creates a new instance of Prefactura */
    public Equivalencia() {
    }

    public static Equivalencia load(java.sql.ResultSet rs)throws java.sql.SQLException{

        Equivalencia equivalencia = new Equivalencia();

        equivalencia.setReg_status( rs.getString("reg_status") );
        equivalencia.setCia_inicial( rs.getString("cia_inicial") );
        equivalencia.setCia_final( rs.getString("cia_final") );
        equivalencia.setClase_inicial( rs.getString("clase_inicial") );
        equivalencia.setCodigo_inicial( rs.getString("codigo_inicial") );
        equivalencia.setClase_final( rs.getString("clase_final") );
        equivalencia.setCodigo_final( rs.getString("codigo_final") );
        equivalencia.setLast_update( rs.getString("last_update") );
        equivalencia.setUser_update( rs.getString("user_update") );
        equivalencia.setCreation_date( rs.getString("creation_date") );
        equivalencia.setCreation_user( rs.getString("creation_user") );

        return equivalencia;
    }





    /**
     * @return the cia_inicial
     */
    public String getCia_inicial() {
        return cia_inicial;
    }

    /**
     * @param cia_inicial the cia_inicial to set
     */
    public void setCia_inicial(String cia_inicial) {
        this.cia_inicial = cia_inicial;
    }

    /**
     * @return the cia_final
     */
    public String getCia_final() {
        return cia_final;
    }

    /**
     * @param cia_final the cia_final to set
     */
    public void setCia_final(String cia_final) {
        this.cia_final = cia_final;
    }

    /**
     * @return the clase_inicial
     */
    public String getClase_inicial() {
        return clase_inicial;
    }

    /**
     * @param clase_inicial the clase_inicial to set
     */
    public void setClase_inicial(String clase_inicial) {
        this.clase_inicial = clase_inicial;
    }

    /**
     * @return the codigo_inicial
     */
    public String getCodigo_inicial() {
        return codigo_inicial;
    }

    /**
     * @param codigo_inicial the codigo_inicial to set
     */
    public void setCodigo_inicial(String codigo_inicial) {
        this.codigo_inicial = codigo_inicial;
    }

    /**
     * @return the clase_final
     */
    public String getClase_final() {
        return clase_final;
    }

    /**
     * @param clase_final the clase_final to set
     */
    public void setClase_final(String clase_final) {
        this.clase_final = clase_final;
    }

    /**
     * @return the codigo_final
     */
    public String getCodigo_final() {
        return codigo_final;
    }

    /**
     * @param codigo_final the codigo_final to set
     */
    public void setCodigo_final(String codigo_final) {
        this.codigo_final = codigo_final;
    }

    /**
     * @return the reg_status
     */
    public String getReg_status() {
        return reg_status;
    }

    /**
     * @param reg_status the reg_status to set
     */
    public void setReg_status(String reg_status) {
        this.reg_status = reg_status;
    }

    /**
     * @return the last_update
     */
    public String getLast_update() {
        return last_update;
    }

    /**
     * @param last_update the last_update to set
     */
    public void setLast_update(String last_update) {
        this.last_update = last_update;
    }

    /**
     * @return the user_update
     */
    public String getUser_update() {
        return user_update;
    }

    /**
     * @param user_update the user_update to set
     */
    public void setUser_update(String user_update) {
        this.user_update = user_update;
    }

    /**
     * @return the creation_date
     */
    public String getCreation_date() {
        return creation_date;
    }

    /**
     * @param creation_date the creation_date to set
     */
    public void setCreation_date(String creation_date) {
        this.creation_date = creation_date;
    }

    /**
     * @return the creation_user
     */
    public String getCreation_user() {
        return creation_user;
    }

    /**
     * @param creation_user the creation_user to set
     */
    public void setCreation_user(String creation_user) {
        this.creation_user = creation_user;
    }



}

