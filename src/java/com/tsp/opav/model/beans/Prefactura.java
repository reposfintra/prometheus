/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.opav.model.beans;

/**
 *
 * @author Alvaro
 */
public class Prefactura {

  private String id_contratista ;
  private String id_orden;
  private String id_accion;
  private String consecutivo;
  private String id_cliente;
  private String nombre_cliente;
  private String direccion;
  private String ciudad;
  private String acciones;
  private double  total_prev1;
  private double  total_prev1_mat;
  private double  total_prev1_mob;
  private double  total_prev1_otr;
  private double  valor_inicial;
  private double  valor_final;
  private double  comision_total;
  private double  comision_ejecutiva;
  private double  comision_canal;
  private int     cuotas_reales;
  private String  tipo_dtf;
  private double  porcentaje_factoring;
  private double  porcentaje_formula;
  private String  num_os;
  private String  factura_conformada;
  private double  administracion;
  private double  imprevisto;
  private double  utilidad;
  private double  base_iva;
  private String esquema_comision;


    /** Creates a new instance of Prefactura */
    public Prefactura() {
    }


    public static Prefactura load(java.sql.ResultSet rs)throws java.sql.SQLException{

        Prefactura prefactura = new Prefactura();

        prefactura.setId_contratista( rs.getString("id_contratista") );
        prefactura.setId_orden(rs.getString("id_orden") );
        prefactura.setId_accion( rs.getString("id_accion") );
        prefactura.setConsecutivo( rs.getString("consecutivo") );
        prefactura.setId_cliente( rs.getString("id_cliente") );
        prefactura.setNombre_cliente( rs.getString("nombre_cliente") );
        prefactura.setDireccion( rs.getString("direccion") );
        prefactura.setCiudad( rs.getString("ciudad") );
        prefactura.setAcciones( rs.getString("acciones") );
        prefactura.setTotal_prev1( rs.getDouble("total_prev1") );
        prefactura.setTotal_prev1_mat( rs.getDouble("total_prev1_mat") );
        prefactura.setTotal_prev1_mob( rs.getDouble("total_prev1_mob") );
        prefactura.setTotal_prev1_otr( rs.getDouble("total_prev1_otr") );
        prefactura.setValor_inicial(rs.getDouble("valor_inicial"));
        prefactura.setValor_final(rs.getDouble("valor_final"));
        prefactura.setComision_total(rs.getDouble("comision_total"));
        prefactura.setComision_ejecutiva(rs.getDouble("comision_ejecutiva"));
        prefactura.setComision_canal(rs.getDouble("comision_canal"));

        prefactura.setCuotas_reales(rs.getInt("cuotas_reales"));
        prefactura.setTipo_dtf(rs.getString("tipo_dtf"));
        prefactura.setPorcentaje_factoring(rs.getString("porcentaje_factoring"));
        prefactura.setPorcentaje_formula(rs.getDouble("porcentaje_formula"));
        prefactura.setNumOs(rs.getString("num_os"));
        prefactura.setFacturaConformada(rs.getString("fact_conformada"));
        prefactura.setAdministracion(rs.getDouble("administracion"));
        prefactura.setImprevisto(rs.getDouble("imprevisto"));
        prefactura.setUtilidad(rs.getDouble("utilidad"));

        prefactura.setEsquemaComision(rs.getString("esquema_comision"));

        prefactura.setBase_iva();

        return prefactura;
    }

    /**
     * @return the contratista
     */
    public String getId_contratista() {
        return id_contratista;
    }

    /**
     * @param contratista the contratista to set
     */
    public void setId_contratista(String id_contratista) {
        this.id_contratista = id_contratista;
    }

    /**
     * @return the id_orden
     */
    public String getId_orden() {
        return id_orden;
    }

    /**
     * @param id_orden the id_orden to set
     */
    public void setId_orden(String id_orden) {
        this.id_orden = id_orden;
    }

    /**
     * @return the id_accion
     */
    public String getId_accion() {
        return id_accion;
    }

    /**
     * @param id_accion the id_accion to set
     */
    public void setId_accion(String id_accion) {
        this.id_accion = id_accion;
    }

    /**
     * @return the consecutivo
     */
    public String getConsecutivo() {
        return consecutivo;
    }

    /**
     * @param consecutivo the consecutivo to set
     */
    public void setConsecutivo(String consecutivo) {
        this.consecutivo = consecutivo;
    }

    /**
     * @return the id_cliente
     */
    public String getId_cliente() {
        return id_cliente;
    }

    /**
     * @param id_cliente the id_cliente to set
     */
    public void setId_cliente(String id_cliente) {
        this.id_cliente = id_cliente;
    }

    /**
     * @return the nombre_cliente
     */
    public String getNombre_cliente() {
        return nombre_cliente;
    }

    /**
     * @param nombre_cliente the nombre_cliente to set
     */
    public void setNombre_cliente(String nombre_cliente) {
        this.nombre_cliente = nombre_cliente;
    }

    /**
     * @return the direccion
     */
    public String getDireccion() {
        return direccion;
    }

    /**
     * @param direccion the direccion to set
     */
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    /**
     * @return the ciudad
     */
    public String getCiudad() {
        return ciudad;
    }

    /**
     * @param ciudad the ciudad to set
     */
    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    /**
     * @return the acciones
     */
    public String getAcciones() {
        return acciones;
    }

    /**
     * @param acciones the acciones to set
     */
    public void setAcciones(String acciones) {
        this.acciones = acciones;
    }

    /**
     * @return the total_prev1
     */
    public double getTotal_prev1() {
        return total_prev1;
    }

    /**
     * @param total_prev1 the total_prev1 to set
     */
    public void setTotal_prev1(double total_prev1) {
        this.total_prev1 = total_prev1;
    }

    /**
     * @return the total_prev1_mat
     */
    public double getTotal_prev1_mat() {
        return total_prev1_mat;
    }

    /**
     * @param total_prev1_mat the total_prev1_mat to set
     */
    public void setTotal_prev1_mat(double total_prev1_mat) {
        this.total_prev1_mat = total_prev1_mat;
    }

    /**
     * @return the total_prev1_mob
     */
    public double getTotal_prev1_mob() {
        return total_prev1_mob;
    }

    /**
     * @param total_prev1_mob the total_prev1_mob to set
     */
    public void setTotal_prev1_mob(double total_prev1_mob) {
        this.total_prev1_mob = total_prev1_mob;
    }

    /**
     * @return the total_prev1_otr
     */
    public double getTotal_prev1_otr() {
        return total_prev1_otr;
    }

    /**
     * @param total_prev1_otr the total_prev1_otr to set
     */
    public void setTotal_prev1_otr(double total_prev1_otr) {
        this.total_prev1_otr = total_prev1_otr;
    }

    /**
     * @return the valor_inicial
     */
    public double getValor_inicial() {
        return valor_inicial;
    }

    /**
     * @param valor_inicial the valor_inicial to set
     */
    public void setValor_inicial(double valor_inicial) {
        this.valor_inicial = valor_inicial;
    }

    /**
     * @return the valor_final
     */
    public double getValor_final() {
        return valor_final;
    }

    /**
     * @param valor_final the valor_final to set
     */
    public void setValor_final(double valor_final) {
        this.valor_final = valor_final;
    }

    /**
     * @return the comision_total
     */
    public double getComision_total() {
        return comision_total;
    }

    /**
     * @param comision_total the comision_total to set
     */
    public void setComision_total(double comision_total) {
        this.comision_total = comision_total;
    }

    /**
     * @return the comision_ejecutiva
     */
    public double getComision_ejecutiva() {
        return comision_ejecutiva;
    }

    /**
     * @param comision_ejecutiva the comision_ejecutiva to set
     */
    public void setComision_ejecutiva(double comision_ejecutiva) {
        this.comision_ejecutiva = comision_ejecutiva;
    }

    /**
     * @return the comision_canal
     */
    public double getComision_canal() {
        return comision_canal;
    }

    /**
     * @param comision_canal the comision_canal to set
     */
    public void setComision_canal(double comision_canal) {
        this.comision_canal = comision_canal;
    }

    /**
     * @return the cuotas_reales
     */
    public int getCuotas_reales() {
        return cuotas_reales;
    }

    /**
     * @param cuotas_reales the cuotas_reales to set
     */
    public void setCuotas_reales(int cuotas_reales) {
        this.cuotas_reales = cuotas_reales;
    }

    /**
     * @return the tipo_dtf
     */
    public String getTipo_dtf() {
        return tipo_dtf;
    }

    /**
     * @param tipo_dtf the tipo_dtf to set
     */
    public void setTipo_dtf(String tipo_dtf) {
        this.tipo_dtf = tipo_dtf;
    }


    /**
     * @return the porcentaje_factoring
     */
    public double getPorcentaje_factoring() {
        return porcentaje_factoring;
    }



    /**
     * @param porcentaje_factoring the porcentaje_factoring to set
     */
    public void setPorcentaje_factoring(String porcentaje_factoring) {
        double numero=Double.valueOf(porcentaje_factoring).doubleValue();
        this.porcentaje_factoring = numero;
    }



    /**
     * @return the porcentaje_formula
     */
    public double  getPorcentaje_formula() {
        return porcentaje_formula;
    }

    /**
     * @param porcentaje_formula the porcentaje_formula to set
     */
    public void setPorcentaje_formula(double porcentaje_formula) {
        this.porcentaje_formula = porcentaje_formula;
    }

    public void setNumOs(String num_os) {
        this.num_os = num_os;
    }
    public String getNumOs() {
        return num_os;
    }

    public void setFacturaConformada(String x1) {
        this.factura_conformada = x1;
    }
    public String getFacturaConformada() {
        return factura_conformada;
    }

    /**
     * @return the administracion
     */
    public double getAdministracion() {
        return administracion;
    }

    /**
     * @param administracion the administracion to set
     */
    public void setAdministracion(double administracion) {
        this.administracion = administracion;
    }

    /**
     * @return the imprevisto
     */
    public double getImprevisto() {
        return imprevisto;
    }

    /**
     * @param imprevisto the imprevisto to set
     */
    public void setImprevisto(double imprevisto) {
        this.imprevisto = imprevisto;
    }

    /**
     * @return the utilidad
     */
    public double getUtilidad() {
        return utilidad;
    }

    /**
     * @param utilidad the utilidad to set
     */
    public void setUtilidad(double utilidad) {
        this.utilidad = utilidad;
    }

    /**
     * @return the base_iva
     */
    public double getBase_iva() {
        return base_iva;
    }

    /**
     * @param base_iva the base_iva to set
     */
    public void setBase_iva() {

        if (this.utilidad != 0){
            this.base_iva = this.utilidad ;}
        else
            this.base_iva = this.total_prev1_mat + this.total_prev1_mob + this.total_prev1_otr - this.comision_total;
    }


    public void setEsquemaComision(String x) {
        this.esquema_comision = x;
    }

    public String getEsquemaComision() {
        return esquema_comision;
    }




}

