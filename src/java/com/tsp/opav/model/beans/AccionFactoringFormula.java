/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.opav.model.beans;

import java.sql.*;


/**
 *
 * @author Alvaro
 */
public class AccionFactoringFormula {


   private String dstrct;
   private String tipo_documento;
   private String documento;
   private String proveedor;
   private String id_accion;
   private double por_factoring_contratista;
   private double val_factoring_contratista;
   private double por_formula_contratista;
   private double val_formula_contratista;






    public static AccionFactoringFormula load(ResultSet rs)throws SQLException {

        AccionFactoringFormula accionFactoringFormula = new AccionFactoringFormula();

        accionFactoringFormula.setDstrct( rs.getString("dstrct") );
        accionFactoringFormula.setTipo_documento(rs.getString("tipo_documento") );
        accionFactoringFormula.setDocumento(rs.getString("documento") );
        accionFactoringFormula.setProveedor(rs.getString("proveedor") );
        accionFactoringFormula.setId_accion(rs.getString("Id_accion") );
        accionFactoringFormula.setPor_factoring_contratista(rs.getDouble("por_factoring_contratista") );
        accionFactoringFormula.setVal_factoring_contratista(rs.getDouble("val_factoring_contratista") );
        accionFactoringFormula.setPor_formula_contratista(rs.getDouble("por_formula_contratista"));
        accionFactoringFormula.setVal_formula_contratista(rs.getDouble("val_formula_contratista"));

        return accionFactoringFormula;
    }








    /**
     * @return the dstrct
     */
    public String getDstrct() {
        return dstrct;
    }

    /**
     * @param dstrct the dstrct to set
     */
    public void setDstrct(String dstrct) {
        this.dstrct = dstrct;
    }

    /**
     * @return the tipo_documento
     */
    public String getTipo_documento() {
        return tipo_documento;
    }

    /**
     * @param tipo_documento the tipo_documento to set
     */
    public void setTipo_documento(String tipo_documento) {
        this.tipo_documento = tipo_documento;
    }

    /**
     * @return the documento
     */
    public String getDocumento() {
        return documento;
    }

    /**
     * @param documento the documento to set
     */
    public void setDocumento(String documento) {
        this.documento = documento;
    }


    /**
     * @return the id_accion
     */
    public String getId_accion() {
        return id_accion;
    }

    /**
     * @param id_accion the id_accion to set
     */
    public void setId_accion(String id_accion) {
        this.id_accion = id_accion;
    }

    /**
     * @return the por_factoring_contratista
     */
    public double getPor_factoring_contratista() {
        return por_factoring_contratista;
    }

    /**
     * @param por_factoring_contratista the por_factoring_contratista to set
     */
    public void setPor_factoring_contratista(double por_factoring_contratista) {
        this.por_factoring_contratista = por_factoring_contratista;
    }

    /**
     * @return the val_factoring_contratista
     */
    public double getVal_factoring_contratista() {
        return val_factoring_contratista;
    }

    /**
     * @param val_factoring_contratista the val_factoring_contratista to set
     */
    public void setVal_factoring_contratista(double val_factoring_contratista) {
        this.val_factoring_contratista = val_factoring_contratista;
    }

    /**
     * @return the por_formula_contratista
     */
    public double getPor_formula_contratista() {
        return por_formula_contratista;
    }

    /**
     * @param por_formula_contratista the por_formula_contratista to set
     */
    public void setPor_formula_contratista(double por_formula_contratista) {
        this.por_formula_contratista = por_formula_contratista;
    }

    /**
     * @return the val_formula_contratista
     */
    public double getVal_formula_contratista() {
        return val_formula_contratista;
    }

    /**
     * @param val_formula_contratista the val_formula_contratista to set
     */
    public void setVal_formula_contratista(double val_formula_contratista) {
        this.val_formula_contratista = val_formula_contratista;
    }

    /**
     * @return the proveedor
     */
    public String getProveedor() {
        return proveedor;
    }

    /**
     * @param proveedor the proveedor to set
     */
    public void setProveedor(String proveedor) {
        this.proveedor = proveedor;
    }





}