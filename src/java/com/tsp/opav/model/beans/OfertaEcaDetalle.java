/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.opav.model.beans;

/**
 *
 * @author Alvaro
 */
public class OfertaEcaDetalle {

  private int     id_orden;
  private String  id_accion;
  private double  total_prev1_applus;
  private double  total_prev1_mat;
  private double  total_prev1_mob;
  private double  total_prev1_otr;
  private double  administracion;
  private double  imprevisto;
  private double  utilidad;
  private int     bonificacion;
  private int     cuotas_reales;
  private String  tipo_dtf;
  private double  porcentaje_factoring;
  private String  fecha_oferta;
  private double  dtf_semana;

  private double  total_prev1;
  private double  iva_total_prev1;
  private double  comision_applus;
  private double  comision_provintegral;
  private double  iva_comision_applus;
  private double  iva_comision_provintegral;
  private double  comision_eca;
  private double  iva_comision_eca;
  private double  financiacion_fintra;
  private double  cuota_pago;
  private double  eca_oferta_calculada;
  private double  eca_oferta;
  private double  oferta;
  private String  fecha_financiacion;
  private String  esquema_comision;

  private double porcentaje_comision_applus;
  private double porcentaje_comision_provintegral;
  private double porcentaje_comision_fintra ;
  private double porcentaje_comision_eca;
  private double porcentaje_iva;
  private double comision_fintra;
  private double iva_comision_fintra;

  private int nic;
  private String nombre_cliente;
  private String num_os;
  private int id_estado_negocio;
  private double porcentaje_factoring_fintra;
  private double comision_factoring_fintra;
  private double iva_comision_factoring_fintra;
  private String esquema_financiacion;
  private String tipo_identificacion;
  private double puntos_dtf;
  private double iva_oferta;
  private double iva_eca_oferta;
  private double oferta_mas_iva;
  private double eca_oferta_mas_iva;
  private double total_financiacion;
  private String estudio_economico;


  private double ec_porcentaje_comision_provintegral;

  private double ec_porcentaje_iva;
  private double ec_porcentaje_factoring_fintra;
  private double ec_porcentaje_comision_applus;
  private double ec_porcentaje_comision_fintra;
  private double ec_porcentaje_comision_eca;

  private double ec_valor_mat;
  private double ec_comision_applus;
  private double ec_comision_provintegral;
  private double ec_comision_factoring_fintra;
  private double ec_comision_fintra;
  private double ec_comision_eca;


  private double ec_iva_valor_mat;
  private double ec_iva_comision_applus;
  private double ec_iva_comision_factoring_fintra ;
  private double ec_iva_comision_fintra;
  private double ec_iva_comision_provintegral;
  private double ec_iva_comision_eca;

  private double ec_financiacion_fintra;

  private double ec_cuota_pago;
  private double ec_total_financiacion;


  private String id_contratista,nombre_contratista;//090715

  private double iva_bonificacion ;







    /** Creates a new instance of OfertaEcaDetalle */
    public OfertaEcaDetalle() {
    }


    public static OfertaEcaDetalle load(java.sql.ResultSet rs)throws java.sql.SQLException{

        OfertaEcaDetalle ofertaEcaDetalle = new OfertaEcaDetalle();


        ofertaEcaDetalle.setId_orden(rs.getInt("id_orden") );
        ofertaEcaDetalle.setId_accion(rs.getString("id_accion") );
        ofertaEcaDetalle.setTotal_prev1_applus(rs.getDouble("total_prev1_applus"));
        ofertaEcaDetalle.setTotal_prev1_mat( rs.getDouble("total_prev1_mat") );
        ofertaEcaDetalle.setTotal_prev1_mob( rs.getDouble("total_prev1_mob") );
        ofertaEcaDetalle.setTotal_prev1_otr( rs.getDouble("total_prev1_otr") );
        ofertaEcaDetalle.setAdministracion(rs.getDouble("administracion"));
        ofertaEcaDetalle.setImprevisto(rs.getDouble("imprevisto"));
        ofertaEcaDetalle.setUtilidad(rs.getDouble("utilidad"));
        ofertaEcaDetalle.setBonificacion(rs.getInt("bonificacion"));
        ofertaEcaDetalle.setCuotas_reales(rs.getInt("cuotas_reales"));
        ofertaEcaDetalle.setTipo_dtf(rs.getString("tipo_dtf"));
        ofertaEcaDetalle.setPorcentaje_factoring(rs.getDouble("porcentaje_factoring")/100.0);
        ofertaEcaDetalle.setFecha_oferta(rs.getString("fecha_oferta"));
        ofertaEcaDetalle.setDtf_semana(rs.getDouble("dtf_semana"));
        ofertaEcaDetalle.setEca_oferta(rs.getDouble("eca_oferta"));
        ofertaEcaDetalle.setOferta(rs.getDouble("oferta"));
        ofertaEcaDetalle.setEsquema_comision(rs.getString("esquema_comision"));
        ofertaEcaDetalle.setPorcentaje_comision_applus(rs.getDouble("porcentaje_comision_applus"));
        ofertaEcaDetalle.setPorcentaje_comision_provintegral(rs.getDouble("porcentaje_comision_provintegral"));
        ofertaEcaDetalle.setPorcentaje_comision_fintra(rs.getDouble("porcentaje_comision_fintra"));
        ofertaEcaDetalle.setPorcentaje_comision_eca(rs.getDouble("porcentaje_comision_eca"));
        ofertaEcaDetalle.setPorcentaje_iva(rs.getDouble("porcentaje_iva"));
        ofertaEcaDetalle.setNic(rs.getInt("nic"));
        ofertaEcaDetalle.setNombre_cliente(rs.getString("nombre_cliente"));
        ofertaEcaDetalle.setNum_os(rs.getString("num_os"));
        ofertaEcaDetalle.setId_estado_negocio(rs.getInt("id_estado_negocio"));
        ofertaEcaDetalle.setPorcentaje_factoring_fintra(rs.getDouble("porcentaje_factoring_fintra")/100);
        ofertaEcaDetalle.setEsquema_financiacion(rs.getString("esquema_financiacion"));
        ofertaEcaDetalle.setTipo_identificacion(rs.getString("tipo_identificacion"));
        ofertaEcaDetalle.setEstudio_economico(rs.getString("estudio_economico"));


        ofertaEcaDetalle.setEc_porcentaje_comision_applus(rs.getDouble("ec_porcentaje_comision_applus"));
        ofertaEcaDetalle.setEc_porcentaje_comision_provintegral(rs.getDouble("ec_porcentaje_comision_provintegral"));
        ofertaEcaDetalle.setEc_porcentaje_comision_fintra(rs.getDouble("ec_porcentaje_comision_fintra"));
        ofertaEcaDetalle.setEc_porcentaje_comision_eca(rs.getDouble("ec_porcentaje_comision_eca"));
        ofertaEcaDetalle.setEc_porcentaje_iva(rs.getDouble("ec_porcentaje_iva"));


        ofertaEcaDetalle.setTotal_prev1(0);
        ofertaEcaDetalle.setIva_total_prev1(0);
        ofertaEcaDetalle.setComision_applus(0);
        ofertaEcaDetalle.setComision_provintegral(0);
        ofertaEcaDetalle.setIva_comision_applus(0);
        ofertaEcaDetalle.setIva_comision_provintegral(0);
        ofertaEcaDetalle.setComision_eca(0);
        ofertaEcaDetalle.setIva_comision_eca(0);
        ofertaEcaDetalle.setFinanciacion_fintra(0);
        ofertaEcaDetalle.setCuota_pago(0);
        ofertaEcaDetalle.setEca_oferta_calculada(0);
        ofertaEcaDetalle.setFecha_financiacion("");
        ofertaEcaDetalle.setComision_fintra(0);
        ofertaEcaDetalle.setIva_comision_fintra(0);
        ofertaEcaDetalle.setComision_factoring_fintra(0);
        ofertaEcaDetalle.setIva_comision_factoring_fintra(0);
        ofertaEcaDetalle.setPuntos_dtf(0);

        ofertaEcaDetalle.setIva_oferta(0);
        ofertaEcaDetalle.setIva_eca_oferta(0);
        ofertaEcaDetalle.setOferta_mas_iva(0);
        ofertaEcaDetalle.setEca_oferta_mas_iva(0);
        ofertaEcaDetalle.setTotal_financiacion(0);


        ofertaEcaDetalle.setEc_valor_mat(0);
        ofertaEcaDetalle.setEc_comision_applus(0);
        ofertaEcaDetalle.setEc_comision_provintegral(0);
        ofertaEcaDetalle.setEc_comision_factoring_fintra(0);
        ofertaEcaDetalle.setEc_comision_fintra(0);
        ofertaEcaDetalle.setEc_comision_eca(0);

        ofertaEcaDetalle.setEc_porcentaje_factoring_fintra(0);

        ofertaEcaDetalle.setEc_iva_valor_mat(0);
        ofertaEcaDetalle.setEc_iva_comision_applus(0);
        ofertaEcaDetalle.setEc_iva_comision_factoring_fintra(0);
        ofertaEcaDetalle.setEc_iva_comision_fintra(0);
        ofertaEcaDetalle.setEc_iva_comision_provintegral(0);
        ofertaEcaDetalle.setEc_iva_comision_eca(0);

        ofertaEcaDetalle.setEc_financiacion_fintra(0);
        ofertaEcaDetalle.setEc_cuota_pago(0);
        ofertaEcaDetalle.setEc_total_financiacion(0);

        ofertaEcaDetalle.setId_contratista(rs.getString("id_contratista"));//090715
        ofertaEcaDetalle.setNombre_contratista(rs.getString("descripcion"));//090715
        ofertaEcaDetalle.setIvaBonificacion(0);

        return ofertaEcaDetalle;
    }


    /**
     * @return the id_orden
     */
    public int getId_orden() {
        return id_orden;
    }

    /**
     * @param id_orden the id_orden to set
     */
    public void setId_orden(int id_orden) {
        this.id_orden = id_orden;
    }

    /**
     * @return the total_prev1_mat
     */
    public double getTotal_prev1_mat() {
        return total_prev1_mat;
    }

    /**
     * @param total_prev1_mat the total_prev1_mat to set
     */
    public void setTotal_prev1_mat(double total_prev1_mat) {
        this.total_prev1_mat = total_prev1_mat;
    }

    /**
     * @return the total_prev1_mob
     */
    public double getTotal_prev1_mob() {
        return total_prev1_mob;
    }

    /**
     * @param total_prev1_mob the total_prev1_mob to set
     */
    public void setTotal_prev1_mob(double total_prev1_mob) {
        this.total_prev1_mob = total_prev1_mob;
    }

    /**
     * @return the total_prev1_otr
     */
    public double getTotal_prev1_otr() {
        return total_prev1_otr;
    }

    /**
     * @param total_prev1_otr the total_prev1_otr to set
     */
    public void setTotal_prev1_otr(double total_prev1_otr) {
        this.total_prev1_otr = total_prev1_otr;
    }

    /**
     * @return the administracion
     */
    public double getAdministracion() {
        return administracion;
    }

    /**
     * @param administracion the administracion to set
     */
    public void setAdministracion(double administracion) {
        this.administracion = administracion;
    }

    /**
     * @return the imprevisto
     */
    public double getImprevisto() {
        return imprevisto;
    }

    /**
     * @param imprevisto the imprevisto to set
     */
    public void setImprevisto(double imprevisto) {
        this.imprevisto = imprevisto;
    }

    /**
     * @return the utilidad
     */
    public double getUtilidad() {
        return utilidad;
    }

    /**
     * @param utilidad the utilidad to set
     */
    public void setUtilidad(double utilidad) {
        this.utilidad = utilidad;
    }

    /**
     * @return the bonificacion
     */
    public int getBonificacion() {
        return bonificacion;
    }

    /**
     * @param bonificacion the bonificacion to set
     */
    public void setBonificacion(int bonificacion) {
        this.bonificacion = bonificacion;
    }

    /**
     * @return the cuotas_reales
     */
    public int getCuotas_reales() {
        return cuotas_reales;
    }

    /**
     * @param cuotas_reales the cuotas_reales to set
     */
    public void setCuotas_reales(int cuotas_reales) {
        this.cuotas_reales = cuotas_reales;
    }

    /**
     * @return the tipo_dtf
     */
    public String getTipo_dtf() {
        return tipo_dtf;
    }

    /**
     * @param tipo_dtf the tipo_dtf to set
     */
    public void setTipo_dtf(String tipo_dtf) {
        this.tipo_dtf = tipo_dtf;
    }

    /**
     * @return the porcentaje_factoring
     */
    public double getPorcentaje_factoring() {
        return porcentaje_factoring;
    }

    /**
     * @param porcentaje_factoring the porcentaje_factoring to set
     */
    public void setPorcentaje_factoring(double porcentaje_factoring) {
        this.porcentaje_factoring = porcentaje_factoring;
    }

    /**
     * @return the fecha_oferta
     */
    public String getFecha_oferta() {
        return fecha_oferta;
    }

    /**
     * @param fecha_oferta the fecha_oferta to set
     */
    public void setFecha_oferta(String fecha_oferta) {
        this.fecha_oferta = fecha_oferta;
    }

    /**
     * @return the dtf_semana
     */
    public double getDtf_semana() {
        return dtf_semana;
    }

    /**
     * @param dtf_semana the dtf_semana to set
     */
    public void setDtf_semana(double dtf_semana) {
        this.dtf_semana = dtf_semana;
    }

    /**
     * @return the total_prev1
     */
    public double getTotal_prev1() {
        return total_prev1;
    }

    /**
     * @param total_prev1 the total_prev1 to set
     */
    public void setTotal_prev1(double total_prev1) {
        this.total_prev1 = total_prev1;
    }

    /**
     * @return the iva_total_prev1
     */
    public double getIva_total_prev1() {
        return iva_total_prev1;
    }

    /**
     * @param iva_total_prev1 the iva_total_prev1 to set
     */
    public void setIva_total_prev1(double iva_total_prev1) {
        this.iva_total_prev1 = iva_total_prev1;
    }

    /**
     * @return the comision_applus
     */
    public double getComision_applus() {
        return comision_applus;
    }

    /**
     * @param comision_applus the comision_applus to set
     */
    public void setComision_applus(double comision_applus) {
        this.comision_applus = comision_applus;
    }

    /**
     * @return the comision_provintegral
     */
    public double getComision_provintegral() {
        return comision_provintegral;
    }

    /**
     * @param comision_provintegral the comision_provintegral to set
     */
    public void setComision_provintegral(double comision_provintegral) {
        this.comision_provintegral = comision_provintegral;
    }

    /**
     * @return the iva_comision_applus
     */
    public double getIva_comision_applus() {
        return iva_comision_applus;
    }

    /**
     * @param iva_comision_applus the iva_comision_applus to set
     */
    public void setIva_comision_applus(double iva_comision_applus) {
        this.iva_comision_applus = iva_comision_applus;
    }

    /**
     * @return the iva_comision_provintegral
     */
    public double getIva_comision_provintegral() {
        return iva_comision_provintegral;
    }

    /**
     * @param iva_comision_provintegral the iva_comision_provintegral to set
     */
    public void setIva_comision_provintegral(double iva_comision_provintegral) {
        this.iva_comision_provintegral = iva_comision_provintegral;
    }

    /**
     * @return the comision_eca
     */
    public double getComision_eca() {
        return comision_eca;
    }

    /**
     * @param comision_eca the comision_eca to set
     */
    public void setComision_eca(double comision_eca) {
        this.comision_eca = comision_eca;
    }

    /**
     * @return the iva_comision_eca
     */
    public double getIva_comision_eca() {
        return iva_comision_eca;
    }

    /**
     * @param iva_comision_eca the iva_comision_eca to set
     */
    public void setIva_comision_eca(double iva_comision_eca) {
        this.iva_comision_eca = iva_comision_eca;
    }

    /**
     * @return the financiacion_fintra
     */
    public double getFinanciacion_fintra() {
        return financiacion_fintra;
    }

    /**
     * @param financiacion_fintra the financiacion_fintra to set
     */
    public void setFinanciacion_fintra(double financiacion_fintra) {
        this.financiacion_fintra = financiacion_fintra;
    }

    /**
     * @return the cuota_pago
     */
    public double getCuota_pago() {
        return cuota_pago;
    }

    /**
     * @param cuota_pago the cuota_pago to set
     */
    public void setCuota_pago(double cuota_pago) {
        this.cuota_pago = cuota_pago;
    }

    /**
     * @return the eca_oferta
     */
    public double getEca_oferta_calculada() {
        return eca_oferta_calculada;
    }

    /**
     * @param eca_oferta the eca_oferta to set
     */
    public void setEca_oferta_calculada(double eca_oferta_calculada) {
        this.eca_oferta_calculada = eca_oferta_calculada;
    }

    /**
     * @return the eca_oferta
     */
    public double getEca_oferta() {
        return eca_oferta;
    }

    /**
     * @param eca_oferta the eca_oferta to set
     */
    public void setEca_oferta(double eca_oferta) {
        this.eca_oferta = eca_oferta;
    }

    /**
     * @return the oferta
     */
    public double getOferta() {
        return oferta;
    }

    /**
     * @param oferta the oferta to set
     */
    public void setOferta(double oferta) {
        this.oferta = oferta;
    }

    /**
     * @return the fecha_financiacion
     */
    public String getFecha_financiacion() {
        return fecha_financiacion;
    }

    /**
     * @param fecha_financiacion the fecha_financiacion to set
     */
    public void setFecha_financiacion(String fecha_financiacion) {
        this.fecha_financiacion = fecha_financiacion;
    }

    /**
     * @return the id_accion
     */
    public String getId_accion() {
        return id_accion;
    }

    /**
     * @param id_accion the id_accion to set
     */
    public void setId_accion(String id_accion) {
        this.id_accion = id_accion;
    }

    /**
     * @return the esquema_comision
     */
    public String getEsquema_comision() {
        return esquema_comision;
    }

    /**
     * @param esquema_comision the esquema_comision to set
     */
    public void setEsquema_comision(String esquema_comision) {
        this.esquema_comision = esquema_comision;
    }

    /**
     * @return the porcentaje_comision_applus
     */
    public double getPorcentaje_comision_applus() {
        return porcentaje_comision_applus;
    }

    /**
     * @param porcentaje_comision_applus the porcentaje_comision_applus to set
     */
    public void setPorcentaje_comision_applus(double porcentaje_comision_applus) {
        this.porcentaje_comision_applus = porcentaje_comision_applus;
    }

    /**
     * @return the porcentaje_comision_provintegral
     */
    public double getPorcentaje_comision_provintegral() {
        return porcentaje_comision_provintegral;
    }

    /**
     * @param porcentaje_comision_provintegral the porcentaje_comision_provintegral to set
     */
    public void setPorcentaje_comision_provintegral(double porcentaje_comision_provintegral) {
        this.porcentaje_comision_provintegral = porcentaje_comision_provintegral;
    }

    /**
     * @return the porcentaje_comision_fintra
     */
    public double getPorcentaje_comision_fintra() {
        return porcentaje_comision_fintra;
    }

    /**
     * @param porcentaje_comision_fintra the porcentaje_comision_fintra to set
     */
    public void setPorcentaje_comision_fintra(double porcentaje_comision_fintra) {
        this.porcentaje_comision_fintra = porcentaje_comision_fintra;
    }

    /**
     * @return the porcentaje_comision_eca
     */
    public double getPorcentaje_comision_eca() {
        return porcentaje_comision_eca;
    }

    /**
     * @param porcentaje_comision_eca the porcentaje_comision_eca to set
     */
    public void setPorcentaje_comision_eca(double porcentaje_comision_eca) {
        this.porcentaje_comision_eca = porcentaje_comision_eca;
    }

    /**
     * @return the porcentaje_iva
     */
    public double getPorcentaje_iva() {
        return porcentaje_iva;
    }

    /**
     * @param porcentaje_iva the porcentaje_iva to set
     */
    public void setPorcentaje_iva(double porcentaje_iva) {
        this.porcentaje_iva = porcentaje_iva;
    }

    /**
     * @return the comision_fintra
     */
    public double getComision_fintra() {
        return comision_fintra;
    }

    /**
     * @param comision_fintra the comision_fintra to set
     */
    public void setComision_fintra(double comision_fintra) {
        this.comision_fintra = comision_fintra;
    }

    /**
     * @return the iva_comision_fintra
     */
    public double getIva_comision_fintra() {
        return iva_comision_fintra;
    }

    /**
     * @param iva_comision_fintra the iva_comision_fintra to set
     */
    public void setIva_comision_fintra(double iva_comision_fintra) {
        this.iva_comision_fintra = iva_comision_fintra;
    }

    /**
     * @return the nic
     */
    public int getNic() {
        return nic;
    }

    /**
     * @param nic the nic to set
     */
    public void setNic(int nic) {
        this.nic = nic;
    }

    /**
     * @return the nombre_cliente
     */
    public String getNombre_cliente() {
        return nombre_cliente;
    }

    /**
     * @param nombre_cliente the nombre_cliente to set
     */
    public void setNombre_cliente(String nombre_cliente) {
        this.nombre_cliente = nombre_cliente;
    }

    /**
     * @return the num_os
     */
    public String getNum_os() {
        return num_os;
    }

    /**
     * @param num_os the num_os to set
     */
    public void setNum_os(String num_os) {
        this.num_os = num_os;
    }

    /**
     * @return the id_estado_negocio
     */
    public int getId_estado_negocio() {
        return id_estado_negocio;
    }

    /**
     * @param id_estado_negocio the id_estado_negocio to set
     */
    public void setId_estado_negocio(int id_estado_negocio) {
        this.id_estado_negocio = id_estado_negocio;
    }

    /**
     * @return the porcentaje_factoring_fintra
     */
    public double getPorcentaje_factoring_fintra() {
        return porcentaje_factoring_fintra;
    }

    /**
     * @param porcentaje_factoring_fintra the porcentaje_factoring_fintra to set
     */
    public void setPorcentaje_factoring_fintra(double porcentaje_factoring_fintra) {
        this.porcentaje_factoring_fintra = porcentaje_factoring_fintra;
    }

    /**
     * @return the comision_factoring_fintra
     */
    public double getComision_factoring_fintra() {
        return comision_factoring_fintra;
    }

    /**
     * @param comision_factoring_fintra the comision_factoring_fintra to set
     */
    public void setComision_factoring_fintra(double comision_factoring_fintra) {
        this.comision_factoring_fintra = comision_factoring_fintra;
    }

    /**
     * @return the iva_comision_factoring_fintra
     */
    public double getIva_comision_factoring_fintra() {
        return iva_comision_factoring_fintra;
    }

    /**
     * @param iva_comision_factoring_fintra the iva_comision_factoring_fintra to set
     */
    public void setIva_comision_factoring_fintra(double iva_comision_factoring_fintra) {
        this.iva_comision_factoring_fintra = iva_comision_factoring_fintra;
    }

    /**
     * @return the esquema_financiacion
     */
    public String getEsquema_financiacion() {
        return esquema_financiacion;
    }

    /**
     * @param esquema_financiacion the esquema_financiacion to set
     */
    public void setEsquema_financiacion(String esquema_financiacion) {
        this.esquema_financiacion = esquema_financiacion;
    }

    /**
     * @return the tipo_identificacion
     */
    public String getTipo_identificacion() {
        return tipo_identificacion;
    }

    /**
     * @param tipo_identificacion the tipo_identificacion to set
     */
    public void setTipo_identificacion(String tipo_identificacion) {
        this.tipo_identificacion = tipo_identificacion;
    }

    /**
     * @return the puntos_dtf
     */
    public double getPuntos_dtf() {
        return puntos_dtf;
    }

    /**
     * @param puntos_dtf the puntos_dtf to set
     */
    public void setPuntos_dtf(double puntos_dtf) {
        this.puntos_dtf = puntos_dtf;
    }

    /**
     * @return the iva_oferta
     */
    public double getIva_oferta() {
        return iva_oferta;
    }

    /**
     * @param iva_oferta the iva_oferta to set
     */
    public void setIva_oferta(double iva_oferta) {
        this.iva_oferta = iva_oferta;
    }

    /**
     * @return the iva_eca_oferta
     */
    public double getIva_eca_oferta() {
        return iva_eca_oferta;
    }

    /**
     * @param iva_eca_oferta the iva_eca_oferta to set
     */
    public void setIva_eca_oferta(double iva_eca_oferta) {
        this.iva_eca_oferta = iva_eca_oferta;
    }

    /**
     * @return the oferta_mas_iva
     */
    public double getOferta_mas_iva() {
        return oferta_mas_iva;
    }

    /**
     * @param oferta_mas_iva the oferta_mas_iva to set
     */
    public void setOferta_mas_iva(double oferta_mas_iva) {
        this.oferta_mas_iva = oferta_mas_iva;
    }

    /**
     * @return the eca_oferta_mas_iva
     */
    public double getEca_oferta_mas_iva() {
        return eca_oferta_mas_iva;
    }

    /**
     * @param eca_oferta_mas_iva the eca_oferta_mas_iva to set
     */
    public void setEca_oferta_mas_iva(double eca_oferta_mas_iva) {
        this.eca_oferta_mas_iva = eca_oferta_mas_iva;
    }

    /**
     * @return the total_financiacion
     */
    public double getTotal_financiacion() {
        return total_financiacion;
    }

    /**
     * @param total_financiacion the total_financiacion to set
     */
    public void setTotal_financiacion(double total_financiacion) {
        this.total_financiacion = total_financiacion;
    }

    /**
     * @return the estudio_economico
     */
    public String getEstudio_economico() {
        return estudio_economico;
    }

    /**
     * @param estudio_economico the estudio_economico to set
     */
    public void setEstudio_economico(String estudio_economico) {
        this.estudio_economico = estudio_economico;
    }

    /**
     * @return the ec_porcentaje_iva
     */
    public double getEc_porcentaje_iva() {
        return ec_porcentaje_iva;
    }

    /**
     * @param ec_porcentaje_iva the ec_porcentaje_iva to set
     */
    public void setEc_porcentaje_iva(double ec_porcentaje_iva) {
        this.ec_porcentaje_iva = ec_porcentaje_iva;
    }

    /**
     * @return the ec_porcentaje_comision_provintegral
     */
    public double getEc_porcentaje_comision_provintegral() {
        return ec_porcentaje_comision_provintegral;
    }

    /**
     * @param ec_porcentaje_comision_provintegral the ec_porcentaje_comision_provintegral to set
     */
    public void setEc_porcentaje_comision_provintegral(double ec_porcentaje_comision_provintegral) {
        this.ec_porcentaje_comision_provintegral = ec_porcentaje_comision_provintegral;
    }

    /**
     * @return the ec_porcentaje_factoring_fintra
     */
    public double getEc_porcentaje_factoring_fintra() {
        return ec_porcentaje_factoring_fintra;
    }

    /**
     * @param ec_porcentaje_factoring_fintra the ec_porcentaje_factoring_fintra to set
     */
    public void setEc_porcentaje_factoring_fintra(double ec_porcentaje_factoring_fintra) {
        this.ec_porcentaje_factoring_fintra = ec_porcentaje_factoring_fintra;
    }

    /**
     * @return the ec_porcentaje_comision_applus
     */
    public double getEc_porcentaje_comision_applus() {
        return ec_porcentaje_comision_applus;
    }

    /**
     * @param ec_porcentaje_comision_applus the ec_porcentaje_comision_applus to set
     */
    public void setEc_porcentaje_comision_applus(double ec_porcentaje_comision_applus) {
        this.ec_porcentaje_comision_applus = ec_porcentaje_comision_applus;
    }

    /**
     * @return the ec_porcentaje_comision_fintra
     */
    public double getEc_porcentaje_comision_fintra() {
        return ec_porcentaje_comision_fintra;
    }

    /**
     * @param ec_porcentaje_comision_fintra the ec_porcentaje_comision_fintra to set
     */
    public void setEc_porcentaje_comision_fintra(double ec_porcentaje_comision_fintra) {
        this.ec_porcentaje_comision_fintra = ec_porcentaje_comision_fintra;
    }

    /**
     * @return the ec_porcentaje_comision_eca
     */
    public double getEc_porcentaje_comision_eca() {
        return ec_porcentaje_comision_eca;
    }

    /**
     * @param ec_porcentaje_comision_eca the ec_porcentaje_comision_eca to set
     */
    public void setEc_porcentaje_comision_eca(double ec_porcentaje_comision_eca) {
        this.ec_porcentaje_comision_eca = ec_porcentaje_comision_eca;
    }

    /**
     * @return the ec_valor_mat
     */
    public double getEc_valor_mat() {
        return ec_valor_mat;
    }

    /**
     * @param ec_valor_mat the ec_valor_mat to set
     */
    public void setEc_valor_mat(double ec_valor_mat) {
        this.ec_valor_mat = ec_valor_mat;
    }

    /**
     * @return the ec_comision_applus
     */
    public double getEc_comision_applus() {
        return ec_comision_applus;
    }

    /**
     * @param ec_comision_applus the ec_comision_applus to set
     */
    public void setEc_comision_applus(double ec_comision_applus) {
        this.ec_comision_applus = ec_comision_applus;
    }

    /**
     * @return the ec_comision_provintegral
     */
    public double getEc_comision_provintegral() {
        return ec_comision_provintegral;
    }

    /**
     * @param ec_comision_provintegral the ec_comision_provintegral to set
     */
    public void setEc_comision_provintegral(double ec_comision_provintegral) {
        this.ec_comision_provintegral = ec_comision_provintegral;
    }

    /**
     * @return the ec_comision_factoring_fintra
     */
    public double getEc_comision_factoring_fintra() {
        return ec_comision_factoring_fintra;
    }

    /**
     * @param ec_comision_factoring_fintra the ec_comision_factoring_fintra to set
     */
    public void setEc_comision_factoring_fintra(double ec_comision_factoring_fintra) {
        this.ec_comision_factoring_fintra = ec_comision_factoring_fintra;
    }

    /**
     * @return the ec_comision_fintra
     */
    public double getEc_comision_fintra() {
        return ec_comision_fintra;
    }

    /**
     * @param ec_comision_fintra the ec_comision_fintra to set
     */
    public void setEc_comision_fintra(double ec_comision_fintra) {
        this.ec_comision_fintra = ec_comision_fintra;
    }

    /**
     * @return the ec_comision_eca
     */
    public double getEc_comision_eca() {
        return ec_comision_eca;
    }

    /**
     * @param ec_comision_eca the ec_comision_eca to set
     */
    public void setEc_comision_eca(double ec_comision_eca) {
        this.ec_comision_eca = ec_comision_eca;
    }

    /**
     * @return the ec_iva_valor_mat
     */
    public double getEc_iva_valor_mat() {
        return ec_iva_valor_mat;
    }

    /**
     * @param ec_iva_valor_mat the ec_iva_valor_mat to set
     */
    public void setEc_iva_valor_mat(double ec_iva_valor_mat) {
        this.ec_iva_valor_mat = ec_iva_valor_mat;
    }

    /**
     * @return the ec_iva_comision_applus
     */
    public double getEc_iva_comision_applus() {
        return ec_iva_comision_applus;
    }

    /**
     * @param ec_iva_comision_applus the ec_iva_comision_applus to set
     */
    public void setEc_iva_comision_applus(double ec_iva_comision_applus) {
        this.ec_iva_comision_applus = ec_iva_comision_applus;
    }

    /**
     * @return the ec_iva_comision_factoring_fintra
     */
    public double getEc_iva_comision_factoring_fintra() {
        return ec_iva_comision_factoring_fintra;
    }

    /**
     * @param ec_iva_comision_factoring_fintra the ec_iva_comision_factoring_fintra to set
     */
    public void setEc_iva_comision_factoring_fintra(double ec_iva_comision_factoring_fintra) {
        this.ec_iva_comision_factoring_fintra = ec_iva_comision_factoring_fintra;
    }

    /**
     * @return the ec_iva_comision_fintra
     */
    public double getEc_iva_comision_fintra() {
        return ec_iva_comision_fintra;
    }

    /**
     * @param ec_iva_comision_fintra the ec_iva_comision_fintra to set
     */
    public void setEc_iva_comision_fintra(double ec_iva_comision_fintra) {
        this.ec_iva_comision_fintra = ec_iva_comision_fintra;
    }

    /**
     * @return the ec_iva_comision_provintegral
     */
    public double getEc_iva_comision_provintegral() {
        return ec_iva_comision_provintegral;
    }

    /**
     * @param ec_iva_comision_provintegral the ec_iva_comision_provintegral to set
     */
    public void setEc_iva_comision_provintegral(double ec_iva_comision_provintegral) {
        this.ec_iva_comision_provintegral = ec_iva_comision_provintegral;
    }

    /**
     * @return the ec_iva_comision_eca
     */
    public double getEc_iva_comision_eca() {
        return ec_iva_comision_eca;
    }

    /**
     * @param ec_iva_comision_eca the ec_iva_comision_eca to set
     */
    public void setEc_iva_comision_eca(double ec_iva_comision_eca) {
        this.ec_iva_comision_eca = ec_iva_comision_eca;
    }

    /**
     * @return the ec_financiacion_fintra
     */
    public double getEc_financiacion_fintra() {
        return ec_financiacion_fintra;
    }

    /**
     * @param ec_financiacion_fintra the ec_financiacion_fintra to set
     */
    public void setEc_financiacion_fintra(double ec_financiacion_fintra) {
        this.ec_financiacion_fintra = ec_financiacion_fintra;
    }

    /**
     * @return the ec_cuota_pago
     */
    public double getEc_cuota_pago() {
        return ec_cuota_pago;
    }

    /**
     * @param ec_cuota_pago the ec_cuota_pago to set
     */
    public void setEc_cuota_pago(double ec_cuota_pago) {
        this.ec_cuota_pago = ec_cuota_pago;
    }

    /**
     * @return the ec_total_financiacion
     */
    public double getEc_total_financiacion() {
        return ec_total_financiacion;
    }

    /**
     * @param ec_total_financiacion the ec_total_financiacion to set
     */
    public void setEc_total_financiacion(double ec_total_financiacion) {
        this.ec_total_financiacion = ec_total_financiacion;
    }

    /**
     * @return the total_prev1_applus
     */
    public double getTotal_prev1_applus() {
        return total_prev1_applus;
    }

    /**
     * @param total_prev1_applus the total_prev1_applus to set
     */
    public void setTotal_prev1_applus(double total_prev1_applus) {
        this.total_prev1_applus = total_prev1_applus;
    }

    public String  getId_contratista() {//090715
        return id_contratista;
    }
    public void setId_contratista(String x1) {
        this.id_contratista = x1;
    }
    public String  getNombre_contratista() {//090715
        return nombre_contratista;
    }
    public void setNombre_contratista(String x1) {
        this.nombre_contratista = x1;
    }


    public double getIvaBonificacion() {
        return iva_bonificacion;
    }

    public void setIvaBonificacion ( double iva_Bonificacion){
        this.iva_bonificacion = iva_Bonificacion;
    }



}