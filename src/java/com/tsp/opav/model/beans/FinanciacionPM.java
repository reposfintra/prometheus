/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.opav.model.beans;


import java.util.*;



/**
 *
 * @author Alvaro
 */
public class FinanciacionPM  {

    private List   facturasPM;
    private List   valorCapital;
    private List   valorInteres;
    private List   simboloVariable;
    private List   multiservicio;
    private List   valorIPM;
    private List   porcentajePM;
    private List   diasAdicionales;
    private List   valoresAdicionales;
    
    private String documento;
    private String numeracion;
    private String nic;
    private String nombreCliente;
    private String nit;
    private String codcli;
    private String fechaFactura;
    private String fechaVencimiento;
    private String descripcion;

    private int    numeroCuotas;
    private double tasaInteres;
    private double tasaMora;
    private double tasaMaxima;
    private double cuotaInicial;    
    
    private double vlrTotalRefinanciar;                             // Valor total capital a refinanciar
    private double vlrTotalRefinanciarPorCuota;
    private double vlrTotalRefinanciarUltimaCuota;
    
    
    
    
    private double vlrTotalInteresesPendientes;
    private double vlrTotalMora;
    private double vlrMoraPorCuota;
    private double vlrMoraUltimaCuota;
    
    private double vlrAdicionalCuota;
    private double vlrAdicionalUltimaCuota;
    
    
    
    
    
    private double nuevaCuota;                                      // Valor de la mensualidad: Incluye una parte de capital y una parte de intereses
    
    private double totalCuotas;                                     // Valor de nuevaCuota * numeroCuotas : Incluye valor total capital y valor total intereses
    private double total_intereses;                                 // Valor de totalCuotas - vlrTotalRefinanciar
    
    private double interesPorCuota;                 
    private double capitalPorCuota;                                 // Calculado por: nuevaCuota - interesPorCuota;
    private double vlrInteresesPendientesPorCuota;                  // Calculado por:  Util.redondear2(vlrTotalInteresesPendientes / numeroCuotas, 0);    
    
    private double interesPorCuotaUltimaFactura;                    // Calculado por:  totalIntereses - interesPorCuota * (numeroCuotas - 1) ;
    private double capitalPorCuotaUltimaFactura;                    // Calculado por:  vlrTotalRefinanciar - capitalPorCuota * (numeroCuotas - 1) ;
    private double vlrInteresesPendientesPorCuotaUltimaFactura;     // Calculado por:  vlrTotalInteresesPendientes - vlrInteresesPendientesPorCuota * (numeroCuotas - 1);
    
    private double valorIva;
    private double valorIvaPorCuota;
    private double valorIvaUltimaCuota;
    private double valorINM;
    private double vlrTotalAdicional;
    private double vlrDiferenciaIntereses ;
    
    private String simboloVariableFactura;
    private String simboloVariableCuota;
    private String observaciones;
    
    

    

    public FinanciacionPM () {

        this.facturasPM         = new LinkedList();
        this.simboloVariable    = new LinkedList();
        this.multiservicio      = new LinkedList();
        this.valorCapital       = new LinkedList();
        this.valorInteres       = new LinkedList();
        this.valorIPM           = new LinkedList();
        this.porcentajePM       = new LinkedList();
        this.diasAdicionales    = new LinkedList();
        this.valoresAdicionales = new LinkedList();        
        
        
        this.capitalPorCuota = 0.00;
        this.capitalPorCuotaUltimaFactura = 0.00;
        this.codcli = "";
        this.cuotaInicial = 0.00;
        this.descripcion = "";
        this.documento = "";
        this.fechaFactura = "";
        this.fechaVencimiento = "";
        this.interesPorCuota = 0.00;
        this.interesPorCuotaUltimaFactura = 0.00;
        this.nic = "";
        this.nit = "";
        this.nombreCliente = "";
        this.nuevaCuota = 0.00;
        this.numeracion = "";
        this.numeroCuotas = 0;
        this.tasaInteres = 0.00;
        this.tasaMora = 0.00;
        this.tasaMaxima = 0.00;
        this.totalCuotas = 0.00;
        this.total_intereses = 0.00;
        this.vlrInteresesPendientesPorCuota = 0.00;
        this.vlrInteresesPendientesPorCuotaUltimaFactura = 0.00;
        this.vlrTotalInteresesPendientes = 0.00;
        this.vlrTotalMora = 0.00;
        this.vlrTotalRefinanciar = 0.00;
        this.vlrTotalRefinanciarPorCuota = 0.00;
        this.vlrTotalRefinanciarUltimaCuota = 0.00;
        
        this.valorIva = 0.00;
        this.valorIvaPorCuota = 0.00;
        this.valorIvaUltimaCuota = 0.00;
        this.valorINM = 0.00;
        
        this.vlrMoraPorCuota = 0.00;
        this.vlrMoraUltimaCuota = 0.00;
        this.vlrAdicionalCuota = 0.00;
        this.vlrAdicionalUltimaCuota = 0.00;
        this.vlrTotalAdicional = 0.00;
        this.vlrDiferenciaIntereses = 0.00;
        
        
        this.simboloVariableFactura = "";
        this.simboloVariableCuota = "";
        this.observaciones = "";

    }    
    
    
    
    
    /**
     * @return the facturasPM
     */
    public List getFacturasPM() {
        return facturasPM;
    }

    /**
     * @param facturasPM the facturasPM to set
     */
    public void setFacturasPM(String facturaPM) {
        this.facturasPM.add(facturaPM); 
    }
        
        
    public void removeListaFacturasPM () {
        this.facturasPM.clear();
    }

    
    
    /**
     * @return the simboloVariable
     */
    public List getSimboloVariable() {
        return simboloVariable;
    }

    /**
     * @param simboloVariable the simboloVariable to set
     */
    public void setSimboloVariable(String simboloVariable) {
        this.simboloVariable.add(simboloVariable); 
    }
    
    public void removeListaSimboloVariable () {
        this.simboloVariable.clear();
    }      
    


    /**
     * @return the valorCapital
     */
    public List getValorCapital() {
        return valorCapital;
    }

    /**
     * @param valorCapital the valorCapital to set
     */
    public void setValorCapital(double valorCapital) {
        this.valorCapital.add(valorCapital);
    }

    public void removeListaValorCapital () {
        this.valorCapital.clear();
    }      
    
    
    
    
    
    
    /**
     * @return the valorInteres
     */
    public List getValorInteres() {
        return valorInteres;
    }

    /**
     * @param valorInteres the valorInteres to set
     */
    public void setValorInteres(double valorInteres) {
        this.valorInteres.add(valorInteres);
    }
                       
    public void removeListaValorInteres() {
        this.valorInteres.clear();
    }
    
    
    

    /**
     * @return the multiservicio
     */
    public List getMultiservicio() {
        return multiservicio;
    }

    
    
    
    /**
     * @param multiservicio the multiservicio to set
     */
    public void setMultiservicio(String multiservicio) {
        this.multiservicio.add(multiservicio); 
    }

    public void removeListaMultiservicio () {
        this.multiservicio.clear();
    }   
    
    
    /**
     * @return the nic
     */
    public String getNic() {
        return nic;
    }

    /**
     * @param nic the nic to set
     */
    public void setNic(String nic) {
        this.nic = nic;
    }

    /**
     * @return the nombreCliente
     */
    public String getNombreCliente() {
        return nombreCliente;
    }

    /**
     * @param nombreCliente the nombreCliente to set
     */
    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    /**
     * @return the nit
     */
    public String getNit() {
        return nit;
    }

    /**
     * @param nit the nit to set
     */
    public void setNit(String nit) {
        this.nit = nit;
    }

    /**
     * @return the codcli
     */
    public String getCodcli() {
        return codcli;
    }

    /**
     * @param codcli the codcli to set
     */
    public void setCodcli(String codcli) {
        this.codcli = codcli;
    }

    /**
     * @return the fechaFactura
     */
    public String getFechaFactura() {
        return fechaFactura;
    }

    /**
     * @param fechaFactura the fechaFactura to set
     */
    public void setFechaFactura(String fechaFactura) {
        this.fechaFactura = fechaFactura;
    }

    /**
     * @return the fechaVencimiento
     */
    public String getFechaVencimiento() {
        return fechaVencimiento;
    }

    /**
     * @param fechaVencimiento the fechaVencimiento to set
     */
    public void setFechaVencimiento(String fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    /**
     * @return the numeroCuotas
     */
    public int getNumeroCuotas() {
        return numeroCuotas;
    }

    /**
     * @param numeroCuotas the numeroCuotas to set
     */
    public void setNumeroCuotas(int numeroCuotas) {
        this.numeroCuotas = numeroCuotas;
    }

    /**
     * @return the tasaInteres
     */
    public double getTasaInteres() {
        return tasaInteres;
    }

    /**
     * @param tasaInteres the tasaInteres to set
     */
    public void setTasaInteres(double tasaInteres) {
        this.tasaInteres = tasaInteres;
    }

    /**
     * @return the tasaMora
     */
    public double getTasaMora() {
        return tasaMora;
    }

    /**
     * @param tasaMora the tasaMora to set
     */
    public void setTasaMora(double tasaMora) {
        this.tasaMora = tasaMora;
    }

    /**
     * @return the cuotaInicial
     */
    public double getCuotaInicial() {
        return cuotaInicial;
    }

    /**
     * @param cuotaInicial the cuotaInicial to set
     */
    public void setCuotaInicial(double cuotaInicial) {
        this.cuotaInicial = cuotaInicial;
    }

    /**
     * @return the vlrTotalRefinanciar
     */
    public double getVlrTotalRefinanciar() {
        return vlrTotalRefinanciar;
    }

    /**
     * @param vlrTotalRefinanciar the vlrTotalRefinanciar to set
     */
    public void setVlrTotalRefinanciar(double vlrTotalRefinanciar) {
        this.vlrTotalRefinanciar = vlrTotalRefinanciar;
    }

    /**
     * @return the vlrTotalInteresesPendientes
     */
    public double getVlrTotalInteresesPendientes() {
        return vlrTotalInteresesPendientes;
    }

    /**
     * @param vlrTotalInteresesPendientes the vlrTotalInteresesPendientes to set
     */
    public void setVlrTotalInteresesPendientes(double vlrTotalInteresesPendientes) {
        this.vlrTotalInteresesPendientes = vlrTotalInteresesPendientes;
    }

    /**
     * @return the vlrTotalMora
     */
    public double getVlrTotalMora() {
        return vlrTotalMora;
    }

    /**
     * @param vlrTotalMora the vlrTotalMora to set
     */
    public void setVlrTotalMora(double vlrTotalMora) {
        this.vlrTotalMora = vlrTotalMora;
    }

    /**
     * @return the nuevaCuota
     */
    public double getNuevaCuota() {
        return nuevaCuota;
    }

    /**
     * @param nuevaCuota the nuevaCuota to set
     */
    public void setNuevaCuota(double nuevaCuota) {
        this.nuevaCuota = nuevaCuota;
    }

    /**
     * @return the totalCuotas
     */
    public double getTotalCuotas() {
        return totalCuotas;
    }

    /**
     * @param totalCuotas the totalCuotas to set
     */
    public void setTotalCuotas(double totalCuotas) {
        this.totalCuotas = totalCuotas;
    }

    /**
     * @return the total_intereses
     */
    public double getTotal_intereses() {
        return total_intereses;
    }

    /**
     * @param total_intereses the total_intereses to set
     */
    public void setTotal_intereses(double total_intereses) {
        this.total_intereses = total_intereses;
    }

    /**
     * @return the interesPorCuota
     */
    public double getInteresPorCuota() {
        return interesPorCuota;
    }

    /**
     * @param interesPorCuota the interesPorCuota to set
     */
    public void setInteresPorCuota(double interesPorCuota) {
        this.interesPorCuota = interesPorCuota;
    }

    /**
     * @return the capitalPorCuota
     */
    public double getCapitalPorCuota() {
        return capitalPorCuota;
    }

    /**
     * @param capitalPorCuota the capitalPorCuota to set
     */
    public void setCapitalPorCuota(double capitalPorCuota) {
        this.capitalPorCuota = capitalPorCuota;
    }

    /**
     * @return the vlrInteresesPendientesPorCuota
     */
    public double getVlrInteresesPendientesPorCuota() {
        return vlrInteresesPendientesPorCuota;
    }

    /**
     * @param vlrInteresesPendientesPorCuota the vlrInteresesPendientesPorCuota to set
     */
    public void setVlrInteresesPendientesPorCuota(double vlrInteresesPendientesPorCuota) {
        this.vlrInteresesPendientesPorCuota = vlrInteresesPendientesPorCuota;
    }

    /**
     * @return the interesPorCuotaUltimaFactura
     */
    public double getInteresPorCuotaUltimaFactura() {
        return interesPorCuotaUltimaFactura;
    }

    /**
     * @param interesPorCuotaUltimaFactura the interesPorCuotaUltimaFactura to set
     */
    public void setInteresPorCuotaUltimaFactura(double interesPorCuotaUltimaFactura) {
        this.interesPorCuotaUltimaFactura = interesPorCuotaUltimaFactura;
    }

    /**
     * @return the capitalPorCuotaUltimaFactura
     */
    public double getCapitalPorCuotaUltimaFactura() {
        return capitalPorCuotaUltimaFactura;
    }

    /**
     * @param capitalPorCuotaUltimaFactura the capitalPorCuotaUltimaFactura to set
     */
    public void setCapitalPorCuotaUltimaFactura(double capitalPorCuotaUltimaFactura) {
        this.capitalPorCuotaUltimaFactura = capitalPorCuotaUltimaFactura;
    }

    /**
     * @return the vlrInteresesPendientesPorCuotaUltimaFactura
     */
    public double getVlrInteresesPendientesPorCuotaUltimaFactura() {
        return vlrInteresesPendientesPorCuotaUltimaFactura;
    }

    /**
     * @param vlrInteresesPendientesPorCuotaUltimaFactura the vlrInteresesPendientesPorCuotaUltimaFactura to set
     */
    public void setVlrInteresesPendientesPorCuotaUltimaFactura(double vlrInteresesPendientesPorCuotaUltimaFactura) {
        this.vlrInteresesPendientesPorCuotaUltimaFactura = vlrInteresesPendientesPorCuotaUltimaFactura;
    }

    /**
     * @return the descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * @param descripcion the descripcion to set
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * @return the documento
     */
    public String getDocumento() {
        return documento;
    }

    /**
     * @param documento the documento to set
     */
    public void setDocumento(String documento) {
        this.documento = documento;
    }
    

    /**
     * @return the numeracion
     */
    public String getNumeracion() {
        return numeracion;
    }

    /**
     * @param numeracion the documento to set
     */
    public void setNumeracion(String numeracion) {
        this.numeracion = numeracion;
    }

    /**
     * @return the tasaMaxima
     */
    public double getTasaMaxima() {
        return tasaMaxima;
    }

    /**
     * @param tasaMaxima the tasaMaxima to set
     */
    public void setTasaMaxima(double tasaMaxima) {
        this.tasaMaxima = tasaMaxima;
    }

    /**
     * @return the valorIva
     */
    public double getValorIva() {
        return valorIva;
    }

    /**
     * @param valorIva the valorIva to set
     */
    public void setValorIva(double valorIva) {
        this.valorIva = valorIva;
    }

    /**
     * @return the valorIvaPorCuota
     */
    public double getValorIvaPorCuota() {
        return valorIvaPorCuota;
    }

    /**
     * @param valorIvaPorCuota the valorIvaPorCuota to set
     */
    public void setValorIvaPorCuota(double valorIvaPorCuota) {
        this.valorIvaPorCuota = valorIvaPorCuota;
    }

    /**
     * @return the valorIvaUltimaCuota
     */
    public double getValorIvaUltimaCuota() {
        return valorIvaUltimaCuota;
    }

    /**
     * @param valorIvaUltimaCuota the valorIvaUltimaCuota to set
     */
    public void setValorIvaUltimaCuota(double valorIvaUltimaCuota) {
        this.valorIvaUltimaCuota = valorIvaUltimaCuota;
    }

    /**
     * @return the valorINM
     */
    public double getValorINM() {
        return valorINM;
    }

    /**
     * @param valorINM the valorINM to set
     */
    public void setValorINM(double valorINM) {
        this.valorINM = valorINM;
    }

    /**
     * @return the valorIPM
     */
    public List getValorIPM() {
        return valorIPM;
    }

    /**
     * @param valorIPM the porcentajePM to set
     */
    public void setValorIPM(double valorIPM) {
        this.valorIPM.add(valorIPM) ;
    }


    public void removeListaValorIPM () {
        this.valorIPM.clear();
    }

    /**
     * @return the porcentajePM
     */
    public List getPorcentajePM() {
        return porcentajePM;
    }

    /**
     * @param porcentajePM the porcentajePM to set
     */
    public void setPorcentajePM(double porcentajePM) {
        this.porcentajePM.add(porcentajePM);
    }
    
    
    public void removeListaPorcentajePM () {
        this.porcentajePM.clear();
    }

    
    
    
    
    
    /**
     * @return the diasAdicionales
     */
    public List getDiasAdicionales() {
        return diasAdicionales;
    }

    /**
     * @param diasAdicionales the diasAdicionales to set
     */
    public void setDiasAdicionales(int diasAdicionales) {
        this.diasAdicionales.add(diasAdicionales);
    }

    public void removeListaDiasAdicionales () {
        this.diasAdicionales.clear();
    }
    
    
    
    
    /**
     * @return the valoresAdicionales
     */
    public List getValoresAdicionales() {
        return valoresAdicionales;
    }

    /**
     * @param valoresAdicionales the valoresAdicionales to set
     */
    public void setValoresAdicionales(double valoresAdicionales) {
        this.valoresAdicionales.add(valoresAdicionales);
    }
    
    public void removeListaValoresAdicionales () {
        this.valoresAdicionales.clear();
    }

    /**
     * @return the vlrMoraPorCuota
     */
    public double getVlrMoraPorCuota() {
        return vlrMoraPorCuota;
    }

    /**
     * @param vlrMoraPorCuota the vlrMoraPorCuota to set
     */
    public void setVlrMoraPorCuota(double vlrMoraPorCuota) {
        this.vlrMoraPorCuota = vlrMoraPorCuota;
    }

    /**
     * @return the vlrMoraUltimaCuota
     */
    public double getVlrMoraUltimaCuota() {
        return vlrMoraUltimaCuota;
    }

    /**
     * @param vlrMoraUltimaCuota the vlrMoraUltimaCuota to set
     */
    public void setVlrMoraUltimaCuota(double vlrMoraUltimaCuota) {
        this.vlrMoraUltimaCuota = vlrMoraUltimaCuota;
    }

    /**
     * @return the vlrTotalRefinanciarPorCuota
     */
    public double getVlrTotalRefinanciarPorCuota() {
        return vlrTotalRefinanciarPorCuota;
    }

    /**
     * @param vlrTotalRefinanciarPorCuota the vlrTotalRefinanciarPorCuota to set
     */
    public void setVlrTotalRefinanciarPorCuota(double vlrTotalRefinanciarPorCuota) {
        this.vlrTotalRefinanciarPorCuota = vlrTotalRefinanciarPorCuota;
    }

    /**
     * @return the vlrTotalRefinanciarUltimaCuota
     */
    public double getVlrTotalRefinanciarUltimaCuota() {
        return vlrTotalRefinanciarUltimaCuota;
    }

    /**
     * @param vlrTotalRefinanciarUltimaCuota the vlrTotalRefinanciarUltimaCuota to set
     */
    public void setVlrTotalRefinanciarUltimaCuota(double vlrTotalRefinanciarUltimaCuota) {
        this.vlrTotalRefinanciarUltimaCuota = vlrTotalRefinanciarUltimaCuota;
    }

    /**
     * @return the vlrAdicionalCuota
     */
    public double getVlrAdicionalCuota() {
        return vlrAdicionalCuota;
    }

    /**
     * @param vlrAdicionalCuota the vlrAdicionalCuota to set
     */
    public void setVlrAdicionalCuota(double vlrAdicionalCuota) {
        this.vlrAdicionalCuota = vlrAdicionalCuota;
    }

    /**
     * @return the vlrAdicionalUltimaCuota
     */
    public double getVlrAdicionalUltimaCuota() {
        return vlrAdicionalUltimaCuota;
    }

    /**
     * @param vlrAdicionalUltimaCuota the vlrAdicionalUltimaCuota to set
     */
    public void setVlrAdicionalUltimaCuota(double vlrAdicionalUltimaCuota) {
        this.vlrAdicionalUltimaCuota = vlrAdicionalUltimaCuota;
    }

    /**
     * @return the vlrTotalAdicional
     */
    public double getVlrTotalAdicional() {
        return vlrTotalAdicional;
    }

    /**
     * @param vlrTotalAdicional the vlrTotalAdicional to set
     */
    public void setVlrTotalAdicional(double vlrTotalAdicional) {
        this.vlrTotalAdicional = vlrTotalAdicional;
    }

    /**
     * @return the vlrDiferenciaIntereses
     */
    public double getVlrDiferenciaIntereses() {
        return vlrDiferenciaIntereses;
    }

    /**
     * @param vlrDiferenciaIntereses the vlrDiferenciaIntereses to set
     */
    public void setVlrDiferenciaIntereses(double vlrDiferenciaIntereses) {
        this.vlrDiferenciaIntereses = vlrDiferenciaIntereses;
    }

    /**
     * @return the simboloVariableFactura
     */
    public String getSimboloVariableFactura() {
        return simboloVariableFactura;
    }

    /**
     * @param simboloVariableFactura the simboloVariableFactura to set
     */
    public void setSimboloVariableFactura(String simboloVariableFactura) {
        this.simboloVariableFactura = simboloVariableFactura;
    }

    /**
     * @return the simboloVariableCuota
     */
    public String getSimboloVariableCuota() {
        return simboloVariableCuota;
    }

    /**
     * @param simboloVariableCuota the simboloVariableCuota to set
     */
    public void setSimboloVariableCuota(String simboloVariableCuota) {
        this.simboloVariableCuota = simboloVariableCuota;
    }

    /**
     * @return the observaciones
     */
    public String getObservaciones() {
        return observaciones;
    }

    /**
     * @param observaciones the observaciones to set
     */
    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }
    

}
