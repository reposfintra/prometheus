/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.opav.model.beans;

/**
 *
 * @author Alvaro
 */
public class SolicitudPorFacturar {



  private String num_os;
  private String id_solicitud;
  private int    parcial;
  private String fecha_entrega_oferta;
  private String id_cliente;
  private String nic;
  private String nit;
  private String nombre;
  private String tipo_distribucion;
  private int    no_es_valoragregado;
  private String es_oficial;
  private String consecutivo_oferta;
  private String fecha_oferta;
  private String regulacion;
  private String tipo_cliente;
  private double dtf_semana;
  private double porcentaje_mora;
  private String tipo;

        public String getTipo() {
           return tipo;
        }

        public void setTipo(String tipo) {
            this.tipo = tipo;
        }

	



    /** Creates a new instance of SolicitudPorFacturar */
    public SolicitudPorFacturar() {
    }


    /** Extrae un registro de la BD correspondientes a solicitudes que no estan facturadas */
    public static SolicitudPorFacturar load(java.sql.ResultSet rs)throws java.sql.SQLException{

        SolicitudPorFacturar solicitudPorFacturar = new SolicitudPorFacturar();

        solicitudPorFacturar.setNum_os( rs.getString("num_os") );
        solicitudPorFacturar.setId_solicitud( rs.getString("id_solicitud") );
        solicitudPorFacturar.setParcial( rs.getInt("parcial") );
        solicitudPorFacturar.setFecha_entrega_oferta(rs.getString("fecha_entrega_oferta") );
        solicitudPorFacturar.setId_cliente(rs.getString("id_cliente") );
        solicitudPorFacturar.setNic(rs.getString("nic") );
        solicitudPorFacturar.setNit(rs.getString("nit") );
        solicitudPorFacturar.setNombre(rs.getString("nombre") );
        solicitudPorFacturar.setTipo_distribucion(rs.getString("tipo_distribucion") );
        solicitudPorFacturar.setNo_es_valoragregado(rs.getInt("no_es_valoragregado") );
        solicitudPorFacturar.setEs_oficial(rs.getString("es_oficial") );
        solicitudPorFacturar.setConsecutivo_oferta( rs.getString("consecutivo_oferta") );
        solicitudPorFacturar.setFecha_oferta( rs.getString("fecha_oferta") );
        solicitudPorFacturar.setRegulacion( rs.getString("regulacion") );
        solicitudPorFacturar.setTipo_cliente( rs.getString("tipo_cliente") );
        solicitudPorFacturar.setDtf_semana( rs.getDouble("dtf_semana") );
        solicitudPorFacturar.setPorcentaje_mora( rs.getDouble("porcentaje_mora") );
        solicitudPorFacturar.setTipo(rs.getString("tipo") );

        return solicitudPorFacturar;

    }



    /**
     * @return the num_os
     */
    public String getNum_os() {
        return num_os;
    }

    /**
     * @param num_os the num_os to set
     */
    public void setNum_os(String num_os) {
        this.num_os = num_os;
    }

    /**
     * @return the id_solicitud
     */
    public String getId_solicitud() {
        return id_solicitud;
    }

    /**
     * @param id_solicitud the id_solicitud to set
     */
    public void setId_solicitud(String id_solicitud) {
        this.id_solicitud = id_solicitud;
    }

    /**
     * @return the fecha_entrega_oferta
     */
    public String getFecha_entrega_oferta() {
        return fecha_entrega_oferta;
    }

    /**
     * @param fecha_entrega_oferta the fecha_entrega_oferta to set
     */
    public void setFecha_entrega_oferta(String fecha_entrega_oferta) {
        this.fecha_entrega_oferta = fecha_entrega_oferta;
    }

    /**
     * @return the id_cliente
     */
    public String getId_cliente() {
        return id_cliente;
    }

    /**
     * @param id_cliente the id_cliente to set
     */
    public void setId_cliente(String id_cliente) {
        this.id_cliente = id_cliente;
    }

    /**
     * @return the nic
     */
    public String getNic() {
        return nic;
    }

    /**
     * @param nic the nic to set
     */
    public void setNic(String nic) {
        this.nic = nic;
    }

    /**
     * @return the nit
     */
    public String getNit() {
        return nit;
    }

    /**
     * @param nit the nit to set
     */
    public void setNit(String nit) {
        this.nit = nit;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the tipo_distribucion
     */
    public String getTipo_distribucion() {
        return tipo_distribucion;
    }

    /**
     * @param tipo_distribucion the tipo_distribucion to set
     */
    public void setTipo_distribucion(String tipo_distribucion) {
        this.tipo_distribucion = tipo_distribucion;
    }

    /**
     * @return the no_es_valoragregado
     */
    public int getNo_es_valoragregado() {
        return no_es_valoragregado;
    }

    /**
     * @param no_es_valoragregado the no_es_valoragregado to set
     */
    public void setNo_es_valoragregado(int no_es_valoragregado) {
        this.no_es_valoragregado = no_es_valoragregado;
    }

    /**
     * @return the es_oficial
     */
    public String getEs_oficial() {
        return es_oficial;
    }

    /**
     * @param es_oficial the es_oficial to set
     */
    public void setEs_oficial(String es_oficial) {
        this.es_oficial = es_oficial;
    }

    /**
     * @return the consecutivo_oferta
     */
    public String getConsecutivo_oferta() {
        return consecutivo_oferta;
    }

    /**
     * @param consecutivo_oferta the consecutivo_oferta to set
     */
    public void setConsecutivo_oferta(String consecutivo_oferta) {
        this.consecutivo_oferta = consecutivo_oferta;
    }

    /**
     * @return the fecha_oferta
     */
    public String getFecha_oferta() {
        return fecha_oferta;
    }

    /**
     * @param fecha_oferta the fecha_oferta to set
     */
    public void setFecha_oferta(String fecha_oferta) {
        this.fecha_oferta = fecha_oferta;
    }

    /**
     * @return the regulacion
     */
    public String getRegulacion() {
        return regulacion;
    }

    /**
     * @param regulacion the regulacion to set
     */
    public void setRegulacion(String regulacion) {
        this.regulacion = regulacion;
    }

    /**
     * @return the dtf_semana
     */
    public double getDtf_semana() {
        return dtf_semana;
    }

    /**
     * @param dtf_semana the dtf_semana to set
     */
    public void setDtf_semana(double dtf_semana) {
        this.dtf_semana = dtf_semana;
    }

    /**
     * @return the parcial
     */
    public int getParcial() {
        return parcial;
    }

    /**
     * @param parcial the parcial to set
     */
    public void setParcial(int parcial) {
        this.parcial = parcial;
    }

    /**
     * @return the tipo_cliente
     */
    public String getTipo_cliente() {
        return tipo_cliente;
    }

    /**
     * @param tipo_cliente the tipo_cliente to set
     */
    public void setTipo_cliente(String tipo_cliente) {
        this.tipo_cliente = tipo_cliente;
    }

    /**
     * @return the porcentaje_mora
     */
    public double getPorcentaje_mora() {
        return porcentaje_mora;
    }

    /**
     * @param porcentaje_mora the porcentaje_mora to set
     */
    public void setPorcentaje_mora(double porcentaje_mora) {
        this.porcentaje_mora = porcentaje_mora;
    }


}