/*
 * To  change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.opav.model.beans;

/**
 *
 * @author Alvaro
 */
public class AccionContratista {


  private String contratista ;
  private String id_solicitud;
  private int    parcial;
  private String id_accion;
  private String id_cliente;
  private String nombre;
  private String direccion;
  private String ciudad;
  private String descripcion;
  private double costo_contratista;
  private double material;
  private double mano_obra;
  private double otros;
  private double bonificacion;
  private double fintra;
  private double administracion;
  private double imprevisto;
  private double utilidad;
  private String num_os;
  private double iva_bonificacion;
  private int    periodo;
  private double porcentaje_factoring;
  private double porcentaje_formula;
  private String estudio_economico;
  private double base_iva_contratista;
  private double iva_contratista;
  private String factura_cliente;
  private String nit ;
  private String valor_agregado;


    /** Creates a new instance of Prefactura */
    public AccionContratista() {
    }


    public static AccionContratista load(java.sql.ResultSet rs,int tipo)throws java.sql.SQLException{

        AccionContratista accionContratista = new AccionContratista();

        accionContratista.setContratista( rs.getString("contratista") );
        accionContratista.setId_solicitud(rs.getString("id_solicitud") );
        accionContratista.setParcial( rs.getInt("parcial") );
        accionContratista.setId_accion( rs.getString("id_accion") );
        accionContratista.setId_cliente( rs.getString("id_cliente") );
        accionContratista.setNombre( rs.getString("nombre") );
        accionContratista.setDireccion( rs.getString("direccion") );
        accionContratista.setCiudad( rs.getString("ciudad") );
        accionContratista.setDescripcion( rs.getString("descripcion") );
        accionContratista.setCosto_contratista( rs.getDouble("costo_contratista") );
        accionContratista.setMaterial( rs.getDouble("material") );
        accionContratista.setMano_obra( rs.getDouble("mano_obra") );
        accionContratista.setOtros( rs.getDouble("otros") );
        accionContratista.setBonificacion(rs.getDouble("bonificacion"));
        accionContratista.setFintra(rs.getDouble("fintra"));
        accionContratista.setAdministracion(rs.getDouble("administracion"));
        accionContratista.setImprevisto(rs.getDouble("imprevisto"));
        accionContratista.setUtilidad(rs.getDouble("utilidad"));
        accionContratista.setNum_os( rs.getString("num_os") );
        accionContratista.setIva_bonificacion(rs.getDouble("iva_bonificacion"));
        accionContratista.setPeriodo(rs.getInt("periodo"));
        accionContratista.setPorcentaje_factoring(rs.getString("porcentaje_factoring"));
        accionContratista.setPorcentaje_formula(rs.getDouble("porcentaje_formula"));
        accionContratista.setEstudio_economico( rs.getString("estudio_economico") );
        accionContratista.setBase_iva_contratista(rs.getDouble("base_iva_contratista"));
        accionContratista.setIva_contratista(rs.getDouble("iva_contratista"));
        if (tipo == 1) {
            accionContratista.setFactura_cliente(rs.getString("factura_cliente"));
            accionContratista.setNit(rs.getString("nit"));
        }
        accionContratista.setValor_agregado(rs.getString("valor_agregado"));

        return accionContratista;
    }

    /**
     * @return the contratista
     */
    public String getContratista() {
        return contratista;
    }

    /**
     * @param contratista the contratista to set
     */
    public void setContratista(String contratista) {
        this.contratista = contratista;
    }

    /**
     * @return the id_solicitud
     */
    public String getId_solicitud() {
        return id_solicitud;
    }

    /**
     * @param id_solicitud the id_solicitud to set
     */
    public void setId_solicitud(String id_solicitud) {
        this.id_solicitud = id_solicitud;
    }

    /**
     * @return the parcial
     */
    public int getParcial() {
        return parcial;
    }

    /**
     * @param parcial the parcial to set
     */
    public void setParcial(int parcial) {
        this.parcial = parcial;
    }

    /**
     * @return the id_accion
     */
    public String getId_accion() {
        return id_accion;
    }

    /**
     * @param id_accion the id_accion to set
     */
    public void setId_accion(String id_accion) {
        this.id_accion = id_accion;
    }

    /**
     * @return the id_cliente
     */
    public String getId_cliente() {
        return id_cliente;
    }

    /**
     * @param id_cliente the id_cliente to set
     */
    public void setId_cliente(String id_cliente) {
        this.id_cliente = id_cliente;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the direccion
     */
    public String getDireccion() {
        return direccion;
    }

    /**
     * @param direccion the direccion to set
     */
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    /**
     * @return the ciudad
     */
    public String getCiudad() {
        return ciudad;
    }

    /**
     * @param ciudad the ciudad to set
     */
    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    /**
     * @return the descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * @param descripcion the descripcion to set
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * @return the total_contratista
     */
    public double getCosto_contratista() {
        return costo_contratista;
    }

    /**
     * @param total_contratista the total_contratista to set
     */
    public void setCosto_contratista(double costo_contratista) {
        this.costo_contratista = costo_contratista;
    }

    /**
     * @return the material
     */
    public double getMaterial() {
        return material;
    }

    /**
     * @param material the material to set
     */
    public void setMaterial(double material) {
        this.material = material;
    }

    /**
     * @return the mano_obra
     */
    public double getMano_obra() {
        return mano_obra;
    }

    /**
     * @param mano_obra the mano_obra to set
     */
    public void setMano_obra(double mano_obra) {
        this.mano_obra = mano_obra;
    }

    /**
     * @return the otros
     */
    public double getOtros() {
        return otros;
    }

    /**
     * @param otros the otros to set
     */
    public void setOtros(double otros) {
        this.otros = otros;
    }

    /**
     * @return the bonificacion
     */
    public double getBonificacion() {
        return bonificacion;
    }

    /**
     * @param bonificacion the bonificacion to set
     */
    public void setBonificacion(double bonificacion) {
        this.bonificacion = bonificacion;
    }

    /**
     * @return the fintra
     */
    public double getFintra() {
        return fintra;
    }

    /**
     * @param fintra the fintra to set
     */
    public void setFintra(double fintra) {
        this.fintra = fintra;
    }

    /**
     * @return the administracion
     */
    public double getAdministracion() {
        return administracion;
    }

    /**
     * @param administracion the administracion to set
     */
    public void setAdministracion(double administracion) {
        this.administracion = administracion;
    }

    /**
     * @return the imprevisto
     */
    public double getImprevisto() {
        return imprevisto;
    }

    /**
     * @param imprevisto the imprevisto to set
     */
    public void setImprevisto(double imprevisto) {
        this.imprevisto = imprevisto;
    }

    /**
     * @return the utilidad
     */
    public double getUtilidad() {
        return utilidad;
    }

    /**
     * @param utilidad the utilidad to set
     */
    public void setUtilidad(double utilidad) {
        this.utilidad = utilidad;
    }

    /**
     * @return the num_os
     */
    public String getNum_os() {
        return num_os;
    }

    /**
     * @param num_os the num_os to set
     */
    public void setNum_os(String num_os) {
        this.num_os = num_os;
    }

    /**
     * @return the iva_bonificacion
     */
    public double getIva_bonificacion() {
        return iva_bonificacion;
    }

    /**
     * @param iva_bonificacion the iva_bonificacion to set
     */
    public void setIva_bonificacion(double iva_bonificacion) {
        this.iva_bonificacion = iva_bonificacion;
    }

    /**
     * @return the periodo
     */
    public int getPeriodo() {
        return periodo;
    }

    /**
     * @param periodo the periodo to set
     */
    public void setPeriodo(int periodo) {
        this.periodo = periodo;
    }



    /**
     * @return the porcentaje_factoring
     */
    public double getPorcentaje_factoring() {
        return porcentaje_factoring;
    }



    /**
     * @param porcentaje_factoring the porcentaje_factoring to set
     */
    public void setPorcentaje_factoring(String porcentaje_factoring) {
        double numero=Double.valueOf(porcentaje_factoring).doubleValue();
        this.porcentaje_factoring = numero;
    }

    /**
     * @return the porcentaje_formula
     */
    public double getPorcentaje_formula() {
        return porcentaje_formula;
    }

    /**
     * @param porcentaje_formula the porcentaje_formula to set
     */
    public void setPorcentaje_formula(double porcentaje_formula) {
        this.porcentaje_formula = porcentaje_formula;
    }

    /**
     * @return the estudio_economico
     */
    public String getEstudio_economico() {
        return estudio_economico;
    }

    /**
     * @param estudio_economico the estudio_economico to set
     */
    public void setEstudio_economico(String estudio_economico) {
        this.estudio_economico = estudio_economico;
    }



    /**
     * @return the base_iva_contratista
     */
    public double getBase_iva_contratista() {
        return base_iva_contratista;
    }

    /**
     * @param base_iva_contratista the base_iva_contratista to set
     */
    public void setBase_iva_contratista(Double base_iva_contratista) {

        this.base_iva_contratista = base_iva_contratista;
    }

    /**
     * @return the iva_contratista
     */
    public double getIva_contratista() {
        return iva_contratista;
    }

    /**
     * @param iva_contratista the iva_contratista to set
     */
    public void setIva_contratista(double iva_contratista) {
        this.iva_contratista = iva_contratista;
    }

    /**
     * @return the factura_cliente
     */
    public String getFactura_cliente() {
        return factura_cliente;
    }

    /**
     * @param factura_cliente the factura_cliente to set
     */
    public void setFactura_cliente(String factura_cliente) {
        this.factura_cliente = factura_cliente;
    }

    /**
     * @return the nit
     */
    public String getNit() {
        return nit;
    }

    /**
     * @param nit the nit to set
     */
    public void setNit(String nit) {
        this.nit = nit;
    }



    /**
     * @return the valor_agregado
     */
    public String getValor_agregado() {
        return valor_agregado;
    }

    /**
     * @param valor_agregado the valor_agregado to set
     */
    public void setValor_agregado(String valor_agregado) {
        this.valor_agregado = valor_agregado;
    }








}