package com.tsp.opav.model.beans;

/**
 * OfertaSeguimiento.java<br/>
 * Bean para la tabla oferta_seguimiento<br/>
 * 18/06/2010
 * @author darrieta-GEOTECH
 * @version 1.0
 */
public class OfertaSeguimiento {

    private String id_solicitud;
    private String fecha;
    private String Observaciones;
    private float avance_registrado;
    private float avance_esperado;
    private String usuario;
    private String reg_status;
    private String creation_user;
    private String user_update;

    /**
     * Get the value of user_update
     *
     * @return the value of user_update
     */
    public String getUser_update() {
        return user_update;
    }

    /**
     * Set the value of user_update
     *
     * @param user_update new value of user_update
     */
    public void setUser_update(String user_update) {
        this.user_update = user_update;
    }

    /**
     * Get the value of creation_user
     *
     * @return the value of creation_user
     */
    public String getCreation_user() {
        return creation_user;
    }

    /**
     * Set the value of creation_user
     *
     * @param creation_user new value of creation_user
     */
    public void setCreation_user(String creation_user) {
        this.creation_user = creation_user;
    }

    /**
     * Get the value of reg_status
     *
     * @return the value of reg_status
     */
    public String getReg_status() {
        return reg_status;
    }

    /**
     * Set the value of reg_status
     *
     * @param reg_status new value of reg_status
     */
    public void setReg_status(String reg_status) {
        this.reg_status = reg_status;
    }

    /**
     * Get the value of usuario
     *
     * @return the value of usuario
     */
    public String getUsuario() {
        return usuario;
    }

    /**
     * Set the value of usuario
     *
     * @param usuario new value of usuario
     */
    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    /**
     * Get the value of avance_esperado
     *
     * @return the value of avance_esperado
     */
    public float getAvance_esperado() {
        return avance_esperado;
    }

    /**
     * Set the value of avance_esperado
     *
     * @param avance_esperado new value of avance_esperado
     */
    public void setAvance_esperado(float avance_esperado) {
        this.avance_esperado = avance_esperado;
    }

    /**
     * Get the value of avance_registrado
     *
     * @return the value of avance_registrado
     */
    public float getAvance_registrado() {
        return avance_registrado;
    }

    /**
     * Set the value of avance_registrado
     *
     * @param avance_registrado new value of avance_registrado
     */
    public void setAvance_registrado(float avance_registrado) {
        this.avance_registrado = avance_registrado;
    }

    /**
     * Get the value of Observaciones
     *
     * @return the value of Observaciones
     */
    public String getObservaciones() {
        return Observaciones;
    }

    /**
     * Set the value of Observaciones
     *
     * @param Observaciones new value of Observaciones
     */
    public void setObservaciones(String Observaciones) {
        this.Observaciones = Observaciones;
    }

    /**
     * Get the value of fecha
     *
     * @return the value of fecha
     */
    public String getFecha() {
        return fecha;
    }

    /**
     * Set the value of fecha
     *
     * @param fecha new value of fecha
     */
    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    /**
     * Get the value of id_solicitud
     *
     * @return the value of id_solicitud
     */
    public String getId_solicitud() {
        return id_solicitud;
    }

    /**
     * Set the value of id_solicitud
     *
     * @param id_solicitud new value of id_solicitud
     */
    public void setId_solicitud(String id_solicitud) {
        this.id_solicitud = id_solicitud;
    }

}