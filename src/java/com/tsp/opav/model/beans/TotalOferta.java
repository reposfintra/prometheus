/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.opav.model.beans;

/**
 *
 * @author Alvaro
 */
public class TotalOferta {

    private int    id_orden;
    private String num_os;

    private double granTotal_prev1 ;
    private double granOferta      ;
    private double granEca_oferta  ;

    private double porcentaje_iva ;
    private double porcentaje_comision_provintegral ;
    private double porcentaje_factoring_fintra ;
    private double porcentaje_comision_applus ;
    private double porcentaje_comision_fintra ;
    private double porcentaje_comision_eca ;

    private double comision_provintegral ;
    private double comision_factoring_fintra ;
    private double comision_applus ;
    private double comision_fintra ;
    private double comision_eca ;

    private double granEca_oferta_calculada ;

    private double iva_granTotal_prev1 ;
    private double iva_comision_provintegral ;
    private double iva_comision_factoring_fintra ;
    private double iva_comision_applus ;
    private double iva_comision_fintra ;
    private double iva_comision_eca ;


    private double ivaGranOferta ;
    private double ivaGranEca_oferta ;

    private double granOferta_mas_iva ;
    private double granEca_oferta_mas_iva ;

    private double financiacion_fintra ;
    private double cuota_pago ;
    private double total_financiacion ;

    private int    cuotas_reales ;
    private double porcentaje_factoring  ;
    private double dtf_semana ;
    private String ano;
    private String mes;
    private String trimestre;
    private PuntosFinanciacion puntosFinanciacion;

    private double dtf_puntos;
    private String tipo_puntos;


    private String esquema_financiacion  ;
    private String regulacion ;

    private String prefijo;
    private String estudio_economico;
    private String esquema_comision;




    private double ec_porcentaje_iva ;
    private double ec_porcentaje_comision_provintegral ;
    private double ec_porcentaje_factoring_fintra ;
    private double ec_porcentaje_comision_applus ;
    private double ec_porcentaje_comision_fintra ;
    private double ec_porcentaje_comision_eca ;
    private double ec_eca_oferta ;
    private double ec_oferta ;
    private double ec_valor_mat ;
    private double ec_comision_applus ;
    private double ec_comision_provintegral ;
    private double ec_comision_factoring_fintra ;
    private double ec_comision_fintra ;
    private double ec_comision_eca ;
    private double ec_financiacion_fintra ;
    private double ec_iva_valor_mat ;
    private double ec_iva_comision_applus ;
    private double ec_iva_comision_factoring_fintra  ;
    private double ec_iva_comision_fintra ;
    private double ec_iva_comision_provintegral ;
    private double ec_iva_comision_eca ;
    private double ec_total_comisiones ;
    private double ec_iva_total ;

    private double ec_cuota_pago ;
    private double ec_total_financiacion ;

    private double ec_granTotal_prev1;


    /** Creates a new instance of OfertaEcaDetalle */
    public TotalOferta() {

        setTotalOferta();
    }



public void setTotalOferta(){

        id_orden        = 0;
        num_os          = "";

        granTotal_prev1 = 0.0;
        granOferta      = 0.0;
        granEca_oferta  = 0.0;

        porcentaje_iva                   = 0.0;
        porcentaje_comision_provintegral = 0.0;
        porcentaje_factoring_fintra      = 0.0;
        porcentaje_comision_applus       = 0.0;
        porcentaje_comision_fintra       = 0.0;
        porcentaje_comision_eca          = 0.0;

        comision_provintegral            = 0.0;
        comision_factoring_fintra        = 0.0;
        comision_applus                  = 0.0;
        comision_fintra                  = 0.0;
        comision_eca                     = 0.0;

        granEca_oferta_calculada         = 0.0;

        iva_granTotal_prev1              = 0.0;
        iva_comision_provintegral        = 0.0;
        iva_comision_factoring_fintra    = 0.0;
        iva_comision_applus              = 0.0;
        iva_comision_fintra              = 0.0;
        iva_comision_eca                 = 0.0;

        ivaGranOferta                    = 0.0;
        ivaGranEca_oferta                = 0.0;

        granOferta_mas_iva               = 0.0;
        granEca_oferta_mas_iva           = 0.0;

        financiacion_fintra              = 0.0;
        cuota_pago                       = 0.0;
        total_financiacion               = 0.0;
        cuotas_reales         = 0;
        porcentaje_factoring  = 0.0;
        dtf_semana            = 0.0;
        esquema_financiacion  = "";
        regulacion            = "";
        ano                   = "";
        mes                   = "";
        trimestre             = "";
        dtf_puntos            = 0.0;
        tipo_puntos           = "";
        puntosFinanciacion    = new PuntosFinanciacion();
        puntosFinanciacion.setPuntos(0.0);
        puntosFinanciacion.setTipo("");

        prefijo = "";
        estudio_economico = "";
        setEsquema_comision("");

        // variables para calculos con oferta y eca oferta de ap+


        ec_porcentaje_iva = 0;
        ec_porcentaje_comision_provintegral = 0;
        ec_porcentaje_factoring_fintra = 0;
        ec_porcentaje_comision_applus = 0;
        ec_porcentaje_comision_fintra = 0;
        ec_porcentaje_comision_eca = 0;
        ec_eca_oferta = 0;
        ec_oferta = 0;
        ec_valor_mat = 0;
        ec_comision_applus = 0;
        ec_comision_provintegral = 0;
        ec_comision_factoring_fintra = 0;
        ec_comision_fintra = 0;
        ec_comision_eca = 0;
        ec_financiacion_fintra = 0;
        ec_iva_valor_mat = 0;
        ec_iva_comision_applus = 0;
        ec_iva_comision_factoring_fintra  = 0;
        ec_iva_comision_fintra = 0;
        ec_iva_comision_provintegral = 0;
        ec_iva_comision_eca = 0;
        ec_total_comisiones = 0;
        ec_iva_total = 0;

        ec_cuota_pago = 0;
        ec_total_financiacion = 0;

        ec_granTotal_prev1 = 0.0;

}



    /**
     * @return the id_orden
     */
    public int getId_orden() {
        return id_orden;
    }

    /**
     * @param id_orden the id_orden to set
     */
    public void setId_orden(int id_orden) {
        this.id_orden = id_orden;
    }

    /**
     * @return the granTotal_prev1
     */
    public double getGranTotal_prev1() {
        return granTotal_prev1;
    }

    /**
     * @param granTotal_prev1 the granTotal_prev1 to set
     */
    public void setGranTotal_prev1(double granTotal_prev1) {
        this.granTotal_prev1 = granTotal_prev1;
    }

    /**
     * @return the granOferta
     */
    public double getGranOferta() {
        return granOferta;
    }

    /**
     * @param granOferta the granOferta to set
     */
    public void setGranOferta(double granOferta) {
        this.granOferta = granOferta;
    }

    /**
     * @return the granEca_oferta
     */
    public double getGranEca_oferta() {
        return granEca_oferta;
    }

    /**
     * @param granEca_oferta the granEca_oferta to set
     */
    public void setGranEca_oferta(double granEca_oferta) {
        this.granEca_oferta = granEca_oferta;
    }

    /**
     * @return the porcentaje_iva
     */
    public double getPorcentaje_iva() {
        return porcentaje_iva;
    }

    /**
     * @param porcentaje_iva the porcentaje_iva to set
     */
    public void setPorcentaje_iva(double porcentaje_iva) {
        this.porcentaje_iva = porcentaje_iva;
    }

    /**
     * @return the porcentaje_comision_provintegral
     */
    public double getPorcentaje_comision_provintegral() {
        return porcentaje_comision_provintegral;
    }

    /**
     * @param porcentaje_comision_provintegral the porcentaje_comision_provintegral to set
     */
    public void setPorcentaje_comision_provintegral(double porcentaje_comision_provintegral) {
        this.porcentaje_comision_provintegral = porcentaje_comision_provintegral;
    }

    /**
     * @return the porcentaje_factoring_fintra
     */
    public double getPorcentaje_factoring_fintra() {
        return porcentaje_factoring_fintra;
    }

    /**
     * @param porcentaje_factoring_fintra the porcentaje_factoring_fintra to set
     */
    public void setPorcentaje_factoring_fintra(double porcentaje_factoring_fintra) {
        this.porcentaje_factoring_fintra = porcentaje_factoring_fintra;
    }

    /**
     * @return the porcentaje_comision_applus
     */
    public double getPorcentaje_comision_applus() {
        return porcentaje_comision_applus;
    }

    /**
     * @param porcentaje_comision_applus the porcentaje_comision_applus to set
     */
    public void setPorcentaje_comision_applus(double porcentaje_comision_applus) {
        this.porcentaje_comision_applus = porcentaje_comision_applus;
    }

    /**
     * @return the porcentaje_comision_fintra
     */
    public double getPorcentaje_comision_fintra() {
        return porcentaje_comision_fintra;
    }

    /**
     * @param porcentaje_comision_fintra the porcentaje_comision_fintra to set
     */
    public void setPorcentaje_comision_fintra(double porcentaje_comision_fintra) {
        this.porcentaje_comision_fintra = porcentaje_comision_fintra;
    }

    /**
     * @return the porcentaje_comision_eca
     */
    public double getPorcentaje_comision_eca() {
        return porcentaje_comision_eca;
    }

    /**
     * @param porcentaje_comision_eca the porcentaje_comision_eca to set
     */
    public void setPorcentaje_comision_eca(double porcentaje_comision_eca) {
        this.porcentaje_comision_eca = porcentaje_comision_eca;
    }

    /**
     * @return the comision_provintegral
     */
    public double getComision_provintegral() {
        return comision_provintegral;
    }

    /**
     * @param comision_provintegral the comision_provintegral to set
     */
    public void setComision_provintegral(double comision_provintegral) {
        this.comision_provintegral = comision_provintegral;
    }

    /**
     * @return the comision_factoring_fintra
     */
    public double getComision_factoring_fintra() {
        return comision_factoring_fintra;
    }

    /**
     * @param comision_factoring_fintra the comision_factoring_fintra to set
     */
    public void setComision_factoring_fintra(double comision_factoring_fintra) {
        this.comision_factoring_fintra = comision_factoring_fintra;
    }

    /**
     * @return the comision_applus
     */
    public double getComision_applus() {
        return comision_applus;
    }

    /**
     * @param comision_applus the comision_applus to set
     */
    public void setComision_applus(double comision_applus) {
        this.comision_applus = comision_applus;
    }

    /**
     * @return the comision_fintra
     */
    public double getComision_fintra() {
        return comision_fintra;
    }

    /**
     * @param comision_fintra the comision_fintra to set
     */
    public void setComision_fintra(double comision_fintra) {
        this.comision_fintra = comision_fintra;
    }

    /**
     * @return the comision_eca
     */
    public double getComision_eca() {
        return comision_eca;
    }

    /**
     * @param comision_eca the comision_eca to set
     */
    public void setComision_eca(double comision_eca) {
        this.comision_eca = comision_eca;
    }

    /**
     * @return the granEca_oferta_calculada
     */
    public double getGranEca_oferta_calculada() {
        return granEca_oferta_calculada;
    }

    /**
     * @param granEca_oferta_calculada the granEca_oferta_calculada to set
     */
    public void setGranEca_oferta_calculada(double granEca_oferta_calculada) {
        this.granEca_oferta_calculada = granEca_oferta_calculada;
    }

    /**
     * @return the iva_granTotal_prev1
     */
    public double getIva_granTotal_prev1() {
        return iva_granTotal_prev1;
    }

    /**
     * @param iva_granTotal_prev1 the iva_granTotal_prev1 to set
     */
    public void setIva_granTotal_prev1(double iva_granTotal_prev1) {
        this.iva_granTotal_prev1 = iva_granTotal_prev1;
    }

    /**
     * @return the iva_comision_provintegral
     */
    public double getIva_comision_provintegral() {
        return iva_comision_provintegral;
    }

    /**
     * @param iva_comision_provintegral the iva_comision_provintegral to set
     */
    public void setIva_comision_provintegral(double iva_comision_provintegral) {
        this.iva_comision_provintegral = iva_comision_provintegral;
    }

    /**
     * @return the iva_comision_factoring_fintra
     */
    public double getIva_comision_factoring_fintra() {
        return iva_comision_factoring_fintra;
    }

    /**
     * @param iva_comision_factoring_fintra the iva_comision_factoring_fintra to set
     */
    public void setIva_comision_factoring_fintra(double iva_comision_factoring_fintra) {
        this.iva_comision_factoring_fintra = iva_comision_factoring_fintra;
    }

    /**
     * @return the iva_comision_applus
     */
    public double getIva_comision_applus() {
        return iva_comision_applus;
    }

    /**
     * @param iva_comision_applus the iva_comision_applus to set
     */
    public void setIva_comision_applus(double iva_comision_applus) {
        this.iva_comision_applus = iva_comision_applus;
    }

    /**
     * @return the iva_comision_fintra
     */
    public double getIva_comision_fintra() {
        return iva_comision_fintra;
    }

    /**
     * @param iva_comision_fintra the iva_comision_fintra to set
     */
    public void setIva_comision_fintra(double iva_comision_fintra) {
        this.iva_comision_fintra = iva_comision_fintra;
    }

    /**
     * @return the iva_comision_eca
     */
    public double getIva_comision_eca() {
        return iva_comision_eca;
    }

    /**
     * @param iva_comision_eca the iva_comision_eca to set
     */
    public void setIva_comision_eca(double iva_comision_eca) {
        this.iva_comision_eca = iva_comision_eca;
    }

    /**
     * @return the ivaGranOferta
     */
    public double getIvaGranOferta() {
        return ivaGranOferta;
    }

    /**
     * @param ivaGranOferta the ivaGranOferta to set
     */
    public void setIvaGranOferta(double ivaGranOferta) {
        this.ivaGranOferta = ivaGranOferta;
    }

    /**
     * @return the ivaGranEca_oferta
     */
    public double getIvaGranEca_oferta() {
        return ivaGranEca_oferta;
    }

    /**
     * @param ivaGranEca_oferta the ivaGranEca_oferta to set
     */
    public void setIvaGranEca_oferta(double ivaGranEca_oferta) {
        this.ivaGranEca_oferta = ivaGranEca_oferta;
    }

    /**
     * @return the granOferta_mas_iva
     */
    public double getGranOferta_mas_iva() {
        return granOferta_mas_iva;
    }

    /**
     * @param granOferta_mas_iva the granOferta_mas_iva to set
     */
    public void setGranOferta_mas_iva(double granOferta_mas_iva) {
        this.granOferta_mas_iva = granOferta_mas_iva;
    }

    /**
     * @return the granEca_oferta_mas_iva
     */
    public double getGranEca_oferta_mas_iva() {
        return granEca_oferta_mas_iva;
    }

    /**
     * @param granEca_oferta_mas_iva the granEca_oferta_mas_iva to set
     */
    public void setGranEca_oferta_mas_iva(double granEca_oferta_mas_iva) {
        this.granEca_oferta_mas_iva = granEca_oferta_mas_iva;
    }

    /**
     * @return the financiacion_fintra
     */
    public double getFinanciacion_fintra() {
        return financiacion_fintra;
    }

    /**
     * @param financiacion_fintra the financiacion_fintra to set
     */
    public void setFinanciacion_fintra(double financiacion_fintra) {
        this.financiacion_fintra = financiacion_fintra;
    }

    /**
     * @return the cuota_pago
     */
    public double getCuota_pago() {
        return cuota_pago;
    }

    /**
     * @param cuota_pago the cuota_pago to set
     */
    public void setCuota_pago(double cuota_pago) {
        this.cuota_pago = cuota_pago;
    }

    /**
     * @return the total_financiacion
     */
    public double getTotal_financiacion() {
        return total_financiacion;
    }

    /**
     * @param total_financiacion the total_financiacion to set
     */
    public void setTotal_financiacion(double total_financiacion) {
        this.total_financiacion = total_financiacion;
    }

    /**
     * @return the cuotas_reales
     */
    public int getCuotas_reales() {
        return cuotas_reales;
    }

    /**
     * @param cuotas_reales the cuotas_reales to set
     */
    public void setCuotas_reales(int cuotas_reales) {
        this.cuotas_reales = cuotas_reales;
    }

    /**
     * @return the porcentaje_factoring
     */
    public double getPorcentaje_factoring() {
        return porcentaje_factoring;
    }

    /**
     * @param porcentaje_factoring the porcentaje_factoring to set
     */
    public void setPorcentaje_factoring(double porcentaje_factoring) {
        this.porcentaje_factoring = porcentaje_factoring;
    }

    /**
     * @return the dtf_semana
     */
    public double getDtf_semana() {
        return dtf_semana;
    }

    /**
     * @param dtf_semana the dtf_semana to set
     */
    public void setDtf_semana(double dtf_semana) {
        this.dtf_semana = dtf_semana;
    }

    /**
     * @return the esquema_financiacion
     */
    public String getEsquema_financiacion() {
        return esquema_financiacion;
    }

    /**
     * @param esquema_financiacion the esquema_financiacion to set
     */
    public void setEsquema_financiacion(String esquema_financiacion) {
        this.esquema_financiacion = esquema_financiacion;
    }

    /**
     * @return the regulacion
     */
    public String getRegulacion() {
        return regulacion;
    }

    /**
     * @param regulacion the regulacion to set
     */
    public void setRegulacion(String regulacion) {
        this.regulacion = regulacion;
    }

    /**
     * @return the prefijo
     */
    public String getPrefijo() {
        return prefijo;
    }

    /**
     * @param prefijo the prefijo to set
     */
    public void setPrefijo(String prefijo) {
        this.prefijo = prefijo;
    }

    /**
     * @return the ano
     */
    public String getAno() {
        return ano;
    }

    /**
     * @param ano the ano to set
     */
    public void setAno(String ano) {
        this.ano = ano;
    }

    /**
     * @return the mes
     */
    public String getMes() {
        return mes;
    }

    /**
     * @param mes the mes to set
     */
    public void setMes(String mes) {
        this.mes = mes;
    }

    /**
     * @return the trimestre
     */
    public String getTrimestre() {
        return trimestre;
    }

    /**
     * @param trimestre the trimestre to set
     */
    public void setTrimestre(String trimestre) {
        this.trimestre = trimestre;
    }

    /**
     * @return the puntosFinanciacion
     */
    public PuntosFinanciacion getPuntosFinanciacion() {
        return puntosFinanciacion;
    }

    /**
     * @param puntosFinanciacion the puntosFinanciacion to set
     */
    public void setPuntosFinanciacion(PuntosFinanciacion puntosFinanciacion) {
        this.puntosFinanciacion = puntosFinanciacion;
    }

    /**
     * @return the dtf_puntos
     */
    public double getDtf_puntos() {
        return dtf_puntos;
    }

    /**
     * @param dtf_puntos the dtf_puntos to set
     */
    public void setDtf_puntos(double dtf_puntos) {
        this.dtf_puntos = dtf_puntos;
    }

    /**
     * @return the tipo_puntos
     */
    public String getTipo_puntos() {
        return tipo_puntos;
    }

    /**
     * @param tipo_puntos the tipo_puntos to set
     */
    public void setTipo_puntos(String tipo_puntos) {
        this.tipo_puntos = tipo_puntos;
    }

    /**
     * @return the ec_porcentaje_iva
     */
    public double getEc_porcentaje_iva() {
        return ec_porcentaje_iva;
    }

    /**
     * @param ec_porcentaje_iva the ec_porcentaje_iva to set
     */
    public void setEc_porcentaje_iva(double ec_porcentaje_iva) {
        this.ec_porcentaje_iva = ec_porcentaje_iva;
    }

    /**
     * @return the ec_porcentaje_comision_provintegral
     */
    public double getEc_porcentaje_comision_provintegral() {
        return ec_porcentaje_comision_provintegral;
    }

    /**
     * @param ec_porcentaje_comision_provintegral the ec_porcentaje_comision_provintegral to set
     */
    public void setEc_porcentaje_comision_provintegral(double ec_porcentaje_comision_provintegral) {
        this.ec_porcentaje_comision_provintegral = ec_porcentaje_comision_provintegral;
    }

    /**
     * @return the ec_porcentaje_factoring_fintra
     */
    public double getEc_porcentaje_factoring_fintra() {
        return ec_porcentaje_factoring_fintra;
    }

    /**
     * @param ec_porcentaje_factoring_fintra the ec_porcentaje_factoring_fintra to set
     */
    public void setEc_porcentaje_factoring_fintra(double ec_porcentaje_factoring_fintra) {
        this.ec_porcentaje_factoring_fintra = ec_porcentaje_factoring_fintra;
    }

    /**
     * @return the ec_porcentaje_comision_applus
     */
    public double getEc_porcentaje_comision_applus() {
        return ec_porcentaje_comision_applus;
    }

    /**
     * @param ec_porcentaje_comision_applus the ec_porcentaje_comision_applus to set
     */
    public void setEc_porcentaje_comision_applus(double ec_porcentaje_comision_applus) {
        this.ec_porcentaje_comision_applus = ec_porcentaje_comision_applus;
    }

    /**
     * @return the ec_porcentaje_comision_fintra
     */
    public double getEc_porcentaje_comision_fintra() {
        return ec_porcentaje_comision_fintra;
    }

    /**
     * @param ec_porcentaje_comision_fintra the ec_porcentaje_comision_fintra to set
     */
    public void setEc_porcentaje_comision_fintra(double ec_porcentaje_comision_fintra) {
        this.ec_porcentaje_comision_fintra = ec_porcentaje_comision_fintra;
    }

    /**
     * @return the ec_porcentaje_comision_eca
     */
    public double getEc_porcentaje_comision_eca() {
        return ec_porcentaje_comision_eca;
    }

    /**
     * @param ec_porcentaje_comision_eca the ec_porcentaje_comision_eca to set
     */
    public void setEc_porcentaje_comision_eca(double ec_porcentaje_comision_eca) {
        this.ec_porcentaje_comision_eca = ec_porcentaje_comision_eca;
    }

    /**
     * @return the ec_eca_oferta
     */
    public double getEc_eca_oferta() {
        return ec_eca_oferta;
    }

    /**
     * @param ec_eca_oferta the ec_eca_oferta to set
     */
    public void setEc_eca_oferta(double ec_eca_oferta) {
        this.ec_eca_oferta = ec_eca_oferta;
    }

    /**
     * @return the ec_oferta
     */
    public double getEc_oferta() {
        return ec_oferta;
    }

    /**
     * @param ec_oferta the ec_oferta to set
     */
    public void setEc_oferta(double ec_oferta) {
        this.ec_oferta = ec_oferta;
    }

    /**
     * @return the ec_valor_mat
     */
    public double getEc_valor_mat() {
        return ec_valor_mat;
    }

    /**
     * @param ec_valor_mat the ec_valor_mat to set
     */
    public void setEc_valor_mat(double ec_valor_mat) {
        this.ec_valor_mat = ec_valor_mat;
    }

    /**
     * @return the ec_comision_applus
     */
    public double getEc_comision_applus() {
        return ec_comision_applus;
    }

    /**
     * @param ec_comision_applus the ec_comision_applus to set
     */
    public void setEc_comision_applus(double ec_comision_applus) {
        this.ec_comision_applus = ec_comision_applus;
    }

    /**
     * @return the ec_comision_provintegral
     */
    public double getEc_comision_provintegral() {
        return ec_comision_provintegral;
    }

    /**
     * @param ec_comision_provintegral the ec_comision_provintegral to set
     */
    public void setEc_comision_provintegral(double ec_comision_provintegral) {
        this.ec_comision_provintegral = ec_comision_provintegral;
    }

    /**
     * @return the ec_comision_factoring_fintra
     */
    public double getEc_comision_factoring_fintra() {
        return ec_comision_factoring_fintra;
    }

    /**
     * @param ec_comision_factoring_fintra the ec_comision_factoring_fintra to set
     */
    public void setEc_comision_factoring_fintra(double ec_comision_factoring_fintra) {
        this.ec_comision_factoring_fintra = ec_comision_factoring_fintra;
    }

    /**
     * @return the ec_comision_fintra
     */
    public double getEc_comision_fintra() {
        return ec_comision_fintra;
    }

    /**
     * @param ec_comision_fintra the ec_comision_fintra to set
     */
    public void setEc_comision_fintra(double ec_comision_fintra) {
        this.ec_comision_fintra = ec_comision_fintra;
    }

    /**
     * @return the ec_comision_eca
     */
    public double getEc_comision_eca() {
        return ec_comision_eca;
    }

    /**
     * @param ec_comision_eca the ec_comision_eca to set
     */
    public void setEc_comision_eca(double ec_comision_eca) {
        this.ec_comision_eca = ec_comision_eca;
    }

    /**
     * @return the ec_financiacion_fintra
     */
    public double getEc_financiacion_fintra() {
        return ec_financiacion_fintra;
    }

    /**
     * @param ec_financiacion_fintra the ec_financiacion_fintra to set
     */
    public void setEc_financiacion_fintra(double ec_financiacion_fintra) {
        this.ec_financiacion_fintra = ec_financiacion_fintra;
    }

    /**
     * @return the ec_iva_valor_mat
     */
    public double getEc_iva_valor_mat() {
        return ec_iva_valor_mat;
    }

    /**
     * @param ec_iva_valor_mat the ec_iva_valor_mat to set
     */
    public void setEc_iva_valor_mat(double ec_iva_valor_mat) {
        this.ec_iva_valor_mat = ec_iva_valor_mat;
    }

    /**
     * @return the ec_iva_comision_applus
     */
    public double getEc_iva_comision_applus() {
        return ec_iva_comision_applus;
    }

    /**
     * @param ec_iva_comision_applus the ec_iva_comision_applus to set
     */
    public void setEc_iva_comision_applus(double ec_iva_comision_applus) {
        this.ec_iva_comision_applus = ec_iva_comision_applus;
    }

    /**
     * @return the ec_iva_comision_factoring_fintra
     */
    public double getEc_iva_comision_factoring_fintra() {
        return ec_iva_comision_factoring_fintra;
    }

    /**
     * @param ec_iva_comision_factoring_fintra the ec_iva_comision_factoring_fintra to set
     */
    public void setEc_iva_comision_factoring_fintra(double ec_iva_comision_factoring_fintra) {
        this.ec_iva_comision_factoring_fintra = ec_iva_comision_factoring_fintra;
    }

    /**
     * @return the ec_iva_comision_fintra
     */
    public double getEc_iva_comision_fintra() {
        return ec_iva_comision_fintra;
    }

    /**
     * @param ec_iva_comision_fintra the ec_iva_comision_fintra to set
     */
    public void setEc_iva_comision_fintra(double ec_iva_comision_fintra) {
        this.ec_iva_comision_fintra = ec_iva_comision_fintra;
    }

    /**
     * @return the ec_iva_comision_provintegral
     */
    public double getEc_iva_comision_provintegral() {
        return ec_iva_comision_provintegral;
    }

    /**
     * @param ec_iva_comision_provintegral the ec_iva_comision_provintegral to set
     */
    public void setEc_iva_comision_provintegral(double ec_iva_comision_provintegral) {
        this.ec_iva_comision_provintegral = ec_iva_comision_provintegral;
    }

    /**
     * @return the ec_iva_comision_eca
     */
    public double getEc_iva_comision_eca() {
        return ec_iva_comision_eca;
    }

    /**
     * @param ec_iva_comision_eca the ec_iva_comision_eca to set
     */
    public void setEc_iva_comision_eca(double ec_iva_comision_eca) {
        this.ec_iva_comision_eca = ec_iva_comision_eca;
    }

    /**
     * @return the ec_total_comisiones
     */
    public double getEc_total_comisiones() {
        return ec_total_comisiones;
    }

    /**
     * @param ec_total_comisiones the ec_total_comisiones to set
     */
    public void setEc_total_comisiones(double ec_total_comisiones) {
        this.ec_total_comisiones = ec_total_comisiones;
    }

    /**
     * @return the ec_iva_total
     */
    public double getEc_iva_total() {
        return ec_iva_total;
    }

    /**
     * @param ec_iva_total the ec_iva_total to set
     */
    public void setEc_iva_total(double ec_iva_total) {
        this.ec_iva_total = ec_iva_total;
    }

    /**
     * @return the ec_cuota_pago
     */
    public double getEc_cuota_pago() {
        return ec_cuota_pago;
    }

    /**
     * @param ec_cuota_pago the ec_cuota_pago to set
     */
    public void setEc_cuota_pago(double ec_cuota_pago) {
        this.ec_cuota_pago = ec_cuota_pago;
    }

    /**
     * @return the ec_total_financiacion
     */
    public double getEc_total_financiacion() {
        return ec_total_financiacion;
    }

    /**
     * @param ec_total_financiacion the ec_total_financiacion to set
     */
    public void setEc_total_financiacion(double ec_total_financiacion) {
        this.ec_total_financiacion = ec_total_financiacion;
    }

    /**
     * @return the estudio_economico
     */
    public String getEstudio_economico() {
        return estudio_economico;
    }

    /**
     * @param estudio_economico the estudio_economico to set
     */
    public void setEstudio_economico(String estudio_economico) {
        this.estudio_economico = estudio_economico;
    }


    /**
     * @return the num_os
     */
    public String getNum_os() {
        return num_os;
    }

    /**
     * @param num_os the num_os to set
     */
    public void setNum_os(String num_os) {
        this.num_os = num_os;
    }

    /**
     * @return the ec_granTotal_prev1
     */
    public double getEc_granTotal_prev1() {
        return ec_granTotal_prev1;
    }

    /**
     * @param ec_granTotal_prev1 the ec_granTotal_prev1 to set
     */
    public void setEc_granTotal_prev1(double ec_granTotal_prev1) {
        this.ec_granTotal_prev1 = ec_granTotal_prev1;
    }

    /**
     * @return the esquema_comision
     */
    public String getEsquema_comision() {
        return esquema_comision;
    }

    /**
     * @param esquema_comision the esquema_comision to set
     */
    public void setEsquema_comision(String esquema_comision) {
        this.esquema_comision = esquema_comision;
    }




}