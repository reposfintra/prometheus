/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.opav.model.beans;

/** * @author  Fintra */
public class OfertaElca implements Cloneable {

    private String id_solicitud, id_cliente, id_oferta, num_os, descripcion, last_update,
            creation_date, creation_user, user_update, reg_status, nic, fecha_entrega_oferta,
            creacion_fecha_entrega_oferta, usuario_entrega_oferta, tipo_solicitud, consecutivo_oferta,
            nombre_solicitud, aviso;
    private String estado_cartera;//JJCASTRO
    private String fec_val_cartera;//JJCASTRO
    private String consideraciones, otras_consideraciones;  //pbassil
    private String oficial;                               //pbassil
    private String responsable;
    private String nomresponsable;
    private String interventor;
      private String tipo_dtf;
      private String nomproyecto;
      private String lineanegocio;
      private String fecha_limite;
      private String tipo_negocio;
      private String tipo_trabajo;

    public String getTipo_negocio() {
        return tipo_negocio;
    }

    public void setTipo_negocio(String tipo_negocio) {
        this.tipo_negocio = tipo_negocio;
    }

    public String getTipo_trabajo() {
        return tipo_trabajo;
    }

    public void setTipo_trabajo(String tipo_trabajo) {
        this.tipo_trabajo = tipo_trabajo;
    }

    

    public String getLineanegocio() {
        return lineanegocio;
    }

    public void setLineanegocio(String lineanegocio) {
        this.lineanegocio = lineanegocio;
    }

    public String getTipo_dtf() {
        return tipo_dtf;
    }

    public String getNomresponsable() {
        return nomresponsable;
    }

    public void setNomresponsable(String nomresponsable) {
        this.nomresponsable = nomresponsable;
    }

    public void setTipo_dtf(String tipo_dtf) {
        this.tipo_dtf = tipo_dtf;
    }

    public String getNomproyecto() {
        return nomproyecto;
    }

    public void setNomproyecto(String nomproyecto) {
        this.nomproyecto = nomproyecto;
    }

    public String getResponsable() {
        return responsable;
    }

    public void setResponsable(String responsable) {
        this.responsable = responsable;
    }

    public String getInterventor() {
        return interventor;
    }

    public void setInterventor(String interventor) {
        this.interventor = interventor;
    }

    public OfertaElca() {
    }

    public String getOficial() {
        return oficial;
    }

    public void setOficial(String oficial) {
        this.oficial = oficial;
    }

    public String getAviso() {
        return aviso;
    }

    public void setAviso(String aviso) {
        this.aviso = aviso;
    }

    public String getConsecutivo_oferta() {
        return consecutivo_oferta;
    }

    public void setConsecutivo_oferta(String consecutivo_oferta) {
        this.consecutivo_oferta = consecutivo_oferta;
    }

    public String getConsideraciones() {
        return consideraciones;
    }

    public void setConsideraciones(String consideraciones) {
        this.consideraciones = consideraciones;
    }

    public String getCreacion_fecha_entrega_oferta() {
        return creacion_fecha_entrega_oferta;
    }

    public void setCreacion_fecha_entrega_oferta(String creacion_fecha_entrega_oferta) {
        this.creacion_fecha_entrega_oferta = creacion_fecha_entrega_oferta;
    }

    public String getCreation_date() {
        return creation_date;
    }

    public void setCreation_date(String creation_date) {
        this.creation_date = creation_date;
    }

    public String getCreation_user() {
        return creation_user;
    }

    public void setCreation_user(String creation_user) {
        this.creation_user = creation_user;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getFecha_entrega_oferta() {
        return fecha_entrega_oferta;
    }

    public void setFecha_entrega_oferta(String fecha_entrega_oferta) {
        this.fecha_entrega_oferta = fecha_entrega_oferta;
    }

    public String getId_cliente() {
        return id_cliente;
    }

    public void setId_cliente(String id_cliente) {
        this.id_cliente = id_cliente;
    }

    public String getId_oferta() {
        return id_oferta;
    }

    public void setId_oferta(String id_oferta) {
        this.id_oferta = id_oferta;
    }

    public String getId_solicitud() {
        return id_solicitud;
    }

    public void setId_solicitud(String id_solicitud) {
        this.id_solicitud = id_solicitud;
    }

    public String getLast_update() {
        return last_update;
    }

    public void setLast_update(String last_update) {
        this.last_update = last_update;
    }

    public String getNic() {
        return nic;
    }

    public void setNic(String nic) {
        this.nic = nic;
    }

    public String getNombre_solicitud() {
        return nombre_solicitud;
    }

    public void setNombre_solicitud(String nombre_solicitud) {
        this.nombre_solicitud = nombre_solicitud;
    }

    public String getNum_os() {
        return num_os;
    }

    public void setNum_os(String num_os) {
        this.num_os = num_os;
    }

    public String getOtras_consideraciones() {
        return otras_consideraciones;
    }

    public void setOtras_consideraciones(String otras_consideraciones) {
        this.otras_consideraciones = otras_consideraciones;
    }

    public String getReg_status() {
        return reg_status;
    }

    public void setReg_status(String reg_status) {
        this.reg_status = reg_status;
    }

    public String getTipo_solicitud() {
        return tipo_solicitud;
    }

    public void setTipo_solicitud(String tipo_solicitud) {
        this.tipo_solicitud = tipo_solicitud;
    }

    public String getUser_update() {
        return user_update;
    }

    public void setUser_update(String user_update) {
        this.user_update = user_update;
    }

    public String getUsuario_entrega_oferta() {
        return usuario_entrega_oferta;
    }

    public void setUsuario_entrega_oferta(String usuario_entrega_oferta) {
        this.usuario_entrega_oferta = usuario_entrega_oferta;
    }

    public String getEstado_cartera() {
        return estado_cartera;
    }

    public void setEstado_cartera(String estado_cartera) {
        this.estado_cartera = estado_cartera;
    }

    public String getFec_val_cartera() {
        return fec_val_cartera;
    }

    public void setFec_val_cartera(String fec_val_cartera) {
        this.fec_val_cartera = fec_val_cartera;
    }

        public String getTipoDtf() {
        return tipo_dtf;
    }
    public void setTipoDtf(String x) {
        this.tipo_dtf= x;
    }
    
    public String getFecha_limite() {
        return fecha_limite;
    }

    public void setFecha_limite(String fecha_limite) {
        this.fecha_limite = fecha_limite;
    }
    
    public Object clone() {
        OfertaElca obj = null;
        try {
            obj = (OfertaElca ) super.clone();
        } catch (CloneNotSupportedException ex) {
            System.out.println(" no se puede duplicar");
        }
        return obj;
    }

}
