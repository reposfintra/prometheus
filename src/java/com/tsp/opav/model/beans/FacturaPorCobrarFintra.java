/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.opav.model.beans;

/**
 *
 * @author Alvaro
 */
public class FacturaPorCobrarFintra {


    private String documento;
    private String codcli;
    private String nit;
    private double valor_factura;
    private double valor_facturame;






    /** Creates a new instance of AccionPorSolicitud */
    public FacturaPorCobrarFintra() {
    }


    /** Extrae un registro de la BD correspondientes a las acciones de una solicitudes que no esta facturada */
    public static FacturaPorCobrarFintra load(java.sql.ResultSet rs)throws java.sql.SQLException{


        FacturaPorCobrarFintra facturaPorCobrarFintra = new FacturaPorCobrarFintra();

        facturaPorCobrarFintra.setDocumento(rs.getString("documento") ) ;
        facturaPorCobrarFintra.setCodcli(rs.getString("codcli") );
        facturaPorCobrarFintra.setNit(rs.getString("nit") );;
        facturaPorCobrarFintra.setValor_factura(rs.getDouble("valor_factura") );
        facturaPorCobrarFintra.setValor_facturame(rs.getDouble("valor_facturame") );


        return facturaPorCobrarFintra;

    }











    /**
     * @return the documento
     */
    public String getDocumento() {
        return documento;
    }

    /**
     * @param documento the documento to set
     */
    public void setDocumento(String documento) {
        this.documento = documento;
    }

    /**
     * @return the codcli
     */
    public String getCodcli() {
        return codcli;
    }

    /**
     * @param codcli the codcli to set
     */
    public void setCodcli(String codcli) {
        this.codcli = codcli;
    }

    /**
     * @return the nit
     */
    public String getNit() {
        return nit;
    }

    /**
     * @param nit the nit to set
     */
    public void setNit(String nit) {
        this.nit = nit;
    }

    /**
     * @return the valor_factura
     */
    public double getValor_factura() {
        return valor_factura;
    }

    /**
     * @param valor_factura the valor_factura to set
     */
    public void setValor_factura(double valor_factura) {
        this.valor_factura = valor_factura;
    }

    /**
     * @return the valor_facturame
     */
    public double getValor_facturame() {
        return valor_facturame;
    }

    /**
     * @param valor_facturame the valor_facturame to set
     */
    public void setValor_facturame(double valor_facturame) {
        this.valor_facturame = valor_facturame;
    }


}