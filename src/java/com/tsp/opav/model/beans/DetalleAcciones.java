/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.opav.model.beans;

/**
 *
 * @author Alvaro
 */
public class DetalleAcciones {

    private String id_accion;
    private String id_solicitud;
    private int    parcial;
    private String contratista;
    private String nombre_contratista;
    private double material;
    private double mano_obra;
    private double otros;
    private double administracion;
    private double imprevisto;
    private double utilidad;
    private double costo_contratista;
    private double iva_contratista;
    private double bonificacion;
    private double iva_bonificacion;
    private double comision_opav;
    private double comision_fintra;
    private double comision_interventoria;
    private double comision_provintegral;
    private double comision_eca;
    private double iva_comision_opav;
    private double iva_comision_fintra;
    private double iva_comision_interventoria;
    private double iva_comision_provintegral;
    private double iva_comision_eca;
    private double valor_a_financiar;
    private double subtotal_iva;
    private double valor_a_financiar_sin_iva;






    /** Creates a new instance of AccionPorSolicitud */
    public DetalleAcciones() {
    }


    /** Extrae un registro de la BD correspondientes a las acciones de una solicitudes que no esta facturada */
    public static DetalleAcciones load(java.sql.ResultSet rs)throws java.sql.SQLException{

        DetalleAcciones detalleAcciones = new DetalleAcciones();

        detalleAcciones.setId_accion( rs.getString("id_accion") );
        detalleAcciones.setId_solicitud( rs.getString("id_solicitud") );
        detalleAcciones.setParcial( rs.getInt("parcial") );
        detalleAcciones.setContratista( rs.getString("contratista") );
        detalleAcciones.setNombre_contratista( rs.getString("nombre_contratista") );
        detalleAcciones.setMaterial( rs.getDouble("material") );
        detalleAcciones.setMano_obra( rs.getDouble("mano_obra") );
        detalleAcciones.setOtros( rs.getDouble("otros") );
        detalleAcciones.setAdministracion( rs.getDouble("administracion") );
        detalleAcciones.setImprevisto( rs.getDouble("imprevisto") );
        detalleAcciones.setUtilidad( rs.getDouble("utilidad") );
        detalleAcciones.setCosto_contratista( rs.getDouble("costo_contratista") );
        detalleAcciones.setIva_contratista( rs.getDouble("iva_contratista") );
        detalleAcciones.setBonificacion( rs.getDouble("bonificacion") );
        detalleAcciones.setIva_bonificacion( rs.getDouble("iva_bonificacion") );
        detalleAcciones.setComision_opav( rs.getDouble("comision_opav") );
        detalleAcciones.setComision_fintra( rs.getDouble("comision_fintra") );
        detalleAcciones.setComision_interventoria( rs.getDouble("comision_interventoria") );
        detalleAcciones.setComision_provintegral( rs.getDouble("comision_provintegral") );
        detalleAcciones.setComision_eca( rs.getDouble("comision_eca") );
        detalleAcciones.setIva_comision_opav( rs.getDouble("iva_comision_opav") );
        detalleAcciones.setIva_comision_fintra( rs.getDouble("iva_comision_fintra") );
        detalleAcciones.setIva_comision_interventoria( rs.getDouble("iva_comision_interventoria") );
        detalleAcciones.setIva_comision_provintegral( rs.getDouble("iva_comision_provintegral") );
        detalleAcciones.setIva_comision_eca( rs.getDouble("iva_comision_eca") );

        double valor =  detalleAcciones.getCosto_contratista() +
                        // detalleAcciones.getBonificacion() + //20100207
                        detalleAcciones.getComision_eca() +
                        detalleAcciones.getComision_fintra() +
                        detalleAcciones.getComision_interventoria() +
                        detalleAcciones.getComision_opav() +
                        detalleAcciones.getComision_provintegral() +
                        detalleAcciones.getIva_bonificacion() + //20100207 sr
                        detalleAcciones.getIva_comision_eca() +
                        detalleAcciones.getIva_comision_fintra() +
                        detalleAcciones.getIva_comision_interventoria() +
                        detalleAcciones.getIva_comision_opav() +
                        detalleAcciones.getIva_contratista() +
                        detalleAcciones.getIva_comision_provintegral() ;

        detalleAcciones.setValor_a_financiar(valor);

        valor =  detalleAcciones.getIva_bonificacion() + //20100207 sr
                 detalleAcciones.getIva_comision_eca() +
                 detalleAcciones.getIva_comision_opav() +
                 detalleAcciones.getIva_comision_fintra() +
                 detalleAcciones.getIva_comision_interventoria() +
                 detalleAcciones.getIva_contratista() +
                 detalleAcciones.getIva_comision_provintegral();

        detalleAcciones.setSubtotal_iva(valor);

        valor =  detalleAcciones.getCosto_contratista() +
                 // detalleAcciones.getBonificacion() + //20100207
                 detalleAcciones.getComision_eca() +
                 detalleAcciones.getComision_fintra() +
                 detalleAcciones.getComision_interventoria() +
                 detalleAcciones.getComision_opav() +
                 detalleAcciones.getComision_provintegral()  ;

        detalleAcciones.setValor_a_financiar_sin_iva(valor);

        return detalleAcciones;

    }





















    /**
     * @return the id_solicitud
     */
    public String getId_solicitud() {
        return id_solicitud;
    }

    /**
     * @param id_solicitud the id_solicitud to set
     */
    public void setId_solicitud(String id_solicitud) {
        this.id_solicitud = id_solicitud;
    }

    /**
     * @return the parcial
     */
    public int getParcial() {
        return parcial;
    }

    /**
     * @param parcial the parcial to set
     */
    public void setParcial(int parcial) {
        this.parcial = parcial;
    }

    /**
     * @return the material
     */
    public double getMaterial() {
        return material;
    }

    /**
     * @param material the material to set
     */
    public void setMaterial(double material) {
        this.material = material;
    }

    /**
     * @return the mano_obra
     */
    public double getMano_obra() {
        return mano_obra;
    }

    /**
     * @param mano_obra the mano_obra to set
     */
    public void setMano_obra(double mano_obra) {
        this.mano_obra = mano_obra;
    }

    /**
     * @return the otros
     */
    public double getOtros() {
        return otros;
    }

    /**
     * @param otros the otros to set
     */
    public void setOtros(double otros) {
        this.otros = otros;
    }

    /**
     * @return the administracion
     */
    public double getAdministracion() {
        return administracion;
    }

    /**
     * @param administracion the administracion to set
     */
    public void setAdministracion(double administracion) {
        this.administracion = administracion;
    }

    /**
     * @return the imprevisto
     */
    public double getImprevisto() {
        return imprevisto;
    }

    /**
     * @param imprevisto the imprevisto to set
     */
    public void setImprevisto(double imprevisto) {
        this.imprevisto = imprevisto;
    }

    /**
     * @return the utilidad
     */
    public double getUtilidad() {
        return utilidad;
    }

    /**
     * @param utilidad the utilidad to set
     */
    public void setUtilidad(double utilidad) {
        this.utilidad = utilidad;
    }

    /**
     * @return the costo_contratista
     */
    public double getCosto_contratista() {
        return costo_contratista;
    }

    /**
     * @param costo_contratista the costo_contratista to set
     */
    public void setCosto_contratista(double costo_contratista) {
        this.costo_contratista = costo_contratista;
    }

    /**
     * @return the iva_contratista
     */
    public double getIva_contratista() {
        return iva_contratista;
    }

    /**
     * @param iva_contratista the iva_contratista to set
     */
    public void setIva_contratista(double iva_contratista) {
        this.iva_contratista = iva_contratista;
    }

    /**
     * @return the bonificacion
     */
    public double getBonificacion() {
        return bonificacion;
    }

    /**
     * @param bonificacion the bonificacion to set
     */
    public void setBonificacion(double bonificacion) {
        this.bonificacion = bonificacion;
    }

    /**
     * @return the iva_bonificacion
     */
    public double getIva_bonificacion() {
        return iva_bonificacion;
    }

    /**
     * @param iva_bonificacion the iva_bonificacion to set
     */
    public void setIva_bonificacion(double iva_bonificacion) {
        this.iva_bonificacion = iva_bonificacion;
    }

    /**
     * @return the comision_opav
     */
    public double getComision_opav() {
        return comision_opav;
    }

    /**
     * @param comision_opav the comision_opav to set
     */
    public void setComision_opav(double comision_opav) {
        this.comision_opav = comision_opav;
    }

    /**
     * @return the comision_fintra
     */
    public double getComision_fintra() {
        return comision_fintra;
    }

    /**
     * @param comision_fintra the comision_fintra to set
     */
    public void setComision_fintra(double comision_fintra) {
        this.comision_fintra = comision_fintra;
    }

    /**
     * @return the comision_interventoria
     */
    public double getComision_interventoria() {
        return comision_interventoria;
    }

    /**
     * @param comision_interventoria the comision_interventoria to set
     */
    public void setComision_interventoria(double comision_interventoria) {
        this.comision_interventoria = comision_interventoria;
    }

    /**
     * @return the comision_provintegral
     */
    public double getComision_provintegral() {
        return comision_provintegral;
    }

    /**
     * @param comision_provintegral the comision_provintegral to set
     */
    public void setComision_provintegral(double comision_provintegral) {
        this.comision_provintegral = comision_provintegral;
    }

    /**
     * @return the comision_eca
     */
    public double getComision_eca() {
        return comision_eca;
    }

    /**
     * @param comision_eca the comision_eca to set
     */
    public void setComision_eca(double comision_eca) {
        this.comision_eca = comision_eca;
    }

    /**
     * @return the iva_comision_opav
     */
    public double getIva_comision_opav() {
        return iva_comision_opav;
    }

    /**
     * @param iva_comision_opav the iva_comision_opav to set
     */
    public void setIva_comision_opav(double iva_comision_opav) {
        this.iva_comision_opav = iva_comision_opav;
    }

    /**
     * @return the iva_comision_fintra
     */
    public double getIva_comision_fintra() {
        return iva_comision_fintra;
    }

    /**
     * @param iva_comision_fintra the iva_comision_fintra to set
     */
    public void setIva_comision_fintra(double iva_comision_fintra) {
        this.iva_comision_fintra = iva_comision_fintra;
    }

    /**
     * @return the iva_comision_interventoria
     */
    public double getIva_comision_interventoria() {
        return iva_comision_interventoria;
    }

    /**
     * @param iva_comision_interventoria the iva_comision_interventoria to set
     */
    public void setIva_comision_interventoria(double iva_comision_interventoria) {
        this.iva_comision_interventoria = iva_comision_interventoria;
    }

    /**
     * @return the iva_comision_provintegral
     */
    public double getIva_comision_provintegral() {
        return iva_comision_provintegral;
    }

    /**
     * @param iva_comision_provintegral the iva_comision_provintegral to set
     */
    public void setIva_comision_provintegral(double iva_comision_provintegral) {
        this.iva_comision_provintegral = iva_comision_provintegral;
    }

    /**
     * @return the iva_comision_eca
     */
    public double getIva_comision_eca() {
        return iva_comision_eca;
    }

    /**
     * @param iva_comision_eca the iva_comision_eca to set
     */
    public void setIva_comision_eca(double iva_comision_eca) {
        this.iva_comision_eca = iva_comision_eca;
    }

    /**
     * @return the valor_a_financiar
     */
    public double getValor_a_financiar() {
        return valor_a_financiar;
    }

    /**
     * @param valor_a_financiar the valor_a_financiar to set
     */
    public void setValor_a_financiar(double valor_a_financiar) {
        this.valor_a_financiar = valor_a_financiar;
    }

    /**
     * @return the subtotal_iva
     */
    public double getSubtotal_iva() {
        return subtotal_iva;
    }

    /**
     * @param subtotal_iva the subtotal_iva to set
     */
    public void setSubtotal_iva(double subtotal_iva) {
        this.subtotal_iva = subtotal_iva;
    }

    /**
     * @return the valor_a_financiar_sin_iva
     */
    public double getValor_a_financiar_sin_iva() {
        return valor_a_financiar_sin_iva;
    }

    /**
     * @param valor_a_financiar_sin_iva the valor_a_financiar_sin_iva to set
     */
    public void setValor_a_financiar_sin_iva(double valor_a_financiar_sin_iva) {
        this.valor_a_financiar_sin_iva = valor_a_financiar_sin_iva;
    }

    /**
     * @return the id_accion
     */
    public String getId_accion() {
        return id_accion;
    }

    /**
     * @param id_accion the id_accion to set
     */
    public void setId_accion(String id_accion) {
        this.id_accion = id_accion;
    }

    /**
     * @return the contratista
     */
    public String getContratista() {
        return contratista;
    }

    /**
     * @param contratista the contratista to set
     */
    public void setContratista(String contratista) {
        this.contratista = contratista;
    }

    /**
     * @return the nombre_contratista
     */
    public String getNombre_contratista() {
        return nombre_contratista;
    }

    /**
     * @param nombre_contratista the nombre_contratista to set
     */
    public void setNombre_contratista(String nombre_contratista) {
        this.nombre_contratista = nombre_contratista;
    }







}