/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.opav.model.beans;

/**
 *
 * @author Alvaro
 */
public class AccionPorSolicitud {

    private String id_solicitud;
    private int    parcial;
    private String cliente_primario;
    private String nombre;
    private String id_accion;
    private String contratista;
    private String nombre_contratista;
    private double material;
    private double mano_obra;
    private double otros;
    private double administracion;
    private double imprevisto;
    private double utilidad;
    private double costo_contratista;
    private double base_iva_contratista;
    private double iva_contratista;
    private double bonificacion;
    private double iva_bonificacion;
    private double comision_opav;
    private double comision_fintra;
    private double comision_interventoria;
    private double comision_provintegral;
    private double comision_eca;
    private double iva_comision_opav;
    private double iva_comision_fintra;
    private double iva_comision_interventoria;
    private double iva_comision_provintegral;
    private double iva_comision_eca;
    private String tipo_distribucion;
    private int indicador_valor_agregado;
    private String es_valor_agregado;
    private double porc_opav;
    private double porc_fintra;
    private double porc_interventoria;
    private double porc_provintegral;
    private double porc_eca;
    private double porc_iva;
    private double valor_a_financiar;

    private double subtotal_iva;
    private double valor_a_financiar_sin_iva;




    /** Creates a new instance of AccionPorSolicitud */
    public AccionPorSolicitud() {
    }


    /** Extrae un registro de la BD correspondientes a las acciones de una solicitudes que no esta facturada */
    public static AccionPorSolicitud load(java.sql.ResultSet rs)throws java.sql.SQLException{

        AccionPorSolicitud accionPorFacturar = new AccionPorSolicitud();

        accionPorFacturar.setId_solicitud( rs.getString("id_solicitud") );
        accionPorFacturar.setParcial( rs.getInt("parcial") );
        accionPorFacturar.setCliente_primario( rs.getString("cliente_primario") );
        accionPorFacturar.setNombre( rs.getString("nombre") );
        accionPorFacturar.setId_accion( rs.getString("id_accion") );
        accionPorFacturar.setContratista( rs.getString("contratista") );
        accionPorFacturar.setNombre_contratista( rs.getString("nombre_contratista") );
        accionPorFacturar.setMaterial( rs.getDouble("material") );
        accionPorFacturar.setMano_obra( rs.getDouble("mano_obra") );
        accionPorFacturar.setOtros( rs.getDouble("otros") );
        accionPorFacturar.setAdministracion( rs.getDouble("administracion") );
        accionPorFacturar.setImprevisto( rs.getDouble("imprevisto") );
        accionPorFacturar.setUtilidad( rs.getDouble("utilidad") );
        accionPorFacturar.setCosto_contratista( rs.getDouble("costo_contratista") );
        accionPorFacturar.setBase_iva_contratista( rs.getDouble("base_iva_contratista") );
        accionPorFacturar.setIva_contratista( rs.getDouble("iva_contratista") );
        accionPorFacturar.setBonificacion( rs.getDouble("bonificacion") );
        accionPorFacturar.setIva_bonificacion( rs.getDouble("iva_bonificacion") );
        accionPorFacturar.setComision_opav( rs.getDouble("comision_opav") );
        accionPorFacturar.setComision_fintra( rs.getDouble("comision_fintra") );
        accionPorFacturar.setComision_interventoria( rs.getDouble("comision_interventoria") );
        accionPorFacturar.setComision_provintegral( rs.getDouble("comision_provintegral") );
        accionPorFacturar.setComision_eca( rs.getDouble("comision_eca") );
        accionPorFacturar.setIva_comision_opav( rs.getDouble("iva_comision_opav") );
        accionPorFacturar.setIva_comision_fintra( rs.getDouble("iva_comision_fintra") );
        accionPorFacturar.setIva_comision_interventoria( rs.getDouble("iva_comision_interventoria") );
        accionPorFacturar.setIva_comision_provintegral( rs.getDouble("iva_comision_provintegral") );
        accionPorFacturar.setIva_comision_eca( rs.getDouble("iva_comision_eca") );
        accionPorFacturar.setTipo_distribucion( rs.getString("tipo_distribucion") );
        accionPorFacturar.setIndicador_valor_agregado( rs.getInt("indicador_valor_agregado") );
        accionPorFacturar.setEs_valor_agregado( rs.getString("es_valor_agregado") );
        accionPorFacturar.setPorc_opav( rs.getDouble("porc_opav") );
        accionPorFacturar.setPorc_fintra( rs.getDouble("porc_fintra") );
        accionPorFacturar.setPorc_interventoria( rs.getDouble("porc_interventoria") );
        accionPorFacturar.setPorc_provintegral( rs.getDouble("porc_provintegral") );
        accionPorFacturar.setPorc_eca( rs.getDouble("porc_eca") );
        accionPorFacturar.setPorc_iva( rs.getDouble("porc_iva") );







        double valor =  accionPorFacturar.getCosto_contratista() +
                        // accionPorFacturar.getBonificacion() + //20100207
                        accionPorFacturar.getComision_eca() +
                        accionPorFacturar.getComision_fintra() +
                        accionPorFacturar.getComision_interventoria() +
                        accionPorFacturar.getComision_opav() +
                        accionPorFacturar.getComision_provintegral() +
                        accionPorFacturar.getIva_bonificacion() + //20100207 sr
                        accionPorFacturar.getIva_comision_eca() +
                        accionPorFacturar.getIva_comision_fintra() +
                        accionPorFacturar.getIva_comision_interventoria() +
                        accionPorFacturar.getIva_comision_opav() +
                        accionPorFacturar.getIva_contratista() +
                        accionPorFacturar.getIva_comision_provintegral() ;

        accionPorFacturar.setValor_a_financiar(valor);

        valor =  accionPorFacturar.getIva_bonificacion() + //20100207 sr
                 accionPorFacturar.getIva_comision_eca() +
                 accionPorFacturar.getIva_comision_opav() +
                 accionPorFacturar.getIva_comision_fintra() +
                 accionPorFacturar.getIva_comision_interventoria() +
                 accionPorFacturar.getIva_contratista() +
                 accionPorFacturar.getIva_comision_provintegral();

        accionPorFacturar.setSubtotal_iva(valor);

        valor =  accionPorFacturar.getCosto_contratista() +
                 // accionPorFacturar.getBonificacion() + //20100207
                 accionPorFacturar.getComision_eca() +
                 accionPorFacturar.getComision_fintra() +
                 accionPorFacturar.getComision_interventoria() +
                 accionPorFacturar.getComision_opav() +
                 accionPorFacturar.getComision_provintegral()  ;

        accionPorFacturar.setValor_a_financiar_sin_iva(valor);



        return accionPorFacturar;

    }








    /**
     * @return the id_solicitud
     */
    public String getId_solicitud() {
        return id_solicitud;
    }

    /**
     * @param id_solicitud the id_solicitud to set
     */
    public void setId_solicitud(String id_solicitud) {
        this.id_solicitud = id_solicitud;
    }

    /**
     * @return the cliente_primario
     */
    public String getCliente_primario() {
        return cliente_primario;
    }

    /**
     * @param cliente_primario the cliente_primario to set
     */
    public void setCliente_primario(String cliente_primario) {
        this.cliente_primario = cliente_primario;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the id_accion
     */
    public String getId_accion() {
        return id_accion;
    }

    /**
     * @param id_accion the id_accion to set
     */
    public void setId_accion(String id_accion) {
        this.id_accion = id_accion;
    }

    /**
     * @return the contratista
     */
    public String getContratista() {
        return contratista;
    }

    /**
     * @param contratista the contratista to set
     */
    public void setContratista(String contratista) {
        this.contratista = contratista;
    }

    /**
     * @return the nombre_contratista
     */
    public String getNombre_contratista() {
        return nombre_contratista;
    }

    /**
     * @param nombre_contratista the nombre_contratista to set
     */
    public void setNombre_contratista(String nombre_contratista) {
        this.nombre_contratista = nombre_contratista;
    }

    /**
     * @return the material
     */
    public double getMaterial() {
        return material;
    }

    /**
     * @param material the material to set
     */
    public void setMaterial(double material) {
        this.material = material;
    }

    /**
     * @return the mano_obra
     */
    public double getMano_obra() {
        return mano_obra;
    }

    /**
     * @param mano_obra the mano_obra to set
     */
    public void setMano_obra(double mano_obra) {
        this.mano_obra = mano_obra;
    }

    /**
     * @return the otros
     */
    public double getOtros() {
        return otros;
    }

    /**
     * @param otros the otros to set
     */
    public void setOtros(double otros) {
        this.otros = otros;
    }

    /**
     * @return the administracion
     */
    public double getAdministracion() {
        return administracion;
    }

    /**
     * @param administracion the administracion to set
     */
    public void setAdministracion(double administracion) {
        this.administracion = administracion;
    }

    /**
     * @return the imprevisto
     */
    public double getImprevisto() {
        return imprevisto;
    }

    /**
     * @param imprevisto the imprevisto to set
     */
    public void setImprevisto(double imprevisto) {
        this.imprevisto = imprevisto;
    }

    /**
     * @return the utilidad
     */
    public double getUtilidad() {
        return utilidad;
    }

    /**
     * @param utilidad the utilidad to set
     */
    public void setUtilidad(double utilidad) {
        this.utilidad = utilidad;
    }

    /**
     * @return the costo_contratista
     */
    public double getCosto_contratista() {
        return costo_contratista;
    }

    /**
     * @param costo_contratista the costo_contratista to set
     */
    public void setCosto_contratista(double costo_contratista) {
        this.costo_contratista = costo_contratista;
    }

    /**
     * @return the base_iva_contratista
     */
    public double getBase_iva_contratista() {
        return base_iva_contratista;
    }

    /**
     * @param base_iva_contratista the base_iva_contratista to set
     */
    public void setBase_iva_contratista(double base_iva_contratista) {
        this.base_iva_contratista = base_iva_contratista;
    }

    /**
     * @return the iva_contratista
     */
    public double getIva_contratista() {
        return iva_contratista;
    }

    /**
     * @param iva_contratista the iva_contratista to set
     */
    public void setIva_contratista(double iva_contratista) {
        this.iva_contratista = iva_contratista;
    }

    /**
     * @return the bonificacion
     */
    public double getBonificacion() {
        return bonificacion;
    }

    /**
     * @param bonificacion the bonificacion to set
     */
    public void setBonificacion(double bonificacion) {
        this.bonificacion = bonificacion;
    }

    /**
     * @return the iva_bonificacion
     */
    public double getIva_bonificacion() {
        return iva_bonificacion;
    }

    /**
     * @param iva_bonificacion the iva_bonificacion to set
     */
    public void setIva_bonificacion(double iva_bonificacion) {
        this.iva_bonificacion = iva_bonificacion;
    }

    /**
     * @return the comision_opav
     */
    public double getComision_opav() {
        return comision_opav;
    }

    /**
     * @param comision_opav the comision_opav to set
     */
    public void setComision_opav(double comision_opav) {
        this.comision_opav = comision_opav;
    }

    /**
     * @return the comision_fintra
     */
    public double getComision_fintra() {
        return comision_fintra;
    }

    /**
     * @param comision_fintra the comision_fintra to set
     */
    public void setComision_fintra(double comision_fintra) {
        this.comision_fintra = comision_fintra;
    }

    /**
     * @return the comision_interventoria
     */
    public double getComision_interventoria() {
        return comision_interventoria;
    }

    /**
     * @param comision_interventoria the comision_interventoria to set
     */
    public void setComision_interventoria(double comision_interventoria) {
        this.comision_interventoria = comision_interventoria;
    }

    /**
     * @return the comision_provintegral
     */
    public double getComision_provintegral() {
        return comision_provintegral;
    }

    /**
     * @param comision_provintegral the comision_provintegral to set
     */
    public void setComision_provintegral(double comision_provintegral) {
        this.comision_provintegral = comision_provintegral;
    }

    /**
     * @return the comision_eca
     */
    public double getComision_eca() {
        return comision_eca;
    }

    /**
     * @param comision_eca the comision_eca to set
     */
    public void setComision_eca(double comision_eca) {
        this.comision_eca = comision_eca;
    }

    /**
     * @return the iva_comision_opav
     */
    public double getIva_comision_opav() {
        return iva_comision_opav;
    }

    /**
     * @param iva_comision_opav the iva_comision_opav to set
     */
    public void setIva_comision_opav(double iva_comision_opav) {
        this.iva_comision_opav = iva_comision_opav;
    }

    /**
     * @return the iva_comision_fintra
     */
    public double getIva_comision_fintra() {
        return iva_comision_fintra;
    }

    /**
     * @param iva_comision_fintra the iva_comision_fintra to set
     */
    public void setIva_comision_fintra(double iva_comision_fintra) {
        this.iva_comision_fintra = iva_comision_fintra;
    }

    /**
     * @return the iva_comision_interventoria
     */
    public double getIva_comision_interventoria() {
        return iva_comision_interventoria;
    }

    /**
     * @param iva_comision_interventoria the iva_comision_interventoria to set
     */
    public void setIva_comision_interventoria(double iva_comision_interventoria) {
        this.iva_comision_interventoria = iva_comision_interventoria;
    }

    /**
     * @return the iva_comision_proventegral
     */
    public double getIva_comision_provintegral() {
        return iva_comision_provintegral;
    }

    /**
     * @param iva_comision_proventegral the iva_comision_proventegral to set
     */
    public void setIva_comision_provintegral(double iva_comision_proventegral) {
        this.iva_comision_provintegral = iva_comision_proventegral;
    }

    /**
     * @return the iva_comision_eca
     */
    public double getIva_comision_eca() {
        return iva_comision_eca;
    }

    /**
     * @param iva_comision_eca the iva_comision_eca to set
     */
    public void setIva_comision_eca(double iva_comision_eca) {
        this.iva_comision_eca = iva_comision_eca;
    }

    /**
     * @return the tipo_distribucion
     */
    public String getTipo_distribucion() {
        return tipo_distribucion;
    }

    /**
     * @param tipo_distribucion the tipo_distribucion to set
     */
    public void setTipo_distribucion(String tipo_distribucion) {
        this.tipo_distribucion = tipo_distribucion;
    }

    /**
     * @return the indicador_valor_agregado
     */
    public int getIndicador_valor_agregado() {
        return indicador_valor_agregado;
    }

    /**
     * @param indicador_valor_agregado the indicador_valor_agregado to set
     */
    public void setIndicador_valor_agregado(int indicador_valor_agregado) {
        this.indicador_valor_agregado = indicador_valor_agregado;
    }

    /**
     * @return the es_valor_agregado
     */
    public String getEs_valor_agregado() {
        return es_valor_agregado;
    }

    /**
     * @param es_valor_agregado the es_valor_agregado to set
     */
    public void setEs_valor_agregado(String es_valor_agregado) {
        this.es_valor_agregado = es_valor_agregado;
    }

    /**
     * @return the porc_opav
     */
    public double getPorc_opav() {
        return porc_opav;
    }

    /**
     * @param porc_opav the porc_opav to set
     */
    public void setPorc_opav(double porc_opav) {
        this.porc_opav = porc_opav;
    }

    /**
     * @return the porc_fintra
     */
    public double getPorc_fintra() {
        return porc_fintra;
    }

    /**
     * @param porc_fintra the porc_fintra to set
     */
    public void setPorc_fintra(double porc_fintra) {
        this.porc_fintra = porc_fintra;
    }

    /**
     * @return the porc_interventoria
     */
    public double getPorc_interventoria() {
        return porc_interventoria;
    }

    /**
     * @param porc_interventoria the porc_interventoria to set
     */
    public void setPorc_interventoria(double porc_interventoria) {
        this.porc_interventoria = porc_interventoria;
    }

    /**
     * @return the porc_provintegral
     */
    public double getPorc_provintegral() {
        return porc_provintegral;
    }

    /**
     * @param porc_provintegral the porc_provintegral to set
     */
    public void setPorc_provintegral(double porc_provintegral) {
        this.porc_provintegral = porc_provintegral;
    }

    /**
     * @return the porc_eca
     */
    public double getPorc_eca() {
        return porc_eca;
    }

    /**
     * @param porc_eca the porc_eca to set
     */
    public void setPorc_eca(double porc_eca) {
        this.porc_eca = porc_eca;
    }

    /**
     * @return the porc_iva
     */
    public double getPorc_iva() {
        return porc_iva;
    }

    /**
     * @param porc_iva the porc_iva to set
     */
    public void setPorc_iva(double porc_iva) {
        this.porc_iva = porc_iva;
    }

    /**
     * @return the valor_a_financiar
     */
    public double getValor_a_financiar() {
        return valor_a_financiar;
    }

    /**
     * @param valor_a_financiar the valor_a_financiar to set
     */
    public void setValor_a_financiar(double valor_a_financiar) {
        this.valor_a_financiar = valor_a_financiar;
    }

    /**
     * @return the parcial
     */
    public int getParcial() {
        return parcial;
    }

    /**
     * @param parcial the parcial to set
     */
    public void setParcial(int parcial) {
        this.parcial = parcial;
    }

    /**
     * @return the subtotal_iva
     */
    public double getSubtotal_iva() {
        return subtotal_iva;
    }

    /**
     * @param subtotal_iva the subtotal_iva to set
     */
    public void setSubtotal_iva(double subtotal_iva) {
        this.subtotal_iva = subtotal_iva;
    }

    /**
     * @return the valor_a_financiar_sin_iva
     */
    public double getValor_a_financiar_sin_iva() {
        return valor_a_financiar_sin_iva;
    }

    /**
     * @param valor_a_financiar_sin_iva the valor_a_financiar_sin_iva to set
     */
    public void setValor_a_financiar_sin_iva(double valor_a_financiar_sin_iva) {
        this.valor_a_financiar_sin_iva = valor_a_financiar_sin_iva;
    }




}