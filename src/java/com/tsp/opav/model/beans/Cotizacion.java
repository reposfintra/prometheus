/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.opav.model.beans;

/**
 *
 * @author dev1
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

public class Cotizacion {

    private String codigo;
    private String fecha;
    private String material;
    private String idAccion;
    private double cantidad;//100226
    private String observacion;
    private String id;//091222
    private String compra;//JJCastro fase2
    private double cantidad_compra;//JJCastro fase2
    private double valor;//Gratis1

    private String tipo_material;
    private String descripcion_categoria;
    private String descripcion_material;
    private double precioen_material;
    private int idcategoria;//COTIZACION NUEVA JCASTRO
    private String aprobado;
   private double cantidadComprada;
   private String oficial;

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }
    public Cotizacion() {
        this.cantidad = 0;
        this.codigo = "";
        this.fecha = "0099-01-01 00:00:00";
        this.idAccion = "";
        this.material = "";
        this.observacion = "Ninguna";
        this.id = "";//091222
        this.compra = "S";//JJCastro fase2
        this.cantidad_compra = 0;//JJCastro fase2
        this.valor = 0;//Gratis1
        this.precioen_material = 0;
        this.oficial="";
    }

    public void setCodigo(String cod) {
        this.codigo = cod;
    }

    public String getCodigo() {
        return this.codigo;
    }

    public String getAprobado() {
        return aprobado;
    }

    public void setAprobado(String aprobado) {
        this.aprobado = aprobado;
    }

    public void setFecha(String dat) {
        this.fecha = dat;
    }

    public String getFecha() {
        return this.fecha;
    }

    public void setMaterial(String mat) {
        this.material = mat;
    }

    public String getMaterial() {
        return this.material;
    }

    public void setAccion(String accion) {
        this.idAccion = accion;
    }

    public String getAccion() {
        return this.idAccion;
    }

    public void setObservacion(String nota) {
        this.observacion = nota;
    }

    public String getObservacion() {
        return this.observacion;
    }

    public void setCantidad(double cant) {//100226
        this.cantidad = cant;
    }

    public double getCantidad() {//100226
        return this.cantidad;
    }

    public void setId(String idx) {
        this.id = idx;
    }

    public String getId() {
        return this.id;
    }

    public double getCantidad_compra() {
        return cantidad_compra;
    }

    public void setCantidad_compra(double cantidad_compra) {
        this.cantidad_compra = cantidad_compra;
    }

    public String getCompra() {
        return compra;
    }

    public void setCompra(String compra) {
        this.compra = compra;
    }

    public double getPrecioen_material() {
        return precioen_material;
    }

    public void setPrecioen_material(double precioen_material) {
        this.precioen_material = precioen_material;
    }

    public String getDescripcion_material() {
        return descripcion_material;
    }

    public void setDescripcion_material(String descripcion_material) {
        this.descripcion_material = descripcion_material;
    }

    public String getDescripcion_categoria() {
        return descripcion_categoria;
    }

    public void setDescripcion_categoria(String descripcion_categoria) {
        this.descripcion_categoria = descripcion_categoria;
    }

    public String getTipo_material() {
        return tipo_material;
    }

    public void setTipo_material(String tipo_material) {
        this.tipo_material = tipo_material;
    }

    public int getIdcategoria() {
        return idcategoria;
    }

    public void setIdcategoria(int idcategoria) {
        this.idcategoria = idcategoria;
    }
   /**
     * Devuleve el valor de la cantidad comprada
     * @author ivargas
     * @return double con la cantidad comprada
     * @since 2011-02-05
     */
    public double getCantidadComprada() {
        return cantidadComprada;
    }

   /**
     * Asigna el valor para la cantidad comprada
     * @author ivargas
     * @param cantidadComprada el valor a asignar
     * @since 2011-02-05
     */
    public void setCantidadComprada(double cantidadComprada) {
        this.cantidadComprada = cantidadComprada;
    }

    /**
     * @return the oficial
     */
    public String getOficial() {
        return oficial;
    }

    /**
     * @param oficial the oficial to set
     */
    public void setOficial(String oficial) {
        this.oficial = oficial;
    }

}
