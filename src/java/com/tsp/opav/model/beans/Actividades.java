package com.tsp.opav.model.beans;

/**
 * Actividades.java<br/>
 * Bean para la tabla actividades, guarda las actividades de una accion<br/>
 * 4/06/2010
 * @author darrieta-GEOTECH
 * @version 1.0
 */
public class Actividades {

    private int id;
    private String descripcion;
    private String tipo;
    private int peso;
    private String id_accion;
    private String responsable;
    private String fechaInicial;
    private String fechaFinal;
    private String reg_status;
    private String creation_user;
    private String user_update;
    private String descAccion;
    private float peso_obra;
    private String fecha;
    private float avance;
    private String id_solicitud;
    private int dias_actividad;
    private int dias_actual;
    private float avance_anterior;
    private float avance_esperado;
    private int orden_predeterminado;
    private String observaciones;     //JPACOSTA, Dic 2013

    /**
     * Get the value of orden_predeterminado
     *
     * @return the value of orden_predeterminado
     */
    public int getOrden_predeterminado() {
        return orden_predeterminado;
    }

    /**
     * Set the value of orden_predeterminado
     *
     * @param orden_predeterminado new value of orden_predeterminado
     */
    public void setOrden_predeterminado(int orden_predeterminado) {
        this.orden_predeterminado = orden_predeterminado;
    }


    /**
     * Get the value of avance_esperado
     *
     * @return the value of avance_esperado
     */
    public float getAvance_esperado() {
        return avance_esperado;
    }

    /**
     * Set the value of avance_esperado
     *
     * @param avance_esperado new value of avance_esperado
     */
    public void setAvance_esperado(float avance_esperado) {
        this.avance_esperado = avance_esperado;
    }

    /**
     * Get the value of avance_anterior
     *
     * @return the value of avance_anterior
     */
    public float getAvance_anterior() {
        return avance_anterior;
    }

    /**
     * Set the value of avance_anterior
     *
     * @param avance_anterior new value of avance_anterior
     */
    public void setAvance_anterior(float avance_anterior) {
        this.avance_anterior = avance_anterior;
    }

    /**
     * Get the value of dias_actual
     *
     * @return the value of dias_actual
     */
    public int getDias_actual() {
        return dias_actual;
    }

    /**
     * Set the value of dias_actual
     *
     * @param dias_actual new value of dias_actual
     */
    public void setDias_actual(int dias_actual) {
        this.dias_actual = dias_actual;
    }

    /**
     * Get the value of dias_actividad
     *
     * @return the value of dias_actividad
     */
    public int getDias_actividad() {
        return dias_actividad;
    }

    /**
     * Set the value of dias_actividad
     *
     * @param dias_actividad new value of dias_actividad
     */
    public void setDias_actividad(int dias_actividad) {
        this.dias_actividad = dias_actividad;
    }


    /**
     * Get the value of id_solicitud
     *
     * @return the value of id_solicitud
     */
    public String getId_solicitud() {
        return id_solicitud;
    }

    /**
     * Set the value of id_solicitud
     *
     * @param id_solicitud new value of id_solicitud
     */
    public void setId_solicitud(String id_solicitud) {
        this.id_solicitud = id_solicitud;
    }

    /**
     * Get the value of avance
     *
     * @return the value of avance
     */
    public float getAvance() {
        return avance;
    }

    /**
     * Set the value of avance
     *
     * @param avance new value of avance
     */
    public void setAvance(float avance) {
        this.avance = avance;
    }

    /**
     * Get the value of fecha
     *
     * @return the value of fecha
     */
    public String getFecha() {
        return fecha;
    }

    /**
     * Set the value of fecha
     *
     * @param fecha new value of fecha
     */
    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    /**
     * Get the value of peso_obra
     *
     * @return the value of peso_obra
     */
    public float getPeso_obra() {
        return peso_obra;
    }

    /**
     * Set the value of peso_obra
     *
     * @param peso_obra new value of peso_obra
     */
    public void setPeso_obra(float peso_obra) {
        this.peso_obra = peso_obra;
    }


    /**
     * Get the value of descAccion
     *
     * @return the value of descAccion
     */
    public String getDescAccion() {
        return descAccion;
    }

    /**
     * Set the value of descAccion
     *
     * @param descAccion new value of descAccion
     */
    public void setDescAccion(String descAccion) {
        this.descAccion = descAccion;
    }


    /**
     * Get the value of user_update
     *
     * @return the value of user_update
     */
    public String getUser_update() {
        return user_update;
    }

    /**
     * Set the value of user_update
     *
     * @param user_update new value of user_update
     */
    public void setUser_update(String user_update) {
        this.user_update = user_update;
    }

    /**
     * Get the value of creation_user
     *
     * @return the value of creation_user
     */
    public String getCreation_user() {
        return creation_user;
    }

    /**
     * Set the value of creation_user
     *
     * @param creation_user new value of creation_user
     */
    public void setCreation_user(String creation_user) {
        this.creation_user = creation_user;
    }

    /**
     * Get the value of reg_status
     *
     * @return the value of reg_status
     */
    public String getReg_status() {
        return reg_status;
    }

    /**
     * Set the value of reg_status
     *
     * @param reg_status new value of reg_status
     */
    public void setReg_status(String reg_status) {
        this.reg_status = reg_status;
    }

    /**
     * Get the value of fechaFinal
     *
     * @return the value of fechaFinal
     */
    public String getFechaFinal() {
        return fechaFinal;
    }

    /**
     * Set the value of fechaFinal
     *
     * @param fechaFinal new value of fechaFinal
     */
    public void setFechaFinal(String fechaFinal) {
        this.fechaFinal = fechaFinal;
    }

    /**
     * Get the value of fechaInicial
     *
     * @return the value of fechaInicial
     */
    public String getFechaInicial() {
        return fechaInicial;
    }

    /**
     * Set the value of fechaInicial
     *
     * @param fechaInicial new value of fechaInicial
     */
    public void setFechaInicial(String fechaInicial) {
        this.fechaInicial = fechaInicial;
    }

    /**
     * Get the value of responsable
     *
     * @return the value of responsable
     */
    public String getResponsable() {
        return responsable;
    }

    /**
     * Set the value of responsable
     *
     * @param responsable new value of responsable
     */
    public void setResponsable(String responsable) {
        this.responsable = responsable;
    }

    /**
     * Get the value of id_accion
     *
     * @return the value of id_accion
     */
    public String getId_accion() {
        return id_accion;
    }

    /**
     * Set the value of id_accion
     *
     * @param id_accion new value of id_accion
     */
    public void setId_accion(String id_accion) {
        this.id_accion = id_accion;
    }


    /**
     * Get the value of pesoPredeterminado
     *
     * @return the value of pesoPredeterminado
     */
    public int getPeso() {
        return peso;
    }

    /**
     * Set the value of pesoPredeterminado
     *
     * @param pesoPredeterminado new value of pesoPredeterminado
     */
    public void setPeso(int peso) {
        this.peso = peso;
    }

    /**
     * Get the value of tipo
     *
     * @return the value of tipo
     */
    public String getTipo() {
        return tipo;
    }

    /**
     * Set the value of tipo
     *
     * @param tipo new value of tipo
     */
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    /**
     * Get the value of descripcion
     *
     * @return the value of descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * Set the value of descripcion
     *
     * @param descripcion new value of descripcion
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }


    /**
     * Get the value of id
     *
     * @return the value of id
     */
    public int getId() {
        return id;
    }

    /**
     * Set the value of id
     *
     * @param id new value of id
     */
    public void setId(int id) {
        this.id = id;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

}