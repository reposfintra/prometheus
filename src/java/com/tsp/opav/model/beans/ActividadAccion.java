/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * ActividadAccion.java : manejadora de las actividades de las acciones de las solicitudes
 * Copyright 2010 Rhonalf Martinez Villa <rhonaldomaster@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.tsp.opav.model.beans;

/**
 *
 * @author rhonalf
 */
public class ActividadAccion {

    private int id;
    private String tipo;
    private String descripcion;
    private int peso_predeterminado;
    private int orden_predeterminado;

    private String responsable_predeterminado;

    public ActividadAccion(){
        this.descripcion="";
        this.id=0;
        this.orden_predeterminado=0;
        this.peso_predeterminado=0;
        this.tipo="";
        this.responsable_predeterminado = "";
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getOrden_predeterminado() {
        return orden_predeterminado;
    }

    public void setOrden_predeterminado(int orden_predeterminado) {
        this.orden_predeterminado = orden_predeterminado;
    }

    public int getPeso_predeterminado() {
        return peso_predeterminado;
    }

    public void setPeso_predeterminado(int peso_predeterminado) {
        this.peso_predeterminado = peso_predeterminado;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getResponsable_predeterminado() {
        return responsable_predeterminado;
    }

    public void setResponsable_predeterminado(String responsable_predeterminado) {
        this.responsable_predeterminado = responsable_predeterminado;
    }

}