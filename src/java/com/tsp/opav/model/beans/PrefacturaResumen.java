/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.opav.model.beans;

/**
 *
 * @author Alvaro
 */
public class PrefacturaResumen {

  private String  id_contratista ;
  private String  prefactura;
  private String  fecha_prefactura;
  private double  total_prev1;
  private double  bonificacion;
  private double  vlr_iva;
  private double  vlr_rmat;
  private double  vlr_rmob;
  private double  vlr_rotr;
  private double  vlr_retencion;
  private double  vlr_mat;
  private double  vlr_mob;
  private double  vlr_otr;
  private double  vlr_factoring;
  private double  vlr_formula;
  private String  factura_conformada;
  private double  vlr_administracion;
  private double  vlr_imprevisto;
  private double  vlr_utilidad;
  private double  vlr_base_iva;
  private double  vlr_formula_provintegral;


    /** Creates a new instance of PrefacturaResumen */
    public PrefacturaResumen() {
    }


    public static PrefacturaResumen load(java.sql.ResultSet rs)throws java.sql.SQLException{

        PrefacturaResumen prefacturaResumen = new PrefacturaResumen();

        prefacturaResumen.setId_contratista( rs.getString("id_contratista") );
        prefacturaResumen.setPrefactura( rs.getString("prefactura") );
        prefacturaResumen.setFecha_prefactura( rs.getString("fecha_prefactura") );
        prefacturaResumen.setTotal_prev1( rs.getDouble("total_prev1") );
        prefacturaResumen.setBonificacion(rs.getDouble("bonificacion"));
        prefacturaResumen.setVlr_iva(rs.getDouble("vlr_iva"));
        prefacturaResumen.setVlr_rmat(rs.getDouble("vlr_rmat"));
        prefacturaResumen.setVlr_rmob(rs.getDouble("vlr_rmob"));
        prefacturaResumen.setVlr_rotr(rs.getDouble("vlr_rotr"));
        prefacturaResumen.setVlr_retencion(rs.getDouble("vlr_retencion"));
        prefacturaResumen.setVlr_mat(rs.getDouble("vlr_mat"));
        prefacturaResumen.setVlr_mob(rs.getDouble("vlr_mob"));
        prefacturaResumen.setVlr_otr(rs.getDouble("vlr_otr"));
        prefacturaResumen.setVlr_factoring(rs.getDouble("vlr_factoring"));
        prefacturaResumen.setVlr_formula(rs.getDouble("vlr_formula"));

        prefacturaResumen.setFacturaConformada(rs.getString("fact_conformada"));

        prefacturaResumen.setVlr_administracion(rs.getDouble("administracion"));
        prefacturaResumen.setVlr_imprevisto(rs.getDouble("imprevisto"));
        prefacturaResumen.setVlr_utilidad(rs.getDouble("utilidad"));
        prefacturaResumen.setVlr_base_iva(rs.getDouble("base_iva"));

        prefacturaResumen.setVlr_formula_provintegral(rs.getDouble("vlr_formula_provintegral"));





        return prefacturaResumen;
    }



    /**
     * @return the id_contratista
     */
    public String getId_contratista() {
        return id_contratista;
    }

    /**
     * @param id_contratista the id_contratista to set
     */
    public void setId_contratista(String id_contratista) {
        this.id_contratista = id_contratista;
    }

    /**
     * @return the prefactura
     */
    public String getPrefactura() {
        return prefactura;
    }

    /**
     * @param prefactura the prefactura to set
     */
    public void setPrefactura(String prefactura) {
        this.prefactura = prefactura;
    }

    /**
     * @return the fecha_prefactura
     */
    public String getFecha_prefactura() {
        return fecha_prefactura;
    }

    /**
     * @param fecha_prefactura the fecha_prefactura to set
     */
    public void setFecha_prefactura(String fecha_prefactura) {
        this.fecha_prefactura = fecha_prefactura;
    }

    /**
     * @return the total_prev1
     */
    public double getTotal_prev1() {
        return total_prev1;
    }

    /**
     * @param total_prev1 the total_prev1 to set
     */
    public void setTotal_prev1(double total_prev1) {
        this.total_prev1 = total_prev1;
    }

    /**
     * @return the bonificacion
     */
    public double getBonificacion() {
        return bonificacion;
    }

    /**
     * @param bonificacion the bonificacion to set
     */
    public void setBonificacion(double bonificacion) {
        this.bonificacion = bonificacion;
    }

    /**
     * @return the vlr_iva
     */
    public double getVlr_iva() {
        return vlr_iva;
    }

    /**
     * @param vlr_iva the vlr_iva to set
     */
    public void setVlr_iva(double vlr_iva) {
        this.vlr_iva = vlr_iva;
    }

    /**
     * @return the vlr_rmat
     */
    public double getVlr_rmat() {
        return vlr_rmat;
    }

    /**
     * @param vlr_rmat the vlr_rmat to set
     */
    public void setVlr_rmat(double vlr_rmat) {
        this.vlr_rmat = vlr_rmat;
    }

    /**
     * @return the vlr_rmob
     */
    public double getVlr_rmob() {
        return vlr_rmob;
    }

    /**
     * @param vlr_rmob the vlr_rmob to set
     */
    public void setVlr_rmob(double vlr_rmob) {
        this.vlr_rmob = vlr_rmob;
    }

    /**
     * @return the vlr_rotr
     */
    public double getVlr_rotr() {
        return vlr_rotr;
    }

    /**
     * @param vlr_rotr the vlr_rotr to set
     */
    public void setVlr_rotr(double vlr_rotr) {
        this.vlr_rotr = vlr_rotr;
    }

    /**
     * @return the vlr_retencion
     */
    public double getVlr_retencion() {
        return vlr_retencion;
    }

    /**
     * @param vlr_retencion the vlr_retencion to set
     */
    public void setVlr_retencion(double vlr_retencion) {
        this.vlr_retencion = vlr_retencion;
    }

    /**
     * @return the vlr_mat
     */
    public double getVlr_mat() {
        return vlr_mat;
    }

    /**
     * @param vlr_mat the vlr_mat to set
     */
    public void setVlr_mat(double vlr_mat) {
        this.vlr_mat = vlr_mat;
    }

    /**
     * @return the vlr_mob
     */
    public double getVlr_mob() {
        return vlr_mob;
    }

    /**
     * @param vlr_mob the vlr_mob to set
     */
    public void setVlr_mob(double vlr_mob) {
        this.vlr_mob = vlr_mob;
    }

    /**
     * @return the vlr_otr
     */
    public double getVlr_otr() {
        return vlr_otr;
    }

    /**
     * @param vlr_otr the vlr_otr to set
     */
    public void setVlr_otr(double vlr_otr) {
        this.vlr_otr = vlr_otr;
    }

    /**
     * @return the vlr_factoring
     */
    public double getVlr_factoring() {
        return vlr_factoring;
    }

    /**
     * @param vlr_factoring the vlr_factoring to set
     */
    public void setVlr_factoring(double vlr_factoring) {
        this.vlr_factoring = vlr_factoring;
    }

    /**
     * @return the vlr_formula
     */
    public double getVlr_formula() {
        return vlr_formula;
    }

    /**
     * @param vlr_formula the vlr_formula to set
     */
    public void setVlr_formula(double vlr_formula) {
        this.vlr_formula = vlr_formula;
    }

    public String getFacturaConformada() {
        return factura_conformada;
    }
    public void setFacturaConformada(String factura_conformada) {
        this.factura_conformada = factura_conformada;
    }

    /**
     * @return the vlr_administracion
     */
    public double getVlr_administracion() {
        return vlr_administracion;
    }

    /**
     * @param vlr_administracion the vlr_administracion to set
     */
    public void setVlr_administracion(double vlr_administracion) {
        this.vlr_administracion = vlr_administracion;
    }

    /**
     * @return the vlr_imprevisto
     */
    public double getVlr_imprevisto() {
        return vlr_imprevisto;
    }

    /**
     * @param vlr_imprevisto the vlr_imprevisto to set
     */
    public void setVlr_imprevisto(double vlr_imprevisto) {
        this.vlr_imprevisto = vlr_imprevisto;
    }

    /**
     * @return the vlr_utilidad
     */
    public double getVlr_utilidad() {
        return vlr_utilidad;
    }

    /**
     * @param vlr_utilidad the vlr_utilidad to set
     */
    public void setVlr_utilidad(double vlr_utilidad) {
        this.vlr_utilidad = vlr_utilidad;
    }

    /**
     * @return the vlr_base_iva
     */
    public double getVlr_base_iva() {
        return vlr_base_iva;
    }

    /**
     * @param vlr_base_iva the vlr_base_iva to set
     */
    public void setVlr_base_iva(double vlr_base_iva) {
        this.vlr_base_iva = vlr_base_iva;
    }


    public double getVlr_formula_provintegral() {
        return vlr_formula_provintegral;
    }

    /**
     * @param vlr_formula the vlr_formula to set
     */
    public void setVlr_formula_provintegral(double x) {
        this.vlr_formula_provintegral= x;
    }
}