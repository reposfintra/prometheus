/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.opav.model.beans;

/**
 *
 * @author Alvaro
 */
public class Contratista {

  private String  id_contratista ;
  private String  nombre_contratista ;
  private int     secuencia_prefactura ;
  private String  nit;
  private String  gran_contribuyente;
  private String  domicilio_comercial;
  private String  actividad;
  private String  regimen;
  private String  autoretenedor;
  private String email;//2010-06-09 rhonalf
  private String clave;//2010-06-09 rhonalf
  private String codigo_reteica;//2010-06-09 rhonalf
  //JPACOSTA, 2014-01. informacion para provintegral 
  private String responsable;   
  private String direccion;
  private String telefono;
  private String celular;

    /** Creates a new instance of Contratista */
    public Contratista() {
        //2010-06-09 rhonalf
        this.actividad="";
        this.autoretenedor="";
        this.clave="";
        this.codigo_reteica="";
        this.domicilio_comercial="";
        this.email="";
        this.gran_contribuyente="";
        this.id_contratista="";
        this.nit="";
        this.nombre_contratista="";
        this.regimen="";
        this.secuencia_prefactura=0;
    }

    public static Contratista load(java.sql.ResultSet rs)throws java.sql.SQLException{

        Contratista contratista = new Contratista();

        contratista.setId_contratista( rs.getString("id_contratista") );
        contratista.setNombre_contratista( rs.getString("nombre_contratista") ); // renombrando campo descripcion
        contratista.setSecuencia_prefactura( rs.getInt("secuencia_prefactura") );
        contratista.setNit( rs.getString("nit") );
        contratista.setGran_contribuyente( rs.getString("gran_contribuyente") );
        contratista.setDomicilio_comercial( rs.getString("domicilio_comercial") );
        contratista.setActividad( rs.getString("actividad") );
        contratista.setRegimen( rs.getString("regimen") );
        contratista.setAutoretenedor( rs.getString("autoretenedor") );
        return contratista;
    }


    /**
     * @return the id_contratista
     */
    public String getId_contratista() {
        return id_contratista;
    }

    /**
     * @param id_contratista the id_contratista to set
     */
    public void setId_contratista(String id_contratista) {
        this.id_contratista = id_contratista;
    }

    /**
     * @return the nombre_contratista
     */
    public String getNombre_contratista() {
        return nombre_contratista;
    }

    /**
     * @param nombre_contratista the nombre_contratista to set
     */
    public void setNombre_contratista(String nombre_contratista) {
        this.nombre_contratista = nombre_contratista;
    }

    /**
     * @return the secuencia_prefactura
     */
    public int getSecuencia_prefactura() {
        return secuencia_prefactura;
    }

    /**
     * @param secuencia_prefactura the secuencia_prefactura to set
     */
    public void setSecuencia_prefactura(int secuencia_prefactura) {
        this.secuencia_prefactura = secuencia_prefactura;
    }

    /**
     * @return the nit
     */
    public String getNit() {
        return nit;
    }

    /**
     * @param nit the nit to set
     */
    public void setNit(String nit) {
        this.nit = nit;
    }

    /**
     * @return the gran_contribuyente
     */
    public String getGran_contribuyente() {
        return gran_contribuyente;
    }

    /**
     * @param gran_contribuyente the gran_contribuyente to set
     */
    public void setGran_contribuyente(String gran_contribuyente) {
        this.gran_contribuyente = gran_contribuyente;
    }

    /**
     * @return the domicilio_comercial
     */
    public String getDomicilio_comercial() {
        return domicilio_comercial;
    }

    /**
     * @param domicilio_comercial the domicilio_comercial to set
     */
    public void setDomicilio_comercial(String domicilio_comercial) {
        this.domicilio_comercial = domicilio_comercial;
    }

    /**
     * @return the actividad
     */
    public String getActividad() {
        return actividad;
    }

    /**
     * @param actividad the actividad to set
     */
    public void setActividad(String actividad) {
        this.actividad = actividad;
    }

    /**
     * @return the regimen
     */
    public String getRegimen() {
        return regimen;
    }

    /**
     * @param regimen the regimen to set
     */
    public void setRegimen(String regimen) {
        this.regimen = regimen;
    }

    /**
     * @return the autoretenedor
     */
    public String getAutoretenedor() {
        return autoretenedor;
    }

    /**
     * @param autoretenedor the autoretenedor to set
     */
    public void setAutoretenedor(String autoretenedor) {
        this.autoretenedor = autoretenedor;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public String getCodigo_reteica() {
        return codigo_reteica;
    }

    public void setCodigo_reteica(String codigo_reteica) {
        this.codigo_reteica = codigo_reteica;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getResponsable() {
        return responsable;
    }

    public void setResponsable(String responsable) {
        this.responsable = responsable;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
}

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }


}