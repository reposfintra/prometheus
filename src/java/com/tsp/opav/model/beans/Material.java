/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.opav.model.beans;

/**
 *
 * @author Rhonalf
 */
public class Material {

    private String codigo;
    private String descripcion;
    private String tipo;
    private String medida;
    private double valor;
    private double valor_compra;
    private String categoria;//091203.
        private int idcategoria;
    private int idsubcategoria;
    private int idtiposubcategoria;
    private String unidad_empaque;
    private String alcance;
    private String desc_categoria;
    private String desc_subcat;
    private String desc_tiposub;


    private String reg_status,idmaterial,last_update,user_update,aprobacion,idmaterial_asociado,fecha_anulacion,user_anulacion;

    private String certificado;
    private String cod_ente;
    private String desc_ente;
    private String ente_certificador;


    //jpinedo

    private double precio_ultima_compra;
    private String fecha_ultima_compra;
    private String fecha_actualizacion_precio_compra;
    private String observacion ;



    private int max_dias_inactividad;
    private int dias_inactividad;
    private String nombrecategoria ;








    public String getDesc_ente() {
        return desc_ente;
    }

    public void setDesc_ente(String desc_ente) {
        this.desc_ente = desc_ente;
    }

    public String getCod_ente() {
        return cod_ente;
    }

    public void setCod_ente(String cod_ente) {
        this.cod_ente = cod_ente;
    }


    public String getCertificado() {
        return certificado;
    }

    public void setCertificado(String certificado) {
        this.certificado = certificado;
    }

    public String getEnte_certificador() {
        return ente_certificador;
    }

    public void setEnte_certificador(String ente_certificador) {
        this.ente_certificador = ente_certificador;
    }

    public Material(){
        this.codigo="";
        this.descripcion="";
        this.medida="UNIDADES";
        this.tipo="M";
        this.valor=0.0;
        this.valor_compra=0.0;
    }
    public void setCodigo(String cod){
        this.codigo=cod;
    }
    public String getCodigo(){
        return this.codigo;
    }

    public void setRegStatus(String x){
        this.reg_status=x;
    }
    public String getRegStatus(){
        return this.reg_status;
    }
    public void setIdMaterial(String x){
        this.idmaterial=x;
    }
    public String getIdMaterial(){
        return this.idmaterial;
    }
    public void setLastUpdate(String x){
        this.last_update=x;
    }
    public String getLastUpdate(){
        return this.last_update;
    }
    public void setUserUpdate(String x){
        this.user_update=x;
    }
    public String getUserUpdate(){
        return this.user_update;
    }
    public void setAprobacion(String x){
        this.aprobacion=x;
    }
    public String getAprobacion(){
        return this.aprobacion;
    }
    public void setIdMaterialAsociado(String x){
        this.idmaterial_asociado=x;
    }
    public String getIdMaterialAsociado(){
        return idmaterial_asociado;
    }
    public void setFechaAnulacion(String x){
        this.fecha_anulacion=x;
    }
    public String getFechaAnulacion(){
        return fecha_anulacion;
    }
    public void setUserAnulacion(String x){
        this.user_anulacion=x;
    }
    public String getUserAnulacion(){
        return user_anulacion;
    }


    public void setDescripcion(String desc){
        this.descripcion=desc;
    }

    public String getDescripcion(){
        return this.descripcion;
    }

    public void setTipo(String tip){
        this.tipo=tip;
    }

    public String getTipo(){
        return this.tipo;
    }

    public void setMedida(String med){
        this.medida=med;
    }

    public String getMedida(){
        return this.medida;
    }

    public void setValor(double v){
        this.valor=v;
    }

    public double getValor(){
        return this.valor;
    }

    public void setValorCompra(double vcomp){
        this.valor_compra=vcomp;
    }

    public double getValorCompra(){
        return this.valor_compra;
    }

    public void setCategoria(String cat){ //091203
        this.categoria = cat;
    }

    public String getCategoria(){ //091203
        return categoria;
    }

    public String getDesc_categoria() {
        return desc_categoria;
    }

    public void setDesc_categoria(String desc_categoria) {
        this.desc_categoria = desc_categoria;
    }

    public String getDesc_subcat() {
        return desc_subcat;
    }

    public void setDesc_subcat(String desc_subcat) {
        this.desc_subcat = desc_subcat;
    }

    public String getDesc_tiposub() {
        return desc_tiposub;
    }

    public void setDesc_tiposub(String desc_tiposub) {
        this.desc_tiposub = desc_tiposub;
    }

    public String getAlcance() {
        return alcance;
    }

    public void setAlcance(String alcance) {
        this.alcance = alcance;
    }

    public int getIdcategoria() {
        return idcategoria;
    }

    public void setIdcategoria(int idcategoria) {
        this.idcategoria = idcategoria;
    }

    public int getIdsubcategoria() {
        return idsubcategoria;
    }

    public void setIdsubcategoria(int idsubcategoria) {
        this.idsubcategoria = idsubcategoria;
    }

    public int getIdtiposubcategoria() {
        return idtiposubcategoria;
    }

    public void setIdtiposubcategoria(int idtiposubcategoria) {
        this.idtiposubcategoria = idtiposubcategoria;
    }

    public String getUnidad_empaque() {
        return unidad_empaque;
    }

    public void setUnidad_empaque(String unidad_empaque) {
        this.unidad_empaque = unidad_empaque;
    }



    public String getFecha_actualizacion_precio_compra() {
        return fecha_actualizacion_precio_compra;
    }

    public void setFecha_actualizacion_precio_compra(String fecha_actualizacion_precio_compra) {
        this.fecha_actualizacion_precio_compra = fecha_actualizacion_precio_compra;
    }

    public String getFecha_ultima_compra() {
        return fecha_ultima_compra;
    }

    public void setFecha_ultima_compra(String fecha_ultima_compra) {
        this.fecha_ultima_compra = fecha_ultima_compra;
    }

    public double getPrecio_ultima_compra() {
        return precio_ultima_compra;
    }

    public void setPrecio_ultima_compra(double precio_ultima_compra) {
        this.precio_ultima_compra = precio_ultima_compra;
    }


    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }



        public int getDias_inactividad() {
        return dias_inactividad;
    }

    public void setDias_inactividad(int dias_inactividad) {
        this.dias_inactividad = dias_inactividad;
    }

    public int getMax_dias_inactividad() {
        return max_dias_inactividad;
    }

    public void setMax_dias_inactividad(int max_dias_inactividad) {
        this.max_dias_inactividad = max_dias_inactividad;
    }


        public String getNombrecategoria() {
        return nombrecategoria;
    }

    public void setNombrecategoria(String nombrecategoria) {
        this.nombrecategoria = nombrecategoria;
    }

}