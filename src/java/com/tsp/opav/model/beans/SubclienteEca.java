package com.tsp.opav.model.beans;

public class SubclienteEca {

    private String id_cliente;
    private String nombre_cliente;
    private String id_solicitud;
    private String porc_cuota_inicial;
    private String val_cuota_inicial;
    private String val_cuota;
    private String periodo;
    private String reg_status;
    private String nic;
    private String tasa;
    private String valor_base;
    private String estudio_economico;
    private String porc_base;
    private String valor_oferta;
    private String meses_mora;
    private String fecha_financiacion;
    private String parcial;
    private String tipo;

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public SubclienteEca(){
    }

    public String getNombre_cliente() {
        return nombre_cliente;
    }

    public void setNombre_cliente(String nombre_cliente) {
        this.nombre_cliente = nombre_cliente;
    }

    public String getFecha_financiacion() {
        return fecha_financiacion;
    }

    public void setFecha_financiacion(String fecha_financiacion) {
        this.fecha_financiacion = fecha_financiacion;
    }

    public String getMeses_mora() {
        return meses_mora;
    }

    public void setMeses_mora(String meses_mora) {
        this.meses_mora = meses_mora;
    }

    public String getParcial() {
        return parcial;
    }

    public void setParcial(String parcial) {
        this.parcial = parcial;
    }

    public String getEstudio_economico() {
        return estudio_economico;
    }

    public void setEstudio_economico(String estudio_economico) {
        this.estudio_economico = estudio_economico;
    }

    public String getId_cliente() {
        return id_cliente;
    }

    public void setId_cliente(String id_cliente) {
        this.id_cliente = id_cliente;
    }

    public String getId_solicitud() {
        return id_solicitud;
    }

    public void setId_solicitud(String id_solicitud) {
        this.id_solicitud = id_solicitud;
    }

    public String getNic() {
        return nic;
    }

    public void setNic(String nic) {
        this.nic = nic;
    }

    public String getPeriodo() {
        return periodo;
    }

    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }

    public String getPorc_base() {
        return porc_base;
    }

    public void setPorc_base(String porc_base) {
        this.porc_base = porc_base;
    }

    public String getPorc_cuota_inicial() {
        return porc_cuota_inicial;
    }

    public void setPorc_cuota_inicial(String porc_cuota_inicial) {
        this.porc_cuota_inicial = porc_cuota_inicial;
    }

    public String getReg_status() {
        return reg_status;
    }

    public void setReg_status(String reg_status) {
        this.reg_status = reg_status;
    }

    public String getTasa() {
        return tasa;
    }

    public void setTasa(String tasa) {
        this.tasa = tasa;
    }

    public String getVal_cuota() {
        return val_cuota;
    }

    public void setVal_cuota(String val_cuota) {
        this.val_cuota = val_cuota;
    }

    public String getVal_cuota_inicial() {
        return val_cuota_inicial;
    }

    public void setVal_cuota_inicial(String val_cuota_inicial) {
        this.val_cuota_inicial = val_cuota_inicial;
    }

    public String getValor_base() {
        return valor_base;
    }

    public void setValor_base(String valor_base) {
        this.valor_base = valor_base;
    }

    public String getValor_oferta() {
        return valor_oferta;
    }

    public void setValor_oferta(String valor_oferta) {
        this.valor_oferta = valor_oferta;
    }
}