/* * Accord.java  * Created on 1 de junio de 2009, 8:08 */
package com.tsp.opav.model.beans;
/** * @author  Fintra */
public class AccionesEca {
    private String id_accion, id_solicitud, estado, contratista, material, mano_obra,
       transporte, administracion, imprevisto, utilidad, porc_administracion,
       porc_imprevisto, porc_utilidad, descripcion, user_update, creation_user,
       reg_status, observaciones, tipo_trabajo, creation_date, last_update,nomcontratista;

    private String alcance;//091103

    public AccionesEca(){
    }

    public String getNomcontratista() {
        return nomcontratista;
    }

    public void setNomcontratista(String nomcontratista) {
        this.nomcontratista = nomcontratista;
    }

    public String getAdministracion() {
        return administracion;
    }

    public void setAdministracion(String administracion) {
        this.administracion = administracion;
    }

    public String getContratista() {
        return contratista;
    }

    public void setContratista(String contratista) {
        this.contratista = contratista;
    }

    public String getCreation_date() {
        return creation_date;
    }

    public void setCreation_date(String creation_date) {
        this.creation_date = creation_date;
    }

    public String getCreation_user() {
        return creation_user;
    }

    public void setCreation_user(String creation_user) {
        this.creation_user = creation_user;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getId_accion() {
        return id_accion;
    }

    public void setId_accion(String id_accion) {
        this.id_accion = id_accion;
    }

    public String getId_solicitud() {
        return id_solicitud;
    }

    public void setId_solicitud(String id_solicitud) {
        this.id_solicitud = id_solicitud;
    }

    public String getImprevisto() {
        return imprevisto;
    }

    public void setImprevisto(String imprevisto) {
        this.imprevisto = imprevisto;
    }

    public String getLast_update() {
        return last_update;
    }

    public void setLast_update(String last_update) {
        this.last_update = last_update;
    }

    public String getMano_obra() {
        return mano_obra;
    }

    public void setMano_obra(String mano_obra) {
        this.mano_obra = mano_obra;
    }

    public String getMaterial() {
        return material;
    }

    public void setMaterial(String material) {
        this.material = material;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public String getPorc_administracion() {
        return porc_administracion;
    }

    public void setPorc_administracion(String porc_administracion) {
        this.porc_administracion = porc_administracion;
    }

    public String getPorc_imprevisto() {
        return porc_imprevisto;
    }

    public void setPorc_imprevisto(String porc_imprevisto) {
        this.porc_imprevisto = porc_imprevisto;
    }

    public String getPorc_utilidad() {
        return porc_utilidad;
    }

    public void setPorc_utilidad(String porc_utilidad) {
        this.porc_utilidad = porc_utilidad;
    }

    public String getReg_status() {
        return reg_status;
    }

    public void setReg_status(String reg_status) {
        this.reg_status = reg_status;
    }

    public String getTipo_trabajo() {
        return tipo_trabajo;
    }

    public void setTipo_trabajo(String tipo_trabajo) {
        this.tipo_trabajo = tipo_trabajo;
    }

    public String getTransporte() {
        return transporte;
    }

    public void setTransporte(String transporte) {
        this.transporte = transporte;
    }

    public String getUser_update() {
        return user_update;
    }

    public void setUser_update(String user_update) {
        this.user_update = user_update;
    }

    public String getUtilidad() {
        return utilidad;
    }

    public void setUtilidad(String utilidad) {
        this.utilidad = utilidad;
    }

    public String getAlcance() {//091103
            return alcance;
    }

    public void setAlcance(String alcance) {//091103
            this.alcance = alcance;
    }
}

