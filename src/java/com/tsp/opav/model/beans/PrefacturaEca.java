/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.opav.model.beans;

/**
 *
 * @author Alvaro
 */
public class PrefacturaEca {

    private String nit;
    private String cmc;
    private int    plazo;
    private int id_orden;
    private String id_cliente;
    private int cuotas_reales;
    private String num_os;
    private String simbolo_variable;
    private String fecha_factura_eca;
    private String factura_eca;
    private double e_total_prev1;
    private double e_iva_total_prev1;
    private double e_comision_applus;
    private double e_comision_provintegral;
    private double e_comision_factoring_fintra;
    private double e_comision_fintra;
    private double e_iva_comision_applus_total;
    private double e_comision_eca;
    private double e_iva_comision_eca;
    private double e_cuota_pago;

    private double e_iva_comision_applus;
    private double e_iva_comision_provintegral;
    private double e_iva_comision_factoring_fintra;
    private double e_iva_comision_fintra;
    private double e_total_financiacion;

    private double ec_financiacion_fintra;
    private double ec_cuota_pago;
    private double ec_total_financiacion;
    private double e_porcentaje_iva;
    private double e_iva_bonificacion;

    /** Creates a new instance of PrefacturaEca */
    public PrefacturaEca() {
    }


    public static PrefacturaEca load(java.sql.ResultSet rs)throws java.sql.SQLException{

        PrefacturaEca prefacturaEca = new PrefacturaEca();

        prefacturaEca.setNit( rs.getString("nit") );
        prefacturaEca.setCmc( rs.getString("cmc") );
        prefacturaEca.setPlazo( rs.getInt("plazo") );
        prefacturaEca.setId_orden( rs.getInt("id_orden") );
        prefacturaEca.setId_cliente(rs.getString("id_cliente"));
        prefacturaEca.setNum_os(rs.getString("num_os"));
        prefacturaEca.setCuotas_reales( rs.getInt("cuotas_reales") );
        prefacturaEca.setSimbolo_variable(rs.getString("simbolo_variable"));
        prefacturaEca.setFecha_factura_eca(rs.getString("fecha_factura_eca"));
        prefacturaEca.setFactura_eca(rs.getString("factura_eca"));
        prefacturaEca.setE_total_prev1(rs.getDouble("e_total_prev1"));
        prefacturaEca.setE_iva_total_prev1(rs.getDouble("e_iva_total_prev1"));
        prefacturaEca.setE_comision_applus(rs.getDouble("e_comision_applus"));
        prefacturaEca.setE_comision_provintegral(rs.getDouble("e_comision_provintegral"));
        prefacturaEca.setE_comision_factoring_fintra(rs.getDouble("e_comision_factoring_fintra"));
        prefacturaEca.setE_comision_fintra(rs.getDouble("e_comision_fintra"));
        prefacturaEca.setE_iva_comision_applus_total(rs.getDouble("e_iva_comision_applus_total"));
        prefacturaEca.setE_comision_eca(rs.getDouble("e_comision_eca"));
        prefacturaEca.setE_iva_comision_eca(rs.getDouble("e_iva_comision_eca"));
        prefacturaEca.setE_cuota_pago(rs.getDouble("e_cuota_pago"));

        prefacturaEca.setE_iva_comision_applus(rs.getDouble("e_iva_comision_applus"));
        prefacturaEca.setE_iva_comision_provintegral(rs.getDouble("e_iva_comision_provintegral"));
        prefacturaEca.setE_iva_comision_factoring_fintra(rs.getDouble("e_iva_comision_factoring_fintra"));
        prefacturaEca.setE_iva_comision_fintra(rs.getDouble("e_iva_comision_fintra"));
        prefacturaEca.setE_total_financiacion(rs.getDouble("e_total_financiacion"));

        prefacturaEca.setEc_financiacion_fintra(rs.getDouble("ec_financiacion_fintra"));
        prefacturaEca.setEc_cuota_pago(rs.getDouble("ec_cuota_pago"));
        prefacturaEca.setEc_total_financiacion(rs.getDouble("ec_total_financiacion"));
        prefacturaEca.setE_porcentaje_iva(rs.getDouble("e_porcentaje_iva"));
        prefacturaEca.setE_iva_bonificacion(rs.getDouble("e_iva_bonificacion"));

        return prefacturaEca;
    }


    /**
     * @return the nit
     */
    public String getNit() {
        return nit;
    }

    /**
     * @param nit the nit to set
     */
    public void setNit(String nit) {
        this.nit = nit;
    }

    /**
     * @return the cmc
     */
    public String getCmc() {
        return cmc;
    }

    /**
     * @param cmc the cmc to set
     */
    public void setCmc(String cmc) {
        this.cmc = cmc;
    }

    /**
     * @return the plazo
     */
    public int getPlazo() {
        return plazo;
    }

    /**
     * @param plazo the plazo to set
     */
    public void setPlazo(int plazo) {
        this.plazo = plazo;
    }

    /**
     * @return the id_orden
     */
    public int getId_orden() {
        return id_orden;
    }

    /**
     * @param id_orden the id_orden to set
     */
    public void setId_orden(int id_orden) {
        this.id_orden = id_orden;
    }

    /**
     * @return the id_cliente
     */
    public String getId_cliente() {
        return id_cliente;
    }

    /**
     * @param id_cliente the id_cliente to set
     */
    public void setId_cliente(String id_cliente) {
        this.id_cliente = id_cliente;
    }

    /**
     * @return the num_os
     */
    public String getNum_os() {
        return num_os;
    }

    /**
     * @param num_os the num_os to set
     */
    public void setNum_os(String num_os) {
        this.num_os = num_os;
    }

    /**
     * @return the simbolo_variable
     */
    public String getSimbolo_variable() {
        return simbolo_variable;
    }

    /**
     * @param simbolo_variable the simbolo_variable to set
     */
    public void setSimbolo_variable(String simbolo_variable) {
        this.simbolo_variable = simbolo_variable;
    }

    /**
     * @return the e_total_prev1
     */
    public Double getE_total_prev1() {
        return e_total_prev1;
    }

    /**
     * @param e_total_prev1 the e_total_prev1 to set
     */
    public void setE_total_prev1(Double e_total_prev1) {
        this.e_total_prev1 = e_total_prev1;
    }

    /**
     * @return the e_iva_total_prev1
     */
    public Double getE_iva_total_prev1() {
        return e_iva_total_prev1;
    }

    /**
     * @param e_iva_total_prev1 the e_iva_total_prev1 to set
     */
    public void setE_iva_total_prev1(Double e_iva_total_prev1) {
        this.e_iva_total_prev1 = e_iva_total_prev1;
    }

    /**
     * @return the e_comision_applus
     */
    public Double getE_comision_applus() {
        return e_comision_applus;
    }

    /**
     * @param e_comision_applus the e_comision_applus to set
     */
    public void setE_comision_applus(Double e_comision_applus) {
        this.e_comision_applus = e_comision_applus;
    }

    /**
     * @return the e_comision_provintegral
     */
    public Double getE_comision_provintegral() {
        return e_comision_provintegral;
    }

    /**
     * @param e_comision_provintegral the e_comision_provintegral to set
     */
    public void setE_comision_provintegral(Double e_comision_provintegral) {
        this.e_comision_provintegral = e_comision_provintegral;
    }

    /**
     * @return the e_comision_factoring_fintra
     */
    public Double getE_comision_factoring_fintra() {
        return e_comision_factoring_fintra;
    }

    /**
     * @param e_comision_factoring_fintra the e_comision_factoring_fintra to set
     */
    public void setE_comision_factoring_fintra(Double e_comision_factoring_fintra) {
        this.e_comision_factoring_fintra = e_comision_factoring_fintra;
    }

    /**
     * @return the e_comision_fintra
     */
    public Double getE_comision_fintra() {
        return e_comision_fintra;
    }

    /**
     * @param e_comision_fintra the e_comision_fintra to set
     */
    public void setE_comision_fintra(Double e_comision_fintra) {
        this.e_comision_fintra = e_comision_fintra;
    }

    /**
     * @return the e_iva_comision_applus
     */
    public Double getE_iva_comision_applus_total() {
        return e_iva_comision_applus_total;
    }

    /**
     * @param e_iva_comision_applus the e_iva_comision_applus to set
     */
    public void setE_iva_comision_applus_total(Double e_iva_comision_applus_total) {
        this.e_iva_comision_applus_total = e_iva_comision_applus_total;
    }

    /**
     * @return the e_comision_eca
     */
    public Double getE_comision_eca() {
        return e_comision_eca;
    }

    /**
     * @param e_comision_eca the e_comision_eca to set
     */
    public void setE_comision_eca(Double e_comision_eca) {
        this.e_comision_eca = e_comision_eca;
    }

    /**
     * @return the e_iva_comision_eca
     */
    public Double getE_iva_comision_eca() {
        return e_iva_comision_eca;
    }

    /**
     * @param e_iva_comision_eca the e_iva_comision_eca to set
     */
    public void setE_iva_comision_eca(Double e_iva_comision_eca) {
        this.e_iva_comision_eca = e_iva_comision_eca;
    }

    /**
     * @return the cuotas_reales
     */
    public int getCuotas_reales() {
        return cuotas_reales;
    }

    /**
     * @param cuotas_reales the cuotas_reales to set
     */
    public void setCuotas_reales(int cuotas_reales) {
        this.cuotas_reales = cuotas_reales;
    }

    /**
     * @return the e_cuota_pago
     */
    public Double getE_cuota_pago() {
        return e_cuota_pago;
    }

    /**
     * @param e_cuota_pago the e_cuota_pago to set
     */
    public void setE_cuota_pago(Double e_cuota_pago) {
        this.e_cuota_pago = e_cuota_pago;
    }

    /**
     * @return the fecha_prefactura_eca
     */
    public String getFecha_factura_eca() {
        return fecha_factura_eca;
    }

    /**
     * @param fecha_prefactura_eca the fecha_prefactura_eca to set
     */
    public void setFecha_factura_eca(String fecha_factura_eca) {
        this.fecha_factura_eca = fecha_factura_eca;
    }

    /**
     * @return the factura_eca
     */
    public String getFactura_eca() {
        return factura_eca;
    }

    /**
     * @param factura_eca the factura_eca to set
     */
    public void setFactura_eca(String factura_eca) {
        this.factura_eca = factura_eca;
    }

    /**
     * @return the e_iva_comision_applus
     */
    public Double getE_iva_comision_applus() {
        return e_iva_comision_applus;
    }

    /**
     * @param e_iva_comision_applus the e_iva_comision_applus to set
     */
    public void setE_iva_comision_applus(Double e_iva_comision_applus) {
        this.e_iva_comision_applus = e_iva_comision_applus;
    }

    /**
     * @return the e_iva_comision_provintegral
     */
    public Double getE_iva_comision_provintegral() {
        return e_iva_comision_provintegral;
    }

    /**
     * @param e_iva_comision_provintegral the e_iva_comision_provintegral to set
     */
    public void setE_iva_comision_provintegral(Double e_iva_comision_provintegral) {
        this.e_iva_comision_provintegral = e_iva_comision_provintegral;
    }

    /**
     * @return the e_iva_comision_factoring_fintra
     */
    public Double getE_iva_comision_factoring_fintra() {
        return e_iva_comision_factoring_fintra;
    }

    /**
     * @param e_iva_comision_factoring_fintra the e_iva_comision_factoring_fintra to set
     */
    public void setE_iva_comision_factoring_fintra(Double e_iva_comision_factoring_fintra) {
        this.e_iva_comision_factoring_fintra = e_iva_comision_factoring_fintra;
    }

    /**
     * @return the e_iva_comision_fintra
     */
    public Double getE_iva_comision_fintra() {
        return e_iva_comision_fintra;
    }

    /**
     * @param e_iva_comision_fintra the e_iva_comision_fintra to set
     */
    public void setE_iva_comision_fintra(Double e_iva_comision_fintra) {
        this.e_iva_comision_fintra = e_iva_comision_fintra;
    }

    /**
     * @return the e_total_financiacion
     */
    public double getE_total_financiacion() {
        return e_total_financiacion;
    }

    /**
     * @param e_total_financiacion the e_total_financiacion to set
     */
    public void setE_total_financiacion(double e_total_financiacion) {
        this.e_total_financiacion = e_total_financiacion;
    }

    /**
     * @return the ec_financiacion_fintra
     */
    public double getEc_financiacion_fintra() {
        return ec_financiacion_fintra;
    }

    /**
     * @param ec_financiacion_fintra the ec_financiacion_fintra to set
     */
    public void setEc_financiacion_fintra(double ec_financiacion_fintra) {
        this.ec_financiacion_fintra = ec_financiacion_fintra;
    }

    /**
     * @return the ec_cuota_pago
     */
    public double getEc_cuota_pago() {
        return ec_cuota_pago;
    }

    /**
     * @param ec_cuota_pago the ec_cuota_pago to set
     */
    public void setEc_cuota_pago(double ec_cuota_pago) {
        this.ec_cuota_pago = ec_cuota_pago;
    }

    /**
     * @return the ec_total_financiacion
     */
    public double getEc_total_financiacion() {
        return ec_total_financiacion;
    }

    /**
     * @param ec_total_financiacion the ec_total_financiacion to set
     */
    public void setEc_total_financiacion(double ec_total_financiacion) {
        this.ec_total_financiacion = ec_total_financiacion;
    }

    /**
     * @return the e_porcentaje_iva
     */
    public double getE_porcentaje_iva() {
        return e_porcentaje_iva;
    }

    /**
     * @param e_porcentaje_iva the e_porcentaje_iva to set
     */
    public void setE_porcentaje_iva(double e_porcentaje_iva) {
        this.e_porcentaje_iva = e_porcentaje_iva;
    }

    /**
     * @return the e_iva_bonificacion
     */
    public double getE_iva_bonificacion() {
        return e_iva_bonificacion;
    }

    /**
     * @param e_iva_bonificacion the e_iva_bonificacion to set
     */
    public void setE_iva_bonificacion(double e_iva_bonificacion) {
        this.e_iva_bonificacion = e_iva_bonificacion;
    }

}