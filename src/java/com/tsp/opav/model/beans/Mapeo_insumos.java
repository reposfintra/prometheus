/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.opav.model.beans;

/**
 *
 * @author user
 */
public class Mapeo_insumos {

    private int id;
    private String fecha;
    private String orden_compra;
    private String multiservicio_o_aviso;
    private String factura;
    private String cliente;
    private String contratista;
    private String codigo_producto;
    private String codigo_material;
    private String producto;
    private String cantidad;
    private String cantidad_x_facturar;
    private String valor_unitario;
    private String rentabilidad;
    private String vlr_compra;
    private String nit_proveedor;
    private String proveedor;
    private String fecha_ultimo_pago;
    private String direccion;
    private String venc_entregas;
    private String nuevo_codigo;
    private String descripcion;
    private String payment_name;

    public String getPayment_name() {
        return payment_name;
    }

    public void setPayment_name(String payment_name) {
        this.payment_name = payment_name;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getCodigo_material() {
        return codigo_material;
    }

    public void setCodigo_material(String codigo_material) {
        this.codigo_material = codigo_material;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getOrden_compra() {
        return orden_compra;
    }

    public void setOrden_compra(String orden_compra) {
        this.orden_compra = orden_compra;
    }

    public String getMultiservicio_o_aviso() {
        return multiservicio_o_aviso;
    }

    public void setMultiservicio_o_aviso(String multiservicio_o_aviso) {
        this.multiservicio_o_aviso = multiservicio_o_aviso;
    }

    public String getFactura() {
        return factura;
    }

    public void setFactura(String factura) {
        this.factura = factura;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public String getContratista() {
        return contratista;
    }

    public void setContratista(String contratista) {
        this.contratista = contratista;
    }

    public String getCodigo_producto() {
        return codigo_producto;
    }

    public void setCodigo_producto(String codigo_producto) {
        this.codigo_producto = codigo_producto;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public String getCantidad() {
        return cantidad;
    }

    public void setCantidad(String cantidad) {
        this.cantidad = cantidad;
    }

    public String getCantidad_x_facturar() {
        return cantidad_x_facturar;
    }

    public void setCantidad_x_facturar(String cantidad_x_facturar) {
        this.cantidad_x_facturar = cantidad_x_facturar;
    }

    public String getValor_unitario() {
        return valor_unitario;
    }

    public void setValor_unitario(String valor_unitario) {
        this.valor_unitario = valor_unitario;
    }

    public String getRentabilidad() {
        return rentabilidad;
    }

    public void setRentabilidad(String rentabilidad) {
        this.rentabilidad = rentabilidad;
    }

    public String getVlr_compra() {
        return vlr_compra;
    }

    public void setVlr_compra(String vlr_compra) {
        this.vlr_compra = vlr_compra;
    }

    public String getNit_proveedor() {
        return nit_proveedor;
    }

    public void setNit_proveedor(String nit_proveedor) {
        this.nit_proveedor = nit_proveedor;
    }

    public String getProveedor() {
        return proveedor;
    }

    public void setProveedor(String proveedor) {
        this.proveedor = proveedor;
    }

    public String getFecha_ultimo_pago() {
        return fecha_ultimo_pago;
    }

    public void setFecha_ultimo_pago(String fecha_ultimo_pago) {
        this.fecha_ultimo_pago = fecha_ultimo_pago;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getVenc_entregas() {
        return venc_entregas;
    }

    public void setVenc_entregas(String venc_entregas) {
        this.venc_entregas = venc_entregas;
    }

    public String getNuevo_codigo() {
        return nuevo_codigo;
    }

    public void setNuevo_codigo(String nuevo_codigo) {
        this.nuevo_codigo = nuevo_codigo;
    }

}
