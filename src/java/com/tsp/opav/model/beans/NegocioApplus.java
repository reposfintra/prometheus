/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.opav.model.beans;

// @author  navi
public class NegocioApplus {

    private String dptoCliente;
    private String fVerificacion_cartera;
    private String fAsignacion_contratista;
    private String fCotizacion;
    private String diaVenc_visita;
    private String diaVenc_cotizacion;
    private String diaVenc_entregaOferta;
    private String diaVenc_totales;
    private boolean venc_visita;
    private boolean venc_cotizacion;
    private boolean venc_oferta;
    private boolean venc_totales;

    private String id = "", vlr = "0", id_accion = "", id_client = "", id_contratista = "", estadito = "";
    private String nombre_contratista = "", nombre_client = "", observacion = "", num_os = "", dif_eca_oferta = "", eca_oferta = "";
    private String tel_cli = "", contacto = "", cuotas = "", val_cuotas = "", estudio = "", fact_conformada = "", fecha = "", simbolo = "";
    private String oferta = "", dif_oferta = "";
    private String esquema_comision = "";
    private String dif_oferta_applus_nuevo = "", dif_oferta_applus_antiguo = "", dif_ecaoferta_applus_nuevo = "", dif_ecaoferta_applus_antiguo = "";
    private String prefactura = "", factura_eca = "", factura_contratista = "", factura_retencion = "", factura_bonificacion = "";
    private String nit_client = "", nic_client = "", observacion_open = "";
    private String factura_app = "", factura_pro = "", factura_comision_eca = "", esquema_financiacion = "";
    private String f_recepcion = "";
    private String tipo_cliente = "", factura_factoring_pro = "", acciones = "";
    private String id_estadito = "";
    private String id_solicitud = "", solicitud = "";//090922
    private String tipo_trabajo = "";
    private String fecha_entrega_oferta = "", creacion_fecha_entrega_oferta = "", usuario_entrega_oferta = "";
    private String alcances = "", adiciones = "", trabajo = "";
    private String fecVisitaPlan;
    private String fecVisitaReal;
    private String usrVisitaHecha;
    private String creationFecVisitaHecha, consecutivo_oferta;
    private String material, mano_obra, otros, administracion, imprevisto, utilidad, porc_a, porc_i, porc_u;
    private String venc_entregaOferta;

    private String fecha_factura_contratista_final="",nota_credito_contratista="";//20100618

    private String fecFactConformed="";//20100714

    //----------- RESPONSABLE OPAV JCASTRO
    private String reg_encontrados;
    private String aviso;
    private String descripcion;
    private String responsable;
    private String ejecutivo;
    private String cuenta_solicitudes;
    private String promedio;



    public String getCuenta_solicitudes() {
        return cuenta_solicitudes;
    }

    public void setCuenta_solicitudes(String cuenta_solicitudes) {
        this.cuenta_solicitudes = cuenta_solicitudes;
    }

    public String getPromedio() {
        return promedio;
    }

    public void setPromedio(String promedio) {
        this.promedio = promedio;
    }

    //----------- RESPONSABLE OPAV JCASTRO




    public String getEjecutivo() {
        return ejecutivo;
    }

    public void setEjecutivo(String ejecutivo) {
        this.ejecutivo = ejecutivo;
    }

    public String getResponsable() {
        return responsable;
    }

    public void setResponsable(String responsable) {
        this.responsable = responsable;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getAviso() {
        return aviso;
    }

    public void setAviso(String aviso) {
        this.aviso = aviso;
    }

    public String getReg_encontrados() {
        return reg_encontrados;
    }

    public void setReg_encontrados(String reg_encontrados) {
        this.reg_encontrados = reg_encontrados;
    }



   //----------- RESPONSABLE OPAV JCASTRO


    public NegocioApplus() {
    }

    public String getFecFacContratistaFin() {//20100618
        return fecha_factura_contratista_final;
    }

    public void setFecFacContratistaFin(String fecha_factura_contratista_final1) {//20100618
        this.fecha_factura_contratista_final = fecha_factura_contratista_final1;
    }

    public String getNotaCredContratista() {//20100618
        return nota_credito_contratista;
    }

    public void setNotaCredContratista(String nota_credito_contratista1) {//20100618
        this.nota_credito_contratista = nota_credito_contratista1;
    }

    public String getAdministracion() {
        return administracion;
    }

    public void setAdministracion(String administracion) {
        this.administracion = administracion;
    }

    public String getImprevisto() {
        return imprevisto;
    }

    public void setImprevisto(String imprevisto) {
        this.imprevisto = imprevisto;
    }

    public String getMano_obra() {
        return mano_obra;
    }

    public void setMano_obra(String mano_obra) {
        this.mano_obra = mano_obra;
    }

    public String getMaterial() {
        return material;
    }

    public void setMaterial(String material) {
        this.material = material;
    }

    public String getOtros() {
        return otros;
    }

    public void setOtros(String otros) {
        this.otros = otros;
    }

    public String getPorc_a() {
        return porc_a;
    }

    public void setPorc_a(String porc_a) {
        this.porc_a = porc_a;
    }

    public String getPorc_i() {
        return porc_i;
    }

    public void setPorc_i(String porc_i) {
        this.porc_i = porc_i;
    }

    public String getPorc_u() {
        return porc_u;
    }

    public void setPorc_u(String porc_u) {
        this.porc_u = porc_u;
    }

    public String getUtilidad() {
        return utilidad;
    }

    public void setUtilidad(String utilidad) {
        this.utilidad = utilidad;
    }

    public String getCreationFecVisitaHecha() {
        return creationFecVisitaHecha;
    }

    public void setCreationFecVisitaHecha(String creationFecVisitaHecha) {
        this.creationFecVisitaHecha = creationFecVisitaHecha;
    }

    public String getFecVisitaPlan() {
        return fecVisitaPlan;
    }

    public void setFecVisitaPlan(String fecVisitaPlan) {
        this.fecVisitaPlan = fecVisitaPlan;
    }

    public String getFecVisitaReal() {
        return fecVisitaReal;
    }

    public void setFecVisitaReal(String fecVisitaReal) {
        this.fecVisitaReal = fecVisitaReal;
    }

    public String getUsrVisitaHecha() {
        return usrVisitaHecha;
    }

    public void setUsrVisitaHecha(String usrVisitaHecha) {
        this.usrVisitaHecha = usrVisitaHecha;
    }

    public java.lang.String getTrabajo() {//0910
        return trabajo;//0910
    }

    public void setTrabajo(java.lang.String trabajox) {//0910
        this.trabajo = trabajox;//0910
    }

    public void setId(java.lang.String id1) {
        this.id = id1;
    }

    public java.lang.String getId() {
        return id;
    }

    public void setVlr(java.lang.String vlr1) {
        this.vlr = vlr1;
    }

    public java.lang.String getVlr() {
        return vlr;
    }

    public void setIdAccion(java.lang.String id_accion1) {
        this.id_accion = id_accion1;
    }

    public java.lang.String getIdAccion() {
        return id_accion;
    }

    public void setIdCliente(java.lang.String id_client1) {
        this.id_client = id_client1;
    }

    public java.lang.String getIdCliente() {
        return id_client;
    }

    public void setIdContratista(java.lang.String id_contratista1) {
        this.id_contratista = id_contratista1;
    }

    public java.lang.String getIdContratista() {
        return id_contratista;
    }

    public java.lang.String getEstado() {
        return estadito;
    }

    public void setEstado(java.lang.String estadito1) {
        this.estadito = estadito1;
    }

    public void setNombreContratista(java.lang.String nombre_contratista1) {
        this.nombre_contratista = nombre_contratista1;
    }

    public java.lang.String getNombreContratista() {
        return nombre_contratista;
    }

    public void setNombreCliente(java.lang.String nombre_client1) {
        this.nombre_client = nombre_client1;
    }

    public java.lang.String getNombreCliente() {
        return nombre_client;
    }

    public void setObservacion(java.lang.String obs1) {
        this.observacion = obs1;
    }

    public java.lang.String getObservacion() {
        return observacion;
    }

    public void setNumOs(java.lang.String x1) {
        this.num_os = x1;
    }

    public java.lang.String getNumOs() {
        return num_os;
    }

    public void setTelCli(java.lang.String x1) {
        this.tel_cli = x1;
    }

    public java.lang.String getTelCli() {
        return tel_cli;
    }

    public void setContacto(java.lang.String x1) {
        this.contacto = x1;
    }

    public java.lang.String getContacto() {
        return contacto;
    }

    public void setCuotas(java.lang.String x1) {
        this.cuotas = x1;
    }

    public java.lang.String getCuotas() {
        return cuotas;
    }

    public void setValCuotas(java.lang.String x1) {
        this.val_cuotas = x1;
    }

    public java.lang.String getValCuotas() {
        return val_cuotas;
    }

    public void setEstudio(java.lang.String x1) {
        this.estudio = x1;
    }

    public java.lang.String getEstudio() {
        return estudio;
    }

    public void setFacturaConformada(java.lang.String x1) {
        this.fact_conformada = x1;
    }

    public java.lang.String getFacturaConformada() {
        return fact_conformada;
    }

    public void setFecha(java.lang.String x1) {
        this.fecha = x1;
    }

    public java.lang.String getFecha() {
        return fecha;
    }

    public void setSimbolo(java.lang.String x1) {
        this.simbolo = x1;
    }

    public java.lang.String getSimbolo() {
        return simbolo;
    }

    public void setEcaOferta(java.lang.String x1) {
        this.eca_oferta = x1;
    }

    public java.lang.String getEcaOferta() {
        return eca_oferta;
    }

    public void setDifEcaOferta(java.lang.String x1) {
        this.dif_eca_oferta = x1;
    }

    public java.lang.String getDifEcaOferta() {
        return dif_eca_oferta;
    }

    public void setOferta(java.lang.String x1) {
        this.oferta = x1;
    }

    public java.lang.String getOferta() {
        return oferta;
    }

    public void setDifOferta(java.lang.String x1) {
        this.dif_oferta = x1;
    }

    public java.lang.String getDifOferta() {
        return dif_oferta;
    }

    public void setEsquemaComision(java.lang.String x1) {
        this.esquema_comision = x1;
    }

    public java.lang.String getEsquemaComision() {
        return esquema_comision;
    }

    public void setDifOfertaApplusNuevo(java.lang.String x1) {
        this.dif_oferta_applus_nuevo = x1;
    }

    public void setDifOfertaApplusAntiguo(java.lang.String x1) {
        this.dif_oferta_applus_antiguo = x1;
    }

    public void setDifEcaOfertaConsorcioNuevo(java.lang.String x1) {
        this.dif_ecaoferta_applus_nuevo = x1;
    }

    public void setDifEcaOfertaConsorcioAntiguo(java.lang.String x1) {
        this.dif_ecaoferta_applus_antiguo = x1;
    }

    public String getDifOfertaApplusNuevo() {
        return dif_oferta_applus_nuevo;
    }

    public String getDifOfertaApplusAntiguo() {
        return dif_oferta_applus_antiguo;
    }

    public String getDifEcaOfertaConsorcioNuevo() {
        return dif_ecaoferta_applus_nuevo;
    }

    public String getDifEcaOfertaConsorcioAntiguo() {
        return dif_ecaoferta_applus_antiguo;
    }

    public void setPrefactura(java.lang.String x1) {
        this.prefactura = x1;
    }

    public String getPrefactura() {
        return prefactura;
    }

    public void setFacturaEca(java.lang.String x1) {
        this.factura_eca = x1;
    }

    public String getFacturaEca() {
        return factura_eca;
    }

    public void setFacturaContratista(java.lang.String x1) {
        this.factura_contratista = x1;
    }

    public String getFacturaContratista() {
        return factura_contratista;
    }

    public void setFacturaRetencion(java.lang.String x1) {
        this.factura_retencion = x1;
    }

    public String getFacturaRetencion() {
        return factura_retencion;
    }

    public void setFacturaBoni(java.lang.String x1) {
        this.factura_bonificacion = x1;
    }

    public String getFacturaBoni() {
        return factura_bonificacion;
    }

    public void setNicClient(java.lang.String x1) {
        this.nic_client = x1;
    }

    public String getNicClient() {
        return nic_client;
    }

    public void setNitClient(java.lang.String x1) {
        this.nit_client = x1;
    }

    public String getNitClient() {
        return nit_client;
    }

    public void setObservacionOpen(java.lang.String obs1) {
        this.observacion_open = obs1;
    }

    public java.lang.String getObservacionOpen() {
        return observacion_open;
    }

    public void setFacturaApp(java.lang.String x1) {
        this.factura_app = x1;
    }

    public java.lang.String getFacturaApp() {
        return factura_app;
    }

    public void setFacturaPro(java.lang.String x1) {
        this.factura_pro = x1;
    }

    public java.lang.String getFacturaPro() {
        return factura_pro;
    }

    public void setFacturaComiEca(java.lang.String x1) {
        this.factura_comision_eca = x1;
    }

    public java.lang.String getFacturaComiEca() {
        return factura_comision_eca;
    }

    public void setEsquemaFinanciacion(java.lang.String x1) {
        this.esquema_financiacion = x1;
    }

    public java.lang.String getEsquemaFinanciacion() {
        return esquema_financiacion;
    }

    public void setFRecepcion(java.lang.String x1) {
        this.f_recepcion = x1;
    }

    public java.lang.String getFRecepcion() {
        return f_recepcion;
    }

    public void setTipoCliente(java.lang.String x1) {
        this.tipo_cliente = x1;
    }

    public java.lang.String getTipoCliente() {
        return tipo_cliente;
    }

    public void setFacturaFactoringPro(java.lang.String x1) {
        this.factura_factoring_pro = x1;
    }

    public java.lang.String getFacturaFactoringPro() {
        return factura_factoring_pro;
    }

    public void setAcciones(java.lang.String acc1) {
        this.acciones = acc1;
    }

    public java.lang.String getAcciones() {
        return acciones;
    }

    public java.lang.String getIdEstado() {
        return id_estadito;
    }

    public void setIdEstado(java.lang.String idestadito1) {
        this.id_estadito = idestadito1;
    }

    public java.lang.String getIdSolicitud() {//090922
        return id_solicitud;//090922
    }

    public void setIdSolicitud(java.lang.String id_solicitud1) {//090922
        this.id_solicitud = id_solicitud1;//090922
    }

    public java.lang.String getSolicitud() {//090922
        return solicitud;//090922
    }

    public void setSolicitud(java.lang.String solicitud1) {//090922
        this.solicitud = solicitud1;//090922
    }

    public java.lang.String getTipoTrabajo() {//
        return tipo_trabajo;//
    }

    public void setTipoTrabajo(java.lang.String tipo_trabajo1) {//
        this.tipo_trabajo = tipo_trabajo1;//
    }

    public java.lang.String getCreacionFechaEntregaOferta() {//090928
        return creacion_fecha_entrega_oferta;//
    }

    public void setCreacionFechaEntregaOferta(java.lang.String creacion_fecha_entrega_oferta1) {//
        this.creacion_fecha_entrega_oferta = creacion_fecha_entrega_oferta1;//
    }

    public java.lang.String getFechaEntregaOferta() {//090928
        return fecha_entrega_oferta;//
    }

    public void setFechaEntregaOferta(java.lang.String fecha_entrega_oferta1) {//
        this.fecha_entrega_oferta = fecha_entrega_oferta1;//
    }

    public java.lang.String getUsuarioEntregaOferta() {//090928
        return usuario_entrega_oferta;//
    }

    public void setUsuarioEntregaOferta(java.lang.String usuario_entrega_oferta1) {//
        this.usuario_entrega_oferta = usuario_entrega_oferta1;//
    }

    public java.lang.String getAlcances() {//091016
        return alcances;//091016
    }

    public void setAlcances(java.lang.String alcancesx) {//091016
        this.alcances = alcancesx;//091016
    }

    public java.lang.String getAdiciones() {//091016
        return adiciones;//091016
    }

    public void setAdiciones(java.lang.String adicionesx) {//091016
        this.adiciones = adicionesx;//091016
    }

    /**
     * Getter for property consecutivo_oferta.
     * @return Value of property consecutivo_oferta.
     */
    public java.lang.String getConsecutivo_oferta() {
        return consecutivo_oferta;
    }

    /**
     * Setter for property consecutivo_oferta.
     * @param consecutivo_oferta New value of property consecutivo_oferta.
     */
    public void setConsecutivo_oferta(java.lang.String consecutivo_oferta) {
        this.consecutivo_oferta = consecutivo_oferta;
    }


    /**
     * Get the value of venc_entregaOferta
     *
     * @return the value of venc_entregaOferta
     */
    public String getVenc_entregaOferta() {
        return venc_entregaOferta;
    }

    /**
     * Set the value of venc_entregaOferta
     *
     * @param venc_entregaOferta new value of venc_entregaOferta
     */
    public void setVenc_entregaOferta(String venc_entregaOferta) {
        this.venc_entregaOferta = venc_entregaOferta;
    }


    /**
     * Get the value of fCotizacion
     *
     * @return the value of fCotizacion
     */
    public String getFCotizacion() {
        return fCotizacion;
    }

    /**
     * Set the value of fCotizacion
     *
     * @param fCotizacion new value of fCotizacion
     */
    public void setFCotizacion(String fCotizacion) {
        this.fCotizacion = fCotizacion;
    }

    /**
     * Get the value of fAsignacion_contratista
     *
     * @return the value of fAsignacion_contratista
     */
    public String getFAsignacion_contratista() {
        return fAsignacion_contratista;
    }

    /**
     * Set the value of fAsignacion_contratista
     *
     * @param fAsignacion_contratista new value of fAsignacion_contratista
     */
    public void setFAsignacion_contratista(String fAsignacion_contratista) {
        this.fAsignacion_contratista = fAsignacion_contratista;
    }

    /**
     * Get the value of fVerificacion_cartera
     *
     * @return the value of fVerificacion_cartera
     */
    public String getFVerificacion_cartera() {
        return fVerificacion_cartera;
    }

    /**
     * Set the value of fVerificacion_cartera
     *
     * @param fVerificacion_cartera new value of fVerificacion_cartera
     */
    public void setFVerificacion_cartera(String fVerificacion_cartera) {
        this.fVerificacion_cartera = fVerificacion_cartera;
    }

    /**
     * Get the value of dptoCliente
     *
     * @return the value of dptoCliente
     */
    public String getDptoCliente() {
        return dptoCliente;
    }

    /**
     * Set the value of dptoCliente
     *
     * @param dptoCliente new value of dptoCliente
     */
    public void setDptoCliente(String dptoCliente) {
        this.dptoCliente = dptoCliente;
    }


    /**
     * Get the value of venc_totales
     *
     * @return the value of venc_totales
     */
    public boolean isVenc_totales() {
        return venc_totales;
    }

    /**
     * Set the value of venc_totales
     *
     * @param venc_totales new value of venc_totales
     */
    public void setVenc_totales(boolean venc_totales) {
        this.venc_totales = venc_totales;
    }

    /**
     * Get the value of venc_oferta
     *
     * @return the value of venc_oferta
     */
    public boolean isVenc_oferta() {
        return venc_oferta;
    }

    /**
     * Set the value of venc_oferta
     *
     * @param venc_oferta new value of venc_oferta
     */
    public void setVenc_oferta(boolean venc_oferta) {
        this.venc_oferta = venc_oferta;
    }

    /**
     * Get the value of venc_cotizacion
     *
     * @return the value of venc_cotizacion
     */
    public boolean isVenc_cotizacion() {
        return venc_cotizacion;
    }

    /**
     * Set the value of venc_cotizacion
     *
     * @param venc_cotizacion new value of venc_cotizacion
     */
    public void setVenc_cotizacion(boolean venc_cotizacion) {
        this.venc_cotizacion = venc_cotizacion;
    }

    /**
     * Get the value of venc_visita
     *
     * @return the value of venc_visita
     */
    public boolean isVenc_visita() {
        return venc_visita;
    }

    /**
     * Set the value of venc_visita
     *
     * @param venc_visita new value of venc_visita
     */
    public void setVenc_visita(boolean venc_visita) {
        this.venc_visita = venc_visita;
    }


    /**
     * Get the value of venc_totales
     *
     * @return the value of venc_totales
     */
    public String getDiaVenc_totales() {
        return diaVenc_totales;
    }

    /**
     * Set the value of venc_totales
     *
     * @param venc_totales new value of venc_totales
     */
    public void setDiaVenc_totales(String venc_totales) {
        this.diaVenc_totales = venc_totales;
    }

    /**
     * Get the value of venc_entregaOferta
     *
     * @return the value of venc_entregaOferta
     */
    public String getDiaVenc_entregaOferta() {
        return diaVenc_entregaOferta;
    }

    /**
     * Set the value of venc_entregaOferta
     *
     * @param venc_entregaOferta new value of venc_entregaOferta
     */
    public void setDiaVenc_entregaOferta(String venc_entregaOferta) {
        this.diaVenc_entregaOferta = venc_entregaOferta;
    }

    /**
     * Get the value of venc_cotizacion
     *
     * @return the value of venc_cotizacion
     */
    public String getDiaVenc_cotizacion() {
        return diaVenc_cotizacion;
    }

    /**
     * Set the value of venc_cotizacion
     *
     * @param venc_cotizacion new value of venc_cotizacion
     */
    public void setDiaVenc_cotizacion(String venc_cotizacion) {
        this.diaVenc_cotizacion = venc_cotizacion;
    }

    /**
     * Get the value of venc_visita
     *
     * @return the value of venc_visita
     */
    public String getDiaVenc_visita() {
        return diaVenc_visita;
    }

    /**
     * Set the value of venc_visita
     *
     * @param venc_visita new value of venc_visita
     */
    public void setDiaVenc_visita(String venc_visita) {
        this.diaVenc_visita = venc_visita;
    }


    public String getFecFactConformed() {//20100714
        return fecFactConformed;//20100714
    }//20100714
    public void setFecFactConformed(String x1) {//20100714
        this.fecFactConformed= x1;//20100714
    }//20100714

}