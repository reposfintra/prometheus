/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.opav.model.beans;




/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;





/**
 *
 * @author Alvaro
 */


public class FacturaRefinanciada implements Serializable {
    
    
    private boolean seleccionada;
    private String id_cliente;
    private String documento;
    private String nit;
    private int    cuota;
    private String fechaFactura;
    private String fechaVencimiento;
    private String multiservicio;
    private String simboloVariable;
    private double valorFactura;
    private double valorSaldo;
    private double valorAbono;
    private double valorCapital;
    private double valorIntereses;
    
    private double ivaNM = 0.00;
    private double ivaNMFinal = 0.00;
    private double valorRefinanciar = 0.00;
    private double valorInteresesPendientes = 0.00;
    private double valorMora = 0.00;
    private int    diasMora = 0;
    private int    diasAdicionales = 0;
    private double valorAdicional = 0.00;
    
    

    
    
    

    
    
    /** Creates a new instance of Actividad */
    public static FacturaRefinanciada load(ResultSet rs)throws SQLException { 
        FacturaRefinanciada facturaRefinanciada =  new FacturaRefinanciada();
        
        facturaRefinanciada.setId_cliente(rs.getString("id_cliente"));
        facturaRefinanciada.setDocumento(rs.getString("documento"));
        facturaRefinanciada.setCuota(rs.getInt("cuota"));
        facturaRefinanciada.setNit( rs.getString("nit") );
        facturaRefinanciada.setFechaFactura(rs.getString("fecha_factura"));
        facturaRefinanciada.setFechaVencimiento(rs.getString("fecha_vencimiento"));
        
        facturaRefinanciada.setMultiservicio(rs.getString("multiservicio"));
        facturaRefinanciada.setSimboloVariable(rs.getString("simbolo_variable"));
        facturaRefinanciada.setValorFactura(rs.getDouble("valor_factura"));
        facturaRefinanciada.setValorAbono(rs.getDouble("valor_abono"));
        facturaRefinanciada.setValorSaldo(rs.getDouble("valor_saldo"));
        facturaRefinanciada.setValorCapital(rs.getDouble("valor_capital"));
        facturaRefinanciada.setValorIntereses(rs.getDouble("valor_intereses"));
        
        
        facturaRefinanciada.setValorRefinanciar(facturaRefinanciada.getValorCapital() ) ;
        facturaRefinanciada.setValorInteresesPendientes(facturaRefinanciada.getValorIntereses() ) ;
        facturaRefinanciada.setValorMora(0.00 ) ;
        facturaRefinanciada.setDiasMora(0 ) ;
        facturaRefinanciada.setSeleccionada(false);
        facturaRefinanciada.setIvaNM(0.00);
        facturaRefinanciada.setIvaNMFinal(0.00);

        facturaRefinanciada.setValorAdicional(0.00 ) ;
        facturaRefinanciada.setDiasAdicionales(0 ) ;        
       
        
        
        return facturaRefinanciada;
    }    
    
    
    
    
    
    
    

    /**
     * @return the documento
     */
    public String getDocumento() {
        return documento;
    }

    /**
     * @param documento the documento to set
     */
    public void setDocumento(String documento) {
        this.documento = documento;
    }

    /**
     * @return the cuota
     */
    public int getCuota() {
        return cuota;
    }

    /**
     * @param cuota the cuota to set
     */
    public void setCuota(int cuota) {
        this.cuota = cuota;
    }

    /**
     * @return the fechaFactura
     */
    public String getFechaFactura() {
        return fechaFactura;
    }

    /**
     * @param fechaFactura the fechaFactura to set
     */
    public void setFechaFactura(String fechaFactura) {
        this.fechaFactura = fechaFactura;
    }

    /**
     * @return the fechaVencimiento
     */
    public String getFechaVencimiento() {
        return fechaVencimiento;
    }

    /**
     * @param fechaVencimiento the fechaVencimiento to set
     */
    public void setFechaVencimiento(String fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    /**
     * @return the multiservicio
     */
    public String getMultiservicio() {
        return multiservicio;
    }

    /**
     * @param multiservicio the multiservicio to set
     */
    public void setMultiservicio(String multiservicio) {
        this.multiservicio = multiservicio;
    }

    /**
     * @return the simboloVariable
     */
    public String getSimboloVariable() {
        return simboloVariable;
    }

    /**
     * @param simboloVariable the simboloVariable to set
     */
    public void setSimboloVariable(String simboloVariable) {
        this.simboloVariable = simboloVariable;
    }

    /**
     * @return the valorFactura
     */
    public double getValorFactura() {
        return valorFactura;
    }

    /**
     * @param valorFactura the valorFactura to set
     */
    public void setValorFactura(double valorFactura) {
        this.valorFactura = valorFactura;
    }

    /**
     * @return the valorSaldo
     */
    public double getValorSaldo() {
        return valorSaldo;
    }

    /**
     * @param valorSaldo the valorSaldo to set
     */
    public void setValorSaldo(double valorSaldo) {
        this.valorSaldo = valorSaldo;
    }

    /**
     * @return the valorCapital
     */
    public double getValorCapital() {
        return valorCapital;
    }

    /**
     * @param valorCapital the valorCapital to set
     */
    public void setValorCapital(double valorCapital) {
        this.valorCapital = valorCapital;
    }



    /**
     * @return the valorAbono
     */
    public double getValorAbono() {
        return valorAbono;
    }

    /**
     * @param valorAbono the valorAbono to set
     */
    public void setValorAbono(double valorAbono) {
        this.valorAbono = valorAbono;
    }

    /**
     * @return the valorRefinanciar
     */
    public double getValorRefinanciar() {
        return valorRefinanciar;
    }

    /**
     * @param valorRefinanciar the valorRefinanciar to set
     */
    public void setValorRefinanciar(double valorRefinanciar) {
        this.valorRefinanciar = valorRefinanciar;
    }

    /**
     * @return the valorIntereses
     */
    public double getValorIntereses() {
        return valorIntereses;
    }

    /**
     * @param valorIntereses the valorIntereses to set
     */
    public void setValorIntereses(double valorIntereses) {
        this.valorIntereses = valorIntereses;
    }

    /**
     * @return the valorMora
     */
    public double getValorMora() {
        return valorMora;
    }

    /**
     * @param valorMora the valorMora to set
     */
    public void setValorMora(double valorMora) {
        this.valorMora = valorMora;
    }

    /**
     * @return the valorInteresesPendientes
     */
    public double getValorInteresesPendientes() {
        return valorInteresesPendientes;
    }

    /**
     * @param valorInteresesPendientes the valorInteresesPendientes to set
     */
    public void setValorInteresesPendientes(double valorInteresesPendientes) {
        this.valorInteresesPendientes = valorInteresesPendientes;
    }

    /**
     * @return the nit
     */
    public String getNit() {
        return nit;
    }

    /**
     * @param nit the nit to set
     */
    public void setNit(String nit) {
        this.nit = nit;
    }

    /**
     * @return the id_cliente
     */
    public String getId_cliente() {
        return id_cliente;
    }

    /**
     * @param id_cliente the id_cliente to set
     */
    public void setId_cliente(String id_cliente) {
        this.id_cliente = id_cliente;
    }

    /**
     * @return the diasMora
     */
    public int getDiasMora() {
        return diasMora;
    }

    /**
     * @param diasMora the diasMora to set
     */
    public void setDiasMora(int diasMora) {
        this.diasMora = diasMora;
    }

    /**
     * @return the seleccionada
     */
    public boolean isSeleccionada() {
        return seleccionada;
    }

    /**
     * @param seleccionada the seleccionada to set
     */
    public void setSeleccionada(boolean seleccionada) {
        this.seleccionada = seleccionada;
    }

    /**
     * @return the ivaNM
     */
    public double getIvaNM() {
        return ivaNM;
    }

    /**
     * @param ivaNM the ivaNM to set
     */
    public void setIvaNM(double ivaNM) {
        this.ivaNM = ivaNM;
    }

    /**
     * @return the ivaNMFinal
     */
    public double getIvaNMFinal() {
        return ivaNMFinal;
    }

    /**
     * @param ivaNMFinal the ivaNMFinal to set
     */
    public void setIvaNMFinal(double ivaNMFinal) {
        this.ivaNMFinal = ivaNMFinal;
    }

    /**
     * @return the diasAdicionales
     */
    public int getDiasAdicionales() {
        return diasAdicionales;
    }

    /**
     * @param diasAdicionales the diasAdicionales to set
     */
    public void setDiasAdicionales(int diasAdicionales) {
        this.diasAdicionales = diasAdicionales;
    }

    /**
     * @return the valorAdicional
     */
    public double getValorAdicional() {
        return valorAdicional;
    }

    /**
     * @param valorAdicional the valorAdicional to set
     */
    public void setValorAdicional(double valorAdicional) {
        this.valorAdicional = valorAdicional;
    }

}


