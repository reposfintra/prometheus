/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.opav.model.beans;

/**
 *
 * @author Alvaro
 */
public class ItemCxPContratista {

    private String referencia;
    private String descripcion;
    private double total_vlr ;





    public static ItemCxPContratista load(java.sql.ResultSet rs)throws java.sql.SQLException{

        ItemCxPContratista itemCxPContratista = new ItemCxPContratista();

        itemCxPContratista.setReferencia( rs.getString("referencia") );
        itemCxPContratista.setDescripcion(rs.getString("descripcion") );
        itemCxPContratista.setTotal_vlr(rs.getDouble("total_vlr") );

        return itemCxPContratista;
    }








    /**
     * @return the referencia
     */
    public String getReferencia() {
        return referencia;
    }

    /**
     * @param referencia the referencia to set
     */
    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    /**
     * @return the descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * @param descripcion the descripcion to set
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * @return the total_vlr
     */
    public double getTotal_vlr() {
        return total_vlr;
    }

    /**
     * @param total_vlr the total_vlr to set
     */
    public void setTotal_vlr(double total_vlr) {
        this.total_vlr = total_vlr;
    }








}