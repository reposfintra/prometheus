/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.opav.model.beans;

/**
 *
 * @author Alvaro
 */
public class IngresoCabecera {

    private String reg_status;
    private String dstrct;
    private String tipo_documento;
    private String num_ingreso;
    private String codcli;
    private String nitcli;
    private String concepto;
    private String tipo_ingreso;
    private String fecha_consignacion ;
    private String fecha_ingreso;
    private String branch_code;
    private String bank_account_no;
    private String codmoneda;
    private String agencia_ingreso;
    private String descripcion_ingreso;
    private String periodo;
    private double vlr_ingreso;
    private double vlr_ingreso_me;
    private double vlr_tasa;
    private String fecha_tasa;
    private int    cant_item ;
    private int    transaccion ;
    private int    transaccion_anulacion;
    private String fecha_impresion;
    private String fecha_contabilizacion;
    private String fecha_anulacion_contabilizacion;
    private String fecha_anulacion ;
    private String creation_user ;
    private String creation_date ;
    private String user_update ;
    private String last_update ;
    private String base ;
    private String nro_consignacion ;
    private String periodo_anulacion ;
    private String cuenta ;
    private String auxiliar ;
    private String abc ;
    private double tasa_dol_bol ;
    private double saldo_ingreso ;
    private String cmc ;
    private String corficolombiana ;
    private String fec_envio_fiducia ;
    private String tipo_referencia_1 ;
    private String referencia_1 ;
    private String tipo_referencia_2 ;
    private String referencia_2 ;
    private String tipo_referencia_3 ;
    private String referencia_3 ;

    /**
     * @return the reg_status
     */
    public String getReg_status() {
        return reg_status;
    }

    /**
     * @param reg_status the reg_status to set
     */
    public void setReg_status(String reg_status) {
        this.reg_status = reg_status;
    }

    /**
     * @return the dstrct
     */
    public String getDstrct() {
        return dstrct;
    }

    /**
     * @param dstrct the dstrct to set
     */
    public void setDstrct(String dstrct) {
        this.dstrct = dstrct;
    }

    /**
     * @return the tipo_documento
     */
    public String getTipo_documento() {
        return tipo_documento;
    }

    /**
     * @param tipo_documento the tipo_documento to set
     */
    public void setTipo_documento(String tipo_documento) {
        this.tipo_documento = tipo_documento;
    }

    /**
     * @return the num_ingreso
     */
    public String getNum_ingreso() {
        return num_ingreso;
    }

    /**
     * @param num_ingreso the num_ingreso to set
     */
    public void setNum_ingreso(String num_ingreso) {
        this.num_ingreso = num_ingreso;
    }

    /**
     * @return the codcli
     */
    public String getCodcli() {
        return codcli;
    }

    /**
     * @param codcli the codcli to set
     */
    public void setCodcli(String codcli) {
        this.codcli = codcli;
    }

    /**
     * @return the nitcli
     */
    public String getNitcli() {
        return nitcli;
    }

    /**
     * @param nitcli the nitcli to set
     */
    public void setNitcli(String nitcli) {
        this.nitcli = nitcli;
    }

    /**
     * @return the concepto
     */
    public String getConcepto() {
        return concepto;
    }

    /**
     * @param concepto the concepto to set
     */
    public void setConcepto(String concepto) {
        this.concepto = concepto;
    }

    /**
     * @return the tipo_ingreso
     */
    public String getTipo_ingreso() {
        return tipo_ingreso;
    }

    /**
     * @param tipo_ingreso the tipo_ingreso to set
     */
    public void setTipo_ingreso(String tipo_ingreso) {
        this.tipo_ingreso = tipo_ingreso;
    }

    /**
     * @return the fecha_consignacion
     */
    public String getFecha_consignacion() {
        return fecha_consignacion;
    }

    /**
     * @param fecha_consignacion the fecha_consignacion to set
     */
    public void setFecha_consignacion(String fecha_consignacion) {
        this.fecha_consignacion = fecha_consignacion;
    }

    /**
     * @return the fecha_ingreso
     */
    public String getFecha_ingreso() {
        return fecha_ingreso;
    }

    /**
     * @param fecha_ingreso the fecha_ingreso to set
     */
    public void setFecha_ingreso(String fecha_ingreso) {
        this.fecha_ingreso = fecha_ingreso;
    }

    /**
     * @return the branch_code
     */
    public String getBranch_code() {
        return branch_code;
    }

    /**
     * @param branch_code the branch_code to set
     */
    public void setBranch_code(String branch_code) {
        this.branch_code = branch_code;
    }

    /**
     * @return the bank_account_no
     */
    public String getBank_account_no() {
        return bank_account_no;
    }

    /**
     * @param bank_account_no the bank_account_no to set
     */
    public void setBank_account_no(String bank_account_no) {
        this.bank_account_no = bank_account_no;
    }

    /**
     * @return the codmoneda
     */
    public String getCodmoneda() {
        return codmoneda;
    }

    /**
     * @param codmoneda the codmoneda to set
     */
    public void setCodmoneda(String codmoneda) {
        this.codmoneda = codmoneda;
    }

    /**
     * @return the agencia_ingreso
     */
    public String getAgencia_ingreso() {
        return agencia_ingreso;
    }

    /**
     * @param agencia_ingreso the agencia_ingreso to set
     */
    public void setAgencia_ingreso(String agencia_ingreso) {
        this.agencia_ingreso = agencia_ingreso;
    }

    /**
     * @return the descripcion_ingreso
     */
    public String getDescripcion_ingreso() {
        return descripcion_ingreso;
    }

    /**
     * @param descripcion_ingreso the descripcion_ingreso to set
     */
    public void setDescripcion_ingreso(String descripcion_ingreso) {
        this.descripcion_ingreso = descripcion_ingreso;
    }

    /**
     * @return the periodo
     */
    public String getPeriodo() {
        return periodo;
    }

    /**
     * @param periodo the periodo to set
     */
    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }

    /**
     * @return the vlr_ingreso
     */
    public double getVlr_ingreso() {
        return vlr_ingreso;
    }

    /**
     * @param vlr_ingreso the vlr_ingreso to set
     */
    public void setVlr_ingreso(double vlr_ingreso) {
        this.vlr_ingreso = vlr_ingreso;
    }

    /**
     * @return the vlr_ingreso_me
     */
    public double getVlr_ingreso_me() {
        return vlr_ingreso_me;
    }

    /**
     * @param vlr_ingreso_me the vlr_ingreso_me to set
     */
    public void setVlr_ingreso_me(double vlr_ingreso_me) {
        this.vlr_ingreso_me = vlr_ingreso_me;
    }

    /**
     * @return the vlr_tasa
     */
    public double getVlr_tasa() {
        return vlr_tasa;
    }

    /**
     * @param vlr_tasa the vlr_tasa to set
     */
    public void setVlr_tasa(double vlr_tasa) {
        this.vlr_tasa = vlr_tasa;
    }

    /**
     * @return the fecha_tasa
     */
    public String getFecha_tasa() {
        return fecha_tasa;
    }

    /**
     * @param fecha_tasa the fecha_tasa to set
     */
    public void setFecha_tasa(String fecha_tasa) {
        this.fecha_tasa = fecha_tasa;
    }

    /**
     * @return the cant_item
     */
    public int getCant_item() {
        return cant_item;
    }

    /**
     * @param cant_item the cant_item to set
     */
    public void setCant_item(int cant_item) {
        this.cant_item = cant_item;
    }

    /**
     * @return the transaccion
     */
    public int getTransaccion() {
        return transaccion;
    }

    /**
     * @param transaccion the transaccion to set
     */
    public void setTransaccion(int transaccion) {
        this.transaccion = transaccion;
    }

    /**
     * @return the transaccion_anulacion
     */
    public int getTransaccion_anulacion() {
        return transaccion_anulacion;
    }

    /**
     * @param transaccion_anulacion the transaccion_anulacion to set
     */
    public void setTransaccion_anulacion(int transaccion_anulacion) {
        this.transaccion_anulacion = transaccion_anulacion;
    }

    /**
     * @return the fecha_impresion
     */
    public String getFecha_impresion() {
        return fecha_impresion;
    }

    /**
     * @param fecha_impresion the fecha_impresion to set
     */
    public void setFecha_impresion(String fecha_impresion) {
        this.fecha_impresion = fecha_impresion;
    }

    /**
     * @return the fecha_contabilizacion
     */
    public String getFecha_contabilizacion() {
        return fecha_contabilizacion;
    }

    /**
     * @param fecha_contabilizacion the fecha_contabilizacion to set
     */
    public void setFecha_contabilizacion(String fecha_contabilizacion) {
        this.fecha_contabilizacion = fecha_contabilizacion;
    }

    /**
     * @return the fecha_anulacion_contabilizacion
     */
    public String getFecha_anulacion_contabilizacion() {
        return fecha_anulacion_contabilizacion;
    }

    /**
     * @param fecha_anulacion_contabilizacion the fecha_anulacion_contabilizacion to set
     */
    public void setFecha_anulacion_contabilizacion(String fecha_anulacion_contabilizacion) {
        this.fecha_anulacion_contabilizacion = fecha_anulacion_contabilizacion;
    }

    /**
     * @return the fecha_anulacion
     */
    public String getFecha_anulacion() {
        return fecha_anulacion;
    }

    /**
     * @param fecha_anulacion the fecha_anulacion to set
     */
    public void setFecha_anulacion(String fecha_anulacion) {
        this.fecha_anulacion = fecha_anulacion;
    }

    /**
     * @return the creation_user
     */
    public String getCreation_user() {
        return creation_user;
    }

    /**
     * @param creation_user the creation_user to set
     */
    public void setCreation_user(String creation_user) {
        this.creation_user = creation_user;
    }

    /**
     * @return the creation_date
     */
    public String getCreation_date() {
        return creation_date;
    }

    /**
     * @param creation_date the creation_date to set
     */
    public void setCreation_date(String creation_date) {
        this.creation_date = creation_date;
    }

    /**
     * @return the user_update
     */
    public String getUser_update() {
        return user_update;
    }

    /**
     * @param user_update the user_update to set
     */
    public void setUser_update(String user_update) {
        this.user_update = user_update;
    }

    /**
     * @return the last_update
     */
    public String getLast_update() {
        return last_update;
    }

    /**
     * @param last_update the last_update to set
     */
    public void setLast_update(String last_update) {
        this.last_update = last_update;
    }

    /**
     * @return the base
     */
    public String getBase() {
        return base;
    }

    /**
     * @param base the base to set
     */
    public void setBase(String base) {
        this.base = base;
    }

    /**
     * @return the nro_consignacion
     */
    public String getNro_consignacion() {
        return nro_consignacion;
    }

    /**
     * @param nro_consignacion the nro_consignacion to set
     */
    public void setNro_consignacion(String nro_consignacion) {
        this.nro_consignacion = nro_consignacion;
    }

    /**
     * @return the periodo_anulacion
     */
    public String getPeriodo_anulacion() {
        return periodo_anulacion;
    }

    /**
     * @param periodo_anulacion the periodo_anulacion to set
     */
    public void setPeriodo_anulacion(String periodo_anulacion) {
        this.periodo_anulacion = periodo_anulacion;
    }

    /**
     * @return the cuenta
     */
    public String getCuenta() {
        return cuenta;
    }

    /**
     * @param cuenta the cuenta to set
     */
    public void setCuenta(String cuenta) {
        this.cuenta = cuenta;
    }

    /**
     * @return the auxiliar
     */
    public String getAuxiliar() {
        return auxiliar;
    }

    /**
     * @param auxiliar the auxiliar to set
     */
    public void setAuxiliar(String auxiliar) {
        this.auxiliar = auxiliar;
    }

    /**
     * @return the abc
     */
    public String getAbc() {
        return abc;
    }

    /**
     * @param abc the abc to set
     */
    public void setAbc(String abc) {
        this.abc = abc;
    }

    /**
     * @return the tasa_dol_bol
     */
    public double getTasa_dol_bol() {
        return tasa_dol_bol;
    }

    /**
     * @param tasa_dol_bol the tasa_dol_bol to set
     */
    public void setTasa_dol_bol(double tasa_dol_bol) {
        this.tasa_dol_bol = tasa_dol_bol;
    }

    /**
     * @return the saldo_ingreso
     */
    public double getSaldo_ingreso() {
        return saldo_ingreso;
    }

    /**
     * @param saldo_ingreso the saldo_ingreso to set
     */
    public void setSaldo_ingreso(double saldo_ingreso) {
        this.saldo_ingreso = saldo_ingreso;
    }

    /**
     * @return the cmc
     */
    public String getCmc() {
        return cmc;
    }

    /**
     * @param cmc the cmc to set
     */
    public void setCmc(String cmc) {
        this.cmc = cmc;
    }

    /**
     * @return the corficolombiana
     */
    public String getCorficolombiana() {
        return corficolombiana;
    }

    /**
     * @param corficolombiana the corficolombiana to set
     */
    public void setCorficolombiana(String corficolombiana) {
        this.corficolombiana = corficolombiana;
    }

    /**
     * @return the fec_envio_fiducia
     */
    public String getFec_envio_fiducia() {
        return fec_envio_fiducia;
    }

    /**
     * @param fec_envio_fiducia the fec_envio_fiducia to set
     */
    public void setFec_envio_fiducia(String fec_envio_fiducia) {
        this.fec_envio_fiducia = fec_envio_fiducia;
    }

    /**
     * @return the tipo_referencia_1
     */
    public String getTipo_referencia_1() {
        return tipo_referencia_1;
    }

    /**
     * @param tipo_referencia_1 the tipo_referencia_1 to set
     */
    public void setTipo_referencia_1(String tipo_referencia_1) {
        this.tipo_referencia_1 = tipo_referencia_1;
    }

    /**
     * @return the referencia_1
     */
    public String getReferencia_1() {
        return referencia_1;
    }

    /**
     * @param referencia_1 the referencia_1 to set
     */
    public void setReferencia_1(String referencia_1) {
        this.referencia_1 = referencia_1;
    }

    /**
     * @return the tipo_referencia_2
     */
    public String getTipo_referencia_2() {
        return tipo_referencia_2;
    }

    /**
     * @param tipo_referencia_2 the tipo_referencia_2 to set
     */
    public void setTipo_referencia_2(String tipo_referencia_2) {
        this.tipo_referencia_2 = tipo_referencia_2;
    }

    /**
     * @return the referencia_2
     */
    public String getReferencia_2() {
        return referencia_2;
    }

    /**
     * @param referencia_2 the referencia_2 to set
     */
    public void setReferencia_2(String referencia_2) {
        this.referencia_2 = referencia_2;
    }

    /**
     * @return the tipo_referencia_3
     */
    public String getTipo_referencia_3() {
        return tipo_referencia_3;
    }

    /**
     * @param tipo_referencia_3 the tipo_referencia_3 to set
     */
    public void setTipo_referencia_3(String tipo_referencia_3) {
        this.tipo_referencia_3 = tipo_referencia_3;
    }

    /**
     * @return the referencia_3
     */
    public String getReferencia_3() {
        return referencia_3;
    }

    /**
     * @param referencia_3 the referencia_3 to set
     */
    public void setReferencia_3(String referencia_3) {
        this.referencia_3 = referencia_3;
    }

}