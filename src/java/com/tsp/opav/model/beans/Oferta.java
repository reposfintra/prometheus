/* * Oferta.java * * Created on 1 de junio de 2009, 8:55 */
package com.tsp.opav.model.beans;
/** * * @author  Fintra */
public class Oferta {
    private String id_orden, id_cliente, costo_oferta_applus, costo_oferta_eca,
       importe_oferta, id_estado_negocio, cuotas, valor_cuotas_r, detalle_inconsistencia,
       fecha_envio_ws, last_update_finv, user_update, marca_ws, fecha_oferta,
       fecha_registro, num_os, estudio_economico, simbolo_variable,
       tipo_dtf, esquema_comision, consecutivo, simbolo_variable_cr,
       comentario, f_recepcion,direccion_cli,telefono_cli,new_num_os,OtrasConsideraciones;

    String nombre_cliente,nombre_estado;
    public Oferta() {    }

    public String getIdOrden() {
        return id_orden;
    }
    public void setIdOrden(String x) {
        this.id_orden= x;
    }

    public String getNewNumOs() {
        return new_num_os;
    }
    public void setNewNumOs(String x) {
        this.new_num_os= x;
    }

    public String getIdCliente() {
        return id_cliente;
    }
    public void setIdCliente(String x) {
        this.id_cliente= x;
    }
    public String getCostoOfertaApplus() {
        return costo_oferta_applus;
    }
    public void setCostoOfertaApplus(String x) {
        this.costo_oferta_applus= x;
    }
    public String getCostoOfertaEca() {
        return costo_oferta_eca;
    }
    public void setCostoOfertaEca(String x) {
        this.costo_oferta_eca= x;
    }
    public String getImporteOferta() {
        return importe_oferta;
    }
    public void setImporteOferta(String x) {
        this.importe_oferta= x;
    }
    public String getIdEstadoNegocio() {
        return id_estado_negocio;
    }
    public void setIdEstadoNegocio(String x) {
        this.id_estado_negocio= x;
    }
    public String getCuotas() {//sale de jsp
        return cuotas;
    }
    public void setCuotas(String x) {
        this.cuotas= x;
    }
    public String getValorCuotasR() {
        return valor_cuotas_r;
    }
    public void setValorCuotasR(String x) {
        this.valor_cuotas_r= x;
    }
    public String getDetalleInconsistencia() {//sale de jsp
        return detalle_inconsistencia;
    }
    public void setDetalleInconsistencia(String x) {
        this.detalle_inconsistencia= x;
    }
    public String getFechaEnvioWs() {
        return fecha_envio_ws;
    }
    public void setFechaEnvioWs(String x) {
        this.fecha_envio_ws= x;
    }
    public String getLastUpdate() {
        return last_update_finv;
    }
    public void setLastUpdate(String x) {
        this.last_update_finv= x;
    }
    public String getUserUpdate() {
        return user_update;
    }
    public void setUserUpdate(String x) {
        this.user_update= x;
    }
    public String getMarcaWs() {
        return marca_ws;
    }
    public void setMarcaWs(String x) {
        this.marca_ws= x;
    }
    public String getFechaOferta() {//sale de jsp
        return fecha_oferta;
    }
    public void setFechaOferta(String x) {
        this.fecha_oferta= x;
    }
    public String getFechaRegistro() {
        return fecha_registro;
    }
    public void setFechaRegistro(String x) {
        this.fecha_registro= x;
    }
    public String getNumOs() {
        return num_os;
    }
    public void setNumOs(String x) {
        this.num_os= x;
    }
    public String getEstudioEconomico() {//sale de jsp
        return estudio_economico;
    }
    public void setEstudioEconomico(String x) {
        this.estudio_economico= x;
    }
    public String getSimboloVariable() {
        return simbolo_variable;
    }
    public void setSimboloVariable(String x) {
        this.simbolo_variable= x;
    }
    public String getTipoDtf() {
        return tipo_dtf;
    }
    public void setTipoDtf(String x) {
        this.tipo_dtf= x;
    }
    public String getEsquemaComision() {//sale de jsp
        return esquema_comision;
    }
    public void setEsquemaComision(String x) {
        this.esquema_comision= x;
    }
    public String getConsecutivo() {//sale de jsp
        return consecutivo;
    }
    public void setConsecutivo(String x) {
        this.consecutivo= x;
    }
    public String getSimboloVariableCr() {
        return simbolo_variable_cr;
    }
    public void setSimboloVariableCr(String x) {
        this.simbolo_variable_cr= x;
    }
    public String getFRecepcion() {
        return f_recepcion;
    }
    public void setFRecepcion(String x) {
        this.f_recepcion= x;
    }
    public String getNombreCliente() {//sale de id_cliente
        return nombre_cliente;
    }
    public void setNombreCliente(String x) {
        this.nombre_cliente= x;
    }
    public String getNombreEstado() {//sale de id_estado_negocio
        return nombre_estado;
    }
    public void setNombreEstado(String x) {
        this.nombre_estado= x;
    }

    public String getDirClient() {
        return direccion_cli;
    }
    public void setDirClient(String x) {
        this.direccion_cli= x;
    }
    public String getTelClient() {
        return telefono_cli;
    }
    public void setTelClient(String x) {
        this.telefono_cli= x;
    }
    
     public String getOtrasConsideraciones() {
        return OtrasConsideraciones;
    }
    
    public String setOtrasConsideraciones(String x) {
        return OtrasConsideraciones = x;
    }

   


}