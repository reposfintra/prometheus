/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.opav.model.beans;

/**
 *
 * @author Alvaro
 */
public class FacturaGeneral {




  private String  id_contratista ;
  private String  prefactura;

  private double  vlr_mat;
  private double  vlr_mob;
  private double  vlr_otr;

  private double  vlr_mat_mob_otr;
  private double  vlr_administracion;
  private double  vlr_imprevisto;

  private double  vlr_utilidad;
  private double  vlr_bonificacion;
  private double  vlr_iva;
  private double  vlr_rmat;
  private double  vlr_rmob;
  private double  vlr_rotr;

  private double  vlr_retencion;

  private double  vlr_factoring;
  private double  vlr_formula;

  private double  vlr_base_iva;

  private double vlr_formula_provintegral;

  private String id_accion;


  private double val_ret_ica_contratista;
  private double val_ret_iva_contratista;
  private double val_ret_aiu_contratista;

  private String cuenta_iva_contratista;
  private String cuenta_ret_material_contratista;
  private String cuenta_ret_mano_obra_contratista;
  private String cuenta_ret_otros_contratista;
  private String cuenta_ret_aiu_contratista;
  private String cuenta_ret_ica_contratista;
  private String cuenta_ret_iva_contratista;
  private String tipo_distribucion;
  private String num_os;



    /** Creates a new instance of FacturaGeneral */
    public FacturaGeneral() {
    }

    public static FacturaGeneral load(java.sql.ResultSet rs)throws java.sql.SQLException{

        FacturaGeneral facturaGeneral = new FacturaGeneral();

        facturaGeneral.setId_contratista( rs.getString("id_contratista") );
        facturaGeneral.setPrefactura( rs.getString("prefactura") );

        facturaGeneral.setVlr_mat(rs.getDouble("vlr_mat"));
        facturaGeneral.setVlr_mob(rs.getDouble("vlr_mob"));
        facturaGeneral.setVlr_otr(rs.getDouble("vlr_otr"));

        facturaGeneral.setVlr_administracion(rs.getDouble("vlr_administracion"));
        facturaGeneral.setVlr_imprevisto(rs.getDouble("vlr_imprevisto"));
        facturaGeneral.setVlr_utilidad(rs.getDouble("vlr_utilidad"));

        facturaGeneral.setVlr_Bonificacion(rs.getDouble("vlr_bonificacion"));
        facturaGeneral.setVlr_iva(rs.getDouble("vlr_iva"));
        facturaGeneral.setVlr_rmat(rs.getDouble("vlr_rmat"));
        facturaGeneral.setVlr_rmob(rs.getDouble("vlr_rmob"));
        facturaGeneral.setVlr_rotr(rs.getDouble("vlr_rotr"));
        facturaGeneral.setVlr_factoring(rs.getDouble("vlr_factoring"));
        facturaGeneral.setVlr_formula(rs.getDouble("vlr_formula"));

        facturaGeneral.setVlr_base_iva(rs.getDouble("vlr_base_iva"));

        facturaGeneral.setVlr_formula_provintegral(rs.getDouble("vlr_formula_provintegral"));

        facturaGeneral.setId_accion( rs.getString("id_accion") );

        facturaGeneral.setVal_ret_ica_contratista(rs.getDouble("val_ret_ica_contratista"));
        facturaGeneral.setVal_ret_iva_contratista(rs.getDouble("val_ret_iva_contratista"));
        facturaGeneral.setVal_ret_aiu_contratista(rs.getDouble("val_ret_aiu_contratista"));

        facturaGeneral.setCuenta_iva_contratista(rs.getString("cuenta_iva_contratista"));
        facturaGeneral.setCuenta_ret_material_contratista(rs.getString("cuenta_ret_material_contratista"));
        facturaGeneral.setCuenta_ret_mano_obra_contratista(rs.getString("cuenta_ret_mano_obra_contratista"));
        facturaGeneral.setCuenta_ret_otros_contratista(rs.getString("cuenta_ret_otros_contratista"));
        facturaGeneral.setCuenta_ret_aiu_contratista(rs.getString("cuenta_ret_aiu_contratista"));
        facturaGeneral.setCuenta_ret_ica_contratista(rs.getString("cuenta_ret_ica_contratista"));
        facturaGeneral.setCuenta_ret_iva_contratista(rs.getString("cuenta_ret_iva_contratista"));
        facturaGeneral.setTipo_distribucion(rs.getString("tipodistribucion"));
        facturaGeneral.setNum_os(rs.getString("num_os"));


        return facturaGeneral;
    }




    /**
     * @return the id_contratista
     */
    public String getId_contratista() {
        return id_contratista;
    }

    /**
     * @param id_contratista the id_contratista to set
     */
    public void setId_contratista(String id_contratista) {
        this.id_contratista = id_contratista;
    }

    /**
     * @return the prefactura
     */
    public String getPrefactura() {
        return prefactura;
    }

    /**
     * @param prefactura the prefactura to set
     */
    public void setPrefactura(String prefactura) {
        this.prefactura = prefactura;
    }

    /**
     * @return the vlr_mat
     */
    public double getVlr_mat() {
        return vlr_mat;
    }

    /**
     * @param vlr_mat the vlr_mat to set
     */
    public void setVlr_mat(double vlr_mat) {
        this.vlr_mat = vlr_mat;
    }

    /**
     * @return the vlr_mob
     */
    public double getVlr_mob() {
        return vlr_mob;
    }

    /**
     * @param vlr_mob the vlr_mob to set
     */
    public void setVlr_mob(double vlr_mob) {
        this.vlr_mob = vlr_mob;
    }

    /**
     * @return the vlr_otr
     */
    public double getVlr_otr() {
        return vlr_otr;
    }

    /**
     * @param vlr_otr the vlr_otr to set
     */
    public void setVlr_otr(double vlr_otr) {
        this.vlr_otr = vlr_otr;
    }

    /**
     * @return the vlr_administracion
     */
    public double getVlr_administracion() {
        return vlr_administracion;
    }

    /**
     * @param vlr_administracion the vlr_administracion to set
     */
    public void setVlr_administracion(double vlr_administracion) {
        this.vlr_administracion = vlr_administracion;
    }

    /**
     * @return the vlr_imprevisto
     */
    public double getVlr_imprevisto() {
        return vlr_imprevisto;
    }

    /**
     * @param vlr_imprevisto the vlr_imprevisto to set
     */
    public void setVlr_imprevisto(double vlr_imprevisto) {
        this.vlr_imprevisto = vlr_imprevisto;
    }

    /**
     * @return the vlr_utilidad
     */
    public double getVlr_utilidad() {
        return vlr_utilidad;
    }

    /**
     * @param vlr_utilidad the vlr_utilidad to set
     */
    public void setVlr_utilidad(double vlr_utilidad) {
        this.vlr_utilidad = vlr_utilidad;
    }

    /**
     * @return the bonificacion
     */
    public double getVlr_Bonificacion() {
        return vlr_bonificacion;
    }

    /**
     * @param bonificacion the bonificacion to set
     */
    public void setVlr_Bonificacion(double bonificacion) {
        this.vlr_bonificacion = bonificacion;
    }

    /**
     * @return the vlr_iva
     */
    public double getVlr_iva() {
        return vlr_iva;
    }

    /**
     * @param vlr_iva the vlr_iva to set
     */
    public void setVlr_iva(double vlr_iva) {
        this.vlr_iva = vlr_iva;
    }

    /**
     * @return the vlr_rmat
     */
    public double getVlr_rmat() {
        return vlr_rmat;
    }

    /**
     * @param vlr_rmat the vlr_rmat to set
     */
    public void setVlr_rmat(double vlr_rmat) {
        this.vlr_rmat = vlr_rmat;
    }

    /**
     * @return the vlr_rmob
     */
    public double getVlr_rmob() {
        return vlr_rmob;
    }

    /**
     * @param vlr_rmob the vlr_rmob to set
     */
    public void setVlr_rmob(double vlr_rmob) {
        this.vlr_rmob = vlr_rmob;
    }

    /**
     * @return the vlr_rotr
     */
    public double getVlr_rotr() {
        return vlr_rotr;
    }

    /**
     * @param vlr_rotr the vlr_rotr to set
     */
    public void setVlr_rotr(double vlr_rotr) {
        this.vlr_rotr = vlr_rotr;
    }

    /**
     * @return the vlr_factoring
     */
    public double getVlr_factoring() {
        return vlr_factoring;
    }

    /**
     * @param vlr_factoring the vlr_factoring to set
     */
    public void setVlr_factoring(double vlr_factoring) {
        this.vlr_factoring = vlr_factoring;
    }

    /**
     * @return the vlr_formula
     */
    public double getVlr_formula() {
        return vlr_formula;
    }

    /**
     * @param vlr_formula the vlr_formula to set
     */
    public void setVlr_formula(double vlr_formula) {
        this.vlr_formula = vlr_formula;
    }

    /**
     * @return the vlr_base_iva
     */
    public double getVlr_base_iva() {
        return vlr_base_iva;
    }

    /**
     * @param vlr_base_iva the vlr_base_iva to set
     */
    public void setVlr_base_iva(double vlr_base_iva) {
        this.vlr_base_iva = vlr_base_iva;
    }


    public double getVlr_formula_provintegral() {
        return vlr_formula_provintegral;
    }
    public void setVlr_formula_provintegral(double x) {
        this.vlr_formula_provintegral = x;
    }

    /**
     * @return the id_accion
     */
    public String getId_accion() {
        return id_accion;
    }

    /**
     * @param id_accion the id_accion to set
     */
    public void setId_accion(String id_accion) {
        this.id_accion = id_accion;
    }

    /**
     * @return the val_ret_ica_contratista
     */
    public double getVal_ret_ica_contratista() {
        return val_ret_ica_contratista;
    }

    /**
     * @param val_ret_ica_contratista the val_ret_ica_contratista to set
     */
    public void setVal_ret_ica_contratista(double val_ret_ica_contratista) {
        this.val_ret_ica_contratista = val_ret_ica_contratista;
    }

    /**
     * @return the val_ret_iva_contratista
     */
    public double getVal_ret_iva_contratista() {
        return val_ret_iva_contratista;
    }

    /**
     * @param val_ret_iva_contratista the val_ret_iva_contratista to set
     */
    public void setVal_ret_iva_contratista(double val_ret_iva_contratista) {
        this.val_ret_iva_contratista = val_ret_iva_contratista;
    }

    /**
     * @return the val_ret_aiu_contratista
     */
    public double getVal_ret_aiu_contratista() {
        return val_ret_aiu_contratista;
    }

    /**
     * @param val_ret_aiu_contratista the val_ret_aiu_contratista to set
     */
    public void setVal_ret_aiu_contratista(double val_ret_aiu_contratista) {
        this.val_ret_aiu_contratista = val_ret_aiu_contratista;
    }

    /**
     * @return the cuenta_iva_contratista
     */
    public String getCuenta_iva_contratista() {
        return cuenta_iva_contratista;
    }

    /**
     * @param cuenta_iva_contratista the cuenta_iva_contratista to set
     */
    public void setCuenta_iva_contratista(String cuenta_iva_contratista) {
        this.cuenta_iva_contratista = cuenta_iva_contratista;
    }

    /**
     * @return the cuenta_ret_material_contratista
     */
    public String getCuenta_ret_material_contratista() {
        return cuenta_ret_material_contratista;
    }

    /**
     * @param cuenta_ret_material_contratista the cuenta_ret_material_contratista to set
     */
    public void setCuenta_ret_material_contratista(String cuenta_ret_material_contratista) {
        this.cuenta_ret_material_contratista = cuenta_ret_material_contratista;
    }

    /**
     * @return the cuenta_ret_mano_obra_contratista
     */
    public String getCuenta_ret_mano_obra_contratista() {
        return cuenta_ret_mano_obra_contratista;
    }

    /**
     * @param cuenta_ret_mano_obra_contratista the cuenta_ret_mano_obra_contratista to set
     */
    public void setCuenta_ret_mano_obra_contratista(String cuenta_ret_mano_obra_contratista) {
        this.cuenta_ret_mano_obra_contratista = cuenta_ret_mano_obra_contratista;
    }

    /**
     * @return the cuenta_ret_otros_contratista
     */
    public String getCuenta_ret_otros_contratista() {
        return cuenta_ret_otros_contratista;
    }

    /**
     * @param cuenta_ret_otros_contratista the cuenta_ret_otros_contratista to set
     */
    public void setCuenta_ret_otros_contratista(String cuenta_ret_otros_contratista) {
        this.cuenta_ret_otros_contratista = cuenta_ret_otros_contratista;
    }

    /**
     * @return the cuenta_ret_aiu_contratista
     */
    public String getCuenta_ret_aiu_contratista() {
        return cuenta_ret_aiu_contratista;
    }

    /**
     * @param cuenta_ret_aiu_contratista the cuenta_ret_aiu_contratista to set
     */
    public void setCuenta_ret_aiu_contratista(String cuenta_ret_aiu_contratista) {
        this.cuenta_ret_aiu_contratista = cuenta_ret_aiu_contratista;
    }

    /**
     * @return the cuenta_ret_ica_contratista
     */
    public String getCuenta_ret_ica_contratista() {
        return cuenta_ret_ica_contratista;
    }

    /**
     * @param cuenta_ret_ica_contratista the cuenta_ret_ica_contratista to set
     */
    public void setCuenta_ret_ica_contratista(String cuenta_ret_ica_contratista) {
        this.cuenta_ret_ica_contratista = cuenta_ret_ica_contratista;
    }

    /**
     * @return the cuenta_ret_iva_contratista
     */
    public String getCuenta_ret_iva_contratista() {
        return cuenta_ret_iva_contratista;
    }

    /**
     * @param cuenta_ret_iva_contratista the cuenta_ret_iva_contratista to set
     */
    public void setCuenta_ret_iva_contratista(String cuenta_ret_iva_contratista) {
        this.cuenta_ret_iva_contratista = cuenta_ret_iva_contratista;
    }

    /**
     * @return the tipo_distribucion
     */
    public String getTipo_distribucion() {
        return tipo_distribucion;
    }

    /**
     * @param tipo_distribucion the tipo_distribucion to set
     */
    public void setTipo_distribucion(String tipo_distribucion) {
        this.tipo_distribucion = tipo_distribucion;
    }

    /**
     * @return the num_os
     */
    public String getNum_os() {
        return num_os;
    }

    /**
     * @param num_os the num_os to set
     */
    public void setNum_os(String num_os) {
        this.num_os = num_os;
    }

}