/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.opav.model.beans;

/**
 *
 * @author Alvaro
 */
public class FacturaClienteCuota {

    private String id_cliente;
    private String factura;
    private int    cuota;
    private double valor_factura;
    private int    item;
    private String nit;
    private String fecha_financiacion;
    private String simbolo_variable;
    private String num_os;
    private String id_solicitud;
    private int    parcial;
    private double valor_intereses;
    private int    item_interes;
    private int    item_opav;
    private double valor_opav;






    public FacturaClienteCuota() {

        inicializar();

    }


    public void inicializar() {

        id_cliente = "";
        factura = "";
        cuota = 0;
        valor_factura = 0.00;
        item = 0;
        nit = "";
        fecha_financiacion ="";
        simbolo_variable = "";
        num_os = "";
        id_solicitud = "";
        parcial = 0;
        valor_intereses = 0.00;
        item_interes = 0;
        item_opav = 0;
        valor_opav = 0.00;

    }






    /**
     * @return the id_cliente
     */
    public String getId_cliente() {
        return id_cliente;
    }

    /**
     * @param id_cliente the id_cliente to set
     */
    public void setId_cliente(String id_cliente) {
        this.id_cliente = id_cliente;
    }

    /**
     * @return the factura
     */
    public String getFactura() {
        return factura;
    }

    /**
     * @param factura the factura to set
     */
    public void setFactura(String factura) {
        this.factura = factura;
    }

    /**
     * @return the cuota
     */
    public int getCuota() {
        return cuota;
    }

    /**
     * @param cuota the cuota to set
     */
    public void setCuota(int cuota) {
        this.cuota = cuota;
    }

    /**
     * @return the valor_factura
     */
    public double getValor_factura() {
        return valor_factura;
    }

    /**
     * @param valor_factura the valor_factura to set
     */
    public void setValor_factura(double valor_factura) {
        this.valor_factura = valor_factura;
    }

    /**
     * @return the item
     */
    public int getItem() {
        return item;
    }

    /**
     * @param item the item to set
     */
    public void setItem(int item) {
        this.item = item;
    }

    /**
     * @return the nit
     */
    public String getNit() {
        return nit;
    }

    /**
     * @param nit the nit to set
     */
    public void setNit(String nit) {
        this.nit = nit;
    }

    /**
     * @return the fecha_financiacion
     */
    public String getFecha_financiacion() {
        return fecha_financiacion;
    }

    /**
     * @param fecha_financiacion the fecha_financiacion to set
     */
    public void setFecha_financiacion(String fecha_financiacion) {
        this.fecha_financiacion = fecha_financiacion;
    }

    /**
     * @return the simbolo_variable
     */
    public String getSimbolo_variable() {
        return simbolo_variable;
    }

    /**
     * @param simbolo_variable the simbolo_variable to set
     */
    public void setSimbolo_variable(String simbolo_variable) {
        this.simbolo_variable = simbolo_variable;
    }

    /**
     * @return the num_os
     */
    public String getNum_os() {
        return num_os;
    }

    /**
     * @param num_os the num_os to set
     */
    public void setNum_os(String num_os) {
        this.num_os = num_os;
    }

    /**
     * @return the id_solicitud
     */
    public String getId_solicitud() {
        return id_solicitud;
    }

    /**
     * @param id_solicitud the id_solicitud to set
     */
    public void setId_solicitud(String id_solicitud) {
        this.id_solicitud = id_solicitud;
    }

    /**
     * @return the parcial
     */
    public int getParcial() {
        return parcial;
    }


    /**
     * @param parcial the parcial to set
     */
    public void setParcial(int parcial) {
        this.parcial = parcial;
    }


    /**
     * @return the valor_intereses
     */
    public double getValor_intereses() {
        return valor_intereses;
    }

    /**
     * @param valor_intereses the valor_intereses to set
     */
    public void setValor_intereses(double valor_intereses) {
        this.valor_intereses = valor_intereses;
    }

    /**
     * @return the item_interes
     */
    public int getItem_interes() {
        return item_interes;
    }

    /**
     * @param item_interes the item_interes to set
     */
    public void setItem_interes(int item_interes) {
        this.item_interes = item_interes;
    }

    /**
     * @return the item_opav
     */
    public int getItem_opav() {
        return item_opav;
    }

    /**
     * @param item_opav the item_opav to set
     */
    public void setItem_opav(int item_opav) {
        this.item_opav = item_opav;
    }

    /**
     * @return the valor_opav
     */
    public double getValor_opav() {
        return valor_opav;
    }

    /**
     * @param valor_opav the valor_opav to set
     */
    public void setValor_opav(double valor_opav) {
        this.valor_opav = valor_opav;
    }



}