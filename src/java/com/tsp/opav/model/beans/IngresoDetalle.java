/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.opav.model.beans;

/**
 *
 * @author Alvaro
 */
public class IngresoDetalle {

  private String  reg_status ;
  private String  dstrct ;
  private String  tipo_documento;
  private String  num_ingreso ;
  private int     item ;
  private String  nitcli;
  private double  valor_ingreso;
  private double  valor_ingreso_me;
  private String  factura;
  private String  fecha_factura;
  private String  codigo_retefuente ;
  private double  valor_retefuente ;
  private double  valor_retefuente_me ;
  private String  tipo_doc;
  private String  documento;
  private String  codigo_reteica;
  private double  valor_reteica ;
  private double  valor_reteica_me ;
  private double  valor_diferencia_tasa;
  private String  creation_user;
  private String  creation_date;
  private String  user_update;
  private String  last_update;
  private String  base;
  private String  cuenta;
  private String  auxiliar;
  private String  fecha_contabilizacion ;
  private String  fecha_anulacion_contabilizacion ;
  private String  periodo;
  private String  fecha_anulacion;
  private String  periodo_anulacion;
  private int     transaccion;
  private int     transaccion_anulacion;
  private String  descripcion;
  private double  valor_tasa ;
  private double  saldo_factura;
  private String  procesado;
  private int     id;
  private String  ref1;
  private String  tipo_referencia_1;
  private String  referencia_1;
  private String  tipo_referencia_2;
  private String  referencia_2;
  private String  tipo_referencia_3;
  private String  referencia_3;

  // Campos adicionales pertenecientes a la cabecera del ingreso o de la nota credito

  private String  codcli;
  private String  cmc;





    /** Creates a new instance of AccionPorSolicitud */
    public IngresoDetalle() {
    }


    /** Extrae un registro de la BD correspondientes a las acciones de una solicitudes que no esta facturada */
    public static IngresoDetalle load(java.sql.ResultSet rs)throws java.sql.SQLException{


        IngresoDetalle ingresoDetalle = new IngresoDetalle();

        ingresoDetalle.setReg_status(rs.getString("reg_status") ) ;
        ingresoDetalle.setDstrct(rs.getString("dstrct") );
        ingresoDetalle.setTipo_documento(rs.getString("tipo_documento") );;
        ingresoDetalle.setNum_ingreso(rs.getString("num_ingreso") );
        ingresoDetalle.setItem(rs.getInt("item") );
        ingresoDetalle.setNitcli(rs.getString("nitcli") );
        ingresoDetalle.setValor_ingreso(rs.getDouble("valor_ingreso") );
        ingresoDetalle.setValor_ingreso_me(rs.getDouble("valor_ingreso_me") );
        ingresoDetalle.setFactura(rs.getString("factura") );
        ingresoDetalle.setFecha_factura(rs.getString("fecha_factura") );
        ingresoDetalle.setCodigo_retefuente(rs.getString("codigo_retefuente") );
        ingresoDetalle.setValor_retefuente(rs.getDouble("valor_retefuente") );
        ingresoDetalle.setValor_retefuente_me(rs.getDouble("valor_retefuente_me") );
        ingresoDetalle.setTipo_doc(rs.getString("tipo_doc") );
        ingresoDetalle.setDocumento(rs.getString("documento") );
        ingresoDetalle.setCodigo_reteica(rs.getString("codigo_reteica") );
        ingresoDetalle.setValor_reteica(rs.getDouble("valor_reteica") );
        ingresoDetalle.setValor_reteica_me(rs.getDouble("valor_reteica_me") );
        ingresoDetalle.setValor_diferencia_tasa(rs.getDouble("valor_diferencia_tasa") );
        ingresoDetalle.setCreation_user(rs.getString("creation_user") );
        ingresoDetalle.setCreation_date(rs.getString("creation_date") );
        ingresoDetalle.setUser_update(rs.getString("user_update") );
        ingresoDetalle.setLast_update(rs.getString("last_update") );
        ingresoDetalle.setBase(rs.getString("base") );
        ingresoDetalle.setCuenta(rs.getString("cuenta") );
        ingresoDetalle.setAuxiliar(rs.getString("auxiliar") );
        ingresoDetalle.setFecha_contabilizacion(rs.getString("fecha_contabilizacion") );
        ingresoDetalle.setFecha_anulacion_contabilizacion(rs.getString("fecha_anulacion_contabilizacion") );
        ingresoDetalle.setPeriodo(rs.getString("periodo") );
        ingresoDetalle.setFecha_anulacion(rs.getString("fecha_anulacion") );
        ingresoDetalle.setPeriodo_anulacion(rs.getString("periodo_anulacion") );
        ingresoDetalle.setTransaccion(rs.getInt("transaccion") );
        ingresoDetalle.setTransaccion_anulacion(rs.getInt("transaccion_anulacion") );
        ingresoDetalle.setDescripcion(rs.getString("descripcion") );
        ingresoDetalle.setValor_tasa(rs.getDouble("valor_tasa") );
        ingresoDetalle.setSaldo_factura(rs.getDouble("saldo_factura") );
        ingresoDetalle.setProcesado(rs.getString("procesado") );
        ingresoDetalle.setId(rs.getInt("id") );
        ingresoDetalle.setRef1(rs.getString("ref1") );
        ingresoDetalle.setTipo_referencia_1(rs.getString("tipo_referencia_1") );
        ingresoDetalle.setReferencia_1(rs.getString("referencia_1") );
        ingresoDetalle.setTipo_referencia_2(rs.getString("tipo_referencia_2") );
        ingresoDetalle.setReferencia_2(rs.getString("referencia_2") );
        ingresoDetalle.setTipo_referencia_3(rs.getString("tipo_referencia_3") );
        ingresoDetalle.setReferencia_3(rs.getString("referencia_3") );


        // Campos para cabecera de la factura


        ingresoDetalle.setCodcli((rs.getString("codcli")));
        ingresoDetalle.setCmc(rs.getString("cmc") );


        return ingresoDetalle;

    }





    /**
     * @return the reg_status
     */
    public String getReg_status() {
        return reg_status;
    }

    /**
     * @param reg_status the reg_status to set
     */
    public void setReg_status(String reg_status) {
        this.reg_status = reg_status;
    }

    /**
     * @return the dstrct
     */
    public String getDstrct() {
        return dstrct;
    }

    /**
     * @param dstrct the dstrct to set
     */
    public void setDstrct(String dstrct) {
        this.dstrct = dstrct;
    }

    /**
     * @return the tipo_documento
     */
    public String getTipo_documento() {
        return tipo_documento;
    }

    /**
     * @param tipo_documento the tipo_documento to set
     */
    public void setTipo_documento(String tipo_documento) {
        this.tipo_documento = tipo_documento;
    }

    /**
     * @return the num_ingreso
     */
    public String getNum_ingreso() {
        return num_ingreso;
    }

    /**
     * @param num_ingreso the num_ingreso to set
     */
    public void setNum_ingreso(String num_ingreso) {
        this.num_ingreso = num_ingreso;
    }

    /**
     * @return the item
     */
    public int getItem() {
        return item;
    }

    /**
     * @param item the item to set
     */
    public void setItem(int item) {
        this.item = item;
    }

    /**
     * @return the nitcli
     */
    public String getNitcli() {
        return nitcli;
    }

    /**
     * @param nitcli the nitcli to set
     */
    public void setNitcli(String nitcli) {
        this.nitcli = nitcli;
    }

    /**
     * @return the valor_ingreso
     */
    public double getValor_ingreso() {
        return valor_ingreso;
    }

    /**
     * @param valor_ingreso the valor_ingreso to set
     */
    public void setValor_ingreso(double valor_ingreso) {
        this.valor_ingreso = valor_ingreso;
    }

    /**
     * @return the valor_ingreso_me
     */
    public double getValor_ingreso_me() {
        return valor_ingreso_me;
    }

    /**
     * @param valor_ingreso_me the valor_ingreso_me to set
     */
    public void setValor_ingreso_me(double valor_ingreso_me) {
        this.valor_ingreso_me = valor_ingreso_me;
    }

    /**
     * @return the factura
     */
    public String getFactura() {
        return factura;
    }

    /**
     * @param factura the factura to set
     */
    public void setFactura(String factura) {
        this.factura = factura;
    }

    /**
     * @return the fecha_factura
     */
    public String getFecha_factura() {
        return fecha_factura;
    }

    /**
     * @param fecha_factura the fecha_factura to set
     */
    public void setFecha_factura(String fecha_factura) {
        this.fecha_factura = fecha_factura;
    }

    /**
     * @return the codigo_retefuente
     */
    public String getCodigo_retefuente() {
        return codigo_retefuente;
    }

    /**
     * @param codigo_retefuente the codigo_retefuente to set
     */
    public void setCodigo_retefuente(String codigo_retefuente) {
        this.codigo_retefuente = codigo_retefuente;
    }

    /**
     * @return the valor_retefuente
     */
    public double getValor_retefuente() {
        return valor_retefuente;
    }

    /**
     * @param valor_retefuente the valor_retefuente to set
     */
    public void setValor_retefuente(double valor_retefuente) {
        this.valor_retefuente = valor_retefuente;
    }

    /**
     * @return the valor_retefuente_me
     */
    public double getValor_retefuente_me() {
        return valor_retefuente_me;
    }

    /**
     * @param valor_retefuente_me the valor_retefuente_me to set
     */
    public void setValor_retefuente_me(double valor_retefuente_me) {
        this.valor_retefuente_me = valor_retefuente_me;
    }

    /**
     * @return the tipo_doc
     */
    public String getTipo_doc() {
        return tipo_doc;
    }

    /**
     * @param tipo_doc the tipo_doc to set
     */
    public void setTipo_doc(String tipo_doc) {
        this.tipo_doc = tipo_doc;
    }

    /**
     * @return the documento
     */
    public String getDocumento() {
        return documento;
    }

    /**
     * @param documento the documento to set
     */
    public void setDocumento(String documento) {
        this.documento = documento;
    }

    /**
     * @return the codigo_reteica
     */
    public String getCodigo_reteica() {
        return codigo_reteica;
    }

    /**
     * @param codigo_reteica the codigo_reteica to set
     */
    public void setCodigo_reteica(String codigo_reteica) {
        this.codigo_reteica = codigo_reteica;
    }

    /**
     * @return the valor_reteica
     */
    public double getValor_reteica() {
        return valor_reteica;
    }

    /**
     * @param valor_reteica the valor_reteica to set
     */
    public void setValor_reteica(double valor_reteica) {
        this.valor_reteica = valor_reteica;
    }

    /**
     * @return the valor_reteica_me
     */
    public double getValor_reteica_me() {
        return valor_reteica_me;
    }

    /**
     * @param valor_reteica_me the valor_reteica_me to set
     */
    public void setValor_reteica_me(double valor_reteica_me) {
        this.valor_reteica_me = valor_reteica_me;
    }

    /**
     * @return the valor_diferencia_tasa
     */
    public double getValor_diferencia_tasa() {
        return valor_diferencia_tasa;
    }

    /**
     * @param valor_diferencia_tasa the valor_diferencia_tasa to set
     */
    public void setValor_diferencia_tasa(double valor_diferencia_tasa) {
        this.valor_diferencia_tasa = valor_diferencia_tasa;
    }

    /**
     * @return the creation_user
     */
    public String getCreation_user() {
        return creation_user;
    }

    /**
     * @param creation_user the creation_user to set
     */
    public void setCreation_user(String creation_user) {
        this.creation_user = creation_user;
    }

    /**
     * @return the creation_date
     */
    public String getCreation_date() {
        return creation_date;
    }

    /**
     * @param creation_date the creation_date to set
     */
    public void setCreation_date(String creation_date) {
        this.creation_date = creation_date;
    }

    /**
     * @return the user_update
     */
    public String getUser_update() {
        return user_update;
    }

    /**
     * @param user_update the user_update to set
     */
    public void setUser_update(String user_update) {
        this.user_update = user_update;
    }

    /**
     * @return the last_update
     */
    public String getLast_update() {
        return last_update;
    }

    /**
     * @param last_update the last_update to set
     */
    public void setLast_update(String last_update) {
        this.last_update = last_update;
    }

    /**
     * @return the base
     */
    public String getBase() {
        return base;
    }

    /**
     * @param base the base to set
     */
    public void setBase(String base) {
        this.base = base;
    }

    /**
     * @return the cuenta
     */
    public String getCuenta() {
        return cuenta;
    }

    /**
     * @param cuenta the cuenta to set
     */
    public void setCuenta(String cuenta) {
        this.cuenta = cuenta;
    }

    /**
     * @return the auxiliar
     */
    public String getAuxiliar() {
        return auxiliar;
    }

    /**
     * @param auxiliar the auxiliar to set
     */
    public void setAuxiliar(String auxiliar) {
        this.auxiliar = auxiliar;
    }

    /**
     * @return the fecha_contabilizacion
     */
    public String getFecha_contabilizacion() {
        return fecha_contabilizacion;
    }

    /**
     * @param fecha_contabilizacion the fecha_contabilizacion to set
     */
    public void setFecha_contabilizacion(String fecha_contabilizacion) {
        this.fecha_contabilizacion = fecha_contabilizacion;
    }

    /**
     * @return the fecha_anulacion_contabilizacion
     */
    public String getFecha_anulacion_contabilizacion() {
        return fecha_anulacion_contabilizacion;
    }

    /**
     * @param fecha_anulacion_contabilizacion the fecha_anulacion_contabilizacion to set
     */
    public void setFecha_anulacion_contabilizacion(String fecha_anulacion_contabilizacion) {
        this.fecha_anulacion_contabilizacion = fecha_anulacion_contabilizacion;
    }

    /**
     * @return the periodo
     */
    public String getPeriodo() {
        return periodo;
    }

    /**
     * @param periodo the periodo to set
     */
    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }

    /**
     * @return the fecha_anulacion
     */
    public String getFecha_anulacion() {
        return fecha_anulacion;
    }

    /**
     * @param fecha_anulacion the fecha_anulacion to set
     */
    public void setFecha_anulacion(String fecha_anulacion) {
        this.fecha_anulacion = fecha_anulacion;
    }

    /**
     * @return the periodo_anulacion
     */
    public String getPeriodo_anulacion() {
        return periodo_anulacion;
    }

    /**
     * @param periodo_anulacion the periodo_anulacion to set
     */
    public void setPeriodo_anulacion(String periodo_anulacion) {
        this.periodo_anulacion = periodo_anulacion;
    }

    /**
     * @return the transaccion
     */
    public int getTransaccion() {
        return transaccion;
    }

    /**
     * @param transaccion the transaccion to set
     */
    public void setTransaccion(int transaccion) {
        this.transaccion = transaccion;
    }

    /**
     * @return the transaccion_anulacion
     */
    public int getTransaccion_anulacion() {
        return transaccion_anulacion;
    }

    /**
     * @param transaccion_anulacion the transaccion_anulacion to set
     */
    public void setTransaccion_anulacion(int transaccion_anulacion) {
        this.transaccion_anulacion = transaccion_anulacion;
    }

    /**
     * @return the descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * @param descripcion the descripcion to set
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * @return the valor_tasa
     */
    public double getValor_tasa() {
        return valor_tasa;
    }

    /**
     * @param valor_tasa the valor_tasa to set
     */
    public void setValor_tasa(double valor_tasa) {
        this.valor_tasa = valor_tasa;
    }

    /**
     * @return the saldo_factura
     */
    public double getSaldo_factura() {
        return saldo_factura;
    }

    /**
     * @param saldo_factura the saldo_factura to set
     */
    public void setSaldo_factura(double saldo_factura) {
        this.saldo_factura = saldo_factura;
    }

    /**
     * @return the procesado
     */
    public String getProcesado() {
        return procesado;
    }

    /**
     * @param procesado the procesado to set
     */
    public void setProcesado(String procesado) {
        this.procesado = procesado;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the ref1
     */
    public String getRef1() {
        return ref1;
    }

    /**
     * @param ref1 the ref1 to set
     */
    public void setRef1(String ref1) {
        this.ref1 = ref1;
    }

    /**
     * @return the tipo_referencia_1
     */
    public String getTipo_referencia_1() {
        return tipo_referencia_1;
    }

    /**
     * @param tipo_referencia_1 the tipo_referencia_1 to set
     */
    public void setTipo_referencia_1(String tipo_referencia_1) {
        this.tipo_referencia_1 = tipo_referencia_1;
    }

    /**
     * @return the referencia_1
     */
    public String getReferencia_1() {
        return referencia_1;
    }

    /**
     * @param referencia_1 the referencia_1 to set
     */
    public void setReferencia_1(String referencia_1) {
        this.referencia_1 = referencia_1;
    }

    /**
     * @return the tipo_referencia_2
     */
    public String getTipo_referencia_2() {
        return tipo_referencia_2;
    }

    /**
     * @param tipo_referencia_2 the tipo_referencia_2 to set
     */
    public void setTipo_referencia_2(String tipo_referencia_2) {
        this.tipo_referencia_2 = tipo_referencia_2;
    }

    /**
     * @return the referencia_2
     */
    public String getReferencia_2() {
        return referencia_2;
    }

    /**
     * @param referencia_2 the referencia_2 to set
     */
    public void setReferencia_2(String referencia_2) {
        this.referencia_2 = referencia_2;
    }

    /**
     * @return the tipo_referencia_3
     */
    public String getTipo_referencia_3() {
        return tipo_referencia_3;
    }

    /**
     * @param tipo_referencia_3 the tipo_referencia_3 to set
     */
    public void setTipo_referencia_3(String tipo_referencia_3) {
        this.tipo_referencia_3 = tipo_referencia_3;
    }

    /**
     * @return the referencia_3
     */
    public String getReferencia_3() {
        return referencia_3;
    }

    /**
     * @param referencia_3 the referencia_3 to set
     */
    public void setReferencia_3(String referencia_3) {
        this.referencia_3 = referencia_3;
    }

    /**
     * @return the codcli
     */
    public String getCodcli() {
        return codcli;
    }

    /**
     * @param codcli the codcli to set
     */
    public void setCodcli(String codcli) {
        this.codcli = codcli;
    }

    /**
     * @return the cmc
     */
    public String getCmc() {
        return cmc;
    }

    /**
     * @param cmc the cmc to set
     */
    public void setCmc(String cmc) {
        this.cmc = cmc;
    }

}