/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Ejecutivo.java: clase contenedora para los registros salidos de la tabla ejecutivos
 * Copyright 2010 Rhonalf Martinez Villa <rhonaldomaster@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.tsp.opav.model.beans;

/**
 *
 * @author rhonalf
 */
public class Ejecutivo {

    private String id;
    private String nombre;
    private String nit;
    private String dpto;
    private String ciudad;
    private String direccion;
    private String telefono;
    private String email;
    private String cargo;
    private String mercado;

    public Ejecutivo(){
        this.cargo="";
        this.ciudad="";
        this.direccion="";
        this.dpto="";
        this.email="";
        this.id="";
        this.mercado="";
        this.nit="";
        this.nombre="";
        this.telefono="";
    }

    /**
     * Coloca el valor del atributo id
     * @param i el id a asignar
     */
    public void setId(String i){
        this.id = i;
    }

    /**
     * Devuelve el valor del atributo id
     * @return String con el valor del atributo
     */
    public String getId(){
        return this.id;
    }

    /**
     * Coloca el valor del atributo nombre
     * @param nm el nombre a asignar
     */
    public void setNombre(String nm){
        this.nombre=nm;
    }

    /**
     * Devuelve el valor del atributo nombre
     * @return String con el valor del atributo
     */
    public String getNombre(){
        return this.nombre;
    }

    /**
     * Coloca el valor del atributo nit
     * @param nt el nit a asignar
     */
    public void setNit(String nt){
        this.nit = nt;
    }

    /**
     * Devuelve el valor del atributo nit
     * @return String con el valor del atributo
     */
    public String getNit(){
        return this.nit;
    }

    /**
     * Coloca el valor del atributo email
     * @param em el email a asignar
     */
    public void setEmail(String em){
        this.email=em;
    }

    /**
     * Devuelve el valor del atributo email
     * @return String con el valor del atributo
     */
    public String getEmail(){
        return this.email;
    }

    /**
     * Coloca el valor del atributo cargo
     * @param c el cargo a asignar
     */
    public void setCargo(String c){
        this.cargo=c;
    }

    /**
     * Devuelve el valor del atributo cargo
     * @return String con el valor del atributo
     */
    public String getCargo(){
        return this.cargo;
    }

    /**
     * Coloca el valor del atributo ciudad
     * @param ciu la ciudad a asignar
     */
    public void setCiudad(String ciu){
        this.ciudad=ciu;
    }

    /**
     * Devuelve el valor del atributo ciudad
     * @return String con el valor del atributo
     */
    public String getCiudad(){
        return this.ciudad;
    }

    /**
     * Coloca el valor del atributo direccion
     * @param dir la direccion a asignar
     */
    public void setDireccion(String dir){
        this.direccion=dir;
    }

    /**
     * Devuelve el valor del atributo direccion
     * @return String con el valor del atributo
     */
    public String getDireccion(){
        return this.direccion;
    }

    /**
     * Coloca el valor del atributo telefono
     * @param tel el telefono a asignar
     */
    public void setTelefono(String tel){
        this.telefono=tel;
    }

    /**
     * Devuelve el valor del atributo telefono
     * @return String con el valor del atributo
     */
    public String getTelefono(){
        return this.telefono;
    }

    /**
     * Coloca el valor del atributo departamento
     * @param dep el departamento a asignar
     */
    public void setDepartamento(String dep){
        this.dpto=dep;
    }

    /**
     * Devuelve el valor del atributo departamento
     * @return String con el valor del atributo
     */
    public String getDepartamento(){
        return this.dpto;
    }

    /**
     * Coloca el valor del atributo mercado
     * @param mer el mercado a asignar
     */
    public void setMercado(String mer){
        this.mercado=mer;
    }

    /**
     * Devuelve el valor del atributo mercado
     * @return String con el valor del atributo
     */
    public String getMercado(){
        return this.mercado;
    }

}