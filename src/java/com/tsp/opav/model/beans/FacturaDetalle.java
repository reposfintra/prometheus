/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.opav.model.beans;

/**
 *
 * @author Alvaro
 */
public class FacturaDetalle {


  private String reg_status ;
  private String dstrct ;
  private String tipo_documento ;
  private String documento ;
  private int    item ;
  private String nit ;
  private String concepto ;
  private String numero_remesa ;
  private String descripcion ;
  private String codigo_cuenta_contable ;
  private double cantidad;
  private double valor_unitario ;
  private double valor_unitariome ;
  private double valor_item ;
  private double valor_itemme ;
  private double valor_tasa ;
  private String moneda ;
  private String last_update ;
  private String user_update ;
  private String creation_date ;
  private String creation_user ;
  private String base ;
  private String auxiliar ;
  private double valor_ingreso ;
  private String tipo_documento_rel ;
  private int    transaccion;
  private String documento_relacionado ;
  private String tipo_referencia_1 ;
  private String referencia_1 ;
  private String tipo_referencia_2 ;
  private String referencia_2 ;
  private String tipo_referencia_3 ;
  private String referencia_3 ;



  public FacturaDetalle () {
      inicializar();

  }


  public void inicializar() {

      reg_status = "" ;
      dstrct = "" ;
      tipo_documento = "" ;
      documento = "" ;
      item = 0 ;
      nit = "" ;
      concepto = "" ;
      numero_remesa = "" ;
      descripcion = "" ;
      codigo_cuenta_contable = "" ;
      cantidad = 0.00 ;
      valor_unitario = 0.00 ;
      valor_unitariome = 0.00 ;
      valor_item = 0.00 ;
      valor_itemme = 0.00 ;
      valor_tasa = 0.00 ;
      moneda = "" ;
      last_update = "0099-01-01 00:00:00" ;
      user_update = "" ;
      creation_date = "0099-01-01 00:00:00" ;
      creation_user = "" ;
      base = "" ;
      auxiliar = "" ;
      valor_ingreso = 0.00 ;
      tipo_documento_rel = "" ;
      transaccion = 0 ;
      documento_relacionado = "" ;
      tipo_referencia_1 = "" ;
      referencia_1 = "" ;
      tipo_referencia_2 = "" ;
      referencia_2 = "" ;
      tipo_referencia_3 = "" ;
      referencia_3 = "" ;

  }




   public static FacturaDetalle load(java.sql.ResultSet rs) throws java.sql.SQLException {


        FacturaDetalle facturaDetalle = new FacturaDetalle();

        facturaDetalle.setReg_status(rs.getString("reg_status") ) ;
        facturaDetalle.setDstrct(rs.getString("dstrct") ) ;
        facturaDetalle.setTipo_documento(rs.getString("tipo_documento") ) ;
        facturaDetalle.setDocumento(rs.getString("documento") ) ;
        facturaDetalle.setItem(rs.getInt("item") ) ;
        facturaDetalle.setNit(rs.getString("nit") ) ;
        facturaDetalle.setConcepto(rs.getString("concepto") ) ;
        facturaDetalle.setNumero_remesa(rs.getString("numero_remesa") ) ;
        facturaDetalle.setDescripcion(rs.getString("descripcion") ) ;
        facturaDetalle.setCodigo_cuenta_contable(rs.getString("codigo_cuenta_contable") ) ;
        facturaDetalle.setCantidad(rs.getDouble("cantidad") ) ;
        facturaDetalle.setValor_unitario(rs.getDouble("valor_unitario") ) ;
        facturaDetalle.setValor_unitariome(rs.getDouble("valor_unitariome") ) ;
        facturaDetalle.setValor_item(rs.getDouble("valor_item") ) ;
        facturaDetalle.setValor_itemme(rs.getDouble("valor_itemme") ) ;
        facturaDetalle.setValor_tasa(rs.getDouble("valor_tasa") ) ;
        facturaDetalle.setMoneda(rs.getString("moneda") ) ;
        facturaDetalle.setLast_update(rs.getString("last_update") ) ;
        facturaDetalle.setUser_update(rs.getString("user_update") ) ;
        facturaDetalle.setCreation_date(rs.getString("creation_date") ) ;
        facturaDetalle.setCreation_user(rs.getString("creation_user") ) ;
        facturaDetalle.setBase(rs.getString("base") ) ;
        facturaDetalle.setAuxiliar(rs.getString("auxiliar") ) ;
        facturaDetalle.setValor_ingreso(rs.getDouble("valor_ingreso") ) ;
        facturaDetalle.setTipo_documento_rel(rs.getString("tipo_documento_rel") ) ;
        facturaDetalle.setTransaccion(rs.getInt("transaccion") ) ;
        facturaDetalle.setDocumento_relacionado(rs.getString("documento_relacionado") ) ;
        facturaDetalle.setTipo_referencia_1(rs.getString("tipo_referencia_1") ) ;
        facturaDetalle.setReferencia_1(rs.getString("referencia_1") ) ;
        facturaDetalle.setTipo_referencia_2(rs.getString("") ) ;
        facturaDetalle.setReferencia_2(rs.getString("tipo_referencia_2") ) ;
        facturaDetalle.setTipo_referencia_3(rs.getString("tipo_referencia_3") ) ;
        facturaDetalle.setReferencia_3(rs.getString("referencia_3") ) ;

        return facturaDetalle;

    }









    /**
     * @return the reg_status
     */
    public String getReg_status() {
        return reg_status;
    }

    /**
     * @param reg_status the reg_status to set
     */
    public void setReg_status(String reg_status) {
        this.reg_status = reg_status;
    }

    /**
     * @return the dstrct
     */
    public String getDstrct() {
        return dstrct;
    }

    /**
     * @param dstrct the dstrct to set
     */
    public void setDstrct(String dstrct) {
        this.dstrct = dstrct;
    }

    /**
     * @return the tipo_documento
     */
    public String getTipo_documento() {
        return tipo_documento;
    }

    /**
     * @param tipo_documento the tipo_documento to set
     */
    public void setTipo_documento(String tipo_documento) {
        this.tipo_documento = tipo_documento;
    }

    /**
     * @return the documento
     */
    public String getDocumento() {
        return documento;
    }

    /**
     * @param documento the documento to set
     */
    public void setDocumento(String documento) {
        this.documento = documento;
    }

    /**
     * @return the item
     */
    public int getItem() {
        return item;
    }

    /**
     * @param item the item to set
     */
    public void setItem(int item) {
        this.item = item;
    }

    /**
     * @return the nit
     */
    public String getNit() {
        return nit;
    }

    /**
     * @param nit the nit to set
     */
    public void setNit(String nit) {
        this.nit = nit;
    }

    /**
     * @return the concepto
     */
    public String getConcepto() {
        return concepto;
    }

    /**
     * @param concepto the concepto to set
     */
    public void setConcepto(String concepto) {
        this.concepto = concepto;
    }

    /**
     * @return the numero_remesa
     */
    public String getNumero_remesa() {
        return numero_remesa;
    }

    /**
     * @param numero_remesa the numero_remesa to set
     */
    public void setNumero_remesa(String numero_remesa) {
        this.numero_remesa = numero_remesa;
    }

    /**
     * @return the descripcion
     */
    public String getDescripcion() {
        return descripcion;
    }

    /**
     * @param descripcion the descripcion to set
     */
    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    /**
     * @return the codigo_cuenta_contable
     */
    public String getCodigo_cuenta_contable() {
        return codigo_cuenta_contable;
    }

    /**
     * @param codigo_cuenta_contable the codigo_cuenta_contable to set
     */
    public void setCodigo_cuenta_contable(String codigo_cuenta_contable) {
        this.codigo_cuenta_contable = codigo_cuenta_contable;
    }

    /**
     * @return the cantidad
     */
    public double getCantidad() {
        return cantidad;
    }

    /**
     * @param cantidad the cantidad to set
     */
    public void setCantidad(double cantidad) {
        this.cantidad = cantidad;
    }

    /**
     * @return the valor_unitario
     */
    public double getValor_unitario() {
        return valor_unitario;
    }

    /**
     * @param valor_unitario the valor_unitario to set
     */
    public void setValor_unitario(double valor_unitario) {
        this.valor_unitario = valor_unitario;
    }

    /**
     * @return the valor_unitariome
     */
    public double getValor_unitariome() {
        return valor_unitariome;
    }

    /**
     * @param valor_unitariome the valor_unitariome to set
     */
    public void setValor_unitariome(double valor_unitariome) {
        this.valor_unitariome = valor_unitariome;
    }

    /**
     * @return the valor_item
     */
    public double getValor_item() {
        return valor_item;
    }

    /**
     * @param valor_item the valor_item to set
     */
    public void setValor_item(double valor_item) {
        this.valor_item = valor_item;
    }

    /**
     * @return the valor_itemme
     */
    public double getValor_itemme() {
        return valor_itemme;
    }

    /**
     * @param valor_itemme the valor_itemme to set
     */
    public void setValor_itemme(double valor_itemme) {
        this.valor_itemme = valor_itemme;
    }

    /**
     * @return the valor_tasa
     */
    public double getValor_tasa() {
        return valor_tasa;
    }

    /**
     * @param valor_tasa the valor_tasa to set
     */
    public void setValor_tasa(double valor_tasa) {
        this.valor_tasa = valor_tasa;
    }

    /**
     * @return the moneda
     */
    public String getMoneda() {
        return moneda;
    }

    /**
     * @param moneda the moneda to set
     */
    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }

    /**
     * @return the last_update
     */
    public String getLast_update() {
        return last_update;
    }

    /**
     * @param last_update the last_update to set
     */
    public void setLast_update(String last_update) {
        this.last_update = last_update;
    }

    /**
     * @return the user_update
     */
    public String getUser_update() {
        return user_update;
    }

    /**
     * @param user_update the user_update to set
     */
    public void setUser_update(String user_update) {
        this.user_update = user_update;
    }

    /**
     * @return the creation_date
     */
    public String getCreation_date() {
        return creation_date;
    }

    /**
     * @param creation_date the creation_date to set
     */
    public void setCreation_date(String creation_date) {
        this.creation_date = creation_date;
    }

    /**
     * @return the creation_user
     */
    public String getCreation_user() {
        return creation_user;
    }

    /**
     * @param creation_user the creation_user to set
     */
    public void setCreation_user(String creation_user) {
        this.creation_user = creation_user;
    }

    /**
     * @return the base
     */
    public String getBase() {
        return base;
    }

    /**
     * @param base the base to set
     */
    public void setBase(String base) {
        this.base = base;
    }

    /**
     * @return the auxiliar
     */
    public String getAuxiliar() {
        return auxiliar;
    }

    /**
     * @param auxiliar the auxiliar to set
     */
    public void setAuxiliar(String auxiliar) {
        this.auxiliar = auxiliar;
    }

    /**
     * @return the valor_ingreso
     */
    public double getValor_ingreso() {
        return valor_ingreso;
    }

    /**
     * @param valor_ingreso the valor_ingreso to set
     */
    public void setValor_ingreso(double valor_ingreso) {
        this.valor_ingreso = valor_ingreso;
    }

    /**
     * @return the tipo_documento_rel
     */
    public String getTipo_documento_rel() {
        return tipo_documento_rel;
    }

    /**
     * @param tipo_documento_rel the tipo_documento_rel to set
     */
    public void setTipo_documento_rel(String tipo_documento_rel) {
        this.tipo_documento_rel = tipo_documento_rel;
    }

    /**
     * @return the transaccion
     */
    public int getTransaccion() {
        return transaccion;
    }

    /**
     * @param transaccion the transaccion to set
     */
    public void setTransaccion(int transaccion) {
        this.transaccion = transaccion;
    }

    /**
     * @return the documento_relacionado
     */
    public String getDocumento_relacionado() {
        return documento_relacionado;
    }

    /**
     * @param documento_relacionado the documento_relacionado to set
     */
    public void setDocumento_relacionado(String documento_relacionado) {
        this.documento_relacionado = documento_relacionado;
    }

    /**
     * @return the tipo_referencia_1
     */
    public String getTipo_referencia_1() {
        return tipo_referencia_1;
    }

    /**
     * @param tipo_referencia_1 the tipo_referencia_1 to set
     */
    public void setTipo_referencia_1(String tipo_referencia_1) {
        this.tipo_referencia_1 = tipo_referencia_1;
    }

    /**
     * @return the referencia_1
     */
    public String getReferencia_1() {
        return referencia_1;
    }

    /**
     * @param referencia_1 the referencia_1 to set
     */
    public void setReferencia_1(String referencia_1) {
        this.referencia_1 = referencia_1;
    }

    /**
     * @return the tipo_referencia_2
     */
    public String getTipo_referencia_2() {
        return tipo_referencia_2;
    }

    /**
     * @param tipo_referencia_2 the tipo_referencia_2 to set
     */
    public void setTipo_referencia_2(String tipo_referencia_2) {
        this.tipo_referencia_2 = tipo_referencia_2;
    }

    /**
     * @return the referencia_2
     */
    public String getReferencia_2() {
        return referencia_2;
    }

    /**
     * @param referencia_2 the referencia_2 to set
     */
    public void setReferencia_2(String referencia_2) {
        this.referencia_2 = referencia_2;
    }

    /**
     * @return the tipo_referencia_3
     */
    public String getTipo_referencia_3() {
        return tipo_referencia_3;
    }

    /**
     * @param tipo_referencia_3 the tipo_referencia_3 to set
     */
    public void setTipo_referencia_3(String tipo_referencia_3) {
        this.tipo_referencia_3 = tipo_referencia_3;
    }

    /**
     * @return the referencia_3
     */
    public String getReferencia_3() {
        return referencia_3;
    }

    /**
     * @param referencia_3 the referencia_3 to set
     */
    public void setReferencia_3(String referencia_3) {
        this.referencia_3 = referencia_3;
    }









}