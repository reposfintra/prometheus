/*
 * To change this license header; choose License Headers in Project Properties.
 * To change this template file; choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.opav.model.beans;

/**
 *
 * @author Wsiado
 */
public class CotizacionSl {

    private int id_cotizacion;
    private String reg_status;
    private String dstrct;
    private String id_accion;
    private String no_cotizacion;
    private String cod_cli;
    private String nonmbre_cliente;
    private String vigencia_cotizacion;
    private String forma_visualizacion;
    private String modalidad_comercial;
    private Double material;
    private Double mano_obra;
    private Double equipos;
    private Double herramientas;
    private Double transporte;
    private Double tramites;
    private Double valor_cotizacion;
    private Double perc_descuento;
    private Double valor_descuento;
    private Double subtotal;
    private Double perc_iva;
    private Double valor_iva;
    private Double administracion;
    private Double imprevisto;
    private Double utilidad;
    private Double perc_aiu;
    private Double valor_aiu;
    private Double perc_administracion;
    private Double perc_imprevisto;
    private Double perc_utilidad;
    private Double total;
    private String anticipo;
    private Double perc_anticipo;
    private Double valor_anticipo;
    private String retegarantia;
    private Double perc_retegarantia;
    private String last_update;
    private String user_update;
    private String creation_date;
    private String creation_user;

    public CotizacionSl() {
    }

    public CotizacionSl(int id_cotizacion, String reg_status, String dstrct, String id_accion, String no_cotizacion, String cod_cli, String nonmbre_cliente, String vigencia_cotizacion, String forma_visualizacion, String modalidad_comercial, Double material, Double mano_obra, Double equipos, Double herramientas, Double transporte, Double tramites, Double valor_cotizacion, Double valor_descuento, Double subtotal, Double perc_iva, Double valor_iva, Double administracion, Double imprevisto, Double utilidad, Double perc_aiu, Double valor_aiu, Double perc_administracion, Double perc_imprevisto, Double perc_utilidad, Double total, String anticipo, Double perc_anticipo, Double valor_anticipo, String retegarantia, Double perc_retegarantia, String last_update, String user_update, String creation_date, String creation_user) {
        this.id_cotizacion = id_cotizacion;
        this.reg_status = reg_status;
        this.dstrct = dstrct;
        this.id_accion = id_accion;
        this.no_cotizacion = no_cotizacion;
        this.cod_cli = cod_cli;
        this.nonmbre_cliente = nonmbre_cliente;
        this.vigencia_cotizacion = vigencia_cotizacion;
        this.forma_visualizacion = forma_visualizacion;
        this.modalidad_comercial = modalidad_comercial;
        this.material = material;
        this.mano_obra = mano_obra;
        this.equipos = equipos;
        this.herramientas = herramientas;
        this.transporte = transporte;
        this.tramites = tramites;
        this.valor_cotizacion = valor_cotizacion;
        this.valor_descuento = valor_descuento;
        this.subtotal = subtotal;
        this.perc_iva = perc_iva;
        this.valor_iva = valor_iva;
        this.administracion = administracion;
        this.imprevisto = imprevisto;
        this.utilidad = utilidad;
        this.perc_aiu = perc_aiu;
        this.valor_aiu = valor_aiu;
        this.perc_administracion = perc_administracion;
        this.perc_imprevisto = perc_imprevisto;
        this.perc_utilidad = perc_utilidad;
        this.total = total;
        this.anticipo = anticipo;
        this.perc_anticipo = perc_anticipo;
        this.valor_anticipo = valor_anticipo;
        this.retegarantia = retegarantia;
        this.perc_retegarantia = perc_retegarantia;
        this.last_update = last_update;
        this.user_update = user_update;
        this.creation_date = creation_date;
        this.creation_user = creation_user;
    }

    public int getId_cotizacion() {
        return id_cotizacion;
    }

    public void setId_cotizacion(int id_cotizacion) {
        this.id_cotizacion = id_cotizacion;
    }

    public String getReg_status() {
        return reg_status;
    }

    public void setReg_status(String reg_status) {
        this.reg_status = reg_status;
    }

    public String getDstrct() {
        return dstrct;
    }

    public void setDstrct(String dstrct) {
        this.dstrct = dstrct;
    }

    public String getId_accion() {
        return id_accion;
    }

    public void setId_accion(String id_accion) {
        this.id_accion = id_accion;
    }

    public String getNo_cotizacion() {
        return no_cotizacion;
    }

    public void setNo_cotizacion(String no_cotizacion) {
        this.no_cotizacion = no_cotizacion;
    }

    public String getCod_cli() {
        return cod_cli;
    }

    public void setCod_cli(String cod_cli) {
        this.cod_cli = cod_cli;
    }

    public String getNonmbre_cliente() {
        return nonmbre_cliente;
    }

    public void setNonmbre_cliente(String nonmbre_cliente) {
        this.nonmbre_cliente = nonmbre_cliente;
    }

    public String getVigencia_cotizacion() {
        return vigencia_cotizacion;
    }

    public void setVigencia_cotizacion(String vigencia_cotizacion) {
        this.vigencia_cotizacion = vigencia_cotizacion;
    }

    public String getForma_visualizacion() {
        return forma_visualizacion;
    }

    public void setForma_visualizacion(String forma_visualizacion) {
        this.forma_visualizacion = forma_visualizacion;
    }

    public String getModalidad_comercial() {
        return modalidad_comercial;
    }

    public void setModalidad_comercial(String modalidad_comercial) {
        this.modalidad_comercial = modalidad_comercial;
    }

    public Double getMaterial() {
        return material;
    }

    public void setMaterial(Double material) {
        this.material = material;
    }

    public Double getMano_obra() {
        return mano_obra;
    }

    public void setMano_obra(Double mano_obra) {
        this.mano_obra = mano_obra;
    }

    public Double getEquipos() {
        return equipos;
    }

    public void setEquipos(Double equipos) {
        this.equipos = equipos;
    }

    public Double getHerramientas() {
        return herramientas;
    }

    public void setHerramientas(Double herramientas) {
        this.herramientas = herramientas;
    }

    public Double getTransporte() {
        return transporte;
    }

    public void setTransporte(Double transporte) {
        this.transporte = transporte;
    }

    public Double getTramites() {
        return tramites;
    }

    public void setTramites(Double tramites) {
        this.tramites = tramites;
    }

    public Double getValor_cotizacion() {
        return valor_cotizacion;
    }

    public void setValor_cotizacion(Double valor_cotizacion) {
        this.valor_cotizacion = valor_cotizacion;
    }

    public Double getValor_descuento() {
        return valor_descuento;
    }

    public void setValor_descuento(Double valor_descuento) {
        this.valor_descuento = valor_descuento;
    }

    public Double getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(Double subtotal) {
        this.subtotal = subtotal;
    }

    public Double getPerc_iva() {
        return perc_iva;
    }

    public void setPerc_iva(Double perc_iva) {
        this.perc_iva = perc_iva;
    }

    public Double getValor_iva() {
        return valor_iva;
    }

    public void setValor_iva(Double valor_iva) {
        this.valor_iva = valor_iva;
    }

    public Double getAdministracion() {
        return administracion;
    }

    public void setAdministracion(Double administracion) {
        this.administracion = administracion;
    }

    public Double getImprevisto() {
        return imprevisto;
    }

    public void setImprevisto(Double imprevisto) {
        this.imprevisto = imprevisto;
    }

    public Double getUtilidad() {
        return utilidad;
    }

    public void setUtilidad(Double utilidad) {
        this.utilidad = utilidad;
    }

    public Double getPerc_aiu() {
        return perc_aiu;
    }

    public void setPerc_aiu(Double perc_aiu) {
        this.perc_aiu = perc_aiu;
    }

    public Double getValor_aiu() {
        return valor_aiu;
    }

    public void setValor_aiu(Double valor_aiu) {
        this.valor_aiu = valor_aiu;
    }

    public Double getPerc_administracion() {
        return perc_administracion;
    }

    public void setPerc_administracion(Double perc_administracion) {
        this.perc_administracion = perc_administracion;
    }

    public Double getPerc_imprevisto() {
        return perc_imprevisto;
    }

    public void setPerc_imprevisto(Double perc_imprevisto) {
        this.perc_imprevisto = perc_imprevisto;
    }

    public Double getPerc_utilidad() {
        return perc_utilidad;
    }

    public void setPerc_utilidad(Double perc_utilidad) {
        this.perc_utilidad = perc_utilidad;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public String getAnticipo() {
        return anticipo;
    }

    public void setAnticipo(String anticipo) {
        this.anticipo = anticipo;
    }

    public Double getPerc_anticipo() {
        return perc_anticipo;
    }

    public void setPerc_anticipo(Double perc_anticipo) {
        this.perc_anticipo = perc_anticipo;
    }

    public Double getValor_anticipo() {
        return valor_anticipo;
    }

    public void setValor_anticipo(Double valor_anticipo) {
        this.valor_anticipo = valor_anticipo;
    }

    public String getRetegarantia() {
        return retegarantia;
    }

    public void setRetegarantia(String retegarantia) {
        this.retegarantia = retegarantia;
    }

    public Double getPerc_retegarantia() {
        return perc_retegarantia;
    }

    public void setPerc_retegarantia(Double perc_retegarantia) {
        this.perc_retegarantia = perc_retegarantia;
    }

    public String getLast_update() {
        return last_update;
    }

    public void setLast_update(String last_update) {
        this.last_update = last_update;
    }

    public String getUser_update() {
        return user_update;
    }

    public void setUser_update(String user_update) {
        this.user_update = user_update;
    }

    public String getCreation_date() {
        return creation_date;
    }

    public void setCreation_date(String creation_date) {
        this.creation_date = creation_date;
    }

    public String getCreation_user() {
        return creation_user;
    }

    public void setCreation_user(String creation_user) {
        this.creation_user = creation_user;
    }

    public Double getPerc_descuento() {
        return perc_descuento;
    }

    public void setPerc_descuento(Double perc_descuento) {
        this.perc_descuento = perc_descuento;
    }

}
