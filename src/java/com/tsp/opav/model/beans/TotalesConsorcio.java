/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.opav.model.beans;

/**
 *
 * @author Alvaro
 */
public class TotalesConsorcio {


    private String[] claseTotal = {"subtotal_base_1","base_iva","valor_a_financiar",
                                   "intereses","valor_con_financiacion",
                                   "subtotal_sin_iva" , "subtotal_iva",
                                   "valor_extemporaneo_1", "cuota_inicial",
                                   "valor_extemporaneo_2", "intereses",
                                   "item_contratista",
                                   "item_comision_opav",
                                   "item_comision_fintra",
                                   "item_comision_interventoria",
                                   "item_comision_provintegral",
                                   "item_comision_eca",
                                   "item_extemporaneo_1",
                                   "item_cuota_inicial",
                                   "item_iva_contratista",
                                   "item_iva_bonificacion",
                                   "item_iva_opav",
                                   "item_iva_fintra",
                                   "item_iva_interventoria",
                                   "item_iva_provintegral",
                                   "item_iva_eca",
                                   "item_extemporaneo_2",
                                   "item_intereses",
                                   "item_bonificacion",
                                   "bonificacion",
                                   "bonificacionSubcliente"};






    private double valores[][] = new double [claseTotal.length][2] ;


    /** Creates a new instance of Subtotal_base_1 */
    public TotalesConsorcio() {

        setClear();

    }



    /**
     * Ubica la posicion en el vector que corresponde al concepto que se desea
     * inicializar o acumular valores
     * @param claseValor Especifica el concepto de la variable a inicializar
     * @return Retorna el indice o -1 que significa que no se encontro ese concepto
     */

    private int getIndice(String claseValor) {
        for ( int i = 0; i<claseTotal.length; i++){
            if(claseValor.equalsIgnoreCase(claseTotal[i]) ) {
                return i;
            }
        }
        return -1;
    }




    /** Inicializa las variables del objeto */
    public void setClear() {

        for ( int i = 0; i<this.valores.length; i++){
            valores[i][0] = 0.00;
            valores[i][1] = 0.00;
        }
    }



/**
 * Inicializa una variable que acumula los totales
 * @param valor Es el valor a inicializar la variable
 * @param claseTotal Especifica el concepto de la variable a inicializar
 * @param operacion  Indica si el valor a inicializar es para un valor total o un valor que acumulara
 * TOTAL   = Indica el valor total acumulado,
 * PARCIAL = Indica la variable que acumulará valores hasta llegar al valor total acumulado
 */

    public void setInicializar(double valor, String claseTotal, String operacion){

        if(operacion.equalsIgnoreCase("TOTAL")){
            int i = getIndice(claseTotal);
            this.valores[i][0] = valor;
            this.valores[i][1] = 0.00;
        }
        else if(operacion.equalsIgnoreCase("PARCIAL")) {
            int i = getIndice(claseTotal);
            this.valores[i][1] = valor;
        }
        else if(operacion.equalsIgnoreCase("SOLO_TOTAL")) {
            int i = getIndice(claseTotal);
            this.valores[i][0] = valor;
        }
    }





    /**
     * Devuelve el mismo valor o la diferencia entre un valor total menos el valor que se ha acumulado.
     * La diferencia entre el valor total menos el valor acumulado corresponderia al ultimo item
     * Desarrollado para determinar el ultimo valor sin problemas de redondeo cuando numeroTotalItem = numeroItemProcesado
     * @param valor Valor del item
     * @param claseTotal Especifica el concepto de la variable a inicializar
     * @param numeroTotalItem Indica el numero total de items a calcular
     * @param numeroItemProcesado Indica el item a calcular
     * @return Retorna el mismo valor o la diferencia entre el total menos los acumulados hasta el momento
     */
    public double getValor(double valor, String claseTotal,  int numeroTotalItem, int numeroItemProcesado){


        int i = getIndice(claseTotal);

        if(numeroItemProcesado==numeroTotalItem){
            // Calcula el ultimo item por diferencia entre la columna de total - y la columna de parcial
            double valor_item = this.valores[i][0] - this.valores[i][1];
            this.valores[i][1]  += valor_item;
            return valor_item;
        }
        else {
            // Devuelve el mismo valor del item y acumula en la columna de parcial
            this.valores[i][1] += valor;
            return valor;
        }

    }



}