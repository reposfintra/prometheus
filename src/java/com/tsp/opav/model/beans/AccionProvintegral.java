/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.opav.model.beans;

/**
 *
 * @author Alvaro
 */
public class AccionProvintegral {



     private String id_accion;
     private String nit;
          private String id_solicitud;
     private String contratista;
     private double vlr_provintegral;
     private double iva_provintegral;
     private String factura_cliente;
     private String fecha_factura;




    public AccionProvintegral () {
    }



     public void Clear() {
         this.contratista = "";
         this.factura_cliente = "";
         this.fecha_factura= "";
         this.id_accion = "";
         this.id_solicitud = "";
         this.iva_provintegral= 0.00;
         this.vlr_provintegral = 0.00;

     }


    public static AccionProvintegral load(java.sql.ResultSet rs)throws java.sql.SQLException{

        AccionProvintegral accionProvintegral = new AccionProvintegral();

        accionProvintegral.setContratista( rs.getString("contratista") );
        accionProvintegral.setFactura_cliente( rs.getString("factura_cliente") );
        accionProvintegral.setFecha_factura( rs.getString("fecha_factura") );
        accionProvintegral.setId_accion( rs.getString("id_accion") );
        accionProvintegral.setId_solicitud( rs.getString("id_solicitud") );
        accionProvintegral.setIva_provintegral( rs.getDouble("iva_provintegral") );
        accionProvintegral.setVlr_provintegral( rs.getDouble("vlr_provintegral") );
        accionProvintegral.setNit( rs.getString("nit") );

        return accionProvintegral;
    }




    /**
     * @return the factura_cliente
     */
    public String getFactura_cliente() {
        return factura_cliente;
    }

    /**
     * @param factura_cliente the factura_cliente to set
     */
    public void setFactura_cliente(String factura_cliente) {
        this.factura_cliente = factura_cliente;
    }

    /**
     * @return the id_accion
     */
    public String getId_accion() {
        return id_accion;
    }

    /**
     * @param id_accion the id_accion to set
     */
    public void setId_accion(String id_accion) {
        this.id_accion = id_accion;
    }

    /**
     * @return the id_solicitud
     */
    public String getId_solicitud() {
        return id_solicitud;
    }

    /**
     * @param id_solicitud the id_solicitud to set
     */
    public void setId_solicitud(String id_solicitud) {
        this.id_solicitud = id_solicitud;
    }

    /**
     * @return the contratista
     */
    public String getContratista() {
        return contratista;
    }

    /**
     * @param contratista the contratista to set
     */
    public void setContratista(String contratista) {
        this.contratista = contratista;
    }

    /**
     * @return the vlr_provintegral
     */
    public double getVlr_provintegral() {
        return vlr_provintegral;
    }

    /**
     * @param vlr_provintegral the vlr_provintegral to set
     */
    public void setVlr_provintegral(double vlr_provintegral) {
        this.vlr_provintegral = vlr_provintegral;
    }

    /**
     * @return the iva_provintegral
     */
    public double getIva_provintegral() {
        return iva_provintegral;
    }

    /**
     * @param iva_provintegral the iva_provintegral to set
     */
    public void setIva_provintegral(double iva_provintegral) {
        this.iva_provintegral = iva_provintegral;
    }

    /**
     * @return the fecha_factura
     */
    public String getFecha_factura() {
        return fecha_factura;
    }

    /**
     * @param fecha_factura the fecha_factura to set
     */
    public void setFecha_factura(String fecha_factura) {
        this.fecha_factura = fecha_factura;
    }

    /**
     * @return the nit
     */
    public String getNit() {
        return nit;
    }

    /**
     * @param nit the nit to set
     */
    public void setNit(String nit) {
        this.nit = nit;
    }

}