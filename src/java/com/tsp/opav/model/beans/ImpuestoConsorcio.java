/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.opav.model.beans;



import java.util.*;

/**
 *
 * @author Alvaro
 */
public class ImpuestoConsorcio {


    private ImpuestoContrato [] iva;
    private ImpuestoContrato [] retencionIva;

    private ImpuestoContrato [] retencionIcaConAIU;
    private ImpuestoContrato [] retencionIcaSinAIU;

    private ImpuestoContrato [] retencionMaterialConAIU;
    private ImpuestoContrato [] retencionMaterialSinAIU;

    private ImpuestoContrato [] retencionManoObraConAIU;
    private ImpuestoContrato [] retencionManoObraSinAIU;

    private ImpuestoContrato [] retencionOtrosConAIU;
    private ImpuestoContrato [] retencionOtrosSinAIU;

    private ImpuestoContrato [] retencionAiuConAIU;
    private ImpuestoContrato [] retencionAiuSinAIU;

    private int numeroAnos;




    /** Creates a new instance of Prefactura */
    public ImpuestoConsorcio(int numeroAnos) {

        iva                     = new ImpuestoContrato[numeroAnos];
        retencionIva            = new ImpuestoContrato[numeroAnos];
        retencionIcaConAIU      = new ImpuestoContrato[numeroAnos];
        retencionIcaSinAIU      = new ImpuestoContrato[numeroAnos];
        retencionMaterialConAIU = new ImpuestoContrato[numeroAnos];
        retencionMaterialSinAIU = new ImpuestoContrato[numeroAnos];
        retencionManoObraConAIU = new ImpuestoContrato[numeroAnos];
        retencionManoObraSinAIU = new ImpuestoContrato[numeroAnos];
        retencionOtrosConAIU    = new ImpuestoContrato[numeroAnos];
        retencionOtrosSinAIU    = new ImpuestoContrato[numeroAnos];
        retencionAiuConAIU      = new ImpuestoContrato[numeroAnos];
        retencionAiuSinAIU      = new ImpuestoContrato[numeroAnos];

        this.numeroAnos = numeroAnos;

        for (int i =0;i<numeroAnos;i++){

            iva[i]=new ImpuestoContrato();  // 20100620 apm
            retencionIva[i]=new ImpuestoContrato();
            retencionIcaConAIU[i]=new ImpuestoContrato();
            retencionIcaSinAIU[i]=new ImpuestoContrato();
            retencionMaterialConAIU[i]=new ImpuestoContrato();
            retencionMaterialSinAIU[i]=new ImpuestoContrato();
            retencionManoObraConAIU[i]=new ImpuestoContrato();
            retencionManoObraSinAIU[i]=new ImpuestoContrato();
            retencionOtrosConAIU[i]=new ImpuestoContrato();
            retencionOtrosSinAIU[i]=new ImpuestoContrato();
            retencionAiuConAIU[i]=new ImpuestoContrato();
            retencionAiuSinAIU[i]=new ImpuestoContrato();
        }
    }


    /**
     * Manejo de impuesto
     * @param impuestoEspecifico
     * @param tipoImpuesto
     * @param indiceAno
     * Modificaciones:
     * 20100620 : Se adiciona la consecucion del impuesto del RIVA obtenido en forma independiente como los otros
     */
    public void setImpuesto(ImpuestoContrato impuestoEspecifico,String tipoImpuesto, int indiceAno) {

        if (tipoImpuesto.equalsIgnoreCase("IVA")){  // 20100620 apm
            iva[indiceAno] = impuestoEspecifico;
        }
        else if (tipoImpuesto.equalsIgnoreCase("RIVA")) {  //20100620 apm
            retencionIva[indiceAno] = impuestoEspecifico;
        }
        else if (tipoImpuesto.equalsIgnoreCase("RETEICA CON AIU")){
            retencionIcaConAIU[indiceAno] = impuestoEspecifico;
        }
        else if (tipoImpuesto.equalsIgnoreCase("RETEICA SIN AIU")){
            retencionIcaSinAIU[indiceAno] = impuestoEspecifico;
        }
        else if (tipoImpuesto.equalsIgnoreCase("RETEFUENTE MATERIAL CON AIU")){
            retencionMaterialConAIU[indiceAno] = impuestoEspecifico;
        }
        else if (tipoImpuesto.equalsIgnoreCase("RETEFUENTE MATERIAL SIN AIU")){
            retencionMaterialSinAIU[indiceAno] = impuestoEspecifico;
        }
        else if (tipoImpuesto.equalsIgnoreCase("RETEFUENTE MANO DE OBRA CON AIU")){
            retencionManoObraConAIU[indiceAno] = impuestoEspecifico;
        }
        else if (tipoImpuesto.equalsIgnoreCase("RETEFUENTE MANO DE OBRA SIN AIU")){
            retencionManoObraSinAIU[indiceAno] = impuestoEspecifico;
        }
        else if (tipoImpuesto.equalsIgnoreCase("RETEFUENTE OTROS CON AIU")){
            retencionOtrosConAIU[indiceAno] = impuestoEspecifico;
        }
        else if (tipoImpuesto.equalsIgnoreCase("RETEFUENTE OTROS SIN AIU")){
            retencionOtrosSinAIU[indiceAno] = impuestoEspecifico;
        }
         else if (tipoImpuesto.equalsIgnoreCase("RETEFUENTE CON AIU")){
            retencionAiuConAIU[indiceAno] = impuestoEspecifico;
        }
        else if (tipoImpuesto.equalsIgnoreCase("RETEFUENTE SIN AIU")){
            retencionAiuSinAIU[indiceAno] = impuestoEspecifico;
        }

    }

    public String getCodigo(String tipoImpuesto, String anoVigencia) {


        // Convertir el anoVigencia en un indice a la tabla de arreglos de impuestos

        // Determina el ano actual : int AAAA
        Calendar ahoraCal = Calendar.getInstance();
        int anoActual = ahoraCal.get(Calendar.YEAR);
        int ano = Integer.parseInt(anoVigencia);
        int indiceAno = anoActual - ano;

        if ( (indiceAno < 0 ) || (indiceAno > 1) ) {
            indiceAno = 0;
        }


        if (tipoImpuesto.equalsIgnoreCase("IVA")){
            return iva[indiceAno].getCodigo_impuesto();
        }
        else if (tipoImpuesto.equalsIgnoreCase("RETEIVA")){
            return retencionIva[indiceAno].getCodigo_impuesto();
        }
        else if (tipoImpuesto.equalsIgnoreCase("RETEICA CON AIU")){
            return retencionIcaConAIU[indiceAno].getCodigo_impuesto();
        }
        else if (tipoImpuesto.equalsIgnoreCase("RETEICA SIN AIU")){
           return retencionIcaSinAIU[indiceAno].getCodigo_impuesto();
        }


        else if (tipoImpuesto.equalsIgnoreCase("RETEFUENTE MATERIAL CON AIU")){
            return retencionMaterialConAIU[indiceAno].getCodigo_impuesto();
        }
        else if (tipoImpuesto.equalsIgnoreCase("RETEFUENTE MATERIAL CON AIU")){
            return retencionMaterialSinAIU[indiceAno].getCodigo_impuesto();
        }


        else if (tipoImpuesto.equalsIgnoreCase("RETEFUENTE MANO DE OBRA CON AIU")){
            return retencionManoObraConAIU[indiceAno].getCodigo_impuesto();
        }
        else if (tipoImpuesto.equalsIgnoreCase("RETEFUENTE MANO DE OBRA SIN AIU")){
            return retencionManoObraSinAIU[indiceAno].getCodigo_impuesto();
        }


        else if (tipoImpuesto.equalsIgnoreCase("RETEFUENTE OTROS CON AIU")){
            return retencionOtrosConAIU[indiceAno].getCodigo_impuesto();
        }
        else if (tipoImpuesto.equalsIgnoreCase("RETEFUENTE OTROS SIN AIU")){
            return retencionOtrosSinAIU[indiceAno].getCodigo_impuesto();
        }


         else if (tipoImpuesto.equalsIgnoreCase("RETEFUENTE CON AIU")){
            return retencionAiuConAIU[indiceAno].getCodigo_impuesto();
        }
        else if (tipoImpuesto.equalsIgnoreCase("RETEFUENTE SIN AIU")){
            return retencionAiuSinAIU[indiceAno].getCodigo_impuesto();
        }
        else
            return "";



    }


    public String getDescripcion(String tipoImpuesto,String anoVigencia) {

        // Convertir el anoVigencia en un indice a la tabla de arreglos de impuestos

        // Determina el ano actual : int AAAA
        Calendar ahoraCal = Calendar.getInstance();
        int anoActual = ahoraCal.get(Calendar.YEAR);
        int ano = Integer.parseInt(anoVigencia);
        int indiceAno = anoActual - ano;

        if (indiceAno < 0) {
            indiceAno = 0;
        }



        if (tipoImpuesto.equalsIgnoreCase("IVA")){
            return iva[indiceAno].getDescripcion();
        }
        else if (tipoImpuesto.equalsIgnoreCase("RETEIVA")){
            return retencionIva[indiceAno].getDescripcion();
        }
        else if (tipoImpuesto.equalsIgnoreCase("RETEICA CON AIU")){
            return retencionIcaConAIU[indiceAno].getDescripcion();
        }
        else if (tipoImpuesto.equalsIgnoreCase("RETEICA SIN AIU")){
           return retencionIcaSinAIU[indiceAno].getDescripcion();
        }


        else if (tipoImpuesto.equalsIgnoreCase("RETEFUENTE MATERIAL CON AIU")){
            return retencionMaterialConAIU[indiceAno].getDescripcion();
        }
        else if (tipoImpuesto.equalsIgnoreCase("RETEFUENTE MATERIAL CON AIU")){
            return retencionMaterialSinAIU[indiceAno].getDescripcion();
        }


        else if (tipoImpuesto.equalsIgnoreCase("RETEFUENTE MANO DE OBRA CON AIU")){
            return retencionManoObraConAIU[indiceAno].getDescripcion();
        }
        else if (tipoImpuesto.equalsIgnoreCase("RETEFUENTE MANO DE OBRA SIN AIU")){
            return retencionManoObraSinAIU[indiceAno].getDescripcion();
        }


        else if (tipoImpuesto.equalsIgnoreCase("RETEFUENTE OTROS CON AIU")){
            return retencionOtrosConAIU[indiceAno].getDescripcion();
        }
        else if (tipoImpuesto.equalsIgnoreCase("RETEFUENTE OTROS SIN AIU")){
            return retencionOtrosSinAIU[indiceAno].getDescripcion();
        }


         else if (tipoImpuesto.equalsIgnoreCase("RETEFUENTE CON AIU")){
            return retencionAiuConAIU[indiceAno].getDescripcion();
        }
        else if (tipoImpuesto.equalsIgnoreCase("RETEFUENTE SIN AIU")){
            return retencionAiuSinAIU[indiceAno].getDescripcion();
        }
        else
            return "";


    }


    public double getPorcentaje(String tipoImpuesto, String anoVigencia) {

        // Convertir el anoVigencia en un indice a la tabla de arreglos de impuestos

        // Determina el ano actual : int AAAA
        Calendar ahoraCal = Calendar.getInstance();
        int anoActual = ahoraCal.get(Calendar.YEAR);
        int ano = Integer.parseInt(anoVigencia);
        int indiceAno = anoActual - ano;

        if (indiceAno < 0) {
            indiceAno = 0;
        }


        if (tipoImpuesto.equalsIgnoreCase("IVA")){
            return iva[indiceAno].getPorcentaje1();
        }
        if (tipoImpuesto.equalsIgnoreCase("RETEIVA")){
            return retencionIva[indiceAno].getPorcentaje1();
        }
        else if (tipoImpuesto.equalsIgnoreCase("RETEICA CON AIU")){
            return retencionIcaConAIU[indiceAno].getPorcentaje1();
        }
        else if (tipoImpuesto.equalsIgnoreCase("RETEICA SIN AIU")){
           return retencionIcaSinAIU[indiceAno].getPorcentaje1();
        }


        else if (tipoImpuesto.equalsIgnoreCase("RETEFUENTE MATERIAL CON AIU")){
            return retencionMaterialConAIU[indiceAno].getPorcentaje1();
        }
        else if (tipoImpuesto.equalsIgnoreCase("RETEFUENTE MATERIAL SIN AIU")){
            return retencionMaterialSinAIU[indiceAno].getPorcentaje1();
        }


        else if (tipoImpuesto.equalsIgnoreCase("RETEFUENTE MANO DE OBRA CON AIU")){
            return retencionManoObraConAIU[indiceAno].getPorcentaje1();
        }
        else if (tipoImpuesto.equalsIgnoreCase("RETEFUENTE MANO DE OBRA SIN AIU")){
            return retencionManoObraSinAIU[indiceAno].getPorcentaje1();
        }


        else if (tipoImpuesto.equalsIgnoreCase("RETEFUENTE OTROS CON AIU")){
            return retencionOtrosConAIU[indiceAno].getPorcentaje1();
        }
        else if (tipoImpuesto.equalsIgnoreCase("RETEFUENTE OTROS SIN AIU")){
            return retencionOtrosSinAIU[indiceAno].getPorcentaje1();
        }


         else if (tipoImpuesto.equalsIgnoreCase("RETEFUENTE CON AIU")){
            return retencionAiuConAIU[indiceAno].getPorcentaje1();
        }
        else if (tipoImpuesto.equalsIgnoreCase("RETEFUENTE SIN AIU")){
            return retencionAiuSinAIU[indiceAno].getPorcentaje1();
        }
        else
            return 0.00;
    }

    /**
     * @return the numeroAnos
     */
    public int getNumeroAnos() {
        return numeroAnos;
    }

    /**
     * @param numeroAnos the numeroAnos to set
     */
    public void setNumeroAnos(int numeroAnos) {
        this.numeroAnos = numeroAnos;
    }



}