/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.opav.model.beans;

public class Comision {

  private float  valor_inicial;
  private float  valor_final;
  private float  comision_total;
  private float  comision_ejecutiva;
  private float  comision_canal;



    /** Creates a new instance of Prefactura */
    public Comision() {
    }


    public static Comision load(java.sql.ResultSet rs)throws java.sql.SQLException{

        Comision comision = new Comision();

        comision.setValor_inicial( rs.getFloat("valor_inicial") );
        comision.setValor_final( rs.getFloat("valor_final") );
        comision.setComision_total( rs.getFloat("comision_total") );
        comision.setComision_ejecutiva( rs.getFloat("comision_ejecutiva") );
        comision.setComision_canal( rs.getFloat("comision_canal") );

        return comision;
    }





    /**
     * @return the valor_inicial
     */
    public float getValor_inicial() {
        return valor_inicial;
    }

    /**
     * @param valor_inicial the valor_inicial to set
     */
    public void setValor_inicial(float valor_inicial) {
        this.valor_inicial = valor_inicial;
    }

    /**
     * @return the valor_final
     */
    public float getValor_final() {
        return valor_final;
    }

    /**
     * @param valor_final the valor_final to set
     */
    public void setValor_final(float valor_final) {
        this.valor_final = valor_final;
    }

    /**
     * @return the comision_total
     */
    public float getComision_total() {
        return comision_total;
    }

    /**
     * @param comision_total the comision_total to set
     */
    public void setComision_total(float comision_total) {
        this.comision_total = comision_total;
    }

    /**
     * @return the comision_ejecutiva
     */
    public float getComision_ejecutiva() {
        return comision_ejecutiva;
    }

    /**
     * @param comision_ejecutiva the comision_ejecutiva to set
     */
    public void setComision_ejecutiva(float comision_ejecutiva) {
        this.comision_ejecutiva = comision_ejecutiva;
    }

    /**
     * @return the comision_canal
     */
    public float getComision_canal() {
        return comision_canal;
    }

    /**
     * @param comision_canal the comision_canal to set
     */
    public void setComision_canal(float comision_canal) {
        this.comision_canal = comision_canal;
    }




}

