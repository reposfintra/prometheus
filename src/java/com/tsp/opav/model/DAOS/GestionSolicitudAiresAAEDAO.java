/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.opav.model.DAOS;

import com.tsp.operation.model.beans.BeanGeneral;
import com.tsp.operation.model.beans.StringStatement;
import java.sql.*;
import java.util.*;

/**
 * @author Ing. Iris Vargas
 */
public class GestionSolicitudAiresAAEDAO extends MainDAO {

    public GestionSolicitudAiresAAEDAO() {
        super("GestionSolicitudAiresAAEDAO.xml");
    }
    public GestionSolicitudAiresAAEDAO(String dataBaseName) {
        super("GestionSolicitudAiresAAEDAO.xml", dataBaseName);
    }

    /**
     * Busca contratistas de un tipo para solicitudes AAAE
     * @param tipo contratista es Instalador o Proveedor
     * @return ArrayList
     * @throws Exception cuando hay error
     */
    public ArrayList buscarContratistasAAAE(String tipo) throws Exception {
        ArrayList cadena = new ArrayList();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SRC_CONTRATISTAS_AAAE";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con = conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, tipo);
            rs = ps.executeQuery();
            while (rs.next()) {
                cadena.add(rs.getString("contratista") + ";_;" + rs.getString("descripcion"));
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error en buscarContratistas AAAE: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return cadena;
    }

    public String infoCliente(String nic) throws Exception {
        String info = "";
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_GET_CLIENTE_NIC";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, nic);
            rs = ps.executeQuery();
            if (rs.next()) {
                info = rs.getString("nic") + ";_;"
                        + rs.getString("titular") + ";_;"
                        + rs.getString("direccion") + ";_;"
                        + rs.getString("telefono") + ";_;"
                        + rs.getString("estrato") + ";_;"
                        + rs.getString("calificacion");

            }
        } catch (Exception e) {
            System.out.println("Error en buscar codigo cliente: " + e.toString());
            e.printStackTrace();
            throw new Exception("Error en buscar codigo cliente: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return info;
    }

    public ArrayList getTiposSolicitud() throws Exception {
        PreparedStatement st = null;
        ResultSet rs = null;
        ArrayList arl = new ArrayList();
        Connection con = null;
        String query = "SQL_TIPOS_SOLICITUDES_AIRES";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                rs = st.executeQuery();
                while (rs.next()) {
                    arl.add(new String[]{rs.getString("tipo_solicitud"), rs.getString("distribucion")});
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("error en getTiposSolicitud:" + e.toString());
            throw new SQLException("ERROR DURANTE SQL_SEARCH_TIPOS_SOLICITUDES. \n " + e.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return arl;
    }

    /**
     * Busca los equipos de tipo
     * @param tipo tipo de  material (M, D,O)
     * @return ArrayList
     * @throws Exception cuando hay error
     */
    public ArrayList buscarMaterialesAAAE(String tipo) throws Exception {
        ArrayList cadena = new ArrayList();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SRC_MATERIALES_AAAE";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con = conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, tipo);
            rs = ps.executeQuery();
            while (rs.next()) {
                cadena.add(rs.getString("table_code") + ";_;" + rs.getString("descripcion"));
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error en buscarMaterialesAAAE: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return cadena;
    }

    /**
     * Busca las instalaciones realacionadas a un equipo
     * @param equipo
     * @return ArrayList
     * @throws Exception cuando hay error
     */
    public ArrayList buscarInstalaciones(String equipo) throws Exception {
        ArrayList cadena = new ArrayList();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SRC_INSTALS_EQ";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con = conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, equipo);
            rs = ps.executeQuery();
            while (rs.next()) {
                cadena.add(rs.getString("table_code") + ";_;" + rs.getString("descripcion"));
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error en buscarInstalaciones: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return cadena;

    }

    public double getPrecioVentaMaterial(String material, String fecha, String tipo_solicitud) throws Exception {
        double precio = 0;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SRC_PRECIO_VENTA_MATERIAL";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, fecha);
            ps.setString(2, tipo_solicitud);
            ps.setString(3, material);
            rs = ps.executeQuery();
            if (rs.next()) {

                precio = rs.getDouble("precio_venta");

            }
        } catch (Exception e) {
            System.out.println("Error en getPrecioVentaMaterial: " + e.toString());
            e.printStackTrace();
            throw new Exception("Error en getPrecioVentaMaterial: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return precio;
    }

    public String insertarEncuenta(BeanGeneral encuesta) throws Exception {
        String respuesta = "";
        StringStatement st = null;
        String query = query = "INSERTAR_ENCUESTA";
        try {
            st = new StringStatement(this.obtenerSQL(query), true);//JJCastro fase2
            st.setString(1, encuesta.getValor_01());
            st.setString(2, encuesta.getValor_02());
            st.setString(3, encuesta.getValor_03());
            st.setString(4, encuesta.getValor_04());
            st.setString(5, encuesta.getValor_05());
            st.setString(6, encuesta.getCreation_user());
            st.setString(7, encuesta.getValor_06());//id_solicitud
            
            respuesta = st.getSql();

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e);
            throw e;
        } finally {
            if (st != null) {
                try {
                    st = null;
                } catch (Exception e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
        }
        return respuesta;
    }

     /**
     * Busca las ciudades de un departamento para solicitudes AAAE
     * @param departamento
     * @return ArrayList
     * @throws Exception cuando hay error
     */
    public ArrayList buscarCiudadesAAAE(String departamento) throws Exception {
        ArrayList cadena = new ArrayList();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SRC_CIUDADES_AAAE";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con = conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, departamento);
            rs = ps.executeQuery();
            while (rs.next()) {
                cadena.add(rs.getString("table_code") + ";_;" + rs.getString("nomciu"));
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error en buscarCiudades AAAE: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return cadena;
    }

     /**
     * Busca los ejecutivos  AAAE
     * @return ArrayList
     * @throws Exception cuando hay error
     */
    public ArrayList buscarEjecutivosAAAE() throws Exception {
        ArrayList cadena = new ArrayList();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_EJECUTIVOS_AAAE";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con = conectarJNDI(query);
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while (rs.next()) {
                cadena.add(rs.getString("table_code") + ";_;" + rs.getString("nombre"));
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error en buscarEjecutivos AAAE: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return cadena;
    }

    /**
     * Busca dato en tablagen
     * @param table_type a buscar
     * @param referencia a buscar
     * @return listado con datos coincidentes
     * @throws Exception cuando hay error
     */
    public ArrayList busquedaGeneralRef(String table_type, String referencia) throws Exception {
        ArrayList cadena = new ArrayList();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SRC_TBG_REF";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con = conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, table_type);
            ps.setString(2, referencia);
            rs = ps.executeQuery();
            while (rs.next()) {
                cadena.add(rs.getString("table_code") + ";_;" + rs.getString("descripcion"));
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error en busqueda general por referencia: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return cadena;
    }

    /**
     * Busca el contratista que debe realizar la proxima visita
     * @return contratista
     * @throws Exception cuando hay error
     */
    public String asignarContratista() throws Exception {
        String contratista = "";
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SRC_ASIGNAR_CONTRATISTA";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            if (rs.next()) {
                contratista = rs.getString("contratista");
            }
        } catch (Exception e) {
            System.out.println("Error en buscar codigo cliente: " + e.toString());
            e.printStackTrace();
            throw new Exception("Error en buscar codigo cliente: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return contratista;
    }

    /**
     * Busca el rango de horas de posible seleccion segun la hora actual
     * @return rango de horas
     * @throws Exception cuando hay error
     */
    public String rangoHoras() throws Exception {
        String rangos = "";
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SRC_RANGO_HORAS";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            if (rs.next()) {
                rangos = rs.getString("hora_ini_visita")+";";
                rangos += rs.getString("hora_fin_visita")+";";
                rangos += rs.getString("dia_suma");
            }
        } catch (Exception e) {
            System.out.println("Error en rango Horas: " + e.toString());
            e.printStackTrace();
            throw new Exception("Error en rango Horas: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return rangos;
    }

    /**
     * Busca si un contratista puede realizar una visita en una fecha y hora determinada
     * @return boolean
     * @throws Exception cuando hay error
     */
    public boolean puedeRealizarVisita(String contratista, String fecha, String hora) throws Exception {
        boolean ok=false;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SRC_VISITAS_ACTUALES";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, contratista);
            ps.setString(2, contratista);
            ps.setString(3, fecha+" "+hora);
            ps.setString(4, fecha+" "+hora);
            rs = ps.executeQuery();
            if (rs.next()) {
                ok = (rs.getInt("visitas")<rs.getInt("num_tecnicos"));
            }
        } catch (Exception e) {
            System.out.println("Error en puedeRealizarVisita: " + e.toString());
            e.printStackTrace();
            throw new Exception("Error en puedeRealizarVisita: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return ok;
    }

     public String insertarVisita(BeanGeneral visita) throws Exception {
        String respuesta = "";
        StringStatement st = null;
        String query = query = "SQL_INSERTAR_VISITA";
        try {
            st = new StringStatement(this.obtenerSQL(query), true);//JJCastro fase2
            st.setString(1, visita.getValor_01());//id_solicitud
            st.setString(2, visita.getValor_02());//fecha
            st.setString(3, visita.getValor_03());//hora
            st.setString(4, visita.getValor_04());//contratista
            st.setString(5, visita.getCreation_user());//usuario creacion

            respuesta = st.getSql();

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e);
            throw e;
        } finally {
            if (st != null) {
                try {
                    st = null;
                } catch (Exception e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
        }
        return respuesta;
    }

      public double getPrecioMaterial(String material) throws Exception {
        double precio = 0;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SRC_PRECIO_MATERIAL";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, material);
            rs = ps.executeQuery();
            if (rs.next()) {

                precio = rs.getDouble("precio");

            }
        } catch (Exception e) {
            System.out.println("Error en getPrecioMaterial: " + e.toString());
            e.printStackTrace();
            throw new Exception("Error en getPrecioMaterial: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return precio;
    }

     public double getValorFinanciado(double valor, int plazo) throws Exception {
        double precio = 0;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SRC_VALOR_FINANCIADO";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setDouble(1, valor);
            ps.setInt(2, plazo);
            ps.setInt(3, plazo);
            ps.setInt(4, plazo);
            rs = ps.executeQuery();
            if (rs.next()) {

                precio = rs.getDouble("precio");

            }
        } catch (Exception e) {
            System.out.println("Error en getValorFinanciado: " + e.toString());
            e.printStackTrace();
            throw new Exception("Error en getValorFinanciado: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return precio;
    }

      /**
     * Lista las horas disponible de una fecha y contratista especifico
     * @return ArrayList
     * @throws Exception cuando hay error
     */
    public ArrayList cargarHorasDisponibles(String fecha, String contratista) throws Exception {
    ArrayList cadena = new ArrayList();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SRC_CARGAR_HORAS_DIPONIBLES";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con = conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, fecha);
            ps.setString(2, fecha);
            ps.setString(3, contratista);
            ps.setString(4, contratista);
            rs = ps.executeQuery();
            while (rs.next()) {
                cadena.add(rs.getString("table_code") + ";_;" + rs.getString("dato"));
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error en busqueda general por referencia: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return cadena;
    }
    
    /**
     * Busca los equipos de una solicitud y su garantia
     * @param opd numero del formulario
     * @return ArrayList
     * @throws Exception cuando hay error
     */
    public ArrayList buscarGarantiaEquipos(String opd) throws Exception {
        ArrayList cadena = new ArrayList();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SRC_GARANTIA_EQUIPO";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con = conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, opd);
            rs = ps.executeQuery();
            while (rs.next()) {
                cadena.add(rs.getString("cod_material") + ";_;" + rs.getString("equipo") + ";_;" + rs.getString("formulario") + ";_;" + rs.getString("garantia") + ";_;" + rs.getString("cantidad") + ";_;" + rs.getString("id_solicitud"));
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error en buscarGarantiaEquipos: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return cadena;
    }

    /**
     *   Inserta la garantia de un equipo
     * @throws Exception cuando hay error
     */
    public String insertarGarantiaEquipo(BeanGeneral garantia) throws Exception {
        String respuesta = "";
        StringStatement st = null;
        String query = query = "SQL_INSERTAR_GARANTIA_EQUIPO";
        try {
            st = new StringStatement(this.obtenerSQL(query), true);//JJCastro fase2
            st.setString(1, garantia.getValor_01());//item
            st.setString(2, garantia.getValor_02());//equipo
            st.setString(3, garantia.getValor_03());//formulario
            st.setString(4, garantia.getValor_04());//garantia
            st.setString(5, garantia.getCreation_user());//usuario creacion
            st.setString(6, garantia.getValor_05());//id_solicitud

            respuesta = st.getSql();

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e);
            throw e;
        } finally {
            if (st != null) {
                try {
                    st = null;
                } catch (Exception e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
        }
        return respuesta;
    }

    /**
     *   Elimina las garatias de equipo de una solicitud
     * @throws Exception cuando hay error
     */
    public String eliminarGarantias(String id_solicitud) throws Exception {
        String respuesta = "";
        StringStatement st = null;
        String query = query = "SQL_ELIMINAR_GARANTIAS";
        try {
            st = new StringStatement(this.obtenerSQL(query), true);//JJCastro fase2
            st.setString(1, id_solicitud);//id_solicitud
            respuesta = st.getSql();

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e);
            throw e;
        } finally {
            if (st != null) {
                try {
                    st = null;
                } catch (Exception e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
        }
        return respuesta;
    }

}
