/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.opav.model.DAOS;

import java.sql.SQLException;

/**
 *
 * @author rsantamaria
 */
public interface OrdenCompraDAO {

    public String cargarBodegas(String id_solicitud, String db) throws SQLException;

    public String obtenerDireccionEntrega(String id_orden_compra) throws SQLException;

    public String cargarBodegaPrincipal(String bd);

    public String cargarInfoBodegaAsignada(String cpdigo_solicitud);

    public String cargarBodegasPorUsuario(String id_solicitud, String bd, String login);
    
    public String cargarBodegasPorUsuarioSolicitudEjecucion(String login);
    
}
