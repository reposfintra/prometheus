/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.opav.model.DAOS.impl;

import com.google.gson.JsonObject;
import com.tsp.opav.model.DAOS.MaestroProyectoDAO;
import com.tsp.opav.model.DAOS.MainDAO;
import com.tsp.operation.model.beans.StringStatement;
import com.tsp.operation.model.beans.Usuario;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author mcastillo
 */
public class MaestroProyectoImpl extends MainDAO implements MaestroProyectoDAO {

    private Connection con = null;
    private PreparedStatement ps = null;
    private ResultSet rs = null;
    private String query = "";
    private StringStatement st = null;

    public MaestroProyectoImpl(String dataBaseName) {
        super("MaestroProyectoDAO.xml", dataBaseName);
    }

    @Override
    public String getInfoWbs(String num_solicitud) {
        Gson gson = new GsonBuilder().serializeNulls().create();
        con = null;
        ps = null;
        rs = null;
        JsonObject objAreas = new JsonObject();
        JsonObject objDisciplinas = new JsonObject();
        JsonObject objCapitulos = new JsonObject();
        query = "SQL_GET_NODO_RAIZ";
        String areas_proyecto = "", disciplinas_area = "", capitulos = "";
        String respuestaJson = "{}";
        JsonArray arr = new JsonArray();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, num_solicitud);
            rs = ps.executeQuery();
            JsonObject fila = null;
            int contador = 1, indice = 1, pindex = 0;
            String padre = "";
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("id", contador);
                fila.addProperty("parent", (rs.getString("padre")));
                fila.addProperty("title", rs.getString("title"));
                fila.addProperty("groupTitle", rs.getString("grouptitle"));
                fila.addProperty("groupTitleColor", rs.getString("groupTitleColor"));
                fila.addProperty("description", rs.getString("descripcion"));
                fila.addProperty("image", "/fintra/images/botones/iconos/Project.png");
                fila.addProperty("nivel", rs.getString("nivel"));
                fila.addProperty("recordId", rs.getString("id_solicitud"));
                fila.addProperty("otro", "");
                fila.addProperty("buttonsType", "Proyecto");
                arr.add(fila);
                contador++;
            }
            /*Obtenemos areas*/
            query = this.obtenerSQL("SQL_GET_AREAS_PROYECTO");
            ps = con.prepareStatement(query);
            ps.setString(1, num_solicitud);
            rs = ps.executeQuery();
            indice = 1;
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("id", contador);
                fila.addProperty("parent", (rs.getString("padre")));
                fila.addProperty("title", rs.getString("title") + " " + indice);
                fila.addProperty("groupTitle", rs.getString("grouptitle"));
                fila.addProperty("groupTitleColor", rs.getString("groupTitleColor"));
                fila.addProperty("description", rs.getString("descripcion"));
                fila.addProperty("image", "/fintra/images/botones/iconos/Work_area.png");
                fila.addProperty("nivel", rs.getString("nivel"));
                fila.addProperty("recordId", rs.getString("id_area_proy"));
                fila.addProperty("otro", "");
                fila.addProperty("buttonsType", "Area");
                arr.add(fila);
                objAreas.addProperty(rs.getString("id_area_proy"), contador);
                areas_proyecto += rs.getInt("id_area_proy") + ",";
                contador++;
                indice++;
            }
            if (!areas_proyecto.equals("")) {
                /*Obtenemos disciplinas*/
                query = this.obtenerSQL("SQL_GET_DISCIPLINAS_AREA").replaceAll("#param", areas_proyecto.substring(0, areas_proyecto.length() - 1));
                ps = con.prepareStatement(query);
                //ps.setString(1, areas_proyecto.substring(0, areas_proyecto.length()-1));
                rs = ps.executeQuery();
                indice = 1;
                padre = "";

                while (rs.next()) {
                    if (!padre.equals(objAreas.get(rs.getString("id_area_proyecto")).getAsString())) {
                        padre = objAreas.get(rs.getString("id_area_proyecto")).getAsString();
                        pindex = ++pindex;
                        indice = 1;
                    }
                    fila = new JsonObject();
                    fila.addProperty("id", contador);
                    fila.addProperty("parent", (objAreas.get(rs.getString("id_area_proyecto")).getAsString()));
                    fila.addProperty("title", rs.getString("title") + " " + pindex + "." + indice);
                    fila.addProperty("groupTitle", rs.getString("grouptitle"));
                    fila.addProperty("groupTitleColor", rs.getString("groupTitleColor"));
                    fila.addProperty("description", rs.getString("descripcion"));
                    fila.addProperty("image", "/fintra/images/botones/iconos/System.png");
                    fila.addProperty("nivel", rs.getString("nivel"));
                    fila.addProperty("recordId", rs.getString("id_disciplina_area"));
                    fila.addProperty("otro", rs.getString("id_disciplina"));
                    fila.addProperty("buttonsType", "Disciplina");
                    arr.add(fila);
                    objDisciplinas.addProperty(rs.getString("id_disciplina_area"), contador);
                    objDisciplinas.addProperty(rs.getString("id_disciplina_area") + "_title", pindex + "." + indice);
                    disciplinas_area += rs.getInt("id_disciplina_area") + ",";
                    contador++;
                    indice++;
                }
            }
            if (!disciplinas_area.equals("")) {
                /*Obtenemos capitulos*/
                query = this.obtenerSQL("SQL_GET_CAPITULOS_DISCIPLINA").replaceAll("#param", disciplinas_area.substring(0, disciplinas_area.length() - 1));
                ps = con.prepareStatement(query);
                rs = ps.executeQuery();
                indice = 1;
                while (rs.next()) {
                    if (!padre.equals(objDisciplinas.get(rs.getString("id_disciplina_area")).getAsString())) {
                        padre = objDisciplinas.get(rs.getString("id_disciplina_area")).getAsString();
                        indice = 1;
                    }
                    fila = new JsonObject();
                    fila.addProperty("id", contador);
                    fila.addProperty("parent", (objDisciplinas.get(rs.getString("id_disciplina_area")).getAsString()));
                    fila.addProperty("title", rs.getString("title") + " " + objDisciplinas.get(rs.getString("id_disciplina_area") + "_title").getAsString() + "." + indice);
                    fila.addProperty("groupTitle", rs.getString("grouptitle"));
                    fila.addProperty("groupTitleColor", rs.getString("groupTitleColor"));
                    fila.addProperty("description", rs.getString("descripcion"));
                    fila.addProperty("image", "/fintra/images/botones/iconos/chapter.png");
                    fila.addProperty("nivel", rs.getString("nivel"));
                    fila.addProperty("recordId", rs.getString("id_capitulo"));
                    fila.addProperty("otro", "");
                    fila.addProperty("buttonsType", "Capitulo");
                    arr.add(fila);
                    objCapitulos.addProperty(rs.getString("id_capitulo"), contador);
                    capitulos += rs.getInt("id_capitulo") + ",";
                    contador++;
                    indice++;
                }
            }
            /*Obtenemos actividades*/
            /* query = this.obtenerSQL("SQL_GET_ACTIVIDADES_CAPITULO").replaceAll("#param",  capitulos.substring(0, capitulos.length()-1));
             ps = con.prepareStatement(query);
             rs = ps.executeQuery();          
             while (rs.next()) {
             fila = new JsonObject();
             fila.addProperty("id", contador);               
             fila.addProperty("parent", (objCapitulos.get(rs.getString("id_capitulo")).getAsString()));
             fila.addProperty("title", rs.getString("title"));
             fila.addProperty("description", rs.getString("descripcion"));
             fila.addProperty("image", "/fintra/images/botones/iconos/activity.png");  
             fila.addProperty("nivel", rs.getString("nivel"));  
             fila.addProperty("recordId", rs.getString("id_actividad"));
             fila.addProperty("otro", "");
             arr.add(fila);                  
             contador++;
             }     */
            respuestaJson = "{\"total\":\"" + contador + "\",\"nodes\":" + gson.toJson(arr) + "}";
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return respuestaJson;
        }
    }

    @Override
    public String cargarAreas() {
        con = null;
        rs = null;
        ps = null;

        query = "SQL_OBTENER_AREAS";
        String respuestaJson = "{}";

        Gson gson = new Gson();
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query).replaceAll("#filtro", "");

            ps = con.prepareStatement(query);

            rs = ps.executeQuery();
            JsonObject fila = new JsonObject();
            while (rs.next()) {
                fila.addProperty(rs.getString("id"), rs.getString("nombre"));
            }
            respuestaJson = gson.toJson(fila);

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return respuestaJson;
    }

    @Override
    public String cargarAreas(String q) {

        con = null;
        ps = null;
        rs = null;
        query = "SQL_OBTENER_AREAS";
        JsonArray lista = null;
        String filtro = " WHERE reg_status = '' AND nombre ilike '" + q + "%'";
        try {
            con = this.conectarJNDI();
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query).replaceAll("#filtro", filtro));
                rs = ps.executeQuery();
                lista = new JsonArray();
                JsonObject fila;
                while (rs.next()) {
                    fila = new JsonObject();
                    fila.addProperty("value", rs.getString("id"));
                    fila.addProperty("label", rs.getString("nombre"));
                    lista.add(fila);
                }
            }
        } catch (Exception e) {
            lista = new JsonArray();
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return new Gson().toJson(lista);
        }
    }

    @Override
    public String cargarDisciplinas() {
        con = null;
        rs = null;
        ps = null;

        query = "SQL_OBTENER_DISCIPLINAS";
        String respuestaJson = "{}";
        String filtro = " WHERE reg_status = ''";

        Gson gson = new Gson();
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query).replaceAll("#filtro", filtro);

            ps = con.prepareStatement(query);

            rs = ps.executeQuery();
            JsonObject fila = new JsonObject();
            while (rs.next()) {
                fila.addProperty(rs.getString("id"), rs.getString("nombre"));
            }
            respuestaJson = gson.toJson(fila);

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return respuestaJson;
    }

    @Override
    public String cargarActividades(String q) {

        con = null;
        ps = null;
        rs = null;
        query = "SQL_OBTENER_ACTIVIDADES";
        JsonArray lista = null;
        String filtro = " WHERE reg_status = '' AND descripcion ilike '" + q + "%'";
        try {
            con = this.conectarJNDI();
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query).replaceAll("#filtro", filtro));
                rs = ps.executeQuery();
                lista = new JsonArray();
                JsonObject fila;
                while (rs.next()) {
                    fila = new JsonObject();
                    fila.addProperty("value", rs.getString("id"));
                    fila.addProperty("label", rs.getString("descripcion"));
                    lista.add(fila);
                }
            }
        } catch (Exception e) {
            lista = new JsonArray();
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return new Gson().toJson(lista);
        }
    }

    @Override
    public String insertarAreaProyecto(String num_solicitud, String nombre_area, Usuario usuario) {
        con = null;
        ps = null;
        query = "SQL_INSERT_AREA_PROYECTO";
        String respuestaJson = "{}";
        int resp = 0, lastId = 0, total = 0;
        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, num_solicitud);
            ps.setString(2, nombre_area);
            ps.setString(3, usuario.getLogin());
            resp = ps.executeUpdate();
            if (resp > 0) {
                rs = ps.getGeneratedKeys();
                if (rs.next()) {
                    lastId = rs.getInt(1);
                }
            }
            respuestaJson = "{\"respuesta\":\"OK\",\"lastRecordId\":\"" + lastId + "\"}";

        } catch (Exception e) {
            try {
                respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR insertarAreaProyecto \n" + e.getMessage());
            } catch (SQLException ex) {

            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    @Override
    public String actualizarAreaProyecto(String id_area_proyecto, String nombre_area, Usuario usuario) {
        con = null;
        ps = null;
        query = "SQL_UPDATE_AREA_PROYECTO";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, nombre_area);
            ps.setString(2, usuario.getLogin());
            ps.setInt(3, Integer.parseInt(id_area_proyecto));
            ps.executeUpdate();

            respuestaJson = "{\"respuesta\":\"OK\",\"lastRecordId\":\"" + id_area_proyecto + "\"}";

        } catch (Exception e) {
            try {
                respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR actualizarAreaProyecto \n" + e.getMessage());
            } catch (SQLException ex) {

            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    @Override
    public String insertarDisciplinaArea(String id_area_proyecto, String id_disciplina, Usuario usuario) {
        con = null;
        ps = null;
        query = "SQL_INSERT_DISCIPLINA_AREA";
        String respuestaJson = "{}";
        int resp = 0, lastId = 0;

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            ps.setInt(1, Integer.parseInt(id_area_proyecto));
            ps.setInt(2, Integer.parseInt(id_disciplina));
            ps.setString(3, usuario.getLogin());

            resp = ps.executeUpdate();
            if (resp > 0) {
                rs = ps.getGeneratedKeys();
                if (rs.next()) {
                    lastId = rs.getInt(1);
                }
            }
            respuestaJson = "{\"respuesta\":\"OK\",\"lastRecordId\":\"" + lastId + "\"}";

        } catch (Exception e) {
            try {
                respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR insertarDisciplinaArea \n" + e.getMessage());
            } catch (SQLException ex) {

            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    @Override
    public String actualizarDisciplinaArea(String id_disciplina_area, String id_disciplina, Usuario usuario) {
        con = null;
        ps = null;
        query = "SQL_UPDATE_DISCIPLINA_AREA";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setInt(1, Integer.parseInt(id_disciplina));
            ps.setString(2, usuario.getLogin());
            ps.setInt(3, Integer.parseInt(id_disciplina_area));
            ps.executeUpdate();

            respuestaJson = "{\"respuesta\":\"OK\",\"lastRecordId\":\"" + id_disciplina_area + "\"}";

        } catch (Exception e) {
            try {
                respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR actualizarDisciplinaArea \n" + e.getMessage());
            } catch (SQLException ex) {

            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    @Override
    public String insertarCapitulo(String id_disciplina, String descripcion, Usuario usuario) {
        con = null;
        ps = null;
        query = "SQL_INSERT_CAPITULO";
        String respuestaJson = "{}";
        int resp = 0, lastId = 0;

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            ps.setInt(1, Integer.parseInt(id_disciplina));
            ps.setString(2, descripcion);
            ps.setString(3, usuario.getLogin());

            resp = ps.executeUpdate();
            if (resp > 0) {
                rs = ps.getGeneratedKeys();
                if (rs.next()) {
                    lastId = rs.getInt(1);
                }
            }
            respuestaJson = "{\"respuesta\":\"OK\",\"lastRecordId\":\"" + lastId + "\"}";

        } catch (Exception e) {
            try {
                respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR insertarCapitulo \n" + e.getMessage());
            } catch (SQLException ex) {

            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    @Override
    public String actualizarCapitulo(String id_capitulo, String descripcion, Usuario usuario) {
        con = null;
        ps = null;
        query = "SQL_UPDATE_CAPITULO";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, descripcion);
            ps.setString(2, usuario.getLogin());
            ps.setInt(3, Integer.parseInt(id_capitulo));
            ps.executeUpdate();

            respuestaJson = "{\"respuesta\":\"OK\",\"lastRecordId\":\"" + id_capitulo + "\"}";

        } catch (Exception e) {
            try {
                respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR actualizarCapitulo \n" + e.getMessage());
            } catch (SQLException ex) {

            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    @Override
    public String insertarActividadCapitulo(String id_capitulo, String id_actividad, Usuario usuario) {
        con = null;
        ps = null;
        query = "SQL_INSERT_ACTIVIDAD_CAPITULO";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setInt(1, Integer.parseInt(id_capitulo));
            ps.setInt(2, Integer.parseInt(id_actividad));
            ps.setString(3, usuario.getLogin());

            ps.executeUpdate();

            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            try {
                respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR insertarActividadCapitulo \n" + e.getMessage());
            } catch (SQLException ex) {

            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    @Override
    public String actualizarActividad(String id_actividad, String nombre, String descripcion, Usuario usuario) {
        con = null;
        ps = null;
        query = "SQL_UPDATE_ACTIVIDAD";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, nombre);
            ps.setString(2, descripcion);
            ps.setString(3, usuario.getLogin());
            ps.setInt(4, Integer.parseInt(id_actividad));
            ps.executeUpdate();

            respuestaJson = "{\"respuesta\":\"OK\",\"lastRecordId\":\"" + id_actividad + "\"}";

        } catch (Exception e) {
            try {
                respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR actualizarActividad \n" + e.getMessage());
            } catch (SQLException ex) {

            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    @Override
    public String eliminarItem(String id, String consulta) {
        con = null;
        ps = null;
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(consulta);
            ps = con.prepareStatement(query);
            ps.setInt(1, Integer.parseInt(id));

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            try {
                respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR eliminarItem \n" + e.getMessage());
            } catch (SQLException ex) {

            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    @Override
    public String cargarGridActividades(String id_capitulo) {
        Gson gson = new Gson();
        con = null;
        ps = null;
        rs = null;
        JsonObject obj = new JsonObject();

        query = "SQL_GET_ACTIVIDADES_CAPITULO";
        String filtro = " WHERE reg_status = ''";

        JsonArray arr = new JsonArray();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query).replaceAll("#param", id_capitulo));
            rs = ps.executeQuery();
            JsonObject fila = null;
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("id", rs.getString("id"));
                fila.addProperty("id_actividad", rs.getString("id_actividad"));
                fila.addProperty("descripcion", rs.getString("descripcion"));
                fila.addProperty("valor_act", rs.getString("valor_act"));
                arr.add(fila);
            }
            obj.add("rows", arr);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return gson.toJson(obj);
        }
    }

    @Override
    public String cargarGridAPUsActividades(String id_actividad) {
        Gson gson = new Gson();
        con = null;
        ps = null;
        rs = null;
        JsonObject obj = new JsonObject();

        query = "SQL_GET_APUS_ACTIVIDADES";

        JsonArray arr = new JsonArray();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setInt(1, Integer.parseInt(id_actividad));
            rs = ps.executeQuery();
            JsonObject fila = null;
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("id", rs.getString("id"));
                fila.addProperty("id_actividad", rs.getString("id_actividad_capitulo"));
                fila.addProperty("id_apu", rs.getString("id_apu"));
                fila.addProperty("descripcion", rs.getString("nombre"));
                arr.add(fila);
            }
            obj.add("rows", arr);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return gson.toJson(obj);
        }
    }

    @Override
    public boolean existeActividad(String nombre, String idActividad) {
        con = null;
        ps = null;
        rs = null;
        boolean resp = false;
        query = "SQL_OBTENER_ACTIVIDADES";
        String filtro = " WHERE descripcion = '" + nombre + "'";
        filtro += (!idActividad.equals("0")) ? " and id not in(" + idActividad + ")" : "";
        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query).replaceAll("#filtro", filtro);

            ps = con.prepareStatement(query);

            rs = ps.executeQuery();

            if (rs.next()) {
                resp = true;
            }
        } catch (Exception e) {
            try {
                resp = false;
                throw new SQLException("ERROR existeActividad \n" + e.getMessage());
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
        }
        return resp;
    }

    @Override
    public String insertarActividad(String descripcion, Usuario usuario) {
        con = null;
        ps = null;
        query = "SQL_INSERT_ACTIVIDAD";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, descripcion);
            ps.setString(2, usuario.getLogin());
            ps.executeUpdate();

            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            try {
                respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR insertarActividad \n" + e.getMessage());
            } catch (SQLException ex) {

            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    @Override
    public String cargarGridActividades() {
        Gson gson = new Gson();
        con = null;
        ps = null;
        rs = null;
        JsonObject obj = new JsonObject();

        query = "SQL_OBTENER_ACTIVIDADES";
        String filtro = "";

        JsonArray arr = new JsonArray();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query).replaceAll("#filtro", filtro));
            rs = ps.executeQuery();
            JsonObject fila = null;
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("id", rs.getString("id"));
                fila.addProperty("descripcion", rs.getString("descripcion"));
                fila.addProperty("reg_status", rs.getString("reg_status"));
                fila.addProperty("cambio", rs.getString("cambio"));
                arr.add(fila);
            }
            obj.add("rows", arr);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return gson.toJson(obj);
        }
    }

    @Override
    public String actualizarActividad(String id, String descripcion, Usuario usuario) {
        con = null;
        ps = null;
        query = "SQL_UPDATE_ACTIVIDAD";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, descripcion);
            ps.setString(2, usuario.getLogin());
            ps.setInt(3, Integer.parseInt(id));
            ps.executeUpdate();

            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            try {
                respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR actualizarActividad \n" + e.getMessage());
            } catch (SQLException ex) {

            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    @Override
    public String activaInactivaActividad(String id, Usuario usuario) {
        con = null;
        ps = null;
        query = "activarInactivarActividad";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, usuario.getLogin());
            ps.setInt(2, Integer.parseInt(id));

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
            throw new SQLException("ERROR activaInactivaActividad \n" + e.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    @Override
    public boolean existeArea(String nombre, String idArea) {
        con = null;
        ps = null;
        rs = null;
        boolean resp = false;
        query = "SQL_OBTENER_AREAS";
        String filtro = " WHERE nombre = '" + nombre + "'";
        filtro += (!idArea.equals("0")) ? " and id not in(" + idArea + ")" : "";
        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query).replaceAll("#filtro", filtro);

            ps = con.prepareStatement(query);

            rs = ps.executeQuery();

            if (rs.next()) {
                resp = true;
            }
        } catch (Exception e) {
            try {
                resp = false;
                throw new SQLException("ERROR existeArea \n" + e.getMessage());
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
        }
        return resp;
    }

    @Override
    public String cargarGridAreas() {
        Gson gson = new Gson();
        con = null;
        ps = null;
        rs = null;
        JsonObject obj = new JsonObject();

        query = "SQL_OBTENER_AREAS";
        String filtro = "";

        JsonArray arr = new JsonArray();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query).replaceAll("#filtro", filtro));
            rs = ps.executeQuery();
            JsonObject fila = null;
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("id", rs.getString("id"));
                fila.addProperty("nombre", rs.getString("nombre"));
                fila.addProperty("reg_status", rs.getString("reg_status"));
                fila.addProperty("cambio", rs.getString("cambio"));
                arr.add(fila);
            }
            obj.add("rows", arr);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return gson.toJson(obj);
        }
    }

    @Override
    public String insertarArea(String nombre, Usuario usuario) {
        con = null;
        ps = null;
        query = "SQL_INSERT_AREA";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, nombre);
            ps.setString(2, usuario.getLogin());
            ps.executeUpdate();

            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            try {
                respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR insertarArea \n" + e.getMessage());
            } catch (SQLException ex) {

            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    @Override
    public String actualizarArea(String id, String nombre, Usuario usuario) {
        con = null;
        ps = null;
        query = "SQL_UPDATE_AREA";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, nombre);
            ps.setString(2, usuario.getLogin());
            ps.setInt(3, Integer.parseInt(id));
            ps.executeUpdate();

            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            try {
                respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR actualizarArea \n" + e.getMessage());
            } catch (SQLException ex) {

            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    @Override
    public String activaInactivaArea(String id, Usuario usuario) {
        con = null;
        ps = null;
        query = "activarInactivarArea";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, usuario.getLogin());
            ps.setInt(2, Integer.parseInt(id));

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
            throw new SQLException("ERROR activaInactivaArea \n" + e.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    @Override
    public boolean existeDisciplina(String nombre, String idDisciplina) {
        con = null;
        ps = null;
        rs = null;
        boolean resp = false;
        query = "SQL_OBTENER_DISCIPLINAS";
        String filtro = " WHERE nombre = '" + nombre + "'";
        filtro += (!idDisciplina.equals("0")) ? " and id not in(" + idDisciplina + ")" : "";
        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query).replaceAll("#filtro", filtro);

            ps = con.prepareStatement(query);

            rs = ps.executeQuery();

            if (rs.next()) {
                resp = true;
            }
        } catch (Exception e) {
            try {
                resp = false;
                throw new SQLException("ERROR existeDisciplina \n" + e.getMessage());
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
        }
        return resp;

    }

    @Override
    public String cargarGridDisciplinas() {
        Gson gson = new Gson();
        con = null;
        ps = null;
        rs = null;
        JsonObject obj = new JsonObject();

        query = "SQL_OBTENER_DISCIPLINAS";
        String filtro = "";

        JsonArray arr = new JsonArray();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query).replaceAll("#filtro", filtro));
            rs = ps.executeQuery();
            JsonObject fila = null;
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("id", rs.getString("id"));
                fila.addProperty("nombre", rs.getString("nombre"));
                fila.addProperty("reg_status", rs.getString("reg_status"));
                fila.addProperty("cambio", rs.getString("cambio"));
                arr.add(fila);
            }
            obj.add("rows", arr);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return gson.toJson(obj);
        }
    }

    @Override
    public String insertarDisciplina(String nombre, Usuario usuario) {
        con = null;
        ps = null;
        query = "SQL_INSERT_DISCIPLINA";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, nombre);
            ps.setString(2, usuario.getLogin());
            ps.executeUpdate();

            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            try {
                respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR insertarDisciplina \n" + e.getMessage());
            } catch (SQLException ex) {

            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    @Override
    public String actualizarDisciplina(String id, String nombre, Usuario usuario) {
        con = null;
        ps = null;
        query = "SQL_UPDATE_DISCIPLINA";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, nombre);
            ps.setString(2, usuario.getLogin());
            ps.setInt(3, Integer.parseInt(id));
            ps.executeUpdate();

            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            try {
                respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR actualizarDisciplina \n" + e.getMessage());
            } catch (SQLException ex) {

            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    @Override
    public String activaInactivaDisciplina(String id, Usuario usuario) {
        con = null;
        ps = null;
        query = "activarInactivarDisciplina";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, usuario.getLogin());
            ps.setInt(2, Integer.parseInt(id));

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
            throw new SQLException("ERROR activaInactivaDisciplina \n" + e.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    @Override
    public String cargarAPUXGrupo(String grupo_apu, String id) {
        Gson gson = new Gson();
        JsonObject respuesta = new JsonObject();
        JsonArray arr = new JsonArray();
        JsonObject object = new JsonObject();;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = "";
        String query = "ConsultarAPUXGrupo";

        try {
            con = this.conectarJNDI(query);
            sql = this.obtenerSQL(query);

            sql = sql.replace("#filtro3", "");

            if (grupo_apu.equals("0")) {
                sql = sql.replace("#filtro1", " and tipo_apu='MASTER' ");
            } else {
                sql = sql.replace("#filtro1", " and c.id_grupo_apu = " + grupo_apu + " and tipo_apu='MASTER' ");
            }

            if (!id.equals("0")) {
                sql = sql.replace("#filtro2", " and a.id not in(select id_apu from opav.sl_rel_actividades_apu where id_actividad_capitulo=" + id + ")  order by a.nombre;");
            }

            ps = con.prepareStatement(sql);

            rs = ps.executeQuery();

            while (rs.next()) {
                object = new JsonObject();
                object.addProperty("id", rs.getString("id"));
                object.addProperty("nombreapu", rs.getString("nombre"));
                object.addProperty("nombre_unidad", rs.getString("nombre_unidad"));
                object.addProperty("idgrupo", rs.getString("id_grupo_apu"));
                object.addProperty("idunidad", rs.getString("id_unidad_medida"));
                arr.add(object);
            }

            System.out.println(new Gson().toJson(arr));
            respuesta.add("rows", arr);
        } catch (Exception exc) {
            respuesta = new JsonObject();
            respuesta.addProperty("error", exc.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception ex) {
            }
            return gson.toJson(respuesta);
        }
    }

    @Override
    public String cargarAPUAsoc(String id) {
        Gson gson = new Gson();
        JsonObject respuesta = new JsonObject();
        JsonArray arr = new JsonArray();
        JsonObject object = new JsonObject();;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = "";
        String query = "ConsultarAPUXGrupo";

        try {
            con = this.conectarJNDI(query);
            sql = this.obtenerSQL(query);

            sql = sql.replace("#filtro1", "");

            sql = sql.replace("#filtro2", " and a.id in(select id_apu from opav.sl_rel_actividades_apu where reg_status!='A' and id_actividad_capitulo=" + id + ") order by posicion");

            sql = sql.replace("#filtro3", " ,(select cantidad from opav.sl_rel_actividades_apu where reg_status!='A' and id_actividad_capitulo=" + id + " and id_apu=a.id) as cantidad,"
                    + " (select estado from opav.sl_rel_actividades_apu where reg_status!='A' and id_actividad_capitulo=" + id + " and id_apu=a.id) as estado ,"
                    + " (select posicion from opav.sl_rel_actividades_apu where reg_status!='A' and id_actividad_capitulo=" + id + " and id_apu=a.id) as posicion ");

            ps = con.prepareStatement(sql);

                rs = ps.executeQuery();

            while (rs.next()) {
                object = new JsonObject();
                object.addProperty("id", rs.getString("id"));
                object.addProperty("nombreapu", rs.getString("nombre"));
                object.addProperty("nombre_unidad", rs.getString("nombre_unidad"));
                object.addProperty("cantidad", rs.getString("cantidad"));
                object.addProperty("estado", rs.getString("estado"));
                object.addProperty("tipo_apu", rs.getString("tipo_apu"));
                object.addProperty("idgrupo", rs.getString("id_grupo_apu"));
                object.addProperty("idunidad", rs.getString("id_unidad_medida"));
                object.addProperty("posicion", rs.getString("posicion"));
                arr.add(object);
            }

            System.out.println(new Gson().toJson(arr));
            respuesta.add("rows", arr);
        } catch (Exception exc) {
            respuesta = new JsonObject();
            respuesta.addProperty("error", exc.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception ex) {
            }
            return gson.toJson(respuesta);
        }
    }

    //////////////////////////////////////////////////////////////////////
    /*public int crearApuEspejo(String listado, Usuario usuario) {
     con = null;
     ps = null;
     String mensaje = "";
     int resp = 0, idCab = 0;
     Statement stmt = null;
     //query = "GuardarEncabezadoInsumo";

     try {

     PreparedStatement ps = null;
     con = this.conectarJNDI();

     String query = "InsertarEspejoApu";
     con.setAutoCommit(false);

     ps = con.prepareStatement(this.obtenerSQL(query), Statement.RETURN_GENERATED_KEYS);

     ps.setString(1, usuario.getLogin());
     ps.setInt(2, Integer.parseInt(listado));

     resp = ps.executeUpdate();
     stmt = con.createStatement();
     if (resp > 0) {
     ResultSet rs = ps.getGeneratedKeys();
     if (rs.next()) {
     idCab = rs.getInt(1);

     stmt.addBatch("insert into opav.sl_apu_det(reg_status, dstrct, id_apu, id_insumo, id_unidad_medida, id_tipo_insumo, cantidad, rendimiento, last_update, user_update, creation_date, creation_user) \n"
     + "select \n"
     + "reg_status,\n"
     + "dstrct,\n"
     + idCab
     + ",id_insumo,\n"
     + "id_unidad_medida,\n"
     + "id_tipo_insumo,\n"
     + "cantidad,\n"
     + "rendimiento,\n"
     + "now(),\n"
     + "'',\n"
     + "now(), '"+ usuario.getLogin()+"' from \n"
     + "opav.sl_apu_det\n"
     + "where \n"
     + "id_apu=" + listado + ";");

     stmt.addBatch("insert into opav.sl_rel_grupo_apu(reg_status, dstrct, id_apu, id_grupo_apu, last_update,user_update,creation_date, creation_user)\n"
     + "select reg_status, dstrct, "+idCab+", id_grupo_apu, now(),'',now(), '"+usuario.getLogin()+"' from opav.sl_rel_grupo_apu where id_apu="+listado);

     stmt.executeBatch();
     stmt.clearBatch();
     }
     }

     con.commit();

     } catch (Exception exc) {
     con.rollback();
     exc.getMessage();
     } finally {
     try {
     if (ps != null) {
     try {
     ps.close();
     } catch (SQLException e) {
     throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
     }
     }
     if (con != null) {
     try {
     con.setAutoCommit(true);
     this.desconectar(con);
     } catch (SQLException e) {
     throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage());
     }
     }
     } catch (Exception ex) {
     }
     return idCab;
     }

     }*/
    ///////////////////////////////////////////////////////////////////////
    @Override
    public String insertarRelAPUActividad(int id, String listado, Usuario usuario) {
        st = null;
        String sql = "";
        query = "insertarRelAPUActividad";

        try {
            query = this.obtenerSQL(query);

            st = new StringStatement(query, true);
            st.setString(1, usuario.getDstrct());
            st.setInt(2, id);
            st.setInt(3, Integer.parseInt(listado));
            st.setString(4, usuario.getLogin());

            sql = st.getSql();

        } catch (Exception e) {
            try {
                throw new SQLException("ERROR insertarRelAPUActividad \n" + e.getMessage());
            } catch (SQLException ex) {
            }
        } finally {
        }
        return sql;
    }

    @Override
    public String EliminarRelAPUActividad(int id, String listado) {
        st = null;
        String sql = "";
        query = "EliminarRelAPUActividad";
        try {
            //query = this.obtenerSQL(query).replace("#filtro", listado);
            query = this.obtenerSQL(query);

            st = new StringStatement(query, true);
            st.setInt(1, id);
            st.setInt(2, Integer.parseInt(listado));

            sql = st.getSql();

        } catch (Exception e) {
            try {
                throw new SQLException("ERROR EliminarRelAPUActividad \n" + e.getMessage());
            } catch (SQLException ex) {
            }
        } finally {
        }
        return sql;
    }

    @Override
    public String listarTipoActores(String mostrarTodos) {
        con = null;
        rs = null;
        ps = null;

        query = "SQL_GET_TIPO_ACTORES";
        String respuestaJson = "{}";
        String filtro = (mostrarTodos.equals("S")) ? "" : " WHERE reg_status = ''";
        JsonObject obj = new JsonObject();
        Gson gson = new Gson();
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query).replaceAll("#filtro", filtro);

            ps = con.prepareStatement(query);

            rs = ps.executeQuery();
            JsonObject fila = null;
            JsonArray arr = new JsonArray();
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("id", rs.getString("id"));
                fila.addProperty("descripcion", rs.getString("descripcion"));
                fila.addProperty("reg_status", rs.getString("reg_status"));
                fila.addProperty("cambio", rs.getString("cambio"));
                arr.add(fila);
            }
            obj.add("rows", arr);
            respuestaJson = gson.toJson(obj);

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return respuestaJson;

    }

    @Override
    public String guardarTipoActor(String descripcion, Usuario usuario) {
        con = null;
        ps = null;
        query = "SQL_INSERT_TIPO_ACTOR";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, usuario.getDstrct());
            ps.setString(2, descripcion);
            ps.setString(3, usuario.getLogin());

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
            throw new SQLException("ERROR guardarTipoActor \n" + e.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    @Override
    public String actualizarTipoActor(String id, String descripcion, Usuario usuario) {
        con = null;
        ps = null;
        query = "SQL_UPDATE_TIPO_ACTOR";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, descripcion);
            ps.setString(2, usuario.getLogin());
            ps.setInt(3, Integer.parseInt(id));

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
            throw new SQLException("ERROR actualizarTipoActor \n" + e.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    @Override
    public String activaInactivaTipoActor(String id, Usuario usuario) {
        con = null;
        ps = null;
        query = "activarInactivarTipoActor";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, usuario.getLogin());
            ps.setInt(2, Integer.parseInt(id));

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
            throw new SQLException("ERROR activaInactivaTipoActor \n" + e.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    @Override
    public String cargarCboTipoActores() {
        con = null;
        rs = null;
        ps = null;

        query = "SQL_GET_TIPO_ACTORES";
        String respuestaJson = "{}";
        String filtro = " WHERE reg_status = ''";

        Gson gson = new Gson();
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query).replaceAll("#filtro", filtro);

            ps = con.prepareStatement(query);

            rs = ps.executeQuery();
            JsonObject fila = new JsonObject();
            while (rs.next()) {
                fila.addProperty(rs.getString("id"), rs.getString("descripcion"));
            }
            respuestaJson = gson.toJson(fila);

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return respuestaJson;

    }

    @Override
    public String cargarTiposIdentificacion() {
        con = null;
        rs = null;
        ps = null;

        query = "SQL_CARGAR_TIPOS_IDENTIFICACION";
        String respuestaJson = "{}";

        Gson gson = new Gson();
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);

            rs = ps.executeQuery();
            JsonObject fila = new JsonObject();
            while (rs.next()) {
                fila.addProperty(rs.getString("table_code"), rs.getString("referencia"));
            }
            respuestaJson = gson.toJson(fila);

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return respuestaJson;
    }

    @Override
    public String cargarGridActores(String mostrarTodos) {
        con = null;
        rs = null;
        ps = null;

        query = "SQL_GET_ACTORES";
        String respuestaJson = "{}";
        String filtro = (mostrarTodos.equals("S")) ? "" : " WHERE act.reg_status = ''";
        JsonObject obj = new JsonObject();
        Gson gson = new Gson();
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query).replaceAll("#filtro", filtro);

            ps = con.prepareStatement(query);

            rs = ps.executeQuery();
            JsonObject fila = null;
            JsonArray arr = new JsonArray();
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("id", rs.getString("id"));
                fila.addProperty("id_tipo_actor", rs.getInt("id_tipo_actor"));
                fila.addProperty("tipo_actor", rs.getString("tipo_actor"));
                fila.addProperty("tipo_documento", rs.getString("tipo_documento"));
                fila.addProperty("documento", rs.getString("documento"));
                fila.addProperty("nombre", rs.getString("nombre"));
                fila.addProperty("codciu", rs.getString("codciu"));
                fila.addProperty("coddpto", rs.getString("coddpto"));
                fila.addProperty("codpais", rs.getString("codpais"));
                fila.addProperty("direccion", rs.getString("direccion"));
                fila.addProperty("telefono", rs.getString("telefono"));
                fila.addProperty("tel_extension", rs.getString("tel_extension"));
                fila.addProperty("celular", rs.getString("celular"));
                fila.addProperty("email", rs.getString("email"));
                fila.addProperty("tarjeta_profesional", rs.getString("tarjeta_profesional"));
                fila.addProperty("doc_lugar_exped", rs.getString("doc_lugar_exped"));
                fila.addProperty("reg_status", rs.getString("reg_status"));
                fila.addProperty("cambio", rs.getString("cambio"));
                arr.add(fila);
            }
            obj.add("rows", arr);
            respuestaJson = gson.toJson(obj);

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return respuestaJson;

    }

    @Override
    public String guardarActor(int tipoActor, String tipoDoc, String documento, String nombre, String ciudad, String dpto, String pais, String direccion, String telefono, String extension, String celular, String email, String tarjeta_prof, String lugarExpCed, Usuario usuario) {
        con = null;
        ps = null;
        query = "insertarActor";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, usuario.getDstrct());
            ps.setInt(2, tipoActor);
            ps.setString(3, tipoDoc);
            ps.setString(4, documento);
            ps.setString(5, nombre);
            ps.setString(6, ciudad);
            ps.setString(7, dpto);
            ps.setString(8, pais);
            ps.setString(9, direccion);
            ps.setString(10, telefono);
            ps.setString(11, extension);
            ps.setString(12, celular);
            ps.setString(13, email);
            ps.setString(14, tarjeta_prof);
            ps.setString(15, lugarExpCed);
            ps.setString(16, usuario.getLogin());

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
            throw new SQLException("ERROR guardarActor \n" + e.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    @Override
    public String actualizarActor(int id, int tipoActor, String tipoDoc, String documento, String nombre, String ciudad, String dpto, String pais, String direccion, String telefono, String extension, String celular, String email, String tarjeta_prof, String lugarExpCed, Usuario usuario) {
        con = null;
        ps = null;
        query = "actualizarActor";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setInt(1, tipoActor);
            ps.setString(2, tipoDoc);
            ps.setString(3, documento);
            ps.setString(4, nombre);
            ps.setString(5, ciudad);
            ps.setString(6, dpto);
            ps.setString(7, pais);
            ps.setString(8, direccion);
            ps.setString(9, telefono);
            ps.setString(10, extension);
            ps.setString(11, celular);
            ps.setString(12, email);
            ps.setString(13, tarjeta_prof);
            ps.setString(14, lugarExpCed);
            ps.setString(15, usuario.getLogin());
            ps.setInt(16, id);

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
            throw new SQLException("ERROR actualizarActor \n" + e.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    @Override
    public String activaInactivaActor(String id, Usuario usuario) {
        con = null;
        ps = null;
        query = "activarInactivarActor";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, usuario.getLogin());
            ps.setInt(2, Integer.parseInt(id));

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
            throw new SQLException("ERROR activaInactivaActor \n" + e.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    public boolean existeApuAsociado(String filtro) {
        con = null;
        ps = null;
        rs = null;
        boolean resp = false;
        query = "SQL_OBTENER_APU_REL";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query).replaceAll("#filtro", filtro);

            ps = con.prepareStatement(query);

            rs = ps.executeQuery();

            if (rs.next()) {
                resp = true;
            }
        } catch (Exception e) {
            try {
                resp = false;
                throw new SQLException("ERROR existeApuAsociado \n" + e.getMessage());
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
        }
        return resp;
    }

    @Override
    public String guardarAPUEdit(JsonObject info) {

        JsonObject respuesta = new JsonObject();
        con = null;
        ps = null;
        String mensaje = "";
        Statement stmt = null;
        int resp = 0, lastId = 0;
        //query = "GuardarEncabezadoInsumo";
        String respuestaJson = "{}";
        if ((!APU_Asociado(info.get("ids").getAsInt())) || (info.get("tipo_apu").getAsString().equals("PROYECTO"))) {

            try {

                JsonArray arr = info.getAsJsonArray("rows");
                PreparedStatement ps = null;
                con = this.conectarJNDI();

                String query = "UpdateEncabezadoAPU";
                con.setAutoCommit(false);

                ps = con.prepareStatement(this.obtenerSQL(query));

                ps.setString(1, info.get("nomapu").getAsString());
                ps.setString(2, info.get("usuario").getAsString());
                ps.setInt(3, info.get("unidad_medida").getAsInt());
                ps.setInt(4, info.get("ids").getAsInt());

                ps.executeUpdate();
                stmt = con.createStatement();

                query = "UpdateGrupoAPU";

                ps = con.prepareStatement(this.obtenerSQL(query));

                ps.setString(1, info.get("usuario").getAsString());
                ps.setInt(2, info.get("grupo_apu").getAsInt());
                ps.setInt(3, info.get("ids").getAsInt());

                ps.executeUpdate();

                stmt.addBatch("delete from opav.sl_apu_det where id_apu=" + info.get("ids").getAsInt() + ";");

                for (int i = 0; i < arr.size(); i++) {
                    respuesta = arr.get(i).getAsJsonObject();

                    stmt.addBatch(insertarAPUDetalle(info.get("ids").getAsInt(), respuesta, info.get("usuario").getAsString(), info.get("dstrct").getAsString()));

                }

                stmt.executeBatch();
                stmt.clearBatch();
                mensaje = "APU actualizado exitosamente...";

                con.commit();

                respuestaJson = "{\"mensaje\":\"" + mensaje + "\"}";

            } catch (Exception exc) {
                con.rollback();
                respuesta = new JsonObject();
                exc.getMessage();
                respuestaJson = "{\"error\":\"Error\"}";
            } finally {
                try {
                    if (ps != null) {
                        try {
                            ps.close();
                        } catch (SQLException e) {
                            throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                        }
                    }
                    if (con != null) {
                        try {
                            con.setAutoCommit(true);
                            this.desconectar(con);
                        } catch (SQLException e) {
                            throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage());
                        }
                    }
                } catch (Exception ex) {
                }
                return respuestaJson;
            }
        } else {
            respuestaJson = "{\"error\":\"Este APU ya se encuentra asociado a un proyecto\"}";
        }
        return respuestaJson;
    }

    @Override
    public String guardarAPUEditEspejo(JsonObject info) {

        JsonObject respuesta = new JsonObject();
        con = null;
        ps = null;
        String mensaje = "";
        Statement stmt = null;
        int resp = 0, lastId = 0;
        //query = "GuardarEncabezadoInsumo";
        String respuestaJson = "{}";

        try {

            JsonArray arr = info.getAsJsonArray("rows");
            PreparedStatement ps = null;
            con = this.conectarJNDI();

            con.setAutoCommit(false);

            if (info.get("tipo_apu").getAsString().equals("MASTER")) {

                String query = "InsertarEspejoApu";
                ps = con.prepareStatement(this.obtenerSQL(query), Statement.RETURN_GENERATED_KEYS);

                ps.setString(1, info.get("nomapu").getAsString());
                ps.setInt(2, info.get("unidad_medida").getAsInt());
                ps.setString(3, info.get("usuario").getAsString());

                ps.setInt(4, info.get("ids").getAsInt());

                resp = ps.executeUpdate();

                stmt = con.createStatement();

                if (resp > 0) {
                    rs = ps.getGeneratedKeys();
                    if (rs.next()) {

                        lastId = rs.getInt(1);

                        for (int i = 0; i < arr.size(); i++) {
                            respuesta = arr.get(i).getAsJsonObject();

                            stmt.addBatch(insertarAPUDetalle(lastId, respuesta, info.get("usuario").getAsString(), info.get("dstrct").getAsString()));

                        }

                    }

                }

                stmt.addBatch("update opav.sl_relacion_cotizacion_detalle_apu set id_apu='" + lastId + "' where id_rel_actividades_apu=(select id from opav.sl_rel_actividades_apu where id_actividad_capitulo=' " + info.get("id_actividad").getAsString() + "' and id_apu='" + info.get("ids").getAsInt() + "') and id_apu='" + info.get("ids").getAsInt() + "';");

                stmt.addBatch("update opav.sl_rel_actividades_apu set id_apu='" + lastId + "' where id_actividad_capitulo='" + info.get("id_actividad").getAsString() + "' and id_apu='" + info.get("ids").getAsInt() + "';");

                /*stmt.addBatch("insert into opav.sl_rel_actividades_apu(reg_status, dstrct, id_actividad_capitulo, id_apu, last_update, user_update, creation_date, creation_user, cantidad) "
                 + "values('', '" + info.get("dstrct").getAsString() + "', '" + info.get("id_actividad").getAsString() + "', '" + lastId + "', now(), '', now(),"
                 + " '" + info.get("usuario").getAsString() + "', '" + info.get("cantidad").getAsString() + "');");*/
                stmt.addBatch("insert into opav.sl_rel_grupo_apu(reg_status, dstrct, id_apu, id_grupo_apu, creation_user)\n"
                        + " values('', '" + info.get("dstrct").getAsString() + "', '" + lastId + "', '" + info.get("grupo_apu").getAsString() + "', '" + info.get("usuario").getAsString() + "');");
                stmt.executeBatch();
                stmt.clearBatch();
                mensaje = "APU actualizado exitosamente..., Por favor ir a presupuesto para actualizarlo...";
                respuestaJson = "{\"mensaje\":\"" + mensaje + "\"}";

            } else {
                mensaje = guardarAPUEdit(info);
                if (mensaje.equals("{\"mensaje\":\"APU actualizado exitosamente...\"}")) {
                    mensaje = "APU actualizado exitosamente..., Por favor ir a presupuesto para actualizarlo...";
                }
                respuestaJson = "{\"mensaje\":\"" + mensaje + "\"}";
            }

            con.commit();

        } catch (Exception exc) {
            con.rollback();
            respuesta = new JsonObject();
            exc.getMessage();
            respuestaJson = "{\"error\":\"Error\"}";
        } finally {
            try {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException e) {
                        throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                    }
                }
                if (con != null) {
                    try {
                        con.setAutoCommit(true);
                        this.desconectar(con);
                    } catch (SQLException e) {
                        throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage());
                    }
                }
            } catch (Exception ex) {
            }
            return respuestaJson;
        }
    }

    public String insertarAPUDetalle(int idCab, JsonObject respuesta, String usuario, String empresa) throws SQLException {

        String cadena = "";
        String query = "InsertarDetalleAPU";
        String sql = "";
        StringStatement st = null;
        sql = this.obtenerSQL(query);
        st = new StringStatement(sql, true);

        try {

            st.setString(1, empresa);
            st.setInt(2, idCab);
            st.setInt(3, respuesta.get("iddescripcionins").getAsInt());
            st.setInt(4, respuesta.get("idunidadmedida").getAsInt());
            st.setInt(5, respuesta.get("idtipoinsumo").getAsInt());
            st.setString(6, respuesta.get("cantidad").getAsString());
            st.setString(7, respuesta.get("rendimiento").getAsString());
            st.setString(8, usuario);
            cadena += st.getSql();

        } catch (Exception e) {
            System.out.println("Error en SQL_INSERTAR_DETALLE_RECAUDO (insertarRecaudosDetalle)" + e.toString());
            e.printStackTrace();
        }
        return cadena;

    }

    @Override
    public boolean existeAPU(String nombre, String empresa, String id_actividad) {
        con = null;
        ps = null;
        rs = null;
        boolean resp = false;
        query = "SQL_EXISTE_APU";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);

            ps.setString(1, empresa);
            ps.setString(2, nombre);

            rs = ps.executeQuery();

            if (rs.next()) {
                resp = true;
            }
        } catch (Exception e) {
            try {
                resp = false;
                throw new SQLException("ERROR existeAPU \n" + e.getMessage());
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
        }
        return resp;

    }

    @Override
    public String ObtenerAccion(String id_solicitud) {

        con = null;
        ps = null;
        rs = null;
        String respuestaJson = "{}";
        query = "ObtenerAccion";

        Gson gson = new Gson();

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);

            ps.setInt(1, Integer.parseInt(id_solicitud));

            rs = ps.executeQuery();
            JsonObject fila = new JsonObject();
            if (rs.next()) {
                fila.addProperty("id_accion", rs.getString("id_accion"));
            }

            respuestaJson = gson.toJson(fila);

        } catch (Exception e) {
            try {
                respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR ObtenerAccion \n" + e.getMessage());
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
        }
        return respuestaJson;

    }

    @Override
    public String ActualizarCantidadAPU(int id_apu, int id_actividad, int cantidad) {
        con = null;
        ps = null;
        query = "UpdateCantRelApuAct";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setInt(1, cantidad);
            ps.setInt(2, id_actividad);
            ps.setInt(3, id_apu);
            ps.executeUpdate();

            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            try {
                respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR ActualizarCantidadAPU \n" + e.getMessage());
            } catch (SQLException ex) {

            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    @Override
    public String ActualizarEstadoRel(int id_apu, int id_actividad) {
        con = null;
        ps = null;
        query = "UpdateEstRelApuAct";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setInt(1, id_actividad);
            ps.setInt(2, id_apu);
            ps.executeUpdate();

            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            try {
                respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR ActualizarEstadoRel \n" + e.getMessage());
            } catch (SQLException ex) {

            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    @Override
    public String CargarPolizas() {
        con = null;
        rs = null;
        ps = null;

        query = "SQL_CARGAR_POLIZAS";
        String respuestaJson = "{}";
        JsonObject obj = new JsonObject();
        Gson gson = new Gson();
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);

            rs = ps.executeQuery();
            JsonObject fila = null;
            JsonArray arr = new JsonArray();
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("id", rs.getString("id"));
                fila.addProperty("nombre_poliza", rs.getString("nombre_poliza"));
                fila.addProperty("descripcion", rs.getString("descripcion"));
                fila.addProperty("reg_status", rs.getString("reg_status"));
                fila.addProperty("cambio", rs.getString("cambio"));
                arr.add(fila);
            }
            obj.add("rows", arr);
            respuestaJson = gson.toJson(obj);

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return respuestaJson;
    }

    @Override
    public String guardarPoliza(String nombre_poliza, String descripcion, Usuario usuario) {
        con = null;
        ps = null;
        String respuestaJson = "{}";
        query = "SQL_GUARDAR_POLIZA";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, nombre_poliza);
            ps.setString(2, descripcion);
            ps.setString(3, usuario.getLogin());
            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";
        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
            }
        }
        return respuestaJson;
    }

    @Override
    public String actualizarPoliza(String id, String nombre_poliza, String descripcion, Usuario usuario) {
        con = null;
        ps = null;
        String respuestaJson = "{}";
        query = "SQL_UPDATE_POLIZA";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, nombre_poliza);
            ps.setString(2, descripcion);
            ps.setString(3, usuario.getLogin());
            ps.setString(4, id);
            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";
        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
            }
        }
        return respuestaJson;
    }

    @Override
    public String activaInactivaPoliza(String id, Usuario usuario) {
        con = null;
        ps = null;
        query = "activarInactivarPoliza";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, usuario.getLogin());
            ps.setInt(2, Integer.parseInt(id));

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
            throw new SQLException("ERROR activaInactivaPoliza \n" + e.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    @Override
    public String cargarGridBroker() {
        con = null;
        rs = null;
        ps = null;

        query = "SQL_GET_BROKER";
        String respuestaJson = "{}";
        JsonObject obj = new JsonObject();
        Gson gson = new Gson();
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);

            rs = ps.executeQuery();
            JsonObject fila = null;
            JsonArray arr = new JsonArray();
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("id", rs.getString("id"));
                fila.addProperty("tipo_documento", rs.getString("tipo_documento"));
                fila.addProperty("documento", rs.getString("documento"));
                fila.addProperty("nombre", rs.getString("nombre"));
                fila.addProperty("codciu", rs.getString("codciu"));
                fila.addProperty("coddpto", rs.getString("coddpto"));
                fila.addProperty("codpais", rs.getString("codpais"));
                fila.addProperty("telcontacto", rs.getString("telcontacto"));
                fila.addProperty("celular_contacto", rs.getString("celular_contacto"));
                fila.addProperty("email_contacto", rs.getString("email_contacto"));
                fila.addProperty("direccion", rs.getString("direccion"));
                fila.addProperty("idusuario", rs.getString("idusuario"));
                fila.addProperty("nombre_contacto", rs.getString("nombre_contacto"));
                fila.addProperty("enviar_correo_contacto", rs.getString("enviar_correo_contacto"));
                fila.addProperty("nombre_asistente", rs.getString("nombre_asistente"));
                fila.addProperty("telasistente", rs.getString("telasistente"));
                fila.addProperty("celular_asistente", rs.getString("celular_asistente"));
                fila.addProperty("email_asistente", rs.getString("email_asistente"));
                fila.addProperty("enviar_correo_asistente", rs.getString("enviar_correo_asistente"));
                fila.addProperty("reg_status", rs.getString("reg_status"));
                fila.addProperty("cambio", rs.getString("cambio"));
                arr.add(fila);
            }
            obj.add("rows", arr);
            respuestaJson = gson.toJson(obj);

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return respuestaJson;

    }

    @Override
    public String guardarBroker(String tipoDoc, String documento, String nombre, String ciudad, String dpto, String pais, String direccion, String nombre_contacto, String telefono, String celular, String email, String enviar_correo_contacto, String idusuario,
            String nombre_asistente, String telefono_asistente, String celular_asistente, String email_asistente, String enviar_correo_asistente, Usuario usuario) {
        con = null;
        ps = null;
        query = "insertarBroker";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, usuario.getDstrct());
            ps.setString(2, tipoDoc);
            ps.setString(3, documento);
            ps.setString(4, nombre);
            ps.setString(5, ciudad);
            ps.setString(6, dpto);
            ps.setString(7, pais);
            ps.setString(8, direccion);
            ps.setString(9, telefono);
            ps.setString(10, celular);
            ps.setString(11, email);
            ps.setString(12, idusuario);
            ps.setString(13, usuario.getLogin());
            ps.setString(14, nombre_contacto);
            ps.setString(15, enviar_correo_contacto);
            ps.setString(16, nombre_asistente);
            ps.setString(17, telefono_asistente);
            ps.setString(18, celular_asistente);
            ps.setString(19, email_asistente);
            ps.setString(20, enviar_correo_asistente);

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Throwable e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
            throw new SQLException("ERROR guardarBroker \n" + e.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    @Override
    public String actualizarBroker(int id, String tipoDoc, String documento, String nombre, String ciudad, String dpto, String pais, String direccion, String nombre_contacto, String telefono, String celular, String email, String enviar_correo_contacto,
            String nombre_asistente, String telefono_asistente, String celular_asistente, String email_asistente, String enviar_correo_asistente, Usuario usuario) {
        con = null;
        ps = null;
        query = "actualizarBroker";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, tipoDoc);
            ps.setString(2, documento);
            ps.setString(3, nombre);
            ps.setString(4, ciudad);
            ps.setString(5, dpto);
            ps.setString(6, pais);
            ps.setString(7, telefono);
            ps.setString(8, celular);
            ps.setString(9, email);
            ps.setString(10, direccion);
            ps.setString(11, usuario.getLogin());
            ps.setString(12, nombre_contacto);
            ps.setString(13, enviar_correo_contacto);
            ps.setString(14, nombre_asistente);
            ps.setString(15, telefono_asistente);
            ps.setString(16, celular_asistente);
            ps.setString(17, email_asistente);
            ps.setString(18, enviar_correo_asistente);
            ps.setInt(19, id);

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
            throw new SQLException("ERROR actualizarBroker \n" + e.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    @Override
    public String activaInactivaBroker(String id, Usuario usuario) {
        con = null;
        ps = null;
        query = "activarInactivarBroker";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, usuario.getLogin());
            ps.setInt(2, Integer.parseInt(id));

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
            throw new SQLException("ERROR activaInactivaBroker \n" + e.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    @Override
    public String cargarGridInsumosEdit(String ids, int tipo, String id_actividad) {
        JsonObject obj = new JsonObject();
        Gson gson = new Gson();
        String respuestaJson = "{}";
        JsonArray arr = new JsonArray();
        JsonObject object = new JsonObject();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "ConsultarGridInsumoEdit";

        try {
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query).replace("#filtro", id_actividad));
            ps.setInt(1, Integer.parseInt(ids));
            rs = ps.executeQuery();

            while (rs.next()) {
                object = new JsonObject();
                object.addProperty("id", rs.getString("id"));
                object.addProperty("tipoinsumo", rs.getString("tipoinsumo"));
                object.addProperty("idtipoinsumo", rs.getString("idtipoinsumo"));
                object.addProperty("descripcionins", rs.getString("descripcionins"));
                object.addProperty("iddescripcionins", rs.getString("iddescripcionins"));
                object.addProperty("unidadmedida", rs.getString("unidadmedida"));
                object.addProperty("idunidadmedida", rs.getString("idunidadmedida"));
                object.addProperty("cantidad", rs.getString("cantidad"));
                object.addProperty("rendimiento", rs.getString("rendimiento"));
                if (tipo == 1) {
                    object.addProperty("valor_insumo", rs.getString("valor_insumo"));
                    object.addProperty("total_insumo", rs.getString("total_insumo"));
                }

                arr.add(object);
            }

            obj.add("rows", arr);
            respuestaJson = gson.toJson(obj);

        } catch (Exception exc) {
            respuestaJson = "{\"error\":\"" + exc.getMessage() + "\"}";
            throw new SQLException("ERROR cargarGridInsumosEdit \n" + exc.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception ex) {
            }
            return respuestaJson;
        }
    }

    @Override
    public JsonObject getInfoCuentaEnvioCorreo() {
        con = null;
        ps = null;
        rs = null;
        query = "GET_INFO_CORREO_SELECTRIK";
        JsonObject obj = null;

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            rs = ps.executeQuery();
            JsonObject fila = null;
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("servidor", rs.getString("servidor"));
                fila.addProperty("puerto", rs.getString("puerto"));
                fila.addProperty("usuario", rs.getString("usuario"));
                fila.addProperty("clave", rs.getString("clave"));
                fila.addProperty("bodyMessage", rs.getString("bodyMessage"));
            }
            obj = fila;

        } catch (Exception e) {
            System.out.println("Error en getInfoCuentaEnvioCorreo: " + e.toString());
            e.printStackTrace();
            throw new Exception("Error en getInfoCuentaEnvioCorreo: " + e.toString());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return obj;
        }
    }

    @Override
    public String listarCorrespondencia() {
        con = null;
        rs = null;
        ps = null;

        query = "SQL_LISTAR_CORRESPONDENCIA";
        String respuestaJson = "{}";
        JsonObject obj = new JsonObject();
        Gson gson = new Gson();
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);

            rs = ps.executeQuery();
            JsonObject fila = null;
            JsonArray arr = new JsonArray();
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("id", rs.getString("id"));
                fila.addProperty("codparam", rs.getString("codparam"));
                fila.addProperty("descripcion", rs.getString("descripcion"));
                fila.addProperty("id_procedencia", rs.getString("id_procedencia"));
                fila.addProperty("procedencia", rs.getString("procedencia"));
                fila.addProperty("reg_status", rs.getString("reg_status"));
                fila.addProperty("cambio", rs.getString("cambio"));
                arr.add(fila);
            }
            obj.add("rows", arr);
            respuestaJson = gson.toJson(obj);

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return respuestaJson;
    }

    @Override
    public String cargarEquivalencias() {
        con = null;
        rs = null;
        ps = null;
        query = "SQL_CARGAR_EQUIVALENCIAS";
        String respuestaJson = "{}";
        JsonObject obj = new JsonObject();
        Gson gson = new Gson();
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);

            rs = ps.executeQuery();
            JsonObject fila = null;
            JsonArray arr = new JsonArray();
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("id", rs.getString("id"));
                fila.addProperty("descripcion", rs.getString("descripcion"));
                arr.add(fila);
            }
            obj.add("rows", arr);
            respuestaJson = gson.toJson(obj);

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return respuestaJson;
    }

    @Override
    public String cambiarEstadoCorrespondencia(String id, Usuario usuario) {
        con = null;
        ps = null;
        query = "SQL_CAMBIARESTADO_CORRESPONDENCIA";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, usuario.getLogin());
            ps.setInt(2, Integer.parseInt(id));

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
            throw new SQLException("ERROR SQL_CAMBIARESTADO_CORRESPONDENCIA \n" + e.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    @Override
    public String guardarCorrespondencia(String descripcion, int id_procedencia, Usuario usuario) {
        con = null;
        ps = null;
        String respuestaJson = "{}";
        query = "SQL_GUARDAR_CORRESPONDENCIA";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, descripcion);
            ps.setInt(2, id_procedencia);
            ps.setString(3, usuario.getLogin());
            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"Guardado\"}";
        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
            }
        }
        return respuestaJson;
    }

    @Override
    public String actualizarCorrespondencia(String id, String descripcion, int id_procedencia, Usuario usuario) {
        con = null;
        ps = null;
        String respuestaJson = "{}";
        query = "SQL_UPDATE_CORRESPONDENCIA";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, descripcion);
            ps.setInt(2, id_procedencia);
            ps.setString(3, usuario.getLogin());
            ps.setString(4, id);
            //ps.setString(4, id);
            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"Guardado\"}";
        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
            }
        }
        return respuestaJson;
    }

    @Override
    public String cargarGridCategoriasAdmon() {
        con = null;
        rs = null;
        ps = null;

        query = "SQL_GET_CATEGORIAS_ADMON";
        String respuestaJson = "{}";
        JsonObject obj = new JsonObject();
        Gson gson = new Gson();
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query).replaceAll("#filtro", "");

            ps = con.prepareStatement(query);

            rs = ps.executeQuery();
            JsonObject fila = null;
            JsonArray arr = new JsonArray();
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("id", rs.getString("id"));
                fila.addProperty("nombre", rs.getString("nombre"));
                fila.addProperty("descripcion", rs.getString("descripcion"));
                fila.addProperty("reg_status", rs.getString("reg_status"));
                arr.add(fila);
            }
            obj.add("rows", arr);
            respuestaJson = gson.toJson(obj);

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return respuestaJson;

    }

    @Override
    public boolean existeCategoriaAdmon(String nombre) {
        con = null;
        rs = null;
        ps = null;
        boolean resp = false;
        query = "SQL_GET_CATEGORIAS_ADMON";
        String filtro = " WHERE nombre = '" + nombre + "'";
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query).replaceAll("#filtro", filtro);

            ps = con.prepareStatement(query);
            rs = ps.executeQuery();

            if (rs.next()) {
                resp = true;
            }
        } catch (Exception e) {
            System.out.println("Error en existeCategoriaAdmon: " + e.toString());
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return resp;
    }

    @Override
    public boolean existeCategoriaAdmon(String idCat, String nombre) {
        con = null;
        rs = null;
        ps = null;
        boolean resp = false;
        query = "SQL_GET_CATEGORIAS_ADMON";
        String filtro = " WHERE nombre = '" + nombre + "' AND id not in(" + idCat + ")";
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query).replaceAll("#filtro", filtro);

            ps = con.prepareStatement(query);
            rs = ps.executeQuery();

            if (rs.next()) {
                resp = true;
            }
        } catch (Exception e) {
            System.out.println("Error en existeCategoriaAdmon: " + e.toString());
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return resp;
    }

    @Override
    public String guardarCategoria(String nombre, String descripcion, Usuario usuario) {
        con = null;
        ps = null;
        query = "guardarCategoriaAdmon";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, usuario.getDstrct());
            ps.setString(2, nombre);
            ps.setString(3, descripcion);
            ps.setString(4, usuario.getLogin());

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Throwable e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
            throw new SQLException("ERROR guardarCategoria \n" + e.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    @Override
    public String actualizarCategoria(String nombre, String descripcion, String idCategoria, Usuario usuario) {
        con = null;
        ps = null;
        query = "actualizarCategoriaAdmon";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, nombre);
            ps.setString(2, descripcion);
            ps.setString(3, usuario.getLogin());
            ps.setInt(4, Integer.parseInt(idCategoria));

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Throwable e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
            throw new SQLException("ERROR actualizarCategoria \n" + e.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    @Override
    public String cargarGridItemsCategoria(String idCategoria) {
        con = null;
        rs = null;
        ps = null;

        query = "SQL_GET_ITEMS_CATEGORIA";
        String respuestaJson = "{}";
        JsonObject obj = new JsonObject();
        Gson gson = new Gson();
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);
            ps.setInt(1, Integer.parseInt(idCategoria));
            rs = ps.executeQuery();
            JsonObject fila = null;
            JsonArray arr = new JsonArray();
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("id", rs.getString("id"));
                fila.addProperty("descripcion", rs.getString("descripcion"));
                fila.addProperty("valor", rs.getString("valor"));
                fila.addProperty("is_default", rs.getString("is_default"));
                fila.addProperty("reg_status", rs.getString("reg_status"));
                arr.add(fila);
            }
            obj.add("rows", arr);
            respuestaJson = gson.toJson(obj);

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return respuestaJson;

    }

    @Override
    public String guardarItemCategoria(String id_categoria, String nombre, String valor, String is_default, Usuario usuario) {
        con = null;
        ps = null;
        query = "guardarItemCategoria";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, usuario.getDstrct());
            ps.setInt(2, Integer.parseInt(id_categoria));
            ps.setString(3, nombre);
            ps.setDouble(4, Double.parseDouble(valor));
            ps.setString(5, is_default);
            ps.setString(6, usuario.getLogin());

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Throwable e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
            throw new SQLException("ERROR guardarItemCategoria \n" + e.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    @Override
    public String actualizarItemCategoria(String idItem, String nombre, String valor, String is_default, Usuario usuario) {
        con = null;
        ps = null;
        query = "actualizarItemCategoria";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, nombre);
            ps.setDouble(2, Double.parseDouble(valor));
            ps.setString(3, is_default);
            ps.setString(4, usuario.getLogin());
            ps.setInt(5, Integer.parseInt(idItem));

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Throwable e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
            throw new SQLException("ERROR actualizarCategoria \n" + e.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    @Override
    public String activaInactivaItem(String id, String Query, Usuario usuario) {
        con = null;
        ps = null;
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(Query));
            ps.setString(1, usuario.getLogin());
            ps.setInt(2, Integer.parseInt(id));

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
            throw new SQLException("ERROR activaInactivaItem \n" + e.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    @Override
    public String cargarGridUndsAdmon() {
        Gson gson = new Gson();
        con = null;
        ps = null;
        rs = null;
        JsonObject obj = new JsonObject();

        query = "SQL_OBTENER_UNIDADES_ADMON";
        String filtro = "";

        JsonArray arr = new JsonArray();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query).replaceAll("#filtro", filtro));
            rs = ps.executeQuery();
            JsonObject fila = null;
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("id", rs.getString("id"));
                fila.addProperty("nombre", rs.getString("nombre"));
                fila.addProperty("descripcion", rs.getString("descripcion"));
                fila.addProperty("reg_status", rs.getString("reg_status"));
                fila.addProperty("cambio", rs.getString("cambio"));
                arr.add(fila);
            }
            obj.add("rows", arr);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return gson.toJson(obj);
        }
    }

    @Override
    public String insertarUndAdmon(String nombre, String descripcion, Usuario usuario) {
        con = null;
        ps = null;
        query = "SQL_INSERT_UNIDAD_ADMON";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, nombre);
            ps.setString(2, descripcion);
            ps.setString(3, usuario.getLogin());
            ps.executeUpdate();

            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            try {
                respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR insertarUndAdmon \n" + e.getMessage());
            } catch (SQLException ex) {

            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    @Override
    public String actualizarUndAdmon(String id, String nombre, String descripcion, Usuario usuario) {
        con = null;
        ps = null;
        query = "SQL_UPDATE_UNIDAD_ADMON";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, nombre);
            ps.setString(2, descripcion);
            ps.setString(3, usuario.getLogin());
            ps.setInt(4, Integer.parseInt(id));
            ps.executeUpdate();

            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            try {
                respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR actualizarUndAdmon \n" + e.getMessage());
            } catch (SQLException ex) {

            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    @Override
    public String activaInactivaUndAdmon(String id, Usuario usuario) {
        con = null;
        ps = null;
        query = "activarInactivarUndAdmon";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, usuario.getLogin());
            ps.setInt(2, Integer.parseInt(id));

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
            throw new SQLException("ERROR activaInactivaUndAdmon \n" + e.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    @Override
    public String CargarAseguradoras() {
        con = null;
        rs = null;
        ps = null;

        query = "SQL_CARGAR_ASEGURADORAS";
        String respuestaJson = "{}";
        JsonObject obj = new JsonObject();
        Gson gson = new Gson();
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);

            rs = ps.executeQuery();
            JsonObject fila = null;
            JsonArray arr = new JsonArray();
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("id", rs.getString("id"));
                fila.addProperty("nit", rs.getString("nit"));
                fila.addProperty("nombre_seguro", rs.getString("nombre_seguro"));
                fila.addProperty("reg_status", rs.getString("reg_status"));
                fila.addProperty("cambio", rs.getString("cambio"));
                arr.add(fila);
            }
            obj.add("rows", arr);
            respuestaJson = gson.toJson(obj);

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return respuestaJson;
    }

    @Override
    public String guardarAseguradora(String nit_aseguradora, String nombre_aseguradora, Usuario usuario) {
        con = null;
        ps = null;
        String respuestaJson = "{}";
        query = "SQL_GUARDAR_ASEGURADORA";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, nit_aseguradora);
            ps.setString(2, nombre_aseguradora);
            ps.setString(3, usuario.getLogin());
            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";
        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
            }
        }
        return respuestaJson;
    }

    @Override
    public String actualizarAseguradora(String id, String nit_aseguradora, String nombre_aseguradora, Usuario usuario) {
        con = null;
        ps = null;
        String respuestaJson = "{}";
        query = "SQL_UPDATE_ASEGURADORA";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, nit_aseguradora);
            ps.setString(2, nombre_aseguradora);
            ps.setString(3, usuario.getLogin());
            ps.setString(4, id);
            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";
        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
            }
        }
        return respuestaJson;
    }

    @Override
    public String activaInactivaAseguradora(String id, Usuario usuario) {
        con = null;
        ps = null;
        query = "activarInactivarAseguradora";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, usuario.getLogin());
            ps.setInt(2, Integer.parseInt(id));

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
            throw new SQLException("ERROR activaInactivaAseguradora \n" + e.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    @Override
    public boolean existeNitEnProveedor(String nit_aseguradora) {
        con = null;
        ps = null;
        rs = null;
        boolean resp = false;
        query = "SQL_EXISTE_NIT_PROVEEDOR";
        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);
            ps.setString(1, nit_aseguradora);
            rs = ps.executeQuery();

            if (rs.next()) {
                resp = true;
            }
        } catch (Exception e) {
            try {
                resp = false;
                throw new SQLException("ERROR existeNitEnProveedor \n" + e.getMessage());
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
        }
        return resp;
    }

    @Override
    public String filtro_Apus(String buscar, String id, String grupo_apus) {
        Gson gson = new Gson();
        JsonObject respuesta = new JsonObject();
        JsonArray arr = new JsonArray();
        JsonObject object = new JsonObject();;
        con = null;
        ps = null;
        rs = null;
        String sql = "";
        String query = "SQL_FILTRO_APUS";

        try {
            con = this.conectarJNDI(query);
            sql = this.obtenerSQL(query);
            buscar = buscar.replaceAll(" ", "%");
            sql = sql.replace("#filtro2", " and a.id not in(select id_apu from opav.sl_rel_actividades_apu where id_actividad_capitulo=" + id + ") order by a.nombre ASC;");
            sql = sql.replace("#filtro3", buscar);
            sql = grupo_apus.equals("0") ? sql.replaceAll("#filtro4","") : sql.replaceAll("#filtro4", " and c.id_grupo_apu=" + grupo_apus + " ");

            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();

            while (rs.next()) {
                object = new JsonObject();
                object.addProperty("id", rs.getString("id"));
                object.addProperty("nombreapu", rs.getString("nombre"));
                object.addProperty("nombre_unidad", rs.getString("nombre_unidad"));
                object.addProperty("idgrupo", rs.getString("id_grupo_apu"));
                object.addProperty("idunidad", rs.getString("id_unidad_medida"));
                arr.add(object);
            }

            System.out.println(new Gson().toJson(arr));
            respuesta.add("rows", arr);
        } catch (Exception exc) {
            respuesta = new JsonObject();
            respuesta.addProperty("error", exc.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception ex) {
            }
            return gson.toJson(respuesta);
        }
    }
    
    @Override
    public String filtro_Apus(String buscar, String id, String id_solicitud,String grupo_apus) {
        Gson gson = new Gson();
        JsonObject respuesta = new JsonObject();
        JsonArray arr = new JsonArray();
        JsonObject object = new JsonObject();;
        con = null;
        ps = null;
        rs = null;
        String sql = "";
        String query = "SQL_FILTRO_APUS_2";

        try {
            con = this.conectarJNDI(query);
            sql = this.obtenerSQL(query);
            buscar = buscar.replaceAll(" ", "%");
            sql = grupo_apus.equals("0") ? sql.replaceAll("#filtro4","")  : sql.replace("#filtro4", "AND c.id_grupo_apu=" + grupo_apus);
            sql = sql.replace("#filtro3", buscar);
            sql = sql.replace("#filtro2", id);
            sql = sql.replace("#filtro", id_solicitud);
            
            

            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();

            while (rs.next()) {
                object = new JsonObject();
                object.addProperty("id", rs.getString("id"));
                object.addProperty("nombreapu", rs.getString("nombre"));
                object.addProperty("nombre_unidad", rs.getString("nombre_unidad"));
                object.addProperty("idgrupo", rs.getString("id_grupo_apu"));
                object.addProperty("idunidad", rs.getString("id_unidad_medida"));
                arr.add(object);
            }

            System.out.println(new Gson().toJson(arr));
            respuesta.add("rows", arr);
        } catch (Exception exc) {
            respuesta = new JsonObject();
            respuesta.addProperty("error", exc.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception ex) {
            }
            return gson.toJson(respuesta);
        }
    }

    @Override
    public String filtro_Insumo(String buscar, String categoria, String subcategoria, String id_tipo_insumo) {
        Gson gson = new Gson();
        JsonObject respuesta = new JsonObject(), fila;
        JsonArray arr = new JsonArray();
        JsonObject object = new JsonObject();;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        String query = "SQL_FILTRO_INSUMO";

        try {
            con = this.conectarJNDI(query);
                String sql = this.obtenerSQL(query);
            buscar = buscar.replaceAll(" ", "%");
            sql = sql.replace("#filtro4", id_tipo_insumo);
            sql = sql.replace("#filtro3", buscar);
            

            if (categoria.equals("0"))    {
                sql = sql.replace("#filtro2", "");

            } else {
                sql = sql.replace("#filtro2", " AND d.id_categoria = " + categoria);
            }
            if (subcategoria.equals("0")) {
                sql = sql.replace("#filtro", "");
            } else {
                sql = sql.replace("#filtro", " AND d.id_subcategoria = " + subcategoria);
            }

            ps = con.prepareStatement(sql);

            rs = ps.executeQuery();

            while (rs.next()) {
                object = new JsonObject();
                object.addProperty("id", rs.getString("id"));
                object.addProperty("nombre", rs.getString("nombre"));
                arr.add(object);
            }

            System.out.println(new Gson().toJson(arr));

            respuesta.add("rows", arr);
        } catch (Exception exc) {
            respuesta = new JsonObject();
            respuesta.addProperty("error", exc.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception ex) {
            }
            return gson.toJson(respuesta);
        }
    }

    public boolean APU_Asociado(int id_apu) {
        con = null;
        ps = null;
        rs = null;
        boolean resp = false;
        query = "SQL_APU_ASOCIADO";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);

            ps.setInt(1, id_apu);

            rs = ps.executeQuery();

            if (rs.next()) {
                resp = true;
            }
        } catch (Exception e) {
            try {
                resp = false;
                throw new SQLException("ERROR existeAPU \n" + e.getMessage());
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
        }
        return resp;

    }

    @Override
    public String ActualizarCantidadAPU(int id_apu, int id_actividad, int cantidad, int posicion) {
        con = null;
        ps = null;
        query = "UpdateCantRelApuAct2";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setInt(1, cantidad);
            ps.setInt(2, posicion);
            ps.setInt(3, id_actividad);
            ps.setInt(4, id_apu);
            ps.executeUpdate();

            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            try {
                respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR ActualizarCantidadAPU \n" + e.getMessage());
            } catch (SQLException ex) {

            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    

}
