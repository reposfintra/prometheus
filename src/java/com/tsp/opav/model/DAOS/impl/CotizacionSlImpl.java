/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.opav.model.DAOS.impl;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.tsp.opav.model.DAOS.MainDAO;
import com.tsp.opav.model.DAOS.CotizacionSlDAO;
import com.tsp.operation.model.TransaccionService;
import com.tsp.operation.model.beans.StringStatement;
import com.tsp.operation.model.beans.Usuario;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author user
 */
public class CotizacionSlImpl extends MainDAO implements CotizacionSlDAO {

    private Connection con = null;
    private PreparedStatement ps = null;
    private ResultSet rs = null;
    private String query = "";
    private StringStatement st = null;
    private Gson gson = new Gson();
    private JsonObject obj;

    public CotizacionSlImpl(String dataBaseName) {
        super("CotizacionSlDAO.xml", dataBaseName);

    }

    @Override
    public String cargarMetodoCalculo() {
        try {
            obj = new JsonObject();
            query = "ConsultarMetodosCal";
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();

            JsonObject fila = new JsonObject();
            while (rs.next()) {
                fila.addProperty(rs.getString("codigo"), rs.getString("nombre"));
            }
            obj = fila;

        } catch (Exception ex) {
            throw new SQLException("ERROR OBTENIENDO TIPOS DE MATERIAL: cargarTiposMaterial()  " + ex.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception ex) {
            }
            return gson.toJson(obj);
        }
    }

    @Override
    public String cargarMetodoCalculoXLinea(String metodo, String codigo) {
        try {
            obj = new JsonObject();
            query = "ConsultarMetodosCalXLinea";
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setInt(1, Integer.parseInt(metodo));
            ps.setString(2, codigo);

            rs = ps.executeQuery();

            JsonObject fila = new JsonObject();
            while (rs.next()) {
                fila.addProperty("valor", rs.getString("valor"));
            }
            obj = fila;

        } catch (Exception ex) {
            throw new SQLException("ERROR OBTENIENDO TIPOS DE MATERIAL: cargarTiposMaterial()  " + ex.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception ex) {
            }
            return gson.toJson(obj);
        }
    }

    @Override
    public JsonObject cargarGridAccion(String apugrupo, String metcalc, String id) {
        JsonObject respuesta = new JsonObject();
        JsonArray arr = new JsonArray();
        JsonObject object = new JsonObject();
        Connection con = null;
        String sql = "";
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "ConsultaDatosAccion";

        try {
            con = this.conectarJNDI(query);
            sql = this.obtenerSQL(query);

            sql = sql.replace("#filtro2", id);
            sql = sql.replace("#filtro3", metcalc);

            ps = con.prepareStatement(sql);
            //ps.setInt(1, Integer.parseInt(ids));
            rs = ps.executeQuery();

            while (rs.next()) {
                object = new JsonObject();
                object.addProperty("metodocalculo", rs.getString("metodocalculo"));
                object.addProperty("tipoinsumo", rs.getString("nombre_insumo"));
                object.addProperty("id_tipo_insumo", rs.getString("id_tipo_insumo"));
                object.addProperty("codinsumo", rs.getString("codigo_material"));
                object.addProperty("descripcionins", rs.getString("descripcion"));
                object.addProperty("cantidad", rs.getString("cantidad"));
                object.addProperty("unidadmedida", rs.getString("nombre_unidad"));
                object.addProperty("idunidadmedida", rs.getString("id_unidad_medida"));
                object.addProperty("pareto", rs.getString("pareto"));
                object.addProperty("preciounitario", rs.getString("precio_unitario"));
                object.addProperty("preciototal", rs.getString("preciototal"));
                object.addProperty("proveedor", rs.getString("proveedor"));
                object.addProperty("nitproveedor", rs.getString("nit_proveedor"));
                object.addProperty("preciohistorico", rs.getString("preciohistorico"));
                object.addProperty("preciounitariofinal", rs.getString("preciounitariofinal"));
                object.addProperty("totalfinal", rs.getString("totalfinal"));
                object.addProperty("rentabilidadprov", rs.getString("rentabilidadprov"));
                object.addProperty("valrentabilidadprov", rs.getString("valrentabilidadprov"));
                object.addProperty("rentabilidadsel", rs.getString("rentabilidadsel"));
                object.addProperty("valrentabilidadsel", rs.getString("valrentabilidadsel"));
                object.addProperty("totalcliente", rs.getString("totalcliente"));
                object.addProperty("id_insumo", rs.getString("id_insumo"));
                object.addProperty("estado", "0");
                arr.add(object);
            }

            //System.out.println(new Gson().toJson(arr));

            respuesta.add("rows", arr);
        } catch (Exception exc) {
            respuesta = new JsonObject();
            respuesta.addProperty("error", exc.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception ex) {
            }
            return respuesta;
        }
    }

    @Override
    public String cargardatoscabecera(String id) {
        try {
            obj = new JsonObject();
            query = "ConsultaDatosCabecera";
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, id);
            rs = ps.executeQuery();

            JsonObject fila = new JsonObject();
            while (rs.next()) {
                fila.addProperty("id_cliente", rs.getString("id_cliente"));
                fila.addProperty("nomcli", rs.getString("nomcli"));
                fila.addProperty("serie_cot", rs.getString("serie_cot"));
                fila.addProperty("existe", rs.getString("existe"));
            }
            obj = fila;

        } catch (Exception ex) {
            throw new SQLException("ERROR OBTENIENDO LA CABECERA: cargardatoscabecera(String id) " + ex.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception ex) {
            }
            return gson.toJson(obj);
        }
    }

    @Override
    public JsonObject cargarAPUs() {
        JsonObject respuesta = new JsonObject(), fila;
        JsonArray arr = new JsonArray();
        JsonObject object = new JsonObject();;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "ConsultaAPus";

        try {
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();

            while (rs.next()) {
                object = new JsonObject();
                object.addProperty("id", rs.getString("id"));
                object.addProperty("nombreapu", rs.getString("nombre"));
                object.addProperty("nombre_unidad", rs.getString("nombre_unidad"));
                arr.add(object);
            }

            //System.out.println(new Gson().toJson(arr));

            respuesta.add("rows", arr);
        } catch (Exception exc) {
            respuesta = new JsonObject();
            respuesta.addProperty("error", exc.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception ex) {
            }
            return respuesta;
        }
    }

    @Override
    public JsonObject cargarAPUXGrupo(String grupo_apu, String apugrupo) {
        JsonObject respuesta = new JsonObject();
        JsonArray arr = new JsonArray();
        JsonObject object = new JsonObject();;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = "";
        String query = "ConsultarAPUXGrupo";

        try {
            con = this.conectarJNDI(query);
            sql = this.obtenerSQL(query);
            if (grupo_apu.equals("0")) {
                sql = sql.replace("#filtro", "");
            }

            if (!apugrupo.equals("")) {
                sql = sql.replace("#filtro", " and c.id_grupo_apu = " + grupo_apu + " and a.id not in(" + apugrupo + ")");
            }

            if (!grupo_apu.equals("0")) {
                sql = sql.replace("#filtro", " and c.id_grupo_apu = " + grupo_apu + "");
            }

            ps = con.prepareStatement(sql);

            rs = ps.executeQuery();

            while (rs.next()) {
                object = new JsonObject();
                object.addProperty("id", rs.getString("id"));
                object.addProperty("nombreapu", rs.getString("nombre"));
                object.addProperty("nombre_unidad", rs.getString("nombre_unidad"));
                arr.add(object);
            }

            //System.out.println(new Gson().toJson(arr));

            respuesta.add("rows", arr);
        } catch (Exception exc) {
            respuesta = new JsonObject();
            respuesta.addProperty("error", exc.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception ex) {
            }
            return respuesta;
        }
    }

    @Override
    public JsonObject guardarCotizacion(JsonObject info, JsonObject info_detalle) {

        JsonObject respuesta = new JsonObject();
        con = null;
        ps = null;
        String mensaje = "";
        int resp = 0, idCab = 0;
        Statement stmt = null;
        //query = "GuardarEncabezadoInsumo";

        try {

            JsonArray arr = info.getAsJsonArray("rows");
            JsonArray arr_det = info_detalle.getAsJsonArray("rows");
            PreparedStatement ps = null;
            con = this.conectarJNDI();

            if (info.get("existe").getAsString().equals("0")) {

                String query = "InsertarCabCotizacion";
                con.setAutoCommit(false);

                ps = con.prepareStatement(this.obtenerSQL(query), Statement.RETURN_GENERATED_KEYS);

                ps.setString(1, info.get("dstrct").getAsString());
                ps.setString(2, info.get("idaccion").getAsString());
                ps.setString(3, info.get("CodCotizacion").getAsString());
                ps.setString(4, info.get("CodCliente").getAsString());
                ps.setString(5, info.get("NombreCliente").getAsString());
                ps.setString(6, info.get("fecha").getAsString());
                ps.setString(7, info.get("visualizacion").getAsString());
                ps.setString(8, info.get("modalidad").getAsString());
                ps.setDouble(9, info.get("val_material").getAsDouble());
                ps.setDouble(10, info.get("val_mano_obra").getAsDouble());
                ps.setDouble(11, info.get("val_equipos").getAsDouble());
                ps.setDouble(12, info.get("val_herramientas").getAsDouble());
                ps.setDouble(13, info.get("val_transporte").getAsDouble());
                ps.setDouble(14, info.get("val_tramites").getAsDouble());
                ps.setDouble(15, info.get("valcotizacion").getAsDouble());
                ps.setDouble(16, info.get("valdesc").getAsDouble());
                ps.setDouble(17, info.get("subtotal").getAsDouble());
                ps.setDouble(18, info.get("perc_iva").getAsDouble());
                ps.setDouble(19, info.get("valiva").getAsDouble());
                ps.setDouble(20, info.get("val_admon").getAsDouble());
                ps.setDouble(21, info.get("val_imprevisto").getAsDouble());
                ps.setDouble(22, info.get("val_utilidad").getAsDouble());
                ps.setDouble(23, info.get("perc_aiu").getAsDouble());
                ps.setDouble(24, info.get("val_aiu").getAsDouble());
                ps.setDouble(25, info.get("perc_admon").getAsDouble());
                ps.setDouble(26, info.get("perc_imprevisto").getAsDouble());
                ps.setDouble(27, info.get("perc_utilidad").getAsDouble());
                ps.setDouble(28, info.get("val_total").getAsDouble());
                ps.setString(29, info.get("anticipo").getAsString());
                ps.setDouble(30, info.get("perc_anticipo").getAsDouble());
                ps.setDouble(31, info.get("val_anticipo").getAsDouble());
                ps.setString(32, info.get("retegarantia").getAsString());
                ps.setDouble(33, info.get("perc_rete").getAsDouble());
                ps.setString(34, info.get("usuario").getAsString());

                resp = ps.executeUpdate();
                stmt = con.createStatement();
                if (resp > 0) {
                    ResultSet rs = ps.getGeneratedKeys();
                    if (rs.next()) {
                        idCab = rs.getInt(1);

                        for (int i = 0; i < arr.size(); i++) {
                            respuesta = arr.get(i).getAsJsonObject();

                            stmt.addBatch(insertarDetalleCotizacion(idCab, respuesta, info.get("usuario").getAsString(), info.get("dstrct").getAsString()));
                        }

                        for (int i = 0; i < arr_det.size(); i++) {
                            respuesta = arr_det.get(i).getAsJsonObject();

                            stmt.addBatch(insertarDetalleCotizacionVal(idCab, respuesta, info.get("usuario").getAsString(), info.get("dstrct").getAsString()));
                        }

//Aqui hay que crear un query para poder meterle los default william alberto siado torres
                        stmt.addBatch(insertar_Porcentajes_Default(idCab));
                        stmt.executeBatch();
                        stmt.clearBatch();
                        mensaje = "Cotizacion Guardada exitosamente...";
                    }
                }

                con.commit();

            } else {

                String query = "UpdateCabCotizacion";
                con.setAutoCommit(false);

                ps = con.prepareStatement(this.obtenerSQL(query));

                ps.setString(1, info.get("fecha").getAsString());
                ps.setString(2, info.get("visualizacion").getAsString());
                ps.setString(3, info.get("modalidad").getAsString());
                ps.setDouble(4, info.get("val_material").getAsDouble());
                ps.setDouble(5, info.get("val_mano_obra").getAsDouble());
                ps.setDouble(6, info.get("val_equipos").getAsDouble());
                ps.setDouble(7, info.get("val_herramientas").getAsDouble());
                ps.setDouble(8, info.get("val_transporte").getAsDouble());
                ps.setDouble(9, info.get("val_tramites").getAsDouble());
                ps.setDouble(10, info.get("valcotizacion").getAsDouble());
                ps.setDouble(11, info.get("valdesc").getAsDouble());
                ps.setDouble(12, info.get("subtotal").getAsDouble());
                ps.setDouble(13, info.get("perc_iva").getAsDouble());
                ps.setDouble(14, info.get("valiva").getAsDouble());
                ps.setDouble(15, info.get("val_admon").getAsDouble());
                ps.setDouble(16, info.get("val_imprevisto").getAsDouble());
                ps.setDouble(17, info.get("val_utilidad").getAsDouble());
                ps.setDouble(18, info.get("perc_aiu").getAsDouble());
                ps.setDouble(19, info.get("val_aiu").getAsDouble());
                ps.setDouble(20, info.get("perc_admon").getAsDouble());
                ps.setDouble(21, info.get("perc_imprevisto").getAsDouble());
                ps.setDouble(22, info.get("perc_utilidad").getAsDouble());
                ps.setDouble(23, info.get("val_total").getAsDouble());
                ps.setString(24, info.get("anticipo").getAsString());
                ps.setDouble(25, info.get("perc_anticipo").getAsDouble());
                ps.setDouble(26, info.get("val_anticipo").getAsDouble());
                ps.setString(27, info.get("retegarantia").getAsString());
                ps.setDouble(28, info.get("perc_rete").getAsDouble());
                ps.setString(29, info.get("usuario").getAsString());
                ps.setInt(30, info.get("id").getAsInt());

                ps.executeUpdate();

                stmt = con.createStatement();

                stmt.addBatch("delete from opav.sl_cotizacion_detalle where id_cotizacion=" + info.get("id").getAsInt() + ";");

                for (int i = 0; i < arr.size(); i++) {
                    respuesta = arr.get(i).getAsJsonObject();

                    stmt.addBatch(insertarDetalleCotizacion(info.get("id").getAsInt(), respuesta, info.get("usuario").getAsString(), info.get("dstrct").getAsString()));
                }

                stmt.addBatch("delete from opav.sl_relacion_cotizacion_detalle_apu where id_cotizacion=" + info.get("id").getAsInt() + ";");

                for (int i = 0; i < arr_det.size(); i++) {
                    respuesta = arr_det.get(i).getAsJsonObject();

                    stmt.addBatch(insertarDetalleCotizacionVal(info.get("id").getAsInt(), respuesta, info.get("usuario").getAsString(), info.get("dstrct").getAsString()));
                }

                stmt.addBatch(insertar_Porcentajes_Default(info.get("id").getAsInt()));

                stmt.executeBatch();
                stmt.clearBatch();
                mensaje = "Cotizacion Actualizada exitosamente...";

                con.commit();

            }

            respuesta = new JsonObject();

            respuesta.addProperty("mensaje", mensaje);

        } catch (Exception exc) {
            con.rollback();
            respuesta = new JsonObject();
            exc.getMessage();
            respuesta.addProperty("error", "Error, no puede registrar productos ya guardados");

        } finally {
            try {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException e) {
                        throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                    }
                }
                if (con != null) {
                    try {
                        con.setAutoCommit(true);
                        this.desconectar(con);
                    } catch (SQLException e) {
                        throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage());
                    }
                }
            } catch (Exception ex) {
            }
            return respuesta;
        }
    }

    public String insertarDetalleCotizacion(int idCab, JsonObject respuesta, String usuario, String empresa) throws SQLException {

        String cadena = "";
        String query = "InsertarDetCotizacion";
        String sql = "";
        StringStatement st = null;
        sql = this.obtenerSQL(query);
        st = new StringStatement(sql, true);

        try {

            st.setString(1, empresa);
            st.setInt(2, idCab);
            st.setInt(3, respuesta.get("id_tipo_insumo").getAsInt());
            st.setInt(4, respuesta.get("id_insumo").getAsInt());
            st.setString(5, respuesta.get("descripcionins").getAsString());
            st.setString(6, respuesta.get("idunidadmedida").getAsString());
            st.setString(7, respuesta.get("nitproveedor").getAsString());
            st.setString(8, respuesta.get("cantidad").getAsString());
            st.setDouble(9, respuesta.get("preciounitario").getAsDouble());
            st.setDouble(10, respuesta.get("preciohistorico").getAsDouble());
            st.setDouble(11, respuesta.get("pareto").getAsDouble());
            st.setDouble(12, respuesta.get("preciounitariofinal").getAsDouble());
            st.setDouble(13, respuesta.get("rentabilidadprov").getAsDouble());
            st.setDouble(14, respuesta.get("valrentabilidadprov").getAsDouble());
            st.setDouble(15, respuesta.get("rentabilidadsel").getAsDouble());
            st.setDouble(16, respuesta.get("valrentabilidadsel").getAsDouble());
            st.setDouble(17, respuesta.get("totalfinal").getAsDouble());
            st.setDouble(18, respuesta.get("totalcliente").getAsDouble());
            st.setString(19, usuario);
            st.setString(20, respuesta.get("proveedor").getAsString());
            cadena += st.getSql();

        } catch (Exception e) {
            System.out.println("Error en SQL_INSERTAR_DETALLE_RECAUDO (insertarRecaudosDetalle)" + e.toString());
            e.printStackTrace();
        }
        return cadena;

    }

    public String insertarDetalleCotizacionVal(int idCab, JsonObject respuesta, String usuario, String empresa) throws SQLException {

        String cadena = "";
        String query = "InsertarDetCotizacionDet";
        String sql = "";
        StringStatement st = null;
        sql = this.obtenerSQL(query);
        st = new StringStatement(sql, true);

        try {

            st.setString(1, empresa);
            st.setInt(2, idCab);
            st.setInt(3, respuesta.get("id_actividad").getAsInt());
            st.setInt(4, respuesta.get("id_apu").getAsInt());
            st.setInt(5, respuesta.get("id_insumo").getAsInt());
            st.setDouble(6, respuesta.get("cantidad_insumo").getAsDouble());
            st.setDouble(7, respuesta.get("rendimiento_insumo").getAsDouble());
            st.setDouble(8, respuesta.get("cantidad_apu").getAsDouble());
            st.setDouble(9, respuesta.get("valor_insumo").getAsDouble());
            st.setString(10, usuario);
            st.setInt(11, respuesta.get("estado").getAsInt());

            if ((respuesta.get("costoper").getAsDouble() >= 0) && (respuesta.get("costoper").getAsDouble() < 1)) {
                st.setDouble(12, respuesta.get("valor_insumo").getAsDouble());
            } else {
                st.setDouble(12, respuesta.get("costoper").getAsDouble());
            }

            st.setDouble(13, respuesta.get("porcesq").getAsDouble());
            st.setDouble(14, respuesta.get("valoresq").getAsDouble());
            st.setDouble(15, respuesta.get("porccont").getAsDouble());
            st.setDouble(16, respuesta.get("valorcont").getAsDouble());
            st.setDouble(17, respuesta.get("valortotal").getAsDouble());
            st.setInt(18, respuesta.get("id_unidad_medida").getAsInt());

            cadena += st.getSql();

        } catch (Exception e) {
            System.out.println("Error en InsertarDetCotizacionDet (insertarDetalleCotizacionVal)" + e.toString());
            e.printStackTrace();
        }
        return cadena;

    }

    @Override
    public boolean existeCotizacion(String id) {
        con = null;
        ps = null;
        rs = null;
        boolean resp = false;
        query = "ExisteCotizacion";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);

            ps.setInt(1, Integer.parseInt(id));

            rs = ps.executeQuery();

            if (rs.next()) {
                resp = true;
            }
        } catch (Exception e) {
            try {
                resp = false;
                throw new SQLException("ERROR existeCotizacion \n" + e.getMessage());
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
        }
        return resp;
    }

    @Override
    public String datosExistecabecera(String id) {
        try {
            obj = new JsonObject();
            query = "ConsultaDatosExisteCabecera";
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, id);
            rs = ps.executeQuery();

            JsonObject fila = new JsonObject();
            while (rs.next()) {
                /*fila.addProperty("id_cliente", rs.getString("id_cliente"));
                 fila.addProperty("nomcli", rs.getString("nomcli"));
                 fila.addProperty("serie_cot", rs.getString("serie_cot"));*/
                fila.addProperty("id", rs.getString("id"));
                fila.addProperty("id_accion", rs.getString("id_accion"));
                fila.addProperty("no_cotizacion", rs.getString("no_cotizacion"));
                fila.addProperty("cod_cli", rs.getString("cod_cli"));
                fila.addProperty("nonmbre_cliente", rs.getString("nonmbre_cliente"));
                fila.addProperty("vigencia_cotizacion", rs.getString("vigencia_cotizacion"));
                fila.addProperty("forma_visualizacion", rs.getString("forma_visualizacion"));
                fila.addProperty("modalidad_comercial", rs.getString("modalidad_comercial"));
                fila.addProperty("material", rs.getString("material"));
                fila.addProperty("mano_obra", rs.getString("mano_obra"));
                fila.addProperty("equipos", rs.getString("equipos"));
                fila.addProperty("herramientas", rs.getString("herramientas"));
                fila.addProperty("transporte", rs.getString("transporte"));
                fila.addProperty("tramites", rs.getString("tramites"));
                fila.addProperty("valor_cotizacion", rs.getString("valor_cotizacion"));
                fila.addProperty("valor_descuento", rs.getString("valor_descuento"));
                fila.addProperty("subtotal", rs.getString("subtotal"));
                fila.addProperty("perc_iva", rs.getString("perc_iva"));
                fila.addProperty("valor_iva", rs.getString("valor_iva"));
                fila.addProperty("administracion", rs.getString("administracion"));
                fila.addProperty("imprevisto", rs.getString("imprevisto"));
                fila.addProperty("utilidad", rs.getString("utilidad"));
                fila.addProperty("perc_aiu", rs.getString("perc_aiu"));
                fila.addProperty("valor_aiu", rs.getString("valor_aiu"));
                fila.addProperty("perc_administracion", rs.getString("perc_administracion"));
                fila.addProperty("perc_imprevisto", rs.getString("perc_imprevisto"));
                fila.addProperty("perc_utilidad", rs.getString("perc_utilidad"));
                fila.addProperty("total", rs.getString("total"));
                fila.addProperty("anticipo", rs.getString("anticipo"));
                fila.addProperty("perc_anticipo", rs.getString("perc_anticipo"));
                fila.addProperty("valor_anticipo", rs.getString("valor_anticipo"));
                fila.addProperty("retegarantia", rs.getString("retegarantia"));
                fila.addProperty("perc_retegarantia", rs.getString("perc_retegarantia"));
                fila.addProperty("existe", rs.getString("existe"));

            }
            obj = fila;

        } catch (Exception ex) {
            throw new SQLException("ERROR OBTENIENDO LA CABECERA: cargardatoscabecera(String id) " + ex.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception ex) {
            }
            return gson.toJson(obj);
        }
    }

    @Override
    public JsonObject cargarGridAccionExiste(String accion) {
        JsonObject respuesta = new JsonObject();
        JsonArray arr = new JsonArray();
        JsonObject object = new JsonObject();
        Connection con = null;
        String sql = "";
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SL_COTIZACION_WBS_PRESUPUESTO";
//        String query = "ConsultaDatosExisteDetalle";

        try {
            con = this.conectarJNDI(query);
//            sql = this.obtenerSQL(query).replace("#filtro3", "2");
//            sql = sql.replace("#filtro2", accion);

            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, accion);
            rs = ps.executeQuery();

            while (rs.next()) {
                object = new JsonObject();

                //object.addProperty("id", rs.getString("id"));
                object.addProperty("metodocalculo", rs.getString("metodocalculo"));
                object.addProperty("tipoinsumo", rs.getString("nombre_insumo"));
                object.addProperty("id_tipo_insumo", rs.getString("id_tipo_insumo"));
                object.addProperty("codinsumo", rs.getString("codigo_material"));
                object.addProperty("descripcionins", rs.getString("descripcion"));
                object.addProperty("cantidad", rs.getString("cantidad"));
                object.addProperty("unidadmedida", rs.getString("nombre_unidad"));
                object.addProperty("idunidadmedida", rs.getString("id_unidad_medida"));
                object.addProperty("pareto", rs.getString("pareto"));
                object.addProperty("preciounitario", rs.getString("precio_unitario"));
                object.addProperty("preciototal", rs.getString("preciototal"));
                object.addProperty("proveedor", rs.getString("proveedor"));
                object.addProperty("nitproveedor", rs.getString("nit_proveedor"));
                object.addProperty("preciohistorico", rs.getString("preciohistorico"));
                object.addProperty("preciounitariofinal", rs.getString("preciounitariofinal"));
                object.addProperty("totalfinal", rs.getString("totalfinal"));
                object.addProperty("rentabilidadprov", rs.getString("rentabilidadprov"));
                object.addProperty("valrentabilidadprov", rs.getString("valrentabilidadprov"));
                object.addProperty("rentabilidadsel", rs.getString("rentabilidadsel"));
                object.addProperty("valrentabilidadsel", rs.getString("valrentabilidadsel"));
                object.addProperty("totalcliente", rs.getString("totalcliente"));
                object.addProperty("id_insumo", rs.getString("id_insumo"));
                object.addProperty("estado", rs.getString("estado"));

                arr.add(object);
            }

            //System.out.println(new Gson().toJson(arr));

            respuesta.add("rows", arr);
        } catch (Exception exc) {
            respuesta = new JsonObject();
            respuesta.addProperty("error", exc.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception ex) {
            }
            return respuesta;
        }
    }

    public String UpdateDetalleCotizacion(JsonObject respuesta, String usuario, String empresa) throws SQLException {

        String cadena = "";
        String query = "UpdateDetCotizacion";
        String sql = "";
        StringStatement st = null;
        sql = this.obtenerSQL(query);
        st = new StringStatement(sql, true);

        try {

            st.setInt(1, respuesta.get("id_tipo_insumo").getAsInt());
            st.setInt(2, respuesta.get("id_insumo").getAsInt());
            st.setString(3, respuesta.get("descripcionins").getAsString());
            st.setString(4, respuesta.get("idunidadmedida").getAsString());
            st.setString(5, respuesta.get("nitproveedor").getAsString());
            st.setString(6, respuesta.get("cantidad").getAsString());
            st.setDouble(7, respuesta.get("preciounitario").getAsDouble());
            st.setDouble(8, respuesta.get("preciohistorico").getAsDouble());
            st.setDouble(9, respuesta.get("pareto").getAsDouble());
            st.setDouble(10, respuesta.get("preciounitariofinal").getAsDouble());
            st.setDouble(11, respuesta.get("rentabilidadprov").getAsDouble());
            st.setDouble(12, respuesta.get("valrentabilidadprov").getAsDouble());
            st.setDouble(13, respuesta.get("rentabilidadsel").getAsDouble());
            st.setDouble(14, respuesta.get("valrentabilidadsel").getAsDouble());
            st.setDouble(15, respuesta.get("totalfinal").getAsDouble());
            st.setDouble(16, respuesta.get("totalcliente").getAsDouble());
            st.setString(17, usuario);
            st.setInt(18, respuesta.get("id").getAsInt());
            cadena += st.getSql();

        } catch (Exception e) {
            System.out.println("Error en UpdateDetalleCotizacion (UpdateDetalleCotizacion)" + e.toString());
            e.printStackTrace();
        }
        return cadena;

    }

    @Override
    public JsonObject cargarTablaDetallada(String id_accion) {
        JsonObject respuesta = new JsonObject();
        JsonArray arr = new JsonArray();
        JsonObject object = new JsonObject();;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = "";
        String query = "CargarTablaDetallada";

        try {
            con = this.conectarJNDI(query);
            sql = this.obtenerSQL(query);

            ps = con.prepareStatement(sql);

            ps.setInt(1, Integer.parseInt(id_accion));

            rs = ps.executeQuery();

            while (rs.next()) {
                object = new JsonObject();
                object.addProperty("id", rs.getString("id"));
                object.addProperty("id_actividad", rs.getString("id_actividad"));
                object.addProperty("id_apu", rs.getString("id_apu"));
                object.addProperty("id_insumo", rs.getString("id_insumo"));
                object.addProperty("cantidad_insumo", rs.getString("cantidad_insumo"));
                object.addProperty("rendimiento_insumo", rs.getString("rendimiento_insumo"));
                object.addProperty("cantidad_apu", rs.getString("cantidad_apu"));
                object.addProperty("valor_insumo", rs.getString("valor_insumo"));
                object.addProperty("estado", rs.getString("estado"));
                
                object.addProperty("costoper", rs.getString("costo_personalizado"));
                object.addProperty("porcesq", rs.getString("perc_esquema"));
                object.addProperty("valoresq", rs.getString("valor_esquema"));
                object.addProperty("porccont", rs.getString("perc_contratista"));
                object.addProperty("valorcont", rs.getString("valor_contratista"));
                object.addProperty("valortotal", rs.getString("gran_total"));
                object.addProperty("id_unidad_medida", rs.getString("id_unidad_medida"));
                arr.add(object);
            }

            ps = con.prepareStatement("select setval('opav.sl_conteo',1);");

            rs = ps.executeQuery();

            //System.out.println(new Gson().toJson(arr));

            respuesta.add("rows", arr);
        } catch (Exception exc) {
            respuesta = new JsonObject();
            respuesta.addProperty("error", exc.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception ex) {
            }
            return respuesta;
        }
    }

    @Override
    public JsonObject cargarTablaDetalladaExiste(String id_accion) {
        JsonObject respuesta = new JsonObject();
        JsonArray arr = new JsonArray();
        JsonObject object = new JsonObject();;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = "";
        String query = "CargarTablaDetalladaExiste";

        try {
            con = this.conectarJNDI(query);
            sql = this.obtenerSQL(query);

            ps = con.prepareStatement(sql);

            ps.setInt(1, Integer.parseInt(id_accion));
            ps.setInt(2, Integer.parseInt(id_accion));

            rs = ps.executeQuery();

            while (rs.next()) {
                object = new JsonObject();
                object.addProperty("id", rs.getString("id"));
                object.addProperty("id_actividad", rs.getString("id_actividad"));
                object.addProperty("id_apu", rs.getString("id_apu"));
                object.addProperty("id_insumo", rs.getString("id_insumo"));
                object.addProperty("cantidad_insumo", rs.getString("cantidad_insumo"));
                object.addProperty("rendimiento_insumo", rs.getString("rendimiento_insumo"));
                object.addProperty("cantidad_apu", rs.getString("cantidad_apu"));
                object.addProperty("valor_insumo", rs.getString("valor_insumo"));
                object.addProperty("estado", rs.getString("estado"));

                object.addProperty("costoper", rs.getString("costo_personalizado"));
                object.addProperty("porcesq", rs.getString("perc_esquema"));
                object.addProperty("valoresq", rs.getString("valor_esquema"));
                object.addProperty("porccont", rs.getString("perc_contratista"));
                object.addProperty("valorcont", rs.getString("valor_contratista"));
                object.addProperty("valortotal", rs.getString("gran_total"));
                object.addProperty("id_unidad_medida", rs.getString("id_unidad_medida"));
                arr.add(object);
            }

            ps = con.prepareStatement("select setval('opav.sl_conteo',1);");

            rs = ps.executeQuery();

            //System.out.println(new Gson().toJson(arr));

            respuesta.add("rows", arr);
        } catch (Exception exc) {
            respuesta = new JsonObject();
            respuesta.addProperty("error", exc.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception ex) {
            }
            return respuesta;
        }
    }

    @Override
    public String cargarCostosAdmon(String Query, String id_categoria, String num_solicitud) {
        Gson gson = new Gson();
        con = null;
        ps = null;
        rs = null;
        JsonObject obj = new JsonObject();

        JsonArray arr = new JsonArray();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(Query));
            ps.setInt(1, Integer.parseInt(id_categoria));
            if (Query.equals("cargarCostosAdmonProyectoByCat")) {
                ps.setString(2, num_solicitud);
            }
            rs = ps.executeQuery();
            JsonObject fila = null;
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("id", rs.getString("id"));
                fila.addProperty("id_categoria", rs.getString("id_categoria"));
                fila.addProperty("id_item", rs.getString("id_item"));
                fila.addProperty("descripcion", rs.getString("descripcion"));
                fila.addProperty("cantidad", rs.getString("cantidad"));
                fila.addProperty("id_unidad_medicion", rs.getString("id_unidad"));
                fila.addProperty("unidad_medicion", rs.getString("unidad"));
                fila.addProperty("duracion", rs.getString("duracion"));
                fila.addProperty("porc_aplicacion", rs.getString("porc_aplicacion"));
                fila.addProperty("valor_item", rs.getString("valor_item"));
                fila.addProperty("valor_total", rs.getString("valor_total"));

                arr.add(fila);
            }
            obj.add("rows", arr);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return gson.toJson(obj);
        }
    }

    @Override
    public String listarUndsAdmon() {
        con = null;
        rs = null;
        ps = null;

        query = "SQL_CARGAR_UNIDADES_ADMON";
        String respuestaJson = "{}";

        Gson gson = new Gson();
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);

            rs = ps.executeQuery();
            JsonObject fila = new JsonObject();
            while (rs.next()) {
                fila.addProperty(rs.getString("id"), rs.getString("valor"));
            }
            respuestaJson = gson.toJson(fila);

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return respuestaJson;
    }

    @Override
    public String listarItemsCategoria(String id_categoria) {
        con = null;
        rs = null;
        ps = null;

        query = "SQL_CARGAR_ITEMS_CATEGORIA";
        String respuestaJson = "{}";

        Gson gson = new Gson();
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);
            ps.setInt(1, Integer.parseInt(id_categoria));
            rs = ps.executeQuery();
            JsonObject fila = new JsonObject();
            while (rs.next()) {
                fila.addProperty(rs.getString("id"), rs.getString("valor"));
            }
            respuestaJson = gson.toJson(fila);

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return respuestaJson;
    }

    @Override
    public String guardarCostosAdmon(String num_solicitud, String id_categoria, JsonObject infoItems, Usuario usuario) {

        String respuestaJson = "{}";
        JsonObject respuesta = new JsonObject();
        TransaccionService stmt = new TransaccionService(usuario.getBd());
        try {

            stmt.crearStatement();

            //Insertamos en tabla costos de administracion proyecto
            JsonArray arrItems = infoItems.getAsJsonArray("items");
            for (int i = 0; i < arrItems.size(); i++) {
                respuesta = arrItems.get(i).getAsJsonObject();
                String id_item = respuesta.get("id_item").getAsString();
                String cantidad = respuesta.get("cantidad").getAsString();
                String id_und_medicion = respuesta.get("id_unidad_medicion").getAsString();
                String duracion = respuesta.get("duracion").getAsString();
                String porc_aplicacion = respuesta.get("porc_aplicacion").getAsString();
                String valor_item = respuesta.get("valor_item").getAsString();
                String valor_total = respuesta.get("valor_total").getAsString();
                if (respuesta.get("id").getAsString().startsWith("neo_")) {
                    stmt.getSt().addBatch(insertarCostosAdmom(num_solicitud, id_categoria, id_item, cantidad, id_und_medicion, duracion, porc_aplicacion, valor_item, valor_total, usuario));
                } else {
                    stmt.getSt().addBatch(actualizarCostosAdmon(respuesta.get("id").getAsString(), num_solicitud, id_categoria, id_item, cantidad, id_und_medicion, duracion, porc_aplicacion, valor_item, valor_total, usuario));
                }

            }

            stmt.execute();
            stmt.closeAll();
            respuestaJson = "{\"respuesta\":\"OK\"}";
        } catch (Exception e) {
            e.printStackTrace();
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                stmt.closeAll();
            } catch (Exception ex) {
            }

            return respuestaJson;
        }

    }

    public String insertarCostosAdmom(String num_solicitud, String id_categoria, String id_item, String cantidad, String id_und_medicion, String duracion,
            String porc_aplicacion, String valor_item, String valor_total, Usuario usuario) throws Exception {
        String respuesta = "";
        st = null;
        query = "insertarCostosAdmom";
        try {
            st = new StringStatement(this.obtenerSQL(query), true);
            st.setString(1, num_solicitud);
            st.setInt(2, Integer.parseInt(id_categoria));
            st.setInt(3, Integer.parseInt(id_item));
            st.setDouble(4, Double.parseDouble(cantidad));
            st.setInt(5, Integer.parseInt(id_und_medicion));
            st.setDouble(6, Double.parseDouble(duracion));
            st.setDouble(7, Double.parseDouble(porc_aplicacion));
            st.setDouble(8, Double.parseDouble(valor_item));
            st.setDouble(9, Double.parseDouble(valor_total));
            st.setString(10, usuario.getLogin());
            st.setString(11, usuario.getDstrct());
            respuesta = st.getSql();
        } catch (Throwable e) {
            e.printStackTrace();
            throw new Exception("ERROR DURANTE insertarCostosAdmom. \n " + e.getMessage());
        }
        return respuesta;

    }

    public String actualizarCostosAdmon(String id, String num_solicitud, String id_categoria, String id_item, String cantidad, String id_und_medicion, String duracion,
            String porc_aplicacion, String valor_item, String valor_total, Usuario usuario) throws Exception {
        st = null;
        String respuesta = "";
        query = "actualizarCostosAdmon";

        try {

            st = new StringStatement(this.obtenerSQL(query), true);
            st.setInt(1, Integer.parseInt(id_item));
            st.setDouble(2, Double.parseDouble(cantidad));
            st.setInt(3, Integer.parseInt(id_und_medicion));
            st.setDouble(4, Double.parseDouble(duracion));
            st.setDouble(5, Double.parseDouble(porc_aplicacion));
            st.setDouble(6, Double.parseDouble(valor_item));
            st.setDouble(7, Double.parseDouble(valor_total));
            st.setString(8, usuario.getLogin());
            st.setInt(9, Integer.parseInt(id));

            respuesta = st.getSql();

        } catch (Throwable e) {
            e.printStackTrace();
            System.out.println(e.toString());
            throw new SQLException("ERROR DURANTE actualizarCostosAdmon. \n " + e.getMessage());
        }

        return respuesta;

    }

    @Override
    public String guardarCostosAdmon(String num_solicitud, JsonObject infoItems, Usuario usuario) {

        String respuestaJson = "{}";
        JsonObject respuesta = new JsonObject();
        TransaccionService stmt = new TransaccionService(usuario.getBd());
        try {

            stmt.crearStatement();

            //Insertamos en tabla costos de administracion proyecto
            JsonArray arrItems = infoItems.getAsJsonArray("items");
            for (int i = 0; i < arrItems.size(); i++) {
                JsonArray jsonArr = arrItems.get(i).getAsJsonArray();
                for (int j = 0; j < jsonArr.size(); j++) {
                    respuesta = jsonArr.get(j).getAsJsonObject();
                    String id_categoria = respuesta.get("id_categoria").getAsString();
                    String id_item = respuesta.get("id_item").getAsString();
                    String cantidad = respuesta.get("cantidad").getAsString();
                    String id_und_medicion = respuesta.get("id_unidad_medicion").getAsString();
                    String duracion = respuesta.get("duracion").getAsString();
                    String porc_aplicacion = respuesta.get("porc_aplicacion").getAsString();
                    String valor_item = respuesta.get("valor_item").getAsString();
                    String valor_total = respuesta.get("valor_total").getAsString();
                    if (respuesta.get("id").getAsString().startsWith("neo_")) {
                        stmt.getSt().addBatch(insertarCostosAdmom(num_solicitud, id_categoria, id_item, cantidad, id_und_medicion, duracion, porc_aplicacion, valor_item, valor_total, usuario));
                    } else {
                        stmt.getSt().addBatch(actualizarCostosAdmon(respuesta.get("id").getAsString(), num_solicitud, id_categoria, id_item, cantidad, id_und_medicion, duracion, porc_aplicacion, valor_item, valor_total, usuario));
                    }
                }

            }

            stmt.execute();
            respuestaJson = "{\"respuesta\":\"OK\"}";
        } catch (Exception e) {
            e.printStackTrace();
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                stmt.closeAll();
            } catch (Exception ex) {
            }

            return respuestaJson;
        }

    }

    @Override
    public boolean existenCostosAdmonProyecto(String num_solicitud) {
        con = null;
        rs = null;
        ps = null;

        query = "cargarCostosAdmonProyecto";
        boolean resp = false;

        Gson gson = new Gson();
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);
            ps.setString(1, num_solicitud);
            rs = ps.executeQuery();
            while (rs.next()) {
                resp = true;
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return resp;
    }

    @Override
    public String cargarMetodoCalculo_w() {
        try {
            obj = new JsonObject();
            query = "ConsultarMetodosCal_w";
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();

            JsonObject fila = new JsonObject();
            while (rs.next()) {
                fila.addProperty(rs.getString("codigo"), rs.getString("nombre"));
            }
            obj = fila;

        } catch (Exception ex) {
            throw new SQLException("ERROR OBTENIENDO TIPOS DE MATERIAL: cargarTiposMaterial()  " + ex.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception ex) {
            }
            return gson.toJson(obj);
        }

    }

    @Override
    public String cargarMetodoCalculoXLinea_w(String metodo, String codigo) {
        try {
            obj = new JsonObject();
            query = "ConsultarMetodosCalXLinea_w";
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setInt(1, Integer.parseInt(metodo));
            ps.setString(2, codigo);

            rs = ps.executeQuery();

            JsonObject fila = new JsonObject();
            while (rs.next()) {
                fila.addProperty("valor", rs.getString("valor"));
            }
            obj = fila;

        } catch (Exception ex) {
            throw new SQLException("ERROR OBTENIENDO TIPOS DE MATERIAL: cargarTiposMaterial()  " + ex.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception ex) {
            }
            return gson.toJson(obj);
        }
    }

    @Override
    public boolean existeCotizacion_w(String id) {
        con = null;
        ps = null;
        rs = null;
        boolean resp = false;
        query = "ExisteCotizacion_w";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);

            ps.setInt(1, Integer.parseInt(id));

            rs = ps.executeQuery();

            if (rs.next()) {
                resp = true;
            }
        } catch (Exception e) {
            try {
                resp = false;
                throw new SQLException("ERROR existeCotizacion \n" + e.getMessage());
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
        }
        return resp;
    }

    @Override
    public JsonObject cargarGridAccion_w(String apugrupo, String metcalc, String id) {
        JsonObject respuesta = new JsonObject();
        JsonArray arr = new JsonArray();
        JsonObject object = new JsonObject();
        Connection con = null;
        String sql = "";
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "ConsultaDatosAccion_w";

        try {
            con = this.conectarJNDI(query);
            sql = this.obtenerSQL(query);

            sql = sql.replace("#filtro2", id);
            sql = sql.replace("#filtro3", metcalc);

            ps = con.prepareStatement(sql);
            //ps.setInt(1, Integer.parseInt(ids));
            rs = ps.executeQuery();

            while (rs.next()) {
                object = new JsonObject();
                object.addProperty("metodocalculo", rs.getString("metodocalculo"));
                object.addProperty("tipoinsumo", rs.getString("nombre_insumo"));
                object.addProperty("id_tipo_insumo", rs.getString("id_tipo_insumo"));
                object.addProperty("codinsumo", rs.getString("codigo_material"));
                object.addProperty("descripcionins", rs.getString("descripcion"));
                object.addProperty("cantidad", rs.getString("cantidad"));
                object.addProperty("unidadmedida", rs.getString("nombre_unidad"));
                object.addProperty("idunidadmedida", rs.getString("id_unidad_medida"));
                object.addProperty("pareto", rs.getString("pareto"));
                object.addProperty("preciounitario", rs.getString("precio_unitario"));
                object.addProperty("preciototal", rs.getString("preciototal"));
                object.addProperty("proveedor", rs.getString("proveedor"));
                object.addProperty("nitproveedor", rs.getString("nit_proveedor"));
                object.addProperty("preciohistorico", rs.getString("preciohistorico"));
                object.addProperty("preciounitariofinal", rs.getString("preciounitariofinal"));
                object.addProperty("totalfinal", rs.getString("totalfinal"));
                object.addProperty("rentabilidadprov", rs.getString("rentabilidadprov"));
                object.addProperty("valrentabilidadprov", rs.getString("valrentabilidadprov"));
                object.addProperty("rentabilidadsel", rs.getString("rentabilidadsel"));
                object.addProperty("valrentabilidadsel", rs.getString("valrentabilidadsel"));
                object.addProperty("totalcliente", rs.getString("totalcliente"));
                object.addProperty("id_insumo", rs.getString("id_insumo"));
                object.addProperty("estado", "0");
                arr.add(object);
            }

            //System.out.println(new Gson().toJson(arr));

            respuesta.add("rows", arr);
        } catch (Exception exc) {
            respuesta = new JsonObject();
            respuesta.addProperty("error", exc.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception ex) {
            }
            return respuesta;
        }
    }

    @Override
    public JsonObject cargarGridAccionExiste_w(String accion, String op, String filtro, String id_rel_actividades_apu) {
        JsonObject respuesta = new JsonObject();
        JsonArray arr = new JsonArray();
        JsonObject object = new JsonObject();
        Connection con = null;
        String sql = "";
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "ConsultaDatosExisteDetalle_w";
        

        String whe = "";

        if (op.equals("1")) {
            whe = " and b.id in(" + filtro + ") ";
        } else {
            if (op.equals("2")) {
                whe = " and c.id in(" + filtro + ") ";
            } else {
                if (op.equals("3")) {
                    whe = " and d.id in(" + filtro + ") ";
                } else {
                    if (op.equals("4")) {
                        whe = " and e.id in(" + filtro + ") ";
                    }
                }
            }
        }

        try {
            con = this.conectarJNDI(query);
            sql = this.obtenerSQL(query).replace("#filtro3", "2");
            sql = sql.replace("#filtro2", accion);
            sql = sql.replace("#filtro4", whe);
            sql = sql.replace("#filtro5", id_rel_actividades_apu);

            ps = con.prepareStatement(sql);
            //ps.setString(1, accion);
            rs = ps.executeQuery();

            while (rs.next()) {
                object = new JsonObject();

                //object.addProperty("id", rs.getString("id"));
                object.addProperty("metodocalculo", rs.getString("metodocalculo"));
                object.addProperty("tipoinsumo", rs.getString("nombre_insumo"));
                object.addProperty("id_tipo_insumo", rs.getString("id_tipo_insumo"));
                object.addProperty("codinsumo", rs.getString("codigo_material"));
                object.addProperty("descripcionins", rs.getString("descripcion"));
                object.addProperty("cantidad", rs.getString("cantidad"));
                object.addProperty("unidadmedida", rs.getString("nombre_unidad"));
                object.addProperty("idunidadmedida", rs.getString("id_unidad_medida"));
                object.addProperty("pareto", rs.getString("pareto"));
                object.addProperty("preciounitario", rs.getString("precio_unitario"));
                object.addProperty("preciototal", rs.getString("preciototal"));
                object.addProperty("proveedor", rs.getString("proveedor"));
                object.addProperty("nitproveedor", rs.getString("nit_proveedor"));
                object.addProperty("preciohistorico", rs.getString("preciohistorico"));
                object.addProperty("preciounitariofinal", rs.getString("preciounitariofinal"));

                object.addProperty("rentabilidadprov", rs.getString("rentabilidadprov"));
                object.addProperty("valrentabilidadprov", rs.getString("valrentabilidadprov"));
                object.addProperty("rentabilidadsel", rs.getString("rentabilidadsel"));
                object.addProperty("valrentabilidadsel", rs.getString("valrentabilidadsel"));
                object.addProperty("totalcliente", rs.getString("totalcliente"));
                object.addProperty("id_insumo", rs.getString("id_insumo"));
                object.addProperty("estado", rs.getString("estado"));

                //si el costo perzonalizado viene en 0 se le ingresa el valor del precio unitario
                if (rs.getString("costo_personalizado").startsWith("0.0")) {
                    object.addProperty("costoperso", rs.getString("preciounitariofinal"));
                    object.addProperty("totalfinal", Double.parseDouble(rs.getString("preciounitariofinal")) * Double.parseDouble(rs.getString("cantidad")));
                } else {
                    object.addProperty("costoperso", rs.getString("costo_personalizado"));
                    object.addProperty("totalfinal", Double.parseDouble(rs.getString("costo_personalizado")) * Double.parseDouble(rs.getString("cantidad")));

                }

                object.addProperty("percesq", rs.getString("perc_esquema"));
                object.addProperty("valesq", rs.getString("valor_esquema"));
                object.addProperty("perccont", rs.getString("perc_contratista"));
                object.addProperty("valcont", rs.getString("valor_contratista"));
                object.addProperty("valortotal", rs.getString("gran_total"));

                arr.add(object);
            }

            //System.out.println(new Gson().toJson(arr));

            respuesta.add("rows", arr);
        } catch (Exception exc) {
            respuesta = new JsonObject();
            respuesta.addProperty("error", exc.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception ex) {
            }
            return respuesta;
        }
    }

    @Override
    public String cargardatoscabecera_w(String id) {
        try {
            obj = new JsonObject();
            query = "ConsultaDatosCabecera_w";
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, id);
            rs = ps.executeQuery();

            JsonObject fila = new JsonObject();
            while (rs.next()) {
                fila.addProperty("id_cliente", rs.getString("id_cliente"));
                fila.addProperty("nomcli", rs.getString("nomcli"));
                fila.addProperty("serie_cot", rs.getString("serie_cot"));
                fila.addProperty("existe", rs.getString("existe"));
            }
            obj = fila;

        } catch (Exception ex) {
            throw new SQLException("ERROR OBTENIENDO LA CABECERA: cargardatoscabecera(String id) " + ex.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception ex) {
            }
            return gson.toJson(obj);
        }
    }

    @Override
    public String datosExistecabecera_w(String id) {
        try {
            obj = new JsonObject();
            query = "ConsultaDatosExisteCabecera_w";
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, id);
            rs = ps.executeQuery();

            JsonObject fila = new JsonObject();
            while (rs.next()) {
                /*fila.addProperty("id_cliente", rs.getString("id_cliente"));
                 fila.addProperty("nomcli", rs.getString("nomcli"));
                 fila.addProperty("serie_cot", rs.getString("serie_cot"));*/
                fila.addProperty("id", rs.getString("id"));
                fila.addProperty("id_accion", rs.getString("id_accion"));
                fila.addProperty("no_cotizacion", rs.getString("no_cotizacion"));
                fila.addProperty("cod_cli", rs.getString("cod_cli"));
                fila.addProperty("nonmbre_cliente", rs.getString("nonmbre_cliente"));
                fila.addProperty("vigencia_cotizacion", rs.getString("vigencia_cotizacion"));
                fila.addProperty("forma_visualizacion", rs.getString("forma_visualizacion"));
                fila.addProperty("modalidad_comercial", rs.getString("modalidad_comercial"));
                fila.addProperty("material", rs.getString("material"));
                fila.addProperty("mano_obra", rs.getString("mano_obra"));
                fila.addProperty("equipos", rs.getString("equipos"));
                fila.addProperty("herramientas", rs.getString("herramientas"));
                fila.addProperty("transporte", rs.getString("transporte"));
                fila.addProperty("tramites", rs.getString("tramites"));
                fila.addProperty("valor_cotizacion", rs.getString("valor_cotizacion"));
                fila.addProperty("valor_descuento", rs.getString("valor_descuento"));
                fila.addProperty("subtotal", rs.getString("subtotal"));
                fila.addProperty("perc_iva", rs.getString("perc_iva"));
                fila.addProperty("valor_iva", rs.getString("valor_iva"));
                fila.addProperty("administracion", rs.getString("administracion"));
                fila.addProperty("imprevisto", rs.getString("imprevisto"));
                fila.addProperty("utilidad", rs.getString("utilidad"));
                fila.addProperty("perc_aiu", rs.getString("perc_aiu"));
                fila.addProperty("valor_aiu", rs.getString("valor_aiu"));
                fila.addProperty("perc_administracion", rs.getString("perc_administracion"));
                fila.addProperty("perc_imprevisto", rs.getString("perc_imprevisto"));
                fila.addProperty("perc_utilidad", rs.getString("perc_utilidad"));
                fila.addProperty("total", rs.getString("total"));
                fila.addProperty("anticipo", rs.getString("anticipo"));
                fila.addProperty("perc_anticipo", rs.getString("perc_anticipo"));
                fila.addProperty("valor_anticipo", rs.getString("valor_anticipo"));
                fila.addProperty("retegarantia", rs.getString("retegarantia"));
                fila.addProperty("perc_retegarantia", rs.getString("perc_retegarantia"));
                fila.addProperty("existe", rs.getString("existe"));
                fila.addProperty("distribucion_rentabilidad_esquema", rs.getString("distribucion_rentabilidad_esquema"));

            }
            obj = fila;

        } catch (Exception ex) {
            throw new SQLException("ERROR OBTENIENDO LA CABECERA: cargardatoscabecera(String id) " + ex.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception ex) {
            }
            return gson.toJson(obj);
        }
    }

    @Override
    public JsonObject cargarAPUs_w() {
        JsonObject respuesta = new JsonObject(), fila;
        JsonArray arr = new JsonArray();
        JsonObject object = new JsonObject();;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "ConsultaAPus_w";

        try {
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();

            while (rs.next()) {
                object = new JsonObject();
                object.addProperty("id", rs.getString("id"));
                object.addProperty("nombreapu", rs.getString("nombre"));
                object.addProperty("nombre_unidad", rs.getString("nombre_unidad"));
                arr.add(object);
            }

            //System.out.println(new Gson().toJson(arr));

            respuesta.add("rows", arr);
        } catch (Exception exc) {
            respuesta = new JsonObject();
            respuesta.addProperty("error", exc.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception ex) {
            }
            return respuesta;
        }
    }

    @Override
    public JsonObject cargarAPUXGrupo_w(String grupo_apu, String apugrupo) {
        JsonObject respuesta = new JsonObject();
        JsonArray arr = new JsonArray();
        JsonObject object = new JsonObject();;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = "";
        String query = "ConsultarAPUXGrupo_w";

        try {
            con = this.conectarJNDI(query);
            sql = this.obtenerSQL(query);
            if (grupo_apu.equals("0")) {
                sql = sql.replace("#filtro", "");
            }

            if (!apugrupo.equals("")) {
                sql = sql.replace("#filtro", " and c.id_grupo_apu = " + grupo_apu + " and a.id not in(" + apugrupo + ")");
            }

            if (!grupo_apu.equals("0")) {
                sql = sql.replace("#filtro", " and c.id_grupo_apu = " + grupo_apu + "");
            }

            ps = con.prepareStatement(sql);

            rs = ps.executeQuery();

            while (rs.next()) {
                object = new JsonObject();
                object.addProperty("id", rs.getString("id"));
                object.addProperty("nombreapu", rs.getString("nombre"));
                object.addProperty("nombre_unidad", rs.getString("nombre_unidad"));
                arr.add(object);
            }

            //System.out.println(new Gson().toJson(arr));

            respuesta.add("rows", arr);
        } catch (Exception exc) {
            respuesta = new JsonObject();
            respuesta.addProperty("error", exc.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }   
            } catch (Exception ex) {
            }
            return respuesta;
        }
    }

    @Override
    public JsonObject guardarCotizacion_w(JsonObject info, JsonObject info_detalle) {
        JsonObject respuesta = new JsonObject();
        con = null;
        ps = null;
        String mensaje = "";
        Statement stmt = null;

        try {
            JsonArray arr_det = info_detalle.getAsJsonArray("rows");
            PreparedStatement ps = null;
            con = this.conectarJNDI();
            con.setAutoCommit(false);
            stmt = con.createStatement();

            stmt.addBatch("delete from opav.sl_relacion_cotizacion_detalle_apu where id_cotizacion=" + info.get("id").getAsInt() + ";");

            for (int i = 0; i < arr_det.size(); i++) {
                respuesta = arr_det.get(i).getAsJsonObject();

                stmt.addBatch(insertarDetalleCotizacionVal(info.get("id").getAsInt(), respuesta, info.get("usuario").getAsString(), info.get("dstrct").getAsString()));
            }

            stmt.executeBatch();
            stmt.clearBatch();
            mensaje = "Cotizacion Actualizada exitosamente...";

            con.commit();

            respuesta = new JsonObject();

            respuesta.addProperty("mensaje", mensaje);

        } catch (Exception exc) {
            con.rollback();
            respuesta = new JsonObject();
            exc.getMessage();
            respuesta.addProperty("error", "Error, no puede registrar productos ya guardados");

        } finally {
            try {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException e) {
                        throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                    }
                }
                if (con != null) {
                    try {
                        con.setAutoCommit(true);
                        this.desconectar(con);
                    } catch (SQLException e) {
                        throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage());
                    }
                }
            } catch (Exception ex) {
            }
            return respuesta;
        }
    }

    @Override
    public JsonObject cargarTablaDetallada_w(String id_accion) {
        JsonObject respuesta = new JsonObject();
        JsonArray arr = new JsonArray();
        JsonObject object = new JsonObject();;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = "";
        String query = "CargarTablaDetallada_w";

        try {
            con = this.conectarJNDI(query);
            sql = this.obtenerSQL(query);

            ps = con.prepareStatement(sql);

            ps.setInt(1, Integer.parseInt(id_accion));

            rs = ps.executeQuery();

            while (rs.next()) {
                object = new JsonObject();
                object.addProperty("id", rs.getString("id"));
                object.addProperty("id_actividad", rs.getString("id_actividad"));
                object.addProperty("id_apu", rs.getString("id_apu"));
                object.addProperty("id_insumo", rs.getString("id_insumo"));
                object.addProperty("cantidad_insumo", rs.getString("cantidad_insumo"));
                object.addProperty("rendimiento_insumo", rs.getString("rendimiento_insumo"));
                object.addProperty("cantidad_apu", rs.getString("cantidad_apu"));
                object.addProperty("valor_insumo", rs.getString("valor_insumo"));
                object.addProperty("estado", rs.getString("estado"));
                arr.add(object);
            }

            ps = con.prepareStatement("select setval('opav.sl_conteo',1);");

            rs = ps.executeQuery();

            //System.out.println(new Gson().toJson(arr));

            respuesta.add("rows", arr);
        } catch (Exception exc) {
            respuesta = new JsonObject();
            respuesta.addProperty("error", exc.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception ex) {
            }
            return respuesta;
        }
    }

    @Override
    public JsonObject cargarTablaDetalladaExiste_w(String id_accion) {
        JsonObject respuesta = new JsonObject();
        JsonArray arr = new JsonArray();
        JsonObject object = new JsonObject();;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = "";
        String query = "CargarTablaDetalladaExiste_w";

        try {
            con = this.conectarJNDI(query);
            sql = this.obtenerSQL(query);

            ps = con.prepareStatement(sql);

            ps.setInt(1, Integer.parseInt(id_accion));

            rs = ps.executeQuery();

            while (rs.next()) {
                object = new JsonObject();
                object.addProperty("id", rs.getString("id"));
                object.addProperty("id_actividad", rs.getString("id_actividad"));
                object.addProperty("id_apu", rs.getString("id_apu"));
                object.addProperty("id_insumo", rs.getString("id_insumo"));
                object.addProperty("cantidad_insumo", rs.getString("cantidad_insumo"));
                object.addProperty("rendimiento_insumo", rs.getString("rendimiento_insumo"));
                object.addProperty("cantidad_apu", rs.getString("cantidad_apu"));
                object.addProperty("valor_insumo", rs.getString("valor_insumo"));
                object.addProperty("estado", rs.getString("estado"));

                object.addProperty("costoper", rs.getString("costo_personalizado"));
                object.addProperty("porcesq", rs.getString("perc_esquema"));
                object.addProperty("valoresq", rs.getString("valor_esquema"));
                object.addProperty("porccont", rs.getString("perc_contratista"));
                object.addProperty("valorcont", rs.getString("valor_contratista"));
                object.addProperty("valortotal", rs.getString("gran_total"));
                object.addProperty("id_unidad_medida", rs.getString("id_unidad_medida"));

                arr.add(object);
            }

            ps = con.prepareStatement("select setval('opav.sl_conteo',1);");

            rs = ps.executeQuery();

            //System.out.println(new Gson().toJson(arr));

            respuesta.add("rows", arr);
        } catch (Exception exc) {
            respuesta = new JsonObject();
            respuesta.addProperty("error", exc.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception ex) {
            }
            return respuesta;
        }
    }

    @Override
    public String cargar_Combo_W(String op) {
        con = null;
        rs = null;
        ps = null;
        switch (op) {
            case "1":
                query = "Cargar_Combo_Rent_Esquema";
                break;
            case "2":
                query = "Cargar_combo_conceptos";
                break;
            case "3":
                query = "Cargar_combo_indicador";
                break;
            default:
                throw new AssertionError();
        }

        String respuestaJson = "{}";

        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);

            rs = ps.executeQuery();

            while (rs.next()) {
                obj.addProperty(rs.getString("id"), rs.getString("descripcion"));

            }
            respuestaJson = gson.toJson(obj);

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return respuestaJson;
    }

    @Override
    public String cargar_Porcentajes(String idaccion) {
        Gson gson = new Gson();
        con = null;
        ps = null;
        rs = null;
        JsonObject obj = new JsonObject();
        String query = "CARGAR_PORCENTAJES";

        JsonArray arr = new JsonArray();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, idaccion);
            rs = ps.executeQuery();
            JsonObject fila = null;
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("id", rs.getString("id"));
                fila.addProperty("valor", rs.getString("valor"));
                fila.addProperty("descripcion", rs.getString("descripcion"));
                arr.add(fila);
            }
            obj.add("rows", arr);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return gson.toJson(obj);
        }
    }

    @Override
    public String anularCostoAdmonProyecto(String id, Usuario usuario) {
        con = null;
        ps = null;
        query = "anularCostoAdmon";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, usuario.getLogin());
            ps.setInt(2, Integer.parseInt(id));

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
            throw new SQLException("ERROR anularCostoAdmonProyecto \n" + e.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    @Override
    public String insertar_Porcentajes_Global_Contratista(String id, String porcentaje, String idaccion) {

        con = null;
        ps = null;
        query = "INSERTAR_PORCENTAJES";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            query = query.replace("#filtro1", porcentaje);
            ps = con.prepareStatement(query);
            ps.setInt(1, Integer.parseInt(id));
            ps.setString(2, idaccion);

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
            throw new SQLException("ERROR Al Insertar Porcentaje Global \n" + e.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    @Override
    public String insertar_rentabilidades_cabecera(String idaccion, String subtotal, String valor_contratista, String perc_contratista, String valor_esquema, String perc_esquema) {

        Double subtotall = Double.parseDouble(subtotal) + Double.parseDouble(valor_contratista) + Double.parseDouble(valor_esquema);
        con = null;
        ps = null;
        query = "INSERTAR_RENTABILIDADES_CABECERA";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setDouble(1, Double.parseDouble(perc_contratista));
            ps.setDouble(2, Double.parseDouble(valor_contratista));
            ps.setDouble(3, Double.parseDouble(perc_esquema));
            ps.setDouble(4, Double.parseDouble(valor_esquema));
            ps.setDouble(5, subtotall);
            ps.setDouble(6, Double.parseDouble(subtotal));
            ps.setString(7, idaccion);

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
            throw new SQLException("ERROR Al Insertar Porcentaje Global \n" + e.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    public String insertar_Porcentajes_Default(int idcotizacion) throws SQLException {

        String cadena = "";
        String query = "INSERTAR_PORCENTAJES_DEFAULT";
        String sql = "";
        StringStatement st = null;
        sql = this.obtenerSQL(query);
        st = new StringStatement(sql, true);

        try {
            st.setInt(1, idcotizacion);

            cadena += st.getSql();

        } catch (Exception e) {
            System.out.println("Error en InsertarDetCotizacionDet (insertarDetalleCotizacionVal)" + e.toString());
            e.printStackTrace();
        }
        return cadena;

    }

    @Override
    public String obtenerValorItemByDefault(String id_categoria, String id_item) {
        String respuestaJson = "{}";
        con = null;
        ps = null;
        rs = null;
        query = "OBTENER_VALOR_ITEM_DEFAULT";
        try {
            con = conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setInt(1, Integer.parseInt(id_categoria));
            ps.setInt(2, Integer.parseInt(id_item));
            rs = ps.executeQuery();
            if (rs.next()) {
                respuestaJson = "{\"respuesta\":\"" + rs.getString("valor") + "\"}";
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
        }

        return respuestaJson;

    }

    @Override
    public String insertar_Porcentajes_Contratista(JsonObject rows, String idaccion) {
        con = null;
        ps = null;
        
        String respuestaJson = "{}";
        JsonArray arr = rows.getAsJsonArray("rows");
        JsonObject res;

        try {
            con = this.conectarJNDI();
            
            for (int i = 0; i < arr.size(); i++) {

                res = arr.get(i).getAsJsonObject();

                
                query = this.obtenerSQL("INSERTAR_PORCENTAJES");
                query = query.replace("#filtro1", res.get("valor").getAsString());
                ps = con.prepareStatement(query);
                ps.setInt(1, res.get("id").getAsInt());
                ps.setString(2, idaccion);
                ps.executeUpdate();

            }
            
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
            throw new SQLException("ERROR Al Insertar Porcentaje Global \n" + e.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    @Override
    public String cargar_totales_categoria(String idaccion, String idtipoinsumo) {
        JsonObject respuesta = new JsonObject();
        JsonArray arr = new JsonArray();
        JsonObject object = new JsonObject();
        Connection con = null;
        String sql = "";
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "CARGAR_TOTALES_CATEGORIA";

        try {
            con = this.conectarJNDI(query);
            sql = this.obtenerSQL(query);
            ps = con.prepareStatement(sql);
            ps.setInt(1, Integer.parseInt(idaccion));
            ps.setInt(2, Integer.parseInt(idtipoinsumo));
            rs = ps.executeQuery();

            while (rs.next()) {
                object = new JsonObject();
                object.addProperty("id", rs.getString("id"));
                object.addProperty("descripcion", rs.getString("descripcion"));
                object.addProperty("total", rs.getString("total"));
                arr.add(object);
            }

            //System.out.println(new Gson().toJson(arr));

            respuesta.add("rows", arr);
        } catch (Exception exc) {
            respuesta = new JsonObject();
            respuesta.addProperty("error", exc.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception ex) {
            }
            return new Gson().toJson(respuesta);
        }
    }

    @Override
    public String cargar_totales_tipo_insumo(String idaccion) {
        JsonObject respuesta = new JsonObject();
        JsonArray arr = new JsonArray();
        JsonObject object = new JsonObject();
        Connection con = null;
        String sql = "";
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "CARGAR_TOTALES_TIPO_INSUMO";

        try {
            con = this.conectarJNDI(query);
            sql = this.obtenerSQL(query);
            ps = con.prepareStatement(sql);
            ps.setInt(1, Integer.parseInt(idaccion));
            rs = ps.executeQuery();

            while (rs.next()) {
                object = new JsonObject();
                object.addProperty("id", rs.getString("id"));
                object.addProperty("descripcion", rs.getString("descripcion"));
                object.addProperty("total", rs.getString("total"));
                arr.add(object);
            }

            //System.out.println(new Gson().toJson(arr));

            respuesta.add("rows", arr);
        } catch (Exception exc) {
            respuesta = new JsonObject();
            respuesta.addProperty("error", exc.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception ex) {
            }
            return new Gson().toJson(respuesta);
        }
    }

}
