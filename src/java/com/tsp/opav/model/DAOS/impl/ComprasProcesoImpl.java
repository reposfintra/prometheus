

package com.tsp.opav.model.DAOS.impl;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


import java.awt.Color;
import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.Date;
import java.util.ResourceBundle;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.itextpdf.text.pdf.ColumnText;
import com.lowagie.text.BadElementException;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.html.simpleparser.HTMLWorker;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfPageEventHelper;
import com.lowagie.text.pdf.PdfWriter;

//import com.lowagie.text.Document;
//import com.lowagie.text.Element;
//import com.lowagie.text.Font;
//import com.lowagie.text.PageSize;
//import com.lowagie.text.Paragraph;
//import com.lowagie.text.Phrase;
//import com.lowagie.text.pdf.PdfPCell;
//import com.lowagie.text.pdf.PdfPTable;
//import com.lowagie.text.pdf.PdfWriter;
import com.tsp.opav.model.DAOS.ComprasProcesoDAO;
//import com.tsp.operation.model.DAOS.AdminFintraDAO;
import com.tsp.operation.model.DAOS.MainDAO;
import com.tsp.operation.model.beans.RMCantidadEnLetras;
//import com.tsp.operation.model.TransaccionService;

//import com.tsp.operation.model.beans.AtributosRowsProd;
//import com.tsp.operation.model.beans.BeanGeneral;
//import com.tsp.operation.model.beans.ParmetrosDinamicaTSP;
//import com.tsp.operation.model.beans.ReporteProducionBeans;
import com.tsp.operation.model.beans.StringStatement;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.threads.HDinamicaContable;
import java.io.IOException;
import java.io.StringReader;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import static jxl.biff.Type.FOOTER;

//public class ProcesosClienteImpl extends MainDAO implements ProcesosClienteDAO {
public class ComprasProcesoImpl extends MainDAO implements ComprasProcesoDAO {

    private Connection con = null;
    private PreparedStatement ps = null;
    private ResultSet rs = null;
    private List listaInfo;
    private Object formato;

    public ComprasProcesoImpl(String dataBaseName) {
        super("ComprasProcesoDAO.xml", dataBaseName);
    }

    @Override
    public String cargarSolicitudes(String Usuario, String IdSolicitud, String ano, String mes, String tiposolicitud, String EstadoSolicitud) throws SQLException {

        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_LISTADO_SOLICITUDES";
        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            String consulta = "";
            String parametro = "";

            consulta = this.obtenerSQL(query);

            //consulta = consulta.replaceAll("#TOCO#", "\'<img src=\"./images/Confirmados.png\" height=\"20\" width=\"20\"  >\',");
            if (!mes.equals("0")) {
                consulta = consulta.replaceAll("#ANOMES#", " and EXTRACT(YEAR FROM ocs.creation_date::date) = '" + ano + "' AND EXTRACT(MONTH FROM ocs.creation_date::date) = '" + mes + "'");
            } else {
                consulta = consulta.replaceAll("#ANOMES#", " and EXTRACT(YEAR FROM ocs.creation_date::date) = '" + ano + "'");
            }

            if (!tiposolicitud.equals("0")) {
                consulta = consulta.replaceAll("#TIPOSOL#", " and tiposolicitud = " + tiposolicitud);
            } else {
                consulta = consulta.replaceAll("#TIPOSOL#", "");
            }

            if (!EstadoSolicitud.equals("0")) {
                consulta = consulta.replaceAll("#ESTADOSOL#", " and estado_solicitud = " + EstadoSolicitud);
            } else {
                consulta = consulta.replaceAll("#ESTADOSOL#", "");
            }

            ps = con.prepareStatement(consulta);

            ps.setString(1, Usuario);
            ps.setString(2, IdSolicitud);

            System.out.println(ps);

            rs = ps.executeQuery();

            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);

        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }

            return informacion;

        }

    }

    @Override
    public String PreSaveInsumos(JsonObject info, String Usuario, String IdSolicitud) {
        String respuesta = "";
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_PREACTUALIZAR_INSUMOS";

        JsonObject objeto = new JsonObject();

        try {

            JsonArray arr = info.getAsJsonArray("json");
            con = this.conectarJNDI();

            for (int i = 0; i < arr.size(); i++) {

                objeto = arr.get(i).getAsJsonObject();

                ps = con.prepareStatement(this.obtenerSQL(query));

                ps.setString(1, objeto.get("CantidadSolicitada").getAsString());
                ps.setString(2, objeto.get("ReferenciaAdicional").getAsString());
                ps.setString(3, objeto.get("ObservacionTxt").getAsString());
                ps.setString(4, Usuario);
                ps.setString(5, Usuario);
                ps.setString(6, IdSolicitud);
                ps.setString(7, objeto.get("CodigoMaterial").getAsString()); //CodigoMaterial Usuario IdSolicitud UserResponsable
                ps.setString(8, objeto.get("unidadmedida").getAsString());
                ps.setString(9, objeto.get("insumoadicional").getAsString());

                System.out.println(ps);

                ps.executeUpdate();

            }

            respuesta = "{\"respuesta\":\"Guardado\"}";

        } catch (Exception ex) {
            respuesta = "{\"respuesta\":\"ERROR\"}";
            ex.getMessage();
            ex.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ex) {
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException ex) {
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException ex) {
                }
            }
            return respuesta;
        }
    }

    public String sp_WhatIdo(String Usuario, String IdSolicitud, String tiposolicitud, String tipo_bodega, String descripcion, String fecha_actual, String fecha_entrega, String direccion_entrega, String id_bodega) throws SQLException {

        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_WHATIDO";
        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";

        try {
            con = this.conectarJNDI();
            String consulta = "";

            consulta = this.obtenerSQL(query);
            ps = con.prepareStatement(consulta);

            ps.setString(1, Usuario);
            ps.setString(2, IdSolicitud);
            ps.setString(3, tiposolicitud);
            ps.setString(4, tipo_bodega);
            ps.setString(5, descripcion);
            ps.setString(6, fecha_entrega);
            ps.setString(7, direccion_entrega);
            ps.setString(8, id_bodega);

            System.out.println(ps);

            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);

        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\", \"mensaje\": \"" + e.getMessage() + "\"";
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }

            return informacion;

        }

    }

    @Override
    public String EstadoSolicitud(String Usuario, String IdSolicitud) throws SQLException {

        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_ESTADO_SOLICITUD";
        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";

        try {
            con = this.conectarJNDI();
            String consulta = "";

            consulta = this.obtenerSQL(query);
            ps = con.prepareStatement(consulta);

            ps.setString(1, Usuario);
            ps.setString(2, IdSolicitud);

            System.out.println(ps);

            rs = ps.executeQuery();
            lista = new JsonArray();

            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);

        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }

            return informacion;

        }

    }

    @Override
    public String VerificarPreOCS(String Usuario) throws SQLException {

        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_VERIFICAR_ESTADO";
        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";

        try {
            con = this.conectarJNDI();
            String consulta = "";

            consulta = this.obtenerSQL(query);
            ps = con.prepareStatement(consulta);

            ps.setString(1, Usuario);

            System.out.println(ps);

            rs = ps.executeQuery();
            lista = new JsonArray();

            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);

        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }

            return informacion;

        }

    }

    @Override
    public String SolicitudesDeTodos(String ano, String mes, String tiposolicitud) throws SQLException {

        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_TODAS_SOLICITUDES";
        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            String consulta = "";
            String parametro = "";

            consulta = this.obtenerSQL(query);

            if (!mes.equals("0")) {
                consulta = consulta.replaceAll("#ANOMES#", " and EXTRACT(YEAR FROM ocs.creation_date::date) = '" + ano + "' AND EXTRACT(MONTH FROM ocs.creation_date::date) = '" + mes + "'");
            } else {
                consulta = consulta.replaceAll("#ANOMES#", " and EXTRACT(YEAR FROM ocs.creation_date::date) = '" + ano + "'");
            }

            if (!tiposolicitud.equals("0")) {
                consulta = consulta.replaceAll("#TIPOSOL#", " and tiposolicitud = " + tiposolicitud);
            } else {
                consulta = consulta.replaceAll("#TIPOSOL#", "");
            }

            ps = con.prepareStatement(consulta);

            System.out.println(ps);

            rs = ps.executeQuery();

            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);

        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }

            return informacion;

        }

    }

    @Override
    public String SolicitudToOrden(String consultaDB, String ano, String mes, String tiposolicitud) throws SQLException {

        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = consultaDB;
        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            String consulta = "";
            String parametro = "";

            consulta = this.obtenerSQL(query);

            if (!mes.equals("0")) {
                consulta = consulta.replaceAll("#ANOMES#", " and EXTRACT(YEAR FROM ocs.creation_date::date) = '" + ano + "' AND EXTRACT(MONTH FROM ocs.creation_date::date) = '" + mes + "'");
            } else {
                consulta = consulta.replaceAll("#ANOMES#", " and EXTRACT(YEAR FROM ocs.creation_date::date) = '" + ano + "'");
            }

            if (!tiposolicitud.equals("0")) {
                consulta = consulta.replaceAll("#TIPOSOL#", " and tiposolicitud = " + tiposolicitud);
            } else {
                consulta = consulta.replaceAll("#TIPOSOL#", "");
            }

            ps = con.prepareStatement(consulta);

            System.out.println(ps);

            rs = ps.executeQuery();

            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);

        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }

            return informacion;

        }

    }

    @Override
    public String LoadOrdenCS(String usuario, String consultaDB, String ano, String mes, String tiposolicitud) throws SQLException {

        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = consultaDB;
        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            String consulta = "";
            String parametro = "";

            consulta = this.obtenerSQL(query);

            if (!mes.equals("0")) {
                consulta = consulta.replaceAll("#ANOMES#", " where EXTRACT(YEAR FROM ocs.creation_date::date) = '" + ano + "' AND EXTRACT(MONTH FROM ocs.creation_date::date) = '" + mes + "'");
            } else {
                consulta = consulta.replaceAll("#ANOMES#", " where EXTRACT(YEAR FROM ocs.creation_date::date) = '" + ano + "'");
            }

            if (!tiposolicitud.equals("0")) {
                consulta = consulta.replaceAll("#TIPOSOL#", " and ocs.tiposolicitud = " + tiposolicitud);
            } else {
                consulta = consulta.replaceAll("#TIPOSOL#", "");
            }

            ps = con.prepareStatement(consulta);
            
            if (!query.equals("SQL_LISTADO_OCS")){
                ps.setString(1, usuario);
            }
            
            System.out.println(ps);

            rs = ps.executeQuery();

            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);

        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\", \"mensaje\": \"" + e.getMessage() + "\"";
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }

            return informacion;

        }

    }

    @Override
    public String VisualizarSolicitudes(String CodSolicitud) throws SQLException {

        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_VISUALIZAR_SOLICITUD";
        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";

        try {
            con = this.conectarJNDI();
            String consulta = "";

            consulta = this.obtenerSQL(query);
            ps = con.prepareStatement(consulta);

            ps.setString(1, CodSolicitud);

            System.out.println(ps);

            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);

        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }

            return informacion;

        }

    }

    @Override
    public String VisualizarOrdenCS(String CodOrden) throws SQLException {

        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_VISUALIZAR_ORDENCS";
        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";

        try {
            con = this.conectarJNDI();
            String consulta = "";

            consulta = this.obtenerSQL(query);
            ps = con.prepareStatement(consulta);

            ps.setString(1, CodOrden);

            System.out.println(ps);

            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);

        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }

            return informacion;

        }

    }

    @Override
    public String DeleteSolicitud(String Usuario, String IdSolicitud, String CodigoSolicitud) {
        String respuesta = "";
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_ELIMINAR_SOLICITUD";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";

        JsonObject objeto = new JsonObject();

        try {

            con = this.conectarJNDI();
            String consulta = "";

            consulta = this.obtenerSQL(query);
            ps = con.prepareStatement(consulta);

            ps.setString(1, Usuario);
            ps.setString(2, IdSolicitud);
            ps.setString(3, CodigoSolicitud);

            System.out.println(ps);

            rs = ps.executeQuery();
            lista = new JsonArray();

            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);

        } catch (Exception ex) {
            respuesta = "{\"respuesta\":\"ERROR\"}";
            ex.getMessage();
            ex.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ex) {
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException ex) {
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException ex) {
                }
            }
            return informacion;
        }
    }

    @Override
    public String PassSolicitudaCompras(String Usuario, String IdSolicitud, String CodigoSolicitud) {
        String respuesta = "";
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_PASAR_COMPRAS";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";

        JsonObject objeto = new JsonObject();

        try {

            con = this.conectarJNDI();
            String consulta = "";

            consulta = this.obtenerSQL(query);
            ps = con.prepareStatement(consulta);

            ps.setString(1, Usuario);
            ps.setString(2, IdSolicitud);
            ps.setString(3, CodigoSolicitud);

            System.out.println(ps);

            rs = ps.executeQuery();
            lista = new JsonArray();

            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
            //este es un procedimiento el cual envia las solicitudes con su detalle a provintegral en caso de 
            //que se encuentre en la tabla opav.sl_traslado_solicitud_compra y procesado sea igual a N  
            //enviar_solicitud_prov_tercerizados();            
        } catch (Exception ex) {
            respuesta = "{\"respuesta\":\"ERROR\"}";
            ex.getMessage();
            ex.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ex) {
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException ex) {
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException ex) {
                }
            }
            return informacion;
        }
    }

    public void enviar_solicitud_prov_tercerizados(){
         
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SL_INSERTAR_SOLICITUD_COMPRA_PROV";
        try {

            con = this.conectarJNDI();
            String consulta = "";

            consulta = this.obtenerSQL(query);
            ps = con.prepareStatement(consulta);
            rs = ps.executeQuery();
            
        } catch (Exception ex) {
            ex.getMessage();
            ex.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ex) {
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException ex) {
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException ex) {
                }
            }
        }
    }
            
    @Override
    public String AddElemToOrderOCS(String modo_compra, String Usuario, String CodigoSolicitud, String cod_insumo) {

        String respuesta = "";
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CREAR_PREOCS";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";

        JsonObject objeto = new JsonObject();

        try {

            con = this.conectarJNDI();
            String consulta = "";

            consulta = this.obtenerSQL(query);
            ps = con.prepareStatement(consulta);

            ps.setString(1, modo_compra);
            ps.setString(2, Usuario);
            ps.setString(3, CodigoSolicitud);
            ps.setString(4, cod_insumo);

            System.out.println(ps);

            rs = ps.executeQuery();
            lista = new JsonArray();

            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);

        } catch (Exception ex) {
            respuesta = "{\"respuesta\":\"ERROR\"}";
            ex.getMessage();
            ex.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ex) {
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException ex) {
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException ex) {
                }
            }
            return informacion;
        }
    }

    @Override
    public String SaveOCS(String Usuario, String tipo_solicitud, String proveedor, String descripcion, String fecha_entrega, String direccion_entrega, String f_pago, String id_bodega) {
        String respuesta = "";
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_GUARDAR_OCS";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";

        JsonObject objeto = new JsonObject();

        try {

            con = this.conectarJNDI();
            String consulta = "";

            consulta = this.obtenerSQL(query);
            ps = con.prepareStatement(consulta);

            ps.setString(1, Usuario);
            ps.setString(2, tipo_solicitud);
            ps.setString(3, proveedor);
            ps.setString(4, descripcion);
            ps.setString(5, fecha_entrega);
            ps.setString(6, direccion_entrega);
            ps.setString(7, f_pago);
            ps.setString(8, id_bodega);

            System.out.println(ps);

            rs = ps.executeQuery();
            lista = new JsonArray();

            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);

        } catch (Exception ex) {
            //informacion = "{\"respuesta\":\"ERROR\"}";
            informacion = "{'error':'" + ex.getMessage() + "'}";
            ex.getMessage();
            ex.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ex) {
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException ex) {
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException ex) {
                }
            }
            return informacion;
        }
    }

    @Override
    public String SaveDetailsOCS(JsonObject info, String oSC) {
        String respuesta = "";
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_UPDATE_DETALLES_OCS";

        JsonObject objeto = new JsonObject();

        try {

            JsonArray arr = info.getAsJsonArray("json");
            con = this.conectarJNDI();

            for (int i = 0; i < arr.size(); i++) {

                objeto = arr.get(i).getAsJsonObject();

                ps = con.prepareStatement(this.obtenerSQL(query));

                ps.setString(1, objeto.get("CostoCompra").getAsString());
                ps.setString(2, objeto.get("CantSolicitar").getAsString()); //CodigoMaterial Usuario IdSolicitud UserResponsable
                ps.setString(3, objeto.get("TotalRow").getAsString());
                ps.setString(4, objeto.get("CodigoMaterial").getAsString());
                ps.setString(5, objeto.get("unidadmedida").getAsString());
                ps.setString(6, objeto.get("insumoadicional").getAsString());
                ps.setString(7, oSC);

                System.out.println(ps);

                ps.executeUpdate();

            }

            respuesta = "{\"respuesta\":\"Guardado\"}";

        } catch (Exception ex) {
            respuesta = "{\"respuesta\":\"ERROR\"}";
            ex.getMessage();
            ex.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ex) {
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException ex) {
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException ex) {
                }
            }
            return respuesta;
        }
    }

    @Override
    public String VisualizarProveedor() throws SQLException {

        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_VISUALIZAR_PROVEEDOR";
        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";

        try {
            con = this.conectarJNDI();
            String consulta = "";

            consulta = this.obtenerSQL(query);
            ps = con.prepareStatement(consulta);

            System.out.println(ps);

            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);

        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }

            return informacion;

        }

    }

    @Override
    public String SalvarCatalogoInsumos(String Usuario, String id_solicitud, String TipoInsumo, String CodigoInsumo, String DescripcionInsumo, String NombreUnidadInsumo, String Cantidad, String CdSol) {
        String respuesta = "";
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_GUARDAR_CATALOGO";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";

        JsonObject objeto = new JsonObject();

        try {

            con = this.conectarJNDI();
            String consulta = "";

            consulta = this.obtenerSQL(query);
            ps = con.prepareStatement(consulta);

            ps.setString(1, Usuario);
            ps.setString(2, id_solicitud);
            ps.setString(3, TipoInsumo);
            ps.setString(4, CodigoInsumo);
            ps.setString(5, DescripcionInsumo);
            ps.setString(6, NombreUnidadInsumo);
            ps.setString(7, Cantidad);
            ps.setString(8, CdSol);

            System.out.println(ps);

            rs = ps.executeQuery();
            lista = new JsonArray();

            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);

        } catch (Exception ex) {
            respuesta = "{\"respuesta\":\"ERROR\"}";
            ex.getMessage();
            ex.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ex) {
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException ex) {
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException ex) {
                }
            }
            return informacion;
        }
    }

    @Override
    public String imprimirOCS(String orden_compra, Usuario usuario) {
        String msg = "OK";
        try {
            String ruta = "";
            String observaciones = null, centroCosto = null, nombre_proyecto = null, multiservisio = null, fechaOcs = null, iva_o_aui = null;
            ps = null;
            rs = null;
            con = this.conectarJNDI();
            ruta = this.directorioArchivo(usuario.getLogin(), orden_compra, "pdf");
            ResourceBundle rsb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");

            DecimalFormat formato = new DecimalFormat("$###,###,###");
            DecimalFormat formato2 = new DecimalFormat("###.##");
            Font fuente = new Font(Font.HELVETICA, 10, Font.NORMAL, new java.awt.Color(0, 0, 0));
            Font f3b = new Font(Font.TIMES_ROMAN, 10, Font.HELVETICA, new Color(255, 255, 255));
            Font f4normalN = new Font(Font.HELVETICA, 8, Font.BOLD, new java.awt.Color(102, 101, 101));
            Font f4normal = new Font(Font.HELVETICA, 8, Font.NORMAL, new java.awt.Color(102, 101, 101));
            Font encabezado = new Font(Font.HELVETICA, 16, Font.BOLD, new java.awt.Color(247, 147, 29));
            Font suben = new Font(Font.HELVETICA, 10, Font.BOLD, new java.awt.Color(247, 147, 29));
            Font f5 = new Font(Font.HELVETICA, 8, Font.BOLD, new java.awt.Color(238, 129, 38));

            Document documento = null;
            documento = this.createDoc();
            PdfWriter pdfWriter = PdfWriter.getInstance(documento, new FileOutputStream(ruta));
//            FooterPDF footer = new FooterPDF(rsb);
//            pdfWriter.setPageEvent(footer);
            documento.open();
            //documento.newPage();            
            //documento.setMargins(0, 70, 0, 70);
            int cant = 0;
            int item = 1;
            encabezados encabezado_ = new encabezados(rsb, f3b, cant, item);
            pdfWriter.setPageEvent(encabezado_);
            Image imagen = Image.getInstance(rsb.getString("ruta") + "/images/logo_selectrik_.jpg");
            imagen.scaleToFit(180, 180);

            String queryP = "SQL_INFORMACION_PROYECTO";
            ps = con.prepareStatement(this.obtenerSQL(queryP));
            ps.setString(1, orden_compra);
            rs = ps.executeQuery();
            while (rs.next()) {
                observaciones = rs.getString("observaciones");
                centroCosto = rs.getString("centro_costos_gastos");
                nombre_proyecto = rs.getString("nombre_proyecto");
                multiservisio = rs.getString("ms");
                fechaOcs = rs.getString("fecha_actual");
                iva_o_aui = rs.getString("cod");
            }

            //tabla de fecha
            PdfPTable table_ = new PdfPTable(4);
            table_.setWidths(new int[]{2, 2, 2, 2});
            table_.setWidthPercentage(7);
            PdfPCell cell_;
            Calendar calendario = Calendar.getInstance();
            String dias = String.valueOf(calendario.get(Calendar.DAY_OF_MONTH));
            String mes = this.mesToString(calendario.get(Calendar.MONTH) + 1);
            String anio = String.valueOf(calendario.get(Calendar.YEAR));
            cell_ = new PdfPCell(new Phrase("FECHA", f5));
            cell_.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell_.setBorder(0);
            table_.addCell(cell_);

            cell_ = new PdfPCell(new Phrase(fechaOcs == null ? "" : fechaOcs.substring(8, 10), f4normal));
            cell_.setHorizontalAlignment(Element.ALIGN_CENTER);
            table_.addCell(cell_);

            cell_ = new PdfPCell(new Phrase(fechaOcs == null ? "" : fechaOcs.substring(5, 7), f4normal));
            cell_.setHorizontalAlignment(Element.ALIGN_CENTER);
            table_.addCell(cell_);

            cell_ = new PdfPCell(new Phrase(fechaOcs == null ? "" : fechaOcs.substring(0, 4), f4normal));
            cell_.setHorizontalAlignment(Element.ALIGN_CENTER);
            table_.addCell(cell_);
            //documento.add(table_);

            //tabla encabezado logo y titulo
            PdfPTable table_0 = new PdfPTable(2);
            table_0.setWidths(new int[]{5, 5});
            table_0.setWidthPercentage(100);
            PdfPCell cell_0;
            cell_0 = new PdfPCell(imagen);
            cell_0.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell_0.setBorder(0);
            table_0.addCell(cell_0);

            cell_0 = new PdfPCell(new Phrase("ORDEN DE COMPRA " + orden_compra, encabezado));
            cell_0.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell_0.setVerticalAlignment(Element.ALIGN_CENTER);
            cell_0.setBorderWidthRight(0f);
            cell_0.setBorderWidthBottom(0f);
            cell_0.setBorderWidthTop(0f);
            cell_0.setBorderWidthLeft(0f);
            cell_0.setPaddingTop(50);
            table_0.addCell(cell_0);

            cell_0 = new PdfPCell(new Phrase("CENTRO DE COSTO: " + centroCosto == null ? "" : centroCosto+"-"+iva_o_aui, suben));
            cell_0.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell_0.setVerticalAlignment(Element.ALIGN_RIGHT);
            cell_0.setColspan(2);
            cell_0.setPaddingRight(22);
            cell_0.setPaddingTop(-30);
            cell_0.setBorder(0);
            table_0.addCell(cell_0);

            cell_0 = new PdfPCell(new Phrase(" ", suben));
            cell_0.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell_0.setBorder(0);
            table_0.addCell(cell_0);

            //A�ADE LA TABLA DE FECHA
            cell_0 = new PdfPCell(table_);
            cell_0.setBorderWidthRight(0f);
            cell_0.setBorderWidthBottom(0f);
            cell_0.setBorderWidthTop(0f);
            cell_0.setBorderWidthLeft(0f);
            cell_0.setPaddingLeft(45);
            table_0.addCell(cell_0);
            documento.add(table_0);

            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(new Paragraph(new Phrase(" ", fuente)));

            String query = "SQL_ORDEN_COMPRA";
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, orden_compra);
            rs = ps.executeQuery();
            while (rs.next()) {
                //primer cuadro            
                PdfPTable table_1 = new PdfPTable(4);
                table_1.setWidths(new int[]{3, 3, 3, 2});
                table_1.setWidthPercentage(100);
                PdfPCell cell_1;

                cell_1 = new PdfPCell(new Phrase("PROVEEDOR", f3b));
                cell_1.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell_1.setBackgroundColor(new Color(93, 93, 93));
                cell_1.setBorderWidthRight(0);
                cell_1.setColspan(2);
                table_1.addCell(cell_1);

                cell_1 = new PdfPCell(new Phrase("DATOS ENVIO", f3b));
                cell_1.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell_1.setBackgroundColor(new Color(93, 93, 93));
                cell_1.setColspan(2);
                table_1.addCell(cell_1);

                cell_1 = new PdfPCell(new Phrase("NOMBRE DE EMPRESA: ", f4normalN));
                cell_1.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell_1.setBorderWidthRight(0f);
                cell_1.setBorderWidthBottom(0f);
                cell_1.setBorderWidthTop(0f);
                table_1.addCell(cell_1);

                cell_1 = new PdfPCell(new Phrase(rs.getString("nombre_proveedor"), f4normal));
                cell_1.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell_1.setBorderWidthRight(0f);
                cell_1.setBorderWidthBottom(0f);
                cell_1.setBorderWidthTop(0f);
                cell_1.setBorderWidthLeft(0f);
                table_1.addCell(cell_1);

                cell_1 = new PdfPCell(new Phrase("RESPONSALE: ", f4normalN));
                cell_1.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell_1.setBorderWidthRight(0f);
                cell_1.setBorderWidthBottom(0f);
                cell_1.setBorderWidthTop(0f);
                cell_1.setBorderWidthLeft(0f);
                table_1.addCell(cell_1);

                cell_1 = new PdfPCell(new Phrase(rs.getString("responsable_envio"), f4normal));
                cell_1.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell_1.setBorderWidthBottom(0f);
                cell_1.setBorderWidthTop(0f);
                cell_1.setBorderWidthLeft(0f);
                table_1.addCell(cell_1);

                cell_1 = new PdfPCell(new Phrase("CONTACTO O DEPARTAMENTO: ", f4normalN));
                cell_1.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell_1.setBorderWidthRight(0f);
                cell_1.setBorderWidthBottom(0f);
                cell_1.setBorderWidthTop(0f);
                table_1.addCell(cell_1);

                cell_1 = new PdfPCell(new Phrase("COMPRAS ", f4normal));
                cell_1.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell_1.setBorderWidthRight(0f);
                cell_1.setBorderWidthBottom(0f);
                cell_1.setBorderWidthTop(0f);
                cell_1.setBorderWidthLeft(0f);
                table_1.addCell(cell_1);

                cell_1 = new PdfPCell(new Phrase("DIRECCION BODEGA O PROYECTO: ", f4normalN));
                cell_1.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell_1.setBorderWidthRight(0f);
                cell_1.setBorderWidthBottom(0f);
                cell_1.setBorderWidthTop(0f);
                cell_1.setBorderWidthLeft(0f);
                table_1.addCell(cell_1);

                cell_1 = new PdfPCell(new Phrase(rs.getString("direccion_entrega"), f4normal));
                cell_1.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell_1.setBorderWidthBottom(0f);
                cell_1.setBorderWidthTop(0f);
                cell_1.setBorderWidthLeft(0f);
                table_1.addCell(cell_1);

                cell_1 = new PdfPCell(new Phrase("DIRECCION: ", f4normalN));
                cell_1.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell_1.setBorderWidthRight(0f);
                cell_1.setBorderWidthBottom(0f);
                cell_1.setBorderWidthTop(0f);
                table_1.addCell(cell_1);

                cell_1 = new PdfPCell(new Phrase(rs.getString("direccion_proveedor"), f4normal));
                cell_1.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell_1.setBorderWidthRight(0f);
                cell_1.setBorderWidthBottom(0f);
                cell_1.setBorderWidthTop(0f);
                cell_1.setBorderWidthLeft(0f);
                table_1.addCell(cell_1);

                cell_1 = new PdfPCell(new Phrase("CIUDAD: ", f4normalN));
                cell_1.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell_1.setBorderWidthBottom(0f);
                cell_1.setBorderWidthTop(0f);
                cell_1.setBorderWidthLeft(0f);
                cell_1.setBorderWidthRight(0f);
                table_1.addCell(cell_1);

                //cell_1 = new PdfPCell(new Phrase(rs.getString("ciudad_envio"), f4normal));
                cell_1 = new PdfPCell(new Phrase("", f4normal));
                cell_1.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell_1.setBorderWidthBottom(0f);
                cell_1.setBorderWidthTop(0f);
                cell_1.setBorderWidthLeft(0f);
                table_1.addCell(cell_1);

                cell_1 = new PdfPCell(new Phrase("CIUDAD: ", f4normalN));
                cell_1.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell_1.setBorderWidthRight(0f);
                cell_1.setBorderWidthBottom(0f);
                cell_1.setBorderWidthTop(0f);
                table_1.addCell(cell_1);

                cell_1 = new PdfPCell(new Phrase(rs.getString("ciudad_proveedor"), f4normal));
                cell_1.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell_1.setBorderWidthBottom(0f);
                cell_1.setBorderWidthTop(0f);
                cell_1.setBorderWidthLeft(0f);
                cell_1.setBorderWidthRight(0f);
                table_1.addCell(cell_1);

                cell_1 = new PdfPCell(new Phrase("TELEFONO: ", f4normalN));
                cell_1.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell_1.setBorderWidthRight(0f);
                cell_1.setBorderWidthBottom(0f);
                cell_1.setBorderWidthTop(0f);
                cell_1.setBorderWidthLeft(0f);
                table_1.addCell(cell_1);

                cell_1 = new PdfPCell(new Phrase(rs.getString("telefono_envio"), f4normal));
                cell_1.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell_1.setBorderWidthLeft(0f);
                cell_1.setBorderWidthBottom(0f);
                cell_1.setBorderWidthTop(0f);
                table_1.addCell(cell_1);

                cell_1 = new PdfPCell(new Phrase("TELEFONO: ", f4normalN));
                cell_1.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell_1.setBorderWidthTop(0f);
                cell_1.setBorderWidthRight(0f);
                table_1.addCell(cell_1);

                cell_1 = new PdfPCell(new Phrase(rs.getString("telefono_proveedor"), f4normal));
                cell_1.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell_1.setColspan(3);
                cell_1.setBorderWidthTop(0f);
                cell_1.setBorderWidthLeft(0f);
                table_1.addCell(cell_1);
                documento.add(table_1);

                documento.add(new Paragraph(new Phrase(" ", fuente)));

            }

            //segundo cuadro
            PdfPTable table_2 = new PdfPTable(3);
            table_2.setWidths(new int[]{3, 4, 3});
            table_2.setWidthPercentage(100);
            PdfPCell cell_2;

            cell_2 = new PdfPCell(new Phrase("MS", f3b));
            cell_2.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell_2.setBackgroundColor(new Color(93, 93, 93));
            table_2.addCell(cell_2);

            cell_2 = new PdfPCell(new Phrase("NOMBRE DEL PROYECTO", f3b));
            cell_2.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell_2.setBackgroundColor(new Color(93, 93, 93));
            table_2.addCell(cell_2);

            cell_2 = new PdfPCell(new Phrase("CONTRATISTA", f3b));
            cell_2.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell_2.setBackgroundColor(new Color(93, 93, 93));
            table_2.addCell(cell_2);

            cell_2 = new PdfPCell(new Phrase(multiservisio == null ? "" : multiservisio, f4normal));
            cell_2.setHorizontalAlignment(Element.ALIGN_CENTER);
            table_2.addCell(cell_2);

            cell_2 = new PdfPCell(new Phrase(nombre_proyecto == null ? "" : nombre_proyecto, f4normal));
            cell_2.setHorizontalAlignment(Element.ALIGN_CENTER);
            table_2.addCell(cell_2);

            cell_2 = new PdfPCell(new Phrase(" ", f4normal));
            cell_2.setHorizontalAlignment(Element.ALIGN_CENTER);
            table_2.addCell(cell_2);
            documento.add(table_2);

            documento.add(new Paragraph(new Phrase(" ", fuente)));

            //tercera tabla
            PdfPTable table_3 = new PdfPTable(5);
            table_3.setWidths(new int[]{3, 5, 2, 3, 3});
            table_3.setWidthPercentage(100);
            PdfPCell cell_3;

            cell_3 = new PdfPCell(new Phrase("REFERENCIA", f3b));
            cell_3.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell_3.setBackgroundColor(new Color(93, 93, 93));
            table_3.addCell(cell_3);

            cell_3 = new PdfPCell(new Phrase("DESCRIPCION", f3b));
            cell_3.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell_3.setBackgroundColor(new Color(93, 93, 93));
            table_3.addCell(cell_3);

            cell_3 = new PdfPCell(new Phrase("CANT", f3b));
            cell_3.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell_3.setBackgroundColor(new Color(93, 93, 93));
            table_3.addCell(cell_3);

            cell_3 = new PdfPCell(new Phrase("P/U", f3b));
            cell_3.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell_3.setBackgroundColor(new Color(93, 93, 93));
            table_3.addCell(cell_3);

            cell_3 = new PdfPCell(new Phrase("TOTAL", f3b));
            cell_3.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell_3.setBackgroundColor(new Color(93, 93, 93));
            table_3.addCell(cell_3);

            double valorTotal = 0;
            String query2 = "SQL_ORDEN_COMPRA_DETALLE";
            ps = con.prepareStatement(this.obtenerSQL(query2));
            ps.setString(1, orden_compra);
            rs = ps.executeQuery();
            while (rs.next()) {
                cant = rs.getInt("cant");
                valorTotal = valorTotal + rs.getInt("costo_total_compra");

                // if (item == cant) {
                cell_3 = new PdfPCell(new Phrase(rs.getString("referencia"), f4normal));
                cell_3.setHorizontalAlignment(Element.ALIGN_CENTER);
//                    cell_3.setBorderWidthTop(0f);
                table_3.addCell(cell_3);

                cell_3 = new PdfPCell(new Phrase(rs.getString("descripcion_insumo"), f4normal));
                cell_3.setHorizontalAlignment(Element.ALIGN_CENTER);
//                    cell_3.setBorderWidthTop(0f);
                table_3.addCell(cell_3);

                cell_3 = new PdfPCell(new Phrase(rs.getString("cantidad_solicitada"), f4normal));
                cell_3.setHorizontalAlignment(Element.ALIGN_CENTER);
//                    cell_3.setBorderWidthTop(0f);
                table_3.addCell(cell_3);

                cell_3 = new PdfPCell(new Phrase(formato.format(rs.getInt("costo_unitario_compra")), f4normal));
                cell_3.setHorizontalAlignment(Element.ALIGN_CENTER);
//                    cell_3.setBorderWidthTop(0f);
                table_3.addCell(cell_3);

                cell_3 = new PdfPCell(new Phrase(formato.format(rs.getInt("costo_total_compra")), f4normal));
                cell_3.setHorizontalAlignment(Element.ALIGN_CENTER);
//                    cell_3.setBorderWidthTop(0f);
                table_3.addCell(cell_3);
               // } 
//                else {
//                    cell_3 = new PdfPCell(new Phrase(rs.getString("referencia"), f4normal));
//                    cell_3.setHorizontalAlignment(Element.ALIGN_CENTER);
//                    cell_3.setBorderWidthBottom(0f);
//                    cell_3.setBorderWidthTop(0f);
//                    table_3.addCell(cell_3);
//
//                    cell_3 = new PdfPCell(new Phrase(rs.getString("descripcion_insumo"), f4normal));
//                    cell_3.setHorizontalAlignment(Element.ALIGN_CENTER);
//                    cell_3.setBorderWidthBottom(0f);
//                    cell_3.setBorderWidthTop(0f);
//                    table_3.addCell(cell_3);
//
//                    cell_3 = new PdfPCell(new Phrase(rs.getString("cantidad_solicitada"), f4normal));
//                    cell_3.setHorizontalAlignment(Element.ALIGN_CENTER);
//                    cell_3.setBorderWidthBottom(0f);
//                    cell_3.setBorderWidthTop(0f);
//                    table_3.addCell(cell_3);
//
//                    cell_3 = new PdfPCell(new Phrase(formato.format(rs.getInt("costo_unitario_compra")), f4normal));
//                    cell_3.setHorizontalAlignment(Element.ALIGN_CENTER);
//                    cell_3.setBorderWidthBottom(0f);
//                    cell_3.setBorderWidthTop(0f);
//                    table_3.addCell(cell_3);
//
//                    cell_3 = new PdfPCell(new Phrase(formato.format(rs.getInt("costo_total_compra")), f4normal));
//                    cell_3.setHorizontalAlignment(Element.ALIGN_CENTER);
//                    cell_3.setBorderWidthTop(0f);
//                    cell_3.setBorderWidthBottom(0f);
//                    table_3.addCell(cell_3);
                // }

                item = item + 1;
            }
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (con != null) {
                this.desconectar(con);
            }
            documento.add(table_3);

            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(new Paragraph(new Phrase(" ", fuente)));

            PdfPTable table_7 = new PdfPTable(1);
            table_7.setWidths(new int[]{5});
            table_7.setWidthPercentage(100);
            PdfPCell cell_7;

            cell_7 = new PdfPCell(new Phrase("OBSERVACIONES", f3b));
            cell_7.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell_7.setBackgroundColor(new Color(93, 93, 93));
            cell_7.setColspan(5);
            table_7.addCell(cell_7);

            cell_7 = new PdfPCell(new Phrase(observaciones == null ? "" : observaciones, f4normal));
            cell_7.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell_7.setColspan(5);
            cell_7.setBorderWidthBottom(0f);
            table_7.addCell(cell_7);

            cell_7 = new PdfPCell(new Phrase(" ", f4normal));
            cell_7.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell_7.setColspan(5);
            cell_7.setBorderWidthTop(0f);
            table_7.addCell(cell_7);

            documento.add(table_7);

            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(new Paragraph(new Phrase(" ", fuente)));

            PdfPTable table_5 = new PdfPTable(1);
            table_5.setWidths(new int[]{3});
            table_5.setWidthPercentage(100);
            PdfPCell cell_5;

            cell_5 = new PdfPCell(new Phrase("", f4normal));
            cell_5.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell_5.setBorderWidthBottom(0f);
            cell_5.setBorderWidthTop(0f);
            cell_5.setFixedHeight(10);
            table_5.addCell(cell_5);

            cell_5 = new PdfPCell(new Phrase(" ", f4normal));
            cell_5.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell_5.setBorderWidthBottom(0f);
            cell_5.setBorderWidthTop(0f);
            cell_5.setFixedHeight(10);
            table_5.addCell(cell_5);

            cell_5 = new PdfPCell(new Phrase("", f4normal));
            cell_5.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell_5.setBorderWidthBottom(0f);
            cell_5.setBorderWidthTop(0f);
            cell_5.setFixedHeight(10);
            table_5.addCell(cell_5);

            cell_5 = new PdfPCell(new Phrase("", f4normal));
            cell_5.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell_5.setBorderWidthBottom(0f);
            cell_5.setBorderWidthTop(0f);
            cell_5.setFixedHeight(10);
            table_5.addCell(cell_5);

            cell_5 = new PdfPCell(new Phrase(formato.format(valorTotal), f4normal));
            cell_5.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell_5.setBorderWidthBottom(0f);
            cell_5.setBorderWidthTop(0f);
            cell_5.setFixedHeight(10);
            table_5.addCell(cell_5);
            //documento.add(table_5);

            PdfPTable table_6 = new PdfPTable(1);
            table_6.setWidths(new int[]{3});
            table_6.setWidthPercentage(100);
            PdfPCell cell_6;

            cell_6 = new PdfPCell(new Phrase(formato.format(valorTotal), f4normal));
            cell_6.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell_6.setBorderWidthBottom(0f);
            cell_6.setBorderWidthTop(0f);
            table_6.addCell(cell_6);

            //cuarto cuadro
            PdfPTable table_4 = new PdfPTable(4);
            table_4.setWidths(new int[]{7, 1, 2, 3});
            table_4.setWidthPercentage(100);
            PdfPCell cell_4;

            cell_4 = new PdfPCell(new Phrase("COMETARIOS O INTRUCCIONES ESPECIALES", f3b));
            cell_4.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell_4.setBackgroundColor(new Color(93, 93, 93));
            table_4.addCell(cell_4);

            cell_4 = new PdfPCell(new Phrase("", f3b));
            cell_4.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell_4.setBorderWidthBottom(0f);
            cell_4.setBorderWidthTop(0f);
            table_4.addCell(cell_4);

            cell_4 = new PdfPCell(new Phrase("SUBTOTAL", f3b));
            cell_4.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell_4.setBackgroundColor(new Color(93, 93, 93));
            cell_4.setBorderWidthBottom(0f);
            table_4.addCell(cell_4);

            cell_4 = new PdfPCell(table_6);
            cell_4.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell_4.setBorderWidthBottom(0f);
            table_4.addCell(cell_4);

            cell_4 = new PdfPCell(new Phrase("Para el tramite de su factura se debe tener en cuenta los siguientes requisitos:\n"
                    + "Anexar Orden de compra y remisi�n debidamente firmada La Factura Original debe ser entregada en la Cra. 55 N� 100-51 piso 5 Centro Empresarial Blue Gardens "
                    + "Barranquilla, solo aqu� se aceptan radicaci�n\n"
                    + "de facturas.", f4normal));
            cell_4.setHorizontalAlignment(Element.ALIGN_LEFT);
            table_4.addCell(cell_4);

            cell_4 = new PdfPCell(new Phrase("", f3b));
            cell_4.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell_4.setBorderWidthBottom(0f);
            cell_4.setBorderWidthTop(0f);
            table_4.addCell(cell_4);

            cell_4 = new PdfPCell(new Phrase("IMPUESTO  FLETE  RETEFUENTE  RETE-ICA  TOTAL", f3b));
            cell_4.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell_4.setBackgroundColor(new Color(93, 93, 93));
            cell_4.setBorderWidthTop(0f);
            table_4.addCell(cell_4);

            cell_4 = new PdfPCell(table_5);
            cell_4.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell_4.setBorderWidthTop(0f);
            table_4.addCell(cell_4);
            documento.add(table_4);

            documento.close();
        } catch (Exception e) {
            msg = "ERROR";
            e.printStackTrace();
        }
        return msg;
    }

    @Override
    public String VisualizarProyectos(String usuario) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query ;
         
        
        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";

        try {
            con = this.conectarJNDI();
            String consulta = "";
            if (EsSuperBodeguista(usuario)){
                query = "SQL_VISUALIZAR_PROYECTOS_SUPER_BODEGISTA";
                consulta = this.obtenerSQL(query);
                ps = con.prepareStatement(consulta);
            }else{
                query= "SQL_VISUALIZAR_PROYECTOS";
                consulta = this.obtenerSQL(query);
                ps = con.prepareStatement(consulta);
                ps.setString(1, usuario);
            }
            System.out.println(ps);

            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);

        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\", \"mensaje\": \"" + e.getMessage() + "\"";
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }

            return informacion;

        }
    }
    
    @Override
    public String CargarProyectosTraslado(String login) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_VISUALIZAR_PROYECTOS_TRASLADO";
        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";

        try {
            con = this.conectarJNDI();
            String consulta = "";

            consulta = this.obtenerSQL(query);
            ps = con.prepareStatement(consulta);

            System.out.println(ps);

            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);

        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\", \"mensaje\": \"" + e.getMessage() + "\"";
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }

            return informacion;

        }
    }

    @Override
    public String VisualizarItemsBodega(String id_bodega, String tipo_movimiento, String usuario, String id_solicitud) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";

        try {
            con = this.conectarJNDI();
            String consulta = "";
            
        switch (tipo_movimiento) {
            case "entrada":
                if (EsSuperBodeguista(usuario)){
                    consulta = this.obtenerSQL("SQL_VISUALIZAR_TODOS_LOS_ITEMS");
                    ps = con.prepareStatement(consulta);
                }else{
                    consulta = this.obtenerSQL("SQL_VISUALIZAR_ITEMS_QUE_SALIERON");
                    ps = con.prepareStatement(consulta);
                    ps.setString(1, id_bodega);
                    ps.setString(2, id_solicitud);
                }    
                break;
            case "salida":
            case "traslado":
                consulta = this.obtenerSQL("SQL_VISUALIZAR_ITEMS_POR_BODEGA");
                ps = con.prepareStatement(consulta);
                ps.setString(1, id_bodega);
                ps.setString(2, id_solicitud);                
                break;
            case "ejecucion":
                consulta = this.obtenerSQL("SQL_VISUALIZAR_ITEMS_POR_BODEGA_EJECUCION");
                ps = con.prepareStatement(consulta);
                ps.setString(1, id_bodega);      
                break;                
            default: 
                consulta = this.obtenerSQL("SQL_VISUALIZAR_ITEMS_POR_USUARIO");
                ps = con.prepareStatement(consulta);
                ps.setString(1, usuario);
                break;
        }                                   

            System.out.println(ps);

            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);

        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\", \"mensaje\": \"" + e.getMessage() + "\"";
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }

            return informacion;

        }
    }

    public String CargarListaInsumos() {
        con = null;
        rs = null;
        ps = null;
        String query = "SQL_CARGAR_UNIDADES_DE_MEDIDA";
        String respuestaJson = "{}";

        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);

            rs = ps.executeQuery();

            while (rs.next()) {
                obj.addProperty(rs.getString("id"), rs.getString("descripcion"));
            }
            respuestaJson = gson.toJson(obj);

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return respuestaJson;
    }

    public String GuardarMovimiento(Usuario usuario, String fecha_transaccion, String responsable, String descripcion, String id_solicitud_proyecto, String id_bodega, String detalle, String tipo_movimiento, String id_solicitud_destino, String id_bodega_destino) {
        con = null;
        rs = null;
        ps = null;
        String query = "SQL_GUARDAR_CABECERA";
        Statement stmt = null;

        JsonObject json_res = new JsonObject();

        JsonArray detalle_json = (JsonArray) (new JsonParser()).parse(detalle);
        System.out.println(detalle_json.get(0).getAsJsonObject());
        System.out.println();
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            con.setAutoCommit(false);

            ps = con.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, id_solicitud_proyecto);
            ps.setString(2, id_bodega);
            ps.setString(3, tipo_movimiento);
            ps.setString(4, tipo_movimiento);
            ps.setString(5, usuario.getLogin());
            ps.setString(6, fecha_transaccion);
            ps.setString(7, responsable);
            ps.setString(8, descripcion);
            ps.setString(9, id_solicitud_destino);
            ps.setString(10, id_bodega_destino);

            System.out.print(ps);

            ps.executeUpdate();
            stmt = con.createStatement();
            rs = ps.getGeneratedKeys();
            String id_cabecera="";
            if (rs.next()) {
                id_cabecera = String.valueOf(rs.getInt(1));
                for (int i = 0; i < detalle_json.size(); i++) {

                    String codigo_material = detalle_json.get(i).getAsJsonObject().get("codigo_material").getAsString();
                    String descripcion_insumo = detalle_json.get(i).getAsJsonObject().get("descripcion").getAsString();
                    String solicitar = detalle_json.get(i).getAsJsonObject().get("solicitar").getAsString();
                    String referencia = detalle_json.get(i).getAsJsonObject().get("referencia").getAsString();
                    String observacion = detalle_json.get(i).getAsJsonObject().get("observacion").getAsString();
                    String unidad_de_medida = detalle_json.get(i).getAsJsonObject().get("unidad_de_medida").getAsString();

                    if(tipo_movimiento.equals("1")){
                        String valor = detalle_json.get(i).getAsJsonObject().get("valor").getAsString();
                        stmt.addBatch(GuardarItemDetalleEntrada(id_cabecera, codigo_material, descripcion_insumo, solicitar, referencia, observacion, unidad_de_medida, usuario.getLogin(), valor));
                    }else{
                        stmt.addBatch(GuardarItemDetalle(id_cabecera, codigo_material, descripcion_insumo, solicitar, referencia, observacion, unidad_de_medida, usuario.getLogin(), id_solicitud_proyecto, id_bodega));
                    }
                    
                    MoverKardex(tipo_movimiento, id_bodega, codigo_material, solicitar, descripcion_insumo, usuario.getLogin(), id_bodega_destino, unidad_de_medida, stmt, id_solicitud_proyecto, id_solicitud_destino, id_cabecera);
                }
            }
            
            stmt.executeBatch();
            stmt.clearBatch();
            if(tipo_movimiento.equals("3")){
                MovimientoTrasladoApoteosys(id_bodega, id_bodega_destino,  id_solicitud_proyecto , id_solicitud_destino,  id_cabecera );            
            }
            if(tipo_movimiento.equals("1")){
                MovimientoEntradaApoteosys(id_bodega, id_solicitud_proyecto ,  id_cabecera );                   
            }
            con.commit();

            json_res.addProperty("status", "200");

        } catch (Exception e) {
            json_res.addProperty("error", e.getMessage());
            try {
                con.rollback();
            } catch (SQLException ex) {
                Logger.getLogger(ComprasProcesoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException e) {
                        throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                    }
                }
                if (con != null) {
                    try {
                        con.setAutoCommit(true);
                        this.desconectar(con);
                    } catch (SQLException e) {
                        throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage());
                    }
                }
            } catch (Exception ex) {
            }
            return json_res.toString();
        }

    }

    public String GuardarDetalleSolicitudEjecucion(String id_solicitud_ejecucion, String codigo_insumo, String descripcion_insumo, String cantidad, String referencia, String observacion, String unidad_de_medida, String usuario) throws Exception {
        String cadena = "";
        String query = "SQL_GUARDAR_ITEM_DETALLE_SOLICITUD_EJECUCION";
        StringStatement st = new StringStatement(this.obtenerSQL(query), true);
      
        try {
            st.setString(1, id_solicitud_ejecucion);
            st.setString(2, codigo_insumo);
            st.setString(3, descripcion_insumo);
            st.setString(4, unidad_de_medida);
            st.setString(5, unidad_de_medida);
            st.setString(6, cantidad);
            st.setString(7, usuario);
            st.setString(8, usuario);             

            cadena += st.getSql();

        } catch (Exception e) {
            throw new Exception("Error : Hubo problemas al construir el insert  SQL_GUARDAR_ITEM_DETALLE_SOLICITUD_EJECUCION");
        }

        return cadena;
    }
    
    public String GuardarItemDetalle(String id_inventario, String codigo_insumo, String descripcion_insumo, String cantidad, String referencia, String observacion, String unidad_de_medida, String usuario , String id_solicitud_proyecto , String id_bodega) throws Exception {
        String cadena = "";
        String query = "SQL_GUARDAR_ITEM_DETALLE";
        StringStatement st = new StringStatement(this.obtenerSQL(query), true);

        try {
            st.setString(1, id_inventario);
            st.setString(2, codigo_insumo);
            st.setString(3, descripcion_insumo);
            st.setString(4, cantidad);
            st.setString(5, referencia);
            st.setString(6, observacion);
            st.setString(7, unidad_de_medida);
            st.setString(8, usuario);
            st.setString(9, unidad_de_medida);
            
            st.setString(10, id_bodega);
            st.setString(11, codigo_insumo);
            st.setString(12, id_solicitud_proyecto);
            st.setString(13, unidad_de_medida);
            
            
            st.setString(14, cantidad);
            
            st.setString(15, id_bodega);
            st.setString(16, codigo_insumo);
            st.setString(17, id_solicitud_proyecto);
            st.setString(18, unidad_de_medida);          
            

            cadena += st.getSql();

        } catch (Exception e) {
            throw new Exception("Error : Existio problemas al construir el insert  SQL_GUARDAR_ITEM_DETALLE");
        }

        return cadena;
    }    
    
    public String GuardarItemDetalleEntrada(String id_inventario, String codigo_insumo, String descripcion_insumo, String cantidad, String referencia, String observacion, String unidad_de_medida, String usuario , String valor   ) throws Exception {
        String cadena = "";
        String query = "SQL_GUARDAR_ITEM_DETALLE_ENTRADA";
        StringStatement st = new StringStatement(this.obtenerSQL(query), true);

        try {
            st.setString(1, id_inventario);
            st.setString(2, codigo_insumo);
            st.setString(3, descripcion_insumo);
            st.setString(4, cantidad);
            st.setString(5, referencia);
            st.setString(6, observacion);
            st.setString(7, unidad_de_medida);
            st.setString(8, usuario);
            st.setString(9, unidad_de_medida);
            
            st.setString(10, valor);
            st.setString(11, cantidad);
            st.setString(12, valor);
            
            cadena += st.getSql();

        } catch (Exception e) {
            throw new Exception("Error : Existio problemas al construir el insert  SQL_GUARDAR_ITEM_DETALLE_ENTRADA");
        }

        return cadena;
    }    

    private void MoverKardex(String tipo_movimiento, String id_bodega, String codigo_material, String solicitar, String descripcion_insumo, String nombre_usuario, String id_bodega_destino, String unidad_de_medida, Statement stmt, String id_solicitud, String id_solicitud_destino , String id_movimiento ) throws Exception {
        String cadena = "";
        double cantidad = Double.parseDouble(solicitar);
        if ((tipo_movimiento.equals("2")) || (tipo_movimiento.equals("3"))) {
            cantidad = cantidad * -1;
        }
        try {
            //SI ES TRASPASO MUEVE DOS VECES EL KARDEX HACIENDO UNA ENTRADA Y UNA SALIDA.
            if (tipo_movimiento.equals("3") && id_bodega_destino != null) {
                if (ObtenerNumeroDeMateriales(id_bodega, codigo_material, id_solicitud , unidad_de_medida) == 0) {
                    stmt.addBatch(InsertarMaterial(id_bodega, codigo_material, cantidad, descripcion_insumo, nombre_usuario, unidad_de_medida, id_solicitud));
                } else {
                    stmt.addBatch(ActualizarMaterial(id_bodega, codigo_material, cantidad, nombre_usuario, id_solicitud , unidad_de_medida ));
                    
                }
                if (ObtenerNumeroDeMateriales(id_bodega_destino, codigo_material, id_solicitud_destino ,unidad_de_medida) == 0) {
                    stmt.addBatch(InsertarMaterial(id_bodega_destino, codigo_material, Double.parseDouble(solicitar), descripcion_insumo, nombre_usuario, unidad_de_medida,id_solicitud_destino));
                    stmt.addBatch(InsertarCostoKardex(id_bodega, id_bodega_destino, codigo_material, id_solicitud , id_solicitud_destino, unidad_de_medida ));
                } else {
                    stmt.addBatch(ActualizarCostoKardex(id_bodega, id_bodega_destino, codigo_material, Double.parseDouble(solicitar), id_solicitud , id_solicitud_destino, unidad_de_medida ));
                    stmt.addBatch(ActualizarMaterial(id_bodega_destino, codigo_material, Double.parseDouble(solicitar), nombre_usuario, id_solicitud_destino , unidad_de_medida));
                }
                //stmt.addBatch(MovimientoTrasladoApoteosys(id_bodega, id_bodega_destino, codigo_material,  id_solicitud , id_solicitud_destino, unidad_de_medida, solicitar, id_movimiento ));
                //SI ES ENTRADA O SALIDA.
            } else {
                if (ObtenerNumeroDeMateriales(id_bodega, codigo_material, id_solicitud , unidad_de_medida) == 0) {
                    stmt.addBatch(InsertarMaterialEntrada(id_bodega, codigo_material, cantidad, descripcion_insumo, nombre_usuario, unidad_de_medida, id_solicitud, id_movimiento));
                } else {
                    stmt.addBatch(ActualizarMaterial(id_bodega, codigo_material, cantidad, nombre_usuario, id_solicitud , unidad_de_medida));
                    if(tipo_movimiento.equals("1")){                        
                        stmt.addBatch(ActualizarCostoKardexEntrada(id_bodega, codigo_material, Double.parseDouble(solicitar), id_solicitud, unidad_de_medida, id_movimiento ));
                    }                                          
                }
            }

        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    private String InsertarMaterial(String id_bodega, String codigo_material, double cantidad, String descripcion_insumo, String nombre_usuario, String unidad_de_medida, String id_solicitud) throws Exception {

        String cadena = "";
        String query = "SQL_INSERTAR_MATERIAL_EN_KARDEX";
        StringStatement st = new StringStatement(this.obtenerSQL(query), true);

        try {

            st.setString(1, id_bodega);
            st.setString(2, codigo_material);
            st.setString(3, String.valueOf(cantidad));
            st.setString(4, nombre_usuario);
            st.setString(5, descripcion_insumo);
            st.setString(6, unidad_de_medida);
            st.setString(7, id_solicitud);

            cadena += st.getSql();

        } catch (Exception e) {
            throw new Exception("Error : Existio problemas al construir el insert  SQL_INSERTAR_MATERIAL_EN_KARDEX");
        }

        return cadena;

    }
    
    private String InsertarMaterialEntrada(String id_bodega, String codigo_material, double cantidad, String descripcion_insumo, String nombre_usuario, String unidad_de_medida, String id_solicitud , String id_movimiento) throws Exception {

        String cadena = "";
        String query = "SQL_INSERTAR_MATERIAL_EN_KARDEX_ENTRADA";
        StringStatement st = new StringStatement(this.obtenerSQL(query), true);

        try {

            st.setString(1, id_bodega);
            st.setString(2, codigo_material);
            st.setString(3, String.valueOf(cantidad));
            st.setString(4, nombre_usuario);
            st.setString(5, descripcion_insumo);
            st.setString(6, unidad_de_medida);
            st.setString(7, id_solicitud);
            st.setString(8, id_movimiento);
            st.setString(9, codigo_material);
            st.setString(10, unidad_de_medida);

            cadena += st.getSql();

        } catch (Exception e) {
            throw new Exception("Error : Existio problemas al construir el insert  SQL_INSERTAR_MATERIAL_EN_KARDEX");
        }

        return cadena;

    }

    private String ActualizarMaterial(String id_bodega, String codigo_material, double cantidad, String nombre_usuario, String id_solicitud) throws Exception {

        String cadena = "";
        String query = "SQL_ACTUALIZAR_MATERIAL_EN_KARDEX";
        StringStatement st = new StringStatement(this.obtenerSQL(query), true);

        try {

            st.setString(1, String.valueOf(cantidad));
            st.setString(2, nombre_usuario);
            st.setString(3, id_solicitud);
            st.setString(4, id_bodega);
            st.setString(5, codigo_material);
            st.setString(6, id_solicitud);

            cadena += st.getSql();

        } catch (Exception e) {

            throw new Exception("Error : Existio problemas al construir el update  SQL_ACTUALIZAR_MATERIAL_EN_KARDEX ");
        }

        return cadena;
    }

    private String ActualizarMaterial(String id_bodega, String codigo_material, double cantidad, String nombre_usuario, String id_solicitud, String unidad_de_medida ) throws Exception {

        String cadena = "";
        String query = "SQL_ACTUALIZAR_MATERIAL_EN_KARDEX_2";
        StringStatement st = new StringStatement(this.obtenerSQL(query), true);

        try {

            st.setString(1, String.valueOf(cantidad));
            st.setString(2, nombre_usuario);
            st.setString(3, id_solicitud);
            st.setString(4, id_bodega);
            st.setString(5, codigo_material);
            st.setString(6, id_solicitud);
            st.setString(7, (unidad_de_medida.equals(""))?"19":unidad_de_medida);

            cadena += st.getSql();

        } catch (Exception e) {

            throw new Exception("Error : Existio problemas al construir el update  SQL_ACTUALIZAR_MATERIAL_EN_KARDEX_2 ");
        }

        return cadena;
    }
    
    private String ActualizarCostoKardex(String id_bodega_origen, String id_bodega_destino, String codigo_material, double cantidad,  String id_solicitud_origen, String id_solicitud_destino, String unidad_de_medida ) throws Exception {

        String cadena = "";
        String query = "SQL_ACTUALIZAR_COSTO_MATERIAL_EN_KARDEX";
        StringStatement st = new StringStatement(this.obtenerSQL(query), true);

        try {

            st.setString(1, String.valueOf(cantidad));
            st.setString(2, id_bodega_origen);
            st.setString(3, codigo_material);
            st.setString(4, id_solicitud_origen);
            st.setString(5, (unidad_de_medida.equals(""))?"19":unidad_de_medida);
            st.setString(6, String.valueOf(cantidad));
            st.setString(7, id_bodega_destino);
            st.setString(8, codigo_material);
            st.setString(9, id_solicitud_destino);
            st.setString(10, (unidad_de_medida.equals(""))?"19":unidad_de_medida);

            cadena += st.getSql();

        } catch (Exception e) {

            throw new Exception("Error : Existio problemas al construir el update  SQL_ACTUALIZAR_COSTO_MATERIAL_EN_KARDEX ");
        }

        return cadena;
    }
    
    
    private String ActualizarCostoKardexEntrada(  String id_bodega_destino, String codigo_material, double cantidad,  String id_solicitud_destino, String unidad_de_medida , String id_movimiento) throws Exception {

        String cadena = "";
        String query = "SQL_ACTUALIZAR_COSTO_MATERIAL_EN_KARDEX_ENTRADA";
        StringStatement st = new StringStatement(this.obtenerSQL(query), true);

        try {

            st.setString(1, String.valueOf(cantidad));
            st.setString(2, id_movimiento);
            st.setString(3, codigo_material);
            st.setString(4, (unidad_de_medida.equals(""))?"19":unidad_de_medida);
            st.setString(5, String.valueOf(cantidad));
            st.setString(6, id_bodega_destino);
            st.setString(7, codigo_material);
            st.setString(8, id_solicitud_destino);
            st.setString(9, (unidad_de_medida.equals(""))?"19":unidad_de_medida);

            cadena += st.getSql();

        } catch (Exception e) {

            throw new Exception("Error : Existio problemas al construir el update  SQL_ACTUALIZAR_COSTO_MATERIAL_EN_KARDEX ");
        }

        return cadena;
    }
    
    private String InsertarCostoKardex(String id_bodega_origen, String id_bodega_destino, String codigo_material, String id_solicitud_origen, String id_solicitud_destino, String unidad_de_medida ) throws Exception {

        String cadena = "";
        String query = "SQL_INSERTAR_COSTO_MATERIAL_EN_KARDEX";
        StringStatement st = new StringStatement(this.obtenerSQL(query), true);

        try {

            st.setString(1, id_bodega_origen);
            st.setString(2, codigo_material);
            st.setString(3, id_solicitud_origen);
            st.setString(4, (unidad_de_medida.equals(""))?"19":unidad_de_medida);
            st.setString(5, id_bodega_destino);
            st.setString(6, codigo_material);
            st.setString(7, id_solicitud_destino);
            st.setString(8, (unidad_de_medida.equals(""))?"19":unidad_de_medida);

            cadena += st.getSql();

        } catch (Exception e) {

            throw new Exception("Error : Existio problemas al construir el insert  SQL_INSERTAR_COSTO_MATERIAL_EN_KARDEX ");
        }

        return cadena;
    }
    
    private String MovimientoTrasladoApoteosys(String id_bodega_origen, String id_bodega_destino, String codigo_material, String id_solicitud_origen, String id_solicitud_destino, String unidad_de_medida , String cantidad , String id_inventario) throws Exception {

        String cadena = "";
        String query = "SQL_MOVIMIENTO_TRASLADO_APOTEOSYS";
        StringStatement st = new StringStatement(this.obtenerSQL(query), true);

        try {

            st.setString(1, id_bodega_origen);
            st.setString(2, id_bodega_destino);
            st.setString(3, codigo_material);
            st.setString(4, id_solicitud_origen);
            st.setString(5, id_solicitud_destino);
            st.setString(6, (unidad_de_medida.equals(""))?"19":unidad_de_medida);
            st.setString(7, cantidad);
            st.setString(8, id_inventario);
            cadena += st.getSql();

        } catch (Exception e) {

            throw new Exception("Error : Existio problemas al construir el insert  SQL_INSERTAR_COSTO_MATERIAL_EN_KARDEX ");
        }

        return cadena;
    }

    
    private void MovimientoTrasladoApoteosys(String id_bodega_origen, String id_bodega_destino,  String id_solicitud_origen, String id_solicitud_destino, String id_inventario) throws Exception {
        rs=null;
        String query = "SQL_MOVIMIENTO_TRASLADO_APOTEOSYS_2";
        query = this.obtenerSQL(query);
        try {

            ps = con.prepareStatement(query);
            ps.setString(1, id_bodega_origen);
            ps.setString(2, id_bodega_destino);
            ps.setString(3, id_solicitud_origen);
            ps.setString(4, id_solicitud_destino);
            ps.setString(5, id_inventario);
            
            System.out.print(ps);

           
            rs = ps.executeQuery();
            JsonObject objetoJson;
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
            }

        } catch (Exception e) {

            throw new Exception("Error : Existio problemas en el S.P.  SQL_MOVIMIENTO_TRASLADO_APOTEOSYS_2 ::: " + e.getMessage());
        }
    }
    
    private void MovimientoEntradaApoteosys(String id_bodega_origen,   String id_solicitud_origen, String id_inventario) throws Exception {
        rs=null;
        String query = "SQL_MOVIMIENTO_ENTRADA_APOTEOSYS";
        query = this.obtenerSQL(query);
        try {

            ps = con.prepareStatement(query);            
            ps.setString(1, id_bodega_origen);
            ps.setString(2, id_solicitud_origen);
            ps.setString(3, id_inventario);
            
            System.out.print(ps);

           
            rs = ps.executeQuery();
            JsonObject objetoJson;
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
            }

        } catch (Exception e) {

            throw new Exception("Error : Existio problemas en el S.P.  SQL_MOVIMIENTO_TRASLADO_APOTEOSYS_2 ::: " + e.getMessage());
        }
    }

    private int ObtenerNumeroDeMateriales(String id_bodega, String codigo_material, String id_solicitud , String unidad_de_medida) {
        Connection con = null;
        ResultSet rs = null;
        PreparedStatement ps = null;

        String query = "SQL_CONSULTAR_KARDEX_POR_MATERIAL_Y_BODEGA";
        String respuestaJson = "{}";
        int count = 0;

        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, codigo_material);
            ps.setString(2, id_bodega);
            ps.setString(3, id_solicitud);
            ps.setString(4, (unidad_de_medida.equals(""))?"19":unidad_de_medida);
            System.out.print(ps);

            rs = ps.executeQuery();

            while (rs.next()) {
                count = Integer.parseInt(rs.getString(rs.getMetaData().getColumnLabel(1)));
            }

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return count;
    }

    @Override
    public String imprimirPDFMovimiento(String cod_movimiento, Usuario usuario) {
        String cod_movimiento2 = cod_movimiento;
        String msg = "OK";
        try {
            String ruta = "";
            String observaciones = null, centroCosto = null, nombre_proyecto = null, contratista = null, multiservisio = null, fechaOcs = null, bodega_destino = null, id_solicitud_origen = null, id_solicitud_destino = null;
            ps = null;
            rs = null;
            con = this.conectarJNDI();
            ruta = this.directorioArchivo(usuario.getLogin(), cod_movimiento2, "pdf");
            ResourceBundle rsb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");

            DecimalFormat formato = new DecimalFormat("$###,###,###");
            DecimalFormat formato2 = new DecimalFormat("###.##");
            Font fuente = new Font(Font.HELVETICA, 10, Font.NORMAL, new java.awt.Color(0, 0, 0));
            Font f3b = new Font(Font.TIMES_ROMAN, 10, Font.HELVETICA, new Color(255, 255, 255));
            Font f4normalN = new Font(Font.HELVETICA, 8, Font.BOLD, new java.awt.Color(102, 101, 101));
            Font f4normal = new Font(Font.HELVETICA, 8, Font.NORMAL, new java.awt.Color(102, 101, 101));
            Font encabezado = new Font(Font.HELVETICA, 16, Font.BOLD, new java.awt.Color(247, 147, 29));
            Font suben = new Font(Font.HELVETICA, 10, Font.BOLD, new java.awt.Color(247, 147, 29));
            Font f5 = new Font(Font.HELVETICA, 8, Font.BOLD, new java.awt.Color(238, 129, 38));

            Document documento = null;
            documento = this.createDoc();
            PdfWriter pdfWriter = PdfWriter.getInstance(documento, new FileOutputStream(ruta));
//            FooterPDF footer = new FooterPDF(rsb);
//            pdfWriter.setPageEvent(footer);
            documento.open();
            //documento.newPage();            
            //documento.setMargins(0, 70, 0, 70);
            int cant = 0;
            int item = 1;
            Image imagen = Image.getInstance(rsb.getString("ruta") + "/images/logo_selectrik_.jpg");
            imagen.scaleToFit(220, 220);

            String queryP;
            boolean es_traslado = cod_movimiento.contains("T");
            if (es_traslado){
                queryP = "SQL_INFORMACION_MOVIMIENTO_PARA_TRASLADOS";           
            }else{
                queryP = "SQL_INFORMACION_MOVIMIENTO";                        
            }; 
            ps = con.prepareStatement(this.obtenerSQL(queryP));
            ps.setString(1, cod_movimiento2);
            rs = ps.executeQuery();
            while (rs.next()) {
                observaciones = rs.getString("ubicacion");
                centroCosto = rs.getString("cod_movimiento");
                nombre_proyecto = rs.getString("responsable");
                multiservisio = rs.getString("tipo_movimiento");
                fechaOcs = rs.getString("fecha_creacion");
                contratista = rs.getString("creation_user");
                id_solicitud_origen = rs.getString("id_solicitud_origen");
                if (es_traslado){
                    bodega_destino = rs.getString("ubicacion_destino");                    
                    id_solicitud_destino = rs.getString("id_solicitud_destino");                                        
                }

            }

            //tabla de fecha
            PdfPTable table_ = new PdfPTable(4);
            table_.setWidths(new int[]{2, 2, 2, 2});
            table_.setWidthPercentage(7);
            PdfPCell cell_;
            Calendar calendario = Calendar.getInstance();
            String dias = String.valueOf(calendario.get(Calendar.DAY_OF_MONTH));
            String mes = this.mesToString(calendario.get(Calendar.MONTH) + 1);
            String anio = String.valueOf(calendario.get(Calendar.YEAR));
            cell_ = new PdfPCell(new Phrase("FECHA", f5));
            cell_.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell_.setBorder(0);
            table_.addCell(cell_);

            cell_ = new PdfPCell(new Phrase(fechaOcs == null ? "" : fechaOcs.substring(8, 10), f4normal));
            cell_.setHorizontalAlignment(Element.ALIGN_CENTER);
            table_.addCell(cell_);

            cell_ = new PdfPCell(new Phrase(fechaOcs == null ? "" : fechaOcs.substring(5, 7), f4normal));
            cell_.setHorizontalAlignment(Element.ALIGN_CENTER);
            table_.addCell(cell_);

            cell_ = new PdfPCell(new Phrase(fechaOcs == null ? "" : fechaOcs.substring(0, 4), f4normal));
            cell_.setHorizontalAlignment(Element.ALIGN_CENTER);
            table_.addCell(cell_);
            //documento.add(table_);

            //tabla encabezado logo y titulo
            PdfPTable table_0 = new PdfPTable(2);
            table_0.setWidths(new int[]{5, 5});
            table_0.setWidthPercentage(100);
            PdfPCell cell_0;
            cell_0 = new PdfPCell(imagen);
            cell_0.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell_0.setBorder(0);
            table_0.addCell(cell_0);

            cell_0 = new PdfPCell(new Phrase("CODIGO DE MOVIMIENTO " + cod_movimiento2, encabezado));
            cell_0.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell_0.setVerticalAlignment(Element.ALIGN_CENTER);
            cell_0.setBorderWidthRight(0f);
            cell_0.setBorderWidthBottom(0f);
            cell_0.setBorderWidthTop(0f);
            cell_0.setBorderWidthLeft(0f);
            cell_0.setPaddingTop(50);
            table_0.addCell(cell_0);

            cell_0 = new PdfPCell(new Phrase(" ", suben));
            cell_0.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell_0.setBorder(0);
            table_0.addCell(cell_0);

            //A�ADE LA TABLA DE FECHA
            cell_0 = new PdfPCell(table_);
            cell_0.setBorderWidthRight(0f);
            cell_0.setBorderWidthBottom(0f);
            cell_0.setBorderWidthTop(0f);
            cell_0.setBorderWidthLeft(0f);
            cell_0.setPaddingLeft(45);
            table_0.addCell(cell_0);
            documento.add(table_0);

            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(new Paragraph(new Phrase(" ", fuente)));

            String query = "SQL_ORDEN_COMPRA";
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, cod_movimiento);
            rs = ps.executeQuery();
            while (rs.next()) {
                //primer cuadro            
                PdfPTable table_1 = new PdfPTable(4);
                table_1.setWidths(new int[]{3, 3, 3, 2});
                table_1.setWidthPercentage(100);
                PdfPCell cell_1;

                cell_1 = new PdfPCell(new Phrase("PROVEEDOR", f3b));
                cell_1.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell_1.setBackgroundColor(new Color(93, 93, 93));
                cell_1.setBorderWidthRight(0);
                cell_1.setColspan(2);
                table_1.addCell(cell_1);

                cell_1 = new PdfPCell(new Phrase("DATOS ENVIO", f3b));
                cell_1.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell_1.setBackgroundColor(new Color(93, 93, 93));
                cell_1.setColspan(2);
                table_1.addCell(cell_1);

                cell_1 = new PdfPCell(new Phrase("NOMBRE DE EMPRESA: ", f4normalN));
                cell_1.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell_1.setBorderWidthRight(0f);
                cell_1.setBorderWidthBottom(0f);
                cell_1.setBorderWidthTop(0f);
                table_1.addCell(cell_1);

                cell_1 = new PdfPCell(new Phrase(rs.getString("nombre_proveedor"), f4normal));
                cell_1.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell_1.setBorderWidthRight(0f);
                cell_1.setBorderWidthBottom(0f);
                cell_1.setBorderWidthTop(0f);
                cell_1.setBorderWidthLeft(0f);
                table_1.addCell(cell_1);

                cell_1 = new PdfPCell(new Phrase("RESPONSALE: ", f4normalN));
                cell_1.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell_1.setBorderWidthRight(0f);
                cell_1.setBorderWidthBottom(0f);
                cell_1.setBorderWidthTop(0f);
                cell_1.setBorderWidthLeft(0f);
                table_1.addCell(cell_1);

                cell_1 = new PdfPCell(new Phrase(rs.getString("responsable_envio"), f4normal));
                cell_1.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell_1.setBorderWidthBottom(0f);
                cell_1.setBorderWidthTop(0f);
                cell_1.setBorderWidthLeft(0f);
                table_1.addCell(cell_1);

                cell_1 = new PdfPCell(new Phrase("CONTACTO O DEPARTAMENTO: ", f4normalN));
                cell_1.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell_1.setBorderWidthRight(0f);
                cell_1.setBorderWidthBottom(0f);
                cell_1.setBorderWidthTop(0f);
                table_1.addCell(cell_1);

                cell_1 = new PdfPCell(new Phrase("COMPRAS ", f4normal));
                cell_1.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell_1.setBorderWidthRight(0f);
                cell_1.setBorderWidthBottom(0f);
                cell_1.setBorderWidthTop(0f);
                cell_1.setBorderWidthLeft(0f);
                table_1.addCell(cell_1);

                cell_1 = new PdfPCell(new Phrase("DIRECCION BODEGA O PROYECTO: ", f4normalN));
                cell_1.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell_1.setBorderWidthRight(0f);
                cell_1.setBorderWidthBottom(0f);
                cell_1.setBorderWidthTop(0f);
                cell_1.setBorderWidthLeft(0f);
                table_1.addCell(cell_1);

                cell_1 = new PdfPCell(new Phrase(rs.getString("direccion_entrega"), f4normal));
                cell_1.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell_1.setBorderWidthBottom(0f);
                cell_1.setBorderWidthTop(0f);
                cell_1.setBorderWidthLeft(0f);
                table_1.addCell(cell_1);

                cell_1 = new PdfPCell(new Phrase("DIRECCION: ", f4normalN));
                cell_1.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell_1.setBorderWidthRight(0f);
                cell_1.setBorderWidthBottom(0f);
                cell_1.setBorderWidthTop(0f);
                table_1.addCell(cell_1);

                cell_1 = new PdfPCell(new Phrase(rs.getString("direccion_proveedor"), f4normal));
                cell_1.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell_1.setBorderWidthRight(0f);
                cell_1.setBorderWidthBottom(0f);
                cell_1.setBorderWidthTop(0f);
                cell_1.setBorderWidthLeft(0f);
                table_1.addCell(cell_1);

                cell_1 = new PdfPCell(new Phrase("CIUDAD: ", f4normalN));
                cell_1.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell_1.setBorderWidthBottom(0f);
                cell_1.setBorderWidthTop(0f);
                cell_1.setBorderWidthLeft(0f);
                cell_1.setBorderWidthRight(0f);
                table_1.addCell(cell_1);

                //cell_1 = new PdfPCell(new Phrase(rs.getString("ciudad_envio"), f4normal));
                cell_1 = new PdfPCell(new Phrase("", f4normal));
                cell_1.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell_1.setBorderWidthBottom(0f);
                cell_1.setBorderWidthTop(0f);
                cell_1.setBorderWidthLeft(0f);
                table_1.addCell(cell_1);

                cell_1 = new PdfPCell(new Phrase("CIUDAD: ", f4normalN));
                cell_1.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell_1.setBorderWidthRight(0f);
                cell_1.setBorderWidthBottom(0f);
                cell_1.setBorderWidthTop(0f);
                table_1.addCell(cell_1);

                cell_1 = new PdfPCell(new Phrase(rs.getString("ciudad_proveedor"), f4normal));
                cell_1.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell_1.setBorderWidthBottom(0f);
                cell_1.setBorderWidthTop(0f);
                cell_1.setBorderWidthLeft(0f);
                cell_1.setBorderWidthRight(0f);
                table_1.addCell(cell_1);

                cell_1 = new PdfPCell(new Phrase("TELEFONO: ", f4normalN));
                cell_1.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell_1.setBorderWidthRight(0f);
                cell_1.setBorderWidthBottom(0f);
                cell_1.setBorderWidthTop(0f);
                cell_1.setBorderWidthLeft(0f);
                table_1.addCell(cell_1);

                cell_1 = new PdfPCell(new Phrase(rs.getString("telefono_envio"), f4normal));
                cell_1.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell_1.setBorderWidthLeft(0f);
                cell_1.setBorderWidthBottom(0f);
                cell_1.setBorderWidthTop(0f);
                table_1.addCell(cell_1);

                cell_1 = new PdfPCell(new Phrase("TELEFONO: ", f4normalN));
                cell_1.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell_1.setBorderWidthTop(0f);
                cell_1.setBorderWidthRight(0f);
                table_1.addCell(cell_1);

                cell_1 = new PdfPCell(new Phrase(rs.getString("telefono_proveedor"), f4normal));
                cell_1.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell_1.setColspan(3);
                cell_1.setBorderWidthTop(0f);
                cell_1.setBorderWidthLeft(0f);
                table_1.addCell(cell_1);
                documento.add(table_1);

                documento.add(new Paragraph(new Phrase(" ", fuente)));

            }

            //segundo cuadro
            PdfPTable table_2 = new PdfPTable(4);
            table_2.setWidths(new int[]{3, 4, 3, 3});
            table_2.setWidthPercentage(100);
            PdfPCell cell_2;

            cell_2 = new PdfPCell(new Phrase("TIPO DE MOVIMIENTO", f3b));
            cell_2.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell_2.setBackgroundColor(new Color(93, 93, 93));
            table_2.addCell(cell_2);

            cell_2 = new PdfPCell(new Phrase("RESPONSABLE", f3b));
            cell_2.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell_2.setBackgroundColor(new Color(93, 93, 93));
            table_2.addCell(cell_2);

            cell_2 = new PdfPCell(new Phrase("USUARIO DE CREACION", f3b));
            cell_2.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell_2.setBackgroundColor(new Color(93, 93, 93));
            table_2.addCell(cell_2);
            
            cell_2 = new PdfPCell(new Phrase("ID PROYECTO", f3b));
            cell_2.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell_2.setBackgroundColor(new Color(93, 93, 93));
            table_2.addCell(cell_2);            

            cell_2 = new PdfPCell(new Phrase(multiservisio == null ? "" : multiservisio, f4normal));
            cell_2.setHorizontalAlignment(Element.ALIGN_CENTER);
            table_2.addCell(cell_2);

            cell_2 = new PdfPCell(new Phrase(nombre_proyecto == null ? "" : nombre_proyecto, f4normal));
            cell_2.setHorizontalAlignment(Element.ALIGN_CENTER);
            table_2.addCell(cell_2);

            cell_2 = new PdfPCell(new Phrase(contratista == null ? "" : contratista, f4normal));
            cell_2.setHorizontalAlignment(Element.ALIGN_CENTER);
            table_2.addCell(cell_2);

            cell_2 = new PdfPCell(new Phrase(id_solicitud_origen == null ? "" : id_solicitud_origen, f4normal));
            cell_2.setHorizontalAlignment(Element.ALIGN_CENTER);
            table_2.addCell(cell_2);            
            documento.add(table_2);

            documento.add(new Paragraph(new Phrase(" ", fuente)));

            //tercera tabla
            PdfPTable table_3 = new PdfPTable(4);
            table_3.setWidths(new int[]{3, 5, 2, 3});
            table_3.setWidthPercentage(100);
            PdfPCell cell_3;

            cell_3 = new PdfPCell(new Phrase("REFERENCIA", f3b));
            cell_3.setHorizontalAlignment(Element.ALIGN_CENTER);
            
            cell_3.setBackgroundColor(new Color(93, 93, 93));
            table_3.addCell(cell_3);

            cell_3 = new PdfPCell(new Phrase("DESCRIPCION", f3b));
            cell_3.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell_3.setBackgroundColor(new Color(93, 93, 93));
            table_3.addCell(cell_3);

            cell_3 = new PdfPCell(new Phrase("CANT", f3b));
            cell_3.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell_3.setBackgroundColor(new Color(93, 93, 93));
            table_3.addCell(cell_3);

            cell_3 = new PdfPCell(new Phrase("UNIDADES", f3b));
            cell_3.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell_3.setBackgroundColor(new Color(93, 93, 93));
            table_3.addCell(cell_3);

            double valorTotal = 0;
            String query2 = "SQL_INVENTARIO_DETALLE";
            ps = con.prepareStatement(this.obtenerSQL(query2));
            ps.setString(1, cod_movimiento2);
            rs = ps.executeQuery();
            while (rs.next()) {
                // if (item == cant) {
                cell_3 = new PdfPCell(new Phrase(rs.getString("codigo_insumo"), f4normal));
                cell_3.setHorizontalAlignment(Element.ALIGN_CENTER);
//                    cell_3.setBorderWidthTop(0f);
                table_3.addCell(cell_3);

                cell_3 = new PdfPCell(new Phrase(rs.getString("descripcion_insumo"), f4normal));
                cell_3.setHorizontalAlignment(Element.ALIGN_CENTER);
//                    cell_3.setBorderWidthTop(0f);
                table_3.addCell(cell_3);

                cell_3 = new PdfPCell(new Phrase(rs.getString("cantidad"), f4normal));
                cell_3.setHorizontalAlignment(Element.ALIGN_CENTER);
//                    cell_3.setBorderWidthTop(0f);
                table_3.addCell(cell_3);

                cell_3 = new PdfPCell(new Phrase(rs.getString("nombre_unidad"), f4normal));
                cell_3.setHorizontalAlignment(Element.ALIGN_CENTER);
//                    cell_3.setBorderWidthTop(0f);
                table_3.addCell(cell_3);

               // }
                item = item + 1;
            }
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (con != null) {
                this.desconectar(con);
            }
            documento.add(table_3);

            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(new Paragraph(new Phrase(" ", fuente)));

            PdfPTable table_7 = new PdfPTable(1);
            table_7.setWidths(new int[]{5});
            table_7.setWidthPercentage(100);
            PdfPCell cell_7;                        
            
            if (es_traslado){
                cell_7 = new PdfPCell(new Phrase("BODEGA DE ORIGEN", f3b));
                cell_7.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell_7.setBackgroundColor(new Color(93, 93, 93));
                cell_7.setColspan(5);
                table_7.addCell(cell_7);

                cell_7 = new PdfPCell(new Phrase(observaciones == null ? "" : observaciones, f4normal));
                cell_7.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell_7.setColspan(5);
                cell_7.setBorderWidthBottom(0f);
                table_7.addCell(cell_7);
                
                cell_7 = new PdfPCell(new Phrase(" ", f4normal));
                cell_7.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell_7.setColspan(5);
                cell_7.setBorderWidthTop(0f);
                table_7.addCell(cell_7);                
                                
                cell_7 = new PdfPCell(new Phrase("BODEGA DE DESTINO", f3b));
                cell_7.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell_7.setBackgroundColor(new Color(93, 93, 93));
                cell_7.setColspan(5);
                table_7.addCell(cell_7);

                cell_7 = new PdfPCell(new Phrase(bodega_destino == null ? "" : bodega_destino, f4normal));
                cell_7.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell_7.setColspan(5);
                cell_7.setBorderWidthBottom(0f);
                table_7.addCell(cell_7);
                
                cell_7 = new PdfPCell(new Phrase(" ", f4normal));
                cell_7.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell_7.setColspan(5);
                cell_7.setBorderWidthTop(0f);
                table_7.addCell(cell_7);                    
                
                cell_7 = new PdfPCell(new Phrase("PROYECTO DE DESTINO", f3b));
                cell_7.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell_7.setBackgroundColor(new Color(93, 93, 93));
                cell_7.setColspan(5);
                table_7.addCell(cell_7);
                
                cell_7 = new PdfPCell(new Phrase(id_solicitud_destino == null ? "" : id_solicitud_destino, f4normal));
                cell_7.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell_7.setColspan(5);
                cell_7.setBorderWidthBottom(0f);
                table_7.addCell(cell_7);                  
            }else{
                cell_7 = new PdfPCell(new Phrase("BODEGA", f3b));
                cell_7.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell_7.setBackgroundColor(new Color(93, 93, 93));
                cell_7.setColspan(5);
                table_7.addCell(cell_7);

                cell_7 = new PdfPCell(new Phrase(observaciones == null ? "" : observaciones, f4normal));
                cell_7.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell_7.setColspan(5);
                cell_7.setBorderWidthBottom(0f);
                table_7.addCell(cell_7);                
            }                                                                                                          

            cell_7 = new PdfPCell(new Phrase(" ", f4normal));
            cell_7.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell_7.setColspan(5);
            cell_7.setBorderWidthTop(0f);
            table_7.addCell(cell_7);

            documento.add(table_7);

            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(new Paragraph(new Phrase(" ", fuente)));

            PdfPTable table_5 = new PdfPTable(1);
            table_5.setWidths(new int[]{3});
            table_5.setWidthPercentage(100);
            PdfPCell cell_5;

            cell_5 = new PdfPCell(new Phrase("", f4normal));
            cell_5.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell_5.setBorderWidthBottom(0f);
            cell_5.setBorderWidthTop(0f);
            cell_5.setFixedHeight(10);
            table_5.addCell(cell_5);

            cell_5 = new PdfPCell(new Phrase(" ", f4normal));
            cell_5.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell_5.setBorderWidthBottom(0f);
            cell_5.setBorderWidthTop(0f);
            cell_5.setFixedHeight(10);
            table_5.addCell(cell_5);

            cell_5 = new PdfPCell(new Phrase("", f4normal));
            cell_5.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell_5.setBorderWidthBottom(0f);
            cell_5.setBorderWidthTop(0f);
            cell_5.setFixedHeight(10);
            table_5.addCell(cell_5);

            cell_5 = new PdfPCell(new Phrase("", f4normal));
            cell_5.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell_5.setBorderWidthBottom(0f);
            cell_5.setBorderWidthTop(0f);
            cell_5.setFixedHeight(10);
            table_5.addCell(cell_5);

            cell_5 = new PdfPCell(new Phrase(formato.format(valorTotal), f4normal));
            cell_5.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell_5.setBorderWidthBottom(0f);
            cell_5.setBorderWidthTop(0f);
            cell_5.setFixedHeight(10);
            table_5.addCell(cell_5);
            //documento.add(table_5);

            PdfPTable table_6 = new PdfPTable(1);
            table_6.setWidths(new int[]{3});
            table_6.setWidthPercentage(100);
            PdfPCell cell_6;

            cell_6 = new PdfPCell(new Phrase(formato.format(valorTotal), f4normal));
            cell_6.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell_6.setBorderWidthBottom(0f);
            cell_6.setBorderWidthTop(0f);
            table_6.addCell(cell_6);

            //cuarto cuadro
            PdfPTable table_4 = new PdfPTable(2);
            table_4.setWidths(new int[]{3,3});
            table_4.setWidthPercentage(100);
            PdfPCell cell_4;

            cell_4 = new PdfPCell(new Phrase("FIRMA:________________________________________________", f4normal));
            cell_4.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell_4.setBorderColor(Color.WHITE);
            table_4.addCell(cell_4);
            
            cell_4 = new PdfPCell(new Phrase("FIRMA:________________________________________________", f4normal));
            cell_4.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell_4.setBorderColor(Color.WHITE);
            table_4.addCell(cell_4);

            documento.add(table_4);

            documento.close();
        } catch (Exception e) {
            msg = "ERROR";
            e.printStackTrace();
        }
        return msg;
    }

    private boolean EsSuperBodeguista(String usuario) {
        Connection con = null;
        ResultSet rs = null;
        PreparedStatement ps = null;

        String query = "SQL_CONSULTAR_SI_USUARIO_ES_SUPERBODEGUISTA";
        String respuestaJson = "{}";
        int count = 0;
        boolean es_bodeguista = false;

        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, usuario);
            System.out.print(ps);

            rs = ps.executeQuery();

            while (rs.next()) {
                count = Integer.parseInt(rs.getString(rs.getMetaData().getColumnLabel(1)));
            }
        if (count > 0){
        es_bodeguista = true;
        }

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return es_bodeguista;
    }

    @Override
    public String CargarHistoricoMaterialEnBodega(String id_bodega, String cod_material, String id_solicitud) {

        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_HISTORICO_DE_MATERIALES";
        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            String consulta = "";

            consulta = this.obtenerSQL(query);
            ps = con.prepareStatement(consulta);

            ps.setString(1, id_bodega);
            ps.setString(2, id_bodega);
            ps.setString(3, cod_material);
            ps.setString(4, id_solicitud);
            ps.setString(5, id_solicitud);               
            System.out.println(ps);

            rs = ps.executeQuery();

            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);

        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\", \"mensaje\": \""+e.getMessage()+"\"";
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }

            return informacion;

        }

    }

    @Override
    public String VerificarOCS(String ocs) {
        Connection con = null;
        ResultSet rs = null;
        PreparedStatement ps = null;

        String query = "SQL_VERIFICAR_OCS";
        String resultado = "{\"respuesta\":\"ok\"}";
        String rs_query = "";

        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, ocs);
            System.out.print(ps);

            rs = ps.executeQuery();

            while (rs.next()) {
                rs_query = rs.getString(rs.getMetaData().getColumnLabel(1));
            }
        if (!rs_query.equals("OK")){
            resultado = "{\"respuesta\":\"error\"}";
        }
        } catch (Exception e) {
            resultado = "{\"respuesta\":\"error\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return resultado;
    }
    
    @Override
    public String cargarSolicitudesEjecucion(String usuario) {
        Connection con = null;
        ResultSet rs = null;
        PreparedStatement ps = null;

        String query = "SQL_CARGAR_SOLICITUDES_EJECUCION";
        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        int id_solicitud_ejecucion = 0;
        String detalle_solicitud = "";

        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, usuario);
            System.out.print(ps);

            rs = ps.executeQuery();

            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                    if (rs.getMetaData().getColumnLabel(i).equals("id")){
                        detalle_solicitud = cargarDetalleSolicitudEjecucion(rs.getString(rs.getMetaData().getColumnLabel(i)));
                        objetoJson.addProperty("detalle", detalle_solicitud);
                    }
                }
                lista.add(objetoJson);
            }
            
        informacion = new Gson().toJson(lista);
        
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\", \"mensaje\": \""+e.getMessage()+"\"";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return informacion;
    }

    @Override
    public String cargarSolicitudesEjecucionParaDespacho(String usuario) {
        Connection con = null;
        ResultSet rs = null;
        PreparedStatement ps = null;

        String query = "SQL_CARGAR_SOLICITUDES_EJECUCION_PARA_DESPACHO";
        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        int id_solicitud_ejecucion = 0;
        String detalle_solicitud = "";

        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, usuario);
            System.out.print(ps);

            rs = ps.executeQuery();

            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                    if (rs.getMetaData().getColumnLabel(i).equals("id")){
                        detalle_solicitud = cargarDetalleSolicitudEjecucion(rs.getString(rs.getMetaData().getColumnLabel(i)));
                        objetoJson.addProperty("detalle", detalle_solicitud);
                    }
                }
                lista.add(objetoJson);
            }
            
        informacion = new Gson().toJson(lista);
        
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\", \"mensaje\": \""+e.getMessage()+"\"";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return informacion;
    }    
    
    @Override
    public String cargarDetalleSolicitudEjecucion(String id_solicitud_ejecucion) {
        Connection con = null;
        ResultSet rs = null;
        PreparedStatement ps = null;

        String query = "SQL_CARGAR_DETALLE_SOLICITUD_EJECUCION";
        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";

        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, id_solicitud_ejecucion);
            System.out.print(ps);

            rs = ps.executeQuery();

            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }
            
        informacion = new Gson().toJson(lista);
        
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\", \"mensaje\": \""+e.getMessage()+"\"";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return informacion;
    }        

    public String guardarDetalleSolicitudEjecucion(String id_solicitud, String id_bodega_ejecucion, String id_bodega_proyecto, String fecha_esperada_entrega, String observaciones, String usuario, String detalle) {                                                   
            rs = null;
            ps = null;
            String query = "SQL_GUARDAR_CABECERA_SOLICITUD_EJECUCION";
            Statement stmt = null;

            JsonObject json_res = new JsonObject();
            try {
                JsonArray detalle_json = (JsonArray) (new JsonParser()).parse(detalle);
                con = this.conectarJNDI(query);
                query = this.obtenerSQL(query);
                con.setAutoCommit(false);

                ps = con.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
                ps.setString(1, usuario);
                ps.setString(2, fecha_esperada_entrega);
                ps.setString(3, observaciones);
                ps.setString(4, usuario);
                ps.setString(5, usuario);
                ps.setString(6, id_bodega_proyecto);

                System.out.print(ps);

                ps.executeUpdate();
                stmt = con.createStatement();
                rs = ps.getGeneratedKeys();
                if (rs.next()) {

                    String id_cabecera = String.valueOf(rs.getInt(1));
                    for (int i = 0; i < detalle_json.size(); i++) {

                        String codigo_material = detalle_json.get(i).getAsJsonObject().get("codigo_material").getAsString();
                        String descripcion_insumo = detalle_json.get(i).getAsJsonObject().get("descripcion").getAsString();
                        String solicitar = detalle_json.get(i).getAsJsonObject().get("solicitar").getAsString();
                        String referencia = detalle_json.get(i).getAsJsonObject().get("referencia").getAsString();
                        String observacion = detalle_json.get(i).getAsJsonObject().get("observacion").getAsString();
                        String unidad_de_medida = detalle_json.get(i).getAsJsonObject().get("unidad_de_medida").getAsString();

                        stmt.addBatch(GuardarDetalleSolicitudEjecucion(id_cabecera, codigo_material, descripcion_insumo, solicitar, referencia, observacion, unidad_de_medida, usuario));                  
                    }
                }
                stmt.executeBatch();
                stmt.clearBatch();
                con.commit();

                json_res.addProperty("status", "200");

            } catch (Exception e) {
                json_res.addProperty("error", e.getMessage());
                try {
                    con.rollback();
                } catch (SQLException ex) {
                    Logger.getLogger(ComprasProcesoImpl.class.getName()).log(Level.SEVERE, null, ex);
                }
            } finally {
                try {
                    if (ps != null) {
                        try {
                            ps.close();
                        } catch (SQLException e) {
                            throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                        }
                    }
                    if (con != null) {
                        try {
                            con.setAutoCommit(true);
                            this.desconectar(con);
                        } catch (SQLException e) {
                            throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage());
                        }
                    }
                } catch (Exception ex) {
                }
                return json_res.toString();
            }

    }    

    public String convertirSolicitudEnMovimiento(String usuario, String id_solicitud_ejecucion) {                                                   
            rs = null;
            ps = null;
            String query = "SQL_SOLICITUD_EJECUCION_A_MOVIMIENTO";
            Statement stmt = null;
            JsonObject json_res = new JsonObject();
            try {
                con = this.conectarJNDI(query);
                query = this.obtenerSQL(query);
                con.setAutoCommit(false);
                ps = con.prepareStatement(query);
                ps.setString(1, id_solicitud_ejecucion);
                ps.setString(2, usuario);

                System.out.print(ps);
                rs = ps.executeQuery();
                //ps.executeUpdate();
                con.commit();

                json_res.addProperty("status", "200");

            } catch (Exception e) {
                json_res.addProperty("error", e.getMessage());
                try {
                    con.rollback();
                } catch (SQLException ex) {
                    Logger.getLogger(ComprasProcesoImpl.class.getName()).log(Level.SEVERE, null, ex);
                }
            } finally {
                try {
                    if (ps != null) {
                        try {
                            ps.close();
                        } catch (SQLException e) {
                            throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                        }
                    }
                    if (con != null) {
                        try {
                            con.setAutoCommit(true);
                            this.desconectar(con);
                        } catch (SQLException e) {
                            throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage());
                        }
                    }
                } catch (Exception ex) {
                }
                return json_res.toString();
            }
    }

    public String rechazarSolicitud(String usuario, String id_solicitud_ejecucion) {                                                   
            rs = null;
            ps = null;
            String query = "SQL_RECHAZAR_SOLICITUD";
            Statement stmt = null;
            JsonObject json_res = new JsonObject();
            try {
                con = this.conectarJNDI(query);
                query = this.obtenerSQL(query);
                con.setAutoCommit(false);
                ps = con.prepareStatement(query);
                ps.setString(1, usuario);
                ps.setString(2, id_solicitud_ejecucion);            

                System.out.print(ps);
                ps.executeUpdate();
                con.commit();

                json_res.addProperty("status", "200");

            } catch (Exception e) {
                json_res.addProperty("error", e.getMessage());
                try {
                    con.rollback();
                } catch (SQLException ex) {
                    Logger.getLogger(ComprasProcesoImpl.class.getName()).log(Level.SEVERE, null, ex);
                }
            } finally {
                try {
                    if (ps != null) {
                        try {
                            ps.close();
                        } catch (SQLException e) {
                            throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                        }
                    }
                    if (con != null) {
                        try {
                            con.setAutoCommit(true);
                            this.desconectar(con);
                        } catch (SQLException e) {
                            throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage());
                        }
                    }
                } catch (Exception ex) {
                }
                return json_res.toString();
            }
    } 

    public String borrarSolicitud(String usuario, String id_solicitud_ejecucion) {                                                   
            rs = null;
            ps = null;
            String query = "SQL_BORRAR_SOLICITUD";
            Statement stmt = null;
            JsonObject json_res = new JsonObject();
            try {
                con = this.conectarJNDI(query);
                query = this.obtenerSQL(query);
                con.setAutoCommit(false);
                ps = con.prepareStatement(query);
                ps.setString(1, usuario);
                ps.setString(2, id_solicitud_ejecucion);            

                System.out.print(ps);
                ps.executeUpdate();
                con.commit();

                json_res.addProperty("status", "200");

            } catch (Exception e) {
                json_res.addProperty("error", e.getMessage());
                try {
                    con.rollback();
                } catch (SQLException ex) {
                    Logger.getLogger(ComprasProcesoImpl.class.getName()).log(Level.SEVERE, null, ex);
                }
            } finally {
                try {
                    if (ps != null) {
                        try {
                            ps.close();
                        } catch (SQLException e) {
                            throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                        }
                    }
                    if (con != null) {
                        try {
                            con.setAutoCommit(true);
                            this.desconectar(con);
                        } catch (SQLException e) {
                            throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage());
                        }
                    }
                } catch (Exception ex) {
                }
                return json_res.toString();
            }
    } 

      
    static class FooterPDF extends PdfPageEventHelper {

        private Image img;

        public FooterPDF(ResourceBundle rb) throws IOException {
            try {
                img = Image.getInstance(rb.getString("ruta") + "/images/pie_paginaSelectrik_.jpg");
                img.setAlignment(PdfPCell.ALIGN_CENTER);
                img.setAbsolutePosition(90f, 0f);
                img.scalePercent(20);
            } catch (BadElementException | IOException r) {
                System.err.println("Error al leer la imagen");
            }
        }

        @Override
        public void onStartPage(PdfWriter writer, Document document) {
            try {
                document.add(img);
            } catch (DocumentException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
        }
    }

    static class encabezados extends PdfPageEventHelper {

        PdfPTable table_3;

        public encabezados(ResourceBundle rb, Font f3b, int cant, int item) throws IOException, DocumentException {
            try {
                if (cant != item && cant != 0) {
                    table_3 = new PdfPTable(5);
                    table_3.setWidths(new int[]{3, 5, 2, 3, 3});
                    table_3.setWidthPercentage(100);
                    PdfPCell cell_3;

                    cell_3 = new PdfPCell(new Phrase("REFERENCIA", f3b));
                    cell_3.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell_3.setBackgroundColor(new Color(93, 93, 93));
                    table_3.addCell(cell_3);

                    cell_3 = new PdfPCell(new Phrase("DESCRIPCION", f3b));
                    cell_3.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell_3.setBackgroundColor(new Color(93, 93, 93));
                    table_3.addCell(cell_3);

                    cell_3 = new PdfPCell(new Phrase("CANT", f3b));
                    cell_3.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell_3.setBackgroundColor(new Color(93, 93, 93));
                    table_3.addCell(cell_3);

                    cell_3 = new PdfPCell(new Phrase("P/U", f3b));
                    cell_3.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell_3.setBackgroundColor(new Color(93, 93, 93));
                    table_3.addCell(cell_3);

                    cell_3 = new PdfPCell(new Phrase("TOTAL", f3b));
                    cell_3.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell_3.setBackgroundColor(new Color(93, 93, 93));
                    table_3.addCell(cell_3);
                }
            } catch (BadElementException r) {
                System.err.println("Error al leer la imagen");
            }
        }

        @Override
        public void onStartPage(PdfWriter writer, Document document) {
            try {
                document.add(table_3);
            } catch (DocumentException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
        }
    }

    private String directorioArchivo(String user, String cons, String extension) throws Exception {
        String ruta = "";
        try {
            ResourceBundle rsb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            ruta = rsb.getString("ruta") + "/exportar/migracion/" + user;
            SimpleDateFormat fmt = new SimpleDateFormat("yyyMMdd_hhmmss");
            File archivo = new File(ruta);
            if (!archivo.exists()) {
                archivo.mkdirs();
            }
            ruta = ruta + "/" + "Movimiento_" + cons + "_" + fmt.format(new Date()) + "." + extension;

        } catch (Exception e) {
            throw new Exception("Error al generar el directorio: " + e.toString());
        }
        return ruta;
    }

    private Document createDoc() {
        Document doc = new Document(PageSize.A4, 35, 35, 35, 70);
        return doc;
    }

    private String mesToString(int mes) {
        String texto = "";
        switch (mes) {
            case 1:
                texto = "Enero";
                break;
            case 2:
                texto = "Febrero";
                break;
            case 3:
                texto = "Marzo";
                break;
            case 4:
                texto = "Abril";
                break;
            case 5:
                texto = "Mayo";
                break;
            case 6:
                texto = "Junio";
                break;
            case 7:
                texto = "Julio";
                break;
            case 8:
                texto = "Agosto";
                break;
            case 9:
                texto = "Septiembre";
                break;
            case 10:
                texto = "Octubre";
                break;
            case 11:
                texto = "Noviembre";
                break;
            case 12:
                texto = "Diciembre";
                break;
            default:
                texto = "Enero";
                break;
        }
        return texto;
    }

    @Override
    public String InfoPreSolicitud(String ReqSolicitud) {
        String respuesta = "";
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_DATOS_PRESOLICITUD";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";

        JsonObject objeto = new JsonObject();

        try {

            con = this.conectarJNDI();
            String consulta = "";

            consulta = this.obtenerSQL(query);
            ps = con.prepareStatement(consulta);

            ps.setString(1, ReqSolicitud);

            System.out.println(ps);

            rs = ps.executeQuery();
            lista = new JsonArray();

            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);

        } catch (Exception ex) {
            respuesta = "{\"respuesta\":\"ERROR\"}";
            ex.getMessage();
            ex.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ex) {
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException ex) {
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException ex) {
                }
            }
            return informacion;
        }
    }

    @Override
    public String EliminarPreOCS(String Usuario) {
        String respuesta = "";
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_ELIMINAR_PREOCS";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";

        JsonObject objeto = new JsonObject();

        try {

            con = this.conectarJNDI();
            String consulta = "";

            consulta = this.obtenerSQL(query);
            ps = con.prepareStatement(consulta);

            ps.setString(1, Usuario);

            System.out.println(ps);

            rs = ps.executeQuery();
            lista = new JsonArray();

            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);

        } catch (Exception ex) {
            respuesta = "{\"respuesta\":\"ERROR\"}";
            ex.getMessage();
            ex.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ex) {
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException ex) {
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException ex) {
                }
            }
            return informacion;
        }
    }

    @Override
    public String GuardarRechazarSolicitud(String Usuario, String SolAprobar, String OpcionAccion, String DescripcionAprobacion) {
        String respuesta = "";
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_APROBAR_SOLICITUD";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";

        JsonObject objeto = new JsonObject();

        try {

            con = this.conectarJNDI();
            String consulta = "";

            consulta = this.obtenerSQL(query);
            ps = con.prepareStatement(consulta);

            ps.setString(1, Usuario);
            ps.setString(2, SolAprobar);
            ps.setString(3, OpcionAccion);
            ps.setString(4, DescripcionAprobacion);

            System.out.println(ps);

            rs = ps.executeQuery();
            lista = new JsonArray();

            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);

        } catch (Exception ex) {
            respuesta = "{\"respuesta\":\"ERROR\"}";
            ex.getMessage();
            ex.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ex) {
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException ex) {
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException ex) {
                }
            }
            return informacion;
        }
    }

    @Override
    public String ActualizarSolicitudes(String uSers, String oSC) {
        String respuesta = "";
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_UPDATE_SOLICITUD";

        JsonObject objeto = new JsonObject();

        try {

            con = this.conectarJNDI();

            ps = con.prepareStatement(this.obtenerSQL(query));

            ps.setString(1, uSers);
            ps.setString(2, oSC);
            ps.setString(3, oSC);

            System.out.println(ps);

            ps.executeUpdate();

            respuesta = "{\"respuesta\":\"Guardado\"}";

        } catch (Exception ex) {
            respuesta = "{\"respuesta\":\"ERROR\"}";
            ex.getMessage();
            ex.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ex) {
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException ex) {
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException ex) {
                }
            }
            return respuesta;
        }
    }

    @Override
    public String DevolverSolicitud(String CodSolicitud) {

        String respuesta = "";
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_DEVOLVER_SOLICITUD";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "";

        JsonObject objeto = new JsonObject();

        try {

            con = this.conectarJNDI();
            String consulta = "";

            consulta = this.obtenerSQL(query);
            ps = con.prepareStatement(consulta);

            ps.setString(1, CodSolicitud);

            System.out.println(ps);

            rs = ps.executeQuery();
            lista = new JsonArray();

            while (rs.next()) {
                informacion = rs.getString("respta");
//                objetoJson = new JsonObject();
//                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
//                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
//                }
//                lista.add(objetoJson);
            }

           // informacion = new Gson().toJson(lista);    
        } catch (Exception ex) {
            respuesta = "{\"respuesta\":\"ERROR\"}";
            ex.getMessage();
            ex.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ex) {
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException ex) {
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException ex) {
                }
            }

            return informacion;
        }

    }

    @Override
    public String EliminarOCS(String codOCS, String Usuario) {

        String respuesta = "";
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_ELIMINAR_OCS";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";

        JsonObject objeto = new JsonObject();

        try {

            con = this.conectarJNDI();
            String consulta = "";

            consulta = this.obtenerSQL(query);
            ps = con.prepareStatement(consulta);

            ps.setString(1, codOCS);
            ps.setString(2, Usuario);

            System.out.println(ps);

            rs = ps.executeQuery();
            lista = new JsonArray();

            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);

        } catch (Exception ex) {
            respuesta = "{\"respuesta\":\"ERROR\"}";
            ex.getMessage();
            ex.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ex) {
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException ex) {
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException ex) {
                }
            }

            return informacion;
        }
    }

    @Override
    public JsonObject getInfoCuentaEnvioCorreo() {
        Connection con = null;
        PreparedStatement ps = null;
        con = null;
        ps = null;
        ResultSet rs = null;
        String query = "GET_INFO_CORREO_REMITENTE";
        JsonObject obj = null;

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            rs = ps.executeQuery();
            JsonObject fila = null;
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("servidor", rs.getString("servidor"));
                fila.addProperty("puerto", rs.getString("puerto"));
                fila.addProperty("usuario", rs.getString("usuario"));
                fila.addProperty("clave", rs.getString("clave"));
                fila.addProperty("bodyMessage", rs.getString("bodyMessage"));
            }
            obj = fila;

        } catch (Exception e) {
            System.out.println("Error en getInfoCuentaEnvioCorreo: " + e.toString());
            e.printStackTrace();
            throw new Exception("Error en getInfoCuentaEnvioCorreo: " + e.toString());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return obj;
        }
    }

    @Override
    public String getInfoCuentaEnvioCorreoDestino(String CodSolicitud) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_EMAIL_DESTINO";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, CodSolicitud);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                informacion = rs.getString("email");
            }

        } catch (Exception e) {
            e.printStackTrace();
            informacion = "ERROR";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return informacion;
        }
    }

    @Override
    public String imprimirOCS_Provintegral(String orden_compra, Usuario usuario) {
        String msg = "OK";
        try {
            String ruta = "";
            String observaciones = null, centroCosto = null, nombre_proyecto = null, contratista = null, multiservisio = null, fechaOcs = null;
            ps = null;
            rs = null;
            con = this.conectarJNDI();
            ruta = this.directorioArchivo(usuario.getLogin(), orden_compra, "pdf");
            ResourceBundle rsb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");

            DecimalFormat formato = new DecimalFormat("$###,###,###");
            DecimalFormat formato2 = new DecimalFormat("###.##");
            Font fuente = new Font(Font.HELVETICA, 10, Font.NORMAL, new java.awt.Color(0, 0, 0));
            Font f3b = new Font(Font.TIMES_ROMAN, 10, Font.HELVETICA, new Color(255, 255, 255));
            Font f4normalN = new Font(Font.HELVETICA, 8, Font.BOLD, new java.awt.Color(102, 101, 101));
            Font f4normal = new Font(Font.HELVETICA, 8, Font.NORMAL, new java.awt.Color(102, 101, 101));
            Font encabezado = new Font(Font.HELVETICA, 16, Font.BOLD, new java.awt.Color(247, 147, 29));
            Font suben = new Font(Font.HELVETICA, 10, Font.BOLD, new java.awt.Color(247, 147, 29));
            Font f5 = new Font(Font.HELVETICA, 8, Font.BOLD, new java.awt.Color(238, 129, 38));

            Document documento = null;
            documento = this.createDoc();
            PdfWriter pdfWriter = PdfWriter.getInstance(documento, new FileOutputStream(ruta));
//            FooterPDF footer = new FooterPDF(rsb);
//            pdfWriter.setPageEvent(footer);
            documento.open();
            //documento.newPage();            
            //documento.setMargins(0, 70, 0, 70);
            int cant = 0;
            int item = 1;
            encabezados encabezado_ = new encabezados(rsb, f3b, cant, item);
            pdfWriter.setPageEvent(encabezado_);
            Image imagen = Image.getInstance(rsb.getString("ruta") + "/images/logo_provintegral1.png");
            imagen.scaleToFit(110, 110);

            String queryP = "SQL_INFORMACION_PROYECTO_PROVINTEGRAL";
            ps = con.prepareStatement(this.obtenerSQL(queryP));
            ps.setString(1, orden_compra);
            rs = ps.executeQuery();
            while (rs.next()) {
                observaciones = rs.getString("observaciones");
                centroCosto = rs.getString("centro_costos_gastos");
                nombre_proyecto = rs.getString("nombre_proyecto");
                multiservisio = rs.getString("ms");
                fechaOcs = rs.getString("fecha_actual");
                contratista = rs.getString("contratista");
            }

            //tabla de fecha
            PdfPTable table_ = new PdfPTable(4);
            table_.setWidths(new int[]{2, 2, 2, 2});
            table_.setWidthPercentage(7);
            PdfPCell cell_;
            Calendar calendario = Calendar.getInstance();
            String dias = String.valueOf(calendario.get(Calendar.DAY_OF_MONTH));
            String mes = this.mesToString(calendario.get(Calendar.MONTH) + 1);
            String anio = String.valueOf(calendario.get(Calendar.YEAR));
            cell_ = new PdfPCell(new Phrase("FECHA", f5));
            cell_.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell_.setBorder(0);
            table_.addCell(cell_);

            cell_ = new PdfPCell(new Phrase(fechaOcs == null ? "" : fechaOcs.substring(8, 10), f4normal));
            cell_.setHorizontalAlignment(Element.ALIGN_CENTER);
            table_.addCell(cell_);

            cell_ = new PdfPCell(new Phrase(fechaOcs == null ? "" : fechaOcs.substring(5, 7), f4normal));
            cell_.setHorizontalAlignment(Element.ALIGN_CENTER);
            table_.addCell(cell_);

            cell_ = new PdfPCell(new Phrase(fechaOcs == null ? "" : fechaOcs.substring(0, 4), f4normal));
            cell_.setHorizontalAlignment(Element.ALIGN_CENTER);
            table_.addCell(cell_);
            //documento.add(table_);

            //tabla encabezado logo y titulo
            PdfPTable table_0 = new PdfPTable(2);
            table_0.setWidths(new int[]{5, 5});
            table_0.setWidthPercentage(100);
            PdfPCell cell_0;
            cell_0 = new PdfPCell(imagen);
            cell_0.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell_0.setBorder(0);
            table_0.addCell(cell_0);

            cell_0 = new PdfPCell(new Phrase("ORDEN DE COMPRA " + orden_compra, encabezado));
            cell_0.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell_0.setVerticalAlignment(Element.ALIGN_CENTER);
            cell_0.setBorderWidthRight(0f);
            cell_0.setBorderWidthBottom(0f);
            cell_0.setBorderWidthTop(0f);
            cell_0.setBorderWidthLeft(0f);
            cell_0.setPaddingTop(50);
            table_0.addCell(cell_0);

            cell_0 = new PdfPCell(new Phrase("CENTRO DE COSTO: " + centroCosto == null ? "" : centroCosto, suben));
            cell_0.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell_0.setVerticalAlignment(Element.ALIGN_RIGHT);
            cell_0.setColspan(2);
            cell_0.setPaddingRight(22);
            cell_0.setPaddingTop(-30);
            cell_0.setBorder(0);
            table_0.addCell(cell_0);

            cell_0 = new PdfPCell(new Phrase(" ", suben));
            cell_0.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell_0.setBorder(0);
            table_0.addCell(cell_0);

            //A�ADE LA TABLA DE FECHA
            cell_0 = new PdfPCell(table_);
            cell_0.setBorderWidthRight(0f);
            cell_0.setBorderWidthBottom(0f);
            cell_0.setBorderWidthTop(0f);
            cell_0.setBorderWidthLeft(0f);
            cell_0.setPaddingLeft(45);
            table_0.addCell(cell_0);
            documento.add(table_0);

            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(new Paragraph(new Phrase(" ", fuente)));

            String query = "SQL_ORDEN_COMPRA";
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, orden_compra);
            rs = ps.executeQuery();
            while (rs.next()) {
                //primer cuadro            
                PdfPTable table_1 = new PdfPTable(4);
                table_1.setWidths(new int[]{3, 3, 3, 2});
                table_1.setWidthPercentage(100);
                PdfPCell cell_1;

                cell_1 = new PdfPCell(new Phrase("PROVEEDOR", f3b));
                cell_1.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell_1.setBackgroundColor(new Color(93, 93, 93));
                cell_1.setBorderWidthRight(0);
                cell_1.setColspan(2);
                table_1.addCell(cell_1);

                cell_1 = new PdfPCell(new Phrase("DATOS ENVIO", f3b));
                cell_1.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell_1.setBackgroundColor(new Color(93, 93, 93));
                cell_1.setColspan(2);
                table_1.addCell(cell_1);

                cell_1 = new PdfPCell(new Phrase("NOMBRE DE EMPRESA: ", f4normalN));
                cell_1.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell_1.setBorderWidthRight(0f);
                cell_1.setBorderWidthBottom(0f);
                cell_1.setBorderWidthTop(0f);
                table_1.addCell(cell_1);

                cell_1 = new PdfPCell(new Phrase(rs.getString("nombre_proveedor"), f4normal));
                cell_1.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell_1.setBorderWidthRight(0f);
                cell_1.setBorderWidthBottom(0f);
                cell_1.setBorderWidthTop(0f);
                cell_1.setBorderWidthLeft(0f);
                table_1.addCell(cell_1);

                cell_1 = new PdfPCell(new Phrase("RESPONSALE: ", f4normalN));
                cell_1.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell_1.setBorderWidthRight(0f);
                cell_1.setBorderWidthBottom(0f);
                cell_1.setBorderWidthTop(0f);
                cell_1.setBorderWidthLeft(0f);
                table_1.addCell(cell_1);

                cell_1 = new PdfPCell(new Phrase(rs.getString("responsable_envio"), f4normal));
                cell_1.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell_1.setBorderWidthBottom(0f);
                cell_1.setBorderWidthTop(0f);
                cell_1.setBorderWidthLeft(0f);
                table_1.addCell(cell_1);

                cell_1 = new PdfPCell(new Phrase("CONTACTO O DEPARTAMENTO: ", f4normalN));
                cell_1.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell_1.setBorderWidthRight(0f);
                cell_1.setBorderWidthBottom(0f);
                cell_1.setBorderWidthTop(0f);
                table_1.addCell(cell_1);

                cell_1 = new PdfPCell(new Phrase("COMPRAS ", f4normal));
                cell_1.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell_1.setBorderWidthRight(0f);
                cell_1.setBorderWidthBottom(0f);
                cell_1.setBorderWidthTop(0f);
                cell_1.setBorderWidthLeft(0f);
                table_1.addCell(cell_1);

                cell_1 = new PdfPCell(new Phrase("DIRECCION BODEGA O PROYECTO: ", f4normalN));
                cell_1.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell_1.setBorderWidthRight(0f);
                cell_1.setBorderWidthBottom(0f);
                cell_1.setBorderWidthTop(0f);
                cell_1.setBorderWidthLeft(0f);
                table_1.addCell(cell_1);

                cell_1 = new PdfPCell(new Phrase(rs.getString("direccion_entrega"), f4normal));
                cell_1.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell_1.setBorderWidthBottom(0f);
                cell_1.setBorderWidthTop(0f);
                cell_1.setBorderWidthLeft(0f);
                table_1.addCell(cell_1);

                cell_1 = new PdfPCell(new Phrase("DIRECCION: ", f4normalN));
                cell_1.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell_1.setBorderWidthRight(0f);
                cell_1.setBorderWidthBottom(0f);
                cell_1.setBorderWidthTop(0f);
                table_1.addCell(cell_1);

                cell_1 = new PdfPCell(new Phrase(rs.getString("direccion_proveedor"), f4normal));
                cell_1.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell_1.setBorderWidthRight(0f);
                cell_1.setBorderWidthBottom(0f);
                cell_1.setBorderWidthTop(0f);
                cell_1.setBorderWidthLeft(0f);
                table_1.addCell(cell_1);

                cell_1 = new PdfPCell(new Phrase("CIUDAD: ", f4normalN));
                cell_1.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell_1.setBorderWidthBottom(0f);
                cell_1.setBorderWidthTop(0f);
                cell_1.setBorderWidthLeft(0f);
                cell_1.setBorderWidthRight(0f);
                table_1.addCell(cell_1);

                //cell_1 = new PdfPCell(new Phrase(rs.getString("ciudad_envio"), f4normal));
                cell_1 = new PdfPCell(new Phrase("", f4normal));
                cell_1.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell_1.setBorderWidthBottom(0f);
                cell_1.setBorderWidthTop(0f);
                cell_1.setBorderWidthLeft(0f);
                table_1.addCell(cell_1);

                cell_1 = new PdfPCell(new Phrase("CIUDAD: ", f4normalN));
                cell_1.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell_1.setBorderWidthRight(0f);
                cell_1.setBorderWidthBottom(0f);
                cell_1.setBorderWidthTop(0f);
                table_1.addCell(cell_1);

                cell_1 = new PdfPCell(new Phrase(rs.getString("ciudad_proveedor"), f4normal));
                cell_1.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell_1.setBorderWidthBottom(0f);
                cell_1.setBorderWidthTop(0f);
                cell_1.setBorderWidthLeft(0f);
                cell_1.setBorderWidthRight(0f);
                table_1.addCell(cell_1);

                cell_1 = new PdfPCell(new Phrase("TELEFONO: ", f4normalN));
                cell_1.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell_1.setBorderWidthRight(0f);
                cell_1.setBorderWidthBottom(0f);
                cell_1.setBorderWidthTop(0f);
                cell_1.setBorderWidthLeft(0f);
                table_1.addCell(cell_1);

                cell_1 = new PdfPCell(new Phrase(rs.getString("telefono_envio"), f4normal));
                cell_1.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell_1.setBorderWidthLeft(0f);
                cell_1.setBorderWidthBottom(0f);
                cell_1.setBorderWidthTop(0f);
                table_1.addCell(cell_1);

                cell_1 = new PdfPCell(new Phrase("TELEFONO: ", f4normalN));
                cell_1.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell_1.setBorderWidthTop(0f);
                cell_1.setBorderWidthRight(0f);
                table_1.addCell(cell_1);

                cell_1 = new PdfPCell(new Phrase(rs.getString("telefono_proveedor"), f4normal));
                cell_1.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell_1.setColspan(3);
                cell_1.setBorderWidthTop(0f);
                cell_1.setBorderWidthLeft(0f);
                table_1.addCell(cell_1);
                documento.add(table_1);

                documento.add(new Paragraph(new Phrase(" ", fuente)));

            }

            //segundo cuadro
            PdfPTable table_2 = new PdfPTable(3);
            table_2.setWidths(new int[]{3, 4, 3});
            table_2.setWidthPercentage(100);
            PdfPCell cell_2;

            cell_2 = new PdfPCell(new Phrase("MS", f3b));
            cell_2.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell_2.setBackgroundColor(new Color(93, 93, 93));
            table_2.addCell(cell_2);

            cell_2 = new PdfPCell(new Phrase("NOMBRE DEL PROYECTO", f3b));
            cell_2.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell_2.setBackgroundColor(new Color(93, 93, 93));
            table_2.addCell(cell_2);

            cell_2 = new PdfPCell(new Phrase("CONTRATISTA", f3b));
            cell_2.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell_2.setBackgroundColor(new Color(93, 93, 93));
            table_2.addCell(cell_2);

            cell_2 = new PdfPCell(new Phrase(multiservisio == null ? "" : multiservisio, f4normal));
            cell_2.setHorizontalAlignment(Element.ALIGN_CENTER);
            table_2.addCell(cell_2);

            cell_2 = new PdfPCell(new Phrase(nombre_proyecto == null ? "" : nombre_proyecto, f4normal));
            cell_2.setHorizontalAlignment(Element.ALIGN_CENTER);
            table_2.addCell(cell_2);

            cell_2 = new PdfPCell(new Phrase(contratista == null ? "" : contratista, f4normal));
            cell_2.setHorizontalAlignment(Element.ALIGN_CENTER);
            table_2.addCell(cell_2);
            documento.add(table_2);

            documento.add(new Paragraph(new Phrase(" ", fuente)));

            //tercera tabla
            PdfPTable table_3 = new PdfPTable(5);
            table_3.setWidths(new int[]{3, 5, 2, 3, 3});
            table_3.setWidthPercentage(100);
            PdfPCell cell_3;

            cell_3 = new PdfPCell(new Phrase("REFERENCIA", f3b));
            cell_3.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell_3.setBackgroundColor(new Color(93, 93, 93));
            table_3.addCell(cell_3);

            cell_3 = new PdfPCell(new Phrase("DESCRIPCION", f3b));
            cell_3.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell_3.setBackgroundColor(new Color(93, 93, 93));
            table_3.addCell(cell_3);

            cell_3 = new PdfPCell(new Phrase("CANT", f3b));
            cell_3.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell_3.setBackgroundColor(new Color(93, 93, 93));
            table_3.addCell(cell_3);

            cell_3 = new PdfPCell(new Phrase("P/U", f3b));
            cell_3.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell_3.setBackgroundColor(new Color(93, 93, 93));
            table_3.addCell(cell_3);

            cell_3 = new PdfPCell(new Phrase("TOTAL", f3b));
            cell_3.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell_3.setBackgroundColor(new Color(93, 93, 93));
            table_3.addCell(cell_3);

            double valorTotal = 0;
            String query2 = "SQL_ORDEN_COMPRA_DETALLE";
            ps = con.prepareStatement(this.obtenerSQL(query2));
            ps.setString(1, orden_compra);
            rs = ps.executeQuery();
            while (rs.next()) {
                cant = rs.getInt("cant");
                valorTotal = valorTotal + rs.getInt("costo_total_compra");

                // if (item == cant) {
                cell_3 = new PdfPCell(new Phrase(rs.getString("referencia"), f4normal));
                cell_3.setHorizontalAlignment(Element.ALIGN_CENTER);
//                    cell_3.setBorderWidthTop(0f);
                table_3.addCell(cell_3);

                cell_3 = new PdfPCell(new Phrase(rs.getString("descripcion_insumo"), f4normal));
                cell_3.setHorizontalAlignment(Element.ALIGN_CENTER);
//                    cell_3.setBorderWidthTop(0f);
                table_3.addCell(cell_3);

                cell_3 = new PdfPCell(new Phrase(rs.getString("cantidad_solicitada"), f4normal));
                cell_3.setHorizontalAlignment(Element.ALIGN_CENTER);
//                    cell_3.setBorderWidthTop(0f);
                table_3.addCell(cell_3);

                cell_3 = new PdfPCell(new Phrase(formato.format(rs.getInt("costo_unitario_compra")), f4normal));
                cell_3.setHorizontalAlignment(Element.ALIGN_CENTER);
//                    cell_3.setBorderWidthTop(0f);
                table_3.addCell(cell_3);

                cell_3 = new PdfPCell(new Phrase(formato.format(rs.getInt("costo_total_compra")), f4normal));
                cell_3.setHorizontalAlignment(Element.ALIGN_CENTER);
//                    cell_3.setBorderWidthTop(0f);
                table_3.addCell(cell_3);
               // } 
//                else {
//                    cell_3 = new PdfPCell(new Phrase(rs.getString("referencia"), f4normal));
//                    cell_3.setHorizontalAlignment(Element.ALIGN_CENTER);
//                    cell_3.setBorderWidthBottom(0f);
//                    cell_3.setBorderWidthTop(0f);
//                    table_3.addCell(cell_3);
//
//                    cell_3 = new PdfPCell(new Phrase(rs.getString("descripcion_insumo"), f4normal));
//                    cell_3.setHorizontalAlignment(Element.ALIGN_CENTER);
//                    cell_3.setBorderWidthBottom(0f);
//                    cell_3.setBorderWidthTop(0f);
//                    table_3.addCell(cell_3);
//
//                    cell_3 = new PdfPCell(new Phrase(rs.getString("cantidad_solicitada"), f4normal));
//                    cell_3.setHorizontalAlignment(Element.ALIGN_CENTER);
//                    cell_3.setBorderWidthBottom(0f);
//                    cell_3.setBorderWidthTop(0f);
//                    table_3.addCell(cell_3);
//
//                    cell_3 = new PdfPCell(new Phrase(formato.format(rs.getInt("costo_unitario_compra")), f4normal));
//                    cell_3.setHorizontalAlignment(Element.ALIGN_CENTER);
//                    cell_3.setBorderWidthBottom(0f);
//                    cell_3.setBorderWidthTop(0f);
//                    table_3.addCell(cell_3);
//
//                    cell_3 = new PdfPCell(new Phrase(formato.format(rs.getInt("costo_total_compra")), f4normal));
//                    cell_3.setHorizontalAlignment(Element.ALIGN_CENTER);
//                    cell_3.setBorderWidthTop(0f);
//                    cell_3.setBorderWidthBottom(0f);
//                    table_3.addCell(cell_3);
                // }

                item = item + 1;
            }
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (con != null) {
                this.desconectar(con);
            }
            documento.add(table_3);

            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(new Paragraph(new Phrase(" ", fuente)));

            PdfPTable table_7 = new PdfPTable(1);
            table_7.setWidths(new int[]{5});
            table_7.setWidthPercentage(100);
            PdfPCell cell_7;

            cell_7 = new PdfPCell(new Phrase("OBSERVACIONES", f3b));
            cell_7.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell_7.setBackgroundColor(new Color(93, 93, 93));
            cell_7.setColspan(5);
            table_7.addCell(cell_7);

            cell_7 = new PdfPCell(new Phrase(observaciones == null ? "" : observaciones, f4normal));
            cell_7.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell_7.setColspan(5);
            cell_7.setBorderWidthBottom(0f);
            table_7.addCell(cell_7);

            cell_7 = new PdfPCell(new Phrase(" ", f4normal));
            cell_7.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell_7.setColspan(5);
            cell_7.setBorderWidthTop(0f);
            table_7.addCell(cell_7);

            documento.add(table_7);

            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(new Paragraph(new Phrase(" ", fuente)));

            PdfPTable table_5 = new PdfPTable(1);
            table_5.setWidths(new int[]{3});
            table_5.setWidthPercentage(100);
            PdfPCell cell_5;

            cell_5 = new PdfPCell(new Phrase("", f4normal));
            cell_5.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell_5.setBorderWidthBottom(0f);
            cell_5.setBorderWidthTop(0f);
            cell_5.setFixedHeight(10);
            table_5.addCell(cell_5);

            cell_5 = new PdfPCell(new Phrase(" ", f4normal));
            cell_5.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell_5.setBorderWidthBottom(0f);
            cell_5.setBorderWidthTop(0f);
            cell_5.setFixedHeight(10);
            table_5.addCell(cell_5);

            cell_5 = new PdfPCell(new Phrase("", f4normal));
            cell_5.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell_5.setBorderWidthBottom(0f);
            cell_5.setBorderWidthTop(0f);
            cell_5.setFixedHeight(10);
            table_5.addCell(cell_5);

            cell_5 = new PdfPCell(new Phrase("", f4normal));
            cell_5.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell_5.setBorderWidthBottom(0f);
            cell_5.setBorderWidthTop(0f);
            cell_5.setFixedHeight(10);
            table_5.addCell(cell_5);

            cell_5 = new PdfPCell(new Phrase(formato.format(valorTotal), f4normal));
            cell_5.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell_5.setBorderWidthBottom(0f);
            cell_5.setBorderWidthTop(0f);
            cell_5.setFixedHeight(10);
            table_5.addCell(cell_5);
            //documento.add(table_5);

            PdfPTable table_6 = new PdfPTable(1);
            table_6.setWidths(new int[]{3});
            table_6.setWidthPercentage(100);
            PdfPCell cell_6;

            cell_6 = new PdfPCell(new Phrase(formato.format(valorTotal), f4normal));
            cell_6.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell_6.setBorderWidthBottom(0f);
            cell_6.setBorderWidthTop(0f);
            table_6.addCell(cell_6);

            //cuarto cuadro
            PdfPTable table_4 = new PdfPTable(4);
            table_4.setWidths(new int[]{7, 1, 2, 3});
            table_4.setWidthPercentage(100);
            PdfPCell cell_4;

            cell_4 = new PdfPCell(new Phrase("COMETARIOS O INTRUCCIONES ESPECIALES", f3b));
            cell_4.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell_4.setBackgroundColor(new Color(93, 93, 93));
            table_4.addCell(cell_4);

            cell_4 = new PdfPCell(new Phrase("", f3b));
            cell_4.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell_4.setBorderWidthBottom(0f);
            cell_4.setBorderWidthTop(0f);
            table_4.addCell(cell_4);

            cell_4 = new PdfPCell(new Phrase("SUBTOTAL", f3b));
            cell_4.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell_4.setBackgroundColor(new Color(93, 93, 93));
            cell_4.setBorderWidthBottom(0f);
            table_4.addCell(cell_4);

            cell_4 = new PdfPCell(table_6);
            cell_4.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell_4.setBorderWidthBottom(0f);
            table_4.addCell(cell_4);

            cell_4 = new PdfPCell(new Phrase("Para el tramite de su factura se debe tener en cuenta los siguientes requisitos:\n"
                    + "Anexar Orden de compra y remisi�n debidamente firmada La Factura Original debe ser entregada en la Cra. 55 N� 100-51 piso 5 Centro Empresarial Blue Gardens "
                    + "Barranquilla, solo aqu� se aceptan radicaci�n\n"
                    + "de facturas.", f4normal));
            cell_4.setHorizontalAlignment(Element.ALIGN_LEFT);
            table_4.addCell(cell_4);

            cell_4 = new PdfPCell(new Phrase("", f3b));
            cell_4.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell_4.setBorderWidthBottom(0f);
            cell_4.setBorderWidthTop(0f);
            table_4.addCell(cell_4);

            cell_4 = new PdfPCell(new Phrase("IMPUESTO  FLETE  RETEFUENTE  RETE-ICA  TOTAL", f3b));
            cell_4.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell_4.setBackgroundColor(new Color(93, 93, 93));
            cell_4.setBorderWidthTop(0f);
            table_4.addCell(cell_4);

            cell_4 = new PdfPCell(table_5);
            cell_4.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell_4.setBorderWidthTop(0f);
            table_4.addCell(cell_4);
            documento.add(table_4);

            documento.close();
        } catch (Exception e) {
            msg = "ERROR";
            e.printStackTrace();
        }
        return msg;
    }

    @Override
    public String InventarioTodos(String usuario, String ano, String mes, String tipomovimiento) throws SQLException {

        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_MOVIMIENTOS_INVENTARIO";
        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            String consulta = "";
            String parametro = "";

            consulta = this.obtenerSQL(query);

            if (!mes.equals("0")) {
                consulta = consulta.replaceAll("#ANOMES#", " WHERE EXTRACT(YEAR FROM creation_date::date) = '" + ano + "' AND EXTRACT(MONTH FROM creation_date::date) = '" + mes + "'");
            } else {
                consulta = consulta.replaceAll("#ANOMES#", " WHERE EXTRACT(YEAR FROM creation_date::date) = '" + ano + "'");
            }

            if (!tipomovimiento.equals("0")) {
                consulta = consulta.replaceAll("#TIPOSOL#", " and id_tipo_movimiento = " + tipomovimiento);
            } else {
                consulta = consulta.replaceAll("#TIPOSOL#", "");
            }

            ps = con.prepareStatement(consulta);
            
            ps.setString(1, usuario);

            System.out.println(ps);

            rs = ps.executeQuery();

            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = lista.toString();

        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }

            return informacion;

        }

    }

    @Override
    public String InventarioDetalle(String CodMovimiento) throws SQLException {

        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_DETALLE_INVENTARIO";
        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";

        try {
            con = this.conectarJNDI();
            String consulta = "";

            consulta = this.obtenerSQL(query);
            ps = con.prepareStatement(consulta);

            ps.setString(1, CodMovimiento);

            System.out.println(ps);

            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);

        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }

            return informacion;

        }

    }

    @Override
    public String SaveDespacho(String Usuario, String tipo_solicitud, String proveedor, String descripcion, String fecha_entrega, String direccion_entrega, String OrdenCompDespacho) {
        String respuesta = "";
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_GUARDAR_DESPACHO";

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";

        JsonObject objeto = new JsonObject();

        try {

            con = this.conectarJNDI();
            String consulta = "";

            consulta = this.obtenerSQL(query);
            ps = con.prepareStatement(consulta);

            ps.setString(1, Usuario);
            ps.setString(2, tipo_solicitud);
            ps.setString(3, proveedor);
            ps.setString(4, descripcion);
            ps.setString(5, fecha_entrega);
            ps.setString(6, direccion_entrega);
            ps.setString(7, OrdenCompDespacho);

            System.out.println(ps);

            rs = ps.executeQuery();
            lista = new JsonArray();

            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);

        } catch (Exception ex) {
            respuesta = "{\"respuesta\":\"ERROR\"}";
            ex.getMessage();
            ex.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ex) {
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException ex) {
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException ex) {
                }
            }
            return informacion;
        }
    }

    @Override
    public String SaveDetailsDispatch(JsonObject info, String dSPTCH) {
        String respuesta = "";
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_GUARDAR_DESPACHO_DETALLE";

        JsonObject objeto = new JsonObject();

        try {

            JsonArray arr = info.getAsJsonArray("json");
            con = this.conectarJNDI();

            for (int i = 0; i < arr.size(); i++) {

                objeto = arr.get(i).getAsJsonObject();

                ps = con.prepareStatement(this.obtenerSQL(query));

                ps.setString(1, objeto.get("IdDetalleInsumo").getAsString());
                ps.setString(2, objeto.get("CostoDespacho").getAsString()); //CodigoMaterial Usuario IdSolicitud UserResponsable
                ps.setString(3, objeto.get("CantDespacho").getAsString());
                ps.setString(4, objeto.get("TotalDespacho").getAsString());
                ps.setString(5, objeto.get("unidadmedida").getAsString());
                ps.setString(6, dSPTCH);

                System.out.println(ps);

                //ps.executeUpdate();
                rs = ps.executeQuery();
                /*
                 lista = new JsonArray();

                 while (rs.next()) {
                 objetoJson = new JsonObject();
                 for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                 objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                 }
                 lista.add(objetoJson);
                 }

                 informacion = new Gson().toJson(lista);                 
                 */
            }

            respuesta = "{\"respuesta\":\"Guardado\"}";

        } catch (Exception ex) {
            respuesta = "{\"respuesta\":\"ERROR\"}";
            ex.getMessage();
            ex.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ex) {
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException ex) {
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException ex) {
                }
            }
            return respuesta;
        }
    }

    @Override
    public String InventarioDespacho(String dSPTCH) {
        String respuesta = "";
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_GUARDAR_INVENTARIO_DESPACHO";

        JsonObject objeto = new JsonObject();

        try {

            //JsonArray arr = info.getAsJsonArray("json");
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));

            ps.setString(1, dSPTCH);
            
            System.out.println(ps);

            rs = ps.executeQuery();

            respuesta = "{\"respuesta\":\"Guardado\"}";

        } catch (Exception ex) {
            respuesta = "{\"respuesta\":\"ERROR\"}";
            ex.getMessage();
            ex.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ex) {
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException ex) {
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException ex) {
                }
            }
            return respuesta;
        }
    }

    @Override
    public String DespachosTodos(String ano, String mes) throws SQLException {

        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_DESPACHOS";
        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            String consulta = "";
            String parametro = "";

            consulta = this.obtenerSQL(query);

            if (!mes.equals("0")) {
                consulta = consulta.replaceAll("#ANOMES#", " WHERE EXTRACT(YEAR FROM creation_date::date) = '" + ano + "' AND EXTRACT(MONTH FROM creation_date::date) = '" + mes + "'");
            } else {
                consulta = consulta.replaceAll("#ANOMES#", " WHERE EXTRACT(YEAR FROM creation_date::date) = '" + ano + "'");
            }

            ps = con.prepareStatement(consulta);

            System.out.println(ps);

            rs = ps.executeQuery();

            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);

        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }

            return informacion;

        }

    }

    @Override
    public String DespachosDetalles(String CodMovimiento) throws SQLException {

        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_DETALLE_DESPACHO";
        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";

        try {
            con = this.conectarJNDI();
            String consulta = "";

            consulta = this.obtenerSQL(query);
            ps = con.prepareStatement(consulta);

            ps.setString(1, CodMovimiento);

            System.out.println(ps);

            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);

        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }

            return informacion;

        }

    }

    @Override
    public String cargar_Combo(String usuario, String op) {
        String query ="";
        con = null;
        rs = null;
        ps = null;
        switch (op) {
            case "1":
                query = "CARGAR_COMBO_ENTRADA";
                break; 
           case "2":
                query = "CARGAR_COMBO_SALIDA";
                break;
            case "3":
                query = "CARGAR_COMBO_TIPO_MOVIMIENTO";
                break;                
            default:
                throw new AssertionError();
        }

        String respuestaJson = "{}";

        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);
            if (op.equals("3")) ps.setString(1, usuario);;           

            rs = ps.executeQuery();

            while (rs.next()) {
                obj.addProperty(rs.getString("id"), rs.getString("descripcion"));

            }
            respuestaJson = gson.toJson(obj);

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return respuestaJson;
    }
    
    @Override
    public String obternerCotizacionTerc(String id_solitud) {
        String respuestaJson = "{}";
        con = null;
        ps = null;
        rs = null;
        String query = "SQL_CARGAR_COTIZACIONES_TERCERIZADAS";
        try {
            con = conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, id_solitud);
            rs = ps.executeQuery();
            if (rs.next()) {
                respuestaJson = "{\"respuesta\":\"" + rs.getString("resultado") + "\"}";
            }
        } catch (Exception e) {
            
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
        }

        return respuestaJson;
    }
}
