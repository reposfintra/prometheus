/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.opav.model.DAOS.impl;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.lowagie.text.BadElementException;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.html.simpleparser.HTMLWorker;
import com.lowagie.text.pdf.PdfPageEventHelper;
import com.lowagie.text.pdf.PdfWriter;
import com.tsp.opav.model.DAOS.ClientesVerDAO;
import com.tsp.opav.model.DAOS.MainDAO;
import com.tsp.opav.model.DAOS.MinutasContratacionDAO;
import com.tsp.operation.model.TransaccionService;
import com.tsp.operation.model.beans.SerieGeneral;
import com.tsp.operation.model.beans.StringStatement;
import com.tsp.operation.model.beans.Usuario;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringReader;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author user
 */
public class MinutasContratacionImpl extends MainDAO implements MinutasContratacionDAO {

    private Connection con = null;
    private PreparedStatement ps = null;
    private ResultSet rs = null;
    private String query = "";
    private StringStatement st = null;

    public MinutasContratacionImpl(String dataBaseName) {
        super("MinutasContratacionDAO.xml", dataBaseName);
    }

    @Override
    public String cargarGridSolicitudesPendientes(String fecha_ini, String fecha_fin, String num_solicitud, String lineaNegocio, String responsable, String etapaActual, String id_cliente, String nom_proyecto, String foms, String tipo_proyecto) {
        Gson gson = new Gson();
        con = null;
        ps = null;
        rs = null;
        JsonObject obj = new JsonObject();
        String filtro = "";
        filtro += (num_solicitud.equals("")) ? "" : " AND  ofe.id_solicitud = '" + num_solicitud + "'";
       
        if (!fecha_ini.equals("") && !fecha_fin.equals("")) {
            filtro = filtro + "  AND fecha_validacion_cartera::date between '" + fecha_ini + "' and '" + fecha_fin + "' ";
        }
        if (!lineaNegocio.equals("")) {
            filtro = filtro + " and tipo_proyecto =  '" + lineaNegocio + "' ";
        }
        if (!responsable.equals("")) {
            filtro = filtro + " and tbg.descripcion ilike '%" + responsable + "%'";
        }

        if (!tipo_proyecto.equals("")) {
            filtro = filtro + " and ofe.nuevo_modulo = '" + tipo_proyecto + "'";
        }

        if (!foms.equals("")) {
            filtro = filtro + " and ofe.num_os ilike  '%" + foms + "%'";
        }

        if (!id_cliente.equals("")) {
            filtro = filtro + " and ofe.id_cliente = '" + id_cliente + "'";
        }

        if (!nom_proyecto.equals("")) {
            filtro = filtro + " and ofe.nombre_proyecto ilike  '%" + nom_proyecto + "%'";
        }

        query = "cargarSolicitudesPendPorContrato";

        JsonArray arr = new JsonArray();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query).replaceAll("#filtro", filtro));
            System.out.println(ps);
            rs = ps.executeQuery();
            JsonObject fila = null;
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("id_solicitud", rs.getString("id_solicitud"));
                fila.addProperty("id_cliente", rs.getString("id_cliente"));
                fila.addProperty("nombre_cliente", rs.getString("nomcli"));
                fila.addProperty("num_os", rs.getString("num_os"));
                fila.addProperty("nombre_proyecto", rs.getString("nombre_proyecto"));
                fila.addProperty("creation_date", rs.getString("creation_date"));
                fila.addProperty("tipo_solicitud", rs.getString("tipo_solicitud"));
                fila.addProperty("estado_cartera", rs.getString("estado_cartera"));
                fila.addProperty("fecha_validacion_cartera", rs.getString("fecha_validacion_cartera"));
                fila.addProperty("id_trazabilidad", rs.getString("id_trazabilidad"));
                fila.addProperty("etapa_trazabilidad", rs.getString("etapa_trazabilidad"));
                fila.addProperty("Responsable", rs.getString("Responsable"));
                fila.addProperty("Interventor", rs.getString("Interventor"));
                fila.addProperty("modalidad_comercial", rs.getString("modalidad_comercial"));
                fila.addProperty("id_estado", rs.getString("id_estado"));
                fila.addProperty("nombre_estado", rs.getString("nombre_estado"));
                fila.addProperty("num_contrato", rs.getString("num_contrato"));
                fila.addProperty("tipo_contrato", rs.getString("tipo_contrato"));
                fila.addProperty("valor_cotizacion", rs.getString("valor_cotizacion"));
                fila.addProperty("total", rs.getString("total"));
                fila.addProperty("perc_anticipo", rs.getString("perc_anticipo"));
                fila.addProperty("perc_retegarantia", rs.getString("perc_retegarantia"));
                fila.addProperty("cant_facturada", rs.getString("cant_facturada"));
                fila.addProperty("valor_faturado", rs.getString("valor_faturado"));

                arr.add(fila);
            }
            obj.add("rows", arr);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return gson.toJson(obj);
        }
    }

    @Override
    public String getInfoSolicitud(String num_solicitud) {
        con = null;
        rs = null;
        ps = null;

        query = (perteneceNuevoModulo("1",num_solicitud))?  "SQL_INFO_SOLICITUD":"SQL_INFO_SOLICITUD_TEM";
        String respuestaJson = "{}";

        Gson gson = new Gson();
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);
            ps.setString(1, num_solicitud);

            rs = ps.executeQuery();
            JsonObject fila = new JsonObject();
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("razon_social_empresa", rs.getString("razon_social_empresa"));
                fila.addProperty("nit_empresa", rs.getString("nit_empresa"));
                fila.addProperty("direccion_empresa", rs.getString("direccion_empresa"));
                fila.addProperty("nombre_representante", rs.getString("nombre_representante"));
                fila.addProperty("cedula_representante", rs.getString("cedula_representante"));
                fila.addProperty("clasificacion", rs.getString("clasificacion"));
                fila.addProperty("regimen", rs.getString("regimen"));
                fila.addProperty("gran_contribuyente", rs.getString("gran_contribuyente"));
                fila.addProperty("agente_retenedor", rs.getString("agente_retenedor"));
                fila.addProperty("autoret_rfte", rs.getString("autoret_rfte"));
                fila.addProperty("autoret_iva", rs.getString("autoret_iva"));
                fila.addProperty("autoret_ica", rs.getString("autoret_ica"));
                fila.addProperty("nombre_proyecto", rs.getString("nombre_proyecto"));
                fila.addProperty("modalidad_comercial", rs.getString("modalidad_comercial"));
                fila.addProperty("perc_administracion", rs.getDouble("perc_administracion"));
                fila.addProperty("administracion", rs.getDouble("administracion"));
                fila.addProperty("perc_imprevisto", rs.getDouble("perc_imprevisto"));
                fila.addProperty("imprevisto", rs.getDouble("imprevisto"));
                fila.addProperty("perc_utilidad", rs.getDouble("perc_utilidad"));
                fila.addProperty("utilidad", rs.getDouble("utilidad"));
                fila.addProperty("perc_aiu", rs.getDouble("perc_aiu"));
                fila.addProperty("valor_aiu", rs.getDouble("valor_aiu"));
                fila.addProperty("perc_iva", rs.getDouble("perc_iva"));
                fila.addProperty("valor_iva", rs.getDouble("valor_iva"));
                fila.addProperty("total", rs.getDouble("total"));
                fila.addProperty("subtotal", rs.getDouble("subtotal"));
                fila.addProperty("anticipo", rs.getDouble("anticipo"));
                fila.addProperty("perc_anticipo", rs.getDouble("perc_anticipo"));
                fila.addProperty("valor_anticipo", rs.getDouble("valor_anticipo"));
                fila.addProperty("retegarantia", rs.getDouble("retegarantia"));
                fila.addProperty("perc_retegarantia", rs.getDouble("perc_retegarantia"));
                fila.addProperty("valor_retegarantia", rs.getDouble("valor_retegarantia"));
                fila.addProperty("num_contrato", rs.getString("num_contrato"));
                fila.addProperty("tipo_contrato", rs.getString("tipo_contrato"));
                fila.addProperty("asignado_broker", rs.getString("asignado_broker"));
            }
            respuestaJson = gson.toJson(fila);

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return respuestaJson;
    }

    @Override
    public String guardarContrato(String num_solicitud, String tipo_contrato, String num_contrato, String vlr_antes_iva, String porc_admon, String valor_admon, String porc_imprevisto, String valor_imprevisto,
            String porc_utilidad, String valor_utilidad, String porc_aiu, String valor_aiu, String porc_iva, String valor_iva, String valor_total, String porc_anticipo, String nit_empresa, String cedula_rep_empresa, Usuario usuario,
            JsonObject garantias, JsonObject garantias_extra, JsonObject infoProveedor, JsonObject infoFormaPago) {

        String respuestaJson = "{}";
        JsonObject respuesta = new JsonObject();
        String action = (existeProveedor(infoProveedor.get("nit_empresa").getAsJsonPrimitive().getAsString())) ? "actualizar" : "insertar";
        String op = "actualizar";
        boolean swHabilitaCot = false;
        TransaccionService stmt = new TransaccionService(usuario.getBd());
        try {

            ClientesVerDAO cvd = new ClientesVerDAO(usuario.getBd());
            if (num_contrato.equals("")) {
                SerieGeneral s_id_contrato = cvd.getSerie("FINV", "OP", "CONTRATO");
                cvd.setSerie("FINV", "OP", "CONTRATO");
                num_contrato = s_id_contrato.getUltimo_prefijo_numero();
                op = "insertar";
            }

            stmt.crearStatement();
            stmt.getSt().addBatch(guardarProveedor(action, infoProveedor, usuario));
            stmt.getSt().addBatch(actualizarCedulaRepLegal(nit_empresa, cedula_rep_empresa));
            stmt.getSt().addBatch(actualizarCotizacionOferta(num_solicitud, infoFormaPago, usuario));
            stmt.getSt().addBatch(guardarContrato(op, num_solicitud, tipo_contrato, num_contrato, vlr_antes_iva, porc_admon, valor_admon, porc_imprevisto, valor_imprevisto, porc_utilidad, valor_utilidad, porc_aiu, valor_aiu, porc_iva, valor_iva, valor_total, porc_anticipo, usuario));
//            if (op.equals("insertar")) {
//                stmt.getSt().addBatch(actualizarTrazabilidadSolicitud(num_solicitud, usuario));
//            }
            JsonArray arrGarantias = garantias.getAsJsonArray("garantias");
            for (int i = 0; i < arrGarantias.size(); i++) {
                respuesta = arrGarantias.get(i).getAsJsonObject();
                String id_poliza = respuesta.get("id_poliza").getAsString();
                String id_beneficiario = respuesta.get("id_beneficiario").getAsString();
                String tipo_entrada = respuesta.get("id_tipo_entrada").getAsString();
                String valor_base = respuesta.get("valor_base").getAsString();
                String porc_poliza = respuesta.get("porcentaje_poliza").getAsString();
                String valor_poliza = respuesta.get("valor_poliza").getAsString();
                String vigencia_poliza = respuesta.get("vigencia_poliza").getAsString();
                String id_causal = respuesta.get("id_causal").getAsString();
                if (respuesta.get("id").getAsString().startsWith("neo_")) {
                    stmt.getSt().addBatch(insertarGarantias(num_contrato, id_poliza, id_beneficiario, tipo_entrada, valor_base, porc_poliza, valor_poliza, vigencia_poliza, id_causal, "N", "N", "0", usuario));
                } else {
                    stmt.getSt().addBatch(actualizarGarantias(respuesta.get("id").getAsString(), id_poliza, id_beneficiario, tipo_entrada, valor_base, porc_poliza, valor_poliza, vigencia_poliza, id_causal, usuario));
                }

            }

            //Insertamos garantias otro si
           /* JsonArray arrGarantiasOtroSi = garantias_otro_si.getAsJsonArray("garantiasOtroSi");
             for (int i = 0; i < arrGarantiasOtroSi.size(); i++) {
             respuesta = arrGarantiasOtroSi.get(i).getAsJsonObject();
             String id_poliza = respuesta.get("id_poliza").getAsString();
             String id_beneficiario = respuesta.get("id_beneficiario").getAsString();
             String tipo_entrada = respuesta.get("id_tipo_entrada").getAsString();
             String valor_base = respuesta.get("valor_base").getAsString();
             String porc_poliza = respuesta.get("porcentaje_poliza").getAsString();
             String valor_poliza = respuesta.get("valor_poliza").getAsString();      
             String vigencia_poliza = respuesta.get("vigencia_poliza").getAsString();
             String id_causal = respuesta.get("id_causal").getAsString();
             if (respuesta.get("id").getAsString().startsWith("neo_")) {
             swHabilitaCot = true;
             stmt.getSt().addBatch(insertarGarantias(num_contrato, id_poliza, id_beneficiario, tipo_entrada, valor_base, porc_poliza, valor_poliza, vigencia_poliza, id_causal, "S", "S", usuario));
             stmt.getSt().addBatch(actualizarCotizadoBroker(num_contrato, id_beneficiario, usuario));
             }else{
             stmt.getSt().addBatch(actualizarGarantias(respuesta.get("id").getAsString(), id_poliza, id_beneficiario, tipo_entrada, valor_base, porc_poliza, valor_poliza, vigencia_poliza, id_causal, usuario));
             }     

             }*/
            //Insertamos garantias extracontractuales
            JsonArray arrGarantiasExtra = garantias_extra.getAsJsonArray("garantiasExtra");
            for (int i = 0; i < arrGarantiasExtra.size(); i++) {
                respuesta = arrGarantiasExtra.get(i).getAsJsonObject();
                String id_poliza = respuesta.get("id_poliza").getAsString();
                String id_beneficiario = respuesta.get("id_beneficiario").getAsString();
                String tipo_entrada = respuesta.get("id_tipo_entrada").getAsString();
                String valor_base = respuesta.get("valor_base").getAsString();
                String porc_poliza = respuesta.get("porcentaje_poliza").getAsString();
                String valor_poliza = respuesta.get("valor_poliza").getAsString();
                String vigencia_poliza = respuesta.get("vigencia_poliza").getAsString();
                String id_causal = respuesta.get("id_causal").getAsString();
                if (respuesta.get("id").getAsString().startsWith("neo_")) {
                    swHabilitaCot = true;
                    stmt.getSt().addBatch(insertarGarantias(num_contrato, id_poliza, id_beneficiario, tipo_entrada, valor_base, porc_poliza, valor_poliza, vigencia_poliza, id_causal, "S", "N", "0", usuario));
                    stmt.getSt().addBatch(actualizarCotizadoBroker(num_contrato, id_beneficiario, usuario));
                } else {
                    stmt.getSt().addBatch(actualizarGarantias(respuesta.get("id").getAsString(), id_poliza, id_beneficiario, tipo_entrada, valor_base, porc_poliza, valor_poliza, vigencia_poliza, id_causal, usuario));
                }

            }

            if (swHabilitaCot) {
                stmt.getSt().addBatch(habilitarCotizacionBroker(num_contrato));
            }
            stmt.execute();
            stmt.closeAll();
            respuestaJson = "{\"respuesta\":\"OK\"}";
        } catch (Exception e) {
            e.printStackTrace();
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                stmt.closeAll();
            } catch (Exception ex) {
                Logger.getLogger(MinutasContratacionImpl.class.getName()).log(Level.SEVERE, null, ex);
            }

            return respuestaJson;
        }

    }

    @Override
    public ArrayList searchNombresArchivos(String directorioArchivos, String numsolicitud) {

        ArrayList nombresArchivos = new ArrayList();
        File dir = new File(directorioArchivos + numsolicitud);
        try {
            if (dir.exists()) {
                String[] ficheros = dir.list();
                for (int x = 0; x < ficheros.length; x++) {
                    nombresArchivos.add(ficheros[x]);
                }
            }

        } catch (Exception e) {
            System.out.println("error en NegociosApplusDAO.getEstadosApplus.." + e.toString() + "__" + e.getMessage());
            e.printStackTrace();
        }

        return nombresArchivos;
    }

    @Override
    public boolean almacenarArchivoEnCarpetaUsuario(
            String negocio, String rutaOrigen, String rutaDestino, String filename
    ) {

        boolean swFileCopied = false;
        try {

            File carpetaDestino = new File(rutaDestino);

            //deleteDirectory(carpetaDestino);
            this.createDir(rutaDestino);

            try {
                //Copiamos el archivo de la carpeta origen a la carpeta destino
                InputStream in = new FileInputStream(rutaOrigen + "/" + filename);
                OutputStream out = new FileOutputStream(rutaDestino + filename);

                // Copy the bits from instream to outstream
                byte[] buf = new byte[1024];
                int len;
                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }
                in.close();
                out.close();
                swFileCopied = true;

            } catch (Exception k) {
                throw new Exception(" FILE: " + k.getMessage());
            }

        } catch (Exception e) {
            System.out.println("errorrr:" + e.toString() + "__" + e.getMessage());
            e.printStackTrace();
        }

        return swFileCopied;
    }

    @Override
    public boolean eliminarArchivo(String directorioArchivos, String numsol, String nomarchivo) {
        boolean deleted = false;
        File dir = new File(directorioArchivos + numsol);
        File archivo = new File(directorioArchivos + numsol + '/' + nomarchivo);
        try {
            if (dir.exists()) {
                if (archivo.exists()) {
                    deleted = archivo.delete();
                }
                if (dir.list().length == 0) {
                    deleteDirectory(dir);
                }
            }
        } catch (Exception e) {
            System.out.println("error en NegociosApplusDAO.getEstadosApplus.." + e.toString() + "__" + e.getMessage());
            e.printStackTrace();
        }
        return deleted;
    }

    public void createDir(String dir) throws Exception {
        try {
            File f = new File(dir);
            if (!f.exists()) {
                f.mkdir();
            }
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    static public boolean deleteDirectory(File path) {
        if (path.exists()) {
            File[] files = path.listFiles();
            for (int i = 0; i < files.length; i++) {
                if (files[i].isDirectory()) {
                    deleteDirectory(files[i]);
                } else {
                    files[i].delete();
                }
            }
        }
        return (path.delete());
    }

    @Override
    public String cargarFacturasParciales(String num_solicitud) {
        Gson gson = new Gson();
        con = null;
        ps = null;
        rs = null;
        JsonObject obj = new JsonObject();
        String query = "cargarFacturasParciales";

        JsonArray arr = new JsonArray();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, num_solicitud);
            rs = ps.executeQuery();
            JsonObject fila = null;
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("id", rs.getString("id"));
                fila.addProperty("num_factura", rs.getString("num_factura"));
                fila.addProperty("valor", rs.getString("valor"));
                fila.addProperty("dias_ejecucion", rs.getString("dias_ejecucion"));
                fila.addProperty("fecha_facturacion", rs.getString("fecha_facturacion"));
                fila.addProperty("dia_pago", rs.getString("dia_pago"));
                fila.addProperty("amortizacion", rs.getString("valor_amortizacion"));
                fila.addProperty("retegarantia", rs.getString("valor_retegarantia"));
                arr.add(fila);
            }
            obj.add("rows", arr);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return gson.toJson(obj);
        }
    }

    @Override
    public String cargarGarantias(String num_solicitud, String extracontractual, String otro_si, String secuencia) {
        Gson gson = new Gson();
        con = null;
        ps = null;
        rs = null;
        JsonObject obj = new JsonObject();
        String query = "cargarGarantias";
        String filtro = " AND extracontractual = '" + extracontractual + "' AND otro_si = '" + otro_si + "'";
        filtro += (!secuencia.equals("")) ? " AND g.secuencia_otro_si =" + secuencia : "";
        JsonArray arr = new JsonArray();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query).replaceAll("#filtro", filtro));
            ps.setString(1, num_solicitud);
            rs = ps.executeQuery();
            JsonObject fila = null;
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("id", rs.getString("id"));
                fila.addProperty("id_poliza", rs.getString("id_poliza"));
                fila.addProperty("poliza", rs.getString("nombre_poliza"));
                fila.addProperty("id_tipo_entrada", rs.getString("id_tipo_entrada"));
                fila.addProperty("tipo_entrada", rs.getString("tipo_entrada"));
                fila.addProperty("valor_base", rs.getString("valor_base"));
                fila.addProperty("porcentaje_poliza", rs.getString("porcentaje_poliza"));
                fila.addProperty("valor_poliza", rs.getString("valor_poliza"));
                fila.addProperty("vigencia_poliza", rs.getString("vigencia_poliza"));
                fila.addProperty("id_beneficiario", rs.getString("id_beneficiario"));
                fila.addProperty("beneficiario", rs.getString("beneficiario"));
                fila.addProperty("id_causal", rs.getString("id_causal"));
                fila.addProperty("causal", rs.getString("causal"));
                fila.addProperty("cxp_aseguradora", rs.getString("cxp_aseguradora"));
                fila.addProperty("secuencia", rs.getString("secuencia"));
                fila.addProperty("valor_asegurado", rs.getString("valor_asegurado"));
                fila.addProperty("cotizacion_aceptada", rs.getString("cotizacion_aceptada"));
                arr.add(fila);
            }
            obj.add("rows", arr);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return gson.toJson(obj);
        }
    }

    private String guardarContrato(String op, String num_solicitud, String tipo_contrato, String num_contrato, String vlr_antes_iva, String porc_admon, String valor_admon, String porc_imprevisto, String valor_imprevisto, String porc_utilidad, String valor_utilidad, String porc_aiu, String valor_aiu, String porc_iva, String valor_iva, String valor_total, String porc_anticipo, Usuario usuario) throws Exception {
        query = (op.equals("insertar")) ? "insertarContrato" : "actualizarContrato";
        String respuesta = "";
        st = null;

        try {
            st = new StringStatement(this.obtenerSQL(query), true);

            st.setString(1, num_solicitud);
            st.setDouble(2, Double.parseDouble(vlr_antes_iva));
            st.setString(3, porc_admon);
            st.setString(4, valor_admon);
            st.setString(5, porc_imprevisto);
            st.setString(6, valor_imprevisto);
            st.setString(7, porc_utilidad);
            st.setString(8, valor_utilidad);
            st.setString(9, porc_aiu);
            st.setString(10, valor_aiu);
            st.setString(11, porc_iva);
            st.setString(12, valor_iva);
            st.setString(13, valor_total);
            st.setString(14, porc_anticipo);
            st.setString(15, usuario.getLogin());
            st.setString(16, usuario.getDstrct());
            st.setString(17, tipo_contrato);
            st.setString(18, num_contrato);

            respuesta = st.getSql();
        } catch (Throwable e) {
            e.printStackTrace();
            throw new Exception("ERROR DURANTE guardarContrato. \n " + e.getMessage());
        }

        return respuesta;
    }

    public String insertarFacturacionParcial(int consecutivo, String num_contrato, String valor,
            String dias_ejecucion, String fecha_facturacion, String dia_pago, String amortizacion,
            String retegarantia, Usuario usuario) throws Exception {

        String respuesta = "";
        st = null;
        query = "insertarFacturacionParcial";

        try {

            st = new StringStatement(this.obtenerSQL(query), true);
            st.setInt(1, consecutivo);
            st.setString(2, num_contrato);
            st.setDouble(3, Double.parseDouble(valor));
            st.setInt(4, Integer.parseInt(dias_ejecucion));
            st.setString(5, fecha_facturacion);
            st.setInt(6, Integer.parseInt(dia_pago));
            st.setDouble(7, Double.parseDouble(amortizacion));
            st.setDouble(8, Double.parseDouble(retegarantia));
            st.setString(9, usuario.getLogin());
            st.setString(10, usuario.getDstrct());

            respuesta = st.getSql();

        } catch (Throwable e) {
            e.printStackTrace();
            System.out.println(e.toString());
            throw new Exception("ERROR DURANTE insertarFacturacionParcial. \n " + e.getMessage());
        }
        return respuesta;
    }

    public String actualizarFacturacionParcial(String id, String valor, String dias_ejecucion, String fecha_facturacion,
            String dia_pago, String amortizacion, String retegarantia, Usuario usuario) throws Exception {

        st = null;
        String respuesta = "";
        query = "actualizarFacturacionParcial";

        try {

            st = new StringStatement(this.obtenerSQL(query), true);
            st.setDouble(1, Double.parseDouble(valor));
            st.setInt(2, Integer.parseInt(dias_ejecucion));
            st.setString(3, fecha_facturacion);
            st.setInt(4, Integer.parseInt(dia_pago));
            st.setDouble(5, Double.parseDouble(amortizacion));
            st.setDouble(6, Double.parseDouble(retegarantia));
            st.setString(7, usuario.getLogin());
            st.setInt(8, Integer.parseInt(id));

            respuesta = st.getSql();

        } catch (Throwable e) {
            e.printStackTrace();
            System.out.println(e.toString());
            throw new SQLException("ERROR DURANTE actualizarFacturacionParcial. \n " + e.getMessage());
        }
        return respuesta;

    }

    public String insertarGarantias(String num_contrato, String id_poliza, String id_beneficiario, String tipo_entrada, String valor_base, String porc_poliza,
            String valor_poliza, String vigencia_poliza, String id_causal, String extracontractual, String otroSi, String secuencia, Usuario usuario) throws Exception {
        String respuesta = "";
        st = null;
        query = "insertarGarantias";
        try {
            st = new StringStatement(this.obtenerSQL(query), true);
            st.setString(1, num_contrato);
            st.setInt(2, Integer.parseInt(id_poliza.equals("") ? "0" : id_poliza));
            st.setInt(3, Integer.parseInt(tipo_entrada));
            st.setDouble(4, Double.parseDouble(valor_base));
            st.setDouble(5, Double.parseDouble(porc_poliza));
            st.setDouble(6, Double.parseDouble(valor_poliza));
            st.setInt(7, Integer.parseInt(vigencia_poliza));
            st.setString(8, usuario.getLogin());
            st.setString(9, usuario.getDstrct());
            st.setString(10, id_beneficiario);
            st.setString(11, id_causal);
            st.setString(12, extracontractual);
            st.setString(13, otroSi);
            st.setString(14, secuencia);
            respuesta = st.getSql();
        } catch (Throwable e) {
            e.printStackTrace();
            throw new Exception("ERROR DURANTE insertarGarantias. \n " + e.getMessage());
        }
        return respuesta;

    }

    public String actualizarGarantias(String id, String id_poliza, String id_beneficiario, String tipo_entrada, String valor_base, String porc_poliza, String valor_poliza, String vigencia_poliza, String id_causal, Usuario usuario) throws Exception {
        st = null;
        String respuesta = "";
        query = "actualizarGarantias";

        try {

            st = new StringStatement(this.obtenerSQL(query), true);
            st.setInt(1, Integer.parseInt(id_poliza));
            st.setInt(2, Integer.parseInt(tipo_entrada));
            st.setDouble(3, Double.parseDouble(valor_base));
            st.setDouble(4, Double.parseDouble(porc_poliza));
            st.setDouble(5, Double.parseDouble(valor_poliza));
            st.setInt(6, Integer.parseInt(vigencia_poliza));
            st.setString(7, usuario.getLogin());
            st.setString(8, id_beneficiario);
            st.setString(9, id_causal);
            st.setInt(10, Integer.parseInt(id));

            respuesta = st.getSql();

        } catch (Throwable e) {
            e.printStackTrace();
            System.out.println(e.toString());
            throw new SQLException("ERROR DURANTE actualizarGarantias. \n " + e.getMessage());
        }

        return respuesta;

    }

    @Override
    public String listarDiasPago() {
        con = null;
        rs = null;
        ps = null;

        query = "SQL_CARGAR_DIAS_PAGO";
        String respuestaJson = "{}";

        Gson gson = new Gson();
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);

            rs = ps.executeQuery();
            JsonObject fila = new JsonObject();
            while (rs.next()) {
                fila.addProperty(rs.getString("id"), rs.getString("valor"));
            }
            respuestaJson = gson.toJson(fila);

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return respuestaJson;
    }

    @Override
    public String listarPolizas() {
        con = null;
        rs = null;
        ps = null;

        query = "SQL_CARGAR_POLIZAS";
        String respuestaJson = "{}";

        Gson gson = new Gson();
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);

            rs = ps.executeQuery();
            JsonObject fila = new JsonObject();
            while (rs.next()) {
                fila.addProperty(rs.getString("id"), rs.getString("nombre_poliza"));
            }
            respuestaJson = gson.toJson(fila);

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return respuestaJson;
    }

    @Override
    public String cargarConfigDocs() {
        con = null;
        rs = null;
        ps = null;

        query = "SQL_GET_CONFIG_DOCS_MINUTA";
        String respuestaJson = "{}";

        Gson gson = new Gson();
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query).replaceAll("#filtro", "");

            ps = con.prepareStatement(query);

            rs = ps.executeQuery();
            JsonObject fila = new JsonObject();
            while (rs.next()) {
                fila.addProperty(rs.getString("tipo_doc"), rs.getString("info"));
            }
            respuestaJson = gson.toJson(fila);

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return respuestaJson;
    }

    public boolean existeConfigDoc(String id_tipo_doc) {
        con = null;
        rs = null;
        ps = null;

        query = "SQL_GET_CONFIG_DOCS_MINUTA";
        String filtro = " AND id_tipo_doc=" + id_tipo_doc;
        boolean resp = false;

        Gson gson = new Gson();
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query).replaceAll("#filtro", filtro);

            ps = con.prepareStatement(query);

            rs = ps.executeQuery();
            while (rs.next()) {
                resp = true;
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return resp;
    }

    @Override
    public String guardarConfigDocs(String tipo_doc, String content, Usuario usuario) {
        con = null;
        ps = null;
        query = (existeConfigDoc(tipo_doc)) ? "actualizarConfigDoc" : "insertarConfigDoc";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, content);
            ps.setString(2, usuario.getLogin());
            ps.setInt(3, Integer.parseInt(tipo_doc));

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
            throw new SQLException("ERROR guardarConfigDocs \n" + e.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    @Override
    public String cargarEquivalencias() {
        con = null;
        ps = null;
        rs = null;
        JsonObject obj = new JsonObject();

        Gson gson = new Gson();
        query = "cargarEquivalencias";
        String respuestaJson = "{}";
        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);
            rs = ps.executeQuery();
            JsonArray datos = new JsonArray();
            JsonObject fila = null;
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("id", rs.getInt("id"));
                fila.addProperty("codparam", rs.getString("codparam"));
                fila.addProperty("descripcion", rs.getString("descripcion"));
                fila.addProperty("procedencia", rs.getString("procedencia"));
                datos.add(fila);
            }
            obj.add("rows", datos);
            respuestaJson = gson.toJson(obj);
        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return respuestaJson;
    }

    @Override
    public String listarClasificacionPersona() {
        con = null;
        rs = null;
        ps = null;

        query = "SQL_CARGAR_CLASIFICACION_PERSONA";
        String respuestaJson = "{}";

        Gson gson = new Gson();
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);

            rs = ps.executeQuery();
            JsonObject fila = new JsonObject();
            while (rs.next()) {
                fila.addProperty(rs.getString("id"), rs.getString("descripcion"));
            }
            respuestaJson = gson.toJson(fila);

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return respuestaJson;
    }

    public String guardarProveedor(String accion, JsonObject proveedor, Usuario usuario) throws Exception {

        String respuesta = "";
        st = null;
        query = (accion.equals("insertar")) ? "SQL_INSERT_PROVEEDOR" : "SQL_UPDATE_PROVEEDOR";

        try {

            st = new StringStatement(this.obtenerSQL(query), true);

            st.setString(1, proveedor.get("razon_social").getAsJsonPrimitive().getAsString());
            st.setString(2, proveedor.get("clasificacion").getAsJsonPrimitive().getAsString());
            st.setString(3, proveedor.get("gran_contribuyente").getAsJsonPrimitive().getAsString());
            st.setString(4, proveedor.get("agente_retenedor").getAsJsonPrimitive().getAsString());
            st.setString(5, proveedor.get("autoretenedor_rfte").getAsJsonPrimitive().getAsString());
            st.setString(6, proveedor.get("autoretenedor_iva").getAsJsonPrimitive().getAsString());
            st.setString(7, proveedor.get("autoretenedor_ica").getAsJsonPrimitive().getAsString());
            st.setString(8, usuario.getLogin());
            st.setString(9, usuario.getDstrct());
            st.setString(10, proveedor.get("regimen").getAsJsonPrimitive().getAsString());
            st.setString(11, proveedor.get("digito_verificacion").getAsJsonPrimitive().getAsString());
            st.setString(12, proveedor.get("nit_empresa").getAsJsonPrimitive().getAsString());

            respuesta = st.getSql();

        } catch (Throwable e) {
            e.printStackTrace();
            System.out.println(e.toString());
            throw new Exception("ERROR DURANTE guardarProveedor. \n " + e.getMessage());
        }
        return respuesta;
    }

    public boolean existeProveedor(String nit) {
        con = null;
        rs = null;
        ps = null;

        query = "SQL_GET_INFO_PROVEEDOR";
        boolean resp = false;

        Gson gson = new Gson();
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);
            ps.setString(1, nit);

            rs = ps.executeQuery();
            while (rs.next()) {
                resp = true;
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return resp;
    }

    @Override
    public String generarCartaAceptacion(String num_solicitud, String rutaOrigen, String rutaDestino, String usuario) {
        con = null;
        ps = null;
        rs = null;
        query = "SQL_GET_INFO_MINUTA";
        String nomarchivo = "", htmldoc = "";
        ResourceBundle rb = null;
        String respuestaJson = "{}";
        try {
            DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
            //get current date time with Date()
            Date date = new Date();
            nomarchivo = "carta_aceptacion_" + dateFormat.format(date) + ".pdf";

            rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            Document document = new Document(PageSize.LEGAL, 85, 85, 72, 52);

            HTMLWorker htmlWorker = new HTMLWorker(document);

            PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(rutaOrigen + "/" + nomarchivo));

            Font f = new Font(Font.TIMES_ROMAN, 12, Font.NORMAL);
            Font f1 = new Font(Font.TIMES_ROMAN, 12, Font.BOLD);

            Font f2 = new Font(Font.TIMES_ROMAN, 8, Font.NORMAL);
            Font f3 = new Font(Font.TIMES_ROMAN, 8, Font.BOLD);

            HeaderFooterPdf header = new HeaderFooterPdf(rb);
            pdfWriter.setPageEvent(header);

            document.open();
            document.addAuthor(usuario);
            document.addCreator("FINTRA S.A.");
            document.addCreationDate();
            document.addTitle("Carta de Aceptación");

            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, "1");
            ps.setString(2, num_solicitud);

            rs = ps.executeQuery();

            while (rs.next()) {
                htmldoc = rs.getString("info");
            }

            htmlWorker.parse(new StringReader("<div>" + htmldoc + "</div>"));
            document.close();
            almacenarArchivoEnCarpetaUsuario(num_solicitud, rutaOrigen, rutaDestino + num_solicitud + "/", nomarchivo);

            respuestaJson = "{\"respuesta\":\"OK\",\"Ruta\":\"" + "/images/multiservicios/" + usuario + "/" + nomarchivo + "\"}";

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
            throw new SQLException("ERROR generarActaPagares \n" + e.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    @Override
    public String generarContrato(String num_solicitud, String rutaOrigen, String rutaDestino, String usuario) {
        con = null;
        ps = null;
        rs = null;
        String sql = "";
        String nomarchivo = "";
        String htmldoc = "";
        ResourceBundle rb = null;
        String respuestaJson = "{}";
        try {
            DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
            //get current date time with Date()
            Date date = new Date();
            nomarchivo = "contrato_" + dateFormat.format(date) + ".pdf";

            rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            Document document = new Document(PageSize.LEGAL, 85, 85, 90, 52);

            HTMLWorker htmlWorker = new HTMLWorker(document);

            PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(rutaOrigen + "/" + nomarchivo));

            HeaderFooterPdf header = new HeaderFooterPdf(rb);
            pdfWriter.setPageEvent(header);

            document.open();
            document.addAuthor(usuario);
            document.addCreator("FINTRA S.A.");
            document.addCreationDate();
            document.addTitle("Contrato de Prestación de Servicios");

            con = this.conectarJNDI();
            query = this.obtenerSQL((perteneceNuevoModulo("1",num_solicitud))?  "SQL_INFO_SOLICITUD":"SQL_INFO_SOLICITUD_TEM");
            ps = con.prepareStatement(query);
            ps.setString(1, num_solicitud);

            rs = ps.executeQuery();
            String num_contrato = "", tipo_contrato = "";
            while (rs.next()) {
                num_contrato = rs.getString("num_contrato");
                tipo_contrato = rs.getString("tipo_contrato");
            }

            query = this.obtenerSQL("SQL_GET_INFO_CONTRATO");
            ps = con.prepareStatement(query);
            ps.setInt(1, Integer.parseInt(tipo_contrato));
            ps.setString(2, num_contrato);

            rs = ps.executeQuery();

            while (rs.next()) {
                htmldoc = rs.getString("info");
            }

            htmlWorker.parse(new StringReader("<div>" + htmldoc + "</div>"));
            document.close();
           //almacenarArchivoEnCarpetaUsuario(num_solicitud, rutaOrigen, rutaDestino + num_solicitud+"/", nomarchivo);

            respuestaJson = "{\"respuesta\":\"OK\",\"Ruta\":\"" + "/images/multiservicios/" + usuario + "/" + nomarchivo + "\"}";

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
            throw new Exception("ERROR generarContrato \n" + e.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    @Override
    public String crearDocsContrato(JsonObject contratos, Usuario usuario) {
        con = null;
        String respuestaJson = "{}";
        query = "creaContratoDocs";

        try {

            con = this.conectarJNDI(this.obtenerSQL(query), usuario.getBd());
            CallableStatement callableStatement = con.prepareCall("{" + this.obtenerSQL(query) + "}");
            JsonArray arrContratos = contratos.getAsJsonArray("contratos");
            for (int i = 0; i < arrContratos.size(); i++) {
                String num_solicitud = arrContratos.get(i).getAsJsonObject().get("num_solicitud").getAsJsonPrimitive().getAsString();
                String num_contrato = arrContratos.get(i).getAsJsonObject().get("num_contrato").getAsJsonPrimitive().getAsString();
                String tipo_contrato = arrContratos.get(i).getAsJsonObject().get("tipo_contrato").getAsJsonPrimitive().getAsString();

                callableStatement.setString(1, usuario.getDstrct());
                callableStatement.setString(2, num_solicitud);
                callableStatement.setString(3, num_contrato);
                callableStatement.setString(4, tipo_contrato);
                callableStatement.setString(5, usuario.getLogin());
                callableStatement.addBatch();

            }
            int[] updateCounts = callableStatement.executeBatch();
            respuestaJson = "{\"respuesta\":\"OK\"}";
        } catch (Exception e) {
            respuestaJson = "{\"respuesta\":\"" + e.getMessage() + "\"}";
            try {
                System.out.println(e.getMessage());
                e.printStackTrace();
                throw new SQLException("ERROR crearDocsContrato \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(MinutasContratacionImpl.class.getName()).log(Level.SEVERE, null, ex.getNextException());
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    @Override
    public boolean existeContratoDoc(String num_contrato, String id_tipo_doc) {
        con = null;
        rs = null;
        ps = null;

        query = "SQL_GET_INFO_CONTRATO";
        boolean resp = false;

        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);
            ps.setInt(1, Integer.parseInt(id_tipo_doc));
            ps.setString(2, num_contrato);

            rs = ps.executeQuery();
            while (rs.next()) {
                resp = true;
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return resp;
    }

    @Override
    public String getInfoContratoDocs(String num_contrato, String tipo_doc) {
        con = null;
        rs = null;
        ps = null;

        query = "SQL_GET_INFO_CONTRATO";
        String respuestaJson = "{}";

        Gson gson = new Gson();
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);
            ps.setInt(1, Integer.parseInt(tipo_doc));
            ps.setString(2, num_contrato);

            rs = ps.executeQuery();
            JsonObject fila = new JsonObject();
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("id_tipo_doc", rs.getString("id_tipo_doc"));
                fila.addProperty("document_info", rs.getString("info"));
            }
            respuestaJson = gson.toJson(fila);

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return respuestaJson;
    }

    public String insertarContratoDocs(String num_solicitud, String num_contrato, String tipo_contrato, Usuario usuario) throws Exception {
        String respuesta = "";
        st = null;
        query = "insertarContratoDocs";
        try {
            st = new StringStatement(this.obtenerSQL(query), true);
            st.setString(1, usuario.getDstrct());
            st.setInt(2, Integer.parseInt(tipo_contrato));
            st.setString(3, num_contrato);
            st.setString(4, tipo_contrato);
            st.setString(5, num_solicitud);
            st.setString(6, usuario.getLogin());
            respuesta = st.getSql();
        } catch (Throwable e) {
            e.printStackTrace();
            throw new Exception("ERROR DURANTE insertarContratoDocs. \n " + e.getMessage());
        }
        return respuesta;

    }

    @Override
    public String actualizarContratoDocs(String tipo_doc, String num_contrato, String content, Usuario usuario) {
        con = null;
        ps = null;
        query = "actualizarContratoDocs";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, content);
            ps.setString(2, usuario.getLogin());
            ps.setInt(3, Integer.parseInt(tipo_doc));
            ps.setString(4, num_contrato);

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
            throw new SQLException("ERROR actualizarContratoDocs \n" + e.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    static class HeaderFooterPdf extends PdfPageEventHelper {

        private Image img;

        public HeaderFooterPdf(ResourceBundle rb) throws IOException {
            try {
                String url_logo = "logo_Selectrik.png";
                img = Image.getInstance(rb.getString("ruta") + "/images/login/" + url_logo);
                img.setAbsolutePosition(20f, 930f);
                img.scalePercent(50);
            } catch (BadElementException | IOException r) {
                System.err.println("Error al leer la imagen");
            }
        }

        @Override
        public void onStartPage(PdfWriter writer, Document document) {
            try {
                document.add(img);
            } catch (DocumentException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
        }
    }

    @Override
    public String listarTipoDocumentos(String mostrarTodos) {
        con = null;
        rs = null;
        ps = null;

        query = "SQL_GET_TIPO_DOCUMENTOS";
        String respuestaJson = "{}";
        String filtro = (mostrarTodos.equals("S")) ? "" : " WHERE reg_status = ''";
        JsonObject obj = new JsonObject();
        Gson gson = new Gson();
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query).replaceAll("#filtro", filtro);

            ps = con.prepareStatement(query);

            rs = ps.executeQuery();
            JsonObject fila = null;
            JsonArray arr = new JsonArray();
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("id", rs.getString("id"));
                fila.addProperty("nombre", rs.getString("nombre"));
                fila.addProperty("descripcion", rs.getString("descripcion"));
                fila.addProperty("reg_status", rs.getString("reg_status"));
                fila.addProperty("cambio", rs.getString("cambio"));
                arr.add(fila);
            }
            obj.add("rows", arr);
            respuestaJson = gson.toJson(obj);

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return respuestaJson;
    }

    @Override
    public String guardarTipoDocumento(String nombre, String descripcion, Usuario usuario) {
        con = null;
        ps = null;
        query = "SQL_INSERT_TIPO_DOCUMENTO";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, usuario.getDstrct());
            ps.setString(2, nombre);
            ps.setString(3, descripcion);
            ps.setString(4, usuario.getLogin());

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
            throw new SQLException("ERROR guardarTipoDocumento \n" + e.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    @Override
    public String actualizarTipoDocumento(String id, String nombre, String descripcion, Usuario usuario) {
        con = null;
        ps = null;
        query = "SQL_UPDATE_TIPO_DOCUMENTO";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, nombre);
            ps.setString(2, descripcion);
            ps.setString(3, usuario.getLogin());
            ps.setInt(4, Integer.parseInt(id));

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
            throw new SQLException("ERROR actualizarTipoDocumento \n" + e.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    @Override
    public String activaInactivaTipoDocumento(String id, Usuario usuario) {
        con = null;
        ps = null;
        query = "activarInactivarTipoDoc";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, usuario.getLogin());
            ps.setInt(2, Integer.parseInt(id));

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
            throw new SQLException("ERROR activaInactivaTipoDocumento \n" + e.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    @Override
    public String cargarGridAsignacionBroker(String fecha_ini, String fecha_fin, String num_solicitud, String nombre_cliente, String lineaNegocio, String responsable,  String etapaActual, String id_cliente, String nom_proyecto, String foms, String tipo_proyecto) {
        Gson gson = new Gson();
        con = null;
        ps = null;
        rs = null;
        JsonObject obj = new JsonObject();
        String filtro = "";
        filtro += (num_solicitud.equals("")) ? "" : " AND  ofe.id_solicitud = '" + num_solicitud + "'";

        if (!fecha_ini.equals("") && !fecha_fin.equals("")) {
            filtro = filtro + " AND co.creation_date::date between '" + fecha_ini + "' and '" + fecha_fin + "' ";
        }
        if (!nombre_cliente.equals("")) {
            filtro = filtro + " and cl.nomcli ilike '%" + nombre_cliente + "%'";
        }
        if (!lineaNegocio.equals("")) {
            filtro = filtro + " and tipo_proyecto =  '" + lineaNegocio + "' ";
        }
        if (!responsable.equals("")) {
            filtro = filtro + " and tbg.descripcion ilike '%" + responsable + "%'";
        }
        
        if (!tipo_proyecto.equals("")) {
            filtro = filtro + " and ofe.nuevo_modulo = '" + tipo_proyecto + "'";
        }

        if (!foms.equals("")) {
            filtro = filtro + " and ofe.num_os ilike  '%" + foms + "%'";
        }

        if (!id_cliente.equals("")) {
            filtro = filtro + " and ofe.id_cliente = '" + id_cliente + "'";
        }

        if (!nom_proyecto.equals("")) {
            filtro = filtro + " and ofe.nombre_proyecto ilike  '%" + nom_proyecto + "%'";
        }
        
        query = (perteneceNuevoModulo("1",num_solicitud))?  "cargarGrillaAsignacionBroker":"cargarGrillaAsignacionBroker_tem";

        JsonArray arr = new JsonArray();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query).replaceAll("#filtro", filtro));
            rs = ps.executeQuery();
            JsonObject fila = null;
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("id_solicitud", rs.getString("id_solicitud"));
                fila.addProperty("id_cliente", rs.getString("id_cliente"));
                fila.addProperty("nombre_cliente", rs.getString("nomcli"));
                fila.addProperty("num_os", rs.getString("num_os"));
                fila.addProperty("nombre_proyecto", rs.getString("nombre_proyecto"));
                fila.addProperty("creation_date", rs.getString("creation_date"));
                fila.addProperty("tipo_solicitud", rs.getString("tipo_solicitud"));
                fila.addProperty("fecha_contrato", rs.getString("fecha_contrato"));
                fila.addProperty("id_trazabilidad", rs.getString("id_trazabilidad"));
                fila.addProperty("etapa_trazabilidad", rs.getString("etapa_trazabilidad"));
                fila.addProperty("Responsable", rs.getString("Responsable"));
                fila.addProperty("Interventor", rs.getString("Interventor"));
                fila.addProperty("id_estado", rs.getString("id_estado"));
                fila.addProperty("nombre_estado", rs.getString("nombre_estado"));
                fila.addProperty("num_contrato", rs.getString("num_contrato"));
                fila.addProperty("tipo_contrato", rs.getString("tipo_contrato"));
                fila.addProperty("cotizado_broker", rs.getString("cotizado_broker"));
                fila.addProperty("cotizacion_aceptada", rs.getString("cotizacion_aceptada"));
                fila.addProperty("cxp_aseguradora", rs.getString("cxp_aseguradora"));
                fila.addProperty("valor_cotizacion", rs.getString("valor_cotizacion"));

                arr.add(fila);
            }
            obj.add("rows", arr);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return gson.toJson(obj);
        }
    }

    @Override
    public String listarBroker() {
        con = null;
        rs = null;
        ps = null;

        query = "SQL_CARGAR_BROKER";
        String respuestaJson = "{}";

        Gson gson = new Gson();
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);

            rs = ps.executeQuery();
            JsonObject fila = new JsonObject();
            while (rs.next()) {
                fila.addProperty(rs.getString("id"), rs.getString("valor"));
            }
            respuestaJson = gson.toJson(fila);

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return respuestaJson;
    }

    @Override
    public String insertarRelBrokerMinuta(String num_contrato, String id_broker, String id_beneficiario, Usuario usuario) {
        con = null;
        ps = null;
        query = "SQL_INSERT_REL_BROKER_MINUTA";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, usuario.getDstrct());
            ps.setString(2, num_contrato);
            ps.setInt(3, Integer.parseInt(id_broker));
            ps.setString(4, usuario.getLogin());
            ps.setInt(5, Integer.parseInt(id_beneficiario));

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
            throw new SQLException("ERROR insertarRelBrokerMinuta \n" + e.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    @Override
    public String actualizarRelBrokerMinuta(String num_contrato, String id_broker, String id_beneficiario, Usuario usuario) {
        con = null;
        ps = null;
        query = "SQL_UPDATE_REL_BROKER_MINUTA";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setInt(1, Integer.parseInt(id_broker));
            ps.setString(2, usuario.getLogin());
            ps.setString(3, num_contrato);
            ps.setInt(4, Integer.parseInt(id_beneficiario));

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
            throw new SQLException("ERROR actualizarRelBrokerMinuta \n" + e.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    @Override
    public String cargarGridSolicitudesBroker(String fecha_ini, String fecha_fin, String num_solicitud, String nombre_cliente, String lineaNegocio, String responsable, Usuario usuario) {
        Gson gson = new Gson();
        con = null;
        ps = null;
        rs = null;
        JsonObject obj = new JsonObject();
        String filtro = "";
        filtro += (num_solicitud.equals("")) ? "" : " AND  ofe.id_solicitud = '" + num_solicitud + "'";

        if (!fecha_ini.equals("") && !fecha_fin.equals("")) {
            filtro = filtro + " AND co.creation_date::date between '" + fecha_ini + "' and '" + fecha_fin + "' ";
        }
        if (!nombre_cliente.equals("")) {
            filtro = filtro + " and cl.nomcli ilike '%" + nombre_cliente + "%'";
        }
        if (!lineaNegocio.equals("")) {
            filtro = filtro + " and tp.cod_proyecto =  '" + lineaNegocio + "' ";
        }
        if (!responsable.equals("")) {
            filtro = filtro + " and tbg.descripcion ilike '%" + responsable + "%'";
        }

        filtro += " AND (select count(*) from opav.sl_rel_minutas_broker rel_broker\n"
                + " INNER JOIN opav.sl_broker br ON br.id = rel_broker.id_broker\n"
                + " WHERE id_contrato = co.numero_contrato AND UPPER(br.idusuario) ='" + usuario.getLogin() + "')>0";

        query = (perteneceNuevoModulo("1",num_solicitud))?  "cargarGrillaAsignacionBroker":"cargarGrillaAsignacionBroker_tem";

        JsonArray arr = new JsonArray();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query).replaceAll("#filtro", filtro));
            rs = ps.executeQuery();
            JsonObject fila = null;
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("id_solicitud", rs.getString("id_solicitud"));
                fila.addProperty("id_cliente", rs.getString("id_cliente"));
                fila.addProperty("nombre_cliente", rs.getString("nomcli"));
                fila.addProperty("num_os", rs.getString("num_os"));
                fila.addProperty("nombre_proyecto", rs.getString("nombre_proyecto"));
                fila.addProperty("creation_date", rs.getString("creation_date"));
                fila.addProperty("tipo_solicitud", rs.getString("tipo_solicitud"));
                fila.addProperty("fecha_contrato", rs.getString("fecha_contrato"));
                fila.addProperty("id_trazabilidad", rs.getString("id_trazabilidad"));
                fila.addProperty("etapa_trazabilidad", rs.getString("etapa_trazabilidad"));
                fila.addProperty("Responsable", rs.getString("Responsable"));
                fila.addProperty("Interventor", rs.getString("Interventor"));
                fila.addProperty("id_estado", rs.getString("id_estado"));
                fila.addProperty("nombre_estado", rs.getString("nombre_estado"));
                fila.addProperty("num_contrato", rs.getString("num_contrato"));
                fila.addProperty("tipo_contrato", rs.getString("tipo_contrato"));
                fila.addProperty("cotizado_broker", rs.getString("cotizado_broker"));
                fila.addProperty("total_cotizaciones", rs.getString("total_cotizaciones"));
                fila.addProperty("cotizacion_aceptada", rs.getString("cotizacion_aceptada"));

                arr.add(fila);
            }
            obj.add("rows", arr);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return gson.toJson(obj);
        }
    }

    @Override
    public String listarAseguradoras() {
        con = null;
        rs = null;
        ps = null;

        query = "SQL_CARGAR_ASEGURADORAS";
        String respuestaJson = "{}";

        Gson gson = new Gson();
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);

            rs = ps.executeQuery();
            JsonObject fila = new JsonObject();
            while (rs.next()) {
                fila.addProperty(rs.getString("id"), rs.getString("valor"));
            }
            respuestaJson = gson.toJson(fila);

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return respuestaJson;
    }

    @Override
    public String cargarCotizacionAseguradora(String num_solicitud, String id_aseguradora, String id_beneficiario, String secuencia) {
        Gson gson = new Gson();
        con = null;
        ps = null;
        rs = null;
        JsonObject obj = new JsonObject();
        query = "cargarCotizacionAseguradora";

        JsonArray arr = new JsonArray();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setInt(1, Integer.parseInt(id_aseguradora));
            ps.setString(2, num_solicitud);
            ps.setInt(3, Integer.parseInt(id_beneficiario));
            ps.setInt(4, Integer.parseInt(secuencia));
            rs = ps.executeQuery();
            JsonObject fila = null;
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("id_cot_aseguradora", rs.getString("id_cot_aseguradora"));
                fila.addProperty("id_garantia", rs.getString("id_garantia"));
                fila.addProperty("poliza", rs.getString("nombre_poliza"));
                fila.addProperty("valor_base", rs.getString("valor_base"));
                fila.addProperty("porcentaje_poliza", rs.getString("porcentaje_poliza"));
                fila.addProperty("valor_poliza", rs.getString("valor_poliza"));
                fila.addProperty("vigencia_poliza", rs.getString("vigencia_poliza"));
                fila.addProperty("porcentaje_aseguradora", rs.getString("porcentaje_aseguradora"));
                fila.addProperty("valor_aseguradora", rs.getString("valor_aseguradora"));
                arr.add(fila);
            }
            obj.add("rows", arr);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return gson.toJson(obj);
        }
    }

    @Override
    public String guardarCotizacionAseguradora(String num_contrato, String id_aseguradora, String id_beneficiario, String secuencia, JsonObject cotizaciones, String otros_gastos, String porc_iva, String valor_iva, Usuario usuario) {

        String respuestaJson = "{}";
        JsonObject respuesta = new JsonObject();
        TransaccionService stmt = new TransaccionService(usuario.getBd());
        try {

            stmt.crearStatement();

            JsonArray arrCotizaciones = cotizaciones.getAsJsonArray("cotizaciones");
            for (int i = 0; i < arrCotizaciones.size(); i++) {
                respuesta = arrCotizaciones.get(i).getAsJsonObject();
                String id_garantia = respuesta.get("id_garantia").getAsString();
                String valor_base = respuesta.get("valor_base").getAsString();
                String porc_poliza = respuesta.get("porcentaje_poliza").getAsString();
                String valor_poliza = respuesta.get("valor_poliza").getAsString();
                String porc_aseguradora = respuesta.get("porcentaje_aseguradora").getAsString();
                String valor_aseguradora = respuesta.get("valor_aseguradora").getAsString();
                if (respuesta.get("id_cot_aseguradora").getAsString().equals("0")) {
                    stmt.getSt().addBatch(insertarCotizacionAseguradora(num_contrato, id_garantia, id_aseguradora, valor_base, porc_poliza, valor_poliza, porc_aseguradora, valor_aseguradora, id_beneficiario, secuencia, usuario));
                } else {
                    stmt.getSt().addBatch(actualizarCotizacionAseguradora(respuesta.get("id_cot_aseguradora").getAsString(), porc_aseguradora, valor_aseguradora, usuario));
                }

            }
            if (!existeOtrosCostosAseguradora(num_contrato, id_aseguradora, id_beneficiario, secuencia)) {
                stmt.getSt().addBatch(insertarCotizAseguradoraOtrosCostos(num_contrato, id_aseguradora, id_beneficiario, secuencia, otros_gastos, porc_iva, valor_iva, usuario));
            } else {
                stmt.getSt().addBatch(actualizarCotizAseguradoraOtrosCostos(num_contrato, id_aseguradora, id_beneficiario, otros_gastos, porc_iva, valor_iva, usuario));
            }

            stmt.execute();
            stmt.closeAll();
            respuestaJson = "{\"respuesta\":\"OK\"}";
        } catch (Exception e) {
            e.printStackTrace();
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                stmt.closeAll();
            } catch (Exception ex) {
                Logger.getLogger(MinutasContratacionImpl.class.getName()).log(Level.SEVERE, null, ex);
            }

            return respuestaJson;
        }

    }

    public String insertarCotizacionAseguradora(String num_contrato, String id_poliza, String id_aseguradora, String valor_base, String porc_poliza,
            String valor_poliza, String porc_aseguradora, String valor_aseguradora, String id_beneficiario, String secuencia, Usuario usuario) throws Exception {
        String respuesta = "";
        st = null;
        query = "insertarCotizacionAseguradora";
        try {
            st = new StringStatement(this.obtenerSQL(query), true);
            st.setString(1, num_contrato);
            st.setInt(2, Integer.parseInt(id_poliza));
            st.setInt(3, Integer.parseInt(id_aseguradora));
            st.setDouble(4, Double.parseDouble(valor_base));
            st.setDouble(5, Double.parseDouble(porc_poliza));
            st.setDouble(6, Double.parseDouble(valor_poliza));
            st.setDouble(7, Double.parseDouble(porc_aseguradora));
            st.setDouble(8, Double.parseDouble(valor_aseguradora));
            st.setString(9, usuario.getLogin());
            st.setString(10, usuario.getDstrct());
            st.setString(11, id_beneficiario);
            st.setString(12, secuencia);
            respuesta = st.getSql();
        } catch (Throwable e) {
            e.printStackTrace();
            throw new Exception("ERROR DURANTE insertarCotizacionAseguradora. \n " + e.getMessage());
        }
        return respuesta;

    }

    public String actualizarCotizacionAseguradora(String id, String porc_aseguradora, String valor_aseguradora, Usuario usuario) throws Exception {
        st = null;
        String respuesta = "";
        query = "actualizarCotizacionAseguradora";

        try {

            st = new StringStatement(this.obtenerSQL(query), true);
            st.setDouble(1, Double.parseDouble(porc_aseguradora));
            st.setDouble(2, Double.parseDouble(valor_aseguradora));
            st.setString(3, usuario.getLogin());
            st.setInt(4, Integer.parseInt(id));

            respuesta = st.getSql();

        } catch (Throwable e) {
            e.printStackTrace();
            System.out.println(e.toString());
            throw new SQLException("ERROR DURANTE actualizarCotizacionAseguradora. \n " + e.getMessage());
        }

        return respuesta;

    }

    @Override
    public String cargarGridTotalesAseguradora(String num_contrato, String cotizado_broker, Usuario usuario) {
        Gson gson = new Gson();
        con = null;
        ps = null;
        rs = null;
        JsonObject obj = new JsonObject();
        query = "SQL_CARGAR_TOTALES_ASEGURADORA";
        String filtro = (cotizado_broker.equals("S")) ? "  AND rel_broker.cotizado_broker = 'S'" : " AND UPPER(br.idusuario) = '" + usuario.getLogin() + "'";
        JsonArray arr = new JsonArray();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query).replaceAll("#cond", filtro));
            ps.setString(1, num_contrato);
            rs = ps.executeQuery();
            JsonObject fila = null;
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("id", rs.getString("id"));
                fila.addProperty("id_aseguradora", rs.getString("id_aseguradora"));
                fila.addProperty("nombre_seguro", rs.getString("nombre_seguro"));
                fila.addProperty("id_beneficiario", rs.getString("id_beneficiario"));
                fila.addProperty("beneficiario", rs.getString("beneficiario"));
                fila.addProperty("secuencia", rs.getString("secuencia"));
                fila.addProperty("otro_si", rs.getString("otro_si"));
                fila.addProperty("valor_prima", rs.getString("valor_prima"));
                fila.addProperty("valor_otros_gastos", rs.getString("valor_otros_gastos"));
                fila.addProperty("valor_iva", rs.getString("valor_iva"));
                fila.addProperty("valor_total", rs.getString("total"));
                fila.addProperty("cotiz_broker_aceptada", rs.getString("cotiz_broker_aceptada"));
                fila.addProperty("cambio", rs.getString("cambio"));
                fila.addProperty("broker", rs.getString("broker"));
                arr.add(fila);
            }
            obj.add("rows", arr);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return gson.toJson(obj);
        }
    }

    @Override
    public String cargarDetalleCotizacionAseguradora(String num_contrato, String id_aseguradora, String id_beneficiario, String secuencia) {
        Gson gson = new Gson();
        con = null;
        ps = null;
        rs = null;
        JsonObject obj = new JsonObject();
        query = "SQL_CARGAR_DETALLE_COTIZACION_ASEGURADORA";
        String filtro = "";

        JsonArray arr = new JsonArray();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query).replaceAll("#filtro", filtro));
            ps.setString(1, num_contrato);
            ps.setString(2, id_aseguradora);
            ps.setString(3, id_beneficiario);
            ps.setString(4, secuencia);
            rs = ps.executeQuery();
            JsonObject fila = null;
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("id", rs.getString("id"));
                fila.addProperty("nombre_poliza", rs.getString("nombre_poliza"));
                fila.addProperty("porcentaje_poliza", rs.getString("porcentaje_poliza"));
                fila.addProperty("valor_poliza", rs.getString("valor_poliza"));
                fila.addProperty("porcentaje_aseguradora", rs.getString("porcentaje_aseguradora"));
                fila.addProperty("valor_aseguradora", rs.getString("valor_aseguradora"));
                arr.add(fila);
            }
            obj.add("rows", arr);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return gson.toJson(obj);
        }
    }

    @Override
    public String enviarCotizacionBroker(String num_contrato, Usuario usuario) {
        con = null;
        ps = null;
        query = "SQL_UPDATE_COTIZADO_BROKER";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, num_contrato);
            ps.setString(2, usuario.getLogin());

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
            throw new SQLException("ERROR enviarCotizacionBroker \n" + e.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    public String rechazaCotizacionesBroker(String num_contrato, String id_aseguradora, String id_beneficiario, String secuencia, Usuario usuario) throws Exception {

        st = null;
        String respuesta = "";
        query = "SQL_RECHAZA_COTIZ_BROKER";
        try {

            st = new StringStatement(this.obtenerSQL(query).replaceAll("#cond", " AND id_aseguradora not in('" + id_aseguradora + "') AND id_beneficiario ='" + id_beneficiario + "' AND secuencia_otro_si=" + secuencia), true);
            st.setString(1, usuario.getLogin());
            st.setString(2, num_contrato);
            respuesta = st.getSql();

        } catch (Throwable e) {
            e.printStackTrace();
            System.out.println(e.toString());
            throw new SQLException("ERROR DURANTE rechazaCotizacionesBroker. \n " + e.getMessage());
        }
        return respuesta;

    }

    public String aceptaCotizacionBroker(String num_contrato, String id_aseguradora, String id_beneficiario, String secuencia, Usuario usuario) throws Exception {

        st = null;
        String respuesta = "";
        query = "SQL_UPDATE_ACEPTADO_COTIZ_BROKER";
        try {

            st = new StringStatement(this.obtenerSQL(query), true);
            st.setString(1, usuario.getLogin());
            st.setString(2, num_contrato);
            st.setInt(3, Integer.parseInt(id_aseguradora));
            st.setInt(4, Integer.parseInt(id_beneficiario));
            st.setInt(5, Integer.parseInt(secuencia));
            respuesta = st.getSql();

        } catch (Throwable e) {
            e.printStackTrace();
            System.out.println(e.toString());
            throw new SQLException("ERROR DURANTE aceptaCotizacionBroker. \n " + e.getMessage());
        }
        return respuesta;

    }

    @Override
    public String seleccionarCotizacionBroker(String num_contrato, String id_aseguradora, String id_beneficiario, String secuencia, Usuario usuario) {

        String respuestaJson = "{}";
        TransaccionService stmt = new TransaccionService(usuario.getBd());
        try {

            stmt.crearStatement();
            stmt.getSt().addBatch(rechazaCotizacionesBroker(num_contrato, id_aseguradora, id_beneficiario, secuencia, usuario));
            stmt.getSt().addBatch(aceptaCotizacionBroker(num_contrato, id_aseguradora, id_beneficiario, secuencia, usuario));
            stmt.getSt().addBatch(habilitarCotizacionBroker(num_contrato));

            stmt.execute();
            stmt.closeAll();
            respuestaJson = "{\"respuesta\":\"OK\"}";
        } catch (Exception e) {
            e.printStackTrace();
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                stmt.closeAll();
            } catch (Exception ex) {
                Logger.getLogger(MinutasContratacionImpl.class.getName()).log(Level.SEVERE, null, ex);
            }

            return respuestaJson;
        }

    }

    public String actualizarCedulaRepLegal(String nitCliente, String cedula_rep) throws Exception {

        String respuesta = "";
        st = null;
        query = "SQL_ACTUALIZA_INFO_REPLEGAL";

        try {

            st = new StringStatement(this.obtenerSQL(query), true);

            st.setString(1, cedula_rep);
            st.setString(2, nitCliente);
            respuesta = st.getSql();

        } catch (Throwable e) {
            e.printStackTrace();
            System.out.println(e.toString());
            throw new Exception("ERROR DURANTE guardarProveedor. \n " + e.getMessage());
        }
        return respuesta;
    }

    @Override
    public String anularGarantiaMinuta(String id, Usuario usuario) {
        con = null;
        ps = null;
        query = "anularGarantia";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, usuario.getLogin());
            ps.setInt(2, Integer.parseInt(id));

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
            throw new SQLException("ERROR anularGarantiaMinuta \n" + e.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    public String insertarCotizAseguradoraOtrosCostos(String num_contrato, String id_aseguradora, String id_beneficiario, String secuencia, String valor_otros_gastos, String porc_iva,
            String valor_iva, Usuario usuario) throws Exception {
        String respuesta = "";
        st = null;
        query = "insertarCotizAseguradoraOtrosCostos";
        try {
            st = new StringStatement(this.obtenerSQL(query), true);
            st.setString(1, num_contrato);
            st.setString(2, id_aseguradora);
            st.setDouble(3, Double.parseDouble(valor_otros_gastos));
            st.setDouble(4, Double.parseDouble(porc_iva));
            st.setDouble(5, Double.parseDouble(valor_iva));
            st.setString(6, usuario.getLogin());
            st.setString(7, usuario.getDstrct());
            st.setString(8, id_beneficiario);
            st.setString(9, secuencia);
            respuesta = st.getSql();
        } catch (Throwable e) {
            e.printStackTrace();
            throw new Exception("ERROR DURANTE insertarCotizAseguradoraOtrosCostos. \n " + e.getMessage());
        }
        return respuesta;

    }

    public String actualizarCotizAseguradoraOtrosCostos(String num_contrato, String id_aseguradora, String id_beneficiario, String valor_otros_gastos, String porc_iva, String valor_iva, Usuario usuario) throws Exception {
        st = null;
        String respuesta = "";
        query = "actualizarCotizAseguradoraOtrosCostos";

        try {

            st = new StringStatement(this.obtenerSQL(query), true);
            st.setDouble(1, Double.parseDouble(valor_otros_gastos));
            st.setDouble(2, Double.parseDouble(porc_iva));
            st.setDouble(3, Double.parseDouble(valor_iva));
            st.setString(4, usuario.getLogin());
            st.setString(5, num_contrato);
            st.setString(6, id_aseguradora);
            st.setString(7, id_beneficiario);

            respuesta = st.getSql();

        } catch (Throwable e) {
            e.printStackTrace();
            System.out.println(e.toString());
            throw new SQLException("ERROR DURANTE actualizarCotizAseguradoraOtrosCostos. \n " + e.getMessage());
        }

        return respuesta;

    }

    public boolean existeOtrosCostosAseguradora(String num_contrato, String id_aseguradora, String id_beneficiario, String secuencia) {
        con = null;
        rs = null;
        ps = null;

        query = "SQL_OBTENER_OTROS_COSTOS_ASEGURADORAS";
        boolean resp = false;

        Gson gson = new Gson();
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);
            ps.setString(1, num_contrato);
            ps.setString(2, id_aseguradora);
            ps.setString(3, id_beneficiario);
            ps.setString(4, secuencia);
            rs = ps.executeQuery();
            while (rs.next()) {
                resp = true;
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return resp;
    }

    @Override
    public String getInfoOtrosCostosAseguradora(String num_solicitud, String id_aseguradora, String id_beneficiario, String secuencia) {
        con = null;
        rs = null;
        ps = null;

        query = "SQL_OBTENER_OTROS_COSTOS_ASEGURADORAS";
        String respuestaJson = "{}";

        Gson gson = new Gson();
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);
            ps.setString(1, num_solicitud);
            ps.setString(2, id_aseguradora);
            ps.setString(3, id_beneficiario);
            ps.setString(4, secuencia);

            rs = ps.executeQuery();
            JsonObject fila = new JsonObject();
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("valor_otros_gastos", rs.getString("valor_otros_gastos"));
                fila.addProperty("valor_iva", rs.getString("valor_iva"));
            }
            respuestaJson = gson.toJson(fila);

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return respuestaJson;

    }

    @Override
    public String generarCxPAseguradora(String num_factura, String num_contrato, String[] x, String valor_gastos, String concepto_iva, String usuario) {
        con = null;
        ps = null;
        rs = null;
        query = "SQL_GENERAR_CXP_ASEGURADORA";
        String retorno = "";
        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);
            ps.setString(1, num_factura);
            ps.setString(2, num_contrato);
            ps.setArray(3, con.createArrayOf("text", x));
            ps.setDouble(4, Double.parseDouble(valor_gastos));
            ps.setString(5, concepto_iva);
            ps.setString(6, usuario);
            rs = ps.executeQuery();
            if (rs.next()) {
                retorno = rs.getString("retorno");
            }

        } catch (Exception e) {
            retorno = "ERROR";
            e.printStackTrace();
            System.out.println(e.toString());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
        }
        return retorno;
    }

    @Override
    public String getInfoAseguradoraAceptada(String num_contrato) {
        con = null;
        rs = null;
        ps = null;

        query = "SQL_OBTENER_ASEGURADORA_COTIZ_ACEPTADA";
        String respuestaJson = "{}";

        Gson gson = new Gson();
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);
            ps.setString(1, num_contrato);

            rs = ps.executeQuery();
            JsonObject fila = new JsonObject();
            while (rs.next()) {
                fila.addProperty(rs.getString("id_aseguradora"), rs.getString("nombre_seguro"));
            }
            respuestaJson = gson.toJson(fila);

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return respuestaJson;
    }

    @Override
    public String cargarCotizacionPendienteCxPAseguradora(String num_contrato, String id_aseguradora, String id_beneficiario, String secuencia) {
        Gson gson = new Gson();
        con = null;
        ps = null;
        rs = null;
        JsonObject obj = new JsonObject();
        query = "SQL_CARGAR_DETALLE_COTIZACION_ASEGURADORA";
        String filtro = " AND cxp_generada = 'N'";

        JsonArray arr = new JsonArray();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query).replaceAll("#filtro", filtro));
            ps.setString(1, num_contrato);
            ps.setString(2, id_aseguradora);
            ps.setString(3, id_beneficiario);
            ps.setString(4, secuencia);
            rs = ps.executeQuery();
            JsonObject fila = null;
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("id", rs.getString("id"));
                fila.addProperty("nombre_poliza", rs.getString("nombre_poliza"));
                fila.addProperty("porcentaje_poliza", rs.getString("porcentaje_poliza"));
                fila.addProperty("valor_poliza", rs.getString("valor_poliza"));
                fila.addProperty("porcentaje_aseguradora", rs.getString("porcentaje_aseguradora"));
                fila.addProperty("valor_aseguradora", rs.getString("valor_aseguradora"));
                arr.add(fila);
            }
            obj.add("rows", arr);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return gson.toJson(obj);
        }
    }

    @Override
    public String listarBeneficiariosPoliza(String excluirId) {
        con = null;
        rs = null;
        ps = null;

        query = "SQL_CARGAR_BENEFICIARIOS_POLIZA";
        String respuestaJson = "{}";
        String filtro = (!excluirId.equals("")) ? " AND id not in(" + excluirId + ")" : "";

        Gson gson = new Gson();
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query).replaceAll("#filtro", filtro);

            ps = con.prepareStatement(query);

            rs = ps.executeQuery();
            JsonObject fila = new JsonObject();
            while (rs.next()) {
                fila.addProperty(rs.getString("id"), rs.getString("valor"));
            }
            respuestaJson = gson.toJson(fila);

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return respuestaJson;
    }

    @Override
    public String listarCausalesPoliza() {
        con = null;
        rs = null;
        ps = null;

        query = "SQL_CARGAR_CAUSALES_POLIZA";
        String respuestaJson = "{}";

        Gson gson = new Gson();
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);

            rs = ps.executeQuery();
            JsonObject fila = new JsonObject();
            while (rs.next()) {
                fila.addProperty(rs.getString("id"), rs.getString("valor"));
            }
            respuestaJson = gson.toJson(fila);

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return respuestaJson;
    }

    @Override
    public String cargarCboBeneficiarioCxP(String num_contrato) {
        con = null;
        rs = null;
        ps = null;

        query = "SQL_OBTENER_BENEFICIARIOS_COTIZ_ACEPTADA";
        String respuestaJson = "{}";

        Gson gson = new Gson();
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);
            ps.setString(1, num_contrato);

            rs = ps.executeQuery();
            JsonObject fila = new JsonObject();
            while (rs.next()) {
                fila.addProperty(rs.getString("id_beneficiario"), rs.getString("nombre"));
            }
            respuestaJson = gson.toJson(fila);

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return respuestaJson;
    }

    public String habilitarCotizacionBroker(String num_contrato) throws Exception {

        st = null;
        String respuesta = "";
        query = "SQL_HABILITA_COTIZ_BROKER";
        try {

            st = new StringStatement(this.obtenerSQL(query), true);
            st.setString(1, num_contrato);
            respuesta = st.getSql();

        } catch (Throwable e) {
            e.printStackTrace();
            System.out.println(e.toString());
            throw new SQLException("ERROR DURANTE habilitarCotizacionBroker. \n " + e.getMessage());
        }
        return respuesta;

    }

    @Override
    public String listarBeneficiariosContrato(String num_contrato) {
        con = null;
        rs = null;
        ps = null;

        query = "SQL_CARGAR_BENEFICIARIOS_CONTRATO";
        String respuestaJson = "{}";

        Gson gson = new Gson();
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);
            ps.setString(1, num_contrato);
            rs = ps.executeQuery();
            JsonObject fila = new JsonObject();
            while (rs.next()) {
                fila.addProperty(rs.getString("id"), rs.getString("nombre"));
            }
            respuestaJson = gson.toJson(fila);

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return respuestaJson;
    }

    @Override
    public boolean existeBrokerRelacionado(String num_contrato, String id_beneficiario) {
        con = null;
        rs = null;
        ps = null;

        query = "SQL_OBTENER_BROKER_RELACIONADOS_CONTRATO";
        boolean resp = false;

        Gson gson = new Gson();
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);
            ps.setString(1, num_contrato);
            ps.setInt(2, Integer.parseInt(id_beneficiario));
            rs = ps.executeQuery();
            while (rs.next()) {
                resp = true;
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return resp;
    }

    @Override
    public String listarBeneficiariosAsignadosBroker(String num_contrato, Usuario usuario) {
        con = null;
        rs = null;
        ps = null;

        query = "SQL_OBTENER_TIPOS_ASIGNADOS_BROKER";
        String respuestaJson = "{}";

        Gson gson = new Gson();
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);
            ps.setString(1, num_contrato);
            ps.setString(2, usuario.getLogin());
            rs = ps.executeQuery();
            JsonObject fila = new JsonObject();
            while (rs.next()) {
                fila.addProperty(rs.getString("id"), rs.getString("nombre"));
            }
            respuestaJson = gson.toJson(fila);

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return respuestaJson;
    }

    @Override
    public boolean cotizacionPendienteBroker(String num_contrato, Usuario usuario) {
        con = null;
        rs = null;
        ps = null;

        query = "SQL_OBTENER_COTIZACION_PENDIENTE_BROKER";
        boolean resp = false;

        Gson gson = new Gson();
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);
            ps.setString(1, num_contrato);
            ps.setString(2, usuario.getLogin());
            rs = ps.executeQuery();
            while (rs.next()) {
                resp = true;
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return resp;
    }

    public String actualizarCotizadoBroker(String num_contrato, String id_beneficiario, Usuario usuario) throws Exception {
        st = null;
        String respuesta = "";
        query = "actualizarCotizadoBroker";

        try {

            st = new StringStatement(this.obtenerSQL(query), true);
            st.setString(1, usuario.getLogin());
            st.setString(2, num_contrato);
            st.setInt(3, Integer.parseInt(id_beneficiario));

            respuesta = st.getSql();

        } catch (Throwable e) {
            e.printStackTrace();
            System.out.println(e.toString());
            throw new SQLException("ERROR DURANTE actualizarCotizadoBroker. \n " + e.getMessage());
        }

        return respuesta;

    }

    @Override
    public String cargarOtrosSi(String num_contrato) {
        Gson gson = new Gson();
        con = null;
        ps = null;
        rs = null;
        JsonObject obj = new JsonObject();
        String query = "cargarOtrosSi";
        String filtro = "";
        JsonArray arr = new JsonArray();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query).replaceAll("#filtro", filtro));
            ps.setString(1, num_contrato);
            rs = ps.executeQuery();
            JsonObject fila = null;
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("id", rs.getString("id"));
                fila.addProperty("descripcion", rs.getString("descripcion"));
                fila.addProperty("id_causal", rs.getString("id_causal"));
                fila.addProperty("causal", rs.getString("causal"));
                fila.addProperty("fecha_creacion", rs.getString("fecha_creacion"));
                fila.addProperty("usuario_crea", rs.getString("usuario_crea"));
                fila.addProperty("valor_otro_si", rs.getString("valor_otro_si"));
                arr.add(fila);
            }
            obj.add("rows", arr);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return gson.toJson(obj);
        }
    }

    @Override
    public String insertarGarantiaOtroSi(String num_contrato, Usuario usuario) {
        String respuestaJson = "{}";
        TransaccionService stmt = new TransaccionService(usuario.getBd());

        try {
            stmt.crearStatement();

            stmt.getSt().addBatch(insertarOtroSi(num_contrato, usuario));
            stmt.getSt().addBatch(actualizarCotizadoBroker(num_contrato, "1", usuario));
            stmt.getSt().addBatch(habilitarCotizacionBroker(num_contrato));

            stmt.execute();
            stmt.closeAll();
            respuestaJson = "{\"respuesta\":\"OK\"}";
        } catch (Exception e) {
            e.printStackTrace();
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                stmt.closeAll();
            } catch (Exception ex) {
                Logger.getLogger(MinutasContratacionImpl.class.getName()).log(Level.SEVERE, null, ex);
            }

            return respuestaJson;
        }
    }

    public String insertarOtroSi(String num_contrato, Usuario usuario) throws Exception {
        String respuesta = "";
        st = null;
        query = "insertarGarantiaOtroSi";
        try {
            st = new StringStatement(this.obtenerSQL(query), true);
            st.setString(1, usuario.getLogin());
            st.setString(2, num_contrato);
            respuesta = st.getSql();
        } catch (Throwable e) {
            e.printStackTrace();
            throw new Exception("ERROR DURANTE insertarGarantias. \n " + e.getMessage());
        }
        return respuesta;
    }

    @Override
    public String guardarOtroSi(String num_contrato, Usuario usuario, JsonObject garantias_otro_si) {
        String respuestaJson = "{}";
        JsonObject respuesta = new JsonObject();
        boolean swHabilitaCot = false;
        TransaccionService stmt = new TransaccionService(usuario.getBd());
        try {

            stmt.crearStatement();

            //Insertamos garantias otro si
            JsonArray arrGarantiasOtroSi = garantias_otro_si.getAsJsonArray("garantiasOtroSi");
            for (int i = 0; i < arrGarantiasOtroSi.size(); i++) {
                respuesta = arrGarantiasOtroSi.get(i).getAsJsonObject();
                String id_poliza = respuesta.get("id_poliza").getAsString();
                String id_beneficiario = respuesta.get("id_beneficiario").getAsString();
                String tipo_entrada = respuesta.get("id_tipo_entrada").getAsString();
                String valor_base = respuesta.get("valor_base").getAsString();
                String porc_poliza = respuesta.get("porcentaje_poliza").getAsString();
                String valor_poliza = respuesta.get("valor_poliza").getAsString();
                String vigencia_poliza = respuesta.get("vigencia_poliza").getAsString();
                String id_causal = respuesta.get("id_causal").getAsString();
                String secuencia = respuesta.get("secuencia").getAsString();
                if (respuesta.get("id").getAsString().startsWith("neo_")) {
                    swHabilitaCot = true;
                    stmt.getSt().addBatch(insertarGarantias(num_contrato, id_poliza, id_beneficiario, tipo_entrada, valor_base, porc_poliza, valor_poliza, vigencia_poliza, id_causal, "S", "S", secuencia, usuario));
                    stmt.getSt().addBatch(actualizarCotizadoBroker(num_contrato, id_beneficiario, usuario));
                } else {
                    stmt.getSt().addBatch(actualizarGarantias(respuesta.get("id").getAsString(), id_poliza, id_beneficiario, tipo_entrada, valor_base, porc_poliza, valor_poliza, vigencia_poliza, id_causal, usuario));
                }
            }

            if (swHabilitaCot) {
                stmt.getSt().addBatch(habilitarCotizacionBroker(num_contrato));
            }
            stmt.execute();
            stmt.closeAll();
            respuestaJson = "{\"respuesta\":\"OK\"}";
        } catch (Exception e) {
            e.printStackTrace();
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                stmt.closeAll();
            } catch (Exception ex) {
                Logger.getLogger(MinutasContratacionImpl.class.getName()).log(Level.SEVERE, null, ex);
            }

            return respuestaJson;
        }
    }

    @Override
    public String anularOtroSi(String num_contrato, String secuencia, Usuario usuario) {
        con = null;
        ps = null;
        query = "anularOtroSi";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, usuario.getLogin());
            ps.setString(2, num_contrato);
            ps.setInt(3, Integer.parseInt(secuencia));

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
            throw new SQLException("ERROR anularOtroSi \n" + e.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    @Override
    public String cargarCboOtrosSiBeneficiario(String num_contrato, String id_beneficiario) {
        con = null;
        rs = null;
        ps = null;

        query = "cargarOtrosSi";
        String respuestaJson = "{}";
        String filtro = " AND id_beneficiario =" + id_beneficiario;
        Gson gson = new Gson();
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query).replaceAll("#filtro", filtro);

            ps = con.prepareStatement(query);
            ps.setString(1, num_contrato);

            rs = ps.executeQuery();
            JsonObject fila = new JsonObject();
            while (rs.next()) {
                fila.addProperty(rs.getString("id"), rs.getString("descripcion"));
            }
            respuestaJson = gson.toJson(fila);

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return respuestaJson;
    }

    @Override
    public String cargarCboOtrosSiCxP(String num_contrato) {
        con = null;
        rs = null;
        ps = null;

        query = "SQL_OBTENER_OTROS_SI_PENDIENTES_CXP";
        String respuestaJson = "{}";

        Gson gson = new Gson();
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);
            ps.setString(1, num_contrato);

            rs = ps.executeQuery();
            JsonObject fila = new JsonObject();
            while (rs.next()) {
                fila.addProperty(rs.getString("id"), rs.getString("descripcion"));
            }
            respuestaJson = gson.toJson(fila);

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return respuestaJson;
    }

    /*Esto es una funcion temporal para marcar que ya fue generado el contrato para la solicitud dada*/
    public String actualizarTrazabilidadSolicitud(String id_solicitud, Usuario usuario) throws Exception {

        String respuesta = "";
        st = null;
        query = "SQL_ACTUALIZA_TRAZABILIDAD_OFERTA";

        try {

            st = new StringStatement(this.obtenerSQL(query), true);
            st.setString(1, usuario.getLogin());
            st.setString(2, id_solicitud);
            respuesta = st.getSql();

        } catch (Throwable e) {
            e.printStackTrace();
            System.out.println(e.toString());
            throw new Exception("ERROR DURANTE actualizarTrazabilidadSolicitud. \n " + e.getMessage());
        }
        return respuesta;
    }
    
    public String actualizarCotizacionOferta(String id_solicitud, JsonObject formaPago, Usuario usuario) throws Exception {

        String respuesta = "";
        st = null;
        query = (perteneceNuevoModulo("1",id_solicitud)) ? "SQL_ACTUALIZA_COTIZACION_OFERTA":"SQL_ACTUALIZA_COTIZACION_OFERTA_TEM";

        try {

            st = new StringStatement(this.obtenerSQL(query), true);

            st.setString(1, formaPago.get("anticipo").getAsJsonPrimitive().getAsString());
            st.setDouble(2, formaPago.get("porc_anticipo").getAsJsonPrimitive().getAsDouble());
            st.setDouble(3, formaPago.get("valor_anticipo").getAsJsonPrimitive().getAsDouble());
            st.setString(4, formaPago.get("retegarantia").getAsJsonPrimitive().getAsString());
            st.setDouble(5, formaPago.get("porc_retegarantia").getAsJsonPrimitive().getAsDouble());                     
            st.setString(6, usuario.getLogin());         
            st.setString(7, id_solicitud);
            respuesta = st.getSql();

        } catch (Throwable e) {
            e.printStackTrace();
            System.out.println(e.toString());
            throw new Exception("ERROR DURANTE actualizarCotizacionOferta. \n " + e.getMessage());
        }
        return respuesta;
    }
    
    public boolean perteneceNuevoModulo(String op, String id) {
        con = null;
        rs = null;
        ps = null;

        query = "SQL_OBTENER__NUEVO_MODULO_OFERTA";
        String filtro = (op.equals("1")) ? " WHERE id_solicitud = '"+id+"'":" WHERE id_solicitud IN (select id_solicitud from opav.acciones where id_accion = '"+id+"')";
        boolean resp = false;

        Gson gson = new Gson();
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query).replaceAll("#filtro", filtro);

            ps = con.prepareStatement(query);
          
            rs = ps.executeQuery();
            while (rs.next()) {
                if (rs.getString("nuevo_modulo").equals("1")){
                   resp = true;
                }
            }

        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return resp;
    }


}
