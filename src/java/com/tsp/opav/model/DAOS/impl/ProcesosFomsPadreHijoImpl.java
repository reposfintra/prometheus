/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.opav.model.DAOS.impl;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.lowagie.text.BadElementException;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.html.simpleparser.HTMLWorker;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfPageEventHelper;
import com.lowagie.text.pdf.PdfWriter;
import com.tsp.opav.model.DAOS.MainDAO;
import com.tsp.opav.model.DAOS.ProcesosFomsPadreHijoDAO;

import com.tsp.opav.model.beans.AccionesEca;
import com.tsp.opav.model.beans.OfertaElca;
import com.tsp.opav.model.beans.CotizacionSl;
import com.tsp.operation.model.beans.BeanGeneral;
import com.tsp.operation.model.beans.Cliente;
import com.tsp.operation.model.beans.SerieGeneral;
import com.tsp.opav.model.beans.NegocioApplus;
import com.tsp.operation.model.TransaccionService;
import com.tsp.operation.model.beans.BeansAdministracion;
import com.tsp.operation.model.beans.BeansMultiservicio;
import com.tsp.operation.model.beans.RMCantidadEnLetras;

/*Beans*/
import com.tsp.operation.model.beans.StringStatement;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.util.Util;
import java.awt.Color;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringReader;
////////////////
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ProcesosFomsPadreHijoImpl extends MainDAO implements ProcesosFomsPadreHijoDAO {

    private Connection con = null;
    private PreparedStatement ps = null;
    private ResultSet rs = null;
    private String query = "";
    private StringStatement st = null;
    private List listaInfo;

    public ProcesosFomsPadreHijoImpl(String dataBaseName) {
        super("ProcesosFomsPadreHijoDAO.xml", dataBaseName);
    }

    @Override
    public String cargarInfoSolicitudes(String solicitud, String foms) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_INFORMACION_SOLICITUDES";
        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            String consulta = "";
            consulta = this.obtenerSQL(query).replaceAll("#parametro1", solicitud).replaceAll("#parametro2", foms);
            ps = con.prepareStatement(consulta);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }
            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return informacion;
        }
    }

    @Override
    public String cargarFomsDispibles(String solicitud) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_FOMS_DISPONIBLES";
        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, solicitud);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }
            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return informacion;
        }
    }

    @Override
    public String cargarFomsRelacionados(String solicitud) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_FOMS_RELACIONADOS";
        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, solicitud);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }
            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return informacion;
        }
    }

    @Override
    public String asociarFoms(String solicitud, String lista, Usuario usuario) {
        con = null;
        Statement stmt = null;
        
        JsonObject json_res = new JsonObject();
        
        JsonArray detalle_json = new JsonParser().parse(lista).getAsJsonObject().getAsJsonArray("id_solicitudes_a_relacionar");
        
        try {
            con = this.conectarJNDI();
            con.setAutoCommit(false);
            stmt = con.createStatement();

             
            if (existeFomsHijo(solicitud) == 0) {
                stmt.addBatch(InsertarFomsHijo(solicitud , solicitud , usuario.getLogin()));
            }
            String id_solcitud_hijo = "";
            for (int i = 0; i < detalle_json.size(); i++) {

                id_solcitud_hijo = detalle_json.get(i).getAsJsonObject().get("id_solicitud").getAsString();
                stmt.addBatch(InsertarFomsHijo(solicitud , id_solcitud_hijo , usuario.getLogin()));
            }
            
            stmt.executeBatch();
            stmt.clearBatch();
            con.commit();

            json_res.addProperty("status", "200");

        } catch (Exception e) {
            json_res.addProperty("error", e.getMessage());
            try {
                con.rollback();
            } catch (SQLException ex) {
                Logger.getLogger(ComprasProcesoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException e) {
                        throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                    }
                }
                if (con != null) {
                    try {
                        con.setAutoCommit(true);
                        this.desconectar(con);
                    } catch (SQLException e) {
                        throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage());
                    }
                }
            } catch (Exception ex) {
            }
            return json_res.toString();
        }

    }
    
    private int existeFomsHijo(String id_solicitud ) {
        Connection con = null;
        ResultSet rs = null;
        PreparedStatement ps = null;

        String query = "SQL_EXISTE_FOMS_HIJO";
        String respuestaJson = "{}";
        int count = 0;

        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, id_solicitud);
            
            System.out.print(ps);

            rs = ps.executeQuery();

            while (rs.next()) {
                count = Integer.parseInt(rs.getString(rs.getMetaData().getColumnLabel(1)));
            }

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return count;
    }
    
    
    private String InsertarFomsHijo(String id_solicitud_padre, String id_solicitud_hijo, String usuario) throws Exception {

        String cadena = "";
        String query = "SQL_INSERTAR_FOMS_HIJO";
        StringStatement st = new StringStatement(this.obtenerSQL(query), true);

        try {

            st.setString(1, id_solicitud_padre);
            st.setString(2, id_solicitud_hijo);
            st.setString(3, usuario);

            cadena += st.getSql();

        } catch (Exception e) {
            throw new Exception("Error : Existio problemas al construir el insert  SQL_INSERTAR_FOMS_HIJO");
        }

        return cadena;

    }

    @Override
    public String desasociarFoms(String solicitud, String lista, Usuario usuario) {
        con = null;
        Statement stmt = null;
        
        JsonObject json_res = new JsonObject();
        
        JsonArray detalle_json = new JsonParser().parse(lista).getAsJsonObject().getAsJsonArray("id_solicitudes_a_desasociar");
        
        try {
            con = this.conectarJNDI();
            con.setAutoCommit(false);
            stmt = con.createStatement();
            
            String id_solcitud_hijo = "";
            for (int i = 0; i < detalle_json.size(); i++) {

                id_solcitud_hijo = detalle_json.get(i).getAsJsonObject().get("id_solicitud").getAsString();
                stmt.addBatch(eliminarFomsHijo(solicitud , id_solcitud_hijo , usuario.getLogin()));
            }
            
            stmt.executeBatch();
            stmt.clearBatch();
            con.commit();

            json_res.addProperty("status", "200");

        } catch (Exception e) {
            json_res.addProperty("error", e.getMessage());
            try {
                con.rollback();
            } catch (SQLException ex) {
                Logger.getLogger(ComprasProcesoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException e) {
                        throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                    }
                }
                if (con != null) {
                    try {
                        con.setAutoCommit(true);
                        this.desconectar(con);
                    } catch (SQLException e) {
                        throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage());
                    }
                }
            } catch (Exception ex) {
            }
            return json_res.toString();
        }
    }
    
    
    private String eliminarFomsHijo(String id_solicitud_padre, String id_solicitud_hijo, String usuario) throws Exception {

        String cadena = "";
        String query = "SQL_ELIMINAR_FOMS_HIJO";
        StringStatement st = new StringStatement(this.obtenerSQL(query), true);

        try {

            st.setString(1, id_solicitud_padre);
            st.setString(2, id_solicitud_hijo);            

            cadena += st.getSql();

        } catch (Exception e) {
            throw new Exception("Error : Existio problemas al construir el delete  SQL_ELIMINAR_FOMS_HIJO");
        }

        return cadena;

    }

}
