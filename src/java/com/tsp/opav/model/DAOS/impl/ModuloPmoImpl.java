/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.opav.model.DAOS.impl;

/**
 *
 * @author Ing.William Siado T
 */
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.lowagie.text.BadElementException;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.html.simpleparser.HTMLWorker;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfPageEventHelper;
import com.lowagie.text.pdf.PdfWriter;
import com.sun.xml.rpc.processor.modeler.j2ee.xml.string;
import com.tsp.opav.model.DAOS.MainDAO;
import com.tsp.opav.model.DAOS.ModuloEjecucionDAO;
import com.tsp.opav.model.DAOS.ModuloPmoDAO;
import com.tsp.operation.model.beans.BeansAdministracion;


/*Beans*/
import com.tsp.operation.model.beans.StringStatement;
import com.tsp.operation.model.beans.Usuario;
import java.awt.Color;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
////////////////

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ModuloPmoImpl extends MainDAO implements ModuloPmoDAO {

    private Connection con = null;
    private PreparedStatement ps = null;
    private ResultSet rs = null;
    private String query = "";
    private StringStatement st = null;

    public ModuloPmoImpl(String dataBaseName) {
        super("ModuloPmoDAO.xml", dataBaseName);
    }

    @Override
    public String cargar_Wbs_Ejecucion(String id_solicitud) {
        Gson gson = new GsonBuilder().serializeNulls().create();
        con = null;
        ps = null;
        rs = null;
        JsonObject objAreas = new JsonObject();
        JsonObject objDisciplinas = new JsonObject();
        JsonObject objCapitulos = new JsonObject();
        query = "SQL_GET_NODO_RAIZ";
        String areas_proyecto = "", disciplinas_area = "", capitulos = "";
        String respuestaJson = "{}";
        JsonArray arr = new JsonArray();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, id_solicitud);
            rs = ps.executeQuery();
            JsonObject fila = null;
            int contador = 1, indice = 1, pindex = 0;
            String padre = "";
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("id", contador);
                fila.addProperty("parent", (rs.getString("padre")));
                fila.addProperty("title", rs.getString("title"));
                fila.addProperty("groupTitle", rs.getString("grouptitle"));
                fila.addProperty("groupTitleColor", rs.getString("groupTitleColor"));
                fila.addProperty("description", rs.getString("descripcion"));
                fila.addProperty("image", "/fintra/images/botones/iconos/Project.png");
                fila.addProperty("nivel", rs.getString("nivel"));
                fila.addProperty("recordId", rs.getString("id_solicitud"));
                fila.addProperty("otro", "");
                fila.addProperty("buttonsType", "Proyecto");
                arr.add(fila);
                contador++;
            }
            respuestaJson = "{\"total\":\"" + contador + "\",\"nodes\":" + gson.toJson(arr) + "}";
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return respuestaJson;
        }
    }

}
