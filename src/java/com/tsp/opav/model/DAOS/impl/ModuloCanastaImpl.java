/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.opav.model.DAOS.impl;

/**
 *
 * @author Ing.William Siado T
 */
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.lowagie.text.BadElementException;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.html.simpleparser.HTMLWorker;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfPageEventHelper;
import com.lowagie.text.pdf.PdfWriter;
import com.sun.org.apache.xalan.internal.xsltc.compiler.Constants;
import com.tsp.opav.model.DAOS.MainDAO;
import com.tsp.opav.model.DAOS.ModuloCanastaDAO;

/*Beans*/
import com.tsp.operation.model.beans.StringStatement;
import com.tsp.operation.model.beans.Usuario;
import java.awt.Color;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Array;
import java.sql.CallableStatement;
////////////////

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ModuloCanastaImpl extends MainDAO implements ModuloCanastaDAO {

    private Connection con = null;
    private PreparedStatement ps = null;
    private ResultSet rs = null;
    private String query = "";
    private StringStatement st = null;

    public ModuloCanastaImpl(String dataBaseName) {
        super("ModuloCanastaDAO.xml", dataBaseName);
    }

    @Override
    public String cargarComboGenerico(String op, String param) {
        con = null;
        rs = null;
        ps = null;
        String filtro = "";
        switch (op) {
            case "1":
                query = "SQL_CARGAR_AREAS_PROYECTO";
                filtro = " AND id_solicitud = " + param;
                break;
            case "2":
                query = "SQL_CARGAR_DISCIPLINAS_AREAS";
                filtro = (!param.equals("")) ? " AND id_area_proyecto = " + param : "";
                break;
            case "3":
                query = "SQL_CARGAR_CAPITULOS_DISCIPLINA";
                filtro = (!param.equals("")) ? " AND id_disciplina_area = " + param : "";
                break;
            case "4":
                query = "SQL_CARGAR_ACTIVIDADES_CAPITULO";
                filtro = (!param.equals("")) ? " AND id_capitulo = " + param : "";
                break;
            case "5":
                query = "SQL_CARGAR_CAUSALES";
                filtro = (!param.equals("")) ? " AND id_capitulo = " + param : "";
                break;
            default:
                throw new AssertionError();
        }

        String respuestaJson = "{}";

        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query).replaceAll("#filtro", filtro);

            ps = con.prepareStatement(query);

            rs = ps.executeQuery();

            while (rs.next()) {
                obj.addProperty(rs.getString("id"), rs.getString("descripcion"));

            }
            respuestaJson = gson.toJson(obj);

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return respuestaJson;
    }

    @Override
    public String cargar_Proyecto_Ejecucion(String id_solicitud, String proceso, String opc, String cond, Usuario usuario) {
        String condicion2 = "", condicion3 = "";

        condicion3 = "";// (id_lote.equals("")) ? "" : " AND GA.id_lote_ejecucion !=  " + id_lote;

        switch (proceso) {

            case "2":
                //condicion2 = " AND cantidad_apu_actual>0 ";
                break;

            default:
                break;
        }

        switch (opc) {

            case "1":
                cond = " AND A.id_area = " + cond;
                break;

            case "2":
                cond = " AND A.id_disciplina_area = " + cond;
                break;

            case "3":
                cond = " AND A.id_capitulo = " + cond;
                break;

            case "4":
                cond = " AND A.id_actividades_capitulo = " + cond;
                break;
            default:
                break;
        }
        String consulta;
        Gson gson = new Gson();
        con = null;
        ps = null;
        rs = null;
        JsonObject obj = new JsonObject();
        String query = "SQL_CARGAR_PROYECTO_EJECUCION";

        JsonArray arr = new JsonArray();
        try {
            con = this.conectarJNDI();
            consulta = this.obtenerSQL(query).replaceAll("#condicion1", cond).replaceAll("#condicion2", condicion2).replaceAll("#condicion3", condicion3);
            ps = con.prepareStatement(consulta);
            ps.setString(1, id_solicitud);
            rs = ps.executeQuery();
            JsonObject fila = null;
            int id = 0;
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("id", id);
                fila.addProperty("id_disciplina_area", rs.getString("id_disciplina_area"));
                fila.addProperty("descripcion_area", rs.getString("descripcion_area"));
                fila.addProperty("id_disciplina", rs.getString("id_disciplina"));
                fila.addProperty("descripcion_displina", rs.getString("descripcion_displina"));
                fila.addProperty("id_capitulo", rs.getString("id_capitulo"));
                fila.addProperty("descripcion_capitulo", rs.getString("descripcion_capitulo"));
                fila.addProperty("id_actividad", rs.getString("id_actividad"));
                fila.addProperty("descripcion_actividad", rs.getString("descripcion_actividad"));
                fila.addProperty("id_actividades_capitulo", rs.getString("id_actividades_capitulo"));
                fila.addProperty("id_rel_actividades_apu", rs.getString("id_rel_actividades_apu"));
                fila.addProperty("id_apu", rs.getString("id_apu"));
                fila.addProperty("descripcion_apu", rs.getString("descripcion_apu"));
                fila.addProperty("unidad_medida_apu", rs.getString("unidad_medida_apu"));
                fila.addProperty("nombre_unidad_medida_apu", rs.getString("nombre_unidad_medida_apu"));
                fila.addProperty("cantidad_apu", rs.getString("cantidad_apu"));
                fila.addProperty("cantidad_apu_actual", rs.getString("cantidad_apu_actual"));
//                fila.addProperty("cantidad_apu_ejecutado", rs.getString("cantidad_apu_ejecutado"));
//                fila.addProperty("valor_apu_ejecutado", rs.getString("valor_apu_ejecutado"));
                fila.addProperty("valor_apu_presupuesto", rs.getString("valor_apu_presupuesto"));
                fila.addProperty("valor_apu_actual", rs.getString("valor_apu_actual"));
                fila.addProperty("total_apu_presupuesto", rs.getString("total_apu_presupuesto"));
                id++;
                arr.add(fila);
            }
            obj.add("rows", arr);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return gson.toJson(obj);
        }
    }

    @Override
    public String get_Apus_Wbs(String id_solicitud, String id_apu) {
        Gson gson = new Gson();
        con = null;
        ps = null;
        rs = null;
        JsonObject obj = new JsonObject();
        String query = "SQL_GET_APUS_WBS";

        JsonArray arr = new JsonArray();
        try {
            con = this.conectarJNDI();
            String consulta = this.obtenerSQL(query);
            ps = con.prepareStatement(consulta);
            ps.setString(1, id_solicitud);
            ps.setString(2, id_apu);
            rs = ps.executeQuery();
            JsonObject fila = null;

            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("id_disciplina_area", rs.getString("id_disciplina_area"));
                fila.addProperty("descripcion_area", rs.getString("descripcion_area"));
                fila.addProperty("id_disciplina", rs.getString("id_disciplina"));
                fila.addProperty("descripcion_displina", rs.getString("descripcion_displina"));
                fila.addProperty("id_capitulo", rs.getString("id_capitulo"));
                fila.addProperty("descripcion_capitulo", rs.getString("descripcion_capitulo"));
                fila.addProperty("id_actividad", rs.getString("id_actividad"));
                fila.addProperty("descripcion_actividad", rs.getString("descripcion_actividad"));
                fila.addProperty("id_actividades_capitulo", rs.getString("id_actividades_capitulo"));
                fila.addProperty("id_rel_actividades_apu", rs.getString("id_rel_actividades_apu"));
                fila.addProperty("id_apu", rs.getString("id_apu"));
                fila.addProperty("descripcion_apu", rs.getString("descripcion_apu"));
                fila.addProperty("unidad_medida_apu", rs.getString("unidad_medida_apu"));
                fila.addProperty("nombre_unidad_medida_apu", rs.getString("nombre_unidad_medida_apu"));
                //id++;
                arr.add(fila);
            }
            obj.add("rows", arr);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return gson.toJson(obj);
        }
    }

    @Override
    public String bloquer_Apus(JsonObject informacion, Usuario usuario) {
        con = null;
        ps = null;
        String respuestaJson = "{}";
        query = "SQL_LIBERAR_APUS";
        JsonObject json = new JsonObject();
        try {

            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);

            JsonArray arr = informacion.getAsJsonArray("apus_bloquear");
            String id_rel_actividades_apu_ = "", id_apus = "", coma = "";
            for (int i = 0; i < arr.size(); i++) {

                json = arr.get(i).getAsJsonObject();

                if (i == arr.size() - 1) {
                    coma = "";
                }

                id_rel_actividades_apu_ = id_rel_actividades_apu_ + json.get("id_rel_actividades_apu").getAsString() + coma;
                id_apus = id_apus + json.get("id_apu").getAsString() + coma;

            }

            ps.setInt(1, informacion.get("id_causal").getAsInt());
            ps.setInt(2, informacion.get("id_solicitud").getAsInt());
            ps.setArray(3, con.createArrayOf("text", id_rel_actividades_apu_.split(",")));
            ps.setArray(4, con.createArrayOf("text", id_apus.split(",")));
            ps.setArray(5, con.createArrayOf("text", informacion.get("id_insumos").getAsString().split(",")));
            ps.setString(6, usuario.getLogin());

            rs = ps.executeQuery();
            while (rs.next()) {
                if (rs.getString("respuesta").equals("OK")) {
                    respuestaJson = "{\"respuesta\":\"OK\"}";
                } else {
                    respuestaJson = "{\"respuesta\":\"ERROR\"}";
                }

                break;
            }

        } catch (Exception e) {
            try {
                respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR guardarMetaProceso \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(ProcesosCatalogoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
            }
            return respuestaJson;
        }
    }

    @Override
    public String cargar_Causales_Canasta() {
        Gson gson = new Gson();
        con = null;
        ps = null;
        rs = null;
        JsonObject obj = new JsonObject();
        String query = "SQL_CARGAR_CAUSALES_CANASTA";

        JsonArray arr = new JsonArray();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            //ps.setString(1, id_solicitud);
            rs = ps.executeQuery();
            JsonObject fila = null;
            while (rs.next()) {
                fila = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    fila.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                arr.add(fila);
            }
            obj.add("rows", arr);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return gson.toJson(obj);
        }
    }

    @Override
    public String guardar_Causales_Canasta(String nombre, String descripcion, Usuario usuario) {
        con = null;
        ps = null;
        String respuestaJson = "{}";
        query = "SQL_GUARDAR_CAUSALES_CANASTA";
        try {

            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);

            ps.setString(1, nombre);
            ps.setString(2, descripcion);
            ps.setString(3, usuario.getLogin());

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            try {
                respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR EN GUARDADO \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(ProcesosCatalogoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
            }
            return respuestaJson;
        }
    }

    @Override
    public String editar_Causales_Canasta(String id, String nombre, String descripcion, Usuario usuario) {
        con = null;
        ps = null;
        String respuestaJson = "{}";
        query = "SQL_EDITAR_CAUSALES_CANASTA";
        try {

            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);

            ps.setString(1, nombre);
            ps.setString(2, descripcion);
            ps.setString(3, usuario.getLogin());
            ps.setInt(4, Integer.parseInt(id));

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            try {
                respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR EN EDICION \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(ProcesosCatalogoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
            }
            return respuestaJson;
        }
    }

    @Override
    public String eliminar_Causales_Canasta(String id, Usuario usuario) {
        con = null;
        ps = null;
        String respuestaJson = "{}";

        try {
            if (!existeCausal(id)) {

                query = "SQL_ELIMINAR_CAUSALES_CANASTA";
                con = this.conectarJNDI(query);
                query = this.obtenerSQL(query);
                ps = con.prepareStatement(query);

                ps.setInt(1, Integer.parseInt(id));

                ps.executeUpdate();
                respuestaJson = "{\"respuesta\":\"OK\"}";
            } else {
                respuestaJson = "{\"error\":\"Esta causal ya se encuentra asociado por tal motivo no se puede eliminar\"}";
            }

        } catch (Exception e) {
            try {
                respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR ELIMINAR \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(ProcesosCatalogoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
            }
            return respuestaJson;
        }
    }

    public boolean existeCausal(String id) {
        boolean sw = false;
        PreparedStatement ps2 = null;
        try {
            ps2 = con.prepareStatement(obtenerSQL("SQL_GET_CAUSAL"));
            ps2.setInt(1, Integer.parseInt(id));
            rs = ps.executeQuery();
            while (rs.next()) {
                sw = true;
                break;
            }

        } catch (Exception e) {
        } finally {
            try {
                if (ps2 != null) {
                    ps2.close();
                }
            } catch (SQLException ex) {
            }
            return sw;

        }

    }

    private Array convertir_string_arrayint(String str) {
        String[] y = str.split(",");
        ArrayList<Integer> x = new ArrayList<>();

        for (int i = 0; i < y.length; i++) {
            x.add(Integer.parseInt(y[i]));
        }

        return (Array) x;
    }

    @Override
    public String cargar_Insumos_Apu(String id_solicitud, String id_rel_actividades_apu, String id_apu, String unidad_medida_apu) {
        Gson gson = new Gson();
        con = null;
        ps = null;
        rs = null;
        JsonObject obj = new JsonObject();
        String query = "SQL_CARGAR_INSUMOS_APU";

        JsonArray arr = new JsonArray();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(obtenerSQL(query).replace("#filtro", "and  cantidad_liberada  <  cantidad_insumo * cantidad_apu *rendimiento_insumo "));
            ps.setString(1, id_solicitud);
            ps.setString(2, id_rel_actividades_apu);
            ps.setString(3, id_apu);
            ps.setString(4, unidad_medida_apu);
            rs = ps.executeQuery();
            JsonObject fila = null;
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("id", rs.getString("id"));
                fila.addProperty("id_relacion_cotizacion_detalle_apu", rs.getString("id_relacion_cotizacion_detalle_apu"));
                fila.addProperty("id_insumo", rs.getString("id_insumo"));
                fila.addProperty("tipo_insumo", rs.getString("tipo_insumo"));
                fila.addProperty("descripcion_insumo", rs.getString("descripcion_insumo"));
                fila.addProperty("unidad_medida_insumo", rs.getString("unidad_medida_insumo"));
                fila.addProperty("nombre_unidad_insumo", rs.getString("nombre_unidad_insumo"));
                fila.addProperty("cantidad_insumo", rs.getString("cantidad_insumo"));
                fila.addProperty("rendimiento_insumo", rs.getString("rendimiento_insumo"));
                fila.addProperty("costo_personalizado", rs.getString("costo_personalizado"));
                fila.addProperty("cantidad_insumo_total", rs.getString("cantidad_insumo_total"));
                fila.addProperty("valor_insumo_total", rs.getString("valor_insumo_total"));
                fila.addProperty("cantidad_insumo_ejecutado", rs.getString("cantidad_insumo_ejecutado"));
                fila.addProperty("valor_insumo_ejecutado", rs.getString("valor_insumo_ejecutado"));
                fila.addProperty("cantidad_insumo_actual", rs.getString("cantidad_insumo_actual"));
                fila.addProperty("valor_insumo_actual", rs.getString("valor_insumo_actual"));
                fila.addProperty("cantidad_apu_actual", rs.getString("cantidad_apu_actual"));
                fila.addProperty("cantidad_insumo_ejecutado", rs.getString("cantidad_insumo_ejecutado"));
                fila.addProperty("valor_insumo_ejecutado", rs.getString("valor_insumo_ejecutado"));
                fila.addProperty("porc_avance_apu", rs.getString("porc_avance_apu"));

                arr.add(fila);
            }
            obj.add("rows", arr);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return gson.toJson(obj);
        }
    }

    @Override
    public String cargar_Insumos_Proyecto(String id_solicitud, String nom_insumo) {
        Gson gson = new Gson();
        con = null;
        ps = null;
        rs = null;
        JsonObject obj = new JsonObject();
        String query = "SQL_CARGAR_INSUMOS_PROYECTO";

        JsonArray arr = new JsonArray();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(obtenerSQL(query).replace("#filtro", "AND  insu.descripcion ilike '%" + nom_insumo + "%'"));
            ps.setString(1, id_solicitud);
            rs = ps.executeQuery();
            JsonObject fila = null;
            while (rs.next()) {
                fila = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    fila.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                arr.add(fila);
            }
            obj.add("rows", arr);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return gson.toJson(obj);
        }
    }

    @Override
    public String cargar_Apus_Insumo(String id_solicitud, String id_insumo, String id_unidad_medida_insumo) {
        Gson gson = new Gson();
        con = null;
        ps = null;
        rs = null;
        JsonObject obj = new JsonObject();
        String query = "SQL_CARGAR_APUS_INSUMO";

        JsonArray arr = new JsonArray();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(obtenerSQL(query));
            ps.setString(1, id_solicitud);
            ps.setString(2, id_insumo);
            ps.setString(3, id_unidad_medida_insumo);
            rs = ps.executeQuery();
            JsonObject fila = null;
            int id = 0;
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("id", id);
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    fila.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                id++;
                arr.add(fila);
            }
            obj.add("rows", arr);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return gson.toJson(obj);
        }
    }

    @Override
    public String bloquear_Apus_Insumo(JsonObject informacion, Usuario usuario) {
        con = null;
        ps = null;
        Statement stmt = null;
        JsonObject json = new JsonObject();

        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            con.setAutoCommit(false);
            stmt = con.createStatement();

            JsonArray arr = informacion.getAsJsonArray("apus_insumos_bloquear");
            for (int i = 0; i < arr.size(); i++) {
                json = arr.get(i).getAsJsonObject();

                stmt.addBatch(insertar_Insumos_Liberar(informacion.get("id_solicitud").getAsInt(), informacion.get("id_insumo").getAsInt(), informacion.get("unidad_medida_insumo").getAsInt(), json, usuario));

            }
            stmt.executeBatch();
            stmt.clearBatch();
            
            con.commit();
            if(liberar_insumos(informacion.get("id_solicitud").getAsInt(), informacion.get("id_causal").getAsInt(), informacion.get("id_insumo").getAsInt(), informacion.get("unidad_medida_insumo").getAsInt(), usuario))
            { 
                respuestaJson = "{\"respuesta\":\"OK\"}";
            }else{  
                respuestaJson = "{\"error\":\"error al liberar Insumos\"}";
            }
            
        } catch (Exception e) {
            try {
                con.rollback();
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException ex) {
                Logger.getLogger(ModuloCanastaImpl.class.getName()).log(Level.SEVERE, null, ex);
            } 
            e.printStackTrace();
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        }

        return respuestaJson;
    }

    public String insertar_Insumos_Liberar(int id_solicitud, int id_insumo, int unidad_medida_insumo, JsonObject insumo, Usuario usuario) {
        String cadena = "";
        String query = "SQL_INSERTAR_INSUMOS_LIBERAR";
        String sql = "";
        StringStatement st = null;
        sql = this.obtenerSQL(query);
        st = new StringStatement(sql, true);

        try {

            st.setInt(1, id_solicitud);
            st.setInt(2, insumo.get("id_disciplina").getAsInt());
            st.setInt(3, insumo.get("id_disciplina_area").getAsInt());
            st.setInt(4, insumo.get("id_capitulo").getAsInt());
            st.setInt(5, insumo.get("id_actividad").getAsInt());
            st.setInt(6, insumo.get("id_actividades_capitulo").getAsInt());
            st.setInt(7, insumo.get("id_rel_actividades_apu").getAsInt());
            st.setInt(8, insumo.get("id_apu").getAsInt());
            st.setInt(9, insumo.get("unidad_medida_apu").getAsInt());
            st.setInt(10, id_insumo);
            st.setInt(11, unidad_medida_insumo);
            st.setDouble(12, insumo.get("liberar").getAsDouble());
            st.setString(13, usuario.getLogin());
            cadena += st.getSql();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return cadena;
    }

    public boolean liberar_insumos(int id_solicitud, int id_causal, int id_insumo, int unidad_medida_insumo, Usuario usuario) throws SQLException {
        
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_LIBERAR_INSUMOS";
        
        boolean respuesta = false;
        
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            
            ps.setInt(1, id_solicitud);
            ps.setInt(2, id_causal);
            ps.setInt(3, id_insumo);
            ps.setInt(4, unidad_medida_insumo);
            ps.setString(5, usuario.getLogin());
            
            
            rs = ps.executeQuery();
            while (rs.next()) {
                if (rs.getString("respuesta").equals("OK")) {
                    respuesta=true;
                } 
                break;
            }
            
        } catch (Exception e) {
            try {
                throw new SQLException("ERROR guardarMetaProceso \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(ProcesosCatalogoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        }finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
            }
            return respuesta;
        }
    }

}
