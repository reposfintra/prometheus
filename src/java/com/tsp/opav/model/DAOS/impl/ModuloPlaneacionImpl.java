/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.opav.model.DAOS.impl;

/**
 *
 * @author User
 */
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.tsp.opav.model.DAOS.MainDAO;
import com.tsp.opav.model.DAOS.ModuloPlaneacionDAO;


/*Beans*/
import com.tsp.operation.model.beans.StringStatement;
import com.tsp.operation.model.beans.Usuario;
////////////////

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.sql.SQLException;
import java.sql.Statement;

public class ModuloPlaneacionImpl extends MainDAO implements ModuloPlaneacionDAO {

    private Connection con = null;
    private PreparedStatement ps = null;
    private ResultSet rs = null;
    private String query = "";
    private StringStatement st = null;

    public ModuloPlaneacionImpl(String dataBaseName) {
        super("ModuloPlaneacionDAO.xml", dataBaseName);
    }

    @Override
    public String cargar_conceptos() {
        con = null;
        rs = null;
        ps = null;

        query = "SQL_GET_CONCEPTOS";
        String respuestaJson = "{}";
        JsonObject obj = new JsonObject();
        Gson gson = new Gson();
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);

            rs = ps.executeQuery();
            JsonObject fila = null;
            JsonArray arr = new JsonArray();
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("id", rs.getString("id"));
                fila.addProperty("nombre", rs.getString("nombre"));
                fila.addProperty("descripcion", rs.getString("descripcion"));
                fila.addProperty("factura", rs.getString("factura"));
                arr.add(fila);
            }
            obj.add("rows", arr);
            respuestaJson = gson.toJson(obj);

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return respuestaJson;
    }

    @Override
    public String guardar_Concepto(String nombre, String descripcion, String factura, Usuario usuario) {
        con = null;
        ps = null;
        query = "SQL_SET_CONCEPTOS";
        String respuestaJson = "{}";
        
        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, usuario.getDstrct());
            ps.setString(2, nombre);
            ps.setString(3, descripcion);
            ps.setString(4, factura);
            ps.setString(5, usuario.getLogin());

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
            throw new SQLException("ERROR guardarTipoDocumento \n" + e.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    @Override
    public String actualizar_Concepto(String id, String nombre, String descripcion, String factura, Usuario usuario) {
        con = null;
        ps = null;
        query = "SQL_UPDATE_CONCEPTOS";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, nombre);
            ps.setString(2, descripcion);
            ps.setString(3, factura);
            ps.setString(4, usuario.getLogin());
            ps.setInt(5, Integer.parseInt(id));

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
            throw new SQLException("ERROR actualizarTipoDocumento \n" + e.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    @Override
    public String anular_Concepto(String id, Usuario usuario) {
        con = null;
        ps = null;
        query = "SQL_ANULAR_CONCEPTOS";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, usuario.getLogin());
            ps.setString(2, id);

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
            throw new SQLException("ERROR actualizarTipoDocumento \n" + e.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    @Override
    public String cargar_Riesgos() {
        con = null;
        rs = null;
        ps = null;

        query = "SQL_GET_RIESGOS";
        String respuestaJson = "{}";
        JsonObject obj = new JsonObject();
        Gson gson = new Gson();
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);

            rs = ps.executeQuery();
            JsonObject fila = null;
            JsonArray arr = new JsonArray();
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("id", rs.getString("id"));
                fila.addProperty("nombre", rs.getString("nombre"));
                fila.addProperty("descripcion", rs.getString("descripcion"));
                arr.add(fila);
            }
            obj.add("rows", arr);
            respuestaJson = gson.toJson(obj);

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return respuestaJson;
    }

    @Override
    public String guardar_Riesgos(String nombre, String descripcion, Usuario usuario) {
        con = null;
        ps = null;
        query = "SQL_SET_RIESGOS";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, usuario.getDstrct());
            ps.setString(2, nombre);
            ps.setString(3, descripcion);
            ps.setString(4, usuario.getLogin());

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
            throw new SQLException("ERROR guardarTipoDocumento \n" + e.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    @Override
    public String actualizar_Riesgos(String id, String nombre, String descripcion, Usuario usuario) {
        con = null;
        ps = null;
        query = "SQL_UPDATE_RIESGOS";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, nombre);
            ps.setString(2, descripcion);
            ps.setString(3, usuario.getLogin());
            ps.setInt(4, Integer.parseInt(id));

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
            throw new SQLException("ERROR actualizarTipoDocumento \n" + e.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    @Override
    public String anular_Riesgos(String id, Usuario usuario) {
        con = null;
        ps = null;
        query = "SQL_ANULAR_RIESGOS";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, usuario.getLogin());
            ps.setString(2, id);

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
            throw new SQLException("ERROR actualizarTipoDocumento \n" + e.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    @Override
    public String cargar_Indicadores_Gestion() {
        con = null;
        rs = null;
        ps = null;

        query = "SQL_GET_INDICADOREGESTION";
        String respuestaJson = "{}";
        JsonObject obj = new JsonObject();
        Gson gson = new Gson();
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);

            rs = ps.executeQuery();
            JsonObject fila = null;
            JsonArray arr = new JsonArray();
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("id", rs.getString("id"));
                fila.addProperty("nombre", rs.getString("nombre"));
                fila.addProperty("descripcion", rs.getString("descripcion"));
                arr.add(fila);
            }
            obj.add("rows", arr);
            respuestaJson = gson.toJson(obj);

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return respuestaJson;
    }

    @Override
    public String actualizar_Indicadores_Gestion(String id, String nombre, String descripcion, Usuario usuario) {
        con = null;
        ps = null;
        query = "SQL_UPDATE_INDICADOREGESTION";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, nombre);
            ps.setString(2, descripcion);
            ps.setString(3, usuario.getLogin());
            ps.setInt(4, Integer.parseInt(id));

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
            throw new SQLException("ERROR actualizarTipoDocumento \n" + e.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    @Override
    public String anular_Indicadores_Gestion(String id, Usuario usuario) {
        con = null;
        ps = null;
        query = "SQL_ANULAR_INDICADOREGESTION";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, usuario.getLogin());
            ps.setString(2, id);

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
            throw new SQLException("ERROR actualizarTipoDocumento \n" + e.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    @Override
    public String insertar_Indicadores_Gestion(String nombre, String descripcion, Usuario usuario) {
        con = null;
        ps = null;
        query = "SQL_SET_INDICADOREGESTION";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, usuario.getDstrct());
            ps.setString(2, nombre);
            ps.setString(3, descripcion);
            ps.setString(4, usuario.getLogin());

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
            throw new SQLException("ERROR guardarTipoDocumento \n" + e.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    @Override
    public String cargar_Unidades_Medida() {
        con = null;
        rs = null;
        ps = null;
        query = "SQL_GET_UNIDADES_MEDIDA";
        String respuestaJson = "{}";

        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);

            rs = ps.executeQuery();

            while (rs.next()) {
                obj.addProperty(rs.getString("id"), rs.getString("descripcion"));
            }
            respuestaJson = gson.toJson(obj);

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return respuestaJson;
    }
    
    @Override
    public String cargar_Unidades_Medida_Opav() {
        con = null;
        rs = null;
        ps = null;
        query = "SQL_GET_UNIDADES_MEDIDA_OPAV";
        String respuestaJson = "{}";

        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);

            rs = ps.executeQuery();

            while (rs.next()) {
                obj.addProperty(rs.getString("id"), rs.getString("descripcion"));
            }
            respuestaJson = gson.toJson(obj);

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return respuestaJson;
    }    

    @Override
    public String guardar_Carga_Laboral(String idActividad, String nivel, String rang_ini, String rang_final, String unidad_medida, Usuario usuario) {
        con = null;
        ps = null;
        query = "SQL_SET_CARGA_LABORAL";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, usuario.getDstrct());
            ps.setInt(2, Integer.parseInt(idActividad));
            ps.setString(3, nivel);
            ps.setDouble(4, Double.parseDouble(rang_ini));
            ps.setDouble(5, Double.parseDouble(rang_final));
            ps.setInt(6, Integer.parseInt(unidad_medida));
            ps.setString(7, usuario.getLogin());

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
            throw new SQLException("ERROR guardarTipoDocumento \n" + e.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }

    }

    @Override
    public String cargar_Cargos() {
        con = null;
        rs = null;
        ps = null;
        query = "SQL_GET_CARGOS";
        String respuestaJson = "{}";

        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);

            rs = ps.executeQuery();

            while (rs.next()) {
                obj.addProperty(rs.getString("id"), rs.getString("descripcion"));
            }
            respuestaJson = gson.toJson(obj);

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return respuestaJson;

    }

    @Override
    public String cargar_Carga_Laboral(String idActividad) {
        con = null;
        rs = null;
        ps = null;

        query = "SQL_GET_CARGA_LABORAL";
        String respuestaJson = "{}";
        JsonObject obj = new JsonObject();
        Gson gson = new Gson();
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);
            ps.setString(1, idActividad);

            rs = ps.executeQuery();
            JsonObject fila = null;
            JsonArray arr = new JsonArray();
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("id", rs.getString("id"));
                fila.addProperty("nivel", rs.getString("nivel"));
                fila.addProperty("rango_ini", rs.getString("rango_ini"));
                fila.addProperty("rango_fin", rs.getString("rango_fin"));
                fila.addProperty("id_unidad_medida_general", rs.getString("id_unidad_medida_general"));
                fila.addProperty("nombre_unidad", rs.getString("nombre_unidad"));
                arr.add(fila);
            }
            obj.add("rows", arr);
            respuestaJson = gson.toJson(obj);

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return respuestaJson;

    }

    @Override
    public String actualizar_Carga_Laboral(String id, String idActividad, String nivel, String rang_ini, String rang_final, String unidad_medida, Usuario usuario) {
        con = null;
        ps = null;
        query = "SQL_UPDATE_CARGA_LABORAL";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, usuario.getDstrct());
            ps.setInt(2, Integer.parseInt(idActividad));
            ps.setString(3, nivel);
            ps.setDouble(4, Double.parseDouble(rang_ini));
            ps.setDouble(5, Double.parseDouble(rang_final));
            ps.setInt(6, Integer.parseInt(unidad_medida));
            ps.setString(7, usuario.getLogin());
            ps.setInt(8, Integer.parseInt(id));

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
            throw new SQLException("ERROR guardarTipoDocumento \n" + e.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;

        }
    }

    @Override
    public String anular_Carga_Laboral(String id, Usuario usuario) {
        con = null;
        ps = null;
        query = "SQL_ANULAR_CARGA_LABORAL";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, usuario.getLogin());
            ps.setString(2, id);

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
            throw new SQLException("ERROR actualizarTipoDocumento \n" + e.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    @Override
    public String cargar_Cargas_Laborales(String id) {
        con = null;
        rs = null;
        ps = null;

        query = "SQL_GET_ACTIVIDADES_PLANEACION2";
        String respuestaJson = "{}";
        JsonObject obj = new JsonObject();
        Gson gson = new Gson();
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);
            ps.setInt(1, Integer.parseInt(id));
            rs = ps.executeQuery();
            JsonObject fila = null;
            JsonArray arr = new JsonArray();
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("id", rs.getString("id"));
                fila.addProperty("nombre", rs.getString("nombre"));
                fila.addProperty("descripcion", rs.getString("descripcion"));
                arr.add(fila);
            }
            obj.add("rows", arr);
            respuestaJson = gson.toJson(obj);

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return respuestaJson;
    }

    @Override
    public String cargar_Cargas_Laborales_Asociado(String id) {
        con = null;
        rs = null;
        ps = null;

        query = "SQL_GET_ACTIVIDADES_PLANEACION_ASOCIADO";
        String respuestaJson = "{}";
        JsonObject obj = new JsonObject();
        Gson gson = new Gson();
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);
            ps.setInt(1, Integer.parseInt(id));
            rs = ps.executeQuery();
            JsonObject fila = null;
            JsonArray arr = new JsonArray();
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("id", rs.getString("id"));
                fila.addProperty("nombre", rs.getString("nombre"));
                fila.addProperty("descripcion", rs.getString("descripcion"));
                fila.addProperty("peso", rs.getString("peso"));
                arr.add(fila);
            }
            obj.add("rows", arr);
            respuestaJson = gson.toJson(obj);

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return respuestaJson;
    }

    @Override
    public String asociar_Carga_Laboral(int id, String listado, Usuario usuario) {
        st = null;
        String sql = "";
        query = "SQL_ASOCIAR_CARGA_LABORAL";

        try {
            query = this.obtenerSQL(query);

            st = new StringStatement(query, true);
            st.setString(1, usuario.getDstrct());
            st.setInt(2, id);
            st.setInt(3, Integer.parseInt(listado));
            st.setString(4, usuario.getLogin());

            sql = st.getSql();

        } catch (Exception e) {
            try {
                throw new SQLException("ERROR insertarRelAPUActividad \n" + e.getMessage());
            } catch (SQLException ex) {
            }
        } finally {
        }
        return sql;
    }

    @Override
    public String cargar_tareas(String id_solicitud) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        PreparedStatement ps1 = null;
        ResultSet rs1 = null;
        String valor = "";
        String query2 = "SQL_CARGAR_TABLA";
        String query = "SQL_CARGAR_TAREAS";
        JsonArray lista = null;
        JsonObject objetoJson = null;
        Statement stmt = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            String consulta = "";
            String consulta2 = "";
            String condicion = id_solicitud;

            consulta2 = this.obtenerSQL(query2).replaceAll("#param1", condicion);
            ps1 = con.prepareStatement(consulta2);
            rs1 = ps1.executeQuery();
            while (rs1.next()) {
                valor = rs1.getString("tabla");
            }

            consulta = this.obtenerSQL(query).replaceAll("#param2", valor);
            ps = con.prepareStatement(consulta);
            rs = ps.executeQuery();
            stmt = con.createStatement();
            stmt.addBatch("drop table " + valor + ";");
            stmt.executeBatch();
            stmt.clearBatch();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }
            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return informacion;
        }
    }

    @Override
    public String cargar_asignacion(String id_solicitud) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_ASIGNACION";
        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            String consulta = "";
            String condicion = id_solicitud;

            consulta = this.obtenerSQL(query).replaceAll("#param1", condicion);
            ps = con.prepareStatement(consulta);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }
            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return informacion;
        }
    }

    @Override
    public boolean existeHitos(String empresa, String nombre, String idaccion) {
        con = null;
        rs = null;
        ps = null;
        boolean resp = false;
        query = "existeHitos";
        
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);

            byte[] bytes = nombre.getBytes("ISO-8859-1");
            nombre = new String(bytes, "ISO-8859-1");

            ps.setString(1, empresa);
            ps.setString(2, nombre);
            ps.setString(3, idaccion);

            rs = ps.executeQuery();

            if (rs.next()) {
                resp = true;
            }
        } catch (Exception e) {
            try {
                throw new Exception("Error en existeHitos " + e.toString());
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return resp;
    }
    
    @Override
    public String cargar_hitos(String idaccion) {
        con = null;
        rs = null;
        ps = null;

        query = "SQL_GET_HITOS";
        String respuestaJson = "{}";
        JsonObject obj = new JsonObject();
        Gson gson = new Gson();
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query).replace("#id", idaccion);
            
            ps = con.prepareStatement(query);

            rs = ps.executeQuery();
            JsonObject fila = null;
            JsonArray arr = new JsonArray();
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("id", rs.getString("id"));
                fila.addProperty("nombre", rs.getString("nombre"));
                fila.addProperty("fecha", rs.getString("fecha"));
                arr.add(fila);
            }
            obj.add("rows", arr);
            respuestaJson = gson.toJson(obj);

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return respuestaJson;
    }

    @Override
    public String guardar_hitos(String nombre, String fecha, Usuario usuario, String idaccion) {
        con = null;
        ps = null;
        query = "SQL_SET_HITOS";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, usuario.getDstrct());
            ps.setString(2, nombre);
            ps.setString(3, fecha);
            ps.setString(4, usuario.getLogin());
            ps.setInt(5, Integer.parseInt(idaccion));            

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
            throw new SQLException("ERROR guardarTipoDocumento \n" + e.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    @Override
    public String actualizar_hitos(String id, String nombre, String fecha, Usuario usuario, String idaccion) {
        con = null;
        ps = null;
        query = "SQL_UPDATE_HITOS";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, nombre);
            ps.setString(2, fecha);
            ps.setString(3, usuario.getLogin());
            ps.setInt(4, Integer.parseInt(id));
            ps.setInt(5, Integer.parseInt(idaccion));

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
            throw new SQLException("ERROR actualizarTipoDocumento \n" + e.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    @Override
    public String anular_hitos(String id, Usuario usuario) {
        con = null;
        ps = null;
        query = "SQL_ANULAR_HITOS";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, usuario.getLogin());
            ps.setString(2, id);

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
            throw new SQLException("ERROR actualizarTipoDocumento \n" + e.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    @Override
    public String cargar_Actividades() {
        con = null;
        rs = null;
        ps = null;

        query = "SQL_GET_ACTIVIDADES_PLANEACION";
        String respuestaJson = "{}";
        JsonObject obj = new JsonObject();
        Gson gson = new Gson();
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);

            rs = ps.executeQuery();
            JsonObject fila = null;
            JsonArray arr = new JsonArray();
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("id", rs.getString("id"));
                fila.addProperty("nombre", rs.getString("nombre"));
                fila.addProperty("descripcion", rs.getString("descripcion"));
                arr.add(fila);
            }
            obj.add("rows", arr);
            respuestaJson = gson.toJson(obj);

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return respuestaJson;
    }

    @Override
    public String insertar_Actividad(String nombre, String descripcion, Usuario usuario) {
        con = null;
        ps = null;
        query = "SQL_SET_ACTIVIDADES_PLANEACION";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, usuario.getDstrct());
            ps.setString(2, nombre);
            ps.setString(3, descripcion);
            ps.setString(4, usuario.getLogin());

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
            throw new SQLException("ERROR guardarTipoDocumento \n" + e.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    @Override
    public String actualizar_Actividad(String id, String nombre, String descripcion, Usuario usuario) {
        con = null;
        ps = null;
        query = "SQL_UPDATE_ACTIVIDADES_PLANEACION";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, nombre);
            ps.setString(2, descripcion);
            ps.setString(3, usuario.getLogin());
            ps.setInt(4, Integer.parseInt(id));

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
            throw new SQLException("ERROR actualizarTipoDocumento \n" + e.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    @Override
    public String anular_Actividad(String id, Usuario usuario) {

        String respuestaJson = "{}";

        try {
            if (existe_asoc_Act_cargos(Integer.parseInt(id))) {
                respuestaJson = "{\"error\":\"No se puede anular este registro debido a que existen cargos asociados\"}";
            } else {
                con = null;
                ps = null;
                query = "SQL_ANULAR_ACTIVIDADES_PLANEACION";
                con = this.conectarJNDI();
                query = this.obtenerSQL(query);
                ps = con.prepareStatement(query);
                ps.setString(1, usuario.getLogin());
                ps.setString(2, id);

                ps.executeUpdate();
                respuestaJson = "{\"respuesta\":\"OK\"}";
            }

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
            throw new SQLException("ERROR actualizarTipoDocumento \n" + e.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    public boolean existe_asoc_Act_cargos(int id) {
        con = null;
        rs = null;
        ps = null;

        query = "SQL_GET_ACTIVIDADES_PLANEACION_CARGOS";
        JsonObject obj = new JsonObject();

        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);
            ps.setInt(1, id);

            rs = ps.executeQuery();

            while (rs.next()) {
                return true;
            }

        } catch (Exception e) {

        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return false;
    }

    @Override
    public JsonObject ingresar_Peso_Actividad_Aso_Cargo(JsonObject rows) {
        JsonObject respuesta = new JsonObject();
        con = null;
        ps = null;

        String mensaje = "";
        Statement stmt = null;

        String respuestaJson = "{}";
        JsonArray arr = rows.getAsJsonArray("rows");
        JsonObject res;

        try {
            con = this.conectarJNDI();
            con.setAutoCommit(false);
            stmt = con.createStatement();

            for (int i = 0; i < arr.size(); i++) {

                res = arr.get(i).getAsJsonObject();
                query = "SQL_SET_PESO_ACTIVIDAD_ASO_CARGO";
                StringStatement st = null;
                String sql = this.obtenerSQL(query);
                st = new StringStatement(sql, true);
                st.setDouble(1, res.get("peso").getAsDouble());
                st.setInt(2, res.get("id").getAsInt());
                stmt.addBatch(st.getSql());

            }
            stmt.executeBatch();
            stmt.clearBatch();
            mensaje = "SE ACTUALIZO DE FORMA CORRECTA";

            con.commit();

            respuesta = new JsonObject();

            respuesta.addProperty("mensaje", mensaje);

        } catch (Exception exc) {
            con.rollback();
            respuesta = new JsonObject();
            exc.getMessage();
            respuesta.addProperty("error", "Error, ");

        } finally {
            try {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException e) {
                        throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                    }
                }
                if (con != null) {
                    try {
                        con.setAutoCommit(true);
                        this.desconectar(con);
                    } catch (SQLException e) {
                        throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage());
                    }
                }
            } catch (Exception ex) {
            }
            return respuesta;
        }
    }

    @Override
    public String cargar_Matris_Interesados(String id_solicitud) {
        con = null;
        rs = null;
        ps = null;

        query = "SQL_GET_MATRIS_INTERESADOS";
        String respuestaJson = "{}";
        JsonObject obj = new JsonObject();
        Gson gson = new Gson();
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);
            ps.setString(1, id_solicitud);
            rs = ps.executeQuery();
            JsonObject fila = null;
            JsonArray arr = new JsonArray();
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("id", rs.getString("id"));
                fila.addProperty("nombre", rs.getString("nombre"));
                fila.addProperty("empresa", rs.getString("empresa"));
                fila.addProperty("tipo_cliente", rs.getString("tipo_cliente"));
                fila.addProperty("telefono", rs.getString("telefono"));
                fila.addProperty("correo_electronico", rs.getString("correo_electronico"));
                fila.addProperty("rol", rs.getString("rol"));
                arr.add(fila);
            }
            obj.add("rows", arr);
            respuestaJson = gson.toJson(obj);

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return respuestaJson;
    }

    @Override
    public String insertar_Matris_Interesados(String id_solicitud, String nombre, String empresa, String tipo_cliente, String telefono, String correo_electronico, String rol, Usuario usuario) {
        con = null;
        ps = null;
        query = "SQL_SET_MATRIS_INTERESADOS";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, usuario.getDstrct());
            ps.setInt(2, Integer.parseInt(id_solicitud));
            ps.setString(3, nombre);
            ps.setString(4, empresa);
            ps.setString(5, tipo_cliente);
            ps.setString(6, telefono);
            ps.setString(7, correo_electronico);
            ps.setString(8, rol);
            ps.setString(9, usuario.getLogin());

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
            throw new SQLException("ERROR guardarTipoDocumento \n" + e.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    @Override
    public String actualizar_Matris_Interesados(String id, String nombre, String empresa, String tipo_cliente, String telefono, String correo_electronico, String rol, Usuario usuario) {
        con = null;
        ps = null;
        query = "SQL_UPDATE_MATRIS_INTERESADOS";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, usuario.getDstrct());
            ps.setString(2, nombre);
            ps.setString(3, empresa);
            ps.setString(4, tipo_cliente);
            ps.setString(5, telefono);
            ps.setString(6, correo_electronico);
            ps.setString(7, rol);
            ps.setString(8, usuario.getLogin());
            ps.setString(9, id);
            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
            throw new SQLException("ERROR actualizarTipoDocumento \n" + e.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    @Override
    public String anular_Item_Matris_Interesados(String id, Usuario usuario) {
        con = null;
        ps = null;
        query = "SQL_ANULAR_ITEM_MATRIS_INTERESADOS";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, usuario.getLogin());
            ps.setString(2, id);

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
            throw new SQLException("ERROR actualizarTipoDocumento \n" + e.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    @Override
    public String cargar_Plan_Contatos(String id_solicitud) {
        con = null;
        rs = null;
        ps = null;

        query = "SQL_GET_PLAN_CONTATOS";
        String respuestaJson = "{}";
        JsonObject obj = new JsonObject();
        Gson gson = new Gson();
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);
            ps.setString(1, id_solicitud);
            rs = ps.executeQuery();
            JsonObject fila = null;
            JsonArray arr = new JsonArray();
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("id", rs.getString("id"));
                fila.addProperty("tipo_documento", rs.getString("tipo_documento"));
                fila.addProperty("medio_transmision", rs.getString("medio_transmision"));
                fila.addProperty("responsable", rs.getString("responsable"));
                fila.addProperty("tipo_informacion", rs.getString("tipo_informacion"));
                fila.addProperty("medio_almacenamiento", rs.getString("medio_almacenamiento"));
                fila.addProperty("periocidad", rs.getString("periocidad"));
                fila.addProperty("destinatario_principal", rs.getString("destinatario_principal"));
                fila.addProperty("destinatario_secundario", rs.getString("destinatario_secundario"));

                arr.add(fila);
            }
            obj.add("rows", arr);
            respuestaJson = gson.toJson(obj);

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return respuestaJson;
    }

    @Override
    public String insertar_Plan_Contatos(String id_solicitud, String tipo_documento, String medio_transmision, String responsable, String tipo_informacion, String medio_almacenamiento, String periocidad, String destinatario_principal, String destinatario_secundario, Usuario usuario) {

        con = null;
        ps = null;
        query = "SQL_SET_PLAN_CONTATOS";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, usuario.getDstrct());
            ps.setInt(2, Integer.parseInt(id_solicitud));
            ps.setString(3, tipo_documento);
            ps.setString(4, medio_transmision);
            ps.setString(5, responsable);
            ps.setString(6, tipo_informacion);
            ps.setString(7, medio_almacenamiento);
            ps.setString(8, periocidad);
            ps.setString(9, destinatario_principal);
            ps.setString(10, destinatario_secundario);
            ps.setString(11, usuario.getLogin());

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
            throw new SQLException("ERROR guardarTipoDocumento \n" + e.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    @Override
    public String actualizar_Plan_Contatos(String id, String tipo_documento, String medio_transmision, String responsable, String tipo_informacion, String medio_almacenamiento, String periocidad, String destinatario_principal, String destinatario_secundario, Usuario usuario) {
        con = null;
        ps = null;
        query = "SQL_UPDATE_PLAN_CONTATOS";
        String respuestaJson = "{}";

        try {

            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, usuario.getDstrct());
            ps.setString(2, tipo_documento);
            ps.setString(3, medio_transmision);
            ps.setString(4, responsable);
            ps.setString(5, tipo_informacion);
            ps.setString(6, medio_almacenamiento);
            ps.setString(7, periocidad);
            ps.setString(8, destinatario_principal);
            ps.setString(9, destinatario_secundario);
            ps.setString(10, usuario.getLogin());
            ps.setString(11, id);
            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
            throw new SQLException("ERROR actualizarTipoDocumento \n" + e.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }

    }

    @Override
    public String anular_Item_Plan_Contatos(String id, Usuario usuario) {
        con = null;
        ps = null;
        query = "SQL_ANULAR_ITEM_PLAN_CONTATOS";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, usuario.getLogin());
            ps.setString(2, id);

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
            throw new SQLException("ERROR actualizarTipoDocumento \n" + e.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    @Override
    public String insertar_Aspectos_Proyecto(String id_solicitud, String descripcion_proyecto, String requisitos_calidad_normas, String alcances_contrato, String Penalizaciones, String fecha_inicio_cliente, String fecha_fin_cliente, String fecha_inicio_real, String fecha_fin_real, String necesidades_dise�o, Usuario usuario) {
        con = null;
        ps = null;
        query = "SQL_SET_ASPECTOS_PROYECTO";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, usuario.getDstrct());
            ps.setInt(2, Integer.parseInt(id_solicitud));
            ps.setString(3, descripcion_proyecto);
            ps.setString(4, requisitos_calidad_normas);
            ps.setString(5, alcances_contrato);
            ps.setString(6, Penalizaciones);
            ps.setString(7, fecha_inicio_cliente);
            ps.setString(8, fecha_fin_cliente);
            ps.setString(9, fecha_inicio_real);
            ps.setString(10, fecha_fin_real);
            ps.setString(11, necesidades_dise�o);
            ps.setString(12, usuario.getLogin());

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
            throw new SQLException("ERROR guardarTipoDocumento \n" + e.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    @Override
    public String cargar_Aspectos_Proyecto(String id_solicitud) {
        con = null;
        rs = null;
        ps = null;

        query = "SQL_GET_ASPECTOS_PROYECTO";
        String respuestaJson = "{}";
        JsonObject obj = new JsonObject();
        Gson gson = new Gson();
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);
            ps.setString(1, id_solicitud);
            rs = ps.executeQuery();
            JsonObject data = null;
            while (rs.next()) {

                data = new JsonObject();
                data.addProperty("id", rs.getString("id"));
                data.addProperty("descripcion_proyecto", rs.getString("descripcion_proyecto"));
                data.addProperty("requisitos_calidad_normas", rs.getString("requisitos_calidad_normas"));
                data.addProperty("alcances_contrato", rs.getString("alcances_contrato"));
                data.addProperty("penalizaciones", rs.getString("penalizaciones"));
                data.addProperty("fecha_inicio_cliente", rs.getString("fecha_inicio_cliente"));
                data.addProperty("fecha_fin_cliente", rs.getString("fecha_fin_cliente"));
                data.addProperty("fecha_inicio_real", rs.getString("fecha_inicio_real"));
                data.addProperty("fecha_fin_real", rs.getString("fecha_fin_real"));
                data.addProperty("necesidades_dise�o", rs.getString("necesidades_dise�o"));

            }
            obj.add("informacion", data);
            obj.addProperty("respuesta", "OK");
            respuestaJson = gson.toJson(obj);

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return respuestaJson;
    }

    @Override
    public String actualizar_Aspectos_Proyecto(String id, String descripcion_proyecto, String requisitos_calidad_normas, String alcances_contrato, String Penalizaciones, String fecha_inicio_cliente, String fecha_fin_cliente, String fecha_inicio_real, String fecha_fin_real, String necesidades_dise�o, Usuario usuario) {
        con = null;
        ps = null;
        query = "SQL_UPDATE_ASPECTOS_PROYECTO";
        String respuestaJson = "{}";

        try {

            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, usuario.getDstrct());
            ps.setString(2, descripcion_proyecto);
            ps.setString(3, requisitos_calidad_normas);
            ps.setString(4, alcances_contrato);
            ps.setString(5, Penalizaciones);
            ps.setString(6, fecha_inicio_cliente);
            ps.setString(7, fecha_fin_cliente);
            ps.setString(8, fecha_inicio_real);
            ps.setString(9, fecha_fin_real);
            ps.setString(10, necesidades_dise�o);
            ps.setString(11, usuario.getLogin());
            ps.setString(12, id);
            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
            throw new SQLException("ERROR actualizarTipoDocumento \n" + e.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    @Override
    public String listar_Cargas_Laborales(String id_solicitud) {

        con = null;
        rs = null;
        ps = null;

        query = "SQL_LISTAR_CARGAS_LABORALES";
        String respuestaJson = "{}";
        JsonObject obj = new JsonObject();
        Gson gson = new Gson();
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);
            ps.setString(1, id_solicitud);

            rs = ps.executeQuery();
            JsonObject fila = null;
            JsonArray arr = new JsonArray();
            int i = 0;
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("id", rs.getString("id"));
                fila.addProperty("id_cargo", rs.getString("id_cargo"));
                fila.addProperty("descripcion_cargo", rs.getString("descripcion_cargo"));
                fila.addProperty("carga_real", rs.getString("carga_real"));
                arr.add(fila);
            }
            obj.add("rows", arr);
            respuestaJson = gson.toJson(obj);

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return respuestaJson;

    }

    @Override
    public String listar_Infomacion_Actividades() {
        con = null;
        rs = null;
        ps = null;

        query = "SQL_LISTAR_INFOMACION_ACTIVIDADES";
        String respuestaJson = "{}";
        JsonObject obj = new JsonObject();
        Gson gson = new Gson();
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);

            rs = ps.executeQuery();
            JsonObject fila = null;
            JsonArray arr = new JsonArray();
            int i = 0;
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("id", rs.getString("id"));
                fila.addProperty("id_actividad", rs.getString("id_actividad"));
                fila.addProperty("nombre_actividad", rs.getString("nombre_actividad"));
                fila.addProperty("nivel", rs.getString("nivel"));
                fila.addProperty("rango_ini", rs.getString("rango_ini"));
                fila.addProperty("rango_fin", rs.getString("rango_fin"));
                fila.addProperty("id_unidad_medida_general", rs.getString("id_unidad_medida_general"));
                fila.addProperty("nombre_unidad", rs.getString("nombre_unidad"));
                arr.add(fila);
            }
            obj.add("rows", arr);
            respuestaJson = gson.toJson(obj);

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return respuestaJson;
    }

    @Override
    public String listarCargasLaborales_Detalle(String id_ind_carga_laboral, String id_cargo) {
        con = null;
        rs = null;
        ps = null;
        String condicion = "";
        if (!(id_ind_carga_laboral == "")) {
            query = "SQL_LISTARCARGASLABORALES_DETALLE_2";
            condicion = "AND a.id = " + id_ind_carga_laboral;
        } else {
            query = "SQL_LISTARCARGASLABORALES_DETALLE";
        }

        String respuestaJson = "{}";
        JsonObject obj = new JsonObject();
        Gson gson = new Gson();
        try {
            con = this.conectarJNDI(query);

            query = this.obtenerSQL(query).replaceAll("#condicion", condicion);;

            ps = con.prepareStatement(query);
            ps.setString(1, id_cargo);

            rs = ps.executeQuery();
            JsonObject fila = null;
            JsonArray arr = new JsonArray();
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("id", rs.getString("id"));
                fila.addProperty("id_cargos", rs.getString("id_cargos"));
                fila.addProperty("id_actividad", rs.getString("id_actividad"));
                fila.addProperty("nombre_actividad", rs.getString("nombre_actividad"));
                fila.addProperty("peso", rs.getString("peso"));
                fila.addProperty("puntaje", rs.getString("puntaje"));
                fila.addProperty("carga_real", rs.getString("carga_real"));
                arr.add(fila);
            }
            obj.add("rows", arr);
            respuestaJson = gson.toJson(obj);

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return respuestaJson;
    }

    @Override
    public String guardar_Asignacion_Puntaje(String id_solicitud, String id_cargo, JsonObject rows, Usuario usuario) {
        con = null;
        ps = null;
        int resp = 0, idCab = 0;
        Statement stmt = null;
        String respuestaJson = "{}";

        try {
            JsonObject objeto = new JsonObject();
            PreparedStatement ps = null;
            con = this.conectarJNDI();
            String query = "SQL_GUARDAR_ASIGNACION_PUNTAJE";
            con.setAutoCommit(false);
            ps = con.prepareStatement(this.obtenerSQL(query), Statement.RETURN_GENERATED_KEYS);
            ps.setInt(1, Integer.parseInt(id_solicitud));
            ps.setInt(2, Integer.parseInt(id_cargo));
            ps.setString(3, usuario.getLogin());

            resp = ps.executeUpdate();
            stmt = con.createStatement();
            if (resp > 0) {
                ResultSet rs = ps.getGeneratedKeys();
                if (rs.next()) {
                    idCab = rs.getInt(1);
                    JsonArray arr = rows.getAsJsonArray("datosGrillas");
                    for (int i = 0; i < arr.size(); i++) {
                        objeto = arr.get(i).getAsJsonObject();
                        stmt.addBatch(guardar_Detalle_Asignacion_Puntaje(idCab, objeto, usuario));
                    }

                    stmt.executeBatch();
                    stmt.clearBatch();
                    respuestaJson = "{\"respuesta\":\"OK\"}";
                }
            }
            con.commit();
        } catch (Exception exc) {
            con.rollback();
            exc.getMessage();
        } finally {
            try {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException e) {
                        throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                    }
                }
                if (con != null) {
                    try {
                        con.setAutoCommit(true);
                        this.desconectar(con);
                    } catch (SQLException e) {
                        throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage());
                    }
                }
            } catch (Exception ex) {
            }
            return respuestaJson;
        }
    }

    public String guardar_Detalle_Asignacion_Puntaje(int idCab, JsonObject rows, Usuario usuario) {
        String cadena = "";
        String query = "SQL_GUARDAR_DETALLE_ASIGNACION_PUNTAJE";
        String sql = "";
        StringStatement st = null;
        sql = this.obtenerSQL(query);
        st = new StringStatement(sql, true);

        try {

            st.setInt(1, idCab);
            st.setInt(2, rows.get("id_actividad").getAsInt());
            st.setDouble(3, rows.get("peso").getAsDouble());
            st.setDouble(4, rows.get("puntaje").getAsDouble());
            st.setDouble(5, rows.get("carga_real").getAsDouble());
            st.setString(6, usuario.getLogin());

            cadena += st.getSql();

        } catch (Exception e) {
            System.out.println("Error en SQL_INSERTARDETALLE_ASIGNACION_PUNTAJE " + e.toString());
            e.printStackTrace();
        }
        return cadena;

    }
    

    @Override
    public String editar_Asignacion_Puntaje(String id, JsonObject rows, Usuario usuario) {
        con = null;
        ps = null;

        Statement stmt = null;
        String respuestaJson = "{}";

        try {
            JsonObject objeto = new JsonObject();
            PreparedStatement ps = null;
            con = this.conectarJNDI();
            con.setAutoCommit(false);

            stmt = con.createStatement();
            stmt.addBatch("delete from opav.sl_ind_carga_laboral_detalle where id_ind_carga_laboral = "+ id);
            JsonArray arr = rows.getAsJsonArray("datosGrillas");
            for (int i = 0; i < arr.size(); i++) {
                objeto = arr.get(i).getAsJsonObject();
                stmt.addBatch(guardar_Detalle_Asignacion_Puntaje(Integer.parseInt(id), objeto, usuario));
            }

            stmt.executeBatch();
            stmt.clearBatch();
            respuestaJson = "{\"respuesta\":\"OK\"}";

            con.commit();
        } catch (Exception exc) {
            con.rollback();
            exc.getMessage();
        } finally {
            try {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException e) {
                        throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                    }
                }
                if (con != null) {
                    try {
                        con.setAutoCommit(true);
                        this.desconectar(con);
                    } catch (SQLException e) {
                        throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage());
                    }
                }
            } catch (Exception ex) {
            }
            return respuestaJson;
        }
    }

    @Override
    public String anular_Asignacion_Puntaje(String id, Usuario usuario) {
        con = null;
        ps = null;

        Statement stmt = null;
        String respuestaJson = "{}";

        try {
            JsonObject objeto = new JsonObject();
            PreparedStatement ps = null;
            con = this.conectarJNDI();
            con.setAutoCommit(false);

            stmt = con.createStatement();
            stmt.addBatch("delete from opav.sl_ind_carga_laboral_detalle where id_ind_carga_laboral = "+ id);
            stmt.addBatch("delete from opav.sl_ind_carga_laboral where id = "+ id);
            

            stmt.executeBatch();
            stmt.clearBatch();
            respuestaJson = "{\"respuesta\":\"OK\"}";

            con.commit();
        } catch (Exception exc) {
            con.rollback();
            exc.getMessage();
        } finally {
            try {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException e) {
                        throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                    }
                }
                if (con != null) {
                    try {
                        con.setAutoCommit(true);
                        this.desconectar(con);
                    } catch (SQLException e) {
                        throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage());
                    }
                }
            } catch (Exception ex) {
            }
            return respuestaJson;
        }
    }
   
    
    @Override
    public String cargar_flujo_caja(String idaccion) {
        con = null;
        rs = null;
        ps = null;

        query = "SQL_GET_FLUJO_CAJA";
        String respuestaJson = "{}";
        JsonObject obj = new JsonObject();
        Gson gson = new Gson();
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
                    //.replace("#id", idaccion);
            
            ps = con.prepareStatement(query);

            ps.setInt(1, Integer.parseInt(idaccion));
            
            rs = ps.executeQuery();
            JsonObject fila = null;
            JsonArray arr = new JsonArray();
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("id", rs.getString("id"));
                fila.addProperty("periodo", rs.getString("periodo"));
                fila.addProperty("valor", rs.getString("valor"));
                fila.addProperty("concepto", rs.getString("concepto"));
                fila.addProperty("id_concepto", rs.getString("id_concepto"));
                fila.addProperty("descripcion", rs.getString("descripcion"));
                arr.add(fila);
            }
            obj.add("rows", arr);
            respuestaJson = gson.toJson(obj);

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return respuestaJson;
    }
    
    @Override
    public String guardar_flujo(String periodo, String valor, String concepto, String descripcion, 
            Usuario usuario, String idaccion) {
        con = null;
        ps = null;
        query = "SQL_SET_FLUJO";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, usuario.getDstrct());
            ps.setInt(2, Integer.parseInt(idaccion));
            ps.setString(3, periodo);
            ps.setDouble(4, Double.parseDouble(valor));
            ps.setInt(5, Integer.parseInt(concepto));
            ps.setString(6, descripcion);
            ps.setString(7, usuario.getLogin());
            ps.setString(8, usuario.getLogin());            

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
            throw new SQLException("ERROR guardarTipoDocumento \n" + e.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }
    
    @Override
    public String actualizar_Flujo(String id, String periodo, String valor, String concepto, 
            String descripcion, Usuario usuario, String idaccion) {
        con = null;
        ps = null;
        query = "SQL_UPDATE_FLUJO";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, periodo);
            ps.setDouble(2, Double.parseDouble(valor));
            ps.setInt(3, Integer.parseInt(concepto));
            ps.setString(4, descripcion);
            ps.setString(5, usuario.getLogin());
            ps.setInt(6, Integer.parseInt(id));
            
            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
            throw new SQLException("ERROR actualizarTipoDocumento \n" + e.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }
    
    @Override
    public String anular_flujo(String id, Usuario usuario) {
        con = null;
        ps = null;
        query = "SQL_ANULAR_FLUJO";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, usuario.getLogin());
            ps.setString(2, id);

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
            throw new SQLException("ERROR actualizarTipoDocumento \n" + e.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }
    
    @Override
    public String cargar_indicador() {
        con = null;
        rs = null;
        ps = null;

        query = "SQL_GET_INDICADOR";
        String respuestaJson = "{}";
        JsonObject obj = new JsonObject();
        Gson gson = new Gson();
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            
            ps = con.prepareStatement(query);

            rs = ps.executeQuery();
            JsonObject fila = null;
            JsonArray arr = new JsonArray();
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("id", rs.getString("id"));
                fila.addProperty("nombre", rs.getString("nombre"));
                fila.addProperty("descripcion", rs.getString("descripcion"));
                arr.add(fila);
            }
            obj.add("rows", arr);
            respuestaJson = gson.toJson(obj);

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return respuestaJson;
    }
    
    @Override
    public String guardar_indicador(String nombre, String descripcion, Usuario usuario) {
        con = null;
        ps = null;
        query = "SQL_SET_INDICADOR";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, usuario.getDstrct());
            ps.setString(2, nombre);
            ps.setString(3, descripcion);
            ps.setString(4, usuario.getLogin());
            ps.setString(5, usuario.getLogin());

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
            throw new SQLException("ERROR guardarTipoDocumento \n" + e.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }
    
    @Override
    public String actualizar_indicador(String id, String nombre, String descripcion, Usuario usuario) {
        con = null;
        ps = null;
        query = "SQL_UPDATE_INDICADOR";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, nombre);
            ps.setString(2, descripcion);
            ps.setString(3, usuario.getLogin());
            ps.setInt(4, Integer.parseInt(id));

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
            throw new SQLException("ERROR actualizarTipoDocumento \n" + e.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    @Override
    public String anular_indicador(String id, Usuario usuario) {
        con = null;
        ps = null;
        query = "SQL_ANULAR_INDICADOR";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, usuario.getLogin());
            ps.setString(2, id);

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
            throw new SQLException("ERROR actualizarTipoDocumento \n" + e.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }
    
    
    //////////////////////////////////////////////////////
    
    @Override
    public String cargar_gestion(String idaccion) {
        con = null;
        rs = null;
        ps = null;

        query = "SQL_GET_GESTION";
        String respuestaJson = "{}";
        JsonObject obj = new JsonObject();
        Gson gson = new Gson();
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query).replace("#id", idaccion);
            
            ps = con.prepareStatement(query);

            rs = ps.executeQuery();
            JsonObject fila = null;
            JsonArray arr = new JsonArray();
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("id", rs.getString("id"));
                fila.addProperty("valor_min", rs.getString("valor_min"));
                fila.addProperty("valor_max", rs.getString("valor_max"));
                fila.addProperty("alerta_min", rs.getString("alerta_min"));
                fila.addProperty("alerta_max", rs.getString("alerta_max"));
                fila.addProperty("id_indicador", rs.getString("id_indicador"));
                fila.addProperty("indicador", rs.getString("indicador"));
                arr.add(fila);
            }
            obj.add("rows", arr);
            respuestaJson = gson.toJson(obj);

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return respuestaJson;
    }
    
    @Override
    public String guardar_gestion(String indicador, String valor_min, String valor_max, String alerta_min, String alerta_max,
            Usuario usuario, String idaccion) {
        con = null;
        ps = null;
        query = "SQL_SET_GESTION";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, usuario.getDstrct());
            ps.setInt(2, Integer.parseInt(idaccion));
            ps.setInt(3, Integer.parseInt(indicador));
            ps.setDouble(4, Double.parseDouble(valor_min));
            ps.setDouble(5, Double.parseDouble(valor_max));
            ps.setDouble(6, Double.parseDouble(alerta_min));
            ps.setDouble(7, Double.parseDouble(alerta_max));
            ps.setString(8, usuario.getLogin());
            ps.setString(9, usuario.getLogin());            

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
            throw new SQLException("ERROR guardarTipoDocumento \n" + e.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }
    
    @Override
    public String actualizar_gestion(String id, String indicador, String valor_min, String valor_max, 
            String alerta_min, String alerta_max, Usuario usuario, String idaccion) {
        con = null;
        ps = null;
        query = "SQL_UPDATE_GESTION";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setInt(1, Integer.parseInt(indicador));
            ps.setDouble(2, Double.parseDouble(valor_min));
            ps.setDouble(3, Double.parseDouble(valor_max));
            ps.setDouble(4, Double.parseDouble(alerta_min));
            ps.setDouble(5, Double.parseDouble(alerta_max));
            ps.setString(6, usuario.getLogin());
            ps.setInt(7, Integer.parseInt(id));
            
            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
            throw new SQLException("ERROR actualizarTipoDocumento \n" + e.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }
    
    @Override
    public String anular_gestion(String id, Usuario usuario) {
        con = null;
        ps = null;
        query = "SQL_ANULAR_GESTION";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, usuario.getLogin());
            ps.setString(2, id);

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
            throw new SQLException("ERROR actualizarTipoDocumento \n" + e.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }
    
    //////////////////////////////////////////
    
    @Override
    public String cargar_proyeccion(String idaccion) {
        con = null;
        rs = null;
        ps = null;

        query = "SQL_GET_PROYECCION";
        String respuestaJson = "{}";
        JsonObject obj = new JsonObject();
        Gson gson = new Gson();
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            
            ps = con.prepareStatement(query);
            ps.setInt(1, Integer.parseInt(idaccion));
            rs = ps.executeQuery();
            JsonObject fila = null;
            JsonArray arr = new JsonArray();
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("id", rs.getString("id"));
                fila.addProperty("periodo", rs.getString("periodo"));
                fila.addProperty("valor", rs.getString("valor"));
                fila.addProperty("concepto", rs.getString("nombre"));
                arr.add(fila);
            }
            obj.add("rows", arr);
            respuestaJson = gson.toJson(obj);

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return respuestaJson;
    }
    
    //////////////////////////////////////////////
    
    @Override
    public String cargar_equipos(String idaccion) {
        con = null;
        rs = null;
        ps = null;
        int i = 0;

        query = "SQL_GET_INSUMO";
        String respuestaJson = "{}";
        JsonObject obj = new JsonObject();
        Gson gson = new Gson();
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            
            ps = con.prepareStatement(query);
            ps.setInt(1, 2);
            ps.setInt(2, Integer.parseInt(idaccion));
            rs = ps.executeQuery();
            JsonObject fila = null;
            JsonArray arr = new JsonArray();
            while (rs.next()) {
                i= i+1;
                fila = new JsonObject();
                fila.addProperty("id", "nu_"+i);
                fila.addProperty("insumo", rs.getString("insumo"));
                fila.addProperty("id_tipo_insumo", rs.getString("id_tipo_insumo"));
                fila.addProperty("id_insumo", rs.getString("id_insumo"));
                arr.add(fila);
            }
            obj.add("rows", arr);
            respuestaJson = gson.toJson(obj);

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return respuestaJson;
    }
    
    @Override
    public boolean existe_equipo(String idaccion) {
        con = null;
        rs = null;
        ps = null;
        
        query = "SQL_GET_INSUMO_EXISTE";
        boolean respuesta = false;
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            
            ps = con.prepareStatement(query);
            ps.setInt(1, 2);
            ps.setInt(2, Integer.parseInt(idaccion));
            rs = ps.executeQuery();

            while (rs.next()) {
                respuesta = true;
                break;
            }
           
        } catch (Exception e) {
            
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return respuesta;
    }
    
    @Override
    public String cargar_equipos_bd(String idaccion) {
        con = null;
        rs = null;
        ps = null;

        query = "SQL_GET_INSUMO_BD";
        String respuestaJson = "{}";
        JsonObject obj = new JsonObject();
        Gson gson = new Gson();
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            
            ps = con.prepareStatement(query);
            ps.setInt(1, 2);
            ps.setInt(2, Integer.parseInt(idaccion));
            rs = ps.executeQuery();
            JsonObject fila = null;
            JsonArray arr = new JsonArray();
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("id", rs.getString("id"));
                fila.addProperty("id_tipo_insumo", rs.getString("id_tipo_insumo"));
                fila.addProperty("id_insumo", rs.getString("id_insumo"));
                fila.addProperty("insumo", rs.getString("insumo"));
                fila.addProperty("periodo_ini", rs.getString("periodo_ini"));
                fila.addProperty("periodo_fin", rs.getString("periodo_fin"));
                fila.addProperty("h_trabajo", rs.getString("h_trabajo"));
                
                arr.add(fila);
            }
            obj.add("rows", arr);
            respuestaJson = gson.toJson(obj);

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return respuestaJson;
    }
    
    @Override
    public JsonObject ingresar_equipos(JsonObject info) {

        JsonObject respuesta = new JsonObject();
        JsonObject recorre = new JsonObject();
        StringStatement ss = null;
        con = null;
        ps = null;
        String mensaje = "";
        //query = "GuardarEncabezadoInsumo";
        //TransaccionService tsrv = null;
        int resp = 0, idinsumo = 0;
        int idCab = 0;

        try {

            JsonArray arr = info.getAsJsonArray("rows");
            con = this.conectarJNDI();
            con.setAutoCommit(false);
            for (int i = 0; i < arr.size(); i++) {
                respuesta = arr.get(i).getAsJsonObject();

                if (respuesta.get("id").getAsString().startsWith("nu_")) {

                    String query = "INSERTAR_NUEVO_INSUMO";

                    ps = con.prepareStatement(this.obtenerSQL(query));
                    
                    ps.setString(1, info.get("dstrct").getAsString());
                    ps.setInt(2, Integer.parseInt(info.get("idaccion").getAsString()));
                    ps.setInt(3, respuesta.get("id_tipo_insumo").getAsInt());
                    ps.setInt(4, respuesta.get("id_insumo").getAsInt());
                    ps.setString(5, respuesta.get("periodo_ini").getAsString());
                    ps.setString(6, respuesta.get("periodo_fin").getAsString());
                    ps.setFloat(7, Float.parseFloat(respuesta.get("h_trabajo").getAsString()));
                    ps.setString(8, info.get("usuario").getAsString());
                    ps.setString(9, info.get("usuario").getAsString());

                    ps.executeUpdate();
                    mensaje = "Insercion satisfactoria";
                    con.commit();

                } else {

                    String query = "ACTUALIZAR_INSUMO";
                    con.setAutoCommit(false);

                    ps = con.prepareStatement(this.obtenerSQL(query));

                    ps.setString(1, respuesta.get("periodo_ini").getAsString());
                    ps.setString(2, respuesta.get("periodo_fin").getAsString());
                    ps.setFloat(3, Float.parseFloat(respuesta.get("h_trabajo").getAsString()));
                    ps.setString(4, info.get("usuario").getAsString());
                    ps.setInt(5, respuesta.get("id").getAsInt());
                    
                    ps.executeUpdate();

                    mensaje = "Edicion satisfactoria";

                }

            }
            con.commit();
            respuesta = new JsonObject();

            respuesta.addProperty(
                    "mensaje", mensaje);

        } catch (Exception exc) {
            respuesta = new JsonObject();
            exc.getMessage();
            respuesta.addProperty("error", "Error, no puede registrar productos ya guardados");
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
            return respuesta;
        }
    }
    
    ////////////////////////////////////////////////
    
    @Override
    public String cargar_personal(String idaccion) {
        con = null;
        rs = null;
        ps = null;
        int i = 0;

        query = "SQL_GET_INSUMO";
        String respuestaJson = "{}";
        JsonObject obj = new JsonObject();
        Gson gson = new Gson();
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            
            ps = con.prepareStatement(query);
            ps.setInt(1, 3);
            ps.setInt(2, Integer.parseInt(idaccion));
            rs = ps.executeQuery();
            JsonObject fila = null;
            JsonArray arr = new JsonArray();
            while (rs.next()) {
                i= i+1;
                fila = new JsonObject();
                fila.addProperty("id", "nu_"+i);
                fila.addProperty("insumo", rs.getString("insumo"));
                fila.addProperty("id_tipo_insumo", rs.getString("id_tipo_insumo"));
                fila.addProperty("id_insumo", rs.getString("id_insumo"));
                arr.add(fila);
            }
            obj.add("rows", arr);
            respuestaJson = gson.toJson(obj);

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return respuestaJson;
    }
    
    @Override
    public boolean existe_personal(String idaccion) {
        con = null;
        rs = null;
        ps = null;
        
        query = "SQL_GET_INSUMO_EXISTE";
        boolean respuesta = false;
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            
            ps = con.prepareStatement(query);
            ps.setInt(1, 3);
            ps.setInt(2, Integer.parseInt(idaccion));
            rs = ps.executeQuery();

            while (rs.next()) {
                respuesta = true;
                break;
            }
           
        } catch (Exception e) {
            
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return respuesta;
    }
    
    @Override
    public String cargar_personal_bd(String idaccion) {
        con = null;
        rs = null;
        ps = null;

        query = "SQL_GET_INSUMO_BD";
        String respuestaJson = "{}";
        JsonObject obj = new JsonObject();
        Gson gson = new Gson();
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            
            ps = con.prepareStatement(query);
            ps.setInt(1, 3);
            ps.setInt(2, Integer.parseInt(idaccion));
            rs = ps.executeQuery();
            JsonObject fila = null;
            JsonArray arr = new JsonArray();
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("id", rs.getString("id"));
                fila.addProperty("id_tipo_insumo", rs.getString("id_tipo_insumo"));
                fila.addProperty("id_insumo", rs.getString("id_insumo"));
                fila.addProperty("insumo", rs.getString("insumo"));
                fila.addProperty("periodo_ini", rs.getString("periodo_ini"));
                fila.addProperty("periodo_fin", rs.getString("periodo_fin"));
                fila.addProperty("h_trabajo", rs.getString("h_trabajo"));
                
                arr.add(fila);
            }
            obj.add("rows", arr);
            respuestaJson = gson.toJson(obj);

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return respuestaJson;
    }
    
    @Override
    public JsonObject ingresar_personal(JsonObject info) {

        JsonObject respuesta = new JsonObject();
        JsonObject recorre = new JsonObject();
        StringStatement ss = null;
        con = null;
        ps = null;
        String mensaje = "";
        //query = "GuardarEncabezadoInsumo";
        //TransaccionService tsrv = null;
        int resp = 0, idinsumo = 0;
        int idCab = 0;

        try {

            JsonArray arr = info.getAsJsonArray("rows");
            con = this.conectarJNDI();
            con.setAutoCommit(false);
            for (int i = 0; i < arr.size(); i++) {
                respuesta = arr.get(i).getAsJsonObject();

                if (respuesta.get("id").getAsString().startsWith("nu_")) {

                    String query = "INSERTAR_NUEVO_INSUMO";

                    ps = con.prepareStatement(this.obtenerSQL(query));
                    
                    ps.setString(1, info.get("dstrct").getAsString());
                    ps.setInt(2, Integer.parseInt(info.get("idaccion").getAsString()));
                    ps.setInt(3, respuesta.get("id_tipo_insumo").getAsInt());
                    ps.setInt(4, respuesta.get("id_insumo").getAsInt());
                    ps.setString(5, respuesta.get("periodo_ini").getAsString());
                    ps.setString(6, respuesta.get("periodo_fin").getAsString());
                    ps.setFloat(7, Float.parseFloat(respuesta.get("h_trabajo").getAsString()));
                    ps.setString(8, info.get("usuario").getAsString());
                    ps.setString(9, info.get("usuario").getAsString());

                    ps.executeUpdate();
                    mensaje = "Insercion satisfactoria";
                    //con.commit();

                } else {

                    String query = "ACTUALIZAR_INSUMO";
                    con.setAutoCommit(false);

                    ps = con.prepareStatement(this.obtenerSQL(query));

                    ps.setString(1, respuesta.get("periodo_ini").getAsString());
                    ps.setString(2, respuesta.get("periodo_fin").getAsString());
                    ps.setFloat(3, Float.parseFloat(respuesta.get("h_trabajo").getAsString()));
                    ps.setString(4, info.get("usuario").getAsString());
                    ps.setInt(5, respuesta.get("id").getAsInt());
                    
                    ps.executeUpdate();

                    mensaje = "Edicion satisfactoria";

                }

            }
            con.commit();
            respuesta = new JsonObject();

            respuesta.addProperty(
                    "mensaje", mensaje);

        } catch (Exception exc) {
            respuesta = new JsonObject();
            exc.getMessage();
            respuesta.addProperty("error", "Error, no puede registrar productos ya guardados");
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
            return respuesta;
        }
    }
    
    ////////////////////////////////////////////
    
    @Override
    public String cargar_material(String idaccion) {
        con = null;
        rs = null;
        ps = null;
        int i = 0;

        query = "SQL_GET_MATERIAL";
        String respuestaJson = "{}";
        JsonObject obj = new JsonObject();
        Gson gson = new Gson();
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            
            ps = con.prepareStatement(query);
            ps.setInt(1, 1);
            ps.setInt(2, Integer.parseInt(idaccion));
            rs = ps.executeQuery();
            JsonObject fila = null;
            JsonArray arr = new JsonArray();
            while (rs.next()) {
                i= i+1;
                fila = new JsonObject();
                fila.addProperty("id", "nu_"+i);
                fila.addProperty("insumo", rs.getString("insumo"));
                fila.addProperty("id_tipo_insumo", rs.getString("id_tipo_insumo"));
                fila.addProperty("id_insumo", rs.getString("id_insumo"));
                fila.addProperty("id_unidad_medida", rs.getString("id_unidad_medida"));
                fila.addProperty("nombre_unidad_insumo", rs.getString("nombre_unidad_insumo"));
                fila.addProperty("cantidad", rs.getString("cantidad"));
                fila.addProperty("costo", rs.getString("costo"));
                arr.add(fila);
            }
            obj.add("rows", arr);
            respuestaJson = gson.toJson(obj);

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return respuestaJson;
    }
    
    @Override
    public boolean existe_material(String idaccion) {
        con = null;
        rs = null;
        ps = null;
        
        query = "SQL_GET_MATERIAL_EXISTE";
        boolean respuesta = false;
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            
            ps = con.prepareStatement(query);
            ps.setInt(1, 1);
            ps.setInt(2, Integer.parseInt(idaccion));
            rs = ps.executeQuery();

            while (rs.next()) {
                respuesta = true;
                break;
            }
           
        } catch (Exception e) {
            
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return respuesta;
    }
    
    @Override
    public String cargar_material_bd(String idaccion) {
        con = null;
        rs = null;
        ps = null;

        query = "SQL_GET_MATERIAL_BD";
        String respuestaJson = "{}";
        JsonObject obj = new JsonObject();
        Gson gson = new Gson();
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            
            ps = con.prepareStatement(query);
            ps.setInt(1, 1);
            ps.setInt(2, Integer.parseInt(idaccion));
            rs = ps.executeQuery();
            JsonObject fila = null;
            JsonArray arr = new JsonArray();
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("id", rs.getString("id"));
                fila.addProperty("id_tipo_insumo", rs.getString("id_tipo_insumo"));
                fila.addProperty("id_insumo", rs.getString("id_insumo"));
                fila.addProperty("insumo", rs.getString("insumo"));
                fila.addProperty("cantidad", rs.getString("cantidad"));
                fila.addProperty("costo", rs.getString("costo"));
                fila.addProperty("id_unidad_medida", rs.getString("id_unidad_medida"));
                fila.addProperty("nombre_unidad_insumo", rs.getString("nombre_unidad_insumo"));
                fila.addProperty("fecha_com", rs.getString("fecha_com"));
                fila.addProperty("criticidad", rs.getString("criticidad"));                
                arr.add(fila);
            }
            obj.add("rows", arr);
            respuestaJson = gson.toJson(obj);

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return respuestaJson;
    }
    
    @Override
    public JsonObject ingresar_material(JsonObject info) {

        JsonObject respuesta = new JsonObject();
        JsonObject recorre = new JsonObject();
        StringStatement ss = null;
        con = null;
        ps = null;
        String mensaje = "";
        //query = "GuardarEncabezadoInsumo";
        //TransaccionService tsrv = null;
        int resp = 0, idinsumo = 0;
        int idCab = 0;

        try {

            JsonArray arr = info.getAsJsonArray("rows");
            con = this.conectarJNDI();
            con.setAutoCommit(false);
            for (int i = 0; i < arr.size(); i++) {
                respuesta = arr.get(i).getAsJsonObject();

                if (respuesta.get("id").getAsString().startsWith("nu_")) {

                    String query = "INSERTAR_NUEVO_MATERIAL";

                    ps = con.prepareStatement(this.obtenerSQL(query));
                    
                    ps.setString(1, info.get("dstrct").getAsString());
                    ps.setInt(2, Integer.parseInt(info.get("idaccion").getAsString()));
                    ps.setInt(3, respuesta.get("id_tipo_insumo").getAsInt());
                    ps.setInt(4, respuesta.get("id_insumo").getAsInt());
                    ps.setFloat(5, Float.parseFloat(respuesta.get("cantidad").getAsString()));
                    ps.setFloat(6, Float.parseFloat(respuesta.get("costo").getAsString().replace("$", "")));
                    ps.setInt(7, Integer.parseInt(respuesta.get("id_unidad_medida").getAsString()));
                    ps.setString(8, respuesta.get("fecha_com").getAsString());
                    ps.setString(9, respuesta.get("criticidad").getAsString());
                    ps.setString(10, info.get("usuario").getAsString());
                    ps.setString(11, info.get("usuario").getAsString());

                    ps.executeUpdate();
                    mensaje = "Insercion satisfactoria";
                    //con.commit();

                } else {

                    String query = "ACTUALIZAR_MATERIAL";
                    con.setAutoCommit(false);

                    ps = con.prepareStatement(this.obtenerSQL(query));

                    ps.setString(1, respuesta.get("fecha_com").getAsString());
                    ps.setString(2, respuesta.get("criticidad").getAsString());
                    ps.setString(3, info.get("usuario").getAsString());
                    ps.setInt(4, respuesta.get("id").getAsInt());
                    
                    ps.executeUpdate();

                    mensaje = "Edicion satisfactoria";

                }

            }
            con.commit();
            respuesta = new JsonObject();

            respuesta.addProperty(
                    "mensaje", mensaje);

        } catch (Exception exc) {
            respuesta = new JsonObject();
            exc.getMessage();
            respuesta.addProperty("error", "Error, no puede registrar productos ya guardados");
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
            return respuesta;
        }
    }
    
    @Override
    public JsonObject cargar_combo_categoria(JsonObject info) {
        JsonObject respuesta = new JsonObject();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "";
        try {
            query = info.get("query").getAsJsonPrimitive().getAsString();
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            if (info.get("filtros") != null) {
                JsonArray ar = info.get("filtros").getAsJsonArray();
                for (int i = 0; i < ar.size(); i++) {
                    ps.setString(i + 1, ar.get(i).getAsString());
                }
            }
            rs = ps.executeQuery();
            //respuesta.addProperty("0", "Todos");
            while (rs.next()) {
                respuesta.addProperty(rs.getString("codigo"), rs.getString("nombre"));
            }
        } catch (Exception exc) {
            respuesta = new JsonObject();
            respuesta.addProperty("error", exc.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception ex) {
            }
            return respuesta;
        }
    }
    
    @Override
    public boolean existeTipoCategoria(String empresa, String nombre, String id) {
        con = null;
        rs = null;
        ps = null;
        boolean resp = false;
        query = "existeTipoCategoria";
        String filtro = (!id.equals("")?" AND id not in("+id+")":"");
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query).replaceAll("#filtro", filtro);

            ps = con.prepareStatement(query);

            byte[] bytes = nombre.getBytes("ISO-8859-1");
            nombre = new String(bytes, "ISO-8859-1");

            ps.setString(1, empresa);
            ps.setString(2, nombre);

            rs = ps.executeQuery();

            if (rs.next()) {
                resp = true;
            }
        } catch (Exception e) {
            try {
                throw new Exception("Error en existeTipoCategoria " + e.toString());
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return resp;
    }

    @Override
    public String guardarTipoCategoria(String empresa, String nombre, String puntaje, String usuario) {
        con = null;
        ps = null;
        query = "guardarTipoCategoria";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);

            byte[] bytes = nombre.getBytes("ISO-8859-1");
            nombre = new String(bytes, "ISO-8859-1");

            ps.setString(1, empresa);
            ps.setString(2, nombre);
            ps.setInt(3, Integer.parseInt(puntaje));
            ps.setString(4, usuario);

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            try {
                respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR guardarTipoCategoria \n" + e.getMessage());
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
            }
            return respuestaJson;
        }
    }
    
    //////////////////////////////////////
    
    @Override
    public String cargarCategoria(String empresa, String status, String categoria) {
        con = null;
        rs = null;
        ps = null;
        int i = 0;

        query = "cargarCategoria";
        String respuestaJson = "{}";
        JsonObject obj = new JsonObject();
        Gson gson = new Gson();
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            
            ps = con.prepareStatement(query);
            ps.setInt(1, Integer.parseInt(categoria));
            rs = ps.executeQuery();
            JsonObject fila = null;
            JsonArray arr = new JsonArray();
            while (rs.next()) {
                i= i+1;
                fila = new JsonObject();
                fila.addProperty("id", rs.getString("id"));
                fila.addProperty("nombre", rs.getString("nombre"));
                fila.addProperty("descripcion", rs.getString("descripcion"));
                fila.addProperty("puntaje", rs.getString("puntaje"));
                arr.add(fila);
            }
            obj.add("rows", arr);
            respuestaJson = gson.toJson(obj);

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return respuestaJson;
    }
    
    //////////////////////////////////////////////////////////////////////////
    
    @Override
    public boolean existeIndicador(String empresa, String nombre, String id) {
        con = null;
        rs = null;
        ps = null;
        boolean resp = false;
        query = "existeIndicador";
        String filtro = (!id.equals("")?" AND id not in("+id+")":"");
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query).replaceAll("#filtro", filtro);

            ps = con.prepareStatement(query);

            byte[] bytes = nombre.getBytes("ISO-8859-1");
            nombre = new String(bytes, "ISO-8859-1");

            ps.setString(1, empresa);
            ps.setString(2, nombre);

            rs = ps.executeQuery();

            if (rs.next()) {
                resp = true;
            }
        } catch (Exception e) {
            try {
                throw new Exception("Error en existeTipoCategoria " + e.toString());
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return resp;
    }

    @Override
    public String guardarIndicador(String empresa, String nombre, String descripcion, String puntaje, String categoria, String usuario) {
        con = null;
        ps = null;
        query = "guardarIndicador";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);

            byte[] bytes = nombre.getBytes("ISO-8859-1");
            nombre = new String(bytes, "ISO-8859-1");

            ps.setString(1, empresa);
            ps.setString(2, nombre);
            ps.setString(3, descripcion);
            ps.setInt(4, Integer.parseInt(puntaje));
            ps.setInt(5, Integer.parseInt(categoria));
            ps.setString(6, usuario);

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            try {
                respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR guardarTipoCategoria \n" + e.getMessage());
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
            }
            return respuestaJson;
        }
    }
    
    ////////////////////////////////////
    @Override
    public String cargar_riesgo_ci(String idaccion) {
        con = null;
        rs = null;
        ps = null;

        query = "SQL_GET_RIESGO_CI";
        String respuestaJson = "{}";
        JsonObject obj = new JsonObject();
        Gson gson = new Gson();
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
                    //.replace("#id", idaccion);
            
            ps = con.prepareStatement(query);

            ps.setInt(1, Integer.parseInt(idaccion));
            
            rs = ps.executeQuery();
            JsonObject fila = null;
            JsonArray arr = new JsonArray();
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("id", rs.getString("id"));
                fila.addProperty("id_tipo_categoria", rs.getString("id_tipo_categoria"));
                fila.addProperty("tipo_categoria", rs.getString("tipo_categoria"));
                fila.addProperty("descripcion", rs.getString("descripcion"));
                fila.addProperty("id_impacto", rs.getString("id_impacto"));
                fila.addProperty("impacto", rs.getString("impacto"));
                fila.addProperty("id_probabilidad", rs.getString("id_probabilidad"));
                fila.addProperty("probabilidad", rs.getString("probabilidad"));
                fila.addProperty("puntaje", rs.getString("puntaje"));
                arr.add(fila);
            }
            obj.add("rows", arr);
            respuestaJson = gson.toJson(obj);

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return respuestaJson;
    }
    
    @Override
    public String guardar_riesgo_ci(String categoria, String impacto, String probabilidad, String descripcion, String puntaje,
            Usuario usuario, String idaccion) {
        con = null;
        ps = null;
        query = "SQL_SET_RIESGO";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, usuario.getDstrct());
            ps.setInt(2, Integer.parseInt(idaccion));
            ps.setInt(3, Integer.parseInt(categoria));
            ps.setInt(4, Integer.parseInt(impacto));
            ps.setInt(5, Integer.parseInt(probabilidad));
            ps.setString(6, descripcion);
            ps.setInt(7, Integer.parseInt(puntaje));
            ps.setString(8, usuario.getLogin());
            ps.setString(9, usuario.getLogin());            

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
            throw new SQLException("ERROR guardarTipoDocumento \n" + e.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }
    
    @Override
    public String cargarImpacto(String empresa, String status, String categoria) {
        con = null;
        rs = null;
        ps = null;
        int i = 0;

        query = "cargarCategoria";
        String respuestaJson = "{}";
        JsonObject obj = new JsonObject();
        Gson gson = new Gson();
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            
            ps = con.prepareStatement(query);
            ps.setInt(1, Integer.parseInt(categoria));
            rs = ps.executeQuery();
            JsonObject fila = null;
            JsonArray arr = new JsonArray();
            while (rs.next()) {
                
                obj.addProperty(rs.getString("id"), rs.getString("nombre"));
                
            }
            
            respuestaJson = gson.toJson(obj);

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return respuestaJson;
    }
    
    @Override
    public String actualizar_riesgo_ci(String id, String categoria, String impacto, String probabilidad, 
            String descripcion, String puntaje, Usuario usuario, String idaccion) {
        con = null;
        ps = null;
        query = "SQL_UPDATE_RIESGO";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setInt(1, Integer.parseInt(categoria));
            ps.setInt(2, Integer.parseInt(impacto));
            ps.setInt(3, Integer.parseInt(probabilidad));
            ps.setString(4, descripcion);
            ps.setInt(5, Integer.parseInt(puntaje));
            ps.setString(6, usuario.getLogin());
            ps.setInt(7, Integer.parseInt(id));
            
            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
            throw new SQLException("ERROR actualizarTipoDocumento \n" + e.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }
    
    @Override
    public String anular_riesgo_ci(String id, Usuario usuario) {
        con = null;
        ps = null;
        query = "SQL_ANULAR_RIESGO";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, usuario.getLogin());
            ps.setString(2, id);

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
            throw new SQLException("ERROR actualizarTipoDocumento \n" + e.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }
    
    
    @Override
    public String actualizar_impacto(String id, String nombre, 
            String descripcion, String puntaje, Usuario usuario) {
        con = null;
        ps = null;
        query = "SQL_UPDATE_IMPACTO";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, nombre);
            ps.setString(2, descripcion);
            ps.setInt(3, Integer.parseInt(puntaje));
            ps.setString(4, usuario.getLogin());
            ps.setInt(5, Integer.parseInt(id));
            
            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
            throw new SQLException("ERROR actualizarTipoDocumento \n" + e.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }
    
    @Override
    public String anular_impacto(String id, Usuario usuario) {
        con = null;
        ps = null;
        query = "SQL_ANULAR_IMPACTO";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, usuario.getLogin());
            ps.setString(2, id);

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
            throw new SQLException("ERROR actualizarTipoDocumento \n" + e.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }
}
    ////////////////////////////////////

