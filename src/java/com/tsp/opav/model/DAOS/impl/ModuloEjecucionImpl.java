/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.opav.model.DAOS.impl;

/**
 *
 * @author Ing.William Siado T
 */
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.lowagie.text.BadElementException;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.html.simpleparser.HTMLWorker;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfPageEventHelper;
import com.lowagie.text.pdf.PdfWriter;
import com.sun.xml.rpc.processor.modeler.j2ee.xml.string;
import com.tsp.opav.model.DAOS.MainDAO;
import com.tsp.opav.model.DAOS.ModuloEjecucionDAO;
import com.tsp.operation.model.beans.BeansAdministracion;


/*Beans*/
import com.tsp.operation.model.beans.StringStatement;
import com.tsp.operation.model.beans.Usuario;
import java.awt.Color;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
////////////////

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ModuloEjecucionImpl extends MainDAO implements ModuloEjecucionDAO {

    private Connection con = null;
    private PreparedStatement ps = null;
    private ResultSet rs = null;
    private String query = "";
    private StringStatement st = null;

    public ModuloEjecucionImpl(String dataBaseName) {
        super("ModuloEjecucionDAO.xml", dataBaseName);
    }

    @Override
    public String cargar_Wbs_Ejecucion(String id_solicitud) {
        Gson gson = new GsonBuilder().serializeNulls().create();
        con = null;
        ps = null;
        rs = null;
        JsonObject objAreas = new JsonObject();
        JsonObject objDisciplinas = new JsonObject();
        JsonObject objCapitulos = new JsonObject();
        query = "SQL_GET_NODO_RAIZ";
        String areas_proyecto = "", disciplinas_area = "", capitulos = "";
        String respuestaJson = "{}";
        JsonArray arr = new JsonArray();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, id_solicitud);
            rs = ps.executeQuery();
            JsonObject fila = null;
            int contador = 1, indice = 1, pindex = 0;
            String padre = "";
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("id", contador);
                fila.addProperty("parent", (rs.getString("padre")));
                fila.addProperty("title", rs.getString("title"));
                fila.addProperty("groupTitle", rs.getString("grouptitle"));
                fila.addProperty("groupTitleColor", rs.getString("groupTitleColor"));
                fila.addProperty("description", rs.getString("descripcion"));
                fila.addProperty("image", "/fintra/images/botones/iconos/Project.png");
                fila.addProperty("nivel", rs.getString("nivel"));
                fila.addProperty("recordId", rs.getString("id_solicitud"));
                fila.addProperty("otro", "");
                fila.addProperty("buttonsType", "Proyecto");
                arr.add(fila);
                contador++;
            }
            respuestaJson = "{\"total\":\"" + contador + "\",\"nodes\":" + gson.toJson(arr) + "}";
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return respuestaJson;
        }
    }

    @Override
    public String cargarComboGenerico(String op, String param) {
        con = null;
        rs = null;
        ps = null;
        String filtro = "";
        switch (op) {
            case "1":
                query = "SQL_CARGAR_AREAS_PROYECTO";
                filtro = " AND id_solicitud = " + param;
                break;
            case "2":
                query = "SQL_CARGAR_DISCIPLINAS_AREAS";
                filtro = (!param.equals("")) ? " AND id_area_proyecto = " + param : "";
                break;
            case "3":
                query = "SQL_CARGAR_CAPITULOS_DISCIPLINA";
                filtro = (!param.equals("")) ? " AND id_disciplina_area = " + param : "";
                break;
            case "4":
                query = "SQL_CARGAR_ACTIVIDADES_CAPITULO";
                filtro = (!param.equals("")) ? " AND id_capitulo = " + param : "";
                break;
            case "5":
                query = "SQL_CARGAR_TIPO_INSUMO";
                filtro = "";
                break;
            case "6":
                query = "SQL_CARGAR_UNIDAD_MEDIDA";
                filtro = "";
                break;
            default:
                throw new AssertionError();
        }

        String respuestaJson = "{}";

        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query).replaceAll("#filtro", filtro);

            ps = con.prepareStatement(query);

            rs = ps.executeQuery();

            while (rs.next()) {
                obj.addProperty(rs.getString("id"), rs.getString("descripcion"));

            }
            respuestaJson = gson.toJson(obj);

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return respuestaJson;

    }

    @Override
    public String cargar_Responsables_Ejecucion(String id_solicitud) {
        con = null;
        rs = null;
        ps = null;
        query = "SQL_GET_RESPONSABLES_EJECUCION";
        String respuestaJson = "{}";
        JsonObject obj = new JsonObject();
        Gson gson = new Gson();
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);
            ps.setString(1, id_solicitud);
            rs = ps.executeQuery();
            JsonObject fila = null;
            JsonArray arr = new JsonArray();
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("id", rs.getString("id"));
                fila.addProperty("responsable", rs.getString("responsable"));
                arr.add(fila);
            }
            obj.add("rows", arr);
            respuestaJson = gson.toJson(obj);

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return respuestaJson;
    }

    @Override
    public String cargarGridFacturacion(String id_solicitud) {
        Gson gson = new Gson();
        con = null;
        ps = null;
        rs = null;
        String respuestaJson = "{}";
        String filtro = "";
        JsonObject obj = new JsonObject();
        JsonArray lista = null;
        JsonObject jsonObject = null;

        query = "cargarGrillaFacturacion";

        JsonArray arr = new JsonArray();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query).replaceAll("#filtro", filtro));
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                jsonObject = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    jsonObject.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(jsonObject);
            }

            respuestaJson = new Gson().toJson(lista);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return gson.toJson(obj);
        }

    }

    @Override
    public String prepararSolicitudParaFacturar(String id_solicitud, String valor_facturar, String valor_material, Usuario usuario) {
        con = null;
        ps = null;
        rs = null;
        query = "SQL_PREPARAR_SOLICITUD_FACTURAR";
        String retorno = "";
        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);
            ps.setString(1, id_solicitud);
            ps.setDouble(2, Double.parseDouble(valor_facturar));
            ps.setDouble(3, Double.parseDouble(valor_material));
            ps.setString(4, usuario.getLogin());
            rs = ps.executeQuery();
            if (rs.next()) {
                retorno = rs.getString("retorno");
            }

        } catch (Exception e) {
            retorno = "ERROR";
            e.printStackTrace();
            System.out.println(e.toString());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
        }
        return retorno;
    }

    @Override
    public String insertar_Responsables_Ejecucion(String id_solicitud, String id_usuario_, Usuario usuario) {
        Gson gson = new Gson();
        con = null;
        ps = null;
        rs = null;
        String respuestaJson = "{}";
        JsonObject obj = new JsonObject();
        JsonObject jsonObject = null;

        query = "SQL_SET_RESPONSABLES_EJECUCION";

        
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, id_solicitud);
            ps.setString(2, id_usuario_);
            ps.setString(3, usuario.getLogin());
            
            rs = ps.executeQuery();
            jsonObject = new JsonObject();
            while (rs.next()) {
                jsonObject.addProperty("respuesta", "OK");
            }

            respuestaJson = jsonObject.toString();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return respuestaJson;
        }
    }

    @Override
    public String actualizar_Responsables_Ejecucions(String id, String nombre, Usuario usuario) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String anular_Responsables_Ejecucion(String id, Usuario usuario) {
        con = null;
        ps = null;
        query = "SQL_ANULAR_RESPONSABLES_EJECUCION";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, usuario.getLogin());
            ps.setString(2, id);
            

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
            throw new SQLException("ERROR actualizarTipoDocumento \n" + e.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    @Override
    public String cargar_Proyectos_Asignados(Usuario usuario) {
        usuario.getPerfil();
        Gson gson = new Gson();
        con = null;
        ps = null;
        rs = null;
        JsonObject obj = new JsonObject();
        String query = "SQL_CARGAR_PROYECTOS_ASIGNADOS";

        JsonArray arr = new JsonArray();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, usuario.getLogin());
            rs = ps.executeQuery();
            JsonObject fila = null;
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("id", rs.getString("id"));
                fila.addProperty("id_solicitud", rs.getString("id_solicitud"));
                fila.addProperty("nombre_proyecto", rs.getString("nombre_proyecto"));
                arr.add(fila);
            }
            obj.add("rows", arr);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return gson.toJson(obj);
        }
    }

    @Override
    public String cargar_Proyecto_Ejecucion(String id_solicitud, String proceso, String opc, String cond, String id_lote, Usuario usuario) {
        String condicion2 = "", condicion3 = "" , id_usuario = "";

        condicion3 = (id_lote.equals("")) ? "" : " AND GA.id_lote_ejecucion !=  " + id_lote;
        id_usuario = usuario.getLogin();
        switch (proceso) {

            case "0":
                id_usuario = "MASTER";
                break;

            case "2":
                condicion2 = " AND cantidad_apu_actual>0 ";
                break;
        }

        switch (opc) {

            case "1":
                cond = " AND A.id_area = " + cond;
                break;

            case "2":
                cond = " AND A.id_disciplina_area = " + cond;
                break;

            case "3":
                cond = " AND A.id_capitulo = " + cond;
                break;

            case "4":
                cond = " AND A.id_actividades_capitulo = " + cond;
                break;
            default:
                break;
        }
        String consulta;
        Gson gson = new Gson();
        con = null;
        ps = null;
        rs = null;
        JsonObject obj = new JsonObject();
        String query = "SQL_CARGAR_PROYECTO_EJECUCION";

        JsonArray arr = new JsonArray();
        try {
            con = this.conectarJNDI();
            consulta = this.obtenerSQL(query).replaceAll("#condicion1", cond).replaceAll("#condicion2", condicion2).replaceAll("#condicion3", condicion3);
            ps = con.prepareStatement(consulta);
            ps.setString(1, id_solicitud);
            ps.setString(2, id_solicitud);
            ps.setString(3, id_usuario);
            rs = ps.executeQuery();
            JsonObject fila = null;
            int id = 0;
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("id", id);
                fila.addProperty("id_disciplina_area", rs.getString("id_disciplina_area"));
                fila.addProperty("descripcion_area", rs.getString("descripcion_area"));
                fila.addProperty("id_disciplina", rs.getString("id_disciplina"));
                fila.addProperty("descripcion_displina", rs.getString("descripcion_displina"));
                fila.addProperty("id_capitulo", rs.getString("id_capitulo"));
                fila.addProperty("descripcion_capitulo", rs.getString("descripcion_capitulo"));
                fila.addProperty("id_actividad", rs.getString("id_actividad"));
                fila.addProperty("descripcion_actividad", rs.getString("descripcion_actividad"));
                fila.addProperty("id_actividades_capitulo", rs.getString("id_actividades_capitulo"));
                fila.addProperty("id_rel_actividades_apu", rs.getString("id_rel_actividades_apu"));
                fila.addProperty("id_apu", rs.getString("id_apu"));
                fila.addProperty("descripcion_apu", rs.getString("descripcion_apu"));
                fila.addProperty("unidad_medida_apu", rs.getString("unidad_medida_apu"));
                fila.addProperty("nombre_unidad_medida_apu", rs.getString("nombre_unidad_medida_apu"));
                fila.addProperty("cantidad_apu", rs.getString("cantidad_apu"));
                fila.addProperty("cantidad_apu_actual", rs.getString("cantidad_apu_actual"));
                fila.addProperty("cantidad_apu_ejecutado", rs.getString("cantidad_apu_ejecutado"));
                fila.addProperty("valor_apu_ejecutado", rs.getString("valor_apu_ejecutado"));
                fila.addProperty("valor_apu_presupuesto", rs.getString("valor_apu_presupuesto"));
                fila.addProperty("valor_apu_actual", rs.getString("valor_apu_actual"));
                fila.addProperty("total_apu_presupuesto", rs.getString("total_apu_presupuesto"));
                id++;
                arr.add(fila);
            }
            obj.add("rows", arr);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return gson.toJson(obj);
        }
    }

    @Override
    public String cargar_Insumos_Apu(String id_solicitud, String id_rel_actividades_apu, String id_apu, String unidad_medida_apu, String usuario) {
        Gson gson = new Gson();
        con = null;
        ps = null;
        rs = null;
        JsonObject obj = new JsonObject();
        String query = "SQL_CARGAR_INSUMOS_APU";

        JsonArray arr = new JsonArray();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(obtenerSQL(query));
            
            
            ps.setString(1, id_solicitud);
            ps.setString(2, id_solicitud);
            ps.setString(3, id_rel_actividades_apu);
            ps.setString(4, id_apu);
            ps.setString(5, unidad_medida_apu);
            ps.setString(6, usuario);

            
            rs = ps.executeQuery();
            JsonObject fila = null;
            while (rs.next()) {
                fila = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    fila.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                arr.add(fila);
            }
            obj.add("rows", arr);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return gson.toJson(obj);
        }
    }

    @Override
    public String cargar_Insumos_Apu_Master(String id_solicitud, String id_rel_actividades_apu, String id_apu, String unidad_medida_apu, String usuario) {
        Gson gson = new Gson();
        con = null;
        ps = null;
        rs = null;
        JsonObject obj = new JsonObject();
        String query = "SQL_CARGAR_INSUMOS_APU_MASTER";

        JsonArray arr = new JsonArray();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(obtenerSQL(query));
            
            
            ps.setString(1, id_solicitud);
            ps.setString(2, id_rel_actividades_apu);
            ps.setString(3, id_apu);
            ps.setString(4, unidad_medida_apu);
            ps.setString(5, usuario);

            
            rs = ps.executeQuery();
            JsonObject fila = null;
            while (rs.next()) {
                fila = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    fila.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                arr.add(fila);
            }
            obj.add("rows", arr);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return gson.toJson(obj);
        }
    }

    @Override
    public String guardar_Cantidades_Insumos_Actual(JsonObject rows, String usuario) {
        con = null;
        ps = null;

        Statement stmt = null;
        StringStatement st = null;
        String respuestaJson = "{}";
        try {

            JsonArray arr = rows.getAsJsonArray("rows");
            JsonObject res;
            con = this.conectarJNDI();

            con.setAutoCommit(false);
            stmt = con.createStatement();

            for (int i = 0; i < arr.size(); i++) {
                query = this.obtenerSQL("SQL_SET_CANTIDADES_INSUMOS_ACTUAL");
                st = new StringStatement(query, true);

                res = arr.get(i).getAsJsonObject();

                st.setDouble(1, numero_sin_comas(res.get("cantidad_apu_actual").getAsString()));
                st.setDouble(2, numero_sin_comas(res.get("cantidad_insumo_actual").getAsString()));
                st.setDouble(3, numero_sin_comas(res.get("valor_insumo_actual").getAsString()));
                st.setDouble(4, numero_sin_comas(res.get("porc_avance_apu").getAsString()));
                st.setString(5, usuario);
                st.setInt(6, res.get("id").getAsInt());

                stmt.addBatch(st.getSql());

            }
            stmt.executeBatch();
            stmt.clearBatch();
            con.commit();

            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            con.rollback();
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
            throw new SQLException("ERROR AL GUARDAR CANTIDADES ACTUALES INSUMOS \n" + e.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    @Override
    public String guardar_Cantidades_Insumos_Actual_Master(JsonObject rows, String usuario) {
        con = null;
        ps = null;

        Statement stmt = null;
        StringStatement st = null;
        String respuestaJson = "{}";
        try {

            JsonArray arr = rows.getAsJsonArray("rows");
            JsonObject res;
            con = this.conectarJNDI();

            con.setAutoCommit(false);
            stmt = con.createStatement();

            for (int i = 0; i < arr.size(); i++) {
                query = this.obtenerSQL("SQL_SET_CANTIDADES_INSUMOS_ACTUAL_MASTER");
                st = new StringStatement(query, true);

                res = arr.get(i).getAsJsonObject();

                st.setDouble(1, numero_sin_comas(res.get("cantidad_apu").getAsString()));
                st.setDouble(2, numero_sin_comas(res.get("cantidad_insumo").getAsString()));
                st.setDouble(3, numero_sin_comas(res.get("valor_insumo_total").getAsString()));
                st.setDouble(4, numero_sin_comas(res.get("cantidad_insumo_total").getAsString()));
                st.setString(5, usuario);
                st.setInt(6, res.get("id").getAsInt());

                stmt.addBatch(st.getSql());

            }
            stmt.executeBatch();
            stmt.clearBatch();
            con.commit();

            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            con.rollback();
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
            throw new SQLException("ERROR AL GUARDAR CANTIDADES ACTUALES INSUMOS \n" + e.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    public double numero_sin_comas(String x) {
        return Double.parseDouble(x.replaceAll(",", ""));

    }

    @Override
    public String insertar_Apu_Wbs_Ejecucion(JsonObject json, Usuario usuario) {
        Gson gson = new Gson();
        con = null;
        ps = null;
        rs = null;
        String respuestaJson="{}";
        query = "SQL_INSERTAR_APU_WBS_EJECUCION";

        JsonArray arr = new JsonArray();
        
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));  
            
            ps.setString(1, json.get("id_solicitud").getAsString());
            ps.setString(2, json.get("id_area").getAsString());
            ps.setString(3, json.get("id_disciplina_area").getAsString());
            ps.setString(4, json.get("id_capitulo").getAsString());
            ps.setString(5, json.get("id_actividades_capitulo").getAsString());
            ps.setString(6,  json.get("apu").getAsJsonObject().get("id").getAsString());
            ps.setInt(7,  json.get("cantidad").getAsInt());
            ps.setString(8, usuario.getLogin());

            
            rs = ps.executeQuery();
            
     
            respuestaJson = "{\"respuesta\":\"OK\"}";
            //respuestaJson = new Gson().toJson(lista);

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
            e.printStackTrace();
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return respuestaJson;
        }
    }

    
    public JsonArray detalleApu(JsonObject info) {
        Gson gson = new Gson();
        con = null;
        ps = null;
        rs = null;
        String respuestaJson = "{}";
        JsonObject obj = new JsonObject();
        JsonArray lista = null;
        JsonObject jsonObject = null;

        query = "SQL_DETALLE_APU";

        JsonArray arr = new JsonArray();
        
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, info.get("apu").getAsJsonObject().get("id").getAsString());
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                jsonObject = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    jsonObject.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(jsonObject);
            }

            //respuestaJson = new Gson().toJson(lista);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return lista;
        }

    }

    
    @Override
    public String cargar_Lotes_Proyecto_Usuario(String id_solicitud, Usuario usuario) {
        con = null;
        rs = null;
        ps = null;

        query = "SQL_GET_LOTES_PROYECTO_USUARIO";
        String respuestaJson = "{}";
        JsonObject obj = new JsonObject();
        Gson gson = new Gson();
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);
            ps.setString(1, id_solicitud);
            ps.setString(2, usuario.getLogin());
            rs = ps.executeQuery();
            JsonObject fila = null;
            JsonArray arr = new JsonArray();
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("id", rs.getString("id"));
                fila.addProperty("no_lote", rs.getString("no_lote"));
                fila.addProperty("descripcion", rs.getString("descripcion"));
                fila.addProperty("creation_date", rs.getString("creation_date").substring(0, 10));
                arr.add(fila);
            }
            obj.add("rows", arr);
            respuestaJson = gson.toJson(obj);

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return respuestaJson;
    }

    @Override
    public String set_Lote_Wbs_Ejecucion(String id_solicitud, String id_lote, Usuario usuario) {

        con = null;
        ps = null;
        rs = null;
        query = "SQL_SET_LOTE_WBS_EJECUCION";
        String respuestaJson = "{}";
        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);
            ps.setInt(1, Integer.parseInt(id_solicitud));
            ps.setInt(2, Integer.parseInt(id_lote));
            rs = ps.executeQuery();
            if (rs.next()) {
                respuestaJson = "{\"respuesta\":\"OK\"}";
            }

        } catch (Exception e) {
            respuestaJson = "{\"respuesta\":\"ERROR\"}";
            e.printStackTrace();
            System.out.println(e.toString());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
        }
        return respuestaJson;

    }

    @Override
    public String guardar_Lote_Ejecucion(String id_solicitud, String descripcion, JsonObject info, Usuario usuario) {

        con = null;
        ps = null;
        int resp = 0, idCab = 0;
        Statement stmt = null;
        String respuestaJson = "{}";

        try {
            String id_lote = get_serie_lote();
            JsonObject objeto = new JsonObject();
            PreparedStatement ps = null;
            con = this.conectarJNDI();
            String query = "SQL_GUARDAR_LOTE_EJECUCION";
            con.setAutoCommit(false);
            ps = con.prepareStatement(this.obtenerSQL(query), Statement.RETURN_GENERATED_KEYS);
            ps.setInt(1, Integer.parseInt(id_solicitud));
            ps.setString(2, id_lote);
            ps.setString(3, descripcion);
            ps.setString(4, usuario.getLogin());

            resp = ps.executeUpdate();
            stmt = con.createStatement();
            if (resp > 0) {
                ResultSet rs = ps.getGeneratedKeys();
                if (rs.next()) {
                    idCab = rs.getInt(1);
                    stmt.addBatch(guardar_Detalle_Asignacion_Puntaje(idCab, id_solicitud, usuario));
                }
                stmt.executeBatch();
                stmt.clearBatch();
            }
            
            movimientoActasWbsejecucion(String.valueOf(idCab));
            
            con.commit();
            respuestaJson = "{\"respuesta\":\"OK\"}";
            
        } catch (Exception exc) {
            con.rollback();
            respuestaJson = "{\"Error\":\" " +exc.getMessage() +" \"}"; 
        } finally {
            try {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException e) {
                        throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                    }
                }
                if (con != null) {
                    try {
                        con.setAutoCommit(true);
                        this.desconectar(con);
                    } catch (SQLException e) {
                        throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage());
                    }
                }
            } catch (Exception ex) {
            }
            return respuestaJson;
        }

    }

    public String guardar_Detalle_Asignacion_Puntaje(int idCab, String id_solicitud, Usuario usuario) {
        String cadena = "";
        String query = "SQL_GUARDAR_DETALLE_ASIGNACION_PUNTAJE";
        String sql = "";
        StringStatement st = null;
        sql = this.obtenerSQL(query);
        st = new StringStatement(sql, true);

        try {

            st.setInt(1, idCab);
            st.setInt(2, 1);
            st.setString(3, usuario.getLogin());
            st.setInt(4, Integer.parseInt(id_solicitud));
            st.setString(5, usuario.getLogin());
            cadena += st.getSql();

        } catch (Exception e) {
            System.out.println("Error en SQL_INSERTARDETALLE_ASIGNACION_PUNTAJE " + e.toString());
            e.printStackTrace();
        }
        return cadena;

    }

    public String guardar_insumo_adicional(int idCab, String id_solicitud, Usuario usuario) {
        String cadena = "";
        String query = "SQL_GUARDAR_INSUMO_ADICIONAL";
        String sql = "";
        StringStatement st = null;
        sql = this.obtenerSQL(query);
        st = new StringStatement(sql, true);

        try {

            st.setInt(1, idCab);
            st.setInt(2, 2);
            st.setString(3, usuario.getLogin());
            st.setInt(4, Integer.parseInt(id_solicitud));

            cadena += st.getSql();

        } catch (Exception e) {
            System.out.println("Error en SQL_INSERTARDETALLE_ASIGNACION_PUNTAJE " + e.toString());
            e.printStackTrace();
        }
        return cadena;

    }
    
    private void movimientoActasWbsejecucion(String _id_lote) throws Exception {
        rs=null;
        String query = "SQL_MOVIMIENTO_ACTAS_WBSEJECUCION";
        query = this.obtenerSQL(query);
        try {

            ps = con.prepareStatement(query);
            ps.setString(1, _id_lote);
           
            rs = ps.executeQuery();
            JsonObject objetoJson;
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
            }

        } catch (Exception e) {

            throw new Exception("Error : Existio problemas en el S.P.  opav.sl_crear_movimiento_actas_wbsejecucion ::: " + e.getMessage());
        }
    }

    @Override
    public String editar_Lote_Ejecucion(String id_solicitud, String id_lote, String descripcion, JsonObject info, Usuario usuario) {
        con = null;
        ps = null;

        Statement stmt = null;
        String respuestaJson = "{}";

        try {
            PreparedStatement ps = null;
            con = this.conectarJNDI();
            con.setAutoCommit(false);

            stmt = con.createStatement();
            stmt.addBatch("update opav.sl_lote_ejecucion set descripcion = ' " + descripcion + "' where id = " + id_lote);
            stmt.addBatch("delete from opav.sl_lote_ejecucion_detalle where id_lote_ejecucion = " + id_lote);
            JsonArray arr = info.getAsJsonArray("datosGrillas");

            stmt.addBatch(guardar_Detalle_Asignacion_Puntaje(Integer.parseInt(id_lote), id_solicitud, usuario));

            stmt.executeBatch();
            stmt.clearBatch();
            respuestaJson = "{\"respuesta\":\"OK\"}";

            con.commit();
        } catch (Exception exc) {
            con.rollback();
            exc.getMessage();
        } finally {
            try {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException e) {
                        throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                    }
                }
                if (con != null) {
                    try {
                        con.setAutoCommit(true);
                        this.desconectar(con);
                    } catch (SQLException e) {
                        throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage());
                    }
                }
            } catch (Exception ex) {
            }
            return respuestaJson;
        }

    }

    public String get_serie_lote() throws SQLException {
        con = null;
        rs = null;
        ps = null;

        query = "SQL_GET_SERIE_LOTE";
        String id_lote = "";
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            rs = ps.executeQuery();

            while (rs.next()) {
                id_lote = rs.getString("id_lote");
                break;
            }

        } catch (Exception e) {
            throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return id_lote;
    }

    @Override
    public String nuevo_Lote_Ejecucion(String id_solicitud, Usuario usuario) {
        con = null;
        ps = null;
        rs = null;
        query = "SQL_SET_WBS_EJECUCION_0";
        String respuestaJson = "{}";
        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);
            ps.setInt(1, Integer.parseInt(id_solicitud));
            ps.setString(2, usuario.getLogin());
            rs = ps.executeQuery();
            if (rs.next()) {
                respuestaJson = "{\"respuesta\":\"OK\"}";
            }

        } catch (Exception e) {
            respuestaJson = "{\"respuesta\":\"ERROR\"}";
            e.printStackTrace();
            System.out.println(e.toString());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
        }
        return respuestaJson;
    }

    @Override
    public String crear_WBS_Ejecucion(String id_solicitud ) {
        con = null;
        ps = null;
        query = "SQL_CREAR_WBS_EJECUCION_MASTER";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, id_solicitud);
            rs = ps.executeQuery();

            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
            throw new SQLException("ERROR actualizarTipoDocumento \n" + e.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }


    @Override
    public String crear_WBS_Ejecucion(String id_solicitud , Usuario usuario  ) {
        con = null;
        ps = null;
        query = "SQL_CREAR_WBS_EJECUCION";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, id_solicitud);
            ps.setString(2, usuario.getLogin());
            rs = ps.executeQuery();

            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
            throw new SQLException("ERROR actualizarTipoDocumento \n" + e.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    @Override
    public String imprimir_Actas(String id_solicitud, String ids_actas, Usuario usuario) {

        ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");//se consiguen los datos de db.properties
        String ruta = rb.getString("ruta") + "/images/multiservicios/" + usuario.getLogin() + "/";//se establece la ruta de la imagen 
        String directorioOrigen = rb.getString("ruta") + "/images/multiservicios/" + usuario.getLogin() + "/";//se establece la ruta de la imagen 

        con = null;
        ps = null;
        rs = null;
        String sql = "";
        query = "SQL_GET_ACTAS";
        String nomarchivo = "";
        String html_doc = "", fecha_corte = "";
        rb = null;
        String respuestaJson = "{}";
        try {
            this.createDir(directorioOrigen);
            DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd HH-mm-ss");
            //get current date time with Date()
            Date date = new Date();
            nomarchivo = "Acta_" + dateFormat.format(date) + ".pdf";
            //System.out.println(dateFormat.format(date));
            rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");

            Document document = new Document(PageSize.LEGAL.rotate(), 85, 85, 15, 52);

            HTMLWorker htmlWorker = new HTMLWorker(document);

            PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(ruta + "/" + nomarchivo));

            Font ff = new Font(Font.TIMES_ROMAN, 14, Font.BOLD, new Color(255, 255, 255));

            Font f = new Font(Font.TIMES_ROMAN, 12, Font.NORMAL);
            Font f1 = new Font(Font.TIMES_ROMAN, 12, Font.BOLD);
            Font f2 = new Font(Font.TIMES_ROMAN, 8, Font.NORMAL);
            Font f3 = new Font(Font.TIMES_ROMAN, 8, Font.BOLD);
            Font f4 = new Font(Font.TIMES_ROMAN, 10, Font.NORMAL);

            Font fb = new Font(Font.TIMES_ROMAN, 12, Font.NORMAL, new Color(255, 255, 255));
            Font f1b = new Font(Font.TIMES_ROMAN, 12, Font.BOLD, new Color(255, 255, 255));
            Font f2b = new Font(Font.TIMES_ROMAN, 8, Font.NORMAL, new Color(255, 255, 255));
            Font f3b = new Font(Font.TIMES_ROMAN, 8, Font.BOLD, new Color(255, 255, 255));
            Font f4b = new Font(Font.TIMES_ROMAN, 10, Font.NORMAL, new Color(255, 255, 255));

            FooterPDF footer = new FooterPDF(rb);
            pdfWriter.setPageEvent(footer);

            document.open();
            document.addAuthor(usuario.getLogin());
            document.addCreator("FINTRA S.A.");
            document.addCreationDate();
            document.addTitle("Actas - Selectrik");

            Image img = Image.getInstance(rb.getString("ruta") + "/images/login/logo_Selectrik2.png");
            //img.setAbsolutePosition(930f, 410f);
            img.scalePercent(48);

            con = this.conectarJNDI();

            PdfPTable table = new PdfPTable(12);
            table.setWidths(new int[]{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1});
            table.setWidthPercentage(100);
            PdfPCell cell;
            //ENCABEZADOS DE LA TABLA

            String no_lote = "";
            query = this.obtenerSQL(query).replaceAll("#registro1", id_solicitud).replaceAll("#registro2", ids_actas);
            ps = con.prepareStatement(query);
            rs = ps.executeQuery();

            String observaciones = "";

            boolean x = false;

            while (rs.next()) {
                if (!no_lote.equals(rs.getString("no_lote"))) {

                    if (x) {

                        cell = new PdfPCell(new Phrase(String.valueOf("Descripcion : \n" + observaciones), f2));
                        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                        cell.setColspan(12);
                        table.addCell(cell);
                        observaciones = rs.getString("lote_descripcion");

                        document.add(table);
                        document.newPage();
                        table = new PdfPTable(12);
                        table.setWidths(new int[]{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1});
                        table.setWidthPercentage(100);

                    } else {
                        observaciones = rs.getString("lote_descripcion");
                        x = true;
                    }

                    // row 1, cell 1
                    cell = new PdfPCell(img);
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setColspan(2);
                    cell.setBackgroundColor(new Color(255, 145, 0));
                    table.addCell(cell);

                    // row 1, cell 2
                    cell = new PdfPCell(new Phrase("REPORTE DIARIO AVANCE DE OBRA", ff));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setColspan(8);
                    cell.setBackgroundColor(new Color(255, 145, 0));
                    table.addCell(cell);

                    // row 1, cell 2
                    cell = new PdfPCell(new Phrase("CALIDAD", f3b));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setColspan(2);
                    cell.setBackgroundColor(new Color(255, 145, 0));
                    table.addCell(cell);

                    cell = new PdfPCell(new Phrase(String.valueOf("Proyecto :"), f3));
                    cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                    cell.setColspan(1);
                    table.addCell(cell);

                    cell = new PdfPCell(new Phrase(String.valueOf(rs.getString("nombre_proyecto")), f2));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setColspan(3);
                    table.addCell(cell);

                    cell = new PdfPCell(new Phrase(String.valueOf("Fecha informe :"), f3));
                    cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                    cell.setColspan(1);
                    table.addCell(cell);

                    cell = new PdfPCell(new Phrase(String.valueOf(rs.getString("fecha_informe")), f2));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setColspan(3);
                    table.addCell(cell);

                    cell = new PdfPCell(new Phrase(String.valueOf("Preparado por :"), f3));
                    cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                    cell.setColspan(1);
                    table.addCell(cell);

                    cell = new PdfPCell(new Phrase(String.valueOf(usuario.getNombre()), f2));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setColspan(3);
                    table.addCell(cell);

                    //-------
                    cell = new PdfPCell(new Phrase(String.valueOf("Cliente :"), f3));
                    cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                    cell.setColspan(1);
                    table.addCell(cell);

                    cell = new PdfPCell(new Phrase(String.valueOf(rs.getString("nomcli")), f2));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setColspan(3);
                    table.addCell(cell);

                    cell = new PdfPCell(new Phrase(String.valueOf("Fecha inpresion :"), f3));
                    cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                    cell.setColspan(1);
                    table.addCell(cell);

                    cell = new PdfPCell(new Phrase(String.valueOf(rs.getString("fecha_impresion")), f2));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setColspan(3);
                    table.addCell(cell);

                    cell = new PdfPCell(new Phrase(String.valueOf("Consecutivo :"), f3));
                    cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                    cell.setColspan(1);
                    table.addCell(cell);

                    cell = new PdfPCell(new Phrase(String.valueOf(rs.getString("no_lote")), f2));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setColspan(3);
                    table.addCell(cell);

                    cell = new PdfPCell();
                    cell.setBorderColorTop(new Color(255, 255, 255));
                    cell.setBorderWidthBottom(0);
                    table.addCell(cell);

                    cell = new PdfPCell(new Phrase(String.valueOf("Item Contrato"), f3b));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setColspan(1);
                    cell.setBackgroundColor(new Color(255, 145, 0));
                    table.addCell(cell);

                    cell = new PdfPCell(new Phrase(String.valueOf("Descripcion APU"), f3b));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setColspan(5);
                    cell.setBackgroundColor(new Color(255, 145, 0));
                    table.addCell(cell);

                    cell = new PdfPCell(new Phrase(String.valueOf("Cantidad \n Contratada"), f3b));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setColspan(1);
                    cell.setBackgroundColor(new Color(255, 145, 0));
                    table.addCell(cell);

                    cell = new PdfPCell(new Phrase(String.valueOf("Cant. \nEjecutda"), f3b));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setColspan(1);
                    cell.setBackgroundColor(new Color(255, 145, 0));
                    table.addCell(cell);

                    cell = new PdfPCell(new Phrase(String.valueOf("% Avance"), f3b));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setColspan(1);
                    cell.setBackgroundColor(new Color(255, 145, 0));
                    table.addCell(cell);

                    cell = new PdfPCell(new Phrase(String.valueOf("Cantidad \nAprobada"), f3b));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setColspan(1);
                    cell.setBackgroundColor(new Color(255, 145, 0));
                    table.addCell(cell);

                    cell = new PdfPCell();
                    cell.setBorderColorBottom(new Color(255, 255, 255));
                    cell.setBorderWidthBottom(0);
                    table.addCell(cell);

                    no_lote = rs.getString("no_lote");
                }

                cell = new PdfPCell();
                cell.setBorderColorTop(new Color(255, 255, 255));
                cell.setBorderWidthBottom(0);
                cell.setBorderWidthTop(0);
                table.addCell(cell);

                cell = new PdfPCell();
                table.addCell(cell);

                cell = new PdfPCell(new Phrase(String.valueOf(rs.getString("descripcion_apu").replaceAll("PROYECTO_", "")), f2));
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setColspan(5);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase(String.valueOf(rs.getString("cantidad_apu_contratada")), f2));
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell.setColspan(1);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase(String.valueOf(rs.getString("cantidad_apu")), f2));
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell.setColspan(1);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase(String.valueOf(rs.getString("porc_avance_apu")), f2));
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell.setColspan(1);
                table.addCell(cell);

                cell = new PdfPCell();
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell.setColspan(1);
                table.addCell(cell);

                cell = new PdfPCell();
                cell.setBorderWidthBottom(0);
                cell.setBorderWidthTop(0);
                table.addCell(cell);

            }

            cell = new PdfPCell(new Phrase(String.valueOf("Observaciones : \n" + observaciones), f2));
            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell.setColspan(12);
            table.addCell(cell);

            document.add(table);

            // step 5
            document.close();

            respuestaJson = "{\"respuesta\":\"OK\",\"Ruta\":\"" + "/images/multiservicios/" + usuario.getLogin() + "/" + nomarchivo + "\"}";

        } catch (Exception e) {
            try {
                respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR generar_Pdf_Cotizacion \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(MinutasContratacionImpl.class.getName()).log(Level.SEVERE, null, ex.getNextException());
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    @Override
    public String imprimir_Liquidacion(String id_solicitud, String ids_actas, Usuario usuario) {

        ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");//se consiguen los datos de db.properties
        String ruta = rb.getString("ruta") + "/images/multiservicios/" + usuario.getLogin() + "/";//se establece la ruta de la imagen 
        String directorioOrigen = rb.getString("ruta") + "/images/multiservicios/" + usuario.getLogin() + "/";//se establece la ruta de la imagen 

        JsonObject modalidad = get_Modalidad_Aiu_iva(id_solicitud);

        con = null;
        ps = null;
        rs = null;
        String sql = "";
        int sub_total = 0;
        query = "SQL_GET_LIQUIDACION";
        String nomarchivo = "";
        String html_doc = "", fecha_corte = "";
        rb = null;
        String respuestaJson = "{}";
        try {
            this.createDir(directorioOrigen);
            DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd HH-mm-ss");
            //get current date time with Date()
            Date date = new Date();
            nomarchivo = "Acta_" + dateFormat.format(date) + ".pdf";
            //System.out.println(dateFormat.format(date));
            rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");

            Document document = new Document(PageSize.LEGAL.rotate(), 85, 85, 15, 52);

            HTMLWorker htmlWorker = new HTMLWorker(document);

            PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(ruta + "/" + nomarchivo));

            Font ff = new Font(Font.TIMES_ROMAN, 14, Font.BOLD, new Color(255, 255, 255));

            Font f = new Font(Font.TIMES_ROMAN, 12, Font.NORMAL);
            Font f1 = new Font(Font.TIMES_ROMAN, 12, Font.BOLD);
            Font f2 = new Font(Font.TIMES_ROMAN, 8, Font.NORMAL);
            Font f3 = new Font(Font.TIMES_ROMAN, 8, Font.BOLD);
            Font f4 = new Font(Font.TIMES_ROMAN, 10, Font.NORMAL);

            Font fb = new Font(Font.TIMES_ROMAN, 12, Font.NORMAL, new Color(255, 255, 255));
            Font f1b = new Font(Font.TIMES_ROMAN, 12, Font.BOLD, new Color(255, 255, 255));
            Font f2b = new Font(Font.TIMES_ROMAN, 8, Font.NORMAL, new Color(255, 255, 255));
            Font f3b = new Font(Font.TIMES_ROMAN, 8, Font.BOLD, new Color(255, 255, 255));
            Font f4b = new Font(Font.TIMES_ROMAN, 10, Font.NORMAL, new Color(255, 255, 255));

            FooterPDF footer = new FooterPDF(rb);
            pdfWriter.setPageEvent(footer);

            document.open();
            document.addAuthor(usuario.getLogin());
            document.addCreator("FINTRA S.A.");
            document.addCreationDate();
            document.addTitle("Actas - Selectrik");

            Image img = Image.getInstance(rb.getString("ruta") + "/images/login/logo_Selectrik2.png");
            //img.setAbsolutePosition(930f, 410f);
            img.scalePercent(48);

            con = this.conectarJNDI();

            PdfPTable table = new PdfPTable(12);
            table.setWidths(new int[]{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1});
            table.setWidthPercentage(100);
            PdfPCell cell;
            //ENCABEZADOS DE LA TABLA

            DecimalFormat formato = new DecimalFormat("$###,###,###");

            String descripcion_apu = "";
            query = this.obtenerSQL(query).replaceAll("#registro1", id_solicitud).replaceAll("#registro2", ids_actas);
            ps = con.prepareStatement(query);
            rs = ps.executeQuery();

            boolean x = false;

            while (rs.next()) {
                if (!descripcion_apu.equals(rs.getString("descripcion_apu"))) {
                    

                    // row 1, cell 1
                    cell = new PdfPCell(img);
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setColspan(2);
                    cell.setBackgroundColor(new Color(255, 145, 0));
                    table.addCell(cell);

                    // row 1, cell 2
                    cell = new PdfPCell(new Phrase("ACTAS DE LIQUIDACION DE OBRA", ff));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setColspan(8);
                    cell.setBackgroundColor(new Color(255, 145, 0));
                    table.addCell(cell);

                    // row 1, cell 2
                    cell = new PdfPCell(new Phrase("CALIDAD", f3b));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setColspan(2);
                    cell.setBackgroundColor(new Color(255, 145, 0));
                    table.addCell(cell);

                    cell = new PdfPCell(new Phrase(String.valueOf("Proyecto :"), f3));
                    cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                    cell.setColspan(1);
                    table.addCell(cell);

                    cell = new PdfPCell(new Phrase(String.valueOf(rs.getString("nombre_proyecto")), f2));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setColspan(7);
                    table.addCell(cell);
//
//                    cell = new PdfPCell(new Phrase(String.valueOf("Fecha informe :"), f3));
//                    cell.setHorizontalAlignment(Element.ALIGN_LEFT);
//                    cell.setColspan(1);
//                    table.addCell(cell);
//
//                    cell = new PdfPCell(new Phrase(String.valueOf(rs.getString("fecha_informe")), f2));
//                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//                    cell.setColspan(3);
//                    table.addCell(cell);

                    cell = new PdfPCell(new Phrase(String.valueOf("Preparado por :"), f3));
                    cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                    cell.setColspan(1);
                    table.addCell(cell);

                    cell = new PdfPCell(new Phrase(String.valueOf(usuario.getNombre()), f2));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setColspan(3);
                    table.addCell(cell);

                    //-------
                    cell = new PdfPCell(new Phrase(String.valueOf("Cliente :"), f3));
                    cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                    cell.setColspan(1);
                    table.addCell(cell);

                    cell = new PdfPCell(new Phrase(String.valueOf(rs.getString("nomcli")), f2));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setColspan(3);
                    table.addCell(cell);

                    cell = new PdfPCell(new Phrase(String.valueOf("Fecha inpresion :"), f3));
                    cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                    cell.setColspan(1);
                    table.addCell(cell);

                    cell = new PdfPCell(new Phrase(String.valueOf(rs.getString("fecha_impresion")), f2));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setColspan(3);
                    table.addCell(cell);

                    cell = new PdfPCell(new Phrase(String.valueOf("Consecutivo :"), f3));
                    cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                    cell.setColspan(1);
                    table.addCell(cell);

                    cell = new PdfPCell();
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setColspan(3);
                    table.addCell(cell);

                    //----------------------------------------
//                    cell = new PdfPCell(new Phrase(String.valueOf("Item Contrato"), f3b));
//                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
//                    cell.setColspan(1);
//                    cell.setBackgroundColor(new Color(255, 145, 0));
//                    table.addCell(cell);
                    cell = new PdfPCell(new Phrase(String.valueOf("Descripcion APU"), f3b));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setColspan(5);
                    cell.setBackgroundColor(new Color(255, 145, 0));
                    table.addCell(cell);

                    cell = new PdfPCell(new Phrase(String.valueOf("Reporte"), f3b));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setColspan(1);
                    cell.setBackgroundColor(new Color(255, 145, 0));
                    table.addCell(cell);

                    cell = new PdfPCell(new Phrase(String.valueOf("Cantidad"), f3b));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setColspan(1);
                    cell.setBackgroundColor(new Color(255, 145, 0));
                    table.addCell(cell);

                    cell = new PdfPCell(new Phrase(String.valueOf("% avance"), f3b));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setColspan(1);
                    cell.setBackgroundColor(new Color(255, 145, 0));
                    table.addCell(cell);

                    cell = new PdfPCell(new Phrase(String.valueOf("Cantidad \nEquivalente"), f3b));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setColspan(1);
                    cell.setBackgroundColor(new Color(255, 145, 0));
                    table.addCell(cell);

                    cell = new PdfPCell(new Phrase(String.valueOf("Cantidad \nAprobada"), f3b));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setColspan(1);
                    cell.setBackgroundColor(new Color(255, 145, 0));
                    table.addCell(cell);

                    cell = new PdfPCell(new Phrase(String.valueOf("Costo \nUnitario"), f3b));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setColspan(1);
                    cell.setBackgroundColor(new Color(255, 145, 0));
                    table.addCell(cell);

                    cell = new PdfPCell(new Phrase(String.valueOf("Costo \nTotal"), f3b));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setColspan(1);
                    cell.setBackgroundColor(new Color(255, 145, 0));
                    table.addCell(cell);
                    
                    if(x){
                        document.newPage();
                        table = new PdfPTable(12);
                        table.setWidths(new int[]{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1});
                        table.setWidthPercentage(100);
                        x=true;
                    }

                }

                cell = new PdfPCell(new Phrase(String.valueOf(rs.getString("descripcion_apu").replaceAll("PROYECTO_", "")), f2));
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                cell.setColspan(5);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase(String.valueOf(rs.getString("no_lote")), f2));
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell.setColspan(1);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase(String.valueOf(rs.getString("cantidad_apu")), f2));
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell.setColspan(1);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase(String.valueOf(rs.getString("porc_avance_apu")), f2));
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell.setColspan(1);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase(String.valueOf(rs.getString("cantidad_equivalente")), f2));
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell.setColspan(1);
                table.addCell(cell);

                cell = new PdfPCell();
                table.addCell(cell);

                cell = new PdfPCell(new Phrase(formato.format(Double.parseDouble(String.valueOf(rs.getString("costo_unitario")))), f2));
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                cell.setColspan(1);
                table.addCell(cell);

                int costoTotal = (int) (rs.getDouble("costo_unitario") * rs.getDouble("cantidad_equivalente"));
                sub_total = sub_total + costoTotal;
                cell = new PdfPCell(new Phrase(formato.format(Double.parseDouble(String.valueOf(costoTotal))), f2));
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                cell.setColspan(1);
                table.addCell(cell);    
                descripcion_apu = rs.getString("descripcion_apu");
            }
            
                

                    
                        
                        cell = new PdfPCell(new Phrase(String.valueOf("Subtotal :"), f2));
                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        cell.setColspan(11);
                        table.addCell(cell);

                        cell = new PdfPCell(new Phrase(formato.format(Double.parseDouble(String.valueOf(sub_total))), f2));
                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        cell.setColspan(1);
                        table.addCell(cell);

                        if (modalidad.get("modalidad_comercial").getAsString().equals("0")) {
                            double iva_vigente = modalidad.get("iva_vigente").getAsDouble(),
                                    valor_iva = (sub_total *  iva_vigente /100),
                                total = sub_total + valor_iva ;
                            
                            cell = new PdfPCell(new Phrase(String.valueOf("IVA (" + iva_vigente + "%) :"), f2));
                            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                            cell.setColspan(11);
                            table.addCell(cell);
                            
                            
                            
                            cell = new PdfPCell(new Phrase(formato.format(valor_iva), f2));
                            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                            cell.setColspan(1);
                            table.addCell(cell);

                            cell = new PdfPCell(new Phrase(String.valueOf("Total :"), f3));
                            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                            cell.setColspan(11);
                            table.addCell(cell);
                            

                            cell = new PdfPCell(new Phrase(formato.format(total), f3));
                            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                            cell.setColspan(1);
                            table.addCell(cell);

                        } else {
                             double perc_administracion = modalidad.get("perc_administracion").getAsDouble()
                                     , perc_imprevisto = modalidad.get("perc_imprevisto").getAsDouble() , 
                                     perc_utilidad = modalidad.get("perc_utilidad").getAsDouble(),
                                     valor_adminisitracion= (sub_total *  perc_administracion /100),
                                     valor_imprevisto= (sub_total *  perc_imprevisto /100),
                                     valor_utilidad= (sub_total *  perc_utilidad /100),
                                     total = sub_total + valor_adminisitracion + valor_imprevisto + valor_utilidad ; 
                             

                            cell = new PdfPCell(new Phrase("Administracion  (" +  perc_administracion + "%) :", f2));
                            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                            cell.setColspan(11);
                            table.addCell(cell);

                            cell = new PdfPCell(new Phrase(formato.format(valor_adminisitracion), f2));
                            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                            cell.setColspan(1);
                            table.addCell(cell);

                            cell = new PdfPCell(new Phrase(String.valueOf("Imprevisto (" + perc_imprevisto + "%) :"), f2));
                            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                            cell.setColspan(11);
                            table.addCell(cell);

                            cell = new PdfPCell(new Phrase(formato.format(valor_imprevisto), f2));
                            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                            cell.setColspan(1);
                            table.addCell(cell);

                            cell = new PdfPCell(new Phrase(String.valueOf("Utilidad  (" + perc_utilidad + "%) :"), f2));
                            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                            cell.setColspan(11);
                            table.addCell(cell);

                            cell = new PdfPCell(new Phrase(formato.format(valor_utilidad), f2));
                            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                            cell.setColspan(1);
                            table.addCell(cell);

                            cell = new PdfPCell(new Phrase(String.valueOf("Total :"), f2));
                            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                            cell.setColspan(11);
                            table.addCell(cell);

                            cell = new PdfPCell(new Phrase(formato.format(total), f2));
                            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                            cell.setColspan(1);
                            table.addCell(cell);

                        }
                        sub_total= 0;
                        
                        
                          

            document.add(table);

            // step 5
            document.close();

            respuestaJson = "{\"respuesta\":\"OK\",\"Ruta\":\"" + "/images/multiservicios/" + usuario.getLogin() + "/" + nomarchivo + "\"}";

        } catch (Exception e) {
            try {
                respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR generar_Pdf_Cotizacion \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(MinutasContratacionImpl.class.getName()).log(Level.SEVERE, null, ex.getNextException());
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    public void createDir(String dir) throws Exception {
        try {
            File f = new File(dir);
            if (!f.exists()) {
                f.mkdir();
            }
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    @Override
    public String insertar_Insumo_Adicional(String id_insumo, String id_unidad_medida, String id_wbs_ejecucion, String cantidad, Usuario usuario) {
        con = null;
        ps = null;
        query = "SQL_INSERTAR_INSUMO_ADICIONAL";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, id_insumo);
            ps.setString(2, id_unidad_medida);
            ps.setString(3, id_wbs_ejecucion);
            ps.setString(4, cantidad);
            ps.setString(5, usuario.getLogin());
            rs = ps.executeQuery();

            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
            throw new SQLException("ERROR actualizarTipoDocumento \n" + e.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }

    }

    static class FooterPDF extends PdfPageEventHelper {

        private Image img;

        public FooterPDF(ResourceBundle rb) throws IOException {
            try {
                String url_logo = "firmas.png";
                img = Image.getInstance(rb.getString("ruta") + "/images/" + url_logo);
                img.setAbsolutePosition(60f, 0f);
                img.scalePercent(70);
            } catch (BadElementException | IOException r) {
                System.err.println("Error al leer la imagen");
            }
        }

        @Override
        public void onStartPage(PdfWriter writer, Document document) {
            try {
                document.add(img);
            } catch (DocumentException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
        }
    }

    public JsonObject get_Modalidad_Aiu_iva(String id_solicitud) {

        con = null;
        ps = null;
        rs = null;

        query = "GET_MODALIDAD_AIU_IVA";

        JsonObject respuesta = new JsonObject();;

        JsonArray arr = new JsonArray();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, id_solicitud);
            rs = ps.executeQuery();

            while (rs.next()) {

                respuesta.addProperty("modalidad_comercial", (rs.getInt("modalidad_comercial")));
                respuesta.addProperty("iva_vigente", rs.getDouble("iva_vigente"));
                respuesta.addProperty("perc_administracion", rs.getString("perc_administracion"));
                respuesta.addProperty("perc_utilidad", rs.getString("perc_utilidad"));
                respuesta.addProperty("perc_imprevisto", rs.getString("perc_imprevisto"));

            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
        }
        return respuesta;

    }

//    static class FooterPDF2 extends PdfPageEventHelper {
//
//        private Image img;
//        private PdfPTable tablew = new PdfPTable(13);
//        private PdfPCell cellw;
//        private Font font1 = new Font(Font.TIMES_ROMAN, 14, Font.BOLD);
//        
//        public FooterPDF(ResourceBundle rb) throws IOException, DocumentException {
//            try {
//                tablew.setWidths(new int[]{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1});
//                tablew.setWidthPercentage(100);
//                
//                
//                cellw = new PdfPCell();
//                tablew.addCell(cellw);
//                
//                cellw = new PdfPCell(new Phrase("Firma", font1));
//                cellw.setHorizontalAlignment(Element.ALIGN_LEFT);
//                cellw.setColspan(3);
//                tablew.addCell(cellw);
//                
//                cellw = new PdfPCell();
//                tablew.addCell(cellw);
//                
//                cellw = new PdfPCell(new Phrase("Firma", font1));
//                cellw.setHorizontalAlignment(Element.ALIGN_LEFT);
//                cellw.setColspan(3);
//                tablew.addCell(cellw);
//                
//                cellw = new PdfPCell();
//                tablew.addCell(cellw);
//                
//                cellw = new PdfPCell(new Phrase("Firma", font1));
//                cellw.setHorizontalAlignment(Element.ALIGN_LEFT);
//                cellw.setColspan(3);
//                tablew.addCell(cellw);
//                
//                cellw = new PdfPCell();
//                tablew.addCell(cellw);
//                
//                tablew.setFooterRows(5);
//                
//                
//                
//
//                
//                
//  
//            } catch (BadElementException r) {
//                System.err.println("Error al leer la imagen");
//            }
////        }
//
//    @Override
//    public void onStartPage(PdfWriter writer, Document document) {
//        try {
//            document.add(img);
//        } catch (DocumentException ex) {
//            System.out.println(ex.getMessage());
//            ex.printStackTrace();
//        }
//    }
//}
}
