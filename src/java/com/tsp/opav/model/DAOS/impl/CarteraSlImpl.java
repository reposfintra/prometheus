/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.opav.model.DAOS.impl;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.tsp.opav.model.DAOS.MainDAO;
import com.tsp.opav.model.DAOS.CarteraSlDAO;
import com.tsp.operation.model.TransaccionService;
import com.tsp.operation.model.beans.StringStatement;
import com.tsp.operation.model.beans.Usuario;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import org.apache.tomcat.jni.User;

/**
 *
 * @author user
 */
public class CarteraSlImpl extends MainDAO implements CarteraSlDAO {

    private Connection con = null;
    private PreparedStatement ps = null;
    private ResultSet rs = null;
    private String query = "";
    private StringStatement st = null;
    private Gson gson = new Gson();
    private JsonObject obj;

    public CarteraSlImpl(String dataBaseName) {
        super("CarteraSlDAO.xml", dataBaseName);

    }

    
    @Override
    public JsonObject cargar_auditoria_tasas() {
        JsonObject          respuesta = new JsonObject();
        JsonArray           arr = new JsonArray();
        JsonObject          objetoJson = null;
        Connection          con = null;
        PreparedStatement   ps = null;
        ResultSet           rs = null;
        String              query = "SL_CARGAR_AUDITORIA_TASAS";

        try {
            con = this.conectarJNDI(query);

            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();

            while (rs.next()) {
                
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }                
                arr.add(objetoJson);
            }
            
            respuesta.add("rows", arr);
        } catch (Exception exc) {
            respuesta = new JsonObject();
            respuesta.addProperty("error", exc.getMessage());
            throw new SQLException("ERROR CarteraSlimpl.java - cargar_auditoria_tasas " + exc.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception ex) {
            }
            return respuesta;
        }
    }
    
    @Override
    public JsonObject isertar_tasa(String tasa , String  usuario) {
        JsonObject          respuesta = new JsonObject();
        JsonObject          objetoJson = null;
        Connection          con = null;
        PreparedStatement   ps = null;
        ResultSet           rs = null;
        String              query = "SL_INSERTAR_TASA";

        try {
            con = this.conectarJNDI(query);

            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, tasa);
            ps.setString(2, usuario);
            rs = ps.executeQuery();

            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }                
            }
            
            respuesta.addProperty("respuesta", "Se inserto la tasa correctamente");
        } catch (Exception exc) {
            respuesta = new JsonObject();
            respuesta.addProperty("error", "Existio un problema al insertar la Tasa "+ exc.getMessage());
            //throw new SQLException("ERROR CarteraSlimpl.java - cargar_auditoria_tasas " + exc.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception ex) {
            }
            return respuesta;
        }
    }

}
