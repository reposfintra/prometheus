/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.opav.model.DAOS.impl;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.tsp.opav.model.DAOS.OrdenCompraDAO;
import com.tsp.opav.model.DAOS.MainDAO;
import com.tsp.operation.model.beans.StringStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author dlopez
 */
public class OrdenCompraImpl extends MainDAO implements OrdenCompraDAO {

    private Connection con = null;
    private PreparedStatement ps = null;
    private ResultSet rs = null;
    private String query = "";
    private StringStatement st = null;
    private List listaInfo;

    public OrdenCompraImpl(String dataBaseName) {        
        super("OrdenCompraDAO.xml", dataBaseName);
    }

    public String cargarBodegas(String idsolicitud, String bd) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query ="";
        String respuesta = "{}";
        Gson gson = new Gson();
        JsonArray lista = null;
        JsonObject obj;
        try {
            lista = new JsonArray();
            con = this.conectarJNDI();
            if(!bd.equals("provintegral")){
                query = "SQL_CARGAR_BODEGAS_INTEGRADAS";
            }else{
                
                query = "SQL_CARGAR_BODEGAS_TERCERIZADAS";
            }
            ps = con.prepareStatement(this.obtenerSQL(query));
            if(!bd.equals("provintegral")){
                ps.setString(1, idsolicitud);
            }
            rs = ps.executeQuery();
            while (rs.next()) {
                obj = new JsonObject();
                obj.addProperty("id", rs.getString("id"));
                obj.addProperty("direccion", rs.getString("descripcion"));
                lista.add(obj);
            }
            respuesta = gson.toJson(lista);
        } catch (Exception ex) {
            respuesta = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return respuesta;
    }

    @Override
    public String obtenerDireccionEntrega(String id_orden_compra) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query ="";
        String respuesta = "{}";
        Gson gson = new Gson();
        JsonArray lista = null;
        JsonObject obj;
        try {            
            lista = new JsonArray();
            con = this.conectarJNDI();
            query = "SQL_OBTENER_DIRECCION_ENTREGA";
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, id_orden_compra);
            rs = ps.executeQuery();
            while (rs.next()) {
                obj = new JsonObject();
                obj.addProperty("direccion_entrega", rs.getString("direccion_entrega"));
                lista.add(obj);
            }
            respuesta = gson.toJson(lista);
        } catch (Exception ex) {
            respuesta = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return respuesta;
    }
    
    @Override
    public String cargarBodegaPrincipal(String bd){
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String respuesta = "{}", tabla ="", consulta = "";        
        Gson gson = new Gson();
        JsonArray lista = null;
        JsonObject obj;
        try {            
            lista = new JsonArray();
            con = this.conectarJNDI();
            
            if(bd.equals("provintegral")){
                tabla = "opav.sl_bodega_terc";
            }else{
                tabla = "opav.sl_bodega";
            }
            
            consulta = "CARGAR_BODEGA_PRINCIPAL";
            consulta = this.obtenerSQL(consulta);
            consulta = consulta.replaceAll("#", tabla);
            ps = con.prepareStatement(consulta);
            rs = ps.executeQuery();
            while (rs.next()) {
                obj = new JsonObject();
                obj.addProperty("direccion", rs.getString("descripcion"));
                obj.addProperty("id", rs.getString("id"));
                lista.add(obj);
            }
            respuesta = gson.toJson(lista);
            
        } catch (Exception ex) {
            respuesta = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return respuesta;
    }

    @Override
    public String cargarInfoBodegaAsignada(String codigo_solicitud) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query ="";
        String respuesta = "{}";
        Gson gson = new Gson();
        JsonArray lista = null;
        JsonObject obj;
        try {            
            lista = new JsonArray();
            con = this.conectarJNDI();
            query = "SQL_CARGAR_BODEGA_ASIGNADA";
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, codigo_solicitud);
            rs = ps.executeQuery();
            while (rs.next()) {
                obj = new JsonObject();
                String id_bodega = rs.getString("id_bodega");
                if (id_bodega == null){
                    obj.addProperty("id_bodega", 0);
                }else{
                    obj.addProperty("id_bodega", Integer.parseInt(id_bodega));    
                }                               
                obj.addProperty("direccion_entrega", rs.getString("direccion_entrega"));
                lista.add(obj);
            }
            respuesta = gson.toJson(lista);
        } catch (Exception ex) {
            respuesta = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return respuesta;
    }

    @Override
    public String cargarBodegasPorUsuario(String id_solicitud, String bd, String login) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query ="";
        String respuesta = "{}";
        Gson gson = new Gson();
        JsonArray lista = null;
        JsonObject obj;
        boolean es_super = esSuperUsuario(login);
        try {
            lista = new JsonArray();
            con = this.conectarJNDI();
            if(es_super){
                query = "SQL_CARGAR_BODEGAS_POR_PROYECTO_SUPER_BODEGUISTA";
            }else{                
                query = "SQL_CARGAR_BODEGAS_POR_PROYECTO_USUARIO_NORMAL";
            }                        
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, id_solicitud);
            
            if (!es_super){
                ps.setString(2, login);
            }
            rs = ps.executeQuery();
            while (rs.next()) {
                obj = new JsonObject();
                obj.addProperty("id", rs.getString("id"));
                obj.addProperty("direccion", rs.getString("descripcion"));
                lista.add(obj);
            }
            respuesta = gson.toJson(lista);
        } catch (Exception ex) {
            respuesta = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return respuesta;
    }
    
    @Override
    public String cargarBodegasPorUsuarioSolicitudEjecucion(String login) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query ="";
        String respuesta = "{}";
        Gson gson = new Gson();
        JsonArray lista = null;
        JsonObject obj;
        boolean es_super = false;
        try {
            lista = new JsonArray();
            con = this.conectarJNDI();             
            query = "SQL_CARGAR_BODEGAS_POR_PROYECTO_USUARIO_NORMAL_EJECUCION";                  
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, login);
            rs = ps.executeQuery();
            while (rs.next()) {
                obj = new JsonObject();
                obj.addProperty("id", rs.getString("id"));
                obj.addProperty("direccion", rs.getString("descripcion"));
                lista.add(obj);
            }
            respuesta = gson.toJson(lista);
        } catch (Exception ex) {
            respuesta = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return respuesta;
    }    
    
    public boolean esSuperUsuario(String login) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query ="";
        String respuesta = "{}";
        Gson gson = new Gson();
        JsonArray lista = null;
        String existe = "";
        boolean existe2 = false;
        try {
            lista = new JsonArray();
            con = this.conectarJNDI();            
            query = "SQL_USUARIO_ES_SUPER_BODEGUISTA";
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, login);
            rs = ps.executeQuery();
            while (rs.next()) {                
                existe = rs.getString("existe");                                
            }
            if(existe.equals("t")){
                existe2 = true;
            }else{
                existe2 = false;
            };
            
        } catch (Exception ex) {
            
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
              
            }
        }
        return existe2;
    }    
   
}
