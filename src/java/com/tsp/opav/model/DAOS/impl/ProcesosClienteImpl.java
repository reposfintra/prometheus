/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.opav.model.DAOS.impl;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.itextpdf.text.BaseColor;

import com.lowagie.text.BadElementException;
import com.lowagie.text.Chapter;
import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.html.HtmlWriter;
import com.lowagie.text.html.simpleparser.HTMLWorker;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfImportedPage;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfPageEventHelper;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfWriter;
import com.tsp.opav.model.DAOS.MainDAO;
import com.tsp.opav.model.DAOS.ProcesosClienteDAO;
import com.tsp.opav.model.DAOS.ClientesVerDAO;
import static com.tsp.opav.model.DAOS.impl.MinutasContratacionImpl.deleteDirectory;
import com.tsp.opav.model.beans.AccionesEca;
import com.tsp.opav.model.beans.OfertaElca;
import com.tsp.opav.model.beans.CotizacionSl;
import com.tsp.operation.model.beans.BeanGeneral;
import com.tsp.operation.model.beans.Cliente;
import com.tsp.operation.model.beans.SerieGeneral;
import com.tsp.opav.model.beans.NegocioApplus;
import com.tsp.operation.model.TransaccionService;
import com.tsp.operation.model.beans.BeansAdministracion;
import com.tsp.operation.model.beans.BeansMultiservicio;
import com.tsp.operation.model.beans.Imagen;
import com.tsp.operation.model.beans.RMCantidadEnLetras;

/*Beans*/
import com.tsp.operation.model.beans.StringStatement;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.util.Util;
import java.awt.Color;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringReader;
////////////////
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import jdk.nashorn.api.scripting.JSObject;

public class ProcesosClienteImpl extends MainDAO implements ProcesosClienteDAO {

    private Connection con = null;
    private PreparedStatement ps = null;
    private ResultSet rs = null;
    private String query = "";
    private StringStatement st = null;
    private List listaInfo;
    
    private String nameFile="";
    
    public static String IMAGE = "";

    
    private static final Font chapterFont = FontFactory.getFont("Calibri", 28, Font.BOLD,new Color(255, 145, 0));
    private static final Font paragraphFont = FontFactory.getFont(FontFactory.HELVETICA, 20, Font.NORMAL,Color.WHITE);
    
//    private BaseFont baseFont = BaseFont.createFont("D:\\Desktop\\Dosis-Regular.ttf", BaseFont.WINANSI, BaseFont.EMBEDDED);
    
    


    public ProcesosClienteImpl(String dataBaseName) {
        super("ProcesosClienteDAO.xml", dataBaseName);
    }

    @Override
    public String cargarTiposClientes() throws SQLException {
        con = null;
        rs = null;
        ps = null;
        query = "Cargar_Tipos_Clientes";
        String respuestaJson = "{}";

        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);

            rs = ps.executeQuery();

            while (rs.next()) {
                obj.addProperty(rs.getString("id"), rs.getString("descripcion"));
            }
            respuestaJson = gson.toJson(obj);

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return respuestaJson;
    }

    @Override
    public String cargarDepartamentos() throws SQLException {
        con = null;
        rs = null;
        ps = null;
        query = "Cargar_Departamentos";
        String respuestaJson = "{}";

        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);
            rs = ps.executeQuery();

            while (rs.next()) {
                obj.addProperty(rs.getString("department_code"), rs.getString("department_name"));
            }
            respuestaJson = gson.toJson(obj);

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return respuestaJson;
    }

    @Override
    public String cargarCiudades(String codDepartamento) throws SQLException {
        con = null;
        rs = null;
        ps = null;
        query = "Cargar_Ciudades";
        String respuestaJson = "{}";

        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);
            ps.setString(1, codDepartamento);

            rs = ps.executeQuery();

            while (rs.next()) {
                obj.addProperty(rs.getString("codciu"), rs.getString("nomciu"));
            }
            respuestaJson = gson.toJson(obj);

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return respuestaJson;
    }

    public boolean verificaExisteCliente(String nit) {
        con = null;
        rs = null;
        ps = null;
        query = "Cargar_Cliente_nit";
        boolean existe = false;

        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);
            ps.setString(1, nit);

            rs = ps.executeQuery();
            while (rs.next()) {
                existe = true;
                break;
            }

        } catch (Exception e) {

        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return existe;
    }

    @Override
    public String crearCliente(Cliente cliente, Usuario usuario, JsonObject info) throws SQLException {

        con = null;
        ps = null;
        Statement stmt = null;
        String respuestaJson = "{}";
        JsonObject respuesta = new JsonObject();
        String query = "";
        try {

            if (cliente.getClasificacion().equals("3") || (!cliente.getClasificacion().equals("3") && !verificaExisteCliente(cliente.getNit()))) {

                ClientesVerDAO cvd = new ClientesVerDAO(usuario.getBd());
                SerieGeneral s_id_cliente = cvd.getSerie("FINV", "OP", (cliente.getClasificacion().equals("3")) ? "AGENCIA" : "CLIENTE");
                cvd.setSerie("FINV", "OP", (cliente.getClasificacion().equals("3")) ? "AGENCIA" : "CLIENTE");
                String id_cliente = s_id_cliente.getUltimo_prefijo_numero();
                cliente.setCodcli(id_cliente);

                con = this.conectarJNDI();
                con.setAutoCommit(false);
                stmt = con.createStatement();

                stmt.addBatch(InsertarCliente(cliente, usuario));
                if (!cliente.getClasificacion().equals("3")) {
                    if (cliente.getId_padre().equals("")) {
                        stmt.addBatch(cvd.insertSubcliente(cliente.getCodcli(), cliente.getCodcli(), usuario.getLogin()));
                    } else {
                        stmt.addBatch(cvd.insertSubcliente(cliente.getCodcli(), cliente.getId_padre(), usuario.getLogin()));
                        if (VerificaClasificacion(cliente.getId_padre())) {

                            stmt.addBatch(ActualizarClasificacion(cliente.getId_padre(), usuario, "2"));

                        }
                    }
                }

                JsonArray arr = info.getAsJsonArray("rows");

                for (int i = 0; i < arr.size(); i++) {
                    respuesta = arr.get(i).getAsJsonObject();

                    if (respuesta.get("id").getAsString().startsWith("neo_")) {
                        if (esnumerico(respuesta.get("valor_01").getAsString())) {
                            query = "INSERTAR_NIC";
                            st = new StringStatement(this.obtenerSQL(query), true);

                            st.setString(1, respuesta.get("valor_01").getAsString());
                            st.setString(2, cliente.getCodcli());
                            st.setString(3, usuario.getLogin());
                            st.setString(4, usuario.getLogin());
                            stmt.addBatch(st.getSql());
                        };

                    } else {

                        //Si desean Actualizar
                    }

                }

                stmt.executeBatch();
                stmt.clearBatch();
                con.commit();
                respuestaJson = "{\"respuesta\":\"OK\"}";

            } else {
                respuestaJson = "{\"error\":\" El Nit ya existe \"}";
            }

        } catch (Exception e) {
            try {
                con.rollback();
                e.printStackTrace();
                respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR anularMetaProceso \n" + e.getMessage());

            } catch (SQLException ex) {

            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
            }
            return respuestaJson;
        }
    }

    private boolean esnumerico(String cadena) {
        try {
            Integer.parseInt(cadena);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    private String InsertarCliente(Cliente cliente, Usuario usuario) throws Exception {
        String query = (cliente.getClasificacion().equals("3")) ? "CREAR_AGENCIA" : "CREAR_CLIENTE";
        String respuesta = "";
        StringStatement st = null;

        try {
            st = new StringStatement(this.obtenerSQL(query), true);

            st.setString(1, cliente.getCodcli());
            st.setString(2, cliente.getNomcli());
            st.setString(3, cliente.getNit());
            st.setString(4, cliente.getTipo());
            st.setString(5, cliente.getCiudad());
            st.setString(6, cliente.getDireccion());
            st.setString(7, cliente.getNomContacto());
            st.setString(8, cliente.getTelContacto());
            st.setString(9, cliente.getCargoContacto());
            st.setString(10, cliente.getNomRepresentante());
            st.setString(11, cliente.getTelRepresentante());
            st.setString(12, cliente.getCelRepresentante());
            st.setString(13, usuario.getLogin());
            st.setString(14, cliente.getSector());
            st.setString(15, cliente.getEmail());
            st.setString(16, cliente.getCelContacto());
            st.setString(17, cliente.getEmailrepresentantelega());
            st.setString(18, (cliente.getClasificacion().equals("3")) ? cliente.getId_padre() : cliente.getClasificacion());
            st.setString(19, cliente.getDigito_verificacion());

            respuesta = st.getSql();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
            throw new SQLException("ERROR DURANTE SQL_INSERTAR_SUBCLIENTE. \n " + e.getMessage());
        } finally {
            if (st != null) {
                try {
                    st = null;
                } catch (Exception e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
        }

        return respuesta;
    }

    private String ActualizarClasificacion(String idcliente, Usuario usuario, String dato) throws Exception {
        String query = "ACTUALIZAR_CLASIFICACION";
        String respuesta = "";
        StringStatement st = null;

        try {
            st = new StringStatement(this.obtenerSQL(query), true);

            st.setString(3, idcliente);
            st.setString(2, usuario.getLogin());
            st.setString(1, dato);

            respuesta = st.getSql();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
            throw new SQLException("ERROR DURANTE SQL_INSERTAR_SUBCLIENTE. \n " + e.getMessage());
        } finally {
            if (st != null) {
                try {
                    st = null;
                } catch (Exception e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
        }

        return respuesta;
    }

    public boolean VerificaClasificacion(String codcliente) {

        rs = null;
        query = "Verifica_Clasificacion";
        boolean hijo = false;

        try {
            //con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);
            ps.setString(1, codcliente);

            rs = ps.executeQuery();
            while (rs.next()) {
                hijo = true;
                break;
            }

        } catch (Exception e) {

        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return hijo;
    }

    private String ModificarCl(Cliente cliente, Usuario usuario) throws Exception {
        String query = (cliente.getCodcli().startsWith("AS")) ? "ACTUALIZAR_AGENCIA" : "ACTUALIZAR_CLIENTE";
        String respuesta = "";
        StringStatement st = null;

        try {
            st = new StringStatement(this.obtenerSQL(query), true);//JJCastro fase2

            st.setString(16, cliente.getCodcli());
            st.setString(1, cliente.getNomcli());
            st.setString(2, cliente.getNit());
            st.setString(3, cliente.getCiudad());
            st.setString(4, cliente.getDireccion());
            st.setString(5, cliente.getNomContacto());
            st.setString(6, cliente.getTelContacto());
            st.setString(7, cliente.getCargoContacto());
            st.setString(8, cliente.getNomRepresentante());
            st.setString(9, cliente.getTelRepresentante());
            st.setString(10, cliente.getCelRepresentante());
            st.setString(11, usuario.getLogin());
            st.setString(12, cliente.getEmail());
            st.setString(13, cliente.getCelContacto());
            st.setString(14, cliente.getEmailrepresentantelega());
            st.setString(15, cliente.getDigito_verificacion());

            respuesta = st.getSql();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
            throw new SQLException("ERROR DURANTE ACTUALIZAR_CLIENTE. \n " + e.getMessage());
        } finally {
            if (st != null) {
                try {
                    st = null;
                } catch (Exception e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
        }

        return respuesta;
    }

    @Override
    public String cargarClientesPadre(String q) throws SQLException {

        con = null;
        ps = null;
        rs = null;
        String query = "CARGAR_CLIENTE_PADRE";
        JsonArray lista = null;

        try {
            con = this.conectarJNDI();
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));
                ps.setString(1, q);
                rs = ps.executeQuery();
                lista = new JsonArray();
                JsonObject fila;
                while (rs.next()) {
                    fila = new JsonObject();
                    fila.addProperty("value", rs.getString("codcli"));
                    fila.addProperty("label", rs.getString("nomnit"));
                    fila.addProperty("id_tipo_cliente", rs.getString("id_tipo_cliente"));
                    fila.addProperty("tipo_cliente", rs.getString("tipo_cliente"));
                    lista.add(fila);
                }
            }
        } catch (Exception e) {
            lista = new JsonArray();
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return new Gson().toJson(lista);
        }
    }

    @Override
    public ArrayList<BeanGeneral> cargarNics2() {
        con = null;
        rs = null;
        ps = null;
        query = "Cargar_Cliente_nit";
        ArrayList<BeanGeneral> lista = new ArrayList<BeanGeneral>();
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);
            ps.setString(1, "800174");
            rs = ps.executeQuery();

            while (rs.next()) {
                BeanGeneral pro = new BeanGeneral();
                pro.setValor_01(rs.getString("nic"));
                lista.add(pro);
            }
        } catch (Exception e) {
            try {
                //throw new Exception("Error en cargarProcesosMeta " + e.toString());
            } catch (Exception ex) {
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return lista;
    }

    @Override
    public String cargarClienteId(String idcliente) throws SQLException {
        con = null;
        rs = null;
        ps = null;
        Cliente cl = new Cliente();
        query = "cargar_Cliente_Id";
        String respuestaJson = "{}";

        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);
            ps.setString(1, idcliente);

            rs = ps.executeQuery();

            while (rs.next()) {
                cl.setCodcli(rs.getString("codcli"));
                cl.setNomcli(rs.getString("nomcli"));
                cl.setNit(rs.getString("nit"));
                cl.setTipo(rs.getString("tipo"));
                cl.setCiudad(rs.getString("ciudad"));
                cl.setDireccion(rs.getString("direccion"));
                cl.setNomContacto(rs.getString("nomcontacto"));
                cl.setTelContacto(rs.getString("telcontacto"));
                cl.setCargoContacto(rs.getString("cargo_contacto"));
                cl.setNomRepresentante(rs.getString("nombre_representante"));
                cl.setTelRepresentante(rs.getString("tel_representante"));
                cl.setCelRepresentante(rs.getString("celular_representante"));
                cl.setEmail(rs.getString("email_contacto"));
                cl.setCelContacto(rs.getString("cel_contacto"));
                cl.setEmailrepresentantelega(rs.getString("email_representante"));
                cl.setClasificacion(rs.getString("clasificacion"));

                cl.setDigito_verificacion(rs.getString("digito_verificacion"));
            }
            respuestaJson = gson.toJson(cl);

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return respuestaJson;
    }

    @Override
    public String cargarPadreId(String idpadre) throws SQLException {
        con = null;
        rs = null;
        ps = null;
        Cliente cl = new Cliente();
        query = "cargar_Padre_Id";
        String respuestaJson = "{}";

        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);
            ps.setString(1, idpadre);

            rs = ps.executeQuery();

            while (rs.next()) {
                obj.addProperty(rs.getString("id_cliente_padre"), rs.getString("nomnit"));
            }
            respuestaJson = gson.toJson(obj);

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return respuestaJson;
    }

    @Override
    public String cargarDepCiuid(String idciudad) throws SQLException {
        con = null;
        rs = null;
        ps = null;

        query = "CARGAR_DEPCIU_IDCIU";
        String respuestaJson = "{}";

        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);
            ps.setString(1, idciudad);

            rs = ps.executeQuery();

            while (rs.next()) {
                obj.addProperty(rs.getString("department_name"), rs.getString("nomciu"));
            }
            respuestaJson = gson.toJson(obj);

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return respuestaJson;
    }

    @Override
    public String obteneridclientenit(String idcliente) throws SQLException {
        con = null;
        rs = null;
        ps = null;
        Cliente cl = new Cliente();
        query = "OBTENER_IDCCLIENTE_NIT";
        String respuestaJson = "{}";

        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);
            ps.setString(1, idcliente);

            rs = ps.executeQuery();

            while (rs.next()) {
                obj.addProperty("idcliente", rs.getString("codcli"));
            }
            respuestaJson = gson.toJson(obj);

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return respuestaJson;
    }

    @Override
    public String modificarCliente(Cliente cliente, Usuario usuario, JsonObject info) throws SQLException {
        con = null;
        ps = null;
        Statement stmt = null;
        JsonObject respuesta = new JsonObject();
        String respuestaJson = "{}";

        try {

            ClientesVerDAO cvd = new ClientesVerDAO(usuario.getBd());

            con = this.conectarJNDI();
            con.setAutoCommit(false);
            stmt = con.createStatement();

            stmt.addBatch(ModificarCl(cliente, usuario));
////            if (!cliente.getPadre().equals("S")) {
//
//                stmt.addBatch("update opav.subclientes_eca set id_cliente_padre= '" + cliente.getId_padre() + "',user_update= '" + usuario.getLogin() + "',last_update=now() where id_subcliente= '" + cliente.getCodcli() + "'");
//            }

            JsonArray arr = info.getAsJsonArray("rows");

            for (int i = 0; i < arr.size(); i++) {
                respuesta = arr.get(i).getAsJsonObject();

                if (respuesta.get("id_cliente").getAsString().startsWith("neo_")) {

                    query = "INSERTAR_NIC";
                    st = new StringStatement(this.obtenerSQL(query), true);

                    st.setString(1, respuesta.get("valor_01").getAsString());
                    st.setString(2, cliente.getCodcli());
                    st.setString(3, usuario.getLogin());
                    st.setString(4, usuario.getLogin());
                    stmt.addBatch(st.getSql());

                } else {

                    //Si desean Actualizar
                }

            }

            stmt.executeBatch();
            stmt.clearBatch();
            con.commit();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            try {
                con.rollback();
                respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR anularMetaProceso \n" + e.getMessage());

            } catch (SQLException ex) {

            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
            }
            return respuestaJson;
        }
    }

    @Override
    public String CargarNics(String idcliente) throws SQLException {

        con = null;
        ps = null;
        rs = null;
        String query = "CARGAR_LISTADO_NICS";
        JsonArray lista = null;

        try {
            con = this.conectarJNDI();
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));
                ps.setString(1, idcliente);
                rs = ps.executeQuery();
                lista = new JsonArray();
                JsonObject fila;
                while (rs.next()) {
                    fila = new JsonObject();
                    fila.addProperty("id_cliente", rs.getString("id_cliente"));
                    fila.addProperty("nic", rs.getString("nic"));
                    lista.add(fila);
                }
            }
        } catch (Exception e) {
            lista = new JsonArray();
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return new Gson().toJson(lista);
        }
    }

    @Override
    public String cargarListadoLineaDeNegocio() throws SQLException {
        con = null;
        ps = null;
        rs = null;
        String query = "CARGAR_LISTADO_LINEA_NEGOCIO";
        JsonArray lista = null;

        try {
            con = this.conectarJNDI();
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));
                rs = ps.executeQuery();
                lista = new JsonArray();
                JsonObject fila;
                while (rs.next()) {
                    fila = new JsonObject();
                    fila.addProperty("value", rs.getString("cod_proyecto"));
                    fila.addProperty("label", rs.getString("descripcion"));
                    lista.add(fila);
                }
            }
        } catch (Exception e) {
            lista = new JsonArray();
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return new Gson().toJson(lista);
        }
    }

    @Override
    public String cargarListadoTipoSolicitud(String q, String codproyecto) throws SQLException {
        con = null;
        ps = null;
        rs = null;
        String query = "CARGAR_LISTADO_TIPO_SOLICITUD";
        JsonArray lista = null;

        try {
            con = this.conectarJNDI();
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));
                ps.setString(1, codproyecto);
                ps.setString(2, q);

                rs = ps.executeQuery();
                lista = new JsonArray();
                JsonObject fila;
                while (rs.next()) {
                    fila = new JsonObject();
                    fila.addProperty("value", rs.getString("id"));
                    fila.addProperty("label", rs.getString("distribucion"));
                    lista.add(fila);
                }
            }
        } catch (Exception e) {
            lista = new JsonArray();
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return new Gson().toJson(lista);
        }

    }

    @Override
    public String cargarListadoResponsable() {
        con = null;
        ps = null;
        rs = null;
        String query = "CARGAR_LISTADO_RESPONSABLES";
        JsonArray lista = null;

        try {
            con = this.conectarJNDI();
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));

                rs = ps.executeQuery();
                lista = new JsonArray();
                JsonObject fila;
                while (rs.next()) {
                    fila = new JsonObject();
                    fila.addProperty("value", rs.getString("id"));
                    fila.addProperty("label", rs.getString("descripcion"));
                    lista.add(fila);
                }
            }
        } catch (Exception e) {
            lista = new JsonArray();
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return new Gson().toJson(lista);
        }
    }

    @Override
    public String cargarListadoInterventor() {
        con = null;
        ps = null;
        rs = null;
        String query = "CARGAR_LISTADO_INTERVENTOR";
        JsonArray lista = null;

        try {
            con = this.conectarJNDI();
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));
                rs = ps.executeQuery();
                lista = new JsonArray();
                JsonObject fila;

                while (rs.next()) {
                    fila = new JsonObject();
                    fila.addProperty("value", rs.getString("id"));
                    fila.addProperty("label", rs.getString("descripcion"));
                    lista.add(fila);
                }
            }
        } catch (Exception e) {
            lista = new JsonArray();
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return new Gson().toJson(lista);
        }
    }

    @Override
    public String crearSolicitud(OfertaElca ofeca, Usuario usuario, String enviar) throws SQLException {

        con = null;
        ps = null;
        Statement stmt = null;
        JsonObject obj = new JsonObject();
        Gson gson = new Gson();
        try {
            ClientesVerDAO cvd = new ClientesVerDAO(usuario.getBd());
            SerieGeneral s_id_cliente = cvd.getSerie("FINV", "OP", "OFMS");
            cvd.setSerie("FINV", "OP", "OFMS");
            ofeca.setId_solicitud(s_id_cliente.getUltimo_prefijo_numero());
            
            con = this.conectarJNDI();
            con.setAutoCommit(false);
            stmt = con.createStatement();
            stmt.addBatch(crearSolicitudQuery(ofeca, usuario, enviar));

            String etapa;
            String concepto;
            String descripcion;
            if (enviar.equals("true")) {
                etapa = "2";
                concepto = "Creacion y Envio";
                descripcion = "Se crea Solicitud y se envia de manera inmediata a validacion cartera";
            } else {

                etapa = "1";
                concepto = "Creacion Solicitud";
                descripcion = "Se creo la Solicitud";
            }

            stmt.addBatch(InsertarTrazabilidadQuery(ofeca.getId_solicitud(), etapa, usuario, descripcion, concepto, ""));
            stmt.addBatch(InsertarAccionQuery(ofeca, usuario));

            stmt.executeBatch();
            stmt.clearBatch();
            con.commit();
            
            obj.addProperty("respuesta", "OK");
            obj.addProperty("id_solicitud",ofeca.getId_solicitud() );
            

        } catch (Exception e) {
            try {
                con.rollback();
                obj.addProperty("error", e.getMessage() ); 
                throw new SQLException("ERROR EN CREACION DE SOLICITUD : \n" + e.getMessage());

            } catch (SQLException ex) {

            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
            }
            
            return gson.toJson(obj);
        }

    }

    private String crearSolicitudQuery(OfertaElca ofeca, Usuario usuario, String enviar) throws Exception {
        String query = "INSERTAR_SOLICITUD";
        String respuesta = "";
        StringStatement st = null;

        try {
            st = new StringStatement(this.obtenerSQL(query), true);

            st.setString(1, ofeca.getId_solicitud());
            st.setString(2, ofeca.getId_cliente());
            st.setString(3, ofeca.getDescripcion());
            st.setString(4, usuario.getLogin());
            st.setString(5, ofeca.getNic());
            st.setString(6, ofeca.getTipo_solicitud());
            st.setString(7, ofeca.getTipoDtf());
            st.setString(8, ofeca.getTipo_solicitud());
            st.setString(9, ofeca.getOficial());
            st.setString(10, ofeca.getAviso());
            st.setString(11, ofeca.getEstado_cartera());
            st.setString(12, ofeca.getFec_val_cartera());
            st.setString(13, ofeca.getResponsable());
            st.setString(14, ofeca.getInterventor());
            st.setString(15, ofeca.getNomproyecto());
            st.setString(16, "1");
            st.setString(17, ofeca.getTipo_negocio());
            st.setString(18 , ofeca.getFecha_limite());
            st.setString(19, ofeca.getTipo_negocio());
            st.setString(20, ofeca.getTipo_trabajo());
            respuesta = st.getSql();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
            throw new SQLException("ERROR DURANTE CREACION SOLICITUD. \n " + e.getMessage());
        } finally {
            if (st != null) {
                try {
                    st = null;
                } catch (Exception e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
        }

        return respuesta;
    }

    @Override
    public String cargarSolicitud(String idsolicitud) {
        con = null;
        rs = null;
        ps = null;
        OfertaElca ofca = new OfertaElca();
        query = "Cargar_Solicitud";
        String respuestaJson = "{}";

        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);
            ps.setString(1, idsolicitud);

            rs = ps.executeQuery();

            while (rs.next()) {
                ofca.setCreacion_fecha_entrega_oferta(rs.getString("creacion_fecha_entrega_oferta"));
                ofca.setCreation_date(rs.getString("creation_date"));
                ofca.setCreation_user(rs.getString("creation_user"));
                ofca.setDescripcion(rs.getString("descripcion"));
                ofca.setFecha_entrega_oferta(rs.getString("fecha_entrega_oferta"));
                ofca.setId_cliente(rs.getString("id_cliente"));
                ofca.setId_oferta(rs.getString("id_oferta"));
                ofca.setId_solicitud(rs.getString("id_solicitud"));
                ofca.setLast_update(rs.getString("last_update"));
                ofca.setNic(rs.getString("nic"));
                ofca.setNum_os(rs.getString("num_os"));
                ofca.setReg_status(rs.getString("reg_status"));
                ofca.setUser_update(rs.getString("user_update"));
                ofca.setUsuario_entrega_oferta(rs.getString("usuario_entrega_oferta"));
                ofca.setTipo_solicitud(rs.getString("tipo_solicitud"));

                ofca.setEstado_cartera(rs.getString("estudio_cartera"));
                ofca.setFec_val_cartera(rs.getString("fecha_validacion_cartera"));

                ofca.setResponsable(rs.getString("responsable"));
                ofca.setNomresponsable(rs.getString("nom_responsable"));
                ofca.setInterventor(rs.getString("interventor"));
                ofca.setAviso(rs.getString("aviso"));
                ofca.setLineanegocio(rs.getString("tipo_proyecto"));
                ofca.setFecha_limite(rs.getString("fecha_limite_entrega"));
            }
            respuestaJson = gson.toJson(ofca);

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return respuestaJson;
    }

    @Override
    public String cargarLinieaNegocioTS(String tiposolicitud) {
        con = null;
        ps = null;
        rs = null;
        String query = "CARGAR_LINEA_NEGOCIO_TP";
        JsonObject obj = new JsonObject();

        try {
            con = this.conectarJNDI();
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));
                ps.setString(1, tiposolicitud);
                rs = ps.executeQuery();
                while (rs.next()) {
                    obj.addProperty("value", rs.getString("cod_proyecto"));
                    obj.addProperty("label", rs.getString("descripcion"));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return new Gson().toJson(obj);
        }
    }

    @Override
    public String modificarSolicitud(OfertaElca ofeca, Usuario usuario) {
        con = null;
        ps = null;
        String respuestaJson = "{}";
        query = "MODIFICAR_SOLICITUD";
        try {
            ClientesVerDAO cvd = new ClientesVerDAO(usuario.getBd());
            ofeca.setTipoDtf(cvd.getDistribucion(ofeca.getTipo_solicitud()));

            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);

            ps.setString(1, (" (" + usuario.getLogin() + ") : " + ofeca.getDescripcion()));
            ps.setString(2, usuario.getLogin());
            ps.setString(3, ofeca.getTipo_solicitud());
            ps.setString(4, ofeca.getTipo_solicitud());
            ps.setString(5, ofeca.getAviso());
            ps.setString(6, ofeca.getResponsable());
            ps.setString(7, ofeca.getInterventor());
            ps.setString(8, ofeca.getTipoDtf());
            ps.setString(9, ofeca.getLineanegocio());
            ps.setString(10, ofeca.getId_solicitud());

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            try {
                respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR guardarMetaProceso \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(ProcesosCatalogoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
            }
            return respuestaJson;
        }
    }

    @Override
    public String cargarCartera() throws SQLException {
        con = null;
        ps = null;
        rs = null;
        String query = "CARGAR_LISTADO_ESTADOS_CARTERA";
        JsonArray lista = null;

        try {
            con = this.conectarJNDI();
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));
                //ps.setString(1, q);
                rs = ps.executeQuery();
                lista = new JsonArray();
                JsonObject fila;
                while (rs.next()) {
                    fila = new JsonObject();
                    fila.addProperty("value", rs.getString("id"));
                    fila.addProperty("label", rs.getString("nombre"));
                    lista.add(fila);
                }
            }
        } catch (Exception e) {
            lista = new JsonArray();
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return new Gson().toJson(lista);
        }

    }

    @Override
    public String cargarContratistas(String tipo) throws SQLException {
        con = null;
        ps = null;
        rs = null;
        String query;
        if ((tipo.equals("Mantenimiento Aires AAE")) || (tipo.equals("Ventas Aires AAE"))) {
            query = "CARGAR_LISTADO_CONTRATISTAS_AAAE";
        } else {
            query = "CARGAR_LISTADO_CONTRATISTAS";
        }

        JsonArray lista = null;

        try {
            con = this.conectarJNDI();
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));
                //ps.setString(1, idcliente);
                rs = ps.executeQuery();
                lista = new JsonArray();
                JsonObject fila;
                while (rs.next()) {
                    fila = new JsonObject();
                    fila.addProperty("id_contratista", rs.getString("id_contratista"));
                    fila.addProperty("descripcion", rs.getString("descripcion"));
                    lista.add(fila);
                }
            }
        } catch (Exception e) {
            lista = new JsonArray();
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return new Gson().toJson(lista);
        }
    }

    @Override
    public String asociarContratistaSolicitud(AccionesEca acceca, Usuario usuario) {
        con = null;
        ps = null;
        String respuestaJson = "{}";
        query = "SQL_INSERT_ACCION_ECA";
        try {
            ClientesVerDAO cvd = new ClientesVerDAO(usuario.getBd());
            SerieGeneral s_id_cliente = cvd.getSerie("FINV", "OP", "ACMS");
            cvd.setSerie("FINV", "OP", "ACMS");
            acceca.setId_accion(s_id_cliente.getUltimo_prefijo_numero());

            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);

            ps.setString(1, acceca.getId_accion());
            ps.setString(2, acceca.getId_solicitud());
            ps.setString(3, acceca.getContratista());
            ps.setString(4, acceca.getDescripcion());
            ps.setString(5, usuario.getLogin());
            ps.setString(6, acceca.getTipo_trabajo());

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            try {
                respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR guardarMetaProceso \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(ProcesosCatalogoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
            }
            return respuestaJson;
        }

    }

    @Override
    public String cargarAccionesAsociadas(String id_solicitud) {
        con = null;
        ps = null;
        rs = null;

        String query = "SQL_GET_ACCIONES_ECA";

        ArrayList<AccionesEca> lista = new ArrayList<AccionesEca>();

        try {
            con = this.conectarJNDI();
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));
                ps.setString(1, id_solicitud);
                rs = ps.executeQuery();

                while (rs.next()) {
                    AccionesEca newacc = new AccionesEca();
                    newacc.setId_accion(rs.getString("id_accion"));
                    newacc.setAdministracion(rs.getString("administracion"));
                    newacc.setContratista(rs.getString("contratista"));
                    newacc.setCreation_date(rs.getString("creation_date"));
                    newacc.setCreation_user(rs.getString("creation_user"));
                    newacc.setDescripcion(rs.getString("descripcion"));
                    newacc.setEstado(rs.getString("estado"));
                    newacc.setId_solicitud(rs.getString("id_solicitud"));
                    newacc.setImprevisto(rs.getString("imprevisto"));
                    newacc.setLast_update(rs.getString("last_update"));
                    newacc.setMano_obra(rs.getString("mano_obra"));
                    newacc.setMaterial(rs.getString("material"));
                    newacc.setObservaciones(rs.getString("observaciones"));
                    newacc.setPorc_administracion(rs.getString("porc_administracion"));
                    newacc.setPorc_imprevisto(rs.getString("porc_imprevisto"));
                    newacc.setPorc_utilidad(rs.getString("porc_utilidad"));
                    newacc.setReg_status(rs.getString("reg_status"));
                    newacc.setTipo_trabajo(rs.getString("tipo_trabajo"));
                    newacc.setTransporte(rs.getString("transporte"));
                    newacc.setUser_update(rs.getString("user_update"));
                    newacc.setUtilidad(rs.getString("utilidad"));
                    newacc.setNomcontratista(rs.getString("descontratista"));
                    lista.add(newacc);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return new Gson().toJson(lista);
        }
    }

    @Override
    public String cargarAccionPrincipal(String idsolicitud) {
        con = null;
        ps = null;
        rs = null;

        String query = "SQL_GET_ACCION_PRINCIPAL";

        ArrayList<AccionesEca> lista = new ArrayList<AccionesEca>();

        try {
            con = this.conectarJNDI();
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));
                ps.setString(1, idsolicitud);
                rs = ps.executeQuery();

                while (rs.next()) {
                    AccionesEca newacc = new AccionesEca();
                    newacc.setId_accion(rs.getString("id_accion"));
                    newacc.setAdministracion(rs.getString("administracion"));
                    newacc.setContratista(rs.getString("contratista"));
                    newacc.setCreation_date(rs.getString("creation_date"));
                    newacc.setCreation_user(rs.getString("creation_user"));
                    newacc.setDescripcion(rs.getString("descripcion"));
                    newacc.setEstado(rs.getString("estado"));
                    newacc.setId_solicitud(rs.getString("id_solicitud"));
                    newacc.setImprevisto(rs.getString("imprevisto"));
                    newacc.setLast_update(rs.getString("last_update"));
                    newacc.setMano_obra(rs.getString("mano_obra"));
                    newacc.setMaterial(rs.getString("material"));
                    newacc.setObservaciones(rs.getString("observaciones"));
                    newacc.setPorc_administracion(rs.getString("porc_administracion"));
                    newacc.setPorc_imprevisto(rs.getString("porc_imprevisto"));
                    newacc.setPorc_utilidad(rs.getString("porc_utilidad"));
                    newacc.setReg_status(rs.getString("reg_status"));
                    newacc.setTipo_trabajo(rs.getString("tipo_trabajo"));
                    newacc.setTransporte(rs.getString("transporte"));
                    newacc.setUser_update(rs.getString("user_update"));
                    newacc.setUtilidad(rs.getString("utilidad"));
                    newacc.setNomcontratista(rs.getString("descontratista"));
                    lista.add(newacc);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return new Gson().toJson(lista);
        }
    }

    @Override
    public String cargarTipoTrabajo(String tipo) {
        con = null;
        ps = null;
        rs = null;
        String query;
        if ((tipo.equals("Mantenimiento Aires AAE")) || (tipo.equals("Ventas Aires AAE"))) {
            query = "SQL_GET_TIPOTRABAJOS_AAAE";
        } else {
            query = "SQL_GET_TIPOTRABAJOS";
        }

        JsonArray lista = null;

        try {
            con = this.conectarJNDI();
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));
                //ps.setString(1, q);
                rs = ps.executeQuery();
                lista = new JsonArray();
                JsonObject fila;
                while (rs.next()) {
                    fila = new JsonObject();
                    fila.addProperty("value", rs.getString("id"));
                    fila.addProperty("label", rs.getString("descripcion"));
                    lista.add(fila);
                }
            }
        } catch (Exception e) {
            lista = new JsonArray();
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return new Gson().toJson(lista);
        }
    }

    @Override
    public String cargarAlcanceAccion(String id_accion, String idsolicitud) {
        con = null;
        rs = null;
        ps = null;
        Cliente cl = new Cliente();
        query = "SQL_CARGAR_ALCANCE_ACCION";
        String respuestaJson = "{}";

        Gson gson = new Gson();
        NegocioApplus negapp = new NegocioApplus();
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);
            ps.setString(1, id_accion);
            ps.setString(2, idsolicitud);

            rs = ps.executeQuery();

            while (rs.next()) {
                negapp.setAcciones(rs.getString("descripcion"));
                negapp.setObservacion(rs.getString("observaciones"));
                negapp.setSolicitud(rs.getString("descripcion_solicitud"));
                negapp.setFecha(rs.getString("creation_date"));
                negapp.setTipoTrabajo(rs.getString("tipo_trabajo"));

                negapp.setAlcances(rs.getString("alcances"));
                negapp.setAdiciones(rs.getString("adicionales"));
                negapp.setTrabajo(rs.getString("trabajo"));

                negapp.setFecVisitaPlan(rs.getString("fec_visita_planeada"));
                negapp.setFecVisitaReal(rs.getString("fec_visita_hecha"));
                negapp.setUsrVisitaHecha(rs.getString("user_visita_hecha"));
                negapp.setCreationFecVisitaHecha(rs.getString("creation_fec_visita_hecha"));

                negapp.setIdEstado(rs.getString("estado"));//20100513pm
            }
            respuestaJson = gson.toJson(negapp);

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return respuestaJson;
    }

    public boolean VerificaNic(String nic) {
        rs = null;
        query = "VALIDA_EXISTE_NIC";
        boolean existeNic = false;

        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);
            ps.setString(1, nic);
            rs = ps.executeQuery();
            while (rs.next()) {
                existeNic = true;
                break;
            }

        } catch (Exception e) {

        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
            }
        }
        return existeNic;
    }

    @Override
    public String cargarClientesAll(String q) throws SQLException {

        con = null;
        ps = null;
        rs = null;
        String query = "CARGAR_CLIENTES_ALL";
        JsonArray lista = null;

        try {
            con = this.conectarJNDI();
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));
                ps.setString(1, q);
                ps.setString(2, q);
                rs = ps.executeQuery();
                lista = new JsonArray();
                JsonObject fila;
                while (rs.next()) {
                    fila = new JsonObject();
                    fila.addProperty("value", rs.getString("codcli"));
                    fila.addProperty("label", rs.getString("nomnit"));
                    fila.addProperty("id_tipo_cliente", rs.getString("id_tipo_cliente"));
                    fila.addProperty("tipo_cliente", rs.getString("tipo_cliente"));
                    lista.add(fila);
                }
            }
        } catch (Exception e) {
            lista = new JsonArray();
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return new Gson().toJson(lista);
        }
    }

    @Override
    public String getInfoClienteById(String idcliente) throws SQLException {
        con = null;
        rs = null;
        ps = null;
        Cliente cl = new Cliente();
        query = "cargar_Clientes_All_By_Id";
        String respuestaJson = "{}";

        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);
            ps.setString(1, idcliente);

            rs = ps.executeQuery();

            while (rs.next()) {
                cl.setCodcli(rs.getString("codcli"));
                cl.setNomcli(rs.getString("nomcli"));
                cl.setNit(rs.getString("nit"));
                cl.setTipo(rs.getString("tipo"));
                cl.setCiudad(rs.getString("ciudad"));
                cl.setDireccion(rs.getString("direccion"));
                cl.setNomContacto(rs.getString("nomcontacto"));
                cl.setTelContacto(rs.getString("telcontacto"));
                cl.setCargoContacto(rs.getString("cargo_contacto"));
                cl.setNomRepresentante(rs.getString("nombre_representante"));
                cl.setTelRepresentante(rs.getString("tel_representante"));
                cl.setCelRepresentante(rs.getString("celular_representante"));
                cl.setEmail(rs.getString("email_contacto"));
                cl.setCelContacto(rs.getString("cel_contacto"));
                cl.setEmailrepresentantelega(rs.getString("email_representante"));
                cl.setClasificacion(rs.getString("clasificacion"));
                cl.setDigito_verificacion(rs.getString("digito_verificacion"));
                cl.setIddepartamento(rs.getString("coddpt"));
            }
            respuestaJson = gson.toJson(cl);

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return respuestaJson;
    }

    @Override
    public String cargarListaApu(String idaccion) throws SQLException {
        con = null;
        ps = null;
        rs = null;
        String query = "CARGAR_LISTA_APU";

        JsonArray lista = null;

        try {
            con = this.conectarJNDI();
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));
                ps.setString(1, idaccion);
                rs = ps.executeQuery();
                lista = new JsonArray();
                JsonObject fila;
                while (rs.next()) {
                    fila = new JsonObject();
                    fila.addProperty("id", rs.getString("id"));
                    fila.addProperty("descripcion", rs.getString("nombre"));
                    lista.add(fila);
                }
            }
        } catch (Exception e) {
            lista = new JsonArray();
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return new Gson().toJson(lista);
        }
    }

    @Override
    public String cargarApuAsociados(String idaccion) {
        con = null;
        ps = null;
        rs = null;
        String query = "CARGAR_APU_ASOCIADOS";

        JsonArray lista = null;

        try {
            con = this.conectarJNDI();
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));
                ps.setString(1, idaccion);
                rs = ps.executeQuery();
                lista = new JsonArray();
                JsonObject fila;
                while (rs.next()) {
                    fila = new JsonObject();
                    fila.addProperty("id", rs.getString("id"));
                    fila.addProperty("id_apu", rs.getString("id_apu"));
                    fila.addProperty("descripcion", rs.getString("descripcion"));
                    lista.add(fila);
                }
            }
        } catch (Exception e) {
            lista = new JsonArray();
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return new Gson().toJson(lista);
        }
    }

    @Override
    public String asociarApuAccion(String idaccion, String listadoidapu, Usuario usuario) {
        con = null;
        ps = null;
        Statement stmt = null;
        String[] listadoidapus = listadoidapu.split(",");
        String respuestaJson = "{}";

        try {

            con = this.conectarJNDI();
            con.setAutoCommit(false);
            stmt = con.createStatement();
            query = "ASOCIAR_APU_ACCION";

            for (int i = 0; i < listadoidapus.length; i++) {

                st = new StringStatement(this.obtenerSQL(query), true);

                st.setString(1, idaccion);
                st.setString(2, listadoidapus[i]);
                st.setString(3, usuario.getLogin());
                st.setString(4, usuario.getLogin());
                stmt.addBatch(st.getSql());

            }

            stmt.executeBatch();
            stmt.clearBatch();
            con.commit();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            try {
                con.rollback();
                respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR anularMetaProceso \n" + e.getMessage());

            } catch (SQLException ex) {

            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
            }
            return respuestaJson;
        }
    }

    @Override
    public String desasociarApuAccion(String idaccion, String listadoidapu, Usuario usuario) {
        con = null;
        ps = null;
        Statement stmt = null;
        String[] listadoidapus = listadoidapu.split(",");
        String respuestaJson = "{}";

        try {

            con = this.conectarJNDI();
            con.setAutoCommit(false);
            stmt = con.createStatement();
            query = "DESASOCIAR_APU_ACCION";

            for (int i = 0; i < listadoidapus.length; i++) {

                st = new StringStatement(this.obtenerSQL(query), true);

                st.setString(1, idaccion);
                st.setString(2, listadoidapus[i]);
                stmt.addBatch(st.getSql());

            }

            stmt.executeBatch();
            stmt.clearBatch();
            con.commit();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            try {
                con.rollback();
                respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR anularMetaProceso \n" + e.getMessage());

            } catch (SQLException ex) {

            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
            }
            return respuestaJson;
        }
    }

    @Override
    public String asignarContratista(String idaccion, String idcontratista, Usuario usuario, String nomcontratista) throws SQLException {
        con = null;
        ps = null;
        String respuestaJson = "{}";
        query = "Asignar_Contratista";
        try {

            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);

            ps.setString(1, (" (" + usuario.getLogin() + ") : Se asigno contratista : (" + nomcontratista + ")."));
            ps.setString(2, idcontratista);
            ps.setString(3, usuario.getLogin());
            ps.setString(4, idaccion);

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            try {
                respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR guardarMetaProceso \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(ProcesosCatalogoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
            }
            return respuestaJson;
        }
    }

    @Override
    public String actualizarCartera(OfertaElca ofeca, Usuario usuario) {
        con = null;
        ps = null;
        Statement stmt = null;
        String respuestaJson = "{}";

        try {

            con = this.conectarJNDI();
            con.setAutoCommit(false);
            stmt = con.createStatement();
            stmt.addBatch(ActualizarCarteraQuery(ofeca, usuario));
            String etapa = "";
            String concepto;
            if (ofeca.getEstado_cartera().equals("010")) {
                etapa = "2";
                concepto = "Aprobado";
            } else {
                etapa = "2";
                concepto = "Rechazado";
            }
            stmt.addBatch(InsertarTrazabilidadQuery(ofeca.getId_solicitud(), etapa, usuario, ofeca.getDescripcion(), concepto, ""));

            stmt.executeBatch();
            stmt.clearBatch();
            con.commit();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            try {
                con.rollback();
                respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR anularMetaProceso \n" + e.getMessage());

            } catch (SQLException ex) {

            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
            }
            return respuestaJson;
        }
    }

    private String ActualizarCarteraQuery(OfertaElca ofeca, Usuario usuario) throws Exception {
        String query = "ACTUALIZAR_CARTERA";
        String respuesta = "";
        StringStatement st = null;

        try {
            st = new StringStatement(this.obtenerSQL(query), true);

            st.setString(1, (" (" + usuario.getLogin() + ") : " + ofeca.getDescripcion()));
            st.setString(2, usuario.getLogin());
            st.setString(3, ofeca.getEstado_cartera());
            st.setString(4, ofeca.getTipo_solicitud());
            st.setString(5, ofeca.getTipo_solicitud());
            st.setString(6, ofeca.getAviso());
            //st.setString(7, ofeca.getResponsable());
            st.setString(7, ofeca.getInterventor());
            st.setString(8, ofeca.getTipoDtf());

//            if (ofeca.getEstado_cartera().equals("010")) {
//                st.setString(10, "3");
//            } else {
//                st.setString(10, "1");
//            }
            st.setString(9, ofeca.getId_solicitud());

            respuesta = st.getSql();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
            throw new SQLException("ERROR DURANTE ACTUALIZAR_CARTERA. \n " + e.getMessage());
        } finally {
            if (st != null) {
                try {
                    st = null;
                } catch (Exception e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
        }

        return respuesta;
    }

    private String InsertarTrazabilidadQuery(String id_solicitud, String etapa, Usuario usuario, String comentarios, String concepto,
            String causal) throws Exception {
        String query = "INSERTAR_TRAZABILIDAD_QUERY";
        String respuesta = "";
        StringStatement st = null;

        try {
            st = new StringStatement(this.obtenerSQL(query), true);

            st.setString(1, id_solicitud);
            st.setInt(2, 1);
            st.setString(3, concepto);
            st.setString(4, usuario.getLogin());
            st.setInt(5, 100);

            respuesta = st.getSql();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
            throw new SQLException("ERROR DURANTE INSERTAR_TRAZABILIDAD_QUERY. \n " + e.getMessage());
        } finally {
            if (st != null) {
                try {
                    st = null;
                } catch (Exception e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
        }

        return respuesta;
    }

    private String InsertarAccionQuery(OfertaElca ofeca, Usuario usuario) throws Exception {

        ClientesVerDAO cvd = new ClientesVerDAO(usuario.getBd());
        SerieGeneral s_id_cliente = cvd.getSerie("FINV", "OP", "ACMS");
        cvd.setSerie("FINV", "OP", "ACMS");
        String idaccion = s_id_cliente.getUltimo_prefijo_numero();

        String query = "SQL_INSERT_ACCION_ECA";
        String respuesta = "";
        StringStatement st = null;

        try {
            st = new StringStatement(this.obtenerSQL(query), true);

            st.setString(1, idaccion);
            st.setString(2, ofeca.getId_solicitud());
            st.setString(3, "");
            st.setString(4, "Fase De Ingenieria y Cotizacion");
            st.setString(5, usuario.getLogin());
            st.setString(6, "Ingenieria Proyectos");
            st.setString(7, "1");
            respuesta = st.getSql();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
            throw new SQLException("ERROR DURANTE INSERTAR_TRAZABILIDAD_QUERY. \n " + e.getMessage());
        } finally {
            if (st != null) {
                try {
                    st = null;
                } catch (Exception e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
        }

        return respuesta;
    }

    @Override
    public String cargarSolicitudesIdcli(String idcli) {
        con = null;
        ps = null;
        rs = null;
        String query = "CARGAR_SOLICITUDES_IDCLI";

        JsonArray lista = null;

        try {
            con = this.conectarJNDI();
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));
                ps.setString(1, idcli);
                rs = ps.executeQuery();
                lista = new JsonArray();
                JsonObject fila;
                while (rs.next()) {
                    fila = new JsonObject();
                    fila.addProperty("id", rs.getString("id_solicitud"));
                    fila.addProperty("tipo_solicitud", rs.getString("tipo_solicitud"));
                    fila.addProperty("creation_date", rs.getString("creation_date"));
                    fila.addProperty("descripcion", rs.getString("descripcion"));
                    lista.add(fila);
                }
            }
        } catch (Exception e) {
            lista = new JsonArray();
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return new Gson().toJson(lista);
        }
    }

    @Override
    public String cargarLineaNegocio() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_LINEA_NEGOCIO";
        String respuesta = "{}";
        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            while (rs.next()) {
                obj.addProperty(rs.getString("cod_proyecto"), rs.getString("descripcion"));
            }
            respuesta = gson.toJson(obj);
        } catch (Exception ex) {
            respuesta = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return respuesta;
    }

    @Override
    public String cargarEstadoCartera() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_ESTADO_CARTERA";
        String respuesta = "{}";
        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            while (rs.next()) {
                obj.addProperty(rs.getString("id"), rs.getString("nombre"));
            }
            respuesta = gson.toJson(obj);
        } catch (Exception ex) {
            respuesta = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return respuesta;
    }

    @Override
    public String cargarInfoSolicitudes(String lineaNegocio, String responsable, String solicitud, String estadoCartera, String fechaInicio, String fechafin, String trazabilidad, String etapaActual, String tipo_proyecto, String foms, String id_cliente, String nom_proyecto) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_INFORMACION_SOLICITUDES";
        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            String consulta = "";
            String condicion = "";
            String condicion2 = "";
            String condicion3 = "";
            //  if (!trazabilidad.equals("1")) {
            if (etapaActual.equals("CARTERA")) {
                condicion = condicion + "and car.id = 'Estudio' and est.id not in ('1')";
            }
            if (etapaActual.equals("EJECUCION")) {
                condicion2 = condicion2 + " inner join opav.acciones acc on (ofe.id_solicitud = acc.id_solicitud )";
                condicion3 = condicion3 + "group by  ofe.fecha_validacion_cartera, ofe.interventor,ofe.id_solicitud ,cl.codcli,cl.nomcli,ofe.num_os,ofe.nombre_proyecto\n"
                        + "            ,ofe.creation_date ,ofe.tipo_solicitud,car.nombre,ofe.fecha_validacion_cartera\n"
                        + "            ,tbg.descripcion ,ofe.trazabilidad,est.nombre_etapa ,eto.id ,eto.nombre_estado,tbg2.descripcion,coo.presupuesto_terminado,coo.valor_cotizacion,ofe.nuevo_modulo";
            }
            

            if (!lineaNegocio.equals("")) {
                condicion = condicion + " and tipo_proyecto =  '" + lineaNegocio + "' ";
            }
            if (!fechaInicio.equals("") && !fechafin.equals("")) {
                condicion = condicion + " and ofe.fecha_validacion_cartera::date  between '" + fechaInicio + "'::date and '" + fechafin + "'::date"; //fecha de actulizacion de estado de la oferta
            }
            if (!responsable.equals("")) {
                condicion = condicion + " and tbg.descripcion ilike '%" + responsable + "%'";
            }
            if (!solicitud.equals("")) {
                condicion = condicion + " and ofe.id_solicitud = '" + solicitud + "'";
            }

            if (!estadoCartera.equals("")) {
                condicion = condicion + " and car.id = '" + estadoCartera + "'";
            }

            if (!tipo_proyecto.equals("")) {
                condicion = condicion + " and ofe.nuevo_modulo = '" + tipo_proyecto + "'";
            }

            if (!foms.equals("")) {
                condicion = condicion + " and ofe.num_os ilike  '%" + foms + "%'";
            }

            if (!id_cliente.equals("")) {
                condicion = condicion + " and ofe.id_cliente = '" + id_cliente + "'";
            }

            if (!nom_proyecto.equals("")) {
                condicion = condicion + " and ofe.nombre_proyecto ilike  '%" + nom_proyecto + "%'";
            }

            consulta = this.obtenerSQL(query).replaceAll("#parametro2", condicion2).replaceAll("#parametro", condicion);
            consulta = consulta.replaceAll("#patametro3", condicion3);
            ps = con.prepareStatement(consulta);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }
            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
 } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return informacion;
        }
    }

    @Override
    public String cargarEtapas(String etapa) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_ETAPA";
        String respuesta = "{}";
        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, etapa);
            rs = ps.executeQuery();
            while (rs.next()) {
                obj.addProperty(rs.getString("id"), rs.getString("nombre_etapa"));
            }
            respuesta = gson.toJson(obj);
        } catch (Exception ex) {
            respuesta = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return respuesta;
    }

    @Override
    public String cargarEstadoEtapas(String etapa, String id_estado) {
        con = null;
        ps = null;
        rs = null;
        String query = "SQL_CARGAR_ESTADO_ETAPA";
        JsonArray lista = null;

        try {
            con = this.conectarJNDI();
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));
                ps.setString(1, id_estado);
//                ps.setString(2, etapa);
                rs = ps.executeQuery();
                lista = new JsonArray();
                JsonObject fila;
                while (rs.next()) {
                    fila = new JsonObject();
                    fila.addProperty("id_estado_actual", rs.getString("id_estado_actual"));
                    fila.addProperty("id_estado_destino", rs.getString("id_estado_destino"));
                    fila.addProperty("id_etapa_destino", rs.getString("id_etapa_destino"));
                    fila.addProperty("nombre_estado", rs.getString("nombre_estado"));
                    lista.add(fila);
                }
            }
        } catch (Exception e) {
            lista = new JsonArray();
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return new Gson().toJson(lista);
        }

    }

    @Override
    public String trazabilidadOferta(String idsolicitud, Usuario usuario, String etapa, String estado, String causal, String observacion) {
        StringStatement st = null;
        String respuesta = "";
        String query = "SQL_TRAZABILIDAD_OFERTA";
        try {
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);
            st.setString(1, idsolicitud);
            st.setString(2, etapa);
            st.setString(3, causal);
            st.setString(4, observacion);
            st.setString(5, usuario.getLogin());
            st.setString(6, estado);
            respuesta = st.getSql();

        } catch (Exception e) {
            e.printStackTrace();
            respuesta = "ERROR";
        }
        return respuesta;
    }

    @Override
    public String cambioEstadoEtapaOferta(String idsolicitud, Usuario usuario, String estado_actual, String estado) {
        StringStatement st = null;
        String respuesta = "";
        String query = "SQL_CAMBIO_ESTADO_ETAPA_OFERTA";
        try {
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);
            st.setString(1, estado_actual);
            st.setString(2, estado);
            st.setString(3, estado);
            st.setString(4, usuario.getLogin());
            st.setString(5, idsolicitud);
            respuesta = st.getSql();

        } catch (Exception e) {
            e.printStackTrace();
            respuesta = "ERROR";
        }
        return respuesta;
    }

    @Override
    public String cargarReporteFacturacion(String numsolicitud) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_FACTURAS_MULTISERVICIO";
        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, numsolicitud);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }
            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                informacion = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return informacion;
    }

    @Override
    public String cargarCotizacion(String idaccion) {
        con = null;
        rs = null;
        ps = null;
        CotizacionSl cotizacion = new CotizacionSl();
        query = "SQL_CARGAR_COTIZACION";
        String respuestaJson = "{}";

        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);
            ps.setString(1, idaccion);

            rs = ps.executeQuery();

            while (rs.next()) {
                cotizacion.setReg_status(rs.getString("reg_status"));
                cotizacion.setDstrct(rs.getString("dstrct"));
                cotizacion.setId_accion(rs.getString("id_accion"));
                cotizacion.setNo_cotizacion(rs.getString("no_cotizacion"));
                cotizacion.setCod_cli(rs.getString("cod_cli"));
                cotizacion.setNonmbre_cliente(rs.getString("nonmbre_cliente"));
                cotizacion.setVigencia_cotizacion(rs.getString("vigencia_cotizacion"));
                cotizacion.setForma_visualizacion(rs.getString("forma_visualizacion"));
                cotizacion.setModalidad_comercial(rs.getString("modalidad_comercial"));
                cotizacion.setMaterial(rs.getDouble("material"));
                cotizacion.setMano_obra(rs.getDouble("mano_obra"));
                cotizacion.setEquipos(rs.getDouble("equipos"));
                cotizacion.setHerramientas(rs.getDouble("herramientas"));
                cotizacion.setTransporte(rs.getDouble("transporte"));
                cotizacion.setTramites(rs.getDouble("tramites"));
                cotizacion.setValor_cotizacion(rs.getDouble("valor_cotizacion"));
                cotizacion.setValor_descuento(rs.getDouble("valor_descuento"));
                cotizacion.setSubtotal(rs.getDouble("subtotal"));
                cotizacion.setPerc_iva(rs.getDouble("perc_iva"));
                cotizacion.setValor_iva(rs.getDouble("valor_iva"));
                cotizacion.setAdministracion(rs.getDouble("administracion"));
                cotizacion.setImprevisto(rs.getDouble("imprevisto"));
                cotizacion.setUtilidad(rs.getDouble("utilidad"));
                cotizacion.setPerc_aiu(rs.getDouble("perc_aiu"));
                cotizacion.setValor_aiu(rs.getDouble("valor_aiu"));
                cotizacion.setPerc_administracion(rs.getDouble("perc_administracion"));
                cotizacion.setPerc_imprevisto(rs.getDouble("perc_imprevisto"));
                cotizacion.setPerc_utilidad(rs.getDouble("perc_utilidad"));
                cotizacion.setTotal(rs.getDouble("total"));
                cotizacion.setAnticipo(rs.getString("anticipo"));
                cotizacion.setPerc_anticipo(rs.getDouble("perc_anticipo"));
                cotizacion.setValor_anticipo(rs.getDouble("valor_anticipo"));
                cotizacion.setRetegarantia(rs.getString("retegarantia"));
                cotizacion.setPerc_retegarantia(rs.getDouble("perc_retegarantia"));
                cotizacion.setLast_update(rs.getString("last_update"));
                cotizacion.setUser_update(rs.getString("user_update"));
                cotizacion.setCreation_date(rs.getString("creation_date"));
                cotizacion.setCreation_user(rs.getString("creation_user"));
                cotizacion.setPerc_descuento(rs.getDouble("perc_descuento"));
            }
            respuestaJson = gson.toJson(cotizacion);

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return respuestaJson;
    }

    @Override
    public String cargarFacturasParciales(String num_solicitud) {
        Gson gson = new Gson();
        con = null;
        ps = null;
        rs = null;
        JsonObject obj = new JsonObject();
        String query = "SQL_CARGAR_FACTURAS_PARCIALES";

        JsonArray arr = new JsonArray();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, num_solicitud);
            rs = ps.executeQuery();
            JsonObject fila = null;
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("id", rs.getString("id"));
                fila.addProperty("num_factura", rs.getString("num_factura"));
                fila.addProperty("valor", rs.getString("valor"));
                fila.addProperty("dias_ejecucion", rs.getString("dias_ejecucion"));
                fila.addProperty("fecha_facturacion", rs.getString("fecha_facturacion"));
                fila.addProperty("dia_pago", rs.getString("dia_pago"));
                fila.addProperty("amortizacion", rs.getString("valor_amortizacion"));
                fila.addProperty("retegarantia", rs.getString("valor_retegarantia"));
                arr.add(fila);
            }
            obj.add("rows", arr);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return gson.toJson(obj);
        }
    }

    @Override
    public String guardar_coste_Proyecto(CotizacionSl cotizacion, JsonObject facturas, String idsolicitud, Usuario usuario) {
        con = null;
        ps = null;
        Statement stmt = null;
        String respuestaJson = "{}";
        JsonObject respuesta = new JsonObject();
        try {
            con = this.conectarJNDI();
            con.setAutoCommit(false);
            stmt = con.createStatement();
            stmt.addBatch(actulizar_cabecera(cotizacion, usuario));

            JsonArray arrFact = facturas.getAsJsonArray("facturas");
            for (int i = 0; i < arrFact.size(); i++) {
                respuesta = arrFact.get(i).getAsJsonObject();
                String valor = respuesta.get("valor").getAsString();
                String dias_ejecucion = respuesta.get("dias_ejecucion").getAsString();
                String fecha_fact = respuesta.get("fecha_facturacion").getAsString();
                String dia_pago = respuesta.get("dia_pago").getAsString();
                String valor_amortizacion = respuesta.get("amortizacion").getAsString();
                String valor_retegarantia = respuesta.get("retegarantia").getAsString();
                if (respuesta.get("id").getAsString().startsWith("neo_")) {
                    stmt.addBatch(insertar_facturacion_parcial(i + 1, idsolicitud, valor, dias_ejecucion, fecha_fact, dia_pago, valor_amortizacion, valor_retegarantia, usuario));
                } else {
                    stmt.addBatch(actualizar_facturacion_parcial(respuesta.get("id").getAsString(), valor, dias_ejecucion, fecha_fact, dia_pago, valor_amortizacion, valor_retegarantia, usuario));
                }

            }
            stmt.executeBatch();
            stmt.clearBatch();
            con.commit();
            respuestaJson = "{\"respuesta\":\"OK\"}";
        } catch (Exception e) {
            try {
                con.rollback();
                e.printStackTrace();
                respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR AL GUARDAR CAMBIOS EN EL COSTO DEL PROYECTO \n" + e.getMessage());

            } catch (SQLException ex) {

            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
            }
            return respuestaJson;
        }
    }

    public String actulizar_cabecera(CotizacionSl cotizacion, Usuario usuario) throws Exception {

        String respuesta = "";
        st = null;
        query = "SQL_ACTUALIZAR_CABECERA_COTIZACION";
        try {
            st = new StringStatement(this.obtenerSQL(query), true);
            st.setString(1, cotizacion.getVigencia_cotizacion());
            st.setString(2, cotizacion.getForma_visualizacion());
            st.setString(3, cotizacion.getModalidad_comercial());
            st.setDouble(4, cotizacion.getValor_descuento());
            st.setDouble(5, cotizacion.getSubtotal());
            st.setDouble(6, cotizacion.getPerc_iva());
            st.setDouble(7, cotizacion.getValor_iva());
            st.setDouble(8, cotizacion.getAdministracion());
            st.setDouble(9, cotizacion.getImprevisto());
            st.setDouble(10, cotizacion.getUtilidad());
            st.setDouble(11, cotizacion.getPerc_aiu());
            st.setDouble(12, cotizacion.getValor_aiu());
            st.setDouble(13, cotizacion.getPerc_administracion());
            st.setDouble(14, cotizacion.getPerc_imprevisto());
            st.setDouble(15, cotizacion.getPerc_utilidad());
            st.setDouble(16, cotizacion.getTotal());
            st.setString(17, cotizacion.getAnticipo());
            st.setDouble(18, cotizacion.getPerc_anticipo());
            st.setDouble(19, cotizacion.getValor_anticipo());
            st.setString(20, cotizacion.getRetegarantia());
            st.setDouble(21, cotizacion.getPerc_retegarantia());
            st.setString(22, usuario.getLogin());
            st.setDouble(23, cotizacion.getPerc_descuento());
            st.setString(24, cotizacion.getId_accion());
        } catch (Throwable e) {
            e.printStackTrace();
            System.out.println(e.toString());
            throw new Exception("ERROR DURANTE actulizar_cabecera. \n " + e.getMessage());
        } finally {
            respuesta = st.getSql();

            return respuesta;
        }

    }

    public String insertar_facturacion_parcial(int consecutivo, String num_contrato, String valor,
            String dias_ejecucion, String fecha_facturacion, String dia_pago, String amortizacion,
            String retegarantia, Usuario usuario) throws Exception {

        String respuesta = "";
        st = null;
        query = "SQL_INSERTAR_FACTURACION_PARCIAL";

        try {

            st = new StringStatement(this.obtenerSQL(query), true);
            st.setInt(1, consecutivo);
            st.setString(2, num_contrato);
            st.setDouble(3, Double.parseDouble(valor));
            st.setInt(4, Integer.parseInt(dias_ejecucion));
            st.setString(5, fecha_facturacion);
            st.setInt(6, Integer.parseInt(dia_pago));
            st.setDouble(7, Double.parseDouble(amortizacion));
            st.setDouble(8, Double.parseDouble(retegarantia));
            st.setString(9, usuario.getLogin());
            st.setString(10, usuario.getDstrct());

            respuesta = st.getSql();

        } catch (Throwable e) {
            e.printStackTrace();
            System.out.println(e.toString());
            throw new Exception("ERROR DURANTE insertarFacturacionParcial. \n " + e.getMessage());
        }
        return respuesta;
    }

    public String actualizar_facturacion_parcial(String id, String valor, String dias_ejecucion, String fecha_facturacion,
            String dia_pago, String amortizacion, String retegarantia, Usuario usuario) throws Exception {

        st = null;
        String respuesta = "";
        query = "SQL_ACTUALIZA_FACTURACION_PARCIAL";

        try {

            st = new StringStatement(this.obtenerSQL(query), true);
            st.setDouble(1, Double.parseDouble(valor));
            st.setInt(2, Integer.parseInt(dias_ejecucion));
            st.setString(3, fecha_facturacion);
            st.setInt(4, Integer.parseInt(dia_pago));
            st.setDouble(5, Double.parseDouble(amortizacion));
            st.setDouble(6, Double.parseDouble(retegarantia));
            st.setString(7, usuario.getLogin());
            st.setInt(8, Integer.parseInt(id));

            respuesta = st.getSql();

        } catch (Throwable e) {
            e.printStackTrace();
            System.out.println(e.toString());
            throw new SQLException("ERROR DURANTE actualizarFacturacionParcial. \n " + e.getMessage());
        }
        return respuesta;

    }

    @Override
    public String cargarFacturasVenta(String num_solicitud) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_FACTURACION";
        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, num_solicitud);
            ps.setString(2, num_solicitud);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }
            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                informacion = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return informacion;
    }

    @Override
    public String cargarCotizacionFac(String num_solicitud) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_COTOZACION_FAC";
        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            if (perteneceNuevoModulo("1", num_solicitud)) {
                query = "SQL_CARGAR_COTOZACION_FAC";
            } else {
                query = "SQL_CARGAR_COTOZACION_FAC_TEM";
            }
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, num_solicitud);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }
            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                informacion = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return informacion;
    }

    @Override
    public String guardarFacturacion(JsonObject informacion, Usuario usuario) {
        JsonObject objeto = new JsonObject();
        StringStatement st = null;
        String respuesta = "";
        //String query = "SQL_CARGAR_REL_PRODUCTOS_TRANS";
        TransaccionService tService = null;
        try {
            tService = new TransaccionService(this.getDatabaseName());
            tService.crearStatement();
            JsonArray arr = informacion.getAsJsonArray("json");

            for (int i = 0; i < arr.size(); i++) {
                objeto = arr.get(i).getAsJsonObject();
                if (objeto.get("id").getAsString().startsWith("neo_")) {
                    //JsonObject f = arr.get(i).getAsJsonObject();
                    st = new StringStatement(this.obtenerSQL("SQL_GUARDAR_FACTURACION"), true);
                    st.setString(1, objeto.get("id_solicitud").getAsString());
                    st.setString(2, objeto.get("valor").getAsString());
                    st.setString(3, objeto.get("fecha_facturacion").getAsString());
                    st.setString(4, objeto.get("fecha_vencimiento").getAsString());
                    st.setString(5, objeto.get("amortizacion").getAsString());
                    st.setString(6, objeto.get("retegarantia").getAsString());
                    st.setString(7, objeto.get("total").getAsString());
                    st.setString(8, usuario.getLogin());
                } else {
                    st = new StringStatement(this.obtenerSQL("SQL_ACTUALIZAR_FACTURACION"), true);
                    st.setString(1, objeto.get("valor").getAsString());
                    st.setString(2, objeto.get("fecha_facturacion").getAsString());
                    st.setString(3, objeto.get("fecha_vencimiento").getAsString());
                    st.setString(4, objeto.get("amortizacion").getAsString());
                    st.setString(5, objeto.get("retegarantia").getAsString());
                    st.setString(6, usuario.getLogin());
                    st.setString(7, objeto.get("total").getAsString());
                    st.setString(8, objeto.get("id").getAsString());
                }

                tService.getSt().addBatch(st.getSql());
            }
            tService.execute();
            respuesta = "{\"respuesta\":\"Guardado\"}";
            //respuesta = st.getSql();
        } catch (Exception ex) {
            respuesta = "{\"respuesta\":\"ERROR\"}";
            //respuesta = ex.getMessage();
        } finally {
            try {
                tService.closeAll();
            } catch (Exception e) {
            }
            return respuesta;
        }
    }

    @Override
    public String cambiarEstadoEnviado(Usuario usuario, String id) {
        StringStatement st = null;
        String respuesta = "";
        String query = "SQL_ACTUALIZAR_ESTADO_A_EVIADO_CLIENTE";
        try {
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);
            st.setString(1, usuario.getLogin());
            st.setString(2, id);

            respuesta = st.getSql();
        } catch (Exception ex) {
            respuesta = ex.getMessage();
        } finally {
            return respuesta;
        }
    }

    @Override
    public String guardarControlAmortizacion(Usuario usuario, String num_factura, String valor, String amortizacion, String retegarantia, String id) {
        StringStatement st = null;
        String respuesta = "";
        String query = "SQL_GUARDAR_CONTROL_AMORTIZACION";
        try {
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);
            st.setString(1, num_factura);
            st.setString(2, valor);
            st.setString(3, amortizacion);
            st.setString(4, retegarantia);
            st.setString(5, usuario.getLogin());
            respuesta = st.getSql();
        } catch (Exception ex) {
            respuesta = ex.getMessage();
        } finally {
            return respuesta;
        }
    }

    @Override
    public String guardarHistoricoControlAmortizacion(Usuario usuario, String num_factura, String valor, String amortizacion, String retegarantia, String id) {
        StringStatement st = null;
        String respuesta = "";
        String query = "SQL_GUARDAR_HITORICO_CONTROL_AMORTIZACION";
        try {
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);
            st.setString(1, num_factura);
            st.setString(2, valor);
            st.setString(3, amortizacion);
            st.setString(4, retegarantia);
            st.setString(5, usuario.getLogin());
            respuesta = st.getSql();
        } catch (Exception ex) {
            respuesta = ex.getMessage();
        } finally {
            return respuesta;
        }
    }

    @Override
    public String eliminarFacturaCliente(String id) {
        StringStatement st = null;
        String respuesta = "";
        String query = "SQL_ELIMINAR_FACTURA_CLIENTE";
        try {
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);
            st.setString(1, id);
            respuesta = st.getSql();
        } catch (Exception ex) {
            respuesta = ex.getMessage();
        } finally {
            return respuesta;
        }
    }

    @Override
    public String eliminarControlAmortizacion(String num_factura) {
        StringStatement st = null;
        String respuesta = "";
        String query = "SQL_ELIMINAR_CONTROL_AMORTIZACION";
        try {
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);
            st.setString(1, num_factura);
            respuesta = st.getSql();
        } catch (Exception ex) {
            respuesta = ex.getMessage();
        } finally {
            return respuesta;
        }
    }

    @Override
    public String anular_factura_parcial(String id_factura_parcial) {
        con = null;
        ps = null;
        String respuestaJson = "{}";
        query = "SQL_ANULAR_FACTURA_PARCIAL";
        try {

            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);

            ps.setString(1, id_factura_parcial);

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
            }

        }
        return respuestaJson;
    }

    @Override
    public ArrayList searchNombresArchivos(String rutaOrigen, String id_solicitud) {
        ArrayList nombresArchivos = new ArrayList();
        File dir = new File(rutaOrigen + id_solicitud);
        try {
            if (dir.exists()) {
                String[] ficheros = dir.list();
                for (int x = 0; x < ficheros.length; x++) {
                    nombresArchivos.add(ficheros[x]);
                }
            }

        } catch (Exception e) {
            System.out.println("error en NegociosApplusDAO.getEstadosApplus.." + e.toString() + "__" + e.getMessage());
            e.printStackTrace();
        }

        return nombresArchivos;
    }

    @Override
    public boolean eliminarArchivo(String directorioArchivos, String numsol, String nomarchivo) {
        boolean deleted = false;
        File dir = new File(directorioArchivos + numsol);
        File archivo = new File(directorioArchivos + numsol + '/' + nomarchivo);
        try {
            if (dir.exists()) {
                if (archivo.exists()) {
                    deleted = archivo.delete();
                }
                if (dir.list().length == 0) {
                    deleteDirectory(dir);
                }
            }
        } catch (Exception e) {
            System.out.println("error en NegociosApplusDAO.getEstadosApplus.." + e.toString() + "__" + e.getMessage());
            e.printStackTrace();
        }
        return deleted;
    }

    @Override
    public boolean almacenarArchivoEnCarpetaUsuario(String numsolicitud, String rutaOrigen, String rutaDestino, String filename) {
        boolean swFileCopied = false;
        try {

            File carpetaDestino = new File(rutaDestino);

            //deleteDirectory(carpetaDestino);
            this.createDir(rutaDestino);

            try {
                //Copiamos el archivo de la carpeta origen a la carpeta destino
                InputStream in = new FileInputStream(rutaOrigen + "/" + filename);
                OutputStream out = new FileOutputStream(rutaDestino + filename);

                // Copy the bits from instream to outstream
                byte[] buf = new byte[1024];
                int len;
                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }
                in.close();
                out.close();
                swFileCopied = true;

            } catch (Exception k) {
                throw new Exception(" FILE: " + k.getMessage());
            }

        } catch (Exception e) {
            System.out.println("errorrr:" + e.toString() + "__" + e.getMessage());
            e.printStackTrace();
        }

        return swFileCopied;
    }

    public void createDir(String dir) throws Exception {
        try {
            File f = new File(dir);
            if (!f.exists()) {
                f.mkdir();
            }
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    @Override
    public String cargar_Insumos_Solicitud(String idsolicitud) {
        Gson gson = new Gson();
        con = null;
        ps = null;
        rs = null;
        JsonObject obj = new JsonObject();
        String query = "CARGAR_INSUMOS_SOLICITUD";

        JsonArray arr = new JsonArray();
        
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, idsolicitud);
            rs = ps.executeQuery();
            JsonObject fila = null;
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("id_solicitud", rs.getString("id_solicitud"));
                fila.addProperty("id_tipo_insumo", rs.getString("id_tipo_insumo"));
                fila.addProperty("descripcion_insumo", rs.getString("descripcion_insumo"));
                fila.addProperty("id_categoria", rs.getString("id_categoria"));
                fila.addProperty("nombre_categoria", rs.getString("nombre_categoria"));
                fila.addProperty("id_subcategoria", rs.getString("id_subcategoria"));
                fila.addProperty("nombre_subcategoria", rs.getString("nombre_subcategoria"));
                fila.addProperty("id_insumo", rs.getString("id_insumo"));
                fila.addProperty("nombre_insumo", rs.getString("nombre_insumo"));
                fila.addProperty("condigo_material", rs.getString("condigo_material"));
                fila.addProperty("id_apu", rs.getString("id_apu"));
                fila.addProperty("nombre_apu", rs.getString("nombre_apu"));
                fila.addProperty("id_grupo_apu", rs.getString("id_grupo_apu"));
                fila.addProperty("nombre_grupo_apu", rs.getString("nombre_grupo_apu"));
                fila.addProperty("id_actividad_wbs", rs.getString("id_actividad_wbs"));
                fila.addProperty("nombre_actividad", rs.getString("nombre_actividad"));
                fila.addProperty("id_capitulo_wbs", rs.getString("id_capitulo_wbs"));
                fila.addProperty("nombre_capitulo", rs.getString("nombre_capitulo"));
                fila.addProperty("id_disciplina_wbs", rs.getString("id_disciplina_wbs"));
                fila.addProperty("nombre_disciplina", rs.getString("nombre_disciplina"));
                fila.addProperty("id_area_proyecto_wbs", rs.getString("id_area_proyecto_wbs"));
                fila.addProperty("nombre_area", rs.getString("nombre_area"));
                fila.addProperty("nombre_proyecto", rs.getString("nombre_proyecto"));
                arr.add(fila);
            }
            obj.add("rows", arr);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return gson.toJson(obj);
        }
    }

    @Override
    public String cargar_Historico_Cotizacion(String idsolicitud) {
        Gson gson = new Gson();
        String idaccion = cargar_idaccion_principal(idsolicitud);
        con = null;
        ps = null;
        rs = null;
        JsonObject obj = new JsonObject();
        String query = "CARGAR_HISTORICO_COTIZACION";

        JsonArray arr = new JsonArray();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, idaccion);
            rs = ps.executeQuery();
            JsonObject fila = null;
            while (rs.next()) {
                fila = new JsonObject();//id,creation_date,total 
                fila.addProperty("id_historico_cotizacion", rs.getString("id"));
                fila.addProperty("creation_date", rs.getString("creation_date"));
                fila.addProperty("last_update", rs.getString("last_update"));
                fila.addProperty("total", rs.getString("total"));
                arr.add(fila);
            }
            obj.add("rows", arr);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return gson.toJson(obj);
        }
    }

    public static JsonObject getObjectJson(String json) {
        JsonParser parser = new JsonParser();
        JsonObject o = parser.parse(json).getAsJsonObject();
        return o;
    }

    public String cargar_idaccion_principal(String idsolicitud) {
        con = null;
        ps = null;
        rs = null;
        String x = "";
        String query = "CARGAR_IDACCION_PRINCIPAL";

        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, idsolicitud);
            rs = ps.executeQuery();

            while (rs.next()) {
                x = rs.getString("id_accion");
                break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return x;
        }
    }

    @Override
    public String crear_Historico_Cotizacion(String idsolicitud, Usuario usuario) {
        String idaccion = cargar_idaccion_principal(idsolicitud);
        String idcotizacion = cargar_idcotizacion(idaccion);
        int resp = 0;
        con = null;
        ps = null;
        Statement stmt = null;
        JsonObject respuesta = new JsonObject();
        String respuestaJson = "{}";

        try {

            con = this.conectarJNDI();
            con.setAutoCommit(false);
            ps = con.prepareStatement(this.obtenerSQL("INSERTAR_CABECERA_COTIZACION_HISTORICO"), Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, usuario.getLogin());
            ps.setString(2, idaccion);
            resp = ps.executeUpdate();
            stmt = con.createStatement();
            if (resp > 0) {
                ResultSet rs = ps.getGeneratedKeys();
                if (rs.next()) {
                    stmt.addBatch(Insertar_Detalle_Cotizacion_Historico(idcotizacion, rs.getString(1), usuario));
                }
            }

            stmt.executeBatch();
            stmt.clearBatch();
            con.commit();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            try {
                con.rollback();
                respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR anularMetaProceso \n" + e.getMessage());

            } catch (SQLException ex) {

            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
            }
            return respuestaJson;
        }

    }

    public String cargar_idcotizacion(String idaccion) {
        con = null;
        ps = null;
        rs = null;
        String x = "";
        String query = "CARGAR_IDCOTIZACION";

        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, idaccion);
            rs = ps.executeQuery();

            while (rs.next()) {
                x = rs.getString("id");
                break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return x;
        }
    }

    private String Insertar_Cabecera_Cotizacion_Historico(String idaccion, Usuario usuario) throws Exception {
        String query = "INSERTAR_CABECERA_COTIZACION_HISTORICO";
        String respuesta = "";
        StringStatement st = null;

        try {
            st = new StringStatement(this.obtenerSQL(query), true);

            st.setString(1, usuario.getLogin());
            st.setString(2, idaccion);

            respuesta = st.getSql();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
            throw new SQLException("ERROR EN LA CREACION DE LA CABECERA DE LA COTIZACION \n " + e.getMessage());
        } finally {
            if (st != null) {
                try {
                    st = null;
                } catch (Exception e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
        }

        return respuesta;
    }

    private String Insertar_Detalle_Cotizacion_Historico(String idcotizacion, String id_historico, Usuario usuario) throws Exception {
        String query = "INSERTAR_DETALLE_COTIZACION_HISTORICO";
        String respuesta = "";
        StringStatement st = null;

        try {
            st = new StringStatement(this.obtenerSQL(query), true);

            st.setString(1, id_historico);
            st.setString(2, usuario.getLogin());
            st.setString(3, idcotizacion);

            respuesta = st.getSql();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
            throw new SQLException("ERROR DURANTE ACTUALIZAR_CLIENTE. \n " + e.getMessage());
        } finally {
            if (st != null) {
                try {
                    st = null;
                } catch (Exception e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
        }

        return respuesta;
    }

    @Override
    public String cargar_Areas_Historico_cotizacion(String idsolicitud) {
        Gson gson = new Gson();
        con = null;
        ps = null;
        rs = null;
        JsonObject obj = new JsonObject();
        String query = "CARGAR_AREAS_HISTORICO_COTIZACION";

        JsonArray arr = new JsonArray();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, idsolicitud);
            rs = ps.executeQuery();
            JsonObject fila = null;
            while (rs.next()) {
                fila = new JsonObject();
                fila.addProperty("id_area_proy", rs.getString("id_area_proy"));
                fila.addProperty("valor", rs.getString("valor"));
                fila.addProperty("descripcion", rs.getString("descripcion"));
                fila.addProperty("id_solicitud", rs.getString("id_solicitud"));
                fila.addProperty("Rent_Contratista", rs.getString("prom_act_contra"));
                fila.addProperty("val_Contratista", rs.getString("sum_act_contra"));
                fila.addProperty("Rent_Esquema", rs.getString("prom_act_esquema"));
                fila.addProperty("val_Esquema", rs.getString("sum_act_esquema"));
                arr.add(fila);
            }
            obj.add("rows", arr);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return gson.toJson(obj);
        }
    }

    @Override
    public String cargar_Disciplinas_Historico_Cotizacion(String idarea) {
        Gson gson = new Gson();
        con = null;
        ps = null;
        rs = null;
        JsonObject obj = new JsonObject();
        String query = "CARGAR_DISCIPLINAS_HISTORICO_COTIZACION";

        JsonArray arr = new JsonArray();
        try {
            if (!idarea.equals("")) {
                con = this.conectarJNDI();
                query = this.obtenerSQL(query).replace("#filtro", idarea);
                ps = con.prepareStatement(query);
                rs = ps.executeQuery();
                JsonObject fila = null;
                while (rs.next()) {
                    fila = new JsonObject();
                    fila.addProperty("id_area_proyecto", rs.getString("id_area_proyecto"));
                    fila.addProperty("id_disciplina_area", rs.getString("id_disciplina_area"));
                    fila.addProperty("valor", rs.getString("valor"));
                    fila.addProperty("descripcion", rs.getString("descripcion"));
                    fila.addProperty("id_disciplina", rs.getString("id_disciplina"));
                    fila.addProperty("Rent_Contratista", rs.getString("prom_act_contra"));
                    fila.addProperty("val_Contratista", rs.getString("sum_act_contra"));
                    fila.addProperty("Rent_Esquema", rs.getString("prom_act_esquema"));
                    fila.addProperty("val_Esquema", rs.getString("sum_act_esquema"));
                    arr.add(fila);
                }
            }
            obj.add("rows", arr);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return gson.toJson(obj);
        }
    }

    @Override
    public String cargar_Capitulos_Historico_cotizacion(String iddisciplina) {
        Gson gson = new Gson();
        con = null;
        ps = null;
        rs = null;
        JsonObject obj = new JsonObject();
        String query = "CARGAR_CAPITULOS_HISTORICO_COTIZACION";

        JsonArray arr = new JsonArray();
        try {
            if (!iddisciplina.equals("")) {

                con = this.conectarJNDI();
                query = this.obtenerSQL(query).replace("#filtro", iddisciplina);
                ps = con.prepareStatement(query);
                rs = ps.executeQuery();
                JsonObject fila = null;
                while (rs.next()) {
                    fila = new JsonObject();
                    fila.addProperty("id_disciplina_area", rs.getString("id_disciplina_area"));
                    fila.addProperty("id_capitulo", rs.getString("id_capitulo"));
                    fila.addProperty("valor", rs.getString("valor"));
                    fila.addProperty("descripcion", rs.getString("descripcion"));
                    fila.addProperty("Rent_Contratista", rs.getString("prom_act_contra"));
                    fila.addProperty("val_Contratista", rs.getString("sum_act_contra"));
                    fila.addProperty("Rent_Esquema", rs.getString("prom_act_esquema"));
                    fila.addProperty("val_Esquema", rs.getString("sum_act_esquema"));
                    arr.add(fila);
                }
            }
            obj.add("rows", arr);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return gson.toJson(obj);
        }
    }

    @Override
    public String cargar_Actividades_Historico_Cotizacion(String idcapitulo) {
        Gson gson = new Gson();
        con = null;
        ps = null;
        rs = null;
        JsonObject obj = new JsonObject();
        String query = "SQL_GET_ACTIVIDADES_CAPITULO";

        JsonArray arr = new JsonArray();
        try {
            if (!idcapitulo.equals("")) {

                con = this.conectarJNDI();
                query = this.obtenerSQL(query).replace("#filtro", idcapitulo);
                ps = con.prepareStatement(query);
                rs = ps.executeQuery();
                JsonObject fila = null;
                while (rs.next()) {
                    fila = new JsonObject();
                    fila.addProperty("id", rs.getString("id"));
                    fila.addProperty("id_actividad", rs.getString("id_actividad"));
                    fila.addProperty("descripcion", rs.getString("descripcion"));
                    fila.addProperty("valor", rs.getString("valor_act"));
                    fila.addProperty("Rent_Contratista", rs.getString("prom_act_contra"));
                    fila.addProperty("val_Contratista", rs.getString("sum_act_contra"));
                    fila.addProperty("Rent_Esquema", rs.getString("prom_act_esquema"));
                    fila.addProperty("val_Esquema", rs.getString("sum_act_esquema"));
                    arr.add(fila);
                }
            }
            obj.add("rows", arr);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return gson.toJson(obj);
        }
    }

    @Override
    public String cargarAnticipos(String num_solicitud) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_ANTICIPOS";
        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, num_solicitud);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }
            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                informacion = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return informacion;
    }

    @Override
    public String guardarAnticipo(JsonObject informacion, Usuario usuario) {
        JsonObject objeto = new JsonObject();
        String respuesta = "";
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_PROCESO_GENERACION_ANTICIPO_Y_DOCUMENTO_OPERATIVOS";
        String retorno = "";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            JsonArray arr = informacion.getAsJsonArray("json");
            for (int i = 0; i < arr.size(); i++) {
                objeto = arr.get(i).getAsJsonObject();
                if (objeto.get("id").getAsString().equals("")) {
                    ps.setString(1, informacion.get("cod_cli").getAsString());
                    ps.setString(2, informacion.get("codcot").getAsString());
                    ps.setString(3, objeto.get("id_solicitud").getAsString());
                    ps.setDouble(4, informacion.get("perc_anticipo").getAsDouble());
                    ps.setDouble(5, (int) (informacion.get("val_anticipo").getAsDouble() != 0 ? informacion.get("val_anticipo").getAsDouble() : 0));
                    ps.setString(6, usuario.getLogin());
                    ps.setString(7, objeto.get("id_tipo_anticipo").getAsString());
                    rs = ps.executeQuery();
                    while (rs.next()) {
                        retorno = rs.getBoolean("retorno") ? "SI" : "NO";
                    }
                }
            }
            respuesta = "{\"respuesta\":\"" + retorno + "\"}";
        } catch (Exception ex) {
            ex.printStackTrace();
            respuesta = "{\"respuesta\":\"ERROR\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
            return respuesta;
        }
    }

    public String numDocumento() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_BUSCAR_NUMERO_DOCUMENTO_CXC_ANTICIPO";
        String informacion = "";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            while (rs.next()) {
                informacion = rs.getString("numdoc");
            }

        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return informacion;
        }
    }

    public String buscarHcCuentas() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_BUSCAR_HC_CUENTA_CXC";
        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));

            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }
            informacion = new Gson().toJson(objetoJson);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return informacion;
        }
    }

    public String nitCliente(String codcli) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_BUSCAR_NIT_CLIENTE";
        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, codcli);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }
            informacion = new Gson().toJson(objetoJson);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "\"respuesta\":\"ERROR\"";
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return informacion;
        }
    }

    @Override
    public String exportarPdfCuentaCobro(Usuario usuario, JsonObject informacion) {
        String respuesta = "";
        JsonObject objeto = new JsonObject();
        try {
            String consecutivo;
            int total;
            String parrafo = "";
            JsonArray arr = informacion.getAsJsonArray("json");
            String nit, nombreCli, porcent, id_solicitud, nombre_proyecto, empresa, firma, cuenta;

            //  for (int i = 0; i < arr.size(); i++) {
            objeto = arr.get(arr.size() - 1).getAsJsonObject();
            String codcli = informacion.get("cod_cli").getAsString();
            JsonObject nitCliente = (JsonObject) new JsonParser().parse(nitCliente(codcli));
            nit = nitCliente.get("nit").getAsString();
            nombreCli = Util.setCodificacionCadena(nitCliente.get("nomcli").getAsString(), "ISO-8859-1");
            nombre_proyecto = Util.setCodificacionCadena(informacion.get("nombre_proyecto").getAsString(), "ISO-8859-1");
            total = (int) (objeto.get("valor").getAsDouble() != 0 ? objeto.get("valor").getAsDouble() : 0);
            consecutivo = objeto.get("cod_anticipo").getAsString();
            porcent = informacion.get("perc_anticipo").getAsString();
            empresa = informacion.get("empresa").getAsString();
            firma = informacion.get("firma").getAsString();
            cuenta = informacion.get("cuenta").getAsString();
            id_solicitud = objeto.get("id_solicitud").getAsString();
            parrafo = "La presente es para informarle que su P?liza Todo Riesgo de Veh?culo sera renovada en "
                    + "el pr?ximo mes. Lo anterior para que realice los respectivos tr?mites ante nuestra entidad.";

            respuesta = generarPdf(total, usuario.getLogin(), consecutivo, codcli, nit, nombreCli, porcent, id_solicitud, nombre_proyecto, empresa, firma, cuenta);
            // }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return respuesta;
    }

    public String generarPdf(double total, String login_usuario, String consecutivo, String codcli, String nit, String nombreCli, String porcent, String id_solicitud, String nombre_proyecto, String empresa, String firma, String cuenta) throws Exception {
        String ruta = "";
        String msg = "OK";
        String nombre_firma = null;
        String cedula_firma = null;
        String debe_a = null;
        String nit_a = null;
        String cuenta_ = null;
        try {
            ruta = this.directorioArchivo(login_usuario, consecutivo, "pdf");
            ResourceBundle rsb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            Image imagen = null;
            Image imagenP = null;
            if (empresa.equals("selectrik")) {
                imagen = Image.getInstance(rsb.getString("ruta") + "/images/logo_selectrik_cuenta_cobro.png");
                imagenP = Image.getInstance(rsb.getString("ruta") + "/images/pie_pagina_Selectrik_2.png");
                debe_a = "SELECTRIK S.A.S.";
                nit_a = "Nit: 900.843.992-3";
            } else if (empresa.equals("fintra")) {
                imagen = Image.getInstance(rsb.getString("ruta") + "/images/logo_Fintra.png");
                imagenP = Image.getInstance(rsb.getString("ruta") + "/images/piefintra.png");
                debe_a = "FINTRA S.A";
                nit_a = "Nit: 802.022.016-1";
            }

            Font fuente = new Font(Font.HELVETICA, 12, Font.NORMAL, new java.awt.Color(0, 0, 0));
            Font fuenteG = new Font(Font.HELVETICA, 12, Font.BOLD, new java.awt.Color(0, 0, 0));
            PdfPCell celda = null;
            Document documento = null;
            documento = this.createDoc();
            PdfWriter.getInstance(documento, new FileOutputStream(ruta));
            documento.open();
            documento.newPage();
            documento.setMargins(0, 0, 0, 0);
            HTMLWorker htmlWorker = new HTMLWorker(documento);
            imagen.setAlignment(PdfPTable.ALIGN_RIGHT);

            //
            imagen.scaleToFit(180, 180);
            documento.add(imagen);

            documento.add(new Paragraph(new Phrase(" ", fuente)));
            Calendar calendario = Calendar.getInstance();
            String mes = "";
            mes = this.mesToString(calendario.get(Calendar.MONTH) + 1);
            documento.add(new Paragraph(new Phrase("Cuenta de cobro No. " + consecutivo, fuente)));
            documento.add(new Paragraph(new Phrase("Barranquilla" + ", " + mes + " " + calendario.get(Calendar.DAY_OF_MONTH) + " de " + calendario.get(Calendar.YEAR), fuente)));
            PdfPTable thead = new PdfPTable(1);
            celda = new PdfPCell();
            celda.setBorder(0);
            celda.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            celda.setPhrase(new Paragraph(new Phrase(" ", fuente)));
            thead.addCell(celda);
            celda.setPhrase(new Paragraph(new Phrase(" ", fuente)));
            thead.addCell(celda);

            celda.setPhrase(new Paragraph(new Phrase(" ", fuente)));
            thead.addCell(celda);
            celda.setPhrase(new Paragraph(new Phrase(" ", fuente)));
            thead.addCell(celda);
            celda.setPhrase(new Paragraph(new Phrase(" ", fuente)));
            thead.addCell(celda);
            documento.add(thead);
            //
            thead = new PdfPTable(1);
            celda.setPhrase(new Paragraph(new Phrase(nombreCli, fuenteG)));
            thead.addCell(celda);
            celda.setPhrase(new Paragraph(new Phrase("", fuente)));
            thead.addCell(celda);
            documento.add(thead);
            //
            thead = new PdfPTable(1);
            celda.setPhrase(new Paragraph(new Phrase("Nit: " + nit, fuente)));
            thead.addCell(celda);
            celda.setPhrase(new Paragraph(new Phrase(" ", fuente)));
            thead.addCell(celda);
            documento.add(thead);
            //
            thead = new PdfPTable(1);
            celda.setPhrase(new Paragraph(new Phrase("Debe A: ", fuente)));
            thead.addCell(celda);
            celda.setPhrase(new Paragraph(new Phrase(" ", fuente)));
            thead.addCell(celda);
            documento.add(thead);
            //
            thead = new PdfPTable(1);
            celda.setPhrase(new Paragraph(new Phrase(debe_a, fuenteG)));
            thead.addCell(celda);
            celda.setPhrase(new Paragraph(new Phrase("", fuente)));
            thead.addCell(celda);
            documento.add(thead);
            //
            thead = new PdfPTable(1);
            celda.setPhrase(new Paragraph(new Phrase(nit_a, fuente)));
            thead.addCell(celda);
            celda.setPhrase(new Paragraph(new Phrase(" ", fuente)));
            thead.addCell(celda);
            celda.setPhrase(new Paragraph(new Phrase(" ", fuente)));
            thead.addCell(celda);
            celda.setPhrase(new Paragraph(new Phrase(" ", fuente)));
            thead.addCell(celda);
            celda.setPhrase(new Paragraph(new Phrase(" ", fuente)));
            thead.addCell(celda);
            documento.add(thead);
            //
            RMCantidadEnLetras c = new RMCantidadEnLetras();
            String[] a;
            a = c.getTexto(total);
            String res = "";
            for (int i = 0; i < a.length; i++) {
                res = res + ((String) a[i]).replace("-", " ");
            }
            res = res.trim();
//            documento.add(new Paragraph(new Phrase("La suma de " + res + " pesos ($ " + Util.customFormat(total) + ") por concepto"
//                    + " de " + porcent + "% ANTICIPO DE PRESTACION DE SERVICIOS PARA " + nombre_proyecto, fuente)));
            String str1 = "La suma de " + res + " pesos ($ " + Util.customFormat(total) + ") por concepto"
                    + " de <b>" + porcent + "% ANTICIPO DE PRESTACION DE SERVICIOS PARA " + nombre_proyecto + "</b>";

            htmlWorker.parse(new StringReader(str1));

            documento.add(new Paragraph(new Phrase(" ", fuente)));

            if (cuenta.equals("cuentaFintra")) {
                cuenta_ = "de ahorro de Bancolombia No.692-258459-48 a nombre de FINTRA S.A NIT 802.022.016-1";
            } else if (cuenta.equals("cuentaSelectrik")) {
                cuenta_ = "corriente de Bancolombia No.692-422832-77 a nombre de SELECTRIK S.A NIT 900.843.992-3";
            }
            String str = "Favor consignar a la cuenta " + cuenta_ + ".";
            htmlWorker.parse(new StringReader(str));
//            documento.add(new Paragraph(new Phrase("Favor consignar a la cuenta de ahorro de Bancolombia No.692-258459-48 a nombre de FINTRA S.A "
//                    + "NIT 802.022.016-1", fuente)));
            //
            if (firma.equals("jgomez")) {
                nombre_firma = "JOSE LUIS GOMEZ OLARTE";
                cedula_firma = "C.C 85.465.887 de Santa Marta";
            } else if (firma.equals("cramos")) {
                nombre_firma = "CARLOS FELIPE RAMOS FONTALVO ";
                cedula_firma = "C.C 1.129.568.031 de Barranquilla";
            }
            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(new Paragraph(new Phrase(" ", fuente)));
            documento.add(new Paragraph(new Phrase("__________________________________", fuente)));
            documento.add(new Paragraph(new Phrase(nombre_firma, fuenteG)));
            documento.add(new Paragraph(new Phrase(cedula_firma, fuente)));
//            imagenP.scaleToFit(180,180);
            if (empresa.equals("selectrik")) {
                imagenP.scaleAbsoluteWidth(600);
                imagenP.scaleAbsoluteHeight(150);
                imagenP.setAbsolutePosition(0f, 0f);
                imagenP.setAlignment(PdfPTable.ALIGN_CENTER);
            } else if (empresa.equals("fintra")) {
                documento.add(new Paragraph(new Phrase(" ", fuente)));
                imagenP.scaleAbsoluteWidth(550);
                imagenP.scaleAbsoluteHeight(80);
                imagenP.setAlignment(PdfPTable.ALIGN_LEFT);
            }
            documento.add(imagenP);
            documento.close();
        } catch (Exception e) {
            msg = "ERROR";
            e.printStackTrace();
        }
        return msg;
    }
    
    
    
    
            
            
    
    @Override
    public String generar_Pdf_Cotizacion_APU(String solicitud, String ruta, JsonObject jsonObjCot, JsonArray jsonArrAreas, JsonArray jsonArrCap,JsonArray jsonArrNotas, Usuario usuario, String detallado , boolean precios) {

        con = null;
        ps = null;
        rs = null;
        String sql = "";
        query = "obtenerInfoCotizacion";
        String nomarchivo = "";
        String html_doc = "", fecha_corte = "";
        ResourceBundle rb = null;
        String respuestaJson = "{}";
        try {
            DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
            //get current date time with Date()
            Date date = new Date();
            nomarchivo =  ("cotizacion_" + dateFormat.format(date)).replace(":", "_") + ".pdf";
            //System.out.println(dateFormat.format(date));
            rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            
             Rectangle pageSize = new Rectangle(960f, 530f); //ancho y alto
            
            Document document = new Document();

            document.setPageSize(pageSize);

            HTMLWorker htmlWorker = null;
            
//            HTMLWorker titleWorker=new HTMLWorker(document);
//           titleWorker.parse(new StringReader("<h1 style='color:orange;font-family:Bebas Neue;font-weight:bold'>VALOR DE LA INVERSI�N</h1>"));


            File newFile = new File(ruta, nomarchivo);
            newFile.createNewFile();
            PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(newFile));
            
            
            
           ResourceBundle rsb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");

            
            
//             HTMLWorker titleWorker=new HTMLWorker(document);
//           titleWorker.parse(new StringReader("<h1 style='color:orange;font-family:Bebas Neue;font-weight:bold'>VALOR DE LA INVERSI�N</h1>"));

              BaseFont baseFontTable = BaseFont.createFont(rsb.getString("rutaImagenes") +"/fuentes/Roboto-Regular.ttf", "Cp1252",  true);
              Font fuenteTable=new Font(baseFontTable);
              fuenteTable.setSize(8f);



            Font f = new Font(Font.TIMES_ROMAN, 12, Font.NORMAL);
            Font f1 = new Font(Font.TIMES_ROMAN, 12, Font.BOLD);

            Font f2 = fuenteTable;
            Font f3 = new Font(Font.TIMES_ROMAN, 8, Font.BOLD);

            Font f4 = new Font(Font.TIMES_ROMAN, 10, Font.NORMAL);

            Font fb = new Font(Font.TIMES_ROMAN, 12, Font.NORMAL, new Color(255, 255, 255));
            Font f1b = new Font(Font.TIMES_ROMAN, 12, Font.BOLD, new Color(255, 255, 255));
            Font f2b = fuenteTable;
            Font f3b = fuenteTable;
            Font f4b = new Font(Font.TIMES_ROMAN, 10, Font.NORMAL, new Color(255, 255, 255));

            HeaderFooterCot header = new HeaderFooterCot(rb);
            pdfWriter.setPageEvent(header);
            

            document.open();
            
            document.addAuthor(usuario.getLogin());
            document.addCreator("SELECTRIK S.A.");
            document.addCreationDate();
            document.addTitle("Cotizacion Selectrik");

            con = this.conectarJNDI();
            /* query = this.obtenerSQL(query);
             ps = con.prepareStatement(query);         
        
             rs = ps.executeQuery();

             while (rs.next()) {              
             html_doc = rs.getString("info");      
             fecha_corte = rs.getString("fecha_corte");      
             }*/

            //htmlWorker.parse(new StringReader("" + html_doc.replaceAll("P1", fecha_corte).replaceAll("P2","  "+ diasMora+" d?as") + ""));     
            // query = "obtenerDetalleCotizacion";
            PdfPTable table = new PdfPTable(6);
            table.setWidths(new int[]{2, 9, 3, 2, 4, 4});
            table.setWidthPercentage(100);
            PdfPCell cell;
            //ENCABEZADOS DE LA TABLA
            // row 1, cell 1
            cell = new PdfPCell(new Phrase("CODIGO", f3b));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBackgroundColor(new Color(255, 145, 0));
            table.addCell(cell);

            // row 1, cell 2
            cell = new PdfPCell(new Phrase("DESCRIPCION", f3b));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBackgroundColor(new Color(255, 145, 0));
            table.addCell(cell);

            // row 1, cell 3
            cell = new PdfPCell(new Phrase("UND", f3b));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBackgroundColor(new Color(255, 145, 0));
            table.addCell(cell);

            // row 1, cell 4
            cell = new PdfPCell(new Phrase("CANT", f3b));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBackgroundColor(new Color(255, 145, 0));
            table.addCell(cell);

            // row 1, cell 5
            cell = new PdfPCell(new Phrase("VR-UNIT", f3b));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBackgroundColor(new Color(255, 145, 0));
            table.addCell(cell);

            // row 1, cell 5
            cell = new PdfPCell(new Phrase("SUB-TOTAL", f3b));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBackgroundColor(new Color(255, 145, 0));
            table.addCell(cell);
            DecimalFormat formato = new DecimalFormat("$###,###,###");
            switch (detallado) {
                case "SI":
                    query = "SQL_ODTENER_DETALLE_COTIZACION_SIN_ADMINISTRACION";
                    break;
                case "NO":
                    query = "obtenerDetalleCotizacion";
                    break;
            }
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, solicitud);
            rs = ps.executeQuery();
            
            String totalInversion="";
            int indexArea = 1, indexCap = 1, indexApu = 1, pindex = 0;
            String id_area = "", area = "", id_capitulo = "", capitulo = "", pIndexApu = "";
            double subtotalCot = 0, totalIva = 0, totalAdm = 0, totalImp = 0, totalUtil = 0, totalCot = 0;
            while (rs.next()) {
                if (!id_area.equals(rs.getString("id_area"))) {
                    id_area = rs.getString("id_area");
                    area = rs.getString("nombre_area");

                    // row 1, cell 1
                    cell = new PdfPCell(new Phrase(String.valueOf(indexArea), f3b));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setBackgroundColor(new Color(255, 145, 0));
                    table.addCell(cell);
                    // row 1, cell 2
                    cell = new PdfPCell(new Phrase(area, f3b));
                    cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                    cell.setColspan(4);
                    cell.setBackgroundColor(new Color(255, 145, 0));
                    table.addCell(cell);
                    // row 1, cell 6
                        cell = new PdfPCell(new Phrase(formato.format(Double.parseDouble(getValueFromArray(jsonArrAreas, rs.getString("id_area"), "total_area"))), f3b));
                    cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    cell.setBackgroundColor(new Color(255, 145, 0));
                    table.addCell(cell);
                    indexCap = 1;
                    pindex++;
                    indexArea++;
                }
                if (!id_capitulo.equals(rs.getString("id_capitulo"))) {
                    id_capitulo = rs.getString("id_capitulo");
                    capitulo = rs.getString("nombre_capitulo");
                    // row 1, cell 1
                    cell = new PdfPCell(new Phrase(String.valueOf(pindex + "." + indexCap), f2b));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setBackgroundColor(new Color(255, 145, 0));
                    table.addCell(cell);
                    // row 1, cell 2
                    cell = new PdfPCell(new Phrase(capitulo, f2b));
                    cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                    cell.setColspan(4);
                    cell.setBackgroundColor(new Color(255, 145, 0));
                    table.addCell(cell);
                    // row 1, cell 6 
                        cell = new PdfPCell(new Phrase(formato.format(Double.parseDouble(getValueFromArray(jsonArrCap, rs.getString("id_capitulo"), "total_capitulo"))), f2b));
                    cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    cell.setBackgroundColor(new Color(255, 145, 0));
                    table.addCell(cell);
                    pIndexApu = pindex + "." + indexCap;
                    indexApu = 1;
                    indexCap++;
                }
                String nombre_apu = rs.getString("nombre_apu").replace("PROYECTO_", "");
                String unidad = rs.getString("nombre_unidad");
                String cantidad_apu = rs.getString("cantidad_apu");
                double valor_unitario = rs.getDouble("valor_unitario");
                double valor_esquema = rs.getDouble("total");
                
                subtotalCot = subtotalCot + valor_esquema;

                // row 1, cell 1
                cell = new PdfPCell(new Phrase(pIndexApu + "." + indexApu, f2));
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(cell);
                // row 1, cell 2
                cell = new PdfPCell(new Phrase(nombre_apu, f2));
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                table.addCell(cell);
                // row 1, cell 3
                cell = new PdfPCell(new Phrase(unidad, f2));
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(cell);
                // row 1, cell 4
                cell = new PdfPCell(new Phrase(cantidad_apu, f2));
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(cell);
                // row 1, cell 5
                    cell = new PdfPCell(new Phrase(formato.format(valor_unitario), f2));
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                table.addCell(cell);
                // row 1, cell 6
                    cell = new PdfPCell(new Phrase(formato.format(valor_esquema), f2));
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                table.addCell(cell);
                indexApu++;
            }

            if ((detallado.equals("SI")) && !(jsonObjCot.get("modalidad_comercial").getAsString().equals("1"))) {
                query = "SQL_CARGAR_ADMINISTRACION_DETALLE";
                query = this.obtenerSQL(query);
                ps = con.prepareStatement(query);
                ps.setString(1, solicitud);
                ps.setString(2, solicitud);
                rs = ps.executeQuery();
                String numad = "";
                int subindex = 1, subindex2 = 1;
                pindex++;
                while (rs.next()) {
                    listaInfo = cargarDetAdministracion(solicitud);
                    BeansAdministracion info = (BeansAdministracion) listaInfo.get(0);
                    if (!numad.equals(info.getNumad())) {
                        numad = info.getNumad();
                        cell = new PdfPCell(new Phrase(String.valueOf(pindex), f3b));
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        cell.setBackgroundColor(new Color(255, 145, 0));
                        table.addCell(cell);
                        cell = new PdfPCell(new Phrase("ADMINISTRACION", f2b));
                        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                        cell.setColspan(4);
                        cell.setBackgroundColor(new Color(255, 145, 0));
                        table.addCell(cell);
                        cell = new PdfPCell(new Phrase(formato.format(info.getTotal()), f3b));
                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        cell.setBackgroundColor(new Color(255, 145, 0));
                        table.addCell(cell);

                        cell = new PdfPCell(new Phrase(String.valueOf(pindex + "." + subindex), f2b));
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        cell.setBackgroundColor(new Color(255, 145, 0));
                        table.addCell(cell);
                        cell = new PdfPCell(new Phrase("ADMINISTRACION", f2b));
                        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                        cell.setColspan(4);
                        cell.setBackgroundColor(new Color(255, 145, 0));
                        table.addCell(cell);
                        cell = new PdfPCell(new Phrase(formato.format(info.getTotal()), f3b));
                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        cell.setBackgroundColor(new Color(255, 145, 0));
                        table.addCell(cell);

                        cell = new PdfPCell(new Phrase(String.valueOf(pindex + "." + subindex + "." + subindex2), f2b));
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        cell.setBackgroundColor(new Color(255, 145, 0));
                        table.addCell(cell);
                        cell = new PdfPCell(new Phrase("ADMINISTRACION", f2));
                        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                        table.addCell(cell);
                        cell = new PdfPCell(new Phrase("UNIDAD", f2));
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        table.addCell(cell);
                        cell = new PdfPCell(new Phrase(String.valueOf(info.getCantidad()), f2));
                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        table.addCell(cell);
                        cell = new PdfPCell(new Phrase(formato.format(info.getTotal()), f2));
                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        table.addCell(cell);
                        cell = new PdfPCell(new Phrase(formato.format(info.getTotal()), f2));
                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        table.addCell(cell);
                        subtotalCot = subtotalCot + info.getTotal();
                        subindex++;
                    }
                }
            }

            // row 1, cell 1
            cell = new PdfPCell(new Phrase("Total Costo Directo", f3b));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setColspan(5);
            cell.setBackgroundColor(new Color(255, 145, 0));
            table.addCell(cell);
            // row 1, cell 6
            cell = new PdfPCell(new Phrase(formato.format(subtotalCot), f3b));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setBackgroundColor(new Color(255, 145, 0));
            table.addCell(cell);

            totalAdm = jsonObjCot.get("administracion").getAsDouble();
            // row 1, cell 1
            cell = new PdfPCell(new Phrase("Administracion", f3b));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setColspan(4);
            cell.setBackgroundColor(new Color(255, 145, 0));
            table.addCell(cell);
            // row 1, cell 5           
            cell = new PdfPCell(new Phrase(jsonObjCot.get("perc_administracioni").getAsString() + "%", f3b));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBackgroundColor(new Color(255, 145, 0));
            table.addCell(cell);
            // row 1, cell 6
            cell = new PdfPCell(new Phrase(formato.format(totalAdm), f3b));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setBackgroundColor(new Color(255, 145, 0));
            table.addCell(cell);

            double percImprevisto = jsonObjCot.get("perc_imprevisto").getAsDouble();
            totalImp = subtotalCot * percImprevisto / 100;
            // row 1, cell 1
            cell = new PdfPCell(new Phrase("Imprevisto", f3b));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setColspan(4);
            cell.setBackgroundColor(new Color(255, 145, 0));
            table.addCell(cell);
            // row 1, cell 5
            cell = new PdfPCell(new Phrase(jsonObjCot.get("perc_imprevistoi").getAsString() + "%", f3b));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBackgroundColor(new Color(255, 145, 0));
            table.addCell(cell);
            // row 1, cell 6
            cell = new PdfPCell(new Phrase(formato.format(totalImp), f3b));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setBackgroundColor(new Color(255, 145, 0));
            table.addCell(cell);

            totalUtil = subtotalCot * jsonObjCot.get("perc_utilidad").getAsDouble() / 100;
            // row 1, cell 1
            cell = new PdfPCell(new Phrase("Utilidad", f3b));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setColspan(4);
            cell.setBackgroundColor(new Color(255, 145, 0));
            table.addCell(cell);
            // row 1, cell 5
            cell = new PdfPCell(new Phrase(jsonObjCot.get("perc_utilidadi").getAsString() + "%", f3b));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBackgroundColor(new Color(255, 145, 0));
            table.addCell(cell);
            // row 1, cell 6
            cell = new PdfPCell(new Phrase(formato.format(totalUtil), f3b));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setBackgroundColor(new Color(255, 145, 0));
            table.addCell(cell);

            totalIva = subtotalCot * jsonObjCot.get("perc_iva").getAsDouble() / 100;
            // row 1, cell 1
            cell = new PdfPCell(new Phrase("IVA pleno o IVA sobre utilidad", f3b));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setColspan(4);
            cell.setBackgroundColor(new Color(255, 145, 0));
            table.addCell(cell);
            // row 1, cell 5
            String perc_iva = jsonObjCot.get("perc_iva").getAsString();
            if (perc_iva.startsWith("0")) {
                perc_iva = "19";
            }
            Phrase p = new Phrase(perc_iva + "%", f3b);
            cell = new PdfPCell(p);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBackgroundColor(new Color(255, 145, 0));
            table.addCell(cell);
            // row 1, cell 6
            if (totalIva == 0) {
                totalIva = totalUtil * 0.19;
            }
            cell = new PdfPCell(new Phrase(formato.format(totalIva), f3b));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setBackgroundColor(new Color(255, 145, 0));
            table.addCell(cell);

            totalCot = subtotalCot + totalAdm + totalImp + totalUtil + totalIva;
            // row 1, cell 1
            
            
            
            totalInversion=formato.format(totalCot);

            cell = new PdfPCell(new Phrase("COSTO TOTAL", f3b));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setColspan(5);
            cell.setBackgroundColor(new Color(255, 145, 0));
            table.addCell(cell);
            // row 1, cell 6
            cell = new PdfPCell(new Phrase(formato.format(totalCot), f3b));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setBackgroundColor(new Color(255, 145, 0));
            table.addCell(cell);
            
            
            
                     BaseFont baseFont = BaseFont.createFont(rsb.getString("rutaImagenes") +"/fuentes/Gelion-Bold.ttf", "Cp1252",  true);
                     BaseFont baseFontp = BaseFont.createFont(rsb.getString("rutaImagenes") +"/fuentes/Roboto-Medium.ttf", "Cp1252",  true);

                    Font fuentetitle=new Font(baseFont);
                    fuentetitle.setColor(93, 93, 93);
                    fuentetitle.setSize(28f);
                    
                    Font fuenteParrafo=new Font(baseFontp);
                    fuenteParrafo.setColor(93, 93, 93);
                    fuenteParrafo.setSize(12f);
                    
                    
                    
                     String titleOA=null;

                    
                    for (int i = 0; i < jsonArrNotas.size(); i++) {
                    JsonObject respuesta = jsonArrNotas.get(i).getAsJsonObject();
            
                  
                   if(respuesta.get("id_tipo_de_nota").getAsString()!=null && respuesta.get("id_tipo_de_nota").getAsInt()==2){
                       
                       
                    titleOA =  "OBJETO Y ALCANCE DE LA OFERTA";   
                       
                    BaseFont baseFontT = BaseFont.createFont(rsb.getString("rutaImagenes") +"/fuentes/Gelion-Bold.ttf", "Cp1252",  true);
                    Font fuenteT=new Font(baseFontT);
                    fuenteT.setColor(5, 38, 65);
                    fuenteT.setSize(26f);
                    Paragraph textoT=new Paragraph(new Phrase(titleOA, fuenteT));
                    textoT.setAlignment(Element.ALIGN_CENTER);
                    
                    
                    
                    htmlWorker=new HTMLWorker(document);

                        
                     document.add(textoT); 
                     document.add(new Paragraph(" "));
                        
                    Paragraph textoHtml= this.parseHtmlToPdfDescription(respuesta.get("nota_de_oferta").getAsString());
                    
                    document.add(textoHtml);
                    
                    
                    document.newPage();
                       
                       
                   }
                    
                    
                    }
                    
                    
                    
                    Paragraph texto=new Paragraph(new Phrase("VALOR DE LA INVERSION", fuentetitle));
                    texto.setAlignment(Element.ALIGN_CENTER);
               


                    Paragraph textop=new Paragraph(new Phrase("La oferta tiene un valor de COP"+totalInversion+", IVA incluido, para mayor detalle ver el siguiente cuadro de precio:", fuenteParrafo));
            
            
//            Chunk porcionTexto = new Chunk("La oferta tiene un valor de COP$"+totalInversion+", IVA incluido, para mayor detalle ver el siguiente cuadro de precio:");

            document.add(texto);
            document.add(new Paragraph(" "));
            document.add(textop);
            document.add(new Paragraph(" "));

            document.add(table);
            PdfPTable table2 = generar_Pdf_Cotizacion_Complemento(solicitud, con, listaInfo, detallado ,precios);
            document.newPage();
            document.add(table2);
            
            
            
            String title=null;
            
             JsonObject respuesta = new JsonObject();
                String valor = "0";
                
                
                int count=0;
                int countPages=0;
                int countFooter=1;
                JsonObject objetoNota=null;

                
                if(jsonArrNotas!=null)
                countPages=jsonArrNotas.size();
                


                for (int i = 0; i < jsonArrNotas.size(); i++) {
                  respuesta = jsonArrNotas.get(i).getAsJsonObject();
            
                  if(respuesta.get("id_tipo_de_nota").getAsString()!=null && respuesta.get("id_tipo_de_nota").getAsInt()==1)
                    title =  "NOTAS ACLARATORIAS";
                  else if(respuesta.get("id_tipo_de_nota").getAsString()!=null && respuesta.get("id_tipo_de_nota").getAsInt()==3)
                    title =  "CONDICIONES COMERCIALES";
                  else if(respuesta.get("id_tipo_de_nota").getAsString()!=null && respuesta.get("id_tipo_de_nota").getAsInt()==4)
                    title =  "DOCUMENTOS REQUERIDOS";

            // Let's create de first Chapter (Creemos el primer cap�tulo)
//            Chapter chapter = new Chapter(new Paragraph(chunk), 1);
//            chapter.setNumberDepth(0); 
//                    ---
                        
                    BaseFont baseFontT = BaseFont.createFont(rsb.getString("rutaImagenes") +"/fuentes/Gelion-Bold.ttf", "Cp1252",  true);
                    Font fuenteT=new Font(baseFontT);
                    fuenteT.setColor(5, 38, 65);
                    fuenteT.setSize(26f);
                    Paragraph textoT=new Paragraph(new Phrase(title, fuenteT));
                    textoT.setAlignment(Element.ALIGN_CENTER);


                   
//                    htmlWorker.parse(new StringReader(title));
//                    htmlWorker.parse(new StringReader("<br><br>"));
//                    
//                    
//
//                    htmlWorker.parse(new StringReader(respuesta.get("nota_de_oferta").getAsString()));
                   
                    if(respuesta.get("id_tipo_de_nota")!=null && respuesta.get("id_tipo_de_nota").getAsInt()!=5 && respuesta.get("id_tipo_de_nota").getAsInt()!=2){
                        
                        
                     count=count+1;
                     
                     if(count==1)
                         document.newPage();
                        
                     htmlWorker=new HTMLWorker(document);

                        
                     document.add(textoT); 
                     document.add(new Paragraph(" "));
                        
                    Paragraph textoHtml= this.parseHtmlToPdfDescription(respuesta.get("nota_de_oferta").getAsString());
                    
                    document.add(textoHtml);
                    
                    
//                     if(countPages!=0 && countPages>1 && i<countPages-count)



                     if(i<countPages-1)
                     document.newPage();

                    }else if(respuesta.get("id_tipo_de_nota")!=null && respuesta.get("id_tipo_de_nota").getAsInt()==5){
                        
                        
                        
                         objetoNota=new JsonObject();
                         objetoNota=respuesta;
                        
                        
                    }
                    
                  
                    
                  
                }
                
                
                
                
//                ResourceBundle rsb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");

         document.newPage();
         document.open();
         BaseFont baseFontFotter = BaseFont.createFont(rsb.getString("rutaImagenes") +"/fuentes/Roboto-Medium.ttf", "Cp1252",  true);
         Font fuente=new Font(baseFontFotter,14,Font.BOLD,Color.white);
         fuente.setColor(5, 38, 65);

         
//         document.newPage();
//         Paragraph texto=new Paragraph(new Phrase("ASESOR : ANA MARIA SOTO", fuente));
//         texto.setAlignment(Element.ALIGN_CENTER);

//         texto.setLeading(100);
//         document.add(texto);
                
//         document.add(new Paragraph(new Phrase("ASESOR : ANA MARIA SOTO", paragraphFont)));


//        PdfWriter writer = PdfWriter.getInstance(document, out);


        PdfContentByte canvas = pdfWriter.getDirectContentUnder();
        
        IMAGE= rsb.getString("rutaImagenes")+"background.png";
        
        Image image = Image.getInstance(IMAGE);
        image.scaleAbsolute(960f,530f);
        image.setAbsolutePosition(0, 0);
        canvas.addImage(image);
        
        


         if(objetoNota!=null){ 
             
        document.add( new Paragraph(" "));
        document.add( new Paragraph(" "));
        document.add( new Paragraph(" "));
        document.add( new Paragraph(" "));
        document.add( new Paragraph(" "));
        document.add( new Paragraph(" "));
        document.add( new Paragraph(" "));
        document.add( new Paragraph(" "));
        document.add( new Paragraph(" "));
        document.add( new Paragraph(" "));
        document.add( new Paragraph(" "));
        document.add( new Paragraph(" "));
        document.add( new Paragraph(" "));
        document.add( new Paragraph(" "));
        document.add( new Paragraph(" "));
        document.add( new Paragraph(" ")); 
        document.add( new Paragraph(" "));   
        document.add( new Paragraph(" "));  
       

         Paragraph objetoNotaHtml= ProcesosClienteImpl.parseHtmlToPdfDescriptionAsesor(objetoNota.get("nota_de_oferta").getAsString());
         objetoNotaHtml.setFont(paragraphFont);
//         Paragraph textoHtml= this.parseHtmlToPdfDescription(respuesta.get("nota_de_oferta").getAsString());

        document.add(objetoNotaHtml);
         }
                
          

        document.close();
 
 

            // step 5
          //  document.close();

            respuestaJson = "{\"respuesta\":\"OK\",\"Ruta\":\"" + "/images/multiservicios/" + usuario.getLogin() + "/" + nomarchivo + "\"}";

        } catch (Exception e) {
            try {
                respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR generar_Pdf_Cotizacion \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(MinutasContratacionImpl.class.getName()).log(Level.SEVERE, null, ex.getNextException());
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }
    
    

    @Override
    public String generar_Pdf_Cotizacion(String solicitud, String ruta, JsonObject jsonObjCot, JsonArray jsonArrAreas, JsonArray jsonArrCap,JsonArray jsonArrNotas, Usuario usuario, String detallado, boolean precios) {

        con = null;
        ps = null;
        rs = null;
        String sql = "";
        query = "obtenerInfoCotizacion";
        String nomarchivo = "";
        String html_doc = "", fecha_corte = "";
        ResourceBundle rb = null;
        String respuestaJson = "{}";
        
        try {
            DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
            //get current date time with Date()
            Date date = new Date();
            nomarchivo =  ("cotizacion_" + dateFormat.format(date)).replace(":", "_") + ".pdf";
            //System.out.println(dateFormat.format(date));
            rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            
             Rectangle pageSize = new Rectangle(960f, 530f); //ancho y alto
            
            Document document = new Document();

            document.setPageSize(pageSize);

            HTMLWorker htmlWorker = null;
            
//            HTMLWorker titleWorker=new HTMLWorker(document);
//           titleWorker.parse(new StringReader("<h1 style='color:orange;font-family:Bebas Neue;font-weight:bold'>VALOR DE LA INVERSI�N</h1>"));


            File newFile = new File(ruta, nomarchivo);
            newFile.createNewFile();
            PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(newFile));
            
            
            
           ResourceBundle rsb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");

            
            
//             HTMLWorker titleWorker=new HTMLWorker(document);
//           titleWorker.parse(new StringReader("<h1 style='color:orange;font-family:Bebas Neue;font-weight:bold'>VALOR DE LA INVERSI�N</h1>"));

              BaseFont baseFontTable = BaseFont.createFont(rsb.getString("rutaImagenes") +"/fuentes/Roboto-Regular.ttf", "Cp1252",  true);
              Font fuenteTable=new Font(baseFontTable);
              fuenteTable.setSize(8f);



            Font f = new Font(Font.TIMES_ROMAN, 12, Font.NORMAL);
            Font f1 = new Font(Font.TIMES_ROMAN, 12, Font.BOLD);

            Font f2 = fuenteTable;
            Font f3 = new Font(Font.TIMES_ROMAN, 8, Font.BOLD);

            Font f4 = new Font(Font.TIMES_ROMAN, 10, Font.NORMAL);

            Font fb = new Font(Font.TIMES_ROMAN, 12, Font.NORMAL, new Color(255, 255, 255));
            Font f1b = new Font(Font.TIMES_ROMAN, 12, Font.BOLD, new Color(255, 255, 255));
            Font f2b = fuenteTable;
            Font f3b = fuenteTable;
            Font f4b = new Font(Font.TIMES_ROMAN, 10, Font.NORMAL, new Color(255, 255, 255));

            HeaderFooterCot header = new HeaderFooterCot(rb);
            pdfWriter.setPageEvent(header);
            

            document.open();
            document.addAuthor(usuario.getLogin());
            document.addCreator("SELECTRIK S.A.");
            document.addCreationDate();
            document.addTitle("Cotizacion Selectrik");

            con = this.conectarJNDI();
            /* query = this.obtenerSQL(query);
             ps = con.prepareStatement(query);         
        
             rs = ps.executeQuery();

             while (rs.next()) {              
             html_doc = rs.getString("info");      
             fecha_corte = rs.getString("fecha_corte");      
             }*/

            //htmlWorker.parse(new StringReader("" + html_doc.replaceAll("P1", fecha_corte).replaceAll("P2","  "+ diasMora+" d?as") + ""));     
            // query = "obtenerDetalleCotizacion";
            PdfPTable table = new PdfPTable(6);
            table.setWidths(new int[]{2, 9, 3, 2, 4, 4});
            table.setWidthPercentage(100);
            PdfPCell cell;
            //ENCABEZADOS DE LA TABLA
            // row 1, cell 1
            cell = new PdfPCell(new Phrase("CODIGO", f3b));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBackgroundColor(new Color(255, 145, 0));
            table.addCell(cell);

            // row 1, cell 2
            cell = new PdfPCell(new Phrase("DESCRIPCION", f3b));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBackgroundColor(new Color(255, 145, 0));
            table.addCell(cell);

            // row 1, cell 3
            cell = new PdfPCell(new Phrase("UND", f3b));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBackgroundColor(new Color(255, 145, 0));
            table.addCell(cell);

            // row 1, cell 4
            cell = new PdfPCell(new Phrase("CANT", f3b));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBackgroundColor(new Color(255, 145, 0));
            table.addCell(cell);

            // row 1, cell 5
            cell = new PdfPCell(new Phrase("VR-UNIT", f3b));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBackgroundColor(new Color(255, 145, 0));
            table.addCell(cell);

            // row 1, cell 5
            cell = new PdfPCell(new Phrase("SUB-TOTAL", f3b));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBackgroundColor(new Color(255, 145, 0));
            table.addCell(cell);
            DecimalFormat formato = new DecimalFormat("$###,###,###");
            switch (detallado) {
                case "SI":
                    query = "SQL_ODTENER_DETALLE_COTIZACION_SIN_ADMINISTRACION";
                    break;
                case "NO":
                    query = "obtenerDetalleCotizacion";
                    break;
            }
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, solicitud);
            rs = ps.executeQuery();
            
            String totalInversion="";
            int indexArea = 1, indexCap = 1, indexApu = 1, pindex = 0;
            String id_area = "", area = "", id_capitulo = "", capitulo = "", pIndexApu = "";
            double subtotalCot = 0, totalIva = 0, totalAdm = 0, totalImp = 0, totalUtil = 0, totalCot = 0;
            while (rs.next()) {
                if (!id_area.equals(rs.getString("id_area"))) {
                    id_area = rs.getString("id_area");
                    area = rs.getString("nombre_area");

                    // row 1, cell 1
                    cell = new PdfPCell(new Phrase(String.valueOf(indexArea), f3b));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setBackgroundColor(new Color(255, 145, 0));
                    table.addCell(cell);
                    // row 1, cell 2
                    cell = new PdfPCell(new Phrase(area, f3b));
                    cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                    cell.setColspan(4);
                    cell.setBackgroundColor(new Color(255, 145, 0));
                    table.addCell(cell);
                    // row 1, cell 6
                    cell = new PdfPCell(new Phrase(formato.format(Double.parseDouble(getValueFromArray(jsonArrAreas, rs.getString("id_area"), "total_area"))), f3b));
                    cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    cell.setBackgroundColor(new Color(255, 145, 0));
                    table.addCell(cell);
                    indexCap = 1;
                    pindex++;
                    indexArea++;
                }
                if (!id_capitulo.equals(rs.getString("id_capitulo"))) {
                    id_capitulo = rs.getString("id_capitulo");
                    capitulo = rs.getString("nombre_capitulo");
                    // row 1, cell 1
                    cell = new PdfPCell(new Phrase(String.valueOf(pindex + "." + indexCap), f2b));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setBackgroundColor(new Color(255, 145, 0));
                    table.addCell(cell);
                    // row 1, cell 2
                    cell = new PdfPCell(new Phrase(capitulo, f2b));
                    cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                    cell.setColspan(4);
                    cell.setBackgroundColor(new Color(255, 145, 0));
                    table.addCell(cell);
                    // row 1, cell 6           
                    cell = new PdfPCell(new Phrase(formato.format(Double.parseDouble(getValueFromArray(jsonArrCap, rs.getString("id_capitulo"), "total_capitulo"))), f2b));
                    cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    cell.setBackgroundColor(new Color(255, 145, 0));
                    table.addCell(cell);
                    pIndexApu = pindex + "." + indexCap;
                    indexApu = 1;
                    indexCap++;
                }
                String nombre_apu = rs.getString("nombre_apu").replace("PROYECTO_", "");
                String unidad = rs.getString("nombre_unidad");
                String cantidad_apu = rs.getString("cantidad_apu");
                double valor_unitario = rs.getDouble("valor_unitario");
                double valor_esquema = rs.getDouble("total");
                
                subtotalCot = subtotalCot + valor_esquema;

                // row 1, cell 1
                cell = new PdfPCell(new Phrase(pIndexApu + "." + indexApu, f2));
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(cell);
                // row 1, cell 2
                cell = new PdfPCell(new Phrase(nombre_apu, f2));
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                table.addCell(cell);
                // row 1, cell 3
                cell = new PdfPCell(new Phrase(unidad, f2));
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(cell);
                // row 1, cell 4
                cell = new PdfPCell(new Phrase(cantidad_apu, f2));
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(cell);
                // row 1, cell 5
                    cell = new PdfPCell(new Phrase(formato.format(valor_unitario), f2));
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                table.addCell(cell);
                // row 1, cell 6
                cell = new PdfPCell(new Phrase(formato.format(valor_esquema), f2));
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                table.addCell(cell);
                indexApu++;
            }

            if ((detallado.equals("SI")) && !(jsonObjCot.get("modalidad_comercial").getAsString().equals("1"))) {
                query = "SQL_CARGAR_ADMINISTRACION_DETALLE";
                query = this.obtenerSQL(query);
                ps = con.prepareStatement(query);
                ps.setString(1, solicitud);
                ps.setString(2, solicitud);
                rs = ps.executeQuery();
                String numad = "";
                int subindex = 1, subindex2 = 1;
                pindex++;
                while (rs.next()) {
                    listaInfo = cargarDetAdministracion(solicitud);
                    BeansAdministracion info = (BeansAdministracion) listaInfo.get(0);
                    if (!numad.equals(info.getNumad())) {
                        numad = info.getNumad();
                        cell = new PdfPCell(new Phrase(String.valueOf(pindex), f3b));
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        cell.setBackgroundColor(new Color(255, 145, 0));
                        table.addCell(cell);
                        cell = new PdfPCell(new Phrase("ADMINISTRACION", f2b));
                        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                        cell.setColspan(4);
                        cell.setBackgroundColor(new Color(255, 145, 0));
                        table.addCell(cell);
                        cell = new PdfPCell(new Phrase(formato.format(info.getTotal()), f3b));
                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        cell.setBackgroundColor(new Color(255, 145, 0));
                        table.addCell(cell);

                        cell = new PdfPCell(new Phrase(String.valueOf(pindex + "." + subindex), f2b));
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        cell.setBackgroundColor(new Color(255, 145, 0));
                        table.addCell(cell);
                        cell = new PdfPCell(new Phrase("ADMINISTRACION", f2b));
                        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                        cell.setColspan(4);
                        cell.setBackgroundColor(new Color(255, 145, 0));
                        table.addCell(cell);
                        cell = new PdfPCell(new Phrase(formato.format(info.getTotal()), f3b));
                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        cell.setBackgroundColor(new Color(255, 145, 0));
                        table.addCell(cell);

                        cell = new PdfPCell(new Phrase(String.valueOf(pindex + "." + subindex + "." + subindex2), f2b));
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        cell.setBackgroundColor(new Color(255, 145, 0));
                        table.addCell(cell);
                        cell = new PdfPCell(new Phrase("ADMINISTRACION", f2));
                        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                        table.addCell(cell);
                        cell = new PdfPCell(new Phrase("UNIDAD", f2));
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        table.addCell(cell);
                        cell = new PdfPCell(new Phrase(String.valueOf(info.getCantidad()), f2));
                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        table.addCell(cell);
                        cell = new PdfPCell(new Phrase(formato.format(info.getTotal()), f2));
                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        table.addCell(cell);
                        cell = new PdfPCell(new Phrase(formato.format(info.getTotal()), f2));
                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        table.addCell(cell);
                        subtotalCot = subtotalCot + info.getTotal();
                        subindex++;
                    }
                }
            }

            // row 1, cell 1
            cell = new PdfPCell(new Phrase("Total Costo Directo", f3b));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setColspan(5);
            cell.setBackgroundColor(new Color(255, 145, 0));
            table.addCell(cell);
            // row 1, cell 6
            cell = new PdfPCell(new Phrase(formato.format(subtotalCot), f3b));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setBackgroundColor(new Color(255, 145, 0));
            table.addCell(cell);

            totalAdm = jsonObjCot.get("administracion").getAsDouble();
            // row 1, cell 1
            cell = new PdfPCell(new Phrase("Administracion", f3b));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setColspan(4);
            cell.setBackgroundColor(new Color(255, 145, 0));
            table.addCell(cell);
            // row 1, cell 5           
            cell = new PdfPCell(new Phrase(jsonObjCot.get("perc_administracioni").getAsString() + "%", f3b));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBackgroundColor(new Color(255, 145, 0));
            table.addCell(cell);
            // row 1, cell 6
            cell = new PdfPCell(new Phrase(formato.format(totalAdm), f3b));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setBackgroundColor(new Color(255, 145, 0));
            table.addCell(cell);

            double percImprevisto = jsonObjCot.get("perc_imprevisto").getAsDouble();
            totalImp = subtotalCot * percImprevisto / 100;
            // row 1, cell 1
            cell = new PdfPCell(new Phrase("Imprevisto", f3b));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setColspan(4);
            cell.setBackgroundColor(new Color(255, 145, 0));
            table.addCell(cell);
            // row 1, cell 5
            cell = new PdfPCell(new Phrase(jsonObjCot.get("perc_imprevistoi").getAsString() + "%", f3b));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBackgroundColor(new Color(255, 145, 0));
            table.addCell(cell);
            // row 1, cell 6
            cell = new PdfPCell(new Phrase(formato.format(totalImp), f3b));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setBackgroundColor(new Color(255, 145, 0));
            table.addCell(cell);

            totalUtil = subtotalCot * jsonObjCot.get("perc_utilidad").getAsDouble() / 100;
            // row 1, cell 1
            cell = new PdfPCell(new Phrase("Utilidad", f3b));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setColspan(4);
            cell.setBackgroundColor(new Color(255, 145, 0));
            table.addCell(cell);
            // row 1, cell 5
            cell = new PdfPCell(new Phrase(jsonObjCot.get("perc_utilidadi").getAsString() + "%", f3b));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBackgroundColor(new Color(255, 145, 0));
            table.addCell(cell);
            // row 1, cell 6
            cell = new PdfPCell(new Phrase(formato.format(totalUtil), f3b));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setBackgroundColor(new Color(255, 145, 0));
            table.addCell(cell);

            totalIva = subtotalCot * jsonObjCot.get("perc_iva").getAsDouble() / 100;
            // row 1, cell 1
            cell = new PdfPCell(new Phrase("IVA pleno o IVA sobre utilidad", f3b));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setColspan(4);
            cell.setBackgroundColor(new Color(255, 145, 0));
            table.addCell(cell);
            // row 1, cell 5
            String perc_iva = jsonObjCot.get("perc_iva").getAsString();
            if (perc_iva.startsWith("0")) {
                perc_iva = "19";
            }
            Phrase p = new Phrase(perc_iva + "%", f3b);
            cell = new PdfPCell(p);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBackgroundColor(new Color(255, 145, 0));
            table.addCell(cell);
            // row 1, cell 6
            if (totalIva == 0) {
                totalIva = totalUtil * 0.19;
            }
            cell = new PdfPCell(new Phrase(formato.format(totalIva), f3b));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setBackgroundColor(new Color(255, 145, 0));
            table.addCell(cell);

            totalCot = subtotalCot + totalAdm + totalImp + totalUtil + totalIva;
            // row 1, cell 1
            
            
            
            totalInversion=formato.format(totalCot);

            cell = new PdfPCell(new Phrase("COSTO TOTAL", f3b));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setColspan(5);
            cell.setBackgroundColor(new Color(255, 145, 0));
            table.addCell(cell);
            // row 1, cell 6
            cell = new PdfPCell(new Phrase(formato.format(totalCot), f3b));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setBackgroundColor(new Color(255, 145, 0));
            table.addCell(cell);
            
            
            
                     BaseFont baseFont = BaseFont.createFont(rsb.getString("rutaImagenes") +"/fuentes/Gelion-Bold.ttf", "Cp1252",  true);
                     BaseFont baseFontp = BaseFont.createFont(rsb.getString("rutaImagenes") +"/fuentes/Roboto-Medium.ttf", "Cp1252",  true);

                    Font fuentetitle=new Font(baseFont);
                    fuentetitle.setColor(5, 38, 65);
                    fuentetitle.setSize(28f);
                    
                    Font fuenteParrafo=new Font(baseFontp);
                    fuenteParrafo.setColor(93, 93, 93);
                    fuenteParrafo.setSize(12f);
                    
                    
                    
                    
                     String titleOA=null;

                    
                    for (int i = 0; i < jsonArrNotas.size(); i++) {
                    JsonObject respuesta = jsonArrNotas.get(i).getAsJsonObject();
            
                  
                   if(respuesta.get("id_tipo_de_nota").getAsString()!=null && respuesta.get("id_tipo_de_nota").getAsInt()==2){
                       
                       
                    titleOA =  "OBJETO Y ALCANCE DE LA OFERTA";   
                       
                    BaseFont baseFontT = BaseFont.createFont(rsb.getString("rutaImagenes") +"/fuentes/Gelion-Bold.ttf", "Cp1252",  true);
                    Font fuenteT=new Font(baseFontT);
                    fuenteT.setColor(5, 38, 65);
                    fuenteT.setSize(26f);
                    Paragraph textoT=new Paragraph(new Phrase(titleOA, fuenteT));
                    textoT.setAlignment(Element.ALIGN_CENTER);

                    
                    
                    
                    htmlWorker=new HTMLWorker(document);

                        
                     document.add(textoT); 
                     document.add(new Paragraph(" "));
                        
                    Paragraph textoHtml= this.parseHtmlToPdfDescription(respuesta.get("nota_de_oferta").getAsString());
                    
                    document.add(textoHtml);
                    
                    
                    document.newPage();
                       
                       
                   }
                    
                    
                    }
                    
                    
                    
                    
                    
                    
                    Paragraph texto=new Paragraph(new Phrase("VALOR DE LA INVERSION", fuentetitle));
                    texto.setAlignment(Element.ALIGN_CENTER);


                    Paragraph textop=new Paragraph(new Phrase("La oferta tiene un valor de COP"+totalInversion+", IVA incluido, para mayor detalle ver el siguiente cuadro de precio:", fuenteParrafo));
            
            
//            Chunk porcionTexto = new Chunk("La oferta tiene un valor de COP$"+totalInversion+", IVA incluido, para mayor detalle ver el siguiente cuadro de precio:");

            document.add(texto);
            document.add(new Paragraph(" "));
            document.add(textop);
            document.add(new Paragraph(" "));

            document.add(table);
//            PdfPTable table2 = generar_Pdf_Cotizacion_Complemento(solicitud, con, listaInfo, detallado);
//            document.newPage();
//            document.add(table2);
            
            
            
            String title=null;
            
             JsonObject respuesta = new JsonObject();
                String valor = "0";
                
                
                int count=0;
                int countPages=0;
                int countFooter=1;
                JsonObject objetoNota=null;

                
                if(jsonArrNotas!=null)
                countPages=jsonArrNotas.size();
                


                for (int i = 0; i < jsonArrNotas.size(); i++) {
                  respuesta = jsonArrNotas.get(i).getAsJsonObject();
            
                  if(respuesta.get("id_tipo_de_nota").getAsString()!=null && respuesta.get("id_tipo_de_nota").getAsInt()==1)
                    title =  "NOTAS ACLARATORIAS";
                  else if(respuesta.get("id_tipo_de_nota").getAsString()!=null && respuesta.get("id_tipo_de_nota").getAsInt()==3)
                    title =  "CONDICIONES COMERCIALES";
                  else if(respuesta.get("id_tipo_de_nota").getAsString()!=null && respuesta.get("id_tipo_de_nota").getAsInt()==4)
                    title =  "DOCUMENTOS REQUERIDOS";

            // Let's create de first Chapter (Creemos el primer cap�tulo)
//            Chapter chapter = new Chapter(new Paragraph(chunk), 1);
//            chapter.setNumberDepth(0); 
//                    
                        
                    BaseFont baseFontT = BaseFont.createFont(rsb.getString("rutaImagenes") +"/fuentes/Gelion-Bold.ttf", "Cp1252",  true);
                    Font fuenteT=new Font(baseFontT);
                    fuenteT.setColor(5, 38, 65);
                    fuenteT.setSize(26f);
                    Paragraph textoT=new Paragraph(new Phrase(title, fuenteT));
                    textoT.setAlignment(Element.ALIGN_CENTER);



                   
//                    htmlWorker.parse(new StringReader(title));
//                    htmlWorker.parse(new StringReader("<br><br>"));
//                    
//                    
//
//                    htmlWorker.parse(new StringReader(respuesta.get("nota_de_oferta").getAsString()));
                   
                    if(respuesta.get("id_tipo_de_nota")!=null && respuesta.get("id_tipo_de_nota").getAsInt()!=5 && respuesta.get("id_tipo_de_nota").getAsInt()!=2){
                        
                        
                     count=count+1;
                     
                     if(count==1)
                         document.newPage();
                        
                     htmlWorker=new HTMLWorker(document);

                        
                     document.add(textoT); 
                     document.add(new Paragraph(" "));
                        
                    Paragraph textoHtml= this.parseHtmlToPdfDescription(respuesta.get("nota_de_oferta").getAsString());
                    
                    document.add(textoHtml);
                    
                    
//                     if(countPages!=0 && countPages>1 && i<countPages-count)



                     if(i<countPages-1)
                     document.newPage();

                    }else if(respuesta.get("id_tipo_de_nota")!=null && respuesta.get("id_tipo_de_nota").getAsInt()==5){
                        
                        
                        
                         objetoNota=new JsonObject();
                         objetoNota=respuesta;
                        
                        
                    }
                    
                  
                    
                  
                }
                
                
                
                
//                ResourceBundle rsb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");

         document.newPage();
         document.open();
         BaseFont baseFontFotter = BaseFont.createFont(rsb.getString("rutaImagenes") +"/fuentes/Roboto-Medium.ttf", "Cp1252",  true);
         Font fuente=new Font(baseFontFotter,14,Font.BOLD,Color.white);
         fuente.setColor(5, 38, 65);

         
//         document.newPage();
//         Paragraph texto=new Paragraph(new Phrase("ASESOR : ANA MARIA SOTO", fuente));
//         texto.setAlignment(Element.ALIGN_CENTER);

//         texto.setLeading(100);
//         document.add(texto);
                
//         document.add(new Paragraph(new Phrase("ASESOR : ANA MARIA SOTO", paragraphFont)));


//        PdfWriter writer = PdfWriter.getInstance(document, out);


        PdfContentByte canvas = pdfWriter.getDirectContentUnder();
        
        IMAGE= rsb.getString("rutaImagenes")+"background.png";
        
        Image image = Image.getInstance(IMAGE);
        image.scaleAbsolute(960f,530f);
        image.setAbsolutePosition(0, 0);
        canvas.addImage(image);
        
        


         if(objetoNota!=null){ 
             
        document.add( new Paragraph(" "));
        document.add( new Paragraph(" "));
        document.add( new Paragraph(" "));
        document.add( new Paragraph(" "));
        document.add( new Paragraph(" "));
        document.add( new Paragraph(" "));
        document.add( new Paragraph(" "));
        document.add( new Paragraph(" "));
        document.add( new Paragraph(" "));
        document.add( new Paragraph(" "));
        document.add( new Paragraph(" "));
        document.add( new Paragraph(" "));
        document.add( new Paragraph(" "));
        document.add( new Paragraph(" "));
        document.add( new Paragraph(" "));
        document.add( new Paragraph(" ")); 
        document.add( new Paragraph(" "));   
        document.add( new Paragraph(" "));  
       

         Paragraph objetoNotaHtml= ProcesosClienteImpl.parseHtmlToPdfDescriptionAsesor(objetoNota.get("nota_de_oferta").getAsString());
         objetoNotaHtml.setFont(paragraphFont);
//         Paragraph textoHtml= this.parseHtmlToPdfDescription(respuesta.get("nota_de_oferta").getAsString());

        document.add(objetoNotaHtml);
         }
                
          

        document.close();
 

        
            // step 5
          //  document.close();

            respuestaJson = "{\"respuesta\":\"OK\",\"Ruta\":\"" + "/images/multiservicios/" + usuario.getLogin() + "/" + nomarchivo + "\"}";

        } catch (Exception e) {
            try {
                respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR generar_Pdf_Cotizacion \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(MinutasContratacionImpl.class.getName()).log(Level.SEVERE, null, ex.getNextException());
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }
    
    
    
    
    
    
    
    
    
    
    public String generar_Pdf_Cotizacion2(String solicitud, String ruta, JsonObject jsonObjCot, JsonArray jsonArrAreas, JsonArray jsonArrCap,JsonArray jsonArrNotas, Usuario usuario, String detallado) {

        con = null;
        ps = null;
        rs = null;
        String sql = "";
        query = "obtenerInfoCotizacion";
        String nomarchivo = "";
        String html_doc = "", fecha_corte = "";
        ResourceBundle rb = null;
        String respuestaJson = "{}";
        try {
            DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
            //get current date time with Date()
            Date date = new Date();
            nomarchivo =  ("cotizacion_" + dateFormat.format(date)).replace(":", "_") + ".pdf";
            //System.out.println(dateFormat.format(date));
            rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            
             Rectangle pageSize = new Rectangle(960f, 530f); //ancho y alto
            
            Document document = new Document();

            document.setPageSize(pageSize);

            HTMLWorker htmlWorker = null;
            
//            HTMLWorker titleWorker=new HTMLWorker(document);
//           titleWorker.parse(new StringReader("<h1 style='color:orange;font-family:Bebas Neue;font-weight:bold'>VALOR DE LA INVERSI�N</h1>"));


            File newFile = new File(ruta, nomarchivo);
            newFile.createNewFile();
            PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(newFile));
            
            
            
           ResourceBundle rsb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");

            
            
//             HTMLWorker titleWorker=new HTMLWorker(document);
//           titleWorker.parse(new StringReader("<h1 style='color:orange;font-family:Bebas Neue;font-weight:bold'>VALOR DE LA INVERSI�N</h1>"));

              BaseFont baseFontTable = BaseFont.createFont(rsb.getString("rutaImagenes") +"/fuentes/Roboto-Regular.ttf", "Cp1252",  true);
              Font fuenteTable=new Font(baseFontTable);
              fuenteTable.setSize(8f);



            Font f = new Font(Font.TIMES_ROMAN, 12, Font.NORMAL);
            Font f1 = new Font(Font.TIMES_ROMAN, 12, Font.BOLD);

            Font f2 = fuenteTable;
            Font f3 = new Font(Font.TIMES_ROMAN, 8, Font.BOLD);

            Font f4 = new Font(Font.TIMES_ROMAN, 10, Font.NORMAL);

            Font fb = new Font(Font.TIMES_ROMAN, 12, Font.NORMAL, new Color(255, 255, 255));
            Font f1b = new Font(Font.TIMES_ROMAN, 12, Font.BOLD, new Color(255, 255, 255));
            Font f2b = fuenteTable;
            Font f3b = fuenteTable;
            Font f4b = new Font(Font.TIMES_ROMAN, 10, Font.NORMAL, new Color(255, 255, 255));

            HeaderFooterCot header = new HeaderFooterCot(rb);
            pdfWriter.setPageEvent(header);
            

            document.open();
            document.addAuthor(usuario.getLogin());
            document.addCreator("SELECTRIK S.A.");
            document.addCreationDate();
            document.addTitle("Cotizacion Selectrik");

            con = this.conectarJNDI();
            /* query = this.obtenerSQL(query);
             ps = con.prepareStatement(query);         
        
             rs = ps.executeQuery();

             while (rs.next()) {              
             html_doc = rs.getString("info");      
             fecha_corte = rs.getString("fecha_corte");      
             }*/

            //htmlWorker.parse(new StringReader("" + html_doc.replaceAll("P1", fecha_corte).replaceAll("P2","  "+ diasMora+" d?as") + ""));     
            // query = "obtenerDetalleCotizacion";
            PdfPTable table = new PdfPTable(6);
            table.setWidths(new int[]{2, 9, 3, 2, 4, 4});
            table.setWidthPercentage(100);
            PdfPCell cell;
            //ENCABEZADOS DE LA TABLA
            // row 1, cell 1
            cell = new PdfPCell(new Phrase("CODIGO", f3b));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBackgroundColor(new Color(255, 145, 0));
            table.addCell(cell);

            // row 1, cell 2
            cell = new PdfPCell(new Phrase("DESCRIPCION", f3b));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBackgroundColor(new Color(255, 145, 0));
            table.addCell(cell);

            // row 1, cell 3
            cell = new PdfPCell(new Phrase("UND", f3b));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBackgroundColor(new Color(255, 145, 0));
            table.addCell(cell);

            // row 1, cell 4
            cell = new PdfPCell(new Phrase("CANT", f3b));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBackgroundColor(new Color(255, 145, 0));
            table.addCell(cell);

            // row 1, cell 5
            cell = new PdfPCell(new Phrase("VR-UNIT", f3b));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBackgroundColor(new Color(255, 145, 0));
            table.addCell(cell);

            // row 1, cell 5
            cell = new PdfPCell(new Phrase("SUB-TOTAL", f3b));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBackgroundColor(new Color(255, 145, 0));
            table.addCell(cell);
            DecimalFormat formato = new DecimalFormat("$###,###,###");
            switch (detallado) {
                case "SI":
                    query = "SQL_ODTENER_DETALLE_COTIZACION_SIN_ADMINISTRACION";
                    break;
                case "NO":
                    query = "obtenerDetalleCotizacion";
                    break;
            }
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, solicitud);
            rs = ps.executeQuery();
            
            String totalInversion="";
            int indexArea = 1, indexCap = 1, indexApu = 1, pindex = 0;
            String id_area = "", area = "", id_capitulo = "", capitulo = "", pIndexApu = "";
            double subtotalCot = 0, totalIva = 0, totalAdm = 0, totalImp = 0, totalUtil = 0, totalCot = 0;
            while (rs.next()) {
                if (!id_area.equals(rs.getString("id_area"))) {
                    id_area = rs.getString("id_area");
                    area = rs.getString("nombre_area");

                    // row 1, cell 1
                    cell = new PdfPCell(new Phrase(String.valueOf(indexArea), f3b));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setBackgroundColor(new Color(255, 145, 0));
                    table.addCell(cell);
                    // row 1, cell 2
                    cell = new PdfPCell(new Phrase(area, f3b));
                    cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                    cell.setColspan(4);
                    cell.setBackgroundColor(new Color(255, 145, 0));
                    table.addCell(cell);
                    // row 1, cell 6
                    cell = new PdfPCell(new Phrase(formato.format(Double.parseDouble(getValueFromArray(jsonArrAreas, rs.getString("id_area"), "total_area"))), f3b));
                    cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    cell.setBackgroundColor(new Color(255, 145, 0));
                    table.addCell(cell);
                    indexCap = 1;
                    pindex++;
                    indexArea++;
                }
                if (!id_capitulo.equals(rs.getString("id_capitulo"))) {
                    id_capitulo = rs.getString("id_capitulo");
                    capitulo = rs.getString("nombre_capitulo");
                    // row 1, cell 1
                    cell = new PdfPCell(new Phrase(String.valueOf(pindex + "." + indexCap), f2b));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setBackgroundColor(new Color(255, 145, 0));
                    table.addCell(cell);
                    // row 1, cell 2
                    cell = new PdfPCell(new Phrase(capitulo, f2b));
                    cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                    cell.setColspan(4);
                    cell.setBackgroundColor(new Color(255, 145, 0));
                    table.addCell(cell);
                    // row 1, cell 6                   
                    cell = new PdfPCell(new Phrase(formato.format(Double.parseDouble(getValueFromArray(jsonArrCap, rs.getString("id_capitulo"), "total_capitulo"))), f2b));
                    cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    cell.setBackgroundColor(new Color(255, 145, 0));
                    table.addCell(cell);
                    pIndexApu = pindex + "." + indexCap;
                    indexApu = 1;
                    indexCap++;
                }
                String nombre_apu = rs.getString("nombre_apu").replace("PROYECTO_", "");
                String unidad = rs.getString("nombre_unidad");
                String cantidad_apu = rs.getString("cantidad_apu");
                double valor_unitario = rs.getDouble("valor_unitario");
                double valor_esquema = rs.getDouble("total");
                
                subtotalCot = subtotalCot + valor_esquema;

                // row 1, cell 1
                cell = new PdfPCell(new Phrase(pIndexApu + "." + indexApu, f2));
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(cell);
                // row 1, cell 2
                cell = new PdfPCell(new Phrase(nombre_apu, f2));
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                table.addCell(cell);
                // row 1, cell 3
                cell = new PdfPCell(new Phrase(unidad, f2));
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(cell);
                // row 1, cell 4
                cell = new PdfPCell(new Phrase(cantidad_apu, f2));
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(cell);
                // row 1, cell 5
                cell = new PdfPCell(new Phrase(formato.format(valor_unitario), f2));
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                table.addCell(cell);
                // row 1, cell 6
                cell = new PdfPCell(new Phrase(formato.format(valor_esquema), f2));
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                table.addCell(cell);
                indexApu++;
            }

            if ((detallado.equals("SI")) && !(jsonObjCot.get("modalidad_comercial").getAsString().equals("1"))) {
                query = "SQL_CARGAR_ADMINISTRACION_DETALLE";
                query = this.obtenerSQL(query);
                ps = con.prepareStatement(query);
                ps.setString(1, solicitud);
                ps.setString(2, solicitud);
                rs = ps.executeQuery();
                String numad = "";
                int subindex = 1, subindex2 = 1;
                pindex++;
                while (rs.next()) {
                    listaInfo = cargarDetAdministracion(solicitud);
                    BeansAdministracion info = (BeansAdministracion) listaInfo.get(0);
                    if (!numad.equals(info.getNumad())) {
                        numad = info.getNumad();
                        cell = new PdfPCell(new Phrase(String.valueOf(pindex), f3b));
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        cell.setBackgroundColor(new Color(255, 145, 0));
                        table.addCell(cell);
                        cell = new PdfPCell(new Phrase("ADMINISTRACION", f2b));
                        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                        cell.setColspan(4);
                        cell.setBackgroundColor(new Color(255, 145, 0));
                        table.addCell(cell);
                        cell = new PdfPCell(new Phrase(formato.format(info.getTotal()), f3b));
                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        cell.setBackgroundColor(new Color(255, 145, 0));
                        table.addCell(cell);

                        cell = new PdfPCell(new Phrase(String.valueOf(pindex + "." + subindex), f2b));
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        cell.setBackgroundColor(new Color(255, 145, 0));
                        table.addCell(cell);
                        cell = new PdfPCell(new Phrase("ADMINISTRACION", f2b));
                        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                        cell.setColspan(4);
                        cell.setBackgroundColor(new Color(255, 145, 0));
                        table.addCell(cell);
                        cell = new PdfPCell(new Phrase(formato.format(info.getTotal()), f3b));
                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        cell.setBackgroundColor(new Color(255, 145, 0));
                        table.addCell(cell);

                        cell = new PdfPCell(new Phrase(String.valueOf(pindex + "." + subindex + "." + subindex2), f2b));
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        cell.setBackgroundColor(new Color(255, 145, 0));
                        table.addCell(cell);
                        cell = new PdfPCell(new Phrase("ADMINISTRACION", f2));
                        cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                        table.addCell(cell);
                        cell = new PdfPCell(new Phrase("UNIDAD", f2));
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        table.addCell(cell);
                        cell = new PdfPCell(new Phrase(String.valueOf(info.getCantidad()), f2));
                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        table.addCell(cell);
                        cell = new PdfPCell(new Phrase(formato.format(info.getTotal()), f2));
                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        table.addCell(cell);
                        cell = new PdfPCell(new Phrase(formato.format(info.getTotal()), f2));
                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        table.addCell(cell);
                        subtotalCot = subtotalCot + info.getTotal();
                        subindex++;
                    }
                }
            }

            // row 1, cell 1
            cell = new PdfPCell(new Phrase("Total Costo Directo", f3b));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setColspan(5);
            cell.setBackgroundColor(new Color(255, 145, 0));
            table.addCell(cell);
            // row 1, cell 6
            cell = new PdfPCell(new Phrase(formato.format(subtotalCot), f3b));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setBackgroundColor(new Color(255, 145, 0));
            table.addCell(cell);

            totalAdm = jsonObjCot.get("administracion").getAsDouble();
            // row 1, cell 1
            cell = new PdfPCell(new Phrase("Administracion", f3b));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setColspan(4);
            cell.setBackgroundColor(new Color(255, 145, 0));
            table.addCell(cell);
            // row 1, cell 5           
            cell = new PdfPCell(new Phrase(jsonObjCot.get("perc_administracioni").getAsString() + "%", f3b));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBackgroundColor(new Color(255, 145, 0));
            table.addCell(cell);
            // row 1, cell 6
            cell = new PdfPCell(new Phrase(formato.format(totalAdm), f3b));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setBackgroundColor(new Color(255, 145, 0));
            table.addCell(cell);

            double percImprevisto = jsonObjCot.get("perc_imprevisto").getAsDouble();
            totalImp = subtotalCot * percImprevisto / 100;
            // row 1, cell 1
            cell = new PdfPCell(new Phrase("Imprevisto", f3b));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setColspan(4);
            cell.setBackgroundColor(new Color(255, 145, 0));
            table.addCell(cell);
            // row 1, cell 5
            cell = new PdfPCell(new Phrase(jsonObjCot.get("perc_imprevistoi").getAsString() + "%", f3b));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBackgroundColor(new Color(255, 145, 0));
            table.addCell(cell);
            // row 1, cell 6
            cell = new PdfPCell(new Phrase(formato.format(totalImp), f3b));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setBackgroundColor(new Color(255, 145, 0));
            table.addCell(cell);

            totalUtil = subtotalCot * jsonObjCot.get("perc_utilidad").getAsDouble() / 100;
            // row 1, cell 1
            cell = new PdfPCell(new Phrase("Utilidad", f3b));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setColspan(4);
            cell.setBackgroundColor(new Color(255, 145, 0));
            table.addCell(cell);
            // row 1, cell 5
            cell = new PdfPCell(new Phrase(jsonObjCot.get("perc_utilidadi").getAsString() + "%", f3b));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBackgroundColor(new Color(255, 145, 0));
            table.addCell(cell);
            // row 1, cell 6
            cell = new PdfPCell(new Phrase(formato.format(totalUtil), f3b));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setBackgroundColor(new Color(255, 145, 0));
            table.addCell(cell);

            totalIva = subtotalCot * jsonObjCot.get("perc_iva").getAsDouble() / 100;
            // row 1, cell 1
            cell = new PdfPCell(new Phrase("IVA pleno o IVA sobre utilidad", f3b));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setColspan(4);
            cell.setBackgroundColor(new Color(255, 145, 0));
            table.addCell(cell);
            // row 1, cell 5
            String perc_iva = jsonObjCot.get("perc_iva").getAsString();
            if (perc_iva.startsWith("0")) {
                perc_iva = "19";
            }
            Phrase p = new Phrase(perc_iva + "%", f3b);
            cell = new PdfPCell(p);
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBackgroundColor(new Color(255, 145, 0));
            table.addCell(cell);
            // row 1, cell 6
            if (totalIva == 0) {
                totalIva = totalUtil * 0.19;
            }
            cell = new PdfPCell(new Phrase(formato.format(totalIva), f3b));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setBackgroundColor(new Color(255, 145, 0));
            table.addCell(cell);

            totalCot = subtotalCot + totalAdm + totalImp + totalUtil + totalIva;
            // row 1, cell 1
            
            
            
            totalInversion=formato.format(totalCot);

            cell = new PdfPCell(new Phrase("COSTO TOTAL", f3b));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setColspan(5);
            cell.setBackgroundColor(new Color(255, 145, 0));
            table.addCell(cell);
            // row 1, cell 6
            cell = new PdfPCell(new Phrase(formato.format(totalCot), f3b));
            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell.setBackgroundColor(new Color(255, 145, 0));
            table.addCell(cell);
            
            
            
                     BaseFont baseFont = BaseFont.createFont(rsb.getString("rutaImagenes") +"/fuentes/Gelion-Bold.ttf", "Cp1252",  true);
                     BaseFont baseFontp = BaseFont.createFont(rsb.getString("rutaImagenes") +"/fuentes/Roboto-Medium.ttf", "Cp1252",  true);

                    Font fuentetitle=new Font(baseFont);
                    fuentetitle.setColor(93, 93, 93);
                    fuentetitle.setSize(28f);
                    
                    Font fuenteParrafo=new Font(baseFontp);
                    fuenteParrafo.setColor(93, 93, 93);
                    fuenteParrafo.setSize(12f);
                    
                    
                    Paragraph texto=new Paragraph(new Phrase("VALOR DE LA INVERSION", fuentetitle));
                    texto.setAlignment(Element.ALIGN_CENTER);


                    Paragraph textop=new Paragraph(new Phrase("La oferta tiene un valor de COP"+totalInversion+", IVA incluido, para mayor detalle ver el siguiente cuadro de precio:", fuenteParrafo));
            
            
//            Chunk porcionTexto = new Chunk("La oferta tiene un valor de COP$"+totalInversion+", IVA incluido, para mayor detalle ver el siguiente cuadro de precio:");

            document.add(texto);
            document.add(new Paragraph(" "));
            document.add(textop);
            document.add(new Paragraph(" "));

            document.add(table);
            PdfPTable table2 = generar_Pdf_Cotizacion_Complemento(solicitud, con, listaInfo, detallado, true);
            document.newPage();
            document.add(table2);
            
            
            
            String title=null;
            
             JsonObject respuesta = new JsonObject();
                String valor = "0";
                
                
                int count=0;
                int countPages=0;
                int countFooter=0;
                
                if(jsonArrNotas!=null)
                countPages=jsonArrNotas.size();
                

                
                for (int i = 0; i < jsonArrNotas.size(); i++) {
                  respuesta = jsonArrNotas.get(i).getAsJsonObject();
            
                  if(respuesta.get("id_tipo_de_nota").getAsString()!=null && respuesta.get("id_tipo_de_nota").getAsInt()==1)
                    title =  "NOTAS ACLARATORIAS";
                  else if(respuesta.get("id_tipo_de_nota").getAsString()!=null && respuesta.get("id_tipo_de_nota").getAsInt()==3)
                    title =  "CONDICIONES COMERCIALES";
                  else if(respuesta.get("id_tipo_de_nota").getAsString()!=null && respuesta.get("id_tipo_de_nota").getAsInt()==4)
                    title =  "DOCUMENTOS REQUERIDOS";

            // Let's create de first Chapter (Creemos el primer cap�tulo)
//            Chapter chapter = new Chapter(new Paragraph(chunk), 1);
//            chapter.setNumberDepth(0); 
//                    
                        
                    BaseFont baseFontT = BaseFont.createFont(rsb.getString("rutaImagenes") +"/fuentes/Gelion-Bold.ttf", "Cp1252",  true);
                    Font fuenteT=new Font(baseFontT);
                    fuenteT.setColor(93, 93, 93);
                    fuenteT.setSize(26f);
                    Paragraph textoT=new Paragraph(new Phrase(title, fuenteT));


                   
//                    htmlWorker.parse(new StringReader(title));
//                    htmlWorker.parse(new StringReader("<br><br>"));
//                    
//                    
//
//                    htmlWorker.parse(new StringReader(respuesta.get("nota_de_oferta").getAsString()));
                   
                    if(respuesta.get("id_tipo_de_nota").getAsString()!=null && respuesta.get("id_tipo_de_nota").getAsInt()!=5){
                        
                     count=count+1;
                     
                     if(count==1)
                         document.newPage();
                        
                     htmlWorker=new HTMLWorker(document);
    
                     document.add(textoT); 
                     document.add(new Paragraph(" "));
                        
                    Paragraph textoHtml= this.parseHtmlToPdfDescription(respuesta.get("nota_de_oferta").getAsString());
                    
                    document.add(textoHtml);
                    
                    
                      
                    }else{
                      countFooter=countFooter+1;  
                        
                    }
                    
                     if(countPages!=0 && countPages>1 && i<countPages-countFooter)
                     document.newPage();
                                    
                }

        document.close();
 

            // step 5
          //  document.close();

            respuestaJson = "{\"respuesta\":\"OK\",\"Ruta\":\"" + "/images/multiservicios/" + usuario.getLogin() + "/" + nomarchivo + "\"}";

        } catch (Exception e) {
            try {
                respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR generar_Pdf_Cotizacion \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(MinutasContratacionImpl.class.getName()).log(Level.SEVERE, null, ex.getNextException());
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }
    
    
    
    
    
    private static Paragraph parseHtmlToPdfDescription(String description) {
        try {
            StringReader strReader = new StringReader(description);
            List p = HTMLWorker.parseToList(strReader, null);
            Paragraph paragraph = new Paragraph();
            
            ResourceBundle rsb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");

            
//            BaseFont helvetica = BaseFont.createFont(BaseFont.TIMES_ROMAN, BaseFont.CP1250, BaseFont.EMBEDDED);

               BaseFont baseFontp = BaseFont.createFont(rsb.getString("rutaImagenes") +"/fuentes/Roboto-Medium.ttf", "Cp1252",  true);

               
                    Font fuente=new Font(baseFontp);
                    fuente.setColor(93, 93, 93);
                    fuente.setSize(15f);
               
               

            for (int k = 0; k < p.size(); ++k) {
                paragraph.setFont(new Font(fuente)); //moje pr�by z ustawianiem czcionki

                paragraph.add((com.lowagie.text.Element) p.get(k));
            }
            return paragraph;
        } catch (DocumentException ex) {
        ex.printStackTrace();      
        } catch (IOException ex) {
           ex.printStackTrace();
        }
        return null;
    }
    
    
    
    
    private static Paragraph parseHtmlToPdfDescriptionAsesor(String description) {
        try {
            StringReader strReader = new StringReader(description);
            List p = HTMLWorker.parseToList(strReader, null);
            Paragraph paragraph = new Paragraph();
//            paragraph.setAlignment(Element.);
//            BaseFont helvetica = BaseFont.createFont(BaseFont.TIMES_ROMAN, BaseFont.CP1250, BaseFont.EMBEDDED);


        ResourceBundle rsb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");


        
          BaseFont baseFontp = BaseFont.createFont(rsb.getString("rutaImagenes") +"/fuentes/Roboto-Regular.ttf", "Cp1252",  true);

               
               
                Font fuente=new Font(baseFontp);
                fuente.setColor(5, 38, 65);
                fuente.setSize(28f);


            for (int k = 0; k < p.size(); ++k) {
                paragraph.add((com.lowagie.text.Element) p.get(k));
                paragraph.setFont(fuente); //moje pr�by z ustawianiem czcionki
                paragraph.setIndentationLeft(150);

                
            }
            return paragraph;
            
        } catch (DocumentException ex) {
        ex.printStackTrace();      
        } catch (IOException ex) {
           ex.printStackTrace();
        }
        return null;
    }
    
    
    

    public PdfPTable generar_Pdf_Cotizacion_Complemento(String solicitud, Connection con, List infoAdministracion, String detallado) {
        int w = 1;
        ps = null;
        rs = null;
        String sql = "";
        PdfPTable table = new PdfPTable(8);
        switch (detallado) {
            case "SI":
                query = "SQL_GENERAR_COTIZACION_DETALLADA_SIN_ADMINISTRACION";
                break;
            case "NO":
                query = "SQL_GENERAR_COTIZACION_DETALLADA";
                break;
        }

        ResourceBundle rb = null;
        
        
        
        
        try {
        
            
                    ResourceBundle rsb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");

            
             BaseFont baseFontTable = BaseFont.createFont(rsb.getString("rutaImagenes") +"/fuentes/Roboto-Regular.ttf", "Cp1252",  true);
              Font fuenteTable=new Font(baseFontTable);
              fuenteTable.setSize(8f);
            
            
            Font f = new Font(Font.TIMES_ROMAN, 12, Font.NORMAL);
            Font f1 = new Font(Font.TIMES_ROMAN, 12, Font.BOLD);

            Font f2 =fuenteTable;
            Font f3 = new Font(Font.TIMES_ROMAN, 8, Font.BOLD);

            Font f4 = new Font(Font.TIMES_ROMAN, 10, Font.NORMAL);

            Font fb = new Font(Font.TIMES_ROMAN, 12, Font.NORMAL, new Color(255, 255, 255));
            Font f1b = new Font(Font.TIMES_ROMAN, 12, Font.BOLD, new Color(255, 255, 255));
            Font f2b =fuenteTable;
            Font f3b = fuenteTable;
            Font f4b = new Font(Font.TIMES_ROMAN, 10, Font.NORMAL, new Color(255, 255, 255));

            table.setWidths(new int[]{2, 4, 9, 3, 2, 2, 4, 4});
            table.setWidthPercentage(100);
            PdfPCell cell;

            DecimalFormat formato = new DecimalFormat("$###,###,###.00");
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, solicitud);
            rs = ps.executeQuery();
            int indexArea = 1, indexCap = 1, indexApu = 1, indexInsumo = 1, pindex = 0, numero_de_fila = -1, filas_dentro_de_apu = 1, numero_de_fila_de_apu=0;
            String id_area = "", area = "", id_capitulo = "", capitulo = "", pIndexApu = "",
                    id_apu = "", apu = "", pIndexinsumo = "",  id_apu2 = "";
            double total = 0, total_esquema = 0;
            while (rs.next()) {
                if (!id_area.equals(rs.getString("id_area"))) {
                    id_area = rs.getString("id_area");
                    area = rs.getString("nombre_area");

                    indexCap = 1;
                    pindex++;
                    indexArea++;
                }
                if (!id_capitulo.equals(rs.getString("id_capitulo"))) {
                    id_capitulo = rs.getString("id_capitulo");
                    capitulo = rs.getString("nombre_capitulo");

                    pIndexApu = pindex + "." + indexCap;
                    indexApu = 1;
                    indexCap++;
                }
                id_apu2 = id_apu;
                if (!id_apu.equals(rs.getString("id_apu"))) {
                    // row espacio
                    if (w == 0) {
                        cell = new PdfPCell(new Phrase(" ", f2));
                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        cell.setColspan(8);
                        table.addCell(cell);
                    } else {
                        w = 0;
                    }
                    //ENCABEZADOS DE LA TABLA
                    // row 1, cell 1
                    cell = new PdfPCell(new Phrase("CODIGO", f3b));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setBackgroundColor(new Color(255, 145, 0));
                    table.addCell(cell);

                    // row 1, cell 2
                    cell = new PdfPCell(new Phrase("TIPO DE INSUMO", f3b));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setBackgroundColor(new Color(255, 145, 0));
                    table.addCell(cell);

                    // row 1, cell 3
                    cell = new PdfPCell(new Phrase("DESCRIPCION", f3b));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setBackgroundColor(new Color(255, 145, 0));
                    table.addCell(cell);

                    // row 1, cell 4
                    cell = new PdfPCell(new Phrase("UND", f3b));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setBackgroundColor(new Color(255, 145, 0));
                    table.addCell(cell);

                    // row 1, cell 5
                    cell = new PdfPCell(new Phrase("CANTIDAD", f3b));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setBackgroundColor(new Color(255, 145, 0));
                    table.addCell(cell);

                    // row 1, cell 6
                    cell = new PdfPCell(new Phrase("RENDIMIENTO", f3b));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setBackgroundColor(new Color(255, 145, 0));
                    table.addCell(cell);

                    // row 1, cell 7
                        cell = new PdfPCell(new Phrase("VALOR UNITARIO", f3b));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setBackgroundColor(new Color(255, 145, 0));
                    table.addCell(cell);

                    // row 1, cell 8
                        cell = new PdfPCell(new Phrase("VALOR TOTAL", f3b));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setBackgroundColor(new Color(255, 145, 0));
                    table.addCell(cell);

                    //agregar el valor 
                    id_apu = rs.getString("id_apu");
                    apu = rs.getString("nombre_apu").replace("PROYECTO_", "");
                    // row 1, cell 1
                    cell = new PdfPCell(new Phrase(String.valueOf(pIndexApu + "." + indexApu), f2b));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setBackgroundColor(new Color(255, 145, 0));
                    table.addCell(cell);
                    // row 1, cell 2
                    cell = new PdfPCell(new Phrase(apu, f2b));
                    cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                    cell.setColspan(6);
                    cell.setBackgroundColor(new Color(255, 145, 0));
                    table.addCell(cell);
                    // row 1, cell 1
                    cell = new PdfPCell(new Phrase(String.valueOf("0.0"), f2b));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setBackgroundColor(new Color(255, 145, 0));                    
                    table.addCell(cell);
                    
                    pIndexinsumo = pIndexApu + "." + indexApu;
                    indexInsumo = 1;
                    indexApu++;
                    //SE SUMA DOS AL NUMERO DE FILA POR LAS DOS FILAS (NARANJAS) DEL ENCABEZADO
                    numero_de_fila = numero_de_fila+2;
                    numero_de_fila_de_apu = numero_de_fila; 
                }               
                String tipo_insumo = rs.getString("tipo_insumo");
                String descripcion_insumo = rs.getString("descripcion_insumo");
                String unidad_medida_insumo = rs.getString("nombre_unidad_insumo");
                String cantidad_insumo = rs.getString("cantidad_insumo");
                String rendimiento_insumo = rs.getString("rendimiento_insumo");
                double valor_unitario = rs.getDouble("valor_unitario");
                double valor_esquema = rs.getDouble("total");               
                total = total + valor_esquema;
                
                //SI SE CUMPLE LA CONDICION INICIA UN NUEVO APU
                if (!id_apu2.equals(rs.getString("id_apu"))) {
                 int fila = numero_de_fila+1-(filas_dentro_de_apu+4);
                 //ASIGNAR A LA CELDA "TOTAL" VALOR TOTAL DEL ESQUEMA
                 setCeldaTotal(fila, total_esquema, table);
                 numero_de_fila++;
                 total_esquema = 0;
                 filas_dentro_de_apu=1;
                }else{
                  filas_dentro_de_apu++;
                }
                total_esquema = total_esquema + valor_esquema;                                              
                // row 1, cell 1
                cell = new PdfPCell(new Phrase(pIndexinsumo + "." + indexInsumo, f2));
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(cell);
                // row 1, cell 2
                cell = new PdfPCell(new Phrase(tipo_insumo, f2));
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(cell);
                // row 1, cell 3
                cell = new PdfPCell(new Phrase(descripcion_insumo, f2));
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                table.addCell(cell);
                // row 1, cell 4
                cell = new PdfPCell(new Phrase(unidad_medida_insumo, f2));
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(cell);
                // row 1, cell 5
                cell = new PdfPCell(new Phrase(cantidad_insumo, f2));
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(cell);
                // row 1, cell 6
                cell = new PdfPCell(new Phrase(rendimiento_insumo, f2));
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(cell);
                // row 1, cell 7
                    cell = new PdfPCell(new Phrase(formato.format(valor_unitario), f2));
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                table.addCell(cell);
                // row 1, cell 8
                    cell = new PdfPCell(new Phrase(formato.format(valor_esquema), f2));
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                table.addCell(cell);
                indexInsumo++;
                numero_de_fila++;
            }
            setCeldaTotal(numero_de_fila_de_apu, total_esquema, table);           
            int subdet = 1;
            pindex++;
            String numad = "";
            for (int i = 0; i < infoAdministracion.size(); i++) {
                BeansAdministracion info = (BeansAdministracion) listaInfo.get(i);

                if (!numad.equals(info.getNumad())) {
                    numad = info.getNumad();
                    cell = new PdfPCell(new Phrase(pindex + ".1.1", f2b));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setBackgroundColor(new Color(255, 145, 0));
                    table.addCell(cell);
                    cell = new PdfPCell(new Phrase("ADMINISTRACION", f2b));
                    cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                    cell.setBackgroundColor(new Color(255, 145, 0));
                    cell.setColspan(6);
                    table.addCell(cell);
                    cell = new PdfPCell(new Phrase(formato.format(info.getValor_total()), f2b));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setBackgroundColor(new Color(255, 145, 0));
                    table.addCell(cell);
                }
                cell = new PdfPCell(new Phrase(pindex + ".1.1." + subdet, f2));
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(cell);
                cell = new PdfPCell(new Phrase("-", f2));
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(cell);
                cell = new PdfPCell(new Phrase(info.getDescripcion(), f2));
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                table.addCell(cell);
                cell = new PdfPCell(new Phrase(info.getUnidad(), f2));
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(cell);
                cell = new PdfPCell(new Phrase(String.valueOf(info.getCantidad()), f2));
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(cell);
                cell = new PdfPCell(new Phrase("-", f2));
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(cell);
                    cell = new PdfPCell(new Phrase(formato.format(info.getValor_unitario()), f2));
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                table.addCell(cell);
                    cell = new PdfPCell(new Phrase(formato.format(info.getValor_total()), f2));
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                table.addCell(cell);
                subdet++;
            }

//            // row 1, cell 1
//            cell = new PdfPCell(new Phrase("COSTO TOTAL", f3));
//            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//            cell.setColspan(7);
//            cell.setBackgroundColor(new Color(255,145,0));
//            table.addCell(cell);
//            // row 1, cell 6
//            cell = new PdfPCell(new Phrase(formato.format(total), f3));
//            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//            cell.setBackgroundColor(new Color(255,145,0));
//            table.addCell(cell);            
        } catch (Exception e) {
            try {
                throw new SQLException("ERROR generar_Pdf_Cotizacion \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(MinutasContratacionImpl.class.getName()).log(Level.SEVERE, null, ex.getNextException());
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return table;
        }
    }

    
    public PdfPTable generar_Pdf_Cotizacion_Complemento(String solicitud, Connection con, List infoAdministracion, String detallado, boolean  precios) {
        int w = 1;
        ps = null;
        rs = null;
        String sql = "";
        PdfPTable table = new PdfPTable(8);
        switch (detallado) {
            case "SI":
                query = "SQL_GENERAR_COTIZACION_DETALLADA_SIN_ADMINISTRACION";
                break;
            case "NO":
                query = "SQL_GENERAR_COTIZACION_DETALLADA";
                break;
        }

        ResourceBundle rb = null;
        
        
        
        
        try {
        
            
                    ResourceBundle rsb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");

            
             BaseFont baseFontTable = BaseFont.createFont(rsb.getString("rutaImagenes") +"/fuentes/Roboto-Regular.ttf", "Cp1252",  true);
              Font fuenteTable=new Font(baseFontTable);
              fuenteTable.setSize(8f);
            
            
            Font f = new Font(Font.TIMES_ROMAN, 12, Font.NORMAL);
            Font f1 = new Font(Font.TIMES_ROMAN, 12, Font.BOLD);

            Font f2 =fuenteTable;
            Font f3 = new Font(Font.TIMES_ROMAN, 8, Font.BOLD);

            Font f4 = new Font(Font.TIMES_ROMAN, 10, Font.NORMAL);

            Font fb = new Font(Font.TIMES_ROMAN, 12, Font.NORMAL, new Color(255, 255, 255));
            Font f1b = new Font(Font.TIMES_ROMAN, 12, Font.BOLD, new Color(255, 255, 255));
            Font f2b =fuenteTable;
            Font f3b = fuenteTable;
            Font f4b = new Font(Font.TIMES_ROMAN, 10, Font.NORMAL, new Color(255, 255, 255));

//            table.setWidths(new int[]{2, 6, 14, 4, 2, 2});
            
            table.setWidths(new int[]{2, 4, 9, 3, 2, 2, 4, 4});
            table.setWidthPercentage(100);
            PdfPCell cell;

            DecimalFormat formato = new DecimalFormat("$###,###,###.00");
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, solicitud);
            rs = ps.executeQuery();
            int indexArea = 1, indexCap = 1, indexApu = 1, indexInsumo = 1, pindex = 0, numero_de_fila = -1, filas_dentro_de_apu = 1, numero_de_fila_de_apu=0;
            String id_area = "", area = "", id_capitulo = "", capitulo = "", pIndexApu = "",
                    id_apu = "", apu = "", pIndexinsumo = "",  id_apu2 = "";
            double total = 0, total_esquema = 0;
            while (rs.next()) {
                if (!id_area.equals(rs.getString("id_area"))) {
                    id_area = rs.getString("id_area");
                    area = rs.getString("nombre_area");

                    indexCap = 1;
                    pindex++;
                    indexArea++;
                }
                if (!id_capitulo.equals(rs.getString("id_capitulo"))) {
                    id_capitulo = rs.getString("id_capitulo");
                    capitulo = rs.getString("nombre_capitulo");

                    pIndexApu = pindex + "." + indexCap;
                    indexApu = 1;
                    indexCap++;
                }
                id_apu2 = id_apu;
                if (!id_apu.equals(rs.getString("id_apu"))) {
                    // row espacio
                    if (w == 0) {
                        cell = new PdfPCell(new Phrase(" ", f2));
                        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        cell.setColspan(8);
                        table.addCell(cell);
                    } else {
                        w = 0;
                    }
                    //ENCABEZADOS DE LA TABLA
                    // row 1, cell 1
                    cell = new PdfPCell(new Phrase("CODIGO", f3b));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setBackgroundColor(new Color(255, 145, 0));
                    table.addCell(cell);

                    // row 1, cell 2
                    cell = new PdfPCell(new Phrase("TIPO DE INSUMO", f3b));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setBackgroundColor(new Color(255, 145, 0));
                    table.addCell(cell);

                    // row 1, cell 3
                    cell = new PdfPCell(new Phrase("DESCRIPCION", f3b));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setBackgroundColor(new Color(255, 145, 0));
                    table.addCell(cell);

                    // row 1, cell 4
                    cell = new PdfPCell(new Phrase("UND", f3b));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setBackgroundColor(new Color(255, 145, 0));
                    table.addCell(cell);

                    // row 1, cell 5
                    cell = new PdfPCell(new Phrase("CANTIDAD", f3b));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setBackgroundColor(new Color(255, 145, 0));
                    table.addCell(cell);

                    // row 1, cell 6
                    cell = new PdfPCell(new Phrase("RENDIMIENTO", f3b));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setBackgroundColor(new Color(255, 145, 0));
                    table.addCell(cell);

                    // row 1, cell 7
                    
                    cell = new PdfPCell(new Phrase("VALOR UNITARIO", f3b));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setBackgroundColor(new Color(255, 145, 0));
                    table.addCell(cell);

                    // row 1, cell 8
                    
                    cell = new PdfPCell(new Phrase("VALOR TOTAL", f3b));                    
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setBackgroundColor(new Color(255, 145, 0));
                    table.addCell(cell);

                    //agregar el valor 
                    id_apu = rs.getString("id_apu");
                    apu = rs.getString("nombre_apu").replace("PROYECTO_", "");
                    // row 1, cell 1
                    cell = new PdfPCell(new Phrase(String.valueOf(pIndexApu + "." + indexApu), f2b));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setBackgroundColor(new Color(255, 145, 0));
                    table.addCell(cell);
                    // row 1, cell 2
                    cell = new PdfPCell(new Phrase(apu, f2b));
                    cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                    cell.setColspan(6);
                    cell.setBackgroundColor(new Color(255, 145, 0));
                    table.addCell(cell);
                    // row 1, cell 1
                    if(!precios){
                        cell = new PdfPCell(new Phrase("-", f2));
                    }else{
                        cell = new PdfPCell(new Phrase(String.valueOf("0.0"), f2b));
                    }
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setBackgroundColor(new Color(255, 145, 0));                    
                    table.addCell(cell);
                    
                    pIndexinsumo = pIndexApu + "." + indexApu;
                    indexInsumo = 1;
                    indexApu++;
                    //SE SUMA DOS AL NUMERO DE FILA POR LAS DOS FILAS (NARANJAS) DEL ENCABEZADO
                    numero_de_fila = numero_de_fila+2;
                    numero_de_fila_de_apu = numero_de_fila; 
                }               
                String tipo_insumo = rs.getString("tipo_insumo");
                String descripcion_insumo = rs.getString("descripcion_insumo");
                String unidad_medida_insumo = rs.getString("nombre_unidad_insumo");
                String cantidad_insumo = rs.getString("cantidad_insumo");
                String rendimiento_insumo = rs.getString("rendimiento_insumo");
                double valor_unitario = rs.getDouble("valor_unitario");
                double valor_esquema = rs.getDouble("total");               
                total = total + valor_esquema;
                
                //SI SE CUMPLE LA CONDICION INICIA UN NUEVO APU
                if (!id_apu2.equals(rs.getString("id_apu"))) {
                    int fila = numero_de_fila+1-(filas_dentro_de_apu+4);
                    //ASIGNAR A LA CELDA "TOTAL" VALOR TOTAL DEL ESQUEMA
                    setCeldaTotal(fila, total_esquema, table);
                    numero_de_fila++;
                    total_esquema = 0;
                    filas_dentro_de_apu=1;
                }else{
                    filas_dentro_de_apu++;
                }
                total_esquema = total_esquema + valor_esquema;                                              
                // row 1, cell 1
                cell = new PdfPCell(new Phrase(pIndexinsumo + "." + indexInsumo, f2));
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(cell);
                // row 1, cell 2
                cell = new PdfPCell(new Phrase(tipo_insumo, f2));
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(cell);
                // row 1, cell 3
                cell = new PdfPCell(new Phrase(descripcion_insumo, f2));
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                table.addCell(cell);
                // row 1, cell 4
                cell = new PdfPCell(new Phrase(unidad_medida_insumo, f2));
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(cell);
                // row 1, cell 5
                cell = new PdfPCell(new Phrase(cantidad_insumo, f2));
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(cell);
                // row 1, cell 6
                cell = new PdfPCell(new Phrase(rendimiento_insumo, f2));
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(cell);
                // row 1, cell 7
                if(!precios){
                    cell = new PdfPCell(new Phrase("-", f2));
                }else{
                    cell = new PdfPCell(new Phrase(formato.format(valor_unitario), f2));
                }   
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                table.addCell(cell);
                // row 1, cell 8
                if(!precios){
                    cell = new PdfPCell(new Phrase("-", f2));
                }else{
                    cell = new PdfPCell(new Phrase(formato.format(valor_esquema), f2));
                } 
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                table.addCell(cell);
                indexInsumo++;
                numero_de_fila++;
            }
            setCeldaTotal(numero_de_fila_de_apu, total_esquema, table);           
            int subdet = 1;
            pindex++;
            String numad = "";
            for (int i = 0; i < infoAdministracion.size(); i++) {
                BeansAdministracion info = (BeansAdministracion) listaInfo.get(i);

                if (!numad.equals(info.getNumad())) {
                    numad = info.getNumad();
                    cell = new PdfPCell(new Phrase(pindex + ".1.1", f2b));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setBackgroundColor(new Color(255, 145, 0));
                    table.addCell(cell);
                    cell = new PdfPCell(new Phrase("ADMINISTRACION", f2b));
                    cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                    cell.setBackgroundColor(new Color(255, 145, 0));
                    cell.setColspan(6);
                    table.addCell(cell);
                    cell = new PdfPCell(new Phrase(formato.format(info.getValor_total()), f2b));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setBackgroundColor(new Color(255, 145, 0));
                    table.addCell(cell);
                }
                cell = new PdfPCell(new Phrase(pindex + ".1.1." + subdet, f2));
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(cell);
                cell = new PdfPCell(new Phrase("-", f2));
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(cell);
                cell = new PdfPCell(new Phrase(info.getDescripcion(), f2));
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                table.addCell(cell);
                cell = new PdfPCell(new Phrase(info.getUnidad(), f2));
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(cell);
                cell = new PdfPCell(new Phrase(String.valueOf(info.getCantidad()), f2));
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(cell);
                cell = new PdfPCell(new Phrase("-", f2));
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(cell);
                if(!precios){
                    cell = new PdfPCell(new Phrase("-", f2));
                }else{
                    cell = new PdfPCell(new Phrase(formato.format(info.getValor_unitario()), f2));
                }  
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                table.addCell(cell);
                if(!precios){
                    cell = new PdfPCell(new Phrase("-", f2));
                }else{
                    cell = new PdfPCell(new Phrase(formato.format(info.getValor_total()), f2));
                } 
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                table.addCell(cell);
                subdet++;
            }

//            // row 1, cell 1
//            cell = new PdfPCell(new Phrase("COSTO TOTAL", f3));
//            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//            cell.setColspan(7);
//            cell.setBackgroundColor(new Color(255,145,0));
//            table.addCell(cell);
//            // row 1, cell 6
//            cell = new PdfPCell(new Phrase(formato.format(total), f3));
//            cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
//            cell.setBackgroundColor(new Color(255,145,0));
//            table.addCell(cell);            
        } catch (Exception e) {
            try {
                throw new SQLException("ERROR generar_Pdf_Cotizacion \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(MinutasContratacionImpl.class.getName()).log(Level.SEVERE, null, ex.getNextException());
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return table;
        }
    }

    public List cargarDetAdministracion(String solicitud) throws Exception {

        List listainfo = new LinkedList();
        query = "SQL_CARGAR_ADMINISTRACION_DETALLE";
        try {
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, solicitud);
            ps.setString(2, solicitud);
            rs = ps.executeQuery();
            while (rs.next()) {
                BeansAdministracion info = new BeansAdministracion();
                info.setNumad(rs.getString("numad"));
                info.setDescripcion(rs.getString("descripcion"));
                info.setUnidad(rs.getString("unidad"));
                info.setCantidad(rs.getInt("cantidad"));
                info.setValor_unitario(rs.getDouble("valor_unitario"));
                info.setValor_total(rs.getDouble("valor_total"));
                info.setTotal(rs.getDouble("total"));
                listainfo.add(info);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return listainfo;
    }

    private String directorioArchivo(String user, String cons, String extension) throws Exception {
        String ruta = "";
        try {
            ResourceBundle rsb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            ruta = rsb.getString("ruta") + "/exportar/migracion/" + user;
            SimpleDateFormat fmt = new SimpleDateFormat("yyyMMdd_hhmmss");
            File archivo = new File(ruta);
            if (!archivo.exists()) {
                archivo.mkdirs();
            }
            ruta = ruta + "/" + "cuenta_cobro_" + cons + "_" + fmt.format(new Date()) + "." + extension;

        } catch (Exception e) {
            throw new Exception("Error al generar el directorio: " + e.toString());
        }
        return ruta;
    }

    private Document createDoc() {
        Document doc = new Document(PageSize.A4, 35, 35, 35, 30);
        return doc;
    }

    private String mesToString(int mes) {
        String texto = "";
        switch (mes) {
            case 1:
                texto = "Enero";
                break;
            case 2:
                texto = "Febrero";
                break;
            case 3:
                texto = "Marzo";
                break;
            case 4:
                texto = "Abril";
                break;
            case 5:
                texto = "Mayo";
                break;
            case 6:
                texto = "Junio";
                break;
            case 7:
                texto = "Julio";
                break;
            case 8:
                texto = "Agosto";
                break;
            case 9:
                texto = "Septiembre";
                break;
            case 10:
                texto = "Octubre";
                break;
            case 11:
                texto = "Noviembre";
                break;
            case 12:
                texto = "Diciembre";
                break;
            default:
                texto = "Enero";
                break;
        }
        return texto;
    }

    @Override
    public String obtener_id_rel_actividades_apu(String id_solicitud, String op, String filtro2) {
        con = null;
        ps = null;
        rs = null;
        String consulta = "", filtro1 = "";
        String query = "SQL_GET_ID_REL_ACTIVIDADES_APU";

        JsonArray arr = new JsonArray();
        if (op.equals("1")) {
            filtro1 = "b.id";
        } else {
            if (op.equals("2")) {
                filtro1 = "c.id";
            } else {
                if (op.equals("3")) {
                    filtro1 = "d.id";
                } else {
                    if (op.equals("4")) {
                        filtro1 = "e.id";
                    }

                }

            }

        }

        try {
            con = this.conectarJNDI();
            if (con != null) {
                consulta = this.obtenerSQL(query).replaceAll("#filtro1", filtro1).replaceAll("#filtro2", filtro2);
                ps = con.prepareStatement(consulta);
                ps.setString(1, id_solicitud);
                rs = ps.executeQuery();

                JsonObject fila = null;
                while (rs.next()) {
                    fila = new JsonObject();
                    fila.addProperty("id_rel_actividades_apu", rs.getString("id_rel_actividades_apu"));
                    arr.add(fila);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return new Gson().toJson(arr);
        }
    }

    @Override
    public String set_Rentabilidad_Global(String filtro, String perc_contratista, String perc_esquema, String tipo, String distribucion_rentabilidad_esquema) {
        con = null;
        ps = null;
        String respuestaJson = "{}", consulta = "";
        query = "SET_RENTABILIDAD_GLOBAL";
        try {
            if (tipo.equals("1")) {
                query = "SET_RENT_ESQUEMA_GLOBAL";
                con = this.conectarJNDI(query);
                consulta = this.obtenerSQL(query).replaceAll("#filtro1", filtro).replaceAll("#filtro2", perc_esquema);
                ps = con.prepareStatement(consulta);

                ps.executeUpdate();

                query = "SET_DISTRIBUCION_RENTABILIDAD_ESQUEMA";
                con = this.conectarJNDI("query");
                consulta = this.obtenerSQL(query).replaceAll("#filtro1", filtro).replaceAll("#filtro2", distribucion_rentabilidad_esquema).replaceAll("#filtro3", perc_esquema);
                ps = con.prepareStatement(consulta);

                ps.executeUpdate();

            } else {
                if (!filtro.equals("")) {
                    con = this.conectarJNDI(query);
                    consulta = this.obtenerSQL(query).replaceAll("#filtro1", filtro).replaceAll("#filtro3", perc_contratista);
                    ps = con.prepareStatement(consulta);

                    ps.executeUpdate();
                }

            }

            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            try {
                respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR guardarMetaProceso \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(ProcesosCatalogoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
            }
            return respuestaJson;
        }
    }

    @Override
    public String buscarAccion(String num_solicitud) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_BUSCAR_ACCION_CAMBIO_COTIZACION";
        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, num_solicitud);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }
            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                informacion = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return informacion;
    }

    @Override
    public String guardarCambioCotizacion(JsonObject informacion, Usuario usuario) {
        JsonObject objeto = new JsonObject();
        StringStatement st = null;
        String respuesta = "";
        TransaccionService tService = null;
        try {
            tService = new TransaccionService(this.getDatabaseName());
            tService.crearStatement();
            JsonArray arr = informacion.getAsJsonArray("json");
            String mensaje = "";
            // op = si es accion : 2 ? num_solictud:1 
            String query = "";
            if (perteneceNuevoModulo("1", informacion.get("id_solicitud").getAsString())) {
//                query = "SQL_ACTUALIZAR_COTIZACION";
                mensaje = "LA SOLICITUD ES NUEVA";
            } else {
                query = "SQL_ACTUALIZAR_COTIZACION_TEMP";
                for (int i = 0; i < arr.size(); i++) {
                    objeto = arr.get(i).getAsJsonObject();
                    st = new StringStatement(this.obtenerSQL(query), true);
                    st.setString(1, objeto.get("valor_cotizacion").getAsString());
                    st.setString(2, objeto.get("valor_descuento").getAsString());
                    st.setString(3, objeto.get("subtotal").getAsString());
                    st.setString(4, objeto.get("perc_iva").getAsString());
                    st.setString(5, objeto.get("valor_iva").getAsString());
                    st.setString(6, objeto.get("administracion").getAsString());
                    st.setString(7, objeto.get("imprevisto").getAsString());
                    st.setString(8, objeto.get("utilidad").getAsString());
                    st.setString(9, objeto.get("perc_aiu").getAsString());
                    st.setString(10, objeto.get("valor_aiu").getAsString());
                    st.setString(11, objeto.get("total").getAsString());
                    st.setString(12, objeto.get("anticipo").getAsString());
                    st.setString(13, objeto.get("perc_anticipo").getAsString());
                    st.setString(14, objeto.get("valor_anticipo").getAsString());
                    st.setString(15, objeto.get("retegarantia").getAsString());
                    st.setString(16, objeto.get("perc_retegarantia").getAsString());
                    st.setString(17, objeto.get("modalidad").getAsString());
                    st.setString(18, objeto.get("presupuesto_terminado").getAsString());
                    st.setString(19, usuario.getLogin());
                    st.setString(20, objeto.get("valor_rentabilidad_contratista").getAsString());
                    st.setString(21, objeto.get("valor_rentabilidad_esquema").getAsString());
                    st.setString(22, informacion.get("id_solicitud").getAsString());

                    tService.getSt().addBatch(st.getSql());
                    mensaje = "Guardado";
                }
            }

            tService.execute();
            respuesta = "{\"respuesta\":\"" + mensaje + "\"}";
            //respuesta = st.getSql();
        } catch (Exception ex) {
            respuesta = "{\"respuesta\":\"ERROR\"}";
            //respuesta = ex.getMessage();
        } finally {
            try {
                tService.closeAll();
            } catch (Exception e) {
            }
            return respuesta;
        }
    }

    @Override
    public String buscarCotizacionTem(String num_solicitud) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        // op = si es accion : 2 ? num_solictud:1 
        String query = "SQL_BUSCAR_CAMBIO_COTIZACION_TEM";
//        if (perteneceNuevoModulo("1", num_solicitud)) {
//            query = "SQL_BUSCAR_CAMBIO_COTIZACION";
//        } else {
//            query = "SQL_BUSCAR_CAMBIO_COTIZACION_TEM";
//        }

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, num_solicitud);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }
            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                informacion = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return informacion;
    }

    public String insertar_cabecera_tabla_temporal(String id_solicitud, Usuario usuario) {

        con = null;
        ps = null;
        Statement stmt = null;
        String respuestaJson = "{}";

        try {
            if (verifica_Existe_Cotizacion_Tem(id_solicitud)) {
                query = "UPDATE_CABECERA_TABLA_TEMPORAL";
            } else {
                query = "INSERTAR_CABECERA_TABLA_TEMPORAL";
            }

            con = this.conectarJNDI();
            con.setAutoCommit(false);
            stmt = con.createStatement();

            st = new StringStatement(this.obtenerSQL(query), true);

            st.setString(1, usuario.getLogin());
            st.setString(2, id_solicitud);

            stmt.addBatch(st.getSql());

            query = "UPDATE_PRESUPUESTO_TERMINADO_SL_COTIZACION";

            st = new StringStatement(this.obtenerSQL(query), true);

            st.setString(1, usuario.getLogin());
            st.setString(2, id_solicitud);

            stmt.addBatch(st.getSql());

            stmt.executeBatch();
            stmt.clearBatch();
            con.commit();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            try {
                con.rollback();
                respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR en el envio de la solicitud. \n" + e.getMessage());

            } catch (SQLException ex) {

            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
            }
            return respuestaJson;
        }
    }

    public boolean verifica_Existe_Cotizacion_Tem(String idSolicitud) {
        con = null;
        rs = null;
        ps = null;
        query = "VERIFICA_EXISTE_COTIZACION_TEM";
        boolean existe = false;

        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, idSolicitud);
            rs = ps.executeQuery();
            while (rs.next()) {
                existe = true;
                break;
            }

        } catch (Exception e) {

        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return existe;
    }

    @Override
    public String autocompletar(String q, String opp) {
        con = null;
        ps = null;
        rs = null;
        String query = "";

        if (opp.equals("1")) {
            query = "CARGAR_CLIENTES_AUTOCOMPLETAR";
        } else if (opp.equals("2")) {
            query = "CARGAR_PROYECTOS_AUTOCOMPLETAR";
        } else if (opp.equals("3")) {
            query = "SQL_CARGAR_RESPONSABLES_AUTOCOMPLETAR";
        }

        JsonArray lista = null;

        try {
            con = this.conectarJNDI();
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));
                ps.setString(1, q);
                rs = ps.executeQuery();
                lista = new JsonArray();
                JsonObject fila;
                while (rs.next()) {
                    fila = new JsonObject();
                    fila.addProperty("value", rs.getString("value"));
                    fila.addProperty("label", rs.getString("descripcion"));
                    lista.add(fila);
                }
            }
        } catch (Exception e) {
            lista = new JsonArray();
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return new Gson().toJson(lista);
        }
    }

    @Override
    public String update_Presupuesto_Terminado(String id_solicitud, String estado, Usuario usuario) {
        con = null;
        ps = null;
        String respuestaJson = "{}", consulta = "";
        query = "SET_RENTABILIDAD_GLOBAL";
        try {

            query = "UPDATE_PRESUPUESTO_TERMINADO";
            con = this.conectarJNDI(query);
            consulta = this.obtenerSQL(query);
            ps = con.prepareStatement(consulta);

            ps.setString(1, estado);
            ps.setString(2, usuario.getLogin());
            ps.setString(3, id_solicitud);

            ps.executeUpdate();

            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            try {
                respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(ProcesosCatalogoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
            }
            return respuestaJson;
        }
    }

    @Override
    public String generar_OT(String id_solicitud, Usuario usuario) {
        con = null;
        rs = null;
        ps = null;
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI();

            ps = con.prepareStatement("SELECT mc_generar_orden_trabajo('" + id_solicitud + "','" + usuario.getLogin() + "');");

            rs = ps.executeQuery();

            while (rs.next()) {
                if (rs.getString("mc_generar_orden_trabajo").equals("t")) {
                    respuestaJson = "{\"respuesta\":\"OK\"}";
                } else {
                    respuestaJson = "{\"respuesta\":\"Error\"}";
                }
            }

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return respuestaJson;
    }

    public boolean perteneceNuevoModulo(String op, String id) {
        con = null;
        rs = null;
        ps = null;
        query = "SQL_OBTENER__NUEVO_MODULO_OFERTA";
        String filtro = "";//(op.equals("1")) ? " WHERE id_solicitud = '" + id + "'" : " WHERE id_solicitud IN (select id_solicitud from opav.acciones where id_accion = '" + id + "')";
        if (op.equals("1")) {
            filtro = " WHERE id_solicitud = '" + id + "'";
        } else if (op.equals("2")) {
            filtro = " WHERE id_solicitud IN (select id_solicitud from opav.acciones where id_accion = '" + id + "')";
        } else if (op.equals("3")) {
            filtro = " WHERE nombre_proyecto ilike '%" + id + "%'";
        }
        boolean resp = false;
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query).replaceAll("#filtro", filtro);
            ps = con.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                if (rs.getString("nuevo_modulo").equals("1")) {
                    resp = true;
                }
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return resp;
    }

    @Override
    public String cargarTrazabilidadOferta(String idsolicitud) {
        con = null;
        ps = null;
        rs = null;
        String x = "";
        JsonObject respuesta = new JsonObject();
        query = "SQL_OBTENER_TRAZABILIDAD_OFERTA";

        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, idsolicitud);
            rs = ps.executeQuery();

            while (rs.next()) {
                x = x + rs.getString("descripcion") + "\n";
            }
            respuesta.addProperty("respuesta", x);
        } catch (Exception e) {
            respuesta.addProperty("error", e.getMessage());
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return respuesta.toString();
        }
    }

    @Override
    public String cargarInformacionModalidadProyecto(String solicitud, String nom_proyecto) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "";
        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {

            con = this.conectarJNDI();
            String consulta = "";
            String condicion = "";
            String condicion_consulta = "";
            String op = "";
            if (!solicitud.equals("")) {
                condicion = condicion + " and ofe.id_solicitud = '" + solicitud + "'";
                condicion_consulta = solicitud;
                op = "1";
            }
            if (!nom_proyecto.equals("")) {
                condicion = condicion + " and ofe.nombre_proyecto ilike  '%" + nom_proyecto + "%'";
                condicion_consulta = nom_proyecto;
                op = "3";
            }

            query = "SQL_CARGAR_INFORMACION_MODALIDAD_PROYECTO";
//            if (perteneceNuevoModulo(op, condicion_consulta)) {
//                query = "SQL_CARGAR_INFORMACION_MODALIDAD_PROYECTO";
//            } else {
//                query = "SQL_CARGAR_INFORMACION_MODALIDAD_PROYECTO_TEMP";
//            }

            consulta = this.obtenerSQL(query).replaceAll("#parametro", condicion);
            ps = con.prepareStatement(consulta);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            consulta = this.obtenerSQL("SQL_CARGAR_INFORMACION_MODALIDAD_PROYECTO_IVA_MATERIAL").replaceAll("#parametro", solicitud);
            ps = con.prepareStatement(consulta);
            rs = ps.executeQuery();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return informacion;
        }
    }

    @Override
    public String guardarInformacionModalidadProyecto(String solicitud, String porc_administracion, String administracion, String porc_imprevisto, String imprevisto, String porc_utilidad, String utilidad, String iva, Usuario usuario, String perc_iva, String total, String modalidad_comercial, String porc_aiu, String aiu, String iva_compensar, String perc_iva_compensado) {
        Connection con = null;
        PreparedStatement ps = null;
        String respuesta = "";
        try {

            String mensaje = "";
            // op = si es accion : 2 ? num_solictud:1 
            String query = "";
//            if (perteneceNuevoModulo("1", solicitud)) {
            query = "SQL_ACTUALIZAR_COTIZACION_MODALIDAD";
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setDouble(1, Double.parseDouble(perc_iva));
            ps.setDouble(2, Double.parseDouble(iva));
            ps.setDouble(3, Double.parseDouble(porc_administracion));
            ps.setDouble(4, Double.parseDouble(administracion));
            ps.setDouble(5, Double.parseDouble(porc_imprevisto));
            ps.setDouble(6, Double.parseDouble(imprevisto));
            ps.setDouble(7, Double.parseDouble(porc_utilidad));
            ps.setDouble(8, Double.parseDouble(utilidad));
            ps.setDouble(9, Double.parseDouble(porc_aiu));
            ps.setDouble(10, Double.parseDouble(aiu));
            ps.setDouble(11, Double.parseDouble(total));
            ps.setString(12, modalidad_comercial);
            ps.setString(13, usuario.getLogin());
            ps.setString(14, iva_compensar);
            ps.setString(15, perc_iva_compensado);
            ps.setString(16, solicitud);
            ps.executeUpdate();

            mensaje = "Guardado";

            respuesta = "{\"respuesta\":\"" + mensaje + "\"}";
            //respuesta = st.getSql();
        } catch (Exception ex) {
            respuesta = "{\"respuesta\":\"ERROR\"}";
            //respuesta = ex.getMessage();
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuesta;
        }
    }

    @Override
    public String cargar_tablas_modulo_impresion_oferta(String idsolicitud, String opc) {
        con = null;
        ps = null;
        rs = null;
        query = "";
        switch (opc) {
            case "6":
                query = "SQL_CARGAR_TBL_APU_IMPRESION2";
                break;
            case "5":
                query = "SQL_CARGAR_TBL_AREAS_IMPRESION";
                break;
            case "4":
                query = "SQL_CARGAR_TBL_DISCIPLINAS_IMPRESION";
                break;
            case "3":
                query = "SQL_CARGAR_TBL_CAPITULOS_IMPRESION";
                break;
            case "2":
                query = "SQL_CARGAR_TBL_ACTIVIDADES_IMPRESION";
                break;
            case "1":
                query = "SQL_CARGAR_TBL_APU_IMPRESION";
                break;
            default:
                throw new AssertionError();
        }

        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            String consulta = "";
            consulta = this.obtenerSQL(query);
            ps = con.prepareStatement(consulta);
            ps.setString(1, idsolicitud);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }
            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return informacion;
        }
    }

    @Override
    public String calcular_iva_compensar(String solicitud, String subtotal) {
        con = null;
        ps = null;
        rs = null;
        query = "";
        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        String consulta;
        try {

            con = this.conectarJNDI();
            query = "SQL_SP_OBTENER_COSTOS_ADICIONALES";

            consulta = this.obtenerSQL(query);
            ps = con.prepareStatement(consulta);
            ps.setString(1, solicitud);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

//            consulta = this.obtenerSQL("SQL_CARGAR_INFORMACION_MODALIDAD_PROYECTO_IVA_MATERIAL").replaceAll("#parametro", solicitud);
//            ps = con.prepareStatement(consulta);
//            rs = ps.executeQuery();
//            while (rs.next()) {
//                objetoJson = new JsonObject();
//                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
//                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
//                }
//                lista.add(objetoJson);
//            }
            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return informacion;
        }
    }

    @Override
    public JsonArray getInfoPdfCotizacion(String Query, String num_solicitud) {
        con = null;
        rs = null;
        ps = null;
        JsonArray lista = null;
        JsonObject jsonObject = null;

        Gson gson = new Gson();
        try {
            con = this.conectarJNDI(Query);
            ps = con.prepareStatement(this.obtenerSQL(Query));
            ps.setString(1, num_solicitud);

            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                jsonObject = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    jsonObject.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(jsonObject);
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return lista;
    }
    
    
    
    
    
    
    
    
    @Override
    public String obtenerNotasOferta(String num_solicitud) {
        
        
         Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "obtenerNotasOferta";
        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            String consulta = "";
          
            
            

            consulta = this.obtenerSQL(query);
          
            ps = con.prepareStatement(consulta);
            ps.setString(1, num_solicitud);

            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }
            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
 } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return informacion;
        }
    }
        
        
        
        
        
    
    
    
    
    
    
     @Override
    public JsonArray getNotasOferta(String num_solicitud) {
        con = null;
        rs = null;
        ps = null;
        JsonArray lista = null;
        JsonObject jsonObject = null;

        Gson gson = new Gson();
        try {
            con = this.conectarJNDI("obtenerNotasOferta");
            ps = con.prepareStatement(this.obtenerSQL("obtenerNotasOferta"));
            ps.setString(1, num_solicitud);

            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                jsonObject = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    jsonObject.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(jsonObject);
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return lista;
    }
    
    
    

    public JsonObject obtenerTotalesApusPresupuesto(String id_solicitud, String id_capitulo, String id_apu) {
        con = null;
        ResultSet rs1 = null;
        PreparedStatement ps1 = null;

        JsonObject obj = new JsonObject();
        query = "obtenerTotalesApusPresupuesto";
        try {
            con = this.conectarJNDI(query);
            ps1 = con.prepareStatement(this.obtenerSQL(query));
            ps1.setString(1, id_solicitud);
            ps1.setString(2, id_capitulo);
            ps1.setString(3, id_apu);

            rs1 = ps1.executeQuery();

            while (rs1.next()) {
                obj.addProperty("unidad_medida_apu", rs1.getString("unidad_medida_apu"));
                obj.addProperty("cantidad_apu", rs1.getString("cantidad_apu"));
                obj.addProperty("subtotal", rs1.getString("subtotal"));
                obj.addProperty("total", rs1.getString("total"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps1 != null) {
                    ps1.close();
                }
                if (rs1 != null) {
                    rs1.close();
                }
            } catch (SQLException ex) {
            }
        }
        return obj;
    }

    public String getValueFromArray(JsonArray jarr, String idBuscar, String campoRetorno) {
        JsonObject respuesta = new JsonObject();
        String valor = "0";
        for (int i = 0; i < jarr.size(); i++) {
            respuesta = jarr.get(i).getAsJsonObject();

            if (respuesta.get("id").getAsString().equals(idBuscar)) {
                valor = respuesta.get(campoRetorno).getAsString();
                break;
            }
        }
        return valor;
    }

    @Override
    public String cargarReporteAnticiposMs() {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_REPORTE_ANTICIPOS_MS";
        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }
            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                informacion = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return informacion;
    }

    @Override
    public String guardarCondionesComerciales(JsonObject informacion, Usuario usuario) {
        JsonObject objeto = new JsonObject();
        StringStatement st = null;
        String respuesta = "";
        String sqlver = "";
        TransaccionService tService = null;
        try {
            tService = new TransaccionService(this.getDatabaseName());
            tService.crearStatement();
            JsonArray arr = informacion.getAsJsonArray("info");
//            System.out.println(informacion.getAsJsonArray("info"));
            String operacion = existeCondicionComercial(informacion.get("id_solicitud").getAsString());
            for (int i = 0; i < arr.size(); i++) {
                objeto = arr.get(i).getAsJsonObject();
                switch (operacion) {
                    case "NO":

                        st = new StringStatement(this.obtenerSQL("SQL_GUARDAR_CONDICIONES_COMERCIALES"), true);
                        st.setString(1, informacion.get("id_solicitud").getAsString());
                        st.setString(2, objeto.get("antes_iva_ant").getAsString());
                        st.setString(3, objeto.get("antes_iva_ret").getAsString());
                        st.setString(4, objeto.get("administracion_ant").getAsString());
                        st.setString(5, objeto.get("administracion_ret").getAsString());
                        st.setString(6, objeto.get("imprevisto_ant").getAsString());
                        st.setString(7, objeto.get("imprevisto_rete").getAsString());
                        st.setString(8, objeto.get("utilidad_ant").getAsString());
                        st.setString(9, objeto.get("utilidad_rete").getAsString());
                        st.setString(10, usuario.getLogin());
                        break;
                    case "SI":
                        st = new StringStatement(this.obtenerSQL("SQL_ACTUALIZAR_CONDICIONES_COMERCIALES"), true);
                        st.setString(1, objeto.get("antes_iva_ant").getAsString());
                        st.setString(2, objeto.get("antes_iva_ret").getAsString());
                        st.setString(3, objeto.get("administracion_ant").getAsString());
                        st.setString(4, objeto.get("administracion_ret").getAsString());
                        st.setString(5, objeto.get("imprevisto_ant").getAsString());
                        st.setString(6, objeto.get("imprevisto_rete").getAsString());
                        st.setString(7, objeto.get("utilidad_ant").getAsString());
                        st.setString(8, objeto.get("utilidad_rete").getAsString());
                        st.setString(9, usuario.getLogin());
                        st.setString(10, informacion.get("id_solicitud").getAsString());
                        break;
                }
                sqlver = st.getSql();
                System.out.println(sqlver);
                tService.getSt().addBatch(sqlver);
//                tService.getSt().addBatch(st.getSql());
            }
            tService.execute();
            respuesta = "{\"respuesta\":\"Guardado\"}";

        } catch (Exception ex) {
            ex.printStackTrace();
            respuesta = "{\"respuesta\":\"ERROR\"}";
        } finally {
            try {
                tService.closeAll();
            } catch (Exception e) {
            }
            return respuesta;
        }
    }

    public String existeCondicionComercial(String id_solicitud) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_EXISTE_CONDICION_COMERCIAL";
        String respuesta = "";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, id_solicitud);
            rs = ps.executeQuery();
            while (rs.next()) {
                respuesta = rs.getString("respuesta");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return respuesta;
    }

    @Override
    public String cargarCondionesComerciales(String id_solicitud) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_CONDICIONES_COMERCIALES";
        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, id_solicitud);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }
            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                informacion = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return informacion;
    }

    @Override
    public String prepararFacturacion(JsonObject informacion, Usuario usuario) {
        con = null;
        ps = null;
        rs = null;
        String query = "SQL_PREPARAR_FACTURACION";
        String respuesta = "{}";
        try {
            con = this.conectarJNDI();
            JsonArray arr = informacion.getAsJsonArray("json");
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));
                ps.setString(1, arr.get(0).getAsJsonObject().get("_id_solicitud").getAsString());
                ps.setString(2, arr.get(0).getAsJsonObject().get("aFacturar").getAsString());
                ps.setString(3, arr.get(0).getAsJsonObject().get("valorMaterial").getAsString());
                ps.setString(4, usuario.getLogin());
                ps.setString(5, arr.get(0).getAsJsonObject().get("observaciones").getAsString());
                rs = ps.executeQuery();
                while (rs.next()) {
                    respuesta = "{\"respuesta\":\"" + rs.getString("retorno") + "\"}";
                }
            }
        } catch (Exception e) {

            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return respuesta;
        }
    }

    @Override
    public String cargarAccionesPreparadas(String id_solicitud) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_ACCIONES_PREPARADAS";
        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, id_solicitud);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }
            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            informacion = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                informacion = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return informacion;
    }

    @Override
    public String cargar_Iva_Compensar(String id_solicitud) {
        con = null;
        rs = null;
        ps = null;
        query = "SQL_CARGAR_IVA_COMPENSAR";
        String respuestaJson = "{}";

        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);
            ps.setString(1, id_solicitud);

            rs = ps.executeQuery();

            while (rs.next()) {
                obj.addProperty("porc_iva_compensar", rs.getDouble("porc_iva_compensar"));
            }
            respuestaJson = gson.toJson(obj);

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return respuestaJson;
    }

    @Override
    public String cargar_CIA(String id_solicitud) {
        con = null;
        rs = null;
        ps = null;
        query = "cargar_CIA";
        String respuestaJson = "{}";

        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);
            ps.setString(1, id_solicitud);

            rs = ps.executeQuery();

            while (rs.next()) {
                obj.addProperty("cotos_indirectos", rs.getDouble("cotos_indirectos"));
            }
            respuestaJson = gson.toJson(obj);

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return respuestaJson;
    }

    @Override
    public String clonacionProyectos(String id_solicitud_origen , String id_solicitud_destino ,  Usuario usuario) {
        con = null;
        rs = null;
        ps = null;
        query = "SQL_CLONACION_PROYECTOS";
        String respuestaJson = "{}";

        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);
            ps.setInt(1,Integer.parseInt(id_solicitud_origen));
            ps.setInt(2, Integer.parseInt(id_solicitud_destino));
            ps.setString(3, usuario.getLogin());

            rs = ps.executeQuery();

            while (rs.next()) {
                obj.addProperty("respuesta", rs.getString("respuesta"));
            }
            respuestaJson = gson.toJson(obj);

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return respuestaJson;
    }

    @Override
    public String trazabilidadOfertafin(String idsolicitud, Usuario usuario, String estado_actual, String estado) {
        StringStatement st = null;
        String respuesta = "";
        String query = "SQL_CAMBIO_ESTADO_ETAPA_OFERTA2";
        try {
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);
            st.setString(1, usuario.getLogin());
            st.setString(2, estado);
            st.setString(3, idsolicitud);
            respuesta = st.getSql();

        } catch (Exception e) {
            e.printStackTrace();
            respuesta = "ERROR";
        }
        return respuesta;
    }

    @Override
    public String generar_Centros_Costos(String idsolicitud, Usuario usuario, Integer id) {
        String respuestaJson = "{}";
        JsonObject oferta = obtener_oferta(idsolicitud);
        if (oferta.get("centro_costos_ingreso").getAsString().equals("")) {
            
            //Ing.William Siado
            if (oferta.get("tipo_proyecto").getAsString().equals("TPR00006")) {
                id = 1;
            }else if(oferta.get("tipo_proyecto").getAsString().equals("TPR00008")){
                id=3;
            }else if(oferta.get("tipo_proyecto").getAsString().equals("TPR00009")){
                id=4;
            }else{
                id=2;
            }
            
            String Centro_Costo = Util.getCentroCostoProy(oferta.get("num_os").getAsString(), id);

            if (Centro_Costo.equals("-100")) {
                respuestaJson = "{\"error\":\" Error generando centro de costos\"}";
            } else {

                String[] x = Centro_Costo.split(";");

                con = null;
                ps = null;
                query = "SQL_INSERTAR_CENTRO_COSTO";
                try {

                    con = this.conectarJNDI(query);
                    query = this.obtenerSQL(query);
                    ps = con.prepareStatement(query);

                    ps.setString(1, x[0]);
                    ps.setString(2, x[1]);
                    ps.setString(3, usuario.getLogin());
                    ps.setString(4, idsolicitud);

                    ps.executeUpdate();
                    respuestaJson = "{\"respuesta\":\"OK\"}";

                } catch (Exception e) {
                    try {
                        respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
                        throw new SQLException("ERROR GUARDANDO CENTROS DE COSTOS \n" + e.getMessage());
                    } catch (SQLException ex) {
                        Logger.getLogger(ProcesosCatalogoImpl.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } finally {
                    insertar_costo_cuentas_control(idsolicitud);
                    Calendar fecha = Calendar.getInstance();
                    int a�o = fecha.get(Calendar.YEAR);
                    int mes = fecha.get(Calendar.MONTH) + 1;
                    Util.LoadApoteosys("COREFINTRA", "1", Integer.toString(a�o), mes, "CUENTA_CONTROL_PROYECTO");
                    try {
                        if (con != null) {
                            this.desconectar(con);
                        }
                        if (ps != null) {
                            ps.close();
                        }
                    } catch (SQLException ex) {
                    }
                    return respuestaJson;
                }
            }
        }

        return respuestaJson;
    }

    public JsonObject obtener_oferta(String id_solicitud) {
        con = null;
        rs = null;
        ps = null;
        query = "SQL_OBTENER_OFERTA";
        JsonObject fila = null;

        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);
            ps.setString(1, id_solicitud);

            rs = ps.executeQuery();

            while (rs.next()) {
                fila = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    fila.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
            }

        } catch (Exception e) {

        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return fila;
    }

    
    public void insertar_costo_cuentas_control(String id_solicitud) {
        con = null;
        rs = null;
        ps = null;
        query = "INSERTAR_COSTO_CUENTAS_CONTROL";
        JsonObject fila = null;

        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);

            ps = con.prepareStatement(query);
            ps.setString(1, id_solicitud);

            rs = ps.executeQuery();

            while (rs.next()) {
                fila = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    fila.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
            }

        } catch (Exception e) {

        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
    }

    @Override
    public String generar_centro_costo_lotes(Usuario usuario , Integer modalidad) {
        con = null;
        ps = null;
        rs = null;
        String query = "SQL_CARGAR_PROYECTOS_GENERAR_CENTROS_COSTOS";
        String respuesta = "";

        try {
            con = this.conectarJNDI();
            if (con != null) {
                ps = con.prepareStatement(this.obtenerSQL(query));
                rs = ps.executeQuery();
                String Centro_Costo = "";
                String[] cc;
                while (rs.next()) {

                    // 1 es especiales
                    //2 es masivos
                    //3 tablerista
                    Centro_Costo = Util.getCentroCostoProy(rs.getString("nombre_proyecto"), modalidad);
                    if (Centro_Costo.equals("-100")) {

                        ps = null;
                        query = "UPDATE tem.sl_centro_costo_lotes set estado='-100' WHERE id_solicitud=?;";

                        if (con == null) {
                            con = this.conectarJNDI(query);
                        }

                        ps = con.prepareStatement(query);
                        ps.setString(1, rs.getString("id_solicitud"));

                        ps.executeUpdate();
                    } else {
                        cc = Centro_Costo.split(";");

                        ps = null;
                        query = "SQL_INSERTAR_CENTRO_COSTO_TABLA_TEMP";

                        if (con == null) {
                            con = this.conectarJNDI(query);
                        }

                        query = this.obtenerSQL(query);
                        ps = con.prepareStatement(query);

                        ps.setString(1, cc[0]);
                        ps.setString(2, cc[1]);
                        ps.setString(3, rs.getString("id_solicitud"));

                        ps.executeUpdate();

                    }

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
                
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return respuesta;
        }
    }

    @Override
    public String cargarCasosAnticipo() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_CASOS_ANTICIPOS";
        String respuesta = "{}";
        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            while (rs.next()) {
                obj.addProperty(rs.getString("id"), rs.getString("caso"));
            }
            respuesta = gson.toJson(obj);
        } catch (Exception ex) {
            respuesta = "{\"error\":\"" + ex.getMessage() + "\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                respuesta = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return respuesta;
    }

    private void setCeldaTotal(int numero_de_fila, double total_esquema, PdfPTable table2) {
        try{
            String total_esquema_formateado = new DecimalFormat("$###,###,###.00").format(total_esquema);
            PdfPCell celda_total= table2.getRow(numero_de_fila).getCells()[7];                    
            celda_total.setPhrase(new Phrase(String.valueOf(total_esquema_formateado), new Font(Font.TIMES_ROMAN, 8, Font.BOLD, Color.WHITE)));
            celda_total.setHorizontalAlignment(Element.ALIGN_RIGHT);
        }catch (ArrayIndexOutOfBoundsException e){
            //pass
            System.err.println("Primer total fuera del index");
        }catch (Exception e){
            System.err.println(e);
        };               
    }

    @Override
    public String merge_Pdf(JsonArray ubicacionPdfs, Usuario usuario,JsonArray notasOferta) {
        String nomarchivo = "";
        String html_doc = "", fecha_corte = "";
        ResourceBundle rb = null;
        String respuestaJson = "{}";
        JsonObject jsonobj;
        String ubicacion;
        
        
        ResourceBundle rsb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
           
        String ruta=rsb.getString("ruta")+"/pdfOferta/";

        
        
        try {
           List<InputStream> list = new ArrayList<InputStream>();
            try {
                for (int i = 0; i < ubicacionPdfs.size(); i++) {
                    jsonobj = ubicacionPdfs.get(i).getAsJsonObject();
                    ubicacion = jsonobj.get("Ruta").getAsString();
                    // PDFs a unir
                    list.add(new FileInputStream(new File(ubicacion)));
                }
                
                
                
               Date currentDate= new Date();
               
               DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");  
               String strDate = dateFormat.format(currentDate);  
               
               
               
               
               String nameFile="Prueba"+strDate;
               
               
               nameFile=nameFile.replace(" ", "-");

               nameFile=nameFile.replace(":", "-");
               
               
               
               this.nameFile=nameFile;

               
               
               this.createDir(ruta);
                
                // PDF final
                OutputStream out = new FileOutputStream(new File(ruta+nameFile+".pdf"));
                
                JsonObject objeto=new JsonObject();
                
                JsonObject objetoNota=null;
                for (int i = 0; i < notasOferta.size(); i++) {
                   
                    
                     objeto = notasOferta.get(i).getAsJsonObject();
            
                        
                     if(objeto.get("id_tipo_de_nota").getAsString()!=null && objeto.get("id_tipo_de_nota").getAsInt()==5){
                         
                         objetoNota=new JsonObject();
                         objetoNota=notasOferta.get(i).getAsJsonObject();
                         
                     }
                }
                
                doMerge(list,objetoNota, out);
            } catch (Exception e) {
               
                e.printStackTrace(System.out);
            }

             respuestaJson = "{\"respuesta\":\"OK\",\"Ruta\":\"" + "/pdfOferta/" + nameFile +".pdf"+ "\"}";

        } catch (Exception e) {
            e.printStackTrace();
            try {
                respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR generar_Pdf_Cotizacion \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(MinutasContratacionImpl.class.getName()).log(Level.SEVERE, null, ex.getNextException());
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }
    
    /**
     * Mezclar m?ltiples archivos PDF en uno
     * 
     * @param list Lista de InputStream de los archivos PDF a unir
     * @param outputStream Stream de archivo de salida del PDF
     * que resulta de la uni?n de los streams de la lista de entrada
     * @throws DocumentException
     * @throws IOException
     */
    public static void doMerge(List<InputStream> list,JsonObject objetoNota, OutputStream outputStream)
            throws DocumentException, IOException {
        //crear un nuevo documento PDF
        Rectangle pageSize = new Rectangle(960f, 530f); //ancho y alto

        Document document = new Document();
        document.setPageSize(pageSize);
//        document.setPageSize(pageSize);
        //document.setPageSize(PageSize.LETTER.rotate());
        //crear un escritor del PDF
        PdfWriter writer = PdfWriter.getInstance(document, outputStream);
        document.open();
        PdfContentByte cb = writer.getDirectContent();
        //para cada PDF en la lista
        //leer su contenido por p?gina e ir agregando
        //cada p?gina en el PDF de la variable document
        
       
        
                
        int countPages=0;
        for (InputStream in : list) {
            PdfReader reader = new PdfReader(in);
            
            for (int i = 1; i <= reader.getNumberOfPages(); i++) {
               countPages=reader.getNumberOfPages();
               
               
               byte bContent [] = reader.getPageContent(i);
               
                ByteArrayOutputStream bs = new ByteArrayOutputStream();
                //write the content to an output stream
                bs.write(bContent);
                
//                if(i<countPages)
                document.newPage();
                //import the page from source pdf
                
                    if(bs.size()>80){
                        
                        
                         PdfImportedPage page = writer.getImportedPage(reader, i);
                           cb.addTemplate(page, 0, 0);
                        
                    }
                
               
                
                //add the   to the destination pdf
                
                
                
              
                
//                 if(i<countPages)
//                document.newPage();

  
            }
        }
        
        
         
         
//        canvas.saveState();
//        canvas.beginText();
//        canvas.moveText(650f, 550f);
//        canvas.setRGBColorFillF(5, 38, 65);
//        canvas.setLineWidth((float)0.5);                   
//        canvas.setFontAndSize(baseFont, 16);
//        canvas.showTextAligned(PdfContentByte.ALIGN_LEFT,asesorHtml.getContent(),150,250,0);
//        canvas.showTextAligned(PdfContentByte.ALIGN_LEFT,"Cargo: Asesor Comercial",150,235,0);

//        canvas.showText("Asesor : Paola Rey");
//        canvas.showText("Cargo : Comercial");
//        canvas.showText("Telefono : 300456729");
//        canvas.showText("Correo : pao@selectrik.co");



//        canvas.endText();
//        canvas.restoreState();
        
        
//        canvas.showTextAligned(Element.ALIGN_LEFT, IMAGE, 200, 300, 0);
               
        
//             OutputStream out = new FileOutputStream(new File("D:\\Desktop\\"+nomarchivo+".pdf"));
                
//                PdfWriter writer = PdfWriter.getInstance(document, out);
      
        //cerrar streams para liberar recursos
        //y cualquier bloqueo de archivo
        outputStream.flush();
        document.close();
        outputStream.close();
    }

    @Override
    public String guardar_notas_oferta(JsonObject informacion, Usuario usuario) {
        JsonObject objeto = new JsonObject();
        StringStatement st = null;
        String respuesta = "";
        TransaccionService tService = null;
        try {
            tService = new TransaccionService(this.getDatabaseName());
            tService.crearStatement();
            JsonArray arr = informacion.getAsJsonArray("json");
            String mensaje = "";
            // op = si es accion : 2 ? num_solictud:1 
            String query = "";
          
                query = "sql_prueba";
                     
                    
                    st = new StringStatement(this.obtenerSQL("SQL_GUARDAR_NOTAS_OFERTA"), true);
                        st.setString(1, informacion.get("tipo_nota").getAsString());
                        st.setString(2, informacion.get("id_solicitud").getAsString());
                        st.setString(3, informacion.get("nota").getAsString());
                        st.setString(4, usuario.getLogin());

                    tService.getSt().addBatch(st.getSql());
                    mensaje = "Guardado";
                
            

            tService.execute();
            respuesta = "{\"respuesta\":\"" + mensaje + "\"}";
            //respuesta = st.getSql();
        } catch (Exception ex) {
            respuesta = "{\"respuesta\":\"ERROR\"}";
            //respuesta = ex.getMessage();
        } finally {
            try {
                tService.closeAll();
            } catch (Exception e) {
            }
            return respuesta;
        }
    }
    
    
    public String actualizarNotaOferta(String idNota, String nota) {
        
        con = null;
        ps = null;

        Statement stmt = null;
        JsonObject respuestaJson = new JsonObject();

        
        String query = "ACTUALIZAR_NOTA";
        StringStatement st = null;

        try {
            PreparedStatement ps = null;
            con = this.conectarJNDI();
            con.setAutoCommit(false);
            
            st = new StringStatement(this.obtenerSQL(query), true);

            st.setString(1, nota);
            st.setString(2, idNota);
              
            stmt = con.createStatement();
            stmt.addBatch(st.getSql());
            
            stmt.executeBatch();
            stmt.clearBatch();
            respuestaJson.addProperty("Respuesta", "OK");

            con.commit();
        } catch (Exception exc) {
            con.rollback();
            exc.getMessage();
        } finally {
            try {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException e) {
                        throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                    }
                }
                if (con != null) {
                    try {
                        con.setAutoCommit(true);
                        this.desconectar(con);
                        respuestaJson.addProperty("Error", "Error  en Actualizacion de la nota");
                    } catch (SQLException e) {
                        throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage());
                    }
                }
            } catch (Exception ex) {
            }
            return respuestaJson.getAsString();
        }
    }

    
    @Override
    public String eliminarNotaOferta(String id) {
        StringStatement st = null;
        String respuesta = "";
        String query = "SQL_ELIMINAR_NOTA_OFERTA";
        try {
            query = this.obtenerSQL(query);
            st = new StringStatement(query, true);
            st.setString(1, id);
            respuesta = st.getSql();
        } catch (Exception ex) {
            respuesta = ex.getMessage();
        } finally {
            return respuesta;
        }
    }

    @Override
    public String cargarComboGenerico(String op, String param) {
         con = null;
        rs = null;
        ps = null;
        String filtro = "";
        switch (op) {
            case "0":
                query = "SQL_SL_TIPO_NEGOCIO";
                break;
            case "1":
                query = "SQL_SL_TIPO_TRABAJO";
                break;
            default:
                throw new AssertionError();
        }

        String respuestaJson = "{}";

        Gson gson = new Gson();
        JsonObject obj = new JsonObject();
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query).replaceAll("#filtro", filtro);

            ps = con.prepareStatement(query);

            rs = ps.executeQuery();

            while (rs.next()) {
                obj.addProperty(rs.getString("id"), rs.getString("descripcion"));

            }
            respuestaJson = gson.toJson(obj);

        } catch (Exception e) {
            respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return respuestaJson;
    }

    static class HeaderFooterCot extends PdfPageEventHelper {

        private Image img;

        public HeaderFooterCot(ResourceBundle rb) throws IOException {
            try {
                String url_logo = "logo_Selectrik2.png";
                img = Image.getInstance(rb.getString("ruta") + "/images/login/" + url_logo);
                img.setAbsolutePosition(410f, 930f);
                img.scalePercent(60);
            } catch (BadElementException | IOException r) {
                System.err.println("Error al leer la imagen");
            }
        }

        @Override
        public void onStartPage(PdfWriter writer, Document document) {
            try {
                document.add(img);
            } catch (DocumentException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
        }
    }

    public String generar_Pdf_Presuesto(String solicitud, String ruta, JsonObject jsonObjCot, JsonArray jsonArrAreas, JsonArray jsonArrCap, Usuario usuario) {
        con = null;
        ps = null;
        rs = null;

        String nomarchivo = "";

        ResourceBundle rb = null;
        String respuestaJson = "{}";
        try {
            DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
            //get current date time with Date()
            Date date = new Date();
            nomarchivo = "cotizacion_" + dateFormat.format(date) + ".pdf";
            //System.out.println(dateFormat.format(date));
            rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            Document document = new Document(PageSize.LEGAL, 85, 85, 90, 52);

            HTMLWorker htmlWorker = new HTMLWorker(document);

            PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(ruta + "/" + nomarchivo));

            
            

            
            
            Font f = new Font(Font.TIMES_ROMAN, 12, Font.NORMAL);
            Font f1 = new Font(Font.TIMES_ROMAN, 12, Font.BOLD);

            Font f2 = new Font(Font.TIMES_ROMAN, 8, Font.NORMAL);
            Font f3 = new Font(Font.TIMES_ROMAN, 8, Font.BOLD);

            Font f4 = new Font(Font.TIMES_ROMAN, 10, Font.NORMAL);

            Font fb = new Font(Font.TIMES_ROMAN, 12, Font.NORMAL, new Color(255, 255, 255));
            Font f1b = new Font(Font.TIMES_ROMAN, 12, Font.BOLD, new Color(255, 255, 255));
            Font f2b = new Font(Font.TIMES_ROMAN, 8, Font.NORMAL, new Color(255, 255, 255));
            Font f3b = new Font(Font.TIMES_ROMAN, 8, Font.BOLD, new Color(255, 255, 255));
            Font f4b = new Font(Font.TIMES_ROMAN, 10, Font.NORMAL, new Color(255, 255, 255));

            HeaderFooterCot header = new HeaderFooterCot(rb);
            pdfWriter.setPageEvent(header);

            document.open();
            document.addAuthor(usuario.getLogin());
            document.addCreator("FINTRA S.A.");
            document.addCreationDate();
            document.addTitle("Ejecucion Selectrik");

            con = this.conectarJNDI();

            PdfPTable table = new PdfPTable(8);
            table.setWidths(new int[]{2, 4, 9, 3, 2, 2, 4, 4});
            table.setWidthPercentage(100);
            PdfPCell cell;
            //ENCABEZADOS DE LA TABLA
            // row 1, cell 1
            cell = new PdfPCell(new Phrase("C?digo", f3b));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBackgroundColor(new Color(255, 145, 0));
            table.addCell(cell);

            // row 1, cell 2
            cell = new PdfPCell(new Phrase("Tipo De Insumo", f3b));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBackgroundColor(new Color(255, 145, 0));
            table.addCell(cell);

            // row 1, cell 3
            cell = new PdfPCell(new Phrase("Descripci?n", f3b));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBackgroundColor(new Color(255, 145, 0));
            table.addCell(cell);

            // row 1, cell 4
            cell = new PdfPCell(new Phrase("Und", f3b));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBackgroundColor(new Color(255, 145, 0));
            table.addCell(cell);

            // row 1, cell 5
            cell = new PdfPCell(new Phrase("Cantidad", f3b));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBackgroundColor(new Color(255, 145, 0));
            table.addCell(cell);

            // row 1, cell 6
            cell = new PdfPCell(new Phrase("Rendimiento", f3b));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBackgroundColor(new Color(255, 145, 0));
            table.addCell(cell);

            // row 1, cell 7
            cell = new PdfPCell(new Phrase("Valor Unitario", f3));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBackgroundColor(new Color(255, 145, 0));
            table.addCell(cell);

            // row 1, cell 8
            cell = new PdfPCell(new Phrase("Valor Total", f3b));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBackgroundColor(new Color(255, 145, 0));
            table.addCell(cell);
            DecimalFormat formato = new DecimalFormat("$###,###,###");
            
            
            
            String totalInversion="";

            query = "obtener_Detalle_Presupuesto";
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            ps.setString(1, solicitud);
            rs = ps.executeQuery();
            int indexArea = 1, indexCap = 1, indexApu = 1, indexInsumo = 1, pindex = 0;
            String id_area = "", area = "", id_capitulo = "", capitulo = "", pIndexApu = "",
                    id_apu = "", apu = "", pIndexinsumo = "";
            double subtotalCot = 0, totalIva = 0, totalAdm = 0, totalImp = 0, totalUtil = 0, totalCot = 0, total = 0;
            while (rs.next()) {
                if (!id_area.equals(rs.getString("id_area"))) {
                    id_area = rs.getString("id_area");
                    area = rs.getString("nombre_area");

                    // row 1, cell 1
                    cell = new PdfPCell(new Phrase(String.valueOf(indexArea), f3b));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setBackgroundColor(new Color(255, 145, 0));
                    table.addCell(cell);
                    // row 1, cell 2
                    cell = new PdfPCell(new Phrase(area, f3b));
                    cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                    cell.setColspan(6);
                    cell.setBackgroundColor(new Color(255, 145, 0));
                    table.addCell(cell);
                    // row 1, cell 6
                    cell = new PdfPCell(new Phrase(formato.format(Double.parseDouble(getValueFromArray(jsonArrAreas, rs.getString("id_area"), "total_area"))), f3b));
                    cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    cell.setBackgroundColor(new Color(255, 145, 0));
                    table.addCell(cell);
                    indexCap = 1;
                    pindex++;
                    indexArea++;
                }
                if (!id_capitulo.equals(rs.getString("id_capitulo"))) {
                    id_capitulo = rs.getString("id_capitulo");
                    capitulo = rs.getString("nombre_capitulo");
                    // row 1, cell 1
                    cell = new PdfPCell(new Phrase(String.valueOf(pindex + "." + indexCap), f2b));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setBackgroundColor(new Color(255, 145, 0));
                    table.addCell(cell);
                    // row 1, cell 2
                    cell = new PdfPCell(new Phrase(capitulo, f2b));
                    cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                    cell.setColspan(6);
                    cell.setBackgroundColor(new Color(255, 145, 0));
                    table.addCell(cell);
                    // row 1, cell 6                   
                    cell = new PdfPCell(new Phrase(formato.format(Double.parseDouble(getValueFromArray(jsonArrCap, rs.getString("id_capitulo"), "total_capitulo"))), f2b));
                    cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    cell.setBackgroundColor(new Color(255, 145, 0));
                    table.addCell(cell);
                    pIndexApu = pindex + "." + indexCap;
                    indexApu = 1;
                    indexCap++;
                }
                if (!id_apu.equals(rs.getString("id_apu"))) {
                    //agregar el valor 

                    id_apu = rs.getString("id_apu");
                    apu = rs.getString("nombre_apu").replace("PROYECTO_", "");
                    // row 1, cell 1
                    cell = new PdfPCell(new Phrase(String.valueOf(pIndexApu + "." + indexApu), f2b));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setBackgroundColor(new Color(255, 145, 0));
                    table.addCell(cell);
                    // row 1, cell 3
                    cell = new PdfPCell(new Phrase(apu, f2b));
                    cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                    cell.setColspan(2);
                    cell.setBackgroundColor(new Color(255, 145, 0));
                    table.addCell(cell);

                    JsonObject obj = obtenerTotalesApusPresupuesto(solicitud, id_capitulo, id_apu);

                    // row 1, cell 1
                    cell = new PdfPCell(new Phrase(obj.get("unidad_medida_apu").getAsString(), f2b));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setBackgroundColor(new Color(255, 145, 0));
                    table.addCell(cell);

                    // row 1, cell 1
                    cell = new PdfPCell(new Phrase(obj.get("cantidad_apu").getAsString(), f2b));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setColspan(2);
                    cell.setBackgroundColor(new Color(255, 145, 0));
                    table.addCell(cell);

                    // row 1, cell 1
                    cell = new PdfPCell(new Phrase(formato.format(Double.parseDouble(obj.get("subtotal").getAsString())), f2b));
                    cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    cell.setBackgroundColor(new Color(255, 145, 0));
                    table.addCell(cell);

                    // row 1, cell 1
                    cell = new PdfPCell(new Phrase(formato.format(Double.parseDouble(obj.get("total").getAsString())), f2b));
                    cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                    cell.setBackgroundColor(new Color(255, 145, 0));
                    table.addCell(cell);
                    
                    
                    
                    totalInversion=obj.get("total").getAsString();

                    pIndexinsumo = pIndexApu + "." + indexApu;
                    indexInsumo = 1;
                    indexApu++;
                }
                String tipo_insumo = rs.getString("tipo_insumo");
                String descripcion_insumo = rs.getString("descripcion_insumo");
                String unidad_medida_insumo = rs.getString("nombre_unidad_insumo");
                String cantidad_insumo = rs.getString("cantidad_insumo");
                String rendimiento_insumo = rs.getString("rendimiento_insumo");
                double valor_unitario = rs.getDouble("valor_unitario");
                double valor_esquema = rs.getDouble("total");
                total = total + valor_esquema;

                // row 1, cell 1
                cell = new PdfPCell(new Phrase(pIndexinsumo + "." + indexInsumo, f2));
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(cell);
                // row 1, cell 2
                cell = new PdfPCell(new Phrase(tipo_insumo, f2));
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(cell);
                // row 1, cell 3
                cell = new PdfPCell(new Phrase(descripcion_insumo, f2));
                cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                table.addCell(cell);
                // row 1, cell 4
                cell = new PdfPCell(new Phrase(unidad_medida_insumo, f2));
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(cell);
                // row 1, cell 5
                cell = new PdfPCell(new Phrase(cantidad_insumo, f2));
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(cell);
                // row 1, cell 6
                cell = new PdfPCell(new Phrase(rendimiento_insumo, f2));
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(cell);
                // row 1, cell 7
                cell = new PdfPCell(new Phrase(formato.format(valor_unitario), f2));
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                table.addCell(cell);
                // row 1, cell 8
                cell = new PdfPCell(new Phrase(formato.format(valor_esquema), f2));
                cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
                table.addCell(cell);
                indexInsumo++;
            }
          
//          Paragraph parrafo = new Paragraph("La oferta tiene un valor de COP$"+totalInversion+", IVA incluido, para mayor detalle ver el siguiente cuadro de precio:");             
 
//          document.add(porcionTexto);
            document.add(table);

            // step 5
            document.close();

            respuestaJson = "{\"respuesta\":\"OK\",\"Ruta\":\"" + "/images/multiservicios/" + usuario.getLogin() + "/" + nomarchivo + "\"}";

        } catch (Exception e) {
            try {
                respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR generar_Pdf_Cotizacion \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(MinutasContratacionImpl.class.getName()).log(Level.SEVERE, null, ex.getNextException());
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
                ex.printStackTrace();
            }
            return respuestaJson;
        }
    }

    @Override
    public String cargarReporteAnticipos(BeansMultiservicio informacion) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_CARGAR_REPORTE_ANTICIPO";
        JsonArray lista = null;
        JsonObject objetoJson = null;
        String datos = "{}";
        String parametro = "";
        String consulta = "";
        try {
            con = this.conectarJNDI();

            if (!informacion.getParametro1().equals("")) {
                parametro = parametro + " and ant.id_solicitud = '" + informacion.getParametro1() + "'";
            }
            if (!informacion.getParametro2().equals("")) {
                parametro = " and num_os ilike '%'|| '" + parametro + informacion.getParametro2() + "' ||'%' ";
            }
            if (!informacion.getParametro4().equals("")) {
                parametro = "and cod_anticipo ='" + informacion.getParametro4() + "'";
            }
            consulta = this.obtenerSQL(query).replaceAll("#parametro", parametro);
            ps = con.prepareStatement(consulta);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }
            datos = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
            datos = "{\"error\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                datos = "{\"error\":\"" + e.getMessage() + "\"}";
            }
        }
        return datos;
    }
    
}
