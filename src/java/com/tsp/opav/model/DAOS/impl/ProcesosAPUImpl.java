/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.opav.model.DAOS.impl;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.tsp.opav.model.DAOS.MainDAO;
import com.tsp.opav.model.DAOS.ProcesosAPUDAO;
import com.tsp.operation.model.beans.StringStatement;
import com.tsp.operation.model.beans.Usuario;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author user
 */
public class ProcesosAPUImpl extends MainDAO implements ProcesosAPUDAO {

    private Connection con = null;
    private PreparedStatement ps = null;
    private ResultSet rs = null;
    private String query = "";
    private StringStatement st = null;
    private Gson gson = new Gson();
    private JsonObject obj;

    public ProcesosAPUImpl(String dataBaseName) {
        super("ProcesosAPUDAO.xml", dataBaseName);

    }

    public JsonObject cargar_combo_grupo(JsonObject info) {
        JsonObject respuesta = new JsonObject();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "";
        try {
            query = info.get("query").getAsJsonPrimitive().getAsString();
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            if (info.get("filtros") != null) {
                JsonArray ar = info.get("filtros").getAsJsonArray();
                for (int i = 0; i < ar.size(); i++) {
                    ps.setString(i + 1, ar.get(i).getAsString());
                }
            }
            rs = ps.executeQuery();
            //respuesta.addProperty("0", "Todos");
            while (rs.next()) {
                respuesta.addProperty(rs.getString("codigo"), rs.getString("nombre"));
            }
        } catch (Exception exc) {
            respuesta = new JsonObject();
            respuesta.addProperty("error", exc.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception ex) {
            }
            return respuesta;
        }
    }

    @Override
    public JsonObject cargar_combo(JsonObject info) {
        JsonObject respuesta = new JsonObject();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "";
        try {
            query = info.get("query").getAsJsonPrimitive().getAsString();
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            if (info.get("filtros") != null) {
                JsonArray ar = info.get("filtros").getAsJsonArray();
                for (int i = 0; i < ar.size(); i++) {
                    ps.setString(i + 1, ar.get(i).getAsString());
                }
            }
            rs = ps.executeQuery();
            respuesta.addProperty("0", "...");
            while (rs.next()) {
                respuesta.addProperty(rs.getString("codigo"), rs.getString("nombre"));
            }
        } catch (Exception exc) {
            respuesta = new JsonObject();
            respuesta.addProperty("error", exc.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception ex) {
            }
            return respuesta;
        }
    }

    @Override
    public boolean existeGrupoApu(String empresa, String nombre) {
        con = null;
        rs = null;
        ps = null;
        boolean resp = false;
        query = "existeGrupoApu";
        String filtro = "";
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query).replaceAll("#filtro", filtro);;

            ps = con.prepareStatement(query);

            byte[] bytes = nombre.getBytes("ISO-8859-1");
            nombre = new String(bytes, "UTF-8");

            ps.setString(1, empresa);
            ps.setString(2, nombre);

            rs = ps.executeQuery();

            if (rs.next()) {
                resp = true;
            }
        } catch (Exception e) {
            try {
                throw new Exception("Error en existeGrupoApu " + e.toString());
            } catch (Exception ex) {
                Logger.getLogger(ProcesosAPUImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return resp;
    }

    @Override
    public String guardarGrupoApu(String empresa, String nombre, String descripcion, String usuario) {
        con = null;
        ps = null;
        query = "guardarGrupoApu";
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);

            byte[] bytes = nombre.getBytes("ISO-8859-1");
            nombre = new String(bytes, "UTF-8");

            byte[] bytes1 = descripcion.getBytes("ISO-8859-1");
            descripcion = new String(bytes1, "UTF-8");

            ps.setString(1, empresa);
            ps.setString(2, nombre);
            ps.setString(3, descripcion);
            ps.setString(4, usuario);

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            try {
                respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR guardarGrupoApu \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(ProcesosAPUImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
            }
            return respuestaJson;
        }
    }

    @Override
    public String cargarInsumosXTipo(String id) {
        try {
            obj = new JsonObject();
            query = "CargaInsumos";
            con = this.conectarJNDI(query);
            String filtro = " where a.reg_status!='A' and c.id_tipo_insumo= " + id;
            ps = con.prepareStatement(this.obtenerSQL(query).replaceAll("#filtro", filtro));

            rs = ps.executeQuery();

            JsonObject fila = new JsonObject();

            while (rs.next()) {
                fila.addProperty(rs.getString("codigo"), rs.getString("nombre"));
            }
            obj = fila;

        } catch (Exception ex) {
            throw new SQLException("ERROR OBTENIENDO INSUMOS: cargarInsumosXTipo(String id)   " + ex.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception ex) {
            }
            return gson.toJson(obj);
        }
    }

    @Override
    public String cargarTiposMaterial() {
        try {
            obj = new JsonObject();
            query = "ConsultaTiposInsumos";
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();

            JsonObject fila = new JsonObject();
            while (rs.next()) {
                fila.addProperty(rs.getString("codigo"), rs.getString("nombre"));
            }
            obj = fila;

        } catch (Exception ex) {
            throw new SQLException("ERROR OBTENIENDO TIPOS DE MATERIAL: cargarTiposMaterial()  " + ex.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception ex) {
            }
            return gson.toJson(obj);
        }
    }

    @Override
    public JsonObject cargarInsumosXFiltro(String idsubcategoria) {
        JsonObject respuesta = new JsonObject(), fila;
        JsonArray arr = new JsonArray();
        JsonObject object = new JsonObject();;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "ConsultarInsumoXFiltro";

        try {
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setInt(1, Integer.parseInt(idsubcategoria));
            rs = ps.executeQuery();

            while (rs.next()) {
                object = new JsonObject();
                object.addProperty("id", rs.getString("id"));
                object.addProperty("nombre", rs.getString("nombre"));
                arr.add(object);
            }

            System.out.println(new Gson().toJson(arr));

            respuesta.add("rows", arr);
        } catch (Exception exc) {
            respuesta = new JsonObject();
            respuesta.addProperty("error", exc.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception ex) {
            }
            return respuesta;
        }
    }

    @Override
    public JsonObject cargarGridInsumosEdit(String ids) {
        JsonObject respuesta = new JsonObject(), fila;
        JsonArray arr = new JsonArray();
        JsonObject object = new JsonObject();;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "ConsultarGridInsumoEdit";

        try {
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setInt(1, Integer.parseInt(ids));
            rs = ps.executeQuery();

            while (rs.next()) {
                object = new JsonObject();
                object.addProperty("id", rs.getString("id"));
                object.addProperty("tipoinsumo", rs.getString("tipoinsumo"));
                object.addProperty("idtipoinsumo", rs.getString("idtipoinsumo"));
                object.addProperty("descripcionins", rs.getString("descripcionins"));
                object.addProperty("iddescripcionins", rs.getString("iddescripcionins"));
                object.addProperty("unidadmedida", rs.getString("unidadmedida"));
                object.addProperty("idunidadmedida", rs.getString("idunidadmedida"));
                object.addProperty("cantidad", rs.getString("cantidad"));
                object.addProperty("rendimiento", rs.getString("rendimiento"));
                arr.add(object);
            }

            System.out.println(new Gson().toJson(arr));

            respuesta.add("rows", arr);
        } catch (Exception exc) {
            respuesta = new JsonObject();
            respuesta.addProperty("error", exc.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception ex) {
            }
            return respuesta;
        }
    }

    @Override
    public JsonObject cargarGridMateriales(String ids, String tipo) {
        JsonObject respuesta = new JsonObject(), fila;
        JsonArray arr = new JsonArray();
        JsonObject object = new JsonObject();;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "ConsultarGridMateriales";

        try {
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setInt(1, Integer.parseInt(ids));
            ps.setInt(2, Integer.parseInt(tipo));
            rs = ps.executeQuery();

            while (rs.next()) {
                object = new JsonObject();
                object.addProperty("id", rs.getString("id"));
                object.addProperty("nombre", rs.getString("descripcion"));
                object.addProperty("unidadmedida", rs.getString("nombre_unidad"));
                object.addProperty("cantidad", rs.getString("cantidad"));
                object.addProperty("rendimiento", rs.getString("rendimiento"));
                arr.add(object);
            }

            System.out.println(new Gson().toJson(arr));

            respuesta.add("rows", arr);
        } catch (Exception exc) {
            respuesta = new JsonObject();
            respuesta.addProperty("error", exc.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception ex) {
            }
            return respuesta;
        }
    }

    @Override
    public JsonObject guardarAPU(JsonObject info) {

        JsonObject respuesta = new JsonObject();
        con = null;
        ps = null;
        String mensaje = "";
        int resp = 0, idCab = 0;
        Statement stmt = null;
        //query = "GuardarEncabezadoInsumo";

        try {

            JsonArray arr = info.getAsJsonArray("rows");
            PreparedStatement ps = null;
            con = this.conectarJNDI();

            if (!verificaExisteAPU(info.get("nomapu").getAsString())) {

                String query = "InsertarEncabezadoAPU";
                con.setAutoCommit(false);

                ps = con.prepareStatement(this.obtenerSQL(query), Statement.RETURN_GENERATED_KEYS);

                ps.setString(1, info.get("dstrct").getAsString());
                ps.setString(2, info.get("nomapu").getAsString());
                ps.setInt(3, info.get("unidadm").getAsInt());
                ps.setString(4, info.get("nit_propietario").getAsString());
                ps.setString(5, info.get("usuario").getAsString());

                resp = ps.executeUpdate();
                stmt = con.createStatement();
                if (resp > 0) {
                    ResultSet rs = ps.getGeneratedKeys();
                    if (rs.next()) {
                        idCab = rs.getInt(1);

                        for (int i = 0; i < arr.size(); i++) {
                            respuesta = arr.get(i).getAsJsonObject();

                            if (respuesta.get("id").getAsString().startsWith("neo_")) {

                                stmt.addBatch(insertarAPUDetalle(idCab, respuesta, info.get("usuario").getAsString(), info.get("dstrct").getAsString()));

                            }
                        }
                        stmt.addBatch("insert into opav.sl_rel_grupo_apu(reg_status, dstrct, id_apu, id_grupo_apu, creation_user)\n"
                                + " values('', '" + info.get("dstrct").getAsString() + "', '" + idCab + "', '" + info.get("grupo_apu").getAsString() + "', '" + info.get("usuario").getAsString() + "');");
                        stmt.executeBatch();
                        stmt.clearBatch();
                        mensaje = "APU creado exitosamente...";
                    }
                }

                con.commit();

            } else {
                mensaje = "El APU ya se encuentra creado...";
            }

            respuesta = new JsonObject();

            respuesta.addProperty("mensaje", mensaje);

        } catch (Exception exc) {
            con.rollback();
            respuesta = new JsonObject();
            exc.getMessage();
            respuesta.addProperty("error", "Error, no puede registrar productos ya guardados");

        } finally {
            try {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException e) {
                        throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                    }
                }
                if (con != null) {
                    try {
                        con.setAutoCommit(true);
                        this.desconectar(con);
                    } catch (SQLException e) {
                        throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage());
                    }
                }
            } catch (Exception ex) {
            }
            return respuesta;
        }
    }

    @Override
    public JsonObject guardarAPUEdit(JsonObject info) {

        JsonObject respuesta = new JsonObject();
        con = null;
        ps = null;
        String mensaje = "";
        Statement stmt = null;
        //query = "GuardarEncabezadoInsumo";

        try {

            JsonArray arr = info.getAsJsonArray("rows");
            PreparedStatement ps = null;
            con = this.conectarJNDI();

            String query = "UpdateEncabezadoAPU";
            con.setAutoCommit(false);

            ps = con.prepareStatement(this.obtenerSQL(query));

            ps.setString(1, info.get("nomapu").getAsString());
            ps.setString(2, info.get("usuario").getAsString());
            ps.setInt(3, info.get("ids").getAsInt());

            ps.executeUpdate();
            stmt = con.createStatement();

            stmt.addBatch("delete from opav.sl_apu_det where id_apu=" + info.get("ids").getAsInt() + ";");

            for (int i = 0; i < arr.size(); i++) {
                respuesta = arr.get(i).getAsJsonObject();

                stmt.addBatch(insertarAPUDetalle(info.get("ids").getAsInt(), respuesta, info.get("usuario").getAsString(), info.get("dstrct").getAsString()));

            }
            /*stmt.addBatch("delete from opav.sl_rel_grupo_apu where id_apu=" + info.get("ids").getAsInt() + ";");
             stmt.addBatch("insert into opav.sl_rel_grupo_apu(reg_status, dstrct, id_apu, id_grupo_apu, creation_user)\n"
             + " values('', '" + info.get("dstrct").getAsString() + "', '" + info.get("ids").getAsInt() + "', '" + info.get("grupo_apu").getAsString() + "', '" + info.get("usuario").getAsString() + "');");*/
            stmt.executeBatch();
            stmt.clearBatch();
            mensaje = "APU actualizado exitosamente...";

            con.commit();

            respuesta = new JsonObject();

            respuesta.addProperty("mensaje", mensaje);

        } catch (Exception exc) {
            con.rollback();
            respuesta = new JsonObject();
            exc.getMessage();
            respuesta.addProperty("error", "Error");

        } finally {
            try {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException e) {
                        throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                    }
                }
                if (con != null) {
                    try {
                        con.setAutoCommit(true);
                        this.desconectar(con);
                    } catch (SQLException e) {
                        throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage());
                    }
                }
            } catch (Exception ex) {
            }
            return respuesta;
        }
    }

    public boolean verificaExisteAPU(String concat) throws SQLException {
        int resp = 0;
        boolean ret = false;
        Connection con1 = null;
        PreparedStatement ps1 = null;
        ResultSet rs1 = null;
        String query1 = "";

        try {
            con1 = this.conectarJNDI("ExisteAPU");
            query1 = this.obtenerSQL("ExisteAPU");
            ps1 = con1.prepareStatement(query1);

            ps1.setString(1, concat);

            rs1 = ps1.executeQuery();

            while (rs1.next()) {
                resp = rs1.getInt("no");
            }

            if (resp > 0) {

                ret = true;

            }

        } catch (SQLException ex) {
            Logger.getLogger(ProcesosCatalogoImpl.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (con1 != null) {
                this.desconectar(con1);
            }
            if (ps1 != null) {
                ps1.close();
            }
            if (rs1 != null) {
                rs1.close();
            }
        }

        return ret;
    }

    public String insertarAPUDetalle(int idCab, JsonObject respuesta, String usuario, String empresa) throws SQLException {

        String cadena = "";
        String query = "InsertarDetalleAPU";
        String sql = "";
        StringStatement st = null;
        sql = this.obtenerSQL(query);
        st = new StringStatement(sql, true);

        try {

            st.setString(1, empresa);
            st.setInt(2, idCab);
            st.setInt(3, respuesta.get("iddescripcionins").getAsInt());
            st.setInt(4, respuesta.get("idunidadmedida").getAsInt());
            st.setInt(5, respuesta.get("idtipoinsumo").getAsInt());
            st.setString(6, respuesta.get("cantidad").getAsString());
            st.setString(7, respuesta.get("rendimiento").getAsString());
            st.setString(8, usuario);
            cadena += st.getSql();

        } catch (Exception e) {
            System.out.println("Error en SQL_INSERTAR_DETALLE_RECAUDO (insertarRecaudosDetalle)" + e.toString());
            e.printStackTrace();
        }
        return cadena;

    }

    public JsonObject cargarAPUXGrupo(String grupo_apu) {
        JsonObject respuesta = new JsonObject();
        JsonArray arr = new JsonArray();
        JsonObject object = new JsonObject();;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql = "";
        String query = "ConsultarAPUXGrupo";

        try {
            con = this.conectarJNDI(query);
            sql = this.obtenerSQL(query);
            if (grupo_apu.equals("0")) {
                sql = sql.replace("#filtro", "");
            } else {
                sql = sql.replace("#filtro", " and a.id_grupo_apu = " + grupo_apu);
            }

            ps = con.prepareStatement(sql);

            rs = ps.executeQuery();

            while (rs.next()) {
                object = new JsonObject();
                object.addProperty("id", rs.getString("id"));
                object.addProperty("nombre", rs.getString("nombre"));
                object.addProperty("idgrupo", rs.getString("id_grupo_apu"));
                object.addProperty("idunidad", rs.getString("id_unidad_medida"));
                arr.add(object);
            }

            System.out.println(new Gson().toJson(arr));

            respuesta.add("rows", arr);
        } catch (Exception exc) {
            respuesta = new JsonObject();
            respuesta.addProperty("error", exc.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception ex) {
            }
            return respuesta;
        }
    }

    @Override
    public boolean existeUnidadMedida(String empresa, String nombre) {
        con = null;
        rs = null;
        ps = null;
        boolean resp = false;
        query = "existeUnidadMedida";
        String filtro = "";
        try {
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query).replaceAll("#filtro", filtro);;

            ps = con.prepareStatement(query);

            byte[] bytes = nombre.getBytes("ISO-8859-1");
            nombre = new String(bytes, "UTF-8");

            ps.setString(1, empresa);
            ps.setString(2, nombre);

            rs = ps.executeQuery();

            if (rs.next()) {
                resp = true;
            }
        } catch (Exception e) {
            try {
                throw new Exception("Error en existeGrupoApu " + e.toString());
            } catch (Exception ex) {
                Logger.getLogger(ProcesosAPUImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException ex) {
            }
        }
        return resp;
    }

    @Override
    public String guardarUnidadMedida(String empresa, String nombre, String usuario) {
        con = null;
        ps = null;
        query = "guardarUnidadMedida";
        int resp = 0, idCab = 0;
        String respuestaJson = "{}";

        try {
            con = this.conectarJNDI(query);
            //query = this.obtenerSQL(query);
            //ps = con.prepareStatement(query);
            ps = con.prepareStatement(this.obtenerSQL(query), Statement.RETURN_GENERATED_KEYS);

            byte[] bytes = nombre.getBytes("ISO-8859-1");
            nombre = new String(bytes, "UTF-8");

            ps.setString(1, empresa);
            ps.setString(2, nombre);
            ps.setString(3, usuario);

            resp = ps.executeUpdate();

            if (resp > 0) {
                ResultSet rs = ps.getGeneratedKeys();
                if (rs.next()) {
                    idCab = rs.getInt(1);
                }
            }

            respuestaJson = "{\"respuesta\":\"OK\",\"idCab\":\"" + idCab + "\"}";

        } catch (Exception e) {
            try {
                respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR guardarGrupoApu \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(ProcesosAPUImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
            }
            return respuestaJson;
        }
    }

    @Override
    public JsonObject cargarGridSolicitudes(String nit_proveedor) {
        JsonObject respuesta = new JsonObject();
        JsonArray arr = new JsonArray();
        JsonObject object = new JsonObject();;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "ConsultaSolicitudes";

        try {
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            //ps.setString(1, nit_proveedor);
            ps.setString(1, nit_proveedor);
            rs = ps.executeQuery();

            while (rs.next()) {
                object = new JsonObject();
                object.addProperty("id", rs.getString("id_solicitud"));
                object.addProperty("descripcion", rs.getString("descripcion"));
                object.addProperty("id_cliente", rs.getString("id_cliente"));
                object.addProperty("nic", rs.getString("nic"));
                object.addProperty("nomcli", rs.getString("nomcli"));
                object.addProperty("tipo", rs.getString("tipo"));
                arr.add(object);
            }

            System.out.println(new Gson().toJson(arr));

            respuesta.add("rows", arr);
        } catch (Exception exc) {
            respuesta = new JsonObject();
            respuesta.addProperty("error", exc.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception ex) {
            }
            return respuesta;
        }
    }

    @Override
    public JsonObject cargarSubGridSolicitudes(String nit_proveedor, String id) {
        JsonObject respuesta = new JsonObject();
        JsonArray arr = new JsonArray();
        JsonObject object = new JsonObject();;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "ConsultaSolicitudesSub";

        try {
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            //ps.setString(1, nit_proveedor);
            ps.setString(1, nit_proveedor);
            ps.setString(2, id);
            rs = ps.executeQuery();

            while (rs.next()) {
                object = new JsonObject();
                object.addProperty("id", rs.getString("id_accion"));
                object.addProperty("descripcion", rs.getString("descripcion"));
                arr.add(object);
            }

            System.out.println(new Gson().toJson(arr));

            respuesta.add("rows", arr);
        } catch (Exception exc) {
            respuesta = new JsonObject();
            respuesta.addProperty("error", exc.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception ex) {
            }
            return respuesta;
        }
    }

    @Override
    public JsonObject cargarGridApuSolicitudes(String id) {
        JsonObject respuesta = new JsonObject();
        JsonArray arr = new JsonArray();
        JsonObject object = new JsonObject();;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "ConsultaApuAcciones";

        try {
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            //ps.setString(1, nit_proveedor);
            ps.setString(1, id);
            rs = ps.executeQuery();

            while (rs.next()) {
                object = new JsonObject();
                object.addProperty("id", rs.getString("id"));
                object.addProperty("id_accion", rs.getString("id_accion"));
                object.addProperty("apu_nombre", rs.getString("nombre"));
                arr.add(object);
            }

            System.out.println(new Gson().toJson(arr));

            respuesta.add("rows", arr);
        } catch (Exception exc) {
            respuesta = new JsonObject();
            respuesta.addProperty("error", exc.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception ex) {
            }
            return respuesta;
        }
    }

    @Override
    public JsonObject cargarSubGridAPUSolicitudes(String id_accion) {
        JsonObject respuesta = new JsonObject();
        JsonArray arr = new JsonArray();
        JsonObject object = new JsonObject();;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "ConsultaSubGridApuAcciones";

        try {
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            //ps.setString(1, nit_proveedor);
            ps.setString(1, id_accion);
            rs = ps.executeQuery();

            while (rs.next()) {
                object = new JsonObject();
                object.addProperty("nominsumo", rs.getString("nombre_insumo"));
                object.addProperty("descripcion", rs.getString("descripcion"));
                arr.add(object);
            }

            System.out.println(new Gson().toJson(arr));

            respuesta.add("rows", arr);
        } catch (Exception exc) {
            respuesta = new JsonObject();
            respuesta.addProperty("error", exc.getMessage());
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception ex) {
            }
            return respuesta;
        }
    }

    @Override
    public JsonObject clonar_apu(String id_apu, Usuario usuario) {

        JsonObject respuesta = new JsonObject();
        con = null;
        ps = null;
        String mensaje = "";
        int resp = 0, idCab = 0;
        Statement stmt = null;

        try {

            PreparedStatement ps = null;
            con = this.conectarJNDI();

            String query = "SQL_CLONAR_APU_CABECERA";
            con.setAutoCommit(false);

            ps = con.prepareStatement(this.obtenerSQL(query), Statement.RETURN_GENERATED_KEYS);

            ps.setString(1, usuario.getLogin());
            ps.setInt(2, Integer.parseInt(id_apu));

            resp = ps.executeUpdate();
            stmt = con.createStatement();
            if (resp > 0) {
                ResultSet rs = ps.getGeneratedKeys();
                if (rs.next()) {
                    idCab = rs.getInt(1);

                    query = this.obtenerSQL("SQL_CLONAR_APU_CABECERA_DETALLE");
                    st = new StringStatement(query, true);

                    st.setInt(1, idCab);
                    st.setString(2, usuario.getLogin());
                    st.setInt(3, Integer.parseInt(id_apu));

                    stmt.addBatch(st.getSql());

                    query = this.obtenerSQL("SQL_CLONAR_GRUPO_APU");
                    st = new StringStatement(query, true);

                    st.setInt(1, idCab);
                    st.setString(2, usuario.getLogin());
                    st.setInt(3, Integer.parseInt(id_apu));
                    
                    stmt.addBatch(st.getSql());

                    stmt.executeBatch();
                    stmt.clearBatch();
                    mensaje = "APU Clonado exitosamente...";
                }
            }

            con.commit();

            respuesta = new JsonObject();

            respuesta.addProperty("mensaje", mensaje);
            respuesta.addProperty("respuesta", "OK");
            

        } catch (Exception exc) {
            con.rollback();
            respuesta = new JsonObject();
            exc.getMessage();
            respuesta.addProperty("error", "Error, no puede registrar productos ya guardados");

        } finally {
            try {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException e) {
                        throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                    }
                }
                if (con != null) {
                    try {
                        con.setAutoCommit(true);
                        this.desconectar(con);
                    } catch (SQLException e) {
                        throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage());
                    }
                }
            } catch (Exception ex) {
            }
            return respuesta;
        }

    }

    @Override
    public JsonObject clonar_apu2(String id_apu, String id_actividad_capitulo, Usuario usuario) {
        
        JsonObject respuesta = new JsonObject();
        con = null;
        ps = null;
        String mensaje = "";
        int resp = 0, idCab = 0;
        Statement stmt = null;

        try {

            PreparedStatement ps = null;
            con = this.conectarJNDI();

            String query = "SQL_CLONAR_APU_CABECERA2";
            con.setAutoCommit(false);

            ps = con.prepareStatement(this.obtenerSQL(query), Statement.RETURN_GENERATED_KEYS);

            ps.setString(1, usuario.getLogin());
            ps.setInt(2, Integer.parseInt(id_apu));

            resp = ps.executeUpdate();
            stmt = con.createStatement();
            if (resp > 0) {
                ResultSet rs = ps.getGeneratedKeys();
                if (rs.next()) {
                    idCab = rs.getInt(1);

                    query = this.obtenerSQL("SQL_CLONAR_APU_CABECERA_DETALLE");
                    st = new StringStatement(query, true);

                    st.setInt(1, idCab);
                    st.setString(2, usuario.getLogin());
                    st.setInt(3, Integer.parseInt(id_apu));

                    stmt.addBatch(st.getSql());

                    query = this.obtenerSQL("SQL_CLONAR_GRUPO_APU");
                    st = new StringStatement(query, true);

                    st.setInt(1, idCab);
                    st.setString(2, usuario.getLogin());
                    st.setInt(3, Integer.parseInt(id_apu));
                    
                    stmt.addBatch(st.getSql());

                    query = this.obtenerSQL("SQL_ASOCIAR_APU_CLONADO");
                    st = new StringStatement(query, true);

                    st.setInt(1, idCab);
                    st.setString(2, usuario.getLogin());
                    st.setInt(3, Integer.parseInt(id_actividad_capitulo));
                    st.setInt(4, Integer.parseInt(id_apu));
                    
                    stmt.addBatch(st.getSql());

                    stmt.executeBatch();
                    stmt.clearBatch();
                    mensaje = "APU Clonado exitosamente...";
                }
            }

            con.commit();

            respuesta = new JsonObject();

            respuesta.addProperty("mensaje", mensaje);
            respuesta.addProperty("respuesta", "OK");
            

        } catch (Exception exc) {
            con.rollback();
            respuesta = new JsonObject();
            exc.getMessage();
            respuesta.addProperty("error", "Error, no puede registrar productos ya guardados");

        } finally {
            try {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException e) {
                        throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                    }
                }
                if (con != null) {
                    try {
                        con.setAutoCommit(true);
                        this.desconectar(con);
                    } catch (SQLException e) {
                        throw new SQLException("ERROR DESCONECTANDO DE LA BD " + e.getMessage());
                    }
                }
            } catch (Exception ex) {
            }
            return respuesta;
        }
    }

}
