/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.opav.model.DAOS.impl;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.tsp.opav.model.DAOS.AceptacionesConsolidadoDAO;
import com.tsp.opav.model.DAOS.MainDAO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 *
 * @author jpacosta
 */
public class AceptacionesConsolidadoImpl extends MainDAO implements AceptacionesConsolidadoDAO {

    private Connection con = null;
    private PreparedStatement ps = null;
    private ResultSet rs = null;
    private String query = "";
    
    public AceptacionesConsolidadoImpl() {
        super("AceptacionesConsolidadoDAO.xml");
    }
    
    @Override
    public String buscar(String periodo, String tipos, String estados, String interventores, String responsables) {
        String json = "{}";
        Gson gson = new Gson();
        JsonObject datos = new JsonObject(), fila, auxiliar;
        try {
            String name = "buscar_consolidado";
            con = this.conectarJNDI(name);
            //---
            query = this.obtenerSQL(name);
            
            query = query.replace("--periodo", (!periodo.equalsIgnoreCase("")
                  ?"WHERE mes_aprobacion BETWEEN "+periodo:""));
            
            query = query.replace("--tipos", (!tipos.equalsIgnoreCase("'0'")
                  ?"AND ofe.tipo_solicitud IN ("+tipos+")":""));
            
            query = query.replace("--estados", (!estados.equalsIgnoreCase("'0'")
                  ?"AND ac2.estado IN ("+estados+")":""));
            
            query = query.replace("--interventores", (!interventores.equalsIgnoreCase("'0'")
                  ?"AND ofe.interventor IN ("+interventores+")":""));
            
            query = query.replace("--responsables", (!responsables.equalsIgnoreCase("'0'")
                  ?"AND ofe.responsable IN ("+responsables+")":""));
            
            //---
            ps = con.prepareStatement(query);
            rs = ps.executeQuery();
            while (rs.next()) {
                try {
                    if(null == datos.getAsJsonObject(rs.getString("mes_aprobacion"))) {
                        datos.add(rs.getString("mes_aprobacion"), new JsonObject());
                        datos.getAsJsonObject(rs.getString("mes_aprobacion")).add("valores", new JsonArray());
                        
                        auxiliar = new JsonObject();
                        auxiliar.addProperty("dias_ejecucion", 0);
                        auxiliar.addProperty("cant_trabajos", 0);
                        auxiliar.addProperty("promedio_dias", 0);
                        auxiliar.addProperty("costo_contratista", 0);
                        auxiliar.addProperty("precio_venta", 0);
                        datos.getAsJsonObject(rs.getString("mes_aprobacion")).add("totales", auxiliar);
                    }
                } catch (Exception nw) { }
                //
                fila = new JsonObject();
                fila.addProperty("sector_cliente", rs.getString("sector_cliente"));
                //fila.addProperty("dias_ejecucion", rs.getString("dias_ejecucion"));
                fila.addProperty("cant_trabajos", rs.getString("cantidad_trabajos"));
                fila.addProperty("promedio_dias", rs.getString("promedio_dias"));
                fila.addProperty("costo_contratista", rs.getString("costo_contratista"));
                fila.addProperty("precio_venta", rs.getString("precio_venta"));
                datos.getAsJsonObject(rs.getString("mes_aprobacion")).getAsJsonArray("valores").add(fila);
                
                auxiliar = datos.getAsJsonObject(rs.getString("mes_aprobacion")).getAsJsonObject("totales");
                //auxiliar.addProperty("dias_ejecucion", (auxiliar.get("dias_ejecucion").getAsInt()+fila.get("dias_ejecucion").getAsInt()));
                auxiliar.addProperty("cant_trabajos", (auxiliar.get("cant_trabajos").getAsInt()+fila.get("cant_trabajos").getAsInt()));
                auxiliar.addProperty("promedio_dias", (auxiliar.get("promedio_dias").getAsDouble()+fila.get("promedio_dias").getAsDouble()));
                auxiliar.addProperty("costo_contratista", (auxiliar.get("costo_contratista").getAsDouble()+fila.get("costo_contratista").getAsDouble()));
                auxiliar.addProperty("precio_venta", (auxiliar.get("precio_venta").getAsDouble()+fila.get("precio_venta").getAsDouble()));
                datos.getAsJsonObject(rs.getString("mes_aprobacion")).add("totales",auxiliar);
            }               
            json = gson.toJson(datos);
        } catch (Exception ese) {
            json = "{mensaje:"+ese.getMessage()+"}";
            ese.printStackTrace();
        } finally {
            try {
                if (con != null) {this.desconectar(con);}
                if (ps != null) { ps.close(); }
                if (rs != null) { rs.close(); }
            } catch (Exception ex) {}
            return json;
        }
    }
    
    @Override
    public String detallarConsolidado(String periodo, String sector) {
        String json = "{}";
        Gson gson = new Gson();
        JsonArray datos = new JsonArray();
        JsonObject fila;
        try {
            query = "detallar_consolidado";
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, sector);
            ps.setString(2, periodo);
            rs = ps.executeQuery();
            while (rs.next()) {
                try {
                    fila = new JsonObject();
                    fila.addProperty("solicitud", rs.getString("id_solicitud"));
                    //fila.addProperty("estado", rs.getString("estado"));
                    fila.addProperty("dias_ejecucion", rs.getString("dias_ejecucion"));
                    fila.addProperty("costo_contratista", rs.getString("costo_contratista"));
                    fila.addProperty("precio_venta", rs.getString("precio_venta"));
                    datos.add(fila);
                } catch(Exception es) { }
            }
            json = gson.toJson(datos);
        } catch(Exception e) {
            json = "{mensaje:"+e.getMessage()+"}";
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {this.desconectar(con);}
                if (ps != null) { ps.close(); }
                if (rs != null) { rs.close(); }
            } catch (Exception ex) {}
            return json;
        }
    }
}
