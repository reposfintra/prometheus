/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.opav.model.DAOS.impl;


/**
 *
 * @author Ing.William Siado T
 */
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.lowagie.text.BadElementException;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.html.simpleparser.HTMLWorker;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfPageEventHelper;
import com.lowagie.text.pdf.PdfWriter;
import com.tsp.opav.model.DAOS.MainDAO;
import com.tsp.opav.model.DAOS.ModuloBodegasDAO;



/*Beans*/
import com.tsp.operation.model.beans.StringStatement;
import com.tsp.operation.model.beans.Usuario;
import java.awt.Color;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
////////////////

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;



public class ModuloBodegasImpl extends MainDAO implements ModuloBodegasDAO{
    
    
    private Connection con = null;
    private PreparedStatement ps = null;
    private ResultSet rs = null;
    private String query = "";
    private StringStatement st = null;

    public ModuloBodegasImpl(String dataBaseName) {
        super("ModuloBodegasDAO.xml", dataBaseName);
    }

    @Override
    public String crear_Bodega(JsonObject informacion, Usuario usuario) {
        con = null;
        ps = null;
        String respuestaJson = "{}";
        query = "SQL_INSERTAR_BODEGA";
        try {

            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            
            ps.setString(1, informacion.get("desc_bodega").getAsString());
            ps.setString(2, informacion.get("id_solicitud").getAsString());
            ps.setString(3, informacion.get("dir_resul").getAsString());
            ps.setString(4, usuario.getLogin());
            ps.setString(5, informacion.get("ciu_dir").getAsString());           
            ps.setString(6, informacion.get("nombre_contacto").getAsString());
            ps.setString(7, informacion.get("cargo_contacto").getAsString());
            ps.setString(8, informacion.get("telefono1_contacto").getAsString());
            ps.setString(9, informacion.get("telefono2_contacto").getAsString());

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            try {
                respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR EN EDICION \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(ProcesosCatalogoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
            }
            return respuestaJson;
        }
    }

    @Override
    public String listar_Contratistas() {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "" , consulta;
        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {

            con = this.conectarJNDI();
            query = "SQL_LISTAR_CONTRATISTAS";
            consulta = this.obtenerSQL(query);
            ps = con.prepareStatement(consulta);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return informacion;
        }
    }

    @Override
    public String listar_Bodegas(String codigo_contratista) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "" , consulta;
        JsonArray lista = null;
        JsonObject objetoJson = null;
        String informacion = "{}";
        try {

            con = this.conectarJNDI();
            query = "SQL_LISTAR_BODEGAS";
            consulta = this.obtenerSQL(query);
            ps = con.prepareStatement(consulta);
            ps.setString(1, codigo_contratista);
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                objetoJson = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    objetoJson.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                }
                lista.add(objetoJson);
            }

            informacion = new Gson().toJson(lista);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            return informacion;
        }
    }

    @Override
    public String crear_Bodega_Tercerizada(JsonObject informacion, Usuario usuario) {
        con = null;
        ps = null;
        String respuestaJson = "{}";
        query = "SQL_INSERTAR_BODEGA_TERCERIZADA";
        try {

            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query);
            ps = con.prepareStatement(query);
            
            ps.setString(1, informacion.get("descripcion").getAsString());
            ps.setString(2, informacion.get("id_contratista").getAsString());
            ps.setString(3, informacion.get("cod_ciudad").getAsString());
            ps.setString(4, informacion.get("direccion").getAsString());
            ps.setString(5, informacion.get("nombre_contacto").getAsString());
            ps.setString(6, informacion.get("cargo_contacto").getAsString());
            ps.setString(7, informacion.get("telefono1_contacto").getAsString());
            ps.setString(8, informacion.get("telefono2_contacto").getAsString());
            ps.setString(9, usuario.getLogin());

            ps.executeUpdate();
            respuestaJson = "{\"respuesta\":\"OK\"}";

        } catch (Exception e) {
            try {
                respuestaJson = "{\"error\":\"" + e.getMessage() + "\"}";
                throw new SQLException("ERROR EN EDICION \n" + e.getMessage());
            } catch (SQLException ex) {
                Logger.getLogger(ProcesosCatalogoImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
        } finally {
            try {
                if (con != null) {
                    this.desconectar(con);
                }
                if (ps != null) {
                    ps.close();
                }
            } catch (SQLException ex) {
            }
            return respuestaJson;
        }
    }
    
}
