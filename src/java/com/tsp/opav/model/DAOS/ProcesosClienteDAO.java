/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.opav.model.DAOS;

/**
 *
 * @author user
 */
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.tsp.opav.model.beans.AccionesEca;
import com.tsp.opav.model.beans.CotizacionSl;
import com.tsp.operation.model.beans.BeanGeneral;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.beans.Cliente;
import com.tsp.opav.model.beans.OfertaElca;
import com.tsp.operation.model.beans.BeansMultiservicio;

import java.sql.SQLException;
import java.util.ArrayList;

public interface ProcesosClienteDAO {

    public String cargarTiposClientes() throws SQLException;

    public String cargarDepartamentos() throws SQLException;

    public String cargarCiudades(String codDepartamento) throws SQLException;

    public String crearCliente(Cliente cliente, Usuario usuario, JsonObject info) throws SQLException;

    public String cargarClientesPadre(String q) throws SQLException;

    public ArrayList<BeanGeneral> cargarNics2();

    public String cargarClienteId(String idcliente) throws SQLException;

    public String cargarPadreId(String parameter) throws SQLException;

    public String cargarDepCiuid(String parameter) throws SQLException;

    public String obteneridclientenit(String parameter) throws SQLException;

    public String modificarCliente(Cliente cliente, Usuario usuario, JsonObject info) throws SQLException;

    public String CargarNics(String parameter) throws SQLException;

    public String cargarListadoLineaDeNegocio() throws SQLException;

    public String cargarListadoTipoSolicitud(String q, String codproyecto) throws SQLException;

    public String cargarListadoResponsable() throws SQLException;

    public String cargarListadoInterventor() throws SQLException;

    public String crearSolicitud(OfertaElca ofeca, Usuario usuario, String enviar) throws SQLException;

    public String cargarSolicitud(String idsolicitud) throws SQLException;

    public String cargarLinieaNegocioTS(String tiposolicitud) throws SQLException;

    public String modificarSolicitud(OfertaElca ofeca, Usuario usuario) throws SQLException;

    public String cargarCartera() throws SQLException;

    public String cargarContratistas(String tipo) throws SQLException;

    public String asociarContratistaSolicitud(AccionesEca acceca, Usuario usuario) throws SQLException;

    public String cargarAccionesAsociadas(String id_solicitud) throws SQLException;

    public String cargarTipoTrabajo(String tipo) throws SQLException;

    public String cargarAlcanceAccion(String id_accion, String idsolicitud) throws SQLException;

    public String cargarListaApu(String idaccion) throws SQLException;

    public String cargarApuAsociados(String idaccion);

    public String asociarApuAccion(String idaccion, String listadoidapu, Usuario usuario);

    public String desasociarApuAccion(String idaccion, String listadoidapu, Usuario usuario);

    public boolean VerificaNic(String nic);

    public String cargarClientesAll(String q) throws SQLException;

    public String getInfoClienteById(String idcliente) throws SQLException;

    public String asignarContratista(String idaccion, String idcontratista, Usuario usuario, String nomcontratista) throws SQLException;

    public String actualizarCartera(OfertaElca ofeca, Usuario usuario);

    public String cargarSolicitudesIdcli(String idcli);

    public String cargarLineaNegocio();

    public String cargarEstadoCartera();

    /**
     *
     * @param lineaNegocio
     * @param responsable
     * @param solicitud
     * @param estadoCartera
     * @param fechaInicio
     * @param fechafin
     * @param trazabilidad
     * @param etapaActual
     * @return
     */
    public String cargarInfoSolicitudes(String lineaNegocio, String responsable, String solicitud, String estadoCartera, String fechaInicio, String fechafin, String trazabilidad, String etapaActual,String tipo_proyecto,String foms,String id_cliente,String nom_proyecto);
    
    public String cargarEtapas(String etapa); 
            
    public String cargarEstadoEtapas(String etapa,String id_estado); 
    
    public String trazabilidadOferta(String idsolicitud,Usuario usuario,String etapa, String estado, String causal, String observacion);
    
    public String cambioEstadoEtapaOferta(String idsolicitud,Usuario usuario,String estado_actual, String estado);
    
    public String cargarReporteFacturacion(String numsolicitud);

    public String cargarAccionPrincipal(String idsolicitud);

    public String cargarCotizacion(String idaccion);

    public String cargarFacturasParciales(String num_solicitud);

    public String guardar_coste_Proyecto(CotizacionSl cotizacion, JsonObject facturas,String idsolicitud, Usuario usuario);

    public String cargarFacturasVenta(String num_solicitud);

    public String cargarCotizacionFac(String num_solicitud);

    public String guardarFacturacion(JsonObject informacion,Usuario usuario);
    
    public String prepararFacturacion(JsonObject informacion,Usuario usuario);

    public String cambiarEstadoEnviado(Usuario usuario, String id);

    public String guardarControlAmortizacion(Usuario usuario, String num_factura, String valor, String amortizacion, String retegarantia, String id);

    public String guardarHistoricoControlAmortizacion(Usuario usuario, String num_factura, String valor, String amortizacion, String retegarantia, String id);

    public String eliminarFacturaCliente(String id);

    public String eliminarControlAmortizacion(String num_factura);

    public String anular_factura_parcial(String id_factura_parcial);

    public ArrayList searchNombresArchivos(String rutaOrigen, String id_solicitud);

    public boolean eliminarArchivo(String directorioArchivos, String numreq, String nomarchivo);

    public boolean almacenarArchivoEnCarpetaUsuario(String numsolicitud,String rutaOrigen, String rutaDestino,String filename);

    public String cargar_Insumos_Solicitud(String idsolicitud);

    public String cargar_Historico_Cotizacion(String idsolicitud);

    public String crear_Historico_Cotizacion(String idsolicitud, Usuario usuario);

    public String cargar_Areas_Historico_cotizacion(String idsolicitud);

    public String cargar_Disciplinas_Historico_Cotizacion(String idarea);

    public String cargarAnticipos(String num_solicitud);

    public String guardarAnticipo(JsonObject informacion,Usuario usuario);

    public String exportarPdfCuentaCobro(Usuario usuario, JsonObject informacion);

    public String cargar_Capitulos_Historico_cotizacion(String iddisciplina);

    public String cargar_Actividades_Historico_Cotizacion(String idcapitulo);

    public String obtener_id_rel_actividades_apu(String id_solicitud,String op, String filtro);

    public String set_Rentabilidad_Global(String filtro,String perc_contratista,String perc_esquema,String tipo,String distribucion_rentabilidad_esquema);

    public String buscarAccion(String num_solicitud);

    public String guardarCambioCotizacion(JsonObject informacion, Usuario usuario);

    public String buscarCotizacionTem(String num_solicitud);

    public String insertar_cabecera_tabla_temporal(String id_solicitud, Usuario usuario);

    public String autocompletar(String q, String opp);

    public String update_Presupuesto_Terminado(String id_solicitud, String estado, Usuario usuario);

    public String generar_OT(String id_solicitud, Usuario usuario);
    
    public String cargarTrazabilidadOferta(String idsolicitud);

    public String cargarInformacionModalidadProyecto(String solicitud, String nom_proyecto);

    /**
     *
     * @param solicitud
     * @param porc_administracion
     * @param administracion
     * @param porc_imprevisto
     * @param imprevisto
     * @param porc_utilidad
     * @param utilidad
     * @param iva
     * @param usuario
     * @param perc_iva
     * @param total
     * @param modalidad_comercialstrin
     * @param porc_aiu
     * @param aiu
     * @param iva_compensar
     * @param perc_iva_compensado
     * @return
     */
    public String guardarInformacionModalidadProyecto(String solicitud, String porc_administracion, String administracion, String porc_imprevisto, String imprevisto, String porc_utilidad, String utilidad, String iva, Usuario usuario, String perc_iva, String total, String modalidad_comercialstrin, String porc_aiu, String aiu, String iva_compensar, String perc_iva_compensado);

    public String cargar_tablas_modulo_impresion_oferta(String idsolicitud, String opc);

    /**
     *
     * @param solicitud
     * @param ruta
     * @param jobjCot
     * @param jsonArrAreas
     * @param jsonArrCap
     * @param jsonArrNotas
     * @param usuario
     * @param detallado
     * @return
     */
    
        public String generar_Pdf_Cotizacion(String solicitud, String ruta,JsonObject jobjCot,JsonArray jsonArrAreas,JsonArray jsonArrCap,JsonArray jsonArrNotas, Usuario usuario ,String detallado, boolean precios);

    
        
        public String generar_Pdf_Cotizacion_APU(String solicitud, String ruta,JsonObject jobjCot,JsonArray jsonArrAreas,JsonArray jsonArrCap,JsonArray jsonArrNotas, Usuario usuario ,String detallado, boolean precios);

    
    
        public JsonArray getNotasOferta(String num_solicitud );

        
        
        public String obtenerNotasOferta(String num_solicitud);
    
    /**
     *
     * @param ubicacionPdfs
     * @param usuario
     * @return
     */
    
    public String merge_Pdf(JsonArray ubicacionPdfs, Usuario usuario,JsonArray notasOferta);
  
    /**
     *
     * @param Query
     * @param num_solicitud
     * @return
     */
    public JsonArray getInfoPdfCotizacion(String Query, String num_solicitud );

    public String calcular_iva_compensar(String solicitud, String subtotal);
    
    public String cargarReporteAnticiposMs();

    public String generar_Pdf_Presuesto(String id_solicitud, String directorioOrigen, JsonObject jobjCot, JsonArray jsonArrAreas, JsonArray jsonArrCap, Usuario usuario);
    
    public String guardarCondionesComerciales(JsonObject informacion,Usuario usuario);

    public String cargarCondionesComerciales(String id_solicitud);
    
    public String cargarAccionesPreparadas(String id_solicitud);

    public String cargar_Iva_Compensar(String id_solicitud);

    public String cargar_CIA(String id_solicitud);

    public String clonacionProyectos(String id_solicitud_origen , String id_solicitud_destino ,  Usuario usuario);
    
    public String cargarReporteAnticipos(BeansMultiservicio informacion);

    public String trazabilidadOfertafin(String idsolicitud, Usuario usuario, String estado_actual, String estado);
    
    public String generar_Centros_Costos(String idsolicitud, Usuario usuario , Integer id);

    public String generar_centro_costo_lotes(Usuario usuario, Integer modalidad);

    public String cargarCasosAnticipo();

    public String guardar_notas_oferta(JsonObject informacion, Usuario usuario);
    
    public String actualizarNotaOferta(String idNota, String nota);

    public String eliminarNotaOferta(String idNota);
    
    public String cargarComboGenerico(String op, String param);
}