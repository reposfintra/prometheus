/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.opav.model.DAOS;


/**
 *
 * @author Ing.William Siado T
 */
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.tsp.opav.model.beans.CotizacionSl;
import com.tsp.operation.model.MenuOpcionesModulos;
import com.tsp.operation.model.beans.BeanGeneral;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.beans.Cliente;
import com.tsp.opav.model.beans.OfertaElca;

import java.sql.SQLException;
import java.util.ArrayList;

public interface ModuloCanastaDAO {

    public String cargarComboGenerico(String op, String param);

    public String cargar_Proyecto_Ejecucion(String id_solicitud, String proceso, String opc, String cond, Usuario usuario);

    public String bloquer_Apus(JsonObject informacion, Usuario usuario);

    public String get_Apus_Wbs(String id_solicitud, String id_apu);

    public String cargar_Causales_Canasta();

    public String guardar_Causales_Canasta(String nombre, String descripcion, Usuario usuario);

    public String editar_Causales_Canasta(String id, String nombre, String descripcion, Usuario usuario);

    public String eliminar_Causales_Canasta(String id, Usuario usuario);

    public String cargar_Insumos_Apu(String id_solicitud, String id_rel_actividades_apu, String id_apu, String unidad_medida_apu);

    public String cargar_Insumos_Proyecto(String id_solicitud, String nom_insumo);

    public String cargar_Apus_Insumo(String id_solicitud, String id_insumo, String id_unidad_medida_insumo);

    public String bloquear_Apus_Insumo(JsonObject informacion, Usuario usuario);
    
}
