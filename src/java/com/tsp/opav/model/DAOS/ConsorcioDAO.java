/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.opav.model.DAOS;



import com.tsp.opav.model.beans.*;
import com.tsp.operation.model.beans.StringStatement;
import com.tsp.operation.model.beans.Tipo_impuesto;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.util.LogWriter;
import com.tsp.util.Util;
import java.sql.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;

//import com.tsp.operation.model.beans.;





/**
 *
 * @author Alvaro
 */
public class ConsorcioDAO extends MainDAO {


    /** Creates a new instance of ApplusDAO */
    public ConsorcioDAO() {
        super("ConsorcioDAO.xml");
    }
    public ConsorcioDAO(String dataBaseName) {
        super("ConsorcioDAO.xml", dataBaseName);
    }


    private List listaSolicitudPorFacturar;
    private List listaSubclientePorSolicitud;

    private List listaAccionPorSolicitud;

    private List listaSolicitudParcialPorFacturar;
    private List listaSubclientePorFacturar;
    





    /* Area original */


    private Comision comision;
    private ImpuestoContrato impuestoContrato;
    private List listaPrefacturaResumen;
    private List listaDetallePrefactura;
    private List listaOfertaEca;
    private List listaOfertaEcaDetalle;


    /* Variables refinanciacion */
    
    private List listaFacturaPM;                                                
    private FinanciacionPM financiacionPM;                                      
    private List listaFacturaCliente;                                           //20101115    


    /**
     * Crea una lista de ofertas pendientes por facturar a los clientes
     */
    public void buscaSolicitudPorFacturar()throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs = null;


        String            query    = "SQL_SOLICITUD_POR_FACTURAR";
        listaSolicitudPorFacturar = null;

        try {
            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );

            rs = st.executeQuery();

            listaSolicitudPorFacturar =  new LinkedList();

            while (rs.next()){
                listaSolicitudPorFacturar.add(SolicitudPorFacturar.load(rs));
            }

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA SELECCION DE REGISTROS DE OFERTAS. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }
    }



   /**
     * Busca la tasa maxima
     */
    public double getTasaMaxima(String periodo)throws SQLException{


        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs = null;


        String            query    = "SQL_GET_TASA_MAXIMA";
        double tasa_maxima = 0;

        try {
            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );

            st           = con.prepareStatement( sql );
            st.setString(1, periodo);

            rs = st.executeQuery();

            if (rs.next()){
                tasa_maxima = rs.getDouble("tasa_maxima");
            }

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA SELECCION DE LA TASA MAXIMA. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }

        return tasa_maxima;



    }













    /**
     * Crea una lista de ofertas pendientes por facturar a los clientes
     */
    public List buscaNcDetallePorTrasladar()throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs = null;


        String            query    = "SQL_NC_TRASLADO_DETALLE";
        List listaNcDetallePorTrasladar = null;

        try {
            con          = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );

            rs = st.executeQuery();

            listaNcDetallePorTrasladar =  new LinkedList();

            while (rs.next()){
                listaNcDetallePorTrasladar.add(IngresoDetalle.load(rs));
            }

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA SELECCION FACTURAS PARA REALIZAR DETALLE DE NC DE TRASLADO. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }

        return listaNcDetallePorTrasladar;
    }






    public String setInsertarIngresoDetalle(IngresoDetalle ingresoDetalle)throws SQLException{

        // PreparedStatement st       = null;
        // Connection        con      = null;
        StringStatement st         = null;

        String comando_sql  = "";

        String            query    = "SQL_INSERTAR_INGRESO_DETALLE";

        try {
            // con          = this.conectarJNDI( query );
            // String sql   = obtenerSQL( query );
            // st           = con.prepareStatement( sql );

            st = new StringStatement (this.obtenerSQL(query), true);


            st.setString(1,ingresoDetalle.getReg_status() );
            st.setString(2,ingresoDetalle.getDstrct() );
            st.setString(3,ingresoDetalle.getTipo_documento() );
            st.setString(4,ingresoDetalle.getNum_ingreso() );
            st.setInt(5,ingresoDetalle.getItem() );
            st.setString(6,ingresoDetalle.getNitcli() );
            st.setDouble(7,ingresoDetalle.getValor_ingreso() );
            st.setDouble(8,ingresoDetalle.getValor_ingreso_me() );
            st.setString(9,ingresoDetalle.getFactura() );
            st.setString(10,ingresoDetalle.getFecha_factura() );
            st.setString(11,ingresoDetalle.getCodigo_retefuente() );
            st.setDouble(12,ingresoDetalle.getValor_retefuente() );
            st.setDouble(13,ingresoDetalle.getValor_retefuente_me() );
            st.setString(14,ingresoDetalle.getTipo_doc() );
            st.setString(15,ingresoDetalle.getDocumento() );
            st.setString(16,ingresoDetalle.getCodigo_reteica() );
            st.setDouble(17,ingresoDetalle.getValor_reteica() );
            st.setDouble(18,ingresoDetalle.getValor_reteica_me() );
            st.setDouble(19,ingresoDetalle.getValor_diferencia_tasa() );
            st.setString(20,ingresoDetalle.getCreation_user() );
            st.setString(21,ingresoDetalle.getCreation_date() );
            st.setString(22,ingresoDetalle.getUser_update() );
            st.setString(23,ingresoDetalle.getLast_update() );
            st.setString(24,ingresoDetalle.getBase() );
            st.setString(25,ingresoDetalle.getCuenta() );
            st.setString(26,ingresoDetalle.getAuxiliar() );
            st.setString(27,ingresoDetalle.getFecha_contabilizacion() );
            st.setString(28,ingresoDetalle.getFecha_anulacion_contabilizacion() );
            st.setString(29,ingresoDetalle.getPeriodo() );
            st.setString(30,ingresoDetalle.getFecha_anulacion() );
            st.setString(31,ingresoDetalle.getPeriodo_anulacion() );
            st.setInt(32,ingresoDetalle.getTransaccion() );
            st.setInt(33,ingresoDetalle.getTransaccion_anulacion() );
            st.setString(34,ingresoDetalle.getDescripcion() );
            st.setDouble(35,ingresoDetalle.getValor_tasa() );
            st.setDouble(36,ingresoDetalle.getSaldo_factura() );
            st.setString(37,ingresoDetalle.getProcesado() );

            st.setString(38,ingresoDetalle.getRef1() );
            st.setString(39,ingresoDetalle.getTipo_referencia_1() );
            st.setString(40,ingresoDetalle.getReferencia_1() );
            st.setString(41,ingresoDetalle.getTipo_referencia_2() );
            st.setString(42,ingresoDetalle.getReferencia_2() );
            st.setString(43,ingresoDetalle.getTipo_referencia_3() );
            st.setString(44,ingresoDetalle.getReferencia_3() );

            // comando_sql = st.toString();

            comando_sql = st.getSql();

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA INSERCION EN REGISTRO INGRESO DETALLE. \n " + e.getMessage());
        }
        finally{
            // st.close();
            // this.desconectar(con);

            if (st  != null){
                try{ st = null;
                }
                catch(Exception e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }

        }
        return comando_sql;
    }
















    public String setInsertarIngresoCabecera(IngresoCabecera ingresoCabecera)throws SQLException{



        // PreparedStatement st       = null;
        // Connection        con      = null;
        StringStatement st         = null;

        String comando_sql  = "";

        String            query    = "SQL_INSERTAR_INGRESO_CABECERA";

        try {
            // con          = this.conectarJNDI( query );
            // String sql   = obtenerSQL( query );
            // st           = con.prepareStatement( sql );

            st = new StringStatement (this.obtenerSQL(query), true);

            st.setString(1,ingresoCabecera.getReg_status() );
            st.setString(2,ingresoCabecera.getDstrct() );
            st.setString(3,ingresoCabecera.getTipo_documento() );;
            st.setString(4,ingresoCabecera.getNum_ingreso() );
            st.setString(5,ingresoCabecera.getCodcli() );
            st.setString(6,ingresoCabecera.getNitcli() );
            st.setString(7,ingresoCabecera.getConcepto() );
            st.setString(8,ingresoCabecera.getTipo_ingreso() );
            st.setString(9,ingresoCabecera.getFecha_consignacion() );
            st.setString(10,ingresoCabecera.getFecha_ingreso() );
            st.setString(11,ingresoCabecera.getBranch_code() );
            st.setString(12,ingresoCabecera.getBank_account_no() );
            st.setString(13,ingresoCabecera.getCodmoneda() );
            st.setString(14,ingresoCabecera.getAgencia_ingreso() );
            st.setString(15,ingresoCabecera.getDescripcion_ingreso() );
            st.setString(16,ingresoCabecera.getPeriodo() );
            st.setDouble(17,ingresoCabecera.getVlr_ingreso() );
            st.setDouble(18,ingresoCabecera.getVlr_ingreso_me() );
            st.setDouble(19,ingresoCabecera.getVlr_tasa() );
            st.setString(20,ingresoCabecera.getFecha_tasa() );
            st.setInt(21,ingresoCabecera.getCant_item() );
            st.setInt(22,ingresoCabecera.getTransaccion() );
            st.setInt(23,ingresoCabecera.getTransaccion_anulacion() );
            st.setString(24,ingresoCabecera.getFecha_impresion() );
            st.setString(25,ingresoCabecera.getFecha_contabilizacion() );
            st.setString(26,ingresoCabecera.getFecha_anulacion_contabilizacion() );
            st.setString(27,ingresoCabecera.getFecha_anulacion() );
            st.setString(28,ingresoCabecera.getCreation_user() );
            st.setString(29,ingresoCabecera.getCreation_date() );
            st.setString(30,ingresoCabecera.getUser_update() );
            st.setString(31,ingresoCabecera.getLast_update() );
            st.setString(32,ingresoCabecera.getBase() );
            st.setString(33,ingresoCabecera.getNro_consignacion() );
            st.setString(34,ingresoCabecera.getPeriodo_anulacion() );
            st.setString(35,ingresoCabecera.getCuenta() );
            st.setString(36,ingresoCabecera.getAuxiliar() );
            st.setString(37,ingresoCabecera.getAbc() );
            st.setDouble(38,ingresoCabecera.getTasa_dol_bol() );
            st.setDouble(39,ingresoCabecera.getSaldo_ingreso() );
            st.setString(40,ingresoCabecera.getCmc() );
            st.setString(41,ingresoCabecera.getCorficolombiana() );
            st.setString(42,ingresoCabecera.getFec_envio_fiducia() );
            st.setString(43,ingresoCabecera.getTipo_referencia_1() );
            st.setString(44,ingresoCabecera.getReferencia_1() );
            st.setString(45,ingresoCabecera.getTipo_referencia_2() );
            st.setString(46,ingresoCabecera.getReferencia_2() );
            st.setString(47,ingresoCabecera.getTipo_referencia_3() );
            st.setString(48,ingresoCabecera.getReferencia_3() );

            // comando_sql = st.toString();

            comando_sql = st.getSql();

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA INSERCION EN REGISTRO INGRESO. \n " + e.getMessage());
        }
        finally{
            // st.close();
            // this.desconectar(con);

            if (st  != null){
                try{ st = null;
                }
                catch(Exception e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
        }
        return comando_sql;
    }




    public String setActualizarFacturaCliente(IngresoCabecera ingresoCabecera)throws SQLException{


        // PreparedStatement st       = null;
        // Connection        con      = null;
        StringStatement st         = null;

        String comando_sql  = "";

        String            query    = "SQL_ACTUALIZAR_FACTURA_CLIENTE";

        try {
            // con          = this.conectarJNDI( query );
            // String sql   = obtenerSQL( query );
            // st           = con.prepareStatement( sql );

            st = new StringStatement (this.obtenerSQL(query), true);

            st.setString(1,ingresoCabecera.getCreation_date() );
            st.setString(2,ingresoCabecera.getTipo_ingreso() );
            st.setString(3,ingresoCabecera.getNum_ingreso() );
            st.setDouble(4,ingresoCabecera.getVlr_ingreso() );
            st.setDouble(5,ingresoCabecera.getVlr_ingreso() );
            st.setDouble(6,ingresoCabecera.getVlr_ingreso_me() );
            st.setDouble(7,ingresoCabecera.getVlr_ingreso_me() );
            st.setString(8,ingresoCabecera.getDstrct() );
            st.setString(9,ingresoCabecera.getTipo_referencia_1() );
            st.setString(10,ingresoCabecera.getReferencia_1() );


            // comando_sql = st.toString();

            comando_sql = st.getSql();

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA ACTUALIZACION DE LA FACTURA DEL CLIENTE. \n " + e.getMessage());
        }
        finally{
            // st.close();
            // this.desconectar(con);

            if (st  != null){
                try{ st = null;
                }
                catch(Exception e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
        }
        return comando_sql;
    }



    public String setActualizarFacturaPM(IngresoCabecera ingresoCabecera)throws SQLException{

        StringStatement st  = null;
        String comando_sql  = "";
        String     query    = "SQL_ACTUALIZAR_FACTURA_PM";

        try {
            st = new StringStatement (this.obtenerSQL(query), true);
            st.setString(1,ingresoCabecera.getTipo_documento() );
            st.setString(2,ingresoCabecera.getNum_ingreso() );
            st.setDouble(3,ingresoCabecera.getVlr_ingreso() );
            st.setDouble(4,ingresoCabecera.getVlr_ingreso() );
            st.setDouble(5,ingresoCabecera.getVlr_ingreso_me() );
            st.setDouble(6,ingresoCabecera.getVlr_ingreso_me() );
            st.setString(7,ingresoCabecera.getDstrct() );
            st.setString(8,ingresoCabecera.getTipo_referencia_1() );
            st.setString(9,ingresoCabecera.getReferencia_1() );

            comando_sql = st.getSql();

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA ACTUALIZACION DE LA FACTURA PM DEL CLIENTE. \n " + e.getMessage());
        }
        finally{

            if (st  != null){
                try{ st = null;
                }
                catch(Exception e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
        }
        return comando_sql;
    }























    public String insertarDetalleCXC(FacturaDetalle facturaDetalle, String dataBaseName) throws SQLException{



        // PreparedStatement st       = null;
        // Connection        con      = null;
        StringStatement st         = null;

        String comando_sql  = "";

        String            query    = "SQL_INSERTAR_DETALLE_CXC";

        try {
            // con          = this.conectarJNDI( query );
            // String sql   = obtenerSQL( query );
            // st           = con.prepareStatement( sql );

            st = new StringStatement (this.obtenerSQL(query), true);

            st.setString(1 ,facturaDetalle.getReg_status() );
            st.setString(2 ,facturaDetalle.getDstrct() );
            st.setString(3 ,facturaDetalle.getTipo_documento() );
            st.setString(4 ,facturaDetalle.getDocumento() );
            st.setInt(5 ,facturaDetalle.getItem() );
            st.setString(6 ,facturaDetalle.getNit() );
            st.setString(7 ,facturaDetalle.getConcepto() );
            st.setString(8 ,facturaDetalle.getNumero_remesa() );
            st.setString(9 ,facturaDetalle.getDescripcion() );
            st.setString(10 ,facturaDetalle.getCodigo_cuenta_contable() );
            st.setDouble(11 ,facturaDetalle.getCantidad() );
            st.setDouble(12 ,facturaDetalle.getValor_unitario() );
            st.setDouble(13 ,facturaDetalle.getValor_unitariome() );
            st.setDouble(14 ,facturaDetalle.getValor_item() );
            st.setDouble(15 ,facturaDetalle.getValor_itemme() );
            st.setDouble(16 ,facturaDetalle.getValor_tasa() );
            st.setString(17 ,facturaDetalle.getMoneda() );
            st.setString(18 ,facturaDetalle.getLast_update() );
            st.setString(19 ,facturaDetalle.getUser_update() );
            st.setString(20 ,facturaDetalle.getCreation_date() );
            st.setString(21 ,facturaDetalle.getCreation_user() );
            st.setString(22 ,facturaDetalle.getBase() );
            st.setString(23 ,facturaDetalle.getAuxiliar() );
            st.setDouble(24 ,facturaDetalle.getValor_ingreso() );
            st.setString(25 ,facturaDetalle.getTipo_documento_rel() );
            st.setInt(26 ,facturaDetalle.getTransaccion() );
            st.setString(27 ,facturaDetalle.getDocumento_relacionado() );
            st.setString(28 ,facturaDetalle.getTipo_referencia_1() );
            st.setString(29 ,facturaDetalle.getReferencia_1() );
            st.setString(30 ,facturaDetalle.getTipo_referencia_2() );
            st.setString(31 ,facturaDetalle.getReferencia_2() );
            st.setString(32 ,facturaDetalle.getTipo_referencia_3() );
            st.setString(33 ,facturaDetalle.getReferencia_3() );

            // comando_sql = st.toString();

            comando_sql = st.getSql();

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA INSERCION DEL ITEM DE LA FACTURA CXC. \n " + e.getMessage());
        }
        finally{
            // st.close();
            // this.desconectar(con);

            if (st  != null){
                try{ st = null;
                }
                catch(Exception e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
        }
        return comando_sql;

    }




    public String insertarCabeceraCXC(FacturaCabecera facturaCabecera, String dataBaseName) throws SQLException{


        // PreparedStatement st       = null;
        // Connection        con      = null;
        StringStatement st         = null;

        String comando_sql  = "";

        String            query    = "SQL_INSERTAR_CABECERA_CXC";

        try {
            // con          = this.conectarJNDI( query );
            // String sql   = obtenerSQL( query );
            // st           = con.prepareStatement( sql );

            st = new StringStatement (this.obtenerSQL(query), true);

            st.setString(1 ,facturaCabecera.getReg_status() ) ;
            st.setString(2 ,facturaCabecera.getDstrct() ) ;
            st.setString(3 ,facturaCabecera.getTipo_documento() ) ;
            st.setString(4 ,facturaCabecera.getDocumento() ) ;
            st.setString(5 ,facturaCabecera.getNit() ) ;
            st.setString(6 ,facturaCabecera.getCodcli() ) ;
            st.setString(7 ,facturaCabecera.getConcepto() ) ;
            st.setString(8 ,facturaCabecera.getFecha_factura() ) ;
            st.setString(9 ,facturaCabecera.getFecha_vencimiento() ) ;
            st.setString(10,facturaCabecera.getFecha_ultimo_pago() ) ;
            st.setString(11 ,facturaCabecera.getFecha_impresion() ) ;
            st.setString(12 ,facturaCabecera.getDescripcion() ) ;
            st.setString(13 ,facturaCabecera.getObservacion() ) ;
            st.setDouble(14 ,facturaCabecera.getValor_factura() ) ;
            st.setDouble(15 ,facturaCabecera.getValor_abono() ) ;
            st.setDouble(16 ,facturaCabecera.getValor_saldo() ) ;
            st.setDouble(17 ,facturaCabecera.getValor_facturame() ) ;
            st.setDouble(18 ,facturaCabecera.getValor_abonome() ) ;
            st.setDouble(19 ,facturaCabecera.getValor_saldome() ) ;
            st.setDouble(20 ,facturaCabecera.getValor_tasa() ) ;
            st.setString(21 ,facturaCabecera.getMoneda() ) ;
            st.setInt(22 ,facturaCabecera.getCantidad_items() ) ;
            st.setString(23 ,facturaCabecera.getForma_pago() ) ;
            st.setString(24 ,facturaCabecera.getAgencia_facturacion() ) ;
            st.setString(25 ,facturaCabecera.getAgencia_cobro() ) ;
            st.setString(26 ,facturaCabecera.getZona() ) ;
            st.setString(27 ,facturaCabecera.getClasificacion1() ) ;
            st.setString(28 ,facturaCabecera.getClasificacion2() ) ;
            st.setString(29 ,facturaCabecera.getClasificacion3() ) ;
            st.setInt(30 ,facturaCabecera.getTransaccion() ) ;
            st.setInt(31 ,facturaCabecera.getTransaccion_anulacion() ) ;
            st.setString(32 ,facturaCabecera.getFecha_contabilizacion() ) ;
            st.setString(33 ,facturaCabecera.getFecha_anulacion() ) ;
            st.setString(34 ,facturaCabecera.getFecha_contabilizacion_anulacion() ) ;
            st.setString(35 ,facturaCabecera.getBase() ) ;
            st.setString(36 ,facturaCabecera.getLast_update() ) ;
            st.setString(37 ,facturaCabecera.getUser_update() ) ;
            st.setString(38 ,facturaCabecera.getCreation_date() ) ;
            st.setString(39 ,facturaCabecera.getCreation_user() ) ;
            st.setString(40 ,facturaCabecera.getFecha_probable_pago() ) ;
            st.setString(41 ,facturaCabecera.getFlujo() ) ;
            st.setString(42 ,facturaCabecera.getRif() ) ;
            st.setString(43 ,facturaCabecera.getCmc() ) ;
            st.setString(44 ,facturaCabecera.getUsuario_anulo() ) ;
            st.setString(45 ,facturaCabecera.getFormato() ) ;
            st.setString(46 ,facturaCabecera.getAgencia_impresion() ) ;
            st.setString(47 ,facturaCabecera.getPeriodo() ) ;
            st.setDouble(48 ,facturaCabecera.getValor_tasa_remesa() ) ;
            st.setString(49 ,facturaCabecera.getNegasoc() ) ;
            st.setString(50 ,facturaCabecera.getNum_doc_fen() ) ;
            st.setString(51 ,facturaCabecera.getObs() ) ;
            st.setString(52 ,facturaCabecera.getPagado_fenalco() ) ;
            st.setString(53 ,facturaCabecera.getCorficolombiana() ) ;
            st.setString(54 ,facturaCabecera.getTipo_ref1() ) ;
            st.setString(55 ,facturaCabecera.getRef1() ) ;
            st.setString(56 ,facturaCabecera.getTipo_ref2() ) ;
            st.setString(57 ,facturaCabecera.getRef2() ) ;
            st.setString(58 ,facturaCabecera.getDstrct_ultimo_ingreso() ) ;
            st.setString(59 ,facturaCabecera.getTipo_documento_ultimo_ingreso() ) ;
            st.setString(60 ,facturaCabecera.getNum_ingreso_ultimo_ingreso() ) ;
            st.setInt(61 ,facturaCabecera.getItem_ultimo_ingreso() ) ;
            st.setString(62 ,facturaCabecera.getFec_envio_fiducia() ) ;
            st.setString(63 ,facturaCabecera.getNit_enviado_fiducia() ) ;
            st.setString(64 ,facturaCabecera.getTipo_referencia_1() ) ;
            st.setString(65 ,facturaCabecera.getReferencia_1() ) ;
            st.setString(66 ,facturaCabecera.getTipo_referencia_2() ) ;
            st.setString(67 ,facturaCabecera.getReferencia_2() ) ;
            st.setString(68 ,facturaCabecera.getTipo_referencia_3() ) ;
            st.setString(69 ,facturaCabecera.getReferencia_3() ) ;
            st.setString(70 ,facturaCabecera.getNc_traslado() ) ;
            st.setString(71 ,facturaCabecera.getFecha_nc_traslado() ) ;
            st.setString(72 ,facturaCabecera.getTipo_nc() ) ;
            st.setString(73 ,facturaCabecera.getNumero_nc() ) ;
            st.setString(74 ,facturaCabecera.getFactura_traslado() );

            // comando_sql = st.toString();

            comando_sql = st.getSql();

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA INSERCION DE LA CABECERA DE LA FACTURA CXC. \n " + e.getMessage());
        }
        finally{
            // st.close();
            // this.desconectar(con);

            if (st  != null){
                try{ st = null;
                }
                catch(Exception e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
        }
        return comando_sql;

    }









    public String actualizarFacturaTrasladada(FacturaDetalle facturaDetalle) throws SQLException{


        // PreparedStatement st       = null;
        // Connection        con      = null;
        StringStatement st         = null;

        String comando_sql  = "";

        String            query    = "SQL_ACTUALIZAR_FACTURA_TRASLADADA";

        try {
            // con          = this.conectarJNDI( query );
            // String sql   = obtenerSQL( query );
            // st           = con.prepareStatement( sql );

            st = new StringStatement (this.obtenerSQL(query), true);

            st.setString(1 ,facturaDetalle.getDocumento() );
            st.setString(2 ,facturaDetalle.getDstrct() );
            st.setString(3 ,facturaDetalle.getTipo_documento() );
            st.setString(4 ,facturaDetalle.getReferencia_1() );

            // comando_sql = st.toString();

            comando_sql = st.getSql();

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA ACTUALIZACION DE LA FACTURA TRASLADADA A FINTRA. \n " + e.getMessage());
        }
        finally{
            // st.close();
            // this.desconectar(con);

            if (st  != null){
                try{ st = null;
                }
                catch(Exception e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
        }
        return comando_sql;

    }




    public String actualizarFacturaCabeceraTrasladada(String dstrct, String tipoDocumento, String documento, String facturaFintra) throws SQLException{



        // PreparedStatement st       = null;
        // Connection        con      = null;
        StringStatement st         = null;

        String comando_sql  = "";

        String            query    = "SQL_ACTUALIZAR_FACTURA_TRASLADADA";

        try {
            // con          = this.conectarJNDI( query );
            // String sql   = obtenerSQL( query );
            // st           = con.prepareStatement( sql );

            st = new StringStatement (this.obtenerSQL(query), true);

            st.setString(1 ,facturaFintra );
            st.setString(2 ,dstrct );
            st.setString(3 ,tipoDocumento );
            st.setString(4 ,documento );

            // comando_sql = st.toString();

            comando_sql = st.getSql();

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA ACTUALIZACION DE LA FACTURA TRASLADADA A FINTRA. \n " + e.getMessage());
        }
        finally{
            // st.close();
            // this.desconectar(con);

            if (st  != null){
                try{ st = null;
                }
                catch(Exception e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
        }
        return comando_sql;

    }



    /**
     * Retorna una Lista de ofertas pendientes de facturar al cliente
     */
    public List getSolicitudPorFacturar() {
        return listaSolicitudPorFacturar;
    }



    /**
     * Crea una lista de subclientes de una solicitud
     */
    public void buscaSubclientePorSolicitud(String id_solicitud, int parcial)throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs = null;


        String            query    = "SQL_SUBCLIENTE_POR_SOLICITUD";
        listaSubclientePorSolicitud = null;

        try {
            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            st.setString( 1, id_solicitud );
            st.setInt( 2, parcial );

            rs = st.executeQuery();

            listaSubclientePorSolicitud =  new LinkedList();
            
            while (rs.next()){
                listaSubclientePorSolicitud.add(SubclientePorSolicitud.load(rs));
            }

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA SELECCION DE SUBCLIENTES. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }
    }






    /**
     * Retorna una Lista de todos los subclientes asociados a una solicitud
     */
    public List getSubclientePorSolicitud() {
        return listaSubclientePorSolicitud;
    }







    /**
     * Crea una lista de las facturas que tienen NC y que no se les ha hecho una factura a Fintra
     */
    public List buscaFacturaPorCobrarFintra()throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs = null;


        String            query    = "SQL_BUSCAR_FACTURAS_POR_COBRAR";
        List listaFacturaPorCobrarFintra = null;

        try {
            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );

            rs = st.executeQuery();

            listaFacturaPorCobrarFintra =  new LinkedList();

            while (rs.next()){
                listaFacturaPorCobrarFintra.add(FacturaPorCobrarFintra.load(rs));
            }

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA SELECCION DE FACTURAS POR COBRAR A FINTRA. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }

        return listaFacturaPorCobrarFintra;
    }




    /**
     * Crea una lista de las facturas que tienen NC y que no se les ha hecho una factura a Fintra
     */
    public List buscaFacturaPorCobrarDetalle(String documento)throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs = null;


        String            query    = "SQL_BUSCAR_FACTURAS_POR_COBRAR_DETALLE";
        List listaFacturaDetalle   = null;

        try {
            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            st.setString(1, documento);


            rs = st.executeQuery();

            listaFacturaDetalle =  new LinkedList();

            while (rs.next()){
                listaFacturaDetalle.add(FacturaDetalle.load(rs));
            }

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA SELECCION DE LOS ITEMS DE FACTURAS POR COBRAR. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }

        return listaFacturaDetalle;
    }

























    /**
     * Devuelve el valor de un item para la factura de traslado segun una condicion
     */
    public double buscaFacturaDetalle(String documento, String condicion)throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs = null;


        String            query    = "SQL_FACTURAS_DETALLE_A_TRASLADAR";
        double valor_item = 0;

        try {
            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            sql = sql.replaceAll("#CONDICION#", condicion);

            st           = con.prepareStatement( sql );
            st.setString(1, documento);

            rs = st.executeQuery();

            if (rs.next()){
                valor_item = rs.getDouble("valor_item");
            }

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA SELECCION DE FACTURAS DETALLE POR TRASLADAR A FINTRA. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }

        return valor_item;
    }






    /**
     *  Crea una lista de las facturas que tienen NC y que no se les ha hecho una factura a Fintra
     * @author Alvaro Pabon M
     * @version 1.0
     * @return Lista de las facturas a trasladar a Fintra
     * @throws SQLException
     */
    public List buscaFacturaPorTrasladar()throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs = null;


        String            query    = "SQL_FACTURAS_A_TRASLADAR";
        List listaFacturaPorTrasladar = null;

        try {
            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );

            rs = st.executeQuery();

            listaFacturaPorTrasladar =  new LinkedList();

            while (rs.next()){
                listaFacturaPorTrasladar.add(FacturaCabecera.load(rs));
            }

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA SELECCION DE FACTURAS POR COBRAR A FINTRA. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }

        return listaFacturaPorTrasladar;
    }







    /**
     *  Crea una lista de las facturas segun una condicion
     * @author Alvaro Pabon M
     * @version 1.0
     * @return Lista de las facturas a trasladar a Fintra
     * @throws SQLException
     */
    public List buscaFacturaCabecera(String condicion, String dataBaseName)throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs = null;


        String            query    = "SQL_FACTURA_CXP_CABECERA";
        List listaFactura = null;

        try {
            con   = this.conectarJNDI( query, dataBaseName );
            String  sql  = obtenerSQL( query );

            sql = sql.replaceAll("#CONDICION#", condicion);

            st           = con.prepareStatement( sql );

            rs = st.executeQuery();

            listaFactura =  new LinkedList();

            while (rs.next()){
                listaFactura.add(FacturaCabeceraCxP.load(rs));
            }

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA SELECCION DE FACTURAS. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }

        return listaFactura;
    }




    public List buscaFacturaCabeceraCxC(String condicion, String dataBaseName)throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs = null;


        String            query    = "SQL_FACTURA_CABECERA_CXC";
        List listaFactura = null;

        try {
            con   = this.conectarJNDI( query, dataBaseName );
            String  sql  = obtenerSQL( query );

            sql = sql.replaceAll("#CONDICION#", condicion);

            st           = con.prepareStatement( sql );

            rs = st.executeQuery();

            listaFactura =  new LinkedList();

            while (rs.next()){
                listaFactura.add(FacturaCabecera.load(rs));
            }

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA SELECCION DE FACTURAS. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }

        return listaFactura;
    }






    /**
     *  Crea una lista de las facturas a las cuales hay que realizarle una nota credito por factoring y formula en la factura analoga en Fintra
     * @author Alvaro Pabon M
     * @version 1.0
     * @return Lista de las facturas
     * @throws SQLException
     */
    public List buscaFacturaFactoringFormula()throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs = null;


        String            query    = "SQL_FACTURA_FACTORING_FORMULA";
        List listaFactura = null;

        try {
            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );

            rs = st.executeQuery();

            listaFactura =  new LinkedList();

            while (rs.next()){
                listaFactura.add(FacturaFactoringFormula.load(rs));
            }

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA SELECCION DE FACTURAS POR COBRAR A FINTRA. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }

        return listaFactura;
    }





    /**
     *  Crea una lista de las facturas que tienen NC y que no se les ha hecho una factura a Fintra
     * @author Alvaro Pabon M
     * @version 1.0
     * @return Lista de las facturas a trasladar a Fintra
     * @throws SQLException
     */
    public List buscaAccionFactoringFormula(String dstrct, String proveedor, String tipo_documento, String documento)throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs = null;


        String            query    = "SQL_ACCIONES_FACTORING_FORMULA";
        List listaAccion = null;

        try {
            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );

            st.setString( 1, dstrct );
            st.setString( 2, proveedor );
            st.setString( 3, tipo_documento );
            st.setString( 4, documento );

            rs = st.executeQuery();

            listaAccion =  new LinkedList();

            while (rs.next()){
                listaAccion.add(AccionFactoringFormula.load(rs));
            }

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA SELECCION DE ACCIONES CON FACTORING O FORMULA. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }

        return listaAccion;
    }




    /**
     * Crea una lista de acciones de una solicitud
     */
    public void buscaAccionPorSolicitud(String id_solicitud, int parcial)throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs = null;


        String            query    = "SQL_ACCION_POR_SOLICITUD";
        listaAccionPorSolicitud = null;

        try {
            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            st.setString( 1, id_solicitud );
            st.setInt( 2, parcial );
            st.setString( 3, id_solicitud );
            st.setInt( 4, parcial );
            rs = st.executeQuery();

            listaAccionPorSolicitud =  new LinkedList();

            while (rs.next()){
                listaAccionPorSolicitud.add(AccionPorSolicitud.load(rs));
            }

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA SELECCION DE LAS ACCIONES. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }
    }




    /**
     * Retorna una Lista de todos las acciones asociados a una solicitud
     */
    public List getAccionPorSolicitud() {
        return listaAccionPorSolicitud;
    }






    /**
     * Forma un comando sql para actualizar las acciones que ha sido prefacturadas
     *
     */


    public String setAcciones(AccionPorSolicitud accion, String last_update, String usuario)throws SQLException{

        // PreparedStatement st       = null;
        // Connection        con      = null;
        StringStatement st         = null;

        String comando_sql  = "";

        String            query    = "SQL_SET_ACCIONES";

        try {
            // con          = this.conectarJNDI( query );
            // String sql   = obtenerSQL( query );
            // st           = con.prepareStatement( sql );

            st = new StringStatement (this.obtenerSQL(query), true);

            st.setString(1,usuario);
            st.setString(2,last_update);
            st.setString(3,"S");
            st.setDouble(4,accion.getBonificacion());
            st.setDouble(5,accion.getComision_opav());
            st.setDouble(6,accion.getComision_fintra());
            st.setDouble(7,accion.getComision_interventoria());
            st.setDouble(8,accion.getComision_provintegral());
            st.setDouble(9,accion.getComision_eca());
            st.setDouble(10,accion.getBase_iva_contratista());
            st.setDouble(11,accion.getIva_contratista());
            st.setDouble(12,accion.getIva_bonificacion());
            st.setDouble(13,accion.getIva_comision_opav());
            st.setDouble(14,accion.getIva_comision_fintra());
            st.setDouble(15,accion.getIva_comision_interventoria());
            st.setDouble(16,accion.getIva_comision_provintegral());
            st.setDouble(17,accion.getIva_comision_eca());
            st.setDouble(18,accion.getValor_a_financiar_sin_iva());
            st.setDouble(19,accion.getIva_contratista() +
                            accion.getIva_bonificacion() +
                            accion.getIva_comision_opav() +
                            accion.getIva_comision_fintra() +
                            accion.getIva_comision_interventoria() +
                            accion.getIva_comision_provintegral() +
                            accion.getIva_comision_eca() ) ;
            st.setDouble(20,accion.getValor_a_financiar());
            st.setString(21,accion.getId_accion());


            // comando_sql = st.toString();

            comando_sql = st.getSql();

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA ACTUALIZACION DE LAS ACCIONES. \n " + e.getMessage());
        }
        finally{
            // st.close();
            // this.desconectar(con);

            if (st  != null){
                try{ st = null;
                }
                catch(Exception e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
        }
        return comando_sql;
    }






    /**
     * Forma un comando sql para actualizar las acciones que ha sido prefacturadas
     *
     */


    public String setSubclientesOfertas( SubclientePorSolicitud subcliente,  String simbolo_variable,
                                         String observacion_subcliente, String fecha_factura,  String usuario)throws SQLException{


        // PreparedStatement st       = null;
        // Connection        con      = null;
        StringStatement st         = null;

        String comando_sql  = "";

        String            query    = "SQL_SET_SUBCLIENTES_OFERTAS";

        try {
            // con          = this.conectarJNDI( query );
            // String sql   = obtenerSQL( query );
            // st           = con.prepareStatement( sql );

            st = new StringStatement (this.obtenerSQL(query), true);

            st.setString(1,subcliente.getTipo());
            st.setString(2,subcliente.getClase_dtf());
            st.setDouble(3,subcliente.getPuntos() );
            st.setDouble(4,subcliente.getValor_sin_iva());
            st.setDouble(5,subcliente.getPorcentaje_base());
            st.setDouble(6,subcliente.getSubtotal_base_1());
            st.setDouble(7,subcliente.getPorcentaje_incremento());
            st.setDouble(8,subcliente.getPorcentaje_extemporaneo());
            st.setDouble(9,subcliente.getValor_extemporaneo_1());
            st.setDouble(10,subcliente.getSubtotal_base_2());
            st.setDouble(11,subcliente.getSubtotal_base_3());
            st.setDouble(12,subcliente.getValor_iva());
            st.setDouble(13,subcliente.getValor_base_iva());
            st.setDouble(14,subcliente.getValor_extemporaneo_2());
            st.setDouble(15,subcliente.getSubtotal_iva());
            st.setDouble(16,subcliente.getSubtotal_a_financiar());
            st.setDouble(17,subcliente.getIntereses());
            st.setDouble(18,subcliente.getValor_con_financiacion());
            st.setDouble(19,subcliente.getValor_cuota());
            st.setString(20,usuario);
            st.setDouble(21,subcliente.getDtf_semana());
            st.setDouble(22,subcliente.getPorcentaje_interes());

            st.setString(23,subcliente.getSimbolo_variable());
            st.setString(24,subcliente.getObservacion() );
            st.setString(25,subcliente.getFecha_factura());

            // st.setString(26,subcliente.getId_cliente());
            // st.setString(27,subcliente.getId_solicitud());
            // st.setInt(28,subcliente.getParcial());

            st.setString(26,subcliente.getFecha_financiacion());//20100508
            st.setString(27,subcliente.getFecha_inicio_pago()); // 20101021 20100508
            st.setString(28,subcliente.getId_cliente());//20100508 20100508
            st.setString(29,subcliente.getId_solicitud());//20100508 20100508
            st.setInt(30,subcliente.getParcial());//20100508 20100508


            // comando_sql = st.toString();

            comando_sql = st.getSql();

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA ACTUALIZACION DE LAS SOLICITUDES DE LOS SUBCLIENTES. \n " + e.getMessage());
        }
        finally{
            // st.close();
            // this.desconectar(con);

            if (st  != null){
                try{ st = null;
                }
                catch(Exception e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
        }
        return comando_sql;
    }




    /**
     * Crea una lista de solicitudes parciales  pendientes por facturar a los clientes
     */
    public void buscaSolicitudParcialPorFacturar()throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs = null;


        String            query    = "SQL_BUSCAR_SOLICITUD_PARCIAL";
        listaSolicitudParcialPorFacturar = null;

        try {
            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );

            rs = st.executeQuery();

            listaSolicitudParcialPorFacturar =  new LinkedList();

            while (rs.next()){
                listaSolicitudParcialPorFacturar.add(SolicitudParcialPorFacturar.load(rs));
            }

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA SELECCION DE SOLICITUDES PARCIALES POR FACTURAR. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }
    }


    /**
     * Retorna una Lista de solicitudes parciales  pendientes de facturar al cliente
     */
    public List getSolicitudParcialPorFacturar() {
        return listaSolicitudParcialPorFacturar;
    }









    /**
     * Crea una lista de solicitudes parciales  pendientes por facturar a los clientes
     */
    public Vector buscaTotalFacturaFiducia(String documento)throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs = null;


        String            query    = "SQL_BUSCA_TOTAL_FACTURA_FIDUCIA";
        Vector      valoresFiducia = null;


        try {
            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );

            st.setString(1,documento);
            rs = st.executeQuery();

            if (rs.next()){
                valoresFiducia = new Vector();
                valoresFiducia.addElement( rs.getDouble("valor_factura") );
                valoresFiducia.addElement( rs.getDouble("valor_capital") );
                valoresFiducia.addElement( rs.getDouble("valor_interes") );
            }

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA SELECCION DE TOTALES DE LA FACTURA DE LA FIDUCIA. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }
        return valoresFiducia;
    }






    public String actualizarCabecera(String documento, double diferencia)throws SQLException{


        // PreparedStatement st       = null;
        // Connection        con      = null;
        StringStatement st         = null;

        String comando_sql  = "";

        String            query    = "SQL_ACTUALIZA_CABECERA";

        try {
            // con          = this.conectarJNDI( query );
            // String sql   = obtenerSQL( query );
            // st           = con.prepareStatement( sql );

            st = new StringStatement (this.obtenerSQL(query), true);

            st.setDouble(1,diferencia );
            st.setDouble(2,diferencia );
            st.setDouble(3,diferencia );
            st.setDouble(4,diferencia );
            st.setString(5,documento );

            // comando_sql = st.toString();

            comando_sql = st.getSql();

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA ACTUALIZACION DE LA CABECERA POR AJUSTE. \n " + e.getMessage());
        }
        finally{
            // st.close();
            // this.desconectar(con);

            if (st  != null){
                try{ st = null;
                }
                catch(Exception e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
        }
        return comando_sql;
    }

    public Vector AjusteSolicitudesEspeciales (String id_solicitud)throws SQLException{
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "SQL_VERIFICACION_IDSOLICITUD";
        String respuestaVerificacion = "";
        Vector comandos_sql = new Vector();
        String comandoSQL1 = "";
        String comandoSQL2 = "";
        try {
            con = this.conectarJNDI(query);
            String sql = obtenerSQL(query);
            st = con.prepareStatement(sql);
            st.setString(1, id_solicitud);
            st.setString(2, id_solicitud);
            rs = st.executeQuery();
            if (rs.next()) {
                respuestaVerificacion = rs.getString("repuesta");
            }
            if (respuestaVerificacion.equals("SI")) {
                comandoSQL1 = actualizarConsorcioProyectoEspecial(id_solicitud);
                comandos_sql.add(comandoSQL1);
                comandoSQL2 = actualizarSubclientesProyectoEspecial(id_solicitud);
                comandos_sql.add(comandoSQL2);
            }
        } catch (Exception e) {
            Util.imprimirTrace(e);
        } finally {
            st.close();
            this.desconectar(con);
        }
        return comandos_sql;
    }

    public String actualizarConsorcioProyectoEspecial(String id_solicitud)throws SQLException{
        StringStatement st = null;
        String comando_sql = "";
        String query = "SQL_ACTUALIZA_FACTURA_CONSORCIO_PE";
        try {
            st = new StringStatement(this.obtenerSQL(query), true);
            st.setString(1, id_solicitud);
            comando_sql = st.getSql();

        } catch (Exception e) {
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA ACTUALIZACION DE LA FACTURA CONSORCIO. \n " + e.getMessage());
        } finally {
            if (st != null) {
                try {
                    st = null;
                } catch (Exception e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
        }
        return comando_sql;
    }
    public String actualizarSubclientesProyectoEspecial(String id_solicitud)throws SQLException{
        StringStatement st = null;
        String comando_sql = "";
        String query = "SQL_ACTUALIZA_SUBCLIENTES_PE";
        try {
            st = new StringStatement(this.obtenerSQL(query), true);
            st.setString(1, id_solicitud);
            comando_sql = st.getSql();
            
        } catch (Exception e) {
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA ACTUALIZACION DE SUBCLIENTES. \n " + e.getMessage());
        } finally {
            if (st != null) {
                try {
                    st = null;
                } catch (Exception e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
        }
        return comando_sql;
    }
    
    public String actualizarItem(String documento, double diferencia, int item)throws SQLException{


        // PreparedStatement st       = null;
        // Connection        con      = null;
        StringStatement st         = null;

        String comando_sql  = "";

        String            query    = "SQL_ACTUALIZA_ITEM";

        try {
            // con          = this.conectarJNDI( query );
            // String sql   = obtenerSQL( query );
            // st           = con.prepareStatement( sql );

            st = new StringStatement (this.obtenerSQL(query), true);

            st.setDouble(1,diferencia );
            st.setDouble(2,diferencia );
            st.setDouble(3,diferencia );
            st.setDouble(4,diferencia );
            st.setString(5,documento );
            st.setInt(6,item);

            // comando_sql = st.toString();

            comando_sql = st.getSql();

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA ACTUALIZACION DEL ITEM POR AJUSTE. \n " + e.getMessage());
        }
        finally{
            // st.close();
            // this.desconectar(con);

            if (st  != null){
                try{ st = null;
                }
                catch(Exception e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
        }
        return comando_sql;
    }









    /**
     * Crea una lista de subclientes con solicitudes parciales  pendientes por facturar a los clientes
     */
    public void buscaSubclientePorFacturar(String id_solicitud, int parcial)throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs = null;


        String            query    = "SQL_BUSCA_SUBCLIENTE_POR_FACTURAR";
        listaSubclientePorFacturar = null;

        try {
            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );

            st.setString(1, id_solicitud );
            st.setString(2, id_solicitud );
            st.setInt(3, parcial );

            rs = st.executeQuery();

            listaSubclientePorFacturar =  new LinkedList();

            while (rs.next()){
                listaSubclientePorFacturar.add(SubclientePorFacturar.load(rs));
            }

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA SELECCION DE LOS SUBCLIENTES QUE SE FACTURAN PARA UNA SOLICITUD PARCIAL. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }
    }


    /**
     * Retorna una Lista de subclientes con solicitudes parciales  pendientes de facturar al cliente
     */
    public List getSubclientePorFacturar() {
        return listaSubclientePorFacturar;
    }




    /**
     * Crea los totales de las accion de una solicitud  parcial  pendientes por facturar a los clientes
     */
    public TotalAcciones buscaTotalAcciones(String id_solicitud, int parcial)throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs = null;


        String            query    = "SQL_TOTAL_ACCIONES";
        TotalAcciones totalAcciones  = null;

        try {
            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            st.setString(1, id_solicitud );
            st.setInt(2, parcial );

            rs = st.executeQuery();
            if(rs.next()){
                totalAcciones = new TotalAcciones();
                totalAcciones = (TotalAcciones.load(rs));
            }

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA SELECCION DEL TOTAL DE ACCIONES DE UNA SOLICITUD. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }
        return totalAcciones;
    }












    /**
     * Forma un comando sql para insertar la cabecera en cxp_doc
     * @prefactura Numero de prefactura a localizar
     */


    public String insertarFacturaConsorcio(FacturaClienteCuota facturaClienteCuota, String fecha_vencimiento)throws SQLException{


        // PreparedStatement st       = null;
        // Connection        con      = null;
        StringStatement st         = null;

        String comando_sql  = "";

        String            query    = "SQL_INSERTAR_FACTURA_CLIENTE_CONSORCIO";

        try {
            // con          = this.conectarJNDI( query );
            // String sql   = obtenerSQL( query );
            // st           = con.prepareStatement( sql );

            st = new StringStatement (this.obtenerSQL(query), true);

            st.setString(1,facturaClienteCuota.getFactura() );
            st.setString(2,facturaClienteCuota.getNit());
            st.setString(3,facturaClienteCuota.getId_cliente());
            st.setString(4,facturaClienteCuota.getFecha_financiacion() );
            st.setString(5,fecha_vencimiento );
            st.setDouble(6,facturaClienteCuota.getValor_factura() );
            st.setDouble(7,facturaClienteCuota.getValor_factura() - facturaClienteCuota.getValor_intereses() );
            st.setDouble(8,facturaClienteCuota.getValor_intereses());
            st.setString(9,facturaClienteCuota.getId_solicitud());
            st.setInt(10,facturaClienteCuota.getParcial());
            st.setString(11,facturaClienteCuota.getFactura());

            // comando_sql = st.toString();

            comando_sql = st.getSql();

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA CONFORMACION DEL COMANDO SQL PARA INSERTAR EN FACTURA CONSORCIO. \n " + e.getMessage());
        }
        finally{
            // st.close();
            // this.desconectar(con);

            if (st  != null){
                try{ st = null;
                }
                catch(Exception e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
        }
        return comando_sql;
    }














    /**
     * Forma un comando sql para insertar la cabecera en cxp_doc
     * @prefactura Numero de prefactura a localizar
     */


    public String actualizarFacturaConsorcio(FacturaClienteCuota facturaClienteCuota , String numeroFacturaCreadaLocalizada)throws SQLException{


        // PreparedStatement st       = null;
        // Connection        con      = null;
        StringStatement st         = null;

        String comando_sql  = "";

        String            query    = "SQL_ACTUALIZAR_FACTURA_CLIENTE_CONSORCIO";

        try {
            // con          = this.conectarJNDI( query );
            // String sql   = obtenerSQL( query );
            // st           = con.prepareStatement( sql );

            st = new StringStatement (this.obtenerSQL(query), true);

            st.setString(2,numeroFacturaCreadaLocalizada );
            st.setString(1,facturaClienteCuota.getFactura() );

            // comando_sql = st.toString();

            comando_sql = st.getSql();

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA CONFORMACION DEL COMANDO SQL PARA ACTUALIZAR EN FACTURA CONSORCIO. \n " + e.getMessage());
        }
        finally{
            // st.close();
            // this.desconectar(con);

            if (st  != null){
                try{ st = null;
                }
                catch(Exception e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
        }
        return comando_sql;
    }













    public String setSolicitudSubcliente(SubclientePorFacturar subclientePorFacturar, String factura) throws SQLException {


        // PreparedStatement st       = null;
        // Connection        con      = null;
        StringStatement st         = null;

        String comando_sql  = "";

        String            query    = "SQL_SET_SOLICITUD_SUBCLIENTE";

        try {
            // con          = this.conectarJNDI( query );
            // String sql   = obtenerSQL( query );
            // st           = con.prepareStatement( sql );

            st = new StringStatement (this.obtenerSQL(query), true);

            st.setString(1,factura);
            st.setString(2,subclientePorFacturar.getId_solicitud());
            st.setInt(3,subclientePorFacturar.getParcial());
            st.setString(4,subclientePorFacturar.getId_cliente());

            // comando_sql = st.toString();

            comando_sql = st.getSql();

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA CONFORMACION DEL COMANDO SQL PARA ACTUALIZAR LOS SUBCLIENTES DE UNA SOLICITUD PARCIAL. \n " + e.getMessage());
        }
        finally{
            // st.close();
            // this.desconectar(con);

            if (st  != null){
                try{ st = null;
                }
                catch(Exception e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
        }
        return comando_sql;



    }
    public String setActulizarAccion(String id_solicitud) throws SQLException {
        StringStatement st = null;
        String comando_sql = "";
        String query = "SQL_ACTUALIZAR_ACCION_FATURADO";
        try {
            st = new StringStatement(this.obtenerSQL(query), true);
            st.setString(1, id_solicitud);
            comando_sql = st.getSql();
        } catch (Exception e) {
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA CONFORMACION DEL COMANDO SQL PARA ACTUALIZAR LAS ACCIONES FACTURADAS. \n " + e.getMessage());
        }
        finally{
            if (st  != null){
                try{ st = null;
                }
                catch(Exception e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
        }
        return comando_sql;
    }





    /**
     * Localiza la tasa extemporanea
     */
    public double getTasaExtemporanea()throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs = null;

        double tasaExtemporanea = 0.00;

        String            query    = "SQL_GET_TASA_EXTEMPORANEA";


        try {
            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );

            rs = st.executeQuery();
            if (rs.next()){
                tasaExtemporanea = rs.getDouble("tasa_extemporanea");
            }

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA SELECCION DE LA TASA EXTEMPORANEA. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }
        return tasaExtemporanea;
    }





























    /**
     * Crea un objeto de los totales de las  acciones solicitudes parciales  pendientes por facturar a los clientes
     */
    public List buscaDetalleAcciones(String id_solicitud, int parcial)throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs = null;

        List listaDetalleAcciones  = null;

        String            query    = "SQL_DETALLE_ACCIONES";
        DetalleAcciones detalleAcciones  = null;

        try {
            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            st.setString(1, id_solicitud );
            st.setInt(2, parcial );

            listaDetalleAcciones = new LinkedList();
            detalleAcciones = new DetalleAcciones();

            rs = st.executeQuery();
            while(rs.next()){

                detalleAcciones = (DetalleAcciones.load(rs));
                listaDetalleAcciones.add(detalleAcciones);
            }

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA SELECCION DEL DETALLE DE LAS ACCIONES DE UNA SOLICITUD. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }
        return listaDetalleAcciones;
    }



    /**
     * Busca el cliente de una oferta. Si es valor agregado devuelve el id_cliente de Electricaribe
     */
    public String getClienteValorAgregado(String id_solicitud)throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet         rs       = null;


        String            query    = "SQL_GET_CLIENTE_VALOR_AGREGADO";
        String id_cliente  = "";

        try {
            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            st.setString(1, id_solicitud );

            rs = st.executeQuery();
            if(rs.next()){
                id_cliente = rs.getString("id_cliente");
            }

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL CLIENTE DE UN VALOR AGREGADO. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }
        return id_cliente;
    }





    /**
     * Busca el numero de factura para una ya creada en Factura Consorcio
     */
    public String buscaNumeroFactura(String id_solicitud, int parcial, String id_cliente)throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs = null;


        String            query    = "SQL_FACTURA_CONSORCIO";
        String    numeroFactura    = "";

        try {
            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            st.setString(1, id_solicitud );
            st.setInt(2, parcial );
            st.setString(3, id_cliente );

            rs = st.executeQuery();
            if(rs.next()){
                numeroFactura =  rs.getString("documento");
            }

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE UN NUMERO DE FACTURA. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }
        return numeroFactura;
    }




    /**
     * Busca el numero de factura para una ya creada en Factura Consorcio
     */
    public String getNumeroFacturaConsorcio(String id_solicitud, int parcial, String id_cliente)throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs = null;


        String            query    = "SQL_GET_FACTURA_CONSORCIO";
        String    numeroFactura    = "";

        try {
            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            st.setString(1, id_solicitud );
            st.setInt(2, parcial );
            st.setString(3, id_cliente );

            rs = st.executeQuery();
            if(rs.next()){
                numeroFactura =  rs.getString("documento") + "%" + rs.getString("documento_corficolombiana");
            }

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE UN NUMERO DE FACTURA CONSORCIO FACTURA. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }
        return numeroFactura;
    }

















    /**
     * Busca el numero de factura para una ya creada en Factura Consorcio
     */
    public String buscaNumeroFacturaFintra(String id_solicitud, int parcial, String id_cliente)throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs = null;


        String            query    = "SQL_FACTURA_CONSORCIO_GENERAL";
        String    numeroFacturaCorficolombiana    = "";

        try {
            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            st.setString(1, id_solicitud );
            st.setInt(2, parcial );
            st.setString(3, id_cliente );

            rs = st.executeQuery();
            if(rs.next()){
                numeroFacturaCorficolombiana =  rs.getString("documento_corficolombiana");
            }

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE UN NUMERO DE FACTURA GENERAL. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }
        return numeroFacturaCorficolombiana;
    }








    public String setFacturaConsorcio(String facturaConsorcio, String facturaCorficolombiana) throws SQLException {


        // PreparedStatement st       = null;
        // Connection        con      = null;
        StringStatement st         = null;

        String comando_sql  = "";

        String            query    = "SQL_ACTUALIZA_FACTURA_CONSORCIO";

        try {
            // con          = this.conectarJNDI( query );
            // String sql   = obtenerSQL( query );
            // st           = con.prepareStatement( sql );

            st = new StringStatement (this.obtenerSQL(query), true);

            st.setString(1,facturaCorficolombiana);
            st.setString(2,facturaConsorcio);

            // comando_sql = st.toString();

            comando_sql = st.getSql();

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA ACTUALIZACION DE LA FACTURA DEL CONSORCIO CON EL NUMERO DE FACTURA A CORFICOLOMBIANA O FACTURA DE FINTRA. \n " + e.getMessage());
        }
        finally{
            // st.close();
            // this.desconectar(con);

            if (st  != null){
                try{ st = null;
                }
                catch(Exception e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
        }
        return comando_sql;

    }





    /**
     * Convierte un codigo en otro codigo equivalente
     * @param ciaInicial Compania inicial desde donde se envia el codigo
     * @param ciaFinal   Comapania final en donde queda el codigo nuevo
     * @param claseInicial  Tipo de equivalencia a realizar
     * @param codigoInicial  Codigo a convertir en un codigo equivalente
     * @return
     * @throws SQLException
     */
    public Equivalencia getEquivalencia(String ciaInicial, String ciaFinal, String claseInicial, String codigoInicial)throws SQLException{

        PreparedStatement st  = null;
        Connection        con = null;
        ResultSet         rs  = null;

        String            query       = "SQL_GET_EQUIVALENCIA";
        Equivalencia equivalencia       = null;

        try {
            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            st.setString( 1, ciaInicial );
            st.setString( 2, ciaFinal );
            st.setString( 3, claseInicial );
            st.setString( 4, codigoInicial );
            rs = st.executeQuery();

            if(rs.next()){
                equivalencia = new Equivalencia();
                equivalencia = (Equivalencia.load(rs));
            }

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA CONSECUCION DE UN CODIGO EQUIVALENTE. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }
        return equivalencia;
    }


















    // *************************************************************************
    // *
    // *  AREA DE METODOS DEL PROCESO DE CONTRATISTA
    // *
    // *************************************************************************




     /**
      * Genera una lista de todas las acciones de un contratista que no han sido prefacturadas
      * @param id_contratista Identificacion del contratista, codigo interno
     * @param tipo
      * @return Lista de objetos PrefacturaContratista
      * @throws SQLException
      */
    public List getAccionesContratista(String id_contratista,int tipo )throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs = null;


        String            query         =(tipo==1)? "SQL_ACCIONES_CONTRATISTA":"SQL_ACCIONES_CONTRATISTA_2";
        List listaAccionesContratista = null;

        try {


            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            st.setString( 1, id_contratista );

            rs = st.executeQuery();

            listaAccionesContratista =  new LinkedList();

            while (rs.next()){
                listaAccionesContratista.add(AccionContratista.load(rs,tipo));
            }

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA SELECCION DE REGISTROS A PREFACTURAR PARA EL CONTRATISTA. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }
        return listaAccionesContratista;
    }




    public String getFechaFactura(String distrito, String tipoDocumento , String facturaCliente ) throws SQLException{

        PreparedStatement st  = null;
        Connection        con = null;
        ResultSet         rs  = null;

        String            query       = "SQL_GET_FECHA_FACTURA";
        String            fechaFactura = "";

        try {
            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            st.setString( 1, distrito );
            st.setString( 2, tipoDocumento);
            st.setString( 3, facturaCliente+"%");
            rs = st.executeQuery();

            if(rs.next()){
                fechaFactura = rs.getString("fecha_factura");
            }

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LA FECHA DE LA FACTURA AL CLIENTE. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }
        return fechaFactura;
    }












    public Contratista getContratista(String id_contratista)throws SQLException{

        PreparedStatement st  = null;
        Connection        con = null;
        ResultSet         rs  = null;

        String            query       = "SQL_CONTRATISTA";
        Contratista contratista       = null;

        try {
            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            st.setString( 1, id_contratista );

            rs = st.executeQuery();

            if(rs.next()){
                contratista = new Contratista();
                contratista = (Contratista.load(rs));
            }


        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL NOMBRE DEL CONTRATISTA. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }
        return contratista;
    }




    /**
     * Localiza impuesto
     * @param codigo_general_impuesto Codigo del impuesto en tabla RTCONTRATO para un impuesto particular
     * @param distrito                Sigla de la compania
     * @param tipo_impuesto           Tipo de impuesto (IVA,RFTE,RICA,RIVA en tabla de impuestos
     */

    public void buscaImpuestoContrato(String codigo_general_impuesto,String distrito,String tipo_impuesto)throws SQLException{

        PreparedStatement st  = null;
        Connection        con = null;
        ResultSet         rs  = null;

        String            query       = "SQL_IMPUESTO";
        impuestoContrato              = null;

        try {
            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            st.setString( 1, codigo_general_impuesto );
            st.setString( 2, distrito );
            st.setString( 3, tipo_impuesto );

            rs = st.executeQuery();

            if(rs.next()){
                impuestoContrato = new ImpuestoContrato();
                impuestoContrato = (ImpuestoContrato.load(rs));
            }


        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL IMPUESTO. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }
    }

    public ImpuestoContrato buscaIva( String regimen, String distrito, String fechaVigencia )throws SQLException{

        PreparedStatement st  = null;
        Connection        con = null;
        ResultSet         rs  = null;

        String            query       = "SQL_IMPUESTO_IVA";
        ImpuestoContrato impuestoIva  = null;

        try {
            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            st.setString( 1, regimen );
            st.setString( 2, distrito);
            st.setString( 3, fechaVigencia);
            rs = st.executeQuery();

            if(rs.next()){
                impuestoIva = new ImpuestoContrato();
                impuestoIva = (ImpuestoContrato.loadIva(rs));
            }

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL IMPUESTO DEL IVA. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }
        return impuestoIva;
    }



    public ImpuestoContrato buscaRiva( String regimen, String distrito, String fechaVigencia )throws SQLException{

        PreparedStatement st  = null;
        Connection        con = null;
        ResultSet         rs  = null;

        String            query       = "SQL_IMPUESTO_RIVA";
        ImpuestoContrato impuestoRiva  = null;

        try {
            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            st.setString( 1, regimen );
            st.setString( 2, distrito);
            st.setString( 3, fechaVigencia);
            st.setString( 4, fechaVigencia);

            rs = st.executeQuery();

            if(rs.next()){
                impuestoRiva = new ImpuestoContrato();
                impuestoRiva = (ImpuestoContrato.loadIva(rs));
            }

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL IMPUESTO DEL RIVA. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }
        return impuestoRiva;
    }










    public ImpuestoContrato buscaReteica( String domicilioComercial, String actividad, String distrito, String aiu, String fechaVigencia )throws SQLException{

        PreparedStatement st  = null;
        Connection        con = null;
        ResultSet         rs  = null;

        String            query       = "SQL_IMPUESTO_RETEICA";
        ImpuestoContrato impuestoIva  = null;

        try {
            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            st.setString( 1, domicilioComercial + " " + actividad + " " + aiu);
            st.setString( 2, distrito);
            st.setString( 3, fechaVigencia);
            rs = st.executeQuery();

            if(rs.next()){
                impuestoIva = new ImpuestoContrato();
                impuestoIva = (ImpuestoContrato.loadIva(rs));
            }

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL IMPUESTO DEL RETEICA. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }
        return impuestoIva;
    }






    public ImpuestoContrato buscaRetefuente( String distrito, String concepto, String aiu, String fechaVigencia )throws SQLException{

        PreparedStatement st  = null;
        Connection        con = null;
        ResultSet         rs  = null;

        String            query       = "SQL_IMPUESTO_RETEFUENTE";
        ImpuestoContrato impuestoRetefuente  = null;

        try {
            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            st.setString( 1,concepto + " " + aiu );
            st.setString( 2, distrito);
            st.setString( 3, fechaVigencia);
            rs = st.executeQuery();

            if(rs.next()){
                impuestoRetefuente = new ImpuestoContrato();
                impuestoRetefuente = (ImpuestoContrato.loadIva(rs));
            }

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL IMPUESTO DE RETEFUENTES. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }
        return impuestoRetefuente;
    }



    /**
     * Retorna el valor de la comision
     */

    public ImpuestoContrato getImpuestoContrato() {

        return impuestoContrato;
    }





    /**
     * Actualiza la secuencia de una prefactura para un contratista
     * @param id_contratista Codigo del contratista

     */


    public String setSecuenciaPrefactura(String id_contratista)throws SQLException{

        // PreparedStatement st       = null;
        // Connection        con      = null;

        StringStatement st         = null;

        String comando_sql  = "";

        String            query       = "SQL_SET_SECUENCIA_PREFACTURA";

        try {

            st = new StringStatement (this.obtenerSQL(query), true);
            st.setString( 1, id_contratista );

            comando_sql = st.getSql();

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA ACTUALIZACION DE LA SECUENCIA DE LA PREFACTURA. \n " + e.getMessage());
        }
        finally{

            if (st  != null){
                try{ st = null;
                }
                catch(Exception e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
        }

        return comando_sql;

    }





    /**
     * Actualiza las acciones seleccionadas para prefacturar
     * @param id_accion Identificacion de la accion
     * @param prefactura Codigo de prefactura

     */


    public String setAccionContratista(String id_accion, String prefactura_contratista,
                                       String usuario_prefactura_contratista, String cod_iva_contratista,
                                       double por_iva_contratista,double val_base_iva_contratista, double val_iva_contratista,
                                       String cod_ret_material_contratista, double por_ret_material_contratista,
                                       double val_ret_material_contratista, String cod_ret_mano_obra_contratista,
                                       double por_ret_mano_obra_contratista, double val_ret_mano_obra_contratista,
                                       String cod_ret_otros_contratista, double por_ret_otros_contratista,
                                       double val_ret_otros_contratista,
                                       String  cod_ret_aiu_contratista, double por_ret_aiu_contratista, double val_ret_aiu_contratista,
                                       double por_factoring_contratista,
                                       double val_factoring_contratista, double por_formula_contratista,
                                       double val_formula_contratista,String cod_ret_ica_contratista,
                                       double por_ret_ica_contratista,double val_ret_ica_contratista,
                                       String cod_ret_iva_contratista,double por_ret_iva_contratista,
                                       double val_ret_iva_contratista)throws SQLException{

        // PreparedStatement st       = null;
        // Connection        con      = null;
        StringStatement st         = null;

        String comando_sql  = "";

        String            query    = "SQL_SET_ACCION_CONTRATISTA";


        try {

            // con          = this.conectarJNDI( query );
            // String sql   = obtenerSQL( query );
            // st           = con.prepareStatement( sql );

            st = new StringStatement (this.obtenerSQL(query), true);

            st.setString(1, prefactura_contratista );
            st.setString(2, usuario_prefactura_contratista );
            st.setString(3, cod_iva_contratista );
            st.setDouble(4, por_iva_contratista );
            st.setDouble(5, val_base_iva_contratista );
            st.setDouble(6, val_iva_contratista );
            st.setString(7, cod_ret_material_contratista );
            st.setDouble(8, por_ret_material_contratista );
            st.setDouble(9, val_ret_material_contratista );
            st.setString(10,cod_ret_mano_obra_contratista );
            st.setDouble(11,por_ret_mano_obra_contratista );
            st.setDouble(12,val_ret_mano_obra_contratista );
            st.setString(13,cod_ret_otros_contratista );
            st.setDouble(14,por_ret_otros_contratista );
            st.setDouble(15,val_ret_otros_contratista );
            st.setDouble(16,por_factoring_contratista );
            st.setDouble(17,val_factoring_contratista);
            st.setDouble(18,por_formula_contratista );
            st.setDouble(19,val_formula_contratista);
            st.setString(20,cod_ret_ica_contratista);
            st.setDouble(21,por_ret_ica_contratista);
            st.setDouble(22,val_ret_ica_contratista );
            st.setString(23,cod_ret_iva_contratista);
            st.setDouble(24,por_ret_iva_contratista);
            st.setDouble(25,val_ret_iva_contratista);

            st.setString(26,cod_ret_aiu_contratista);
            st.setDouble(27,por_ret_aiu_contratista);
            st.setDouble(28,val_ret_aiu_contratista);

            st.setString(29,id_accion );

            // comando_sql = st.toString();

            comando_sql = st.getSql();

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA ACTUALIZACION DE LAS ACCIONES AL PREFACTURAR. \n " + e.getMessage());
        }
        finally{
            // st.close();
            // this.desconectar(con);

            if (st  != null){
                try{ st = null;
                }
                catch(Exception e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
        }
        return comando_sql;
    }








    /**
     * Lista de las acciones para realizar las facturas internas de proveintegral
     */


    public List getAccionesProvintegral(String distrito, LogWriter logWriter)throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs = null;

        String            query    = "SQL_GET_ACCIONES_PROVINTEGRAL";
        List listaAccionProvintegral   = null;

        try {

            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            rs = st.executeQuery();

            listaAccionProvintegral =  new LinkedList();

            while (rs.next()){
                listaAccionProvintegral.add(AccionProvintegral.load(rs));
            }

        }catch(Exception e){
            logWriter.log(e.getMessage(),1);
            throw new SQLException("ERROR DURANTE LA SELECCION DE LAS ACCIONES DE PROVINTEGRAL. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }
        return listaAccionProvintegral;
    }





    public List getFacturaProvintegral(LogWriter logWriter)throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs = null;

        String            query    = "SQL_GET_FACTURA_PROVINTEGRAL";
        List listaFacturaProvintegral   = null;

        try {

            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            rs = st.executeQuery();

            listaFacturaProvintegral =  new LinkedList();

            while (rs.next()){
                listaFacturaProvintegral.add(rs.getString("fc_provintegral") ) ;
            }

        }catch(Exception e){
         logWriter.log(e.getMessage(),1);
            throw new SQLException("ERROR DURANTE LA SELECCION DE LAS FACTURAS DE PROVINTEGRAL. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }
        return listaFacturaProvintegral;
    }


    public List getFacturaInternaProvintegral(String facturaConformada, LogWriter logWriter)throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs = null;

        String            query    = "SQL_GET_FACTURA_INTERNA_PROVINTEGRAL";
        List listaFacturaInternaProvintegral   = null;

        try {

            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );

            st.setString(1,facturaConformada);
            rs = st.executeQuery();

            listaFacturaInternaProvintegral =  new LinkedList();

            while (rs.next()){
                listaFacturaInternaProvintegral.add(rs.getString("registro") ) ;
            }

        }catch(Exception e){
            logWriter.log(e.getMessage(),1);
            throw new SQLException("ERROR DURANTE LA SELECCION DE LAS FACTURAS INTERNA DE PROVINTEGRAL. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }
        return listaFacturaInternaProvintegral;
    }



    /**
     * Lista resumida de las prefacturas de un contratista para facturarlas
     */


    public List getFacturaGeneral()throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs = null;

        String            query    = "SQL_FACTURA_GENERAL";
        List listaFacturaGeneral        = null;

        try {

            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            rs = st.executeQuery();

            listaFacturaGeneral =  new LinkedList();

            while (rs.next()){
                listaFacturaGeneral.add(FacturaGeneral.load(rs));
            }

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA SELECCION DE LAS PREFACTURAS PARA FACTURAR AL CONTRATISTA. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }
        return listaFacturaGeneral;
    }





    /**
     * Lista resumida de las prefacturas de un contratista para facturarlas
     */


    public String  setNcProvintegral( String documento, LogWriter logWriter )throws SQLException{

        StringStatement st         = null;
        String comando_sql         = "";
        String            query    = "SQL_SET_NC_PROVINTEGRAL";

        try {

            st = new StringStatement (this.obtenerSQL(query), true);

            st.setString(1,documento);
            st.setString(2,documento);


            comando_sql = st.getSql();

        }catch(Exception e){
             logWriter.log(e.getMessage(),1);
            throw new SQLException("ERROR DURANTE LA CONFORMACION DEL COMANDO SQL PARA ACTUALIZAR ACCIONES CON NOTAS CREDITO DE PROVINTEGRAL. \n " + e.getMessage());
        }
        finally{

            if (st  != null){
                try{ st = null;
                }
                catch(Exception e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
        }
        return comando_sql;
    }



    public String  setFechaFacturaConformadaProvintegral(  String documento, String fechaDocumento, LogWriter logWriter )throws SQLException{

        StringStatement st         = null;
        String comando_sql         = "";
        String            query    = "SQL_SET_FECHA_FACTURA_CONFORMADA_PROVINTEGRAL";

        try {

            st = new StringStatement (this.obtenerSQL(query), true);

            st.setString(1,fechaDocumento);
            st.setString(2,documento);


            comando_sql = st.getSql();

        }catch(Exception e){
        logWriter.log(e.getMessage(),1);
            throw new SQLException("ERROR EN LA ACTUALIZACION DE LA ACCION CON LA FECHA DE LA FACTURA CONFORMADA DE PROVINTEGRAL. \n " + e.getMessage());
        }
        finally{

            if (st  != null){
                try{ st = null;
                }
                catch(Exception e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
        }
        return comando_sql;
    }














    public String setCxp_doc(String dstrct,String proveedor, String tipo_documento,String documento,
                             String descripcion,String agencia,
                             String handle_code, String fecha_aprobacion,
                             String aprobador,String usuario_aprobacion,
                             String banco,String sucursal,String moneda, Double vlr_neto,
                             Double vlr_total_abonos,Double vlr_saldo,Double vlr_neto_me,Double vlr_total_abonos_me,
                             Double vlr_saldo_me, Double tasa,String observacion,String user_update,
                             String creation_user,String base,String clase_documento,String moneda_banco,
                             String fecha_documento,String fecha_vencimiento,String clase_documento_rel,
                             String last_update, String creation_date,
                             String tipo_referencia_1, String referencia_1, String tipo_referencia_2, String referencia_2,
                             String tipo_documento_rel,  String documento_relacionado,
                             String tipo_referencia_3, String referencia_3, String indicador_traslado_fintra )throws SQLException{

        StringStatement st         = null;
        String comando_sql         = "";
        String            query    = "SQL_INSERTAR_CXP_DOC";

        try {

            st = new StringStatement (this.obtenerSQL(query), true);


            st.setString(1,dstrct);
            st.setString(2,proveedor);
            st.setString(3,tipo_documento);
            st.setString(4,documento);
            st.setString(5,descripcion);
            st.setString(6,agencia);
            st.setString(7,handle_code);
            st.setString(8,aprobador);
            st.setString(9,usuario_aprobacion);
            st.setString(10,banco);
            st.setString(11,sucursal);
            st.setString(12,moneda);
            st.setDouble(13,vlr_neto);
            st.setDouble(14,vlr_total_abonos);
            st.setDouble(15,vlr_saldo);
            st.setDouble(16,vlr_neto_me);
            st.setDouble(17,vlr_total_abonos_me);
            st.setDouble(18,vlr_saldo_me);
            st.setDouble(19,tasa);
            st.setString(20,observacion);
            st.setString(21,user_update);
            st.setString(22,creation_user);
            st.setString(23,base);
            st.setString(24,clase_documento);
            st.setString(25,moneda_banco);
            st.setString(26,fecha_documento);
            st.setString(27,fecha_vencimiento);
            st.setString(28,clase_documento_rel);
            st.setString(29,last_update);
            st.setString(30,creation_date);
            st.setString(31,tipo_referencia_1);
            st.setString(32,referencia_1);
            st.setString(33,tipo_referencia_2);
            st.setString(34,referencia_2);
            st.setString(35,tipo_documento_rel);
            st.setString(36,documento_relacionado);
            st.setString(37,tipo_referencia_3);
            st.setString(38,referencia_3);
            st.setString(39,indicador_traslado_fintra);
            st.setString(40,fecha_aprobacion);

            comando_sql = st.getSql();

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA CONFORMACION DEL COMANDO SQL PARA INSERTAR EN CXP_DOC. \n " + e.getMessage());
        }
        finally{

            if (st  != null){
                try{ st = null;
                }
                catch(Exception e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
        }
        return comando_sql;


    }



















    /**
     * Forma un comando sql para insertar la cabecera en cxp_doc
     * @prefactura Numero de prefactura a localizar
     */


    public String setCxp_items_doc(String dstrct, String proveedor, String tipo_documento,
                                   String documento, String item,String descripcion,
                                   double vlr, double vlr_me, String codigo_cuenta,
                                   String user_update,
                                   String creation_user, String base,
                                   String concepto, String auxiliar,
                                   String last_update, String creation_date,
                                   String tipo_referencia_1, String referencia_1, String tipo_referencia_2, String referencia_2,
                                   String tipo_referencia_3, String referencia_3)throws SQLException{


        // PreparedStatement st       = null;
        // Connection        con      = null;
        StringStatement st         = null;

        String comando_sql  = "";

        String            query    = "SQL_INSERTAR_CXP_ITEMS_DOC";

        try {
            // con          = this.conectarJNDI( query );
            // String sql   = obtenerSQL( query );
            // st           = con.prepareStatement( sql );

            st = new StringStatement (this.obtenerSQL(query), true);

            st.setString(1,dstrct);
            st.setString(2,proveedor);
            st.setString(3,tipo_documento);
            st.setString(4,documento);
            st.setString(5,item);
            st.setString(6,descripcion);
            st.setDouble(7,vlr);
            st.setDouble(8,vlr_me);
            st.setString(9,codigo_cuenta);
            st.setString(10,user_update);
            st.setString(11,creation_user);
            st.setString(12,base);
            st.setString(13,concepto);
            st.setString(14,auxiliar);
            st.setString(15,last_update);
            st.setString(16,creation_date);
            st.setString(17,tipo_referencia_1);
            st.setString(18,referencia_1);
            st.setString(19,tipo_referencia_2);
            st.setString(20,referencia_2);

            // comando_sql = st.toString();

            comando_sql = st.getSql();


        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA CONFORMACION DEL COMANDO SQL PARA INSERTAR EN CXP_ITEMS_DOC. \n " + e.getMessage());
        }
        finally{
            // st.close();
            // this.desconectar(con);

            if (st  != null){
                try{ st = null;
                }
                catch(Exception e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
        }
        return comando_sql;
    }








    /**
     * Forma un comando sql para actualizar la factura del contratista en las acciones
     * @id_contratista Identificacion del contratista
     * @prefactura Numero de prefactura facturada
     */


    public String setFacturaContratista(String factura_contratista_fintra, String id_accion)throws SQLException{

        // PreparedStatement st       = null;
        // Connection        con      = null;
        StringStatement st         = null;

        String comando_sql  = "";

        String            query    = "SQL_ACTUALIZA_FACTURA_CONTRATISTA";

        try {
            // con          = this.conectarJNDI( query );
            // String sql   = obtenerSQL( query );
            // st           = con.prepareStatement( sql );

            st = new StringStatement (this.obtenerSQL(query), true);

            st.setString(1,factura_contratista_fintra);
            st.setString(2,id_accion);


            // comando_sql = st.toString();

            comando_sql = st.getSql();

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA CONFORMACION DEL COMANDO SQL PARA ACTUALIZAR FECHA FACTURA CONTRATISTA. \n " + e.getMessage());
        }
        finally{
            // st.close();
            // this.desconectar(con);

            if (st  != null){
                try{ st = null;
                }
                catch(Exception e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
        }
        return comando_sql;
    }




    /**
     * Forma un comando sql para actualizar la accion con la factura interna de Provintegral
     * @id_accion Identifica la accion que se actualizara con el numero de factura interna de provintegral
     */


    public String setFacturaProvintegral(String id_accion)throws SQLException{

        // PreparedStatement st       = null;
        // Connection        con      = null;
        StringStatement st         = null;

        String comando_sql  = "";

        String            query    = "SQL_ACTUALIZA_ACCION_PROVINTEGRAL";

        try {

            st = new StringStatement (this.obtenerSQL(query), true);

            st.setString(1,id_accion);
            st.setString(2,id_accion);

            comando_sql = st.getSql();

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA CONFORMACION DEL COMANDO SQL PARA ACTUALIZAR LA ACCION CON FACTURA INTERNA DE PROVINTEGRAL. \n " + e.getMessage());
        }
        finally{

            if (st  != null){
                try{ st = null;
                }
                catch(Exception e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
        }
        return comando_sql;
    }



















    /**
     * Lista de facturas de cxp
     */


    public List getFacturaCxPCabecera(String condicion)throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs = null;

        String            query    = "SQL_GET_FACTURA_CABECERA_CXP";
        List listaFacturaCabeceraCxP  = null;

        try {

            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );

            sql = sql.replaceAll("#CONDICION#", condicion);

            st           = con.prepareStatement( sql );

            rs = st.executeQuery();

            listaFacturaCabeceraCxP =  new LinkedList();

            while (rs.next()){


               listaFacturaCabeceraCxP.add(FacturaCabeceraCxP.load(rs));
            }

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA SELECCION DE LA CABECERA DE LAS FACTURAS CXP. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }
        return listaFacturaCabeceraCxP;
    }


    public List getFacturaCxPCabecera(String condicion, String dataBaseName)throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs = null;

        String            query    = "SQL_GET_FACTURA_CABECERA_CXP";
        List listaFacturaCabeceraCxP  = null;

        try {

            con   = this.conectarJNDI( query, dataBaseName );
            String  sql  = obtenerSQL( query );

            sql = sql.replaceAll("#CONDICION#", condicion);

            st           = con.prepareStatement( sql );

            rs = st.executeQuery();

            listaFacturaCabeceraCxP =  new LinkedList();

            while (rs.next()){


               listaFacturaCabeceraCxP.add(FacturaCabeceraCxP.load(rs));
            }

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA SELECCION DE LA CABECERA DE LAS FACTURAS CXP. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }
        return listaFacturaCabeceraCxP;
    }




    /**
     * Lista de facturas de cxp
     */


    public List getFacturaCxPDetalle(String condicion)throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs = null;

        String            query    = "SQL_GET_FACTURA_DETALLE_CXP";
        List listaFacturaDetalleCxP  = null;

        try {

            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );

            sql = sql.replaceAll("#CONDICION#", condicion);

            st           = con.prepareStatement( sql );

            rs = st.executeQuery();

            listaFacturaDetalleCxP =  new LinkedList();

            while (rs.next()){


               listaFacturaDetalleCxP.add(FacturaDetalleCxP.load(rs));
            }

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA SELECCION DE LOS ITEMS DE LAS FACTURAS CXP. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }
        return listaFacturaDetalleCxP;
    }





    /**
     * Actualiza una accion con un numero de nota credito correspondiente a la cancelacion de la factura interna
     * @nc Numero de la nota credito
     * @id_accion Numero de la accion que se actualiza
     */


    public String setAccionNC(String nc, String id_accion)throws SQLException{


        StringStatement st         = null;

        String comando_sql  = "";

        String            query    = "SQL_SET_ACCION_NC";

        try {


            st = new StringStatement (this.obtenerSQL(query), true);

            st.setString(1,nc);
            st.setString(2,id_accion);

            comando_sql = st.getSql();

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA ACTUALIZACION DE LA ACCION CON LA NOTA CREDITO A LA FACTURA INTERNA. \n " + e.getMessage());
        }
        finally{
            // st.close();
            // this.desconectar(con);

            if (st  != null){
                try{ st = null;
                }
                catch(Exception e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
        }
        return comando_sql;
    }




    public String setCancelaCxP(String dstrct, String proveedor, String tipoDocumento , String documento )throws SQLException{


        StringStatement st         = null;

        String comando_sql  = "";

        String            query    = "SQL_SET_CANCELA_CXP";

        try {


            st = new StringStatement (this.obtenerSQL(query), true);

            st.setString(1,dstrct);
            st.setString(2,proveedor);
            st.setString(3,tipoDocumento);
            st.setString(4,documento);

            comando_sql = st.getSql();

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA ACTUALIZACION DE LA FACTURA CXP CANCELADA. \n " + e.getMessage());
        }
        finally{
            // st.close();
            // this.desconectar(con);

            if (st  != null){
                try{ st = null;
                }
                catch(Exception e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
        }
        return comando_sql;
    }






    public String setAbonaCxP(String dstrct, String proveedor, String tipoDocumento , String documento, double valor_abono )throws SQLException{


        StringStatement st         = null;

        String comando_sql  = "";

        String            query    = "SQL_SET_ABONA_CXP";

        try {


            st = new StringStatement (this.obtenerSQL(query), true);


            st.setDouble(1,valor_abono);
            st.setDouble(2,valor_abono);
            st.setDouble(3,valor_abono);
            st.setDouble(4,valor_abono);
            st.setString(5,dstrct);
            st.setString(6,proveedor);
            st.setString(7,tipoDocumento);
            st.setString(8,documento);

            comando_sql = st.getSql();

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA ACTUALIZACION DE LA FACTURA CXP ABONADA. \n " + e.getMessage());
        }
        finally{
            // st.close();
            // this.desconectar(con);

            if (st  != null){
                try{ st = null;
                }
                catch(Exception e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
        }
        return comando_sql;
    }














    public String setCancelaFacturaCxP(String dstrct, String proveedor, String tipoDocumento , String documento, String camposAdicionales  )throws SQLException{


        StringStatement st         = null;

        String comando_sql  = "";

        String            query    = "SQL_SET_CANCELA_FACTURA_CXP";

        try {

            String  sql  = obtenerSQL( query );
            sql = sql.replaceAll("#CAMPOS_ADICIONALES#", camposAdicionales);

            st = new StringStatement (sql, true);

            st.setString(1,dstrct);
            st.setString(2,proveedor);
            st.setString(3,tipoDocumento);
            st.setString(4,documento);

            comando_sql = st.getSql();

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA ACTUALIZACION DE LA FACTURA CXP CANCELADA. \n " + e.getMessage());
        }
        finally{


            if (st  != null){
                try{ st = null;
                }
                catch(Exception e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
        }
        return comando_sql;
    }







    public String setActualizaCxP(String dstrct, String proveedor, String tipoDocumento , String documento, String datos )throws SQLException{


        StringStatement st         = null;

        String comando_sql  = "";
        String sql = "";

        String            query    = "SQL_SET_ACTUALIZA_CXP";

        try {


            sql = this.obtenerSQL(query);
            sql = sql.replaceAll("#DATOS#", datos);

            st = new StringStatement (sql, true);

            st.setString(1,dstrct);
            st.setString(2,proveedor);
            st.setString(3,tipoDocumento);
            st.setString(4,documento);

            comando_sql = st.getSql();

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA ACTUALIZACION DE LA FACTURA CXP. \n " + e.getMessage());
        }
        finally{
            // st.close();
            // this.desconectar(con);

            if (st  != null){
                try{ st = null;
                }
                catch(Exception e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
        }
        return comando_sql;
    }









    /**
     * Lista resumida de las prefacturas de un contratista para facturarlas
     */


    public List getFacturaContratistaFintra()throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs = null;

        String            query    = "SQL_GET_FACTURA_CONTRATISTA_FINTRA";
        List listaFacturaContratistafintra        = null;

        try {

            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            rs = st.executeQuery();

            listaFacturaContratistafintra =  new LinkedList();

            while (rs.next()){


                listaFacturaContratistafintra.add(rs.getString("contratista") + "%" +
                                                  rs.getString("nit")  +   "%" +
                                                  rs.getString("factura_contratista_fintra") );
            }

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA SELECCION DE LAS FACTURAS CONTRATISTAS FINTRA PARA CREAR NOTA CREDITO. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }
        return listaFacturaContratistafintra;
    }






    public ArrayList getFacturaContratistaDefinitiva()throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs = null;

        String            query      = "SQL_GET_FACTURA_CONTRATISTA_DIFINITIVA";
        ArrayList listaFacturaContratista = null;

        try {

            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            rs = st.executeQuery();

            listaFacturaContratista =  new ArrayList();

            while (rs.next()){
                listaFacturaContratista.add(rs.getString("contratista") + "%"
                        + rs.getString("factura_contratista") + "%" + rs.getString("tipodistribucion") + "%" + rs.getString("num_os"));
            }

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA SELECCION DE LAS FACTURAS CONTRATISTAS FINTRA PARA CREAR FACTURA FINAL. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }
        return listaFacturaContratista;
    }



    public List getFacturaProvintegralConformada()throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs = null;

        String            query      = "SQL_GET_FACTURA_PROVINTEGRAL_CONFORMADA";
        List listaFacturaProvintegralConformada = null;

        try {

            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            rs = st.executeQuery();

            listaFacturaProvintegralConformada =  new LinkedList();

            while (rs.next()){
                listaFacturaProvintegralConformada.add(rs.getString("fc_provintegral") ) ;
            }

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA SELECCION DE LAS FACTURAS CONFORMADAS DE PROVINTEGRAL PARA CREAR FACTURA FINAL. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }
        return listaFacturaProvintegralConformada;
    }














    public ArrayList getFacturaCxpInterna(String codigo_contratista, String factura_contratista)throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs = null;

        String            query      = "SQL_GET_FACTURA_INTERNA";
        ArrayList listaFacturaInterna = null;

        try {

            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );

            st           = con.prepareStatement( sql );
            st.setString(1,codigo_contratista);
            st.setString(2,factura_contratista);

            rs = st.executeQuery();

            listaFacturaInterna =  new ArrayList();

            while (rs.next()){
                listaFacturaInterna.add(rs.getString("nit") + "%" +
                                        rs.getString("factura_contratista_fintra")  );
            }

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA SELECCION DE LAS FACTURAS INTERNAS DEL CONTRATISTA FINTRA PARA CREAR FACTURA FINAL. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }
        return listaFacturaInterna;
    }





    public String getNit(String id_cliente, String dataBaseName)throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs = null;

        String query = "SQL_GET_NIT";
        String nit   =  "";

        try {

            con   = this.conectarJNDI( query, dataBaseName );
            String  sql  = obtenerSQL( query );

            st           = con.prepareStatement( sql );
            st.setString(1,id_cliente);

            rs = st.executeQuery();

            if (rs.next()) {
                nit = rs.getString("nit");
            }

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL NIT EN FINTRA. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }
        return nit;
    }






    public String setAccionFacturaContratista(String factura_contratista, String fechaFacturaContratista )throws SQLException{


        StringStatement st         = null;

        String comando_sql  = "";

        String            query    = "SQL_SET_ACCIONES_FACTURA_CONTRATISTA";

        try {


            st = new StringStatement (this.obtenerSQL(query), true);

            st.setString(1,fechaFacturaContratista);
            st.setString(2,factura_contratista);

            comando_sql = st.getSql();

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA ACTUALIZACION DE LAS ACCIONES CON LA  FACTURA CXP CONTRATISTA FINAL. \n " + e.getMessage());
        }
        finally{
            // st.close();
            // this.desconectar(con);

            if (st  != null){
                try{ st = null;
                }
                catch(Exception e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
        }
        return comando_sql;
    }








    public ArrayList getFacturaDetalleContratista(String distrito, String proveedor, String tipo_documento, String documento)throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs = null;

        String            query      = "SQL_FACTURA_DETALLE_FINAL_CONTRATISTA";
        ArrayList listaItem = null;

        try {
            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );

            st           = con.prepareStatement( sql );

            st.setString(1,distrito);
            st.setString(2,proveedor);
            st.setString(3,tipo_documento);
            st.setString(4,documento);

            rs = st.executeQuery();

            listaItem =  new ArrayList();

            while (rs.next()){
                listaItem.add( ItemCxPContratista.load(rs));
            }

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA SELECCION DE LOS ITEMS DE LAS FACTURAS FINALES DEL CONTRATISTA PARA TRASLADARLAS A FINTRA. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }
        return listaItem;
    }






    public String setReferencia3(String tipoReferencia3, String referencia3, String distrito,
                                 String proveedor, String tipoDocumento, String documento )throws SQLException{


        StringStatement st         = null;

        String comando_sql  = "";

        String            query    = "SQL_SET_REFERENCIA_3";

        try {


            st = new StringStatement (this.obtenerSQL(query), true);

            st.setString(1,tipoReferencia3);
            st.setString(2,referencia3);
            st.setString(3,distrito);
            st.setString(4,proveedor);
            st.setString(5,tipoDocumento);
            st.setString(6,documento);

            comando_sql = st.getSql();

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA ACTUALIZACION DEL TIPO DE REFERENCIA 3 EN LA TABLA DE CXP. \n " + e.getMessage());
        }
        finally{
            // st.close();
            // this.desconectar(con);

            if (st  != null){
                try{ st = null;
                }
                catch(Exception e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
        }
        return comando_sql;
    }








    // *************************************************************************
    // *
    // *  FIN DEL AREA DE METODOS DEL PROCESO DE CONTRATISTA
    // *
    // *************************************************************************










/* Area Original */




    /**
     * Retorna el valor de la comision
     */


    public Comision getComision() {

        return comision;
    }




    /**
     * Localiza informacion de una comision
     * @param valor del contrato para determinar la comision

     */

    public void buscaComision(double valor)throws SQLException{

        PreparedStatement st  = null;
        Connection        con = null;
        ResultSet         rs  = null;

        String            query       = "SQL_COMISION";
        comision                      = null;

        try {
            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            st.setDouble( 1, valor );
            st.setDouble( 2, valor );

            rs = st.executeQuery();

            if(rs.next()){
                comision = new Comision();
                comision = (Comision.load(rs));
            }


        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL NOMBRE DEL CONTRATISTA. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }
    }





    /**
     * Lista del detalle de la prefactura
     * @prefactura Numero de prefactura a localizar
     */


    public void buscaDetallePrefactura(String prefactura)throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs = null;

        String            query    = "SQL_DETALLE_PREFACTURA";
        listaDetallePrefactura     = null;

        try {

            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            st.setString( 1, prefactura);

            rs = st.executeQuery();

            listaDetallePrefactura =  new LinkedList();

            while (rs.next()){
                listaDetallePrefactura.add(DetallePrefactura.load(rs));
            }

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA SELECCION DEL DETALLE DE LA PREFACTURA. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }
    }


    /**
     * Returna una Lista items de una prefactura
     */


    public List getDetallePrefactura() {

        return listaDetallePrefactura;
    }





    /**
     * Lista de prefacturas por contratista
     */


    public List getContratistaPrefactura()throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs               = null;

        String            query    = "SQL_LISTA_A_FACTURAR";
        List listaPrefacturaContratista = null ;

        try {
            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );

            rs = st.executeQuery();

            listaPrefacturaContratista =  new LinkedList();

            while (rs.next()){
                listaPrefacturaContratista.add(PrefacturaContratista.load(rs));
            }

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA SELECCION DE PREFACTURAS A FACTURAR. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }
        return listaPrefacturaContratista;
    }







    /**
     * Metodo EjecutarSQL, realiza comandos sql sobre la base de datos en
     * operaciones de tipo transaccion.
     * @param   : Vector de comandos de SQl
     * @autor   : Alvaro Pabon
     * @version : 1.0
     */

    public void ejecutarSQL(Vector comandosSQL) throws SQLException {

        Connection        con      = null;
        Statement        stmt      = null;

        String            query    = "SQL_GET_CONECTION";

        try{
            con   = this.conectarJNDI( query );

            if (con != null){

                con.setAutoCommit(false);
                stmt = con.createStatement();
                for(int i=0; i<comandosSQL.size();i++){
                    String comando =(String) comandosSQL.elementAt(i);
                    System.out.println(comando);
                    stmt.executeUpdate(comando);

                }

                con.commit();
            }
        }catch (SQLException e) {
            // Efectuando rollback en caso de error
            try {

                con.rollback();
            }
            catch (SQLException ignored) {
                throw new SQLException("NO SE PUDO HACER ROLLBACK " + ignored.getMessage() + " " + ignored.getErrorCode()+" <br> La siguiente exception es : ----"+ignored.getNextException());
            }
            throw new SQLException("ERROR DURANTE LA TRANSACCION, LOS CAMBIOS NO SE PRODUJERON EL LA BASE DE DATOS " + e.getMessage() + " " + e.getErrorCode()+" <br> La siguiente exception es : ----"+e.getNextException());
        }

        finally{
            stmt.close();
            this.desconectar(con);
        }


    }



    public void ejecutarSQL(Vector comandosSQL, LogWriter logWriter) throws SQLException {

        Connection        con      = null;
        Statement        stmt      = null;

        String            query    = "SQL_GET_CONECTION";
        String            comando  = "";


        try{
            con   = this.conectarJNDI( query );

            if (con != null){

                con.setAutoCommit(false);
                stmt = con.createStatement();
                for(int i=0; i<comandosSQL.size();i++){
                    comando =(String) comandosSQL.elementAt(i);
                    ////System.out.println(comando);
                    stmt.executeUpdate(comando);

                }

                con.commit();
            }
        }catch (SQLException e) {
            // Efectuando rollback en caso de error

            logWriter.log("ERROR: Se genero error de SQLException al ejecutar un SQL en el Batch. "+
                          "Se efectuara rollback. \n" +
                          "                                     SQL : " +  comando + "\n" +
                          "                                 Mensaje : " + e.getMessage() + "\n" +
                          "                         Codigo de error : " + e.getErrorCode() + "\n" +
                          "                       Proxima Exception : " + e.getNextException() + "\n"  ,LogWriter.INFO);
            try {

                con.rollback();
            }
            catch (SQLException ignored) {

                logWriter.log("ERROR: Se genero error al ejecutar el rollback en el Batch. "+
                              " No Se efectuo rollback. \n" +
                              "                                 Mensaje : " + ignored.getMessage() + "\n" +
                              "                         Codigo de error : " + ignored.getErrorCode() + "\n" +
                              "                       Proxima Exception : " + ignored.getNextException() + "\n"  ,LogWriter.INFO);



                throw new SQLException("NO SE PUDO HACER ROLLBACK " + ignored.getMessage() + " " + ignored.getErrorCode()+" <br> La siguiente exception es : ----"+ignored.getNextException());
            }
            throw new SQLException("ERROR DURANTE LA TRANSACCION, LOS CAMBIOS NO SE PRODUJERON EL LA BASE DE DATOS " + e.getMessage() + " " + e.getErrorCode()+" <br> La siguiente exception es : ----"+e.getNextException());
        }

        finally{
            stmt.close();
            this.desconectar(con);
        }


    }




    public void ejecutarSQL(Vector comandosSQL, LogWriter logWriter, String dataBaseName) throws SQLException {

        Connection        con      = null;
        Statement        stmt      = null;

        String            query    = "SQL_GET_CONECTION";
        String            comando  = "";


        try{
            con   = this.conectarJNDI( query, dataBaseName );

            if (con != null){

                con.setAutoCommit(false);
                stmt = con.createStatement();
                for(int i=0; i<comandosSQL.size();i++){
                    comando =(String) comandosSQL.elementAt(i);
                    ////System.out.println(comando);
                    stmt.executeUpdate(comando);

                }

                con.commit();
            }
        }catch (SQLException e) {
            // Efectuando rollback en caso de error

            logWriter.log("ERROR: Se genero error de SQLException al ejecutar un SQL en el Batch. "+
                          "Se efectuara rollback. \n" +
                          "                                     SQL : " +  comando + "\n" +
                          "                                 Mensaje : " + e.getMessage() + "\n" +
                          "                         Codigo de error : " + e.getErrorCode() + "\n" +
                          "                       Proxima Exception : " + e.getNextException() + "\n"  ,LogWriter.INFO);
            try {

                con.rollback();
            }
            catch (SQLException ignored) {

                logWriter.log("ERROR: Se genero error al ejecutar el rollback en el Batch. "+
                              " No Se efectuo rollback. \n" +
                              "                                 Mensaje : " + ignored.getMessage() + "\n" +
                              "                         Codigo de error : " + ignored.getErrorCode() + "\n" +
                              "                       Proxima Exception : " + ignored.getNextException() + "\n"  ,LogWriter.INFO);



                throw new SQLException("NO SE PUDO HACER ROLLBACK " + ignored.getMessage() + " " + ignored.getErrorCode()+" <br> La siguiente exception es : ----"+ignored.getNextException());
            }
            throw new SQLException("ERROR DURANTE LA TRANSACCION, LOS CAMBIOS NO SE PRODUJERON EL LA BASE DE DATOS " + e.getMessage() + " " + e.getErrorCode()+" <br> La siguiente exception es : ----"+e.getNextException());
        }

        finally{
            stmt.close();
            this.desconectar(con);
        }


    }









    /**
     * Metodo EjecutarSQL, realiza comandos sql sobre la base de datos en
     * operaciones de tipo transaccion.
     * @param   : Vector de comandos de SQl
     * @autor   : Alvaro Pabon
     * @version : 1.0
     */

    public void ejecutarSQL(Vector comandosSQL, String dataBaseName) throws SQLException {

        Connection        con      = null;
        Statement        stmt      = null;

        String            query    = "SQL_GET_CONECTION";

        try{
            con   = this.conectarJNDI( query, dataBaseName );

            if (con != null){

                con.setAutoCommit(false);
                stmt = con.createStatement();
                for(int i=0; i<comandosSQL.size();i++){
                    String comando =(String) comandosSQL.elementAt(i);
                    ////System.out.println(comando);
                    stmt.executeUpdate(comando);

                }

                con.commit();
            }
        }catch (SQLException e) {
            // Efectuando rollback en caso de error
            try {

                con.rollback();
            }
            catch (SQLException ignored) {
                throw new SQLException("NO SE PUDO HACER ROLLBACK " + ignored.getMessage() + " " + ignored.getErrorCode()+" <br> La siguiente exception es : ----"+ignored.getNextException());
            }
            throw new SQLException("ERROR DURANTE LA TRANSACCION, LOS CAMBIOS NO SE PRODUJERON EL LA BASE DE DATOS " + e.getMessage() + " " + e.getErrorCode()+" <br> La siguiente exception es : ----"+e.getNextException());
        }

        finally{
            stmt.close();
            this.desconectar(con);
        }


    }








    /**
     * Lista resumida de facturas elaboradas al contratista para crear factura
     * para pagar a Applus por concepto de retencion y bonificacion
     */


    public List getFacturaApplus(String fechaInicial,String fechaFinal, String tipoFactura)throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs = null;

        String            query    = "SQL_FACTURA_APPLUS_"+tipoFactura;
        List listaFacturaApplus    = null;

        try {

            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            st.setString( 1, fechaInicial + " 00:00:01" );
            st.setString( 2, fechaFinal + " 23:59:59" );

            rs = st.executeQuery();

            listaFacturaApplus =  new LinkedList();

            while (rs.next()){
                listaFacturaApplus.add(FacturaApplus.load(rs));
            }

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA SELECCION DE LAS FACTURAS DEL CONTRATISTA PARA FACTURARLE A APPLUS. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }
        return listaFacturaApplus;
    }





    /**
     * Forma un comando sql para actualizar la factura de applus
     * @id_contratista Identificacion del contratista
     * @prefactura Numero de prefactura facturada
     */


    public String setFacturaApplus(String factura_applus, String fecha_factura_applus,
                                   String id_contratista, String prefactura, String tipoFactura)throws SQLException{


        // PreparedStatement st       = null;
        // Connection        con      = null;
        StringStatement st         = null;

        String comando_sql  = "";

        String            query    = "SQL_ACTUALIZA_FACTURA_APPLUS_";

        try {
            // con          = this.conectarJNDI( query );
            // String sql   = obtenerSQL( query );
            // st           = con.prepareStatement( sql );

            st = new StringStatement (this.obtenerSQL(query), true);
            st.setString(1,factura_applus);
            st.setString(2,fecha_factura_applus);
            st.setString(3,id_contratista);
            st.setString(4,prefactura);


            // comando_sql = st.toString();

            comando_sql = st.getSql();

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA CONFORMACION DEL COMANDO SQL PARA ACTUALIZAR FECHA FACTURA DE APPLUS. \n " + e.getMessage());
        }
        finally{
            // st.close();
            // this.desconectar(con);

            if (st  != null){
                try{ st = null;
                }
                catch(Exception e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
        }
        return comando_sql;
    }








    /**
     * Lista las acciones de un contratista sin prefacturar
     * @id_contratista Identificacion del contratista, codigo interno

     */


    public void buscaOfertaEca()throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs = null;


        String            query    = "SQL_OFERTAS";
        listaOfertaEca = null;

        try {
            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );

            rs = st.executeQuery();

            listaOfertaEca =  new LinkedList();

            while (rs.next()){
                listaOfertaEca.add(OfertaEca.load(rs));
            }

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA SELECCION DE REGISTROS DE OFERTAS. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }
    }



    /**
     * Returna una Lista de ofertas
     */


    public List getOfertaEca() {

        return listaOfertaEca;
    }


    public void buscaOfertaEcaDetalle(int id_orden, String fecha_financiacion)throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs = null;

        String            query    = "SQL_OFERTAS_DETALLE";
        listaOfertaEcaDetalle = null;

        try {

            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            System.out.println("id_orden"+id_orden);
            System.out.println(" fecha financiacion :" + fecha_financiacion);
            st.setString( 1, ""+id_orden );
            st.setString( 2, fecha_financiacion );
            st.setString( 3, fecha_financiacion );
            rs = st.executeQuery();

            listaOfertaEcaDetalle =  new LinkedList();

            while (rs.next()){



                listaOfertaEcaDetalle.add(OfertaEcaDetalle.load(rs));
            }

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA SELECCION DE REGISTROS DE OFERTAS DETALLADAS. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }
    }


    public List getOfertaEcaDetalle() {

        return listaOfertaEcaDetalle;
    }

    public double  getPorcentajeFactoringFintra(String clase_dtf, int cuotas_reales)throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs = null;

        String            query    = "SQL_PORCENTAJE_FACTORING";
        String            porcentaje_factoring_fintra      = "0.0";

        try {

            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            String sCuotas_reales = Integer.toString(cuotas_reales);
            st.setString( 1, clase_dtf );
            st.setString( 2, sCuotas_reales );
            rs = st.executeQuery();

            if (rs.next()){
                porcentaje_factoring_fintra=rs.getString("descripcion");
            }

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL PORCENTAJE FACTORING FINTRA. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }

        return (Double.parseDouble(porcentaje_factoring_fintra)/100);
    }







    /**
     * Forma un comando sql para actualizar la factura de applus
     * @id_contratista Identificacion del contratista
     * @prefactura Numero de prefactura facturada
     */


    public String setSimboloVariable(String simbolo_variable, String observacion, int id_estado_negocio,
                                     int id_orden,String userx)throws SQLException{


        // PreparedStatement st       = null;
        // Connection        con      = null;
        StringStatement st         = null;

        String comando_sql  = "";

        String            query    = "SQL_ACTUALIZA_SIMBOLO";

        try {
            // con          = this.conectarJNDI( query );
            // String sql   = obtenerSQL( query );
            // st           = con.prepareStatement( sql );

            st = new StringStatement (this.obtenerSQL(query), true);

            st.setString(1,simbolo_variable);
            st.setString(2,observacion);
            st.setInt(3,id_estado_negocio);

            st.setString(4,userx);
            st.setInt(5,id_orden);

            st.setString(6,simbolo_variable);
            st.setInt(7,id_estado_negocio);
            st.setString(8,userx);
            st.setInt(9,id_orden);

            // comando_sql = st.toString();

            comando_sql = st.getSql();

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA CONFORMACION DEL COMANDO SQL PARA ACTUALIZAR SIMBOLO VARIABLE. \n " + e.getMessage());
        }
        finally{
            // st.close();
            // this.desconectar(con);

            if (st  != null){
                try{ st = null;
                }
                catch(Exception e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
        }
        return comando_sql;
    }



    public String setLiquidacionEca(OfertaEcaDetalle ofertaEcaDetalle)throws SQLException{


        // PreparedStatement st       = null;
        // Connection        con      = null;
        StringStatement st         = null;

        String comando_sql  = "";

        String            query    = "SQL_ACTUALIZA_LIQUIDACION_ECA";

        try {
            // con          = this.conectarJNDI( query );
            // String sql   = obtenerSQL( query );
            // st           = con.prepareStatement( sql );

            st = new StringStatement (this.obtenerSQL(query), true);

            st.setDouble(1,ofertaEcaDetalle.getTotal_prev1());
            st.setDouble(2,ofertaEcaDetalle.getIva_total_prev1());
            st.setDouble(3,ofertaEcaDetalle.getComision_applus());
            st.setDouble(4,ofertaEcaDetalle.getComision_provintegral());
            st.setDouble(5,ofertaEcaDetalle.getComision_fintra());
            st.setDouble(6,ofertaEcaDetalle.getIva_comision_applus());
            st.setDouble(7,ofertaEcaDetalle.getIva_comision_provintegral());
            st.setDouble(8,ofertaEcaDetalle.getIva_comision_fintra());
            st.setDouble(9,ofertaEcaDetalle.getComision_eca());
            st.setDouble(10,ofertaEcaDetalle.getEca_oferta_calculada());
            st.setDouble(11,ofertaEcaDetalle.getFinanciacion_fintra());
            st.setInt(12,ofertaEcaDetalle.getCuotas_reales());
            st.setDouble(13,ofertaEcaDetalle.getDtf_semana());
            st.setDouble(14,ofertaEcaDetalle.getPorcentaje_factoring());
            st.setString(15,ofertaEcaDetalle.getFecha_financiacion());
            st.setDouble(16,ofertaEcaDetalle.getCuota_pago());
            st.setDouble(17,ofertaEcaDetalle.getPorcentaje_comision_applus());
            st.setDouble(18,ofertaEcaDetalle.getPorcentaje_comision_provintegral());
            st.setDouble(19,ofertaEcaDetalle.getPorcentaje_comision_fintra());
            st.setDouble(20,ofertaEcaDetalle.getPorcentaje_comision_eca());
            st.setDouble(21,ofertaEcaDetalle.getPorcentaje_iva());
            st.setDouble(22,ofertaEcaDetalle.getIva_comision_eca());
            st.setDouble(23,ofertaEcaDetalle.getComision_factoring_fintra());
            st.setDouble(24,ofertaEcaDetalle.getIva_comision_factoring_fintra());
            st.setDouble(25,ofertaEcaDetalle.getPorcentaje_factoring_fintra());
            st.setDouble(26,ofertaEcaDetalle.getPuntos_dtf());
            st.setDouble(27,ofertaEcaDetalle.getTotal_financiacion());

            st.setDouble(28,ofertaEcaDetalle.getEc_porcentaje_comision_provintegral());
            st.setDouble(29,ofertaEcaDetalle.getEc_porcentaje_iva());
            st.setDouble(30,ofertaEcaDetalle.getEc_porcentaje_factoring_fintra());
            st.setDouble(31,ofertaEcaDetalle.getEc_porcentaje_comision_applus());
            st.setDouble(32,ofertaEcaDetalle.getEc_porcentaje_comision_fintra());
            st.setDouble(33,ofertaEcaDetalle.getEc_porcentaje_comision_eca());
            st.setDouble(34,ofertaEcaDetalle.getEc_valor_mat());
            st.setDouble(35,ofertaEcaDetalle.getEc_comision_applus());
            st.setDouble(36,ofertaEcaDetalle.getEc_comision_provintegral());
            st.setDouble(37,ofertaEcaDetalle.getEc_comision_factoring_fintra());
            st.setDouble(38,ofertaEcaDetalle.getEc_comision_fintra());
            st.setDouble(39,ofertaEcaDetalle.getEc_comision_eca());
            st.setDouble(40,ofertaEcaDetalle.getEc_iva_valor_mat());
            st.setDouble(41,ofertaEcaDetalle.getEc_iva_comision_applus());
            st.setDouble(42,ofertaEcaDetalle.getEc_iva_comision_factoring_fintra ());
            st.setDouble(43,ofertaEcaDetalle.getEc_iva_comision_fintra());
            st.setDouble(44,ofertaEcaDetalle.getEc_iva_comision_provintegral());
            st.setDouble(45,ofertaEcaDetalle.getEc_iva_comision_eca());
            st.setDouble(46,ofertaEcaDetalle.getEc_financiacion_fintra());
            st.setDouble(47,ofertaEcaDetalle.getEc_cuota_pago());
            st.setDouble(48,ofertaEcaDetalle.getEc_total_financiacion ());
            st.setDouble(49,ofertaEcaDetalle.getIvaBonificacion() );
            st.setString(50,ofertaEcaDetalle.getId_accion());

            //inicio de colocar bien tipo_dtf
            if (ofertaEcaDetalle.getPuntos_dtf()==9 || ofertaEcaDetalle.getPuntos_dtf()==11 || ofertaEcaDetalle.getPuntos_dtf()==12){
                st.setString(50,("DTF+"+ofertaEcaDetalle.getPuntos_dtf()).replaceAll(".0",""));
            }else{
                st.setString(50,"MAXIMA");
            }

            st.setString(51,""+ofertaEcaDetalle.getId_orden());

            if (ofertaEcaDetalle.getPuntos_dtf()==9 || ofertaEcaDetalle.getPuntos_dtf()==11 || ofertaEcaDetalle.getPuntos_dtf()==12){//090730
                st.setString(52,("DTF+"+ofertaEcaDetalle.getPuntos_dtf()).replaceAll(".0",""));    //090730
            }else{//090730
                st.setString(52,"MAXIMA");//090730
            }//090730
            st.setString(53,""+ofertaEcaDetalle.getId_orden());//090730

            //fin de colocar bien tipo_dtf

            // comando_sql = st.toString();

            comando_sql = st.getSql();

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA CONFORMACION DEL COMANDO SQL PARA ACTUALIZAR LIQUIDACION ECA. \n " + e.getMessage());
        }
        finally{
            // st.close();
            // this.desconectar(con);

            if (st  != null){
                try{ st = null;
                }
                catch(Exception e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
        }
        return comando_sql;
    }



    public List  buscaPrefacturaEca()throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs = null;

        String            query    = "SQL_BUSCA_PREFACTURA_ECA";
        List listaPrefacturaEca = null;

        try {

            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            rs = st.executeQuery();

            listaPrefacturaEca =  new LinkedList();

            while (rs.next()){
                listaPrefacturaEca.add(PrefacturaEca.load(rs));
            }

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA SELECCION DE REGISTROS DE OFERTAS PARA FACTURAR A ECA. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }

        return listaPrefacturaEca;
    }





    public List  buscaPrefacturaEcaContratista(int id_orden)throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs = null;

        String            query    = "SQL_ORDEN_CONTRATISTA";
        List listaPrefacturaEcaContratista = null;

        try {

            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            st.setInt( 1, id_orden );
            rs = st.executeQuery();

            listaPrefacturaEcaContratista =  new LinkedList();

            while (rs.next()){
                listaPrefacturaEcaContratista.add(PrefacturaEcaContratista.load(rs));
            }

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA SELECCION DE REGISTROS DE ACCIONES DE CONTRATISTA. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }

        return listaPrefacturaEcaContratista;
    }







    public String setFacturaCxC(String dstrct, String tipo_documento, String documento,
                             String nit, String codcli, String concepto,
                             String fecha_factura, String fecha_vencimiento,
                             String fecha_impresion,String descripcion, String observacion,
                             Double valor_factura, Double valor_abono, Double valor_saldo,
                             Double valor_facturame, Double valor_abonome, Double valor_saldome,
                             Double valor_tasa, String moneda,
                             int cantidad_items, String forma_pago, String agencia_facturacion,
                             String agencia_cobro,String zona, String base,String last_update, String user_update,
                             String creation_date, String creation_user, String cmc,
                             String formato, String agencia_impresion,
                             String tipo_ref1,String ref1,String tipo_ref2,String ref2,
                             String tipo_referencia_1, String referencia_1,
                             String tipo_referencia_2, String referencia_2,
                             String tipo_referencia_3, String referencia_3)throws SQLException{


        // PreparedStatement st       = null;
        // Connection        con      = null;
        StringStatement st         = null;

        String comando_sql  = "";

        String            query    = "SQL_INSERTA_FACTURA_CXC";

        try {
            // con          = this.conectarJNDI( query );
            // String sql   = obtenerSQL( query );
            // st           = con.prepareStatement( sql );

            st = new StringStatement (this.obtenerSQL(query), true);

            st.setString(1,dstrct);
            st.setString(2,tipo_documento);
            st.setString(3,documento);
            st.setString(4,nit);
            st.setString(5,codcli);
            st.setString(6,concepto);
            st.setString(7,fecha_factura);
            st.setString(8,fecha_vencimiento);
            st.setString(9,fecha_impresion);
            st.setString(10,descripcion);
            st.setString(11,observacion);
            
            st.setDouble(12,valor_factura);
            st.setDouble(13,valor_abono);
            st.setDouble(14,valor_saldo);
            st.setDouble(15,valor_facturame);
            st.setDouble(16,valor_abonome);
            st.setDouble(17,valor_saldome);
            st.setDouble(18,valor_tasa);
            st.setString(19,moneda);
            st.setInt(20,cantidad_items);
            st.setString(21,forma_pago);
            st.setString(22,agencia_facturacion);
            st.setString(23,agencia_cobro);
            st.setString(24,zona);
            st.setString(25,base);
            st.setString(26,last_update);
            st.setString(27,user_update);
            st.setString(28,creation_date);
            st.setString(29,creation_user);
            st.setString(30,cmc);
            st.setString(31,formato);
            st.setString(32,agencia_impresion);
            st.setString(33,tipo_ref1);
            st.setString(34,ref1);
            st.setString(35,tipo_ref2);
            st.setString(36,ref2);

            st.setString(37,tipo_referencia_1);
            st.setString(38,referencia_1);
            st.setString(39,tipo_referencia_2);
            st.setString(40,referencia_2);
            st.setString(41,tipo_referencia_3);
            st.setString(42,referencia_3);

            // comando_sql = st.toString();

            comando_sql = st.getSql();

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA CONFORMACION DEL COMANDO SQL PARA AGREGAR UNA FACTURA. \n " + e.getMessage());
        }
        finally{
            // st.close();
            // this.desconectar(con);

            if (st  != null){
                try{ st = null;
                }
                catch(Exception e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
        }
        return comando_sql;
    }



    public String setFacturaDetalleCxC(String dstrct, String tipo_documento, String documento,
                                    int item, String nit, String concepto,
                                    String descripcion, String codigo_cuenta_contable,
                                    double cantidad, double valor_unitario , double valor_unitariome , double valor_item,
                                    double valor_itemme , double valor_tasa, String moneda,
                                    String last_update, String user_update, String creation_date,
                                    String creation_user, String base, String auxiliar,
                                    String tipo_referencia_1, String referencia_1,
                                    String tipo_referencia_2, String referencia_2,
                                    String tipo_referencia_3, String referencia_3)throws SQLException{


        // PreparedStatement st       = null;
        // Connection        con      = null;
        StringStatement st         = null;

        String comando_sql  = "";

        String            query    = "SQL_INSERTA_FACTURA_DETALLE_CXC";

        try {
            // con          = this.conectarJNDI( query );
            // String sql   = obtenerSQL( query );
            // st           = con.prepareStatement( sql );

            st = new StringStatement (this.obtenerSQL(query), true);

            st.setString(1,dstrct);
            st.setString(2,tipo_documento);
            st.setString(3,documento);
            st.setInt(4,item);
            st.setString(5,nit);
            st.setString(6,concepto);
            st.setString(7,descripcion);
            st.setString(8,codigo_cuenta_contable);
            st.setDouble(9,cantidad);
            st.setDouble(10,valor_unitario);
            st.setDouble(11,valor_unitariome);
            st.setDouble(12,valor_item);
            st.setDouble(13,valor_itemme);
            st.setDouble(14,valor_tasa);
            st.setString(15,moneda);
            st.setString(16,last_update);
            st.setString(17,user_update);
            st.setString(18,creation_date);
            st.setString(19,creation_user);
            st.setString(20,base);
            st.setString(21,auxiliar);

            st.setString(22,tipo_referencia_1);
            st.setString(23,referencia_1);
            st.setString(24,tipo_referencia_2);
            st.setString(25,referencia_2);
            st.setString(26,tipo_referencia_3);
            st.setString(27,referencia_3);



            // comando_sql = st.toString();

            comando_sql = st.getSql();

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA INSERCION DE LOS REGISTROS DE DETALLE DE UNA FACTURA. \n " + e.getMessage());
        }
        finally{
            // st.close();
            // this.desconectar(con);

            if (st  != null){
                try{ st = null;
                }
                catch(Exception e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
        }
        return comando_sql;
    }





    public String setOferta(int id_orden, String factura_eca, String fecha_factura_eca)throws SQLException{


        // PreparedStatement st       = null;
        // Connection        con      = null;
        StringStatement st         = null;

        String comando_sql  = "";

        String            query    = "SQL_ACTUALIZA_OFERTA";

        try {
            // con          = this.conectarJNDI( query );
            // String sql   = obtenerSQL( query );
            // st           = con.prepareStatement( sql );

            st = new StringStatement (this.obtenerSQL(query), true);

            st.setString(1,factura_eca);
            st.setString(2,fecha_factura_eca);
            st.setInt(3,id_orden);

            // comando_sql = st.toString();

            comando_sql = st.getSql();

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA ACTUALIZACION DE LA OFERTA CON EL NUMERO DE FACTURA ECA. \n " + e.getMessage());
        }
        finally{
            // st.close();
            // this.desconectar(con);

            if (st  != null){
                try{ st = null;
                }
                catch(Exception e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
        }
        return comando_sql;
    }







    public String setFechaFacturaEca(int id_orden, String fecha_factura_eca)throws SQLException{

        // PreparedStatement st       = null;
        // Connection        con      = null;
        StringStatement st         = null;

        String comando_sql  = "";

        String            query    = "SQL_ACTUALIZA_FECHA_FACTURA_ECA";

        try {
            // con          = this.conectarJNDI( query );
            // String sql   = obtenerSQL( query );
            // st           = con.prepareStatement( sql );

            st = new StringStatement (this.obtenerSQL(query), true);


            st.setString(1,fecha_factura_eca);
            st.setInt(2,id_orden);

            // comando_sql = st.toString();

            comando_sql = st.getSql();

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA ACTUALIZACION DE LA FECHA DE FACTURA ECA. \n " + e.getMessage());
        }
        finally{
            // st.close();
            // this.desconectar(con);

            if (st  != null){
                try{ st = null;
                }
                catch(Exception e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
        }
        return comando_sql;
    }




    public List  buscaPrefacturaApp()throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs = null;

        String            query    = "SQL_GENERAR_FACTURA_APP";
        List listaPrefacturaEca = null;

        try {

            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            rs = st.executeQuery();

            listaPrefacturaEca =  new LinkedList();

            while (rs.next()){
                listaPrefacturaEca.add(PrefacturaEca.load(rs));
            }

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA SELECCION DE REGISTROS DE OFERTAS PARA FACTURAR A APPLUS. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }

        return listaPrefacturaEca;
    }



    public String setFacturaComision(int id_orden,String documento,String fecha_factura_eca)throws SQLException{


        // PreparedStatement st       = null;
        // Connection        con      = null;
        StringStatement st         = null;

        String comando_sql  = "";

        String            query    = "SQL_ACTUALIZA_FACTURA_COMISION_APP";

        try {
            // con          = this.conectarJNDI( query );
            // String sql   = obtenerSQL( query );
            // st           = con.prepareStatement( sql );

            st = new StringStatement (this.obtenerSQL(query), true);

            st.setString(1,documento);
            st.setString(2,fecha_factura_eca);
            st.setInt(3,id_orden);

            // comando_sql = st.toString();

            comando_sql = st.getSql();

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA ACTUALIZACION DE LA FACTURA APPLUS. \n " + e.getMessage());
        }
        finally{
            // st.close();
            // this.desconectar(con);

            if (st  != null){
                try{ st = null;
                }
                catch(Exception e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
        }
        return comando_sql;
    }





    public List  buscaPrefacturaPro()throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs = null;

        String            query    = "SQL_GENERAR_FACTURA_PRO";
        List listaPrefacturaEca = null;

        try {

            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            rs = st.executeQuery();

            listaPrefacturaEca =  new LinkedList();

            while (rs.next()){
                listaPrefacturaEca.add(PrefacturaEca.load(rs));
            }

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA SELECCION DE REGISTROS DE OFERTAS PARA FACTURAR A PROVINTEGRAL. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }

        return listaPrefacturaEca;
    }



    public String setFacturaPro(int id_orden,String documento,String fecha_factura_eca)throws SQLException{


        // PreparedStatement st       = null;
        // Connection        con      = null;
        StringStatement st         = null;

        String comando_sql  = "";

        String            query    = "SQL_ACTUALIZA_FACTURA_COMISION_PRO";

        try {
            // con          = this.conectarJNDI( query );
            // String sql   = obtenerSQL( query );
            // st           = con.prepareStatement( sql );

            st = new StringStatement (this.obtenerSQL(query), true);

            st.setString(1,documento);
            st.setString(2,fecha_factura_eca);
            st.setInt(3,id_orden);

            // comando_sql = st.toString();

            comando_sql = st.getSql();

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA ACTUALIZACION DE LA FACTURA PROVINTEGRAL. \n " + e.getMessage());
        }
        finally{
            // st.close();
            // this.desconectar(con);

            if (st  != null){
                try{ st = null;
                }
                catch(Exception e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
        }
        return comando_sql;
    }






    public List  buscaPrefacturaComisionEca()throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs = null;

        String            query    = "SQL_GENERAR_COMISION_FACTURA_ECA";
        List listaPrefacturaEca = null;

        try {

            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            rs = st.executeQuery();

            listaPrefacturaEca =  new LinkedList();

            while (rs.next()){
                listaPrefacturaEca.add(PrefacturaEca.load(rs));
            }

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA SELECCION DE REGISTROS DE OFERTAS PARA FACTURAR A ECA. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }

        return listaPrefacturaEca;
    }



    public String setFacturaComisionEca(int id_orden,String documento,String fecha_factura_eca)throws SQLException{



        // PreparedStatement st       = null;
        // Connection        con      = null;
        StringStatement st         = null;

        String comando_sql  = "";

        String            query    = "SQL_ACTUALIZA_FACTURA_COMISION_ECA";

        try {
            // con          = this.conectarJNDI( query );
            // String sql   = obtenerSQL( query );
            // st           = con.prepareStatement( sql );

            st = new StringStatement (this.obtenerSQL(query), true);

            st.setString(1,documento);
            st.setString(2,fecha_factura_eca);
            st.setInt(3,id_orden);

            // comando_sql = st.toString();

            comando_sql = st.getSql();

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA ACTUALIZACION DE LA FACTURA ECA. \n " + e.getMessage());
        }
        finally{
            // st.close();
            // this.desconectar(con);

            if (st  != null){
                try{ st = null;
                }
                catch(Exception e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
        }
        return comando_sql;
    }





    public PuntosFinanciacion getPuntosFinanciacion(String esquema, String regulacion, double valor, String ano,
                                                    String trimestre, int cuotas,String edificio)throws SQLException{


       // System.out.println("esquema"+esquema+"regulacion"+regulacion+"valor"+valor+"ano"+ano+"trimestre"+trimestre+"cuotas"+cuotas);

        PreparedStatement st  = null;
        Connection        con = null;
        ResultSet         rs  = null;

        String            query1       = "SQL_PUNTOS_NUEVO_R";
        String            query2       = "SQL_PUNTOS_NUEVO_NR";
        String            query3       = "SQL_PUNTOS_VIEJO";
        String            query4       = "SQL_PUNTOS_NUEVO_NR_MAYOR_12";
        String            query        = "";

        double            puntos       = 0.0;
        PuntosFinanciacion puntosFinanciacion = null;

        try {



            if ( (esquema.equalsIgnoreCase("NUEVO") || esquema.equalsIgnoreCase("AIRES")) && (regulacion.equalsIgnoreCase("R")) ) {
                query =  query1 ;
            }
            if ( (esquema.equalsIgnoreCase("NUEVO") || esquema.equalsIgnoreCase("AIRES")) && (regulacion.equalsIgnoreCase("NR")) && cuotas > 12 )
            {
                query =  query4 ;
            }
            else
            {
                if ( (esquema.equalsIgnoreCase("NUEVO") || esquema.equalsIgnoreCase("AIRES")) && (regulacion.equalsIgnoreCase("NR")) )
                {
                    query =  query2 ;
                }

            }

            if  (esquema.equalsIgnoreCase("VIEJO") ) {
                query =  query3 ;
            }

            if  (edificio.equalsIgnoreCase("S") )
            {
                query =  query4 ;
            }


            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );

             if (edificio.equalsIgnoreCase("S") )
            {
               int periodo=Integer.parseInt(trimestre) * 3;
               String parametro="";


               if(periodo<=9)
               {
                 parametro = ano+"0"+periodo;
               }
            else
               {
                parametro = ano+periodo;
            }


                st.setString(1, parametro);
                st.setString(2, parametro);
                st.setString(3, esquema);
                st.setString(4, ano);
                st.setString(5, ano);
                st.setString(6, trimestre);
                st.setString(7, trimestre);
                st.setString(8, parametro);
            }
            else
             {
                    if ( (esquema.equalsIgnoreCase("NUEVO") || esquema.equalsIgnoreCase("AIRES")) && (regulacion.equalsIgnoreCase("R"))  )
                    {
                        st.setString( 1, esquema );
                        st.setInt( 2, cuotas );
                        st.setInt( 3, cuotas );
                        st.setString( 4, ano );
                        st.setString( 5, ano );
                        st.setString( 6, trimestre );
                        st.setString( 7, trimestre );

                    }



                    if ( (esquema.equalsIgnoreCase("NUEVO") || esquema.equalsIgnoreCase("AIRES")) && (regulacion.equalsIgnoreCase("NR"))&&  cuotas > 12  )
                    {
               int periodo=Integer.parseInt(trimestre) * 3;
               String parametro="";


               if(periodo<=9)
               {
                 parametro = ano+"0"+periodo;
               }
            else
               {
                parametro = ano+periodo;
            }


                        st.setString(1, parametro);
                        st.setString(2, parametro);
                        st.setString(3, esquema);
                        st.setString(4, ano);
                        st.setString(5, ano);
                        st.setString(6, trimestre);
                        st.setString(7, trimestre);
                        st.setString(8, parametro);
                    }
                    else
                    {
                        if ( (esquema.equalsIgnoreCase("NUEVO") || esquema.equalsIgnoreCase("AIRES") ) && (regulacion.equalsIgnoreCase("NR")) )
                        {
                            
                            st.setString( 1, esquema );
                            st.setDouble( 2, valor );
                            st.setDouble( 3, valor );
                            st.setString( 4, ano );
                            st.setString( 5, ano );
                            st.setString( 6, trimestre );
                            st.setString( 7, trimestre );
                           
                        }
                    }
            }




  

            rs = st.executeQuery();

            if(rs.next()){
                puntosFinanciacion = new PuntosFinanciacion();
                puntosFinanciacion = (PuntosFinanciacion.load(rs));
            }


        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS PUNTOS PARA LA FINANCIACION. \n " + e.getMessage());
        }
        finally{

            st.close();
            this.desconectar(con);
        }
        return puntosFinanciacion;
    }

    public String  buscaF_facturado_cliente(String orden)throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs = null;

        String            query    = "SQL_OBTENER_F_FACTURADO_CLIENTE";
        String f_facturado_cliente= "0099-01-01";

        try {

            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            st.setString( 1, orden );
            rs = st.executeQuery();

            if (rs.next()){
                f_facturado_cliente=rs.getString("f_facturado_cliente");
            }

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE buscaF_facturado_cliente. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }

        return f_facturado_cliente;
    }



    public String getNombreCliente(String id_cliente)throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs = null;


        String            query    = "SQL_GET_NOMBRE_CLIENTE";
        String    nombreCliente    = "";

        try {
            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            st.setString(1, id_cliente );

            rs = st.executeQuery();
            if(rs.next()){
                nombreCliente =  rs.getString("nomcli") ;
            }

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL NOMBRE DEL CLIENTE. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }
        return nombreCliente;
    }












    public Tipo_impuesto getTipoImpuesto(String dstrct, String codigo, String fecha, String concepto, String agencia ) throws SQLException {




        PreparedStatement st  = null;
        Connection        con = null;
        ResultSet          rs = null;

        String            query    = "SQL_GET_TIPO_IMPUESTO";


        Tipo_impuesto datos = null;

        try {

            con          = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );


            st           = con.prepareStatement( sql );
            st.setString(1, dstrct);
            st.setString(2, codigo);
            st.setString(3, fecha);
            st.setString(4, agencia);
            st.setString(5, concepto);

            rs = st.executeQuery();
            while(rs.next()){
                datos = Tipo_impuesto.loadRFTE(rs);
                break;
            }
        } catch(Exception e) {
            throw new SQLException("Error al leer la tabla de Tip� de Impuesto... \n"+e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }
        return datos;
    }



/**
     * Lee los registros de una factura cxp interna de contratista para crear en base a ellos los registros
     * de la nota credito que se aplicara a la factura cxp interna
     * @param condicion Especifica el criterio con el cual se extreran los registros
     * @return Retorna una lista de objetos FacturaDetalleCxP conteniendo la informacion
     * de cada item del detalle de una factura
     * @throws SQLException
     */
    public List getFacturaCxPDetalleInterna(String condicion, String hc)throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs = null;

        String            query    = "SQL_GET_FACTURA_DETALLE_CXP_INTERNA";
        List listaFacturaDetalleCxP  = null;

        try {

            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );

            sql = sql.replaceAll("#CONDICION#", condicion);

            st           = con.prepareStatement( sql );
            if(hc.trim().equalsIgnoreCase("AS")){
             st.setString(1, "AIRES CUENTA NC CONTRATISTA CONCEPTO ");
            }else{
             st.setString(1, "CUENTA NC CONTRATISTA CONCEPTO "); 
            }
            rs = st.executeQuery();

            listaFacturaDetalleCxP =  new LinkedList();

            while (rs.next()){


               listaFacturaDetalleCxP.add(FacturaDetalleCxP.load(rs));
            }

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA SELECCION DE LOS ITEMS DE LAS FACTURAS CXP. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }
        return listaFacturaDetalleCxP;
    }



    
    
    // *************************************************************************
    // *
    // *  INICIO DEL AREA DE METODOS DEL PROCESO DE REFINANCIACION
    // *
    // *************************************************************************

    
        /**
     *
     * @return Una lista de objetos FacturaPM de la tabla Factura
     * @see buscaFacturaPM
     * @see FacturaPM.java
     */
        public List getFacturaPM() {                                                //20101115
            return listaFacturaPM;
        }

      /**
     *
     * @return Una lista de facturas con saldos
     * @see buscaFacturaCliente
     */
        
    public List getFacturaCliente() {                                                //20101115
        
        return listaFacturaCliente;
    }
          
    

    /**
     * Retorna un objeto con la informacion de la refinanciacin de PM
     */
    public FinanciacionPM getFinanciacionPM() {
        return financiacionPM;
    }


    public void inicializarFinanciacionPM() {
        financiacionPM = new FinanciacionPM();
        return;
    }    
    

    
    
    

    /**
     * Extrae una lista de las facturas PM de un cliente que no esten canceladas y las deja en un objeto tipo List
     * @param idCliente Codigo del cliente
     * @throws SQLException
     */
     public void buscaFacturaCliente(String idCliente) throws SQLException {    //20101115

        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs = null;


        String            query    = "SQL_FACTURA_CLIENTE";
        listaFacturaCliente        = null;

        try {
            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );

            st           = con.prepareStatement( sql );
            
            st.setString( 1, idCliente );
            st.setString( 2, idCliente );
            st.setString( 3, idCliente );
            
            
            rs = st.executeQuery();

            listaFacturaCliente =  new LinkedList();

            while (rs.next()){
                FacturaRefinanciada facturaRefinanciada = new FacturaRefinanciada();
                listaFacturaCliente.add(facturaRefinanciada.load(rs));
            }

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA SELECCION DE FACTURAS DEL CLIENTE. \n " + e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}

        }
     }


     
     
     
     
     
     
    
    

    /**
     * Extrae una lista de las facturas PM de un multiservicio que no esten canceladas y las deja en un objeto tipo List
     * @param idMultiservicio Numero del multiservicio
     * @throws SQLException
     */
     public void buscaFacturaClienteMS(String idMultiservicio) throws SQLException {    //20101115

        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs = null;


        String            query    = "SQL_FACTURA_CLIENTE_POR_MS";
        listaFacturaCliente        = null;

        try {
            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );

            st           = con.prepareStatement( sql );
            
            st.setString( 1, idMultiservicio );
            st.setString( 2, idMultiservicio );
            st.setString( 3, idMultiservicio );
            
            
            rs = st.executeQuery();

            listaFacturaCliente =  new LinkedList();

            while (rs.next()){
                FacturaRefinanciada facturaRefinanciada = new FacturaRefinanciada();
                listaFacturaCliente.add(facturaRefinanciada.load(rs));
            }

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA SELECCION DE FACTURAS DEL CLIENTE POR MULTISERVICIO. \n " + e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}

        }
     }
     
     
     
     
     
     
     
     
 
     
     
     
     
     
     
            
    
    public String crearIngresoCabecera( String reg_status, String dstrct, String tipo_documento,String  num_ingreso, String codcli, String nitcli,
                                        String concepto, String tipo_ingreso, String fecha_consignacion, String fecha_ingreso, String branch_code,
                                        String bank_account_no, String codmoneda, String agencia_ingreso, String descripcion_ingreso,
                                        String periodo, double vlr_ingreso, double vlr_ingreso_me, double vlr_tasa, String fecha_tasa, int cant_item,
                                        int transaccion, int transaccion_anulacion, String fecha_impresion, String fecha_contabilizacion,
                                        String fecha_anulacion_contabilizacion, String fecha_anulacion, String creation_user,
                                        String creation_date, String user_update, String last_update, String base, String nro_consignacion,
                                        String periodo_anulacion, String cuenta, String auxiliar, String abc, double tasa_dol_bol, double saldo_ingreso,
                                        String cmc, String corficolombiana, String fec_envio_fiducia, String tipo_referencia_1, String referencia_1,
                                        String tipo_referencia_2, String referencia_2,String  tipo_referencia_3,String  referencia_3,  LogWriter logWriter)throws SQLException{   


        StringStatement st       = null;
        Connection      con      = null;

        String          sql      = "";
        String  comando_sql      = "";

        String          query    = "SQL_INSERTAR_INGRESO_CABECERA";
        sql                      = this.obtenerSQL(query);

        try {


            st = new StringStatement (sql, true);

            st.setString( 1, reg_status );
            st.setString( 2, dstrct );
            st.setString( 3, tipo_documento );
            st.setString( 4, num_ingreso );
            st.setString( 5, codcli );
            st.setString( 6, nitcli );
            st.setString( 7, concepto );
            st.setString( 8, tipo_ingreso );
            st.setString( 9, fecha_consignacion );
            st.setString( 10, fecha_ingreso );
            st.setString( 11, branch_code );
            st.setString( 12, bank_account_no );
            st.setString( 13, codmoneda );
            st.setString( 14, agencia_ingreso );
            st.setString( 15, descripcion_ingreso );
            st.setString( 16, periodo );
            st.setDouble( 17, vlr_ingreso );
            st.setDouble( 18, vlr_ingreso_me );
            st.setDouble( 19, vlr_tasa );
            st.setString( 20, fecha_tasa );
            st.setInt( 21, cant_item );
            st.setInt( 22, transaccion );
            st.setInt( 23, transaccion_anulacion );
            st.setString( 24, fecha_impresion );
            st.setString( 25, fecha_contabilizacion );
            st.setString( 26, fecha_anulacion_contabilizacion );
            st.setString( 27, fecha_anulacion );
            st.setString( 28, creation_user );
            st.setString( 29, creation_date );
            st.setString( 30, user_update );
            st.setString( 31, last_update );
            st.setString( 32, base );
            st.setString( 33, nro_consignacion );
            st.setString( 34, periodo_anulacion );
            st.setString( 35, cuenta );            
            st.setString( 36, auxiliar );
            st.setString( 37, abc );
            st.setDouble( 38, tasa_dol_bol );
            st.setDouble( 39, saldo_ingreso );
            st.setString( 40, cmc );
            st.setString( 41, corficolombiana );            
            st.setString( 42, fec_envio_fiducia );
            st.setString( 43, tipo_referencia_1 );
            st.setString( 44, referencia_1 );
            st.setString( 45, tipo_referencia_2 );
            st.setString( 46, referencia_2 );
            st.setString( 47, tipo_referencia_3 );
            st.setString( 48, referencia_3 );

            comando_sql = st.getSql();

        }catch(Exception e){
            logWriter.log("Error al generar string del SQL : SQL_INSERTAR_INGRESO_CABECERA");
            logWriter.log(comando_sql);
            Util.imprimirTrace(e);
            comando_sql = "";
            throw new SQLException("ERROR DURANTE LA CREACION DE LA CABECERA DEL INGRESO CABECERA DE LA NC DE REFINANCIACION. \n " + e.getMessage());
        }
        finally{
            if (st  != null){ try{ st = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return comando_sql;


    }




    public String crearIngresoDetalle( String reg_status, String dstrct, String tipo_documento, String num_ingreso, int item, String nitcli,
                                        double valor_ingreso, double valor_ingreso_me, String factura, String fecha_factura, String codigo_retefuente,
                                        double valor_retefuente, double valor_retefuente_me, String tipo_doc, String documento, String  codigo_reteica,
                                        double valor_reteica, double valor_reteica_me, double valor_diferencia_tasa, String creation_user,
                                        String creation_date, String user_update, String last_update, String base, String cuenta, String auxiliar,
                                        String fecha_contabilizacion, String fecha_anulacion_contabilizacion, String periodo,
                                        String fecha_anulacion, String periodo_anulacion, int transaccion, int transaccion_anulacion,
                                        String descripcion, double valor_tasa, double saldo_factura, String procesado, String ref1,
                                        String tipo_referencia_1, String referencia_1, String tipo_referencia_2,String  referencia_2,
                                        String tipo_referencia_3, String referencia_3, LogWriter logWriter)throws SQLException{   // 20101115


        StringStatement st       = null;
        Connection      con      = null;

        String          sql      = "";
        String  comando_sql      = "";

        String          query    = "SQL_INSERTAR_INGRESO_DETALLE";
        sql                      = this.obtenerSQL(query);

        try {


            st = new StringStatement (sql, true);

            st.setString( 1, reg_status );
            st.setString( 2, dstrct );
            st.setString( 3, tipo_documento );
            st.setString( 4, num_ingreso );
            st.setInt   ( 5, item );
            st.setString( 6, nitcli );
            st.setDouble( 7, valor_ingreso );
            st.setDouble( 8, valor_ingreso_me );
            st.setString( 9, factura );
            st.setString( 10, fecha_factura );
            st.setString( 11, codigo_retefuente );
            st.setDouble( 12, valor_retefuente  );
            st.setDouble( 13, valor_retefuente_me );
            st.setString( 14, tipo_doc );
            st.setString( 15, documento );
            st.setString( 16, codigo_reteica );
            st.setDouble( 17, valor_reteica );
            st.setDouble( 18, valor_reteica_me );            
            st.setDouble( 19, valor_diferencia_tasa );
            st.setString( 20, creation_user );
            st.setString( 21, creation_date );
            st.setString( 22, user_update );
            st.setString( 23, last_update );
            st.setString( 24, base );
            st.setString( 25, cuenta );
            st.setString( 26, auxiliar );
            st.setString( 27, fecha_contabilizacion );
            st.setString( 28, fecha_anulacion_contabilizacion );
            st.setString( 29, periodo );
            st.setString( 30, fecha_anulacion );
            st.setString( 31, periodo_anulacion );
            st.setInt( 32, transaccion );
            st.setInt( 33, transaccion_anulacion );
            st.setString( 34, descripcion );
            st.setDouble( 35, valor_tasa );
            st.setDouble( 36, saldo_factura ); 
            st.setString( 37, procesado );
            st.setString( 38, ref1 );
            st.setString( 39, tipo_referencia_1 );
            st.setString( 40, referencia_1 );
            st.setString( 41, tipo_referencia_2 );
            st.setString( 42, referencia_2 );
            st.setString( 43, tipo_referencia_3 );
            st.setString( 44, referencia_3 );
           

            comando_sql = st.getSql();

        }catch(Exception e){
            logWriter.log("Error al generar string del SQL : SQL_INSERTAR_INGRESO_DETALLE");
            logWriter.log(comando_sql);
            Util.imprimirTrace(e);
            comando_sql = "";
            throw new SQLException("ERROR DURANTE LA CREACION DE LA CABECERA DEL INGRESO DETALLE DE LA NC DE REFINANCIACION. \n " + e.getMessage());
        }
        finally{
            if (st  != null){ try{ st = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return comando_sql;


    }


    

    public String actualizarCabeceraCxC(String documento, double abono)throws SQLException{

        StringStatement st         = null;

        String comando_sql  = "";

        String            query    = "SQL_ACTUALIZA_SALDOS_CXC";

        try {

            st = new StringStatement (this.obtenerSQL(query), true);

            st.setDouble(1,abono );
            st.setDouble(2,abono );
            st.setDouble(3,abono );
            st.setDouble(4,abono );
            st.setString(5,documento );

            comando_sql = st.getSql();

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA ACTUALIZACION DE LA CABECERA POR AJUSTE. \n " + e.getMessage());
        }
        finally{

            if (st  != null){
                try{ st = null;
                }
                catch(Exception e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
        }
        return comando_sql;
    }

    

            


    public Double getIvaNM(String documento) throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs = null;


        String            query      =  "SQL_GET_IVA_NM";
        Double            valorIvaNM = 0.00;

        try {
            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            st.setString(1, documento );

            rs = st.executeQuery();
            if(rs.next()){
                valorIvaNM =  rs.getDouble("valor_iva") ;
            }

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL VALOR DEL IVA. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }
        return valorIvaNM;
    }

        

    public Double getIvaCF(String documento) throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs = null;


        String            query      =  "SQL_GET_IVA_CF";
        Double            valorIvaCF = -1.00;

        try {
            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            st.setString(1, documento );

            rs = st.executeQuery();
            if(rs.next()){
                valorIvaCF =  rs.getDouble("valor_iva") ;
            }

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL VALOR DEL IVA. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }
        return valorIvaCF;
    }        
        
    

    
    public double getValorIPM( String comprobanteIPM )  throws SQLException{


        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs = null;


        String            query      =  "SQL_GET_COMPROBANTE_IPM";
        Double            valorIPM = 0.00;

        try {
            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            st.setString(1, comprobanteIPM  );

            rs = st.executeQuery();
            if(rs.next()){
                valorIPM =  rs.getDouble("valor_ipm") ;
            }

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL VALOR DE LAS IPM. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }
        return valorIPM;
    }                
        
        
        

    
    
    public String getCuentaCabeceraPM( String pm )  throws SQLException{


        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs = null;


        String            query      =  "SQL_GET_CUENTA_CABECERA_PM";
        String            cuenta     =  "";

        try {
            con   = this.conectarJNDI( query );
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            st.setString(1, pm );

            rs = st.executeQuery();
            if(rs.next()){
                cuenta =  rs.getString("cuenta") ;
            }

        }catch(Exception e){
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LA CUENTA DE LA CABECERA DE LA PM. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(con);
        }
        return cuenta;
    }                
            
    // *************************************************************************
    // *
    // *  FINAL DEL AREA DE METODOS DEL PROCESO DE REFINANCIACION
    // *
    // *************************************************************************

     /**
     *Metodo que genera la factura de traslado de selectric a fintra.
     * @author Ing.Edgar Gonzalez Mendoza.
     * @version 1.0
     * @param usuario
     * @throws java.sql.SQLException
     */
    public void generarCxcFintra(Usuario usuario)throws SQLException{
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;

        String query = "SQL_CXC_FINTRA";
        boolean retorno = false;

        try {
            con = this.conectarJNDI(query, usuario.getBd());
            String sql = obtenerSQL(query);
            st = con.prepareStatement(sql);
            st.setString(1, usuario.getLogin());
            rs = st.executeQuery();
            if (rs.next()) {
                retorno = rs.getBoolean("retorno");
            }

        } catch (Exception e) {
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA CREACION DE LA CXX A FINTRA  \n " + e.getMessage());
        } finally {
            st.close();
            this.desconectar(con);
        }
    }
    
    
     /**
     *Metodo que genera la factura de traslado de selectric a fintra.
     * @author Ing.Edgar Gonzalez Mendoza.
     * @version 1.0
     * @param usuario
     * @throws java.sql.SQLException
     */
    public void generarTrasladoFacturas(Usuario usuario)throws SQLException{
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;

        String query = "SQL_TRASLADO_FACTURAS";
        boolean retorno = false;

        try {
            con = this.conectarJNDI(query, usuario.getBd());
            String sql = obtenerSQL(query);
            st = con.prepareStatement(sql);
            st.setString(1, usuario.getLogin());
            rs = st.executeQuery();
            if (rs.next()) {
                retorno = rs.getBoolean("retorno");
            }

        } catch (Exception e) {
            Util.imprimirTrace(e);
            throw new SQLException("ERROR DURANTE LA CREACION DE LA CXX A FINTRA  \n " + e.getMessage());
        } finally {
            st.close();
            this.desconectar(con);
        }
    }
        
    

}