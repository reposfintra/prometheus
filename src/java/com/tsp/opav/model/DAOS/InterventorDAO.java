/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.opav.model.DAOS;
import java.sql.*;
import java.util.*;
import com.tsp.opav.model.beans.*;
import com.tsp.operation.model.TransaccionService;
import com.tsp.operation.model.beans.StringStatement;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Ing. Jose Castro
 */
public class InterventorDAO  extends MainDAO{

        public InterventorDAO()  {
        super("InterventorDAO.xml");
        }
        public InterventorDAO(String dataBaseName)  {
        super("InterventorDAO.xml", dataBaseName);
        }


/**
 *
 * @return
 * @throws NullPointerException
 * @throws Exception
 */
    public ArrayList listadoInterventores() throws NullPointerException,Exception{
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query="SQL_LISTADO_INTERVENTORES";
        ArrayList resultado = new ArrayList();
        Interventor i = new Interventor();
        try{
            con = this.conectarJNDI(query);
            if(con!=null){
            String sql = obtenerSQL(query);
            st = con.prepareStatement(sql);
            rs=st.executeQuery();

            i = new Interventor();
            i.setCodigo("0");
            i.setNombre("Seleccione un Interventor");
            resultado.add(i);
            i = null;
            while(rs.next()){
                i = new Interventor();
                i.setCodigo(rs.getString("table_code"));
                i.setNombre(rs.getString("descripcion"));
                resultado.add(i);
                i = null;
            }

        }}
        catch(NullPointerException ec){
            throw new NullPointerException("No hay resultado ..." + ec.toString());
        }
        catch(Exception ec){
            throw new Exception("Error en la consulta buscar por: "+ec.toString());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return resultado;
    }





/**
 *
 * @param cod_interventor
 * @return
 * @throws NullPointerException
 * @throws Exception
 */
    public ArrayList listadoDepartamentos(String cod_interventor, String opcion) throws NullPointerException,Exception{
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query="";

        if(opcion.equals("listadepartamentos"))query ="SQL_LISTADO_DEPARTAMENTOS";
        else query= "SQL_LISTADO_DEPARTAMENTOS_ASOCIADOS";


        ArrayList resultado = new ArrayList();
        Interventor i = new Interventor();
        try{
            con = this.conectarJNDI(query);
            if(con!=null){
            String sql = obtenerSQL(query);
            st = con.prepareStatement(sql);
            st.setString(1, cod_interventor);
            rs=st.executeQuery();

            i = new Interventor();
            while(rs.next()){
                i = new Interventor();
                i.setCodigo(rs.getString("department_code"));
                i.setNombre(rs.getString("department_name"));
                resultado.add(i);
                i = null;
            }

        }}
        catch(NullPointerException ec){
            throw new NullPointerException("No hay resultado ..." + ec.toString());
        }
        catch(Exception ec){
            throw new Exception("Error en la consulta buscar por: "+ec.toString());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return resultado;
    }




/**
 *
 * @param descripcion
 * @param precio
 * @param consec
 * @param tipo
 * @param medida
 * @param categoria
 * @param usuario
 * @throws Exception
 * MODIFICADO JOSE CASTRO MATERIALES
 */
public void insertarRelacionInterventor(ArrayList listadoDepartamentos, String cod_inter, String login) throws Exception {
        StringStatement st = null;
        String query0 = "SQL_UPDATE_DTPO_INTER";
        String query="SQL_INSERTAR_INT_DPTO";
        String ejecutable = "";
        int rowCount=0;
        try{

            st = new StringStatement(this.obtenerSQL(query0), true);//JJCastro fase2
            st.setString(1, login);
            st.setString(2, cod_inter);
            ejecutable = ejecutable + st.getSql();
            st = null;


            for (int i = 0; i < listadoDepartamentos.size(); i++) {
            st = new StringStatement(this.obtenerSQL(query), true);
            st.setString(1, (String)listadoDepartamentos.get(i));
            st.setString(2, cod_inter);
            st.setString(3, login);
            ejecutable = ejecutable + st.getSql();
            st = null;
            }


            TransaccionService scv = new TransaccionService(this.getDatabaseName());
            if (!ejecutable.equals("")) {
                try {
                    scv.crearStatement();
                    scv.getSt().addBatch(ejecutable);
                    scv.execute();
                } catch (SQLException e) {
                    throw new SQLException("ERROR DURANTE SQL_INSERTAR_INT_DPTO" + e.getMessage());
                }
            }
            ejecutable = null;

        }
        catch(Exception ec){
            throw new Exception("Error en la consulta insertar relacion interventor Depto: "+ec.toString());
        }finally{
            if (st  != null){ try{ st = null;              } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }
    }





        public ArrayList listadoSolicitudes(String cod_interventor, String opcion) throws NullPointerException,Exception{
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query="SQL_SOLICITUDES_ASIGNAR";


        ArrayList resultado = new ArrayList();
        Interventor i = new Interventor();
        try{
            con = this.conectarJNDI(query);
            if(con!=null){
            String sql = obtenerSQL(query);
            st = con.prepareStatement(sql);
            st.setString(1, cod_interventor);
            st.setString(2, opcion.equals("listadepartamentosasociados")?cod_interventor:"");
            rs=st.executeQuery();



            //&aacute
            i = new Interventor();
            while(rs.next()){
                i = new Interventor();
                i.setCodigo(rs.getString("id_solicitud"));
                i.setNombre(resetTilde(rs.getString("nombre")).trim().replaceAll("&", ""));
                resultado.add(i);
                i = null;
            }

        }}
        catch(NullPointerException ec){
            throw new NullPointerException("No hay resultado ..." + ec.toString());
        }
        catch(Exception ec){
            throw new Exception("Error en la consulta buscar por: "+ec.toString());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return resultado;
    }



    private String resetTilde(String val){
            if(val==null){
               val = "";
            }else{
            val = val.replaceAll("�", "a");
            val = val.replaceAll("�", "e");
            val = val.replaceAll("�", "i");
            val = val.replaceAll("�", "o");
            val = val.replaceAll("�", "u");

            val = val.replaceAll("�", "A");
            val = val.replaceAll("�", "E");
            val = val.replaceAll("�", "I");
            val = val.replaceAll("�", "O");
            val = val.replaceAll("�", "U");



            }
            return val;
    }

//--------------------- Asociar Solicitud


/**
 *
 * @param listadoSolicitudes
 * @param cod_inter
 * @param login
 * @throws Exception
 */
public void insertarSolicitudInterventor(ArrayList listadoSolicitudes, String cod_inter, String login) throws Exception {
        StringStatement st = null;
        String query0 = "SQL_UPDATE_QUITAR_INTERVENTOR";
        String query = "SQL_UPDATE_INTERVENTOR_SOLICITUD";
        String ejecutable = "";
        int rowCount=0;
        try{


            st = new StringStatement(this.obtenerSQL(query0), true);//JJCastro fase2
            st.setString(1, login);
            st.setString(2, cod_inter);
            ejecutable = ejecutable + st.getSql();
            st = null;


            for (int i = 0; i < listadoSolicitudes.size(); i++) {
            st = new StringStatement(this.obtenerSQL(query), true);
            st.setString(1, login);
            st.setString(2, cod_inter);
            st.setString(3, (String)listadoSolicitudes.get(i));
            ejecutable = ejecutable + st.getSql();
            st = null;
            }


            TransaccionService scv = new TransaccionService(this.getDatabaseName());
            if (!ejecutable.equals("")) {
                try {
                    scv.crearStatement();
                    scv.getSt().addBatch(ejecutable);
                    scv.execute();
                } catch (SQLException e) {
                    throw new SQLException("ERROR DURANTE SQL_INSERTAR_INT_DPTO" + e.getMessage());
                }
            }
            ejecutable = null;

        }
        catch(Exception ec){
            throw new Exception("Error en la consulta insertar relacion interventor Depto: "+ec.toString());
        }finally{
            if (st  != null){ try{ st = null;              } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }
    }


public String  obtenerInterventor(String id_solicitud, String tipo_accion) throws Exception {
    Connection con = null;
    PreparedStatement ps = null;
    ResultSet rs = null;
    String query = "SQL_OBTENER_INTERVENTOR_ASOCIAR";
    String interventor = "";
    String ejecutar = "";

    ejecutar = tipo_accion.equals("Oferta")?" 'Programado', 'ValorAgregado' ":"";
    if (ejecutar.equals(""))//20101001
    ejecutar = tipo_accion.equals("NuevaAccion")?" 'Emergencia' ":"";


    try {
        con = this.conectarJNDI(query);
        if (con != null) {
            ps = con.prepareStatement(this.obtenerSQL(query).replaceAll("#TIPOACCION#", ejecutar));
            ps.setString(1, id_solicitud);
            rs = ps.executeQuery();
            if (rs.next()) {
            interventor = reset(rs.getString("interventor"));
            }


        }
    }
        catch(Exception ec){
            throw new Exception("Error en la consulta insertar  interventor Solicitud: "+ec.toString());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return interventor;
    }


/**
 *
 * @param id_solicitud
 * @param interventor
 * @throws Exception
 */
    public void updateOfertasInteventor(String id_solicitud, String interventor, String login) throws Exception {
        PreparedStatement st = null;
        Connection con = null;
        String query="SQL_UPDATE_INTERVENTOR_SOLICITUD";
        int rowCount=0;
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
           st = con.prepareStatement(this.obtenerSQL(query));
           st.setString(1,login);
           st.setString(2,interventor);
           st.setString(3,id_solicitud);
            rowCount=st.executeUpdate();
            if(rowCount>0) System.out.println("Oferta actualizada..."+id_solicitud);
            else System.out.println("no se inserto la fila");
            } }
        catch(Exception ec){
            throw new Exception("Error al actualizar la oferta: "+ec.toString());
        }finally{
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }

    }


    private String reset(String val){
            if(val==null)
               val = "";
            return val;
    }


}