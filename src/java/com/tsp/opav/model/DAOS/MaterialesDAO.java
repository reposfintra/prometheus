/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.opav.model.DAOS;
import java.sql.*;
import java.util.*;
import com.tsp.opav.model.beans.*;
import com.tsp.operation.model.beans.StringStatement;
/**
 *
 * @author Rhonalf
 */
public class MaterialesDAO extends MainDAO {

    public MaterialesDAO() {
        super("MaterialesDAO.xml");
    }
    public MaterialesDAO(String dataBaseName) {
        super("MaterialesDAO.xml", dataBaseName);
    }

/**
 *
 * @param codigo
 * @param usuario
 * @throws Exception
 */
    public void anularProducto(String codigo,String usuario) throws Exception{
        PreparedStatement st = null;
        Connection con = null;
        String query="ANULAR_PROD";
        try{
            con = this.conectarJNDI(query);
            if(con!=null){
            String sql = obtenerSQL(query);
            st = con.prepareStatement(sql);
            st.setString(1,usuario);
            st.setString(2,codigo);
            st.executeUpdate();
        }}
        catch(Exception ec){
            throw new Exception("Error en la consulta anular producto: "+ec.toString());
        }finally{
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }

/**
 *
 * @param descripcion
 * @param precio
 * @param consec
 * @param tipo
 * @param medida
 * @param categoria
 * @param usuario
 * @throws Exception
 */
public void insertarProducto(String reg_status, String descripcion,double precio,String consec,String tipo,String medida,String usuario,  int categoria, int subcat, int tiposubcat, String empaque, String alcance, String certificado, String ente_certificador,double valor_compra) throws Exception {
        PreparedStatement st = null;
        Connection con = null;
        String query="INSERTAR_PROD";
        int rowCount=0;
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
           st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1,reg_status);
            st.setString(2,descripcion);
            st.setDouble(3, precio);
            st.setString(4, consec);
            st.setString(5, tipo);
            st.setString(6, usuario);
            st.setString(7, medida);
            st.setDouble(8, valor_compra);//05082010
            st.setInt(9, categoria);//05082010
            st.setInt(10, subcat);//05082010
            st.setInt(11, tiposubcat);//05082010
         
            st.setString(12, empaque);//05082010
            st.setString(13, alcance);//06082010
            st.setString(14, certificado);//05082010
            st.setString(15, ente_certificador);//06082010

            rowCount=st.executeUpdate();
            if(rowCount>0) System.out.println("Producto agregado...");
            else System.out.println("Oops ... paso algo porque no se inserto la fila");
            } }
        catch(Exception ec){
            throw new Exception("Error en la consulta insertar producto: "+ec.toString());
        }finally{
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }

/**
 *
 * @return
 * @throws Exception
 */
    private int getMaxProd() throws Exception {
        int max = 0;
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query="GENERAR_CONS";
        String cod="PR000001";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                //System.out.println("Conectado...");
                String sql = obtenerSQL(query);
                //System.out.println("Query "+sql);
                st = con.prepareStatement(sql);
                //System.out.println("Antes de ejecutar...");
                rs = st.executeQuery();
                //System.out.println("Despues de ejecutar...");
                while (rs.next()) {
                    cod = rs.getString("cod_producto");
                    //System.out.println("codigo "+cod+" descripcion "+dsc);
                }
                max = Integer.parseInt(cod.substring(2));

            }
        }catch(Exception ec){
            throw new Exception("Error en la consulta listar todos los productos: "+ec.toString());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return max;

    }

    public int contarDigitos(int numero){
        int contador=0;
        while(numero>0){
            numero /=10;
            contador++;
        }
        return contador;
    }

    public String contarProductos(){
        int contar=0;
        String r="PR";
        try{
            contar=this.getMaxProd()+1;
            int ndig = this.contarDigitos(contar);
            switch(ndig){
                case 0:
                    r="PR000001";
                break;
                case 1:
                    r +="00000"+contar;
                break;
                case 2:
                    r +="0000"+contar;
                break;
                case 3:
                    r +="000"+contar;
                break;
                case 4:
                    r +="00"+contar;
                break;
                case 5:
                    r +="0"+contar;
                break;
                default:
                    r+=contar;
                break;
            }
        }
        catch(Exception e){
            System.out.println("Error en generacion de consecutivo: "+e.toString());
            e.printStackTrace();
        }
        return r;
    }

    public ArrayList verTodos() throws Exception{
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query="VER_TODOS";
        ArrayList resultado = new ArrayList();
        Material m = null;
        String tipo="";
        try{
            con = this.conectarJNDI(query);
            if(con!=null){
            String sql = obtenerSQL(query);
            st = con.prepareStatement(sql);
            rs=st.executeQuery();
            while(rs.next()){
                m = new Material();
                m.setCodigo(rs.getString("cod_material"));
                m.setDescripcion(rs.getString("descripcion"));
                m.setValor(Double.parseDouble(rs.getString("precio")));
                m.setMedida(rs.getString("medida"));
                if(rs.getString("tipo_material").equals("M")) tipo = "Material";
                if(rs.getString("tipo_material").equals("D")) tipo = "Mano de obra";
                if(rs.getString("tipo_material").equals("O")) tipo = "Otros";
                m.setTipo(tipo);
                m.setIdMaterial(rs.getString("idmaterial"));
                m.setIdMaterialAsociado(reset(rs.getString("excodigo")));
                resultado.add(m);
            }

        }}
        catch(Exception ec){
            System.out.println("error en verTodos"+ec.toString());
            throw new Exception("Error en la consulta listar todos los productos: "+ec.toString());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return resultado;
    }

    public ArrayList listarMateriales() throws Exception{
        ArrayList resultado = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query="LISTAR_MAT";
        Material m = null;
        try{
            con = this.conectarJNDI(query);
            if(con!=null){
            String sql = obtenerSQL(query);
            st = con.prepareStatement(sql);
            rs=st.executeQuery();
            while(rs.next()){
                m = new Material();
                m.setCodigo(rs.getString("cod_producto"));
                m.setDescripcion(rs.getString("descripcion"));
                m.setValor(Double.parseDouble(rs.getString("precio")));
                m.setMedida(rs.getString("medida"));
                m.setTipo(rs.getString("tipo"));
                resultado.add(m);
            }

        }}
        catch(Exception ec){
            throw new Exception("Error en la consulta listar todos los materiales: "+ec.toString());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
         return resultado;
    }

 /**
  *
  * @return
  * @throws Exception
  */
    public ArrayList listarManos() throws Exception{
        ArrayList resultado = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query="LISTAR_MANO";
        Material m = null;
        try{
            con = this.conectarJNDI(query);
            if(con!=null){
            String sql = obtenerSQL(query);
            st = con.prepareStatement(sql);
            rs=st.executeQuery();
            while(rs.next()){
                m = new Material();
                m.setCodigo(rs.getString("cod_producto"));
                m.setDescripcion(rs.getString("descripcion"));
                m.setValor(Double.parseDouble(rs.getString("precio")));
                m.setMedida(rs.getString("medida"));
                m.setTipo(rs.getString("tipo"));
                resultado.add(m);
            }

        }}
        catch(Exception ec){
            throw new Exception("Error en la consulta listar todos las manos: "+ec.toString());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return resultado;
    }


 /**
 *
 * @return
 * @throws Exception
 */
    public ArrayList listarOtros() throws Exception{
        ArrayList resultado = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query="LISTAR_OTROS";
        Material m = null;
        try{
            con = this.conectarJNDI(query);
            if(con!=null){
            String sql = obtenerSQL(query);
            st = con.prepareStatement(sql);
            rs=st.executeQuery();
            while(rs.next()){
                m = new Material();
                m.setCodigo(rs.getString("cod_producto"));
                m.setDescripcion(rs.getString("descripcion"));
                m.setValor(Double.parseDouble(rs.getString("precio")));
                m.setMedida(rs.getString("medida"));
                m.setTipo(rs.getString("tipo"));
                resultado.add(m);
            }

        }}
        catch(Exception ec){
            throw new Exception("Error en la consulta listar todos los otros: "+ec.toString());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return resultado;
    }

    //091022
    /*public ArrayList buscarPor(int filtro,String cadena) throws Exception{
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query="BUSCAR_FILTRO";
        ArrayList resultado = new ArrayList();
        String cod="";
        String dsc="";
        String cad="";
        String pr="";
        String id="";
        try{
            con = this.conectarJNDI(query);
            String sql = obtenerSQL(query);
            st = con.prepareStatement(sql.replaceAll("#PARAM", this.opcion(filtro)));
            st.setString(1, "%"+cadena+"%");
            rs=st.executeQuery();
            while(rs.next()){
                cod=rs.getString("cod_material");
                dsc=rs.getString("descripcion");
                pr=rs.getString("precio");
                id=rs.getString("idmaterial");
                cad=cod+";"+dsc+";"+pr+";"+id+";";
                resultado.add(cad);
            }
            return resultado;
        }
        catch(NullPointerException ec){
            throw new NullPointerException("No hay resultado ..." + ec.toString());
        }
        catch(Exception ec){
            throw new Exception("Error en la consulta buscar por: "+ec.toString());
        }
        finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("Error al cerrar la conexion " + e.getMessage() );
                }
            }
            this.desconectar(con);
        }
    }*/

    /**
     *
     * @param filtro
     * @param cadena
     * @return
     * @throws NullPointerException
     * @throws Exception
     */
    public ArrayList buscarPor(int filtro,String cadena) throws NullPointerException,Exception{
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query="BUSCAR_FILTRO";
        ArrayList resultado = new ArrayList();
        Material m = new Material();
        try{
            con = this.conectarJNDI(query);
            if(con!=null){
            String sql = obtenerSQL(query);
            st = con.prepareStatement(sql.replaceAll("#PARAM", this.opcion(filtro)));
            st.setString(1, "%"+cadena+"%");
            rs=st.executeQuery();
            while(rs.next()){
                m = new Material();//091031
                m.setCodigo(rs.getString("cod_material"));
                m.setDescripcion(rs.getString("descripcion"));
                m.setValor(Double.parseDouble(rs.getString("precio")));
                m.setIdMaterial(rs.getString("idmaterial"));

                m.setMedida(rs.getString("medida"));//091029

                m.setRegStatus(rs.getString("reg_status"));//091031
                m.setCategoria(rs.getString("categoria"));//091217
                m.setTipo(rs.getString("tipo_material"));//091217
                resultado.add(m);
            }

        }}
        catch(NullPointerException ec){
            throw new NullPointerException("No hay resultado ..." + ec.toString());
        }
        catch(Exception ec){
            throw new Exception("Error en la consulta buscar por: "+ec.toString());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return resultado;
    }


    private String opcion(int tipo){
        String txt="";
        switch(tipo){
            case 1:
              txt="cod_material";
            break;
            case 2:
              txt="descripcion";
            break;
            case 3:
              txt="precio";
            break;
            default:
              txt="cod_material";
            break;
        }
        return txt;
    }

    private String reset(String val){
            if(val==null)
               val = "";
            return val;
    }

    //091202
    public ArrayList cargaTipo(int tipo) throws NullPointerException,Exception{
        ArrayList resp=new ArrayList();
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query="TIPO_MATS";
        switch(tipo){
            case 1:
                query = "TIPO_MATS";
            break;
            case 2:
                query = "TIPO_MANO";
            break;
            case 3:
                query = "TIPO_OTROS";
            break;
        }
         try{
            con = this.conectarJNDI(query);
            if(con!=null){
            String sql = obtenerSQL(query);
            st = con.prepareStatement(sql);
            rs=st.executeQuery();
            while(rs.next()){
                resp.add(rs.getString(1));
            }

        }}
        catch(NullPointerException ec){
            throw new NullPointerException("No hay resultado ..." + ec.toString());
        }
        catch(Exception ec){
            throw new Exception("Error en la consulta buscar categorias: "+ec.toString());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return resp;
    }

    //091203
    public ArrayList buscarPor(int categoria) throws NullPointerException,Exception {
        ArrayList resp=new ArrayList();
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query="TIP_MATS";
        switch(categoria){
            case 1:
                    query = "TIP_MATS";
            break;
            case 2:
                    query = "TIP_MAN";
            break;
            case 3:
                    query = "TIP_OTS";
            break;
        }
        Material mt = null;
        try{
            con = this.conectarJNDI(query);
            if(con!=null){
            String sql = obtenerSQL(query);
            st = con.prepareStatement(sql);
            rs=st.executeQuery();
            while(rs.next()){
                mt = new Material();
                mt.setCodigo(rs.getString("cod_material"));
                mt.setDescripcion(rs.getString("descripcion"));
                mt.setMedida(rs.getString("medida"));
                //mt.setTipo(rs.getString("tipo"));
                mt.setValor(rs.getDouble("precio"));
                mt.setCategoria(rs.getString("categoria"));
                resp.add(mt);
            }

        }}
        catch(NullPointerException ec){
            throw new NullPointerException("No hay resultado ..." + ec.toString());
        }
        catch(Exception ec){
            throw new Exception("Error en la consulta buscar por categorias: "+ec.toString());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return resp;
    }



 /**
  *
  * @param tipo
  * @return
  * @throws Exception
  */
public ArrayList getCategorias(String tipo) throws Exception{
        Connection          con     = null;
        PreparedStatement   ps      = null;
        ResultSet           rs      = null;
        String              query   = "";
        String              sql     = "";
        ArrayList           cats  = new ArrayList();

        if(tipo.equals("m")){  query   = "TIPO_MATS";   }
        if(tipo.equals("d")){  query   = "TIPO_MANO";   }
        if(tipo.equals("o")){  query   = "TIPO_OTROS";  }

        try{
            con         = this.conectarJNDI(query);
            if(con!=null){
            sql         = this.obtenerSQL(query);
            ps          = con.prepareStatement(sql);
            rs          = ps.executeQuery();

            while(rs.next()){
                cats.add( rs.getString("cat") );
            }

        }}
        catch(Exception e){
            throw new Exception(e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION " + e.getMessage()); }}
        }
        return cats;
    }

/**
 *
 * @param filtro
 * @param cadena
 * @return
 * @throws Exception
 */
public ArrayList buscarPorAnul(int filtro,String cadena) throws Exception{
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query="BUSCAR_ANUL";
        ArrayList resultado = new ArrayList();
        Material m = new Material();
        try{
            con = this.conectarJNDI(query);
             if(con!=null){
            String sql = obtenerSQL(query);
            st = con.prepareStatement(sql.replaceAll("#PARAM", this.opcion(filtro)));
            st.setString(1, "%"+cadena+"%");
            rs=st.executeQuery();
            while(rs.next()){
                m = new Material();//091031
                m.setCodigo(rs.getString("cod_material"));
                m.setDescripcion(rs.getString("descripcion"));
                m.setValor(Double.parseDouble(rs.getString("precio")));
                m.setIdMaterial(rs.getString("idmaterial"));
                m.setCategoria(rs.getString("categoria"));//091210
                m.setMedida(rs.getString("medida"));//091029
                m.setTipo(rs.getString("tipo_material"));
                m.setRegStatus(rs.getString("reg_status"));//091031

                resultado.add(m);
            }

        }}
        catch(NullPointerException ec){
            throw new NullPointerException("No hay resultado ..." + ec.toString());
        }
        catch(Exception ec){
            throw new Exception("Error en la consulta buscar por: "+ec.toString());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return resultado;
    }

/**
     *
     * @param filtro
     * @param cadena
     * @return
     * @throws NullPointerException
     * @throws Exception
     * Created by: Jose Castro
     */
    public ArrayList buscarPorMatCat(int filtro,String cadena, int idcat, int idsubcat, int idtiposub,String tipomat) throws NullPointerException,Exception{
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query="BUSCAR_FILTRO_MATERIALES";
        ArrayList resultado = new ArrayList();
        Material m = new Material();
        try{
            con = this.conectarJNDI(query);
            if(con!=null){
            String sql = obtenerSQL(query);
            sql  = sql.replaceAll("#PARAM", "m."+this.opcion(filtro));
            sql = idcat!=0? sql.replaceAll("#CATEGORIA#", " AND c.idcategoria = "+idcat+""):sql.replaceAll("#CATEGORIA#", "");
            sql = idsubcat!=0? sql.replaceAll("#SUBCATEGORIA#", " AND s.idsubcategoria = "+idsubcat+""):sql.replaceAll("#SUBCATEGORIA#", "");
            sql = idtiposub!=0? sql.replaceAll("#TSUB#", " AND t.idtiposubcategoria = "+idtiposub+""):sql.replaceAll("#TSUB#", "");
            sql = sql.replaceAll("#tipomat#", "and m.tipo_material ='"+tipomat+"' ");

            st = con.prepareStatement(sql);
            st.setString(1, "%"+cadena+"%");
            rs=st.executeQuery();
            while(rs.next()){
                m = new Material();//091031
                m.setCodigo(rs.getString("cod_material"));
                m.setDescripcion(rs.getString("descripcion"));
                m.setValor(Double.parseDouble(rs.getString("precio")));
                m.setIdMaterial(rs.getString("idmaterial"));

                m.setMedida(rs.getString("medida"));//091029

                m.setRegStatus(reset(rs.getString("reg_status")).equals("")?"ACTIVO":"INACTIVO");
                m.setCategoria(rs.getString("categoria"));//091217
                m.setTipo(rs.getString("tipo_material"));//091217

                m.setDesc_categoria(reset(rs.getString("idcategoria")));//06082010
                m.setDesc_subcat(reset(rs.getString("idsubcategoria")));//06082010
                m.setDesc_tiposub(reset(rs.getString("idtiposubcategoria")));//06082010

                m.setUnidad_empaque(reset(rs.getString("unidad_empaque")));//06082010

                m.setEnte_certificador(reset(rs.getString("ente_certificador")));//JJCASTRO CERTIFICADOR


                resultado.add(m);
            }

        }}
        catch(NullPointerException ec){
            throw new NullPointerException("No hay resultado ..." + ec.toString());
        }
        catch(Exception ec){
            throw new Exception("Error en la consulta buscar por: "+ec.toString());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return resultado;
    }


    ////////////////////////////////

/**
 *
 * @param idmaterial
 * @return
 * @throws Exception
 */
    public String buscarPrecioMaterial(String idmaterial) throws Exception {
        Connection con = null;
        ResultSet rs = null;
        String query = "SQL_PRECIO_MATERIAL";
        String respuesta = "";
        PreparedStatement st = null;

        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setString(1, idmaterial);
                rs=st.executeQuery();
                if (rs.next()) {
                    respuesta = rs.getString("precio");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
            throw new SQLException("ERROR DURANTE SQL_PRECIO_MATERIAL. \n " + e.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();} catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();} catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }

        return respuesta;
    }

    /**
     *
     * @param filtro
     * @param cadena
     * @return
     * @throws NullPointerException
     * @throws Exception
     * Created by: Jose Castro
     */
    public ArrayList buscarCerficadores() throws NullPointerException,Exception{
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query="SQL_ENTE_CERTIFICADOR";
        ArrayList resultado = new ArrayList();
        Material m = new Material();
        try{
            con = this.conectarJNDI(query);
            if(con!=null){
            String sql = obtenerSQL(query);
            st = con.prepareStatement(sql);
            rs=st.executeQuery();

            m = new Material();
            m.setCod_ente("");
            m.setDesc_ente("Seleccione un Certificador");
            resultado.add(m);
            m = null;
            while(rs.next()){
                m = new Material();//091031
                m.setCod_ente(rs.getString("table_code"));
                m.setDesc_ente(rs.getString("descripcion"));
                resultado.add(m);
                m = null;
            }

        }}
        catch(NullPointerException ec){
            throw new NullPointerException("No hay resultado ..." + ec.toString());
        }
        catch(Exception ec){
            throw new Exception("Error en la consulta buscar por: "+ec.toString());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return resultado;
    }










     /**
     *
     * @param 
     * @return  materiales con variacion en precios
     * @throws NullPointerException
     * @throws Exception
     * Created by: Jesus pinedo
     */
    public ArrayList  getMaterialesVariaciones() throws NullPointerException,Exception{
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query="SQL_GET_MATERIALES_VARIACIONES";
        ArrayList vm = new ArrayList ();

        Material m = new Material();
        try{
            con = this.conectarJNDI(query);
            if(con!=null){
            String sql = obtenerSQL(query);
            st = con.prepareStatement(sql);
            rs=st.executeQuery();
            while(rs.next()){
                m = new Material();//091031
                m.setRegStatus(reset(rs.getString("reg_status")));
                m.setDescripcion(reset(rs.getString("descripcion")));
                m.setValor(Double.parseDouble((rs.getString("precio"))));
                m.setPrecio_ultima_compra(Double.parseDouble((rs.getString("precio_ultima_compra"))));
                m.setCodigo(reset(rs.getString("cod_material")));
                m.setMedida(reset(rs.getString("medida")));
                m.setValorCompra(Double.parseDouble(rs.getString("valor_compra")));                             
                m.setObservacion(rs.getString("observacion"));//JJCASTRO MATERIAL 04082010
                m.setFecha_actualizacion_precio_compra(reset(rs.getString("fecha_actualizacion_precio_compra")));//JJCASTRO MATERIAL 04082010
                m.setFecha_ultima_compra(reset(rs.getString("fecha_ultima_compra")));//JJCASTRO MATERIAL 04082010

                vm.add(m);
                m = null;
            }

        }}
        catch(NullPointerException ec){
            throw new NullPointerException("No hay resultado ..." + ec.toString());
        }
        catch(Exception ec){
            throw new Exception("Error en la consulta buscar por: "+ec.toString());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return vm;
    }


     /**
     *
     * @param
     * @return  materiales con variacion en precios
     * @throws NullPointerException
     * @throws Exception
     * Created by: Jesus pinedo
     */
     public void ActualizarlistaPrecios(String [] materiales,String [] obs,String [] act_precio)throws Exception{
            Connection  con=null;
            PreparedStatement ps=null;
            String sql="";
            String consulta=obtenerSQL("SQL_ACTUALIZAR_LISTA_PRECIOS");
            try{
                con=conectarJNDI("SQL_ACTUALIZAR_LISTA_PRECIOS");
                for(int i=0;i<materiales.length;i++)
                {
                    if(act_precio[i].equals("N")) {continue;}
                    StringStatement st= new StringStatement(consulta, true);
                    st.setString(1,obs[i]);
                    st.setString(2,materiales[i]);
                    sql=sql+st.getSql();
                    if(act_precio[i].equals("S"))
                    {
                      
                      sql=sql.replace("#campos",",valor_compra=precio_ultima_compra,fecha_actualizacion_precio_compra=now(),precio= ceiling(precio_ultima_compra*1.17) ");
                    }
                    else
                    {
                      sql=sql.replace("#campos",",valor_compra="+act_precio[i]+",fecha_actualizacion_precio_compra=now(),precio= ceiling("+act_precio[i]+"*1.17) ");
                    }
                }
                ps=con.prepareStatement(sql);
                ps.executeUpdate();
            }
            catch(Exception e)
            {   e.printStackTrace();
                System.out.println(e.toString());
                throw new Exception( "Actualizar lista de precios " + e.getMessage());

            }
            finally{
                if(ps!=null) ps.close();
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
            }
        }





     /**
     *
     * @param
     * @return  materiales con variacion en precios
     * @throws NullPointerException
     * @throws Exception
     * Created by: Jesus pinedo
     */
     public void ActualizarlistaPreciosInactividad(String [] materiales,String [] precios,String [] act_precio)throws Exception{
            Connection  con=null;
            PreparedStatement ps=null;
            String sql="";
            String consulta=obtenerSQL("SQL_ACTUALIZAR_LISTA_PRECIOS_INACTIVIDAD");
            try{
                con=conectarJNDI("SQL_ACTUALIZAR_LISTA_PRECIOS_INACTIVIDAD");
                for(int i=0;i<materiales.length;i++)
                {   StringStatement st= new StringStatement(consulta, true);
                    if(act_precio[i].equals("S"))
                    {                    
                    st.setString(1,materiales[i]);
                    sql=sql+st.getSql();

                    sql=sql.replace("#campos",",valor_compra="+precios[i]+",precio= ceiling("+precios[i]+"*1.17) ");
                    }

                }
                ps=con.prepareStatement(sql);
                ps.executeUpdate();
            }
            catch(Exception e)
            {   e.printStackTrace();
                System.out.println(e.toString());
                throw new Exception( "Actualizar lista de precios inactividad " + e.getMessage());

            }
            finally{
                if(ps!=null) ps.close();
                if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
            }
        }












/**
     *
     * @param
     * @return  materiales con variacion en precios
     * @throws NullPointerException
     * @throws Exception
     * Created by: Jesus pinedo
     */
    public ArrayList  getMaterialesInactividad() throws NullPointerException,Exception{
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query="SQL_GET_MATERIALES_INACTIVIDAD";
        ArrayList vm = new ArrayList ();

        Material m = new Material();
        try{
            con = this.conectarJNDI(query);
            if(con!=null){
            String sql = obtenerSQL(query);
            st = con.prepareStatement(sql);
            rs=st.executeQuery();
            while(rs.next()){
                m = new Material();//091031
                m.setRegStatus(reset(rs.getString("reg_status")));
                m.setDescripcion(reset(rs.getString("descripcion")));
                m.setValor(Double.parseDouble((rs.getString("precio"))));
                m.setPrecio_ultima_compra(Double.parseDouble((rs.getString("precio_ultima_compra"))));
                m.setCodigo(reset(rs.getString("cod_material")));
                m.setMedida(reset(rs.getString("medida")));
                m.setValorCompra(Double.parseDouble(rs.getString("valor_compra")));
                m.setObservacion(rs.getString("observacion"));//JJCASTRO MATERIAL 04082010
                m.setFecha_actualizacion_precio_compra(reset(rs.getString("fecha_actualizacion_precio_compra")));//JJCASTRO MATERIAL 04082010
                m.setFecha_ultima_compra(reset(rs.getString("fecha_ultima_compra")));//JJCASTRO MATERIAL 04082010

                m.setMax_dias_inactividad(rs.getInt("max_dias_inactividad"));//Jpinedo
                m.setDias_inactividad(rs.getInt("dias_inactividad"));//Jpinedo
                m.setNombrecategoria(rs.getString("nombre_categoria"));//Jpinedo

                vm.add(m);
                m = null;
            }

        }}
        catch(NullPointerException ec){
            throw new NullPointerException("No hay resultado ..." + ec.toString());
        }
        catch(Exception ec){
            throw new Exception("Error en la consulta buscar por: "+ec.toString());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return vm;
    }

}