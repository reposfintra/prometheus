package com.tsp.opav.model.DAOS;

import com.tsp.opav.model.beans.*;
import com.tsp.operation.model.TransaccionService;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.BeanGeneral;
import com.tsp.operation.model.beans.StringStatement;


public class ElectricaribeOtDAO extends MainDAO{

    private DatosOferta  oe;
    private OrdenTrabajo ot;

    public ElectricaribeOtDAO(){
        super("ElectricaribeOtDAO.xml");
    }
    public ElectricaribeOtDAO(String dataBaseName){
        super("ElectricaribeOtDAO.xml", dataBaseName);
    }

    public DatosOferta setEca(String id_sol) throws Exception{
        Connection          con     = null;
        PreparedStatement   ps      = null;
        ResultSet           rs      = null;
        String              query   = "SQL_OFERTA_INFO";
        String              sql     = "";
        DatosOferta         orofer  = null;

        try{
            orofer      = new DatosOferta();

            con         = this.conectar(query);
            sql         = this.obtenerSQL(query);
            ps          = con.prepareStatement(sql);
            ps.setString(1, id_sol);
            System.out.println("\n\n"+ps.toString()+"\n\n");
            rs          = ps.executeQuery();

            if(rs.next()){
                orofer.setId_solicitud(id_sol);
                orofer.setOferta(               checkVar( rs.getString("nombre_solicitud") ));
                orofer.setElaboradoPor(         checkVar( rs.getString("nombre_elaborado") ));
                orofer.setFechaGeneracion(      checkVar( rs.getString("fecha_oferta") ));
                orofer.setAprobadoPor(          checkVar( rs.getString("nombre_aprobado") ));
                orofer.setEjecutivo(            checkVar( rs.getString("nombre_ejecutivo") ));
                orofer.setCliente(              checkVar( rs.getString("nombre_cliente") ));
                orofer.setNIC(                  checkVar( rs.getString("nic") ));
                orofer.setConsecutivo(          checkVar( rs.getString("consecutivo_oferta") ));
                orofer.setCiudad(               checkVar( rs.getString("ciudad") ));
                orofer.setDepartamento(         checkVar( rs.getString("departamento") ));
                orofer.setDireccion(            checkVar( rs.getString("direccion") ));
                orofer.setRepresentante(        checkVar( rs.getString("nombre_representante") ));
                orofer.setNIT(                  checkVar( rs.getString("nit") ));
                orofer.setOtras_consideraciones(checkVar( rs.getString("otras_consideraciones") ));
                orofer.setTipo_cliente(         checkVar( rs.getString("tipo_cliente") ));
                orofer.setTelefono(             checkVar( rs.getString("telefono") ));
                orofer.setCelular(              checkVar( rs.getString("celular") ));
                orofer.setNombre_contacto(checkVar(rs.getString("nombre_contacto")));//JJCASTRO PDF OT
                orofer.setResponsable(rs.getString("responsable"));
            }
        }
        catch(Exception e){
            throw new Exception(e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(query); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }

        return orofer;
    }

    public OrdenTrabajo setOT(String id_sol) throws Exception{
        Connection          con     = null;
        PreparedStatement   ps      = null;
        ResultSet           rs      = null;
        String              query   = "SQL_OT_INFO";
        String              sql     = "";
        OrdenTrabajo        ortra;

        try{
            ortra          = new OrdenTrabajo();

            con         = this.conectar(query);
            sql         = this.obtenerSQL(query);
            ps          = con.prepareStatement(sql);
            ps.setString(1, id_sol);
            rs          = ps.executeQuery();

            if(rs.next()){
                ortra.setId_solicitud(id_sol);
                ortra.setId_multiservicio(     checkVar( rs.getString("id_orden") )    );
            }
        }
        catch(Exception e){
            throw new Exception(e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(query); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }

        return ortra;
    }

public String sqlIngresarOT(OrdenTrabajo orden, boolean flag, String usuario) throws Exception{

        Connection          con         = null;
        PreparedStatement   ps          = null;
        ResultSet           rs          = null;
        String              query1      = "SQL_GET_OPAV_NUM";
        String              query2      = "SQL_GET_TIPO_OFERTA";
        String              query3      = "SQL_GET_OPAV_YEAR";
        String              query4      = "SQL_SET_OPAV_YEAR";
        String              query5      = "SQL_GENERAR_OT";
        String              query6      = "SQL_CAMBIAR_ESTADO_ACCIONES";
        String              query7      = "SQL_UPDATE_OFERTA";
        String              query8      = "SQL_DELETE_OT";
        String              sql         = "";
        String              sqlGlobal   = "";

        String              consec      = "";
        String              sufijo      = "";
        String              year        = "";

        try{
            con         = this.conectar(query2);
            sql         = this.obtenerSQL(query2);
            sql         = sql.replaceAll("param1", orden.getId_solicitud());
            ps          = con.prepareStatement(sql);
            rs          = ps.executeQuery();
            if(rs.next()){
                sufijo = rs.getString("tipo");
            }
            //this.desconectar(query2);



            rs=null;
            ps=null;
            sql         = this.obtenerSQL(query3);
            ps          = con.prepareStatement(sql);
            rs          = ps.executeQuery();
            if(rs.next()){
                year = rs.getString("concepto");
            }

            /*
             * Esta validacion la hago para ver si el
             * a�o es el actual. En caso de no ser la
             * misma entonces el consecutivo se
             * reiniciaria.
             */
            String y    = String.valueOf( Calendar.getInstance().get(Calendar.YEAR) );
            y           = y.substring(2, 4);
            if(!y.equals(year)){
                rs=null;
                ps=null;
                sql         = this.obtenerSQL(query4);
                ps          = con.prepareStatement(sql);
                ps.setString(1, y);
                ps.setString(2, usuario);//Agregado jpena 05-04-2010
                sqlGlobal=sqlGlobal+ps.toString()+";";
                //ps.executeUpdate();
            }



            rs=null;
            ps=null;
            sql         = this.obtenerSQL(query1);
            ps          = con.prepareStatement(sql);
            rs          = ps.executeQuery();
            if(rs.next()){
                consec = rs.getString("numero");
            }

            consec = consec.replaceAll("SU", sufijo);
            consec += "-"+year;

            orden.setId_multiservicio(consec);


            if(flag == false){
            rs=null;
            ps=null;
                sql         = this.obtenerSQL(query8);
                sql         = sql.replaceAll("param1", orden.getId_solicitud());
                sql         = sql.replaceAll("param2", usuario);//Agregado jpena 05-04-2010
                ps          = con.prepareStatement(sql);
                sqlGlobal=sqlGlobal+ps.toString()+";";
                //ps.executeUpdate();

                sql = this.obtenerSQL(query5);
                sql = sql.replaceAll("param1", consec);
                sql = sql.replaceAll("param2", orden.getId_solicitud());
                sql = sql.replaceAll("param3", orden.getObservaciones());
                sql = sql.replaceAll("param5", orden.getUser_update());
                sql = sql.replaceAll("param6", orden.getUser_update());
                sqlGlobal += sql + ";";
                sql = null;

                sql = this.obtenerSQL(query7);
                sql = sql.replaceAll("param1", consec);
                sql = sql.replaceAll("param2", orden.getId_solicitud());
                sql = sql.replaceAll("param3", usuario);//Agregado jpena 05-04-2010
                sqlGlobal += sql + ";";
                sql = null;
            }


            sql = this.obtenerSQL(query6);
            sql = sql.replaceAll("param1", orden.getId_solicitud());
       sql = sql.replaceAll("param2", usuario);//Agregado jpena 05-04-2010
            sqlGlobal += sql + ";";
            sql = null;

        }
        catch(Exception e){
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();               } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();               } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(query1); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return sqlGlobal;
    }

public void ingresarOT(OrdenTrabajo orden, boolean flag, String usuario) throws Exception{
        Connection          con         = null;
        StringStatement   ps          = null;
        String              query1      = "SQL_GET_OPAV_NUM";
        String              sqlGlobal   = "";
        String[]          listasql;
        TransaccionService tser = new TransaccionService(this.getDatabaseName());
        try{
            tser.crearStatement();
            //con         = this.conectar(query1);
          sqlGlobal=this.sqlIngresarOT(orden, flag, usuario);

            //ps = con.prepareStatement(sqlGlobal);
          listasql = sqlGlobal.split(";");
          for(int i = 0; i < listasql.length; i++) {
              tser.getSt().addBatch(listasql[i]);
          }
          tser.execute();
        }
        catch(Exception e){
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
        finally{
            if (ps  != null){ try{ tser.closeAll();               } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(query1); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }

    public void insertarCotizacionOrdenCompra(OrdenTrabajo informacion,String usuario) throws Exception {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_INSERTAR_COTIZACION_NUEVO";
        String respuesta;
        try {
            con = this.conectarJNDI();
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, informacion.getId_solicitud());
            rs = ps.executeQuery();
            while (rs.next()) {
                respuesta = rs.getString("respuesta");
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (ps != null) {
                    ps.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            } catch (SQLException e) {
                e.getMessage();
            }
        }
    }


    public ArrayList<AccionesEca> getActions(String id_sol, String id_cont) throws Exception{
        Connection              con   = null;
        PreparedStatement       ps    = null;
        ResultSet               rs    = null;
        String                  query = "SQL_GET_ACCIONES";
        String                  sql   = "";
        ArrayList<AccionesEca>  aecas = new ArrayList<AccionesEca>();
        AccionesEca             aeca;

        try{
            con         = this.conectar(query);
            sql         = this.obtenerSQL(query);
            ps          = con.prepareStatement(sql);
            ps.setString(1, id_sol);
            ps.setString(2, id_cont);
            rs          = ps.executeQuery();
            while(rs.next()){
                aeca = new AccionesEca();
                aeca.setContratista(rs.getString("nombre_cont"));
                aeca.setDescripcion(rs.getString("accion"));
                aeca.setObservaciones(rs.getString("observaciones"));
                aecas.add(aeca);
            }
        }
        catch(Exception e){
            throw new Exception(e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();               } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();               } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(query);  } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }

        return aecas;
    }

    public ArrayList<Contratista> getContratistas(String id_sol) throws Exception{
        Connection              con     = null;
        PreparedStatement       ps      = null;
        ResultSet               rs      = null;
        String                  query   = "SQL_GET_CONTRATISTAS";
        String                  sql     = "";
        ArrayList<Contratista>  con_arr = new ArrayList<Contratista>();
        Contratista             contra;

        try{
            con         = this.conectar(query);
            sql         = this.obtenerSQL(query);
            ps          = con.prepareStatement(sql);
            ps.setString(1, id_sol);
            rs          = ps.executeQuery();
            while(rs.next()){
                contra = new Contratista();
                contra.setId_contratista(rs.getString("contratista"));
                contra.setNombre_contratista(rs.getString("descripcion"));
                con_arr.add(contra);
            }
            this.desconectar(query);
        }
        catch(Exception e){
            throw new Exception(e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();               } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();               } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(query);  } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }

        return con_arr;
    }

    /**
     *
     * @param cont
     * @param sol
     * @param flag
     * @return
     * @throws Exception
     * Modificado por Ing. Jose Castro
     */
    public float getValor(String cont, String sol, int flag) throws Exception{
        Connection              con     = null;
        PreparedStatement       ps      = null;
        ResultSet               rs      = null;
        String                  query   = "SQL_GET_TOTALES";
        String                  sql     = "";
        float                     numero  = 0;

        try{
            con         = this.conectarJNDI(query);//JJCASTRO FASE2
            sql         = this.obtenerSQL(query);
            ps          = con.prepareStatement(sql);
            ps.setString(1, sol);
            ps.setString(2, cont);
            rs          = ps.executeQuery();
            if(rs.next()){
//JJCASTRO PDF OT
                if(flag == 1)numero = rs.getFloat("material");
                if(flag == 2)numero = rs.getFloat("mano_obra");
                if(flag == 3)numero = rs.getFloat("transporte");
                if(flag == 4)numero = rs.getFloat("administracion");
                if(flag == 5)numero = rs.getFloat("imprevisto");
                if(flag == 6)numero = rs.getFloat("utilidad");
//JJCASTRO PDF OT

            }

        }
        catch(Exception e){
            throw new Exception(e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();               } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();               } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con);  } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }

        return numero;
    }

    public boolean isOtReady(String id_sol) throws Exception{
        Connection          con     = null;
        PreparedStatement   ps      = null;
        ResultSet           rs      = null;
        String              query   = "SQL_ACCIONES_NO_APTAS";
        String              sql     = "";
        boolean             ok      = true;

        try{
            //con         = this.conectar(query);//20101122
            con= this.conectarJNDI(query);//20101122

            sql         = this.obtenerSQL(query);
            ps          = con.prepareStatement(sql);
            ps.setString(1, id_sol);
            rs          = ps.executeQuery();

            if(rs.next()){
                ok = false;
            }
        }
        catch(Exception e){
            throw new Exception(e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            //if (con != null){ try{ this.desconectar(query); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}//20101122
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}//20101122
        }

        return ok;
    }

    public boolean isOtReadyForPDF(String id_sol) throws Exception{
        Connection          con     = null;
        PreparedStatement   ps      = null;
        ResultSet           rs      = null;
        String              query   = "SQL_OT_STUFF";
        String              sql     = "";
        boolean             ok      = false;

        try{
            con         = this.conectar(query);
            sql         = this.obtenerSQL(query);
            ps          = con.prepareStatement(sql);
            ps.setString(1, id_sol);
            rs          = ps.executeQuery();

            if(rs.next()){
                ok = true;
            }
        }
        catch(Exception e){
            throw new Exception(e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(query); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }

        return ok;
    }

    public boolean isPaymentReady(String id_sol) throws Exception{
        Connection          con     = null;
        PreparedStatement   ps      = null;
        ResultSet           rs      = null;
        String              query   = "SQL_ACCIONES_NO_APTAS2";
        String              sql     = "";
        boolean             ok      = true;

        try{
            con         = this.conectar(query);
            sql         = this.obtenerSQL(query);
            ps          = con.prepareStatement(sql);
            ps.setString(1, id_sol);
            rs          = ps.executeQuery();

            if(rs.next()){
                ok = false;
            }
        }
        catch(Exception e){
            throw new Exception(e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(query); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }

        return ok;
    }

    public String checkVar(String var){
        if(var == null){
            return "";
        }
        else
        {
            return var;
        }
    }

    /* Los siguientes metodos son para
     * la aceptacion en insercion de pagos
     * de cada cliente.
     */

    public void financiacion(String fecha, String id) throws Exception{
        Connection          con     = null;
        PreparedStatement   ps      = null;
        ResultSet           rs      = null;
        String              query   = "SQL_UPDATE_FINANCIACION";
        String              sql     = "";

        try{
            con         = this.conectar(query);
            sql         = this.obtenerSQL(query);
            ps          = con.prepareStatement(sql);
            ps.setString(1, fecha);
            ps.setString(2, id);
            ps.executeUpdate();
        }
        catch(Exception e){
            throw new Exception(e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(query); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }

    public String getStuffOT(String id_sol, String atributo) throws Exception{
        Connection              con   = null;
        PreparedStatement       ps    = null;
        ResultSet               rs    = null;
        String                  query = "SQL_OT_STUFF";
        String                  sql   = "";
        ArrayList<AccionesEca>  aecas = new ArrayList<AccionesEca>();
        String                  stuff   = "";

        try{
            con         = this.conectar(query);
            sql         = this.obtenerSQL(query);
            ps          = con.prepareStatement(sql);
            ps.setString(1, id_sol);
            rs          = ps.executeQuery();

            if(rs.next()){
                stuff = rs.getString(atributo);
            }

        }
        catch(Exception e){
            System.out.println("Error"+e.getMessage());
            throw new Exception(e.getMessage());


        }
        finally{
            if (rs  != null){ try{ rs.close();               } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();               } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(query);  } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }

        return stuff;
    }

    public DatosOferta getEca(){
        return oe;
    }

    public OrdenTrabajo getOT(){
        return ot;
    }

public void insertPayments(ArrayList<SubclienteEca> array, String user) throws Exception{
        Connection          con         = null;
        PreparedStatement   ps          = null;
        ResultSet           rs          = null;
        String              query1      = "SQL_GET_OFFER_VALUE";
        String              query2      = "SQL_INSERT_PAYMENT";
        String              query3      = "SQL_UPDATE_ACTIONS";
        String              query4      = "SQL_CANCEL_PAYMENTS";
        String              sql         = "";
        String              sqlGlobal   = "";

        double              value       = 0;
        double              valorBase   = 0;
        double              valorCuotaI = 0;

        try{
            con         = this.conectarJNDI(query1);//100311
            //con         = this.conectar(query1);//100311
            sql         = this.obtenerSQL(query1);
            ps          = con.prepareStatement(sql);
            ps.setString(1, array.get(0).getId_solicitud());
            rs          = ps.executeQuery();

            if(rs.next()){
                value = rs.getDouble("suma");
            }

            this.desconectar(con);//100311
            //this.desconectar(query1);//100311

            //------------------------------------------------------

            ElectricaribeOfertaDAO eod=new ElectricaribeOfertaDAO(this.getDatabaseName());
            
            TransaccionService tservice =new TransaccionService(this.getDatabaseName());
            tservice.crearStatement();
            
            sql = this.obtenerSQL(query4);
            sql = sql.replaceAll("param1",   array.get(0).getId_solicitud());
            tservice.getSt().addBatch(sql);
            //sqlGlobal = sqlGlobal + sql + ";";

            for (int i = 0; i < array.size(); i++){

                sql         = this.obtenerSQL(query2);

                /*valorBase   = value     * (Double.parseDouble(array.get(i).getPorc_base()) / 100);
                valorCuotaI = valorBase * (Double.parseDouble(array.get(i).getPorc_cuota_inicial()) / 100);*/

                sql = sql.replaceAll("param1",   array.get(i).getId_cliente());
                sql = sql.replaceAll("param2",   array.get(i).getId_solicitud());
                sql = sql.replaceAll("param3",   array.get(i).getPorc_cuota_inicial());
                //sql = sql.replaceAll("param4",   String.valueOf(valorCuotaI));
                sql = sql.replaceAll("param4",   array.get(i).getVal_cuota_inicial());
                sql = sql.replaceAll("param5",   array.get(i).getPeriodo());
                sql = sql.replaceAll("param6",   user);
                sql = sql.replaceAll("param7",   user);
                sql = sql.replaceAll("param8",   array.get(i).getNic());
                sql = sql.replaceAll("param9",   String.valueOf(valorBase));
                sql = sql.replaceAll("parame10", array.get(i).getPorc_base());
                sql = sql.replaceAll("parame11", array.get(i).getMeses_mora());
                sql = sql.replaceAll("parame12", array.get(i).getTipo());

                //sqlGlobal = sqlGlobal + sql + ";";
                String[] stringsql=sql.split(";");
                tservice.getSt().addBatch(stringsql[0]);
                tservice.getSt().addBatch(stringsql[1]);


            }

            //------------------------------------------------------
            //-- Emergencia JCastro
            String query5 = "SQL_UPDATE_ACTIONS_EMERGENCIA";
            String tipo_solicitud = eod.tipoOferta(array.get(0).getId_solicitud());
            query3 = tipo_solicitud.equals("Emergencia")?query5:query3;
            //-- Emergencia JCastro

            sql = this.obtenerSQL(query3);
            sql = sql.replaceAll("param1",   array.get(0).getId_solicitud());
            tservice.getSt().addBatch(sql);
            tservice.getSt().addBatch(eod.actPrecio_venta(array.get(0).getId_solicitud(), "substring (replace (a.fecha_oferta,'-',''),1,6)"));
           // sqlGlobal = sqlGlobal + sql + ";"+ eod.actPrecio_venta(array.get(0).getId_solicitud(), "substring (replace (a.fecha_oferta,'-',''),1,6)");

            System.out.println("\n\n"+sqlGlobal+"\n\n");
            
            tservice.execute();

//            con         = this.conectarJNDI(query3);//100311
//            //con         = this.conectar(query3);//100311
//            ps          = con.prepareStatement(sqlGlobal);
//            ps.executeUpdate();
//            this.desconectar(con);//100311
//            //this.desconectar(query3);//100311

        }
        catch(Exception e){
            throw new Exception(e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (!con.isClosed()){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }



    public ArrayList<SubclienteEca> getPayments(String id) throws Exception{
        Connection          con     = null;
        PreparedStatement   ps      = null;
        ResultSet           rs      = null;
        String              query   = "SQL_GET_PAYMENT";
        String              sql     = "";
        ArrayList<SubclienteEca> secas = new ArrayList<SubclienteEca>();
        SubclienteEca       seca;


        try{
            con         = this.conectarJNDI(query);//100311
            //con         = this.conectar(query);//100311
            sql         = this.obtenerSQL(query);
            ps          = con.prepareStatement(sql);
            ps.setString(1, id);
            rs = ps.executeQuery();

            while( rs.next() ){
                seca = new SubclienteEca();

                seca.setId_cliente(         rs.getString("id_cliente"));
                seca.setMeses_mora(         rs.getString("meses_mora"));
                seca.setNombre_cliente(     rs.getString("nomcli"));
                seca.setId_solicitud(       rs.getString("id_solicitud"));
                seca.setPeriodo(            rs.getString("periodo"));
                seca.setPorc_cuota_inicial( rs.getString("porc_cuota_inicial"));
                seca.setTipo(rs.getString("tipo"));
                seca.setVal_cuota_inicial(  rs.getString("val_cuota_inicial"));
                seca.setPorc_base(          rs.getString("porc_base"));
                seca.setFecha_financiacion( rs.getString("fecha_financiacion"));
                seca.setNic(                rs.getString("nic"));
                secas.add(seca);
            }
        }
        catch(Exception e){
            throw new Exception(e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (!con.isClosed()){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }

        return secas;
    }




    public ArrayList<ClienteEca> getHijosList(String id_cli) throws Exception{
        Connection              con     = null;
        PreparedStatement       ps      = null;
        ResultSet               rs      = null;
        String                  query   = "SQL_GET_HIJOS";
        String                  sql     = "";
        ArrayList<ClienteEca>   cli_arr = new ArrayList<ClienteEca>();
        ClienteEca              cliente;

        try{
            con         = this.conectar(query);
            sql         = this.obtenerSQL(query);
            ps          = con.prepareStatement(sql);
            ps.setString(1, id_cli);
            ps.setString(2, id_cli);
            rs          = ps.executeQuery();
            while(rs.next()){
                cliente = new ClienteEca();
                cliente.setId_cliente(  rs.getString("id_subcliente"));
                cliente.setNombre(      rs.getString("nomcli"));
                cli_arr.add(cliente);
            }
            this.desconectar(query);
        }
        catch(Exception e){
            throw new Exception(e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();               } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();               } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(query);  } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }

        return cli_arr;
    }

    public ArrayList getNics(String id_cli) throws Exception{
        Connection              con     = null;
        PreparedStatement       ps      = null;
        ResultSet               rs      = null;
        String                  query   = "SQL_GET_CLIENT_NICS";
        String                  sql     = "";
        ArrayList                nics   = new ArrayList();
        int                     i       = 0;

        try{
            con         = this.conectar(query);
            sql         = this.obtenerSQL(query);
            ps          = con.prepareStatement(sql);
            ps.setString(1, id_cli);
            rs          = ps.executeQuery();

            while(rs.next()){
                nics.add(rs.getString("nic"));
                i++;
            }

            this.desconectar(query);
        }
        catch(Exception e){
            throw new Exception(e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();               } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();               } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(query);  } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }

        return nics;
    }
/**
     * metodo para obtener el mes de mora de una solicitud
     * @param id id de la solicitud
     * @author MGarizao - GEOTECH
     * @date 06/04/2010
     * @version 1.0
     * @return el mes de mora
     * @throws Exception
     */
    public String getMesesMora(String id) throws Exception{

        Connection              con   = null;
        PreparedStatement       ps    = null;
        ResultSet               rs    = null;
        String                  query = "SQL_GET_INFO_SOLICITUD";
        String                  sql   = "";
        String                  mes   = "";

        try{
            con         = this.conectar(query);
            sql         = this.obtenerSQL(query);
            ps          = con.prepareStatement(sql);
            ps.setString(1, id);
            rs          = ps.executeQuery();

            if(rs.next()){

               mes = rs.getString("meses_mora");

            }

        }
        catch(Exception e){
            throw new Exception(e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();               } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();               } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(query);  } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }

        return mes;

    }

public BeanGeneral getInfoAceptarPagos(String id_solicitud) throws Exception {
        BeanGeneral info = new BeanGeneral();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_INFO_ACEPTAR_PAGOS";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, id_solicitud);
            rs = ps.executeQuery();
            if (rs.next()) {
                info.setValor_01(rs.getString("id_cliente"));
                info.setValor_02(rs.getString("estado_sol"));
                info.setValor_03(rs.getString("plazo"));
                info.setValor_04(rs.getString("nic"));
                info.setValor_05(rs.getString("base"));
            }
        } catch (Exception e) {
            System.out.println("Error en getInfoAceptarPagos: " + e.toString());
            e.printStackTrace();
            throw new Exception("Error en getInfoAceptarPagos: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return info;
    }

    public String aceptarPagos(BeanGeneral info) throws Exception {
        String respuesta = "";
        StringStatement st = null;
        String query = query = "SQL_ACEPTAR_PAGOS";
        try {
            st = new StringStatement(this.obtenerSQL(query), true);//JJCastro fase2
            st.setString(1, info.getValor_01());
            st.setString(2, info.getValor_06());
            st.setDouble(3, 0);
            st.setDouble(4, 0);
            st.setDouble(5, Double.parseDouble(info.getValor_03()));
            st.setString(6, info.getValor_07());
            st.setString(7, info.getValor_07());
            st.setString(8, info.getValor_04());
            st.setDouble(9, Double.parseDouble(info.getValor_05()));
            st.setDouble(10,100);
            respuesta = st.getSql();

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e);
            throw e;
        } finally {
            if (st != null) {
                try {
                    st = null;
                } catch (Exception e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
        }
        return respuesta;
    }

}