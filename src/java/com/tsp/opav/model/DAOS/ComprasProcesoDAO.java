/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.opav.model.DAOS;

/**
 *
 * @author user
 */
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.tsp.opav.model.beans.AccionesEca;
import com.tsp.opav.model.beans.CotizacionSl;
import com.tsp.operation.model.MenuOpcionesModulos;
import com.tsp.operation.model.beans.BeanGeneral;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.beans.Cliente;
import com.tsp.opav.model.beans.OfertaElca;

import java.sql.SQLException;
import java.util.ArrayList;

public interface ComprasProcesoDAO {

    public String cargarSolicitudes(String Usuario, String IdSolicitud, String ano, String mes, String tiposolicitud, String EstadoSolicitud) throws SQLException;
    
    public String PreSaveInsumos(JsonObject obj, String Usuario, String IdSolicitud) throws SQLException;
    
    public String sp_WhatIdo(String Usuario, String IdSolicitud, String tiposolicitud, String tipo_bodega, String descripcion, String fecha_actual, String fecha_entrega, String direccion_entrega, String id_bodega) throws SQLException;
    
    public String EstadoSolicitud(String Usuario, String IdSolicitud) throws SQLException;
    
    public String SolicitudesDeTodos(String ano, String mes, String tiposolicitud) throws SQLException;
    
    public String VisualizarSolicitudes(String CodSolicitud) throws SQLException;    
    
    public String DeleteSolicitud(String Usuario, String IdSolicitud, String CodigoSolicitud) throws SQLException;
    
    public String PassSolicitudaCompras(String Usuario, String IdSolicitud, String CodigoSolicitud) throws SQLException;
    
    public String SolicitudToOrden(String consultaDB, String ano, String mes, String tiposolicitud) throws SQLException;
    
    public String AddElemToOrderOCS(String modo_compra, String Usuario, String CodigoSolicitud, String cod_insumo) throws SQLException;
    
    public String SaveOCS(String Usuario, String tipo_solicitud, String proveedor, String descripcion, String fecha_entrega, String direccion_entrega, String f_pago , String id_bodega) throws SQLException;
    
    public String SaveDetailsOCS(JsonObject obj, String oSC) throws SQLException;
    
    public String VisualizarProveedor() throws SQLException;
    
    public String LoadOrdenCS(String usuario, String consultaDB, String ano, String mes, String tiposolicitud) throws SQLException;    

    public String VisualizarOrdenCS(String CodOrden) throws SQLException;
    
    public String VerificarPreOCS(String Usuario) throws SQLException;

    public String imprimirOCS(String orden_compra,Usuario usuario);
    
    public String imprimirOCS_Provintegral(String orden_compra,Usuario usuario);
    
    public String SalvarCatalogoInsumos(String Usuario, String id_solicitud, String TipoInsumo, String CodigoInsumo, String DescripcionInsumo, String NombreUnidadInsumo, String Cantidad, String CdSol) throws SQLException;

    public String InfoPreSolicitud(String ReqSolicitud) throws SQLException;
    
    public String EliminarPreOCS(String uSer) throws SQLException;
    
    public String GuardarRechazarSolicitud(String uSer, String SolAprobar, String OpcionAccion, String DescripcionAprobacion) throws SQLException;
    
    public String ActualizarSolicitudes(String uSers, String oSC) throws SQLException;
    
    public String DevolverSolicitud(String CodSolicitud) throws SQLException;
    
    public String EliminarOCS(String codOCS, String uSer) throws SQLException;
    
    public String InventarioTodos(String usuario, String ano, String mes, String tipomovimiento) throws SQLException;    
    
    public String InventarioDetalle(String cod_movimiento) throws SQLException;    
    
    public JsonObject getInfoCuentaEnvioCorreo();
    
    public String getInfoCuentaEnvioCorreoDestino(String CodSolicitud);
    
    public String SaveDespacho(String Usuario, String tipo_solicitud, String proveedor, String descripcion, String fecha_entrega, String direccion_entrega, String OrdenCompDespacho) throws SQLException;    
    
    public String SaveDetailsDispatch(JsonObject obj, String dSPTCH) throws SQLException;
    
    public String InventarioDespacho(String dSPTCH) throws SQLException;        
    
    public String DespachosTodos(String ano, String mes) throws SQLException;
    
    public String DespachosDetalles(String cod_movimiento) throws SQLException;        

    public String VisualizarProyectos(String usuario) throws SQLException;  

    public String VisualizarItemsBodega(String id_bodega, String tipo_movimiento, String usuario, String id_solicitud);

    public String CargarListaInsumos();

    public String GuardarMovimiento(Usuario usuario, String fecha_transaccion, String responsable, String descripcion, String id_solicitud_proyecto, String id_bodega, String detalle, String tipo_movimiento, String id_solicitud_destino, String id_bodega_destino);

    public String imprimirPDFMovimiento(String cod_movimiento, Usuario usuario);

    public String cargar_Combo(String usuario, String op);

    public String CargarProyectosTraslado(String login);

    public String CargarHistoricoMaterialEnBodega(String id_bodega, String cod_material, String id_solicitud);

    public String VerificarOCS(String ocs);
    
    public String cargarSolicitudesEjecucion(String usuario);
    
    public String cargarDetalleSolicitudEjecucion(String id_solicitud_ejecucion);

    public String guardarDetalleSolicitudEjecucion(String parameter, String parameter0, String parameter1, String parameter2, String parameter3, String parameter4, String parameter5);

    public String cargarSolicitudesEjecucionParaDespacho(String login);

    public String convertirSolicitudEnMovimiento(String usuario, String parameter);

    public String rechazarSolicitud(String login, String parameter);
    
    public String borrarSolicitud(String login, String parameter);

    public String obternerCotizacionTerc(String id_solitud);
        
}
