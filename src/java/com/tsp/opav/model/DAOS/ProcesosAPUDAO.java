/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.opav.model.DAOS;

import com.google.gson.JsonObject;
import com.tsp.opav.model.beans.Categoria;
import com.tsp.operation.model.beans.Usuario;
import java.util.ArrayList;

/**
 *
 * @author user
 */
public interface ProcesosAPUDAO {
    
    public JsonObject cargar_combo_grupo(JsonObject info);
    
    public JsonObject cargar_combo(JsonObject info);

    public boolean existeGrupoApu(String empresa, String nombre);

    public String guardarGrupoApu(String empresa, String nombre, String descripcion, String login);

    public String cargarInsumosXTipo(String id);

    public String cargarTiposMaterial();

    public JsonObject cargarInsumosXFiltro(String idsubcategoria);

    public JsonObject guardarAPU(JsonObject info);

    public JsonObject cargarAPUXGrupo(String grupo_apu);

    public JsonObject cargarGridInsumosEdit(String ids);

    public JsonObject guardarAPUEdit(JsonObject info);

    public JsonObject cargarGridMateriales(String ids, String tipo);

    public String guardarUnidadMedida(String empresa, String nombre, String login);

    public boolean existeUnidadMedida(String empresa, String nombre);

    public JsonObject cargarGridSolicitudes(String nit_proveedor);

    public JsonObject cargarSubGridSolicitudes(String nit_proveedor, String id);

    public JsonObject cargarGridApuSolicitudes(String id);
    
    public JsonObject cargarSubGridAPUSolicitudes(String id_accion);

    public JsonObject clonar_apu(String id_apu, Usuario usuario);

    public JsonObject clonar_apu2(String id_apu, String id_actividad_capitulo, Usuario usuario);
    
}
