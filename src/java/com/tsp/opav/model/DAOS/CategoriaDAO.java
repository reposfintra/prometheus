/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.opav.model.DAOS;
import java.sql.*;
import java.util.*;
import com.tsp.opav.model.beans.*;
import com.tsp.operation.model.beans.StringStatement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.text.DateFormat;

/**
 *
 * @author jose
 */
public class CategoriaDAO extends MainDAO{

    public CategoriaDAO() {
        super("CategoriaDAO.xml");
    }
    public CategoriaDAO(String dataBaseName) {
        super("CategoriaDAO.xml", dataBaseName);
    }


     /**
     *
     * @param id_accion
     * @return
     * @throws NullPointerException
     * @throws Exception
     * Created by Ing. Jose Castro
     */
         public ArrayList listCategorias() throws NullPointerException,Exception {
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query="SQL_GET_CATEGORIAS";
        ArrayList resultado = new ArrayList();
        Cotizacion cot = new Cotizacion();
        try{
            con = this.conectarJNDI(query);
            if(con!=null){
            st = con.prepareStatement(this.obtenerSQL(query));
            rs=st.executeQuery();
            while(rs.next()){
                resultado.add(new String[]{rs.getString("idcategoria"), rs.getString("descripcion")});

            }

        }}
        catch(NullPointerException ec){
            throw new NullPointerException("No hay resultado ..." + ec.toString());
        }
        catch(Exception ec){
            throw new Exception("Error en la consulta de Categorias: "+ec.toString());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return resultado;
    }



     /**
     *
     * @param id_accion
     * @return
     * @throws NullPointerException
     * @throws Exception
     * Created by Ing. Jose Castro
     */
         public ArrayList listCategoria(String categoria) throws NullPointerException,Exception {
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query="SQL_GET_CATEGORIA";
        ArrayList resultado = new ArrayList();
        Cotizacion cot = new Cotizacion();
        try{
            con = this.conectarJNDI(query);
            if(con!=null){
            st = con.prepareStatement(this.obtenerSQL(query));
            st.setString(1, categoria);
            rs=st.executeQuery();
            while(rs.next()){
                resultado.add(new String[]{rs.getString("idcategoria"), rs.getString("descripcion"), rs.getString("reg_status") });

            }

        }}
        catch(NullPointerException ec){
            throw new NullPointerException("No hay resultado ..." + ec.toString());
        }
        catch(Exception ec){
            throw new Exception("Error en la consulta de Categorias: "+ec.toString());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return resultado;
    }



      /**
     *
     * @param id_accion
     * @return
     * @throws NullPointerException
     * @throws Exception
     * Created by Ing. Jose Castro
     */
         public ArrayList listSubcategorias(String categoria) throws NullPointerException,Exception {
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query="SQL_GET_SUBCATEGORIAS";
        ArrayList resultado = new ArrayList();
        Cotizacion cot = new Cotizacion();
        try{
            con = this.conectarJNDI(query);
            if(con!=null){
            st = con.prepareStatement(this.obtenerSQL(query));
            st.setString(1, categoria);
            rs=st.executeQuery();
            while(rs.next()){
                resultado.add(new String[]{rs.getString("idsubcategoria"), rs.getString("descripcion"), rs.getString("reg_status")});

            }

        }}
        catch(NullPointerException ec){
            throw new NullPointerException("No hay resultado ..." + ec.toString());
        }
        catch(Exception ec){
            throw new Exception("Error en la consulta de Categorias: "+ec.toString());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return resultado;
    }


     /*
     * @param id_accion
     * @return
     * @throws NullPointerException
     * @throws Exception
     * Created by Ing. Jose Castro
     */
         public ArrayList listtipoSubcategorias(String subcategoria) throws NullPointerException,Exception {
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query="SQL_GET_TIPOSUBCATEGORIAS";
        ArrayList resultado = new ArrayList();
        Cotizacion cot = new Cotizacion();
        try{
            con = this.conectarJNDI(query);
            if(con!=null){
            st = con.prepareStatement(this.obtenerSQL(query));
            st.setString(1, subcategoria);
            rs=st.executeQuery();
            while(rs.next()){
                resultado.add(new String[]{rs.getString("idtiposubcategoria"), rs.getString("descripcion"), rs.getString("reg_status")});

            }

        }}
        catch(NullPointerException ec){
            throw new NullPointerException("No hay resultado ..." + ec.toString());
        }
        catch(Exception ec){
            throw new Exception("Error en la consulta de Categorias: "+ec.toString());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return resultado;
    }



    public int  insertarCategoria(String descripcion, String reg_status, String usuario) throws Exception {
        PreparedStatement st = null;
        Connection con = null;
        String query="INSERT_CATEGORIA";
        int rowCount=0;
        int idcat = this.getIdCategoria();
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
           st = con.prepareStatement(this.obtenerSQL(query));
           st.setInt(1,idcat);
           st.setString(2,reg_status);
           st.setString(3,descripcion);
           st.setString(4, usuario);
            rowCount=st.executeUpdate();
            if(rowCount>0) System.out.println("Categoria agregada...");
            else System.out.println("no se inserto la fila");
            } }
        catch(Exception ec){
            throw new Exception("Error al insertar la Categoria: "+ec.toString());
        }finally{
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return idcat;
    }



        public int getIdCategoria() throws Exception{
        Connection          con     = null;
        PreparedStatement   ps      = null;
        ResultSet           rs      = null;
            String              query   = "SQL_GET_IDCATEGORIA";
        String              sql     = "";
        int              id = 0    ;

        try{
            con         = this.conectarJNDI(query);
            sql         = this.obtenerSQL(query);
            ps          = con.prepareStatement(sql);
            rs          = ps.executeQuery();
            if(rs.next()){
                id = rs.getInt("idcategoria");
            }
        }
        catch(Exception e){
            throw new Exception(e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con);   } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }

        return id;
    }


    public int  insertarSubCategoria(int idcategoria, String descripcion, String reg_status, String usuario) throws Exception {
        PreparedStatement st = null;
        Connection con = null;
        String query="INSERT_SUBCATEGORIA";
        int rowCount=0;
        int idsubcat = this.getIdSubCategoria();
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
           st = con.prepareStatement(this.obtenerSQL(query));
           st.setInt(1,idcategoria);
           st.setInt(2,idsubcat);
           st.setString(3,reg_status);
           st.setString(4,descripcion);
           st.setString(5, usuario);
            rowCount=st.executeUpdate();
            if(rowCount>0) System.out.println("SubCategoria agregado...");
            else System.out.println("no se inserto la fila sub");
            } }
        catch(Exception ec){
            throw new Exception("Error al insertar la SubCategoria: "+ec.toString());
        }finally{
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return idsubcat;
    }



        public int getIdSubCategoria() throws Exception{
        Connection          con     = null;
        PreparedStatement   ps      = null;
        ResultSet           rs      = null;
            String              query   = "SQL_GET_SUBIDCATEGORIA";
        String              sql     = "";
        int              id = 0    ;

        try{
            con         = this.conectarJNDI(query);
            sql         = this.obtenerSQL(query);
            ps          = con.prepareStatement(sql);
            rs          = ps.executeQuery();
            if(rs.next()){
                id = rs.getInt("idsubcategoria");
            }
        }
        catch(Exception e){
            throw new Exception(e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con);   } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }

        return id;
    }





      public String insertartipoSubCategoria(int idcategoria, int idsubcategoria, String descripcion, String reg_status, String usuario) throws Exception {
        String query = "INSERT_TIPOSUBCATEGORIA";
        String respuesta = "";
        StringStatement st = null;

        try {
            st = new StringStatement (this.obtenerSQL(query), true);//JJCastro fase2
            st.setInt(1, idcategoria);
            st.setInt(2, idsubcategoria);
            st.setString(3, reg_status);
            st.setString(4, descripcion);
            st.setString(5, usuario);
            respuesta = st.getSql();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
            throw new SQLException("ERROR DURANTE SQL_INSERTAR_TIPOSUBCATEGORIA. \n " + e.getMessage());
        }finally{
            if (st  != null){ try{ st = null;              } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }

        return respuesta;
    }


    public void updateCategoria(String descripcion, String reg_status, String usuario, int idcat) throws Exception {
        PreparedStatement st = null;
        Connection con = null;
        String query="UPDATE_CATEGORIA";
        int rowCount=0;
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
           st = con.prepareStatement(this.obtenerSQL(query));
           st.setString(1,reg_status);
           st.setString(2,descripcion);
           st.setString(3, usuario);
           st.setInt(4,idcat);
            rowCount=st.executeUpdate();
            if(rowCount>0) System.out.println("Categoria actualizada...");
            else System.out.println("no se inserto la fila");
            } }
        catch(Exception ec){
            throw new Exception("Error al actualizar la Categoria: "+ec.toString());
        }finally{
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }

    }




    public void updateSubCategoria(String descripcion, String reg_status, String usuario, int idcat, int idsubcat) throws Exception {
        PreparedStatement st = null;
        Connection con = null;
        String query="UPDATE_SUBCATEGORIA";
        int rowCount=0;
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
           st = con.prepareStatement(this.obtenerSQL(query));
           st.setString(1,reg_status);
           st.setString(2,descripcion);
           st.setString(3, usuario);
           st.setInt(4,idcat);
           st.setInt(5,idsubcat);
            rowCount=st.executeUpdate();
            if(rowCount>0) System.out.println("SubCategoria actualizada...");
            else System.out.println("no se inserto la fila");
            } }
        catch(Exception ec){
            throw new Exception("Error al actualizar la SubCategoria: "+ec.toString());
        }finally{
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }

    }



      public String updatetipoSubCategoria(String descripcion, String reg_status, String usuario, int idcategoria, int idsubcategoria, int idtiposub) throws Exception {
        String query = "UPDATE_TIPOSUBCATEGORIA";
        String respuesta = "";
        StringStatement st = null;

        try {
            st = new StringStatement (this.obtenerSQL(query), true);//JJCastro fase2
            st.setString(1, reg_status);
            st.setString(2, descripcion);
            st.setString(3, usuario);
            st.setInt(4, idcategoria);
            st.setInt(5, idsubcategoria);
            st.setInt(6, idtiposub);
            respuesta = st.getSql();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
            throw new SQLException("ERROR DURANTE UPDATE_TIPOSUBCATEGORIA. \n " + e.getMessage());
        }finally{
            if (st  != null){ try{ st = null;              } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }

        return respuesta;
    }




  private TreeMap listadoCategorias;
  private TreeMap listadoSubCategorias;
  private TreeMap listadoTipoSubCategorias;

  public void listadoCategorias()throws SQLException{

        Connection con=null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_LISTAR_CATEGORIAS";
        try {
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            if(con!=null){
                rs = ps.executeQuery();
                listadoCategorias = new TreeMap();
                listadoCategorias.put("Seleccione una Categoria", "0");
                while(rs.next()){
                    listadoCategorias.put(rs.getString("descripcion"), rs.getString("idcategoria"));
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS DETINOS DE UN ORIGEN" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con);   } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }


  public void listadoSubCategorias(int idcategoria)throws SQLException{

        Connection con=null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_LISTAR_SUBCATEGORIAS";
        try {
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setInt(1, idcategoria);
            if(con!=null){
                rs = ps.executeQuery();
                listadoSubCategorias = new TreeMap();
                listadoSubCategorias.put("Seleccione una SubCategoria", "0");
                while(rs.next()){
                    listadoSubCategorias.put(rs.getString("descripcion"), rs.getString("idsubcategoria"));
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS DETINOS DE UN ORIGEN" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con);   } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }



  public void listadoTipoSubCategorias(int idcategoria, int idsubcategoria)throws SQLException{

        Connection con=null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_LISTAR_TIPOSUBCATEGORIAS";
        try {
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setInt(1, idcategoria);
            ps.setInt(2, idsubcategoria);
            if(con!=null){
                rs = ps.executeQuery();
                listadoTipoSubCategorias = new TreeMap();
                listadoTipoSubCategorias.put("Seleccione una TipoSubCategoria", "0");
                while(rs.next()){
                    listadoTipoSubCategorias.put(rs.getString("descripcion"), rs.getString("idtiposubcategoria"));
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LAS TIPOSUBCATEGORIAS" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con);   } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }



    public TreeMap getListadoCategorias(){

        return listadoCategorias;
    }



    public void setListadoCategorias(TreeMap t){

        listadoCategorias=t;
    }


    public TreeMap getListadoSubCategorias(){

        return listadoSubCategorias;
    }



    public void setListadoSubCategorias(TreeMap t){

        listadoSubCategorias=t;
    }



    public TreeMap getListadoTipoSubCategorias(){

        return listadoTipoSubCategorias;
    }



    public void setListadoTipoSubCategorias(TreeMap t){

        listadoTipoSubCategorias=t;
    }





    /**
     *
     * @return
     * @throws NullPointerException
     * @throws Exception
     * Created by: Jose Castro
     */
    public ArrayList categoriasExcel() throws NullPointerException,Exception{
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query="SQL_LISTAR_EXCEL_CATEGORIAS";
        ArrayList resultado = new ArrayList();
        Material m = new Material();
        try{
            con = this.conectarJNDI(query);
            if(con!=null){
            String sql = obtenerSQL(query);

            st = con.prepareStatement(sql);
            rs=st.executeQuery();
            while(rs.next()){
                m = new Material();
                m.setIdcategoria(rs.getInt("idcategoria"));
                m.setDesc_categoria(reset(rs.getString("descripcion_categoria")));
                m.setIdsubcategoria(rs.getInt("idsubcategoria"));
                m.setDesc_subcat(reset(rs.getString("descripcion_subcategoria")));
                m.setIdtiposubcategoria(rs.getInt("idtiposubcategoria"));
                m.setDesc_tiposub(reset(rs.getString("descripcion_tiposubcategoria")));
                resultado.add(m);
            }

        }}
        catch(NullPointerException ec){
            throw new NullPointerException("No hay resultado ..." + ec.toString());
        }
        catch(Exception ec){
            throw new Exception("Error en la consulta buscar por: "+ec.toString());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return resultado;
    }


    private String reset(String val){
            if(val==null)
               val = "";
            return val;
    }





    ///////////////////Cotizacion nueva


  private TreeMap listadoCategoriasTipo;


  public void listadoCategoriasTipo(String tipo)throws SQLException{

        Connection con=null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_LISTAR_CATEGORIAS_TIPO";
        try {
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, tipo);
            if(con!=null){
                rs = ps.executeQuery();
                listadoCategoriasTipo = new TreeMap();
                listadoCategoriasTipo.put("Seleccione una Categoria", "0");
                while(rs.next()){
                    listadoCategoriasTipo.put(rs.getString("descripcion"), rs.getString("idcategoria"));
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS DETINOS DE UN ORIGEN" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con);   } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }

    public TreeMap getListadoCategoriasTipo(){
        return listadoCategoriasTipo;
    }

    public void setListadoCategoriasTipo(TreeMap t){
        listadoCategoriasTipo=t;
    }


/////////////////

  private TreeMap listadoMateriales;


  public void listadoMateriales(String tipo, int categoria, int subcat, int tipsubcat)throws SQLException{

        Connection con=null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_LISTAR_MATERIALES_TIPO";
        String param0 = "";
        String param1 = "";
        String param2 = "";
        String param3 = "";

        try {
            con = this.conectarJNDI(query);


            if(categoria!=0)    param1= " AND IDCATEGORIA     = "+categoria;
            if(subcat!=0)       param1= " AND IDSUBCATEGORIA     = "+subcat;
            if(tipsubcat!=0)    param2= " AND IDTIPOSUBCATEGORIA     = "+tipsubcat;
            //if(!tipo.equals("D"))       param3= " AND ID_SL_INSUMO != 0";



            ps = con.prepareStatement(this.obtenerSQL(query).replaceAll("#IDCATEGORIA#", param0).replaceAll("#IDSUBCATEGORIA#", param1).replaceAll("#IDTIPOSUBCATEGORIA#", param2).replaceAll("#NUEVO_CATALOGO#", param3));
            ps.setString(1, tipo);

            if(con!=null){
                rs = ps.executeQuery();
                listadoMateriales = new TreeMap();
                listadoMateriales.put("Seleccione un Material", "0");
                while(rs.next()){
                    listadoMateriales.put(rs.getString("descripcion"), rs.getString("COD_MATERIAL"));
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA de" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con);   } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }

    public TreeMap getListadoMateriales(){
        return listadoMateriales;
    }

    public void setListadoMateriales(TreeMap t){
        listadoMateriales=t;
    }



    /**
     *
     * @return
     * @throws NullPointerException
     * @throws Exception
     * Created by: Jose Castro
     */
    public ArrayList listadoSubTiposub(int idcategoria) throws NullPointerException,Exception{
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query="SQL_LISTAR_SUB_TIPOSUB";
        ArrayList resultado = new ArrayList();
        Material m = new Material();
        try{
            con = this.conectarJNDI(query);
            if(con!=null){
            String sql = obtenerSQL(query);

            st = con.prepareStatement(sql);
            st.setInt(1, idcategoria);
            rs=st.executeQuery();
            while(rs.next()){
                m = new Material();
                m.setIdcategoria(rs.getInt("idcategoria"));
                m.setDesc_categoria(reset(rs.getString("descripcion_categoria")));
                m.setIdsubcategoria(rs.getInt("idsubcategoria"));
                m.setDesc_subcat(reset(rs.getString("descripcion_subcategoria")));
                m.setIdtiposubcategoria(rs.getInt("idtiposubcategoria"));
                m.setDesc_tiposub(reset(rs.getString("descripcion_tiposubcategoria")));
                resultado.add(m);
            }

        }}
        catch(NullPointerException ec){
            throw new NullPointerException("No hay resultado ..." + ec.toString());
        }
        catch(Exception ec){
            throw new Exception("Error en la consulta buscar por: "+ec.toString());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return resultado;
    }









//        public int insertarCategoria153(String descripcion, String reg_status, String usuario) throws Exception {
//
//        PreparedStatement st = null;
//        Connection con = null;
//        String query="SQL_SEC_CATEGORIA";
//        String query1="INSERT_CATEGORIA";
//
//
//
//
//        PreparedStatement   ps      = null;
//        ResultSet           rs      = null;
//
//        String              sql     = "";
//        String              id      = "";
//
//        try{
//            con         = this.conectarJNDI(query);
//            sql         = this.obtenerSQL(query);
//            ps          = con.prepareStatement(sql);
//            ps.setString(1, idaccion);
//            rs          = ps.executeQuery();
//
//            if(rs.next()){
//                id = rs.getString("id_solicitud");
//            }
//        }
//        catch(Exception e){
//            throw new Exception(e.getMessage());
//        }
//        finally{
//            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
//            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
//            if (con != null){ try{ this.desconectar(con);   } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
//        }
//
//        return id;
//    }





}