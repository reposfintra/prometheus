/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * ActividadAccionDAO.java : Conecta con la base de datos para manejra lo concerniente a las actividades
 * Copyright 2010 Rhonalf Martinez Villa <rhonaldomaster@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.tsp.opav.model.DAOS;
import java.sql.*;
import java.util.*;
import com.tsp.opav.model.beans.*;
/**
 *
 * @author rhonalf
 */
public class ActividadAccionDAO extends MainDAO {

    public ActividadAccionDAO(){
        super("ActividadAccionDAO.xml");
    }
    public ActividadAccionDAO(String dataBaseName){
        super("ActividadAccionDAO.xml", dataBaseName);
    }

    /**
     * Busca datos de actividades de accion
     * @param filtro nombre de la columna de la tabla actividades por la cual se quiere filtrar
     * @param param el dato a buscar
     * @return lista con objetos ActividadAccion que se ajustaron al criterio de busqueda
     * @throws Exception cuando hay un error
     */
    public ArrayList buscar(String filtro,String param) throws Exception{
        ArrayList lista = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        Connection con = null;
        String query = "BUSCAR_PARAM";
        String sql="";
        ActividadAccion cont = null;
        try {
            lista = new ArrayList();
            con = this.conectarJNDI(query);
            sql = this.obtenerSQL(query);
            sql = sql.replaceAll("#param", param);
            sql = sql.replaceAll("#filtro", filtro);
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while(rs.next()){
                cont = new ActividadAccion();
                cont.setDescripcion(rs.getString("descripcion"));
                cont.setId(rs.getInt("id"));
                cont.setOrden_predeterminado(rs.getInt("orden_predeterminado"));
                cont.setPeso_predeterminado(rs.getInt("peso_predeterminado"));
                cont.setTipo(rs.getString("tipo"));

                cont.setResponsable_predeterminado(rs.getString("responsable_predeterminado"));

                lista.add(cont);
            }
            rs.close();
            ps.close();
            this.desconectar(con);
        }
        catch (Exception e) {
            throw new Exception("Error en ActividadAccionDAO.java en la funcion buscar: "+e.toString());
        }
        finally{
            if (rs != null){ try{ rs.close(); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps != null){ try{ ps.close(); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (!con.isClosed()){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;
    }

    /**
     * Inserta un nuevo registro en la tabla de actividades
     * @param actividad Objeto de tipo ActividadAccion con los datos
     * @param user Usuario que actualiza el registro
     * @throws Exception Cuando hay un error
     */
    public void insertar(ActividadAccion actividad, String user) throws Exception{
        PreparedStatement ps = null;
        Connection con = null;
        String query = "INSERT_REG";
        String sql="";
        try {
            con = this.conectarJNDI(query);
            sql = this.obtenerSQL(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, actividad.getTipo());
            ps.setString(2, actividad.getDescripcion());
            ps.setInt(3, actividad.getPeso_predeterminado());
            ps.setString(4, user);
            ps.setInt(5, actividad.getOrden_predeterminado());
            ps.setString(6, actividad.getResponsable_predeterminado());
            int krow = ps.executeUpdate();
            if(krow>0){
                System.out.println("Se ha ingresado el registro en la tabla actividades");
            }
            else{
                System.out.println("No se ha ingresado el registro en la tabla actividades");
            }
            ps.close();
            this.desconectar(con);
        }
        catch (Exception e) {
            throw new Exception("Error en ActividadAccionDAO.java en la funcion insertar: "+e.toString());
        }
        finally {
            if (ps != null){ try{ ps.close(); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (!con.isClosed()){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }

    /**
     * Actualiza un registro de la tabla de actividades
     * @param actividad Objeto de tipo ActividadAccion con los datos
     * @param user Usuario que actualiza el registro
     * @throws Exception Cuando hay un error
     */
    public void actualizar(ActividadAccion actividad, String user) throws Exception{
        PreparedStatement ps = null;
        Connection con = null;
        String query = "UPDATE_REG";
        String sql="";
        try {
            con = this.conectarJNDI(query);
            sql = this.obtenerSQL(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, actividad.getTipo());
            ps.setString(2, actividad.getDescripcion());
            ps.setInt(3, actividad.getPeso_predeterminado());
            ps.setString(4, user);
            ps.setInt(5, actividad.getOrden_predeterminado());
            ps.setString(6, actividad.getResponsable_predeterminado());
            ps.setInt(7, actividad.getId());
            int krow = ps.executeUpdate();
            if(krow>0){
                System.out.println("Se ha actualizado el registro en la tabla actividades");
            }
            else{
                System.out.println("No se ha actualizado el registro en la tabla actividades");
            }
        }
        catch (Exception e) {
            throw new Exception("Error en ActividadAccionDAO.java en la funcion actualizar: "+e.toString());
        }
        finally {
            if (ps != null){ try{ ps.close(); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (!con.isClosed()){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }

    /**
     * Anula un registro de la tabla de actividades
     * @param codigo El codigo del registro
     * @param user Usuario que actualiza el registro
     * @throws Exception Cuando hay un error
     */
    public void anular(String codigo, String user) throws Exception{
        PreparedStatement ps = null;
        Connection con = null;
        String query = "ANUL_REG";
        String sql="";
        try {
            con = this.conectarJNDI(query);
            sql = this.obtenerSQL(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, user);
            ps.setString(2, codigo);
            int krow = ps.executeUpdate();
            if(krow>0){
                System.out.println("Se ha anulado el registro en la tabla actividades");
            }
            else{
                System.out.println("No se ha anulado el registro en la tabla actividades");
            }
            ps.close();
            this.desconectar(con);
        }
        catch (Exception e) {
            throw new Exception("Error en ActividadAccionDAO.java en la funcion anular: "+e.toString());
        }
        finally {
            if (ps != null){ try{ ps.close(); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (!con.isClosed()){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }

    /**
     * Busca la lista de responsables predeterminados en tablagen
     * @return Listado con los datos encontrados
     * @since 2010-08-12
     * @throws Exception cuando hay error
     */
    public ArrayList<String> datosResponsables() throws Exception{
        ArrayList<String> lista = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        Connection con = null;
        String query = "DATOS_RESP";
        String sql="";
        try {
            lista = new ArrayList<String>();
            con = this.conectarJNDI(query);
            sql = this.obtenerSQL(query);
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while(rs.next()){
                lista.add(rs.getString("table_code")+";_;"+rs.getString("referencia"));
            }
        }
        catch (Exception e) {
            throw new Exception("Error en ActividadAccionDAO.java en la funcion datosResponsables: "+e.toString());
        }
        finally {
            if (ps != null){ try{ ps.close(); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (!con.isClosed()){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;
    }

    /**
     * Busca el nombre del tipo de responsable predeterminados en tablagen
     * @param tipodat table_code en tablagen
     * @return Cadena con el dato encontrado
     * @throws Exception Cuando hay error
     */
    public String nomDat(String tipodat) throws Exception{
        String nom = "";
        PreparedStatement ps = null;
        ResultSet rs = null;
        Connection con = null;
        String query = "NOM_RESP";
        String sql="";
        try {
            con = this.conectarJNDI(query);
            sql = this.obtenerSQL(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, tipodat);
            rs = ps.executeQuery();
            if(rs.next()){
                nom = rs.getString("referencia");
            }
        }
        catch (Exception e) {
            throw new Exception("Error en ActividadAccionDAO.java en la funcion nomDat: "+e.toString());
        }
        finally {
            if (ps != null){ try{ ps.close(); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (!con.isClosed()){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return nom;
    }

}