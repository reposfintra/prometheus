package com.tsp.opav.model.DAOS;

import com.tsp.opav.model.DAOS.MainDAO;
import com.tsp.opav.model.beans.*;
import com.tsp.opav.model.beans.Cotizacion;
import com.tsp.operation.model.TransaccionService;
import com.tsp.operation.model.beans.StringStatement;
import com.tsp.operation.model.beans.TablaGen;
import com.tsp.operation.model.beans.TablaItem;
import java.sql.*;
import java.text.*;
import java.util.*;
import java.util.Date;

public class ElectricaribeOfertaDAO extends MainDAO {

    private DatosOferta oe = new DatosOferta();
    private String numOferta;
    private ArrayList<TablaItem> tisM;
    private ArrayList<TablaItem> tisD;
    private ArrayList<TablaItem> tisO;

    public ElectricaribeOfertaDAO(){
        super("ElectricaribeOfertaDAO.xml");
    }
    public ElectricaribeOfertaDAO(String dataBaseName){
        super("ElectricaribeOfertaDAO.xml", dataBaseName);
    }
    public void getEcaInfo() throws Exception{
        Connection          con      = null;
        PreparedStatement   ps       = null;
        ResultSet           rs       = null;
        String              query    = "SQL_OFERTA_INFO";
        String              sql      = "";

        try{
            oe          = null;
            oe          = new DatosOferta();

            con         = this.conectarJNDI(query);
            sql         = this.obtenerSQL(query);
            ps          = con.prepareStatement(sql);
            ps.setString(1, this.getNumOferta());
            rs          = ps.executeQuery();

            if(rs.next()){
                oe.setId_solicitud(this.getNumOferta());
                oe.setOferta(rs.getString("nombre_solicitud"));
                oe.setElaboradoPor(rs.getString("nombre_elaborado"));
                oe.setFechaGeneracion(rs.getString("fecha_generada"));
                oe.setAprobadoPor(rs.getString("nombre_aprobado"));
                oe.setEjecutivo(rs.getString("nombre_ejecutivo"));
                oe.setCliente(rs.getString("nombre_cliente"));
                oe.setNIC(rs.getString("nic"));
                oe.setConsecutivo(rs.getString("consecutivo_oferta"));
                oe.setCiudad(rs.getString("ciudad"));
                oe.setDepartamento(rs.getString("departamento"));
                oe.setDireccion(rs.getString("direccion"));
                oe.setRepresentante(rs.getString("nombre_representante"));
                oe.setNIT(rs.getString("nit"));
                oe.setOtras_consideraciones(rs.getString("otras_consideraciones"));
                oe.setValorAgregado(rs.getString("noesvaloragregado"));
                oe.setTipo_cliente(rs.getString("tipo"));
                oe.setOficial(rs.getString("esoficial"));
                oe.setAviso(rs.getString("aviso"));
                oe.setTipo_solicitud(rs.getString("tipo_solicitud"));
            }
        }
        catch(Exception e){
            throw new Exception(e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    
    /* Metodo para saber si el cliente al que se le genera
     * la oferta es un edificio o no. 
     */
    public String obtenerTipoCliente(String idOferta)throws Exception{
       
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_TIPO_CLIENTE_OFERTA";
        String sql = "";
        String resultado="";

        try {
           
            con = this.conectarJNDI(query);
            sql = this.obtenerSQL(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, idOferta);
            rs = ps.executeQuery();

            if (rs.next()) {
               resultado=rs.getString("edificio");
            }
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return resultado;

    }

    public void getEcaInfoForPDF() throws Exception{
        Connection          con      = null;
        PreparedStatement   ps       = null;
        ResultSet           rs       = null;
        String              query    = "SQL_OFERTA_INFO_PDF";
        String              sql      = "";

        try{
            oe          = null;
            oe          = new DatosOferta();

            con         = this.conectarJNDI(query);
            sql         = this.obtenerSQL(query);
            ps          = con.prepareStatement(sql);
            ps.setString(1, this.getNumOferta());
            rs          = ps.executeQuery();

            if(rs.next()){
                oe.setId_solicitud(this.getNumOferta());
                oe.setOferta(rs.getString("nombre_solicitud"));
                oe.setElaboradoPor(rs.getString("nombre_elaborado"));
                oe.setFechaGeneracion(rs.getString("fecha_generada")); //generar oferta cambio
                oe.setAprobadoPor(rs.getString("nombre_aprobado"));
                oe.setEjecutivo(rs.getString("nombre_ejecutivo"));
                oe.setCliente(rs.getString("nombre_cliente"));
                oe.setNIC(rs.getString("nic"));
                oe.setConsecutivo(rs.getString("consecutivo_oferta"));
                oe.setCiudad(rs.getString("ciudad"));
                oe.setDepartamento(rs.getString("departamento"));
                oe.setDireccion(rs.getString("direccion"));
                oe.setRepresentante(rs.getString("nombre_representante"));
                oe.setNIT(rs.getString("nit"));
                oe.setOtras_consideraciones(rs.getString("otras_consideraciones"));
                oe.setValorAgregado(rs.getString("noesvaloragregado"));
                oe.setTipo_cliente(rs.getString("tipo"));
                oe.setOficial(rs.getString("esoficial"));
                oe.setAviso(rs.getString("aviso"));
                oe.setTipo_solicitud(rs.getString("tipo_solicitud"));
            }
        }
        catch(Exception e){
            throw new Exception(e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }

    public ArrayList<AccionesEca> getAcciones() throws Exception{
        Connection              con     = null;
        PreparedStatement       ps      = null;
        ResultSet               rs      = null;
        String                  query   = "SQL_ACCIONES";
        String                  sql     = "";
        ArrayList<AccionesEca>  aecas   = new ArrayList<AccionesEca>();
        AccionesEca             aeca;

        try{
            con         = this.conectarJNDI(query);
            sql         = this.obtenerSQL(query);
            ps          = con.prepareStatement(sql);
            ps.setString(1, this.getNumOferta());
            rs          = ps.executeQuery();

            while(rs.next()){
                aeca = new AccionesEca();
                aeca.setId_accion(rs.getString("id_accion"));
                aeca.setDescripcion(rs.getString("descripcion"));
                aeca.setAlcance(rs.getString("alcances"));
                aeca.setAdministracion(rs.getString("administracion"));
                aeca.setImprevisto(rs.getString("imprevisto"));
                aeca.setUtilidad(rs.getString("utilidad"));
                aeca.setPorc_administracion(rs.getString("porc_administracion"));
                aeca.setPorc_imprevisto(rs.getString("porc_imprevisto"));
                aeca.setPorc_utilidad(rs.getString("porc_utilidad"));
                aecas.add(aeca);
            }
        }
        catch(Exception e){
            throw new Exception(e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return aecas;
    }

    public ArrayList<MaterialEca> getMaterialesPorTipo(String accion, String tipo) throws Exception{
        Connection              con     = null;
        PreparedStatement       ps      = null;
        ResultSet               rs      = null;
        String                  query   = "SQL_TODOS_POR_TIPO";
        String                  sql     = "";
        ArrayList<MaterialEca>  mecas   = new ArrayList<MaterialEca>();
        MaterialEca             meca;

        try{
            DateFormat dateFormat = new SimpleDateFormat("yyyyMM");
            Date dat = new Date();

            con         = this.conectarJNDI(query);
            sql         = this.obtenerSQL(query);
            ps          = con.prepareStatement(sql);
            ps.setString(1, this.getNumOferta());
            ps.setString(2, accion);
            ps.setString(3, tipo);

            System.out.print("\n\n"+ ps.toString() +"\n\n");

            rs          = ps.executeQuery();

            while(rs.next()){
                meca = new MaterialEca();
                meca.setDescripcion(rs.getString("descripcion"));
                meca.setUnidad(rs.getString("medida"));
                meca.setCantidad(rs.getString("cantidad"));
                meca.setVlr_unitario(rs.getString("precio_venta"));
                meca.setVlr_total(rs.getString("valor_total"));
                mecas.add(meca);
            }
        }
        catch(Exception e){
            throw new Exception(e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return mecas;
    }

    public ArrayList<TablaGen> getConsideraciones() throws Exception{
        Connection              con     = null;
        PreparedStatement       ps      = null;
        ResultSet               rs      = null;
        String                  query1   = "SQL_CONSIDERACIONES";
        String                  query2   = "SQL_CONSIDERACIONES2";
        String                  sql     = "";

        ArrayList<TablaGen>     consis  = new ArrayList<TablaGen>();
        TablaGen                consi;
        try{
            con         = this.conectarJNDI(query1);
            sql         = this.obtenerSQL(query1);
            ps          = con.prepareStatement(sql);
            ps.setString(1, this.getNumOferta());
            rs = ps.executeQuery();

            while(rs.next()){
                consi = new TablaGen();
                consi.setTable_code(rs.getString("table_code"));
                consi.setDescripcion(rs.getString("descripcion"));
                consi.setDato("true");
                consis.add(consi);
            }

          // con.close();

           // con         = this.conectarJNDI(query2);
            sql         = this.obtenerSQL(query2);
            ps          = con.prepareStatement(sql);
            ps.setString(1, this.getNumOferta());
            rs          = ps.executeQuery();

            while(rs.next()){
                consi = new TablaGen();
                consi.setTable_code(rs.getString("table_code"));
                consi.setDescripcion(rs.getString("descripcion"));
                consi.setDato("false");
                consis.add(consi);
            }

        }
        catch(Exception e){
            throw new Exception(e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return consis;
    }

    public ArrayList<TablaGen> getConsideracionesByOffer() throws Exception{
        Connection              con     = null;
        PreparedStatement       ps      = null;
        ResultSet               rs      = null;
        String                  query1   = "SQL_CONSIDERACIONES";
        String                  sql     = "";

        ArrayList<TablaGen>     consis  = new ArrayList<TablaGen>();
        TablaGen                consi;

        try{
            con         = this.conectarJNDI(query1);
            sql         = this.obtenerSQL(query1);
            ps          = con.prepareStatement(sql);
            ps.setString(1, this.getNumOferta());
            rs          = ps.executeQuery();

            while(rs.next()){
                consi = new TablaGen();
                consi.setTable_code(rs.getString("table_code"));
                consi.setDescripcion(rs.getString("descripcion"));
                consi.setDato("true");
                consis.add(consi);
            }
        }
        catch(Exception e){
            throw new Exception(e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return consis;
    }
    
    public Oferta getOtrasConsideracionesByOffer() throws Exception{
        Connection              con     = null;
        PreparedStatement       ps      = null;
        ResultSet               rs      = null;
        String                  query1   = "SQL_OTRASCONSIDERACIONES";
        String                  sql     = "";

    //    Oferta     oconsis  = new Oferta();
        Oferta                consi = null;

        try{
            con         = this.conectarJNDI(query1);
            sql         = this.obtenerSQL(query1);
            ps          = con.prepareStatement(sql);
            ps.setString(1, this.getNumOferta());
            rs          = ps.executeQuery();

            while (rs.next()) {
                consi = new Oferta();
                consi.setOtrasConsideraciones(rs.getString("otras_consideraciones"));
            }
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return consi;
    
    }
    

    public DatosOferta getOferta(){
        return oe;
    }

    public String getNumOferta(){
        return numOferta;
    }

    public String getOfferValue(String id_sol) throws Exception{

        Connection          con     = null;
        PreparedStatement   ps      = null;
        ResultSet           rs      = null;
        String              query1  = "SQL_GET_OFFER_VALUE";
        String              sql     = "";
        String              number  = "";

        try{
            con         = this.conectarJNDI(query1);
            sql         = this.obtenerSQL(query1);
            ps          = con.prepareStatement(sql);
            ps.setString(1, id_sol);
            System.out.print(ps.toString());
            rs          = ps.executeQuery();

            if(rs.next()){
                number = rs.getString("suma");
            }
        }
        catch(Exception e){
            throw new Exception(e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();              }   catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              }   catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); }  catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }

        return number;
    }

    public float getIncremento(String periodo) throws Exception{
        Connection              con     = null;
        PreparedStatement       ps      = null;
        ResultSet               rs      = null;
        String                  query1  = "SQL_GET_INCREMENTO";
        String                  sql     = "";
        float                   number  = 0;

        try{
            con         = this.conectarJNDI(query1);
            sql         = this.obtenerSQL(query1);
            ps          = con.prepareStatement(sql);
            ps.setString(1, periodo);
            ps.setString(2, periodo);
            ps.setString(3, this.getNumOferta());
            rs          = ps.executeQuery();

            if(rs.next()){
                number = rs.getFloat("tasa");
            }
        }
        catch(Exception e){
            throw new Exception(e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return number;
    }

    public String getMesesOficial() throws Exception{
        Connection              con     = null;
        PreparedStatement       ps      = null;
        ResultSet               rs      = null;
        String                  query1  = "SQL_GET_MESES";
        String                  sql     = "";
        String                  number  = "No tiene";

        try{
            con         = this.conectarJNDI(query1);
            sql         = this.obtenerSQL(query1);
            ps          = con.prepareStatement(sql);
            ps.setString(1, this.getNumOferta());
            System.out.print(ps.toString());
            rs          = ps.executeQuery();

            if(rs.next()){
                if(rs.getString("consecutivo_oferta").length() > 0){
                    number = rs.getString("esoficial");
                }
            }

        }
        catch(Exception e){
            throw new Exception(e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }

        return number;
    }

    public void setNumOferta(String numOferta) {
        this.numOferta = numOferta;
    }

    public ArrayList<TablaItem> getTisM() {
        return tisM;
    }

    public ArrayList<TablaItem> getTisD() {
        return tisD;
    }

    public ArrayList<TablaItem> getTisO() {
        return tisO;
    }

    /* Los procedimientos siguientes son para
     * la actualizacion de las ofertas
     */

    /**
     * Este metodo me verifica si la oferta es actualizable
     * dependiendo del estado.
     *
     * @param num
     * @return
     * @throws Exception
     */
    public boolean isOfferReady(String num) throws Exception{
        Connection              con     = null;
        PreparedStatement       ps      = null;
        ResultSet               rs      = null;
        String                  query   = "SQL_ACCIONES_TO_UPDATE";
        String                  sql     = "";
        boolean                 ok      = true;

        try{
            //con = this.conectarJNDI(query);
            con= this.conectarJNDI(query);//20101122

            sql = this.obtenerSQL(query);
            ps  = con.prepareStatement(sql);
            ps.setString(1, num);
            rs  = ps.executeQuery();

            if(rs.next()){
                ok = false;
            }
        }
        catch(Exception e){
            throw new Exception(e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            //if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}//20101122
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}//20101122
        }

        return ok;
    }

/**
     * Este metodo actualiza toda la informacion acerca de la oferta
     * incluyendo sus consideraciones y sus cotizaciones.
     *
     * @param oeca Es el bean de la oferta a modificar.
     * @throws Exception
     */
    public void actualizarOferta(String user, OfertaElca oeca,int caso, String tipo) throws Exception{
        Connection con          = null;
        PreparedStatement st    = null;
        ResultSet rs            = null;
        String query            = "SQL_UPDATE_OFERTAS";
        String query5           = "SQL_GET_OPAV_NUM";
        String query6           = "SQL_GET_TIPO_OPAV";
        String query4           = "SQL_UPDATE_ACCIONES";
        String query1           = "SQL_DELETE_CONSIDERACIONES";
        String query2           = "SQL_INSERT_CONSIDERACIONES";
        String consid           = "";
        String sql              = "";
        String sqlGlobal        = "";
        String conseOpav        = "";
        String initipo="";

        String query7 = "SQL_UPDATE_ACCIONES_EMERGENCIA";

        System.out.println("user_"+user+"_oeca_"+oeca+"_caso_"+caso);
        try {

            //con = this.conectarJNDI(query5);//20101122
            con= this.conectarJNDI(query5);//20101122

            con.setAutoCommit(false);
             st = con.prepareStatement(this.obtenerSQL(query6).replaceAll("param1", oeca.getId_solicitud()));
                rs = st.executeQuery();

                if(rs.next()){
                    initipo= rs.getString("tipo");
                }

                st = null;
                rs = null;
               System.out.print("consecu"+oeca.getConsecutivo_oferta());
            if(caso==0&&(oeca.getConsecutivo_oferta()==null||oeca.getConsecutivo_oferta().equals("") || !oeca.getConsecutivo_oferta().contains(".OPA"))){
                st = con.prepareStatement(this.obtenerSQL(query5));
                rs = st.executeQuery();
                if(rs.next()){
                    Date d=new Date();
                    System.out.print(d.toString());
                    conseOpav = rs.getString("numero");
                    conseOpav = d.toString().substring(d.toString().length()-2)+".OPA" + conseOpav;
                }

                st = null;
                rs = null;

                conseOpav = conseOpav + initipo;
            }
            else{
                if(!oeca.getConsecutivo_oferta().substring(13,14).equals(initipo)){
                    conseOpav = oeca.getConsecutivo_oferta().substring(0, 13)+initipo;
                    if(oeca.getConsecutivo_oferta().length()>14){
                       conseOpav=conseOpav+ oeca.getConsecutivo_oferta().substring(14);
                    } else if(oeca.getConsecutivo_oferta().length()>13 && oeca.getConsecutivo_oferta().endsWith("R")){
                       conseOpav = oeca.getConsecutivo_oferta()+initipo;
                    }
                }else{
                conseOpav = "none";
                }
            }
                sql = this.obtenerSQL(query);
                sql = sql.replaceAll("param1", oeca.getNombre_solicitud());
                sql = sql.replaceAll("param2", oeca.getOtras_consideraciones());
                sql = sql.replaceAll("param3", oeca.getId_solicitud());
                if(conseOpav.equals("none")){
                    sql = sql.replaceAll(",consecutivo_oferta = 'param4'","");
                }
                else{
                    sql = sql.replaceAll("param4", conseOpav);
                }
                sql = sql.replaceAll("param5", user);
                sql = sql.replaceAll("param6", oeca.getOficial());

                if (caso==0){//20100422
                    sql = sql.replaceAll("fechitaoferta", " , fecha_oferta = now() ");  //20100422
                }else{//20100422
                    sql = sql.replaceAll("fechitaoferta", " ");  //20100422
                }//20100422

                sqlGlobal += sql + ";";

            sql = this.obtenerSQL(query4);
            //Tocaria hacer el 050 y el 060  junto con el 080 si es emergencia arriba ya esta el paso a 080
            String estado = "";
            if(tipo.equals("Emergencia")){
                    //Estado 050
                    estado = " estado = '050' ";//JJCASTRO EMERGENCIA
                    sql = sql.replaceAll("param1", oeca.getId_solicitud());
                    sql = sql.replaceAll("param2", user);
                    sql = sql.replaceAll("#ESTADO#", estado);//JJCASTRO EMERGENCIA
                    sqlGlobal += sql + ";";
                    //Estado 060
                    String sqresult =    this.asignar(user, "now()",  oeca.getId_solicitud());
                    sqlGlobal += sqresult + ";";

        //
            }
            estado = tipo.equals("Emergencia")?" estado = '080' ":" estado = '050' ";//JJCASTRO EMERGENCIA
            sql = this.obtenerSQL(query7);//JJCASTRO EMERGENCIA
            sql = sql.replaceAll("param1", oeca.getId_solicitud());
            sql = sql.replaceAll("param2", user);
            sql = sql.replaceAll("#ESTADO#", estado);//JJCASTRO EMERGENCIA

            sqlGlobal += sql + ";";


            sql = this.obtenerSQL(query1);
            sql = sql.replaceAll("param1", oeca.getId_solicitud());
            sqlGlobal += sql + ";";

            if(oeca.getConsideraciones().length()>0){
                consid = oeca.getConsideraciones();
                StringTokenizer tokens = new StringTokenizer(consid, ",");
                while (tokens.hasMoreTokens()) {
                    sql = this.obtenerSQL(query2);
                    sql = sql.replaceAll("param1", oeca.getId_solicitud());
                    sql = sql.replaceAll("param2", tokens.nextToken());
                    sqlGlobal += sql + ";";
                }
            }


            DateFormat dateFormat = new SimpleDateFormat("yyyyMM");
            Date dat = new Date();

            if(caso == 1){

                sqlGlobal += actPrecio_venta(oeca.getId_solicitud(),"substring (replace (a.fecha_oferta,'-',''),1,6)");
            }else{

                sqlGlobal += actPrecio_venta(oeca.getId_solicitud(),dateFormat.format(dat));
            }



            System.out.print("\n\n"+sqlGlobal+"\n\n");

            st  = con.prepareStatement(sqlGlobal);
            st.executeUpdate();

            con.commit();
        }
        catch (SQLException e) {
            con.rollback();
            System.out.println("\n\nROLLBACK...");
            throw new SQLException("\n\nERROR DURANTE LA TRANSACCION LOS CAMBIOS NO SE PRODUJERON EL LA BASE DE DATOS:\n" + e.getMessage() + "La siguiente exception es : ----> " + e.getNextException());
        }
        finally {
            if (st  != null){ try{ st.close();                  } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            //if (con != null){ try{ this.desconectar(query5);    } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}//20101122
            if (con != null){ try{ this.desconectar(con);    } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}//20101122
        }
    }

    /* Los procedimientos siguientes son para
     * la anulacion de las ofertas
     */

    public boolean isAnulable(String num) throws Exception{
        Connection              con     = null;
        PreparedStatement       ps      = null;
        ResultSet               rs      = null;
        String                  query   = "SQL_ACCIONES_TO_ANULAR";
        String                  sql     = "";
        boolean                 ok      = true;

        try{
            con = this.conectarJNDI(query);
            sql = this.obtenerSQL(query);
            ps  = con.prepareStatement(sql);
            ps.setString(1, num);
            rs  = ps.executeQuery();

            if(rs.next()){
                ok = false;
            }
        }
        catch(Exception e){
            System.out.println("error en isAnulable"+e.toString()+"__"+e.getMessage());
            throw new Exception(e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return ok;
    }

    public String actualizarAnulacionOferta(OfertaElca oeca, boolean flag,String userx) throws Exception{
        String respuesta="Denegacion pendiente.";
        Connection con          = null;
        PreparedStatement st    = null;
        ResultSet rs            = null;
        String query            = "SQL_ANULAR_OFERTAS";
        //String query5           = "SQL_GET_OPAV_NUM";
        //String query6           = "SQL_GET_TIPO_OPAV";
        String query4           = "SQL_DENEGAR_ACCIONES";
        String query1           = "SQL_DELETE_RAZONES_ANULACIONES";
        String query2           = "SQL_INSERT_RAZONES_ANULACIONES";
        //String query3           = "SQL_UPDATE_COTDETS";
        String consid           = "";
        String sql              = "";
        String sqlGlobal        = "";
        String conseOpav        = "";
        String[] listSql = null;

        try {

            //con = this.conectarJNDI(query);
            //con.setAutoCommit(false);

            //-----------------------------------------------------------------------

            sql = this.obtenerSQL(query);
            //sql = sql.replaceAll("param1", oeca.getNombre_solicitud());
            sql = sql.replaceAll("param2", oeca.getOtras_consideraciones());
            sql = sql.replaceAll("param3", oeca.getId_solicitud());
            sql = sql.replaceAll("param5", userx);
            /*if(conseOpav.equals("none")){
                sql = sql.replaceAll(",consecutivo_oferta = 'param4'","");
            }
            else{
                sql = sql.replaceAll("param4", conseOpav);
            } */
            sqlGlobal += sql + ";";

            sql = this.obtenerSQL(query4);
            sql = sql.replaceAll("param1", oeca.getId_solicitud());
            sql = sql.replaceAll("param5", userx);
            //.out.println("sql::"+sql);
            sqlGlobal += sql + ";";

            sql = this.obtenerSQL(query1);
            sql = sql.replaceAll("param1", oeca.getId_solicitud());
            sqlGlobal += sql + ";";

            if(oeca.getConsideraciones().length()>0){
                consid = oeca.getConsideraciones();
                StringTokenizer tokens = new StringTokenizer(consid, ",");
                while (tokens.hasMoreTokens()) {
                    sql = this.obtenerSQL(query2);
                    sql = sql.replaceAll("param1", oeca.getId_solicitud());
                    sql = sql.replaceAll("param2", tokens.nextToken());
                    sql = sql.replaceAll("param5", userx);
                    sqlGlobal += sql + ";";
                }
            }
            
            listSql = sqlGlobal.split(";");
            TransaccionService  scv = new TransaccionService(this.getDatabaseName());
            scv.crearStatement();
            for(int i = 0; i < listSql.length; i++){
                scv.getSt().addBatch(listSql[i]);
            }
            scv.execute();
            /*sql = this.obtenerSQL(query3);
            sql = sql.replaceAll("param1", oeca.getId_solicitud());
            sqlGlobal += sql + ";";*/

           // st  = con.prepareStatement(sqlGlobal);
           // st.executeUpdate(sqlGlobal);

            //con.commit();
            respuesta="Oferta denegada.";
        }
        catch (SQLException e) {
            System.out.println("errorsql en anular oferta: "+e.toString()+"__"+e.getMessage());
            con.rollback();
            System.out.println("\n\nROLLBACK...");
            throw new SQLException("\n\nERROR DURANTE LA TRANSACCION LOS CAMBIOS NO SE PRODUJERON EL LA BASE DE DATOS:\n" + e.getMessage() + "La siguiente exception es : ----> " + e.getNextException());
        }catch(Exception ee){
            System.out.println("error en anular oferta: "+ee.toString()+"__"+ee.getMessage());
        }finally {
            if (st  != null){ try{ st.close();                  } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con);    } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return respuesta;
    }

    public DatosOferta ofertaInfoAnul(String num) throws Exception{
        Connection          con      = null;
        PreparedStatement   ps       = null;
        ResultSet           rs       = null;
        String              query    = "SQL_OFERTA_INFO";
        String              sql      = "";
        DatosOferta oe2=new DatosOferta();
        try{
            oe2          = null;
            oe2          = new DatosOferta();

            con         = this.conectarJNDI(query);
            sql         = this.obtenerSQL(query);
            ps          = con.prepareStatement(sql);
            ps.setString(1, num);
            rs          = ps.executeQuery();

            if(rs.next()){
                oe2.setId_solicitud(num);
                oe2.setOferta(rs.getString("nombre_solicitud"));
                oe2.setElaboradoPor(rs.getString("nombre_elaborado"));
                oe2.setFechaGeneracion(rs.getString("fecha_oferta"));
                oe2.setAprobadoPor(rs.getString("nombre_aprobado"));
                oe2.setEjecutivo(rs.getString("nombre_ejecutivo"));
                oe2.setCliente(rs.getString("nombre_cliente"));
                oe2.setNIC(rs.getString("nic"));
                oe2.setConsecutivo(rs.getString("consecutivo_oferta"));
                oe2.setCiudad(rs.getString("ciudad"));
                oe2.setDepartamento(rs.getString("departamento"));
                oe2.setDireccion(rs.getString("direccion"));
                oe2.setRepresentante(rs.getString("nombre_representante"));
                oe2.setNIC(rs.getString("nit"));
                oe2.setOtras_consideraciones(rs.getString("otras_anulaciones"));
            }
        }
        catch(Exception e){
            System.out.println("errore en ofertaInfoAnul"+e.toString()+"__"+e.getMessage());
            throw new Exception(e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return oe2;
    }

    public ArrayList<TablaGen> getAnulaciones(String num_of) throws Exception{
        Connection              con     = null;
        PreparedStatement       ps      = null;
        ResultSet               rs      = null;
        String                  query1   = "SQL_ANULACIONES";
        String                  query2   = "SQL_ANULACIONES2";
        String                  sql     = "";

        ArrayList<TablaGen>     anuls  = new ArrayList<TablaGen>();
        TablaGen                anul;
        try{
            try{
                con         = this.conectarJNDI(query1);
                sql         = this.obtenerSQL(query1);
                ps          = con.prepareStatement(sql);
                ps.setString(1, num_of);
                //.out.println("ps1"+ps.toString());
                rs          = ps.executeQuery();

                while(rs.next()){
                    anul = new TablaGen();
                    anul.setTable_code(rs.getString("table_code"));
                    anul.setDescripcion(rs.getString("descripcion"));
                    anul.setDato("true");
                    anuls.add(anul);
                }

            }finally{
                this.desconectar(con);
            }
            con         = this.conectarJNDI(query2);
            sql         = this.obtenerSQL(query2);
            ps          = con.prepareStatement(sql);
            ps.setString(1, num_of);
            ////.out.println("ps2"+ps.toString());
            rs          = ps.executeQuery();

            while(rs.next()){
                anul = new TablaGen();
                anul.setTable_code(rs.getString("table_code"));
                anul.setDescripcion(rs.getString("descripcion"));
                anul.setDato("false");
                anuls.add(anul);
            }

        }
        catch(Exception e){
            System.out.println("error en getAnulaciones"+e.toString());
            throw new Exception(e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return anuls;
    }

    public void actualizarCotizacion(String id_solicitud) throws Exception{
        Connection          con         = null;
        PreparedStatement   ps          = null;
        ResultSet           rs          = null;
        String              query       = "SQL_UPDATE_COTDETS";
        DateFormat          dateFormat  = new SimpleDateFormat("yyyyMM");
        Date                dat         = new Date();
        String              sql         = "";

        try{
            con         = this.conectarJNDI(query);
            sql         = this.obtenerSQL(query);
            sql = sql.replaceAll("param1", id_solicitud);
            sql = sql.replaceAll("param2", dateFormat.format(dat));
            ps          = con.prepareStatement(sql);

            System.out.println("\n\n"+sql);

            ps.executeUpdate();
        }
        catch(Exception e){
            throw new Exception(e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con);   } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }

    public String actPrecio_venta(String id_solicitud,String fecha)throws Exception{
            String query3           = "SQL_UPDATE_COTDETS";
            String sql = this.obtenerSQL(query3);
            sql = sql.replaceAll("param1", id_solicitud);
            sql = sql.replaceAll("'param2'", fecha);
            return sql + ";";
    }

    /**
     * metodo para actualizar el cambio de cliente de solicitud
     * @param idclie id del cliente
     * @param idsol id de las solicitud
     * @param nic numero del nic
     * @author MGarizao - GEOTECH
     * @date 07/04/2010
     * @version 1.0
     * @return
     * @throws Exception
     */
     public String actualizarCambioClienteSol(String idclie, String idsol, String nic) throws Exception{

        Connection con          = null;
        PreparedStatement st    = null;
        ResultSet rs            = null;
        String query1           = "SQL_UPDATE_OFERTAS2";
        String query2           = "SQL_UPDATE_ORDEN_TRABAJO";
        String sql              = "";
        String sqlGlobal        = "";
        String msn = "";


        try {

            con = this.conectarJNDI(query1);
            con.setAutoCommit(false);

            sql = this.obtenerSQL(query1);
            sql = sql.replaceAll("param1", idclie);
            sql = sql.replaceAll("param2", nic);
            sql = sql.replaceAll("param3", idsol);
            sqlGlobal += sql + ";";



            sql = this.obtenerSQL(query2);
            sql = sql.replaceAll("param1", nic);
            sql = sql.replaceAll("param2", idsol);
            sqlGlobal += sql + ";";


            String[] listSql = sqlGlobal.split(";");
            for(int i=0; i < listSql.length; i++){
                st  = con.prepareStatement(listSql[i]);
                st.executeUpdate();
            }
            con.commit();

            msn = "Transaccion exitosa";
        }
        catch (SQLException e) {
            e.printStackTrace();
            con.rollback();
            throw new SQLException("\n\nERROR DURANTE LA TRANSACCION LOS CAMBIOS NO SE PRODUJERON EL LA BASE DE DATOS:\n" + e.getMessage() + "La siguiente exception es : ----> " + e.getNextException());
        }
        finally {
            if (st  != null){ try{ st.close();                  } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con);    } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }

        return msn;
    }
    /**
     * Encuentra el correo del ejecutivo asociado a la solicitud
     * @param id_solicitud El codigo del la solicitud
     * @return String con el correo del ejecutivo
     * @throws Exception Cuando hay error
     * @since 2010-05-10 rhonalf
     */
    public String mailEjecutivo(String id_solicitud) throws Exception{
        String mail = "";
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "MAIL_EJEC";
        String sql = "";
        try {
            con = this.conectarJNDI(query);
            sql = this.obtenerSQL(query);
            st = con.prepareStatement(sql);
            st.setString(1, id_solicitud);
            rs = st.executeQuery();
            if(rs.next()){
                mail = rs.getString("correo");
            }
            rs.close();
            st.close();
            this.desconectar(con);
        }
        catch (Exception e) {
            throw new Exception("Error al buscar el mail del ejecutivo: "+e.toString());
        }
        finally {
            if(st!=null) st.close();
            if(!con.isClosed()) this.desconectar(con);
        }
        return mail;
    }

    /**
     * Busca desde cual correo se envia el mensaje
     * @return String con el mail buscado
     * @throws Exception Cuando hay error
     * @since 2010-05-10 rhonalf
     */
    public String mailFrom() throws Exception{
        String from="";
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "MAIL_FROM";
        String sql = "";
        try {
            con = this.conectarJNDI(query);
            sql = this.obtenerSQL(query);
            st = con.prepareStatement(sql);
            rs = st.executeQuery();
            if(rs.next()){
                from = rs.getString("dato");
            }
            rs.close();
            st.close();
            this.desconectar(con);
        }
        catch (Exception e) {
            throw new Exception("Error al buscar el mail_from: "+e.toString());
        }
        finally {
            if(st!=null) st.close();
            if(!con.isClosed()) this.desconectar(con);
        }
        return from;
    }

    /**
     * Busca a cual correo se envia el mensaje
     * @return String con el mail buscado
     * @throws Exception Cuando hay error
     * @since 2010-05-10 rhonalf
     */
    public String mailTo() throws Exception{
        String to="";
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "MAIL_TO";
        String sql = "";
        try {
            con = this.conectarJNDI(query);
            sql = this.obtenerSQL(query);
            st = con.prepareStatement(sql);
            rs = st.executeQuery();
            if(rs.next()){
                to = rs.getString("dato");
            }
            rs.close();
            st.close();
            this.desconectar(con);
        }
        catch (Exception e) {
            throw new Exception("Error al buscar el mail_from: "+e.toString());
        }
        finally {
            if(st!=null) st.close();
            if(!con.isClosed()) this.desconectar(con);
        }
        return to;
    }

    public String[] obtainGerente() throws Exception{//20100702
        String[] to=new String[3];
        to[0]="";
        to[1]="";
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "GET_GERENTE";
        String sql = "";
        try {
            con = this.conectarJNDI(query);
            sql = this.obtenerSQL(query);
            st = con.prepareStatement(sql);
            rs = st.executeQuery();
            if(rs.next()){
                to[0] = rs.getString(1);
                to[1] = rs.getString(2);
                to[2] = rs.getString(3);
            }
            rs.close();
            st.close();
            this.desconectar(con);
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error al obtainGerente: "+e.toString());
        }
        finally {
            if(st!=null) st.close();
            if(!con.isClosed()) this.desconectar(con);
        }
        return to;
    }

    public String asignar(String loginx, String fecof,String oferticas) throws Exception{

        StringStatement  st    = null;
        String             query = "SQL_ASIGNAR";
        Connection con=null;
        String sql = "";
        try{
            st            =   new StringStatement( this.obtenerSQL(query).replaceAll("solicitudesx",oferticas), true );
            st.setString       (1, loginx);
            st.setString       (2, loginx);
            String estad_tem="060";
            st.setString       (3, estad_tem);
            st.setString       (4, estad_tem);
            sql = st.getSql();
        }catch(Exception e){
            System.out.println("asignar error:"+e.toString()+"__"+e.getMessage());
            e.printStackTrace();
            throw new SQLException("Error asignar() : "+ e.getMessage());
        }
        finally{
            if (st  != null){ try{ st = null;              } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con);   } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return sql;
    }
/**
     * Este metodo devuelve el estado minimo de las acciones de la oferta
     *
     *
     * @param id_sol    ID de la oferta.
     * @return          Devuelve un int con el estado.
     */
    public int minEstado(String id_sol) throws Exception{
         Connection              con     = null;
        PreparedStatement       ps      = null;
        ResultSet               rs      = null;
        String                  query   = "SQL_MIN_ESTADO";
        String                  sql     = "";
        int                min=0;

        try{
            //con = this.conectarJNDI(query);
            con= this.conectarJNDI(query);//20101122

            sql = this.obtenerSQL(query);
            ps  = con.prepareStatement(sql);
            ps.setString(1, id_sol);
            rs  = ps.executeQuery();
            if(rs.next()){
                min = Integer.parseInt(rs.getString("min"));
            }
        }
        catch(Exception e){
            throw new Exception(e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            //if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}//20101122
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}//20101122
        }

        return min;
    }

     /**
     * Este metodo devuelve el estado maximo de las acciones de la oferta
     *
     *
     * @param id_sol    ID de la oferta.
     * @return          Devuelve un int con el estado.
     */
    public int maxEstado(String id_sol) throws Exception{
         Connection              con     = null;
        PreparedStatement       ps      = null;
        ResultSet               rs      = null;
        String                  query   = "SQL_MAX_ESTADO";
        String                  sql     = "";
        int                max=999;

        try{
            //con = this.conectarJNDI(query);
            con= this.conectarJNDI(query);//20101122

            sql = this.obtenerSQL(query);
            ps  = con.prepareStatement(sql);
            ps.setString(1, id_sol);
            rs  = ps.executeQuery();
            if(rs.next()){
                max = Integer.parseInt(rs.getString("max"));
            }
        }
        catch(Exception e){
            throw new Exception(e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            //if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}//20101122
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}//20101122
        }

        return max;
    }


    /**
     * Este metodo devuelve un arrayList con las ofertas que se van a denegar automaticamente
     *
     * @param num   validez de la oferta.
     * @return      Devuelve un arrayList con las ofertas que se van a denegar automaticamente
     */
    public ArrayList ofertaDenegacion(int num) throws Exception{
        Connection              con     = null;
        PreparedStatement       ps      = null;
        ResultSet               rs      = null;
        String                  query   = "SQL_DENEGACION_AUTOMATICA";
        String                  sql     = "";

        ArrayList<String> solicitudes=new ArrayList();

        try{
            con= this.conectarJNDI(query);

            sql = this.obtenerSQL(query);
            ps  = con.prepareStatement(sql);
            ps.setInt(1, num);
            rs  = ps.executeQuery();

            while(rs.next()){
                solicitudes.add( rs.getString("id_solicitud"));
            }
        }
        catch(Exception e){
            throw new Exception(e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }

        return solicitudes;
    }

    /**
     * Este metodo devuelve un entero con el numero de dias en que es valida una oferta
     *
     * @return      Devuelve un int con el numero de dias en que es valida una oferta
     */
    public int vaidezOferta() throws Exception{
        Connection              con     = null;
        PreparedStatement       ps      = null;
        ResultSet               rs      = null;
        String                  query   = "SQL_VALIDEZ_OFERTA";
        String                  sql     = "";
        int val=1000;

        try{
            con= this.conectarJNDI(query);

            sql = this.obtenerSQL(query);
            ps  = con.prepareStatement(sql);
            rs  = ps.executeQuery();

            if(rs.next()){
                val= Integer.parseInt(rs.getString("referencia"));
            }
        }
        catch(Exception e){
            throw new Exception(e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }

        return val;
    }

    /**
     * Este metodo devuelve un string con el aviso o opd
     *
     * @param solicitud   id de la solicitud.
     * @return      devuelve un string con el aviso o opd
     */
    public String getOpdAviso(String solicitud) throws Exception{
        Connection              con     = null;
        PreparedStatement       ps      = null;
        ResultSet               rs      = null;
        String                  query   = "SQL_OPD_AVISO";
        String                  sql     = "";
        String num="";

        try{
            con= this.conectarJNDI(query);

            sql = this.obtenerSQL(query);
            ps  = con.prepareStatement(sql);
            rs  = ps.executeQuery();

            if(rs.next()){
                num=rs.getString("opd");
            }
        }
        catch(Exception e){
            throw new Exception(e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }

        return num;
    }

    /**
     * Este metodo reevalua la oferta
     * @param id_sol id oferta
     * @param login  usuario activo
     * @return Devuelve un boolean que indica si fue reevaluda correctamente.
     * @throws Exception 
     * JPACOSTA
     */
    public boolean revaluarOferta(String id_sol, String login) throws Exception {
        boolean ok = false;
        Connection con = null;
        PreparedStatement ps = null;
        String sqlO = "SQL_UPDATE_OFERTA_REEV";
        String sqlA = "SQL_UPDATE_ACCIONES";
        try {
            con= this.conectarBDJNDI("fintra");

            sqlO = this.obtenerSQL(sqlO);
            sqlO = sqlO.replaceAll("nombre_solicitud='',", "");
            sqlO = sqlO.replaceAll("param1", id_sol);
            sqlO = sqlO.replaceAll("param2", login);
            sqlO = sqlO.replaceAll("'param3'", "consecutivo_oferta||'R'");
            
            ps  = con.prepareStatement(sqlO);
            ps.executeUpdate();
            
            sqlA = this.obtenerSQL(sqlA);
            sqlA = sqlA.replaceAll("param1", id_sol);
            sqlA = sqlA.replaceAll("param2", login);
            sqlA = sqlA.replaceAll("#ESTADO#", "estado = '055'");
            sqlA = sqlA.replaceAll("(estado = '040' or estado = '050')", "(estado::integer between 030 and 060)");
            
            ps  = con.prepareStatement(sqlA);
            ps.executeUpdate();
            
            ok = true;
        } catch(Exception e) {
            e.printStackTrace();
        } finally {
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
            return ok;
        }
    }
    
    /**
     * Este metodo reevalua la oferta
     *
     *
     * @param id_sol    ID de la oferta.
     * @return          Devuelve un boolean que indica si fue reevaluda correctamente.
     */
    public boolean reevaluarOferta(String id_sol, String login) throws Exception {
        boolean ok = false;
        ArrayList<String> list = new ArrayList();
        PreparedStatement ps = null;
        ResultSet rs = null;
        String sql;
        String query = "SQL_ACCIONES_REEVALUACION";
        String query1 = "SQL_COT_REEVALUACION";
        String query2 = "SQL_ULT_COT";
        String query3 = "SQL_DET_COT";
        String query4 = "SQL_UPDATE_DET_COT";
        String query5 = "SQL_UPDATE_COT";
        String query6 = "SQL_INSERT_DET_COT";
        String query7 = "SQL_INSERT_COT";
        String query8 = "SQL_CONSECUTIVO_OFERTA";
        String query9 = "SQL_UPDATE_OFERTA_REEV";
        Connection con = null;
        Statement stmt = null;
        int max = 0;
        String reev = "1";

        try {
            con = this.conectarJNDI(query);
            con.setAutoCommit(false);
            try {
                stmt = con.createStatement();
                max = maxEstado(id_sol);
                if (max >= 30) {
                    System.out.print("respuesta 30 ");
                    sql = this.obtenerSQL(query);
                    sql = sql.replaceAll("param1", id_sol);
                    sql = sql.replaceAll("param2", login);
                    list.add(sql);
                }
                if (max == 40) {
                    System.out.print("respuesta 40");
                    sql = this.obtenerSQL(query1);
                    sql = sql.replaceAll("param1", id_sol);
                    sql = sql.replaceAll("param2", login);
                    list.add(sql);
                }
                if (max >= 50) {
                    System.out.print("respuesta 50");
                    Cotizacion cot = null;
                    ArrayList<Cotizacion> res = new ArrayList();
                    ArrayList cbcot = new ArrayList();

                    String conse = "";
                    sql = this.obtenerSQL(query2);
                    ps = con.prepareStatement(sql);
                    ps.setString(1, id_sol);
                    rs = ps.executeQuery();
                    if (rs.next()) {
                        conse = rs.getString("consecutivo");
                    }
                    rs = null;
                    ps = null;
                    String[] nreev = conse.split("-");
                    System.out.print("reevaluacion"+nreev.length);
                    if (nreev.length > 1) {
                        reev = nreev[1];
                        reev=(Integer.parseInt(reev)+1)+"";
                    }
                    sql = this.obtenerSQL(query3);
                    ps = con.prepareStatement(sql);
                    ps.setString(1, id_sol);
                    rs = ps.executeQuery();
                    while (rs.next()) {
                        cot = new Cotizacion();//091222
                        cot.setCodigo(rs.getString("cod_cotizacion"));
                        cot.setAprobado(rs.getString("aprobado"));
                        cot.setMaterial(rs.getString("codigo_material"));
                        cot.setFecha(rs.getString("fecha"));
                        cot.setObservacion(rs.getString("observacion"));
                        cot.setAccion(rs.getString("id_accion"));
                        cot.setCantidad(rs.getDouble("cantidad"));
                        cot.setCompra(rs.getString("compra_provint"));
                        cot.setCantidad_compra(rs.getDouble("cant_provint"));
                        cot.setValor(rs.getDouble("precio"));
                        cot.setPrecioen_material(rs.getDouble("precio_venta"));
                        res.add(cot);
                    }
                    rs = null;
                    ps = null;
                    if (!res.isEmpty()) {
                        sql = this.obtenerSQL(query4);
                        sql = sql.replaceAll("param1", id_sol);
                        sql = sql.replaceAll("param2", login);
                        list.add(sql);

                        sql = this.obtenerSQL(query5);
                        sql = sql.replaceAll("param1", id_sol);
                        sql = sql.replaceAll("param2", login);
                        list.add(sql);

                        for (int i = 0; i < res.size(); i++) {
                            String codigo=res.get(i).getCodigo();
                            if(res.get(i).getCodigo().contains("-")){
                                codigo=res.get(i).getCodigo().substring(0,res.get(i).getCodigo().indexOf("-"));
                            }
                            if (!cbcot.contains(res.get(i).getCodigo())) {
                                sql = this.obtenerSQL(query7);
                                sql = sql.replaceAll("param1", codigo + "-" + reev);
                                sql = sql.replaceAll("param2", res.get(i).getFecha());
                                sql = sql.replaceAll("param3", res.get(i).getAccion());
                                sql = sql.replaceAll("param4", login);
                                list.add(sql);
                                cbcot.add(res.get(i).getCodigo());
                            }

                            sql = this.obtenerSQL(query6);
                            sql = sql.replaceAll("param1", res.get(i).getMaterial());
                            sql = sql.replaceAll("param2", res.get(i).getCantidad() + "");
                            sql = sql.replaceAll("param3", res.get(i).getAprobado());
                            sql = sql.replaceAll("param4", codigo+ "-" + reev);
                            sql = sql.replaceAll("param5", res.get(i).getFecha());
                            sql = sql.replaceAll("param6", res.get(i).getObservacion());
                            sql = sql.replaceAll("param7", res.get(i).getAccion());
                            sql = sql.replaceAll("param8", res.get(i).getCompra());
                            sql = sql.replaceAll("param9", res.get(i).getCantidad_compra() + "");
                            sql = sql.replaceAll("param-10", res.get(i).getValor() + "");
                            sql = sql.replaceAll("param-11", res.get(i).getPrecioen_material() + "");
                            sql = sql.replaceAll("param-12", login);
                            list.add(sql);
                        }
                    }
                    String oferta="";
                    sql = this.obtenerSQL(query8);
                    ps = con.prepareStatement(sql);
                    ps.setString(1, id_sol);
                    rs = ps.executeQuery();
                    if (rs.next()) {
                        oferta=rs.getString("consecutivo_oferta");
                    }
                    if(oferta.contains("-")){
                                oferta=oferta.substring(0,oferta.indexOf("-"));
                    }
                    oferta=oferta+"-"+reev;
                    sql = this.obtenerSQL(query9);
                    sql = sql.replaceAll("param1", id_sol);
                    sql = sql.replaceAll("param2", login);
                    sql = sql.replaceAll("param3", oferta);
                    list.add(sql);

                }

                for (int i = 0; i < list.size(); i++) {
                    stmt.addBatch(list.get(i));
                }

                stmt.executeBatch();
                stmt.clearBatch();
                con.commit();
                ok = true; //mcaseres
            } catch (Exception e) {
                System.out.println("ROLLBACK...");
                con.rollback();
                ok = false; //mcaseres
                if (e instanceof SQLException) {
                    throw new SQLException("ERROR DURANTE LA TRANSACCION LOS CAMBIOS NO SE PRODUJERON EL LA BASE DE DATOS:\n" + e.getMessage() + "La siguiente exception es : ----> " + ((SQLException) e).getNextException());
                } else {
                    e.printStackTrace();
                }
            } finally {
                if (stmt != null) {
                    stmt.close();
                }
                if (con != null) {
                    this.desconectar(con);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return ok;
    }

      public ArrayList<TablaGen> getAnulaciones() throws Exception {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query1 = "SQL_ANULACIONES3";
        String sql = "";

        ArrayList<TablaGen> anuls = new ArrayList<TablaGen>();
        TablaGen anul;
        try {

            con = this.conectarJNDI(query1);
            sql = this.obtenerSQL(query1);
            ps = con.prepareStatement(sql);

            rs = ps.executeQuery();

            while (rs.next()) {
                anul = new TablaGen();
                anul.setTable_code(rs.getString("table_code"));
                anul.setDescripcion(rs.getString("descripcion"));
                anul.setDato("false");
                anuls.add(anul);
            }

        } catch (Exception e) {
            System.out.println("error en getAnulaciones" + e.toString());
            throw new Exception(e.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return anuls;
    }
/***
 *
 * @param id_sol
 * @return
 * @throws Exception
 */
        public String tipoOferta(String id_sol) throws Exception{

        Connection          con     = null;
        PreparedStatement   ps      = null;
        ResultSet           rs      = null;
        String              query1  = "SQL_TIPO_OFERTA";
        String              tipo  = "";

            try {
                con = this.conectarJNDI(query1);
                if (con != null) {
                    ps = con.prepareStatement(this.obtenerSQL(query1));
                    ps.setString(1, id_sol);
                    System.out.print(ps.toString());
                    rs = ps.executeQuery();

                    if (rs.next()) {
                        tipo = rs.getString("tipo_solicitud");
                    }
                }
            }
        catch(Exception e){
            throw new Exception(e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();              }   catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              }   catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); }  catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }

        return tipo;
    }

public String getConsecutivoOferta() throws Exception {
        String consecutivo = "";
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_GET_OPAV_NUM";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            if (rs.next()) {
                consecutivo = rs.getString("numero");

            }
        } catch (Exception e) {
            System.out.println("Error en getConsecutivoOferta: " + e.toString());
            e.printStackTrace();
            throw new Exception("Error en getConsecutivoOferta: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return consecutivo;
    }

    public String actualizarOferta(OfertaElca oferta) throws Exception {
        String respuesta = "";
        StringStatement st = null;
        String query = query = "SQL_UPDATE_OFERTA";
        try {
            st = new StringStatement(this.obtenerSQL(query), true);//JJCastro fase2
            st.setString(1, oferta.getConsecutivo_oferta());
            st.setString(2, oferta.getUser_update());
            st.setString(3, oferta.getUser_update());
            st.setString(4, oferta.getId_solicitud());

            respuesta = st.getSql();

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e);
            throw e;
        } finally {
            if (st != null) {
                try {
                    st = null;
                } catch (Exception e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
        }
        return respuesta;
    }

    public String[] obtenerAprobador(String id_solicitud) throws Exception{//20100702
        String[] to=new String[3];
        to[0]="";
        to[1]="";
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "GET_APROBADOR";
        String sql = "";
        try {
            con = this.conectarJNDI(query);
            sql = this.obtenerSQL(query);
            st = con.prepareStatement(sql);
            st.setString(1, id_solicitud);
            rs = st.executeQuery();
            if(rs.next()){
                to[0] = rs.getString(1);
                to[1] = rs.getString(2);
                to[2] = rs.getString(3);
            }
            rs.close();
            st.close();
            this.desconectar(con);
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error al obtainGerente: "+e.toString());
        }
        finally {
            if(st!=null) st.close();
            if(!con.isClosed()) this.desconectar(con);
        }
        return to;
    }

}