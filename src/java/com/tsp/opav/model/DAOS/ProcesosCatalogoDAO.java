/*opav/model/daos
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.opav.model.DAOS;

import com.google.gson.JsonObject;
import com.tsp.operation.model.beans.CmbGeneralScBeans;
import com.tsp.opav.model.beans.Categoria;
import com.tsp.opav.model.beans.Mapeo_insumos;
import com.tsp.operation.model.beans.UnidadesNegocio;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.beans.Proveedor;
import com.tsp.opav.model.beans.Especificaciones;
import com.tsp.opav.model.beans.ValorPredeterminado;
import com.tsp.opav.model.beans.UnidadMedida;
import java.util.ArrayList;

/**
 *
 * @author fnunez
 */
public interface ProcesosCatalogoDAO {

    /**
     *
     * @param empresa
     * @param statusoperation.
     * @return
     */
    
    public abstract  ArrayList<String> cargarNom_subCat(String insumo );
    
    public abstract JsonObject modificar_esp(JsonObject info);
    
    public abstract JsonObject modificar_espVal(JsonObject info);
    
    public abstract JsonObject modificar_espVal_edit(JsonObject info);
    
    public abstract JsonObject modificar_Val(JsonObject info);
    
    public ArrayList<UnidadMedida> cargarunidadmedida(String empresa,String status);
    
    public ArrayList<ValorPredeterminado> listar_valorespredeterminados(String empresa,String status);
    
    public ArrayList<ValorPredeterminado> listar_valorespredeterminados_filtro(String empresa,String status, String id, String ids);
    
    public ArrayList<ValorPredeterminado> listar_valorespredeterminados_filtro_edit(String empresa,String status, String id);
    
    public ArrayList<ValorPredeterminado> listar_valorespredeterminados_edit(String empresa,String status, String id);
    
    public ArrayList<Proveedor> cargarproveedores(String empresa,String status);
    
    public ArrayList<Categoria> cargarProcesosMeta(String empresa, String status,String insumo);
    
    public ArrayList<Mapeo_insumos> cargarMapeo(String subcategoria);//////////////////////jean
    
    public ArrayList<CmbGeneralScBeans> cargarComboEmpresa();

    public boolean existeMetaProceso(String empresa, String descripcion);

    public boolean existeEspecificacion(String empresa, String descripcion);

    public String guardarMetaProceso(String insumo,String empresa, String nombre, String descripcion, String usuario);
    
    public String guardarValorPredeterminado(String empresa, String nombre, String usuario);

    public String guardarEspecificacion(String empresa, String nombre, String descripcion, String usuario,String idtipo);

    public String actualizarMetaProceso(String empresa, String nombre, String descripcion, String idProceso, String usuario);
    
    public String obtenerEspecificacionesColumnas(String subcategoria);
    
    public String cargarComboProcesoMeta(String empresa);
    
    //public String cargarComboInsumos(String empresa);

    public ArrayList<Categoria> cargarProcesoInterno();

    public boolean existeProcesoInterno(String procesoMeta, String descripcion);
    
    public boolean existeSubcategoria(String descripcion, String dstrct);
    
    public int guardarSubcategoria(String nombre, String descripcion, String usuario, String dstrct);
    
    public boolean existeRelCatSubcategoria(int idsubcategoria, String idcategoria);
    
    public int guardarRelCatSubcategoria(String idsubcategoria, int idcategoria, String usuario, String dstrct);
    
    public int actualizarRelCatSubcategoria(String idsubcategoria, int idcategoria, String dstrct);
    
    public int obtenerIdSubcategoria(String nombre, String dstrct);
    
    public String guardarProcesoInterno(String procesoMeta, String nombre, String descripcion, String usuario, String empresa);

    public int obtenerIdProcesoInterno(String nomProceso, String idProMeta);

    public String insertarRelUnidadProInterno(int idProInterno, String string, String string2, String empresa);
    
    public String insertarRelValPred(int idProInterno, String string, String empresa, String usuario);

    public ArrayList<Especificaciones> cargarUndNegocioProinterno(String idProinterno);
    
    public String cargarCategorias( );

    public String actualizarProcesoInterno(int idProinterno, String nombre, String descripcion, int idProMeta, String usuario, String empresa, int idSubcategoria);

    public String eliminarUndProinterno(String idProinterno, String idUnidad);

    public ArrayList<Usuario> listarUsuario();

    public ArrayList<Categoria> listarProinternoUsuario(int parseInt);

    public ArrayList<Categoria> listarProinterno();

    public String insertarRelProInternoUser(String idProInt, int codUsuario, String login, String empresa);
    
    public boolean existeMetaProceso(String empresa, String descripcion, int idProceso);
    
    public boolean existeProcesoInterno(String procesoMeta, String descripcion, int idProceso);
    
    public ArrayList<Categoria> cargarProcesoInterno(int idMetaProceso, String status);
    
    public ArrayList<Especificaciones> cargarUnidadesNegocio(int parseInt);
       
    public String anularMetaProceso(int idProceso,String login);
     
    public String anularProcesoInterno(int idProinterno,int idProcesoMeta,String login);
    
    public ArrayList<Usuario> listarUsuariosProinterno(int parseInt);
    
    public ArrayList<Usuario> listarUsuariosRelProInterno(int idProceso);
    
    public String eliminarUsuarioProinterno(int idProinterno, String idUsuario);
    
    public boolean existenUsuariosRelProceso(int idProceso, String tipo);
    
    public String activarMetaProceso(int idProceso);
    
    public String activarProcesoInterno(int idProceso);
    
    public String actualizarModerador(int idProceso, int idusuario, String moderador);
    
    public ArrayList<Usuario> listarUsuariosRelProInterno();
    
    public ArrayList<Categoria> cargarProcesosRelUsuario(String usuario);
    
    public ArrayList<Categoria> cargarTipoRequisicionRelUsuario(String usuario);
    
    public String eliminarRelProcesoReqUser(int idUsuario);
    
    public String insertarRelProcesoReqUser(String idProceso, int codUsuario, String login, Usuario usuario);
    
    public String eliminarRelTipoReqUser(int idUsuario);
    
    public String insertarRelTipoReqUser(String idTipoReq, int codUsuario, String login, Usuario usuario);

    public JsonObject cargar_combo(JsonObject info);
    
    public JsonObject consultar(String idsubcategoria);
    
    public boolean verificaExisteSub(int categoria);

    public ArrayList<String> cargarNom_Especificacion(String idcategoria);
    
    public boolean anularInsumo(int id);

    public JsonObject cargar_combo_insumos(JsonObject info);
    
    public boolean existeTipoInsumo(String empresa, String nombre, String id);
    
    public String guardarTipoInsumo(String empresa, String nombre, String porcentaje_rentabilidad,  String iva_con_aiu, String login);

    public String cargar_combo_tipo_insumos();

    public String cargar_combo_categoria(int id_insumo);
    
    public String CargarTipoInsumos();
    
    public String actualizarTipoInsumo(String id, String nombre, String porcentaje_rentabilidad, String iva_con_aiu, Usuario usuario);
    
    public String activaInactivaTipoInsumo(String id, Usuario usuario);
    
}


