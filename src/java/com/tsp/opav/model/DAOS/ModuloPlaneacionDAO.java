/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.opav.model.DAOS;

/**
 *
 * @author Ing.William Siado T
 */
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.tsp.opav.model.beans.AccionesEca;
import com.tsp.opav.model.beans.CotizacionSl;
import com.tsp.operation.model.MenuOpcionesModulos;
import com.tsp.operation.model.beans.BeanGeneral;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.beans.Cliente;
import com.tsp.opav.model.beans.OfertaElca;

import java.sql.SQLException;
import java.util.ArrayList;

public interface ModuloPlaneacionDAO {


    public String cargar_conceptos();

    public String guardar_Concepto(String nombre, String descripcion, String factura, Usuario usuario);

    public String actualizar_Concepto(String id, String nombre, String descripcion, String factura, Usuario usuario);

    public String anular_Concepto(String id, Usuario usuario);

    public String cargar_Riesgos();

    public String guardar_Riesgos(String nombre, String descripcion, Usuario usuario);

    public String actualizar_Riesgos(String id, String nombre, String descripcion, Usuario usuario);

    public String anular_Riesgos(String id, Usuario usuario);

    public String cargar_Indicadores_Gestion();

    public String actualizar_Indicadores_Gestion(String id, String nombre, String descripcion, Usuario usuario);

    public String anular_Indicadores_Gestion(String id, Usuario usuario);

    public String insertar_Indicadores_Gestion(String nombre, String descripcion, Usuario usuario);

    public String cargar_Unidades_Medida();

    public String guardar_Carga_Laboral(String idActividad,  String nivel, String rang_ini, String rang_final, String unidad_medida, Usuario usuario);

    public String cargar_Carga_Laboral(String idActividad);

    public String actualizar_Carga_Laboral(String id, String idActividad, String nivel, String rang_ini, String rang_final, String unidad_medida, Usuario usuario);

    public String anular_Carga_Laboral(String id, Usuario usuario);

    public String cargar_Cargos();

    public String cargar_Cargas_Laborales(String id);

    public String cargar_Cargas_Laborales_Asociado(String id);

    public String asociar_Carga_Laboral(int id, String listado, Usuario usuario);
    
    public String cargar_tareas(String id_solicitud);
    
    public String cargar_asignacion(String id_solicitud);
    
    public boolean existeHitos(String empresa, String nombre, String idaccion);

    public String cargar_hitos(String idaccion);

    public String guardar_hitos(String nombre, String fecha, Usuario usuario, String idaccion);

    public String actualizar_hitos(String id, String nombre, String fecha, Usuario usuario, String idaccion);

    public String anular_hitos(String id, Usuario usuario);
    
    public String cargar_flujo_caja(String idaccion);
    
    public String guardar_flujo(String periodo, String valor, String concepto, String descripcion, Usuario usuario, String idaccion);
    
    public String actualizar_Flujo(String id, String periodo, String valor, String concepto, String descripcion, Usuario usuario, String idaccion);
    
    public String anular_flujo(String id, Usuario usuario);
    
    public String cargar_indicador();
    
    public String guardar_indicador(String nombre, String descripcion, Usuario usuario);
    
    public String actualizar_indicador(String id, String nombre, String descripcion, Usuario usuario);

    public String anular_indicador(String id, Usuario usuario);
    
    ///////////////////////////////////////////////
    
    public String cargar_gestion(String idaccion);
    
    public String guardar_gestion(String indicador, String valor_min, String valor_max, String alerta_min, String alerta_max, Usuario usuario, String idaccion);
    
    public String actualizar_gestion(String id, String indicador, String valor_min, String valor_max, String alerta_min, String alerta_max, Usuario usuario, String idaccion);
    
    public String anular_gestion(String id, Usuario usuario);
    
    /////////////////////////////////////////////////
    
    public String cargar_proyeccion(String idaccion);
    
    ///////////////////////////////////////////////
    
    public String cargar_equipos(String idaccion);
    
    public boolean existe_equipo(String idaccion);
    
    public String cargar_equipos_bd(String idaccion);
    
    public abstract JsonObject ingresar_equipos(JsonObject info);
    
    ////////////////////////////////////////////////
    
    public String cargar_personal(String idaccion);
    
    public boolean existe_personal(String idaccion);
    
    public String cargar_personal_bd(String idaccion);
    
    public abstract JsonObject ingresar_personal(JsonObject info);
    
    //////////////////////////////////////////////////
    
    public String cargar_material(String idaccion);
    
    public boolean existe_material(String idaccion);
    
    public String cargar_material_bd(String idaccion);
    
    public abstract JsonObject ingresar_material(JsonObject info);
    
    public JsonObject cargar_combo_categoria(JsonObject info);
    
    public boolean existeTipoCategoria(String empresa, String nombre, String id);
    
    public String guardarTipoCategoria(String empresa, String nombre, String puntaje, String login);
    
    public String cargarCategoria(String empresa, String status, String categoria);
    
    public boolean existeIndicador(String empresa, String nombre, String id);
    
    public String guardarIndicador(String empresa, String nombre, String descripcion, String puntaje, String categoria, String login);

    //////////////////////////////////////
    public String cargar_riesgo_ci(String idaccion);
    
    public String guardar_riesgo_ci(String categoria, String impacto, String probabilidad, String descripcion, String puntaje, Usuario usuario, String idaccion);
    
    public String cargarImpacto(String empresa, String status, String categoria);
    
    
    public String actualizar_riesgo_ci(String id, String categoria, String impacto, String probabilidad, String descripcion, String puntaje, Usuario usuario, String idaccion);
    
    public String anular_riesgo_ci(String id, Usuario usuario);
    
    public String actualizar_impacto(String id, String nombre, String descripcion, String puntaje, Usuario usuario);
    
    public String anular_impacto(String id, Usuario usuario);
    /////////////////////////////////
    public String insertar_Actividad(String nombre, String descripcion, Usuario usuario);

    public String actualizar_Actividad(String id, String nombre, String descripcion, Usuario usuario);

    public String anular_Actividad(String id, Usuario usuario);

    public String cargar_Actividades();

    public JsonObject ingresar_Peso_Actividad_Aso_Cargo(JsonObject rows);

    public String cargar_Matris_Interesados(String id_solicitud);

    public String insertar_Matris_Interesados(String id_solicitud, String nombre, String empresa, String tipo_cliente, String telefono, String correo_electronico, String rol, Usuario usuario);

    public String actualizar_Matris_Interesados(String id, String nombre, String empresa, String tipo_cliente, String telefono, String correo_electronico, String rol, Usuario usuario);

    public String anular_Item_Matris_Interesados(String id, Usuario usuario);

    public String cargar_Plan_Contatos(String id_solicitud);

    public String actualizar_Plan_Contatos(String id, String tipo_documento, String medio_transmision, String responsable, String tipo_informacion, String medio_almacenamiento, String periocidad, String destinatario_principal, String destinatario_secundario, Usuario usuario);

    public String anular_Item_Plan_Contatos(String id, Usuario usuario);

    public String insertar_Plan_Contatos(String id_solicitud, String tipo_documento, String medio_transmision, String responsable, String tipo_informacion, String medio_almacenamiento, String periocidad, String destinatario_principal, String destinatario_secundario, Usuario usuario);

    public String insertar_Aspectos_Proyecto(String id_solicitud, String descripcion_proyecto, String requisitos_calidad_normas, String alcances_contrato, String Penalizaciones, String fecha_inicio_cliente, String fecha_fin_cliente, String fecha_inicio_real, String fecha_fin_real, String necesidades_dise�o, Usuario usuario);

    public String cargar_Aspectos_Proyecto(String id_solicitud);

    public String actualizar_Aspectos_Proyecto(String id, String descripcion_proyecto, String requisitos_calidad_normas, String alcances_contrato, String Penalizaciones, String fecha_inicio_cliente, String fecha_fin_cliente, String fecha_inicio_real, String fecha_fin_real, String necesidades_dise�o, Usuario usuario);

    public String listar_Cargas_Laborales(String id_solicitud);

    public String listar_Infomacion_Actividades();

    public String listarCargasLaborales_Detalle(String id_ind_carga_laboral, String id_cargo);

    public String guardar_Asignacion_Puntaje(String id_solicitud, String id_cargo, JsonObject rows, Usuario usuario);

    public String editar_Asignacion_Puntaje(String id, JsonObject rows, Usuario usuario);

    public String anular_Asignacion_Puntaje(String id, Usuario usuario);

    public String cargar_Unidades_Medida_Opav();

}
