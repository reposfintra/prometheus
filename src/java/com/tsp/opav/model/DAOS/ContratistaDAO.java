/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * ContratistaDAO.java :
 * Copyright 2010 Rhonalf Martinez Villa <rhonaldomaster@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.tsp.opav.model.DAOS;
import java.sql.*;
import java.util.*;
import com.tsp.opav.model.beans.*;
/**
 *
 * @author rhonalf
 */
public class ContratistaDAO extends MainDAO {

    public ContratistaDAO(){
        super("ContratistaDAO.xml");
    }
    public ContratistaDAO(String dataBaseName){
        super("ContratistaDAO.xml", dataBaseName);
    }

    /**
     * Lista los tipos de impuestos
     * @param tipo Se escoge con los numeros:<br>1:reteica<br>2:retefuente
     * @return listado con los registros obtenidos
     * @throws Exception cuando hay error
     */
    public ArrayList listarImpuestos(int tipo) throws Exception{
        ArrayList lista = null;
        String timp = "";
        PreparedStatement ps = null;
        ResultSet rs = null;
        Connection con = null;
        String query = "LISTAR_IMP";
        String sql="";
        String cod="";
        switch(tipo){
            case 1:
                timp="RETEICA";
            break;
            case 2:
                timp="RETEFUENTE";
            break;
        }
        try {
            lista = new ArrayList();
            con = this.conectarJNDI(query);
            sql = this.obtenerSQL(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, timp);
            rs = ps.executeQuery();
            while(rs.next()){
                cod = "<option value='"+rs.getString("referencia")+"'>"+rs.getString("descripcion")+" "+rs.getString("table_code")+"</option>";
                lista.add(cod);
            }
            rs.close();
            ps.close();
            this.desconectar(con);
        }
        catch (Exception e) {
            throw new Exception("Error en contratistadao.java en la funcion listarImpuestos: "+e.toString());
        }
        finally{
            if (rs != null){ try{ rs.close(); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps != null){ try{ ps.close(); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (!con.isClosed()){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;
    }

    /**
     * Lista las actividades
     * @return ArrayList con el codigo html a escribir
     * @throws Exception cuando hay error
     */
    public ArrayList listaActividades() throws Exception{
        ArrayList lista = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        Connection con = null;
        String query = "LIST_ACT";
        String sql="";
        String cod="";
        try {
            lista = new ArrayList();
            con = this.conectarJNDI(query);
            sql = this.obtenerSQL(query);
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while(rs.next()){
                cod = "<option value='"+rs.getString("actividad")+"'>"+rs.getString("actividad")+"</option>";
                lista.add(cod);
            }
            rs.close();
            ps.close();
            this.desconectar(con);
        }
        catch (Exception e) {
            throw new Exception("Error en contratistadao.java en la funcion listaActividades: "+e.toString());
        }
        finally{
            if (rs != null){ try{ rs.close(); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps != null){ try{ ps.close(); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (!con.isClosed()){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;
    }
    /**
     * Busca datos de contratistas
     * @param filtro nombre de la columna de la tabla app_contratistas por la cual se quiere filtrar
     * @param param el dato a buscar
     * @return lista con objetos Contratista que se ajustaron al criterio de busqueda
     * @throws Exception cuando hay un error
     */
    public ArrayList buscar(String filtro,String param) throws Exception{
        ArrayList lista = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        Connection con = null;
        String query = "BUSCAR_PARAM";
        String sql="";
        Contratista cont = null;
        try {
            lista = new ArrayList();
            con = this.conectarJNDI(query);
            sql = this.obtenerSQL(query);
            sql = sql.replaceAll("#param", param);
            sql = sql.replaceAll("#filtro", filtro);
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while(rs.next()){
                cont = new Contratista();
                cont.setActividad(rs.getString("actividad")!=null?rs.getString("actividad"):"");
                cont.setAutoretenedor(rs.getString("autoretenedor")!=null?rs.getString("autoretenedor"):"");
                cont.setClave(rs.getString("clave")!=null?rs.getString("clave"):"");
                cont.setCodigo_reteica(rs.getString("codigo_reteica"));
                cont.setDomicilio_comercial(rs.getString("domicilio_comercial")!=null?rs.getString("domicilio_comercial"):"");
                cont.setEmail(rs.getString("email")!=null?rs.getString("email"):"");
                cont.setGran_contribuyente(rs.getString("gran_contribuyente")!=null?rs.getString("gran_contribuyente"):"");
                cont.setId_contratista(rs.getString("id_contratista")!=null?rs.getString("id_contratista"):"");
                cont.setNit(rs.getString("nit")!=null?rs.getString("nit"):"");
                cont.setNombre_contratista(rs.getString("descripcion")!=null?rs.getString("descripcion"):"");
                cont.setRegimen(rs.getString("regimen")!=null?rs.getString("regimen"):"");
                cont.setSecuencia_prefactura(rs.getInt("secuencia_prefactura"));
                cont.setResponsable(rs.getString("responsable")!=null?rs.getString("responsable"):"");
                cont.setDireccion(rs.getString("direccion")!=null?rs.getString("direccion"):"");
                cont.setTelefono(rs.getString("telefono")!=null?rs.getString("telefono"):"");
                cont.setCelular(rs.getString("celular")!=null?rs.getString("celular"):"");
                lista.add(cont);
            }
            rs.close();
            ps.close();
            this.desconectar(con);
        }
        catch (Exception e) {
            throw new Exception("Error en contratistadao.java en la funcion buscar: "+e.toString());
        }
        finally{
            if (rs != null){ try{ rs.close(); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps != null){ try{ ps.close(); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (!con.isClosed()){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;
    }

    /**
     * Inserta un nuevo registro en la tabla de contratistas
     * @param contratista Objeto de tipo Contratista con los datos
     * @throws Exception Cuando hay un error
     */
    public void insertar(Contratista contratista) throws Exception{
        PreparedStatement ps = null;
        Connection con = null;
        String query = "INSERT_REG";
        String sql="";
        try {
            con = this.conectarJNDI(query);
            sql = this.obtenerSQL(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, contratista.getNombre_contratista());
            ps.setInt(2, contratista.getSecuencia_prefactura());
            ps.setString(3, contratista.getNit());
            ps.setString(4, contratista.getEmail());
            ps.setString(5, contratista.getClave());
            ps.setString(6, contratista.getCodigo_reteica());
            ps.setString(7, contratista.getGran_contribuyente());
            ps.setString(8, contratista.getDomicilio_comercial());
            ps.setString(9, contratista.getActividad());
            ps.setString(10, contratista.getRegimen());
            ps.setString(11, contratista.getAutoretenedor());
            ps.setString(12, contratista.getResponsable());
            ps.setString(13, contratista.getDireccion());
            ps.setString(14, contratista.getTelefono());
            ps.setString(15, contratista.getCelular());
            int krow = ps.executeUpdate();
            if(krow>0){
                System.out.println("Se ha ingresado el registro en la tabla app_contratistas");
            }
            else{
                System.out.println("No se ha ingresado el registro en la tabla app_contratistas");
            }
            ps.close();
            this.desconectar(con);
        }
        catch (Exception e) {
            throw new Exception("Error en contratistadao.java en la funcion insertar: "+e.toString());
        }
        finally {
            if (ps != null){ try{ ps.close(); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (!con.isClosed()){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }

    /**
     * Actualiza un registro de la tabla de contratistas
     * @param contratista Objeto de tipo Contratista con los datos
     * @throws Exception Cuando hay un error
     */
    public void actualizar(Contratista contratista) throws Exception{
        PreparedStatement ps = null;
        Connection con = null;
        String query = "UPDATE_REG";
        String sql="";
        try {
            con = this.conectarJNDI(query);
            sql = this.obtenerSQL(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, contratista.getNombre_contratista());
            ps.setInt(2, contratista.getSecuencia_prefactura());
            ps.setString(3, contratista.getNit());
            ps.setString(4, contratista.getEmail());
            ps.setString(5, contratista.getClave());
            ps.setString(6, contratista.getCodigo_reteica());
            ps.setString(7, contratista.getGran_contribuyente());
            ps.setString(8, contratista.getDomicilio_comercial());
            ps.setString(9, contratista.getActividad());
            ps.setString(10, contratista.getRegimen());
            ps.setString(11, contratista.getAutoretenedor());
            ps.setString(12, contratista.getResponsable());
            ps.setString(13, contratista.getDireccion());
            ps.setString(14, contratista.getTelefono());
            ps.setString(15, contratista.getCelular());
            ps.setString(16, contratista.getId_contratista());
            int krow = ps.executeUpdate();
            if(krow>0){
                System.out.println("Se ha actualizado el registro en la tabla app_contratistas");
            }
            else{
                System.out.println("No se ha actualizado el registro en la tabla app_contratistas");
            }
            ps.close();
            this.desconectar(con);
        }
        catch (Exception e) {
            throw new Exception("Error en contratistadao.java en la funcion actualizar: "+e.toString());
        }
        finally {
            if (ps != null){ try{ ps.close(); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (!con.isClosed()){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }

    /**
     * Anula un registro de la tabla de contratistas
     * @param codigo El codigo del contratista
     * @throws Exception Cuando hay un error
     */
    public void anular(String codigo) throws Exception{
        PreparedStatement ps = null;
        Connection con = null;
        String query = "ANUL_REG";
        String sql="";
        try {
            con = this.conectarJNDI(query);
            sql = this.obtenerSQL(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, codigo);
            int krow = ps.executeUpdate();
            if(krow>0){
                System.out.println("Se ha anulado el registro en la tabla app_contratistas");
            }
            else{
                System.out.println("No se ha anulado el registro en la tabla app_contratistas");
            }
            ps.close();
            this.desconectar(con);
        }
        catch (Exception e) {
            throw new Exception("Error en contratistadao.java en la funcion anular: "+e.toString());
        }
        finally {
            if (ps != null){ try{ ps.close(); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (!con.isClosed()){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }

}