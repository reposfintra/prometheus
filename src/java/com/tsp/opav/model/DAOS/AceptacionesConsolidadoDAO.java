/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.opav.model.DAOS;

/**
 *
 * @author jpacosta
 */
public interface AceptacionesConsolidadoDAO {
    public abstract String buscar (String periodo, String tipos, String estados, String interventores, String responsables);
    public abstract String detallarConsolidado(String periodo, String sector);
}
