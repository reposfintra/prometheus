/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.opav.model.DAOS;

import com.google.gson.JsonObject;
import com.tsp.operation.model.beans.Usuario;
import java.util.ArrayList;

/**
 *
 * @author user
 */
public interface MinutasContratacionDAO {
    
    public String cargarGridSolicitudesPendientes(String fecha_ini, String fecha_fin, String num_solicitud ,String lineaNegocio,String responsable,  String etapaActual, String id_cliente, String nom_proyecto, String foms, String tipo_proyecto);
    
    public String guardarContrato(String num_solicitud,String tipo_contrato,String num_contrato,String vlr_antes_iva,String porc_admon,String valor_admon,String porc_imprevisto,String valor_imprevisto,String porc_utilidad,String valor_utilidad,String porc_aiu,
            String valor_aiu,String porc_iva,String valor_iva,String valor_total,String porc_anticipo, String nit_empresa, String cedula_rep_empresa, Usuario usuario, JsonObject garantias, JsonObject garantias_extra, JsonObject infoProveedor, JsonObject infoFormaPago);
    
    public String getInfoSolicitud(String num_solicitud);
    
    public ArrayList searchNombresArchivos(String directorioArchivos, String numsolicitud);
    
    public boolean almacenarArchivoEnCarpetaUsuario(String numsolicitud,String rutaOrigen, String rutaDestino,String filename);
    
    public boolean eliminarArchivo(String directorioArchivos, String numreq, String nomarchivo);
    
    public String cargarFacturasParciales(String numsolicitud);
    
    public String cargarGarantias(String numsolicitud, String extracontractual, String otro_si, String secuencia);
    
    public String listarDiasPago();
    
    public String listarPolizas();
    
    public String cargarConfigDocs();     
    
    public String guardarConfigDocs(String tipo_doc, String content, Usuario usuario);
    
    public String cargarEquivalencias();
    
    public String listarClasificacionPersona();
    
    public String generarCartaAceptacion(String num_solicitud, String rutaOrigen, String rutaDestino, String usuario);
    
    public String generarContrato(String num_solicitud, String rutaOrigen, String rutaDestino, String usuario);
    
    public String listarTipoDocumentos(String mostrarTodos);
    
    public String guardarTipoDocumento(String nombre, String descripcion, Usuario usuario);
     
    public String actualizarTipoDocumento(String id, String nombre, String descripcion, Usuario usuario);
    
    public String activaInactivaTipoDocumento(String id, Usuario usuario);
    
    public String crearDocsContrato(JsonObject contratos, Usuario usuario);
    
    public boolean existeContratoDoc(String num_contrato, String id_tipo_doc);
    
    public String getInfoContratoDocs(String num_contrato, String tipo_doc);
    
    public String actualizarContratoDocs(String tipo_doc, String num_contrato, String content, Usuario usuario);
    
    public String cargarGridAsignacionBroker(String fecha_ini, String fecha_fin, String num_solicitud, String nombre_cliente, String lineaNegocio, String responsable,  String etapaActual, String id_cliente, String nom_proyecto, String foms, String tipo_proyecto);
    
    public String listarBroker();
    
    public String insertarRelBrokerMinuta(String num_contrato, String id_broker, String id_beneficiario, Usuario usuario);
     
    public String actualizarRelBrokerMinuta(String num_contrato, String id_broker, String id_beneficiario, Usuario usuario);
    
    public String cargarGridSolicitudesBroker(String fecha_ini, String fecha_fin, String num_solicitud, String nombre_cliente, String lineaNegocio, String responsable, Usuario usuario);
  
    public String listarAseguradoras();
    
    public String cargarCotizacionAseguradora(String num_solicitud, String id_aseguradora, String id_beneficiario, String secuencia);
    
    public String guardarCotizacionAseguradora(String num_contrato,String id_aseguradora, String id_beneficiario, String secuencia, JsonObject cotizaciones, String otros_gastos, String porc_iva, String valor_iva, Usuario usuario);
    
    public String cargarGridTotalesAseguradora(String num_contrato, String cotizado_broker, Usuario usuario);
    
    public String cargarDetalleCotizacionAseguradora(String num_contrato, String id_aseguradora, String id_beneficiario, String secuencia);
    
    public String enviarCotizacionBroker(String num_contrato, Usuario usuario);
    
    public String seleccionarCotizacionBroker(String num_contrato, String id_aseguradora, String id_beneficiario, String secuencia, Usuario usuario);
    
    public String anularGarantiaMinuta(String id, Usuario usuario);
    
    public String getInfoOtrosCostosAseguradora(String num_solicitud, String id_aseguradora, String id_beneficiario, String secuencia);
    
    public String generarCxPAseguradora(String num_factura, String num_contrato, String[] x, String valor_gastos, String concepto_iva, String usuario);
    
    public String getInfoAseguradoraAceptada(String num_contrato);
    
    public String cargarCotizacionPendienteCxPAseguradora(String num_contrato, String id_aseguradora, String id_beneficiario, String secuencia);    
    
    public String listarBeneficiariosPoliza(String excluirId);
    
    public String listarCausalesPoliza();
    
    public String cargarCboBeneficiarioCxP(String num_contrato);
    
    public String listarBeneficiariosContrato(String num_contrato);
    
    public boolean existeBrokerRelacionado(String num_contrato, String id_beneficiario);
    
    public String listarBeneficiariosAsignadosBroker(String num_contrato, Usuario usuario);
    
    public boolean cotizacionPendienteBroker(String num_contrato, Usuario usuario);
    
    public String cargarOtrosSi(String num_contrato);
    
    public String insertarGarantiaOtroSi(String num_contrato, Usuario usuario);
    
    public String guardarOtroSi(String num_contrato, Usuario usuario, JsonObject garantias_otro_si);
    
    public String anularOtroSi(String num_contrato, String secuencia, Usuario usuario);
    
    public String cargarCboOtrosSiBeneficiario(String num_contrato, String id_beneficiario);
    
    public String cargarCboOtrosSiCxP(String num_contrato);
    
}
