/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.opav.model.DAOS;

/**
 *
 * @author user
 */
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.tsp.operation.model.beans.Usuario;

import java.sql.SQLException;
import java.util.ArrayList;

public interface ProcesosFomsPadreHijoDAO {

    public String cargarInfoSolicitudes(String solicitud, String foms);

    public String cargarFomsDispibles(String solicitud);

    public String cargarFomsRelacionados(String solicitud);

    public String asociarFoms(String solicitud, String lista, Usuario usuario);

    public String desasociarFoms(String solicitud, String lista, Usuario usuario);
}
