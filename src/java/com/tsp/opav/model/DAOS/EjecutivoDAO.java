/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * EjecutivoDAO.java : clase manejadora de acceso a los datos concernientes a la gestion de ejecutivos
 * Copyright 2010 Rhonalf Martinez Villa <rhonaldomaster@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.tsp.opav.model.DAOS;
import java.sql.*;
import com.tsp.opav.model.beans.*;
import java.util.*;
/**
 *
 * @author rhonalf
 */
public class EjecutivoDAO extends MainDAO {

    public EjecutivoDAO(){
        super("EjecutivoDAO.xml");
    }
    public EjecutivoDAO(String dataBaseName){
        super("EjecutivoDAO.xml", dataBaseName);
    }

    /**
     * Busca los datos de un ejecutivo identificado por un id especifico
     * @param id el id del ejecutivo
     * @return Objeto Ejecutivo con los datos
     * @throws Exception cuando ocurra algun error
     */
    public Ejecutivo getDatosEjec(String id) throws Exception{
        Ejecutivo ejec = null;
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_SEARCH_UNIQ";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con = this.conectarJNDI(query);
            st = con.prepareStatement(sql);
            st.setString(1, id);
            rs = st.executeQuery();
            if(rs.next()){
                ejec = new Ejecutivo();
                ejec.setCargo(rs.getString("cargo"));
                ejec.setCiudad(rs.getString("ciudad"));
                ejec.setDepartamento(rs.getString("depto"));
                ejec.setDireccion(rs.getString("direccion"));
                ejec.setEmail(rs.getString("correo"));
                ejec.setId(rs.getString("id_ejecutivo"));
                ejec.setMercado(rs.getString("mercado"));
                ejec.setNit(rs.getString("nit"));
                ejec.setNombre(rs.getString("nombre"));
                ejec.setTelefono(rs.getString("tel1"));
            }
            rs.close();
            st.close();
            this.desconectar(con);
        }
        catch (Exception e) {
            System.out.println("Ha ocurrido un error en EjecutivoDAO en el metodo getDatosEjec : "+e.toString()+" : "+e.getMessage());
            throw new Exception("Ha ocurrido un error en EjecutivoDAO en el metodo getDatosEjec : "+e.toString()+" : "+e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(!con.isClosed()) this.desconectar(con);
        }
        return ejec;
    }

    /**
     * Busca los datos de los ejecutivos de acuerdo a un parametro especifico
     * @param param El nombre del atributo a buscar en la tabla
     * @param cadena El valor a buscar
     * @return Arraylist de objetos Ejecutivo
     * @throws Exception cuando ocurre un error
     */
    public ArrayList<Ejecutivo> buscarEjecutivos(String param,String cadena) throws Exception{
        ArrayList<Ejecutivo> ejecs = null;
        Ejecutivo ejec = null;
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_SEARCH_PARAM";
        String sql = "";
        try {
            ejecs = new ArrayList<Ejecutivo>();
            sql = this.obtenerSQL(query);
            sql = sql.replaceAll("#param", param);
            con = this.conectarJNDI(query);
            st = con.prepareStatement(sql);
            st.setString(1, "%"+cadena+"%");
            rs = st.executeQuery();
            while(rs.next()){
                ejec = new Ejecutivo();
                ejec.setCargo(rs.getString("cargo"));
                ejec.setCiudad(rs.getString("ciudad"));
                ejec.setDepartamento(rs.getString("depto"));
                ejec.setDireccion(rs.getString("direccion"));
                ejec.setEmail(rs.getString("correo"));
                ejec.setId(rs.getString("id_ejecutivo"));
                ejec.setMercado(rs.getString("mercado"));
                ejec.setNit(rs.getString("nit"));
                ejec.setNombre(rs.getString("nombre"));
                ejec.setTelefono(rs.getString("tel1"));
                ejecs.add(ejec);
            }
            rs.close();
            st.close();
            this.desconectar(con);
        }
        catch (Exception e) {
            System.out.println("Ha ocurrido un error en EjecutivoDAO en el metodo buscarEjecutivos : "+e.toString()+" : "+e.getMessage());
            throw new Exception("Ha ocurrido un error en EjecutivoDAO en el metodo buscarEjecutivos : "+e.toString()+" : "+e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(!con.isClosed()) this.desconectar(con);
        }
        return ejecs;
    }

    /**
     * Inserta un ejecutivo al sistema
     * @param ejec Objeto Ejecutivo con los datos
     * @param user Usuario que hace la insercion
     * @throws Exception Cuando ocurre un error
     */
    public void insertarEjecutivo(Ejecutivo ejec,String user) throws Exception{
        Connection con = null;
        PreparedStatement st = null;
        String query = "SQL_INSERT";
        String sql = "";
        int ok=0;
        try {
            sql = this.obtenerSQL(query);
            con = this.conectarJNDI(query);
            st = con.prepareStatement(sql);
            st.setString(1, ejec.getNit());
            st.setString(2, ejec.getNombre());
            st.setString(3, ejec.getDepartamento());
            st.setString(4, ejec.getCiudad());
            st.setString(5, ejec.getDireccion());
            st.setString(6, ejec.getTelefono());
            st.setString(7, ejec.getEmail());
            st.setString(8, user);
            st.setString(9, ejec.getCargo());
            st.setString(10, ejec.getMercado());
            ok = st.executeUpdate();
            if(ok>0){
                System.out.println("Se ha insertado el ejecutivo correctamente");
            }
            else{
                System.out.println("Ooops ... ha ocurrido algo y no se inserto el ejecutivo");
            }
            st.close();
            this.desconectar(con);
        }
        catch (Exception e) {
            System.out.println("Ha ocurrido un error en EjecutivoDAO en el metodo insertarEjecutivo : "+e.toString()+" : "+e.getMessage());
            throw new Exception("Ha ocurrido un error en EjecutivoDAO en el metodo insertarEjecutivo : "+e.toString()+" : "+e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(!con.isClosed()) this.desconectar(con);
        }
    }

    /**
     * Elimina un registro de la tabla ejecutivos
     * @param id El id del ejecutivo a eliminar
     * @param user El usuario que elimina
     * @throws Exception Cuando hay un error
     */
    public void eliminarEjecutivo(String id,String user) throws Exception{
        Connection con = null;
        PreparedStatement st = null;
        String query = "SQL_ANULAR";
        String sql = "";
        int ok=0;
        try {
            sql = this.obtenerSQL(query);
            con = this.conectarJNDI(query);
            st = con.prepareStatement(sql);
            st.setString(1, user);
            st.setString(2, id);
            ok = st.executeUpdate();
            if(ok>0){
                System.out.println("Se ha anulado el ejecutivo correctamente");
            }
            else{
                System.out.println("Ooops ... ha ocurrido algo y no se anulo el ejecutivo");
            }
            st.close();
            this.desconectar(con);
        }
        catch (Exception e) {
            System.out.println("Ha ocurrido un error en EjecutivoDAO en el metodo eliminarEjecutivo : "+e.toString()+" : "+e.getMessage());
            throw new Exception("Ha ocurrido un error en EjecutivoDAO en el metodo eliminarEjecutivo : "+e.toString()+" : "+e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(!con.isClosed()) this.desconectar(con);
        }
    }

    /**
     * Genera un listado de los cargos que estan en la columna cargo de la tabla ejecutivos
     * @return ArrayList de String con los nombres de los cargos
     * @throws Exception Cuando hay un error
     */
    public ArrayList listadoCargos() throws Exception{
        ArrayList lista = null;
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_ANULAR";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con = this.conectarJNDI(query);
            st = con.prepareStatement(sql);
            rs = st.executeQuery();
            while(rs.next()){
                lista.add(rs.getString(1));
            }
            rs.close();
            st.close();
            this.desconectar(con);
        }
        catch (Exception e) {
            System.out.println("Ha ocurrido un error en EjecutivoDAO en el metodo listadoCargos : "+e.toString()+" : "+e.getMessage());
            throw new Exception("Ha ocurrido un error en EjecutivoDAO en el metodo listadoCargos : "+e.toString()+" : "+e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(!con.isClosed()) this.desconectar(con);
        }
        return lista;
    }

    /**
     * Actualiza los datos de un ejecutivo
     * @param ejec Objeto con los datos
     * @param user Usuario que actualiza
     * @param id Codigo del ejecutivo
     * @throws Exception Cuando hay un error
     */
    public void actualizarEjecutivo(Ejecutivo ejec,String user,String id) throws Exception{
        Connection con = null;
        PreparedStatement st = null;
        String query = "SQL_UPDATE_EJEC";
        String sql = "";
        int ok=0;
        try {
            sql = this.obtenerSQL(query);
            con = this.conectarJNDI(query);
            st = con.prepareStatement(sql);
            st.setString(1, ejec.getNit());
            st.setString(2, ejec.getNombre());
            st.setString(3, ejec.getDepartamento());
            st.setString(4, ejec.getCiudad());
            st.setString(5, ejec.getDireccion());
            st.setString(6, ejec.getTelefono());
            st.setString(7, ejec.getEmail());
            st.setString(8, user);
            st.setString(9, ejec.getCargo());
            st.setString(10, ejec.getMercado());
            st.setString(11, id);
            ok = st.executeUpdate();
            if(ok>0){
                System.out.println("Se ha actualizado el ejecutivo correctamente");
            }
            else{
                System.out.println("Ooops ... ha ocurrido algo y no se inserto el ejecutivo");
            }
            st.close();
            this.desconectar(con);
        }
        catch (Exception e) {
            System.out.println("Ha ocurrido un error en EjecutivoDAO en el metodo actualizarEjecutivo : "+e.toString()+" : "+e.getMessage());
            throw new Exception("Ha ocurrido un error en EjecutivoDAO en el metodo actualizarEjecutivo : "+e.toString()+" : "+e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(!con.isClosed()) this.desconectar(con);
        }
    }

}