package com.tsp.opav.model.DAOS;

import com.tsp.opav.model.beans.Actividades;
import com.tsp.opav.model.beans.NegocioApplus;
import com.tsp.opav.model.beans.OfertaSeguimiento;
import com.tsp.operation.model.beans.StringStatement;
import com.tsp.operation.model.beans.StringStatement2;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * SeguimientoEjecucionDAO.java<br/>
 * Clase DAO para las tablas de seguimiento de la ejecucion <br/>
 * 4/06/2010
 * @author darrieta-GEOTECH
 * @version 1.0
 */
public class SeguimientoEjecucionDAO  extends MainDAO {

    public SeguimientoEjecucionDAO() {
        super("SeguimientoEjecucionDAO.xml");
    }
    public SeguimientoEjecucionDAO(String dataBaseName) {
        super("SeguimientoEjecucionDAO.xml", dataBaseName);
    }

    /**
     * Obtiene la informacion de la accion y solicitud
     * @param idSolicitud identificador de la solicitud
     * @param idAccion identificador de la accion
     * @return bean NegocioApplus con los datos consultados
     * @throws SQLException
     */
    public NegocioApplus obtenerInfoAccion(String idSolicitud, String idAccion) throws SQLException{
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query = "SQL_GET_DATOS_ACCION";
        NegocioApplus n = null;

        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setString(1, idSolicitud);
                st.setString(2, idAccion);
                rs = st.executeQuery();
                if (rs.next()) {
                    n = new NegocioApplus();
                    n.setIdAccion(rs.getString("id_accion"));
                    n.setIdSolicitud(rs.getString("id_solicitud"));
                    n.setNumOs(rs.getString("num_os"));
                    n.setAcciones(rs.getString("desc_accion"));
                    n.setNombreCliente(rs.getString("nomcliente"));
                    n.setNombreContratista(rs.getString("nomcontratista"));
                    n.setTipoTrabajo(rs.getString("tipo_trabajo"));
                    n.setFecha(rs.getString("fecha_inicial_oferta"));
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE obtenerInfoAccion[SeguimientoEjecucionDAO] \n " + e.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return n;
    }


    public NegocioApplus obtenerInfoSolicitud(String idSolicitud) throws SQLException{
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query = "SQL_GET_DATOS_SOLICITUD";
        NegocioApplus n = null;

        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setString(1, idSolicitud);
                rs = st.executeQuery();
                if (rs.next()) {
                    n = new NegocioApplus();
                    n.setIdSolicitud(rs.getString("id_solicitud"));
                    n.setNumOs(rs.getString("num_os"));
                    n.setNombreCliente(rs.getString("nomcliente"));
                    n.setFecha((rs.getString("fecha_inicial").startsWith("0099"))?"":rs.getString("fecha_inicial"));
                    n.setFRecepcion(rs.getString("fecha_final"));
                    n.setConsecutivo_oferta(rs.getString("consecutivo_oferta"));
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE obtenerInfoSolicitud[SeguimientoEjecucionDAO] \n " + e.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return n;
    }

    /**
     * Obtiene la lista de acciones para un tipo de trabajo
     * @param tipoTrabajo tipo de trabajo de la accion
     * @return ArrayList con el listado de actividades
     * @throws SQLException
     */
    public ArrayList<Actividades> obtenerListaActividadesTrabajo(String tipoTrabajo) throws SQLException{
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query = "SQL_LISTA_ACTIVIDADES_POR_TIPO";
        ArrayList<Actividades> lista = new ArrayList<Actividades>();

        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setString(1, tipoTrabajo);
                rs = st.executeQuery();
                while (rs.next()) {
                    Actividades act = new Actividades();
                    act.setId(rs.getInt("id"));
                    act.setDescripcion(rs.getString("descripcion"));
                    act.setTipo(rs.getString("tipo"));
                    act.setPeso(rs.getInt("peso_predeterminado"));
                    act.setResponsable(rs.getString("responsable_predeterminado"));
                    lista.add(act);
                    act=null;
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE obtenerListaActividadesTrabajo[SeguimientoEjecucionDAO] \n " + e.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return lista;
    }

    /**
     * Inserta una actividad en la tabla accion_programacion
     * @param actividad bean con la informacion a insertar
     * @return query generado para la insercion
     * @throws SQLException
     */
    public String guardarActividadAccion(Actividades actividad) throws SQLException{
        StringStatement2 st = null;
        Connection con = null;
        String query = "SQL_GUARDAR_ACTIVIDAD_ACCION";
        String sql = "";

        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = new StringStatement2(this.obtenerSQL(query));
                st.setString(actividad.getId_accion());
                st.setInt(actividad.getId());
                st.setString(actividad.getResponsable());
                st.setInt(actividad.getPeso());
                st.setString(actividad.getFecha());
                st.setString(actividad.getFechaInicial());
                st.setString(actividad.getFecha());
                st.setString(actividad.getFechaFinal());
                st.setString(actividad.getReg_status());
                st.setString(actividad.getCreation_user());
                st.setString(actividad.getUser_update());
                sql = st.getSql();
            }
        }catch(Exception e){
            throw new SQLException("ERROR DURANTE guardarActividadAccion[SeguimientoEjecucionDAO] \n " + e.getMessage());
        } finally {
            if (st != null) {
                st = null;
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return sql;
    }

    /**
     * Actualiza un registro en la tabla
     * @param actividad bean con la informacion a actualizar
     * @return query generado para la insercion
     * @throws SQLException
     */
    public String actualizarActividadAccion(Actividades actividad) throws SQLException{
        StringStatement st = null;
        Connection con = null;
        String query = "SQL_ACTUALIZAR_ACTIVIDAD_ACCION";
        String sql = "";

        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = new StringStatement(this.obtenerSQL(query));
                st.setString(actividad.getResponsable());
                st.setInt(actividad.getPeso());
                st.setString(actividad.getFecha());
                st.setString(actividad.getFechaInicial());
                st.setString(actividad.getFecha());
                st.setString(actividad.getFechaFinal());
                st.setString(actividad.getReg_status());
                st.setString(actividad.getUser_update());
                st.setString(actividad.getId_accion());
                st.setInt(actividad.getId());
                sql = st.getSql();
            }
        }catch(Exception e){
            throw new SQLException("ERROR DURANTE actualizarActividadAccion[SeguimientoEjecucionDAO] \n " + e.getMessage());
        } finally {
            if (st != null) {
                st = null;
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return sql;
    }

    /**
     * Elimina todas las actividades para una accion
     * @param id_accion identificador para la accion a eliminar
     * @return query generado para la eliminacion
     * @throws SQLException
     */
    public String eliminarActividadesAccion(String id_accion) throws SQLException{
        StringStatement st = null;
        Connection con = null;
        String query = "SQL_ELIMINAR_ACTIVIDADES_ACCION";
        String sql = "";

        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = new StringStatement(this.obtenerSQL(query));
                st.setString(id_accion);
                sql = st.getSql();
            }
        }catch(Exception e){
            throw new SQLException("ERROR DURANTE eliminarActividadAccion[SeguimientoEjecucionDAO] \n " + e.getMessage());
        } finally {
            if (st != null) {
                st = null;
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return sql;
    }

    /**
     * Consulta un registro de la tabla accion_programacion
     * @param id_accion identificador de la accion
     * @param id_actividad identificador de la actividad
     * @return bean Actividades con la informacion consultada
     * @throws SQLException
     */
    public Actividades consultarActividadAccion(String id_accion, int id_actividad) throws SQLException{
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query = "SQL_BUSCAR_ACTIVIDAD_ACCION";
        Actividades actividad = null;

        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setString(1, id_accion);
                st.setInt(2, id_actividad);
                rs = st.executeQuery();
                if (rs.next()) {
                    actividad = new Actividades();
                    actividad.setId(id_actividad);
                    actividad.setId_accion(id_accion);
                    actividad.setResponsable(rs.getString("responsable"));
                    actividad.setFechaInicial(rs.getString("fecha_inicial"));
                    actividad.setFechaFinal(rs.getString("fecha_final"));
                    actividad.setPeso(rs.getInt("peso"));
                    actividad.setReg_status(rs.getString("reg_status"));
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE consultarActividadAccion[SeguimientoEjecucionDAO] \n " + e.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return actividad;
    }

    /**
     * Obtiene todas las actividades de una accion
     * @param id_accion identificador de la accion
     * @return ArrayList con los resultados obtenidos
     * @throws SQLException
     */
    public ArrayList<Actividades> consultarActividadesAccion(String id_accion) throws SQLException{
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query = "SQL_BUSCAR_ACTIVIDADES_ACCION";
        ArrayList<Actividades> lista = new ArrayList<Actividades>();

        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setString(1, id_accion);
                rs = st.executeQuery();
                while (rs.next()) {
                    Actividades actividad = new Actividades();
                    actividad.setId(rs.getInt("id_actividad"));
                    actividad.setTipo(rs.getString("tipo"));
                    actividad.setId_accion(id_accion);
                    actividad.setResponsable(rs.getString("responsable"));
                    actividad.setFechaInicial(rs.getString("fecha_inicial"));
                    actividad.setFechaFinal(rs.getString("fecha_final"));
                    actividad.setPeso(rs.getInt("peso"));
                    actividad.setReg_status(rs.getString("reg_status"));
                    lista.add(actividad);
                    actividad = null;
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE consultarActividadesAccion[SeguimientoEjecucionDAO] \n " + e.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return lista;
    }

    /**
     * Obtiene todas las actividades de una solicitud
     * @param id_solicitud identificador de la solicitud
     * @return ArrayList con los resultados obtenidos
     * @throws SQLException
     */
    public ArrayList<Actividades> consultarActividadesSolicitud(String id_solicitud) throws SQLException{
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query = "SQL_BUSCAR_ACTIVIDADES_SOLICITUD";
        ArrayList<Actividades> lista = new ArrayList<Actividades>();

        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setString(1, id_solicitud);
                st.setString(2, id_solicitud);
                rs = st.executeQuery();
                while (rs.next()) {
                    Actividades actividad = new Actividades();
                    actividad.setId(rs.getInt("id_actividad"));
                    actividad.setDescripcion(rs.getString("desc_actividad"));
                    actividad.setId_accion(rs.getString("id_accion"));
                    actividad.setDescAccion(rs.getString("desc_accion"));
                    actividad.setResponsable(rs.getString("responsable"));
                    actividad.setFechaInicial(rs.getString("fecha_inicial"));
                    actividad.setFechaFinal(rs.getString("fecha_final"));
                    actividad.setPeso(rs.getInt("peso"));
                    actividad.setReg_status(rs.getString("reg_status"));
                    actividad.setPeso_obra(rs.getFloat("peso_obra"));
                    lista.add(actividad);
                    actividad = null;
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE consultarActividadesSolicitud[SeguimientoEjecucionDAO] \n " + e.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return lista;
    }

    /**
     * Obtiene todas las actividades de una solicitud
     * @param id_solicitud identificador de la solicitud
     * @param id_accion identificador de la accion
     * @return ArrayList con los resultados obtenidos
     * @throws SQLException
     */
    public ArrayList<Actividades> consultarActividadesSolicitud(String id_solicitud, String id_accion) throws SQLException{
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query = "SQL_BUSCAR_ACTIVIDADES_SOLICITUD2";
        ArrayList<Actividades> lista = new ArrayList<Actividades>();

        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setString(1, id_solicitud);
                st.setString(2, id_solicitud);
                st.setString(3, id_accion);
                rs = st.executeQuery();
                while (rs.next()) {
                    Actividades actividad = new Actividades();
                    actividad.setId(rs.getInt("id_actividad"));
                    actividad.setDescripcion(rs.getString("desc_actividad"));
                    actividad.setId_accion(rs.getString("id_accion"));
                    actividad.setDescAccion(rs.getString("desc_accion"));
                    actividad.setResponsable(rs.getString("responsable"));
                    actividad.setFechaInicial(rs.getString("fecha_inicial"));
                    actividad.setFechaFinal(rs.getString("fecha_final"));
                    actividad.setPeso(rs.getInt("peso"));
                    actividad.setReg_status(rs.getString("reg_status"));
                    actividad.setPeso_obra(rs.getFloat("peso_obra"));
                    lista.add(actividad);
                    actividad = null;
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE consultarActividadesSolicitud[SeguimientoEjecucionDAO] \n " + e.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return lista;
    }

    /**
     * Busca si una solicitud tiene completa la programacion de todas las actividades para cada accion
     * @param id_solicitud identificador de la solicitud a consultar
     * @return true si la programacion esta completa, false si no esta completa
     * @throws SQLException
     */
    public boolean tieneProgramacion(String id_solicitud) throws SQLException{
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query = "SQL_BUSCAR_ACCIONES_SIN_PROGRAMACION";
        boolean programado = true;

        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setString(1, id_solicitud);
                rs = st.executeQuery();
                if (rs.next()) {
                    programado = false;
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE tieneProgramacion[SeguimientoEjecucionDAO] \n " + e.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return programado;
    }

    /**
     * consulta todas las observaciones registradas al seguimiento de una solicitud
     * @param id_solicitud
     * @param reg_status estado del registro a buscar 'G' guardado o '' confirmado
     * @return ArryList de beans OfertaSeguimiento con los resultados obtenidos
     * @throws SQLException
     */
    public ArrayList<OfertaSeguimiento> consultarObservacionesSolicitud(String id_solicitud, String reg_status) throws SQLException{
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query = "SQL_BUSCAR_OBSERVACIONES_SOLICITUD";
        ArrayList<OfertaSeguimiento> lista = new ArrayList<OfertaSeguimiento>();

        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setString(1, id_solicitud);
                st.setString(2, reg_status);
                rs = st.executeQuery();
                while (rs.next()) {
                    OfertaSeguimiento seguimiento = new OfertaSeguimiento();
                    seguimiento.setFecha(rs.getString("fecha"));
                    seguimiento.setObservaciones(rs.getString("observaciones"));
                    seguimiento.setUsuario(rs.getString("user_update"));
                    lista.add(seguimiento);
                    seguimiento = null;
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE consultarObservacionesSolicitud[SeguimientoEjecucionDAO] \n " + e.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return lista;
    }


    public String eliminarOfertaEjecucion(Actividades actividad) throws SQLException{
        StringStatement st = null;
        Connection con = null;
        String query = "SQL_ELIMINAR_EJECUCION";
        String sql = "";

        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = new StringStatement(this.obtenerSQL(query));
                st.setString(actividad.getId_solicitud());
                st.setString(actividad.getFecha());
                sql = st.getSql();
            }
        }catch(Exception e){
            throw new SQLException("ERROR DURANTE eliminarOfertaEjecucion[SeguimientoEjecucionDAO] \n " + e.getMessage());
        } finally {
            if (st != null) {
                st = null;
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return sql;
    }


    public String eliminarOfertaSeguimiento(OfertaSeguimiento ofertaSeg) throws SQLException{
        StringStatement st = null;
        Connection con = null;
        String query = "SQL_ELIMINAR_SEGUIMIENTO";
        String sql = "";

        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = new StringStatement(this.obtenerSQL(query));
                st.setString(ofertaSeg.getId_solicitud());
                st.setString(ofertaSeg.getFecha());
                sql = st.getSql();
            }
        }catch(Exception e){
            throw new SQLException("ERROR DURANTE eliminarOfertaSeguimiento[SeguimientoEjecucionDAO] \n " + e.getMessage());
        } finally {
            if (st != null) {
                st = null;
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return sql;
    }


    public String guardarOfertaEjecucion(Actividades actividad) throws SQLException{
        StringStatement st = null;
        Connection con = null;
        String query = "SQL_INSERTAR_OFERTA_EJECUCION";
        String sql = "";

        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = new StringStatement(this.obtenerSQL(query));
                st.setString(actividad.getId_solicitud());
                st.setString(actividad.getFecha());
                st.setString(actividad.getId_accion());
                st.setInt(actividad.getId());
                st.setDouble(actividad.getAvance());
                st.setDouble(actividad.getAvance_esperado());
                st.setString(actividad.getReg_status());
                st.setString(actividad.getCreation_user());
                st.setString(actividad.getUser_update());
                st.setString(actividad.getObservaciones());
                sql = st.getSql();
            }
        }catch(Exception e){
            throw new SQLException("ERROR DURANTE guardarOfertaEjecucion[SeguimientoEjecucionDAO] \n " + e.getMessage());
        } finally {
            if (st != null) {
                st = null;
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return sql;
    }

    public String guardarOfertaSeguimiento(OfertaSeguimiento ofertaSeg) throws SQLException{
        StringStatement2 st = null;
        Connection con = null;
        String query = "SQL_INSERTAR_OFERTA_SEGUIMIENTO";
        String sql = "";

        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = new StringStatement2(this.obtenerSQL(query));
                st.setString(ofertaSeg.getId_solicitud());
                st.setString(ofertaSeg.getFecha());
                st.setString(ofertaSeg.getObservaciones());
                st.setDouble(ofertaSeg.getAvance_registrado());
                st.setDouble(ofertaSeg.getAvance_esperado());
                st.setString(ofertaSeg.getReg_status());
                st.setString(ofertaSeg.getCreation_user());
                st.setString(ofertaSeg.getUser_update());
                sql = st.getSql();
            }
        }catch(Exception e){
            throw new SQLException("ERROR DURANTE guardarOfertaSeguimiento[SeguimientoEjecucionDAO] \n " + e.getMessage());
        } finally {
            if (st != null) {
                st = null;
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return sql;
    }

    public ArrayList<Actividades> consultarAvanceSolicitud(String id_solicitud, String fecha) throws SQLException{
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query = "SQL_BUSCAR_AVANCE_ACTIVIDADES";
        ArrayList<Actividades> lista = new ArrayList<Actividades>();

        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setString(1, id_solicitud);
                st.setString(2, fecha);
                st.setString(3, fecha);
                st.setString(4, fecha);
                st.setString(5, id_solicitud);
                st.setString(6, id_solicitud);
                st.setString(7, id_solicitud);
                rs = st.executeQuery();
                while (rs.next()) {
                    Actividades actividad = new Actividades();
                    actividad.setId(rs.getInt("id_actividad"));
                    actividad.setDescripcion(rs.getString("desc_actividad"));
                    actividad.setId_accion(rs.getString("id_accion"));
                    actividad.setDescAccion(rs.getString("desc_accion"));
                    actividad.setResponsable(rs.getString("responsable"));
                    actividad.setPeso_obra(rs.getFloat("peso_obra"));
                    actividad.setDias_actividad(rs.getInt("dias_actividad"));
                    actividad.setDias_actual(rs.getInt("dias_actual"));
                    actividad.setAvance_anterior(rs.getFloat("avance_anterior"));
                    actividad.setFecha(rs.getString("fecha_anterior"));
                    actividad.setAvance(rs.getFloat("avance_actual")==0?rs.getFloat("avance_anterior"):rs.getFloat("avance_actual"));
                    actividad.setObservaciones(rs.getString("observaciones"));
                    actividad.setFechaInicial(rs.getString("fecha_inicial"));
                    actividad.setFechaFinal(rs.getString("fecha_final"));
                    lista.add(actividad);
                    actividad = null;
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE consultarSeguimientoSolicitud[SeguimientoEjecucionDAO] \n " + e.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return lista;
    }

    public String buscarFechaSeguimiento(String idSolicitud, String regStatus) throws SQLException{
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query = "SQL_BUSCAR_FECHA_SEGUIMIENTO";
        String fecha = null;

        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setString(1, idSolicitud);
                st.setString(2, regStatus);
                rs = st.executeQuery();
                if (rs.next()) {
                    fecha = rs.getString("fecha");
                }
            }
        }catch(Exception e){
            throw new SQLException("ERROR DURANTE buscarFechaSeguimientoGuardado[SeguimientoEjecucionDAO] \n " + e.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return fecha;
    }

    public int obtenerNumeroSeguimientos(String idSolicitud) throws SQLException{
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query = "SQL_NUM_SEGUIMIENTOS_SOLICITUD";
        int conteo = 0;

        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setString(1, idSolicitud);
                rs = st.executeQuery();
                if (rs.next()) {
                    conteo = rs.getInt("conteo");
                }
            }
        }catch(Exception e){
            throw new SQLException("ERROR DURANTE obtenerNumeroSeguimientos[SeguimientoEjecucionDAO] \n " + e.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return conteo;
    }


    public ArrayList<Actividades> consultarSeguimientosSolicitud(String id_solicitud) throws SQLException{
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query = "SQL_BUSCAR_SEGUIMIENTOS_ACTIVIDADES";
        ArrayList<Actividades> lista = new ArrayList<Actividades>();

        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setString(1, id_solicitud);
                rs = st.executeQuery();
                while (rs.next()) {
                    Actividades actividad = new Actividades();
                    actividad.setId(rs.getInt("id_actividad"));
                    actividad.setId_accion(rs.getString("id_accion"));
                    actividad.setFecha(rs.getString("fecha"));
                    actividad.setAvance(rs.getFloat("avance"));
                    actividad.setAvance_esperado(rs.getFloat("avance_esperado"));
                    lista.add(actividad);
                    actividad = null;
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE consultarSeguimientosSolicitud[SeguimientoEjecucionDAO] \n " + e.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return lista;
    }

    public ArrayList<Actividades> consultarSeguimientosSolicitud(String id_solicitud,String id_accion) throws SQLException{
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query = "SQL_BUSCAR_SEGUIMIENTOS_ACTIVIDADES_ACCION";
        ArrayList<Actividades> lista = new ArrayList<Actividades>();

        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                String cond = "";
                if(id_accion.equals("-1")){
                    cond="oe.id_solicitud='"+id_solicitud+"'";
                }else{
                    cond="oe.id_accion='"+id_accion+"'";
                }
                st = con.prepareStatement(this.obtenerSQL(query).replaceAll("#FILTRO#", cond));
                rs = st.executeQuery();
                while (rs.next()) {
                    Actividades actividad = new Actividades();
                    actividad.setId(rs.getInt("id_actividad"));
                    actividad.setId_accion(rs.getString("id_accion"));
                    actividad.setFecha(rs.getString("fecha"));
                    actividad.setAvance(rs.getFloat("avance"));
                    actividad.setAvance_esperado(rs.getFloat("avance_esperado"));
                    actividad.setObservaciones(rs.getString("observaciones"));
                    lista.add(actividad);
                    actividad = null;
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE consultarSeguimientosSolicitud[SeguimientoEjecucionDAO] \n " + e.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return lista;
    }


    public ArrayList<OfertaSeguimiento> consultarTotalesSeguimientos(String id_solicitud) throws SQLException{
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query = "SQL_BUSCAR_TOTALES_SEGUIMIENTOS";
        ArrayList<OfertaSeguimiento> lista = new ArrayList<OfertaSeguimiento>();

        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setString(1, id_solicitud);
                rs = st.executeQuery();
                while (rs.next()) {
                    OfertaSeguimiento seguimiento = new OfertaSeguimiento();
                    seguimiento.setFecha(rs.getString("fecha"));
                    seguimiento.setAvance_registrado(rs.getFloat("avance_registrado"));
                    seguimiento.setAvance_esperado(rs.getFloat("avance_esperado"));
                    lista.add(seguimiento);
                    seguimiento = null;
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE consultarTotalesSeguimientos[SeguimientoEjecucionDAO] \n " + e.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return lista;
    }
    
    public ArrayList<OfertaSeguimiento> consultarTotalesSeguimientos2(String idSolicitud) throws SQLException{
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query = "SQL_BUSCAR_TOTALES_SEGUIMIENTOS2";
        ArrayList<OfertaSeguimiento> lista = new ArrayList<OfertaSeguimiento>();

        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setString(1, idSolicitud);
                rs = st.executeQuery();
                while (rs.next()) {
                    OfertaSeguimiento seguimiento = new OfertaSeguimiento();
                    seguimiento.setFecha(rs.getString("fecha"));
                    seguimiento.setAvance_registrado(rs.getFloat("avance_registrado"));
                    seguimiento.setAvance_esperado(rs.getFloat("avance_esperado"));
                    lista.add(seguimiento);
                    seguimiento = null;
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE consultarTotalesSeguimientos[SeguimientoEjecucionDAO] \n " + e.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return lista;
    }

    public String obtenerFechaFinalSolicitud(String idSolicitud) throws SQLException{
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query = "SQL_GET_FECHA_FINAL_SOLICITUD";
        String fechaFinal = "";

        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setString(1, idSolicitud);
                rs = st.executeQuery();
                if (rs.next()) {
                    fechaFinal = rs.getString("fecha_final");
                }
            }
        }catch(Exception e){
            throw new SQLException("ERROR DURANTE obtenerFechaFinalSolicitud[SeguimientoEjecucionDAO] \n " + e.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return fechaFinal;
    }
    
    public String actualizarFechaInicial(String idSolicitud, String fecha) throws SQLException {
        StringStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query = "SQL_ACTUALIZAR_SOLICITUD";
        String fechaFinal = "";

        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = new StringStatement(this.obtenerSQL(query));
                st.setString(fecha);
                st.setString(idSolicitud);
                query = st.getSql();
            }
        }catch(Exception e){
            throw new SQLException("ERROR DURANTE actualizarFechaInicial[SeguimientoEjecucionDAO] \n " + e.getMessage());
        } finally {
            if (st != null) {
                st = null;
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return query;
    }
}