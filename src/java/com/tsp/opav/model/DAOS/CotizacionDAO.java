/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.opav.model.DAOS;
import com.google.gson.JsonObject;
import java.sql.*;
import java.util.*;
import com.tsp.opav.model.beans.*;
import com.tsp.opav.model.beans.Cotizacion;
import com.tsp.operation.model.TransaccionService;
import com.tsp.operation.model.beans.StringStatement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.text.DateFormat;


/**
 *
 * @author Rhonalf
 */
public class CotizacionDAO extends MainDAO {

    public CotizacionDAO() {
        super("CotizacionDAO.xml");
    }
    public CotizacionDAO(String dataBaseName) {
        super("CotizacionDAO.xml", dataBaseName);
    }

    private int contarDigitos(int numero){
        int contador=0;
        while(numero>0){
            numero /=10;
            contador++;
        }
        return contador;
    }

    public String getConsecutivo() throws Exception{
		PreparedStatement st = null;
		ResultSet rs = null;
		Connection con = null;
		String query="GET_COTSER";
		String cantidad = "";
		try{
		    con = this.conectarJNDI(query);
		    String sql = obtenerSQL(query);
		    st = con.prepareStatement(sql);
		    st.setString(1, "COTSER");
		    rs=st.executeQuery();
		    if(rs.next())
		    {   cantidad = rs.getString("codigo");
		    }
		}
		catch(Exception ec){
		    throw new Exception("Error en la consulta contar todas las cotizaciones: "+ec.toString());
		}
		finally{
		    if(st != null){
		        try{
		            st.close();
		        }
		        catch(SQLException e){
		            throw new SQLException("Error al cerrar la conexion " + e.getMessage() );
		        }
		    }
		    this.desconectar(con);
		}

	       /* try{
		    //contar = contarTodas() + 1;
		    contar = contarTodas() + 1;
		    int ndig = this.contarDigitos(contar);
		    switch(ndig){
		        case 0:
		            r="CT0000001";
		        break;
		        case 1:
		            r +="000000"+contar;
		        break;
		        case 2:
		            r +="00000"+contar;
		        break;
		        case 3:
		            r +="0000"+contar;
		        break;
		        case 4:
		            r +="000"+contar;
		        break;
		        case 5:
		            r +="00"+contar;
		        break;
		        case 6:
		            r +="0"+contar;
		        break;
		        default:
		            r+=contar;
		        break;
		    }
		}
		catch(Exception e){
		    System.out.println("Error en generacion de consecutivo: "+e.toString());
		    e.printStackTrace();
		}*/
		return cantidad;
    }

    private int contarTodas() throws Exception{
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query="CONTAR_TODAS_2";
        int cantidad = 0;
        try{
            con = this.conectarJNDI(query);
            String sql = obtenerSQL(query);
            st = con.prepareStatement(sql);
            rs=st.executeQuery();
            while(rs.next()){
                cantidad = rs.getInt("numero");
            }
            return cantidad;
        }
        catch(Exception ec){
            throw new Exception("Error en la consulta contar todas las cotizaciones: "+ec.toString());
        }
        finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("Error al cerrar la conexion " + e.getMessage() );
                }
            }
            this.desconectar(con);
        }
    }

    public void aprobarOrden(String consecutivo) throws Exception{
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query="APROBAR_COT";
        int rowCount = 0;
        try {
            con = this.conectarJNDI(query);
            String sql = obtenerSQL(query);
            st = con.prepareStatement(sql);
            st.setString(1,consecutivo);
            rowCount=st.executeUpdate();
            if(rowCount>0) System.out.println("Cotizacion aprobada");
            else System.out.println("Oops...");
        }
        catch(NullPointerException e){
            throw new NullPointerException("Valor nulo en la consulta aprobar orden: "+e.toString());
        }
        catch(Exception e){
            throw new Exception("Error en la consulta de aprobar cotizacion: "+e.toString());
        }
        finally{
             if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("Error al cerrar la conexion " + e.getMessage() );
                }
            }
            this.desconectar(con);
        }
    }



   /**
    *
    * @param criterio
    * @param cadena
    * @return
    * @throws Exception
    * Modificaco Ing. Jose Castro
    */
    public ArrayList buscar(String criterio,String cadena) throws Exception {
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query="BUSCAR_HEAD";
        Cotizacion cot = null;
        ArrayList res = new ArrayList();
        try {
            con = this.conectarJNDI(query);
            String sql = obtenerSQL(query);
            sql = sql.replaceAll("#PARAM", criterio);
            st = con.prepareStatement(sql);
            st.setString(1, "%"+cadena+"%");
            rs = st.executeQuery();
            while(rs.next()){
                cot = new Cotizacion();
                cot.setCodigo(rs.getString("consecutivo"));
                cot.setFecha(rs.getString("fecha"));
                cot.setAccion(rs.getString("id_accion"));
                res.add(cot);
            }
            return res;
        }
        catch(NullPointerException e){
            throw new NullPointerException("Valor nulo en la consulta buscar datos cotizacion: "+e.toString());
        }
        catch(Exception e){
            throw new Exception("Error en la consulta de buscar datos cotizacion: "+e.toString());
        }
        finally{
             if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("Error al cerrar la conexion " + e.getMessage() );
                }
            }
            this.desconectar(con);
        }
    }




 /**
  *
  * @param criterio
  * @param cadena
  * @return
  * @throws Exception
  * Modificado Ing. Jose Castro 11 Mayo de 2010
  */
    public ArrayList buscarDets(String criterio,String cadena) throws Exception {
        System.out.println("criterio:"+criterio+"_cadena:"+cadena);
         PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query="BUSCAR_COT";
        Cotizacion cot = null;
        ArrayList res = new ArrayList();
        try {
            con = this.conectarJNDI(query);
            String sql = obtenerSQL(query);
            sql = sql.replaceAll("#PARAM", criterio);
            st = con.prepareStatement(sql);
            st.setString(1, "%"+cadena+"%");
            rs = st.executeQuery();
            while(rs.next()){
                System.out.println("filax");
                cot = new Cotizacion();//091222
                cot.setCodigo(rs.getString("cod_cotizacion"));
                cot.setMaterial(rs.getString("codigo_material"));
                cot.setFecha(rs.getString("fecha"));
                cot.setObservacion(rs.getString("observacion"));//JJCastro fase2
                cot.setAccion(rs.getString("id_accion"));
                cot.setCantidad(rs.getDouble("cantidad"));
                cot.setId(rs.getString("idcotizaciondets"));//091222


                cot.setCompra(rs.getString("compra_provint"));//JJCastro fase2
                cot.setCantidad_compra(rs.getDouble("cant_provint"));//JJCastro fase2
                cot.setValor(rs.getDouble("precio"));//Gratis1
	        cot.setCantidadComprada(rs.getDouble("cant_comprada"));
	        res.add(cot);
            }
            return res;
        }
        catch(NullPointerException e){
            throw new NullPointerException("Valor nulo en la consulta buscar orden: "+e.toString());
        }
        catch(Exception e){
            throw new Exception("Error en la consulta de buscar orden: "+e.toString());
        }
        finally{
             if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("Error al cerrar la conexion " + e.getMessage() );
                }
            }
            this.desconectar(con);
        }
    }


/*
    private String getConsecutivo(String accion,String fecha) throws Exception {
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query="BUSCAR_CONSEC";
        String retorno="";
        try {
            con = this.conectarJNDI(query);
            String sql = obtenerSQL(query);
            st = con.prepareStatement(sql);
            st.setString(1,accion);
            st.setString(2,fecha);
            rs = st.executeQuery();
            if(rs.next()){
                retorno = rs.getString("consecutivo");
            }else{
                retorno = this.getConsecutivo();
            }

            return retorno;
        }
        catch(Exception e){
            throw new Exception("Error en la consulta de generacion de consecutivo con params: "+e.toString());
        }
        finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("Error al cerrar la conexion " + e.getMessage() );
                }
            }
            this.desconectar(con);
        }
    }*/



    //jjcastro

        public String getConsecutivo(String accion,String fecha) throws SQLException {

        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "BUSCAR_CONSEC";
        String retorno = "";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query).replaceAll("#FECHA#", fecha));//JJCastro fase2
            st.setString(1, accion);

            rs = st.executeQuery();
           if (rs.next()) {
                retorno = rs.getString("consecutivo");
            }else{
                retorno = this.getConsecutivo();
            }

            }} catch (Exception e) {
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL CONSECUTIVO. \n " + e.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return retorno;

    }



    //jjcastro














    private boolean existeConsec(String consecutivo) throws Exception {
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query="ENCONTRAR_CONSEC";
        boolean retorno=false;
        try {
            con = this.conectarJNDI(query);
            String sql = obtenerSQL(query);
            st = con.prepareStatement(sql);
            st.setString(1,consecutivo);
            rs = st.executeQuery();
            if(rs.next()) {
                if(Integer.parseInt(rs.getString("numero"))>0) retorno = true;
            }
            return retorno;
        }
        catch(Exception e){
            throw new Exception("Error en la consulta de busqueda de consecutivo: "+e.toString());
        }
        finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("Error al cerrar la conexion " + e.getMessage() );
                }
            }
            this.desconectar(con);
        }
    }


     public void anularCotizacion(String consecutivo,String user) throws Exception{
        PreparedStatement st = null;
        Connection con = null;
        String query="ANULAR_ORDEN";
        int rowCount = 0;
        String accion="";
        try {
            accion = getAccion(consecutivo);
            System.out.println("antes de anular cotizacion "+consecutivo+" accion "+accion);
            con = this.conectarJNDI(query);
            String sql = obtenerSQL(query);
            st = con.prepareStatement(sql);
            st.setString(1,user);
            st.setString(2, consecutivo);

            st.setString(3, user);
            st.setString(4,accion);
            st.setString(5,consecutivo);

            rowCount=st.executeUpdate();
            if(rowCount>0) System.out.println("Cotizacion "+consecutivo+" anulada");
            else System.out.println("Oops... no se pudo anular...");

            this.desconectar(con);
        }
        catch(NullPointerException e){
            throw new NullPointerException("Valor nulo en la consulta anular cotizacion: "+e.toString());
        }
        catch(Exception e){
            throw new Exception("Error en la consulta de anular cotizacion: "+e.toString());
        }
        finally{
             if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("Error al cerrar la conexion " + e.getMessage() );
                }
            }
             if (con != null){
                try{
                    this.desconectar(con);
                } catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }

        }
    }


     private String getAccion(String consecutivo) throws Exception {
        String retorno="";
        try {
            ArrayList x = this.buscar("consecutivo", consecutivo);
            Cotizacion cot = (Cotizacion)x.get(0);
            retorno = cot.getAccion();
            return retorno;
        }
        catch(Exception e){
            throw new Exception("Error en la consulta de buscar accion: "+e.toString());
        }
     }

     private void actualizarAccion(String accion,String user) throws Exception{
        PreparedStatement st = null;
        Connection con = null;
        String query="ACTUALIZAR_ACCION";
        int rowCount = 0;
        try {
            con = this.conectarJNDI(query);
            String sql = obtenerSQL(query);
            st = con.prepareStatement(sql);
            st.setString(1,user);
            st.setString(2,accion);
            rowCount=st.executeUpdate();
            if(rowCount>0) System.out.println("Accion modificada");
            else System.out.println("Oops...");
        }
        catch(NullPointerException e){
            throw new NullPointerException("Valor nulo en la consulta modificar Accion: "+e.toString());
        }
        catch(Exception e){
            throw new Exception("Error en la consulta de modificar Accion: "+e.toString());
        }
        finally{
             if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("Error al cerrar la conexion " + e.getMessage() );
                }
            }
            this.desconectar(con);
        }
     }

     public ArrayList detallesConsec(String consecutivo) throws NullPointerException,Exception {
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query="BUSCAR_POR_CONSEC";
        ArrayList resultado = new ArrayList();
        Cotizacion cot = null;
        try{
            con = this.conectarJNDI(query);
            String sql = obtenerSQL(query);
            st = con.prepareStatement(sql);
            st.setString(1, consecutivo);
            rs=st.executeQuery();
            while(rs.next()){
               cot = new Cotizacion();
               cot.setAccion(rs.getString("id_accion"));
               cot.setCantidad(rs.getDouble("cantidad"));
               cot.setCodigo(rs.getString("cod_cotizacion"));
               cot.setFecha(rs.getString("fecha"));
               cot.setMaterial(rs.getString("codigo_material"));
               cot.setObservacion(rs.getString("observacion"));
               cot.setValor(rs.getDouble("precio"));//Gratis1
		cot.setCantidadComprada(rs.getDouble("cant_comprada"));
               resultado.add(cot);
            }
            return resultado;
        }
        catch(NullPointerException e){
            throw new NullPointerException("Valor nulo en la consulta listar detalles: "+e.toString());
        }
        catch(Exception e){
            throw new Exception("Error en la consulta de listado de detalles: "+e.toString());
        }
        finally{
             if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("Error al cerrar la conexion " + e.getMessage() );
                }
            }
            this.desconectar(con);
        }
    }

     public ArrayList verPendientes() throws NullPointerException,Exception{
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query="VER_PEND";
        ArrayList resultado = new ArrayList();
        Cotizacion cot = null;
        try{
            con = this.conectarJNDI(query);
            String sql = obtenerSQL(query);
            st = con.prepareStatement(sql);
            rs=st.executeQuery();
            while(rs.next()){
                cot = new Cotizacion();
                cot.setAccion(rs.getString("id_accion"));
                cot.setCodigo(rs.getString("consecutivo"));
                cot.setFecha(rs.getString("fecha"));
                resultado.add(cot);
            }
            return resultado;
        }
        catch(NullPointerException e){
            throw new NullPointerException("Valor nulo en la consulta listar todas las cotizaciones pendientes: "+e.toString());
        }
        catch(Exception e){
            throw new Exception("Error en la consulta de listado de cotizaciones pendientes: "+e.toString());
        }
        finally{
             if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("Error al cerrar la conexion " + e.getMessage() );
                }
            }
            this.desconectar(con);
        }
    }

    public ArrayList ver_Aprobadas() throws Exception{
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query="VER_APROBADAS";
        ArrayList resultado = new ArrayList();
        Cotizacion cot = null;
        try{
            con = this.conectarJNDI(query);
            String sql = obtenerSQL(query);
            st = con.prepareStatement(sql);
            rs=st.executeQuery();
            while(rs.next()){
                cot = new Cotizacion();
                cot.setAccion(rs.getString("id_accion"));
                cot.setCodigo(rs.getString("consecutivo"));
                cot.setFecha(rs.getString("fecha"));
                resultado.add(cot);
            }
            return resultado;
        }
        catch(NullPointerException e){
            throw new NullPointerException("Valor nulo en la consulta listar todas las cotizaciones aprobadas: "+e.toString());
        }
        catch(Exception e){
            throw new Exception("Error en la consulta de listado de cotizaciones aprobadas: "+e.toString());
        }
        finally{
             if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("Error al cerrar la conexion " + e.getMessage() );
                }
            }
            this.desconectar(con);
        }
    }

    public ArrayList verTodas() throws Exception{
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query="VER_TODAS";
        ArrayList resultado = new ArrayList();
        Cotizacion cot = null;
        try{
            con = this.conectarJNDI(query);
            String sql = obtenerSQL(query);
            st = con.prepareStatement(sql);
            rs=st.executeQuery();
            while(rs.next()){
                cot = new Cotizacion();
                cot.setAccion(rs.getString("id_accion"));
                cot.setCodigo(rs.getString("consecutivo"));
                cot.setFecha(rs.getString("fecha"));
                resultado.add(cot);
            }
            return resultado;
        }
        catch(NullPointerException e){
            throw new NullPointerException("Valor nulo en la consulta listar todas las cotizaciones aprobadas: "+e.toString());
        }
        catch(Exception e){
            throw new Exception("Error en la consulta de listado de cotizaciones aprobadas: "+e.toString());
        }
        finally{
             if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("Error al cerrar la conexion " + e.getMessage() );
                }
            }
            this.desconectar(con);
        }
    }

    public TreeMap datosMs() throws Exception {
        TreeMap x = new TreeMap();
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query="DATOS_MS";
        try{
            con = this.conectarJNDI(query);
            String sql = obtenerSQL(query);
            st = con.prepareStatement(sql);
            rs=st.executeQuery();
            int i = 0;
            x.put("...","...");
            while(rs.next()) {
                x.put(rs.getString(1)+" "+rs.getString(2)+" "+rs.getString(3),rs.getString(1));
                i++;
            }
        }
        catch(NullPointerException e){
            throw new NullPointerException("Valor nulo en la consulta datos ms: "+e.toString());
        }
        catch(Exception e){
            throw new Exception("Error en la consulta de datos ms: "+e.toString());
        }
        finally{
             if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("Error al cerrar la conexion " + e.getMessage() );
                }
            }
            this.desconectar(con);
        }
        return x;
    }

    public String datosAccion(String idaccion) throws Exception {
        String x = "";
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query="DATOS_ACT";
        try{
            con = this.conectarJNDI(query);
            String sql = obtenerSQL(query);
            st = con.prepareStatement(sql);
            st.setString(1, idaccion);
            rs=st.executeQuery();
            int i = 0;
            while(rs.next()) {
                     x = rs.getString(2)+";"+rs.getString(3)+";"+rs.getString(4);//plantilla cotizacion castro
                i++;
            }
        }
        catch(NullPointerException e){
            throw new NullPointerException("Valor nulo en la consulta datos accion: "+e.toString());
        }
        catch(Exception e){
            throw new Exception("Error en la consulta de datos accion: "+e.toString());
        }
        finally{
             if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("Error al cerrar la conexion " + e.getMessage() );
                }
            }
            this.desconectar(con);
        }
        return x;
    }

    public boolean existeCotizacion(String accion) throws Exception {
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query="VERIFICAR_EXIST";
        boolean retorno=false;
        try {
            con = this.conectarJNDI(query);
            String sql = obtenerSQL(query);
            st = con.prepareStatement(sql);
            st.setString(1,accion);
            rs = st.executeQuery();
            if(rs.next()) {
                if(Integer.parseInt(rs.getString("numero"))>0) retorno = true;
            }
            return retorno;
        }
        catch(Exception e){
            throw new Exception("Error en la consulta de busqueda de cotizacion: "+e.toString());
        }
        finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("Error al cerrar la conexion " + e.getMessage() );
                }
            }
            this.desconectar(con);
        }
    }

    //added on 24-oct-2009
    /*public ArrayList getvaloresAccion(String idaccion) throws Exception{
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query="DATOS_PARA_ACCION";
        ArrayList resp = new ArrayList();
        double[] vals = new double[3];
        try {
            con = this.conectarJNDI(query);
            String sql = obtenerSQL(query);
            st = con.prepareStatement(sql);
            st.setString(1, idaccion);
            rs = st.executeQuery();
            while(rs.next()){
                if(rs.getString(1).equals("M")) vals[0] = rs.getDouble(3);
                if(rs.getString(1).equals("D")) vals[1] = rs.getDouble(3);
                if(rs.getString(1).equals("O")) vals[2] = rs.getDouble(3);

                System.out.println("vals[0]"+vals[0]+"vals[1]"+vals[1]+"vals[2]"+vals[2]);

                resp.add(vals);
            }
            return resp;
        }
        catch(NullPointerException e){
            throw new NullPointerException("Valor nulo en la consulta buscar valores accion: "+e.toString());
        }
        catch(Exception e){
            throw new Exception("Error en la consulta de buscar valores accion: "+e.toString());
        }
        finally{
             if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("Error al cerrar la conexion " + e.getMessage() );
                }
            }
            this.desconectar(con);
        }
    }*/

    public ArrayList getvaloresAccion(String idaccion) throws Exception{//091029
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query="DATOS_PARA_ACCION";
        ArrayList resp = new ArrayList();
        double[] vals = new double[3];
        try {
            con = this.conectarJNDI(query);
            String sql = obtenerSQL(query);
            st = con.prepareStatement(sql);
            st.setString(1, idaccion);
            rs = st.executeQuery();
            while(rs.next()){
                if(rs.getString(1).equals("M")) vals[0] = rs.getDouble(3);
                if(rs.getString(1).equals("D")) vals[1] = rs.getDouble(3);
                if(rs.getString(1).equals("O")) vals[2] = rs.getDouble(3);
            }
            System.out.println("vals[0]"+vals[0]+"vals[1]"+vals[1]+"vals[2]"+vals[2]);
            resp.add(vals);//091029
            return resp;
        }
        catch(NullPointerException e){
            throw new NullPointerException("Valor nulo en la consulta buscar valores accion: "+e.toString());
        }
        catch(Exception e){
            throw new Exception("Error en la consulta de buscar valores accion: "+e.toString());
        }
        finally{
             if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("Error al cerrar la conexion " + e.getMessage() );
                }
            }
            this.desconectar(con);
        }
    }

    public String getConsecutivo(String accion) throws Exception {
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query="BUSCAR_CONSEC_2";
        String retorno="";
        try {
            con = this.conectarJNDI(query);
            String sql = obtenerSQL(query);
            st = con.prepareStatement(sql);
            st.setString(1,accion);
            rs = st.executeQuery();
            if(rs.next()) {
                retorno = rs.getString("consecutivo");
            }
            else retorno = this.getConsecutivo();

            return retorno;
        }
        catch(Exception e){
            throw new Exception("Error en la consulta de generacion de consecutivo con params: "+e.toString());
        }
        finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("Error al cerrar la conexion " + e.getMessage() );
                }
            }
            this.desconectar(con);
        }
    }

public void valorizarAIU(double pa,double pi,double pu,double a,double i,double u,String user,String accion, double materiales, double manos_obra, double otros) throws Exception {
        StringStatement st = null;
        String query="VALOR_AIU";
        String sql="";
        PreparedStatement st2 = null;
        Connection con = null;
        try{
            //Actualiza los valores de AIU
            st = new StringStatement(obtenerSQL(query));
            st.setDouble(pa);
            st.setDouble(pi);
            st.setDouble(pu);
            st.setDouble(a);
            st.setDouble(i);
            st.setDouble(u);
            st.setString(user);
            st.setDouble(materiales);
            st.setDouble(manos_obra);
            st.setDouble(otros);
            st.setString(accion);

            //Actualiza precio de venta
            ElectricaribeOfertaDAO eod=new ElectricaribeOfertaDAO(this.getDatabaseName());
            sql=st.getSql()+";"+eod.actPrecio_venta(this.getIdSolicitud(accion), "substring (replace (a.fecha_oferta,'-',''),1,6)");
            //System.out.println(sql);
            //Ejecuta todo
            con = this.conectarJNDI(query);
            st2 = con.prepareStatement(sql);
            st2.execute();

        }
        catch(Exception e){
            e.printStackTrace();
            throw new Exception("Error en la consulta de generacion de consecutivo con params: "+e.toString());
        }
        finally{
            if(st2 != null){
                try{
                    st2.close();
                }
                catch(SQLException e){
                    throw new SQLException("Error al cerrar la conexion " + e.getMessage() );
                }
            }
            this.desconectar(con);
        }
    }

    //added 27-oct-2009
    public boolean hayValoresAiu(String idaccion) throws Exception{
        boolean hay = true;
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query="HAY_AIU";
        String retorno = "";
        try {
            con = this.conectarJNDI(query);
            String sql = obtenerSQL(query);
            st = con.prepareStatement(sql);
            st.setString(1, idaccion);
            rs = st.executeQuery();
            if(rs.next()) {
                retorno = rs.getString(1);
                hay = false;
            }
            return hay;
        }
        catch(Exception e){
            throw new Exception("Error en la consulta de buscar valores existentes aiu: "+e.toString());
        }
        finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("Error al cerrar la conexion " + e.getMessage() );
                }
            }
            this.desconectar(con);
        }
    }


    public double[] getPorcentajesAIU(String idaccion) throws Exception{//091029
        double[] hay = new double[3];
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query="GET_PORC_AIU";
        String retorno = "";
        try {
            con = this.conectarJNDI(query);
            String sql = obtenerSQL(query);
            st = con.prepareStatement(sql);
            st.setString(1, idaccion);
            rs = st.executeQuery();
            if(rs.next()) {
                hay[0]=Double.parseDouble(rs.getString("porc_administracion"));
                hay[1]=Double.parseDouble(rs.getString("porc_imprevisto"));
                hay[2]=Double.parseDouble(rs.getString("porc_utilidad"));
                //hay = false;
            }
            return hay;
        }
        catch(Exception e){
            throw new Exception("Error en la consulta de buscar valores existentes aiu: "+e.toString());
        }
        finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("Error al cerrar la conexion " + e.getMessage() );
                }
            }
            this.desconectar(con);
        }
    }

    public void guardarBorrador(String producto,String accion,int cantidad,String nota,String fecha,String usuario) throws Exception {
        PreparedStatement st = null;
        Connection con = null;
        String query="GUARDAR_COT";
        String consec = getConsecutivo(accion,fecha);
        int rowCount=0;
        con = this.conectarJNDI(query);
        String sql = obtenerSQL(query);
        st = con.prepareStatement(sql);
        try{
            if(existeConsec(consec)==true){
                System.out.println("La fila ya esta en tabla cotizacion. Pasar a adicion de detalles");
            }
            else {
                st.setString(1,consec);
                st.setString(2, fecha);
                st.setString(3, accion);
                st.setString(4, usuario);
                rowCount=st.executeUpdate();
                if(rowCount>0) System.out.println("Insertada fila borrador en tabla cotizacion. Pasar a insercion de detalles");
                else System.out.println("Oops... por alguna razon no se inserto la fila borrador en cotizacion...");
            }
            query="INSERTAR_ORDEN_DETS";
            sql = obtenerSQL(query);
            st = con.prepareStatement(sql);
            st.setString(1,producto);
            st.setInt(2, cantidad);
            st.setString(3, consec);
            st.setString(4, fecha);
            st.setString(5, nota);
            st.setString(6, accion);
            rowCount=st.executeUpdate();
            if(rowCount>0) {
                System.out.println("Insertada fila tabla detalles");
            }
            else System.out.println("Oops... por alguna razon no se inserto la fila en detalles...");
        }
        catch(Exception e){
            throw new Exception("Error en la consulta de insercion de orden plus dets: "+e.toString());
        }
        finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("Error al cerrar la conexion " + e.getMessage() );
                }
            }
            this.desconectar(con);
        }
    }

    public void borrarDetalles(String accion) throws Exception {
        PreparedStatement st = null;
        Connection con = null;
        String query="DEL_COT";
        int rowCount=0;
        try {
            con = this.conectarJNDI(query);
            String sql = obtenerSQL(query);
            st = con.prepareStatement(sql);
            st.setString(1, accion);
            rowCount = st.executeUpdate();
            if(rowCount>0) System.out.println("Detalles de cotizacion de la accion "+accion+" han sido anulados");
            else System.out.println("Los detalles de cotizacion de la accion "+accion+" no pudieron ser anulados");
        }
        catch(Exception e){
            throw new Exception("Error en la consulta de borrado de detalles: "+e.toString());
        }
        finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("Error al cerrar la conexion " + e.getMessage() );
                }
            }
            this.desconectar(con);
        }
    }

    public boolean buscarBorrador(String accion) throws Exception {
        boolean existe = false;
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query="BUSCAR_GUARDADO";
        try{
            con = this.conectarJNDI(query);
            String sql = obtenerSQL(query);
            st = con.prepareStatement(sql);
            st.setString(1, accion);
            rs = st.executeQuery();
            if (rs.next()) existe = true;
            return existe;
        }
        catch(Exception e){
            throw new Exception("Error buscando existencia de borrador: "+e.toString());
        }
        finally{
            if(st != null){
                try{
                    st.close();
                    if(rs!=null) rs.close();
                }
                catch(SQLException e){
                    throw new SQLException("Error al cerrar la conexion " + e.getMessage() );
                }
            }
            this.desconectar(con);
        }
    }

    public void actualizarEstado(String accion,String usuario) throws Exception{
        PreparedStatement st = null;
        Connection con = null;
        String query="ACT_COT";
        int rowCount=0;
        try{
            con = this.conectarJNDI(query);
            String sql = obtenerSQL(query);
            sql = sql.replaceAll("#user", usuario);
            st = con.prepareStatement(sql);
            st.setString(1, accion);
            st.setString(2, accion);
            rowCount = st.executeUpdate();
            if(rowCount>0) System.out.println("El estado de la cotizacion para la accion "+accion+" ha sido actualizado");
            else System.out.println("El estado de la cotizacion para la accion "+accion+" no pudo ser actualizado");
            //this.desconectar(con);
        }
        catch(Exception e){
            throw new Exception("Error en la actualizacion de estado de cotizacion: "+e.toString());
        }
        finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("Error al cerrar la conexion " + e.getMessage() );
                }
            }
            if (con != null){
                try{
                    this.desconectar(con);
                } catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }

        }
    }


    public String nombreContratista(String idaction) throws SQLException,NullPointerException,Exception {
        String nombre="";
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query="CONT_ACCION";
        try{
            con = this.conectarJNDI(query);
            String sql = obtenerSQL(query);
            st = con.prepareStatement(sql);
            st.setString(1, idaction);
            rs = st.executeQuery();
            if(rs.next()) nombre = rs.getString(1);
            return nombre;
        }
        catch(NullPointerException e){
            throw new NullPointerException("Valor nulo en la consulta buscar nombre contratista de la accion: "+e.toString());
        }
        catch(Exception e){
            throw new Exception("Error en la consulta de buscar nombre contratista de la accion: "+e.toString());
        }
        finally {
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("Error al cerrar la conexion " + e.getMessage() );
                }
            }
            this.desconectar(con);
        }
    }

    public String getIdSolicitud(String idaccion) throws Exception{
        Connection          con     = null;
        PreparedStatement   ps      = null;
        ResultSet           rs      = null;
        String              query   = "SQL_GET_IDSOLICITUD";
        String              sql     = "";
        String              id      = "";

        try{
            con         = this.conectarJNDI(query);
            sql         = this.obtenerSQL(query);
            ps          = con.prepareStatement(sql);
            ps.setString(1, idaccion);
            rs          = ps.executeQuery();

            if(rs.next()){
                id = rs.getString("id_solicitud");
            }
        }
        catch(Exception e){
            throw new Exception(e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con);   } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }

        return id;
    }




/**
 *
 * @param lista
 * @param usuario
 * @param fecha
 * @throws Exception
 * @since 12 05 2010
 * Modificado por: Ing. Jose Castro
 */
    public void insertDetPlusOrden(ArrayList lista,String usuario,String fecha) throws Exception{
        int tam = lista.size();
        String query="";
        String sql = "";
        String cons = "";
        String accion="";
        Cotizacion ct = null;
        StringStatement st = null;
        boolean xst = false;
        ArrayList<String> batch = new ArrayList<String>();
        try{
            ct = (Cotizacion) lista.get(0);
            cons = this.getConsecutivo(ct.getAccion(), fecha);
            batch.add(this.borrarDets(ct.getAccion()));
            if(tam>0){
                for(int i=0;i<tam;i++){
                    ct = (Cotizacion) lista.get(i);
                    accion = ct.getAccion();
                    if(i==0) { System.out.println("Insertando datos de cotizacion para la accion "+ct.getAccion() +" ,cotizacion num. "+cons); }
                    if(this.buscarBorrador(accion)==false && this.existeConsec(cons)==false && xst==false) {//jjcastro
                        //Aca se inserta la cabecera y un detalle
                        query="INSERTAR_ORDEN";
                        sql = this.obtenerSQL(query);
                        st = new StringStatement(sql,true);
                        st.setString(1,cons);
                        st.setString(2, ct.getFecha());
                        st.setString(3, ct.getAccion());
                        st.setString(4, usuario);
                        xst=true;
                        batch.add(st.getSql());
                        //this.desconectar(con);

                        System.out.println("Agregando cabecera...");

                        query="INSERTAR_ORDEN_DETS";
                        sql = this.obtenerSQL(query);
                        st = new StringStatement(sql,true);
                        st.setString(1,ct.getMaterial());
                        st.setDouble(2, ct.getCantidad());
                        st.setString(3, cons);
                        st.setString(4, ct.getFecha());
                        st.setString(5, ct.getObservacion());
                        st.setString(6, ct.getAccion());

                        st.setString(7, ct.getCompra());//JJCastro fase2
                        st.setDouble(8, ct.getCantidad_compra());//JJCastro fase2
                        st.setDouble(9, ct.getValor());//Gratis1
                        st.setString(10, ct.getOficial());//marcar la cotizacion como oficial.

                         // this.desconectar(con);
                        System.out.println("Agregando detalle...");
                    }
                    else {                      
                        //Aca se inserta un detalle
                        query="INSERTAR_ORDEN_DETS";
                        sql = this.obtenerSQL(query);
                        //con = this.conectar(query);
                        st = new StringStatement(sql,true);
                        st.setString(1,ct.getMaterial());
                        st.setDouble(2, ct.getCantidad());
                        st.setString(3, cons);
                        st.setString(4, ct.getFecha());
                        st.setString(5, ct.getObservacion());
                        st.setString(6, ct.getAccion());


                        st.setString(7, ct.getCompra());//JJCastro fase2
                        st.setDouble(8, ct.getCantidad_compra());//JJCastro fase2
                        st.setDouble(9, ct.getValor());//Gratis1
                        st.setString(10, ct.getOficial());//marcar la cotizacion como oficial.

                        //this.desconectar(con);
                        System.out.println("Agregando detalle...");
                    }
                    batch.add(st.getSql());

                }
                
                sql = this.obtenerSQL("UPD_COT");
                sql = sql.replaceAll("#accion", accion);
                batch.add(sql);
                
                ClientesVerDAO clvd = new ClientesVerDAO(this.getDatabaseName());
                String estado = clvd.estadoAccion(accion);
                
                //query = "UPD_ACCIONES";
                //con = this.conectar("UPD_ACCIONES");
                sql = this.obtenerSQL("UPD_ACCIONES");
                
                st = new StringStatement(sql,true);
                st.setString(1, usuario);
                st.setString(2, accion);
                
                batch.add(st.getSql());
                //se actualizan los valores de la accion
                
                //Si se esta creando una cotizacion nueva
                if(estado.equals("030")){
                    sql = "UPDATE opav.acciones SET estado='040' WHERE id_accion="+accion;
                    batch.add(sql);
                    System.out.println("Creando cotizacion y actualizando accion...");
                }

                if(estado.equals("030") || estado.equals("040")){
                     System.out.println("No se actualizaran precios venta ya que no se ha generado oferta para la accion "+accion);
                }
                else{
                    ElectricaribeOfertaDAO eod=new ElectricaribeOfertaDAO(this.getDatabaseName());
                    batch.add(eod.actPrecio_venta(this.getIdSolicitud(accion),"substring (replace (a.fecha_oferta,'-',''),1,6)"));
                    System.out.println("Se actualizaran precios venta para la accion "+accion);


/*                    String idsolicitud = this.getIdSolicitud(accion);
                    query = "SQL_PRECVEN";
                    sql = this.obtenerSQL(query);
                    con = this.conectar(query);
                    st = con.prepareStatement(sql);
                    st.setString(1, idsolicitud);*/
                    //this.desconectar(con);

                }

                TransaccionService transerv = new TransaccionService(this.getDatabaseName());
                transerv.crearStatement();
                for (String sql1 : batch) {
                    transerv.getSt().addBatch(sql1);
                }
                transerv.execute();
                System.out.println("Proceso realizado...");
            }

        }
        catch(Exception e){
             throw new Exception("Error creando cotizacion: "+e.getMessage());
        }
        finally{
            if (st  != null){
                st = null;
            }
           
        }
    }





/**
 *
 * @param lista
 * @param usuario
 * @param fecha
 * @throws SQLException
 * @throws Exception
 * Modificado por: Ing. Jose Castro Mayo 11 2010
 */
    public void guardarBorrador(ArrayList lista,String usuario,String fecha) throws SQLException,Exception{
        int tam = lista.size();
        String query="";
        String sql = "";
        String cons = "";
        Cotizacion ct = null;
        StringStatement st = null;
        boolean xst = false;
        ArrayList<String> batch = new ArrayList<String>();
        try{
            ct = (Cotizacion) lista.get(0);
            cons = this.getConsecutivo(ct.getAccion(), fecha);
            batch.add(this.borrarDets(ct.getAccion()));
            for(int i=0;i<tam;i++){
                ct = (Cotizacion) lista.get(i);
                if(i==0) { System.out.println("Insertando datos de cotizacion para la accion "+ct.getAccion() +" ,cotizacion num. "+cons); }
                if( this.existeConsec(cons)==false && xst==false) {
                    query="GUARDAR_COT";
                    sql = this.obtenerSQL(query);
                    st = new StringStatement(sql,true);
                    st.setString(1,cons);
                    st.setString(2, ct.getFecha());
                    st.setString(3, ct.getAccion());
                    st.setString(4, usuario);
                    
                    batch.add(st.getSql());
                    xst = true;
                    System.out.println("Agregando cabecera borrador....");
                    //this.desconectar(con);

                    query="INSERTAR_ORDEN_DETS";
                    sql = this.obtenerSQL(query);
                    //con = this.conectar(query);
                    st = new StringStatement(sql,true);
                    st.setString(1,ct.getMaterial());
                    st.setDouble(2, ct.getCantidad());
                    st.setString(3, cons);
                    st.setString(4, ct.getFecha());
                    st.setString(5, ct.getObservacion());
                    st.setString(6, ct.getAccion());

                    st.setString(7, ct.getCompra());//JJCastro fase2
                    st.setDouble(8, ct.getCantidad_compra());//JJCastro fase2
                    st.setDouble(9, ct.getValor());//Gratis1
                    st.setString(10, ct.getOficial());//marcar la cotizacion como oficial.

                    batch.add(st.getSql());
                    //this.desconectar(con);
                    System.out.println("Agregando detalle...");
                }
                else {
                    query="INSERTAR_ORDEN_DETS";
                    sql = this.obtenerSQL(query);
                    //con = this.conectar(query);
                    st = new StringStatement(sql,true);
                    st.setString(1,ct.getMaterial());
                    st.setDouble(2, ct.getCantidad());
                    st.setString(3, cons);
                    st.setString(4, ct.getFecha());
                    st.setString(5, ct.getObservacion());
                    st.setString(6, ct.getAccion());



                    st.setString(7, ct.getCompra());//JJCastro fase2
                    st.setDouble(8, ct.getCantidad_compra());//JJCastro fase2
                    st.setDouble(9, ct.getValor());//Gratis1
                    st.setString(10, ct.getOficial());//marcar la cotizacion como oficial.



                    batch.add(st.getSql());
                    //this.desconectar(con);
                    System.out.println("Agregando detalle...");
                }

            }

            //this.desconectar(con);
           TransaccionService transerv = new TransaccionService(this.getDatabaseName());
           transerv.crearStatement();
           for (String sql1 : batch) {
               transerv.getSt().addBatch(sql1);
           }
           transerv.execute();
           System.out.println("Proceso realizado...");
        }
        catch(Exception e){
             throw new Exception("Error creando cotizacion: "+e.getMessage());
        }
        finally{
            if (st  != null){
                st = null;
            }
        }
    }

    public void anularRegistro(ArrayList listado,String usuario) throws Exception {
        Vector batch = new Vector();
        int dsize = listado.size();
        String id_registro="";
        String qstr="";
        Cotizacion cds = null;
        String query = "";
        String sql="";
        String accion="";
        PreparedStatement st = null;
        Connection con = null;
        try{
            if(dsize>0){
                for(int i=0;i<dsize;i++) {
                    id_registro = (String)listado.get(i);
                    qstr = qstr + "UPDATE cotizaciondets SET reg_status='A',precio_venta=0 WHERE idcotizaciondets = '"+id_registro+"' ;\n";
                    System.out.println("anular registro "+id_registro);

                }
                cds = (Cotizacion)this.buscarDets("idcotizaciondets", id_registro).get(0);
                accion=cds.getAccion();
                query = "UPD_COT";
                sql = this.obtenerSQL(query);
                sql = sql.replaceAll("#accion", accion);
                sql = qstr+sql;

                con = this.conectar(query);
                st = con.prepareStatement(sql);
                st.setString(1, usuario);
                batch.add(st.toString());

                st.close();
                this.desconectar(con);

                ApplusDAO apdao = new ApplusDAO();
                apdao.ejecutarSQL(batch);
            }
        }
        catch(Exception e){
            throw new Exception("Error anulando detalle: "+e.getMessage());
        }
        finally{
            if (st  != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null){
                try{
                    this.desconectar(con);
                } catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
    }


        //jjcastro plantilla cotizacion
    /**
     *
     * @param id_accion
     * @return
     * @throws NullPointerException
     * @throws Exception
     * Created by Ing. Jose Castro
     */
         public ArrayList cotizacionesContratista(String contratista) throws NullPointerException,Exception {
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query="SQL_COTIZACIONES_CONTRATISTA";
        ArrayList resultado = new ArrayList();
        Cotizacion cot = new Cotizacion();
        try{
            con = this.conectarJNDI(query);
            if(con!=null){
            st = con.prepareStatement(this.obtenerSQL(query));
            st.setString(1, contratista);
            rs=st.executeQuery();
            while(rs.next()){
                resultado.add(new String[]{rs.getString("id_solicitud"), rs.getString("descripcion") ,  rs.getString("nomcli"),  rs.getString("fecha") ,  rs.getString("id_accion") ,  rs.getString("id_cliente") ,  rs.getString("consecutivo")});

            }

        }}
        catch(NullPointerException ec){
            throw new NullPointerException("No hay resultado ..." + ec.toString());
        }
        catch(Exception ec){
            throw new Exception("Error en la consulta cotizaciones de un contratista: "+ec.toString());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return resultado;
    }

    /**
 * Consulta los detalles de una solicitud dado su id_accion
 * @param criterio
 * @param id_accion_original
 * @param id_accion
 * @return
 * @throws Exception
 */
      public ArrayList buscarDetsPlantilla(String criterio,String id_accion_original, String id_accion) throws Exception {

        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query="SQL_COTIZACIONES_PLANTILLA";
        Cotizacion cot = null;
        ArrayList res = new ArrayList();
        try {
            con = this.conectarJNDI(query);
            String sql = obtenerSQL(query);


            if(criterio.equals("REEMPLAZAR"))   sql = sql.replaceAll("#ACCIONES#", "('"+id_accion+"')");
            if(criterio.equals("ADICIONAR"))     sql = sql.replaceAll("#ACCIONES#", "('"+id_accion_original+"','"+id_accion+"')");
            if(criterio.equals("CANCELAR"))       sql = sql.replaceAll("#ACCIONES#",  "('"+id_accion_original+"')");
            st = con.prepareStatement(sql);

            rs = st.executeQuery();
            while(rs.next()){
                System.out.println("filax");
                cot = new Cotizacion();//091222
                cot.setCodigo(reset(rs.getString("cod_cotizacion")));
                cot.setIdcategoria(rs.getInt("idcategoria"));
                cot.setMaterial(rs.getString("codigo_material"));
                cot.setTipo_material(rs.getString("tipo_material"));
                cot.setFecha(rs.getString("fecha"));
                cot.setObservacion(rs.getString("observacion"));
                cot.setAccion(rs.getString("id_accion"));
                cot.setCantidad(rs.getDouble("cantidad"));
                cot.setId(rs.getString("idcotizaciondets"));

                cot.setCompra(rs.getString("compra_provint"));
                cot.setCantidad_compra(rs.getDouble("cant_provint"));
                cot.setValor(rs.getDouble("precio"));
                cot.setDescripcion_categoria(rs.getString("descripcion"));
                cot.setDescripcion_material(rs.getString("desc_material"));
                cot.setPrecioen_material(rs.getDouble("precio_mat"));
		cot.setCantidadComprada(rs.getDouble("cant_comprada"));
                res.add(cot);
            }
            return res;
        }
        catch(NullPointerException e){
            throw new NullPointerException("Valor nulo en la consulta buscar orden: "+e.toString());
        }
        catch(Exception e){
            throw new Exception("Error en la consulta de buscar orden: "+e.toString());
        }
        finally{
             if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("Error al cerrar la conexion " + e.getMessage() );
                }
            }
            this.desconectar(con);
        }
    }

       /**
       *
       * @param id_accion
       * @param estado_cotizacion
       * @return
       * @throws Exception
       * Consulta los detalles de una cotizacion dado el idaccion
       */

      public ArrayList buscarDetCotizacion(String id_accion,String estado_cotizacion) throws Exception {

        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query="SQL_DETALLES_COTIZACION";
        Cotizacion cot = null;
        ArrayList res = new ArrayList();
        try {
            con = this.conectarJNDI(query);
            String sql = obtenerSQL(query);
            st = con.prepareStatement(sql);
            st.setString(1, id_accion);
            st.setString(2, estado_cotizacion);
            rs = st.executeQuery();
            while(rs.next()){
                cot = new Cotizacion();
                cot.setCodigo(reset(rs.getString("cod_cotizacion")));
                cot.setIdcategoria(rs.getInt("idcategoria"));
                cot.setMaterial(rs.getString("codigo_material"));
                cot.setTipo_material(rs.getString("tipo_material"));
                cot.setFecha(rs.getString("fecha"));
                cot.setObservacion(rs.getString("observacion"));
                cot.setAccion(rs.getString("id_accion"));
                cot.setCantidad(rs.getDouble("cantidad"));
                cot.setId(rs.getString("idcotizaciondets"));

                cot.setCompra(rs.getString("compra_provint"));
                cot.setCantidad_compra(rs.getDouble("cant_provint"));
                cot.setValor(rs.getDouble("precio"));
                cot.setDescripcion_categoria(rs.getString("descripcion"));
                cot.setDescripcion_material(rs.getString("desc_material"));
                cot.setPrecioen_material(rs.getDouble("precio_mat"));
	        cot.setCantidadComprada(rs.getDouble("cant_comprada"));
                cot.setOficial(rs.getString("oficial"));
                res.add(cot);
            }
            return res;
        }
        catch(NullPointerException e){
            throw new NullPointerException("Valor nulo en la consulta de detalles de Cotizacion: "+e.toString());
        }
        catch(Exception e){
            throw new Exception("Error en la consulta de detalles de Cotizacion: "+e.toString());
        }
        finally{
             if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("Error al cerrar la conexion " + e.getMessage() );
                }
            }
            this.desconectar(con);
        }
    }



    private String reset(String val){
            if(val==null)
               val = "";
            return val;
    }
     /**
     * actualiza el detalle de una cotizacion
     * @param lista los item de la cotizacion a modificar
     * @param usuario
     * @param fecha
     * @throws Exception
     * @since 10 02 2011
     * @autor Ing. Iris Vargas
     */
    public void updateDetPlusOrden(ArrayList lista, String usuario, String fecha, String ms) throws Exception {
        int tam = lista.size();
        String query = "";
        String sql = "";
        String cons = "";
        String accion = "";
        Cotizacion ct = null;
        StringStatement st = null;
        //Connection con = null;
        ArrayList<String> batch = new ArrayList<String>();
        try {
            ct = (Cotizacion) lista.get(0);
            cons = this.getConsecutivo(ct.getAccion(), fecha);
            batch.add(this.borrarCotDet(ms));
            if (tam > 0) {
                for (int i = 0; i < tam; i++) {
                    ct = (Cotizacion) lista.get(i);
                    accion = ct.getAccion();

                    if (ct.getId().equals("")) {

                        query = "INSERTAR_ORDEN_DETS";
                        sql = this.obtenerSQL(query);
                       // con = this.conectar(query);
                        st = new StringStatement(sql,true);
                        st.setString(1, ct.getMaterial());
                        st.setDouble(2, ct.getCantidad());
                        st.setString(3, cons);
                        st.setString(4, ct.getFecha());
                        st.setString(5, ct.getObservacion());
                        st.setString(6, ct.getAccion());

                        st.setString(7, ct.getCompra());
                        st.setDouble(8, ct.getCantidad_compra());
                        st.setDouble(9, ct.getValor());
                        st.setString(10, ct.getOficial());//marcar la cotizacion como oficial.
                        
                        // this.desconectar(con);

                    } else {

                        //se actualiza un detalle
                        query = "UPDATE_ORDEN_DETS";
                        sql = this.obtenerSQL(query);
                       // con = this.conectar(query);
                        st = new StringStatement(sql, true);
                        st.setString(1, ct.getMaterial());
                        st.setDouble(2, ct.getCantidad());
                        st.setString(3, cons);
                        st.setString(4, ct.getFecha());
                        st.setString(5, ct.getObservacion());
                        st.setString(6, ct.getAccion());
                        st.setString(7, ct.getCompra());
                        st.setDouble(8, ct.getCantidad_compra());
                        st.setDouble(9, ct.getValor());
                        st.setInt(10, Integer.parseInt(ct.getId()));

                        
                      //  this.desconectar(con);
                    }

                    batch.add(st.getSql());
                }

                //se actualizan los valores de la accion
                sql = this.obtenerSQL("UPD_COT");
                sql = sql.replaceAll("#accion", accion);
                batch.add(sql);
                
                ClientesVerDAO clvd = new ClientesVerDAO(this.getDatabaseName());
                String estado = clvd.estadoAccion(accion);
                
                //query = "UPD_ACCIONES";
                //con = this.conectar("UPD_ACCIONES");
                sql = this.obtenerSQL("UPD_ACCIONES");
                
                st = new StringStatement(sql,true);
                st.setString(1, usuario);
                st.setString(2, accion);
                
                batch.add(st.getSql());
              //  this.desconectar(con);

                if (estado.equals("040")) {
                    System.out.println("No se actualizaran precios venta ya que no se ha generado oferta para la accion " + accion);
                } else {
                    ElectricaribeOfertaDAO eod = new ElectricaribeOfertaDAO(this.getDatabaseName());
                    batch.add(eod.actPrecio_venta(this.getIdSolicitud(accion), "substring (replace (a.fecha_oferta,'-',''),1,6)"));
                    System.out.println("Se actualizaran precios venta para la accion " + accion);

                }

                TransaccionService transerv = new TransaccionService(this.getDatabaseName());
                transerv.crearStatement();
                for (String sql1 : batch) {
                    transerv.getSt().addBatch(sql1);
                }
                transerv.execute();
                //transerv.ejecutarSQL(batch);
                System.out.println("Proceso realizado...");
            }

        } catch (Exception e) {
            throw new Exception("Error actualizando cotizacion: " + e.getMessage());
        } finally {
            if (st != null) {
                st = null;
            }
        
        }
    }

    /**
     * anula todos los detalles de una cotizacion
     * @param accion
     * @throws Exception
     * @since 10 02 2011
     * @autor Ing. Iris Vargas
     */
    public String borrarCotDet(String accion) throws Exception {
        PreparedStatement st = null;
        Connection con = null;
        String query = "ANUL_COT_DET";
        ;
        try {
            con = this.conectarJNDI(query);
            String sql = obtenerSQL(query);
            st = con.prepareStatement(sql);
            st.setString(1, accion);
            return st.toString();

        } catch (Exception e) {
            throw new Exception("Error en la consulta de borrado de detalles: " + e.toString());
        } finally {
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("Error al cerrar la conexion " + e.getMessage());
                }
            }
            this.desconectar(con);
        }
    }
public String insertarCotizacion(Cotizacion ct, String usuario) throws Exception {
        String respuesta = "";
        StringStatement st = null;
        String query = query = "INSERTAR_ORDEN";
        try {
            st = new StringStatement(this.obtenerSQL(query), true);//JJCastro fase2
            st.setString(1, ct.getCodigo());
            st.setString(2, ct.getFecha());
            st.setString(3, ct.getAccion());
            st.setString(4, usuario);

            respuesta = st.getSql();

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e);
            throw e;
        } finally {
            if (st != null) {
                try {
                    st = null;
                } catch (Exception e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
}
            }
        }
        return respuesta;
    }

    public String insertarCotizacionDet(Cotizacion ct, String usuario) throws Exception {
        String respuesta = "";
        StringStatement st = null;
        String query = "INSERTAR_ORDEN_DETS";
        try {
            st = new StringStatement(this.obtenerSQL(query), true);
            st.setString(1, ct.getMaterial());
            st.setDouble(2, ct.getCantidad());
            st.setString(3, ct.getCodigo());
            st.setString(4, ct.getFecha());
            st.setString(5, ct.getObservacion());
            st.setString(6, ct.getAccion());
            st.setString(7, ct.getCompra());
            st.setDouble(8, ct.getCantidad_compra());
            st.setDouble(9, ct.getValor());
            st.setString(10,ct.getOficial());

            respuesta = st.getSql();

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e);
            throw e;
        } finally {
            if (st != null) {
                try {
                    st = null;
                } catch (Exception e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
        }
        return respuesta;
    }

    public String updateAccion(String accion,String usuario) throws Exception {
        String query = "UPD_COT";
        String sql = "";
        try {

        sql = this.obtenerSQL(query);
        sql = sql.replaceAll("#accion", accion);

                } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e);
            throw e;
        } finally {
            
        }
        return sql;
    }
    
     public String updateEstadoAccion(String accion,String usuario, String estado) throws Exception {
         StringStatement st = null;
        String query = "ACT_EST_ACCION";
        String sql = "";
        try {

        sql = this.obtenerSQL(query);
         st = new StringStatement(sql, true);
                st.setString(1,estado);
                st.setString(2, usuario);
                st.setString(3, accion);
                sql = st.getSql();
                } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e);
            throw e;
        } finally {
            if (st != null) {
                try {
                    st = null;
                } catch (Exception e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
        }
        return sql;
    }
   
     public String borrarDets(String accion) throws Exception {
        PreparedStatement st = null;
        Connection con = null;
        String query = "DEL_COT";
        ;
        try {
            con = this.conectarJNDI(query);
            String sql = obtenerSQL(query);
            st = con.prepareStatement(sql);
            st.setString(1, accion);
            return st.toString();

        } catch (Exception e) {
            throw new Exception("Error en la consulta de borrado de detalles: " + e.toString());
        } finally {
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("Error al cerrar la conexion " + e.getMessage());
                }
            }
            //this.desconectar(con);
        }
    }
}