
/*
 * MainDAO.java
 * Esta clase es la clase Padre de todos los DAO's que usan el sistema de
 * consultas a traves de XML
 *
 * Modificado Febrero de 2007
 */

package com.tsp.opav.model.DAOS;


import com.tsp.operation.model.beans.Sql;
import com.tsp.util.connectionpool.PoolManager;
import com.tsp.util.xml.QryDaoHandler;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.ResourceBundle;
import java.util.Vector;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.naming.NoInitialContextException;
import javax.sql.DataSource;
import org.apache.log4j.Logger;



/**
 * Clase Padre de todos los DAO's que usan el sistema de lectura de sentencias
 * SQL a traves de un archivo XML.
 *
 * @author     Alejandro Payares         -   apayares@mail.tsp.com
 * @author     Henry A. Osorio Gonzalez  -   hosorio@mail.tsp.com
 */
public class MainDAO {

    private ArrayList connectionsActive;
    private int connectionsActiveSize;

    //JJCASTRO

    /**
	 * Permite conectar el DAO con la base de datos recibida por parametros
	 *
	 * @param nombreBaseDatos el nombre de la base de datos asociada a la conexiondatabaseName
	 * @throws SQLException si no hay Conexion a la base de datos
	 * @return La Conexion a la base de datos
	 */
    @Deprecated
	public synchronized Connection conectarBDJNDI(String databaseName) throws SQLException {
               String metodo=new Throwable().fillInStackTrace().getStackTrace()[1].getMethodName();
                boolean manual = false;
                if ( this.databaseName != null && this.databaseName.length() > 0 ){
                    databaseName = this.databaseName;
                } // databaseName = obtenerBDConsulta("");
		try {
			Context contextoInicial = new InitialContext(); // Equivalente: new InitialContext(null).
			dataSource = (DataSource) contextoInicial.lookup("java:/comp/env/jdbc/" + databaseName);
                        //System.out.println("DATASOURCE NULL? " + (dataSource == null));
		}
		catch (javax.naming.NameNotFoundException e) {
			manual = true;
                        System.out.println("NAME NOT FOUND DB");
		}
		catch (NoInitialContextException ie) {
			manual = true;
                        System.out.println("NO INITIAL CONTEXT BD");
		}
		catch (NamingException e1) {
                    System.out.println("NAMING BD");
		}
		if (dataSource == null) {
			System.out.println("FUENTE DE DATOS NULA");
		}
		Connection con = dataSource.getConnection();
		if ((con == null) || con.isClosed()) {
			throw new SQLException("NO HAY CONEXION A LA BASE DE DATOS " + databaseName);
		}

			//this.listener.actionPerformed("conectarBDJNDI", "String databaseName");

		return con;
	}

    //JJCASTRO

    private void removeConnection(String consulta)
    {   boolean sw=true;
        for(int i=0;i<connectionsActive.size()&&sw;i++)
        {   if(consulta!=null&&((String)connectionsActive.get(i)).equals(consulta))
            {  // System.out.println("consulta "+consulta+" removida");
                connectionsActive.remove(i);
                sw=false;
            }
            else
            {   if(consulta==null)
                {   System.out.println("Error de null pointer exception en el xml "+xmlFile);
                }
            }
        }
    }

    private void printConnectionsActive()
    {   System.out.println("begin");
        for(int i=0;i<connectionsActive.size();i++)
        {   System.out.println("LA CONSULTA "+(String)connectionsActive.get(i)+" QUEDO ABIERTA MIENTRAS SE ABRIAN OTRAS");
        }
        System.out.println("end");

    }


    /**
     * Objeto usado para escribir informaci�n en el log del sistema.
     */
    private Logger logger;

    /**
     * Variable est�tica para almacenar el path donde residen los archivos XML.
     */
    private static String XML_PATH;

    /*
     * Segmento de inicializaci�n del path de archivos XML
     */
    static {
        ResourceBundle rb = ResourceBundle.getBundle(
        "com/tsp/util/connectionpool/db" );
        XML_PATH = rb.getString("opav.pathXmlDao");
    }

    /**
     * El objeto que administra el Pool de conexiones.
     */
    private PoolManager poolManager;

     /**
     * El objeto que contiene la fuente de datos
     */
    private DataSource dataSource;

    /**
     * Una tabla que lleva un registro de las diferentes conexiones a las
     * diferentes Bases de datos necesarias para llevar a cabo una transacci�n
     * de un DAO.
     */
    private Hashtable connectionInfo;

    /**
     * Se encarga de controlar el acceso al archivo XML
     * especificado
     */
    private QryDaoHandler handler;

    /**
     * El nombre de la base de datos actual del DAO
     */
    private String databaseName;

    /**
     * La ruta del directorio donde se encuentran los archivos XML
     * para realizar las consultas SQL.
     */
    private String url;

    /**
     * El nombre del archivo XML donde se encuentran guardadas las
     * consultas SQL
     */
    private String xmlFile;


    /**
     * El prefijo para identificar una conexi�n en la tabla connectionInfo.
     */
    private static final String CONNECTION_PREFIX = "Connection";

    /**
     * El prefijo para identificar el contador de accesos de conexion actuales
     * en la tabla connectionInfo.
     */
    private static final String COUNTER_PREFIX = "Counter";

    /**
     * Bandera para determinar cuando es necesario validar el archivo XML
     * y no caer en validaciones repetitivas e innecesarias
     */
    private boolean esNecesarioValidarXML = false;

    /**
     * Un vector para guardar las consultas para las cuales existe una conexi�n abierta.
     */
    private Vector consultas;

    public synchronized Connection conectarJNDI() throws Exception {  
        if (databaseName == null || databaseName.length() == 0) {
            throw new RuntimeException(
                    "El nombre de la base de datos no ha sido proporcionado, "
                    + "para indicar el nombre de la base de datos use el metodo "
                    + "setDatabaseName().");
        }
        try {
            Context contextoInicial = new InitialContext(); // Equivalente: new InitialContext(null).
            dataSource = (DataSource) contextoInicial.lookup("java:/comp/env/jdbc/" + databaseName);
            Connection con = dataSource.getConnection();
            if ((con == null) || con.isClosed()) {
                throw new SQLException("NO HAY CONEXION A LA BASE DE DATOS " + databaseName);
            }
            return con;
        } catch (NamingException e) {
            e.printStackTrace();
        }
        return null;
    }
    
    public synchronized Connection conectarJNDIDBLink(String dataBaseName) throws Exception {  
        if (dataBaseName == null || dataBaseName.length() == 0) {
            throw new RuntimeException("El nombre de la base de datos no ha sido proporcionado.");
        }
        try {
            Context contextoInicial = new InitialContext(); // Equivalente: new InitialContext(null).
            dataSource = (DataSource) contextoInicial.lookup("java:/comp/env/jdbc/" + dataBaseName);
            Connection con = dataSource.getConnection();
            if ((con == null) || con.isClosed()) {
                throw new SQLException("NO HAY CONEXION A LA BASE DE DATOS " + dataBaseName);
            }
            return con;
        } catch (NamingException e) {
            e.printStackTrace();
        }
        return null;
    }
    
    /**
     * Permite crear una instancia de la clase MainDAO
     * @param xmlFile El nombre del archivo XML donde se encuentran guardadas
     * las consultas SQL.
     */
    @Deprecated
    public MainDAO(String xmlFile){
        this(xmlFile,(String)null);
        if( connectionsActive==null)
        {    connectionsActive=new ArrayList();
             connectionsActiveSize=0;
        }
    }

    /**
     * Permite crear una instancia de la clase MainDAO
     * @param xmlFile El nombre del archivo XML donde se encuentran guardadas
     * las consultas SQL.
     * @param c La clase que aparecera en el log cuando ocurra algun error.
     */
    @Deprecated
    public MainDAO(String xmlFile, Logger logger) {
        this(xmlFile,(String)null);
        this.logger = logger;
    }

    /**
     * Permite crear una instancia de la clase MainDAO
     * @param databaseName El nombre de la base de datos a la que se conectar�
     * el DAO.
     * @param xmlFile El nombre del archivo XML donde se encuentran guardadas
     * las consultas SQL.
     */
    public MainDAO(String xmlFile, String databaseName) {
        setURL(XML_PATH);
        poolManager = PoolManager.getInstance();
        handler = new QryDaoHandler();
        setXmlFile(xmlFile);
        connectionInfo = new Hashtable();
        this.databaseName = databaseName;
        if ( databaseName != null ) {
            logger = Logger.getLogger(MainDAO.class);
        }
        if( connectionsActive==null)
        {    connectionsActive=new ArrayList();
             connectionsActiveSize=0;
        }
    }


    /**
     * Permite conectar el DAO con la base de datos asociada al nombre de la
     * consulta dada. Ignora el nombre de la base de datos actual del DAO.
     * @see <B>Este m�todo modifica el nombre de la base de datos actual del DAO</B>
     * @param nombreConsulta el nombre de la consulta a la cual esta asociado
     * el nombre de la base de datos necesaria para conectarse.
     * @throws SQLException si no hay conexi�n a la base de datos
     * @return La conexi�n a la base de datos asociada al nombre de la consulta dada.
     */
    @Deprecated
    public synchronized Connection conectar(String nombreConsulta) throws SQLException {
        if(connectionsActiveSize!=connectionsActive.size())
        {   printConnectionsActive();
            connectionsActiveSize=connectionsActive.size();
        }
        if(nombreConsulta != null)
        {   connectionsActive.add(nombreConsulta);
        }
        else
        {   System.out.println("Error de null pointer exception en el xml "+xmlFile);
        }
        esNecesarioValidarXML = true;
        setXmlFile(xmlFile);
        String dataBaseName = obtenerBDConsulta(nombreConsulta);
        if ( dataBaseName == null || dataBaseName.length() == 0 ){
            throw new RuntimeException(
            "El nombre de la base de datos no ha sido proporcionado, " +
            "para indicar el nombre de la base de datos use el metodo " +
            "setDatabaseName(), o tambien puede usar el metodo " +
            "conectar(String nombreConsulta) el cual obtiene el nombre de la" +
            " base de datos asociado a la consulta.");
        }
        Connection con = (Connection) connectionInfo.get(dataBaseName + CONNECTION_PREFIX );
        Entero c = (Entero) connectionInfo.get(dataBaseName + COUNTER_PREFIX );
        if ( (con == null || con.isClosed()) && (c == null || c.getValor() == 0) ){
            con = poolManager.getConnection(dataBaseName);
            if ((con == null) || con.isClosed())
                throw new SQLException("NO HAY CONEXION A LA BASE DE DATOS "+dataBaseName);
            c = new Entero(0);
            connectionInfo.put(dataBaseName + CONNECTION_PREFIX, con);
            connectionInfo.put(dataBaseName + COUNTER_PREFIX, c );
            if ( consultas == null ) {
                consultas = new Vector();
            }
        }
        consultas.addElement(dataBaseName + nombreConsulta);
        c.incrementar();
        return con;
    }

    /**
     * Permite conectar el DAO con la base de datos asociada al nombre de la
     * consulta dada. Ignora el nombre de la base de datos actual del DAO.
     * @see <B>Este m�todo modifica el nombre de la base de datos actual del DAO</B>
     * @param nombreConsulta el nombre de la consulta a la cual esta asociado
     * el nombre de la base de datos necesaria para conectarse.
     * @throws SQLException si no hay conexi�n a la base de datos
     * @return La conexi�n a la base de datos asociada al nombre de la consulta dada.
     */
    @Deprecated
    public synchronized Connection conectarJNDI(String nombreConsulta) throws SQLException {

        esNecesarioValidarXML = true;
        setXmlFile(xmlFile);
            //validamos que se cre� la sesion.          
            String dataBaseName = obtenerBDConsulta(nombreConsulta);
            if ( dataBaseName == null || dataBaseName.length() == 0 ){
            throw new RuntimeException(
            "El nombre de la base de datos no ha sido proporcionado, " +
            "para indicar el nombre de la base de datos use el metodo " +
            "setDatabaseName(), o tambien puede usar el metodo " +
            "conectar(String nombreConsulta) el cual obtiene el nombre de la" +
            " base de datos asociado a la consulta.");
        }
        /*Obteniendo la conexion por medio de jndi*/
        try {
            Context contextoInicial = new InitialContext(); // Equivalente: new InitialContext(null).
                dataSource = (DataSource) contextoInicial.lookup("java:/comp/env/jdbc/"+dataBaseName);
            Connection con = dataSource.getConnection();
            if ((con == null) || con.isClosed())
                throw new SQLException("NO HAY CONEXION A LA BASE DE DATOS "+dataBaseName);
            return con;
        } catch(NamingException e) {
            e.printStackTrace();
        }
        return null;
    }
    @Deprecated
    public synchronized Connection conectarJNDI(String nombreConsulta, String dataBaseName ) throws SQLException {


        if (dataBaseName.isEmpty() ) {
            esNecesarioValidarXML = true;
            setXmlFile(xmlFile);
            dataBaseName = obtenerBDConsulta(nombreConsulta);
            if ( dataBaseName == null || dataBaseName.length() == 0 ){
                throw new RuntimeException(
                "El nombre de la base de datos no ha sido proporcionado, " +
                "para indicar el nombre de la base de datos use el metodo " +
                "setDatabaseName(), o tambien puede usar el metodo " +
                "conectar(String nombreConsulta) el cual obtiene el nombre de la" +
                " base de datos asociado a la consulta.");
            }
            /*Obteniendo la conexion por medio de jndi*/
            try {
                Context contextoInicial = new InitialContext(); // Equivalente: new InitialContext(null).
                dataSource = (DataSource) contextoInicial.lookup("java:/comp/env/jdbc/"+dataBaseName);
                Connection con = dataSource.getConnection();
                if ((con == null) || con.isClosed())
                    throw new SQLException("NO HAY CONEXION A LA BASE DE DATOS "+dataBaseName);
                return con;
            } catch(NamingException e) {
                e.printStackTrace();
            }
            return null;
        }
        else{

            setXmlFile(xmlFile);
            if (databaseName != null && databaseName.length() > 0) {
                 dataBaseName = databaseName;
            } //dataBaseName = obtenerBDConsulta("");
            Context contextoInicial = null;
            if (dataBaseName == null || dataBaseName.length() == 0) {
                throw new RuntimeException("El nombre de la base de datos no ha sido proporcionado en " +
                                           "el metodo conectarJNDI " );
            }
            /* Obteniendo la conexion por medio de jndi */
            try {
                contextoInicial = new InitialContext(); // Equivalente: new InitialContext(null).
                dataSource      = (DataSource) contextoInicial.lookup("java:/comp/env/jdbc/" + dataBaseName);

            }
            catch (javax.naming.NameNotFoundException e) {
                System.out.println("NAME NOT FOUND");
            }
            catch (NoInitialContextException ie) {
                System.out.println("NO INITIAL CONTEXT");
            }
            catch (NamingException e1) {
                System.out.println("NAMING");
            }
            if (dataSource == null) {
                System.out.println("FUENTE DE DATOS NULA");
            }
            Connection con = dataSource.getConnection();
            if ((con == null) || con.isClosed()) {
                System.out.println("NO HAY CONEXION A LA BASE DE DATOS");
                throw new SQLException("NO HAY CONEXION A LA BASE DE DATOS " + dataBaseName);
            }
            return con;
        }
    }

    /**
     * Permite obtener la sentencia SQL que se encuentra almacenada en el
     * archivo XML especificado previamente.
     * @param nombreConsulta el nombre de la consulta en el archivo XML
     * @return la sentencia SQL asociada al nombre de la consulta en el archivo XML
     */
    public String obtenerSQL(String nombreConsulta){
        esNecesarioValidarXML = true;
        setXmlFile(xmlFile);
        Sql sql = handler.getQry(nombreConsulta);
        if ( sql == null ){
            throw new RuntimeException("La sentencia SQL asociada a la consulta "+nombreConsulta+" no fue encontrada en el archivo: "+url+xmlFile);
        }
        return sql.getSql();
    }


    /**
     * Permite obtener el nombre de la base de datos asociada al nombre de la
     * consulta en el archivo XML.
     * @param nombreConsulta el nombre de la consulta en el archivo XML
     * @return el nombre de la base de datos asociada
     */
    public String obtenerBDConsulta(String nombreConsulta){
        try {
            if (databaseName == null || databaseName.length() == 0) {
                throw new Exception("no tiene base datos");
                /*esNecesarioValidarXML = true;
                setXmlFile(xmlFile);
                Sql sql = handler.getQry(nombreConsulta);
                if ( sql == null ){
                    throw new RuntimeException("El nombre de la base de datos asociada a la consulta "+nombreConsulta+" no fue encontrado en el archivo: "+url+xmlFile);
                }
                return sql.getDb();*/
            } else {    
                return databaseName;
            }
        } catch(Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }


    /**
     * Permite obtener un PreparedStatement a partir del nombre de la consulta dada.
     * El nombre de la consulta debe estar en el archivo XML especificado y del
     * cual es sacado el nombre de la Base de datos indicada en la consulta
     * que tiene ese nombre.
     * @param nombreConsulta el nombre de la consulta a la cual esta asociado
     * el nombre de la base de datos necesaria para conectarse y crear el PreparedStatement.
     * @throws SQLException si el nombre de la consulta no existe en el archivo XML,
     * o si el nombre de la base de datos asociado al nombre de la consulta no puede
     * conectarse en el Pool de conexxiones.
     * @return un PreparedStatement que contiene la consulta asociada al nombre
     * de la consulta dada.
     */
    public synchronized PreparedStatement crearPreparedStatement(String nombreConsulta) throws SQLException {
        Connection c = conectar(nombreConsulta);
        return c.prepareStatement(obtenerSQL(nombreConsulta));
    }

    /**
     * Permite obtener un CallableStatement a partir del nombre de la consulta dada.
     * El nombre de la consulta debe estar en el archivo XML especificado y del
     * cual es sacado el nombre de la Base de datos indicada en la consulta
     * que tiene ese nombre.
     * @param nombreConsulta el nombre de la consulta a la cual esta asociado
     * el nombre de la base de datos necesaria para conectarse y crear el PreparedStatement.
     * @throws SQLException si el nombre de la consulta no existe en el archivo XML,
     * o si el nombre de la base de datos asociado al nombre de la consulta no puede
     * conectarse en el Pool de conexxiones.
     * @return un PreparedStatement que contiene la consulta asociada al nombre
     * de la consulta dada.
     */
    public CallableStatement crearCallableStatement(String nombreConsulta) throws SQLException {
        Connection c = conectar(nombreConsulta);
        return c.prepareCall(obtenerSQL(nombreConsulta));
    }

    /**
     * Permite liberar la conexi�n asociada al nombre de la consulta dada del Pool de conexiones.
     * @see <b>este metodo modifica el nombre de la base de datos actual del DAO</b>
     * @param nombreConsulta el nombre de la consulta a la cual esta asociado
     * el nombre de la base de datos necesaria para liberar la conexion.
     * @throws SQLException Si la liberaci�n de la conexi�n no pudo efectuarse.
     */
    public synchronized void desconectar(String nombreConsulta) throws SQLException {
        this.removeConnection(nombreConsulta);
        String dataBaseName = obtenerBDConsulta(nombreConsulta);
        if ( consultas == null || !consultas.contains(dataBaseName + nombreConsulta) ) {
            return;
        }
        Connection con = (Connection) connectionInfo.get(dataBaseName + CONNECTION_PREFIX );
        Entero c = (Entero) connectionInfo.get(dataBaseName + COUNTER_PREFIX );
        if ( con != null && !con.isClosed() && c.getValor() == 1 ){
            con = (Connection) connectionInfo.remove(dataBaseName + CONNECTION_PREFIX);
            connectionInfo.remove(dataBaseName + COUNTER_PREFIX );
            poolManager.freeConnection(dataBaseName, con);
        }
        if ( c != null && c.getValor() > 0 ){
            c.decrementar();
        }
        consultas.removeElement(dataBaseName + nombreConsulta);
    }
     /**
     * Permite liberar la conexi�n asociada al nombre de la consulta dada
     * @param con El objeto que contiene la conexion a la base de datos.
     * @throws SQLException Si la liberaci�n de la conexi�n no pudo efectuarse.
     */
    public synchronized void desconectar(Connection con) throws SQLException {
        String metodo=new Throwable().fillInStackTrace().getStackTrace()[1].getMethodName();
        System.out.print("metodo se desconecto "+metodo);
        //if ( con != null ){
        if ( con != null && !(con.isClosed())){//20101119
            con.close();
        }
    }

    /**
     * Retorna el valor del URL actual donde est�n almacenados los archivos XML.
     * @return el URL del directorio donde se encuentra el archivo XML de donde
     * se leen las consultas SQL.
     */
    public String getURL() {
        return url;
    }

    /**
     * Establece la cadena que formar� el URL del directorio donde se encuentran
     * los archivos XML de donde se extrane las consultas SQL.
     * @param url el valor del directorio donde se encuentran los archivos XML.
     */
    public void setURL(String url) {
        if ( xmlFile != null && esNecesarioValidarXML ){
            validarXML(url,xmlFile);
            esNecesarioValidarXML = !handler.XMLHasidoValidado();
        }
        this.url = url;
    }

    /**
     * Retorna el nombre de la Base de datos actual
     * @return el valor del nombre de la base de datos.
     */
    public String getDatabaseName() {
        return databaseName;
    }

    /**
     * Establece el nombre de la Base de datos.
     * @param databaseName nuevo valor para el nombre de la Base de datos
     */
    public void setDatabaseName(java.lang.String databaseName) {
        this.databaseName = databaseName;
    }

    /**
     * Retorna el nombre del archivo XML de donde se leen las consultas SQL
     * @return el valor del nombre del archivo XML.
     */
    public java.lang.String getXmlFile() {
        return xmlFile;
    }

    /**
     * Establece el nombre del archivo XML de donde  se leen las consultas SQL
     * @param xmlFile el nuevo valor del nombre del archivo XML.
     */
    public void setXmlFile(java.lang.String xmlFile) {
        if ( url != null && esNecesarioValidarXML ){
            validarXML(url,xmlFile);
            esNecesarioValidarXML = !handler.XMLHasidoValidado();
        }
        this.xmlFile = xmlFile;
    }

    /**
     * Valida el archivo XML que se encuentra en el URL dado
     * @param URL La ruta donde se encuentra el archivo XML a validar
     * @param XML El nombre del archivo XML
     */
    private void validarXML(String URL, String XML){
        if ( URL == null || XML == null ){
            throw new IllegalArgumentException("No se puede validar el archivo XML -> url = " + URL + ", archivo XML = " + XML );
        }
        handler.parse(URL +XML);
    }

    /**
     * Envia al log el String recibido por parametro.
     * @param texto el String que ser� enviado al log.
     */
    protected void logg(String texto){
        logger.info(texto);
    }

    /**
     * Envia al log la pila de seguimiento de la Excepcion recibida por parametro.
     * @param t la excepcion a imprimir en el log.
     */
    protected void logg(Throwable t){
        logger.error("Error: "+getStackTrace(t));
    }

    /**
     * Devuelve un String con la pila de seguimiento de la excepcion dada.
     * @param t la excepcion cuya pila de seguimiento ser� concatenada en un String.
     * @return un String con la pila de seguimiento de t.
     */
    protected String getStackTrace(Throwable t){
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        BufferedOutputStream bos = new BufferedOutputStream(baos);
        PrintStream ps = new PrintStream(bos);
        t.printStackTrace(ps);
        ps.flush();
        return baos.toString();
    }


    /**
     * Clase que encapsula un dato primitivo int dentro de un objeto, como lo hace
     * la clase java.lang.Integer, pero que a diferencia de la clase java.lang.Integer,
     * el entero encapsulado en un Entero puede ser modificado para poder llevar el
     * conteo de los accesos de conexi�n a una base de datos y de esa forma pueda
     * ser almacenado en el Hashtable, connectionInfo.
     */
    private class Entero {

        /**
         * El valor del entero.
         */
        private int valor;

        /**
         * Construye un Entero con el valor dado.
         */
        public Entero(int valor){
            this.valor = valor;
        }

        /**
         * Retorna el valor int del Entero.
         */
        public int getValor(){
            return valor;
        }

        /**
         * Establece el valor del Entero.
         */
        public void setValor(int valor){
            this.valor = valor;
        }

        /**
         * Aumenta en 1 el valor del Entero.
         */
        public void incrementar(){
            valor++;
        }

        /**
         * Disminuye en 1 el valor del Entero.
         */
        public void decrementar(){
            valor--;
        }

        /**
         * Devuelve una representaci�n de Cadena de caracteres
         * del valor del Entero.
         */
        public String toString(){
            return ""+valor;
        }
    }

}