/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.opav.model.DAOS;

import com.google.gson.JsonObject;
import com.tsp.operation.model.beans.Usuario;
/**
 *
 * @author user
 */
public interface CotizacionSlDAO {
    
    public JsonObject cargarGridAccion(String apugrupo, String metcalc, String id);

    public String cargardatoscabecera(String id);

    public JsonObject cargarAPUs();

    public JsonObject cargarAPUXGrupo(String grupo_apu, String apugrupo);

    public JsonObject guardarCotizacion(JsonObject info, JsonObject info_detalle);

    public String cargarMetodoCalculo();

    public String cargarMetodoCalculoXLinea(String metodo, String codigo);

    public boolean existeCotizacion(String id);

    //public String existeCotizacion(String id);
    
    public String datosExistecabecera(String id);
    
    public JsonObject cargarGridAccionExiste(String accion);

    public JsonObject cargarTablaDetallada(String id_accion);

    public JsonObject cargarTablaDetalladaExiste(String id_accion);

    public String cargarMetodoCalculo_w();

    public String cargarMetodoCalculoXLinea_w(String metodo, String codigo);

    public boolean existeCotizacion_w(String id);

    public JsonObject cargarGridAccion_w(String apugrupo, String metcalc, String id);

    public JsonObject cargarGridAccionExiste_w(String id,String op,String filtro,String id_rel_actividades_apu);

    public String cargardatoscabecera_w(String id);

    public String datosExistecabecera_w(String id);

    public JsonObject cargarAPUs_w();

    public JsonObject cargarAPUXGrupo_w(String grupo_apu, String apugrupo);

    public JsonObject guardarCotizacion_w(JsonObject info, JsonObject info_detalle);

    public JsonObject cargarTablaDetallada_w(String id_accion);

    public JsonObject cargarTablaDetalladaExiste_w(String id_accion);

    public String cargar_Combo_W(String op);

    public String cargar_Porcentajes(String idaccion);
    
    public String cargarCostosAdmon(String Query, String id_categoria, String num_solicitud);
    
    public String listarUndsAdmon();
    
    public String listarItemsCategoria(String id_categoria);
    
    public String guardarCostosAdmon(String num_solicitud,String id_categoria,JsonObject infoItems, Usuario usuario);
    
    public String guardarCostosAdmon(String num_solicitud,JsonObject infoItems, Usuario usuario);
    
    public boolean existenCostosAdmonProyecto(String num_solicitud);
    
    public String anularCostoAdmonProyecto(String id, Usuario usuario);

    public String insertar_Porcentajes_Global_Contratista(String id, String porcentaje, String idaccion);

    public String insertar_rentabilidades_cabecera(String idaccion, String subtotal, String valor_contratista,String perc_contratista, String valor_esquema , String perc_esquema);
    
    public String obtenerValorItemByDefault(String id_categoria, String id_item);      

    public String insertar_Porcentajes_Contratista(JsonObject rows, String idaccion);

    public String cargar_totales_categoria(String idaccion, String idtipoinsumo);

    public String cargar_totales_tipo_insumo(String idaccion);

}
