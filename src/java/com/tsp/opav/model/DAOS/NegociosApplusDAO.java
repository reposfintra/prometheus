/*
 * NegociosApplusDAO.java
 * Created on 2 de febrero de 2009, 17:33
 */
package com.tsp.opav.model.DAOS;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.tsp.opav.model.DAOS.MainDAO;
import java.sql.*;
import java.util.ArrayList;
import com.tsp.opav.model.beans.NegocioApplus;
import java.io.ByteArrayInputStream;
import java.util.Dictionary;
import com.tsp.util.Utility;
import java.util.List;
import java.util.LinkedList;
import com.tsp.operation.model.beans.Imagen;
import java.io.InputStream;
import java.io.File;
import java.io.FileOutputStream;
import com.tsp.operation.model.beans.FacturaContratista;
import com.tsp.operation.model.beans.Accord;
import java.util.TreeMap;
import com.tsp.operation.model.TransaccionService;
import com.tsp.operation.model.threads.HSendMail2;
import com.tsp.opav.model.beans.Material;
import com.tsp.operation.model.beans.BeanGeneral;//20100219
import java.util.Vector;//20100219
import com.tsp.operation.model.beans.*;//JJCastro fase2



/** * @author  Fintra */
public class NegociosApplusDAO  extends MainDAO {

    public NegociosApplusDAO() {
        super("NegociosApplusDAO.xml");
    }
    public NegociosApplusDAO(String dataBaseName) {
        super("NegociosApplusDAO.xml", dataBaseName);
    }

    public ArrayList getNegociosApplus(String estado,String contratista,String numosxi,String factconformed,String loginx) throws Exception{
        Connection         con     = null;
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        String             query   = "SQL_SEARCH_NEGOCIOS";
        ArrayList listNegocios=new ArrayList();
        boolean respuesta=false;
        try{
            con = this.conectarJNDI(query);
            String sql    =   this.obtenerSQL( query );

            if (contratista!=null && !(contratista.equals(""))){
                sql=sql.replaceAll("contratixx", "'"+contratista+"'");
            }else{
                sql=sql.replaceAll("contratixx", "a.id_contratista");
            }

            if (factconformed!=null && !(factconformed.equals(""))){
                sql=sql.replaceAll("factconformedxx", "'"+factconformed+"'");
            }else{
                sql=sql.replaceAll("factconformedxx", "a.fact_conformada");
            }

            if (numosxi!=null && !(numosxi.equals(""))){
                sql=sql.replaceAll("numosxx", "'"+numosxi+"'");
            }else{
                sql=sql.replaceAll("numosxx", "o.num_os");
            }

            if (estado!=null && !(estado.equals("")) && !(estado.equals("99"))){
                sql=sql.replaceAll("estadito", "'"+estado+"'");
            }else{
                if (numosxi!=null && !(numosxi.equals(""))){
                    sql=sql.replaceAll("estadito", "o.id_estado_negocio");
                }else{
                    sql=sql.replaceAll("estadito", "'"+estado+"'");
                }
            }

            //sql=sql.replaceAll("condicionrara1", "  AND ('"+loginx+"' IN (SELECT table_code FROM tablagen WHERE table_type ='USR_VIP_MS') OR f_facturado_cliente>='2009-01-01' OR f_facturado_cliente='--' ) ");

            //sql=sql.replaceAll("condicionrara2", "  AND ('"+loginx+"' IN (SELECT table_code FROM tablagen WHERE table_type ='USR_RED_MS') OR id_estado_negocio!='11' OR (id_estado_negocio='11' AND (SELECT fecha_prefactura FROM app_accord WHERE id_accion=a.id_accion)<='2009-04-01')) ");

            //sql=sql.replaceAll("condicionrara1", "  AND ('"+loginx+"' IN (SELECT table_code FROM tablagen WHERE table_type ='USR_VIP_MS') OR f_facturado_cliente>='2009-01-01' OR f_facturado_cliente='--' OR (SELECT prefactura FROM app_accord WHERE id_accion=a.id_accion)!='') ");
            sql=sql.replaceAll("condicionrara1", " ");
            sql=sql.replaceAll("condicionrara2", "  AND ('"+loginx+"' IN (SELECT table_code FROM tablagen WHERE table_type ='USR_RED_MS') OR (f_facturado_cliente>='2009-04-01'  OR f_facturado_cliente='--' OR (SELECT prefactura FROM app_accord WHERE id_accion=a.id_accion)!='')) ");

            st            =   con.prepareStatement( sql );

            //.out.println("sql"+sql);
            //st.setString(1,estado);
            rs = st.executeQuery();
            while (rs.next()){
                //ystem.out.println("1 fila");
                //ystem.out.println("un dato leido::"+rs.getString("id"));
                NegocioApplus negocioApplus=new NegocioApplus();
                //anticipoGasolina.setDstrct(rs.getString("dstrct"));
                negocioApplus.setNumOs(rs.getString("num_os"));
                negocioApplus.setId(rs.getString("id_orden"));
                negocioApplus.setIdAccion(rs.getString("id_accion"));
                negocioApplus.setVlr(rs.getString("total_prev1"));
                negocioApplus.setIdCliente(rs.getString("id_cliente"));
                negocioApplus.setNombreCliente(rs.getString("nombre_cliente"));
                negocioApplus.setTelCli(rs.getString("telefono"));
                negocioApplus.setContacto(rs.getString("contacto"));
                negocioApplus.setIdContratista(rs.getString("id_contratista"));
                negocioApplus.setNombreContratista(rs.getString("descripcion"));
                negocioApplus.setCuotas(rs.getString("cuotas"));
                negocioApplus.setValCuotas(rs.getString("valor_cuotas_r"));
                negocioApplus.setEstudio(rs.getString("estudio_economico"));
                negocioApplus.setSimbolo(rs.getString("simbolo_variable"));
                negocioApplus.setFacturaConformada(rs.getString("fact_conformada"));
                negocioApplus.setFecha(rs.getString("f_facturado_cliente"));
                negocioApplus.setObservacion(rs.getString("detalle_inconsistencia"));

                negocioApplus.setObservacionOpen(rs.getString("observacion_open"));

                negocioApplus.setOferta(rs.getString("oferta"));
                if (negocioApplus.getObservacion()==null){negocioApplus.setObservacion("");}
                listNegocios.add(negocioApplus);
                negocioApplus.setEcaOferta(rs.getString("eca_oferta"));
                //negocioApplus.setDifEcaOferta(rs.getString("dif_eca_oferta"));
                //negocioApplus.setDifOferta(rs.getString("dif_oferta"));
                negocioApplus.setDifEcaOfertaConsorcioAntiguo(rs.getString("dif_ecaoferta_consorcio_antiguo"));
                negocioApplus.setDifEcaOfertaConsorcioNuevo(rs.getString("dif_ecaoferta_consorcio_nuevo"));
                negocioApplus.setDifOfertaApplusAntiguo(rs.getString("dif_oferta_applus_antiguo"));
                negocioApplus.setDifOfertaApplusNuevo(rs.getString("dif_oferta_applus_nuevo"));

                negocioApplus.setEsquemaComision(rs.getString("esquema_comision"));

                negocioApplus.setPrefactura(rs.getString("prefactura"));
                negocioApplus.setFacturaEca(rs.getString("factura_eca"));
                negocioApplus.setFacturaContratista(rs.getString("factura_contratista"));
                negocioApplus.setFacturaRetencion(rs.getString("factura_retencion"));
                negocioApplus.setFacturaBoni(rs.getString("factura_bonificacion"));
                negocioApplus.setNicClient(rs.getString("nic"));
                negocioApplus.setNitClient(rs.getString("nit"));

                negocioApplus.setFacturaApp(rs.getString("factura_app"));
                negocioApplus.setFacturaPro(rs.getString("factura_pro"));
                negocioApplus.setFacturaComiEca(rs.getString("factura_comision_eca"));
                negocioApplus.setEsquemaFinanciacion(rs.getString("esquema_financiacion"));

                negocioApplus.setEstado(rs.getString("estado"));
                negocioApplus.setFRecepcion(rs.getString("f_recepcion"));
                negocioApplus.setTipoCliente(rs.getString("tipo_identificacion"));

                negocioApplus.setFacturaFactoringPro(rs.getString("factura_formula_provintegral"));

                negocioApplus.setAcciones(rs.getString("acciones"));
                //negocioApplus.setIdSolicitud(rs.getString("id_solicitud"));//090922
            }

        }catch(Exception e){
            System.out.println("error en NegociosApplusDAO..."+e.toString()+"__"+e.getMessage());
            throw new Exception(e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return listNegocios;
    }








    public String CambiarEstado(String[] ofertas,String nuevo_estado,String observacion,String esquema_comision,String svx,
     String contratista_consultar,String fact_conformed,String cuoticas,String fecfaccli,String fact_conformada_consultar,
     String esquema_financiacion,String userx) throws Exception{
        String oferticas="";
        for (int i=0;i<ofertas.length;i++){
            oferticas=oferticas+ofertas[i]+",";
        }

        Connection         con     = null;
        PreparedStatement  st      = null;
        String             query   = "SQL_CAMBIAR_ESTADO";
        String consultaSQL ="";
        int affected=0 ;
        try{
            con = this.conectarJNDI(query);//JJCastro fase2

            String sql    =   this.obtenerSQL( query );

            if (observacion!=null && !(observacion.equals(""))){
            	//sql=sql.replaceAll("detallexx","'"+observacion+"'");
                sql=sql.replaceAll("detallexx","detalle_inconsistencia || '<br>------- ' || current_date || ' _ " + userx+": "+""+observacion+"'");
            }else{
                sql=sql.replaceAll("detallexx","detalle_inconsistencia");
            }

            if (esquema_comision!=null && !(esquema_comision.equals(""))){
            	sql=sql.replaceAll("esquemaxx","'"+esquema_comision+"'");
            }else{
                sql=sql.replaceAll("esquemaxx","esquema_comision");
            }

            if (svx!=null && !(svx.equals(""))){
            	sql=sql.replaceAll("svxx","'"+svx+"'");
            }else{
                sql=sql.replaceAll("svxx","simbolo_variable");
            }

            if (cuoticas!=null && !(cuoticas.equals(""))){
            	sql=sql.replaceAll("cuoticasxx","'"+cuoticas+"'");
            }else{
                sql=sql.replaceAll("cuoticasxx","cuotas");
            }

            if (contratista_consultar!=null && !(contratista_consultar.equals(""))){
            	sql=sql.replaceAll("contratistaxx","'"+contratista_consultar+"'");
            }else{
                sql=sql.replaceAll("contratistaxx","id_contratista");
            }

            if (fact_conformada_consultar!=null && !(fact_conformada_consultar.equals(""))){
            	sql=sql.replaceAll("fact_conformadaxx","'"+fact_conformada_consultar+"'");
            }else{
                sql=sql.replaceAll("fact_conformadaxx","fact_conformada");
            }

            if (fact_conformed!=null && !(fact_conformed.equals(""))){
            	sql=sql.replaceAll("factconfxx","'"+fact_conformed+"'");
            }else{
                sql=sql.replaceAll("factconfxx","fact_conformada");
            }

            if (fecfaccli!=null && !(fecfaccli.equals(""))){
            	sql=sql.replaceAll("fecfacclixx","'"+fecfaccli+"'");
            }else{
                sql=sql.replaceAll("fecfacclixx","f_facturado_cliente");
            }
            if (esquema_financiacion!=null && !(esquema_financiacion.equals(""))){
            	sql=sql.replaceAll("esquema_financiacionxx","'"+esquema_financiacion+"'");
            }else{
                sql=sql.replaceAll("esquema_financiacionxx","esquema_financiacion");
            }
            sql=sql.replaceAll("user_updatexx","'"+userx+"'");

            sql=sql.replaceAll("oferticas",oferticas.substring(0,(oferticas.length()-1) ));
            //ystem.out.println("sql::"+sql);

            if (nuevo_estado!=null && !(nuevo_estado.equals("nada"))){//090721
                sql=sql.replaceAll("estadinho","'"+nuevo_estado+"'");
            }else{
                sql=sql.replaceAll("estadinho","id_estado_negocio");
            }

            st = con.prepareStatement( sql );
            //st.setString(1, nuevo_estado);
            //st.setString(2, observacion);
            //.out.println("sqlll:"+st.toString());
            st.execute();

        }catch(Exception e){
            System.out.println("error en CambiarEstado en NegociosApplusDAO..."+e.toString()+"__"+e.getMessage());
            throw new Exception(e.getMessage());
        }
        finally{
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return "prueba";
    }



    public ArrayList getEstadosApplus() throws Exception{
        Connection         con     = null;
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        String             query   = "SQL_SEARCH_ESTADOS";
        ArrayList listEstados=new ArrayList();
        boolean respuesta=false;
        try{
            con = this.conectarJNDI(query);//JJCastro fase2
            String sql    =   this.obtenerSQL( query );
            st            =   con.prepareStatement( sql );
            String[] estado;
            rs = st.executeQuery();
            while (rs.next()){
                estado=new String[2];
                estado[0]=rs.getString("codigo");
                estado[1]=rs.getString("estado");
                listEstados.add(estado);
            }

        }catch(Exception e){
            System.out.println("error en NegociosApplusDAO.getEstadosApplus.."+e.toString()+"__"+e.getMessage());
            throw new Exception(e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return listEstados;
    }

    public void insertImagen(ByteArrayInputStream bfin, int longitud,  Dictionary fields, String idxx5,String tipito ) throws SQLException {
         //ystem.out.println("idxx5"+idxx5);
        Connection con = null;
        PreparedStatement  st    = null;

        String             query = "SQL_INSERT_ARCHIVO";
        if (idxx5!=null && !(idxx5.equals(""))){
           query = "SQL_UPDATE_ARCHIVO";
        }
        try{

            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2;

                  String actividad      =  (String)fields.get("actividad");
                  String tipoDocumento  =  (String)fields.get("tipoDocumento");
                  String documento      =  (String)fields.get("documento");
                  String user           =  (String)fields.get("usuario");
                  String agencia        =  (String)fields.get("agencia");
                  String filename       =  (String)fields.get("archivo");

                  if(documento.equals(""))
                       documento = Utility.getHoy("");


                  st.setString       (1, documento);
                  st.setString       (2, filename);

                  st.setBinaryStream (3, bfin, longitud);
                  st.setString       (4, user);
                  st.setString       (5, agencia);
                  st.setString       (6, tipito);

                  if (idxx5!=null && !(idxx5.equals(""))){st.setString       (7, idxx5);}


            st.execute();

            } }catch(Exception e){
            System.out.println("nooo:"+e.toString()+"__"+e.getMessage());
            throw new SQLException("Error insertImagen() : "+ e.getMessage());
        }finally{
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }

    public List searcArchivos(

                                 String documento
                                ,String ruta ,String tipito
                               ) throws SQLException {

        PreparedStatement  st    = null;
        ResultSet          rs    = null;
        List               lista = null;
        String             query = "SQL_SEARCH_ARCHIVOS";
        Connection         con   = null;

        String             sql   = "";

        try{
            File carpeta =new File(ruta);
            //boolean borrar=carpeta.delete();
            //ystem.out.println("borrar::"+borrar);

            deleteDirectory(carpeta);
            this.createDir(ruta);
            con = this.conectarJNDI(query);//JJCastro fase2

            sql          =  this.obtenerSQL( query );

            String where = " WHERE  reg_status ='' ";
            String resto = " and tipo = '"+tipito+"' ";
            resto += " and  (id)               = ('"+ documento + "') ";

            sql += where + resto;

            //ystem.out.println("La consulta sql es: "+sql);
            st  =   con.prepareStatement( sql );
            rs  =   st.executeQuery();
            while(rs.next()){
                if( lista == null) lista = new LinkedList();
                Imagen imagen = new Imagen();
                   imagen.setActividad     ( rs.getString("id")      );

                   imagen.setDocumento     ( rs.getString("document")      );
                   imagen.setNameImagen    ( rs.getString("filename")      );
                   imagen.setBinary        ( rs.getBinaryStream("filebinary"));
                   imagen.setCreation_user ( rs.getString("creation_user"));
                   String fileName        = imagen.getNameImagen();
                   String[] name          = fileName.split(".");
                   if(name.length>0)
                          fileName        = name[0] + rs.getString("filename").replaceAll(" |.|-|:","") + name[1];

                   imagen.setFileName      ( fileName);
                   imagen.setFecha_creacion (rs.getString("creation_date").substring(0,16));
                   try{

                 //--- Eliminamos la Imagen
                 //      delete(ruta,fileName);

                 //--- Escribimos el archivo :
                       InputStream      in   = rs.getBinaryStream("filebinary");
                       int data;

                       File             f    = new File( ruta + imagen.getActividad()+"__"+fileName );
                       FileOutputStream out  = new FileOutputStream(f);
                       while( (data = in.read()) != -1 )
                           out.write( data );

                       in.close();
                       out.close();
                   }catch(Exception k){ throw new Exception(" FILE: " + k.getMessage());}
                lista.add( imagen );
            }
        }catch(Exception e){
            System.out.println("errorrr:"+e.toString()+"__"+e.getMessage());
            throw new SQLException("Error searcImagenes() [DAO] : " + sql +" ->"+ e.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;
    }

    public void createDir(String dir) throws Exception{
        try{
        File f = new File(dir);
        if(! f.exists() ) f.mkdir();
     /*   else{
            File []arc =  f.listFiles();
            while( arc.length >0  ){
               File  imagen = arc[0];
               imagen.delete();
               arc =  f.listFiles();
            }
        }*/
        }catch(Exception e){ throw new Exception ( e.getMessage());}
   }

    static public boolean deleteDirectory(File path) {
    if( path.exists() ) {
      File[] files = path.listFiles();
      for(int i=0; i<files.length; i++) {
         if(files[i].isDirectory()) {
           deleteDirectory(files[i]);
         }
         else {
           files[i].delete();
         }
      }
    }
    return( path.delete() );
  }

    public ArrayList searchNombresArchivos(String numosx,String loginx,String tipito) throws Exception{
        Connection         con     = null;
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        String             query   = "SQL_SEARCH_NOMBRES_ARCHIVOS";
        ArrayList nombresArchivos=new ArrayList();

        try{
            con = this.conectarJNDI(query);//JJCastro fase2
            String sql    =   this.obtenerSQL( query );
            st            =   con.prepareStatement( sql );
            st.setString(1,numosx);
            st.setString(2,tipito);
            String[] nombre_archivo;
            rs = st.executeQuery();
            while (rs.next()){
                nombre_archivo=new String[2];
                nombre_archivo[0]=rs.getString("id");
                nombre_archivo[1]=rs.getString("filename");
                nombresArchivos.add(nombre_archivo);
            }

        }catch(Exception e){
            System.out.println("error en NegociosApplusDAO.getEstadosApplus.."+e.toString()+"__"+e.getMessage());
            throw new Exception(e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return nombresArchivos;
    }

    /*public ArrayList searchDatosFacturaEca(String factura_eca) throws Exception{
        Connection         con     = null;
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        String             query   = "SQL_SEARCH_FACTURAECA";
        ArrayList AccionesFacturaEca=new ArrayList();

        try{
            con = this.conectar(query);
            String sql    =   this.obtenerSQL( query );
            st            =   con.prepareStatement( sql );
            st.setString(1,factura_eca);



            rs = st.executeQuery();
            while (rs.next()){
                nombre_archivo=new String[2];
                nombre_archivo[0]=rs.getString("id");
                nombre_archivo[1]=rs.getString("filename");
                AccionesFacturaEca.add(nombre_archivo);
            }

        }catch(Exception e){
            ystem.out.println("error en NegociosApplusDAO.getEstadosApplus.."+e.toString()+"__"+e.getMessage());
            throw new Exception(e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(query); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return AccionesFacturaEca;
    }*/

    public ArrayList getContratistas() throws Exception{
        Connection         con     = null;
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        String             query   = "SQL_SEARCH_CONTRATISTAS";
        ArrayList listContratistas=new ArrayList();
        boolean respuesta=false;
        try{
            con = this.conectarJNDI(query);//JJCastro fase2
            String sql    =   this.obtenerSQL( query );
            st            =   con.prepareStatement( sql );
            String[] contratista;
            rs = st.executeQuery();
            while (rs.next()){
                contratista=new String[2];
                contratista[0]=rs.getString("id_contratista");
                contratista[1]=rs.getString("descripcion");
                listContratistas.add(contratista);
            }

        }catch(Exception e){
            System.out.println("error en NegociosApplusDAO.SQL_SEARCH_CONTRATISTAS.."+e.toString()+"__"+e.getMessage());
            throw new Exception(e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return listContratistas;
    }

    public String[] getExPrefacturaEca(String id_orde)  throws Exception{
        Connection         con     = null;
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        String             query   = "SQL_SEARCH_EXPREFACTURAECA";
        String[] ExPrefacturaEca={"","","",""};
        try{
            con = this.conectarJNDI(query);//JJCastro fase2
            String sql    =   this.obtenerSQL( query );
            st            =   con.prepareStatement( sql );
            st.setString(1,id_orde);
            String[] nombre_archivo;
            rs = st.executeQuery();
            if (rs.next()){
                ExPrefacturaEca[0]=rs.getString("exprefactura_eca");
                ExPrefacturaEca[1]=rs.getString("simbolo_variable");
                ExPrefacturaEca[2]=rs.getString("observacion");
                ExPrefacturaEca[3]=rs.getString("fecha_factura_eca");
            }
        }catch(Exception e){
            System.out.println("error en NegociosApplusDAO.getExPrefacturaEca.."+e.toString()+"__"+e.getMessage());
            throw new Exception(e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return ExPrefacturaEca;
    }

    public String[] getExPrefacturaContratista(String id_orde,String id_accio)  throws Exception{
        Connection         con     = null;
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        String             query   = "SQL_SEARCH_EXPREFACTURA";
        String[] ExPrefactura={""};
        try{
            con = this.conectarJNDI(query);//JJCastro fase2
            String sql    =   this.obtenerSQL( query );
            st            =   con.prepareStatement( sql );
            st.setString(1,id_orde);
            st.setString(2,id_accio);
            String[] nombre_archivo;
            rs = st.executeQuery();
            if (rs.next()){
                ExPrefactura[0]=rs.getString("exprefactura");


            }
        }catch(Exception e){
            System.out.println("error en NegociosApplusDAO.getExPrefacturaContratista.."+e.toString()+"__"+e.getMessage());
            throw new Exception(e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return ExPrefactura;

    }

    public String getEsquemaComision(String id_orde)  throws Exception{
        Connection         con     = null;
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        String             query   = "SQL_SEARCH_ESQUEMA";
        String esquema="";
        try{
            con = this.conectarJNDI(query);//JJCastro fase2
            String sql    =   this.obtenerSQL( query );
            st            =   con.prepareStatement( sql );
            st.setString(1,id_orde);

            rs = st.executeQuery();
            if (rs.next()){
                esquema=rs.getString("esquema_comision");
            }
        }catch(Exception e){
            System.out.println("error en NegociosApplusDAO.getEsquemaComision.."+e.toString()+"__"+e.getMessage());
            throw new Exception(e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return esquema;
    }




    public List obtainCxpsContratista(  ) throws SQLException {
        PreparedStatement  st    = null;
        ResultSet          rs    = null;
        List               lista = null;
        String             query = "SQL_SEARCH_CXPS_CONTRATISTAS";
        Connection         con   = null;
        String             sql   = "";
        try{
            lista=new LinkedList();
            con = this.conectarJNDI(query);//JJCastro fase2
            sql          =  this.obtenerSQL( query );
            st  =   con.prepareStatement( sql );
            rs  =   st.executeQuery();
            while(rs.next()){
                FacturaContratista facturaContratista= FacturaContratista.load(rs);
                lista.add( facturaContratista);
            }
        }catch(Exception e){
            System.out.println("errorrr:"+e.toString()+"__"+e.getMessage());
            throw new SQLException("Error searcImagenes() [DAO] : " + sql +" ->"+ e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;
    }

    public String updateAccord (String factura_contratista,String factura_formula_prov) throws SQLException{

        StringStatement st       = null;
        String comando_sql  = "";
        String            query    = "SQL_ACTUALIZA_ACCORD_PROV";

        try {
            st = new StringStatement (this.obtenerSQL(query), true);//JJCastro fase2
            st.setString(1,factura_formula_prov);
            st.setString(2,factura_contratista);
            comando_sql = st.getSql();

        }catch(Exception e){
            System.out.println("ERROR DURANTE updateAccord. :"+e.toString()+"__"+e.getMessage());
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE updateAccord. \n " + e.getMessage());
        }
        finally{
            if (st  != null){ try{ st=null;            } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }
        return comando_sql;
    }

    public ArrayList obtainAccords(String[] negocios) throws Exception{
        ArrayList respuesta=new ArrayList();
        String oferticas="";
        for (int i=0;i<negocios.length;i++){
            oferticas=oferticas+negocios[i]+",";
        }
        oferticas=oferticas.substring(0,(oferticas.length()-1) );

        Connection         con     = null;
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        String             query   = "SQL_SEARCH_ACCORDS";

        String             sql   = "";
        try{
            con = this.conectarJNDI(query);//JJCastro fase2
            sql          =  this.obtenerSQL( query );
            sql=sql.replaceAll("oferticas",oferticas);
            st  =   con.prepareStatement( sql );
            rs  =   st.executeQuery();
            while(rs.next()){
                NegocioApplus negocioApplus =new   NegocioApplus();
                negocioApplus.setIdEstado(rs.getString("id_estado_negocio"));
                negocioApplus.setNumOs(rs.getString("num_os"));
                negocioApplus.setNombreCliente(rs.getString("nombre_cliente"));
                negocioApplus.setIdCliente(rs.getString("id_cliente"));
                negocioApplus.setAcciones(rs.getString("acciones"));
                negocioApplus.setIdContratista(rs.getString("id_contratista"));
                negocioApplus.setEstado(rs.getString("estado"));
                respuesta.add( negocioApplus);
            }
        }catch(Exception e){
            System.out.println("errorrr:"+e.toString()+"__"+e.getMessage());
            throw new SQLException("Error obtainAccords() [DAO] : " + sql +" ->"+ e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }

        return respuesta;
    }



    public String validarCambioEstado(String[] acciones,String nuevo_estado) throws Exception{
        if (nuevo_estado.equals("nada")) {return "ok";}
        String respuesta="ok";
        PreparedStatement  st    = null;
        ResultSet          rs    = null;
        String             query = "SQL_SEARCH_CAMBIO_ESTADO";
        Connection         con   = null;
        String             sql   = "";
        try{

            con = this.conectarJNDI(query);//JJCastro fase2
            sql          =  this.obtenerSQL( query );

            for (int i=0;i<acciones.length;i++){

                st  =   con.prepareStatement( sql );
                st.setString(1,acciones[i]);
                st.setString(2,nuevo_estado);
                //.out.println("query::"+st.toString());
                rs  =   st.executeQuery();
                if(!(rs.next())){
                    respuesta="mal";
                    i=acciones.length;
                }
            }
        }catch(Exception e){
            System.out.println("errorrr:"+e.toString()+"__"+e.getMessage());
            throw new SQLException("Error validarCambioEstado() [DAO] : " + sql +" ->"+ e.getMessage());
        } finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        //.out.println("respuesta"+respuesta);
        return respuesta;
    }

    public ArrayList getEstadosApplusUser(String loginxx) throws Exception{
        Connection         con     = null;
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        String             query   = "SQL_SEARCH_ESTADOS_USR";
        ArrayList listEstados=new ArrayList();
        boolean respuesta=false;
        try{
            con = this.conectarJNDI(query);//JJCastro fase2
            String sql    =   this.obtenerSQL( query );
            st            =   con.prepareStatement( sql );
            String[] estado;
            st.setString(1, loginxx);
            rs = st.executeQuery();
            while (rs.next()){
                estado=new String[2];
                estado[0]=rs.getString("codigo");
                estado[1]=rs.getString("estado");
                listEstados.add(estado);
            }

        }catch(Exception e){
            System.out.println("error en NegociosApplusDAO.getEstadosApplusUser.."+e.toString()+"__"+e.getMessage());
            throw new Exception(e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return listEstados;
    }


    public ArrayList getNegociosApplus2(String estado,String contratista,String numosxi,String factconformed,String loginx,String id_solici,String nicc ,String nomclie,String id_cliente,String id_conse, String interventor) throws Exception{//--INTERVENTOR JJCASTRO
		Connection         con     = null;
		PreparedStatement  st      = null;
		ResultSet          rs      = null;
		String             query   = "SQL_SEARCH_NEGOCIOS2_REMIX";

                boolean sw_datos=true;//20100608

		ArrayList listNegocios=new ArrayList();
		boolean respuesta=false;
		try{
		    con = this.conectarJNDI(query);//JJCastro fase2
		    String sql    =   this.obtenerSQL( query );

                    //--------------INTERVENTOR JJCASTRO
		    if (interventor!=null && !(interventor.equals(""))){
		        sql=sql.replaceAll("#INTERVENTOR#", " AND o.interventor= '"+interventor+"'");

		    }else{
		        sql=sql.replaceAll("#INTERVENTOR#", " ");
		    }

                    //--------------INTERVENTOR JJCASTRO

		    if (contratista!=null && !(contratista.equals(""))){
		        sql=sql.replaceAll("contratixx", " AND aa.contratista= '"+contratista+"'");//contratixx

		    }else{
		        sql=sql.replaceAll("contratixx", " ");
		    }

		    if (numosxi!=null && !(numosxi.equals(""))){
		        sql=sql.replaceAll("numosxx", " AND (o.num_os LIKE '%' ||  '"+numosxi+"' || '%' OR o.num_os IS NULL)");//AND (o.num_os LIKE '%' || numosxx || '%' OR o.num_os IS NULL)
		    }else{
		        sql=sql.replaceAll("numosxx", " ");
		    }

		    if (estado!=null && !(estado.equals("0"))){
		        sql=sql.replaceAll("estadito", " AND aa.estado='"+estado+"'");//AND a.estado=estadito
		    }else{
		        sql=sql.replaceAll("estadito", " ");
		    }

		    if (id_solici!=null && !(id_solici.equals(""))){//090924
		        sql=sql.replaceAll("id_solicixx", " AND o.id_solicitud='"+id_solici+"'");//
		    }else{
		        sql=sql.replaceAll("id_solicixx", " ");
		    }

		    if ((id_cliente!=null && !(id_cliente.equals("")))&&(nomclie==null || (nomclie.equals("")))){//090924
                        id_cliente=clientes_hijos(id_cliente);
		        sql=sql.replaceAll("id_clientexxx", " AND o.id_cliente in ("+id_cliente+")");//
		    }
                    else
                    {
		        sql=sql.replaceAll("id_clientexxx", " ");
		    }

		    if (nicc!=null && !(nicc.equals(""))){//090924
		        sql=sql.replaceAll("niccxx", " AND o.nic='"+nicc+"'");//
		    }else{
		        sql=sql.replaceAll("niccxx", " ");
		    }

		    if (nomclie!=null && !(nomclie.equals(""))){//090924
		        sql=sql.replaceAll("nomcliexx", " AND UPPER(cl.nombre) LIKE UPPER('%"+nomclie+"%') ");//
		    }else{
		        sql=sql.replaceAll("nomcliexx", " ");
		    }

                    if (id_conse!=null && !(id_conse.equals(""))){//090924
		         sql=sql.replaceAll("consexx", " AND o.consecutivo_oferta LIKE '%' ||  '"+id_conse+"' || '%'");
		    }else{
		        sql=sql.replaceAll("consexx", " ");
		    }

		    if ((estado==null || estado.equals("0")) && (numosxi==null || numosxi.equals("")) &&
		        //(contratista==null || contratista.equals("")) &&
		        (id_solici==null || id_solici.equals("")) &&
		        (nicc==null || nicc.equals("")) &&
		        (id_cliente==null || id_cliente.equals("")) &&
		        (nomclie==null || nomclie.equals("")) &&
                        (id_conse==null || id_conse.equals(""))
		        ){
                        sw_datos=false;//20100608
		    }

                    //jjcastro
                    /*
                    if(estado.equals("000")||(estado.equals("0"))){
                         sql=sql.replaceAll("validacionx", " ");
                         sql=sql.replaceAll("#estado#", "AND e.table_code='000' and estudio_cartera in( '000', 'Estudio') ");
                    }else{

                       if(estado.equals("010")){
                        sql=sql.replaceAll("validacionx", " ");
                         sql=sql.replaceAll("#estado#", "AND e.table_code='010' and estudio_cartera not in( '000', 'Estudio') ");
                       }else{
                           sql=sql.replaceAll("#estado#", "AND e.table_code='010'");
                       }

		    if ((estado==null || estado.equals("0") || estado.equals("010")) && (contratista==null || contratista.equals(""))){
                        sql=sql.replaceAll("#estado#", "AND e.table_code='010'");
		        sql=sql.replaceAll("validacionx", " ");
		    }else{
		        sql=sql.replaceAll("validacionx", " AND 1=3 ");
		    }
                    }
                    */
                    //jjcastro

                    if (contratista!=null && !(contratista.equals(""))){//20101116
                        sql=sql.replaceAll("validacionx", " AND 1=3 ");//20101116 linea temporal
                    }//20101116

                    //i20100608
                    if(estado.equals("0")){
                         sql=sql.replaceAll("validacionx", " ");//20100608
                         sql=sql.replaceAll("#estado#", "AND e.table_code=(CASE WHEN o.estudio_cartera='000' THEN '005' WHEN o.estudio_cartera='010' THEN  '010' WHEN o.estudio_cartera='Estudio' THEN '000' END ) ");
                    }else{
                        if(estado.equals("000") ){
                             sql=sql.replaceAll("validacionx", " ");//20100608
                             sql=sql.replaceAll("#estado#", "AND e.table_code='"+estado+"' AND o.estudio_cartera='Estudio'  ");
                        }else{
                            if ( estado.equals("005") ){
                                     sql=sql.replaceAll("validacionx", " ");//20100608
                                     sql=sql.replaceAll("#estado#", "AND e.table_code='"+estado+"' AND o.estudio_cartera='000'  ");
                            }else{
                                if ( estado.equals("010")){
                                     sql=sql.replaceAll("validacionx", " ");//20100608
                                     sql=sql.replaceAll("#estado#", "AND e.table_code='"+estado+"' AND o.estudio_cartera='010'  ");

                                }else{
                                        sql=sql.replaceAll("#estado#", " ");
                                        sql=sql.replaceAll("validacionx", " AND 1=3 ");//20100608
                                }
                            }


                        }
                   }
                   //f20100608

		    st            =   con.prepareStatement( sql );
		    System.out.println("sql query:"+sql);
                  if (sw_datos) { //20100608
		    rs = st.executeQuery();
                    while (rs.next()){


		        /*o.id_solicitud, cl.id_cliente, cl.nic, cl.nit, cl.nombre, cl.tipo,co.descripcion,
		        (a.administracion+a.imprevisto+a.utilidad+a.material+a.mano_obra+a.transporte) AS total_prev1_calculado,
		        a.estado,o.id_oferta,o.num_os,a.id_accion*/
		        NegocioApplus negocioApplus=new NegocioApplus();
		        negocioApplus.setIdSolicitud(reset(rs.getString("id_solicitud")));
		        negocioApplus.setIdCliente(reset(rs.getString("codcli")));
		        negocioApplus.setNicClient(reset(rs.getString("nic")));
		        negocioApplus.setNitClient(reset(rs.getString("nit")));
		        negocioApplus.setNombreCliente(reset(rs.getString("nomcli")));
		        negocioApplus.setTipoCliente(reset(rs.getString("tipo")));
		        negocioApplus.setNombreContratista(reset(rs.getString("descripcion")));
		        negocioApplus.setVlr(resetNum(rs.getString("total_prev1_calculado")));
		        negocioApplus.setEstado(reset(rs.getString("estado")));
		        negocioApplus.setId(reset(rs.getString("id_oferta")));
		        negocioApplus.setNumOs(reset(rs.getString("num_os")));
		        negocioApplus.setIdAccion(reset(rs.getString("id_accion")));
		        negocioApplus.setAcciones(reset(rs.getString("acciones")));
		        negocioApplus.setCreacionFechaEntregaOferta(resetFecha1(rs.getString("creacion_fecha_entrega_oferta")));
		        negocioApplus.setFechaEntregaOferta(resetFecha2(rs.getString("fecha_entrega_oferta")));
		        negocioApplus.setUsuarioEntregaOferta(reset(rs.getString("usuario_entrega_oferta")));
		        negocioApplus.setAlcances(reset(rs.getString("alcances")));
		        negocioApplus.setAdiciones(reset(rs.getString("adicionales")));
		        negocioApplus.setConsecutivo_oferta(reset(rs.getString("consecutivo_oferta")));
		        //System.out.println("rs.getString(precio_total)"+rs.getString("precio_total")+"_");//091119
		        negocioApplus.setEcaOferta(resetNum(rs.getString("precio_total")));                                //091119

                        negocioApplus.setMaterial(resetNum(rs.getString("material")));
                        negocioApplus.setMano_obra(resetNum(rs.getString("mano_obra")));
                        negocioApplus.setOtros(resetNum(rs.getString("transporte")));
                        negocioApplus.setPorc_a(resetNum(rs.getString("porc_administracion")));
                        negocioApplus.setPorc_i(resetNum(rs.getString("porc_imprevisto")));
                        negocioApplus.setPorc_u(resetNum(rs.getString("porc_utilidad")));
                        negocioApplus.setAdministracion(resetNum(rs.getString("administracion")));
                        negocioApplus.setImprevisto(resetNum(rs.getString("imprevisto")));
                        negocioApplus.setUtilidad(resetNum(rs.getString("utilidad")));

                        negocioApplus.setIdEstado( reset(rs.getString("id_estado")) );//20100214

                        negocioApplus.setFacturaConformada(rs.getString("fact_conformada"));//2010-05-31 rhonalf

                        negocioApplus.setPrefactura(rs.getString("prefactura_contratista"));//20100618
                        negocioApplus.setFecFacContratistaFin(rs.getString("fecha_factura_contratista_final"));//20100618
                        negocioApplus.setNotaCredContratista(rs.getString("nota_credito_contratista"));//20100618

                        negocioApplus.setFecFactConformed(rs.getString("fecha_factura_contratista"));//20100714

		        listNegocios.add(negocioApplus);
		    }
                   }
		}catch(Exception e){
		    System.out.println("error en NegociosApplusDAO..."+e.toString()+"__"+e.getMessage());
		    throw new Exception(e.getMessage());
		}
		finally{
		    if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
		    if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
		    if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
		}
		return listNegocios;
	}


    public ArrayList getEstadosApplus2() throws Exception{
        Connection         con     = null;
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        String             query   = "SQL_SEARCH_ESTADOS2";
        ArrayList listEstados=new ArrayList();
        boolean respuesta=false;
        try{
            con = this.conectarJNDI(query);//JJCastro fase2
            String sql    =   this.obtenerSQL( query );
            st            =   con.prepareStatement( sql );
            String[] estado;
            rs = st.executeQuery();
            while (rs.next()){
                estado=new String[2];
                estado[0]=rs.getString("table_code");
                estado[1]=rs.getString("referencia");
                listEstados.add(estado);
            }

        }catch(Exception e){
            System.out.println("error en NegociosApplusDAO.getEstadosApplus2.."+e.toString()+"__"+e.getMessage());
            throw new Exception(e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return listEstados;
    }

    public NegocioApplus obtainAccione(String solicitud_consultable,String id_accion,String loginx) throws Exception{
        Connection         con     = null;
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        String             query   = "SQL_SEARCH_ACCIONE";
        Accord accione=new Accord();
        NegocioApplus negapp=new NegocioApplus();
        try{
            con = this.conectarJNDI(query);//JJCastro fase2
            String sql    =   this.obtenerSQL( query );
            st            =   con.prepareStatement( sql );
            st.setString(1, id_accion);
            st.setString(2, solicitud_consultable);

            rs = st.executeQuery();

            System.out.println("st"+st.toString());
            if (rs.next()){
                negapp.setAcciones(reset(rs.getString("descripcion")));
                negapp.setObservacion(reset(rs.getString("observaciones")));
                negapp.setSolicitud(reset(rs.getString("descripcion_solicitud")));
                negapp.setFecha(reset(rs.getString("creation_date")));
                negapp.setTipoTrabajo(reset(rs.getString("tipo_trabajo")));
            }

        }catch(Exception e){
            System.out.println("error en NegociosApplusDAO.obtainAccione.."+e.toString()+"__"+e.getMessage());
            throw new Exception(e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return negapp;
    }




    /*jpinedo */
    public String  clientes_hijos(String id_cliente) throws Exception{
        Connection         con     = null;
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        String             query   = "SQL_HIJOS_CLIENTE_PADRE";
        String clientes="";
        boolean sw=true;//si  cliente padre es el mismo

        try{
            con = this.conectarJNDI(query);//JJCastro fase2
            String sql    =   this.obtenerSQL( query );
            st            =   con.prepareStatement( sql );
            st.setString(1, id_cliente);
            rs = st.executeQuery();
 
                while (rs.next())
                {
                    clientes=clientes+"'"+rs.getString("id_subcliente")+"',";
                    sw=false;
                }
          
                 if(sw==true)
                 {
                        clientes="'"+id_cliente+"'";
                 }
                 else
                 {
                   int lc=clientes.length();
                   clientes=clientes.substring(0, lc-1);
                 }

        }
            catch(Exception e){
            System.out.println("error en clientes_hijos"+e.toString()+"__"+e.getMessage());
            throw new Exception(e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return clientes;
    }





        /*jpinedo */
    public String  getNomClientepadre(String id_cliente) throws Exception{
        Connection         con     = null;
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        String             query   = "SQL_GET_CLIENTE_PADRE";
        String nombre="No Registra";


        try{
            con = this.conectarJNDI(query);//JJCastro fase2
            String sql    =   this.obtenerSQL( query );
            st            =   con.prepareStatement( sql );
            st.setString(1, id_cliente);
            rs = st.executeQuery();
            if(rs.next())
            {
               nombre=rs.getString("nomcli");
            }



        }
            catch(Exception e){
            System.out.println("error en getnomclientepadre"+e.toString()+"__"+e.getMessage());
            throw new Exception(e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return nombre;
    }





    public java.util.TreeMap getClientesEca() throws Exception
    {

        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String  query = "SQL_CLIENTES_ECA";
        TreeMap Cust = new TreeMap();
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            rs = ps.executeQuery();
            while (rs.next())
            {
                Cust.put(rs.getString("nombre") + "_"+rs.getString("nit") , rs.getString("id_cliente"));
            }
        }}
        catch (Exception ex)
        {
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
       return Cust;
    }




    private String reset(String val){
            if(val==null)
               val = "";
            return val;
    }

    public String getClientesEca(String cl,String nomselect,String def) throws Exception
    {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String  query = "SQL_CLIENTES_ECA2";
        String ret="";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
             ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            ps.setString(1, nomselect);
            ps.setString(2, cl);
            ps.setString(3, def);
            ps.setString(4, cl);
            System.out.println(ps.toString());
            rs = ps.executeQuery();
            ret=cl+";;;;;;;;;;";
            if (rs.next())
            ret=ret+rs.getString("nombre");
            /*while (rs.next())
            {
                Cust.put(rs.getString("nombre") + "_"+rs.getString("nit") , rs.getString("id_cliente"));
            }*/
        }}
        catch (Exception ex)
        {
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
       return ret;
    }

/**
 *
 * @param solicitud_consultable
 * @param id_accion
 * @param loginx
 * @param tipotraba
 * @param observaci
 * @param extipotraba
 * @param fecvisitaplan
 * @param fecvisitareal
 * @return
 * @throws Exception
 */
    public String marcarTipoTrabajo(String solicitud_consultable,String id_accion,String loginx,String tipotraba,
            String observaci,String extipotraba,String fecvisitaplan,String fecvisitareal,String estadinho) throws Exception{//20100513

        java.util.Date fechaActual = new java.util.Date();
        java.text.SimpleDateFormat formato = new java.text.SimpleDateFormat("ddMMyyyy hh:mm");
        String cadenaFecha = formato.format(fechaActual);

        String respuesta="ok";
        StringStatement  st    = null;
        String             query = "SQL_MARCAR_TIPO_TRABAJO";
        Connection con=null;
        String ejecutable="";
       String tipo_sol=this.getDescTipoSolc(solicitud_consultable);
        try{
            String sql    =   this.obtenerSQL( query );

            if (fecvisitareal!=null && !(fecvisitareal.equals(""))  && estadinho.equals("020")&&tipotraba!=null&&(!tipotraba.equals(""))&&(!tipotraba.equals("Sin Definir"))){//20110131
                if(tipo_sol.equals("AAAE")){
                    sql=sql.replaceAll("estaditox", " ,estado= '060' ");//20100513
                }else{
                sql=sql.replaceAll("estaditox", " ,estado= '030' ");//20100513
                }
            }else{
                sql=sql.replaceAll("estaditox", " ");
            }

            if (fecvisitareal!=null && !(fecvisitareal.equals("")) && estadinho.equals("020")){//20100513
                sql=sql.replaceAll("fecvisitarealx", " ,fec_visita_hecha='"+fecvisitareal+"' , user_visita_hecha='"+loginx+"', creation_fec_visita_hecha=NOW() ");
            }else{
                sql=sql.replaceAll("fecvisitarealx", " ");
            }

            if (fecvisitaplan!=null && !(fecvisitaplan.equals(""))){//091030
                sql=sql.replaceAll("fecvisitaplanx", " ,fec_visita_planeada='"+fecvisitaplan+"' ");
            }else{
                sql=sql.replaceAll("fecvisitaplanx", " ");
            }

            con= this.conectarJNDI(query);//JJCastro fase2
            st = new StringStatement (sql, true);//JJCastro fase2
            st.setString       (1, tipotraba);
            st.setString       (2, "\n("+cadenaFecha+") "+loginx+": "+observaci);
            st.setString       (3, loginx);
            st.setString       (4, id_accion);
            st.setString       (5, solicitud_consultable);

            ejecutable=ejecutable+st.getSql();

            //st.execute();

            if (extipotraba.equals("")){//si se puso el tipo de trabajo y era el unico tipo de trabajo pendiente para esa solicitud se cambia el estado a pendiente por hacer cotizacion

                    PreparedStatement ps = null;
                    ResultSet rs = null;
                    String  Query = "SQL_ESTADO_TIPO_TRABAJO";
                    try{
                        ps  = con.prepareStatement(this.obtenerSQL(Query));//JJCastro fase2
                        ps.setString(1,solicitud_consultable);
                        ps.setString(2,id_accion);

                        rs = ps.executeQuery();
                        if (rs.next()){//si no era el unico tipo de trabajo pendiente para esa solicitud
                            //ejecutable.replaceAll("estaditox", " ");
                            HSendMail2 hSendMail2=new HSendMail2();
                            hSendMail2.start("imorales@fintravalores.com",
                                "imorales@fintravalores.com", "", "",
                                "tipo de trabajo",
                                "Ha sido puesto el tipo de trabajo para la solicitud "+solicitud_consultable+" en el item "+id_accion+" . Favor consultar Aplicativo de Consorcio Multiservicios en http://200.30.77.38/consorcio  .",loginx,"","");
                        }else{             //si era el unico tipo de trabajo pendiente para esa solicitud

                            HSendMail2 hSendMail2=new HSendMail2();
                            hSendMail2.start("imorales@fintravalores.com",
                                "imorales@fintravalores.com", "", "",
                                "pendiente cotizacion",
                                "Ha sido puesto el tipo de trabajo para la solicitud "+solicitud_consultable+" en el item "+id_accion+" . Favor consultar Aplicativo de Consorcio Multiservicios en http://200.30.77.38/consorcio  .",loginx,"","");
                        }




                    }
                    catch (Exception ex)
                    {
                        System.out.println("error en SQL_ESTADO_TIPO_TRABAJO"+ex.toString());
                        ex.printStackTrace();
                        throw new Exception(ex.getMessage());
                    }
        finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            }



            }

            TransaccionService  scv = new TransaccionService(this.getDatabaseName());
            if(!ejecutable.equals("")){
                try{
                    scv.crearStatement();
                    scv.getSt().addBatch(ejecutable);
                    scv.execute();
                }catch(SQLException e){
                    throw new SQLException("ERROR DURANTE INSERCION DE FACTURAS"+e.getMessage());
                }
            }
            ejecutable=null;

        }catch(Exception e){
            System.out.println("marcarTipoTrabajo error:"+e.toString()+"__"+e.getMessage());
            throw new SQLException("Error marcarTipoTrabajo() : "+ e.getMessage());
        }
        finally{
            if (st  != null){ try{ st = null;              } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con);   } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return respuesta;
    }





    public String asignar(String loginx, String fecof,String solicitudes[]) throws Exception{
        String oferticas="";
        for (int i=0;i<solicitudes.length;i++){
            oferticas=oferticas+"'"+solicitudes[i]+"',";
        }
        if (oferticas.length()>1){
            oferticas=oferticas.substring(0,(oferticas.length()-1) );
        }

        String respuesta="ok";
        StringStatement  st    = null;
        String             query = "SQL_ASIGNAR";
        TransaccionService tser = new TransaccionService(this.getDatabaseName());
        Connection con=null;
        try{
            String[] sql    =   this.obtenerSQL( query ).replaceAll("solicitudesx",oferticas).split(";");
            
            tser.crearStatement();
            st            =   new StringStatement( sql[0], true );
            st.setString       (1, loginx);
            st.setString       (2, loginx);
            st.setString       (3, fecof);
            tser.getSt().addBatch(st.getSql());
            
            String estad_tem="060";
            st            =   new StringStatement( sql[1], true  );
            st.setString       (1, estad_tem);
            st.setString       (2, oferticas);
            tser.getSt().addBatch(st.getSql());
            
            tser.execute();

        }catch(Exception e){
            System.out.println("asignar error:"+e.toString()+"__"+e.getMessage());
            e.printStackTrace();
            throw new SQLException("Error asignar() : "+ e.getMessage());
        }
        finally{
            if (st  != null){ try{ tser.closeAll();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con);   } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return respuesta;
    }





    private String resetFecha1(String val){
            if(val==null){val = "";}
            if (val.equals("0099-01-01 00:00:00")){val="";}
            if (val.length()>16){val=val.substring(0,16);}
            return val;
    }
    private String resetFecha2(String val){
            if(val==null){val = "";}
            if (val.equals("0099-01-01 00:00:00")){val="";}
            if (val.length()>10){val=val.substring(0,10);}
            return val;
    }

    public String validacionSeguridadSolicitud(String id_solicitudx,String id_accionx,String loginxx,String pasoadar){
        return "";
    }

    private String resetNum(String val){
            if(val==null)
               val = "0";
            return val;
    }

    public String getIdClie(String nic)  throws Exception{
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String  query = "SQL_ID_CLIENTE_NIC";
        String respuesta="";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            ps.setString(1, nic);
            rs = ps.executeQuery();
            if (rs.next())            {
                respuesta=rs.getString("id_cliente");
            }
        }}
        catch (Exception ex)
        {
            System.out.println("error en getIdClie__"+ex.toString());
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
       return respuesta;
    }




    public NegocioApplus obtainAlcanc(String solicitud_consultable,String id_accion,String loginx) throws Exception{//091016
        Connection         con     = null;
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        String             query   = "SQL_SEARCH_ALCANCES";
        Accord accione=new Accord();
        NegocioApplus negapp=new NegocioApplus();
        try{
            con = this.conectarJNDI(query);//JJCastro fase2
            String sql    =   this.obtenerSQL( query );
            st            =   con.prepareStatement( sql );
            st.setString(1, id_accion);
            st.setString(2, solicitud_consultable);

            rs = st.executeQuery();

            //System.out.println("st"+st.toString());
            if (rs.next()){
                negapp.setAcciones(reset(rs.getString("descripcion")));
                negapp.setObservacion(reset(rs.getString("observaciones")));
                negapp.setSolicitud(reset(rs.getString("descripcion_solicitud")));
                negapp.setFecha(reset(rs.getString("creation_date")));
                negapp.setTipoTrabajo(reset(rs.getString("tipo_trabajo")));

                negapp.setAlcances(reset(rs.getString("alcances")));
                negapp.setAdiciones(reset(rs.getString("adicionales")));
                negapp.setTrabajo(reset(rs.getString("trabajo")));

                negapp.setFecVisitaPlan(reset(rs.getString("fec_visita_planeada")));//091030
                negapp.setFecVisitaReal(reset(rs.getString("fec_visita_hecha")));//091030
                negapp.setUsrVisitaHecha(reset(rs.getString("user_visita_hecha")));//091030
                negapp.setCreationFecVisitaHecha(reset(rs.getString("creation_fec_visita_hecha")));//091030

                negapp.setIdEstado(rs.getString("estado"));//20100513pm

            }

        }catch(Exception e){
            System.out.println("error en NegociosApplusDAO.obtainALCANCES.."+e.toString()+"__"+e.getMessage());
            throw new Exception(e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return negapp;
    }

       public String marcarAvance(String solicitud_consultable,String id_accion,String loginx,String alcanci,String adicioni,String trabaji)
         throws Exception {

        System.out.println("solicitud_consultable"+solicitud_consultable+"id_accion"+id_accion+"loginx"+loginx+"alcanci"+alcanci+"adicion1"+adicioni);
        java.util.Date fechaActual = new java.util.Date();
        java.text.SimpleDateFormat formato = new java.text.SimpleDateFormat("ddMMyyyy hh:mm");
        String cadenaFecha = formato.format(fechaActual);
        String respuesta="ok";
        StringStatement  st    = null;
        String             query = "SQL_MARCAR_ALCANCE";
        Connection con=null;
        String ejecutable="";
        try{
            st = new StringStatement (this.obtenerSQL(query), true);//JJCastro fase2
            //st = this.crearPreparedStatement(query);
            st.setString       (1, alcanci);
            st.setString       (2, adicioni);
            st.setString       (3, loginx);
            st.setString       (4, trabaji);
            st.setString       (5, id_accion);
            st.setString       (6, solicitud_consultable);

            ejecutable=ejecutable+st.getSql();


            TransaccionService  scv = new TransaccionService(this.getDatabaseName());
            if(!ejecutable.equals("")){
                try{
                   scv.crearStatement();
                    scv.getSt().addBatch(ejecutable);
                    scv.execute();
                   /* Vector comandos=new Vector();//20101122
                    comandos.add(ejecutable);//20101122
                    scv.insertar(comandos);*/ //20101122
                }catch(SQLException e){
                    throw new SQLException("ERROR DURANTE SQL_MARCAR_AVANCE"+e.getMessage());
                }
            }
            ejecutable=null;

        }catch(Exception e){
            System.out.println("marcarAvance error:"+e.toString()+"__"+e.getMessage());
            throw new SQLException("Error marcarAvance() : "+ e.getMessage());
        }finally{
            if (st  != null){ try{ st = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }
        return respuesta;
    }

/**
 *
 *
 * @param exproducto
 * @param consec
 * @param descr
 * @param pre
 * @param tipo
 * @param medida
 * @param loginx
 * @param categoria
 * @return
 * @throws Exception
 */

  public String modificarProducto(String reg_status,String exproducto,String consec,String descr,double pre,String tipo,String medida,String loginx, int categoria, int subcategoria, int tiposubcategoria, String empaque, String alcance, String certificado, String ente_certificador,double pre_compra)  throws Exception{
        String respuesta = "ok";
        StringStatement st = null;
        String query1 = "SQL_ACTUALIZAR_PRODUCT";
        Connection con = null;
        String ejecutable = "";
        try {
            st = new StringStatement(this.obtenerSQL(query1), true);//JJCastro fase2
            st.setString(1, reg_status);//05082010 SET reg_status=?,
            st.setString(2, descr);   //descripcion=?,
            st.setString(3, "" + pre);  //precio=?,
            st.setString(4, tipo);//ipo_material=?,
            st.setString(5, loginx);//user_update=?,
            st.setString(6, medida); // medida=?,
            st.setInt(7, categoria);//20100218    idcategoria=?,
            st.setInt(8, subcategoria);//03 08 2010  idsubcategoria=?,
            st.setInt(9, tiposubcategoria);//03 08 2010    idtiposubcategoria=?,
            st.setString(10, empaque);//04082010  unidad_empaque=?,
            st.setString(11, alcance);//04082010   alcance=?,
            st.setString(12, certificado);//04082010  certificado=?,
            st.setString(13, ente_certificador);//04082010  ente_certificador=?)
            st.setString(14, "" + pre_compra);  //precio=?,
            st.setString(15, exproducto);//04082010  consec)
            ejecutable = ejecutable + st.getSql();
            System.out.println("ejecutable" + ejecutable);

            TransaccionService scv = new TransaccionService(this.getDatabaseName());
            if (!ejecutable.equals("")) {
                try {
                    scv.crearStatement();
                    scv.getSt().addBatch(ejecutable);
                    scv.execute();
                } catch (SQLException e) {
                    throw new SQLException("ERROR DURANTE SQL_actualizacion_PRODUCT" + e.getMessage());
                }
            }
            ejecutable = null;

        }catch(Exception e){
            System.out.println("modificarProducto error:"+e.toString()+"__"+e.getMessage());
            throw new SQLException("Error modificarProducto() : "+ e.getMessage());
        }
        finally{
            if (st  != null){ try{ st = null;} catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            }
        return respuesta;
    }


/**
 *
 *
 * @param pk_material
 * @return
 * @throws Exception
 */


    public Material getMaterial(String pk_material) throws Exception{
        Connection         con     = null;
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        String             query   = "SQL_SEARCH_MATERIAL";
        Material m=new Material();
        try{
            con = this.conectarJNDI(query);//JJCastro fase2
            String sql    =   this.obtenerSQL( query );
            st            =   con.prepareStatement( sql );
            st.setString(1, pk_material);

            rs = st.executeQuery();

            if (rs.next()){
                //semi load de material
                m.setRegStatus(reset(rs.getString("reg_status")));
                m.setIdMaterial(reset(rs.getString("idmaterial")));
                m.setDescripcion(reset(rs.getString("descripcion")));
                m.setValor(Double.parseDouble((rs.getString("precio"))));
                m.setCodigo(reset(rs.getString("cod_material")));
                m.setTipo(reset(rs.getString("tipo_material")));
                m.setLastUpdate(reset(rs.getString("last_update")));
                m.setUserUpdate(reset(rs.getString("user_update")));
                m.setAprobacion(reset(rs.getString("aprobacion")));
                m.setMedida(reset(rs.getString("medida")));
                m.setValorCompra(Double.parseDouble(rs.getString("valor_compra")));
                m.setIdMaterialAsociado(reset(rs.getString("idmaterial_asociado")));
                m.setFechaAnulacion(reset(rs.getString("fecha_anulacion")));
                m.setUserAnulacion(reset(rs.getString("user_anulacion")));
                m.setCategoria(reset(rs.getString("categoria")));

                m.setIdcategoria(rs.getInt("idcategoria"));//JJCASTRO MATERIAL 04082010
                m.setIdsubcategoria(rs.getInt("idsubcategoria"));//JJCASTRO MATERIAL 04082010
                m.setIdtiposubcategoria(rs.getInt("idtiposubcategoria"));//JJCASTRO MATERIAL 04082010
                m.setUnidad_empaque(reset(rs.getString("unidad_empaque")));//JJCASTRO MATERIAL 04082010
                m.setAlcance(reset(rs.getString("alcance")));//JJCASTRO MATERIAL 04082010

                m.setCertificado((reset(rs.getString("certificado")).equals("")||reset(rs.getString("certificado")).equals("N"))?"N":"S");//28-08-2010
                m.setEnte_certificador(reset(rs.getString("ente_certificador")));//28-08-2010
                /*reg_status, idmaterial, descripcion, precio, cod_material, tipo_material,
               last_update, user_update, aprobacion, medida, valor_compra, idmaterial_asociado,
               fecha_anulacion, user_anulacion*/
            }

        }catch(Exception e){
            System.out.println("error en NegociosApplusDAO.getMaterial.."+e.toString()+"__"+e.getMessage());
            throw new Exception(e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return m;
    }




    public String asignarEstado(String loginx, String estado_asignable,String acciones[]) throws Exception{
        String accioncitas="";
        for (int i=0;i<acciones.length;i++){
            accioncitas=accioncitas+"'"+acciones[i]+"',";
        }
        if (accioncitas.length()>1){
            accioncitas=accioncitas.substring(0,(accioncitas.length()-1) );
        }

        String respuesta="ok";
        PreparedStatement  st    = null;
        String             query = "SQL_ASIGNAR_ESTADO";
        Connection con=null;
        try{
            String sql    =   this.obtenerSQL( query );
            con = this.conectarJNDI(query);//JJCastro fase2
            sql=sql.replaceAll("accionesx",accioncitas);
            st            =   con.prepareStatement( sql );
            System.out.println("sqll:::"+sql);
            st.setString       (1, loginx);
            st.setString       (2, estado_asignable);
            //st.setString       (3, accioncitas);

            int affectadas=st.executeUpdate();

        }catch(Exception e){
            System.out.println("asignarEstado error:"+e.toString()+"__"+e.getMessage());
            e.printStackTrace();
            throw new SQLException("Error asignarEstado() : "+ e.getMessage());
        }
        finally{
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return respuesta;
    }





/**
 *
 * @param solicitud
 * @return
 * @throws Exception
 */
    public BeanGeneral getDatRecepObra(String solicitud) throws Exception{
        Connection         con     = null;
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        String             query   = "SQL_DAT_RECEP_OBRA";
        BeanGeneral respuesta =new BeanGeneral();

        try{
            con = this.conectarJNDI(query);//JJCastro fase2
            String sql    =   this.obtenerSQL( query );
            st            =   con.prepareStatement( sql );
            st.setString(1, solicitud);
            st.setString(2, solicitud);//JJCASTRO RECEPCION OBRA
            st.setString(3, solicitud);//JJCASTRO RECEPCION OBRA

            rs = st.executeQuery();
            String num_os="";
            Vector  dat_recep_obra=new Vector();
            while (rs.next()){
                BeanGeneral bg =new BeanGeneral();
                respuesta.setValor_01(reset(rs.getString("nombre_cliente")));
                respuesta.setValor_02(reset(rs.getString("consecutivo_oferta")));
                respuesta.setValor_03(reset(rs.getString("num_os")));
                respuesta.setValor_04(reset(rs.getString("fecha_ultimo_seguimiento")));//JJCASTRO RECEPCION OBRA
                respuesta.setValor_05(reset(rs.getString("por_avance_global")));//JJCASTRO RECEPCION OBRA

                bg.setValor_01(resetFecBlanco(rs.getString("fecha_finalizacion")));//JJCASTRO RECEPCION OBRA
                bg.setValor_02(reset(rs.getString("contratista")));
                bg.setValor_03(reset(rs.getString("id_accion")));
                bg.setValor_04(reset(rs.getString("descripcion")));
                bg.setValor_05(resetFecBlanco(rs.getString("fecha_interventoria")));//JJCASTRO RECEPCION OBRA
                bg.setValor_06(reset(rs.getString("estado")));
                bg.setValor_07(reset(rs.getString("estadito")));
                bg.setValor_08(reset(rs.getString("observacion_recepcion")));
                bg.setValor_09(reset(resetFecBlanco(reset(rs.getString("fec_creacion_recepcion")))));//JJCASTRO RECEPCION OBRA
                bg.setValor_10(reset(rs.getString("user_recepcion")));
                bg.setValor_11(reset(rs.getString("por_avance_individual")));//JJCASTRO RECEPCION OBRA
                bg.setValor_12(reset(resetFecBlanco(rs.getString("fecha_documentacion"))));//JJCASTRO RECEPCION OBRA
                bg.setValor_13(resetFecBlanco(rs.getString("liquidacion")));//JJCASTRO RECEPCION OBRA
                bg.setValor_14(resetFecBlanco(rs.getString("informe")));//JJCASTRO RECEPCION OBRA
                bg.setValor_15(resetFecBlanco(rs.getString("acta")));//JJCASTRO RECEPCION OBRA

                dat_recep_obra.add(bg);

            }
            respuesta.setVec(dat_recep_obra);

        }catch(Exception e){
            System.out.println("error en getDatRecepObra en negappdao.."+e.toString()+"__"+e.getMessage());
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return respuesta;
    }


/**
 *
 * @param solicitud
 * @param observaciones
 * @param acciones
 * @param fecFin
 * @param fecInterventor
 * @param loginx
 * @return
 * @throws Exception
 */
    public String aceptarRecepObra(String solicitud,String[] observaciones,String[] acciones,String[] fecFin,String[] fecInterventor,String loginx, String[] fecDocumentacion, String[] liquidacion, String[] informe, String[] acta , String opcion) throws Exception{

        String respuesta = "ok";
        StringStatement st = null;
        PreparedStatement ps = null;
        String             query = "SQL_RECIBIR_OBRA";
        Connection con=null;
        String gransql="";

        java.util.Date fechaActual = new java.util.Date();
        java.text.SimpleDateFormat formato = new java.text.SimpleDateFormat("yyyy-MM-dd hh:mm");
        String cadenaFecha = formato.format(fechaActual);


        try{

            String sustituir = opcion.equals("aceptar")?", estado=CASE WHEN estado='080' THEN '090' ELSE estado END":"";

            
            TransaccionService  scv = new TransaccionService(this.getDatabaseName());
            scv.crearStatement();
            for (int i=0;i<observaciones.length;i++){
                st = new StringStatement(this.obtenerSQL(query).replaceAll("#estado#", sustituir),true);//JJCastro fase2//20100422
                st.setString (1, loginx);
                st.setString (2, loginx);
                st.setString (3,fecFin[i].equals("")?"0099-01-01":fecFin[i]);
                st.setString (4, fecInterventor[i].equals("")?"0099-01-01":fecInterventor[i]);
                st.setString (5, observaciones[i]);
                //jcastro
                st.setString(6, fecDocumentacion[i].equals("")?"0099-01-01":fecDocumentacion[i]);
                st.setString(7, liquidacion[i].equals("S")?"S":"");
                st.setString(8, informe[i].equals("S")?"S":"");
                st.setString(9, acta[i].equals("S")?"S":"");

                st.setString(10, informe[i].equals("S")?cadenaFecha:"0099-01-01");
                st.setString(11, liquidacion[i].equals("S")?cadenaFecha:"0099-01-01");
                st.setString(12, acta[i].equals("S")?cadenaFecha:"0099-01-01");

                //jcastro
                st.setString (13, acciones[i]);
              
                scv.getSt().addBatch(st.getSql());
                st=null;//20100422
            }


            if(opcion.equals("aceptar")) respuesta="Obra recibida.";
            if(opcion.equals("guardar")) respuesta="Obra Guardada, pendiente por confirmacion";

          
                try{
                    scv.execute();
                }catch(SQLException e){
                    throw new SQLException("ERROR DURANTE INSERCION DE FACTURAS"+e.getMessage());
                }


        }catch(Exception e){
            respuesta="Error:"+e.toString();
            System.out.println("aceptarRecepObra error:"+e.toString()+"__"+e.getMessage());
            e.printStackTrace();
            throw new SQLException("Error aceptarRecepObra() : "+ e.getMessage());
        }  finally{
            if (st  != null){ try{ st=null;            } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return respuesta;
    }




    private String resetFecHoy(String val){//20100222
        if(val==null || val.startsWith("0099-01-01")){
            val = Utility.getHoy("-");;
        }
        return val;
    }

     /**
     * metodo para crear el listado de los clientes con el nic asociado
     * @param cl filtro de busqueda
     * @param nomselect  nombre del objeto html
     * @param def definicion
     * @author MGarizao - GEOTECH
     * @date 07/04/2010
     * @version 1.0
     * @return cadena con las descripcion de un objeto select
     * @throws Exception
     */
     public String getClientesEca_nics(String cl,String nomselect,String def) throws Exception
    {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String  Query = "SQL_CLIENTES_ECA3";
        String ret="";
        try{
            ps = this.crearPreparedStatement(Query);
            ps.setString(1, nomselect);
            ps.setString(2, nomselect);
            ps.setString(3, cl);
            ps.setString(4, def);
            ps.setString(5, cl);
            System.out.println(ps.toString());
            rs = ps.executeQuery();
            ret=cl+";;;;;;;;;;";
            if (rs.next())
            ret=ret+rs.getString("nombre");
            /*while (rs.next())
            {
                Cust.put(rs.getString("nombre") + "_"+rs.getString("nit") , rs.getString("id_cliente"));
            }*/
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
        finally
        {
            if (rs!=null) rs.close();
            if (ps!=null) ps.close();
            this.desconectar(Query);
        }
       return ret;
    }

    public ArrayList listarTiposTrabajo() throws SQLException{
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query = "SQL_LISTA_TIPOS_TRABAJO";
        ArrayList lista = new ArrayList();

        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));
                rs = st.executeQuery();
                while (rs.next()) {
                    lista.add(rs.getString("tipo_trabajo"));
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE listarTiposTrabajo[NegociosApplusDAO] \n " + e.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return lista;
    }

    /**
     * Consulta las solicitudes segun los parametros enviados y calcula los dias vencidos segun los estados de las solicitudes
     * @param estado estado de la solicitud
     * @param vencido indicador de vencido para el estado seleccionado
     * @param solicitud numero de la solicitud
     * @param cliente nombre del cliente
     * @param ejecutivo nombre del ejecutivo
     * @param contratista codigo del contratista
     * @param departamento nombre del departamento
     * @param consecutivo
     * @param nic
     * @param fechaInicial fecha inicial para el rango de fechas de creacion de la solicitud
     * @param fechaFinal fecha final para el rango de fechas de creacion de la solicitud
     * @return ArrayList con los resultados obtenidos
     * @throws Exception
     */
    public ArrayList consultarSolicitudes(String estado, String vencido, String solicitud, String cliente, String ejecutivo, String contratista, String departamento, String consecutivo, String nic, String fechaInicial, String fechaFinal, String tipo_solicitud, String responsable) throws Exception {
        ArrayList<String> listaCant = new ArrayList<String>();
        String reg_encontrados = "";
        PreparedStatement st = null;
        ResultSet rs = null;
        ArrayList lista = new ArrayList();
        Connection con = null;
        String query = "SQL_CONSULTAR_SOLICITUDES";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                TreeMap<String,Integer> map = obtenerVencimientoSolicitudes();
                String condicion = "";
                if(!estado.equals("-1")){
                    condicion += " and a.estado='"+estado+"'";
                }
                if(!solicitud.equals("")){
                    condicion += " and o.id_solicitud='"+solicitud+"'";
                }
                if(!cliente.equals("")){
                    condicion += " and trim(upper(cl.nombre),' ')='"+cliente.toUpperCase().trim()+"'";
                }
                if(!ejecutivo.equals("")){
                    condicion += " and cl.id_ejecutivo = (select id_ejecutivo from ejecutivos where trim(upper(nombre),' ')='"+ejecutivo.toUpperCase().trim()+"')";
                }
                if(!contratista.equals("-1")){
                    condicion += " and a.contratista='"+contratista+"'";
                }
                if(!departamento.equals("-1")){
                    condicion += " and cl.departamento='"+departamento.toUpperCase()+"'";
                }
                if(!consecutivo.equals("")){
                    condicion += " and o.consecutivo_oferta='"+consecutivo+"'";
                }
                if(!nic.equals("")){
                    condicion += " and o.nic='"+nic+"'";
                }
                if(!fechaInicial.equals("") && !fechaFinal.equals("")){
                    condicion += " and o.creation_date between '"+fechaInicial+"' and '"+fechaFinal+"'";
                }

                //--- RESPONSABLE OPAV JCASTRO
                if(!tipo_solicitud.equals("")){
                    condicion += " and o.tipo_solicitud='"+tipo_solicitud+"'";
                }
                if(!responsable.equals("")){
                    condicion += " and o.responsable='"+responsable+"'";
                }
//--- RESPONSABLE OPAV JCASTRO


                System.out.println("vencido: "+vencido);
                if(vencido != null){
                    System.out.println("entre");;
                    String venc = "";
                    if(estado.equals("-1")){
                        estado="TOTAL";
                        venc = "venc.dvenc_total";
                    } else if(estado.equals("020")){
                        venc = "venc.dvenc_visita";
                    } else if(estado.equals("030")){
                        venc = "venc.dvenc_cotizacion";
                    } else if(estado.equals("040")){
                        venc = "venc.dvenc_oferta";
                    }
                    condicion += "and (((a.tipo_trabajo='' or upper(a.tipo_trabajo)='SIN DEFINIR') and "+venc+" > (select min(referencia) from tablagen where table_type='VENCI_SOL' and table_code like '%_"+estado+"'))" +
                            "or (a.tipo_trabajo!='' and upper(a.tipo_trabajo)!='SIN DEFINIR' and "+venc+" > (select referencia from tablagen where table_type='VENCI_SOL' and table_code like upper(a.tipo_trabajo)||'_"+estado+"')))";
                }

                st = con.prepareStatement(this.obtenerSQL(query).replaceAll("#FILTROS#", condicion));
                rs = st.executeQuery();
                while (rs.next()) {
                    NegocioApplus n = new NegocioApplus();
                    n.setIdSolicitud(rs.getString("id_solicitud"));
                    n.setDptoCliente(rs.getString("departamento"));
                    n.setNicClient(rs.getString("nic"));
                    n.setNombreCliente(rs.getString("nomcli"));
                    n.setTipoCliente(rs.getString("tipo"));
                    n.setEstado(rs.getString("estado"));
                    n.setNombreContratista(rs.getString("nomcontratista"));
                    n.setFecha(rs.getString("fsolicitud"));
                    n.setFVerificacion_cartera(rs.getString("fecha_validacion_cartera"));
                    n.setFAsignacion_contratista(rs.getString("fasignacion_contratista"));
                    n.setFecVisitaReal(rs.getString("fec_visita_hecha"));
                    n.setCreationFecVisitaHecha(rs.getString("creation_fec_visita_hecha"));
                    n.setFCotizacion(rs.getString("fcotizacion"));
                    n.setFechaEntregaOferta(rs.getString("fecha_entrega_oferta"));
                    n.setDiaVenc_visita(rs.getString("dvenc_visita"));
                    n.setDiaVenc_cotizacion(rs.getString("dvenc_cotizacion"));
                    n.setDiaVenc_entregaOferta(rs.getString("dvenc_oferta"));
                    n.setDiaVenc_totales(rs.getString("dvenc_total"));
                    n.setObservacion(rs.getString("observaciones"));

                    n.setIdAccion(rs.getString("id_accion"));//------- OPAV RESPONSABLE JCASTRO
                    n.setAviso(reset(rs.getString("aviso")));//------- OPAV RESPONSABLE JCASTRO
                    n.setResponsable(rs.getString("responsable"));//------- OPAV RESPONSABLE JCASTRO
                    n.setDescripcion(reset(rs.getString("descripcion")));//------- OPAV RESPONSABLE JCASTRO

                    n.setEjecutivo(reset(rs.getString("ejecutivo")));

                    if(n.getDiaVenc_visita()!=null){
                        Integer diasVenc = map.get(rs.getString("tipo_trabajo").toUpperCase()+"_020");
                        if(diasVenc!=null && Integer.parseInt(n.getDiaVenc_visita()) > diasVenc.intValue()){
                            n.setVenc_visita(true);
                        }
                    }
                    if(n.getDiaVenc_cotizacion()!=null){
                        Integer diasVenc = map.get(rs.getString("tipo_trabajo").toUpperCase()+"_030");
                        if(diasVenc!=null && Integer.parseInt(n.getDiaVenc_cotizacion()) > diasVenc.intValue()){
                            n.setVenc_cotizacion(true);
                        }
                    }
                    if(n.getDiaVenc_entregaOferta()!=null){
                        Integer diasVenc = map.get(rs.getString("tipo_trabajo").toUpperCase()+"_040");
                        if(diasVenc!=null && Integer.parseInt(n.getDiaVenc_entregaOferta()) > diasVenc.intValue()){
                            n.setVenc_oferta(true);
                        }
                    }
                    if(n.getDiaVenc_totales()!=null){
                        Integer diasVenc = map.get(rs.getString("tipo_trabajo").toUpperCase()+"_TOTAL");
                        if(diasVenc!=null && Integer.parseInt(n.getDiaVenc_totales()) > diasVenc.intValue()){
                            n.setVenc_totales(true);
                        }
                    }

                    //--------- RESPONSABLE OPAV JCASTRO
                    if(!listaCant.contains(rs.getString("id_solicitud"))) listaCant.add(rs.getString("id_solicitud"));
                    reg_encontrados = listaCant.size()+"";

                    n.setReg_encontrados(reg_encontrados);

                //--------- RESPONSABLE OPAV JCASTRO

                    lista.add(n);
                    n=null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE consultarSolicitudes[NegociosApplusDAO] \n " + e.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return lista;
    }

    public TreeMap<String,Integer> obtenerVencimientoSolicitudes() throws SQLException{
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query = "SQL_BUSCAR_VENCISOL";
        TreeMap<String,Integer> tree = new TreeMap<String,Integer>();

        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));
                rs = st.executeQuery();
                while (rs.next()) {
                    tree.put(rs.getString("table_code"), Integer.valueOf(rs.getInt("referencia")));
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE obtenerVencimientoSolicitudes[NegociosApplusDAO] \n " + e.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }

        return tree;
    }

    /**
     * Actualiza el parametro fact_conformada de la tabla acciones
     * @param acciones String de acciones separadas por la cadena ;_;
     * <br>ej: "12003;_;85221;_;95523"
     * @param factura el numero de factura
     * @param login usuario que coloca el numero de factura
     * @param fecfactconf
     * @return Cadena con un mensaje de confirmacion de la actualizacion o reportando que no fue posible
     * @throws Exception cuando hay un error;
     */
    public String facturaConformada(String acciones,String factura,String login, String fecfactconf) throws Exception{//20100825
        String cadsql = "";
        StringStatement st = null;
        String sql = "";
        String query="FACT_VERIF";
        String accion = "";
        PreparedStatement ps = null;
        Connection con = null;
        ResultSet rs = null;
        int cont = 0;
        String cadenaresp = "";
        String mensaje="Datos ingresados correctamente";
        try {
            sql = this.obtenerSQL(query);
            String[] lista = acciones.split(";_;");
            //verificar que se pueda hacer la asignacion
            con = this.conectarJNDI(query);

            for (int i = 0; i < lista.length; i++) {
                ps = con.prepareStatement(sql);
                ps.setString(1, lista[i]);
                ps.setString(2, factura);

                rs = ps.executeQuery();
                if(rs.next()){
                    if(rs.getInt("respuesta")==0){
                        cont++;
                        if(i==0){ cadenaresp = lista[i]; }
                        else{
                            cadenaresp = cadenaresp + "," + lista[i];
                        }
                    }
                }
            }

            if(cont>0){
                mensaje = "Las siguentes acciones no pueden ser actualizadas:\n"+cadenaresp;
            }
            else{
                query = "FACT_CONF";
                sql = this.obtenerSQL(query);
                for (int i = 0; i < lista.length; i++) {
                    accion = lista[i];
                    st = new StringStatement(sql,true);
                    st.setString(1, factura);
                    st.setString(2, accion);
                    st.setString(3,login);
                    st.setString(4,fecfactconf);//20100825
                    cadsql = cadsql + st.getSql() + "\n";
                    if(i<(lista.length-1)){//20100625
                        cadsql=cadsql+ " union all  \n";
                    }
                    st = null;
                }
                ps = con.prepareStatement(cadsql);
                rs = ps.executeQuery();
                if(!rs.next()){
                    mensaje = "No fue posible ingresar los datos";
                    System.out.println("No se ejecuto la consulta... :");
                    System.out.println(cadsql);
                }
            }

            ps.close();
            //this.desconectar(con);
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error al asignar facturas conformadas: "+e.toString());
        }
        finally{
            if (ps!= null){ try{ ps=null; } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (!con.isClosed()){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return mensaje;
    }

    /**
     * Obtiene el listado de acciones de una solicitud
     * @param id_solicitud
     * @return ArrayList de beans NegocioApplus
     * @throws SQLException
     */
    public ArrayList listarAccionesSolicitud(String id_solicitud) throws SQLException{
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query = "SQL_GET_ACCIONES_SOLICITUD";
        ArrayList lista = new ArrayList();

        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setString(1, id_solicitud);
                rs = st.executeQuery();
                while (rs.next()) {
                    NegocioApplus n = new NegocioApplus();
                    n.setIdAccion(rs.getString("id_accion"));
                    n.setAcciones(rs.getString("desc_accion"));
                    lista.add(n);
                    n=null;
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE listarAccionesSolicitud[NegociosApplusDAO] \n " + e.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return lista;
    }

    //JJCASTRO RECEPCION OBRA
    private String resetFecBlanco(String val){//20100222
        if(val==null || val.startsWith("0099-01-01")){
            val = "";
        }
        return val;
    }

    /**
 *
 * @param cod_material
 * @return
 * @throws Exception
 * MATERIAL JOSE CASTRO
 */
    public String getId(String cod_material)  throws Exception{
        Connection         con     = null;
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        String             query   = "SQL_BUSQUEDA_IDMATERIAL";
        String idmaterial="";
        try{
            con = this.conectarJNDI(query);//JJCastro fase2
            String sql    =   this.obtenerSQL( query );
            st            =   con.prepareStatement( sql );
            st.setString(1,cod_material);

            rs = st.executeQuery();
            if (rs.next()){
                idmaterial=rs.getString("idmaterial");
            }
        }catch(Exception e){
            System.out.println("error en NegociosApplusDAO BUSCANDO ID"+e.toString()+"__"+e.getMessage());
            throw new Exception(e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return idmaterial;
    }

    public ArrayList consultarSolicitudesInforme(String estado, String vencido, String solicitud, String cliente, String ejecutivo, String contratista, String departamento, String consecutivo, String nic, String fechaInicial, String fechaFinal, String tipo_solicitud, String responsable) throws Exception {
        PreparedStatement st = null;
        ResultSet rs = null;
        ArrayList lista = new ArrayList();



        Connection con = null;
        String query = "SQL_CONSULTAR_SOLICITUDES_INFORME";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                TreeMap<String,Integer> map = obtenerVencimientoSolicitudes();
                String condicion = "";
                if(!estado.equals("-1")){
                    condicion += " and a.estado='"+estado+"'";
                }
                if(!solicitud.equals("")){
                    condicion += " and o.id_solicitud='"+solicitud+"'";
                }
                if(!cliente.equals("")){
                    condicion += " and trim(upper(cl.nombre),' ')='"+cliente.toUpperCase().trim()+"'";
                }
                if(!ejecutivo.equals("")){
                    condicion += " and cl.id_ejecutivo = (select id_ejecutivo from ejecutivos where trim(upper(nombre),' ')='"+ejecutivo.toUpperCase().trim()+"')";
                }
                if(!contratista.equals("-1")){
                    condicion += " and a.contratista='"+contratista+"'";
                }
                if(!departamento.equals("-1")){
                    condicion += " and cl.departamento='"+departamento.toUpperCase()+"'";
                }
                if(!consecutivo.equals("")){
                    condicion += " and o.consecutivo_oferta='"+consecutivo+"'";
                }
                if(!nic.equals("")){
                    condicion += " and o.nic='"+nic+"'";
                }
                if(!fechaInicial.equals("") && !fechaFinal.equals("")){
                    condicion += " and o.creation_date between '"+fechaInicial+"' and '"+fechaFinal+"'";
                }


//--- RESPONSABLE OPAV JCASTRO
                if(!tipo_solicitud.equals("")){
                    condicion += " and o.tipo_solicitud='"+tipo_solicitud+"'";
                }
                if(!responsable.equals("")){
                    condicion += " and o.responsable='"+responsable+"'";
                }
//--- RESPONSABLE OPAV JCASTRO

                System.out.println("vencido: "+vencido);
                if(vencido != null){
                    System.out.println("entre");;
                    String venc = "";
                    if(estado.equals("-1")){
                        estado="TOTAL";
                        venc = "venc.dvenc_total";
                    } else if(estado.equals("020")){
                        venc = "venc.dvenc_visita";
                    } else if(estado.equals("030")){
                        venc = "venc.dvenc_cotizacion";
                    } else if(estado.equals("040")){
                        venc = "venc.dvenc_oferta";
                    }
                    condicion += "and (((a.tipo_trabajo='' or upper(a.tipo_trabajo)='SIN DEFINIR') and "+venc+" > (select min(referencia) from tablagen where table_type='VENCI_SOL' and table_code like '%_"+estado+"'))" +
                            "or (a.tipo_trabajo!='' and upper(a.tipo_trabajo)!='SIN DEFINIR' and "+venc+" > (select referencia from tablagen where table_type='VENCI_SOL' and table_code like upper(a.tipo_trabajo)||'_"+estado+"')))";
                }

                st = con.prepareStatement(this.obtenerSQL(query).replaceAll("#FILTROS#", condicion));
                System.out.println("consulta solicitudes_______"+st.toString());
                rs = st.executeQuery();
                while (rs.next()) {
                    NegocioApplus n = new NegocioApplus();


                    n.setDptoCliente(rs.getString("departamento"));
                    n.setNombreContratista(rs.getString("contratista"));
                    n.setCuenta_solicitudes(rs.getString("cuenta_solicitudes"));
                    n.setPromedio(rs.getString("promedio"));


                    lista.add(n);


                    n=null;
                }




            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE consultarSolicitudes[NegociosApplusDAO] \n " + e.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return lista;
    }
 public ArrayList getNegociosApplusEquipos(String estado,String contratista,String numosxi,String factconformed,String loginx,String id_solici,String nicc ,String nomclie,String id_cliente,String id_conse, String interventor) throws Exception{//--INTERVENTOR JJCASTRO
		Connection         con     = null;
		PreparedStatement  st      = null;
		ResultSet          rs      = null;
		String             query   = "SQL_SEARCH_NEGOCIOS_EQUIPOS";

                boolean sw_datos=true;//20100608

		ArrayList listNegocios=new ArrayList();
		boolean respuesta=false;
		try{
		    con = this.conectarJNDI(query);//JJCastro fase2
		    String sql    =   this.obtenerSQL( query );

                    //--------------INTERVENTOR JJCASTRO
		    if (interventor!=null && !(interventor.equals(""))){
		        sql=sql.replaceAll("#INTERVENTOR#", " AND o.interventor= '"+interventor+"'");

		    }else{
		        sql=sql.replaceAll("#INTERVENTOR#", " ");
}

                    //--------------INTERVENTOR JJCASTRO

		    if (contratista!=null && !(contratista.equals(""))){
		        sql=sql.replaceAll("contratixx", " AND aa.contratista= '"+contratista+"'");//contratixx

		    }else{
		        sql=sql.replaceAll("contratixx", " ");
		    }

		    if (numosxi!=null && !(numosxi.equals(""))){
		        sql=sql.replaceAll("numosxx", " AND (o.num_os LIKE '%' ||  '"+numosxi+"' || '%' OR o.num_os IS NULL)");//AND (o.num_os LIKE '%' || numosxx || '%' OR o.num_os IS NULL)
		    }else{
		        sql=sql.replaceAll("numosxx", " ");
		    }

		    if (estado!=null && !(estado.equals("0"))){
		        sql=sql.replaceAll("estadito", " AND aa.estado='"+estado+"'");//AND a.estado=estadito
		    }else{
		        sql=sql.replaceAll("estadito", " ");
		    }

		    if (id_solici!=null && !(id_solici.equals(""))){//090924
		        sql=sql.replaceAll("id_solicixx", " AND o.id_solicitud='"+id_solici+"'");//
		    }else{
		        sql=sql.replaceAll("id_solicixx", " ");
		    }

		    if ((id_cliente!=null && !(id_cliente.equals("")))&&(nomclie==null || (nomclie.equals("")))){//090924
                        id_cliente=clientes_hijos(id_cliente);
		        sql=sql.replaceAll("id_clientexxx", " AND o.id_cliente in ("+id_cliente+")");//
		    }
                    else
                    {
		        sql=sql.replaceAll("id_clientexxx", " ");
		    }

		    if (nicc!=null && !(nicc.equals(""))){//090924
		        sql=sql.replaceAll("niccxx", " AND o.nic='"+nicc+"'");//
		    }else{
		        sql=sql.replaceAll("niccxx", " ");
		    }

		    if (nomclie!=null && !(nomclie.equals(""))){//090924
		        sql=sql.replaceAll("nomcliexx", " AND UPPER(cl.nombre) LIKE UPPER('%"+nomclie+"%') ");//
		    }else{
		        sql=sql.replaceAll("nomcliexx", " ");
		    }

                    if (id_conse!=null && !(id_conse.equals(""))){//090924
		         sql=sql.replaceAll("consexx", " AND o.consecutivo_oferta LIKE '%' ||  '"+id_conse+"' || '%'");
		    }else{
		        sql=sql.replaceAll("consexx", " ");
		    }

		    if ((estado==null || estado.equals("0")) && (numosxi==null || numosxi.equals("")) &&
		        //(contratista==null || contratista.equals("")) &&
		        (id_solici==null || id_solici.equals("")) &&
		        (nicc==null || nicc.equals("")) &&
		        (id_cliente==null || id_cliente.equals("")) &&
		        (nomclie==null || nomclie.equals("")) &&
                        (id_conse==null || id_conse.equals(""))
		        ){
                        sw_datos=false;//20100608
		    }

                    if (contratista!=null && !(contratista.equals(""))){//20101116
                        sql=sql.replaceAll("validacionx", " AND 1=3 ");//20101116 linea temporal
                    }//20101116

                    //i20100608
                    if(estado.equals("0")){
                         sql=sql.replaceAll("validacionx", " ");//20100608
                         sql=sql.replaceAll("#estado#", "AND e.table_code=(CASE WHEN o.estudio_cartera='000' THEN '005' WHEN o.estudio_cartera='010' THEN  '010' WHEN o.estudio_cartera='Estudio' THEN '000' END ) ");
                    }else{
                        if(estado.equals("000") ){
                             sql=sql.replaceAll("validacionx", " ");//20100608
                             sql=sql.replaceAll("#estado#", "AND e.table_code='"+estado+"' AND o.estudio_cartera='Estudio'  ");
                        }else{
                            if ( estado.equals("005") ){
                                     sql=sql.replaceAll("validacionx", " ");//20100608
                                     sql=sql.replaceAll("#estado#", "AND e.table_code='"+estado+"' AND o.estudio_cartera='000'  ");
                            }else{
                                if ( estado.equals("010")){
                                     sql=sql.replaceAll("validacionx", " ");//20100608
                                     sql=sql.replaceAll("#estado#", "AND e.table_code='"+estado+"' AND o.estudio_cartera='010'  ");

                                }else{
                                        sql=sql.replaceAll("#estado#", " ");
                                        sql=sql.replaceAll("validacionx", " AND 1=3 ");//20100608
                                }
                            }


                        }
                   }
                   //f20100608

		    st            =   con.prepareStatement( sql );
		    System.out.println("sql query:"+sql);
                  if (sw_datos) { //20100608
		    rs = st.executeQuery();
                    while (rs.next()){


		        /*o.id_solicitud, cl.id_cliente, cl.nic, cl.nit, cl.nombre, cl.tipo,co.descripcion,
		        (a.administracion+a.imprevisto+a.utilidad+a.material+a.mano_obra+a.transporte) AS total_prev1_calculado,
		        a.estado,o.id_oferta,o.num_os,a.id_accion*/
		        NegocioApplus negocioApplus=new NegocioApplus();
		        negocioApplus.setIdSolicitud(reset(rs.getString("id_solicitud")));
		        negocioApplus.setIdCliente(reset(rs.getString("codcli")));
		        negocioApplus.setNicClient(reset(rs.getString("nic")));
		        negocioApplus.setNitClient(reset(rs.getString("nit")));
		        negocioApplus.setNombreCliente(reset(rs.getString("nomcli")));
		        negocioApplus.setTipoCliente(reset(rs.getString("tipo")));
		        negocioApplus.setNombreContratista(reset(rs.getString("descripcion")));
		        negocioApplus.setVlr(resetNum(rs.getString("total_prev1_calculado")));
		        negocioApplus.setEstado(reset(rs.getString("estado")));
		        negocioApplus.setId(reset(rs.getString("id_oferta")));
		        negocioApplus.setNumOs(reset(rs.getString("num_os")));
		        negocioApplus.setIdAccion(reset(rs.getString("id_accion")));
		        negocioApplus.setAcciones(reset(rs.getString("acciones")));
		        negocioApplus.setCreacionFechaEntregaOferta(resetFecha1(rs.getString("creacion_fecha_entrega_oferta")));
		        negocioApplus.setFechaEntregaOferta(resetFecha2(rs.getString("fecha_entrega_oferta")));
		        negocioApplus.setUsuarioEntregaOferta(reset(rs.getString("usuario_entrega_oferta")));
		        negocioApplus.setAlcances(reset(rs.getString("alcances")));
		        negocioApplus.setAdiciones(reset(rs.getString("adicionales")));
		        negocioApplus.setConsecutivo_oferta(reset(rs.getString("consecutivo_oferta")));
		        //System.out.println("rs.getString(precio_total)"+rs.getString("precio_total")+"_");//091119
		        negocioApplus.setEcaOferta(resetNum(rs.getString("precio_total")));                                //091119

                        negocioApplus.setMaterial(resetNum(rs.getString("material")));
                        negocioApplus.setMano_obra(resetNum(rs.getString("mano_obra")));
                        negocioApplus.setOtros(resetNum(rs.getString("transporte")));
                        negocioApplus.setPorc_a(resetNum(rs.getString("porc_administracion")));
                        negocioApplus.setPorc_i(resetNum(rs.getString("porc_imprevisto")));
                        negocioApplus.setPorc_u(resetNum(rs.getString("porc_utilidad")));
                        negocioApplus.setAdministracion(resetNum(rs.getString("administracion")));
                        negocioApplus.setImprevisto(resetNum(rs.getString("imprevisto")));
                        negocioApplus.setUtilidad(resetNum(rs.getString("utilidad")));

                        negocioApplus.setIdEstado( reset(rs.getString("id_estado")) );//20100214

                        negocioApplus.setFacturaConformada(rs.getString("fact_conformada"));//2010-05-31 rhonalf

                        negocioApplus.setPrefactura(rs.getString("prefactura_contratista"));//20100618
                        negocioApplus.setFecFacContratistaFin(rs.getString("fecha_factura_contratista_final"));//20100618
                        negocioApplus.setNotaCredContratista(rs.getString("nota_credito_contratista"));//20100618

                        negocioApplus.setFecFactConformed(rs.getString("fecha_factura_contratista"));//20100714
                        negocioApplus.setAviso(rs.getString("aviso"));
                        negocioApplus.setFecha(rs.getString("creation_date"));

		        listNegocios.add(negocioApplus);
		    }
                   }
		}catch(Exception e){
		    System.out.println("error en NegociosApplusDAO..."+e.toString()+"__"+e.getMessage());
		    throw new Exception(e.getMessage());
		}
		finally{
		    if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
		    if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
		    if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
		}
		return listNegocios;
	}

     public String  getDescTipoSolc(String id_solicitud) throws Exception {
        String tipo_sol = "";
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_DESC_TIPOSOLICITUD";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, id_solicitud);;
            rs = ps.executeQuery();
            if (rs.next()) {
                tipo_sol = rs.getString("descripcion");

            }
        } catch (Exception e) {
            System.out.println("Error en getDescTipoSolc: " + e.toString());
            e.printStackTrace();
            throw new Exception("Error en getDescTipoSolc: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return tipo_sol;
    }
     
     /**
     * Genera una listado por referencia de la tabla especificada
     * @return lista con los datos encontrados
     * @throws Exception cuando hay error
     */
    public ArrayList busquedaGeneralRef(String tabletype, String referencia) throws Exception {
        ArrayList cadena = new ArrayList();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SRC_TBG_REF";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con = conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, tabletype);
            ps.setString(2, referencia);
            rs = ps.executeQuery();
            while (rs.next()) {
                cadena.add(rs.getString("table_code") + ";_;" + rs.getString("dato"));
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error al busquedaGeneralRef: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return cadena;
    }

     /**
     * Busca dato en tablagen
     * @param dato table_type a buscar
     * @return listado con datos coincidentes
     * @throws Exception cuando hay error
     */
    public ArrayList busquedaGeneral(String dato) throws Exception {
        ArrayList cadena = new ArrayList();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SRC_TBG";
        String sql = "";
        try {
            sql = this.obtenerSQL(query);
            con = conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, dato);
            rs = ps.executeQuery();
            while (rs.next()) {
                cadena.add(rs.getString("table_code") + ";_;" + rs.getString("dato"));
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error en busqueda general: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return cadena;
    }

    public ArrayList getEstadosApplusAAAE() throws Exception{
        Connection         con     = null;
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        String             query   = "SQL_SEARCH_ESTADOS_AAAE";
        ArrayList listEstados=new ArrayList();
        boolean respuesta=false;
        try{
            con = this.conectarJNDI(query);//JJCastro fase2
            String sql    =   this.obtenerSQL( query );
            st            =   con.prepareStatement( sql );
            String[] estado;
            rs = st.executeQuery();
            while (rs.next()){
                estado=new String[2];
                estado[0]=rs.getString("table_code");
                estado[1]=rs.getString("referencia");
                listEstados.add(estado);
            }

        }catch(Exception e){
            System.out.println("error en NegociosApplusDAO.getEstadosApplus2.."+e.toString()+"__"+e.getMessage());
            throw new Exception(e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return listEstados;
    }

    public String insertarEncuesta(JsonObject informacion) {
        String respuesta = "Ingresado con exito";
        Connection     con = null;
        PreparedStatement st  = null;
        String query = "INSERTAR_ENCUESTA";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));
            st.setString(1, informacion.get("pregunta1").getAsString());
            st.setString(2, informacion.get("pregunta2").getAsString());
            st.setString(3, informacion.get("pregunta3").getAsString());
            st.setString(4, informacion.get("pregunta4").getAsString());
            st.setString(5, informacion.get("pregunta5").getAsString());
            st.setString(6, informacion.get("pregunta6").getAsString());
            st.setString(7, informacion.get("pregunta7").getAsString());
            st.setString(8, informacion.get("pregunta8").getAsString());
            st.setString(9, informacion.get("pregunta9").getAsString());
            st.setString(10, informacion.get("pregunta10").getAsString());
            st.setString(11, informacion.get("pregunta11").getAsString());
            st.setString(12, informacion.get("pregunta12").getAsString());
            st.setString(13, informacion.get("pregunta13").getAsString());
            st.setString(14, informacion.get("pregunta14").getAsString());
            st.setString(15, informacion.get("pregunta15").getAsString());
            st.setString(16, informacion.get("pregunta16").getAsString());
            st.setString(17, informacion.get("comentario").getAsString());
            st.setString(18, informacion.get("usuario").getAsString());
            st.setString(19, informacion.get("solicitud").getAsString());
            
            st.executeUpdate();
            
        } catch (Exception e) {
            e.printStackTrace();
            respuesta = "Error: " + e.getMessage();
        } finally {
            if (st  != null){ try{ st.close();            } catch(SQLException e){ respuesta = ("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ respuesta = ("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return respuesta;
    }
    
    public JsonObject buscarEncuesta(String solicitud) {
        JsonObject respuesta = new JsonObject();
        Connection     con = null;
        PreparedStatement st  = null;
        ResultSet      rs  = null;
        try {
            con = this.conectarJNDI("BUSCAR_ENCUESTA");
            st = con.prepareStatement(this.obtenerSQL("BUSCAR_ENCUESTA"));
            st.setString(1, solicitud);
            rs = st.executeQuery();
            
            while (rs.next()) {
                respuesta.addProperty("nombcliente", rs.getString("nombcliente"));
                respuesta.addProperty("fecha", rs.getString("fecha"));
                respuesta.addProperty("ms", rs.getString("ms"));
                respuesta.addProperty("pregunta1", rs.getString("pregunta1"));
                respuesta.addProperty("pregunta2", rs.getString("pregunta2"));
                respuesta.addProperty("pregunta3", rs.getString("pregunta3"));
                respuesta.addProperty("pregunta4", rs.getString("pregunta4"));
                respuesta.addProperty("pregunta5", rs.getString("pregunta5"));
                respuesta.addProperty("pregunta6", rs.getString("pregunta6"));
                respuesta.addProperty("pregunta7", rs.getString("pregunta7"));
                respuesta.addProperty("pregunta8", rs.getString("pregunta8"));
                respuesta.addProperty("pregunta9", rs.getString("pregunta9"));
                respuesta.addProperty("pregunta10", rs.getString("pregunta10"));
                respuesta.addProperty("pregunta11", rs.getString("pregunta11"));
                respuesta.addProperty("pregunta12", rs.getString("pregunta12"));
                respuesta.addProperty("pregunta13", rs.getString("pregunta13"));
                respuesta.addProperty("pregunta14", rs.getString("pregunta14"));
                respuesta.addProperty("pregunta15", rs.getString("pregunta15"));
                respuesta.addProperty("pregunta16", rs.getString("pregunta16"));
                respuesta.addProperty("comentario", rs.getString("comentario"));
                respuesta.addProperty("creation_user", rs.getString("creation_user"));
            }
        } catch(Exception e) {
            respuesta.addProperty("nombcliente", "");
            respuesta.addProperty("fecha", "");
            respuesta.addProperty("ms", "");
            respuesta.addProperty("creation_user", "");
        } finally {
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ }}
            return respuesta;
        }
    }
}