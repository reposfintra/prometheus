/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.opav.model.DAOS;

/**
 *
 * @author Ing.William Siado T
 */
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.tsp.opav.model.beans.AccionesEca;
import com.tsp.opav.model.beans.CotizacionSl;
import com.tsp.operation.model.MenuOpcionesModulos;
import com.tsp.operation.model.beans.BeanGeneral;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.beans.Cliente;
import com.tsp.opav.model.beans.OfertaElca;

import java.sql.SQLException;
import java.util.ArrayList;

public interface ModuloEjecucionDAO {


    public String cargar_Wbs_Ejecucion(String id_solicitud);
    
    public String cargarComboGenerico(String op, String param);

    public String cargarGridFacturacion(String id_solicitud);

    public String prepararSolicitudParaFacturar(String id_solicitud, String valor_facturar, String valor_material, Usuario usuario);

    public String cargar_Responsables_Ejecucion(String id_solicitud);

    public String insertar_Responsables_Ejecucion(String id_solicitud, String id_usuario_, Usuario usuario);

    public String actualizar_Responsables_Ejecucions(String id, String nombre, Usuario usuario);

    public String anular_Responsables_Ejecucion(String id, Usuario usuario);

    public String cargar_Proyectos_Asignados(Usuario usuario);

    public String cargar_Proyecto_Ejecucion(String id_solicitud, String proceso, String opc, String cond, String id_lote , Usuario usuario);

    public String cargar_Insumos_Apu(String id_solicitud, String id_rel_actividades_apu, String id_apu, String unidad_medida_apu, String usuario );
    
    public String cargar_Insumos_Apu_Master(String id_solicitud, String id_rel_actividades_apu, String id_apu, String unidad_medida_apu, String usuario );

    public String guardar_Cantidades_Insumos_Actual(JsonObject rows, String usuario);

    public String guardar_Cantidades_Insumos_Actual_Master(JsonObject rows, String usuario);

    public String cargar_Lotes_Proyecto_Usuario(String id_solicitud, Usuario usuario);

    public String set_Lote_Wbs_Ejecucion(String id_solicitud, String id_lote, Usuario usuario);

    public String guardar_Lote_Ejecucion(String id_solicitud, String descripcion, JsonObject info, Usuario usuario);

    public String editar_Lote_Ejecucion(String id_solicitud , String id_lote, String descripcion, JsonObject info, Usuario usuario);

    public String nuevo_Lote_Ejecucion(String id_solicitud , Usuario usuario);

    public String crear_WBS_Ejecucion(String id_solicitud);
    
    public String crear_WBS_Ejecucion(String id_solicitud, Usuario usuario );

    public String imprimir_Actas(String id_solicitud ,String ids_actas , Usuario usuario);

    public String imprimir_Liquidacion(String id_solicitud, String ids_actas, Usuario usuario);

    public String insertar_Apu_Wbs_Ejecucion(JsonObject json, Usuario usuario);

    public String insertar_Insumo_Adicional(String id_insumo, String id_unidad_medida, String id_wbs_ejecucion, String cantidad, Usuario usuario);

    
}
