/* * electricaribeDAO.java * * Created on 1 de junio de 2009, 10:52 */
package com.tsp.opav.model.DAOS;
/** * * @author  Fintra */
import java.sql.*;
import com.tsp.opav.model.beans.*;
import com.tsp.operation.model.beans.SerieGeneral;
import java.util.*;
public class electricaribeDAO  extends MainDAO {
    private List listaPrefacturas;
    public electricaribeDAO() {
        super("electricaribeDAO.xml");
    }
    public electricaribeDAO(String dataBaseName) {
        super("electricaribeDAO.xml", dataBaseName);
    }

    public String insertarMs(Oferta oferta ,ArrayList acciones,String usuario,String tipo)throws Exception{
        //.out.println("dao");
        String respuesta="error";
        PreparedStatement st  = null;
        Connection        con = null;
        String            query       = "SQL_SET_MS";
        String code_sql="";
        Vector codes_sql=new Vector();
        String id_orden="";
        String tipo_serie="CONMS";
        if (tipo.equals("EM")){
            tipo_serie="CONEM";
        }
        try {
            String consecutiv="";
            con   = this.conectar( query );
            String sql  = obtenerSQL( query );
            if(oferta.getIdOrden()==null){


                st           = con.prepareStatement( sql );
                SerieGeneral s_id_orden = getSerie("FINV","OP","ORDMS");
                setSerie("FINV","OP","ORDMS");

                SerieGeneral s_consecutivo= getSerie("FINV","OP",tipo_serie);
                setSerie("FINV","OP",tipo_serie);
                consecutiv= s_consecutivo.getUltimo_prefijo_numero();

                id_orden = s_id_orden.getUltimo_prefijo_numero();
                oferta.setIdOrden(id_orden);

                st.setString( 1, id_orden );
                st.setString( 2, oferta.getIdCliente());
                st.setString( 3, "0" );
                st.setString( 4, "0" );

                st.setString( 5, "0" );
                oferta.setIdEstadoNegocio("94");
                st.setString( 6, oferta.getIdEstadoNegocio() );
                st.setString( 7, oferta.getCuotas() );
                st.setString( 8, "0" );
                st.setString( 9, oferta.getDetalleInconsistencia() );

                st.setString( 10, "3000-01-01 00:00:00" );
                st.setString( 11, usuario);
                st.setString( 12, "N");
                st.setString( 13, oferta.getFechaOferta());

                st.setString( 14, tipo+consecutiv+"-09");
                oferta.setNumOs(tipo+consecutiv+"-09");
                st.setString( 15, oferta.getEstudioEconomico());
                st.setString( 16, "0");

                st.setString( 17, "DTF+11");
                st.setString( 18, oferta.getEsquemaComision());
                st.setString( 19, consecutiv);
                st.setString( 20, "0");

                st.setString( 21, "programa");
                st.setString( 22, oferta.getFechaOferta());

                //st.executeUpdate();

                code_sql=st.toString();
                //.out.println("codew_sql"+code_sql);
                codes_sql.add(code_sql);
            }else{

                query="SQL_BORRAR_ACCIONES";
                sql = obtenerSQL( query );
                st  = con.prepareStatement( sql );
                st.setString( 1, oferta.getIdOrden());
                st.setString( 2, oferta.getIdOrden());
                code_sql=st.toString();
                //.out.println("sqllll"+code_sql);
                codes_sql.add(code_sql);

                if (oferta.getNewNumOs()!=null && !(oferta.getNewNumOs().equals(""))){

                    query="SQL_UPDATE_NUMOS";
                    sql = obtenerSQL( query );
                    st  = con.prepareStatement( sql );
                    st.setString( 1, oferta.getNewNumOs());
                    st.setString( 2,oferta.getNumOs());
                    st.setString( 3,usuario);
                    st.setString( 4,oferta.getIdOrden());

                    st.setString( 5, oferta.getNewNumOs());
                    st.setString( 6,oferta.getNumOs());
                    st.setString( 7,usuario);
                    st.setString( 8,oferta.getIdOrden());
                    code_sql=st.toString();
                    //.out.println("sqllll"+code_sql);
                    codes_sql.add(code_sql);
                }
            }

            query="SQL_SET_ACCIONES";

            for (int i=0;i<acciones.size();i++){
                Accord accion=(Accord) acciones.get(i);

                sql = obtenerSQL( query );
                st  = con.prepareStatement( sql );

                SerieGeneral s_id_accion = getSerie("FINV","OP","ACCMS");
                setSerie("FINV","OP","ACCMS");
                String id_accion = s_id_accion.getUltimo_prefijo_numero();

                st.setString( 1, id_accion);
                st.setString( 2, oferta.getIdOrden());
                st.setString( 3, consecutiv );
                st.setString( 4, accion.getIdContratista() );
                st.setString( 5, accion.getAcciones());

                accion.setTotalPrev1(""+(Double.parseDouble(accion.getAdministracion())+
                Double.parseDouble(accion.getImprevistos())+
                Double.parseDouble(accion.getUtilidad())+
                Double.parseDouble(accion.getValorManoObra())+
                Double.parseDouble(accion.getValorMateriales())+
                Double.parseDouble(accion.getValorOtros())));

                st.setString( 6, accion.getTotalPrev1());

                st.setString( 7, "3000-01-01 00:00:00" );
                st.setString( 8, "0" );

                st.setString( 9, accion.getValorMateriales() );
                st.setString( 10, accion.getValorManoObra());
                st.setString( 11, accion.getValorOtros());
                st.setString( 12, "0");

                st.setString( 13, "");

                double ofertaxx=0;
                double eca_ofertaxx=0;
                double ofertaxx16=0;
                double eca_ofertaxx16=0;
                if (oferta.getEsquemaComision().equals("MODELO_NUEVO")){
                        ofertaxx=Double.parseDouble(accion.getTotalPrev1()) * 1.13;
                }else{
                        ofertaxx=Double.parseDouble(accion.getTotalPrev1()) * 1.11;
                }

                if (oferta.getEsquemaComision().equals("MODELO_NUEVO")){
                        eca_ofertaxx=Double.parseDouble(accion.getTotalPrev1()) * 1.13*1.1112;
                }else{
                        eca_ofertaxx=Double.parseDouble(accion.getTotalPrev1()) * 1.11*1.136;
                }

                st.setString( 14, ""+eca_ofertaxx);
                st.setString( 15, ""+ofertaxx);


                st.setString( 16, "0099-01-01");
                st.setString( 17, "0099-01-01");

                st.setString(18, accion.getAdministracion());
                st.setString(19, accion.getImprevistos());
                st.setString(20, accion.getUtilidad());
                st.setString(21, "");
                st.setString(22, "programa");

                code_sql=st.toString();
                //.out.println("codew_sql"+code_sql);
                codes_sql.add(code_sql);
            }

            ejecutarSQL(codes_sql);
            respuesta="El multiservicio "+oferta.getNumOs()+" ha sido procesado.";
            //respuesta="El multiservicio CS"+id_orden+"-09 ha sido creado.";


        }catch(Exception e){
            System.out.println("ERROR en dao:"+e.toString()+"__"+e.getMessage());
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE SQL_SET_MS. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(query);
        }
        //.out.println("respuesta"+respuesta);
        return respuesta;
    }

    /*public void buscaPrefactura(String id_contratista)throws SQLException{
        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs = null;

        String            query    = "SQL_PREFACTURAR";
        listaPrefacturas = null;

        try {

            con   = this.conectar( query );
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            st.setString( 1, id_contratista );

            rs = st.executeQuery();

            listaPrefacturas =  new LinkedList();

            while (rs.next()){
                listaPrefacturas.add(Prefactura.load(rs));
            }

        }catch(Exception e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA SELECCION DE REGISTROS A PREFACTURAR. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(query);
        }
    }*/

    public SerieGeneral getSerie (String dstrct, String  agency_id, String document_type)throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;
        ResultSet rs               = null;


        String            query    = "SQL_GET_ULTIMO_NUMERO";
        SerieGeneral      serie    = null;


        try {


            con   = this.conectar( query );
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            st.setString( 1, dstrct );
            st.setString( 2, agency_id );
            st.setString( 3, document_type );


            rs = st.executeQuery();

            if(rs.next()){
                serie = new SerieGeneral();
                serie = (SerieGeneral.load(rs));
            }

        }catch(Exception e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA SELECCION DE LA BUSQUEDA DE UNA SERIE. \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(query);
        }
        return serie;

    }

    public void setSerie (String dstrct, String  agency_id, String document_type)throws SQLException{

        PreparedStatement st       = null;
        Connection        con      = null;

        String            query    = "SQL_SET_ULTIMO_NUMERO";

        try {
            con   = this.conectar( query );
            String  sql  = obtenerSQL( query );
            st           = con.prepareStatement( sql );
            st.setString( 1, dstrct );
            st.setString( 2, agency_id );
            st.setString( 3, document_type );

            st.executeUpdate();

        }catch(Exception e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA ACTUALIZACION DE LA SERIE.  \n " + e.getMessage());
        }
        finally{
            st.close();
            this.desconectar(query);
        }

    }


    public void ejecutarSQL(Vector comandosSQL) throws SQLException {

        Connection        con      = null;
        Statement        stmt      = null;

        String            query    = "SQL_GET_CONECTION";

        try{
            con   = this.conectar( query );

            if (con != null){

                con.setAutoCommit(false);
                stmt = con.createStatement();
                for(int i=0; i<comandosSQL.size();i++){
                    String comando =(String) comandosSQL.elementAt(i);
                    //////.out.println(comando);
                    stmt.executeUpdate(comando);

                }

                con.commit();
            }
        }catch (SQLException e) {
            // Efectuando rollback en caso de error
            try {

                con.rollback();
            }
            catch (SQLException ignored) {
                System.out.println("error en execuuute"+ignored.toString());
                throw new SQLException("NO SE PUDO HACER ROLLBACK " + ignored.getMessage() + " " + ignored.getErrorCode()+" <br> La siguiente exception es : ----"+ignored.getNextException());
            }
            throw new SQLException("ERROR DURANTE LA TRANSACCION, LOS CAMBIOS NO SE PRODUJERON EL LA BASE DE DATOS " + e.getMessage() + " " + e.getErrorCode()+" <br> La siguiente exception es : ----"+e.getNextException());
        }

        finally{
            stmt.close();
            this.desconectar(query);
        }


    }

    public java.util.TreeMap getClientes() throws Exception
    {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String  Query = "SQL_CLIENTES";
        TreeMap Cust = new TreeMap();
        try{
            ps = this.crearPreparedStatement(Query);
            rs = ps.executeQuery();
            while (rs.next())
            {
                Cust.put(rs.getString(1) + "_"+rs.getString(2) , rs.getString(1) + "_"+rs.getString(2) );
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
        finally
        {
            if (rs!=null) rs.close();
            if (ps!=null) ps.close();
            this.desconectar(Query);
        }
       return Cust;
    }

    public ArrayList CargarArrayAccords(String id_orden)throws Exception
    {   ArrayList arl=new ArrayList();
        Accord acc=new Accord();
        PreparedStatement ps = null;
        ResultSet rs = null;
        String  Query = "SQL_GET_ACCORDS";
        try{
            ps = this.crearPreparedStatement(Query);
            ps.setString(1,id_orden);
            //.out.println(ps.toString());
            rs = ps.executeQuery();
            int i=0;
            while(rs.next())
            {
                acc=new Accord();
                acc.setIdOrden(rs.getString("id_orden"));
                acc.setConsecutivo(rs.getString("consecutivo"));
                acc.setIdContratista(rs.getString("id_contratista"));
                acc.setAcciones(rs.getString("acciones"));
                acc.setTotalPrev1(rs.getString("total_prev1"));
                acc.setPorActualizar(rs.getString("por_actualizar"));
                acc.setValorMateriales(rs.getString("valor_materiales"));
                acc.setValorManoObra(rs.getString("valor_mano_obra"));
                acc.setValorOtros(rs.getString("valor_otros"));
                acc.setFactConformada(rs.getString("fact_conformada"));
                acc.setEcaOferta(rs.getString("eca_oferta"));
                acc.setOferta(rs.getString("oferta"));
                acc.setImprevistos(rs.getString("aiu_imprevistos"));
                acc.setUtilidad(rs.getString("aiu_utilidad"));
                acc.setAdministracion(rs.getString("aiu_administracion"));
                acc.setNombreContratista(rs.getString("descripcion"));
                arl.add(acc);
            }
        }
        catch (Exception ex)
        {
            System.out.println("errr"+ex.toString());
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
        finally
        {
            if (rs!=null) rs.close();
            if (ps!=null) ps.close();
            this.desconectar(Query);
        }
        return arl;
    }

    public Oferta CargarOferta (String id_orden)throws Exception
    {   PreparedStatement ps = null;
        ResultSet rs = null;
        String  Query = "SQL_GET_OFERTA";
        Oferta ofer=new Oferta();
        try{
            ps = this.crearPreparedStatement(Query);
            ps.setString(1,id_orden);
            //.out.println(ps.toString());
            rs = ps.executeQuery();
            int i=0;
            if(rs.next())
            {   ofer.setIdOrden(rs.getString("id_orden"));
                ofer.setIdCliente(rs.getString("id_cliente"));
                ofer.setCostoOfertaApplus(rs.getString("costo_oferta_applus"));
                ofer.setCostoOfertaEca(rs.getString("costo_oferta_eca"));
                ofer.setImporteOferta(rs.getString("importe_oferta"));
                ofer.setCuotas(rs.getString("cuotas"));
                ofer.setValorCuotasR(rs.getString("valor_cuotas_r"));
                ofer.setDetalleInconsistencia(rs.getString("detalle_inconsistencia"));
                ofer.setFechaOferta(rs.getString("fecha_oferta"));
                ofer.setEstudioEconomico(rs.getString("estudio_economico"));
                ofer.setSimboloVariable(rs.getString("simbolo_variable"));
                ofer.setTipoDtf(rs.getString("tipo_dtf"));
                ofer.setEsquemaComision(rs.getString("esquema_comision"));
                ofer.setConsecutivo(rs.getString("consecutivo"));
                ofer.setSimboloVariableCr(rs.getString("simbolo_variable_cr"));
                ofer.setNombreCliente(rs.getString("nombre"));
                ofer.setNumOs(rs.getString("num_os"));
                ofer.setDirClient(rs.getString("direccion"));
                ofer.setTelClient(rs.getString("telefono"));
            }
        }
        catch (Exception ex)
        {   System.out.println(ex.toString());
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
        finally
        {
            if (rs!=null) rs.close();
            if (ps!=null) ps.close();
            this.desconectar(Query);
        }
        //.out.println("ofer."+ofer.getIdOrden());
        return ofer;
    }

    public java.util.TreeMap getPreMultiservicios() throws Exception
    {
        PreparedStatement ps = null;
        ResultSet rs = null;
        String  Query = "SQL_PREMULTISERVICIOS";
        TreeMap Cust = new TreeMap();
        try{
            ps = this.crearPreparedStatement(Query);
            rs = ps.executeQuery();
            int i=0;
            Cust.put("...","...");
            while (rs.next())
            {   Cust.put(rs.getString("num_os")+"_"+rs.getString("nombre_cliente"),rs.getString("id_orden"));
                i++;
            }
        }
        catch (Exception ex)
        {
            System.out.println("errr"+ex.toString());
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
        finally
        {
            if (rs!=null) rs.close();
            if (ps!=null) ps.close();
            this.desconectar(Query);
        }
       return Cust;
    }

    public ArrayList getContratistas() throws Exception{
        Connection         con     = null;
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        String             query   = "SQL_SEARCH_CONTRATISTAS";
        ArrayList listContratistas=new ArrayList();
        boolean respuesta=false;
        try{
            con = this.conectar(query);
            String sql    =   this.obtenerSQL( query );
            st            =   con.prepareStatement( sql );
            String[] contratista;
            rs = st.executeQuery();
            while (rs.next()){
                contratista=new String[3];
                contratista[0]=rs.getString("id_contratista");
                contratista[1]=rs.getString("descripcion");
                contratista[2]=rs.getString("email");
                listContratistas.add(contratista);
            }

        }catch(Exception e){
            System.out.println("error en NegociosApplusDAO.SQL_SEARCH_CONTRATISTAS.."+e.toString()+"__"+e.getMessage());
            throw new Exception(e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(query); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return listContratistas;
    }


    public ArrayList getEntidades() throws Exception{
        Connection         con     = null;
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        String             query   = "SQL_SEARCH_ENTIDADES";
        ArrayList listEntidades=new ArrayList();
        boolean respuesta=false;
        try{
            con = this.conectar(query);
            String sql    =   this.obtenerSQL( query );
            st            =   con.prepareStatement( sql );
            String[] entidad;
            rs = st.executeQuery();
            while (rs.next()){
                entidad=new String[3];
                entidad[0]=rs.getString("table_code");
                entidad[1]=entidad[0];
                entidad[2]=rs.getString("referencia");
                listEntidades.add(entidad);
            }

        }catch(Exception e){
            System.out.println("error en NegociosApplusDAO.SQL_SEARCH_ENTIDADES.."+e.toString()+"__"+e.getMessage());
            throw new Exception(e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(query); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return listEntidades;
    }

    public String[] getDatosClient(String idcli) throws Exception{
        Connection         con     = null;
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        String             query   = "SQL_SEARCH_CLIENT";
        String[] cli=null;
        boolean respuesta=false;
        try{
            con = this.conectar(query);
            String sql    =   this.obtenerSQL( query );
            st            =   con.prepareStatement( sql );
            st.setString(1, idcli);
            rs = st.executeQuery();
            while (rs.next()){
                cli=new String[13];
                cli[0]=rs.getString("nic");
                cli[1]=rs.getString("nit");
                cli[2]=rs.getString("tipo_identificacion");
                cli[3]=rs.getString("nombre_cliente");
                cli[4]=rs.getString("zona");
                cli[5]=rs.getString("ciudad");
                cli[6]=rs.getString("sector");
                cli[7]=rs.getString("direccion");
                cli[8]=rs.getString("telefono");
                cli[9]=rs.getString("celular");
                cli[10]=rs.getString("ejecutivo_cta");
                cli[11]=rs.getString("contacto");
                cli[12]=rs.getString("cargo");

            }

        }catch(Exception e){
            System.out.println("error en getDatosClient.."+e.toString()+"__"+e.getMessage());
            throw new Exception(e.getMessage());
        }
        finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(query); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return cli;
    }
}