/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.opav.model.DAOS;

import com.google.gson.JsonObject;
import com.tsp.operation.model.beans.Usuario;

/**
 *
 * @author mcastillo
 */
public interface MaestroProyectoDAO {

    public String getInfoWbs(String num_solicitud);

    public String cargarAreas();

    public String cargarAreas(String q);

    public String cargarDisciplinas();

    public String cargarActividades(String q);

    public String insertarAreaProyecto(String num_solicitud, String nombre_area, Usuario usuario);

    public String actualizarAreaProyecto(String id_area_proyecto, String nombre_area, Usuario usuario);

    public String insertarDisciplinaArea(String id_area_proyecto, String id_disciplina, Usuario usuario);

    public String actualizarDisciplinaArea(String id_disciplina_area, String id_disciplina, Usuario usuario);

    public String insertarCapitulo(String id_disciplina, String descripcion, Usuario usuario);

    public String actualizarCapitulo(String id_capitulo, String descripcion, Usuario usuario);

    public String insertarActividadCapitulo(String id_capitulo, String id_actividad, Usuario usuario);

    public String actualizarActividad(String id_actividad, String nombre, String descripcion, Usuario usuario);

    public String eliminarItem(String id, String consulta);

    public String cargarGridActividades(String id_capitulo);

    public String cargarGridAPUsActividades(String id_actividad);

    public boolean existeActividad(String nombre, String idActividad);

    public String cargarGridActividades();

    public String insertarActividad(String descripcion, Usuario usuario);

    public String actualizarActividad(String id, String descripcion, Usuario usuario);

    public String activaInactivaActividad(String id, Usuario usuario);

    public boolean existeArea(String nombre, String idArea);

    public String cargarGridAreas();

    public String insertarArea(String nombre, Usuario usuario);

    public String actualizarArea(String id, String nombre, Usuario usuario);

    public String activaInactivaArea(String id, Usuario usuario);

    public boolean existeDisciplina(String nombre, String idDisciplina);

    public String cargarGridDisciplinas();

    public String insertarDisciplina(String nombre, Usuario usuario);

    public String actualizarDisciplina(String id, String nombre, Usuario usuario);

    public String activaInactivaDisciplina(String id, Usuario usuario);

    public String cargarAPUXGrupo(String grupo_apu, String id);

    public String cargarAPUAsoc(String id);

    public String insertarRelAPUActividad(int id, String listado, Usuario usuario);

    public String EliminarRelAPUActividad(int id, String listado);

    public String listarTipoActores(String mostrarTodos);

    public String guardarTipoActor(String descripcion, Usuario usuario);

    public String actualizarTipoActor(String id, String descripcion, Usuario usuario);

    public String activaInactivaTipoActor(String id, Usuario usuario);

    public String cargarCboTipoActores();

    public String cargarTiposIdentificacion();

    public String cargarGridActores(String mostrarTodos);

    public String guardarActor(int tipoActor, String tipoDoc, String documento, String nombre, String ciudad, String dpto, String pais, String direccion, String telefono, String extension, String celular, String email, String tarjeta_prof, String lugarExpCed, Usuario usuario);

    public String actualizarActor(int id, int tipoActor, String tipoDoc, String documento, String nombre, String ciudad, String dpto, String pais, String direccion, String telefono, String extension, String celular, String email, String tarjeta_prof, String lugarExpCed, Usuario usuario);

    public String activaInactivaActor(String id, Usuario usuario);

    public boolean existeApuAsociado(String filtro);

    public String guardarAPUEditEspejo(JsonObject info);

    public boolean existeAPU(String empresa, String nombre, String id_actividad);

    public String ObtenerAccion(String id_solicitud);

    public String ActualizarCantidadAPU(int id_apu, int id_actividad, int cantidad);

    public String ActualizarEstadoRel(int id_apu, int id_actividad);
    
    public String CargarPolizas();

    public String guardarPoliza(String nombre_poliza, String descripcion, Usuario usuario);

    public String actualizarPoliza(String id, String nombre_poliza, String descripcion, Usuario usuario);

    public String activaInactivaPoliza(String id, Usuario usuario);
    
    public String cargarGridBroker();

    public String guardarBroker(String tipoDoc, String documento, String nombre, String ciudad, String dpto, String pais, String direccion, String nombre_contacto, String telefono, String celular, String email, String enviar_correo_contacto,
                                String idusuario, String nombre_asistente, String telefono_asistente, String celular_asistente, String email_asistente, String enviar_correo_asistente, Usuario usuario);

    public String actualizarBroker(int id, String tipoDoc, String documento, String nombre, String ciudad, String dpto, String pais, String direccion, String nombre_contacto, String telefono, String celular, String email, String enviar_correo_contacto, 
                                   String nombre_asistente, String telefono_asistente, String celular_asistente, String email_asistente, String enviar_correo_asistente, Usuario usuario);

    public String activaInactivaBroker(String id, Usuario usuario);

    public String guardarAPUEdit(JsonObject info);

    public String cargarGridInsumosEdit(String ids, int tipo, String id_actividad);
    public String listarCorrespondencia();

    public String cargarEquivalencias();

    /**
     *
     * @param id
     * @param usuario
     * @return
     */
    public String cambiarEstadoCorrespondencia(String id, Usuario usuario);

    /**
     *
     * @param descripcion
     * @param id_procedencia
     * @param usuario
     * @return
     */
    public String guardarCorrespondencia(String descripcion, int id_procedencia, Usuario usuario);

    /**
     *
     * @param id
     * @param descripcion
     * @param id_procedencia
     * @param usuario
     * @return
     */
    public String actualizarCorrespondencia(String id, String descripcion, int id_procedencia, Usuario usuario);

    
    public JsonObject getInfoCuentaEnvioCorreo();
     
    public String cargarGridCategoriasAdmon();
    
    public String guardarCategoria(String nombre, String descripcion, Usuario usuario);
    
    public String actualizarCategoria(String nombre, String descripcion, String idCategoria, Usuario usuario);
    
    public boolean existeCategoriaAdmon(String nombre);
    
    public boolean existeCategoriaAdmon(String idCat, String nombre);
    
    public String cargarGridItemsCategoria(String idCategoria);
    
    public String guardarItemCategoria(String id_categoria, String nombre, String valor, String is_default, Usuario usuario);
    
    public String actualizarItemCategoria(String idItem, String nombre, String valor, String is_default, Usuario usuario);
    
    public String activaInactivaItem(String id, String Query, Usuario usuario);
    
    public String cargarGridUndsAdmon();

    public String insertarUndAdmon(String nombre, String descripcion, Usuario usuario);

    public String actualizarUndAdmon(String id, String nombre, String descripcion, Usuario usuario);

    public String activaInactivaUndAdmon(String id, Usuario usuario);
    
    public String CargarAseguradoras();

    public String guardarAseguradora(String nit_aseguradora, String nombre_aseguradora, Usuario usuario);

    public String actualizarAseguradora(String id, String nit_aseguradora, String nombre_aseguradora, Usuario usuario);

    public String activaInactivaAseguradora(String id, Usuario usuario);
    
    public boolean existeNitEnProveedor(String nit_aseguradora);

    public String filtro_Apus(String buscar, String id, String grupo_apus);
    
    /**
     *
     * @param buscar
     * @param id
     * @param id_solicitud
     * @param grupo_apus
     * @return
     */
    public String filtro_Apus(String buscar, String id, String id_solicitud, String grupo_apus);

    public String filtro_Insumo(String buscar , String categoria , String subcategoria, String id_tipo_insumo);

    public String ActualizarCantidadAPU(int id_apu, int id_actividad, int cantidad, int posicion);

    
   
}
