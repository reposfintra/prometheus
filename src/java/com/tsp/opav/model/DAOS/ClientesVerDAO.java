/* * electricaribeDAO.java * * Created on 1 de junio de 2009, 10:52 */
package com.tsp.opav.model.DAOS;

/** * * @author  Fintra */

import java.sql.*;
import com.tsp.opav.model.beans.AccionesEca;
import com.tsp.opav.model.beans.OfertaElca;
import com.tsp.operation.model.beans.Cliente;
import com.tsp.operation.model.beans.SerieGeneral;
import com.tsp.operation.model.beans.StringStatement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class ClientesVerDAO extends MainDAO {

    String idusuario;
    private List listaPrefacturas;

//    public ClientesVerDAO() {
//        super("ClientesVerDAO.xml");
//    }
    public ClientesVerDAO(String dataBaseName) {
        super("ClientesVerDAO.xml", dataBaseName);
    }

    public String insertarCl(Cliente cl, String usuario) throws Exception {
        String respuesta = "";
        PreparedStatement st = null;
        String query = "SQL_INSERT_CLIENTE_ECA";
        try {
            st = crearPreparedStatement(query);

            SerieGeneral s_id_cliente = getSerie("FINV", "OP", "CLIENTE");
            setSerie("FINV", "OP", "CLIENTE");
            String id_cliente = s_id_cliente.getUltimo_prefijo_numero();
            cl.setCodcli(id_cliente);
            st.setString(1, cl.getCodcli());
            st.setString(2, cl.getNomcli());
            st.setString(3, cl.getNit());
            st.setString(4, cl.getTipo());
            st.setString(5, cl.getCiudad());
            st.setString(6, cl.getDireccion());
            st.setString(7, cl.getNomContacto());
            st.setString(8, cl.getTelContacto());            
            st.setString(9, cl.getCargoContacto());
            st.setString(10, cl.getNomRepresentante());
            st.setString(11, cl.getTelRepresentante());
            st.setString(12, cl.getCelRepresentante());
            st.setString(13, usuario);
            st.setString(14, usuario);
            st.setString(15, cl.getIdEjecutivo());
            st.setString(17, cl.getSector());
            if (cl.isOficial()) {
                st.setString(16, "S");
            } else {
                st.setString(16, "N");
            }
            st.setString(18, cl.getEdif());
            st.setString(19, cl.getEmail());
            st.setString(20, cl.getCelContacto());
            st.setString(21, cl.getDigito_verificacion());
            respuesta = st.toString();
            System.out.println(respuesta);

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e);
            throw e;
        } finally {
            st.close();
            this.desconectar(query);
        }
        return respuesta;
    }







    public String insertSubcliente(String hijo, String padre, String user) throws Exception {
        String query = "SQL_INSERTAR_SUBCLIENTE";
        String respuesta = "";
        StringStatement st = null;
        if((padre==null)|| padre.equals(""))
        {
            padre=hijo;
        }

        try {
            st = new StringStatement (this.obtenerSQL(query), true);//JJCastro fase2
            st.setString(1, hijo);
            st.setString(2, padre==null?hijo:padre);//13/04/2010 JJCastro
            st.setString(3, user);
            st.setString(4, user);
            respuesta = st.getSql();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
            throw new SQLException("ERROR DURANTE SQL_INSERTAR_SUBCLIENTE. \n " + e.getMessage());
        }finally{
            if (st  != null){ try{ st = null;              } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }

        return respuesta;
    }

    public String updatePadre(Cliente cl, String usuario) throws Exception {
        String respuesta = "";
        StringStatement st = null;
        String query = "SQL_UPDATE_PADRE";
        String sql = "";
        Connection conn = null;
        try {

           st = new StringStatement (this.obtenerSQL(query), true);//JJCastro fase2
            st.setString(1, cl.getId_padre());
            st.setString(2, usuario);
            st.setString(3, cl.getCodcli());
            respuesta = st.getSql();
            System.out.println(respuesta);

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e);
            throw e;
        } finally{
            if (st  != null){ try{ st = null;              } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }
        return respuesta;
    }

public String insertarOf(OfertaElca ofca, String usuario) throws Exception {
        String respuesta = "";
        StringStatement st = null;
        String query = "SQL_INSERT_OFERTA_ECA";
        try {
            st = new StringStatement (this.obtenerSQL(query), true);//JJCastro fase2

            SerieGeneral s_id_cliente = getSerie("FINV", "OP", "OFMS");
            setSerie("FINV", "OP", "OFMS");
            ofca.setId_solicitud(s_id_cliente.getUltimo_prefijo_numero());

            st.setString(1, ofca.getId_solicitud());
            st.setString(2, ofca.getId_cliente());
            st.setString(3, ofca.getDescripcion());
            st.setString(4, usuario);
            st.setString(5, ofca.getNic());
            st.setString(6, ofca.getTipo_solicitud());
            st.setString(7, ofca.getTipoDtf());
            st.setString(8, ofca.getTipo_solicitud());
            st.setString(9, ofca.getOficial());
            st.setString(10, ofca.getAviso());

            st.setString(11, ofca.getEstado_cartera());//JJCASTRO
            st.setString(12, ofca.getFec_val_cartera());//JJCASTRO
            st.setString(13, ofca.getResponsable());//RESPONSABLE OPAV JJCASTRO
            st.setString(14, ofca.getInterventor());//RESPONSABLE OPAV JJCASTRO

            respuesta = st.getSql();
            System.out.println(respuesta);

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e);
            throw e;
        }finally{
            if (st  != null){ try{ st = null;              } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }
        return respuesta;
    }

    public String insertarAcc(AccionesEca acceca, String usuario) throws Exception {
        String respuesta = "";
        StringStatement st = null;
        String query = "SQL_INSERT_ACCION_ECA";
        try {
           st = new StringStatement (this.obtenerSQL(query), true);//JJCastro fase2

            SerieGeneral s_id_cliente = getSerie("FINV", "OP", "ACMS");
            setSerie("FINV", "OP", "ACMS");
            acceca.setId_accion(s_id_cliente.getUltimo_prefijo_numero());
            
            st.setString(1, acceca.getId_accion());
            st.setString(2, acceca.getId_solicitud());
            st.setString(3, acceca.getContratista());
            st.setString(4, acceca.getDescripcion());
            st.setString(5, usuario);
            respuesta = st.getSql();
            System.out.println(respuesta);

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e);
            throw e;
        }finally{
            if (st  != null){ try{ st = null;              } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }
        return respuesta;
    }

    public String delAcc(AccionesEca acceca, String usuario) throws Exception {
        String respuesta = "";
        StringStatement st = null;
        String query = "SQL_DELETE_ACCION_ECA";
        try {
           st = new StringStatement (this.obtenerSQL(query), true);//JJCastro fase2

            st.setString(1, usuario);
            st.setString(2, acceca.getId_accion());
            respuesta = st.getSql();
            System.out.println(respuesta);

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e);
            throw e;
        } finally{
            if (st  != null){ try{ st = null;              } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }
        return respuesta;
    }

    public String delDetCot(AccionesEca acceca, String usuario) throws Exception {
        String respuesta = "";
        StringStatement st = null;
        String query = "SQL_UPDATE_DET_COT";
        try {
           st = new StringStatement (this.obtenerSQL(query), true);

            st.setString(1, usuario);
            st.setString(2, acceca.getId_accion());
            respuesta = st.getSql();
            System.out.println(respuesta);

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e);
            throw e;
        } finally{
            if (st  != null){ try{ st = null;              } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }
        return respuesta;
    }

    public String delCot(AccionesEca acceca, String usuario) throws Exception {
        String respuesta = "";
        StringStatement st = null;
        String query = "SQL_UPDATE_COT";
        try {
           st = new StringStatement (this.obtenerSQL(query), true);//JJCastro fase2

            st.setString(1, usuario);
            st.setString(2, acceca.getId_accion());
            respuesta = st.getSql();
            System.out.println(respuesta);

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e);
            throw e;
        } finally{
            if (st  != null){ try{ st = null;              } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }
        return respuesta;
    }


    /**
     *
     * @param cleca
     * @param usuario
     * @param sw
     * @return
     * @throws Exception
     */
    public String updateCl(Cliente cl, String usuario, boolean sw) throws Exception {
        String respuesta = "";
        StringStatement st = null;
        String query = "SQL_UPDATE_CLIENTE_ECA";
        String sql = "";
        try {
            String param = "";
            param = param + "nit='" + cl.getNit() + "', ";
            param = param + "nomcli='" + cl.getNomcli() + "', ";
            param = param + "nomcontacto='" + cl.getNomContacto() + "', ";
            param = param + "telcontacto='" + cl.getTelContacto() + "', ";
            param = param + "cel_contacto='" + cl.getCelContacto() + "', ";
            param = param + "tipo='" + cl.getTipo() + "', ";
            param = param + "sector='" + cl.getSector() + "', ";
            param = param + "ciudad='" + cl.getCiudad() + "', ";
            param = param + "direccion='" + cl.getDireccion() + "', ";
            param = param + "nombre_representante='" + cl.getNomRepresentante() + "', ";
            param = param + "cargo_contacto='" + cl.getCargoContacto() + "', ";
            param = param + "tel_representante='" + cl.getTelRepresentante() + "', ";
            param = param + "celular_representante='" + cl.getCelRepresentante() + "', ";
            param = param + "user_update='" + usuario + "', ";
            param = param + "edificio='" + cl.getEdif() + "', ";
            param = param + "email_contacto='" + cl.getEmail()+ "', ";
            if (sw) {
                param = param + "id_ejecutivo='" + cl.getIdEjecutivo() + "', ";
            }
            param = param + "esoficial='" + ((cl.isOficial()) ? "S" : "N") + "', ";
            param = param + "digito_verificacion ='" + cl.getDigito_verificacion()+ "', ";

            sql = this.obtenerSQL("SQL_UPDATE_CLIENTE_ECA").replaceAll("#PARAM#", param);//JJCastro fase2
            st = new StringStatement (sql, true);//JJCastro fase2
            st.setString(1, cl.getCodcli());
            respuesta = st.getSql();
            System.out.println(respuesta);

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e);
            throw e;
        }finally{
            if (st  != null){ try{ st = null;              } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }
        return respuesta;
    }


/**
 *
 * @param ofca
 * @param usuario
 * @return
 * @throws Exception
 */
    public String updateOf(OfertaElca ofca, String usuario) throws Exception {
        String respuesta = "";
        StringStatement st = null;
        String query = "SQL_UPDATE_OFERTA_ECA";
        String sql = "";
        try {
            String estado = this.estadoSolicitud(ofca.getId_solicitud());
            if(!(estado.equals("030") || estado.equals("040"))){
                ElectricaribeOfertaDAO eod=new ElectricaribeOfertaDAO();
                DateFormat dateFormat = new SimpleDateFormat("yyyyMM");
                //sql = eod.actPrecio_venta(ofca.getId_solicitud(), dateFormat.format(new java.util.Date()));
                sql = eod.actPrecio_venta(ofca.getId_solicitud(), "substring (replace (a.fecha_oferta,'-',''),1,6)");
                //2010-04-30 rhonalf
            }
            //st = new StringStatement (sql + this.obtenerSQL(query), true);//JJCastro fase2
            st = new StringStatement ( this.obtenerSQL(query)+sql , true);//JJCastro fase2

                st.setString(1, ofca.getDescripcion());
                //st.setString(2, ofca.getOficial());
                st.setString(2, usuario);

                st.setString(3, ofca.getEstado_cartera());//JJCASTRO
                st.setString(4, ofca.getFec_val_cartera());//JJCASTRO

                st.setString(5, ofca.getTipo_solicitud());//2010-04-30 rhonalf

                st.setString(6, ofca.getTipo_solicitud());//20100605

                st.setString(7, ofca.getAviso());
                st.setString(8, ofca.getResponsable());
                st.setString(9, ofca.getInterventor());
                st.setString(11, ofca.getId_solicitud());
                st.setString(10, ofca.getTipoDtf());

            respuesta = st.getSql();
            System.out.println(respuesta);

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e);
            throw e;
        }finally{
            if (st  != null){ try{ st = null;              } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }
        return respuesta;
    }



    /**
     *
     * @param idcliente
     * @param usuario
     * @param nics
     * @return
     * @throws Exception
     */
    public ArrayList<String> insertarNics(String idcliente, String usuario, String[] nics) throws Exception {
        ArrayList<String> respuesta = new ArrayList<String>();
        StringStatement st = null;
        String query = "SQL_INSERT_NICS";
        try {            
            for (int i = 0; nics != null && i < nics.length; i++) {
                st = new StringStatement (this.obtenerSQL(query), true);//JJCastro fase2
                st.setString(1, nics[i]);
                st.setString(2, idcliente);
                st.setString(3, usuario);
                st.setString(4, usuario);
                respuesta.add(st.getSql());
            }
            System.out.println(respuesta);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e);
            throw e;
        }finally{
            if (st  != null){ try{ st = null;              } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }
        return respuesta;
    }

    public String buscarCl(Cliente cl) throws Exception {
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String respuesta = "";
        boolean sw = false;
    
        String query = "SQL_GET_CLIENTE";
        String query2 = "SQL_GET_PADRE" ;

        try {
            con = this.conectarJNDI(query);
            if (con != null) {
             st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, cl.getCodcli());
            rs = st.executeQuery();

            if (rs.next()) {
                cl.setCiudad(rs.getString("ciudad"));
                 cl.setNomcli(rs.getString("nomcli"));
                
                
                //cleca.setDepartamento(rs.getString("departamento"));
                cl.setDireccion(rs.getString("direccion"));
                cl.setNit(rs.getString("nit"));
                cl.setCargoContacto(rs.getString("cargo_contacto"));
                cl.setNomContacto(rs.getString("nomcontacto"));
                cl.setTelContacto(rs.getString("telcontacto"));
                cl.setCelContacto(rs.getString("cel_contacto"));
                cl.setEmail(rs.getString("email_contacto"));
                cl.setNomRepresentante(rs.getString("nombre_representante"));                                      
                cl.setTelRepresentante(rs.getString("tel_representante"));
                cl.setCelRepresentante(rs.getString("celular_representante"));
                cl.setTipo(rs.getString("tipo"));
                cl.setIdEjecutivo(rs.getString("nombre_ejecutivo"));
                cl.setCreationUser(rs.getString("creation_user"));
                cl.setEdif(rs.getString("edificio"));
                cl.setNics(getNics(cl.getCodcli(), con));
                cl.setSector(rs.getString("sector"));
                cl.setDigito_verificacion(rs.getString("digito_verificacion"));
                sw = true;
            }


            if (sw) {
                st = con.prepareStatement(this.obtenerSQL(query2));//JJCastro fase2
                st.setString(1, cl.getCodcli());
                rs = st.executeQuery();

                if (rs.next()) {
                    cl.setId_padre(rs.getString("id_cliente_padre"));
                   // cl.setNombre_padre(rs.getString("nombre"));
                }
            }


            if (!sw) {
                respuesta = "No hay ningun usuario con ese id";
            }
            }} catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
            throw new SQLException("ERROR DURANTE SQL_GET_CLENTE \n " + e.getMessage());
        }finally{//JJCastro fase2
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return respuesta;
    }




    /**
     *
     * @param ofca
     * @return
     * @throws Exception
     */

    public String buscarOf(OfertaElca ofca) throws Exception {
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String respuesta = "";
        boolean sw = false;
        ofca.setCreacion_fecha_entrega_oferta("");
        ofca.setCreation_date("");
        ofca.setCreation_user("");
        ofca.setDescripcion("");
        ofca.setFecha_entrega_oferta("");
        ofca.setId_cliente("");
        ofca.setId_oferta("");
        ofca.setLast_update("");
        ofca.setNic("");
        ofca.setNum_os("");
        ofca.setReg_status("");
        ofca.setUser_update("");
        ofca.setUsuario_entrega_oferta("");
        String query = "SQL_GET_OFERTA";

        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, ofca.getId_solicitud());
            rs = st.executeQuery();
            if (rs.next()) {
                ofca.setCreacion_fecha_entrega_oferta(rs.getString("creacion_fecha_entrega_oferta"));
                ofca.setCreation_date(rs.getString("creation_date"));
                ofca.setCreation_user(rs.getString("creation_user"));
                ofca.setDescripcion(rs.getString("descripcion"));
                ofca.setFecha_entrega_oferta(rs.getString("fecha_entrega_oferta"));
                ofca.setId_cliente(rs.getString("id_cliente"));
                ofca.setId_oferta(rs.getString("id_oferta"));
                ofca.setLast_update(rs.getString("last_update"));
                ofca.setNic(rs.getString("nic"));
                ofca.setNum_os(rs.getString("num_os"));
                ofca.setReg_status(rs.getString("reg_status"));
                ofca.setUser_update(rs.getString("user_update"));
                ofca.setUsuario_entrega_oferta(rs.getString("usuario_entrega_oferta"));
                ofca.setTipo_solicitud(rs.getString("tipo_solicitud"));

                ofca.setEstado_cartera(rs.getString("estudio_cartera"));//jjcastro cot
                ofca.setFec_val_cartera(rs.getString("fecha_validacion_cartera"));//jjcastro cot

                ofca.setResponsable(rs.getString("responsable"));
                ofca.setInterventor(rs.getString("interventor"));

                sw = true;
            }
            if (!sw) {
                respuesta = "No hay ninguna solicitud con ese id";
            }
            }} catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
            throw new SQLException("ERROR DURANTE SQL_GET_OFERTA \n " + e.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return respuesta;
    }


    /**
     *
     * @param acceca
     * @param nitprop
     * @return
     * @throws Exception
     */
    public ArrayList buscarAcc(AccionesEca acceca, String nitprop) throws Exception {
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String respuesta = "";
        ArrayList arl = new ArrayList();
        boolean sw = false;
        String query = "SQL_GET_ACCIONES_ECA";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
           st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, acceca.getId_solicitud());
            if (nitprop.length() > 2 && nitprop.substring(0, 2).equals("CC")) {
                st.setString(2, nitprop);
            } else {
                st.setString(2, "%");
            }

            System.out.println("sqllll" + st.toString());
            rs = st.executeQuery();
            while (rs.next()) {
                AccionesEca newacc = new AccionesEca();
                newacc.setId_accion(rs.getString("id_accion"));
                newacc.setAdministracion(rs.getString("administracion"));
                newacc.setContratista(rs.getString("contratista"));
                newacc.setCreation_date(rs.getString("creation_date"));
                newacc.setCreation_user(rs.getString("creation_user"));
                newacc.setDescripcion(rs.getString("descripcion"));
                newacc.setEstado(rs.getString("estado"));
                newacc.setId_solicitud(rs.getString("id_solicitud"));
                newacc.setImprevisto(rs.getString("imprevisto"));
                newacc.setLast_update(rs.getString("last_update"));
                newacc.setMano_obra(rs.getString("mano_obra"));
                newacc.setMaterial(rs.getString("material"));
                newacc.setObservaciones(rs.getString("observaciones"));
                newacc.setPorc_administracion(rs.getString("porc_administracion"));
                newacc.setPorc_imprevisto(rs.getString("porc_imprevisto"));
                newacc.setPorc_utilidad(rs.getString("porc_utilidad"));
                newacc.setReg_status(rs.getString("reg_status"));
                newacc.setTipo_trabajo(rs.getString("tipo_trabajo"));
                newacc.setTransporte(rs.getString("transporte"));
                newacc.setUser_update(rs.getString("user_update"));
                newacc.setUtilidad(rs.getString("utilidad"));
                arl.add(newacc);
                sw = true;
            }
            if (!sw) {
                respuesta = "No hay ninguna accion con ese id";
            }
            }} catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
            throw new SQLException("ERROR DURANTE SQL_GET_ACCION \n " + e.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return arl;
    }




    /**
     *
     * @param param
     * @return
     * @throws Exception
     */
    public String[] getNics(String param) throws Exception {
        PreparedStatement st = null;
        ResultSet rs = null;
        ArrayList arl = new ArrayList();
        String[] arll = null;
        String query = "SQL_GET_NICS";
        Connection con = null;
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, param);
            rs = st.executeQuery();
            while (rs.next()) {
                arl.add(rs.getString("nic"));
            }
            arll = new String[arl.size()];
            for (int i = 0; i < arl.size(); i++) {
                arll[i] = (String) arl.get(i);
            }
            } } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
            throw new SQLException("ERROR DURANTE SQL_GET_NICS \n " + e.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return arll;
    }
   
    /**
     * @autor egonzalez
     * @param param
     * @param con
     * @return
     * @throws Exception
     */
    public String[] getNics(String param, Connection con) throws Exception {
        PreparedStatement st = null;
        ResultSet rs = null;
        ArrayList arl = new ArrayList();
        String[] arll = null;
        String query = "SQL_GET_NICS";

        try {
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setString(1, param);
                rs = st.executeQuery();
                while (rs.next()) {
                    arl.add(rs.getString("nic"));
                }
                arll = new String[arl.size()];
                for (int i = 0; i < arl.size(); i++) {
                    arll[i] = (String) arl.get(i);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE SQL_GET_NICS \n " + e.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
        }
        return arll;
    }




/**
 *
 * @param param
 * @return
 * @throws Exception
 */
    public String buscarMails(String param) throws Exception {
        PreparedStatement st = null;
        ResultSet rs = null;
        String arl = "";
        String query = "SQL_GET_MAILS";
        Connection con = null;
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, param);
            rs = st.executeQuery();
            while (rs.next()) {
                arl = arl + rs.getString("email") + ";";
            }
            }} catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
            throw new SQLException("ERROR DURANTE SQL_GET_MAILS \n " + e.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return arl;
    }



/**
 *
 * @param param
 * @return
 * @throws Exception
 */
    public String getMailEjecutivo(String param) throws Exception {
        PreparedStatement st = null;
        ResultSet rs = null;
        String arl = "";

        Connection con = null;
        String query = "SQL_GET_MAIL_EJECUTIVO";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, param);
            rs = st.executeQuery();
            if (rs.next()) {
                arl = rs.getString("correo");
            }
            }} catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
            throw new SQLException("ERROR DURANTE SQL_GET_MAIL_EJECUTIVO \n " + e.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return arl;
    }


/**
 *
 * @param param
 * @return
 * @throws Exception
 */
    public String getMailContratista(String param) throws Exception {
        PreparedStatement st = null;
        ResultSet rs = null;
        String arl = "";
        Connection con = null;
        String query = "SQL_GET_MAIL_CONTRATISTA";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
             st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, param);
            rs = st.executeQuery();
            if (rs.next()) {
                arl = rs.getString("email");
            }
            }} catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
            throw new SQLException("ERROR DURANTE SQL_GET_MAIL_CONTRATISTA \n " + e.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return arl;
    }


/**
 *
 * @param param
 * @return
 * @throws Exception
 */
    public String getLoginEjecutivo(String param) throws Exception {
        PreparedStatement st = null;
        ResultSet rs = null;
        String arl = "";
        Connection con = null;
        String query = "SQL_GET_LOGIN_EJECUTIVO";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, param);
            rs = st.executeQuery();
            if (rs.next()) {
                arl = rs.getString("idusuario");
            }
        }} catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
            throw new SQLException("ERROR DURANTE SQL_LOGIN_EJECUTIVO \n " + e.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return arl;
    }



/**
 *
 * @param param
 * @return
 * @throws Exception
 */
    public ArrayList getDatos(String param) throws Exception {
        PreparedStatement st = null;
        ResultSet rs = null;
        ArrayList arl = new ArrayList();
        Connection con = null;
        String query = "SQL_GET_DATOS";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query).replaceAll("#PARAM", param));//JJCastro fase2
            rs = st.executeQuery();
            while (rs.next()) {
                arl.add(rs.getString("datos"));
            }
            }} catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
            throw new SQLException("ERROR DURANTE SQL_SET_MS. \n " + e.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return arl;
    }


    /**
     *
     * @return
     * @throws Exception
     */
    public ArrayList getEjecutivos() throws Exception {
        PreparedStatement st = null;
        ResultSet rs = null;
        ArrayList arl = new ArrayList();
        Connection con = null;
        String query = "SQL_GET_EJECUTIVOS";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            rs = st.executeQuery();
            while (rs.next()) {
                arl.add(new String[]{rs.getString("nombre"), rs.getString("id_ejecutivo") + "," + rs.getString("nombre")});
            }
            }} catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
            throw new SQLException("ERROR DURANTE SQL_GET_EJECUTIVOS. \n " + e.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return arl;
    }



/**
 *
 * @return
 * @throws Exception
 */
    public ArrayList getPadres() throws Exception {
        PreparedStatement st = null;
        ResultSet rs = null;
        ArrayList arl = new ArrayList();
        Connection con = null;
        String query = "SQL_GET_PADRES";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            rs = st.executeQuery();
            while (rs.next()) {
                arl.add(new String[]{rs.getString("nomcli"), rs.getString("id_cliente_padre")});
            }
            }} catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
            throw new SQLException("ERROR DURANTE SQL_GET_PADRES. \n " + e.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return arl;
    }

/**
 *
 * @param id_sol
 * @return
 * @throws SQLException
 */
    public String getAviso(String id_sol) throws SQLException{
        PreparedStatement   st    = null;
        ResultSet           rs    = null;
        ArrayList           arl   = new ArrayList();
        Connection          con  = null;
        String              aviso = "";
        String query = "SQL_GET_AVISO";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, id_sol);
            rs = st.executeQuery();

            if (rs.next()) {
                aviso = rs.getString("aviso");
            }

            }}
        catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
            throw new SQLException("ERROR DURANTE SQL_GET_AVISO. \n " + e.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return aviso;
    }



    /**
     *
     * @return
     * @throws Exception
     */
    public ArrayList getContratistas() throws Exception {
        PreparedStatement st = null;
        ResultSet rs = null;
        ArrayList arl = new ArrayList();
        Connection con = null;
        String query = "SQL_GET_CONTRATISTAS";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            rs = st.executeQuery();
            while (rs.next()) {
                arl.add(new String[]{rs.getString("descripcion"), rs.getString("id_contratista")});
            }
            }} catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
            throw new SQLException("ERROR DURANTE SQL_GET_CONTRATISTAS. \n " + e.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return arl;
    }


 /**
  *
  * @return
  * @throws Exception
  */
    public ArrayList getEstado() throws Exception {
        PreparedStatement st = null;
        ResultSet rs = null;
        ArrayList arl = new ArrayList();
        Connection con = null;
        String query = "SQL_GET_ESTADOS";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            rs = st.executeQuery();
            while (rs.next()) {
                arl.add(new String[]{rs.getString("estado"), rs.getString("id_estado")});
            }
        }} catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
            throw new SQLException("ERROR DURANTE SQL_GET_ESTADOS. \n " + e.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return arl;
    }


/**
 *
 *
 * @param perfil
 * @param accion
 * @return
 * @throws Exception
 */
    public boolean ispermitted(String perfil, String accion) throws Exception {
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection conn = null;
        boolean answ = false;
        String acciones = "";
        String sql = "";
        String query = "SQL_GET_PERMISO";

        try {
            String arg[] = perfil.split(",");
            for(int i = 0;i < arg.length - 1;i++){
               acciones += "upper('"+arg[i]+accion+"'),";
             }

            acciones += "upper('" + arg[arg.length - 1] + accion + "')";
            sql = this.obtenerSQL("SQL_GET_PERMISO");
            sql = sql.replaceAll("param1", acciones);

            conn = this.conectarJNDI(query);//JJCastro fase2
            if (conn != null) {
                st = conn.prepareStatement(sql);
                st.setString(1, accion);
                rs = st.executeQuery();

                while (rs.next()) {
                    if (rs.getString("dato").equals("S")) {
                        answ = true;
                        break;
                    }
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
            throw new SQLException("ERROR DURANTE SQL_GET_PERMISO. \n " + e.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (conn != null){ try{ this.desconectar(conn); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return answ;
    }
   /*ANTES GARIZAO
    public boolean ispermitted(String perfil, String accion) throws Exception {
        PreparedStatement st = null;
        ResultSet rs = null;
        ArrayList arl = new ArrayList();
        Connection con = null;
        String answ = "N";
        String query = "SQL_GET_PERMISO";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, perfil);
            st.setString(2, accion);
            rs = st.executeQuery();
            if (rs.next()) {
                answ = rs.getString("dato");
            }
        }} catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
            throw new SQLException("ERROR DURANTE SQL_GET_PERMISO. \n " + e.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        if (answ.equals("S")) {
            return true;
        } else {
            return false;
        }
    }*/


/**
 *
 * @param usuario
 * @return
 * @throws Exception
 */
 public String getPerfil(String usuario) throws Exception {
        PreparedStatement st = null;
        ResultSet rs = null;
        ArrayList arl = new ArrayList();
        Connection con = null;
        String answ = "";
        String query = "SQL_GET_PERFIL";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, usuario);
            rs = st.executeQuery();
            if (rs.next()) {
                answ = rs.getString("dato");
            }
            }} catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
            throw new SQLException("ERROR DURANTE SQL_GET_PERMISO. \n " + e.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return answ;
    }


 /**
  *
  * @param dstrct
  * @param agency_id
  * @param document_type
  * @return
  * @throws SQLException
  */

    public SerieGeneral getSerie(String dstrct, String agency_id, String document_type) throws SQLException {

        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String query = "SQL_GET_ULTIMO_NUMERO";
        SerieGeneral serie = null;
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, dstrct);
            st.setString(2, agency_id);
            st.setString(3, document_type);
           rs = st.executeQuery();

            if (rs.next()) {
                serie = new SerieGeneral();
                serie = (SerieGeneral.load(rs));
            }

            }} catch (Exception e) {
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA SELECCION DE LA BUSQUEDA DE UNA SERIE. \n " + e.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return serie;

    }



/**
 *
 * @param dstrct
 * @param agency_id
 * @param document_type
 * @throws SQLException
 */
    public void setSerie(String dstrct, String agency_id, String document_type) throws SQLException {

        PreparedStatement st = null;
        Connection con = null;
        String query = "SQL_SET_ULTIMO_NUMERO";

        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, dstrct);
            st.setString(2, agency_id);
            st.setString(3, document_type);

            st.executeUpdate();

            }} catch (Exception e) {
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA ACTUALIZACION DE LA SERIE.  \n " + e.getMessage());
        }finally{
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }

    }

/**
 *
 * @param comandosSQL
 * @throws SQLException
 */
    public void ejecutarSQL(Vector comandosSQL) throws SQLException {

        Connection con = null;
        Statement stmt = null;
        String query = "SQL_GET_CONECTION";

        try {
            con = this.conectarJNDI(query);//JJCastro fase2
            if (con != null) {
                con.setAutoCommit(false);
                stmt = con.createStatement();
                for (int i = 0; i < comandosSQL.size(); i++) {
                    String comando = (String) comandosSQL.elementAt(i);
                    ////System.out.println(comando);
                    stmt.executeUpdate(comando);
                }
                con.commit();
            }
        } catch (SQLException e) {
            // Efectuando rollback en caso de error
            try {
                con.rollback();
            } catch (SQLException ignored) {
                System.out.println("error en execuuute" + ignored.toString());
                throw new SQLException("NO SE PUDO HACER ROLLBACK " + ignored.getMessage() + " " + ignored.getErrorCode() + " <br> La siguiente exception es : ----" + ignored.getNextException());
            }
            throw new SQLException("ERROR DURANTE LA TRANSACCION, LOS CAMBIOS NO SE PRODUJERON EL LA BASE DE DATOS " + e.getMessage() + " " + e.getErrorCode() + " <br> La siguiente exception es : ----" + e.getNextException());
        }finally{
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }




    public String[] getDatosMensaje(String accion) throws Exception{
        PreparedStatement st = null;
        ResultSet rs = null;

        String[] arll = new String[4];

        Connection con = null;
        String query = "SQL_GET_DATOS_MSG";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, accion);
            rs = st.executeQuery();
            if (rs.next()) {
                arll[0] = rs.getString("nombre_contra");
                arll[1] = rs.getString("sector");
                arll[2] = rs.getString("nombre_cli");
                arll[3] = rs.getString("mail_contra");
            }
            }} catch (Exception e) {
            e.printStackTrace();
            System.out.println("error en getDatosMensaje:" + e.toString());
            throw new SQLException("ERROR DURANTE SQL_GET_DATOS_MSG \n " + e.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return arll;
    }




    /**
     *
     * @param id
     * @return
     * @throws Exception
     */
    public boolean isOficial(String id) throws Exception {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_IS_OFICIAL";
        String sql = "";
        boolean ok = false;

        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            ps.setString(1, id);
            rs = ps.executeQuery();

            if (rs.next()) {
                ok = true;
            }
            }} catch (Exception e) {
            throw new Exception(e.getMessage());
        } finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }

        return ok;
    }
    //2010-02-04
    //Busca en que estado se encuentra una accion
    public String estadoAccion(String accion) throws Exception{
        String estado="";
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "ESTADO_ACCION";
        String sql = "";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
           ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            ps.setString(1, accion);
            rs = ps.executeQuery();
            if(rs.next()) estado = rs.getString("estado");
            }}
        catch (Exception e) {
            throw new Exception("Ha ocurrido un error al buscar el estado de la accion "+accion+" :"+e.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return estado;
    }
     /**
     * Busca el estado en que esta una solicitud
     * @author rhonalf
     * @param solicitud el id_solicitud
     * @return String con el estado
     * @throws Exception Cuando hay un error
     * @since 2010-04-30
     */
    public String estadoSolicitud(String solicitud) throws Exception{
        String estado="";
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "ESTADO_SOL";
        String sql = "";
        try{
            con = this.conectarJNDI(query);
            sql = this.obtenerSQL(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, solicitud);
            rs = ps.executeQuery();
            if(rs.next()) estado = rs.getString("estado");
            rs.close();
            ps.close();
            this.desconectar(con);
        }
        catch(Exception e){
            throw new Exception("Error al buscar los estados de la solicitud: "+e.toString());
        }
        finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (!con.isClosed()){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return estado;
    }

    /**
     * Devuelve los meses de mora de una solicitud
     * @author rhonalf
     * @param solicitud el id_solicitud
     * @return String con el dato
     * @throws Exception Cuando hay error
     * @since 2010-04-30
     */
    public String getMesesMora(String solicitud) throws Exception{
        String meses = "";
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "MES_MORA";
        String sql = "";
        try {
            con = this.conectarJNDI(query);
            sql = this.obtenerSQL(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, solicitud);
            rs = ps.executeQuery();
            if(rs.next()) meses = rs.getString("esoficial");
            rs.close();
            ps.close();
            this.desconectar(con);
        }
        catch (Exception e) {
            throw new Exception("Error al buscar los meses de mora: "+e.toString());
        }
        finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (ps  != null){ try{ ps.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (!con.isClosed()){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return meses;
    }
    /**
     * Obtiene el listado de departamentos de clientes_eca
     * @return ArrayList con los departamentos
     * @throws Exception
     * @author darrieta-Geotech 08/05/2010
     */
     public ArrayList listarDepartamentos() throws Exception {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_BUSCAR_DEPARTAMENTO";
        ArrayList listaDpto = new ArrayList();
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));
                rs = st.executeQuery();
                while (rs.next()) {
//                    listaDpto.add(rs.getString("departamento"));

                    listaDpto.add(new String[]{rs.getString("department_code"), rs.getString("department_name")});//------- RESPONSABLE OPAV JCASTRO

                }
            }
        } catch (Exception e) {
            throw new Exception("Error en listarDepartamentos[ClientesVerDAO] " + e.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return listaDpto;
    }

    /**
     * Obtiene el listado de ejecutivos
     * @return ArrayList de String[] con los ejecutivos obtenidos
     * @throws Exception
     * @author darrieta-Geotech 08/05/2010
     */
    public ArrayList buscarEjecutivo(String ejecutivo) throws Exception {
        PreparedStatement st = null;
        ResultSet rs = null;
        ArrayList arl = new ArrayList();
        Connection con = null;
        String query = "SQL_BUSCAR_EJECUTIVOS";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setString(1, ejecutivo.toUpperCase());
                rs = st.executeQuery();
                while (rs.next()) {
                    arl.add(new String[]{rs.getString("id_ejecutivo"), rs.getString("nombre")});
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE listarEjecutivos[ClientesVerDAO] \n " + e.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return arl;
    }

    /**
     * Obtiene el listado de ejecutivos
     * @return ArrayList de String[] con los ejecutivos obtenidos
     * @throws Exception
     * @author darrieta-Geotech 08/05/2010
     */
    public ArrayList buscarCliente(String cliente) throws Exception {
        PreparedStatement st = null;
        ResultSet rs = null;
        ArrayList arl = new ArrayList();
        Connection con = null;
        String query = "SQL_BUSCAR_CLIENTES";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setString(1, cliente.toUpperCase());
                rs = st.executeQuery();
                while (rs.next()) {
                    arl.add(new String[]{rs.getString("id_cliente"), rs.getString("nombre")});
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE listarClientes[ClientesVerDAO] \n " + e.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return arl;
    }
    public ArrayList buscarId() throws Exception{
        ArrayList cl = null;
        PreparedStatement ps = null;
        Connection con = null;
        ResultSet rs = null;
        String sql = "";
        String query="BUSCAR_ID";
        try {
            cl = new ArrayList();
            sql = this.obtenerSQL(query);
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while(rs.next()){
                cl.add(rs.getString("nombre")+";_;"+rs.getString("nit"));
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error al buscar datos: "+e.toString());
        }
        finally{
            if (ps!= null){ try{ ps=null; } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (!con.isClosed()){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return cl;
    }


/**
     * Permite hacer el proceso de migracion de clientes de consorcio a fintra
     * @return cadena con string de fecha de proceso en el formato AAAAMMDD
     * @throws Exception cuando ocurre error en el proceso
     */
    /**
     * Permite hacer el proceso de migracion de clientes de consorcio a fintra
     * @return cadena con string de fecha de proceso en el formato AAAAMMDD
     * @throws Exception cuando ocurre error en el proceso
     */
    public String insercion() throws Exception{
        PreparedStatement ps = null;
        Connection con = null;
        ResultSet res = null;
        int rs = 0;
        String sql = "";
        String query="INS_CL";
        String stringfec="";
        try {
            //Se insertan los registros en la tabla cliente (consorcio)
            System.out.println("Inicia migracion de clientes consorcio -> fintra");
            SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMdd");
            stringfec = fmt.format( new java.util.Date() );
            sql = this.obtenerSQL(query);
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(sql);
            rs = ps.executeUpdate();
            this.desconectar(con);
            System.out.println("insercion en cliente(consorcio)");
            if(rs==0){ System.out.println("no se ingresaron registros en clientesverdao en la func. insercion"); }
            //else{//Comentado en 20100817
                //sacar los registros de la tabla cliente en consorcio
                ArrayList<Cliente> listcl = this.crearLista();
                //this.desconectar(con);
                System.out.println("backup de cliente(consorcio) listo");
                //crear las tablas temporales(consorcio)
                query="TEMP_CL";
                sql = this.obtenerSQL(query);
                sql = sql.replaceAll("#param", stringfec);
                con = this.conectarJNDI(query);
                ps = con.prepareStatement(sql);
                ps.executeUpdate();
                this.desconectar(con);
                System.out.println("crear las tablas temporales(consorcio) listo");
                //crear las tablas temporales(fintra)
                query="TEMP_CL2";
                sql = this.obtenerSQL(query);
                sql = sql.replaceAll("#param", stringfec);
                con = this.conectarJNDI(query);
                ps = con.prepareStatement(sql);
                ps.executeUpdate();
                //this.desconectar(con);
                System.out.println("crear las tablas temporales(fintra) listo");
                //insertar datos en la tabla temporal (fintra)
                query="INS_TEM";
                sql = this.obtenerSQL(query);
                sql = sql.replaceAll("#param", stringfec);
                //con = this.conectarJNDI(query);
                for (int i = 0; i < listcl.size(); i++) {
                    ps = con.prepareStatement(sql);
                    ps.setString(1, listcl.get(i).getCodcli());
                    ps.setString(2, listcl.get(i).getNomcli());
                    ps.setString(3, listcl.get(i).getNotas());
                    ps.setString(4, listcl.get(i).getAgduenia());
                    ps.setString(5, listcl.get(i).getReg_status());
                    ps.setString(6, listcl.get(i).getCreation_date());
                    ps.setString(7, listcl.get(i).getLast_update());
                    ps.setString(8, listcl.get(i).getBase());
                    ps.setString(9, listcl.get(i).getTexto_oc());
                    ps.setString(10, listcl.get(i).getNit());
                    ps.setFloat(11, listcl.get(i).getRentabilidad());
                    ps.setString(12, listcl.get(i).getBank_account_no());
                    ps.setString(13, listcl.get(i).getCmc());
                    ps.setString(14, listcl.get(i).getUnidad());
                    ps.setString(15, listcl.get(i).getAgenciaFacturacion());
                    ps.setString(16, listcl.get(i).getDireccion());
                    ps.setString(17, listcl.get(i).getTelefono());
                    ps.executeUpdate();
                }
                //this.desconectar(con);
                System.out.println("insertar datos en la tabla temporal (fintra) listo");
                //insertar en la tabla cliente (fintra)
                query="INS_CL_FIN";
                sql = this.obtenerSQL(query);
                sql = sql.replaceAll("#param", stringfec);
                //con = this.conectarJNDI(query);
                ps = con.prepareStatement(sql);
                rs = ps.executeUpdate();
                if(rs==0){ System.out.println("no se ingresaron registros en clientesverdao en la tabla cliente (fintra)"); }
                this.desconectar(con);
                System.out.println("insertar en la tabla cliente (fintra) listo");
                //insertar en equivalencia
                query="INS_EQ";
                sql = this.obtenerSQL(query);
                con = this.conectarJNDI(query);
                ps = con.prepareStatement(sql);
                rs = ps.executeUpdate();
                //this.desconectar(con);
                if(rs==0){ System.out.println("no se ingresaron registros en clientesverdao en la tabla equivalencia (fintra)"); }
                System.out.println("insertar en equivalencia(fintra) listo");
                //se procesan los casos en los que coinciden tanto los nit como el nombre
                query="INS_EQ_DIR";
                sql = this.obtenerSQL(query);
                sql = sql.replaceAll("#param", stringfec);
                ps = con.prepareStatement(sql);
                rs = ps.executeUpdate();
                System.out.println("Casos automaticos procesados(fintra)");
                query="TEMP_EQ";
                sql = this.obtenerSQL(query);
                sql = sql.replaceAll("#param", stringfec);
                //con = this.conectarJNDI(query);
                ps = con.prepareStatement(sql);
                rs = ps.executeUpdate();
                this.desconectar(con);
                System.out.println("backup de equivalencia(fintra) listo");
                //restaurar equivalencia en consorcio
                this.restoreEq();
                System.out.println("restore de equivalencia(consorcio) listo");
            //}//Comentado en 20100817
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error al insertar datos en clientesverdao en la func. insercion: "+e.toString());
        }
        finally{
            if (ps!= null){ try{ ps=null; } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (!con.isClosed()){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        System.out.println("Fin migracion de clientes consorcio -> fintra");
        return stringfec;
    }

/**
     * Ver los datos que no pudieron incluirse en la tabla de clientes (consorcio)
     * @param cadtabla fecha de creacion de la tabla temporal (formato AAAAMMDD)
     * @return listado con los datos
     * @throws Exception cuando hay error
     */
    public ArrayList<String> verEquiv1(String cadtabla) throws Exception{
        ArrayList<String> listado = null;
        PreparedStatement ps = null;
        Connection con = null;
        ResultSet rs = null;
        String sql = "";
        String query="VER1_EQ";
        try {
            listado = new ArrayList();
            sql = this.obtenerSQL(query);
            sql = sql.replaceAll("#param", cadtabla);
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while(rs.next()){
                listado.add(rs.getString("codcli_consorcio")+";_;"+rs.getString("nit_consorcio")+";_;"+rs.getString("nombre_consorcio"));
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error en verEquiv1: "+e.toString());
        }
        finally{
            if (ps!= null){ try{ ps=null; } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (!con.isClosed()){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return listado;
    }



/**
     * Ver los datos que no pudieron incluirse en la tabla de clientes (fintra)
     * @param cadtabla fecha de creacion de la tabla temporal (formato AAAAMMDD)
     * @return listado con los datos
     * @throws Exception cuando hay error
     */
    public ArrayList<String> verEquiv2(String cadtabla) throws Exception{
        ArrayList<String> listado = null;
        PreparedStatement ps = null;
        Connection con = null;
        ResultSet rs = null;
        String sql = "";
        String query="VER2_EQ";
        try {
            listado = new ArrayList();
            sql = this.obtenerSQL(query);
            sql = sql.replaceAll("#param", cadtabla);
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while(rs.next()){
                listado.add(rs.getString("codcli_fintra")+";_;"+rs.getString("nit_fintra")+";_;"+rs.getString("nombre_fintra"));
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error en verEquiv2: "+e.toString());
        }
        finally{
            if (ps!= null){ try{ ps=null; } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (!con.isClosed()){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return listado;
    }

    /**
     * Saca los registros de la tabla cliente en consorcio
     * @return ArrayList de tipo Cliente
     * @throws Exception cuando hay error
     */
    public ArrayList<Cliente> crearLista() throws Exception{
        ArrayList<Cliente> listcl = null;
        PreparedStatement ps = null;
        Connection con = null;
        ResultSet res = null;
        String sql = "";
        String query="";
        try {
            listcl = new ArrayList<Cliente>();
            query="BACK_CL";
            sql = this.obtenerSQL(query);
            Cliente client = null;
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(sql);
            res = ps.executeQuery();//si hay filas
            while(res.next()){
                client = new Cliente();
                client.setAgduenia(res.getString("agduenia"));
                client.setAgenciaFacturacion(res.getString("agfacturacion"));
                client.setAgente(res.getString("cedagente"));
                client.setBank_account_no(res.getString("bank_account_no"));
                client.setBase(res.getString("base"));
                client.setCmc(res.getString("cmc"));//no se que es, pero va ...
                client.setCodcli( res.getString("codcli") );
                client.setCreation_date(res.getString("creation_date"));
                client.setDireccion(res.getString("direccion"));
                client.setEmail(res.getString("nomcli"));
                client.setEstado(res.getString("nomcli"));
                client.setLast_update(res.getString("last_update"));
                client.setNit(res.getString("nit"));
                client.setNomcli(res.getString("nomcli") );
                client.setNotas( res.getString("notas") );
                client.setReg_status(res.getString("reg_status"));
                client.setRentabilidad(res.getFloat("rentabilidad"));//?
                client.setRif(res.getString("rif"));//?
                client.setSec_standard(res.getString("notas"));
                client.setTelefono(res.getString("telefono"));
                client.setTexto_oc(res.getString("texto_oc"));
                client.setUnidad(res.getString("unidad"));
                listcl.add(client);
            }
        }
        catch (Exception e) {
            System.out.println("Error en crearLista: "+e.toString());
            e.printStackTrace();
            throw new Exception("Error en crearLista: "+e.toString());
        }
        finally{
            if (ps!= null){ try{ ps=null; } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (!con.isClosed()){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return listcl;
    }

    /**
     * Datos de la tabla equivalencia (fintra)
     * @return Listado con los datos
     * @throws Exception cuando hay error
     */
    public ArrayList<String> datoseq() throws Exception{
        ArrayList<String> lista = null;
        PreparedStatement ps = null;
        Connection con = null;
        ResultSet rs = null;
        String sql = "";
        String query="";
        try {
            query="OBTAIN_EQ";
            lista = new ArrayList<String>();
            sql = this.obtenerSQL(query);
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(sql);
            rs = ps.executeQuery();
            while(rs.next()){
                lista.add(rs.getString("codigo_inicial")+";_;"+rs.getString("codigo_final"));
            }
        }
        catch (Exception e) {
            System.out.println("Error en ClientesVerDAO en el metodo datoseq: ");
            e.printStackTrace();
            throw new Exception("Error en ClientesVerDAO en el metodo datoseq: "+e.toString());
        }
        finally{
            if (ps!= null){ try{ ps=null; } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (!con.isClosed()){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;
    }

    /**
     * Reestablecer la tabla equivalencia de fintra en consorcio
     * @throws Exception cuando ocurre error
     */
    public void restoreEq() throws Exception{
        ArrayList<String> lista = null;
        PreparedStatement ps = null;
        Connection con = null;
        String sql = "";
        String query="";
        StringStatement consulta = null;
        String consql = "";
        String[] cad = null;
        try {
            lista = this.datoseq();
            if(lista.size()>0){
                query="DEL_EQ";
                sql = this.obtenerSQL(query);
                con = this.conectarJNDI(query);
                ps = con.prepareStatement(sql);
                ps.executeUpdate();
                System.out.println("borrar datos de la tabla equivalencia(consorcio) listo");
                query="REST_EQ";
                sql = this.obtenerSQL(query);
                for (int i = 0; i < lista.size(); i++) {
                    consulta = new StringStatement(sql, true);
                    cad = (lista.get(i)).split(";_;");
                    consulta.setString(1, cad[0]);
                    consulta.setString(2, cad[1]);
                    consql = consql + consulta.getSql() + "\n";
                }
                //con = this.conectarJNDI(query);
                ps = con.prepareStatement(consql);
                ps.executeUpdate();
                System.out.println("insertar datos en la tabla equivalencia(consorcio) listo");
            }
        }
        catch (Exception e) {
            System.out.println("Error en ClientesVerDAO en el metodo restoreEq: ");
            e.printStackTrace();
            throw new Exception("Error en ClientesVerDAO en el metodo restoreEq: "+e.toString());
        }
        finally{
            if (ps!= null){ try{ ps=null; } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (!con.isClosed()){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }

/**
     * Inserta una lista de clientes de consorcio en la tabla cliente de fintra
     * @param lista listado de codigos de clientes a pasar
     * @param stringfec fecha del proceso en formato AAAAMMDD
     * @throws Exception caundo hay error en el proceso
     */
    public void insFin(ArrayList lista,String stringfec) throws Exception{
        PreparedStatement ps = null;
        Connection con = null;
        String sql = "";
        String query="";
        String globalsql="";
        StringStatement st = null;
        int k = 0;
        try {
            if(lista.size()>0){
                query="INS_CL_FINV";
                sql = this.obtenerSQL(query);
                Cliente client = null;
                System.out.println("Inicia insercion directa en fintra desde consorcio");
                for(int i=0;i<lista.size();i++){
                    client = this.datosCliente((String)(lista.get(i)));
                    st = new StringStatement(sql, true);
                    st.setString(1, client.getNomcli());
                    st.setString(2, client.getNotas());
                    st.setString(3, client.getAgduenia());
                    st.setString(4, client.getTexto_oc());
                    st.setString(5, client.getNit());
                    st.setString(6, client.getBank_account_no());
                    st.setString(7, client.getCmc());
                    st.setString(8, client.getUnidad());
                    st.setString(9, client.getAgenciaFacturacion());
                    st.setString(10, client.getDireccion());
                    st.setString(11, client.getTelefono());
                    st.setString(12, client.getCodcli());
                    globalsql += st.getSql() + "\n";
                    st = null;
                }
                con = this.conectarJNDI(query);
                ps = con.prepareStatement(globalsql);
                k = ps.executeUpdate();
                if(k==0) System.out.println("No se insertaron filas en la tabla");
                else {
                    System.out.println("Insercion de filas fue correcta");
                    query="INS_EQ";
                    sql = this.obtenerSQL(query);
                    ps = con.prepareStatement(sql);
                    k = ps.executeUpdate();
                    if(k==0){ System.out.println("no se ingresaron registros en clientesverdao en la tabla equivalencia (fintra)"); }
                    System.out.println("insertar en equivalencia(fintra) listo");
                    query="TEMP_EQ";
                    sql = this.obtenerSQL(query);
                    sql = sql.replaceAll("#param", stringfec);
                    ps = con.prepareStatement(sql);
                    k = ps.executeUpdate();
                    this.desconectar(con);
                    System.out.println("backup de equivalencia(fintra) listo");
                    this.restoreEq();
                    System.out.println("restore de equivalencia(consorcio) listo");
                }
                System.out.println("Fin insercion directa en fintra desde consorcio");
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error en ClientesVerDAO en el metodo insFin: "+e.toString());
        }
        finally{
            if (ps!= null){ try{ ps=null; } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (!con.isClosed()){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }

    /**
     * Busca los datos de un cliente basandose en el codigo
     * @param codcli El codigo del cliente
     * @return Objeto de tipo Cliente con los datos
     * @throws Exception Cuando hay error
     */
    public Cliente datosCliente(String codcli) throws Exception{
        Cliente client = null;
        PreparedStatement ps = null;
        Connection con = null;
        ResultSet res = null;
        String sql = "";
        String query="";
        try {
            client = new Cliente();
            client.setCodcli(codcli);
            query="BUSCAR_CL";
            sql = this.obtenerSQL(query);
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, codcli);
            res = ps.executeQuery();
            if(res.next()){
                client.setAgduenia(res.getString("agduenia"));
                client.setAgenciaFacturacion(res.getString("agfacturacion"));
                client.setAgente(res.getString("cedagente"));
                client.setBank_account_no(res.getString("bank_account_no"));
                client.setBase(res.getString("base"));
                client.setCmc(res.getString("cmc"));
                client.setCreation_date(res.getString("creation_date"));
                client.setDireccion(res.getString("direccion"));
                client.setEmail(res.getString("nomcli"));
                client.setEstado(res.getString("nomcli"));
                client.setLast_update(res.getString("last_update"));
                client.setNit(res.getString("nit"));
                client.setNomcli(res.getString("nomcli") );
                client.setNotas( res.getString("notas") );
                client.setReg_status(res.getString("reg_status"));
                client.setRentabilidad(res.getFloat("rentabilidad"));
                client.setRif(res.getString("rif"));
                client.setSec_standard(res.getString("notas"));
                client.setTelefono(res.getString("telefono"));
                client.setTexto_oc(res.getString("texto_oc"));
                client.setUnidad(res.getString("unidad"));
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error en ClientesVerDAO en el metodo datosCliente: "+e.toString());
        }
        finally{
            if (ps!= null){ try{ ps=null; } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (!con.isClosed()){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return client;
    }

/**
     * Busca las coincidencias de un nit en la tabla de clientes
     * @param nit Dato a buscar
     * @param fec fecha del proceso en formato AAAAMMDD
     * @return Listado de coincidencias
     * @throws Exception Cuando hay error
     */
    public ArrayList<String> buscarReps(String nit,String fec) throws Exception{
        ArrayList<String> lista = null;
        PreparedStatement ps = null;
        Connection con = null;
        ResultSet res = null;
        String sql = "";
        String query="BUSCAR_REPS";
        try {
            lista = new  ArrayList<String>();
            sql = this.obtenerSQL(query);
            sql = sql.replaceAll("#param", fec);
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, nit);
            ps.setString(2, nit); // 20100810 rh
            res = ps.executeQuery();
            while(res.next()){
                lista.add(res.getString("nombre")+";_;"+res.getString("nit"));
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error en ClientesVerDAO en el metodo buscarReps: "+e.toString());
        }
        finally{
            if (ps!= null){ try{ ps=null; } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (!con.isClosed()){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;
    }



    /**
     * Inserta registros directamente en la tabla de equivalencia
     * @param lista Listado con codigos de clientes de consorcio a insertar en equivalencia
     * @param stringfec fecha del proceso en formato AAAAMMDD
     * @throws Exception caundo hay error en el proceso
     */
    public void directoequiv(ArrayList lista,String stringfec) throws Exception{
        PreparedStatement ps = null;
        Connection con = null;
        String sql = "";
        String query="REST_EQF";
        String idfinv = "";
        StringStatement st = null;
        String codigo = "";
        System.out.println("inicia insercion directa en equivalencia");
        try {
            sql = this.obtenerSQL(query);
            for(int i=0;i<lista.size();i++){
                st = new StringStatement(sql, true);
                idfinv = this.codclifinv(this.nitcons(((String)(lista.get(i)))));
                st.setString(1, ((String)(lista.get(i))) );
                st.setString(2, idfinv);
                codigo = codigo + st.getSql() + "\n";
                st = null;
            }
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(codigo);
            ps.executeUpdate();
            query="TEMP_EQ";
            sql = this.obtenerSQL(query);
            sql = sql.replaceAll("#param", stringfec);
            ps = con.prepareStatement(sql);
            ps.executeUpdate();
            this.desconectar(con);
            System.out.println("backup de equivalencia(fintra) listo");
            this.restoreEq();
            System.out.println("restore de equivalencia(consorcio) listo");
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error en ClientesVerDAO en el metodo directoequiv: "+e.toString());
        }
        finally{
            if (ps!= null){ try{ ps=null; } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (!con.isClosed()){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        System.out.println("fin insercion directa en equivalencia");
    }

    /**
     * Busca el nit de un cliente de consorcio dado su codigo
     * @param id codigo del cliente
     * @return String con el nit
     * @throws Exception Cuando hay error
     */
    public String nitcons(String id) throws Exception{
        String nit = "";
        PreparedStatement ps = null;
        Connection con = null;
        ResultSet res = null;
        String sql = "";
        String query="NIT_CONS";
        try {
            sql = this.obtenerSQL(query);
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, id);
            res = ps.executeQuery();
            if(res.next()){
                nit = res.getString("nit");
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error en ClientesVerDAO en el metodo nitcons: "+e.toString());
        }
        finally{
            if (ps!= null){ try{ ps=null; } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (!con.isClosed()){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return nit;
    }

    /**
     * Busca el codigo del cliente dado el nit
     * @param nit Nit del cliente a buscar
     * @return String con el codigo del cliente
     * @throws Exception Cuando hay error
     */
    public String codclifinv(String nit) throws Exception{
        String id = "";
        PreparedStatement ps = null;
        Connection con = null;
        ResultSet res = null;
        String sql = "";
        String query="NIT_FINV";
        try {
            sql = this.obtenerSQL(query);
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, nit);
            res = ps.executeQuery();
            if(res.next()){
                id = res.getString("codcli");
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error en ClientesVerDAO en el metodo codclifinv: "+e.toString());
        }
        finally{
            if (ps!= null){ try{ ps=null; } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (!con.isClosed()){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return id;
    }

    public ArrayList getTiposSolicitud() throws Exception {
        PreparedStatement st = null;
        ResultSet rs = null;
        ArrayList arl = new ArrayList();
        Connection con = null;
        String query = "SQL_SEARCH_TIPOS_SOLICITUDES";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                rs = st.executeQuery();
                while (rs.next()) {
                    arl.add(new String[]{rs.getString("descripcionx"), rs.getString("codigox")});
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("error en getTiposSolicitud:"+e.toString());
            throw new SQLException("ERROR DURANTE SQL_SEARCH_TIPOS_SOLICITUDES. \n " + e.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return arl;
    }




       public boolean  Validar_modifcacion_cliente(String id_cliente) throws Exception {
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean sw  = true;
        Connection con = null;
        String query = "SQL_VALIDA_MODIFICACION_CLIENTES";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, id_cliente);
                rs = st.executeQuery();
                while (rs.next()) {
                    sw=false;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(" Validar_modifcacion_cliente():"+e.toString());
            throw new SQLException("ERROR DURANTE SQL_SEARCH_TIPOS_SOLICITUDES. \n " + e.getMessage());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return sw;
    }



    /**
     * Busca las coincidencias de un nit en la tabla de clientes
     * @param nit Dato a buscar
     * @param fec Fecha del proceso en formato AAAAMMDD
     * @return Listado de coincidencias
     * @throws Exception Cuando hay error
     */
    public ArrayList<String> srchReps(String nit,String fec) throws Exception{
        ArrayList<String> lista = null;
        PreparedStatement ps = null;
        Connection con = null;
        ResultSet res = null;
        String sql = "";
        String query="SRCH_REPS";
        try {
            lista = new  ArrayList<String>();
            sql = this.obtenerSQL(query);
            sql = sql.replaceAll("#param", fec);
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, nit);
            ps.setString(2, nit);
            res = ps.executeQuery();
            while(res.next()){
                lista.add(res.getString("codcli"));
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error en ClientesVerDAO en el metodo buscarReps: "+e.toString());
        }
        finally{
            if (ps!= null){ try{ ps=null; } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (!con.isClosed()){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return lista;
    }

    /**
     * Inserta registros directamente en la tabla de equivalencia
     * @param lista Listado con codigos de clientes de consorcio a insertar en equivalencia
     * @param stringfec fecha del proceso en formato AAAAMMDD
     * @throws Exception caundo hay error en el proceso
     */
    public void insertEquivalencia(ArrayList<String> lista,String stringfec) throws Exception{
        PreparedStatement ps = null;
        Connection con = null;
        String sql = "";
        String query="REST_EQF";
        StringStatement st = null;
        String codigo = "";
        String[] cadtemp = null;
        System.out.println("inicia insercion directa en equivalencia");
        try {
            sql = this.obtenerSQL(query);
            for(int i=0;i<lista.size();i++){
                st = new StringStatement(sql, true);
                cadtemp = (lista.get(i)).split(";_;");
                st.setString(1, cadtemp[0]);
                st.setString(2, cadtemp[1]);
                codigo = codigo + st.getSql() + "\n";
                st = null;
            }
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(codigo);
            ps.executeUpdate();
            query="TEMP_EQ";
            sql = this.obtenerSQL(query);
            sql = sql.replaceAll("#param", stringfec);
            ps = con.prepareStatement(sql);
            ps.executeUpdate();
            this.desconectar(con);
            System.out.println("backup de equivalencia(fintra) listo");
            this.restoreEq();
            System.out.println("restore de equivalencia(consorcio) listo");
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error en ClientesVerDAO en el metodo directoequiv: "+e.toString());
        }
        finally{
            if (ps!= null){ try{ ps=null; } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (!con.isClosed()){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        System.out.println("fin insercion directa en equivalencia");
    }

     public ArrayList listadoResponsables() throws NullPointerException,Exception {
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query="SQL_LISTADO_RESPONSABLES";
        ArrayList resultado = new ArrayList();

        try{
            con = this.conectarJNDI(query);
            if(con!=null){
            st = con.prepareStatement(this.obtenerSQL(query));

            rs=st.executeQuery();
            //resultado.add(new String[]{ "0", "Seleccione"});
            while(rs.next()){
                resultado.add(new String[]{rs.getString("table_code"), rs.getString("descripcion")});

            }

        }}
        catch(NullPointerException ec){
            throw new NullPointerException("No hay resultado ..." + ec.toString());
        }
        catch(Exception ec){
            throw new Exception("Error en la consulta cotizaciones de un contratista: "+ec.toString());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return resultado;
    }

     public String updateOf2(OfertaElca ofca, String usuario) throws Exception {
        String respuesta = "";
        StringStatement st = null;
        String query = "SQL_UPDATE_OFERTA_ECA2";
        String sql = "";
        try {
            String estado = this.estadoSolicitud(ofca.getId_solicitud());
            /*if(!(estado.equals("030") || estado.equals("040"))){
                ElectricaribeOfertaDAO eod=new ElectricaribeOfertaDAO();
                DateFormat dateFormat = new SimpleDateFormat("yyyyMM");
                //sql = eod.actPrecio_venta(ofca.getId_solicitud(), dateFormat.format(new java.util.Date()));
                sql = eod.actPrecio_venta(ofca.getId_solicitud(), "substring (replace (a.fecha_oferta,'-',''),1,6)");
                //2010-04-30 rhonalf
            }*/
            //st = new StringStatement (sql + this.obtenerSQL(query), true);//JJCastro fase2
            st = new StringStatement ( this.obtenerSQL(query)+sql , true);//JJCastro fase2

            st.setString(1, ofca.getDescripcion());
            //st.setString(2, ofca.getOficial());
            st.setString(2, usuario);

            st.setString(3, ofca.getEstado_cartera());//JJCASTRO
            st.setString(4, ofca.getFec_val_cartera());//JJCASTRO

            st.setString(5, ofca.getTipo_solicitud());//2010-04-30 rhonalf

	    st.setString(6, ofca.getTipo_solicitud());//20100605

            //st.setString(7, ofca.getAviso());
            //st.setString(8, ofca.getResponsable());
            st.setString(7, ofca.getId_solicitud());

            respuesta = st.getSql();
            //System.out.println(respuesta);

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("error en updateOf2:"+e.toString());
            throw e;
        }finally{
            if (st  != null){ try{ st = null;              } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }
        return respuesta;
    }

     
     
    public String updateOf3(OfertaElca ofca, String usuario) throws Exception {
        String respuesta = "";
        StringStatement st = null;
        String query = "SQL_UPDATE_OFERTA_ECA3";
        String sql = "";
        try {
            String estado = this.estadoSolicitud(ofca.getId_solicitud());
            if(!(estado.equals("030") || estado.equals("040"))){
                ElectricaribeOfertaDAO eod=new ElectricaribeOfertaDAO(this.getDatabaseName());
                DateFormat dateFormat = new SimpleDateFormat("yyyyMM");
                //sql = eod.actPrecio_venta(ofca.getId_solicitud(), dateFormat.format(new java.util.Date()));
                sql = eod.actPrecio_venta(ofca.getId_solicitud(), "substring (replace (a.fecha_oferta,'-',''),1,6)");
                //2010-04-30 rhonalf
            }
            //st = new StringStatement (sql + this.obtenerSQL(query), true);//JJCastro fase2
            st = new StringStatement ( this.obtenerSQL(query)+sql , true);//JJCastro fase2

            st.setString(1, ofca.getDescripcion());
            //st.setString(2, ofca.getOficial());
            st.setString(2, usuario);

            //st.setString(3, ofca.getEstado_cartera());//JJCASTRO
            //st.setString(4, ofca.getFec_val_cartera());//JJCASTRO

            st.setString(3, ofca.getTipo_solicitud());//2010-04-30 rhonalf

	    st.setString(4, ofca.getTipo_solicitud());//20100605

            st.setString(5, ofca.getId_solicitud());

            respuesta = st.getSql();
            System.out.println(respuesta);

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e);
            throw e;
        }finally{
            if (st  != null){ try{ st = null;              } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }
        return respuesta;
    }

/**jpinedo*/
       public Cliente MarcarCliente(String nit) throws Exception {
        String query = "SQL_MARCA_CLIENTE";
        String respuesta = "";
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        Cliente cl = new Cliente();
        try{
            con = this.conectarJNDI(query);
            if(con!=null){
            st = con.prepareStatement(this.obtenerSQL(query));
            st.setString(1, nit);
            rs=st.executeQuery();
            if(rs.next())
            {
                
                cl.setNit(rs.getString(1));
                cl.setNomcli(rs.getString(2));
                cl.setCodcli(rs.getString(3));

               
            }
           



        }}
        catch(NullPointerException ec){
            throw new NullPointerException("No hay resultado ..." + ec.toString());
        }
        catch(Exception ec){
            throw new Exception("Error en la consulta mARCAR CLIENTE: "+ec.toString());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }

        return cl;
    }





       /**jpinedo*/
       public String ValidaClienteXnit(String nit) throws Exception {
        String query = "SQL_VALIDA_CLIENTE";
        String respuesta = "NX";
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        try{
            con = this.conectarJNDI(query);
            if(con!=null){
            st = con.prepareStatement(this.obtenerSQL(query));
            st.setString(1, nit);

            rs=st.executeQuery();
            //resultado.add(new String[]{ "0", "Seleccione"});
            while(rs.next()){
                respuesta= rs.getString("cliente_eca");

            }

        }}
        catch(NullPointerException ec){
            throw new NullPointerException("No hay resultado ..." + ec.toString());
        }
        catch(Exception ec){
            throw new Exception("Error en la consulta cotizaciones de un contratista: "+ec.toString());
        }finally{
            if (rs  != null){ try{ rs.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULTSET " + e.getMessage()); }}
            if (st  != null){ try{ st.close();              } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }

        return respuesta;
    }
 public String marcarCliente(String nit) throws Exception {
        String query = "SQL_MARCA_CLIENTE_ECA";
        String respuesta = "";
        StringStatement st = null;

        try {
            st = new StringStatement(this.obtenerSQL(query), true);//JJCastro fase2
            st.setString(1, nit);
            respuesta = st.getSql();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
            throw new SQLException("ERROR DURANTE SQL_MARCA_CLIENTE \n " + e.getMessage());
        } finally {
            if (st != null) {
                try {
                    st = null;
                } catch (Exception e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
        }

        return respuesta;
}

    public String insertarOf(OfertaElca ofca) throws Exception {
        String respuesta = "";
        StringStatement st = null;
        String query = "SQL_INSERT_OFERTA_ECA";
        try {
            st = new StringStatement (this.obtenerSQL(query), true);//JJCastro fase2

            st.setString(1, ofca.getId_solicitud());
            st.setString(2, ofca.getId_cliente());
            st.setString(3, ofca.getDescripcion());
            st.setString(4, ofca.getCreation_user());
            st.setString(5, ofca.getNic());
            st.setString(6, ofca.getTipo_solicitud());
            st.setString(7, ofca.getTipoDtf());
            st.setString(8, ofca.getTipo_solicitud());
            st.setString(9, ofca.getOficial());
            st.setString(10, ofca.getAviso());

            st.setString(11, ofca.getEstado_cartera());//JJCASTRO
            st.setString(12, ofca.getFec_val_cartera());//JJCASTRO
            st.setString(13, ofca.getResponsable());//RESPONSABLE OPAV JJCASTRO

            respuesta = st.getSql();
            System.out.println(respuesta);

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e);
            throw e;
        }finally{
            if (st  != null){ try{ st = null;              } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }
        return respuesta;
    }

     public String insertarAcc(String fecha_vis_plan,AccionesEca acceca ) throws Exception {
        String respuesta = "";
        StringStatement st = null;
        String query = "SQL_INSERT_ACCION_AAAE";
        try {
           st = new StringStatement (this.obtenerSQL(query), true);//JJCastro fase2

            st.setString(1, acceca.getId_accion());
            st.setString(2, acceca.getId_solicitud());
            st.setString(3, acceca.getContratista());
            st.setString(4, acceca.getDescripcion());
            st.setString(5, acceca.getCreation_user());
            st.setString(6, fecha_vis_plan);
            st.setString(7, acceca.getAlcance());
            st.setString(8, acceca.getTipo_trabajo());
            respuesta = st.getSql();

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e);
            throw e;
        }finally{
            if (st  != null){ try{ st = null;              } catch(Exception e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
        }
        return respuesta;
    }

public String insertarCliente(Cliente cl, String usuario) throws Exception {
        String respuesta = "";
        PreparedStatement st = null;
        String query = "SQL_INSERT_CLIENTE_ECA";
        try {
            st = crearPreparedStatement(query);

            st.setString(1, cl.getCodcli());
            st.setString(2, cl.getNomcli());
            st.setString(3, cl.getNit());
            st.setString(4, cl.getTipo());
            st.setString(5, cl.getCiudad());
            st.setString(6, cl.getDireccion());
            st.setString(7, cl.getNomContacto());
            st.setString(8, cl.getTelContacto());
            st.setString(9, cl.getCargoContacto());
            st.setString(10, cl.getNomRepresentante());
            st.setString(11, cl.getTelRepresentante());
            st.setString(12, cl.getCelRepresentante());
            st.setString(13, usuario);
            st.setString(14, usuario);
            st.setString(15, cl.getIdEjecutivo());
            st.setString(17, cl.getSector());
            if (cl.isOficial()) {
                st.setString(16, "S");
            } else {
                st.setString(16, "N");
            }
            st.setString(18, cl.getEdif());
            st.setString(19, "");
            respuesta = st.toString();
            System.out.println(respuesta);

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e);
            throw e;
        } finally {
            st.close();
            this.desconectar(query);
        }
        return respuesta;
    }

public boolean existeNic(String nic) throws Exception {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_EXISTE_NIC";
        String sql = "";

        try {
            sql = this.obtenerSQL(query);
            con = conectarJNDI(query);
            ps = con.prepareStatement(sql);
            ps.setString(1, nic);
            rs = ps.executeQuery();
            if (rs.next()) {

                return true;
            }
        } catch (Exception e) {
            System.out.println("Error en busqueda de existeNic: " + e.toString());
            e.printStackTrace();
            throw new Exception("Error en busqueda de existeNic: " + e.toString());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando el statement: " + e.toString());
                }
            }
            if (!con.isClosed()) {
                try {
                    con.close();
                } catch (Exception e) {
                    throw new Exception("Error cerrando la conexion: " + e.toString());
                }
            }
        }
        return false;
    }

    public String getDistribucion(String tipo_solicitud) {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SELECT dato FROM tablagen WHERE table_type='TIPO_DISTR' AND table_code=? AND reg_status!='A'";
        String sql = "1";

        try {
            //sql = this.obtenerSQL(query);
            con = conectarBDJNDI("fintra");
            ps = con.prepareStatement(query);
            ps.setString(1, tipo_solicitud);
            rs = ps.executeQuery();
            if (rs.next()) {
                sql = rs.getString("dato");
            }
        } catch (Exception e) {
            System.out.println("Error en busqueda de existeNic: " + e.toString());
            e.printStackTrace();
        } finally {
            try {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (Exception e) {}
                }
                if (!con.isClosed()) {
                    try {
                        con.close();
                    } catch (Exception e) {}
                }
            } catch (SQLException ex) {
                
            } return sql;
        }
    }

}
