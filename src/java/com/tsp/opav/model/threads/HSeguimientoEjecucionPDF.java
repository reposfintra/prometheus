package com.tsp.opav.model.threads;

//PDF
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import com.tsp.opav.model.*;
import com.tsp.opav.model.beans.Actividades;
import com.tsp.opav.model.beans.NegocioApplus;
import com.tsp.opav.model.beans.OfertaSeguimiento;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.io.File;
import java.io.FileOutputStream;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.ResourceBundle;
import org.jfree.chart.JFreeChart;

/**
 * HSeguimientoEjecucionPDF.java<br/>
 * Hilo para generar el PDF con los seguimientos para una solicitud<br/>
 * 25/06/2010
 * @author darrieta-GEOTECH
 * @version 1.0
 */
public class HSeguimientoEjecucionPDF  extends Thread{

    private String usuario;
    private ModelOpav model;
    private String processName = "Reporte Seguimientos a la ejecucion";
    private String ruta;
    private String idSolicitud;
    private JFreeChart chart;
    private int chartWidth;
    private String tipo = "";
    
    public void start(String tipo, String usuario, ModelOpav model, String idSolicitud, JFreeChart chart, int chartWidth){
        this.tipo=tipo;
        this.usuario = usuario;
        this.model = model;
        this.idSolicitud = idSolicitud;
        this.chart = chart;
        this.chartWidth = chartWidth;
        super.start();
    }
    
    @Override
    public void run(){
        try{
            //model.LogProcesosSvc.InsertProceso(this.processName, this.hashCode(), "Reporte Seguimientos a la ejecucion", usuario);

            this.generarPDF();

            //model.LogProcesosSvc.finallyProceso(this.processName, this.hashCode(), usuario, "PROCESO EXITOSO");
        }catch (Exception ex){
            /*try{
                model.LogProcesosSvc.finallyProceso(this.processName, this.hashCode(), usuario, "Finalizado con error ...\n" + ex.getMessage());
            }catch (Exception e){
                e.printStackTrace();
            }*/
            ex.printStackTrace();
        }
    }

    

    private void generarPDF() throws Exception{
        //obtener cabecera de ruta
        try {
            //armar la ruta
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String path = rb.getString("ruta");
            ruta = path + "/exportar/migracion/" + usuario + "/";
            File dir = new File(ruta);
            dir.mkdir();

            
            NegocioApplus infoSolicitud = model.seguimientoEjecucionService.obtenerInfoSolicitud(idSolicitud);
            ArrayList<Actividades> consulta= model.seguimientoEjecucionService.consultarActividadesSolicitud(idSolicitud);
            ArrayList<Actividades> seguimientos = model.seguimientoEjecucionService.consultarSeguimientosSolicitud(idSolicitud, "-1");
            ArrayList<OfertaSeguimiento> totales = model.seguimientoEjecucionService.consultarTotalesSeguimientos(idSolicitud);

            //generar el pdf
            int anchoAccion = 180;
            int anchoActividad = 180;
            int anchoResponsable = 110;
            int anchoPeso = 70;
            int anchoAvances = 70;
            int ancho = (anchoAccion+anchoActividad+anchoResponsable+anchoPeso+anchoAvances) + (totales.size()*anchoAvances) + 60;
            ancho = ancho>chartWidth ? ancho : chartWidth;
            Rectangle pageSize = new Rectangle(ancho, 800);
            Document document=new Document(pageSize, 30, 30, 25, 25);

            PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(ruta+"seguimientoEjecucion.pdf"));
            document.open();
            document.addAuthor("Consorcio multiservicios");
            
            int numCols;
            PdfPTable table;
            PdfPCell cell;
            int[] widths;
            
            if(!this.tipo.equalsIgnoreCase("Cronograma")) {
            document.addTitle("Reporte seguimientos ejecuci�n");

            
            //Crear la tabla de seguimientos
            numCols = 5 + totales.size();
            table = new PdfPTable(numCols);
            table.setHeaderRows(2);
            table.setWidthPercentage(100);
            table.setKeepTogether(true);
            table.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);

            //Vector de tamanos de las columnas
            widths = new int[numCols];
            int col = 0;
            widths[col++] = anchoAccion;
            widths[col++] = anchoActividad;
            widths[col++] = anchoResponsable;
            widths[col++] = anchoPeso;
            widths[col++] = anchoAvances;

            //filas encabezado
            cell = new PdfPCell(new Phrase("Acci�n"));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(cell);
            cell = new PdfPCell(new Phrase("Actividad"));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(cell);
            cell = new PdfPCell(new Phrase("Responsable"));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(cell);
            cell = new PdfPCell(new Phrase("Peso obra"));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(cell);
            cell = new PdfPCell(new Phrase("Proyectado / Ejecutado"));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(cell);
            for (int i = 0; i < totales.size(); i++) {
                widths[col++] = anchoAvances;
                cell = new PdfPCell(new Phrase(seguimientos.get(i).getFecha()));
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(cell);
            }
            table.setWidths(widths);
            table.addCell("");
            table.addCell("");
            table.addCell("");
            table.addCell("");
            table.addCell("P");
            for (int i = 0; i < totales.size(); i++) {
                cell = new PdfPCell(new Phrase(String.valueOf(totales.get(i).getAvance_esperado())));
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(cell);
            }

            //fila totales
            table.addCell("");
            table.addCell("");
            table.addCell("");
            table.addCell("");
            table.addCell("E");
            for (int i = 0; i < totales.size(); i++) {
                cell = new PdfPCell(new Phrase(String.valueOf(totales.get(i).getAvance_registrado())));
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(cell);
            }

            //filas de avances
            for (int j = 0; j < consulta.size(); j++) {

                Actividades act = consulta.get(j);
                table.addCell(act.getDescAccion());
                table.addCell(act.getDescripcion());
                table.addCell(act.getResponsable());
                table.addCell(String.valueOf(act.getPeso_obra()));
                table.addCell("P");
                for (int i = 0; i < totales.size(); i++) {
                    String esperado = String.valueOf( seguimientos.get((j*totales.size())+i).getAvance_esperado() );
                    cell = new PdfPCell(new Phrase(esperado));
                    cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    table.addCell(cell);
                }
                table.addCell("");
                table.addCell("");
                table.addCell("");
                table.addCell("");
                table.addCell("E");
                for (int i = 0; i < totales.size(); i++) {
                    String registrado = String.valueOf( seguimientos.get((j*totales.size())+i).getAvance() );
                    cell = new PdfPCell(new Phrase(registrado));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                    table.addCell(cell);
                }
            }
            
            document.add(table);
            document.newPage();
            } else {
                document.addTitle("Cronograma");
                Phrase nombre = new Phrase("\nMULTISERVICIOS\n");
                nombre.getFont().setStyle(Font.BOLD);
                document.add(nombre);

                Phrase header = new Phrase();
                header.add("Id Solicitud: "+idSolicitud+". ");
                header.add("Consecutivo: "+infoSolicitud.getConsecutivo_oferta()+"\n");
                header.add("Cliente: "+infoSolicitud.getNombreCliente()+"\n");
                document.add(header);
            }
            
            //Insertar cronograma. JPACOSTA
            Calendar fechaI = null;
            if (infoSolicitud.getFecha() != "" && !infoSolicitud.getFecha().startsWith("0099-01-01")) {
                DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                fechaI = new GregorianCalendar(df.getTimeZone());
                document.add(new Paragraph("Fecha Inicio: "+infoSolicitud.getFecha()));
            }
            numCols = 0; int a, b; boolean dias = true;
            
            for (int i = 0; i < consulta.size(); i++) {
               try {
                   a = Integer.parseInt(((Actividades) consulta.get(i)).getFechaFinal());
               } catch (NumberFormatException nFe) {
                   a = 0;
                }
               numCols = (numCols < a) ? a : numCols;
           }

           if (numCols > 45) {
               numCols = (Integer) (numCols / 7) + 1;
               dias = false;
           }
           document.setPageSize(PageSize.LETTER.rotate());
           //document.newPage();
           document.add(new Paragraph(" "));
           document.add(new Paragraph("CRONOGRAMA DE ACTIVIDADES EN "
                        +((dias)?"DIAS":"SEMANAS")+"."));
           document.add(new Paragraph(" "));
           table = new PdfPTable(numCols + 1);
           table.setWidthPercentage(100);
           table.setKeepTogether(true);
           table.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
           
           widths = new int[numCols + 1];
           widths[0] = 30;
           
           //celdas informacion 
           for (int j = 0; j < consulta.size(); j++) {
               Actividades act = consulta.get(j);
               if (j > 0 && act.getDescAccion().equals(consulta.get(j - 1).getDescAccion())) {
                   //table.addCell(act.getDescAccion());                    
               } else {
                   //filas encabezado
                    cell = new PdfPCell(new Phrase("Acci�n"));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    table.addCell(cell);
                    cell = new PdfPCell(new Phrase(act.getDescAccion()));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setColspan(numCols);
                    table.addCell(cell);
                    
                    cell = new PdfPCell(new Phrase("Actividad"));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    table.addCell(cell);

                    //titulos fechas
                    for (int i = 0; i < numCols; i++) {
                        cell = new PdfPCell(new Phrase("" + (i + 1)));
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        table.addCell(cell);
                        widths[i + 1] = 8;
                    }
               }
               table.addCell(new Phrase(act.getDescripcion()));
               a = Integer.parseInt(((Actividades) consulta.get(j)).getFechaInicial());
               b = Integer.parseInt(((Actividades) consulta.get(j)).getFechaFinal());
               if (!dias) {
                   a /=7; b/=7;
               }
               for (int k = 0; k < numCols; k++) {
                   cell = new PdfPCell(new Phrase(""));
                   if (k >= (a - 1) && k < b) {
                       //BaseColor myColor = WebColors.getRGBColor("#A00000");//BaseColor.GREEN
                       cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
                   }   table.addCell(cell);
               }
           }
            
            table.setWidths(widths);
            
            document.add(table);

            if(chart != null) { 
                document.newPage();
                document.setPageSize(PageSize.LETTER.rotate());
                
                //Insertar el grafico
                int width = 700;
                int height = (int)document.getPageSize().getHeight() - 100;
                PdfContentByte contentByte = writer.getDirectContent();
                PdfTemplate temp = contentByte.createTemplate(width, height);
                Graphics2D g2d = temp.createGraphics(width, height, new DefaultFontMapper());
                Rectangle2D r2d = new Rectangle2D.Double(0, 0, width, height);
                chart.draw(g2d, r2d);
                g2d.dispose();
                contentByte.addTemplate(temp, 50, 50);
            }
            if(!this.tipo.equalsIgnoreCase("Cronograma")) {
            //mostrarObservaciones(document);
            document.newPage();
            document.setPageSize(PageSize.LETTER.rotate());
            Paragraph paragraphObs = new Paragraph();
            Phrase titulo = new Phrase("\nOBSERVACIONES\n\n");
            titulo.getFont().setStyle(Font.BOLD);
            paragraphObs.add(titulo);
            
            Actividades seg;
            Phrase obs;
            
            for (int j = 0; j < consulta.size(); j++) {
                obs = new Phrase();
                obs.add(consulta.get(j).getDescripcion()+"\n");
                
                for (int i = 0; i < totales.size(); i++) {
                    seg = seguimientos.get((j*totales.size())+i);

                    obs.add(seg.getFecha()+": ");
                    obs.add(seg.getObservaciones()+"\n");
                }
                obs.add("\n");
                paragraphObs.add(obs);
            }
            document.add(paragraphObs);
            
            Phrase nombre = new Phrase("\nMULTISERVICIOS\n");
            nombre.getFont().setStyle(Font.BOLD);
            document.add(nombre);

            Phrase header = new Phrase();
            header.add("Id Solicitud: "+idSolicitud+"\n");
            header.add("Cliente: "+infoSolicitud.getNombreCliente()+"\n");
            header.add("Orden de trabajo: "+infoSolicitud.getNumOs()+"\n");
            document.add(header);
            } else {
                document.add(new Paragraph(" "));
                document.add(new Paragraph("Nota: El d�a uno del cronograma de actividades adjunto ser� igual al d�a en que se firme el acta de inicio de obras "));
            }

            document.close();


        } catch (Exception e) {
            throw e;
        }
    }

    public void mostrarObservaciones(Document document) throws SQLException, DocumentException{

        ArrayList<OfertaSeguimiento> observaciones = model.seguimientoEjecucionService.consultarObservacionesSolicitud(idSolicitud,"");

        document.newPage();
        Paragraph paragraphObs = new Paragraph();
        Phrase titulo = new Phrase("\nOBSERVACIONES\n\n");
        titulo.getFont().setStyle(Font.BOLD);
        paragraphObs.add(titulo);

        
        for (int idx = 0; idx < observaciones.size(); idx++) {
            OfertaSeguimiento os = observaciones.get(idx);
            Phrase obs = new Phrase();
            obs.add(os.getUsuario()+" "+os.getFecha()+"\n");
            obs.add(os.getObservaciones()+"\n\n");
            paragraphObs.add(obs);
        }

        document.add(paragraphObs);
    }

}