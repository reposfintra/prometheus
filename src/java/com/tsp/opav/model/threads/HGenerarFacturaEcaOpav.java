/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


package com.tsp.opav.model.threads;


import com.tsp.finanzas.contab.model.Model;
import com.tsp.opav.model.ModelOpav;
import com.tsp.opav.model.beans.*;
import com.tsp.util.Util;
import com.tsp.opav.model.services.ConstanteService;
import com.tsp.operation.model.beans.SerieGeneral;
import com.tsp.operation.model.beans.Usuario;

import java.util.*;



import com.tsp.util.LogWriter;


/**
 *
 * @author Alvaro
 */
public class HGenerarFacturaEcaOpav extends Thread {


    private ConstanteService constanteService;

    private ModelOpav model;
    private Model modelcontab;
    private Usuario usuario;


    /** Creates a new instance of HPrefacturaDetalle */
    public HGenerarFacturaEcaOpav () {
    }

    public void start(ModelOpav model, Usuario usuario){

        this.usuario = usuario;
        this.model = model;
        this.modelcontab= new Model(usuario.getBd());
        this.constanteService = new ConstanteService(usuario.getBd());
        
        super.start();
    }

// 2010-06-14



    /*
     * Modificaciones:
     * 2010-06-14 Cambia el uso de .size por .empty
     * 2010-06-14 Adiciona Override para el uso del run
     */
    @Override  // 2010-06-14
    public synchronized void run(){
        try{
            //model.LogProcesosSvc.InsertProceso(this.processName, this.hashCode(), "Generacion del reporte de prefactura : " + prefactura, usuario.getLogin());


            // Definicion de CONSTANTES
            String  EJECUTAR_TRASLADO_CXC             = constanteService.getValor("FINV", "EJECUTAR_TRASLADO_CXC", "");
            String  EJECUTAR_CREACION_NC_CXC          = constanteService.getValor("FINV", "EJECUTAR_CREACION_NC_CXC", "");
            String  EJECUTAR_CREACION_CXC             = constanteService.getValor("FINV", "EJECUTAR_CREACION_CXC", "");
            String  EJECUTAR_CREACION_PROVINTEGRAL    = constanteService.getValor("FINV", "EJECUTAR_CREACION_PROVINTEGRAL", "");
            String  EJECUTAR_CREACION_NC_PROVINTEGRAL = constanteService.getValor("FINV", "EJECUTAR_CREACION_NC PROVINTEGRAL", "");
            String  EJECUTAR_CREACION_FAC_PROVINTEGRAL = constanteService.getValor("FINV", "EJECUTAR_CREACION_FAC_PROVINTEGRAL", "");
            String  EJECUTAR_TRASLADO_FAC_PROVINTEGRAL = constanteService.getValor("FINV", "EJECUTAR_TRASLADO_FAC_PROVINTEGRAL", "");


            // -------------------------------------------------------------------------
            // DEFINICION DEL LOG DE ERROR

            LogWriter logWriter = new LogWriter("com/tsp/util/connectionpool/db", usuario.getLogin(), "/Facturas de clientes ", usuario.getLogin() );

            // FIN DEFINICION DEL LOG DE ERROR
            // -------------------------------------------------------------------------


            logWriter.tituloInicial("GENERACION DE FACTURAS CLIENTE");

            try {


                Vector comandos_sql =new Vector();
                String comandoSQL = "";
                String comandoSQLAccion ="";
                List listaSubclientePorFacturar = null;
                String estado = "";
                java.util.Date fechaActual = new Date();
                String creation_date = fechaActual.toString();


                if (!EJECUTAR_CREACION_CXC.equalsIgnoreCase("NO")) {

                    // Elabora una lista de solicitudes que se van a facturar
                    model.consorcioService.buscaSolicitudParcialPorFacturar();
                    logWriter.log("LISTA DE SOLICITUDES PARCIALES POR FACTURAR CREADA \n", LogWriter.INFO);
                    
                    List listaSolicitudParcialPorFacturar = model.consorcioService.getSolicitudParcialPorFacturar();
                    
                    if (!listaSolicitudParcialPorFacturar.isEmpty()) {  // 2010-06-14

                        SolicitudParcialPorFacturar solicitudParcialPorFacturar = new SolicitudParcialPorFacturar();
                        SubclientePorFacturar subclientePorFacturar = new SubclientePorFacturar();
                        FacturaConsorcio facturaConsorcio = new FacturaConsorcio();
                        SerieGeneral serie = new SerieGeneral();
                        TotalesConsorcio totalesConsorcio = new TotalesConsorcio();
                        Vector listaCabecera = new Vector();
                        Vector listaComandoSQL = new Vector();
                        String existeFacturaConsorcio = "";
                        
                        Iterator it = listaSolicitudParcialPorFacturar.iterator();
                        while (it.hasNext()) {

                            // Control si existe un error en la solicitud a efectos de cancelar toda la facturacion de la solicitud
                            String cancelaFacturaSolicitud = "NO";

                            // Una solicitud parcial para facturar
                            solicitudParcialPorFacturar = (SolicitudParcialPorFacturar) it.next();
                            listaComandoSQL.clear();

                            // Elabora una lista de subclientes a facturar por cada solicitud parcial
                            String id_solicitud = solicitudParcialPorFacturar.getId_solicitud();
                            int parcial = solicitudParcialPorFacturar.getParcial();
                            
                            logWriter.log("INICIA PROCESO PARA SOLICITUD : " + id_solicitud + " - " + parcial + "\n", LogWriter.INFO);

                            // Consigue el total de acciones para esa solicitud
                            TotalAcciones totalAcciones = model.consorcioService.buscaTotalAcciones(id_solicitud, parcial);
                        if (totalAcciones != null) {
                            totalesConsorcio.setInicializar(totalAcciones.getBonificacion(), "bonificacion", "TOTAL");
                            
                            model.consorcioService.buscaSubclientePorFacturar(id_solicitud, parcial);
                            listaSubclientePorFacturar = model.consorcioService.getSubclientePorFacturar();
                            int totalSubclientes = listaSubclientePorFacturar.size();
                            int numeroSubcliente = 0;
                            listaCabecera.clear();
                            
                            String numeroFacturaCreada = "";
                            
                            Iterator it0 = listaSubclientePorFacturar.iterator();
                            while ((it0.hasNext())
                                    & (cancelaFacturaSolicitud.equalsIgnoreCase("NO"))) {

                              // UN SUBCLIENTE A FACTURAR
                                subclientePorFacturar = (SubclientePorFacturar) it0.next();

                                // Proporciona el valor de la bonificacion total de la solicitud entre los diferentes subclientes
                                double valor = Util.redondear2(subclientePorFacturar.getPorcentaje_base() * totalAcciones.getBonificacion() / 100, 0);
                                subclientePorFacturar.setBonificacion(totalesConsorcio.getValor(valor, "bonificacion", totalSubclientes, ++numeroSubcliente));
                                
                                List listaItem = new LinkedList();
                                ItemFacturaCliente item = new ItemFacturaCliente();

                              // logWriter.log("   Inicia factura para subcliente : " + subclientePorFacturar.getId_cliente() + "\n",LogWriter.INFO);
                                // Localiza el numero de factura con la cual se facturo inicialmente y se envio a Corficolombiana
                                existeFacturaConsorcio = "";
                                numeroFacturaCreada = model.consorcioService.buscaNumeroFactura(subclientePorFacturar.getId_solicitud(),
                                        subclientePorFacturar.getParcial(),
                                        subclientePorFacturar.getId_cliente());
                                
                                if (numeroFacturaCreada.equalsIgnoreCase("")) {
                                    existeFacturaConsorcio = "NO";
                                } else {
                                    existeFacturaConsorcio = "SI";
                                }

                              // Consigue un numero de factura de la serie del Consorcio
                                // serie = model.serieGeneralService.getSerie("FINV","OP","FMSOP");
                                serie = modelcontab.serieGeneralService.getSerie("FINV", "OP", "FACCF");
                                modelcontab.serieGeneralService.setSerie("FINV", "OP", "FACCF");
                                String documentoConsorcio = serie.getUltimo_prefijo_numero();
                                //validamos el tipo de documento a generar
                                if (subclientePorFacturar.getTipo_distribucion().trim().equalsIgnoreCase("43") && subclientePorFacturar.getNum_os().subSequence(0, 4).equals("ACON")) {
                                    documentoConsorcio = documentoConsorcio.replace("NM", "PM");
                                }
                                
                                comandoSQL = model.consorcioService.setSolicitudSubcliente(subclientePorFacturar, documentoConsorcio);
                                comandos_sql.add(comandoSQL);
                                 // Controla totales de la factura para evitar perdida por redondeo. Inicializa los valores totales y las variables que acumulan
                                totalesConsorcio.setInicializar(subclientePorFacturar.getVal_a_financiar(), "valor_a_financiar", "TOTAL");
                                totalesConsorcio.setInicializar(subclientePorFacturar.getVal_con_financiacion(), "valor_con_financiacion", "TOTAL");
                                
                                listaItem = (List) model.consorcioService.generaItem(subclientePorFacturar, documentoConsorcio, totalesConsorcio);
                                
                                String num_os = subclientePorFacturar.getNum_os();
                                String simbolo_variable = subclientePorFacturar.getSimbolo_variable();
                                String solicitud = subclientePorFacturar.getId_solicitud() + "-" + subclientePorFacturar.getParcial();
                                
                                String descripcion_factura = "Factura Eca   NUMERO OS: " + num_os + "   SOLICITUD: " + solicitud
                                        + "   SIMBOLO VARIABLE: " + simbolo_variable;

                              // Genera registros de facturas de la fiducia.
                                // Para cada item de la lista de items se debe generar un numero de cuotas segun el subcliente
                                Iterator it1 = listaItem.iterator();
                                while ((it1.hasNext())
                                        & (cancelaFacturaSolicitud.equalsIgnoreCase("NO"))) {
                                    
                                    item = (ItemFacturaCliente) it1.next();

                                    // Inicializa los totales para cada uno de los items, para evitar problemas de redondeo
                                    totalesConsorcio.setInicializar(item.getCosto_contratista(), "item_contratista", "TOTAL");
                                    totalesConsorcio.setInicializar(item.getComision_opav(), "item_comision_opav", "TOTAL");
                                    totalesConsorcio.setInicializar(item.getComision_fintra(), "item_comision_fintra", "TOTAL");
                                    totalesConsorcio.setInicializar(item.getComision_interventoria(), "item_comision_interventoria", "TOTAL");
                                    totalesConsorcio.setInicializar(item.getComision_provintegral(), "item_comision_provintegral", "TOTAL");
                                    totalesConsorcio.setInicializar(item.getComision_eca(), "item_comision_eca", "TOTAL");
                                    totalesConsorcio.setInicializar(item.getValor_extemporaneo_1(), "item_extemporaneo_1", "TOTAL");
                                    totalesConsorcio.setInicializar(item.getCuota_inicial(), "item_cuota_inicial", "TOTAL");
                                    totalesConsorcio.setInicializar(item.getIva_contratista(), "item_iva_contratista", "TOTAL");
                                    totalesConsorcio.setInicializar(item.getIva_bonificacion(), "item_iva_bonificacion", "TOTAL");
                                    totalesConsorcio.setInicializar(item.getIva_comision_opav(), "item_iva_opav", "TOTAL");
                                    totalesConsorcio.setInicializar(item.getIva_comision_fintra(), "item_iva_fintra", "TOTAL");
                                    totalesConsorcio.setInicializar(item.getIva_comision_interventoria(), "item_iva_interventoria", "TOTAL");
                                    totalesConsorcio.setInicializar(item.getIva_comision_provintegral(), "item_iva_provintegral", "TOTAL");
                                    totalesConsorcio.setInicializar(item.getIva_comision_eca(), "item_iva_eca", "TOTAL");
                                    totalesConsorcio.setInicializar(item.getValor_extemporaneo_2(), "item_extemporaneo_2", "TOTAL");
                                    totalesConsorcio.setInicializar(item.getIntereses(), "item_intereses", "TOTAL");
                                    totalesConsorcio.setInicializar(item.getBonificacion(), "item_bonificacion", "TOTAL");
                                    
                                    for (int numeroCuota = 1; numeroCuota <= subclientePorFacturar.getPeriodo(); numeroCuota++) {

                                      // logWriter.log("    Inicia creacion factura : " + documento + "\n",LogWriter.INFO);
                                        // Arma el numero de factura con la serie del consorcio y un consecutivo por cuota
                                        String documento = documentoConsorcio + "_" + Integer.toString(numeroCuota);
                                        // Arma el numero de factura con el numero de factura ya creado inicialmente y que fue notificado a Corficolombiana
                                        String documentoInicial = "";
                                        if (existeFacturaConsorcio.equalsIgnoreCase("SI")) {
                                            documentoInicial = numeroFacturaCreada + "_" + Integer.toString(numeroCuota);
                                        }
                                        
                                        String error = model.consorcioService.creaFacturaFiducia(subclientePorFacturar,
                                                facturaConsorcio, documento,
                                                documentoInicial,
                                                numeroCuota, totalesConsorcio,
                                                item, listaComandoSQL,
                                                creation_date, usuario.getLogin(),
                                                descripcion_factura, listaCabecera);
                                        if (error.equalsIgnoreCase("SI")) {
                                            cancelaFacturaSolicitud = "SI";
                                            logWriter.log("ERROR: Se genero error al crear factura : " + documento + "\n", LogWriter.INFO);
                                            break;
                                        }
                                        
                                    } // Final del for que genera tantas facturas como cuotas haya

                                } // Final del while de la lista de items
                                
                                // logWriter.log("    Actualizacion de subclientes con la factura agregada al batch de SQL \n",LogWriter.INFO);
                                comandoSQLAccion = model.consorcioService.setActulizarAccion(id_solicitud);
                                comandos_sql.add(comandoSQLAccion);   

                            } // Final del while que recorre la lista de subclientes de una solicitud a facturar

                          // Generar las cabeceras de todas las facturas y el registro en factura consorcio
                            if (cancelaFacturaSolicitud.equalsIgnoreCase("NO")) {
                                cancelaFacturaSolicitud = model.consorcioService.generaCabecera(subclientePorFacturar, listaCabecera, listaComandoSQL,
                                        creation_date, usuario.getLogin(),
                                        logWriter);
                            }
                            
                            if (cancelaFacturaSolicitud.equalsIgnoreCase("NO")) {

                                // Agregando la lista de facturas (detalles y cabeceras al batch
                                comandos_sql.addAll(listaComandoSQL);

                                estado = model.consorcioService.ejecutarBatchSQL(comandos_sql, logWriter);
                                
                                if (estado.equalsIgnoreCase("ERROR")) {
                                    logWriter.log("ERROR: Se genero error al ejecutar el Batch de SQL. Se efectuo rollback. No se proceso solicitud: " + id_solicitud + " \n", LogWriter.INFO);
                                } else {
                                    // logWriter.log("    Registra en base de datos sin error \n",LogWriter.INFO);
                                }
                            } else {
                                logWriter.log("ERROR: Se genero error al ejecutar el procedimiento consorcioService.generaCabecera. No se proceso solicitud: " + id_solicitud + " \n", LogWriter.INFO);
                            }
                            
                            model.consorcioService.generaAjusteFactura(numeroFacturaCreada, listaCabecera, logWriter);
                            
                            model.consorcioService.ajustesProyectosEspeciales(id_solicitud ,logWriter);

                            // Inicializa los vectores de comandos para una proxima solicitud
                            comandos_sql.clear();
                            listaComandoSQL.clear();
                            
                            logWriter.log("FINAL  PROCESO PARA SOLICITUD : " + id_solicitud + " - " + parcial + "\n", LogWriter.INFO);
                        } 
                     } // Final del while que recorre la lista de solicitudes a facturar

                    } // Final de la lista de solicitudes
                    else {
                        logWriter.log("ADVERTENCIA: No existen solicitudes por facturar. \n", LogWriter.INFO);
                    }
                } else {
                    logWriter.log("ADVERTENCIA: No se crearon facturas en Consorcio porque la constante EJECUTAR_CREACION_CXC esta configurada en NO. \n", LogWriter.INFO);
                }

                // ELABORACION DE UNA NOTA CREDITO QUE CANCELA LA FACTURA DEL CONSORCIO A LOS CLIENTES �RA PASARLA A FINTRA.
                if (!EJECUTAR_CREACION_NC_CXC.equalsIgnoreCase("NO")) {
                    model.consorcioService.generaNcTraslado(usuario.getLogin(), model, logWriter);
                } else {
                    logWriter.log("ADVERTENCIA: No se efectuaron traslados porque la constante EJECUTAR_CREACION_NC_CXC esta configurada en NO. \n", LogWriter.INFO);
                }

                //Metodo modificado por ing.edgar gonzalez 
                // ELABORACION DE UNA FACTURA EN FINTRA CORRESPONDIENTE A UNA FACTURA DE SELECTRIK
                if (!EJECUTAR_TRASLADO_CXC.equalsIgnoreCase("NO")) {
                    model.consorcioService.generarCxcFintra(usuario);
                } else {
                    logWriter.log("ADVERTENCIA: No se efectuaron traslados porque la constante EJECUTAR_TRASLADO_CXC esta configurada en NO. \n", LogWriter.INFO);
                }


//              se comenta este bloque de codigo 
//                // ELABORACION DE UNA FACTURA INTERNA DE PROVINTEGRAL
//                if(!EJECUTAR_CREACION_PROVINTEGRAL.equalsIgnoreCase("NO")) {
//                  model.consorcioService.generarFacturaInternaProvintegral(usuario.getLogin(), model, logWriter);
//                }
//                else {
//                    logWriter.log("ADVERTENCIA: No se efectuaron traslados porque la constante EJECUTAR_TRASLADO_CXC esta configurada en NO. \n",LogWriter.INFO);
//                }
//
//
//
//
//                // ELABORACION DE UNA NOTA CREDITO QUE CANCELA LA FACTURA INTERNA DE PROVINTEGRAL
//                if(!EJECUTAR_CREACION_NC_PROVINTEGRAL.equalsIgnoreCase("NO")) {
//                  model.consorcioService.generaNcInternaProvintegral(usuario.getLogin(), model,logWriter);
//                }
//                else {
//                    logWriter.log("ADVERTENCIA: No se efectuaron traslados porque la constante EJECUTAR_CREACION_NC_PROVINTEGRAL esta configurada en NO. \n",LogWriter.INFO);
//                }
//
//
//
//                // ELABORACION DE LA FACTURA CONFORMADA DE PROVINTEGRAL
//                if(!EJECUTAR_CREACION_FAC_PROVINTEGRAL.equalsIgnoreCase("NO")) {
//                  model.consorcioService.generaCxpProvintegral(usuario.getLogin(),  model,  logWriter);
//                }
//                else {
//                    logWriter.log("ADVERTENCIA: No se efectuaron facturas conformadas de Provintegral porque la constante EJECUTAR_CREACION_FAC_PROVINTEGRAL esta configurada en NO. \n",LogWriter.INFO);
//                }


                logWriter.tituloFinal();

            }
            catch (Exception e) {

                logWriter.log("ERROR: Se presento el siguiente error:  \n"  ,LogWriter.INFO);
                logWriter.log(e.getMessage());

            }

            //model.LogProcesosSvc.finallyProceso(this.processName, this.hashCode(), usuario.getLogin(), "PROCESO EXITOSO");
        }catch (Exception ex){
            ex.printStackTrace();

            try{
                //model.LogProcesosSvc.finallyProceso(this.processName, this.hashCode(), usuario.getLogin(), "Finalizado con error ...\n" + ex.getMessage());
            }catch (Exception e){
                System.out.println("Error HGenerarFacturaEca ...\n"  + e.getMessage());
            }
        }

    }




}