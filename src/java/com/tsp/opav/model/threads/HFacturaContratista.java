/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.opav.model.threads;


import com.tsp.opav.model.ModelOpav;
import com.tsp.opav.model.beans.*;

import com.tsp.opav.model.services.ConstanteService;
import com.tsp.operation.model.beans.Usuario;

import com.tsp.util.LogWriter;




/**
 *
 * @author Alvaro
 */
public class HFacturaContratista extends Thread{

    private ConstanteService constanteService;

    private ModelOpav model;
    private Usuario usuario;
    private String dstrct;


    /** Creates a new instance of HPrefacturaDetalle */
    public HFacturaContratista () {
    }

    public void start(ModelOpav model, Usuario usuario, String distrito){

        this.usuario = usuario;
        this.model = model;
        this.dstrct = distrito;
        this.constanteService = new ConstanteService(usuario.getBd());


        super.start();
    }


    @Override
    public synchronized void run(){



        try{


            String  EJECUTAR_CREACION_CXP_INTERNA     = constanteService.getValor("FINV", "EJECUTAR_CREACION_CXP_INTERNA", "");
            String  EJECUTAR_CREACION_NC_CXP_INTERNA  = constanteService.getValor("FINV", "EJECUTAR_CREACION_NC_CXP_INTERNA", "");
            String  EJECUTAR_CREACION_CXP_CONTRATISTA_FINAL  = constanteService.getValor("FINV", "EJECUTAR_CREACION_CXP_CONTRATISTA_FINAL", "");
            String  EJECUTAR_NC_TRASLADO_CONTRATISTA = constanteService.getValor("FINV", "EJECUTAR_NC_TRASLADO_CONTRATISTA", "");
            String  EJECUTAR_TRASLADO_CONTRATISTA = constanteService.getValor("FINV", "EJECUTAR_TRASLADO_CONTRATISTA", "");
            String  EJECUTAR_NC_FACTORING_FORMULA = constanteService.getValor("FINV", "EJECUTAR_NC_FACTORING_FORMULA", "");
            
            String  EJECUTAR_PROCESO_TRASLADO = constanteService.getValor("FINV", "EJECUTAR_PROCESO_TRASLADO", "");






            // ---------------------------------------------------------------------------------------------------------------------------------------------
            // DEFINICION DEL LOG DE ERROR
            LogWriter logWriter = new LogWriter("com/tsp/util/connectionpool/db", usuario.getLogin(), "/Prefacturas de contratistas ", usuario.getLogin() );
            // FIN DEFINICION DEL LOG DE ERROR
            // ---------------------------------------------------------------------------------------------------------------------------------------------






            // ELABORACION DE FACTURAS CXP INTERNAS DEL CONSORCIO POR CADA ACCION PREFACTURADA SI

            if(!EJECUTAR_CREACION_CXP_INTERNA.equalsIgnoreCase("NO")) {
              model.consorcioService.generaCxpInterna(usuario.getLogin(), dstrct,  model, logWriter);
            }
            else {
                logWriter.log("ADVERTENCIA: No se efectuaron facturas internas porque la constante EJECUTAR_CREACION_CXP_INTERNA esta configurada en NO. \n",LogWriter.INFO);
            }


            // ELABORACION DE UNA NOTA CREDITO QUE CANCELA LA FACTURA INTERNA DEL CONTRATISTA SI
            if(!EJECUTAR_CREACION_NC_CXP_INTERNA.equalsIgnoreCase("NO")) {
              model.consorcioService.generaNcCxpInterna_Contratista(usuario.getLogin(), dstrct,  model,logWriter);
            }
            else {
                logWriter.log("ADVERTENCIA: No se efectuaron traslados porque la constante EJECUTAR_CREACION_NC_CXP_INTERNA_CONTRATISTA esta configurada en NO. \n",LogWriter.INFO);
            }

            // ELABORACION DE LA FACTURA FINAL DEL CONTRATISTA SI
            if(!EJECUTAR_CREACION_CXP_CONTRATISTA_FINAL.equalsIgnoreCase("NO")) {
              model.consorcioService.generaCxpFinalContratista(usuario.getLogin(), dstrct,  model,logWriter);
            }
            else {
                logWriter.log("ADVERTENCIA: No se efectuaron traslados porque la constante EJECUTAR_CREACION_CXP_CONTRATISTA_FINAL esta configurada en NO. \n",LogWriter.INFO);
            }
            
            // ELABORACION DE UNA NOTA CREDITO PARA APLICAR FACTORING Y FORMULA A LA FACTURA EN FINTRA CORRESPONDIENTE A UNA FACTURA DEL CONSORCIO SI
            if(!EJECUTAR_NC_FACTORING_FORMULA.equalsIgnoreCase("NO")) {
              model.consorcioService.generarNcFactoringFormula(usuario.getLogin(), model, logWriter);
            }
            else {
                logWriter.log("ADVERTENCIA: No se efectuaron Notas credito para aplicar factoring y formula porque la constante EJECUTAR_NC_FACTORING_FORMULA esta configurada en NO. \n",LogWriter.INFO);
            }
            
            
             // EJECUCION DEL PROCESO NUEVO DE TRASLADO 
            if(!EJECUTAR_PROCESO_TRASLADO.equalsIgnoreCase("NO")) {
              model.consorcioService.generarTrasladoFacturas(usuario, model, logWriter);
            }
            else {
                logWriter.log("ADVERTENCIA: No se efectuaron Notas credito para aplicar factoring y formula porque la constante EJECUTAR_NC_FACTORING_FORMULA esta configurada en NO. \n",LogWriter.INFO);
            }
            
            
            
            

            //-----------------------------------------------------nuevoo--------------------------------------------------------------------//
            // ELABORACION DE LA NOTA CREDITO A LA FACTURA FINAL DEL CONTRATISTA PARA TRASLADARLA A FINTRA SI
//            if(!EJECUTAR_NC_TRASLADO_CONTRATISTA.equalsIgnoreCase("NO")) {
//              model.consorcioService.generaNcTrasladoContratista(usuario.getLogin(), dstrct,  model,logWriter);
//            }
//            else {
//                logWriter.log("ADVERTENCIA: No se efectuaron traslados porque la constante EJECUTAR_NC_TRASLADO_CONTRATISTA esta configurada en NO. \n",LogWriter.INFO);
//            }
//
//
//            // ELABORACION DE LA FACTURA FINAL DEL CONTRATISTA PARA TRASLADARLA A FINTRA
//            if(!EJECUTAR_TRASLADO_CONTRATISTA.equalsIgnoreCase("NO")) {
//              model.consorcioService.generaTrasladoContratista(usuario.getLogin(), dstrct,  model,logWriter);
//            }
//            else {
//                logWriter.log("ADVERTENCIA: No se efectuaron traslados porque la constante EJECUTAR_TRASLADO_CONTRATISTA esta configurada en NO. \n",LogWriter.INFO);
//            }

           


            //model.LogProcesosSvc.finallyProceso(this.processName, this.hashCode(), usuario.getLogin(), "PROCESO EXITOSO");
        }catch (Exception ex){
            ex.printStackTrace();
            try{
                //model.LogProcesosSvc.finallyProceso(this.processName, this.hashCode(), usuario.getLogin(), "Finalizado con error ...\n" + ex.getMessage());
            }catch (Exception e){
                System.out.println("Error HFacturaContratista ...\n" + e.getMessage());
            }
        }







    }


}