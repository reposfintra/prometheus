/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * ActividadAccionService.java :
 * Copyright 2010 Rhonalf Martinez Villa <rhonaldomaster@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.tsp.opav.model.services;
import java.util.*;
import com.tsp.opav.model.beans.*;
import com.tsp.opav.model.DAOS.*;
/**
 *
 * @author rhonalf
 */
public class ActividadAccionService {

    private ActividadAccionDAO adao;

    public ActividadAccionService() {
        adao = new ActividadAccionDAO();
    }
    public ActividadAccionService(String dataBaseName) {
        adao = new ActividadAccionDAO(dataBaseName);
    }

    /**
     * Busca datos de actividades de accion
     * @param filtro nombre de la columna de la tabla actividades por la cual se quiere filtrar
     * @param param el dato a buscar
     * @return lista con objetos ActividadAccion que se ajustaron al criterio de busqueda
     * @throws Exception cuando hay un error
     */
    public ArrayList buscar(String filtro,String param) throws Exception{
        ArrayList lista=null;
        try {
            lista = adao.buscar(filtro, param);
        }
        catch (Exception e) {
            throw new Exception("Error al buscar datos de contratistas: "+e.toString());
        }
        return lista;
    }

     /**
     * Inserta un nuevo registro en la tabla de actividades
     * @param actividad Objeto de tipo ActividadAccion con los datos
     * @param user Usuario que actualiza el registro
     * @throws Exception Cuando hay un error
     */
    public void insertar(ActividadAccion actividad, String user) throws Exception{
        try {
            adao.insertar(actividad,user);
        }
        catch (Exception e) {
            throw new Exception("Error al insertar datos: "+e.toString());
        }
    }

    /**
     * Actualiza un registro de la tabla de actividades
     * @param actividad Objeto de tipo ActividadAccion con los datos
     * @param user Usuario que actualiza el registro
     * @throws Exception Cuando hay un error
     */
    public void actualizar(ActividadAccion actividad, String user) throws Exception{
        try {
            adao.actualizar(actividad,user);
        }
        catch (Exception e) {
            throw new Exception("Error al actualizar datos: "+e.toString());
        }
    }

    /**
     * Anula un registro de la tabla de actividades
     * @param codigo El codigo del registro
     * @param user Usuario que actualiza el registro
     * @throws Exception Cuando hay un error
     */
    public void anular(String codigo, String user) throws Exception{
        try {
            adao.anular(codigo,user);
        }
        catch (Exception e) {
            throw new Exception("Error al eliminar datos: "+e.toString());
        }
    }

    /**
     * Busca la lista de responsables predeterminados en tablagen
     * @return Listado con los datos encontrados
     * @since 2010-08-12
     * @throws Exception Cuando hay error
     */
    public String datosResponsables() throws Exception{
        ArrayList<String> lista = null;
        String cad = "";
        String[] splitter = null;
        try {
            lista = adao.datosResponsables();
            if(lista!=null && lista.size()>0){
                for (int i = 0; i < lista.size(); i++) {
                    splitter = (lista.get(i)).split(";_;");
                    cad += "<option value='"+splitter[0]+"'>"+splitter[1]+"</option>";
                }
            }
        }
        catch (Exception e) {
            throw new Exception("Error al buscar datos de responsables: "+e.toString());
        }
        return cad;
    }

    /**
     * Busca el nombre del tipo de responsable predeterminados en tablagen
     * @param tipodat table_code en tablagen
     * @return Cadena con el dato encontrado
     * @throws Exception Cuando hay error
     */
    public String nomDat(String tipodat) throws Exception{
        String nom = "";
        try {
            nom = adao.nomDat(tipodat);
        }
        catch (Exception e) {
             throw new Exception("Error al buscar datos de responsables: "+e.toString());
        }
        return nom;
    }

}