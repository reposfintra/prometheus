package com.tsp.opav.model.services;


import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.html.simpleparser.HTMLWorker;
import com.lowagie.text.html.simpleparser.StyleSheet;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import com.tsp.opav.model.DAOS.ElectricaribeOfertaDAO;
import com.tsp.opav.model.beans.*;
import com.tsp.util.Util;

import com.tsp.operation.model.beans.Cliente;
import com.tsp.operation.model.beans.Email2;
import com.tsp.operation.model.beans.MyPageEvent;
import com.tsp.operation.model.beans.RMCantidadEnLetras;
import com.tsp.operation.model.beans.TablaGen;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.services.EmailSendingEngineService;
import java.awt.Color;
import java.util.*;
import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import javax.servlet.ServletException;


/**
 * Clase para tratar todo lo referente al PDF de la oferta y
 * la modificacion de la oferta.
 *
 * @author Pablo Emilio Bassil Orozco
 */
public class ElectricaribeOfertaService {

  //  BaseFont bf;

    private Font mTitulo    = new Font(Font.HELVETICA,14,Font.BOLD);
    private Font mSubtitulo = new Font( Font.BOLDITALIC,14);
    private Font mNormal    = new Font(12);

    //Estos son los tipos de letras para parrafos normales
    private Font titulo     = new Font( Font.HELVETICA,10,Font.BOLD);
    private Font subtitulo  = new Font(Font.HELVETICA,10,  Font.BOLDITALIC);
    private Font normal     = new Font( 9);

    //Letras para tablas
    private Font tTitulo    = new Font( Font.HELVETICA,10,Font.BOLD);
    private Font tSubtitulo = new Font(Font.HELVETICA,10);

     private Font tsubtotales = new Font(Font.HELVETICA,10,Font.BOLD);
    private Font tNormal    = new Font(Font.HELVETICA,10);

    //Letras para la parte de los anexos
    private Font cTitulo    = new Font( Font.HELVETICA,8,Font.BOLD);
    private Font cSubtitulo = new Font( Font.HELVETICA,8,Font.BOLDITALIC);
    private Font cNormal    = new Font(Font.getStyleValue("Calibri"),8);

    private double subtMateriales       = 0;
    private double subtManoDeObra       = 0;
    private double subtOtros            = 0;

    private double totalOffer = 0;

    private Document        document;
    private ResourceBundle  rb;
    private String          ruta;
    private String          next;
    private String[]        meses = {"","Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};

    private DateFormat offerFormat  = new SimpleDateFormat("yyyy-MM-dd");
    private DateFormat emergFormat  = new SimpleDateFormat("yyyyMMdd");
    private DateFormat df           = new SimpleDateFormat("yyyyMM");
    private Date dat                = new Date();

    private PdfWriter               writer;
    private ElectricaribeOfertaDAO  epd;
    private MyPageEvent             mpe;

    private ArrayList<AccionesEca> aecas;

    String[] gerente=null;//20100702
    private String globales;

    SeguimientoEjecucionService ses; 
    ArrayList<Actividades> consulta;
           
    /**
     * Constructor de la clase ElectricaribeOfertaService, el cual
     * inicia las letras y la fecha para la creacion del PDF
     *
     * @throws Exception
     */
    public ElectricaribeOfertaService() throws Exception {
        //bf = BaseFont.createFont( BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED );
        ses = new SeguimientoEjecucionService();
        epd = new ElectricaribeOfertaDAO();
    }
    public ElectricaribeOfertaService(String dataBaseName) throws Exception {
        epd = new ElectricaribeOfertaDAO(dataBaseName);
        ses = new SeguimientoEjecucionService(dataBaseName); 
    }

    /**
     * Este metodo inicia la ruta donde se va a guardar el archivo y
     * es el primer paso para la creacion del PDF, ya que inicia el objeto
     * para la creacion del mismo.
     *
     * @param num Numero de la oferta
     * @param user Usuario logueado
     * @throws Exception
     */
    public void iniciar(String num, String user) throws Exception {

        try {
            epd.setNumOferta(num);
            epd.getEcaInfoForPDF();
            aecas = epd.getAcciones();

            rb          = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            this.ruta   = rb.getString("ruta") + "/exportar/migracion/"+user+"/"+ epd.getOferta().getConsecutivo() + " - " + emergFormat.format(dat).substring(0, 8) + ".pdf";

            File dir = new File(rb.getString("ruta") + "/exportar/migracion/"+user);
            dir.mkdir();


            document    =  new Document(PageSize.LETTER, 80, 60, 70, 70);//
           this.writer= PdfWriter.getInstance(document, new FileOutputStream(this.ruta));
            document.setHeader(this.getMyHeader());
            document.setFooter(this.getMyFooter(this.getOfferDao().getOferta().getTipo_solicitud()));
            subtMateriales      = 0;
            subtManoDeObra      = 0;
            subtOtros           = 0;

            totalOffer          = 0;
        }
        catch (Exception ex) {
            System.out.println("error pdf:"+ex.toString());
                ex.printStackTrace();
            next="Error creando la ruta del archivo";
        }
    }

   
       /**
     * Este metodo es el segundo paso para la creacion del PDF.
     * Ac� es donde todo se agrega a la hoja del PDF.
     */
    public void PDF() throws Exception{
        int ano = 0;
        int mes = 0;
        int dia = 0;
        consulta= ses.consultarActividadesSolicitud(epd.getOferta().getId_solicitud());
        try {

            ano = Integer.valueOf( epd.getOferta().getFechaGeneracion().substring(0, 4) );
            mes = Integer.valueOf( epd.getOferta().getFechaGeneracion().substring(5, 7) );
            dia = Integer.valueOf( epd.getOferta().getFechaGeneracion().substring(8, 10) );

            if (gerente==null){//20100702
                gerente=epd.obtenerAprobador(epd.getOferta().getId_solicitud());//20100702
            }

            document.open();
            document.add(new Paragraph(" "));
            document.add(phtml(epd.getOferta().getCiudad()+", "+dia+" de "+meses[mes]+" de "+ano+"<br/><br/>Se�ores<br/>"+epd.getOferta().getCliente().toUpperCase()+"<br/>NIC:"+epd.getOferta().getNIC()+"<br/>Ciudad<br/>"));
            document.add(new Paragraph(" "));

            document.add(new Paragraph("ASUNTO: OFERTA MERCANTIL PARA  "+epd.getOferta().getOferta().toUpperCase(), titulo));
            document.add(new Paragraph(" "));
           // document.add(new Paragraph(" "));
       
            document.add(phtml("<strong>"+gerente[1]+"</strong>"+" identificado con c�dula de ciudadan�a n�mero "+gerente[0]+" de "+gerente[2]+", actuando en su calidad de Representante Legal de  la sociedad Comercial <strong>SELECTRIK S.A.S.</strong>, ejecutora del programa integral de servicios <strong>MULTISERVICIOS</strong>, en adelante <strong>MULTISERVICIOS EN ASOCIO CON ELECTRICARIBE (Intervenida por Superservicios)</strong> quien en adelante se denominar� EL OFERENTE, presenta a la consideraci�n de "+ epd.getOferta().getCliente()+", identificada con NIT "+ epd.getOferta().getNIT()+", quien en adelante se denominar� EL CLIENTE, una oferta mercantil previa las siguientes consideraciones:<br/>"));
            document.add(new Paragraph("CONVENIO ELECTRICARIBE S.A. E.S.P. (Intervenida por Superservicios) MULTISERVICIOS", titulo));
            document.add(phtml("<br/>"+ GetEmpresa(epd.getOferta().getTipo_cliente())+", ha realizado un convenio para el desarrollo, ejecuci�n y puesta en marcha de sus proyectos el�ctricos con MULTISERVICIOS, el cual es el directo responsable de la ejecuci�n de los trabajos." + GetEmpresa(epd.getOferta().getTipo_cliente())+" actuar� exclusivamente como Agente que factura y recauda estos servicios a trav�s del recibo de energ�a y quien est� facultado para negociar dichos trabajos en t�rminos de precio, financiaci�n o acuerdo a Plazos.<br/>"));
            //document.add(new Paragraph(" ", mNormal));
            document.add(phtml("Para cualquier inquietud respecto a los trabajos realizados, garant�as, p�lizas, contrataci�n, acuerdo a plazos, puede contactar a su ejecutivo de cuenta asignado de "+ GetEmpresa(epd.getOferta().getTipo_cliente())+", para gestionar cualquier solicitud ante MULTISERVICIOS."));
            document.add(new Paragraph(" ", mNormal));
            document.add(phtml("<strong>PRIMERA-OBJETO.</strong> El objeto de la presente oferta comprende la prestaci�n de los servicios descritos a continuaci�n, con plena autonom�a laboral, t�cnica y administrativa: EL OFERENTE ofrece prestar a EL CLIENTE el " +epd.getOferta().getOferta().toUpperCase()+"."));

            //--------------------------------------------------------------

            //document.newPage();
            document.add(new Paragraph(" "));
            document.add(phtml("<strong>SEGUNDA: DESCRIPCION DEL SERVICIO.</strong>"));
  
            document.add(new Paragraph("", mNormal));
            document.add(phtml("El oferente realizar� el servicio mencionado en la cl�usula anterior como sigue:"));

            document.add(new Paragraph("", mNormal));
            document.add(new Paragraph("", mNormal));

            if(aecas.size() > 0){               
                for(int i = 0; i < aecas.size(); i++){
                    if (aecas.get(i).getAlcance().length() > 0){
                        document.add(phtml("<UL type = square><UL><LI><UL><LI> "+aecas.get(i).getAlcance()+"</UL></UL>"));
                    }
                }
            }

            document.add( new Paragraph(" ", normal) );

            if(aecas.size() > 0){
                document.add(new Paragraph("2.2 DESCRIPCIONES DE LA OFERTA", subtitulo));
                for(int i = 0; i < aecas.size(); i++){
                    if (aecas.get(i).getDescripcion().length() > 0){
                        document.add(phtml("<UL type = square><UL><LI><UL><LI> "+aecas.get(i).getDescripcion().replace("\n", "<LI>")+"</UL></UL>"));
                    }
                }
            }

            document.add( new Paragraph(" ", normal) );

            //--------------------------------------------------------------

         //   document.add(phtml("<strong>PARAGRAFO: GARANTIA:</strong>La garant�a del mantenimiento del transformador es de doce (12) meses contra posibles fugas de aceite a los empaques y radiadores y bajo condiciones normales de operaci�n. Para mantener las condiciones de la garant�a se requiere la instalaci�n de tres descargadores de sobretensi�n con puesta a tierra normalizada en el punto de conexi�n.</strong>"));
         //   document.add(new Paragraph(" ", normal));
            document.add(phtml("<strong>TERCERA- ORGANIZACI�N Y MEDIOS.</strong> EL OFERENTE  a trav�s del Ejecutivo de Cuenta de " +GetEmpresa(epd.getOferta().getTipo_cliente())+ "coordinar� con EL CLIENTE, el inicio de los trabajos contratados con el fin de planificar de forma adecuada entre las partes. EL OFERENTE, se presentar� en obra en el plazo, con los siguientes medios:"));
        
            document.add(new Paragraph(" ", normal));
            document.add(phtml("<UL type = square><UL><LI><UL><LI>Equipo humano <LI>Equipos de inspecci�n requeridos, as� como con los materiales consumibles y accesorios</UL></UL>"));
          
            document.add(new Paragraph(" ", normal));
            document.add(new Paragraph("", normal));
            document.add(new Paragraph("", normal));
            document.add(phtml("As� mismo, <strong>EL OFERENTE</strong> se compromete a:"));

            document.add(phtml("<UL type = square><UL><LI><UL><LI> Informar verbalmente y de forma inmediata, al personal designado por EL CLIENTE, de los defectos aparecidos y acciones recomendadas.<LI>Elaborar y entregar un informe final con el resultado de las inspecciones y/o trabajos efectuados.</UL></UL>"));

            document.add(new Paragraph(" ", normal));

            document.add(phtml("<strong>EL CLIENTE </strong>realizar�, a trav�s de su personal de mantenimiento o un designado por �l, las maniobras de apertura o cierre de seccionadores, breakers o totalizadores. En caso que no se cuente con esta persona,<strong>EL OFERENTE </strong> realizar� dichas maniobras a cuenta y riesgo de <strong>EL CLIENTE </strong>"));
             document.add(new Paragraph(" ", normal));
            document.add(phtml("<strong>EL CLIENTE </strong> pondr� a disposici�n de los expertos de <strong>EL OFERENTE</strong> el personal auxiliar necesario conocedor de la red interna de distribuci�n con los permisos de acceso necesarios y las llaves o instrumentos precisos para acceder a las celdas o gabinetes de circuito. Igualmente, <strong>EL CLIENTE</strong> facilitar� todos los esquemas, informaci�n y planos disponibles, necesarios para la correcta realizaci�n de los trabajos. La duraci�n de la inspecci�n y/o de los trabajos estar� pactada o acordada entre el personal de <strong>EL OFERENTE</strong>, el Ejecutivo de Cuenta de "+GetEmpresa(epd.getOferta().getTipo_cliente())+" y el representante por parte de <strong>EL CLIENTE.</strong>"));
           
            document.add(new Paragraph(" ", normal));
            document.add(new Paragraph(" ", normal));
            document.add(new Paragraph(" ", normal));

            //--------------------------------------------------------------

           

            document.add(phtml("<strong>CUARTA-PRECIO.</strong> El valor convenido como precio para la ejecuci�n de esta oferta comercial ser�: "));
            document.add(new Paragraph(" ", normal));
          

            for(int i = 0; i < aecas.size(); i++){
                document.add(new Paragraph("Alcance "+(i+1), normal));
                document.add(new Paragraph(" ", normal));

                if(epd.getMaterialesPorTipo(aecas.get(i).getId_accion(), "M").size() > 0){
                    subtMateriales  = tablaItems(document, aecas.get(i), epd.getMaterialesPorTipo(aecas.get(i).getId_accion(), "M"), "MATERIALES");
                    document.add(new Paragraph(" ", normal));
                }

                if(epd.getMaterialesPorTipo(aecas.get(i).getId_accion(), "D").size() > 0){
                    subtManoDeObra  = tablaItems(document, aecas.get(i), epd.getMaterialesPorTipo(aecas.get(i).getId_accion(), "D"), "MANO DE OBRA");
                    document.add(new Paragraph(" ", normal));
                }

                if(epd.getMaterialesPorTipo(aecas.get(i).getId_accion(), "O").size() > 0){
                    subtOtros       = tablaItems(document, aecas.get(i), epd.getMaterialesPorTipo(aecas.get(i).getId_accion(), "O"), "OTROS");
                    document.add(new Paragraph(" ", normal));
                }

                double[] res = {subtMateriales, subtManoDeObra, subtOtros};

                tablaTotales(document, aecas.get(i), res);

                res = null;
                document.add(new Paragraph(" ", normal));
                document.add(new Paragraph(" ", normal));

                subtMateriales = 0;
                subtManoDeObra = 0;
                subtOtros = 0;
            }

            tablaTotalesGeneral(document);
            
            document.add(new Paragraph("Consideraciones adicionales sobre la presente oferta:", subtitulo));
            for(int i = 0; i < epd.getConsideracionesByOffer().size(); i++){
                document.add(new Paragraph("    - " + epd.getConsideracionesByOffer().get(i).getDescripcion(), normal));
            }
            
            document.add(new Paragraph("Otras consideraciones:", subtitulo));
            document.add(new Paragraph("    - " + epd.getOtrasConsideracionesByOffer().getOtrasConsideraciones(), normal));
            
            
            
            
           // document.add(new Paragraph(" ", normal));

            //document.add(phtml("<strong>PARAGRAFO I:</strong> Los costos en que deba incurrirse por desplazamientos adicionales a otras ciudades diferentes a Barranquilla ser�n facturados a <strong>EL CLIENTE</strong>, esto incluye: desplazamiento, alojamiento y manutenci�n."));
            document.add(new Paragraph(" ", normal));
            document.add(phtml("<strong>PARAGRAFO I:</strong> En caso de requerir trabajos adicionales estos ser�n ofertados y se esperar� por parte del <strong>CLIENTE</strong> su aprobaci�n"));

            document.add(new Paragraph(" ", normal));
            document.add(phtml("<strong>QUINTA-. FORMA Y PROGRAMA DE PAGO:</strong> cancelar� el total de la oferta, a trav�s de la factura del servicio de energ�a el�ctrica emitida por "+ GetEmpresa(epd.getOferta().getTipo_cliente())+". Adicionalmente<strong> EL CLIENTE</strong> podr� solicitar los siguientes planes de financiaci�n de acuerdo a las pol�ticas de cr�dito de<strong> EL OFERENTE:</strong>"));
            document.add(new Paragraph(" ", normal));
            document.add(phtml("<strong>PARAGRAFO I:</strong> Al aceptar la presente oferta el cliente autoriza a "+ GetEmpresa(epd.getOferta().getTipo_cliente())+" a cobrar en la factura del servicio de energ�a el�ctrica el valor de los trabajos ofertados."));
            document.add(new Paragraph(" ", normal));

            document.add(this.datosPagos(epd.getOferta().getTipo_cliente()));

     
            document.newPage();
            document.add(new Paragraph(" "));
            document.add(phtml("<strong>SEXTA-NATURALEZA DE LA OFERTA.</strong> Esta oferta es de naturaleza comercial, dentro de los fines se�alados en la cl�usula primera sobre su objeto. De llegar a aceptarse, <strong>EL CLIENTE</strong> no contraer� v�nculo laboral con ninguno de los trabajadores que emplee <strong>EL OFERENTE</strong> para su ejecuci�n, careciendo, por tanto, de facultades para aplicarles procesos disciplinarios e imponerles sanciones. As� mismo, entre <strong>EL CLIENTE</strong> y <strong>EL OFERENTE</strong> no existir� v�nculo comercial distinto al que surja entre ellos si se aceptare esta oferta, y, en consecuencia, entre ellos no se configura, en virtud de tal aceptaci�n, una sociedad de hecho, ni ninguno se constituir� en agente comercial, ni en representante legal, especial o general, del otro."));
            document.add(new Paragraph(" "));
            document.add(phtml("<strong>SEPTIMA-DERECHO DE RETENCI�N.</strong>Cuando en el desarrollo de los servicios ofrecidos se configure, a juicio de <strong>EL OFERENTE</strong> un incumplimiento de las obligaciones contra�das por <strong>EL CLIENTE</strong> a ra�z de la aceptaci�n de esta oferta, <strong>EL OFERENTE</strong> estar� facultado para retener todos y cada uno de los bienes y/o materiales instalados en ejecuci�n del objeto de esta oferta. De igual forma, <strong>EL CLIENTE</strong> se obliga a pagar o reconocer el costo en que sea necesario incurrir para recuperar o retirar dichos bienes. Para tal efecto, <strong>EL CLIENTE</strong> reconoce, con la aceptaci�n de esta oferta, que el valor de dicho costo ser� el que <strong>EL OFERENTE</strong> exprese en su cobro prejudicial o en la respectiva demanda ejecutiva."));
            document.add(new Paragraph(" ", normal));
            document.add(phtml("<strong>OCTAVA. IRREVOCABILIDAD DE LA OFERTA Y PLAZO PARA SU ACEPTACI�N.</strong> Esta oferta de servicios es irrevocable. Si llegare a aceptarla, <strong>EL CLIENTE</strong> deber� expedir la correspondiente CARTA DE ACEPTACI�N dentro de los quince (15) d�as h�biles siguientes a la presentaci�n de esta oferta. En caso de ser aprobada la oferta nos debe hacer llegar carta firmada por el administrador informando la forma de pago y el NIC al cual se cargar� el costo del servicio, de igual manera autorizar el cobro del anticipo de los trabajos. La validez y el plazo de entrega se mantendr�n siempre y cuando, antes de su aprobaci�n no se presenten hechos graves e impredecibles que modifiquen sustancialmente nuestros costos totales, como una s�bita devaluaci�n monetaria, cambio radical en el precio del acero y del cobre, cambio en otros componentes o problemas de fuerza mayor en el suministro de materia prima.")); document.add(new Paragraph(" ", normal));
            document.add(new Paragraph(" "));
            document.add(phtml("<strong>NOVENA-NULIDAD PARCIAL.</strong> Si llegare a declararse la nulidad de una cualquiera de las disposiciones o cl�usulas de esta oferta, las dem�s conservar�n su validez, siempre y cuando aquella que se declar� nula no fuere de tal relevancia que <strong>EL OFERENTE</strong> no habr�a presentado esta oferta y/o EL <strong>CLIENTE</strong> no la habr�a aceptado sin ella.")); document.add(new Paragraph(" ", normal));
            document.add(new Paragraph(" "));
            document.add(phtml("<strong>DECIMA -ACUERDO INTEGRAL.</strong> Si <strong>EL CLIENTE</strong> aceptare esta oferta, ella sustituir� entonces cualquier convenci�n, pacto, contrato, promesa, propuesta y/u oferta aceptadas, y, en general, cualquier otro acuerdo, verbal o escrito, sin importar que denominaci�n se le otorgue, que haya sido celebrado entre las mismas partes con el mismo objeto, antes de la aceptaci�n de �sta."));
            //-------------------------------------------------------------------------------------------------
            document.add(new Paragraph(" "));            

            document.add(phtml("<strong>ANEXOS DE ESTA OFERTA:</strong>"));
            document.add(new Paragraph(" ", normal));
            document.add(phtml("<UL type = square><UL><LI><UL><LI>Carta de aceptaci�n de oferta.</UL></UL>"));
            document.add(phtml("<UL type = square><UL><LI><UL><LI>Carta de autorizaci�n para la firma del acta de finalizaci�n.</UL></UL>"));
            if (!consulta.isEmpty()) {
                document.add(phtml("<UL type = square><UL><LI><UL><LI>Cronograma de actividades.</UL></UL>"));
            }


            document.add(new Paragraph(" "));
            document.add(new Paragraph(" "));
            document.add(phtml("Atentamente, "));
            document.add(new Paragraph(" ", normal));
            document.add(new Paragraph(" ", normal));
            document.add(phtml("<strong>"+gerente[1]+"</strong>"));
            document.add(new Paragraph("Representante Legal", normal));
            anexos();

        }
        catch (Exception ex) {
            System.out.println("error pdf:"+ex.toString());
                ex.printStackTrace();
            next="Error escribiendo en el archivo";
        }
    }









           /**
     * Este metodo es el segundo paso para la creacion del PDF.
     * Ac� es donde todo se agrega a la hoja del PDF.
     */
    public void PDFEmergencia() throws Exception{

          int ano = 0;
        int mes = 0;
        int dia = 0;
        try {

            ano = Integer.valueOf( epd.getOferta().getFechaGeneracion().substring(0, 4) );
            mes = Integer.valueOf( epd.getOferta().getFechaGeneracion().substring(5, 7) );
            dia = Integer.valueOf( epd.getOferta().getFechaGeneracion().substring(8, 10) );

            if (gerente==null){//20100702
                gerente=epd.obtenerAprobador(epd.getOferta().getId_solicitud());//20100702
            }

            document.open();
            document.add(new Paragraph(" "));
            document.add(phtml(epd.getOferta().getCiudad()+", "+dia+" de "+meses[mes]+" de "+ano+"<br/><br/>Se�ores<br/>"+epd.getOferta().getCliente().toUpperCase()+"<br/>NIC:"+epd.getOferta().getNIC()+"<br/>Ciudad<br/>"));
            document.add(new Paragraph(" "));

            document.add(new Paragraph("ASUNTO: OFERTA MERCANTIL DE EMERGENCIA  ("+epd.getOferta().getOferta().toUpperCase()+" )", titulo));
            document.add(new Paragraph(" "));
            document.add(new Paragraph(" "));

            document.add(phtml("<strong>"+gerente[1]+"</strong>"+" identificado con c�dula de ciudadan�a n�mero "+gerente[0]+" de "+gerente[2]+", actuando en su calidad de Representante Legal de  la sociedad Comercial <strong>SELECTRIK S.A.S.</strong>, ejecutora del programa integral de servicios <strong>MULTISERVICIOS</strong>, en adelante <strong>MULTISERVICIOS EN ASOCIO CON ELECTRICARIBE (Intervenida por Superservicios)</strong> quien en adelante se denominar� EL OFERENTE, presenta a  "+ epd.getOferta().getCliente()+", identificada con NIT "+ epd.getOferta().getNIT()+", quien en adelante se denominar� EL CLIENTE, La oferta mercantil de emergencia con las siguientes consideraciones:<br/>"));
            document.add(new Paragraph("CONVENIO ELECTRICARIBE S.A. E.S.P. (Intervenida por Superservicios) MULTISERVICIOS", titulo));
            document.add(phtml("<br/>"+ GetEmpresa(epd.getOferta().getTipo_cliente())+", ha realizado un convenio para el desarrollo, ejecuci�n y puesta en marcha de sus proyectos el�ctricos con MULTISERVICIOS, el cual es el directo responsable de la ejecuci�n de los trabajos." + GetEmpresa(epd.getOferta().getTipo_cliente())+" actuar� exclusivamente como Agente que factura y recauda estos servicios a trav�s del recibo de energ�a y quien est� facultado para negociar dichos trabajos en t�rminos de precio, financiaci�n o acuerdo a Plazos.<br/>"));
            //document.add(new Paragraph(" ", mNormal));
            document.add(phtml("Para cualquier inquietud respecto a los trabajos realizados, garant�as, p�lizas, contrataci�n, acuerdo a plazos, puede contactar a su ejecutivo de cuenta asignado de "+ GetEmpresa(epd.getOferta().getTipo_cliente())+", para gestionar cualquier solicitud ante MULTISERVICIOS."));
            document.add(new Paragraph(" ", mNormal));
            document.add(phtml("<strong>PRIMERA-OBJETO.</strong> El objeto de la presente oferta comprende la presentaci�n de los costos incurridos a continuaci�n, con plena autonom�a laboral, t�cnica y administrativa: EL OFERENTE atendi� a EL CLIENTE el servicio de " +epd.getOferta().getOferta().toUpperCase()+"."));

            //--------------------------------------------------------------

            document.newPage();
            document.add(new Paragraph(" "));
            document.add(phtml("<strong>SEGUNDA: DESCRIPCION DEL SERVICIO.</strong>"));

            document.add(new Paragraph("", mNormal));
            document.add(phtml("El oferente realiz� el dia "+dia+" de "+meses[mes]+" de "+ano+" el  servicio mencionado en la cl�usula anterior como sigue:"));

            document.add(new Paragraph("", mNormal));
            document.add(new Paragraph("", mNormal));

            if(aecas.size() > 0){
                for(int i = 0; i < aecas.size(); i++){
                    if (aecas.get(i).getAlcance().length() > 0){
                        document.add(phtml("<UL type = square><UL><LI><UL><LI> "+aecas.get(i).getAlcance()+"</UL></UL>"));
                    }
                }
            }

            document.add( new Paragraph(" ", normal) );

            if(aecas.size() > 0){
                document.add(new Paragraph("2.2 DESCRIPCIONES DE LA OFERTA", subtitulo));
                for(int i = 0; i < aecas.size(); i++){
                    if (aecas.get(i).getDescripcion().length() > 0){
                        document.add(phtml("<UL type = square><UL><LI><UL><LI> "+aecas.get(i).getDescripcion().replace("\n", "<LI>")+"</UL></UL>"));
                    }
                }
            }

            document.add( new Paragraph(" ", normal) );

            //--------------------------------------------------------------

            document.add(phtml("<strong>TERCERA-PRECIO.</strong> El valor de la presente Oferta Mercantil de Emergencia es de: "));
            document.add(new Paragraph(" ", normal));


            for(int i = 0; i < aecas.size(); i++){
                document.add(new Paragraph("Alcance "+(i+1), normal));
                document.add(new Paragraph(" ", normal));

                if(epd.getMaterialesPorTipo(aecas.get(i).getId_accion(), "M").size() > 0){
                    subtMateriales  = tablaItems(document, aecas.get(i), epd.getMaterialesPorTipo(aecas.get(i).getId_accion(), "M"), "MATERIALES");
                    document.add(new Paragraph(" ", normal));
                }

                if(epd.getMaterialesPorTipo(aecas.get(i).getId_accion(), "D").size() > 0){
                    subtManoDeObra  = tablaItems(document, aecas.get(i), epd.getMaterialesPorTipo(aecas.get(i).getId_accion(), "D"), "MANO DE OBRA");
                    document.add(new Paragraph(" ", normal));
                }

                if(epd.getMaterialesPorTipo(aecas.get(i).getId_accion(), "O").size() > 0){
                    subtOtros       = tablaItems(document, aecas.get(i), epd.getMaterialesPorTipo(aecas.get(i).getId_accion(), "O"), "OTROS");
                    document.add(new Paragraph(" ", normal));
                }

                double[] res = {subtMateriales, subtManoDeObra, subtOtros};

                tablaTotales(document, aecas.get(i), res);

                res = null;
                document.add(new Paragraph(" ", normal));
                document.add(new Paragraph(" ", normal));

                subtMateriales = 0;
                subtManoDeObra = 0;
                subtOtros = 0;
            }

            tablaTotalesGeneral(document);
            
            document.add(phtml("<strong>Consideraciones adicionales sobre la presente oferta"));
            for(int i = 0; i < epd.getConsideracionesByOffer().size(); i++){
                document.add(new Paragraph("    � " + epd.getConsideracionesByOffer().get(i).getDescripcion(), normal));
            }
            
            
            
            document.add(new Paragraph(" ", normal));

            document.add(phtml("<strong>PARAGRAFO - PRESTACION DE SERVICIOS Y TRANSFORMADORES:</strong> En caso de presentarse en la Emergencia un suministro de transformador en calidad de pr�stamo; el equipo se entender� prestado hasta la presentaci�n de la oferta de reparaci�n por parte de Fintra S.A., la cual se har� dentro de los quince (15) d�as calendarios siguientes a la firma del CONVENIO DE PRESTACI�N DE SERVICIOS Y TRANSFORMADORES, sujeto dicho pr�stamo a la condici�n de que el CLIENTE acepte la oferta mercant�l de Reparaci�n o compra del transformador. En caso de no aceptar la oferta, se le cobrar� por el uso del equipo transformador, de acuerdo con la tarifa diaria dependiendo del tipo y potencia de este."));


            document.add(new Paragraph(" ", normal));
            document.add(phtml("<strong>CUARTA-. FORMA Y PROGRAMA DE PAGO:</strong> cancelar� el total de la oferta, a trav�s de la factura del servicio de energ�a el�ctrica emitida por "+ GetEmpresa(epd.getOferta().getTipo_cliente())+". Adicionalmente<strong> EL CLIENTE</strong> podr� solicitar los siguientes planes de financiaci�n de acuerdo a las pol�ticas de cr�dito de<strong> EL OFERENTE:</strong>"));
            document.add(new Paragraph(" ", normal));
            document.add(phtml("<strong>PARAGRAFO I:</strong> Al aceptar la presente Oferta Mercantil de Emergencia el cliente autoriza a "+ GetEmpresa(epd.getOferta().getTipo_cliente())+" a cobrar en la factura del servicio de energ�a el�ctrica el valor de los trabajos ofertados."));
            document.add(new Paragraph(" ", normal));

            document.add(this.datosPagos(epd.getOferta().getTipo_cliente()));


            document.newPage();
            document.add(new Paragraph(" "));
            document.add(phtml("<strong>QUINTA-NATURALEZA DE LA OFERTA.</strong> Esta oferta es de naturaleza comercial, dentro de los fines se�alados en la cl�usula primera sobre su objeto. De llegar a aceptarse, <strong>EL CLIENTE</strong> no contraer� v�nculo laboral con ninguno de los trabajadores que emplee <strong>EL OFERENTE</strong> para su ejecuci�n, careciendo, por tanto, de facultades para aplicarles procesos disciplinarios e imponerles sanciones. As� mismo, entre <strong>EL CLIENTE</strong> y <strong>EL OFERENTE</strong> no existir� v�nculo comercial distinto al que surja entre ellos si se aceptare esta oferta, y, en consecuencia, entre ellos no se configura, en virtud de tal aceptaci�n, una sociedad de hecho, ni ninguno se constituir� en agente comercial, ni en representante legal, especial o general, del otro."));
            document.add(new Paragraph(" "));
            document.add(phtml("<strong>SEXTA-DERECHO DE RETENCI�N.</strong>Cuando en el desarrollo de los servicios ofrecidos se configure, a juicio de <strong>EL OFERENTE</strong> un incumplimiento de las obligaciones contra�das por <strong>EL CLIENTE</strong> a ra�z de la aceptaci�n de esta oferta, <strong>EL OFERENTE</strong> estar� facultado para retener todos y cada uno de los bienes y/o materiales instalados en ejecuci�n del objeto de esta oferta. De igual forma, <strong>EL CLIENTE</strong> se obliga a pagar o reconocer el costo en que sea necesario incurrir para recuperar o retirar dichos bienes. Para tal efecto, <strong>EL CLIENTE</strong> reconoce, con la aceptaci�n de esta oferta, que el valor de dicho costo ser� el que <strong>EL OFERENTE</strong> exprese en su cobro prejudicial o en la respectiva demanda ejecutiva."));
            document.add(new Paragraph(" "));
            document.add(phtml("<strong>SEPTIMA. PLAZO PARA SU ACEPTACI�N Y ESCOGENCIA DE PAGO.</strong><strong> EL CLIENTE</strong> despu�s de recibir la Oferta Mercantil de Emergencia tendr� un plazo m�ximo de cinco (5) d�as h�biles para escoger la forma de pago. El Cliente deber� hacernos llegar carta firmada por el Representante Legal informando la forma de pago y el NIC al cual se cargar� el costo del servicio; en caso contrario se facturar� a doce (12) meses."));
            document.add(new Paragraph(" "));
            document.add(phtml("<strong>OCTAVA-NULIDAD PARCIAL.</strong> Si llegare a declararse la nulidad de una cualquiera de las disposiciones o cl�usulas de esta oferta, las dem�s conservar�n su validez, siempre y cuando aquella que se declar� nula no fuere de tal relevancia que <strong>EL OFERENTE</strong> no habr�a presentado esta oferta y/o EL <strong>CLIENTE</strong> no la habr�a aceptado sin ella.")); 
            document.add(new Paragraph(" "));
            document.add(phtml("<strong>NOVENA -ACUERDO INTEGRAL.</strong> Si <strong>EL CLIENTE</strong> aceptare esta oferta, ella sustituir� entonces cualquier convenci�n, pacto, contrato, promesa, propuesta y/u oferta aceptadas, y, en general, cualquier otro acuerdo, verbal o escrito, sin importar que denominaci�n se le otorgue, que haya sido celebrado entre las mismas partes con el mismo objeto, antes de la aceptaci�n de �sta."));
            //-------------------------------------------------------------------------------------------------




            document.add(new Paragraph(" "));
             document.newPage();
            document.add(new Paragraph(" "));


            document.add(phtml("<strong>ANEXO DE ESTA OFERTA:</strong><BR/>"));
            document.add(phtml("<UL type = square><UL><LI><UL><LI>Carta de aceptaci�n de oferta</UL></UL>"));


            document.add(new Paragraph(" "));
            document.add(new Paragraph(" "));
            document.add(phtml("Atentamente, "));
            document.add(new Paragraph(" "));
            document.add(phtml("<strong>JOSE LUIS GOMEZ OLARTE</strong>"));
            document.add(new Paragraph("Representante Legal", normal));



            //-------------------------ANEXO1--------------------------------//
                    document.newPage();
            document.add(new Paragraph(" ", cNormal));
            document.add(new Paragraph("ANEXO 1                       CARTA DE ACEPTACION DE LA OFERTA  ", titulo));
            document.add(new Paragraph("  ", cNormal));
           document.add(phtml("Barranquilla, d�a __ mes __ a�o ____<br/>SE�ORES<br/>MULTISERVICIOS.<br/>Atn: "+epd.getOferta().getEjecutivo()+" <br/>"+epd.getOferta().getCiudad()+"<br/>"));
           
            document.add(phtml("REFERENCIA: AUTORIZACION DE TRABAJOS OFERTA MERCANTIL " + epd.getOferta().getConsecutivo() + " Y EXPEDICI�N DE ORDEN DE COMPRA"));
            document.add(new Paragraph(" ", cNormal));
            document.add(phtml("    <strong>I.   ACEPTACION Y AUTORIZACION</strong>"));



            document.add(phtml("Por medio de la presenta yo, <strong>" + epd.getOferta().getRepresentante() + "</strong>, en representaci�n de  <strong>" + epd.getOferta().getCliente().toUpperCase() + "</strong> con NIT No. " + epd.getOferta().getNIT() + " y en calidad de REPRESENTANTE LEGAL, me permito aceptar la oferta y en consecuencia autorizar a Ustedes ejecutar las labores cotizada en la oferta de la referencia."));
            document.add(new Paragraph(" ", cNormal));

            document.add(phtml("De igual manera autorizo sea cargada un anticipo del trabajo del ____% a la factura de energ�a el�ctrica el cual est� en el sistema comercial inidentificado con el NIC " + epd.getOferta().getNIC() + " y el ____% restante por favor diferirlo en _______ cuotas mensuales a trav�s del mismo n�mero de Contrato."));
            document.add(new Paragraph(" ", cNormal));
            document.add(phtml("Teniendo en cuenta lo anterior, por medio del presente documento autorizo a " + GetEmpresa(epd.getOferta().getTipo_cliente()) + "expresamente a cobrar en la factura de energ�a el�ctrica estos valores."));
            document.add(new Paragraph(" ", cNormal));
            document.add(phtml("    <strong>II.   ORDEN DE COMPRA </strong>"));
           // document.add(new Paragraph(" ", cNormal));
            document.add(phtml("Con base en todo lo expuesto, la empresa  <strong>" + epd.getOferta().getCliente().toUpperCase() + "</strong> con NIT No.  <strong>" + epd.getOferta().getNIT() + "</strong> que represento emite la Orden de Compra No.__________ para la ejecuci�n de los trabajos contenidos en la Oferta mercantil No.<strong> " + epd.getOferta().getConsecutivo() + ", en la forma antes indicada.</strong>."));
            document.add(new Paragraph(" ", cNormal));
            document.add(phtml("El valor de la oferta: <strong>$ "+Util.customFormat(totalOffer)+"</strong> ("+numToStr( (int)totalOffer )+")."));
            document.add(phtml("<h5>El valor de la presenta oferta puede variar en funci�n de la liquidaci�n final de los trabajos contratados</h5>"));
            document.add(new Paragraph(" ", cNormal));

            //document.add(phtml("Agradezco su atenci�n,  "));
            //document.add(new Paragraph(" ", cNormal));
            document.add(phtml("Cordialmente, "));
           // document.add(new Paragraph(" ", normal));
            document.add(phtml(" <strong>" + epd.getOferta().getRepresentante() + "</strong>, "));
            document.add(new Paragraph("c.c.", normal));

        }
        catch (Exception ex) {
            System.out.println("error pdf:"+ex.toString());
                ex.printStackTrace();
            next="Error escribiendo en el archivo";
        }

    }













    public void anexos () throws DocumentException, IOException, Exception{
//------------------------ Anexos-------------------------------------------------//

            document.newPage();
            document.add(new Paragraph(" ", cNormal));
            document.add(new Paragraph("ANEXO 1                       CARTA DE ACEPTACION DE LA OFERTA  ", titulo));
            document.add(new Paragraph("  ", cNormal));
            document.add(phtml("Barranquilla, d�a __ mes __ a�o ____<br/>SE�ORES<br/>MULTISERVICIOS.<br/>Atn: "+epd.getOferta().getEjecutivo()+" <br/>"+epd.getOferta().getCiudad()+"<br/>"));

            document.add(phtml("REFERENCIA: AUTORIZACION DE TRABAJOS OFERTA MERCANTIL " + epd.getOferta().getConsecutivo() + " Y EXPEDICI�N DE ORDEN DE COMPRA"));
            document.add(new Paragraph(" ", cNormal));
            document.add(phtml("    <strong>I.   ACEPTACION Y AUTORIZACION</strong>"));



            document.add(phtml("Por medio de la presenta yo, <strong>" + epd.getOferta().getRepresentante() + "</strong>, en representaci�n de  <strong>" + epd.getOferta().getCliente().toUpperCase() + "</strong> con NIT No. " + epd.getOferta().getNIT() + " y en calidad de REPRESENTANTE LEGAL, me permito aceptar la oferta y en consecuencia autorizar a Ustedes ejecutar las labores cotizada en la oferta de la referencia."));
            document.add(new Paragraph(" ", cNormal));

            document.add(phtml("De igual manera autorizo sea cargada un anticipo del trabajo del ____% a la factura de energ�a el�ctrica el cual est� en el sistema comercial inidentificado con el NIC " + epd.getOferta().getNIC() + " y el ____% restante por favor diferirlo en _______ cuotas mensuales a trav�s del mismo n�mero de Contrato."));
            document.add(new Paragraph(" ", cNormal));
            document.add(phtml("Teniendo en cuenta lo anterior, por medio del presente documento autorizo a " + GetEmpresa(epd.getOferta().getTipo_cliente()) + "expresamente a cobrar en la factura de energ�a el�ctrica estos valores."));
            document.add(new Paragraph(" ", cNormal));
            document.add(phtml("    <strong>II.   ORDEN DE COMPRA </strong>"));
            document.add(new Paragraph(" ", cNormal));
            document.add(phtml("Con base en todo lo expuesto, la empresa  <strong>" + epd.getOferta().getCliente().toUpperCase() + "</strong> con NIT No.  <strong>" + epd.getOferta().getNIT() + "</strong> que represento emite la Orden de Compra No.__________ para la ejecuci�n de los trabajos contenidos en la Oferta mercantil No.<strong> " + epd.getOferta().getConsecutivo() + ", en la forma antes indicada.</strong>"));
            document.add(new Paragraph(" ", cNormal));
            document.add(phtml("El valor de la oferta: <strong>$ "+Util.customFormat(totalOffer)+"</strong> ("+numToStr( (int)totalOffer )+")."));
            document.add(phtml("<h5>El valor de la presenta oferta puede variar en funci�n de la liquidaci�n final de los trabajos contratados</h5>"));
            document.add(new Paragraph(" ", cNormal));
            
            //document.add(phtml("Agradezco su atenci�n,  "));
            //document.add(new Paragraph(" ", cNormal));
            document.add(phtml("Cordialmente, "));
            document.add(new Paragraph(" ", cNormal));
            document.add(phtml(" <strong>" + epd.getOferta().getRepresentante() + "</strong>, "));
            document.add(new Paragraph("c.c.", normal));


/*---------------------------------------------------------------------------------------------------------------------------------*/
             document.newPage();
             document.add(new Paragraph(" "));
             document.add(new Paragraph("ANEXO 2  CARTA DE AUTORIZACION PARA LA FIRMA DEL ACTA DE FINALIZACION", titulo));
             document.add(new Paragraph(" "));
             document.add(phtml("Yo, <strong>" + epd.getOferta().getRepresentante() + "</strong> identificado con c�dula de ciudadan�a No. ________________ en calidad de REPRESENTANTE LEGAL de la Sociedad de <strong>" + epd.getOferta().getCliente().toUpperCase() + "</strong> con NIT No. <strong>" + epd.getOferta().getNIT() + "</strong>, me permito autorizar a nuestro funcionario ____________________________________ identificado con c�dula de ciudadan�a No. _____________________, a recibir los trabajos ejecutados correspondientes a la oferta de la referencia <strong> " + epd.getOferta().getConsecutivo() + ",</strong>  realizada por <strong>EL OFERENTE</strong>, incluyendo trabajos adicionales."));//20100702
             document.add(new Paragraph(" ", cNormal));
             document.add(phtml("Atentamente,"));
             document.add(new Paragraph(" ", cNormal));
             document.add(new Paragraph(" ", cNormal));
             document.add(phtml("<strong>" + epd.getOferta().getRepresentante()));
             document.add(new Paragraph("c.c.", normal));
/*---------------------------------------------------------------------------------------------------------------------------------*/
             if (!consulta.isEmpty()) {
             
           int numCols = 0; int a, b; boolean dias = true;
           for (int i = 0; i < consulta.size(); i++) {
               try {
                   a = Integer.parseInt(((Actividades) consulta.get(i)).getFechaFinal());
               } catch (NumberFormatException nFe) {
                   a = 0;
                }
               numCols = (numCols < a) ? a : numCols;
           }

           if (numCols > 45) {
               numCols = (Integer) (numCols / 7) + 1;
               dias = false;
           }
           document.setPageSize(PageSize.LETTER.rotate());
           document.newPage();
           document.add(new Paragraph(" "));
           document.add(new Paragraph("ANEXO 3  CRONOGRAMA DE ACTIVIDADES EN "
                        +((dias)?"DIAS":"SEMANAS")+".", titulo));
           document.add(new Paragraph(" "));
           PdfPTable table = new PdfPTable(numCols + 1);
           table.setWidthPercentage(100);
           table.setKeepTogether(true);
           table.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
           PdfPCell cell;
           int[] widths = new int[numCols + 1];
           widths[0] = 30;
           
           //celdas informacion 
           for (int j = 0; j < consulta.size(); j++) {
               Actividades act = consulta.get(j);
               if (j > 0 && act.getDescAccion().equals(consulta.get(j - 1).getDescAccion())) {
                   //table.addCell(act.getDescAccion());                    
               } else {
                   //filas encabezado
                    cell = new PdfPCell(new Phrase("Acci�n"));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    table.addCell(cell);
                    cell = new PdfPCell(new Phrase(act.getDescAccion(), cSubtitulo));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setColspan(numCols);
                    table.addCell(cell);
                    
                    cell = new PdfPCell(new Phrase("Actividad"));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    table.addCell(cell);

                    //titulos fechas
                    for (int i = 0; i < numCols; i++) {
                        cell = new PdfPCell(new Phrase("" + (i + 1)));
                        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                        table.addCell(cell);
                        widths[i + 1] = 8;
                    }
               }
               table.addCell(new Phrase(act.getDescripcion(), cSubtitulo));
               a = Integer.parseInt(((Actividades) consulta.get(j)).getFechaInicial());
               b = Integer.parseInt(((Actividades) consulta.get(j)).getFechaFinal());
               if (!dias) {
                   a /=7; b/=7;
               }
               for (int k = 0; k < numCols; k++) {
                   cell = new PdfPCell(new Phrase(""));
                   if (k >= (a - 1) && k < b) {
                       //BaseColor myColor = WebColors.getRGBColor("#A00000");//BaseColor.GREEN
                       cell.setBackgroundColor(new Color(173, 216, 230));
                   }
                   table.addCell(cell);
               }
           }
           table.setWidths(widths);
           document.add(table);
           document.add(new Paragraph(" "));
           document.add(new Paragraph("Nota: El d�a uno del cronograma de actividades adjunto ser� igual al d�a en que se firme el acta de inicio de obras ", tSubtitulo));
           document.add(new Paragraph(" "));
             }
    }







   protected PdfPTable datosPagos(String tipo)throws DocumentException, Exception {

        PdfPTable tabla_temp = new PdfPTable(2);
        float[] medidaCeldas = {0.400f, 0.400f};
        tabla_temp.setWidths(medidaCeldas);
        PdfPCell celda_temp = new PdfPCell();
       Color backg=new Color(173, 216, 230);


        celda_temp.setPhrase(new Phrase("Plazo", this.tSubtitulo));
        celda_temp.setColspan(0);
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        celda_temp.setBackgroundColor(backg);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Tasa ", tSubtitulo));
        celda_temp.setColspan(0);
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        celda_temp.setBackgroundColor(backg);
        tabla_temp.addCell(celda_temp);


        if(tipo.equals("R"))
        {
            // fila 2
            if (validarEdificio(epd.getOferta().getId_solicitud()).equals("S")) {
                celda_temp = new PdfPCell();
                celda_temp.setPhrase(new Phrase("De 2 a 12 Meses ", tSubtitulo));
                celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
                tabla_temp.addCell(celda_temp);
               
                celda_temp = new PdfPCell();
                celda_temp.setPhrase(new Phrase("Tasa M�xima Legal Vigente ", tSubtitulo));
                celda_temp.setColspan(0);
                celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
                tabla_temp.addCell(celda_temp);
           
            } else {
                celda_temp = new PdfPCell();
                celda_temp.setPhrase(new Phrase("De 2 a 6 Meses ", tSubtitulo));
                celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
                tabla_temp.addCell(celda_temp);
                celda_temp = new PdfPCell();
                celda_temp.setPhrase(new Phrase("DTF + 9 ", tSubtitulo));
                celda_temp.setColspan(0);
                celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
                tabla_temp.addCell(celda_temp);

                celda_temp = new PdfPCell();
                celda_temp.setPhrase(new Phrase("De 7 a 9 Meses ", tSubtitulo));
                celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
                tabla_temp.addCell(celda_temp);
                celda_temp = new PdfPCell();
                celda_temp.setPhrase(new Phrase("DTF + 12 ", tSubtitulo));
                celda_temp.setColspan(0);
                celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
                tabla_temp.addCell(celda_temp);

                celda_temp = new PdfPCell();
                celda_temp.setPhrase(new Phrase("De 10 a 12 Meses ", tSubtitulo));
                celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
                tabla_temp.addCell(celda_temp);
                celda_temp = new PdfPCell();
                celda_temp.setPhrase(new Phrase("Tasa M�xima Legal Vigente ", tSubtitulo));
                celda_temp.setColspan(0);
                celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
                tabla_temp.addCell(celda_temp);
            }
       }
       else
       {
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("De $0 a $10 Millones ", tSubtitulo));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Tasa M�xima Legal Vigente ", tSubtitulo));
        celda_temp.setColspan(0);
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("De $10.1 a $100 Millones ", tSubtitulo));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("DTF + 12 ", tSubtitulo));
        celda_temp.setColspan(0);
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);

        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("De $100.1 en adelante ", tSubtitulo));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("DTF + 9 ", tSubtitulo));
        celda_temp.setColspan(0);
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);

        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("El plazo m�ximo de financiaci�n ser� de 12 meses ", tSubtitulo));
        celda_temp.setColspan(2);
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);
       }

        return tabla_temp;
    }
















     public Paragraph phtml(String texto) throws IOException
    {
            //Creas un p�rrafo:
            Paragraph parrafo = new Paragraph();
            //Estableces el tipo de alineaci�n:
            parrafo.setAlignment(Element.ALIGN_JUSTIFIED);
            //As� se alinear� todo lo que tengas en el p�rrfo. Si el texto, por ejemplo se toma de un HTML, que puede estar formateado y con varios p�rrafos, todos los p�rrafos que tenga el HTML se justifican:
            ArrayList htmlObjs = HTMLWorker.parseToList(new StringReader("<span style=\"font-size: 10px; font-family: Calibri;text-align:justify \">"+texto+"</span>"),this.stylehtml());
            for (int k = 0; k < htmlObjs.size(); ++k) {
                Element elementHtml = (Element) htmlObjs.get(k);
                //Vamos a�adiendo el elemento HTML al p�rrafo anterior:
                parrafo.add(elementHtml);
            }   //A�adimos el p�rrafo al PDF:

            return parrafo;

    }

    public StyleSheet stylehtml() throws IOException
    {

            StyleSheet  styles = new StyleSheet();
            styles.loadTagStyle("body", "face", "times new roman");
            styles.loadTagStyle("body", "size", "14px");
             styles.loadTagStyle("<span>", "size", "14px");
            return  styles;

    }




    public HeaderFooter getMyHeader() throws ServletException {
        try {
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String url_logo = "selectrik.jpg";
            Image img = Image.getInstance(rb.getString("ruta") + "/images/" + url_logo);
            Image img2 = Image.getInstance(rb.getString("ruta") + "/images/electricaribe_logo_super_.png");
//Se comenta esta parte por solicitud del sr Antonio Melendez. A partir de ahora todas las ofertas seran Electricaribe.
//            if (epd.getOferta().getTipo_cliente().equals("NR")){
//               img2 = Image.getInstance(rb.getString("ruta") + "/images/energia_empresarial.png");
//
//            }



           // img2.scaleToFit(100, 400);

            img.scaleAbsolute(125, 44);
            img2.scaleAbsolute(160, 46);
            img2.setAlignment(Image.ALIGN_RIGHT);


            Phrase p = new Phrase();
            p.add(new Chunk(img,0,-20));
            p.add(new Chunk("                                                 "));
            p.add(new Chunk(img2,0, -20));

            HeaderFooter header = new HeaderFooter(p, false);
            header.setBorder(Rectangle.NO_BORDER);
            header.setAlignment(Paragraph.ALIGN_LEFT);

            return header;
        } catch (Exception ex) {
            throw new ServletException("Header Error");
        }
    }



 public HeaderFooter getMyFooter(String tipo) throws ServletException {
        try {
            String texto="Oferta Mercantil - ";

             if(tipo.equals("Emergencia"))
             {
              texto="Oferta Mercantil de Emergencia -";
             }

             Phrase p = new Phrase();
             Font fuente = new Font(Font.HELVETICA, 9);
             p.clear();
             p.add(new Paragraph(texto+epd.getOferta().getConsecutivo()+"  ",fuente));
             HeaderFooter header = new HeaderFooter(p, false);
             header.setBorder(Rectangle.NO_BORDER);
             header.setAlignment(Paragraph.ALIGN_LEFT);

             return header;
         } catch (Exception ex) {
            throw new ServletException("Header Error");
         }
     }













 /* public void onEndPage(PdfWriter writer, Document document) {
        int pageNumberAlignment = Element.ALIGN_CENTER;
           PdfTemplate totalPages ; totalPages = writer.getDirectContent().createTemplate(100, 100);
           BaseFont baseFont   = load("fonts", "tahoma.ttf");;


	        PdfContentByte cb = writer.getDirectContent();
	        cb.saveState();
	        String text = String.format("Page %s of ", writer.getPageNumber());

	        float textBase = document.bottom() - 20;
	        float textSize = 10;

	        cb.beginText();
	        cb.setFontAndSize(baseFont, 8f);
	        if(Element.ALIGN_CENTER == pageNumberAlignment) {
	            cb.setTextMatrix((document.right() / 2), textBase);
            cb.showText(text);
	            cb.endText();
	            cb.addTemplate(totalPages, (document.right() / 2) + textSize, textBase);
	        } else if(Element.ALIGN_LEFT == pageNumberAlignment) {
	            cb.setTextMatrix(document.left(), textBase);
	            cb.showText(text);
	            cb.endText();
	            cb.addTemplate(totalPages, document.left() + textSize, textBase);
	        } else {
	            float adjust = baseFont.getWidthPoint("0", 8f);
	            cb.setTextMatrix(document.right() - textSize - adjust, textBase);
	            cb.showText(text);
	            cb.endText();
	            cb.addTemplate(totalPages, document.right() - adjust, textBase);
	        }
	        cb.restoreState();
	    }

*/






  /**
     * Este metodo sirve para devolver el precio venta de
     * la solicitud
     *
     * @param id_sol Numero de la solicitud
     * @return precio venta de la solicitud
     */
    public String getOfferValue(String id_sol) throws Exception{
        return epd.getOfferValue(id_sol);
    }

    /**
     * Este m�todo me devuelve el DAO de la oferta de electricaribe
     * con todos los datos.
     *
     * @return ElectricaribeOfertaDAO
     */
    public ElectricaribeOfertaDAO getOfferDao(){
        return epd;
    }


    /**
     * Este metodo es el segundo paso para la creacion del PDF.
     * Ac� es donde todo se agrega a la hoja del PDF.
     */
    public void tablaTitulos(Document doc, String cad) throws Exception{
        PdfPTable table = new PdfPTable(1);
        table.setWidthPercentage(100);

        celda(cad, table, tTitulo,Color.WHITE,Color.WHITE);
        doc.add(table);
    }



    /**
     * Esta tabla sirve para listar la lista de los materiales de cada accion
     * de la oferta ya sean MANO DE OBRA, MATERIAL o OTROS.
     *
     * @param doc El documento donde se va a incluir la tabla (Variable tipo Document)
     * @param al ArrayList que contiene la informacion de cada elemento que va a ser ingresado en tabla
     * @param titulo Titulo que va a llevar la tabla
     * @return Devuelve el dinero total que da la tabla
     */
    public double tablaItems(Document doc, AccionesEca ae, ArrayList<MaterialEca> al, String titulo){
        try{
            float[] widths = new float[6];
            if (!this.getGlobales().equals("1")) {
            widths[0] = 0.10f;
            widths[1] = 0.4f;
            widths[2] = 0.10f;
            widths[3] = 0.10f;
            widths[4] = 0.15f;
            widths[5] = 0.15f;
            }else{
            widths = new float[5];
            widths[0] = 0.10f;
            widths[1] = 0.4f;
            widths[2] = 0.10f;
            widths[3] = 0.10f;
            widths[4] = 0.15f;
            }

            double total = 0;
            double num = 0;

            MaterialEca ti;
            PdfPTable table;
            PdfPTable subt;

            table = new PdfPTable(widths);
            table.setWidthPercentage(100);
            subt = new PdfPTable(widths);
            subt.setWidthPercentage(100);

            //Titulo
            if (!this.getGlobales().equals("1")) {
                celda(titulo, table, tTitulo, Color.WHITE, Color.BLACK, 6);
            } else {
                celda(titulo, table, tTitulo, Color.WHITE, Color.BLACK, 5);
            }

            //Subtitulos
            celda("ITEM", table, tsubtotales, new Color(173, 216, 230));
            celda("DESCRIPCI�N", table, tsubtotales, new Color(173, 216, 230));
            celda("UNDS", table, tsubtotales, new Color(173, 216, 230));
            celda("CANT", table, tsubtotales, new Color(173, 216, 230));
            if (!this.getGlobales().equals("1")) {
                celda("VR UNITARIO", table, tsubtotales, new Color(173, 216, 230));
            }
            celda("VR TOTAL", table, tsubtotales, new Color(173, 216, 230));

            for (int i = 0; i < al.size(); i++) {
                ti = al.get(i);
                total += Double.parseDouble(ti.getVlr_total());
            }

            //Valores
            for (int i = 0; i < al.size(); i++) {
                ti = al.get(i);

                 num = Double.parseDouble(ti.getVlr_unitario());

                celda(i + 1, table, tNormal, Color.WHITE, Color.BLACK);
                celda(ti.getDescripcion(), table, tNormal, Color.WHITE, Color.BLACK);
                if (i == 0 && this.getGlobales().equals("1")) {
                    celda(al.size(),"Global", table, tNormal, Color.WHITE, Color.BLACK);
                    celda(al.size(),"1.00", table, tNormal, Color.WHITE, Color.BLACK);
                    celda(al.size(),"$ " + Util.customFormat( total ) , table, tNormal, Color.WHITE, Color.BLACK);
                }
                if(!this.getGlobales().equals("1")){
                celda(ti.getUnidad(), table, tNormal, Color.WHITE, Color.BLACK);
                celda(ti.getCantidad(), table, tNormal, Color.WHITE, Color.BLACK);
                celda("$ " + Util.customFormat( Double.parseDouble(ti.getVlr_unitario()) ) , table, tNormal, Color.WHITE, Color.BLACK);
                celda("$ " + Util.customFormat( Double.parseDouble(ti.getVlr_total()) ) , table, tNormal, Color.WHITE, Color.BLACK);
                }

            }

            //Parte de subtotal
            celda(" ", table, tSubtitulo, Color.WHITE, Color.WHITE, true);
            celda("Subtotal", table, tsubtotales, Color.WHITE, Color.WHITE, true);
            celda(" ", table, tsubtotales, Color.WHITE, Color.WHITE, true);
            celda(" ", table, tsubtotales, Color.WHITE, Color.WHITE, true);
            if (!this.getGlobales().equals("1")) {
                celda(" ", table, tsubtotales, Color.WHITE, Color.WHITE, true);
            }
            celda("$ " + Util.customFormat(total), table, tsubtotales, Color.WHITE, Color.WHITE, true);

            doc.add(table);

            return total;

        }
        catch (DocumentException ex){
            System.out.println("error pdf:"+ex.toString());
                ex.printStackTrace();
            next="Error escribiendo la tabla de "+titulo+" en el archivo";
            return 0;
        }
    }

    /**
     * Tabla que devuelve los totales de cada acci�n.
     *
     * @param doc       El documento donde se va a incluir la tabla (Variable tipo Document).
     * @param aeca      Es el bean AccionesEca el cual contiene la informacion de la accion.
     * @param results   Un Array de tipo entero que tiene los resultados de MANO DE OBRA, MATERIALES y OTROS de la accion.
     */
    public void tablaTotales(Document doc, AccionesEca aeca, double[] results) throws Exception{
        float[] widths  = {0.5f, 0.5f};
        double total = 0;

        PdfPTable table;
        table = new PdfPTable(widths);
        table.setWidthPercentage(100);

        double numero = Double.parseDouble(aeca.getUtilidad());

        celda("COSTOS DIRECTOS", table, tSubtitulo, Color.WHITE, Color.BLACK);
        celda(  Util.customFormat( results[0]+results[1]+results[2] ),
                table, tSubtitulo, Color.WHITE, Color.BLACK);

        //inicio de 20100223
        aeca.setAdministracion(""+(results[0]+results[1]+results[2])*Double.parseDouble(aeca.getPorc_administracion())/100.0);
        aeca.setImprevisto(""+(results[0]+results[1]+results[2])*Double.parseDouble(aeca.getPorc_imprevisto())/100.0);
        aeca.setUtilidad(""+(results[0]+results[1]+results[2])*Double.parseDouble(aeca.getPorc_utilidad())/100.0);
        //fin de 20100223

        if(numero > 0){
            celda("Administracion "+(int)Double.parseDouble(aeca.getPorc_administracion()), table, tSubtitulo, Color.WHITE, Color.WHITE);
            celda(  Util.customFormat( Double.parseDouble(aeca.getAdministracion())),
                    table, tSubtitulo, Color.WHITE, Color.WHITE);
            celda("Imprevistos "+(int)Double.parseDouble(aeca.getPorc_imprevisto())       , table, tSubtitulo, Color.WHITE, Color.WHITE);
            celda(  Util.customFormat( Double.parseDouble(aeca.getImprevisto())),
                    table, tSubtitulo, Color.WHITE, Color.WHITE);
            celda("Utilidad "+(int)Double.parseDouble(aeca.getPorc_utilidad())                 , table, tSubtitulo, Color.WHITE, Color.WHITE);
            celda(  Util.customFormat( Double.parseDouble(aeca.getUtilidad())),
                    table, tSubtitulo, Color.WHITE, Color.WHITE);
        }

        celda("IVA 19%", table, tSubtitulo, Color.WHITE, Color.BLACK);
        if((int)Double.parseDouble(aeca.getUtilidad()) > 0){
            celda(  Util.customFormat((Double.parseDouble(aeca.getUtilidad())*0.19)),
                    table, tSubtitulo, Color.WHITE, Color.BLACK);
        }
        else{
            celda(  Util.customFormat(((results[0]+results[1]+results[2])*0.19)),
                    table, tSubtitulo, Color.WHITE, Color.BLACK);
        }

        celda("VALOR TOTAL", table, tsubtotales, Color.WHITE, Color.BLACK);
        if((int)Double.parseDouble(aeca.getUtilidad()) > 0){
            total = ((results[0]+results[1]+results[2]) +
                    Double.parseDouble(aeca.getAdministracion()) +
                    Double.parseDouble(aeca.getImprevisto()) +
                    Double.parseDouble(aeca.getUtilidad()) +
                    ( Double.parseDouble(aeca.getUtilidad()) *0.19));

        }
        else{
            total = ((results[0]+results[1]+results[2]) + ((results[0]+results[1]+results[2])*0.19));
        }
            float inc = epd.getIncremento(df.format(dat));
            //total = total * inc;//20100223

            totalOffer += total;

            celda(  Util.customFormat(total), table, tsubtotales, Color.WHITE, Color.BLACK);
        try {
            doc.add(table);
        }
        catch (DocumentException ex) {
            System.out.println("error en pdf:"+ex.toString());
            ex.printStackTrace();
            next="Error escribiendo la tabla de totales en el archivo";
        }
    }

    /**
     * Este metodo arma la tabla de totales de todas las acciones.
     *
     * @param doc El documento donde se va a incluir la tabla (Variable tipo Document)
     */
    public void tablaTotalesGeneral(Document doc){
        float[] widths  = {1f};

        PdfPTable table;
        table = new PdfPTable(widths);
        table.setWidthPercentage(60);

        celda("TOTAL DE LA OFERTA", table, tTitulo, new Color(173, 216, 230));
        celda( "$ " + Util.customFormat(totalOffer) , table, tNormal, Color.WHITE);

        try {
            doc.add(table);
            doc.add(new Paragraph(" ", normal));

            try {
                doc.add(this.phtml("<strong>"+numToStr( (int)totalOffer )+ "<strong>"));
            }
            catch (Exception ex) {
                System.out.println("error pdf:"+ex.toString());
                ex.printStackTrace();
                doc.add(new Paragraph("EL NUMERO NO SERA MOSTRADO POR SER DEMASIADO GRANDE", normal));
            }
        }
        catch (Exception ex) {
            System.out.println("error pdf:"+ex.toString());
            ex.printStackTrace();
            next="Error escribiendo la tabla de totales generales en el archivo";
        }
    }

    /**
     * Este m�todo arma la tabla de firmas que esta a lo ultimo del documento PDF.
     *
     * @param doc El documento donde se va a incluir la tabla (Variable tipo Document)
     */
    public void tablaFirmas(Document doc){

        PdfPTable table;
        table = new PdfPTable(2);
        table.setWidthPercentage(100);

        celda(""+gerente[1], table, cTitulo, Color.WHITE, true, Element.ALIGN_LEFT);//20100702
        celda("_____________________________________", table, cTitulo, Color.WHITE, true, Element.ALIGN_LEFT);
        celda("C.C. No. "+gerente[0]+" de "+gerente[2]+"", table, cTitulo, Color.WHITE, true, Element.ALIGN_LEFT);//20100702
        celda("Doc No. "+epd.getOferta().getNIT()+"", table, cTitulo, Color.WHITE, true, Element.ALIGN_LEFT);
        celda("Representante", table, cTitulo, Color.WHITE, true, Element.ALIGN_LEFT);
        celda("Representante Legal ("+epd.getOferta().getRepresentante()+")", table, cTitulo, Color.WHITE, true, Element.ALIGN_LEFT);
        celda("", table, cTitulo, Color.WHITE, true, Element.ALIGN_LEFT);
        celda("Nombre Propio (_____________________________)", table, cTitulo, Color.WHITE, true, Element.ALIGN_LEFT);
        celda("", table, cTitulo, Color.WHITE, true, Element.ALIGN_LEFT);
        celda("", table, cTitulo, Color.WHITE, true, Element.ALIGN_LEFT);
        celda("MULTISERVICIOS FINTRA", table, cTitulo, Color.WHITE, true, Element.ALIGN_LEFT);
        celda("El CLIENTE", table, cTitulo, Color.WHITE, true, Element.ALIGN_LEFT);

        try {
            doc.add(table);
        }
        catch (DocumentException ex){
            System.out.println("error pdf:"+ex.toString());
            ex.printStackTrace();
            next="Error escribiendo la tabla de las firmas en el archivo";
        }
    }

    /**
     * Este metodo es para hacer celdas ya preparadas.
     *
     * @param img Esta es la imagen a colocar en la celda.
     * @param tab Tabla la cual va a recibir la celda.
     */
    public void celda(Image img, PdfPTable tab){
        PdfPCell cell;
        cell = new PdfPCell(img);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setBorderColor(Color.WHITE);
        tab.addCell(cell);
    }

    /**
     * Este metodo es para hacer celdas ya preparadas.
     *
     * @param val       Valor a colocar en la celda
     * @param tab       Tabla la cual va a recibir la celda.
     * @param font      Tipo de letra para la celda.
     * @param backg     Color de fondo de la celda.
     */
    public void celda(Object val, PdfPTable tab, Font font, Color backg){
        PdfPCell cell;
        Paragraph p = new Paragraph( String.valueOf(val), font);
        cell = new PdfPCell(p);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setBackgroundColor(backg);
        tab.addCell(cell);

    }

    /**
     * Este metodo es para hacer celdas ya preparadas.
     *
     * @param val       Valor a colocar en la celda
     * @param tab       Tabla la cual va a recibir la celda.
     * @param font      Tipo de letra para la celda.
     * @param backg     Color de fondo de la celda.
     * @param border    Color del borde de la celda.
     */
    public void celda(Object val, PdfPTable tab, Font font, Color backg, Color border){
        PdfPCell cell;
        Paragraph p = new Paragraph( String.valueOf(val), font);
        cell = new PdfPCell(p);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setBorderColor(border);
        cell.setBackgroundColor(backg);
        tab.addCell(cell);
    }

    /**
     * Este metodo es para hacer celdas ya preparadas.
     *
     * @param val       Valor a colocar en la celda.
     * @param tab       Tabla la cual va a recibir la celda.
     * @param font      Tipo de letra para la celda.
     * @param backg     Color de fondo de la celda.
     * @param disable   Deshabilita los bordes.
     * @param align     El alineamiento del texto en la celda.
     */
    public void celda(Object val, PdfPTable tab, Font font, Color backg, boolean disable, int align){
        PdfPCell cell;
        Paragraph p = new Paragraph( String.valueOf(val), font);
        cell = new PdfPCell(p);
        cell.setHorizontalAlignment(align);

        if (disable = true){
            cell.disableBorderSide(PdfPCell.BOTTOM);
            cell.disableBorderSide(PdfPCell.TOP);
            cell.disableBorderSide(PdfPCell.LEFT);
            cell.disableBorderSide(PdfPCell.RIGHT);
        }

        cell.setBackgroundColor(backg);
        tab.addCell(cell);
    }

    /**
     * Este metodo es para hacer celdas ya preparadas.
     *
     * @param val       Valor a colocar en la celda.
     * @param tab       Tabla la cual va a recibir la celda.
     * @param font      Tipo de letra para la celda.
     * @param backg     Color de fondo de la celda.
     * @param border    Color de los bordes.
     * @param disable   Deshabilita los bordes.
     */
    public void celda(Object val, PdfPTable tab, Font font, Color backg, Color border, boolean disable){
        PdfPCell cell;
        Paragraph p = new Paragraph( String.valueOf(val), font);
        cell = new PdfPCell(p);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);

        if (disable = true){
            cell.disableBorderSide(PdfPCell.BOTTOM);
            cell.disableBorderSide(PdfPCell.TOP);
            cell.disableBorderSide(PdfPCell.LEFT);
            cell.disableBorderSide(PdfPCell.RIGHT);
        }

        cell.setBorderColor(border);
        cell.setBackgroundColor(backg);
        tab.addCell(cell);
    }

    /**
     * Este metodo es para hacer celdas ya preparadas.
     *
     * @param val       Valor a colocar en la celda.
     * @param tab       Tabla la cual va a recibir la celda.
     * @param font      Tipo de letra para la celda.
     * @param backg     Color de fondo de la celda.
     * @param border    Color de los bordes.
     * @param Colspan   Ancho de la celda en columnas.
     */
    public void celda(Object val, PdfPTable tab, Font font, Color backg, Color border, int Colspan){
        PdfPCell cell;
        Paragraph p = new Paragraph( String.valueOf(val), font);
        cell = new PdfPCell(p);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setBorderColor(border);
        cell.setColspan(Colspan);
        cell.setBackgroundColor(backg);
        tab.addCell(cell);
    }

    /**
     * Este metodo es para hacer celdas ya preparadas.
     *
     * @param val       Valor a colocar en la celda.
     * @param tab       Tabla la cual va a recibir la celda.
     * @param font      Tipo de letra para la celda.
     * @param backg     Color de fondo de la celda.
     * @param border    Color de los bordes.
     * @param rowspan   Ancho de la celda en columnas.
     */
    public void celda( int rowspan,Object val, PdfPTable tab, Font font, Color backg, Color border){
        PdfPCell cell;
        Paragraph p = new Paragraph( String.valueOf(val), font);
        cell = new PdfPCell(p);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setBorderColor(border);
        cell.setColspan(rowspan);
        cell.setBackgroundColor(backg);
        tab.addCell(cell);
    }

    /**
     *
     * @param doc   El documento donde se va a incluir la tabla (Variable tipo Document).
     * @param val   Texto que se va a introducir en el parrafo.
     * @param align Alineamiento del texto.
     * @param letra Tipo de letra a utilizar en el texto.
     */
    public void parrafo(Document doc, String val, int align, Font letra){
        try {
            Paragraph p = new Paragraph(val, letra);
            p.setAlignment(align);
            document.add(p);
        }
        catch (DocumentException documentException) {
            System.out.println("error pdf:"+documentException.toString());
            documentException.printStackTrace();
            next="Error escribiendo un p�rrafo en el archivo";
        }
    }

    /**
     * Este metodo recoje un entero, le elimina los espacios en blanco
     * y me devuelve el numero en letras.
     *
     * @param val   Numero a convertir.
     * @return      Cadena con el numero convertido en palabras.
     */
    public String numToStr(int val){
        String[] texto = RMCantidadEnLetras.getTexto(val);
        String res = "";

        for (int i = 0; i < texto.length; i++) {
            res += texto[i]+" ";
        }

        return res.trim();
    }

    /**
     * Este metodo recoje un entero, le elimina los espacios en blanco
     * y me devuelve el numero en letras.
     *
     * @param val   Numero a convertir.
     * @return      Cadena con el numero convertido en palabras.
     */
    public String numToStr(double val){
        String[] texto = RMCantidadEnLetras.getTexto(val);
        String res = "";

        for (int i = 0; i < texto.length; i++) {
            res += texto[i]+" ";
        }

        return res.trim();
    }

    /**
     * Este metodo cierra el documento principal donde
     * se esta ingresando la informacion.
     */
    public void cerrar(){
        document.close();
    }

    /* Los procedimientos siguientes son para
     * la actualizacion de las ofertas
     */

    public String getMesesOficial() throws Exception{
        return epd.getMesesOficial();
    }

    /**
     * Este metodo me actualiza la informacion principal
     * de la oferta. Antes de actualizar se revisa si se
     * tiene las acciones en estado 040.
     *
     * @param oeca Es el bean de la oferta a modificar.
     * @param flag Indica si se va a reemplazar el consecutivo.
     */
    public void actualizarOferta(Usuario u, OfertaElca oelca,int caso, String tipo){
        try {
            epd.actualizarOferta(u.getLogin(), oelca,caso, tipo);
            next="Oferta generada exitosamente";
            /*ClientesVerService cldao = new ClientesVerService();
            next = next + cldao.updateOf(oelca, u.getLogin());*/
        }
        catch (Exception ex) {
            System.out.println("error pdf:"+ex.toString());
            ex.printStackTrace();
            next="Error generando la oferta";
        }
    }

    /**
     * Este metodo indica si la solicitud esta
     * preparada para generarse como oferta
     *
     * @param id_sol    ID de la oferta.
     * @return          Devuelve un bean tipo OfertaEca.
     */
    public boolean isOfferReady(String id_acc) throws Exception{
        return epd.isOfferReady(id_acc);
    }

     /**
     * Este metodo devuelve el estado minimo de las acciones de la oferta
     *
     *
     * @param id_sol    ID de la oferta.
     * @return          Devuelve un int con el estado.
     */
    public int minEstado(String id_sol) throws Exception{
        return epd.minEstado(id_sol);
    }

     /**
     * Este metodo reevalua la oferta
     *
     *
     * @param id_sol    ID de la oferta.
     * @return          Devuelve un boolean que indica si fue reevaluda correctamente.
     */
    public boolean reevaluarOferta(String id_sol, String login) throws Exception{
        return epd.revaluarOferta(id_sol, login);
    }

    /**
     * Este metodo devuelve el estado maximo de las acciones de la oferta
     *
     *
     * @param id_sol    ID de la oferta.
     * @return          Devuelve un int con el estado.
     */
    public int maxEstado(String id_sol) throws Exception{
        return epd.maxEstado(id_sol);
    }

        /**
     * Este metodo devuelve un arrayList con las ofertas que se van a denegar automaticamente
     *
     * @param num   validez de la oferta.
     * @return      Devuelve un arrayList con las ofertas que se van a denegar automaticamente
     */
    public ArrayList ofertaDenegacion(int num) throws Exception{
        return epd.ofertaDenegacion(num);
    }

    /**
     * Este metodo devuelve un entero con el numero de dias en que es valida una oferta
     *
     * @return      Devuelve un int con el numero de dias en que es valida una oferta
     */
    public int vaidezOferta() throws Exception{
        return epd.vaidezOferta();
    }

    public String getGlobales() {
        return globales;
    }

    public void setGlobales(String globales) {
        this.globales = globales;
    }

    /**
     * Este metodo devuelve un bean con la info
     * de la oferta deseada.
     *
     * @param num   Consecutivo de la oferta.
     * @return      Devuelve un bean tipo OfertaEca
     */
    public DatosOferta ofertaInfo(String num) throws Exception{
        epd.setNumOferta(num);
        epd.getEcaInfo();
        return epd.getOferta();
    }
    
     /**
     * Este metodo devuelve si el cliente es un edificio.
     *
     * @param num   Consecutivo de la oferta.
     * @return      Devuelve S o N
     */
    public String validarEdificio(String num) throws Exception{
        
        return epd.obtenerTipoCliente(num);
    }


    /**
     * Este metodo devuelve un String con la ruta
     * a la cual la pagina va a ser destinada automaticamente.
     *
     * @return String con la ruta proxima.
     */
    public String returnNext(){
        return next;
    }

    /**
     * Este metodo devuelve todas las consideraciones disponibles
     *
     * @return ArrayList con las consideraciones en beans.
     * @throws Exception
     */
    public ArrayList<TablaGen> getConsideracionesItems() throws Exception{
        return epd.getConsideraciones();
    }

    /**
     * Este metodo es para devolver las
     * consideraciones por oferta
     *
     * @return ArrayList con las consideraciones de las ofertas
     * @throws Exception
     */
    public ArrayList<TablaGen> getConsideracionesByOffer() throws Exception{
        return epd.getConsideracionesByOffer();
    }


    public String actualizarAnulacionOferta(OfertaElca oelca, boolean flag, String userx){
        String respuesta="Denegacion pendiente...";
        try {
            ////.out.println("antes isAnulable ");
            if(epd.isAnulable(oelca.getId_solicitud())){
                respuesta=epd.actualizarAnulacionOferta(oelca, flag,userx);
                if(respuesta.equals("Oferta denegada.")){//2010-05-10 rhonalf
                    respuesta="1";
                    ClientesVerService clsrv = new ClientesVerService(this.epd.getDatabaseName());
                    // 2010-06-15 rhonalf
                    oelca.setOficial("0");
                    oelca.setEstado_cartera("000");
                    oelca.setFec_val_cartera("0099-01-01 00:00:00");
                    oelca.setTipo_solicitud("Programado");
                    // 2010-06-15 rhonalf end
                    oelca.setDescripcion(" (" + userx + "): " + oelca.getOtras_consideraciones());
                    // 2010-06-15 rhonalf
                    try {
                        respuesta = respuesta + clsrv.updateOf2(oelca,userx);
                    }
                    catch (Exception e) {
                        respuesta = respuesta + "\n" + "Ocurrio un error al actualizar los datos de la solicitud "+oelca.getId_solicitud();
                        System.out.println("Ocurrio un error al actualizar los datos de la solicitud "+oelca.getId_solicitud());
                        e.printStackTrace();
                    }
                    // 2010-06-15 rhonalf end
                    try {//2010-06-10 rhonalf
                        respuesta = respuesta + this.mailDenegacion(oelca, oelca.getId_solicitud());
                    }
                    catch (Exception e) {
                        respuesta = respuesta + "\n" + "No se pudo enviar correo de denegacion de la solicitud "+oelca.getId_solicitud();
                        System.out.println("No se pudo enviar correo de denegacion de la solicitud "+oelca.getId_solicitud()+" : "+e.toString());
                        e.printStackTrace();
                    }
                }else{
                    respuesta="No se pudo denegar la solicitud "+oelca.getId_solicitud();
                }
            }else{
                respuesta="Las acciones no estan en estado para denegar.";//091222
            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("error anulando la oferta___"+ex.toString());
        }
        return respuesta;
    }



    public DatosOferta ofertaInfoAnul(String num) throws Exception{

        return epd.ofertaInfoAnul(num);
    }

    public ArrayList<TablaGen> getAnulacionesItems(String num_of) throws Exception{
        return epd.getAnulaciones(num_of);
    }
     public ArrayList<TablaGen> getAnulacionesItems() throws Exception{
        return epd.getAnulaciones();
    }

    /**
     * Envia un mail al ejecutivo de cuenta de la solicitud
     * @param ofca Objeto OfertaElca con informacion de la oferta
     * @param solicitud El id de la solicitud
     * @return String con la confirmacion de envio del mensaje
     * @throws Exception Cuando hay error
     * @since 2010-05-10 rhonalf
     */
    public String mailDenegacion(OfertaElca ofca,String solicitud) throws Exception{
        String mensaje="";
        try {
            String from = epd.mailFrom();
            String copyto = epd.mailTo();
            String mailejec = epd.mailEjecutivo(solicitud);
            String nomcli="";
            ClientesVerService csr = new ClientesVerService(epd.getDatabaseName());
            Cliente cl = new Cliente();
            if(!(csr.buscarOf(ofca).equals("No hay ninguna solicitud con ese id"))){
                cl.setCodcli(ofca.getId_cliente());
                 String resp = csr.buscarCl(cl);
                if(resp.equals("")){
                    nomcli = cl.getNomcli();
                }
                cl=null;
                csr = null;
                String consideraciones = "";
                ArrayList ofertas = this.getAnulacionesItems(solicitud);
                for(int i = 0; i < ofertas.size(); i++){
                    TablaGen oferta=(TablaGen)ofertas.get(i);
                    if(oferta.getDato().equals("true")){
                        consideraciones = consideraciones + "\n" + oferta.getDescripcion();
                    }
                }
                String observaciones = ofca.getOtras_consideraciones();
                String num=epd.getOpdAviso(solicitud);
                String bodymail = "Ha sido denegada la solicitud "+solicitud+" con "+num+" del cliente "+nomcli+" por la(s) siguiente(s) causa(s) :"+consideraciones+".";
                if(!(observaciones.equals(""))){
                    bodymail = bodymail + "\n\nObservaciones:\n" + observaciones;
                }
                bodymail = bodymail + "\n\nAtentamente,\n\nFintra S.A\nSoporte t\u00e9cnico";
                Email2 mail = new Email2();
                mail.setEmailfrom(from);
                mail.setEmailto(mailejec);//mailejec
                mail.setEmailcopyto(copyto);//copyto
                mail.setEmailHiddencopyto(from);
                //mail.setEmailsubject("Denegaci\u00f3n de solicitud: "+solicitud);
                mail.setEmailsubject("Denegaci\u00f3n de solicitud: "+nomcli);
                mail.setEmailbody(bodymail);
                EmailSendingEngineService mailserv = new EmailSendingEngineService();
                mailserv.send(mail);
            }
            else{
                mensaje = "No se pudo enviar el correo al ejecutivo de cuenta ... no se encontraron datos para la solicitud";
            }
        }
        catch (Exception e) {
            mensaje = "No se pudo enviar el correo al ejecutivo de cuenta";
            throw new Exception("Error al enviar un mail al ejecutivo de cuenta de la solicitud "+solicitud+" : "+e.toString());
        }
        return mensaje;
    }

    public String actualizarCambioClienteSol(String id_cliente,String id_sol,String nic) throws Exception{
        return epd.actualizarCambioClienteSol(id_cliente,id_sol,nic);
    }

    /**
 *
 * @param id_sol
 * @return
 * @throws Exception
 * JJCASTRO EMERGENCIA
 */
    public String tipoOferta(String id_sol) throws Exception{
        return epd.tipoOferta(id_sol);
    }





   public String GetEmpresa(String tipo) throws Exception{
        String e="ELECTRICARIBE S.A. E.S.P. (Intervenida por Superservicios)";

//Se comenta esta parte por solicitud del sr Antonio Melendez. A partir de ahora todas las ofertas seran Electricaribe.
//           if(epd.getOferta().getTipo_cliente().equals("NR") )
//           {
//             e="ENERGIA EMPRESARIAL DE LA COSTA S.A. E.S.P.";
//           }


       return e;
    }

    public String getConsecutivoOferta() throws Exception {
        return epd.getConsecutivoOferta();
    }

    public String actualizarOferta(OfertaElca oferta) throws Exception {
        return epd.actualizarOferta(oferta);
    }

    public String actPrecio_venta(String id_solicitud, String fecha) throws Exception {
        return epd.actPrecio_venta(id_solicitud, fecha);
    }

     
}

