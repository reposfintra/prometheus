/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.opav.model.services;
import java.util.*;
import com.tsp.opav.model.beans.*;
import com.tsp.opav.model.DAOS.*;

/**
 *
 * @author jose
 */
public class InterventorService {

    private InterventorDAO idao;

    public InterventorService() {
        idao = new InterventorDAO();
    }
    public InterventorService(String dataBaseName) {
        idao = new InterventorDAO(dataBaseName);
    }



/**
 *
 * @return
 * @throws Exception
 */
    public ArrayList listadoInterventores() throws Exception {
        ArrayList listinter = new ArrayList();
        try{
            listinter = idao.listadoInterventores();
            if(listinter.size()<0) throw new Exception("No hay valores para la busqueda solicitada");
        }
        catch(Exception e){
            e.printStackTrace();
        }
        return listinter;
    }



/**
 *
 * @return
 * @throws Exception
 */
    public ArrayList listadoDepartamentos(String cod_interventor, String opcion) throws Exception {
        ArrayList listdepartamentos = new ArrayList();
        try{
            listdepartamentos = idao.listadoDepartamentos(cod_interventor, opcion);
            if(listdepartamentos.size()<0) throw new Exception("No hay valores para la busqueda solicitada");
        }
        catch(Exception e){
            e.printStackTrace();
        }
        return listdepartamentos;
    }


/**
 *
 * @param listadoDepartamentos
 * @param cod_inter
 * @param login
 * @throws Exception
 */
    public void insertarRelacionInterventor(ArrayList listadoDepartamentos,  String cod_inter, String login) throws Exception {
        try {
            idao.insertarRelacionInterventor(listadoDepartamentos,  cod_inter, login);
        } catch (Exception e) {
            System.out.println("Error en insertar producto: " + e.toString());
        }

    }


/**
 *
 * @return
 * @throws Exception
 */
    public ArrayList listadoSolicitudes(String cod_interventor, String opcion) throws Exception {
        ArrayList listadoSolicitudes = new ArrayList();
        try{
            listadoSolicitudes = idao.listadoSolicitudes(cod_interventor, opcion);
            if(listadoSolicitudes.size()<0) throw new Exception("No hay valores para la busqueda solicitada");
        }
        catch(Exception e){
            e.printStackTrace();
        }
        return listadoSolicitudes;
    }



/**
 *
 * @param listadoDepartamentos
 * @param cod_inter
 * @param login
 * @throws Exception
 */
    public void insertarSolicitudInterventor(ArrayList listadoDepartamentos,  String cod_inter, String login) throws Exception {
        try {
            idao.insertarSolicitudInterventor(listadoDepartamentos,  cod_inter, login);
        } catch (Exception e) {
            System.out.println("Error en insertar producto: " + e.toString());
        }

    }

    public String obtenerInterventor(String id_solicitud,String tipo_accion) throws Exception {
        return idao.obtenerInterventor(id_solicitud, tipo_accion);
    }


    public void updateOfertasInteventor(String id_solicitud, String interventor, String login) throws Exception {
        this.idao.updateOfertasInteventor(id_solicitud, interventor, login);
    }




}