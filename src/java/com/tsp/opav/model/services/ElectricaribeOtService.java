package com.tsp.opav.model.services;

import com.tsp.opav.model.DAOS.ElectricaribeOtDAO;
import com.tsp.opav.model.beans.*;
import com.tsp.util.Util;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.*;
import com.tsp.opav.model.beans.OTPageEvent;

import java.awt.Color;
import java.util.*;
import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import com.tsp.operation.model.beans.BeanGeneral;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.tsp.opav.model.DAOS.NegociosApplusDAO;


public class ElectricaribeOtService{

    private BaseFont bf;

    private Font mTitulo    = new Font(bf, 14, Font.BOLD);
    private Font mSubtitulo = new Font(bf, 14, Font.BOLDITALIC);
    private Font mNormal    = new Font(bf, 12);

    //Estos son los tipos de letras para parrafos normales
    private Font titulo     = new Font(bf, 10, Font.BOLD);
    private Font subtitulo  = new Font(bf, 10, Font.BOLDITALIC);
    private Font normal     = new Font(bf, 9);

    //Letras para tablas
    private Font tTitulo    = new Font(bf, 10, Font.BOLD);
    private Font tSubtitulo = new Font(bf, 10, Font.BOLDITALIC);
    private Font tNormal    = new Font(bf, 9);

    //Letras para la parte de los anexos
    private Font cTitulo    = new Font(bf, 8, Font.BOLD);
    private Font cNormal    = new Font(bf, 8);

    private ElectricaribeOtDAO ElectOTDao;
    private DatosOferta oe;
    private OrdenTrabajo ot;

    private Document document;
    private PdfWriter writer;
    private ResourceBundle rb;
    private String ruta;
    private String next;

    private DateFormat df;
    private Date date;

    private ArrayList<Contratista> conts;
    private ArrayList<AccionesEca> aecas;

    private OTPageEvent otpe;

    private String idsol = "";
    
    private String tipo_sol="";

    public ElectricaribeOtService(){
        ElectOTDao = new ElectricaribeOtDAO();

        df   = new SimpleDateFormat("yyyyMMddHHmmss");
        date = new Date();
    }
    public ElectricaribeOtService(String dataBaseName){
        ElectOTDao = new ElectricaribeOtDAO(dataBaseName);

        df   = new SimpleDateFormat("yyyyMMddHHmmss");
        date = new Date();
    }

    public ArrayList getHijosList(String id) throws Exception{
        return ElectOTDao.getHijosList(id);
    }

    public ArrayList getNics(String id) throws Exception{
        return ElectOTDao.getNics(id);
    }

    public void financiacion(String fecha, String id) throws Exception{
        ElectOTDao.financiacion(fecha, id);
    }

    public void insertPayments(ArrayList payments, String user) throws Exception{
        ElectOTDao.insertPayments(payments, user);
    }

    public ArrayList<SubclienteEca> getPayments(String id) throws Exception{
        return ElectOTDao.getPayments(id);
    }

    /* Estos metodos que vienen a continuacion
     * son para la insercion de la orden de
     * trabajo.
     */

    public void setBeans(String id_sol) throws Exception{
        oe = ElectOTDao.setEca(id_sol);
        ot = ElectOTDao.setOT(id_sol);
    }

    public boolean isOtReady(String id_sol) throws Exception{
        boolean ok = true;

        if (!ElectOTDao.isOtReady(id_sol)) {
            next = "La oferta debe estar en estado 60 o mayor";
            ok = false;
        }

        return ok;
    }

    public boolean isOtReadyForPDF(String id_sol) throws Exception{
        return ElectOTDao.isOtReadyForPDF(id_sol);
    }

    public boolean isPaymentReady(String id_sol) throws Exception{
        boolean ok = true;

        if (!ElectOTDao.isPaymentReady(id_sol)) {
            next = "La oferta debe estar en estado 50 o mayor";
            ok = false;
        }

        return ok;
    }

    public void insertarOT(OrdenTrabajo ote, boolean flag, String usuario) throws Exception{
            ElectOTDao.ingresarOT(ote, flag, usuario);
    }
    
    public void insertarCotizacionOrdenCompra(OrdenTrabajo informacion, String usuario) throws Exception{
            ElectOTDao.insertarCotizacionOrdenCompra(informacion, usuario);
    }

    
    /* Las funciones a continuacion
     * sirven para hacer el PDF de la
     * orden de trabajo
     */

    public void doPDF(String user, String id_sol){

        idsol = id_sol;
        NegociosApplusDAO negdao= new NegociosApplusDAO(this.ElectOTDao.getDatabaseName());  
        try {
            tipo_sol = negdao.getDescTipoSolc(idsol);
        } catch (Exception ex) {
            Logger.getLogger(ElectricaribeOtService.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {

            for(int i=0; i<conts.size(); i++){
            rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            this.ruta = rb.getString("ruta") + "/exportar/migracion/" + user + "/" + ElectOTDao.getStuffOT(idsol, "id_orden") + " - " + df.format(date) + i+".pdf";//JJCASTRO PDF OT

            File dir = new File(rb.getString("ruta") + "/exportar/migracion/"+user);
            dir.mkdir();

            document = new Document(PageSize.LETTER, 110, 70, 140, 70);
            writer = PdfWriter.getInstance(document, new FileOutputStream(this.ruta));

            otpe = new OTPageEvent(writer, document, rb.getString("ruta"),    tipo_sol.equals("AAAE")?true:false);
            writer.setPageEvent(otpe);

            PDFContent(document, i);//JJCASTRO PDF OT
          }


        }
        catch (Exception ex) {
            next = "Error, generando el PDF";
        }
    }

    public void PDFContent(Document doc, int i) throws Exception{
        doc.open();
        
        doc.add(new Paragraph(" "));

//        for(int i=0; i<conts.size(); i++){
            float[] widths = {0.5f, 0.5f};
            PdfPTable header;
            header = new PdfPTable(widths);
            header.setWidthPercentage(100);

            aecas = ElectOTDao.getActions( ElectOTDao.getStuffOT(idsol, "id_oferta"), conts.get(i).getId_contratista() );
            
            tablaCabecera(doc);
            doc.add(new Paragraph(" ", tTitulo));
            tablaContratista(doc, conts.get(i).getNombre_contratista(), ElectOTDao.getStuffOT(idsol, "departamento"));

            if(aecas.size()>0){
                doc.add(new Paragraph(" ", tTitulo));
                doc.add(new Paragraph("ACCIONES", tTitulo));

                for(int j=0; j<aecas.size(); j++){
                    doc.add(new Paragraph("   - "+aecas.get(j).getDescripcion(), tNormal));
                }

                doc.add(new Paragraph(" ", tTitulo));
                doc.add(new Paragraph(" ", tTitulo));
                doc.add(new Paragraph("COSTOS", tTitulo));
                doc.add(new Paragraph((!tipo_sol.equals("AAAE")?"   Materiales:       $ ":("   Equipos:       $ ")) + Util.customFormat( (int)ElectOTDao.getValor(conts.get(i).getId_contratista(), ElectOTDao.getStuffOT(idsol, "id_oferta"), 1) ), tNormal));
                doc.add(new Paragraph("   Mano de obra:     $ " + Util.customFormat( (int)ElectOTDao.getValor(conts.get(i).getId_contratista(), ElectOTDao.getStuffOT(idsol, "id_oferta"), 2) ), tNormal));
                doc.add(new Paragraph("   Administraci�n:   $ " + Util.customFormat( (int)ElectOTDao.getValor(conts.get(i).getId_contratista(), ElectOTDao.getStuffOT(idsol, "id_oferta"), 4) ), tNormal));//JJCASTRO PDF OT
                doc.add(new Paragraph("   Imprevisto:       $ " + Util.customFormat( (int)ElectOTDao.getValor(conts.get(i).getId_contratista(), ElectOTDao.getStuffOT(idsol, "id_oferta"), 5) ), tNormal));//JJCASTRO PDF OT
                doc.add(new Paragraph("   Utilidad:         $ " + Util.customFormat( (int)ElectOTDao.getValor(conts.get(i).getId_contratista(), ElectOTDao.getStuffOT(idsol, "id_oferta"), 6) ), tNormal));//JJCASTRO PDF OT
                doc.add(new Paragraph("   Otros:            $ " + Util.customFormat( (int)ElectOTDao.getValor(conts.get(i).getId_contratista(), ElectOTDao.getStuffOT(idsol, "id_oferta"), 3) ), tNormal));
                doc.add(new Paragraph(" ", tTitulo));
                doc.add(new Paragraph(" ", tTitulo));
            }

            tablaCliente(doc);

           if (!tipo_sol.equals("AAAE")) {
            doc.newPage();

            anexo(doc);

            doc.newPage();
        }
//        }

        doc.close();
    }

    public void tablaCabecera(Document doc) throws Exception{
        try {
            float[] widths = {0.15f, 0.35f, 0.25f, 0.25f};
            PdfPTable table = new PdfPTable(widths);
            table.setWidthPercentage(100);
            PdfPCell cell;

            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");

            celda("Orden de trabajo", table, tTitulo, BaseColor.WHITE, BaseColor.BLACK);
            celda(ElectOTDao.getStuffOT(idsol, "id_orden") , table, tNormal, BaseColor.WHITE, BaseColor.BLACK);
            celda("Fecha"           , table, tTitulo, BaseColor.WHITE, BaseColor.BLACK);
            celda(dateFormat.format(date), table, tNormal, BaseColor.WHITE, BaseColor.BLACK);
            celda("Consecutivo Oferta", table, tTitulo, BaseColor.WHITE, BaseColor.BLACK, 2);
            celda(ElectOTDao.getStuffOT(idsol, "consecutivo_oferta"), table, tTitulo, BaseColor.WHITE, BaseColor.BLACK, 2);
            if( ElectOTDao.getStuffOT(idsol, "tipo_solicitud").equals("Emergencia") ){
                celda("No. Aviso", table, tTitulo, BaseColor.WHITE, BaseColor.BLACK, 2);
                celda(  ((ElectOTDao.getStuffOT(idsol, "aviso")!=null)?ElectOTDao.getStuffOT(idsol, "aviso"):"No presenta")  , table, tTitulo, BaseColor.WHITE, BaseColor.BLACK, 2);
            }

            doc.add(table);
        }
        catch (DocumentException ex) {
        }
    }

    public void tablaContratista(Document doc, String con, String zon) throws Exception{
        try {
            float[] widths = {0.15f, 0.45f, 0.20f, 0.20f};
            PdfPTable table = new PdfPTable(widths);
            table.setWidthPercentage(100);
            PdfPCell cell;

            celda("Contratista",    table, cTitulo, BaseColor.WHITE, BaseColor.BLACK);
            celda(con,              table, cNormal, BaseColor.WHITE, BaseColor.BLACK);
            celda("Zona",           table, cTitulo, BaseColor.WHITE, BaseColor.BLACK);
            celda(zon,              table, cNormal, BaseColor.WHITE, BaseColor.BLACK);
            celda("Descripcion", table, tTitulo, BaseColor.WHITE, BaseColor.BLACK, 1);
            if (!tipo_sol.equals("AAAE")) {
                celda(ElectOTDao.getStuffOT(idsol, "nombre_solicitud"), table, tTitulo, BaseColor.WHITE, BaseColor.BLACK, 3);
            }else{
                String desc="";
                 for(int j=0; j<aecas.size(); j++){
                    desc+=aecas.get(j).getObservaciones()+"\n";
                }
                celda(desc, table, cNormal, BaseColor.WHITE, BaseColor.BLACK, 3);
            }


            doc.add(table);
        }
        catch (DocumentException ex) {
        }
    }

    public void tablaCliente(Document doc){
        try {
            float[]     widths = {0.15f, 0.35f, 0.25f, 0.25f};
            PdfPTable   table = new PdfPTable(widths);
            table.setWidthPercentage(100);
            PdfPCell    cell;

            celda("NIC",                table, cTitulo, BaseColor.WHITE, BaseColor.BLACK);  celda(oe.getNIC() ,             table, cNormal, BaseColor.WHITE, BaseColor.BLACK);
            celda("TIPO CLIENTE",       table, cTitulo, BaseColor.WHITE, BaseColor.BLACK);  celda(oe.getTipo_cliente(),     table, cNormal, BaseColor.WHITE, BaseColor.BLACK);
            celda("CLIENTE",            table, cTitulo, BaseColor.WHITE, BaseColor.BLACK);  celda(oe.getCliente(),          table, cNormal, BaseColor.WHITE, BaseColor.BLACK);
            celda("TELEFONO",           table, cTitulo, BaseColor.WHITE, BaseColor.BLACK);  celda(oe.getTelefono(),         table, cNormal, BaseColor.WHITE, BaseColor.BLACK);

            celda("CELULAR",            table, cTitulo, BaseColor.WHITE, BaseColor.BLACK);  celda(oe.getCelular(),          table, cNormal, BaseColor.WHITE, BaseColor.BLACK);
            celda("DIRECCION",          table, cTitulo, BaseColor.WHITE, BaseColor.BLACK);  celda(oe.getDireccion(),        table, cNormal, BaseColor.WHITE, BaseColor.BLACK);
            celda("EJECUTIVO",          table, cTitulo, BaseColor.WHITE, BaseColor.BLACK);  celda(oe.getEjecutivo() ,       table, cNormal, BaseColor.WHITE, BaseColor.BLACK);
            celda("CONTACTO",                  table, cTitulo, BaseColor.WHITE, BaseColor.BLACK);//JJCASTRO PDF OT
           if (!tipo_sol.equals("AAAE")) {
                celda(oe.getNombre_contacto(), table, cNormal, BaseColor.WHITE, BaseColor.BLACK);//JJCASTRO PDF OT
            } else {
                celda(oe.getResponsable(), table, cNormal, BaseColor.WHITE, BaseColor.BLACK);
            }
/*
             if (!tipo_sol.equals("AAAE")) {
                celda("ELABORO: Cristina Botero (cbotero@fintra.co)", table, cTitulo, BaseColor.WHITE, BaseColor.BLACK, 4);
            }else{
*/               celda("ELABORO: Sullivan Robinson Altamar (srobinson@fintra.co)", table, cTitulo, BaseColor.WHITE, BaseColor.BLACK, 4);
//            }


            doc.add(table);

            doc.add(new Paragraph("", tTitulo));
            doc.add(new Paragraph("OBSERVACIONES: Una vez recibidos los trabajos a satisfacci�n por parte del cliente, digitalizarla y subirla al Aplicativo de Multiservicios (APMS) del OPAV, para darle el respectivo Visto Bueno y proceder a darle recepci�n de Obra.", tTitulo));
        }
        catch (DocumentException ex) {
        }
    }

    public void anexo(Document doc) throws Exception{
        doc.add(new Paragraph("ANEXO", mTitulo));
        doc.add(new Paragraph(" ", tTitulo));

        doc.add(new Paragraph("Para todos los trabajos, tanto proyectos como mantenimientos y emergencias deben cumplir con ciertas pruebas de puesta en marcha.", tNormal));
        doc.add(new Paragraph(" ", tTitulo));
        doc.add(new Paragraph("Proyectos y/o trabajos programados:", tTitulo));
        doc.add(new Paragraph("     1.  Entregar cronograma de obra aprobado por el cliente.", tNormal));
        doc.add(new Paragraph("     2.  Llevar bit�cora de obra en sitio para revisi�n y supervisi�n del proyecto.", tNormal));
        doc.add(new Paragraph("     3.  Entrega con informe final, con todos los esquemas, planos AS BUILT y documentaci�n (manuales de operaci�n, cat�logos y certificados).", tNormal));
        doc.add(new Paragraph("     4.  En caso de suministro e instalaci�n de tableros el�ctricos se deben tomar las medidas de los existentes y los equipos a instalar, para evitar inconvenientes en el momento del montaje; estos deben llevar su diagrama unifilar de instalaci�n y procedimiento cuando este se requiera Ej. Operaci�n de transferencia autom�tica.", tNormal));
        doc.add(new Paragraph("     5.  Los tableros el�ctricos deber�n tener instalados barreras de protecci�n (acr�licos) para evitar contactos involuntarios y contar con se�alizaci�n (marcaci�n de prevenci�n y secuencia de fases y numeraci�n de cableado) como lo dice la  norma NTC2050.", tNormal));
        doc.add(new Paragraph("     6.  Pruebas mec�nicas de funcionamiento.", tNormal));
        doc.add(new Paragraph("     7.  Probar y ajustar todas las protecciones realizando las pruebas de rutina seg�n la norma.", tNormal));
        doc.add(new Paragraph("     8.  Todas las pruebas mec�nicas y el�ctricas las deben realizar en presencia del personal del CONSORCIO DE MULTISERVICIOS.", tNormal));
        doc.add(new Paragraph("     9.  En caso de suministrar e instalar un transformador de aceite se exigir� la construcci�n de un foso cortafuego para recogida del aceite.", tNormal));
        doc.add(new Paragraph("     10. Las puestas a tierra deben cumplir con la norma IEEE y el RETIE.", tNormal));
        doc.add(new Paragraph("     11. Todos los materiales suministrados deben de estar homologados por le CIDET y la ejecuci�n de la obra deben cumplir con la norma NTC y el RETIE.", tNormal));
    }

    public void getContratistas(String id_sol) throws Exception{
        conts = ElectOTDao.getContratistas(id_sol);
    }

    /**
     * Este metodo es para hacer celdas ya preparadas.
     *
     * @param val       Valor a colocar en la celda
     * @param tab       Tabla la cual va a recibir la celda.
     * @param font      Tipo de letra para la celda.
     * @param backg     Color de fondo de la celda.
     */
    public void celda(Object val, PdfPTable tab, Font font, BaseColor backg){
        PdfPCell cell;
        Paragraph p = new Paragraph( String.valueOf(val), font);
        cell = new PdfPCell(p);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setBackgroundColor(backg);
        tab.addCell(cell);
    }

    /**
     * Este metodo es para hacer celdas ya preparadas.
     *
     * @param val       Valor a colocar en la celda
     * @param tab       Tabla la cual va a recibir la celda.
     * @param font      Tipo de letra para la celda.
     * @param backg     Color de fondo de la celda.
     * @param border    Color del borde de la celda.
     */
    public void celda(Object val, PdfPTable tab, Font font, BaseColor backg, BaseColor border){
        PdfPCell cell;
        Paragraph p = new Paragraph( String.valueOf(val), font);
        cell = new PdfPCell(p);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setBorderColor(border);
        cell.setBackgroundColor(backg);
        tab.addCell(cell);
    }

    /**
     * Este metodo es para hacer celdas ya preparadas.
     *
     * @param val       Valor a colocar en la celda.
     * @param tab       Tabla la cual va a recibir la celda.
     * @param font      Tipo de letra para la celda.
     * @param backg     Color de fondo de la celda.
     * @param disable   Deshabilita los bordes.
     * @param align     El alineamiento del texto en la celda.
     */
    public void celda(Object val, PdfPTable tab, Font font, BaseColor backg, boolean disable, int align){
        PdfPCell cell;
        Paragraph p = new Paragraph( String.valueOf(val), font);
        cell = new PdfPCell(p);
        cell.setHorizontalAlignment(align);

        if (disable = true){
            cell.disableBorderSide(PdfPCell.BOTTOM);
            cell.disableBorderSide(PdfPCell.TOP);
            cell.disableBorderSide(PdfPCell.LEFT);
            cell.disableBorderSide(PdfPCell.RIGHT);
        }

        cell.setBackgroundColor(backg);
        tab.addCell(cell);
    }

    /**
     * Este metodo es para hacer celdas ya preparadas.
     *
     * @param val       Valor a colocar en la celda.
     * @param tab       Tabla la cual va a recibir la celda.
     * @param font      Tipo de letra para la celda.
     * @param backg     Color de fondo de la celda.
     * @param border    Color de los bordes.
     * @param disable   Deshabilita los bordes.
     */
    public void celda(Object val, PdfPTable tab, Font font, BaseColor backg, BaseColor border, boolean disable){
        PdfPCell cell;
        Paragraph p = new Paragraph( String.valueOf(val), font);
        cell = new PdfPCell(p);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);

        if (disable = true){
            cell.disableBorderSide(PdfPCell.BOTTOM);
            cell.disableBorderSide(PdfPCell.TOP);
            cell.disableBorderSide(PdfPCell.LEFT);
            cell.disableBorderSide(PdfPCell.RIGHT);
        }

        cell.setBorderColor(border);
        cell.setBackgroundColor(backg);
        tab.addCell(cell);
    }

    /**
     * Este metodo es para hacer celdas ya preparadas.
     *
     * @param val       Valor a colocar en la celda.
     * @param tab       Tabla la cual va a recibir la celda.
     * @param font      Tipo de letra para la celda.
     * @param backg     Color de fondo de la celda.
     * @param border    Color de los bordes.
     * @param Colspan   Ancho de la celda en columnas.
     */
    public void celda(Object val, PdfPTable tab, Font font, BaseColor backg, BaseColor border, int Colspan){
        PdfPCell cell;
        Paragraph p = new Paragraph( String.valueOf(val), font);
        cell = new PdfPCell(p);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setBorderColor(border);
        cell.setColspan(Colspan);
        cell.setBackgroundColor(backg);
        tab.addCell(cell);
    }

    /**
     * Este metodo es para hacer celdas ya preparadas.
     *
     * @param img Esta es la imagen a colocar en la celda.
     * @param tab Tabla la cual va a recibir la celda.
     */
    public void celda(Image img, PdfPTable tab){
        PdfPCell cell;
        cell = new PdfPCell(img);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setBorderColor(BaseColor.WHITE);
        tab.addCell(cell);
    }

    /* Las funciones a continuacion suplen
     * varias funciones durante el proceso.
     */

    public String getStuff(String id_sol, String atr) throws Exception{
        return ElectOTDao.getStuffOT(id_sol, atr);
    }

    public String getNext(){
        return next;
    }

 /**
     * metodo para obtener el mes de mora
     * @param id id de la solicitud
     * @author MGarizao - GOETECH
     * @date 06/04/2010
     * @version 1.0
     * @return mes de mora
     * @throws Exception
     */
    public  String  getMesesMora(String id) throws Exception{
      return ElectOTDao.getMesesMora(id);
    }
public BeanGeneral getInfoAceptarPagos(String id_solicitud) throws Exception {
        return ElectOTDao.getInfoAceptarPagos(id_solicitud);
    }

    public String aceptarPagos(BeanGeneral info) throws Exception {
        return ElectOTDao.aceptarPagos(info);
    }

    public String sqlIngresarOT(OrdenTrabajo orden, boolean flag, String usuario) throws Exception {
        return ElectOTDao.sqlIngresarOT(orden, flag, usuario);
    }

}