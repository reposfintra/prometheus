/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * ContratistaService.java :
 * Copyright 2010 Rhonalf Martinez Villa <rhonaldomaster@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.tsp.opav.model.services;
import java.util.*;
import com.tsp.opav.model.beans.*;
import com.tsp.opav.model.DAOS.*;
/**
 *
 * @author rhonalf
 */
public class ContratistaService {

    private ContratistaDAO cdao;

    public ContratistaService(){
        cdao = new ContratistaDAO();
    }
    public ContratistaService(String dataBaseName){
        cdao = new ContratistaDAO(dataBaseName);
    }

    /**
     * Lista los tipos de impuestos
     * @param tipo Se escoge con los numeros:<br>1:reteica<br>2:retefuente
     * @return listado con los registros obtenidos
     * @throws Exception cuando hay error
     */
    public ArrayList listarImpuestos(int tipo) throws Exception{
        ArrayList lista = null;
        try {
            lista = cdao.listarImpuestos(tipo);
        }
        catch (Exception e) {
            throw new Exception("Error al listar impuestos: "+e.toString());
        }
        return lista;
    }

     /**
     * Busca datos de contratistas
     * @param filtro nombre de la columna de la tabla app_contratista por la cual se quiere filtrar
     * @param param el dato a buscar
     * @return lista con objetos Contratista que se ajustaron al criterio de busqueda
     * @throws Exception cuando hay un error
     */
    public ArrayList buscar(String filtro,String param) throws Exception{
        ArrayList lista=null;
        try {
            lista = cdao.buscar(filtro, param);
        }
        catch (Exception e) {
            throw new Exception("Error al buscar datos de contratistas: "+e.toString());
        }
        return lista;
    }

    /**
     * Inserta un nuevo registro en la tabla de contratistas
     * @param contratista Objeto de tipo Contratista con los datos
     * @throws Exception Cuando hay un error
     */
    public void insertar(Contratista contratista) throws Exception{
        try {
            cdao.insertar(contratista);
        }
        catch (Exception e) {
            throw new Exception("Error al insertar datos: "+e.toString());
        }
    }

    /**
     * Actualiza un registro de la tabla de contratistas
     * @param contratista Objeto de tipo Contratista con los datos
     * @throws Exception Cuando hay un error
     */
    public void actualizar(Contratista contratista) throws Exception{
        try {
            cdao.actualizar(contratista);
        }
        catch (Exception e) {
            throw new Exception("Error al actualizar datos: "+e.toString());
        }
    }

    /**
     * Anula un registro de la tabla de contratistas
     * @param codigo El codigo del contratista
     * @throws Exception Cuando hay un error
     */
    public void anular(String codigo) throws Exception{
        try {
            cdao.anular(codigo);
        }
        catch (Exception e) {
            throw new Exception("Error al eliminar datos: "+e.toString());
        }
    }

    /**
     * Lista las actividades
     * @return ArrayList con el codigo html a escribir
     * @throws Exception cuando hay error
     */
    public ArrayList listaActividades() throws Exception{
        ArrayList lista=null;
        try {
            lista = cdao.listaActividades();
        }
        catch (Exception e) {
            throw new Exception("Error al listar actividades: "+e.toString());
        }
        return lista;
    }

}