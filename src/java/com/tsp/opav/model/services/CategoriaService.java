/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.opav.model.services;
import java.util.*;
import com.tsp.opav.model.beans.*;
import com.tsp.opav.model.DAOS.*;
/**
 *
 * @author Ing. Jose Castro
 */
public class CategoriaService {

    private CategoriaDAO cdao;

    public CategoriaService() {
        cdao = new CategoriaDAO();
    }
    public CategoriaService(String dataBaseName) {
        cdao = new CategoriaDAO(dataBaseName);
    }


    /**
    *
    * @return
    * @throws Exception
    * Created by Ing. Jose Castro
    */
     public ArrayList listCategorias() throws Exception {
        ArrayList arl = cdao.listCategorias();
        return arl;
    }


    /**
    *
    * @return
    * @throws Exception
    * Created by Ing. Jose Castro
    */
     public ArrayList listCategoria(String categoria) throws Exception {
        ArrayList arl = cdao.listCategoria(categoria);
        return arl;
    }


         /**
    *
    * @return
    * @throws Exception
    * Created by Ing. Jose Castro
    */
     public ArrayList listSubcategorias(String categoria) throws Exception {
        ArrayList arl = cdao.listSubcategorias(categoria);
        return arl;
    }

         /**
    *
    * @return
    * @throws Exception
    * Created by Ing. Jose Castro
    */
     public ArrayList listtipoSubcategorias(String subcategoria) throws Exception {
        ArrayList arl = cdao.listtipoSubcategorias(subcategoria);
        return arl;
    }

/**
 *
 * @param descripcion
 * @param reg_status
 * @param usuario
 * @throws Exception
 */
    public int  insertarCategoria(String descripcion, String reg_status, String usuario) throws Exception {
    return  cdao.insertarCategoria(descripcion, reg_status, usuario);
    }


  /**
 *
 * @param descripcion
 * @param reg_status
 * @param usuario
 * @throws Exception
 */
    public int  insertarSubCategoria(int idcategoria, String descripcion, String reg_status, String usuario) throws Exception {
    return  cdao.insertarSubCategoria(idcategoria, descripcion, reg_status, usuario);
    }


/**
 *
 * @param idcategoria
 * @param idsubcategoria
 * @param descripcion
 * @param reg_status
 * @param usuario
 * @return
 * @throws Exception
 */
    public String  insertartipoSubCategoria(int idcategoria, int idsubcategoria, String descripcion, String reg_status, String usuario) throws Exception {
    return  cdao.insertartipoSubCategoria(idcategoria, idsubcategoria, descripcion, reg_status, usuario);
    }


/**
 *
 * @param descripcion
 * @param reg_status
 * @param usuario
 * @param idcat
 * @throws Exception
 */
 public void updateCategoria(String descripcion, String reg_status, String usuario, int idcat) throws Exception {
     cdao.updateCategoria(descripcion, reg_status, usuario, idcat);
 }


/**
 *
 * @param descripcion
 * @param reg_status
 * @param usuario
 * @param idcat
 * @throws Exception
 */
 public void updateSubCategoria(String descripcion, String reg_status, String usuario, int idcat, int idsubcat) throws Exception {
     cdao.updateSubCategoria(descripcion, reg_status, usuario, idcat, idsubcat);
 }


public String updatetipoSubCategoria(String descripcion, String reg_status, String usuario, int idcategoria, int idsubcategoria, int idtiposub) throws Exception {
  return cdao.updatetipoSubCategoria(descripcion, reg_status, usuario, idcategoria, idsubcategoria, idtiposub);
}


    public void listadoCategorias()throws Exception{
        cdao.listadoCategorias();
    }

    public TreeMap getListadoCategorias(){
        return cdao.getListadoCategorias();
    }

    public void setListadoCategorias(TreeMap t){
     cdao.setListadoCategorias(t);
    }


    //SubCategorias
    public void listadoSubCategorias(int idcat)throws Exception{
        cdao.listadoSubCategorias(idcat);
    }

    public TreeMap getListadoSubCategorias(){
        return cdao.getListadoSubCategorias();
    }

    public void setListadoSubCategorias(TreeMap t){
     cdao.setListadoSubCategorias(t);
    }
    //TipoSubCategorias
    public void listadoTipoSubCategorias(int idcat, int idsubcat)throws Exception{
        cdao.listadoTipoSubCategorias(idcat, idsubcat);
    }

    public TreeMap getListadoTipoSubCategorias(){
        return cdao.getListadoTipoSubCategorias();
    }

    public void setListadoTipoSubCategorias(TreeMap t){
     cdao.setListadoTipoSubCategorias(t);
    }



    /**
 *
 * @param filtro
 * @param cadena
 * @param idcat
 * @param idsubcat
 * @param idtiposub
 * @return
 * @throws Exception
 * Created by: Jose Castro
 */
    public ArrayList categoriasExcel() throws NullPointerException, Exception {
        ArrayList listado = new ArrayList();
        try {
            listado = cdao.categoriasExcel();
            if (listado.size() < 0) {
                throw new Exception("No hay valores para la busqueda solicitada");
            }
        } catch (Exception e) {
            System.out.println("Error : " + e.toString());
            e.printStackTrace();
        }
        return listado;
    }



    //////////////////Cotizacion nueva


        public void listadoCategoriasTipo(String tipo)throws Exception{
        cdao.listadoCategoriasTipo(tipo);
    }

    public TreeMap getListadoCategoriasTipo(){
        return cdao.getListadoCategoriasTipo();
    }

    public void setListadoCategoriasTipo(TreeMap t){
     cdao.setListadoCategoriasTipo(t);
    }


/////

     public void listadoMateriales(String tipo, int idcategoria, int subcat, int tipsubcat)throws Exception{
        cdao.listadoMateriales(tipo, idcategoria, subcat, tipsubcat);
    }

    public TreeMap getlistadoMateriales(){
        return cdao.getListadoMateriales();
    }

    public void setlistadoMateriales(TreeMap t){
     cdao.setListadoMateriales(t);
    }



/**
 *
 * @param idcategoria
 * @return
 * @throws NullPointerException
 * @throws Exception
 * @return listado de subcategorias y tiposubcategorias dada una categoria
 */
    public ArrayList listadoSubTiposub(int idcategoria) throws NullPointerException, Exception {
        ArrayList listado = new ArrayList();
        try {
            listado = cdao.listadoSubTiposub(idcategoria);
            if (listado.size() < 0) {
                throw new Exception("No hay valores para la busqueda solicitada");
            }
        } catch (Exception e) {
            System.out.println("Error : " + e.toString());
            e.printStackTrace();
        }
        return listado;
    }






}