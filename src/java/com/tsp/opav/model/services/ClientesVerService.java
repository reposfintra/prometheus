/*
 * creacionCompraCarteraService.java
 *
 * Created on 1 de enero de 2008, 05:58 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */
package com.tsp.opav.model.services;
import java.util.*;
import com.tsp.opav.model.DAOS.*;
import com.tsp.opav.model.beans.*;
import com.tsp.operation.model.TransaccionService;
import com.tsp.operation.model.beans.Cliente;
import com.tsp.operation.model.beans.SerieGeneral;
import java.sql.*;

/**
 *
 * @author NAVI
 */
public class ClientesVerService {
     ClientesVerDAO cldao ;

//    public ClientesVerService()
//    {
//         cldao = new ClientesVerDAO();
//    }
    
    public ClientesVerService(String dataBaseName) {
         cldao = new ClientesVerDAO(dataBaseName);
    }   

    public String insertarCl(Cliente cl, String usuario) throws Exception {
        String respuesta = "";
        TransaccionService scv = new TransaccionService(cldao.getDatabaseName());
        scv.crearStatement();
        try {
            scv.getSt().addBatch(cldao.insertarCl(cl, usuario));
            ArrayList<String> listSql = cldao.insertarNics(cl.getCodcli(), usuario, cl.getNics());
            for (String sql : listSql) {
                scv.getSt().addBatch(sql);
            }
            scv.getSt().addBatch(cldao.insertSubcliente(cl.getCodcli(), cl.getId_padre(), usuario));  //PBASSIL
            respuesta = "Cliente creado exitosamente!!";
            scv.execute();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Codigo de el error:" + e.toString());
            respuesta = "Error en la creacion del cliente, porfavor verifique los datos";
            cl.setCodcli("");
            cl.setNics(null);
        } finally {
            scv.closeAll();
        }
        return respuesta;
    }

    public String insertarOf(OfertaElca ofca, String usuario) throws Exception {
        String respuesta = "";
        TransaccionService scv = new TransaccionService(this.cldao.getDatabaseName());
        scv.crearStatement();
        try {
            scv.getSt().addBatch(cldao.insertarOf(ofca, usuario));
            respuesta = "Oferta creada exitosamente!!";
            scv.execute();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Codigo de el error:" + e.toString());
            respuesta = "Error en la creacion de la solicitud, porfavor verifique los datos";
            //ofca.setId_oferta("");//091030
            ofca.setId_solicitud("");//091030
        } finally {
            scv.closeAll();
        }
        return respuesta;
    }

    public String insertarAcc(AccionesEca acceca, String usuario) throws Exception {
        String respuesta = "";
        TransaccionService scv = new TransaccionService(this.cldao.getDatabaseName());
        scv.crearStatement();
        try {
            scv.getSt().addBatch(cldao.insertarAcc(acceca, usuario));
            respuesta = "Accion creada exitosamente!!";
            scv.execute();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Codigo de el error:" + e.toString());
            respuesta = "Error en la creacion de la accion, porfavor verifique los datos";
            acceca.setId_accion("");
        } finally {
            scv.closeAll();
        }
        return respuesta;
    }

    public String delAcc(AccionesEca acceca, String usuario) throws Exception {
        String respuesta = "";
        TransaccionService scv = new TransaccionService(this.cldao.getDatabaseName());
        scv.crearStatement();
        try {
            scv.getSt().addBatch(cldao.delAcc(acceca, usuario));
            scv.getSt().addBatch(cldao.delDetCot(acceca, usuario));
            scv.getSt().addBatch(cldao.delCot(acceca, usuario));
            respuesta = "Accion eliminada exitosamente!!";
            scv.execute();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Codigo de el error:" + e.toString());
            respuesta = "Error en la eliminacion de la accion";
            acceca.setId_accion("");
        } finally {
            scv.closeAll();
        }
        return respuesta;
    }

    public String updateCl(Cliente cl, String usuario, boolean sw) throws Exception {
        String respuesta = "";
        TransaccionService scv = new TransaccionService(this.cldao.getDatabaseName());
        scv.crearStatement();
        try {
            scv.getSt().addBatch(cldao.updateCl(cl, usuario, sw));
            ArrayList<String> listSql = cldao.insertarNics(cl.getCodcli(), usuario, cl.getNics());
            for (String sql : listSql) {
                scv.getSt().addBatch(sql);
            }
            scv.getSt().addBatch(cldao.updatePadre(cl, usuario));
            respuesta = "Cliente actualizado exitosamente!!";
            scv.execute();
            cl.setNics(cldao.getNics(cl.getCodcli()));
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Codigo de el error:" + e.toString());
            respuesta = "Error en la actualizacion del cliente, porfavor verifique los datos";
            cl.setNics(cldao.getNics(cl.getCodcli()));
        } finally {
            scv.closeAll();
        }
        return respuesta;
    }

    public String updateOf(OfertaElca ofca, String usuario) throws Exception {
        String respuesta = "";
        TransaccionService scv = new TransaccionService(this.cldao.getDatabaseName());
        scv.crearStatement();
        try {
            String[] lista = cldao.updateOf(ofca, usuario).split(";");
            for (String sql : lista) {
                scv.getSt().addBatch(sql);
            }
            String est = this.estadoSolicitud(ofca.getId_solicitud());
            if(!(est.equals("030") || est.equals("040"))){
                respuesta = respuesta + "\nSe han recalculado los precios de venta.Debe volver a generar el pdf de la oferta";//2010-04-30 rhonalf
            }//2010-04-30 rhonalf
            respuesta = "Oferta actualizada exitosamente!!";
            scv.execute();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Codigo de el error:" + e.toString());
            respuesta = "Error en la actualizacion del cliente, porfavor verifique los datos";
        } finally {
            scv.closeAll();
        }
        return respuesta;
    }

    public String buscarCl(Cliente cl) throws Exception {
        String respuesta = "";
        try {
            respuesta = cldao.buscarCl(cl);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Codigo de el error:" + e.toString());
            respuesta = "Error en la busqueda del cliente, porfavor verifique los datos";
        }
        return respuesta;
    }

    public String buscarOf(OfertaElca ofca) throws Exception {
        String respuesta = "";
        try {
            respuesta = cldao.buscarOf(ofca);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Codigo de el error:" + e.toString());
            respuesta = "Error en la busqueda de la solicitud, porfavor verifique los datos";
        }
        return respuesta;
    }

    public ArrayList buscarAcc(AccionesEca acceca, String nitprop) throws Exception {
        ArrayList respuesta = null;
        try {
            respuesta = cldao.buscarAcc(acceca, nitprop);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Codigo de el error:" + e.toString());
        }
        return respuesta;
    }

    public String getMails(String opc, String codigo_ejecutivo, String codigo_contratista) throws Exception {
        String respuesta = "";
        try {
            if (opc.equals("1") || opc.equals("3")) {
                respuesta = cldao.buscarMails("opav");
            }
            if (opc.equals("2") || opc.equals("3")) {
                respuesta = respuesta + cldao.buscarMails("ejecutivo_admin");
            }
            respuesta = respuesta + cldao.getMailEjecutivo(codigo_ejecutivo);
            if (!codigo_contratista.equals("")) {
                respuesta = respuesta + ";" + cldao.getMailContratista(codigo_contratista);
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Codigo de el error:" + e.toString());
        }
        return respuesta;
    }

    public String getLoginEjecutivo(String codigo_cliente) throws Exception {
        String respuesta = "";
        try {
            respuesta = cldao.getLoginEjecutivo(codigo_cliente);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Codigo de el error:" + e.toString());
        }
        return respuesta;
    }

    public ArrayList getDatos(String param) throws Exception {
        ArrayList arl = cldao.getDatos(param);
        return arl;
    }

    public ArrayList getContratistas() throws Exception {
        ArrayList arl = cldao.getContratistas();
        return arl;
    }

    public ArrayList getEstados() throws Exception {
        ArrayList arl = cldao.getEstado();
        return arl;
    }

    public ArrayList getEjecutivos() throws Exception {
        ArrayList arl = cldao.getEjecutivos();
        return arl;
    }

    public ArrayList getPadres() throws Exception {
        ArrayList arl = cldao.getPadres();
        return arl;
    }

    public boolean isOficial(String id) throws Exception {
        return cldao.isOficial(id);
    }

    public boolean ispermitted(String perfil, String accion) throws Exception {//07/04/2010 - MGarizao GEOTECH

        return cldao.ispermitted(perfil, accion);
    }

    public String getPerfil(String usuario) throws Exception {
        return cldao.getPerfil(usuario);
    }

    public String getAviso(String id_sol) throws Exception{
        return cldao.getAviso(id_sol);
    }

    public String[] getDatosMensaje(String accion) throws Exception {//091203
        return cldao.getDatosMensaje(accion);
    }
        //100204
    public String estadoAccion(String accion) {
        String estado = "";
        try{
            estado = cldao.estadoAccion(accion);
        }
        catch(Exception e){
            System.out.println("Error buscando estado de la accion: "+e.getMessage());
            e.printStackTrace();
        }
        return estado;
    }
     /**
     * Busca el estado en que esta una solicitud
     * @author rhonalf
     * @param solicitud el id_solicitud
     * @return String con el estado
     * @throws Exception Cuando hay un error
     * @since 2010-04-30
     */
    public String estadoSolicitud(String solicitud) throws Exception{
        String est="";
        try {
            est = cldao.estadoSolicitud(solicitud);
        }
        catch (Exception e) {
            throw new Exception("Error al buscar los estados de la solicitud: "+e.toString());
        }
        return est;
    }

     /**
     * Devuelve los meses de mora de una solicitud
     * @author rhonalf
     * @param solicitud el id_solicitud
     * @return String con el dato
     * @throws Exception Cuando hay error
     * @since 2010-04-30
     */
    public String getMesesMora(String solicitud) throws Exception{
        String mes = "";
        try {
            mes = cldao.getMesesMora(solicitud);
        } catch (Exception e) {
            throw new Exception("Error al buscar los meses de mora: "+e.toString());
        }
        return mes;
    }

    /**
     * Obtiene el listado de departamentos de clientes_eca
     * @return ArrayList con los departamentos
     * @throws Exception
     * @author darrieta-Geotech 08/05/2010
     */
    public ArrayList listarDepartamentos() throws Exception {
        return cldao.listarDepartamentos();
    }

    /**
     * Obtiene el listado de ejecutivos
     * @return ArrayList de String[] con los ejecutivos obtenidos
     * @throws Exception
     * @author darrieta-Geotech 08/05/2010
     */
    public ArrayList buscarCliente(String cliente) throws Exception {
        return cldao.buscarCliente(cliente);
    }

    /**
     * Obtiene el listado de ejecutivos
     * @return ArrayList de String[] con los ejecutivos obtenidos
     * @throws Exception
     * @author darrieta-Geotech 08/05/2010
     */
    public ArrayList buscarEjecutivo(String ejecutivo) throws Exception {
        return cldao.buscarEjecutivo(ejecutivo);
    }
public ArrayList buscarId() throws Exception{
        ArrayList lista = null;
        try {
            lista = cldao.buscarId();
        }
        catch (Exception e) {
            throw new Exception("Error al buscar datos: "+e.toString());
        }
        return lista;
    }

    public String insercion() throws Exception{
        String resp="";
        try {
            resp = cldao.insercion();
        }
        catch (Exception e) {
            throw new Exception("Error al insertar datos: "+e.toString());
        }
        return resp;
    }

    public ArrayList<String> verEquiv1(String cadtabla) throws Exception{
        ArrayList<String> listado = null;
        try {
            listado = cldao.verEquiv1(cadtabla);
        }
        catch (Exception e) {
            throw new Exception("Error al listar datos: "+e.toString());
        }
        return listado;
    }

    public ArrayList<String> verEquiv2(String cadtabla) throws Exception{
        ArrayList<String> listado = null;
        try {
            listado = cldao.verEquiv2(cadtabla);
        }
        catch (Exception e) {
            throw new Exception("Error al listar datos: "+e.toString());
        }
        return listado;
    }

/**
     * Inserta una lista de clientes de onsorcio en la tabla cliente de fintra
     * @param lista listado de codigos de clientes a pasar
     * @param stringfec fecha del proceso en formato AAAAMMDD
     * @throws Exception caundo hay error en el proceso
     */
    public void insFin(ArrayList lista,String stringfec) throws Exception{
        try {
            cldao.insFin(lista, stringfec);
        }
        catch (Exception e) {
            throw new Exception("Error al insertar datos: "+e.toString());
        }
    }

    /**
     * Busca las coincidencias de un nit en la tabla de clientes
     * @param nit Dato a buscar
     * @param fec fecha del proceso en formato AAAAMMDD
     * @return Listado de coincidencias
     * @throws Exception Cuando hay error
     */
    public ArrayList<String> buscarReps(String nit,String fec) throws Exception{
        ArrayList<String> lista = null;
        try {
            lista = cldao.buscarReps(nit,fec);//20100810 rh
        }
        catch (Exception e) {
            throw new Exception("Error al listar datos: "+e.toString());
        }
        return lista;
    }
    /**
     * Busca las coincidencias de un nit en la tabla de clientes
     * @param nit Dato a buscar
     * @param fec Fecha del proceso en formato AAAAMMDD
     * @return Listado de coincidencias
     * @throws Exception Cuando hay error
     */
    public String srchReps(String nit,String fec) throws Exception{
        ArrayList<String> lista = null;
        String cadresp = "";
        try {
            lista = cldao.srchReps(nit,fec);
            if(lista.size()>0){
                for (int i = 0; i < lista.size(); i++) {
                    cadresp += "<option value='"+lista.get(i)+"'>"+lista.get(i)+"</option>";
                }
            }
            else{
                cadresp = "<option value='' selected>No se encontraron coincidencias</option>";
            }
        }
        catch (Exception e) {
            cadresp = "<option value='' selected>No se encontraron registros</option>";
            throw new Exception("Error al listar datos: "+e.toString());
        }
        return cadresp;
    }
    /**
     * Inserta registros directamente en la tabla de equivalencia
     * @param lista Listado con codigos de clientes de consorcio a insertar en equivalencia
     * @param stringfec fecha del proceso en formato AAAAMMDD
     * @throws Exception cuando hay error en el proceso
     */
    public void insertEquivalencia(ArrayList<String> lista,String stringfec) throws Exception{
        try {
            cldao.insertEquivalencia(lista, stringfec);
        }
        catch (Exception e) {
             throw new Exception("Error al insertar datos: "+e.toString());
        }
    }
    /**
     * Inserta registros directamente en la tabla de equivalencia
     * @param lista Listado con codigos de clientes de consorcio a insertar en equivalencia
     * @param stringfec fecha del proceso en formato AAAAMMDD
     * @throws Exception caundo hay error en el proceso
     */
    public void directoequiv(ArrayList lista,String stringfec) throws Exception{
        try {
           cldao.insertEquivalencia(lista, stringfec);//20100810
        }
        catch (Exception e) {
            throw new Exception("Error al insertar datos: "+e.toString());
        }
    }

    public ArrayList getTiposSolicitud() throws Exception {
        ArrayList arl = cldao.getTiposSolicitud();
        return arl;
    }



    public boolean Validar_modifcacion_cliente(String id_cliente) throws Exception {
        boolean  sw = cldao.Validar_modifcacion_cliente(id_cliente);
        return sw;
    }
    /**
     *
     * @return
     * @throws Exception
     * Created by Ing. Jose Castro
     */
      public ArrayList listadoResponsables() throws NullPointerException,Exception {
        ArrayList arl = cldao.listadoResponsables();
        return arl;
    }

      public String updateOf2(OfertaElca ofca, String usuario) throws Exception {//20101123
        String respuesta = "";
        TransaccionService scv = new TransaccionService(this.cldao.getDatabaseName());
        scv.crearStatement();
        try {
            String[] lista = cldao.updateOf3(ofca, usuario).split(";");
            for (String sql : lista) {
                scv.getSt().addBatch(sql);
            }
            String est = this.estadoSolicitud(ofca.getId_solicitud());
            if(!(est.equals("030") || est.equals("040"))){
                respuesta = respuesta + "\nSe han recalculado los precios de venta.Debe volver a generar el pdf de la oferta";//2010-04-30 rhonalf
            }//2010-04-30 rhonalf
            respuesta = "";
            scv.execute();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Codigo de el error:" + e.toString());
            respuesta = "Error en la actualizacion del cliente, porfavor verifique los datos";
        } finally {
            scv.closeAll();
        }
        return respuesta;
    }




        public String ValidaClienteXnit(String nit) throws Exception {//20101123
        String respuesta = "";

        try
        {
         respuesta= cldao.ValidaClienteXnit(nit);
        }
        catch (Exception e) {
            e.printStackTrace();
            System.out.println("Codigo de el error:" + e.toString());
            respuesta = "Error en la validacion  del cliente, porfavor verifique los datos";
        } 
        return respuesta;
    }




           public Cliente MarcarCliente(String nit) throws Exception {//20101123

        Cliente cl = new Cliente();

        try
        {
         cl= cldao.MarcarCliente(nit);
        }
        catch (Exception e) {
            e.printStackTrace();
            System.out.println("Codigo de el error:" + e.toString());
           
        }
        return cl;
    }
public String insertarCli(Cliente cl, String usuario) throws Exception {
        return cldao.insertarCl(cl, usuario);
    }

    /**
     *
     * @param idcliente
     * @param usuario
     * @param nics
     * @return
     * @throws Exception
     */
    public ArrayList<String> insertarNics(String idcliente, String usuario, String[] nics) throws Exception {
        return cldao.insertarNics(idcliente, usuario, nics);
    }

    public String insertSubcliente(String hijo, String padre, String user) throws Exception {
        return cldao.insertSubcliente(hijo, padre, user);
    }

    public String marcarCliente(String nit) throws Exception {
        return cldao.marcarCliente(nit);
    }

    public String insertarOf(OfertaElca ofca) throws Exception {

        return cldao.insertarOf(ofca);
    }

    /**
     *
     * @param dstrct
     * @param agency_id
     * @param document_type
     * @return
     * @throws SQLException
     */
    public SerieGeneral getSerie(String dstrct, String agency_id, String document_type) throws SQLException {
        return cldao.getSerie(dstrct, agency_id, document_type);
    }

    /**
     *
     * @param dstrct
     * @param agency_id
     * @param document_type
     * @throws SQLException
     */
    public void setSerie(String dstrct, String agency_id, String document_type) throws SQLException {
        cldao.setSerie(dstrct, agency_id, document_type);
    }

     public String insertarAcc(String fecha_vis_plan, AccionesEca acceca) throws Exception {
        return cldao.insertarAcc(fecha_vis_plan, acceca );
    }

    public String insertarCliente(Cliente cl, String usuario) throws Exception {
        return cldao.insertarCliente(cl, usuario);
    }
    
     /**
     * Busca el codigo del cliente dado el nit
     * @param nit Nit del cliente a buscar
     * @return String con el codigo del cliente
     * @throws Exception Cuando hay error
     */
    public String codclifinv(String nit) throws Exception{
        return cldao.codclifinv(nit);
    }

    public boolean existeNic(String nic) throws Exception {
        return cldao.existeNic(nic);
    }

    public String getDistribucion(String tipo_solicitud) {
        return cldao.getDistribucion(tipo_solicitud);
    }

}