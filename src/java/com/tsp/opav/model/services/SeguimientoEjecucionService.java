package com.tsp.opav.model.services;

import com.tsp.opav.model.DAOS.SeguimientoEjecucionDAO;
import com.tsp.opav.model.beans.Actividades;
import com.tsp.opav.model.beans.NegocioApplus;
import com.tsp.opav.model.beans.OfertaSeguimiento;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Vector;

/**
 * SeguimientoEjecucionService.java<br/>
 * Service para el programa de seguimiento a la ejecucion de trabajos<br/>
 * 4/06/2010
 * @author darrieta-GEOTECH
 * @version 1.0
 */
public class SeguimientoEjecucionService {

    private SeguimientoEjecucionDAO dao;

    public SeguimientoEjecucionService() {
        dao = new SeguimientoEjecucionDAO();
    }
    public SeguimientoEjecucionService(String dataBaseName) {
        dao = new SeguimientoEjecucionDAO(dataBaseName);
    }

    /**
     * Obtiene la informacion de la accion y solicitud
     * @param idSolicitud identificador de la solicitud
     * @param idAccion identificador de la accion
     * @return bean NegocioApplus con los datos consultados
     * @throws SQLException
     */
    public NegocioApplus obtenerInfoAccion(String idSolicitud, String idAccion) throws SQLException{
        return dao.obtenerInfoAccion(idSolicitud, idAccion);
    }

    public NegocioApplus obtenerInfoSolicitud(String idSolicitud) throws SQLException{
        return dao.obtenerInfoSolicitud(idSolicitud);
    }

    /**
     * Obtiene la lista de acciones para un tipo de trabajo
     * @param tipoTrabajo tipo de trabajo de la accion
     * @return ArrayList con el listado de actividades
     * @throws SQLException
     */
    public ArrayList<Actividades> obtenerListaActividadesTrabajo(String tipoTrabajo) throws SQLException{
        return dao.obtenerListaActividadesTrabajo(tipoTrabajo);
    }

    /**
     * Guarda las actividades de una accion en la tabla accion_programacion
     * @param listaActividades ArrayList con la informacion de las actividades a insertar
     * @return Vector con los comandos sql generados
     * @throws SQLException
     */
    public Vector guardarProgramacionActividades(ArrayList listaActividades)throws SQLException{
        Vector queries = new Vector();
        queries.add(dao.eliminarActividadesAccion(((Actividades)listaActividades.get(0)).getId_accion()));
        for (int i = 0; i < listaActividades.size(); i++) {
            Actividades actividad = (Actividades)listaActividades.get(i);
            queries.add(dao.guardarActividadAccion(actividad));
        }
        return queries;
    }

    /**
     * Actualiza un registro en la tabla
     * @param actividad bean con la informacion a actualizar
     * @return query generado para la insercion
     * @throws SQLException
     */
    public String actualizarActividadAccion(Actividades actividad) throws SQLException{
        return dao.actualizarActividadAccion(actividad);
    }

    /**
     * Obtiene todas las actividades de una accion
     * @param id_accion identificador de la accion
     * @return ArrayList de beans Actividades con los resultados obtenidos
     * @throws SQLException
     */
    public ArrayList<Actividades> consultarActividadesAccion(String id_accion) throws SQLException{
        return dao.consultarActividadesAccion(id_accion);
    }

    /**
     * Obtiene todas las actividades de una solicitud
     * @param id_solicitud identificador de la solicitud
     * @return ArrayList con los resultados obtenidos
     * @throws SQLException
     */
    public ArrayList<Actividades> consultarActividadesSolicitud(String id_solicitud) throws SQLException{
        return dao.consultarActividadesSolicitud(id_solicitud);
    }

    /**
     * Obtiene todas las actividades de una solicitud
     * @param id_solicitud identificador de la solicitud
     * @param id_accion identificador de la accion
     * @return ArrayList con los resultados obtenidos
     * @throws SQLException
     */
    public ArrayList<Actividades> consultarActividadesSolicitud(String id_solicitud, String id_accion) throws SQLException{
        return dao.consultarActividadesSolicitud(id_solicitud, id_accion);
    }

    /**
     * Busca si una solicitud tiene completa la programacion de todas las actividades para cada accion
     * @param id_solicitud identificador de la solicitud a consultar
     * @return true si la programacion esta completa, false si no esta completa
     * @throws SQLException
     */
    public boolean tieneProgramacion(String id_solicitud) throws SQLException{
        return dao.tieneProgramacion(id_solicitud);
    }

    /**
     * consulta todas las observaciones registradas al seguimiento de una solicitud
     * @param id_solicitud
     * @param reg_status estado del registro a buscar 'G' guardado o '' confirmado
     * @return ArryList de beans OfertaSeguimiento con los resultados obtenidos
     * @throws SQLException
     */
    public ArrayList<OfertaSeguimiento> consultarObservacionesSolicitud(String id_solicitud, String reg_status) throws SQLException{
        return dao.consultarObservacionesSolicitud(id_solicitud, reg_status);
    }

    public Vector guardarSeguimientoActividades(ArrayList listaActividades, OfertaSeguimiento ofertaSeg)throws SQLException{
        Vector queries = new Vector();
        queries.add(dao.eliminarOfertaEjecucion(((Actividades)listaActividades.get(0))));
        queries.add(dao.eliminarOfertaSeguimiento(ofertaSeg));
        //Guardar observacion y totales
        queries.add(dao.guardarOfertaSeguimiento(ofertaSeg));
        //guardar seguimiento para cada actividad
        for (int i = 0; i < listaActividades.size(); i++) {
            Actividades actividad = (Actividades)listaActividades.get(i);
            queries.add(dao.guardarOfertaEjecucion(actividad));
        }
        return queries;
    }

    public ArrayList<Actividades> consultarAvanceSolicitud(String id_solicitud, String fecha) throws SQLException{
        return dao.consultarAvanceSolicitud(id_solicitud, fecha);
    }

    public String buscarFechaSeguimiento(String idSolicitud, String regStatus) throws SQLException{
        return dao.buscarFechaSeguimiento(idSolicitud, regStatus);
    }

    public ArrayList<Actividades> consultarSeguimientosSolicitud(String id_solicitud,String id_accion) throws SQLException{
        return dao.consultarSeguimientosSolicitud(id_solicitud, id_accion);
    }

    public int obtenerNumeroSeguimientos(String idSolicitud) throws SQLException{
        return dao.obtenerNumeroSeguimientos(idSolicitud);
    }

    public ArrayList<OfertaSeguimiento> consultarTotalesSeguimientos(String id_solicitud) throws SQLException{
        return dao.consultarTotalesSeguimientos(id_solicitud);
    }

    public String obtenerFechaFinalSolicitud(String idSolicitud) throws SQLException{
        return dao.obtenerFechaFinalSolicitud(idSolicitud);
    }
    
    public String actualizarFechaInicial(String idSolicitud, String fecha) throws SQLException {
        return dao.actualizarFechaInicial(idSolicitud, fecha);
    }

    public ArrayList<OfertaSeguimiento> consultarTotalesSeguimientos2(String idSolicitud) throws SQLException {
        return dao.consultarTotalesSeguimientos2(idSolicitud);
}
}