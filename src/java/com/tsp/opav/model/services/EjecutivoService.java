/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * EjecutivoService.java : interfaz para el manejo del dao controlador de ejecutivos
 * Copyright 2010 Rhonalf Martinez Villa <rhonaldomaster@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.tsp.opav.model.services;
import java.util.*;
import com.tsp.opav.model.DAOS.*;
import com.tsp.opav.model.beans.*;
/**
 *
 * @author rhonalf
 */
public class EjecutivoService {

    private EjecutivoDAO edao;

    public EjecutivoService(){
        edao = new EjecutivoDAO();
    }
    public EjecutivoService(String dataBaseName){
        edao = new EjecutivoDAO(dataBaseName);
    }

    /**
     * Busca los datos de los ejecutivos de acuerdo a un parametro especifico
     * @param param El nombre del atributo a buscar en la tabla
     * @param cadena El valor a buscar
     * @return Arraylist de objetos Ejecutivo
     * @throws Exception cuando ocurre un error
     */
    public ArrayList<Ejecutivo> buscarEjecutivos(String param,String cadena) throws Exception{
        ArrayList<Ejecutivo> ejecs = null;
        try {
            ejecs = edao.buscarEjecutivos(param, cadena);
        }
        catch (Exception e) {
            System.out.println("Error en EjecutivoService en el metodo buscarEjecutivos "+e.toString());
            throw new Exception("Error en EjecutivoService en el metodo buscarEjecutivos "+e.toString());
        }
        return ejecs;
    }

    /**
     * Busca los datos de un ejecutivo identificado por un id especifico
     * @param id el id del ejecutivo
     * @return Objeto Ejecutivo con los datos
     * @throws Exception cuando ocurra algun error
     */
    public Ejecutivo getDatosEjec(String id) throws Exception{
        Ejecutivo ejec = null;
        try {
            ejec = edao.getDatosEjec(id);
        }
        catch (Exception e) {
            System.out.println("Error en EjecutivoService en el metodo getDatosEjec "+e.toString());
            throw new Exception("Error en EjecutivoService en el metodo getDatosEjec "+e.toString());
        }
        return ejec;
    }

    /**
     * Inserta un ejecutivo al sistema
     * @param ejec Objeto Ejecutivo con los datos
     * @param user Usuario que hace la insercion
     * @throws Exception Cuando ocurre un error
     */
    public void insertarEjecutivo(Ejecutivo ejec,String user) throws Exception{
        try {
            edao.insertarEjecutivo(ejec, user);
        }
        catch (Exception e) {
            System.out.println("Error en EjecutivoService en el metodo insertarEjecutivo "+e.toString());
            throw new Exception("Error en EjecutivoService en el metodo insertarEjecutivo "+e.toString());
        }
    }

    /**
     * Elimina un registro de la tabla ejecutivos
     * @param id El id del ejecutivo a eliminar
     * @param user El usuario que elimina
     * @throws Exception Cuando hay un error
     */
    public void eliminarEjecutivo(String id,String user) throws Exception{
        try {
            edao.eliminarEjecutivo(id, user);
        }
        catch (Exception e) {
            System.out.println("Error en EjecutivoService en el metodo eliminarEjecutivo "+e.toString());
            throw new Exception("Error en EjecutivoService en el metodo eliminarEjecutivo "+e.toString());
        }
    }

    /**
     * Genera un listado de los cargos que estan en la columna cargo de la tabla ejecutivos
     * @return ArrayList de String con los nombres de los cargos
     * @throws Exception Cuando hay un error
     */
    public ArrayList listadoCargos() throws Exception{
        ArrayList listado = null;
        try {
            listado = edao.listadoCargos();
        }
        catch (Exception e) {
            System.out.println("Error en EjecutivoService en el metodo listadoCargos "+e.toString());
            throw new Exception("Error en EjecutivoService en el metodo listadoCargos "+e.toString());
        }
        return listado;
    }

    /**
     * Actualiza los datos de un ejecutivo
     * @param ejec Objeto con los datos
     * @param user Usuario que actualiza
     * @param id Codigo del ejecutivo
     * @throws Exception Cuando hay un error
     */
    public void actualizarEjecutivo(Ejecutivo ej,String user,String id) throws Exception{
        try {
            edao.actualizarEjecutivo(ej, user, id);
        }
        catch (Exception e) {
            System.out.println("Error en EjecutivoService en el metodo actualizarEjecutivo "+e.toString());
            throw new Exception("Error en EjecutivoService en el metodo actualizarEjecutivo "+e.toString());
        }
    }

}