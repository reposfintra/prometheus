/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.opav.model.services;

import java.util.*;
import com.tsp.opav.model.DAOS.*;
import com.tsp.operation.model.beans.BeanGeneral;

/**
 *
 * @author Ing. Iris Vargas
 */
public class GestionSolicitudAiresAAEService {

    private GestionSolicitudAiresAAEDAO dao;

    public GestionSolicitudAiresAAEService() {
        dao = new GestionSolicitudAiresAAEDAO();
    }
    public GestionSolicitudAiresAAEService(String dataBaseName) {
        dao = new GestionSolicitudAiresAAEDAO(dataBaseName);
    }

    /**
     * Busca contratistas de un tipo para solicitudes AAAE
     * @param tipo contratista es Instalador o Proveedor
     * @return ArrayList
     * @throws Exception cuando hay error
     */
    public ArrayList buscarContratistasAAAE(String tipo) throws Exception {
        ArrayList cadena = new ArrayList();
        try {
            cadena = dao.buscarContratistasAAAE(tipo);

        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error al listar datos: " + e.toString());
        }
        return cadena;
    }

    public String infoCliente(String nit) throws Exception {
        String codigo = "";
        try {
            codigo = dao.infoCliente(nit);
        } catch (Exception e) {
            throw new Exception("Error en buscar codigo cliente: " + e.toString());
        }
        return codigo;
    }

    public ArrayList getTiposSolicitud() throws Exception {
        ArrayList arl = dao.getTiposSolicitud();
        return arl;
    }

       /**
     * Busca los equipos de tipo
     * @param tipo tipo de  material (M, D,O)
     * @return ArrayList
     * @throws Exception cuando hay error
     */
    public ArrayList buscarMaterialesAAAE(String tipo) throws Exception {
        ArrayList cadena = new ArrayList();
        try {
            cadena = dao.buscarMaterialesAAAE(tipo);

        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error al listar datos: " + e.toString());
        }
        return cadena;
    }

     /**
     * Busca las instalaciones realacionadas a un equipo
     * @param equipo
     * @return ArrayList
     * @throws Exception cuando hay error
     */
    public ArrayList buscarInstalaciones(String equipo) throws Exception {
        ArrayList cadena = new ArrayList();
        try {
            cadena = dao.buscarInstalaciones(equipo);

        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error al listar datos: " + e.toString());
        }
        return cadena;
    }

    public double getPrecioVentaMaterial(String material, String fecha, String tipo_solicitud) throws Exception {
        return dao.getPrecioVentaMaterial(material, fecha, tipo_solicitud);
    }
    
    public String insertarEncuesta(BeanGeneral encuesta) throws Exception {
        return dao.insertarEncuenta(encuesta);
    }

    /**
     * Busca las ciudades de un departamento para solicitudes AAAE
     * @param departamento
     * @return ArrayList
     * @throws Exception cuando hay error
     */
    public ArrayList buscarCiudadesAAAE(String departamento) throws Exception {
        return dao.buscarCiudadesAAAE(departamento);
    }
    /**
     * Busca dato en tablagen
     * @param table_type a buscar
     * @param referencia a buscar
     * @return listado con datos coincidentes
     * @throws Exception cuando hay error
     */
    public ArrayList busquedaGeneralRef(String table_type, String referencia) throws Exception {
        return dao.busquedaGeneralRef(table_type, referencia);
    }

      /**
     * Busca los ejecutivos  AAAE
     * @return ArrayList
     * @throws Exception cuando hay error
     */
    public ArrayList buscarEjecutivosAAAE() throws Exception {
        return dao.buscarEjecutivosAAAE();
    }

     /**
     * Busca el contratista que debe realizar la proxima visita
     * @return contratista
     * @throws Exception cuando hay error
     */
     public String asignarContratista() throws Exception {
           return dao.asignarContratista();
     }

     /**
     * Busca el rango de horas de posible seleccion segun la hora actual
     * @return rango de horas
     * @throws Exception cuando hay error
     */
    public String rangoHoras() throws Exception {
        return dao.rangoHoras();
    }

    /**
     * Busca si un contratista puede realizar una visita en una fecha y hora determinada
     * @return boolean
     * @throws Exception cuando hay error
     */
    public boolean puedeRealizarVisita(String contratista, String fecha, String hora) throws Exception {
        return dao.puedeRealizarVisita(contratista, fecha, hora);
    }

    public String insertarVisita(BeanGeneral visita) throws Exception {
        return dao.insertarVisita(visita);
    }

      public double getPrecioMaterial(String material) throws Exception {
          return dao.getPrecioMaterial(material);
      }

       public double getValorFinanciado(double valor, int plazo) throws Exception {
          return dao.getValorFinanciado(valor, plazo);
      }

    /**
     * Lista las horas disponible de una fecha y contratista especifico
     * @return ArrayList
     * @throws Exception cuando hay error
     */
    public ArrayList cargarHorasDisponibles(String fecha, String contratista) throws Exception {
        return dao.cargarHorasDisponibles(fecha, contratista);
    }
    
    
    /**
     * Busca los equipos de una solicitud y su garantia
     * @param opd numero del formulario
     * @return ArrayList
     * @throws Exception cuando hay error
     */
    public ArrayList buscarGarantiaEquipos(String opd) throws Exception {
        return dao.buscarGarantiaEquipos(opd);
    }

    /**
     *   Inserta la garantia de un equipo
     * @throws Exception cuando hay error
     */
    public String insertarGarantiaEquipo(BeanGeneral garantia) throws Exception {
        return dao.insertarGarantiaEquipo(garantia);
    }

    /**
     *   Elimina las garatias de equipo de una solicitud
     * @throws Exception cuando hay error
     */
    public String eliminarGarantias(String id_solicitud) throws Exception {
        return dao.eliminarGarantias(id_solicitud);
    }
}
