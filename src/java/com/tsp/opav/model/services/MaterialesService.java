/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.opav.model.services;
import com.tsp.opav.model.DAOS.*;
import com.tsp.opav.model.beans.*;
import java.util.*;
/**
 *
 * @author Rhonalf
 */
public class MaterialesService {

    private MaterialesDAO mdao;

    private ArrayList resultado;

    public MaterialesService(){
        mdao = new MaterialesDAO();
    }
    public MaterialesService(String dataBaseName){
        mdao = new MaterialesDAO(dataBaseName);
    }
public ArrayList getCategorias(String tipo) throws Exception{
        return mdao.getCategorias(tipo);
    }
    public void anularProducto(String codigo,String user){
        try{
            mdao.anularProducto(codigo, user);
        }
        catch(Exception e){
            System.out.println("Error en anular producto: "+e.toString());
        }
    }

    public void insertarProducto(String reg_status, String descripcion,double precio,String consec,String tipo,String medida,String usuario, int categoria, int subcategoria, int tiposubcategoria, String empaque, String alcance, String certificado, String ente_certificador,double valor_compra) {
         try{
             mdao.insertarProducto(reg_status, descripcion, precio, consec, tipo, medida, usuario, categoria, subcategoria, tiposubcategoria, empaque, alcance, certificado, ente_certificador,valor_compra);
         }
         catch(Exception e){
            System.out.println("Error en insertar producto: "+e.toString());
        }
     }


     public String contarProductos(){
         return mdao.contarProductos();
     }

      public ArrayList verTodos(){
         ArrayList res = null;
          try{
             res = mdao.verTodos();
         }
          catch(Exception e){
              System.out.println("Error en listar todos: "+e.toString());
          }
         return res;
      }

      public ArrayList verMateriales(){
         ArrayList res = null;
          try{
             res = mdao.listarMateriales();
         }
          catch(Exception e){
              System.out.println("Error en listar materiales: "+e.toString());
          }
         return res;
      }

      public ArrayList verMano(){
         ArrayList res = null;
          try{
             res = mdao.listarManos();
         }
          catch(Exception e){
              System.out.println("Error en listar manos: "+e.toString());
          }
         return res;
      }

      public ArrayList verOtros(){
         ArrayList res = null;
          try{
             res = mdao.listarOtros();
         }
          catch(Exception e){
              System.out.println("Error en listar otros: "+e.toString());
          }
         return res;
      }

      public ArrayList buscarPor(int filtro,String texto) throws Exception{
        ArrayList rx4 = new ArrayList();
        try{
            rx4 = mdao.buscarPor(filtro,texto);
            setResult(rx4);
        }
        catch(Exception e){
            System.out.println("Error en buscar por: "+e.toString());
            e.printStackTrace();
        }
        return rx4;
    }

    private void setResult(ArrayList r){
        this.resultado = r;
    }

    public ArrayList getResultado(){
        return this.resultado;
    }

    //091202
    public ArrayList cargaTipoMats() throws Exception {
        ArrayList rx4 = new ArrayList();
        try{
            rx4 = mdao.cargaTipo(1);
            if(rx4.size()<0) throw new Exception("No hay valores para la busqueda solicitada");
        }
        catch(Exception e){
            System.out.println("Error: "+e.toString());
            e.printStackTrace();
        }
        return rx4;
    }

    public ArrayList cargaTipoMano() throws Exception {
        ArrayList rx4 = new ArrayList();
        try{
            rx4 = mdao.cargaTipo(2);
             if(rx4.size()<0) throw new Exception("No hay valores para la busqueda solicitada");
        }
        catch(Exception e){
            System.out.println("Error: "+e.toString());
            e.printStackTrace();
        }
        return rx4;
    }

    public ArrayList cargaTipoOtros() throws Exception {
        ArrayList rx4 = new ArrayList();
        try{
            rx4 = mdao.cargaTipo(3);
            if(rx4.size()<0) throw new Exception("No hay valores para la busqueda solicitada");
        }
        catch(Exception e){
            System.out.println("Error : "+e.toString());
            e.printStackTrace();
        }
        return rx4;
    }

    public ArrayList buscarPor(int criterio) throws Exception {
        ArrayList rx4 = new ArrayList();
        try{
            rx4 = mdao.buscarPor(criterio);
            if(rx4.size()<0) throw new Exception("No hay valores para la busqueda solicitada");
        }
        catch(Exception e){
            System.out.println("Error : "+e.toString());
            e.printStackTrace();
        }
        return rx4;
    }
    public ArrayList buscarPorAnul(int filtro,String texto) throws Exception{
        ArrayList rx4 = new ArrayList();
        try{
            rx4 = mdao.buscarPorAnul(filtro,texto);
            setResult(rx4);
        }
        catch(Exception e){
            System.out.println("Error en buscar por anul.: "+e.toString());
            e.printStackTrace();
        }
        return rx4;
    }

     /**
 *
 * @param filtro
 * @param cadena
 * @param idcat
 * @param idsubcat
 * @param idtiposub
 * @return
 * @throws Exception
 * Created by: Jose Castro
 */
    public ArrayList buscarCerficadores() throws Exception {
        ArrayList rx4 = new ArrayList();
        try{
            rx4 = mdao.buscarCerficadores();
            if(rx4.size()<0) throw new Exception("No hay valores para la busqueda solicitada");
        }
        catch(Exception e){
            System.out.println("Error : "+e.toString());
            e.printStackTrace();
        }
        return rx4;
    }

    /**
 *
 * @param filtro
 * @param cadena
 * @param idcat
 * @param idsubcat
 * @param idtiposub
 * @return
 * @throws Exception
 * Created by: Jose Castro
 */
    public ArrayList buscarPorMatCat(int filtro,String cadena, int idcat, int idsubcat, int idtiposub) throws Exception {
        ArrayList rx4 = new ArrayList();
        try{
            rx4 = mdao.buscarPorMatCat(filtro, cadena, idcat, idsubcat, idtiposub,"M");
            if(rx4.size()<0) throw new Exception("No hay valores para la busqueda solicitada");
        }
        catch(Exception e){
            System.out.println("Error : "+e.toString());
            e.printStackTrace();
        }
        return rx4;
    }

    public ArrayList buscarPorMatCat(int filtro,String cadena, int idcat, int idsubcat, int idtiposub,String tipomat) throws Exception {
        ArrayList rx4 = new ArrayList();
        try{
            rx4 = mdao.buscarPorMatCat(filtro, cadena, idcat, idsubcat, idtiposub,tipomat);
            if(rx4.size()<0) throw new Exception("No hay valores para la busqueda solicitada");
        }
        catch(Exception e){
            System.out.println("Error : "+e.toString());
            e.printStackTrace();
        }
        return rx4;
    }
public String buscarPrecioMaterial(String idmaterial) throws Exception {
  return mdao.buscarPrecioMaterial(idmaterial);
}

    /**
 *
 * @param filtro
 * @param cadena
 * @param idcat
 * @param idsubcat
 * @param idtiposub
 * @return
 * @throws Exception
 * Created by: Jesus pinedo
 */
     public ArrayList  getMaterialesVariaciones() throws NullPointerException,Exception{
        ArrayList vm = new ArrayList();
        try{
            vm = mdao.getMaterialesVariaciones();
        }
        catch(Exception e){
            System.out.println("Error : "+e.toString());
            e.printStackTrace();
        }
        return vm;
    }



     /**
     *
     * @param
     * @return  materiales con variacion en precios
     * @throws NullPointerException
     * @throws Exception
     * Created by: Jesus pinedo
     */
     public void ActualizarlistaPrecios(String [] materiales,String [] obs,String [] act_precio)throws Exception
     {

         try
         {
             mdao.ActualizarlistaPrecios(materiales, obs, act_precio);
         }
         catch(Exception e){
             System.out.println("Error : "+e.toString());
             e.printStackTrace();
         }
     }



          /**
     *
     * @param
     * @return  materiales con variacion en precios
     * @throws NullPointerException
     * @throws Exception
     * Created by: Jesus pinedo
     */
     public void ActualizarlistaPreciosInactividad(String [] materiales,String [] precios,String [] act_precio)throws Exception
     {

         try
         {
             mdao.ActualizarlistaPreciosInactividad(materiales, precios, act_precio);
         }
         catch(Exception e){
             System.out.println("Error : "+e.toString());
             e.printStackTrace();
         }
     }



      /* @param filtro
     * @param cadena
     * @param idcat
     * @param idsubcat
     * @param idtiposub
     * @return
     * @throws Exception
     * Created by: Jesus pinedo
     */
     public ArrayList  getMaterialesInactividad() throws NullPointerException,Exception{
        ArrayList vm = new ArrayList();
        try{
            vm = mdao.getMaterialesInactividad();
        }
        catch(Exception e){
            System.out.println("Error : "+e.toString());
            e.printStackTrace();
        }
        return vm;
    }
}