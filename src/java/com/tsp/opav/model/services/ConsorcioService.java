/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.opav.model.services;


/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */



import com.tsp.opav.model.beans.*;
import com.tsp.opav.model.ModelOpav;
import com.tsp.operation.model.beans.SerieGeneral;
import com.tsp.operation.model.services.SerieGeneralService;

import java.util.*;
import java.util.Date;
import java.io.*;

import java.sql.*;
import com.tsp.opav.model.DAOS.*;

import com.tsp.util.Util;
import com.tsp.util.LogWriter;
import java.text.*;
import com.aspose.cells.*;
import com.tsp.opav.model.beans.AccionPorSolicitud;
import com.tsp.opav.model.beans.Contratista;
import com.tsp.opav.model.beans.FacturaCabecera;
import com.tsp.opav.model.beans.FacturaConsorcio;
import com.tsp.opav.model.beans.FacturaDetalle;
import com.tsp.opav.model.beans.FacturaGeneral;
import com.tsp.opav.model.beans.ImpuestoContrato;
import com.tsp.opav.model.beans.PuntosFinanciacion;
import com.tsp.opav.model.beans.SolicitudPorFacturar;
import com.tsp.opav.model.beans.SubclientePorFacturar;
import com.tsp.opav.model.beans.SubclientePorSolicitud;
import com.tsp.opav.model.beans.TotalesConsorcio;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.ProveedorService;







/**
 *
 * @author Alvaro
 */
public class ConsorcioService {

    private ConsorcioDAO consorcioDao;
    private SerieGeneralService serieGeneralService;
    private ConstanteService constanteService;

    private SolicitudPorFacturar solicitud;
    private List listaDetalleAcciones;
    private List listaAccionesContratista;
    private Contratista contratista;
    private ImpuestoConsorcio impuestoContratista;
    ProveedorService proveedorService;



    // -------------------------------------------------------------------------
    // DEFINICION PARA USAR EXCEL
    // Variables del archivo de excel

    AsposeUtil xlsUtil = new AsposeUtil();
    Workbook   wb;
    String     rutaInformes;
    String     nombre;

    Style header  , titulo1, titulo2, titulo3 , titulo4, titulo5, letra, numero,  numeroCentrado;
    Style dinero, dineroMarron, dineroAzul, dineroVerde;
    Style porcentaje, letraCentrada, numeroNegrita, ftofecha;
    Style dineroSinCentavo, dineroSinCentavoMarron , dineroSinCentavoAzul, dineroSinCentavoVerde;
    Style idMarron, idAzul, idVerde, idLetra;
    Style letraMarron, letraAzul, letraVerde;
    Style gtLetra, gtDineroSinCentavo, gtId, fLetra, fDineroSinCentavo, fId, T1Letra ;

    Hashtable colorPersonalizado = new Hashtable();

    int fila = 0;
    // FIN DEFINICION PARA USAR EXCEL
    // -------------------------------------------------------------------------


    // Variables
    private SimpleDateFormat fmt;



    /** Creates a new instance of ConsorcioService */
    public ConsorcioService() {
        consorcioDao = new ConsorcioDAO();
        impuestoContratista = new ImpuestoConsorcio(5);
        serieGeneralService = new SerieGeneralService();
        constanteService = new ConstanteService();
        proveedorService = new ProveedorService();
    }
    public ConsorcioService(String dataBaseName) {
        consorcioDao = new ConsorcioDAO(dataBaseName);
        impuestoContratista = new ImpuestoConsorcio(5);
        serieGeneralService = new SerieGeneralService(dataBaseName);
        constanteService = new ConstanteService(dataBaseName);
        proveedorService = new ProveedorService(dataBaseName);
    }



    /** Crea una lista de ofertas pendientes de facturar a los clientes */
    public void buscaSolicitudPorFacturar()throws SQLException{
        consorcioDao.buscaSolicitudPorFacturar();
    }


    public List getSolicitudPorFacturar() throws SQLException{
        return consorcioDao.getSolicitudPorFacturar();
    }


    /** Busca el objeto OfertaPorFacturar correspondiente a la oferta seleccionada */
    public void buscarSolicitudPorFacturar(String id_solicitud, int parcial){

        try {
            List listaSolicitudPorFacturar = getSolicitudPorFacturar();

            solicitud = null;

            Iterator it = listaSolicitudPorFacturar.iterator();
            while (it.hasNext()) {
                 solicitud = (SolicitudPorFacturar)it.next();

                 if( (id_solicitud.equalsIgnoreCase(solicitud.getId_solicitud()) ) &&
                     (parcial == solicitud.getParcial() )    ) {
                     break;
                 }
            }
        }
        catch (Exception e) {
             Util.imprimirTrace(e);
        }


    }


    public SolicitudPorFacturar getSolicitud() {
        return solicitud;
    }


    /** Crea una lista de subclientes que estan asociados a una solicitud
     *
     *
     */
    public void buscaSubclientePorSolicitud(String id_solicitud, int parcial, String fechaInicioPago)throws SQLException{

        consorcioDao.buscaSubclientePorSolicitud(id_solicitud, parcial);



        try {
            List listaSubclientePorSolicitud = consorcioDao.getSubclientePorSolicitud();

            SubclientePorSolicitud subclientePorSolicitud = null;

            Iterator it = listaSubclientePorSolicitud.iterator();
            while (it.hasNext()) {
                 subclientePorSolicitud = (SubclientePorSolicitud)it.next();
                 subclientePorSolicitud.setFecha_inicio_pago(fechaInicioPago);
            }

        }
        catch (Exception e) {
            Util.imprimirTrace(e);
        }

    }


   /** Retorna una lista de subclientes que estan asociados a una solicitud */
    public List getSubclientePorSolicitud() throws SQLException{
        return consorcioDao.getSubclientePorSolicitud();
    }


    /** Crea una lista de acciones que estan asociados a una solicitud */
    public void buscaAccionPorSolicitud(String id_solicitud, int parcial)throws SQLException{
        consorcioDao.buscaAccionPorSolicitud(id_solicitud, parcial);
    }


   /** Retorna una lista de acciones que estan asociados a una solicitud */
    public List getAccionPorSolicitud() throws SQLException{
        return consorcioDao.getAccionPorSolicitud();
    }



    public void CalculaFinanciacion() throws SQLException  {

        List listaAccionPorSolicitud = this.getAccionPorSolicitud();

        // Totaliza los valores a financiar de cada accion



        AccionPorSolicitud accion = null;
        double total_a_financiar = 0.00;
        double total_a_financiar_sin_iva = 0.00;
        double total_iva =  0.00;


        Iterator it = listaAccionPorSolicitud.iterator();
        while (it.hasNext()) {
           accion = (AccionPorSolicitud)it.next();
           total_a_financiar         += accion.getValor_a_financiar();
           total_a_financiar_sin_iva += accion.getValor_a_financiar_sin_iva();
           total_iva                 += accion.getSubtotal_iva();
        }



        // Determina parametros para financiar

        SolicitudPorFacturar solicitudPorFacturar = this.getSolicitud();

        String ano = "";
        String mes = "";
        String trimestre = "";


        String regulacion      = solicitudPorFacturar.getRegulacion();
        double porcentaje_mora = solicitudPorFacturar.getPorcentaje_mora();
        double dtf_semana = 0.00 ;
        int cuotas_reales = 0;
        double financiacion_fintra = 0.00;
        PuntosFinanciacion puntosFinanciacion = null;
        double dtf_puntos  = 0.00;
        String tipo_puntos = "";
        double cuota_pago = 0;
        double valor_con_financiacion = 0.00;
        String tipo_distribucion=solicitudPorFacturar.getTipo_distribucion().trim();





        // Actualiza el valor a financiar para cada subcliente.
        SubclientePorSolicitud subcliente = null;
        List listaSubclientePorSolicitud = consorcioDao.getSubclientePorSolicitud();
        ListIterator lit = listaSubclientePorSolicitud.listIterator();

        int numero_subclientes = listaSubclientePorSolicitud.size();
        int i = 0;

        TotalesConsorcio totalesConsorcio = new TotalesConsorcio();

        totalesConsorcio.setInicializar(total_a_financiar_sin_iva,"subtotal_base_1" ,"TOTAL");
        totalesConsorcio.setInicializar(total_iva,"base_iva" ,"TOTAL");



        while (lit.hasNext()) {

           i++;
           subcliente  = (SubclientePorSolicitud) lit.next();

           calculosPorSubcliente(subcliente, lit, ano,  mes, trimestre, regulacion, porcentaje_mora,
                                 total_a_financiar_sin_iva, total_iva, totalesConsorcio,
                                 numero_subclientes, i,tipo_distribucion);
        }
    }



public void calculosPorSubcliente (SubclientePorSolicitud subcliente, ListIterator lit,
                                   String ano, String mes, String trimestre,
                                   String regulacion, double porcentaje_mora,
                                   double total_a_financiar_sin_iva,
                                   double total_iva, TotalesConsorcio totalesConsorcio,
                                   int numero_subclientes, int i, String tipo_distrubucion) throws SQLException {


       PuntosFinanciacion puntosFinanciacion = null;
       double valor = 0.00;



       double dtf_semana = subcliente.getDtf_semana();

       subcliente.getFecha_financiacion();
       ano = subcliente.getFecha_financiacion().substring(0,4);
       mes = subcliente.getFecha_financiacion().substring(5,7);
       trimestre = getTrimestre(mes);

       // Valor de la solicitud sin iva = Valor contratista + bonificacion + comisiones
       subcliente.setValor_sin_iva(total_a_financiar_sin_iva);
       // Valor de la solicitud sin iva distribuido entre los subclientes a traves del porcentaje que le corresponde a cada uno de ellos
       valor = Util.redondear2(subcliente.getValor_sin_iva() * subcliente.getPorcentaje_base()/100,0);
       subcliente.setSubtotal_base_1 ( totalesConsorcio.getValor(valor, "subtotal_base_1", numero_subclientes, i ));


       // Porcentaje de incremento trimestral por pagos extemporaneo
       subcliente.setPorcentaje_incremento(porcentaje_mora);


       // Porcentaje de incremento por extemporaneidad considerando el numero de meses de mora por cada subcliente
       // subcliente.setPorcentaje_extemporaneo(getTasaExtemporanea() * subcliente.getMeses_incremento()  /100 );  // 20100618
       subcliente.setPorcentaje_extemporaneo(porcentaje_mora * subcliente.getMeses_incremento() /100 );//20100618


       // Valor extemporaneo 1 correspondiente al valor sin iva para cada subcliente
       subcliente.setValor_extemporaneo_1( Util.redondear2( subcliente.getSubtotal_base_1() *   subcliente.getPorcentaje_extemporaneo(),0)  );

       // Valor de la solicitud sin iva mas el valor extemporaneo de cada subcliente
       subcliente.setSubtotal_base_2( subcliente.getSubtotal_base_1() + subcliente.getValor_extemporaneo_1()  );
       // Valor de la solicitud sin iva mas el valor extemporaneo de cada subcliente menos el valor de la cuota inicial
       subcliente.setSubtotal_base_3( subcliente.getSubtotal_base_2() - subcliente.getValor_cuota_inicial()  );

       // Valor iva total de la solicitud
       subcliente.setValor_iva(total_iva);
       // Valor iva distribuido por cada subcliente
       valor = Util.redondear2(subcliente.getValor_iva() * subcliente.getPorcentaje_base()/100,0) ;
       subcliente.setValor_base_iva ( totalesConsorcio.getValor(valor, "base_iva", numero_subclientes, i ));




       // Valor extemporaneo 2 correspondiente al iva de cada subcliente
       subcliente.setValor_extemporaneo_2( Util.redondear2( subcliente.getValor_iva()/numero_subclientes  * subcliente.getPorcentaje_extemporaneo() ,0)  );

       // Valor del iva mas valor extemporaneo para cada subcliente
       subcliente.setSubtotal_iva( subcliente.getValor_base_iva() + subcliente.getValor_extemporaneo_2() ) ;

       // Valor a financiar
       subcliente.setSubtotal_a_financiar(subcliente.getSubtotal_base_3() + subcliente.getSubtotal_iva() );

       // Valor extemporaneo total
       subcliente.setValor_extemporaneo(subcliente.getValor_extemporaneo_1() + subcliente.getValor_extemporaneo_2() );



       // Calculo de la cuota de pago
       int cuotas_reales = subcliente.getPeriodo();
       double valorAFinanciar = subcliente.getSubtotal_a_financiar();
       puntosFinanciacion  = getPuntosFinanciacion(tipo_distrubucion.equals("43")?"AIRES":"NUEVO", regulacion, valorAFinanciar, ano,
                                                    trimestre, cuotas_reales,subcliente.getEdificio());
       double dtf_puntos = puntosFinanciacion.getPuntos();
       String tipo_puntos= puntosFinanciacion.getTipo();

       subcliente.setPuntos(dtf_puntos);
       subcliente.setTipo(tipo_puntos);
       subcliente.setClase_dtf(puntosFinanciacion.getClase_dtf());



       if(subcliente.getTipo().equalsIgnoreCase("MAXIMA")) {
           subcliente.setPorcentaje_interes(subcliente.getPuntos());
       }
       else
       {
            subcliente.setPorcentaje_interes(dtf_semana + subcliente.getPuntos());
       }

       double cuota_pago = 0.00;

       // Valor de la cuota

       if (cuotas_reales == 1){
            cuota_pago = valorAFinanciar;
       }
       else {
           if (tipo_puntos.equalsIgnoreCase("MAXIMA")){
             cuota_pago  = Util.redondear2( get_anualidad(valorAFinanciar ,dtf_puntos , cuotas_reales ) , 0);
           }
           else {
             cuota_pago  = Util.redondear2( get_anualidad_dtf(valorAFinanciar , dtf_semana + dtf_puntos , cuotas_reales ) , 0);
           }
       }


       subcliente.setValor_cuota(cuota_pago);

       double valor_con_financiacion = cuota_pago * cuotas_reales;
       subcliente.setValor_con_financiacion(valor_con_financiacion);
       subcliente.setIntereses(valor_con_financiacion - valorAFinanciar );



       // Valor calculo de interes adicional por normalizacion


       if (cuotas_reales > 1){

           String fecha_inicio_pago = subcliente.getFecha_inicio_pago();            //Fecha inicio de pago
           double intereses_normalizados = 0.00;

           if(!fecha_inicio_pago.equals("0099-01-01")) {
               java.util.Date hoy = new Date();                                     //Fecha de hoy
               Calendar calendarFechaInicioPago = Calendar.getInstance();
               String anoInicioPago = fecha_inicio_pago.substring(0,4);
               String mesInicioPago = fecha_inicio_pago.substring(5,7);
               String diaInicioPago = fecha_inicio_pago.substring(8,10);
               calendarFechaInicioPago.set(Integer.parseInt(anoInicioPago),Integer.parseInt(mesInicioPago)-1,Integer.parseInt(diaInicioPago),8,0,0);

               final long MILLSECS_PER_DAY = 24 * 60 * 60 * 1000;                   //Milisegundos al d�a
               java.sql.Date fecha = new java.sql.Date(calendarFechaInicioPago.getTimeInMillis());
               long diferencia = ( ( fecha.getTime() - hoy.getTime() ) / MILLSECS_PER_DAY ) - 30 + 1;
               double periodo_normalizado = diferencia/30.00;

               if (diferencia > 0) {

                   double tasaMaxima = consorcioDao.getTasaMaxima(ano + mes);

                   double cuota_pago_normalizada  = Util.redondear2( get_anualidad(valorAFinanciar ,tasaMaxima , periodo_normalizado ) , 0);
                   double valor_con_financiacion_normalizado = cuota_pago_normalizada * periodo_normalizado;
                   intereses_normalizados = Util.redondear2( (valor_con_financiacion_normalizado - valorAFinanciar), 0 ) ;

                   subcliente.setIntereses(subcliente.getIntereses() + intereses_normalizados );

                   // 20101007
                   subcliente.setValor_con_financiacion(subcliente.getValor_con_financiacion() + intereses_normalizados) ;
                   subcliente.setValor_cuota( Util.redondear2( subcliente.getValor_con_financiacion() / subcliente.getPeriodo() , 0 ) ) ;
                   // 20101007


                   // System.out.println("fecha_inicio_pago : " + fecha_inicio_pago);
                   // System.out.println("diferencia : " + diferencia);
                   // System.out.println("periodo_normalizado : " + periodo_normalizado);
                   // System.out.println("tasaMaxima : " + tasaMaxima);
                   // System.out.println("cuota_pago_normalizada : " + cuota_pago_normalizada);
                   // System.out.println("valor_con_financiacion_normalizado : " + valor_con_financiacion_normalizado);
                   // System.out.println("valorAFinanciar : " + valorAFinanciar);
                   // System.out.println("intereses_normalizados : " + intereses_normalizados);


               }

           }
      }


      lit.set(subcliente);

}



/**
 * Localiza la tasa extemporanea
 */
public double getTasaExtemporanea()throws SQLException{

    return consorcioDao.getTasaExtemporanea();
}




public String  setAcciones(AccionPorSolicitud accion,  String last_update, String usuario) throws SQLException   {
    return  consorcioDao.setAcciones(accion,last_update, usuario );

}


public String setSubclientesOfertas( SubclientePorSolicitud subcliente, String simbolo_variable,
                                     String observacion_subcliente, String fecha_factura, String usuario)throws SQLException{
    return consorcioDao.setSubclientesOfertas(subcliente, simbolo_variable, observacion_subcliente, fecha_factura, usuario);
}



    public String ejecutarBatchSQL(Vector comandosSQL) throws SQLException {

        try {
            consorcioDao.ejecutarSQL(comandosSQL);
        }
        catch (Exception e) {
            return "ERROR";
        }
        return "";

    }


    public String ejecutarBatchSQL(Vector comandosSQL, LogWriter logWriter) throws SQLException {

        try {
            consorcioDao.ejecutarSQL(comandosSQL, logWriter);
        }
        catch (Exception e) {
            return "ERROR";
        }
        return "";

    }




    public String ejecutarBatchSQL(Vector comandosSQL, String dataBaseName) throws SQLException {

        try {
            consorcioDao.ejecutarSQL(comandosSQL, dataBaseName);
        }
        catch (Exception e) {
            return "ERROR";
        }
        return "";

    }



    public String ejecutarBatchSQL(Vector comandosSQL, LogWriter logWriter, String dataBaseName) throws SQLException {

        try {
            consorcioDao.ejecutarSQL(comandosSQL, logWriter, dataBaseName);
        }
        catch (Exception e) {
            return "ERROR";
        }
        return "";

    }







    /** Crea una lista de solicitudes pendientes de facturar a los clientes */
    public void buscaSolicitudParcialPorFacturar()throws SQLException{
        consorcioDao.buscaSolicitudParcialPorFacturar();
    }

    /** Retorna una lista de solicitudes pendientes de facturar a los clientes */
    public List getSolicitudParcialPorFacturar() throws SQLException{
        return consorcioDao.getSolicitudParcialPorFacturar();
    }

    public void buscaSubclientePorFacturar(String id_solicitud, int parcial)throws SQLException{
        consorcioDao.buscaSubclientePorFacturar(id_solicitud, parcial);
    }


    /** Retorna una lista de subclientes con solicitudes pendientes de facturar a los clientes */
    public List getSubclientePorFacturar() throws SQLException{
        return consorcioDao.getSubclientePorFacturar();
    }



    public String insertarFacturaConsorcio(FacturaClienteCuota facturaClienteCuota, String fecha_vencimiento)throws SQLException{
        return consorcioDao.insertarFacturaConsorcio(facturaClienteCuota,  fecha_vencimiento);
    }


    public String actualizarFacturaConsorcio(FacturaClienteCuota facturaClienteCuota , String numeroFacturaCreadaLocalizada )throws SQLException{
        return consorcioDao.actualizarFacturaConsorcio(facturaClienteCuota, numeroFacturaCreadaLocalizada );
    }






    /**
     * Crea un registro en la tabla factura consorcio con el objeto de informar a Corficolombiana
     * @param subclientePorFacturar
     * @param facturaConsorcio
     * @param documento String Numero de factura con la nomenclatura de la serie del Consorcio
     * @param documentoInicial String Numero de factura inicialmente creado con nomenclatura NM y que corresponde a serie de Fintra,
     *        y con la cual se informo a Corficolombiana. Si ha sido creada este documentoInicial va en vacio.
     * @param numeroCuota
     * @param totalesConsorcio
     * @param item
     * @param listaComandoSQL
     * @param creation_date
     * @param login
     * @param descripcion_factura
     * @param listaCabecera
     * @return
     * @throws SQLException
     */
    public String creaFacturaFiducia(SubclientePorFacturar subclientePorFacturar,
                                   FacturaConsorcio facturaConsorcio,
                                   String documento, String documentoInicial, int numeroCuota,
                                   TotalesConsorcio totalesConsorcio,
                                   ItemFacturaCliente item,
                                   Vector listaComandoSQL,
                                   String creation_date, String login, String descripcion_factura,
                                   Vector listaCabecera) throws SQLException {

        
            String CUENTA_CONCEPTO_CONTRATISTA ="";
            String CUENTA_CONCEPTO_BONIFICACION ="";
            String CUENTA_CONCEPTO_OPAV ="";
            String CUENTA_CONCEPTO_INTERVENTORIA="";
            String CUENTA_CONCEPTO_PROVINTEGRAL="";
            String CUENTA_CONCEPTO_ECA ="";
            String CUENTA_CONCEPTO_FINTRA="";
            String CUENTA_CONCEPTO_EXTEMPORANEO_1 ="";
            String CUENTA_CONCEPTO_EXTEMPORANEO_2 ="";
            String CUENTA_CONCEPTO_INTERES ="";
            String CUENTA_CONCEPTO_CUOTA_INICIAL ="";

            String CUENTA_CONCEPTO_IVA_CONTRATISTA  ="";
            String CUENTA_CONCEPTO_IVA_BONIFICACION  ="";
            String CUENTA_CONCEPTO_IVA_OPAV  ="";
            String CUENTA_CONCEPTO_IVA_INTERVENTORIA  ="";
            String CUENTA_CONCEPTO_IVA_PROVINTEGRAL ="";
            String CUENTA_CONCEPTO_IVA_ECA ="";
            String CUENTA_CONCEPTO_IVA_FINTRA  ="";
     
            // Definicion de las cuentas para cada concepto
        if (subclientePorFacturar.getNum_os().subSequence(0, 4).equals("ACON") && subclientePorFacturar.getTipo_distribucion().trim().equals("43")) {
            //si es aires...
             CUENTA_CONCEPTO_CONTRATISTA = constanteService.getValor("FINV", "AIRES_CUENTA_CONCEPTO_CONTRATISTA", "");//SI
            // CUENTA_CONCEPTO_BONIFICACION = constanteService.getValor("FINV", "CUENTA_CONCEPTO_BONIFICACION", "");
             CUENTA_CONCEPTO_OPAV = constanteService.getValor("FINV", "AIRES_CUENTA_CONCEPTO_OPAV", "");//SI
            // CUENTA_CONCEPTO_INTERVENTORIA = constanteService.getValor("FINV", "CUENTA_CONCEPTO_INTERVENTORIA", "");
            // CUENTA_CONCEPTO_PROVINTEGRAL = constanteService.getValor("FINV", "CUENTA_CONCEPTO_PROVINTEGRAL", "");
             CUENTA_CONCEPTO_ECA = constanteService.getValor("FINV", "AIRES_CUENTA_CONCEPTO_ECA", "");//SI
             CUENTA_CONCEPTO_FINTRA = constanteService.getValor("FINV", "AIRES_CUENTA_CONCEPTO_FINTRA", "");//SI
            // CUENTA_CONCEPTO_EXTEMPORANEO_1 = constanteService.getValor("FINV", "CUENTA_CONCEPTO_EXTEMPORANEO_1", "");
            // CUENTA_CONCEPTO_EXTEMPORANEO_2 = constanteService.getValor("FINV", "CUENTA_CONCEPTO_EXTEMPORANEO_2", "");
             CUENTA_CONCEPTO_INTERES = constanteService.getValor("FINV", "AIRES_CUENTA_CONCEPTO_INTERES", "");//SI
             CUENTA_CONCEPTO_CUOTA_INICIAL = constanteService.getValor("FINV", "AIRES_CUENTA_CONCEPTO_CUOTA_INICIAL", "");//SI

             CUENTA_CONCEPTO_IVA_CONTRATISTA = constanteService.getValor("FINV", "AIRES_CUENTA_CONCEPTO_IVA_CONTRATISTA", "");//SI
            // CUENTA_CONCEPTO_IVA_BONIFICACION = constanteService.getValor("FINV", "CUENTA_CONCEPTO_IVA_BONIFICACION", "");
             CUENTA_CONCEPTO_IVA_OPAV = constanteService.getValor("FINV", "AIRES_CUENTA_CONCEPTO_IVA_OPAV", "");//SI
            // CUENTA_CONCEPTO_IVA_INTERVENTORIA = constanteService.getValor("FINV", "CUENTA_CONCEPTO_IVA_INTERVENTORIA", "");
            // CUENTA_CONCEPTO_IVA_PROVINTEGRAL = constanteService.getValor("FINV", "CUENTA_CONCEPTO_IVA_PROVINTEGRAL", "");
             CUENTA_CONCEPTO_IVA_ECA = constanteService.getValor("FINV", "AIRES_CUENTA_CONCEPTO_IVA_ECA", "");//SI
             CUENTA_CONCEPTO_IVA_FINTRA = constanteService.getValor("FINV", "AIRES_CUENTA_CONCEPTO_IVA_FINTRA", "");//SI

        } else {
            
             CUENTA_CONCEPTO_CONTRATISTA = constanteService.getValor("FINV", "CUENTA_CONCEPTO_CONTRATISTA", "");
             CUENTA_CONCEPTO_BONIFICACION = constanteService.getValor("FINV", "CUENTA_CONCEPTO_BONIFICACION", "");
             CUENTA_CONCEPTO_OPAV = constanteService.getValor("FINV", "CUENTA_CONCEPTO_OPAV", "");
             CUENTA_CONCEPTO_INTERVENTORIA = constanteService.getValor("FINV", "CUENTA_CONCEPTO_INTERVENTORIA", "");
             CUENTA_CONCEPTO_PROVINTEGRAL = constanteService.getValor("FINV", "CUENTA_CONCEPTO_PROVINTEGRAL", "");
             CUENTA_CONCEPTO_ECA = constanteService.getValor("FINV", "CUENTA_CONCEPTO_ECA", "");
             CUENTA_CONCEPTO_FINTRA = constanteService.getValor("FINV", "CUENTA_CONCEPTO_FINTRA", "");
             CUENTA_CONCEPTO_EXTEMPORANEO_1 = constanteService.getValor("FINV", "CUENTA_CONCEPTO_EXTEMPORANEO_1", "");
             CUENTA_CONCEPTO_EXTEMPORANEO_2 = constanteService.getValor("FINV", "CUENTA_CONCEPTO_EXTEMPORANEO_2", "");
             CUENTA_CONCEPTO_INTERES = constanteService.getValor("FINV", "CUENTA_CONCEPTO_INTERES", "");
             CUENTA_CONCEPTO_CUOTA_INICIAL = constanteService.getValor("FINV", "CUENTA_CONCEPTO_CUOTA_INICIAL", "");

             CUENTA_CONCEPTO_IVA_CONTRATISTA = constanteService.getValor("FINV", "CUENTA_CONCEPTO_IVA_CONTRATISTA", "");
             CUENTA_CONCEPTO_IVA_BONIFICACION = constanteService.getValor("FINV", "CUENTA_CONCEPTO_IVA_BONIFICACION", "");
             CUENTA_CONCEPTO_IVA_OPAV = constanteService.getValor("FINV", "CUENTA_CONCEPTO_IVA_OPAV", "");
             CUENTA_CONCEPTO_IVA_INTERVENTORIA = constanteService.getValor("FINV", "CUENTA_CONCEPTO_IVA_INTERVENTORIA", "");
             CUENTA_CONCEPTO_IVA_PROVINTEGRAL = constanteService.getValor("FINV", "CUENTA_CONCEPTO_IVA_PROVINTEGRAL", "");
             CUENTA_CONCEPTO_IVA_ECA = constanteService.getValor("FINV", "CUENTA_CONCEPTO_IVA_ECA", "");
             CUENTA_CONCEPTO_IVA_FINTRA = constanteService.getValor("FINV", "CUENTA_CONCEPTO_IVA_FINTRA", "");
        }
        //hasta aqui voy a cambiar las cuentas para aires..

        String  CONCEPTO_CONTRATISTA                = constanteService.getValor("FINV", "CONCEPTO_CONTRATISTA", "");
        String  CONCEPTO_BONIFICACION               = constanteService.getValor("FINV", "CONCEPTO_BONIFICACION", "");
        String  CONCEPTO_OPAV                       = constanteService.getValor("FINV", "CONCEPTO_OPAV", "");
        String  CONCEPTO_INTERVENTORIA              = constanteService.getValor("FINV", "CONCEPTO_INTERVENTORIA", "");
        String  CONCEPTO_PROVINTEGRAL               = constanteService.getValor("FINV", "CONCEPTO_PROVINTEGRAL", "");
        String  CONCEPTO_ECA                        = constanteService.getValor("FINV", "CONCEPTO_ECA", "");
        String  CONCEPTO_FINTRA                     = constanteService.getValor("FINV", "CONCEPTO_FINTRA", "");
        String  CONCEPTO_EXTEMPORANEO_1             = constanteService.getValor("FINV", "CONCEPTO_EXTEMPORANEO_1", "");
        String  CONCEPTO_EXTEMPORANEO_2             = constanteService.getValor("FINV", "CONCEPTO_EXTEMPORANEO_2", "");
        String  CONCEPTO_INTERES                    = constanteService.getValor("FINV", "CONCEPTO_INTERES", "");
        String  CONCEPTO_CUOTA_INICIAL              = constanteService.getValor("FINV", "CONCEPTO_CUOTA_INICIAL", "");

        String  CONCEPTO_IVA_CONTRATISTA            = constanteService.getValor("FINV", "CONCEPTO_IVA_CONTRATISTA", "");
        String  CONCEPTO_IVA_BONIFICACION           = constanteService.getValor("FINV", "CONCEPTO_IVA_BONIFICACION", "");
        String  CONCEPTO_IVA_OPAV                   = constanteService.getValor("FINV", "CONCEPTO_IVA_OPAV", "");
        String  CONCEPTO_IVA_INTERVENTORIA          = constanteService.getValor("FINV", "CONCEPTO_IVA_INTERVENTORIA", "");
        String  CONCEPTO_IVA_PROVINTEGRAL           = constanteService.getValor("FINV", "CONCEPTO_IVA_PROVINTEGRAL", "");
        String  CONCEPTO_IVA_ECA                    = constanteService.getValor("FINV", "CONCEPTO_IVA_ECA", "");
        String  CONCEPTO_IVA_FINTRA                 = constanteService.getValor("FINV", "CONCEPTO_IVA_FINTRA", "");


        String error = "NO";
        double valor = 0.00;

        String comandoSQL = "";

        try {

            facturaConsorcio.setDocumento(documento);
            facturaConsorcio.setFecha_factura(subclientePorFacturar.getFecha_financiacion());


            // Calculo de la fecha de vencimiento de la factura

            DateFormat formato_fecha;
            formato_fecha = new SimpleDateFormat("yyyy-MM-dd");

            String fecha_factura_eca = facturaConsorcio.getFecha_factura();
            String fecha_inicio_pago = subclientePorFacturar.getFecha_inicio_pago();

            Calendar calendarFechaVencimiento = Calendar.getInstance();
            String ano = fecha_inicio_pago.substring(0,4);
            String mes = fecha_inicio_pago.substring(5,7);
            String dia = fecha_inicio_pago.substring(8,10);

            calendarFechaVencimiento.set(Integer.parseInt(ano),Integer.parseInt(mes)-1,Integer.parseInt(dia),8,0,0);
            calendarFechaVencimiento.add(Calendar.MONTH, numeroCuota);

            String fecha_vencimiento = formato_fecha.format(calendarFechaVencimiento.getTime()) ;


            facturaConsorcio.setFecha_vencimiento(fecha_vencimiento);
            facturaConsorcio.setId_cliente(subclientePorFacturar.getId_cliente() );
            facturaConsorcio.setId_solicitud(subclientePorFacturar.getId_solicitud() );
            facturaConsorcio.setNit(subclientePorFacturar.getNit());

            facturaConsorcio.setParcial(subclientePorFacturar.getParcial());


            valor = Util.redondear2(subclientePorFacturar.getVal_a_financiar() /  subclientePorFacturar.getPeriodo(),0) ;
            facturaConsorcio.setValor_capital(totalesConsorcio.getValor(valor,"valor_a_financiar",subclientePorFacturar.getPeriodo(), numeroCuota)  ) ;

            valor = Util.redondear2(subclientePorFacturar.getVal_con_financiacion() /  subclientePorFacturar.getPeriodo(),0) ;
            facturaConsorcio.setValor_factura(totalesConsorcio.getValor(valor,"valor_con_financiacion",subclientePorFacturar.getPeriodo(), numeroCuota)  ) ;

            facturaConsorcio.setValor_interes(facturaConsorcio.getValor_factura() - facturaConsorcio.getValor_capital());


            // Marca la factura del consorcio con el numero de factura de Fintra con la cual se informo a Corficolombiana
            // si no se habia hecho factura previamente se marca en
            if(documentoInicial.isEmpty()){
                facturaConsorcio.setDocumento_corficolombiana("");
            }
            else {
                facturaConsorcio.setDocumento_corficolombiana(documentoInicial);
            }

            facturaConsorcio.setDocumento_consorcio(documento);

            double  valor_factura = 0.00;



                String nit  = subclientePorFacturar.getNit();
                String id_cliente = subclientePorFacturar.getId_cliente();
                String observacionCambioNit = "";
                // Si la oferta es valor agregado se le debe facturar a Electricaribe
                String clienteValorAgregado = this.getClienteValorAgregado(item.getId_solicitud());
                if (clienteValorAgregado.equalsIgnoreCase("10838")) {
                    id_cliente = "10838" ;
                    nit = "8020076706";
                    observacionCambioNit = " Nit original: " + subclientePorFacturar.getNit() + " ";
                }


            // Items de la factura del cliente





            // Item valor contratista

            int k = 0 ;
            valor = Util.redondear2(item.getCosto_contratista()/subclientePorFacturar.getPeriodo(),0);
            if (numeroCuota == 1) {

                // Valor del contratista restando el valor de la bonificacion para la primera cuota
                valor = totalesConsorcio.getValor(valor,"item_contratista",subclientePorFacturar.getPeriodo(), numeroCuota)   ;
                valor = valor - item.getBonificacion();
                String descripcion = "Costo contratista ";
                comandoSQL = setFacturaDetalleCxC( "FINV", "FAC",  documento,
                                                getNumeroItemFactura(listaCabecera,subclientePorFacturar.getId_cliente(), documento, numeroCuota) +1,
                                                nit,
                                                CONCEPTO_CONTRATISTA, descripcion + "   Accion: " + item.getId_accion() +
                                                "  " + descripcion_factura + "   " + observacionCambioNit + item.getNombre_contratista(),
                                                CUENTA_CONCEPTO_CONTRATISTA, 1.0,
                                                valor , valor , valor,
                                                valor , 1.0, "PES",
                                                creation_date, login, creation_date,
                                                login, "COL", item.getContratista(), "ACC", item.getId_accion(),
                                                "SOL", item.getId_solicitud(), "PAR", Integer.toString(item.getParcial())  );
                listaComandoSQL.add(comandoSQL);
                totalizarCabeceraFactura(listaCabecera, subclientePorFacturar, documento, numeroCuota, valor, 0,"",0);


                // Valor de la bonificacion para la primera cuota


                if (valor != 0 && (!subclientePorFacturar.getNum_os().subSequence(0, 4).equals("ACON") && !subclientePorFacturar.getTipo_distribucion().trim().equals("43"))) {
                    valor = item.getBonificacion();
                    descripcion = "Bonificacion ";
                    comandoSQL = setFacturaDetalleCxC( "FINV", "FAC",  documento,
                                                    getNumeroItemFactura(listaCabecera,subclientePorFacturar.getId_cliente(), documento, numeroCuota) + 1,
                                                    nit,
                                                    CONCEPTO_BONIFICACION, descripcion + "   Accion: " + item.getId_accion() +
                                                    "  " + descripcion_factura + "   " + observacionCambioNit + item.getNombre_contratista(),
                                                    CUENTA_CONCEPTO_BONIFICACION, 1.0,
                                                    valor , valor , valor,
                                                    valor , 1.0, "PES",
                                                    creation_date, login, creation_date,
                                                    login, "COL", item.getContratista(), "ACC", item.getId_accion(),
                                                    "SOL", item.getId_solicitud(), "PAR", Integer.toString(item.getParcial()) );
                    listaComandoSQL.add(comandoSQL);
                    totalizarCabeceraFactura(listaCabecera, subclientePorFacturar, documento, numeroCuota, valor, 0,"",0);
                }
            }else {

                // Valor del contratista para las cuotas del 2 en adelante

                valor = totalesConsorcio.getValor(valor,"item_contratista",subclientePorFacturar.getPeriodo(), numeroCuota)   ;
                String descripcion = "Costo contratista ";
                comandoSQL = setFacturaDetalleCxC( "FINV", "FAC",  documento,
                                                getNumeroItemFactura(listaCabecera,subclientePorFacturar.getId_cliente(), documento, numeroCuota) + 1,
                                                nit,
                                                CONCEPTO_CONTRATISTA, descripcion + "   Accion: " + item.getId_accion() +
                                                "  " + descripcion_factura + "   " + observacionCambioNit + item.getNombre_contratista(),
                                                CUENTA_CONCEPTO_CONTRATISTA, 1.0,
                                                valor , valor , valor,
                                                valor , 1.0, "PES",
                                                creation_date, login, creation_date,
                                                login, "COL", item.getContratista(), "ACC", item.getId_accion(),
                                                "SOL", item.getId_solicitud(), "PAR", Integer.toString(item.getParcial()) );
                listaComandoSQL.add(comandoSQL);
                totalizarCabeceraFactura(listaCabecera, subclientePorFacturar, documento, numeroCuota, valor, 0,"",0);
            }


            // Item valor comision opav
            valor = Util.redondear2(item.getComision_opav()/subclientePorFacturar.getPeriodo(),0);
            if (valor != 0) {
                valor = totalesConsorcio.getValor(valor,"item_comision_opav",subclientePorFacturar.getPeriodo(), numeroCuota)   ;
                String descripcion = "Comision Opav ";
                int item_opav = getNumeroItemFactura(listaCabecera,subclientePorFacturar.getId_cliente(), documento, numeroCuota) + 1;
                comandoSQL = setFacturaDetalleCxC( "FINV", "FAC",  documento, item_opav,
                                                nit,
                                                CONCEPTO_OPAV, descripcion + "   Accion: " + item.getId_accion() +
                                                "  " + descripcion_factura + "   " + observacionCambioNit + item.getNombre_contratista(),
                                                CUENTA_CONCEPTO_OPAV, 1.0,
                                                valor , valor , valor,
                                                valor , 1.0, "PES",
                                                creation_date, login, creation_date,
                                                login, "COL", item.getContratista(), "ACC", item.getId_accion(),
                                                "SOL", item.getId_solicitud() , "PAR", Integer.toString(item.getParcial()) );
                listaComandoSQL.add(comandoSQL);
                totalizarCabeceraFactura(listaCabecera, subclientePorFacturar, documento, numeroCuota, valor, 0,"112",item_opav);
            }


            // Item valor comision fintra
            valor = Util.redondear2(item.getComision_fintra()/subclientePorFacturar.getPeriodo(),0);
            if (valor != 0) {
                valor = totalesConsorcio.getValor(valor,"item_comision_fintra",subclientePorFacturar.getPeriodo(), numeroCuota)   ;
                String descripcion = "Comision selectrik ";
                comandoSQL = setFacturaDetalleCxC( "FINV", "FAC",  documento,
                                                getNumeroItemFactura(listaCabecera,subclientePorFacturar.getId_cliente(), documento, numeroCuota) + 1,
                                                nit,
                                                CONCEPTO_FINTRA, descripcion + "   Accion: " + item.getId_accion() +
                                                "  " + descripcion_factura + "   " + observacionCambioNit + item.getNombre_contratista(),
                                                CUENTA_CONCEPTO_FINTRA, 1.0,
                                                valor , valor , valor,
                                                valor , 1.0, "PES",
                                                creation_date, login, creation_date,
                                                login, "COL", item.getContratista(), "ACC", item.getId_accion(),
                                                "SOL", item.getId_solicitud() , "PAR", Integer.toString(item.getParcial()) );
                listaComandoSQL.add(comandoSQL);
                totalizarCabeceraFactura(listaCabecera, subclientePorFacturar, documento, numeroCuota, valor, 0,"",0);
            }


            // Item valor comision interventoria
            valor = Util.redondear2(item.getComision_interventoria()/subclientePorFacturar.getPeriodo(),0);
            if (valor != 0 && (!subclientePorFacturar.getNum_os().subSequence(0, 4).equals("ACON") && !subclientePorFacturar.getTipo_distribucion().trim().equals("43"))) {
                valor = totalesConsorcio.getValor(valor,"item_comision_interventoria",subclientePorFacturar.getPeriodo(), numeroCuota)   ;
                String descripcion = "Comision interventoria ";
                comandoSQL = setFacturaDetalleCxC( "FINV", "FAC",  documento,
                                                getNumeroItemFactura(listaCabecera,subclientePorFacturar.getId_cliente(), documento, numeroCuota) + 1,
                                                nit,
                                                CONCEPTO_INTERVENTORIA, descripcion + "   Accion: " + item.getId_accion() +
                                                "  " + descripcion_factura + "   " + observacionCambioNit + item.getNombre_contratista(),
                                                CUENTA_CONCEPTO_INTERVENTORIA, 1.0,
                                                valor , valor , valor,
                                                valor , 1.0, "PES",
                                                creation_date, login, creation_date,
                                                login, "COL", item.getContratista(), "ACC", item.getId_accion(),
                                                "SOL", item.getId_solicitud() , "PAR", Integer.toString(item.getParcial()) );
                listaComandoSQL.add(comandoSQL);
                totalizarCabeceraFactura(listaCabecera, subclientePorFacturar, documento, numeroCuota, valor, 0,"",0);

            }


            // Item valor comision provintegral
            valor = Util.redondear2(item.getComision_provintegral()/subclientePorFacturar.getPeriodo(),0);
            if (valor != 0 && (!subclientePorFacturar.getNum_os().subSequence(0, 4).equals("ACON") && !subclientePorFacturar.getTipo_distribucion().trim().equals("43"))) {
                valor = totalesConsorcio.getValor(valor,"item_comision_provintegral",subclientePorFacturar.getPeriodo(), numeroCuota)   ;
                String descripcion = "Comision provintegral ";
                comandoSQL = setFacturaDetalleCxC( "FINV", "FAC",  documento,
                                                getNumeroItemFactura(listaCabecera,subclientePorFacturar.getId_cliente(), documento, numeroCuota) + 1,
                                                nit,
                                                CONCEPTO_PROVINTEGRAL, descripcion + "   Accion: " + item.getId_accion() +
                                                "  " + descripcion_factura + "   " + observacionCambioNit + item.getNombre_contratista(),
                                                CUENTA_CONCEPTO_PROVINTEGRAL, 1.0,
                                                valor , valor , valor,
                                                valor , 1.0, "PES",
                                                creation_date, login, creation_date,
                                                login, "COL", item.getContratista(), "ACC", item.getId_accion(),
                                                "SOL", item.getId_solicitud() , "PAR", Integer.toString(item.getParcial()) );
                listaComandoSQL.add(comandoSQL);
                totalizarCabeceraFactura(listaCabecera, subclientePorFacturar, documento, numeroCuota, valor, 0,"",0);

            }


            // Item valor comision eca
            valor = Util.redondear2(item.getComision_eca()/subclientePorFacturar.getPeriodo(),0);
            if (valor != 0) {
                valor = totalesConsorcio.getValor(valor,"item_comision_eca",subclientePorFacturar.getPeriodo(), numeroCuota)   ;
                String descripcion = "Comision eca ";
                comandoSQL = setFacturaDetalleCxC( "FINV", "FAC",  documento,
                                                getNumeroItemFactura(listaCabecera,subclientePorFacturar.getId_cliente(), documento, numeroCuota) + 1,
                                                nit,
                                                CONCEPTO_ECA, descripcion + "   Accion: " + item.getId_accion() +
                                                "  " + descripcion_factura + "   " + observacionCambioNit + item.getNombre_contratista(),
                                                CUENTA_CONCEPTO_ECA, 1.0,
                                                valor , valor , valor,
                                                valor , 1.0, "PES",
                                                creation_date, login, creation_date,
                                                login, "COL", item.getContratista(), "ACC", item.getId_accion(),
                                                "SOL", item.getId_solicitud(), "PAR", Integer.toString(item.getParcial()) );
                listaComandoSQL.add(comandoSQL);
                totalizarCabeceraFactura(listaCabecera, subclientePorFacturar, documento, numeroCuota, valor, 0,"",0);

            }


            // Item valor extemporaneo 1
            valor = Util.redondear2(item.getValor_extemporaneo_1()/subclientePorFacturar.getPeriodo(),0);
            if (valor != 0 && (!subclientePorFacturar.getNum_os().subSequence(0, 4).equals("ACON") && !subclientePorFacturar.getTipo_distribucion().trim().equals("43"))) {
                valor = totalesConsorcio.getValor(valor,"item_extemporaneo_1",subclientePorFacturar.getPeriodo(), numeroCuota)   ;
                String descripcion = "Valor extemporaneo 1 ";
                comandoSQL = setFacturaDetalleCxC( "FINV", "FAC",  documento,
                                                getNumeroItemFactura(listaCabecera,subclientePorFacturar.getId_cliente(), documento, numeroCuota) + 1,
                                                nit,
                                                CONCEPTO_EXTEMPORANEO_1, descripcion + "   Accion: " + item.getId_accion() +
                                                "  " + descripcion_factura + "   " + observacionCambioNit + item.getNombre_contratista(),
                                                CUENTA_CONCEPTO_EXTEMPORANEO_1, 1.0,
                                                valor , valor , valor,
                                                valor , 1.0, "PES",
                                                creation_date, login, creation_date,
                                                login, "COL", item.getContratista(), "ACC", item.getId_accion(),
                                                "SOL", item.getId_solicitud(), "PAR", Integer.toString(item.getParcial()) );
                listaComandoSQL.add(comandoSQL);
                totalizarCabeceraFactura(listaCabecera, subclientePorFacturar, documento, numeroCuota, valor, 0,"",0);

            }


            // Item valor cuota inicial
            valor = Util.redondear2(item.getCuota_inicial()/subclientePorFacturar.getPeriodo(),0);
            if (valor != 0) {
                valor = totalesConsorcio.getValor(valor,"item_cuota_inicial",subclientePorFacturar.getPeriodo(), numeroCuota)   ;
                String descripcion = "Cuota inicial ";
                comandoSQL = setFacturaDetalleCxC( "FINV", "FAC",  documento,
                                                getNumeroItemFactura(listaCabecera,subclientePorFacturar.getId_cliente(), documento, numeroCuota) + 1,
                                                nit,
                                                CONCEPTO_CUOTA_INICIAL, descripcion + "   Accion: " + item.getId_accion() +
                                                "  " + descripcion_factura + "   " + observacionCambioNit + item.getNombre_contratista(),
                                                CUENTA_CONCEPTO_CUOTA_INICIAL, 1.0,
                                                valor , valor , valor,
                                                valor , 1.0, "PES",
                                                creation_date, login, creation_date,
                                                login, "COL", item.getContratista(), "ACC", item.getId_accion(),
                                                "SOL", item.getId_solicitud(), "PAR", Integer.toString(item.getParcial()) );
                listaComandoSQL.add(comandoSQL);
                totalizarCabeceraFactura(listaCabecera, subclientePorFacturar, documento, numeroCuota, valor, 0,"",0);

            }


            // Item valor iva contratista
            valor = Util.redondear2(item.getIva_contratista()/subclientePorFacturar.getPeriodo(),0);
            if (valor != 0) {
                valor = totalesConsorcio.getValor(valor,"item_iva_contratista",subclientePorFacturar.getPeriodo(), numeroCuota)   ;
                String descripcion = "Iva contratista ";
                comandoSQL = setFacturaDetalleCxC( "FINV", "FAC",  documento,
                                                getNumeroItemFactura(listaCabecera,subclientePorFacturar.getId_cliente(), documento, numeroCuota) + 1,
                                                nit,
                                                CONCEPTO_IVA_CONTRATISTA, descripcion + "   Accion: " + item.getId_accion() +
                                                "  " + descripcion_factura + "   " + observacionCambioNit + item.getNombre_contratista(),
                                                CUENTA_CONCEPTO_IVA_CONTRATISTA, 1.0,
                                                valor , valor , valor,
                                                valor , 1.0, "PES",
                                                creation_date, login, creation_date,
                                                login, "COL", item.getContratista(), "ACC", item.getId_accion(),
                                                "SOL", item.getId_solicitud(), "PAR", Integer.toString(item.getParcial()) );
                listaComandoSQL.add(comandoSQL);
                totalizarCabeceraFactura(listaCabecera, subclientePorFacturar, documento, numeroCuota, valor, 0,"",0);

            }


            // Item iva bonificacion
            valor = Util.redondear2(item.getIva_bonificacion()/subclientePorFacturar.getPeriodo(),0);
            if (valor != 0 && (!subclientePorFacturar.getNum_os().subSequence(0, 4).equals("ACON") && !subclientePorFacturar.getTipo_distribucion().trim().equals("43"))) {
                valor = totalesConsorcio.getValor(valor,"item_iva_bonificacion",subclientePorFacturar.getPeriodo(), numeroCuota)   ;
                String descripcion = "Iva bonificacion ";
                comandoSQL = setFacturaDetalleCxC( "FINV", "FAC",  documento,
                                                getNumeroItemFactura(listaCabecera,subclientePorFacturar.getId_cliente(), documento, numeroCuota) + 1,
                                                nit,
                                                CONCEPTO_IVA_BONIFICACION, descripcion + "   Accion: " + item.getId_accion() +
                                                "  " + descripcion_factura + "   " + observacionCambioNit + item.getNombre_contratista(),
                                                CUENTA_CONCEPTO_IVA_BONIFICACION, 1.0,
                                                valor , valor , valor,
                                                valor , 1.0, "PES",
                                                creation_date, login, creation_date,
                                                login, "COL", item.getContratista(), "ACC", item.getId_accion(),
                                                "SOL", item.getId_solicitud(), "PAR", Integer.toString(item.getParcial()) );
                listaComandoSQL.add(comandoSQL);
                totalizarCabeceraFactura(listaCabecera, subclientePorFacturar, documento, numeroCuota, valor, 0,"",0);

            }


            // Item iva comision opav
            valor = Util.redondear2(item.getIva_comision_opav()/subclientePorFacturar.getPeriodo(),0);
            if (valor != 0) {
                valor = totalesConsorcio.getValor(valor,"item_iva_opav",subclientePorFacturar.getPeriodo(), numeroCuota)   ;
                String descripcion = "Iva opav ";
                comandoSQL = setFacturaDetalleCxC( "FINV", "FAC",  documento,
                                                getNumeroItemFactura(listaCabecera,subclientePorFacturar.getId_cliente(), documento, numeroCuota) + 1,
                                                nit,
                                                CONCEPTO_IVA_OPAV, descripcion + "   Accion: " + item.getId_accion() +
                                                "  " + descripcion_factura + "   " + observacionCambioNit + item.getNombre_contratista(),
                                                CUENTA_CONCEPTO_IVA_OPAV, 1.0,
                                                valor , valor , valor,
                                                valor , 1.0, "PES",
                                                creation_date, login, creation_date,
                                                login, "COL", item.getContratista(), "ACC", item.getId_accion(),
                                                "SOL", item.getId_solicitud(), "PAR", Integer.toString(item.getParcial()) );
                listaComandoSQL.add(comandoSQL);
                totalizarCabeceraFactura(listaCabecera, subclientePorFacturar, documento, numeroCuota, valor, 0,"",0);

            }


            // Item iva comision fintra
            valor = Util.redondear2(item.getIva_comision_fintra()/subclientePorFacturar.getPeriodo(),0);
            if (valor != 0) {
                valor = totalesConsorcio.getValor(valor,"item_iva_fintra",subclientePorFacturar.getPeriodo(), numeroCuota)   ;
                String descripcion = "Iva selectrik ";
                comandoSQL = setFacturaDetalleCxC( "FINV", "FAC",  documento,
                                                getNumeroItemFactura(listaCabecera,subclientePorFacturar.getId_cliente(), documento, numeroCuota) + 1,
                                                nit,
                                                CONCEPTO_IVA_FINTRA, descripcion + "   Accion: " + item.getId_accion() +
                                                "  " + descripcion_factura + "   " + observacionCambioNit + item.getNombre_contratista(),
                                                CUENTA_CONCEPTO_IVA_FINTRA, 1.0,
                                                valor , valor , valor,
                                                valor , 1.0, "PES",
                                                creation_date, login, creation_date,
                                                login, "COL", item.getContratista(), "ACC", item.getId_accion(),
                                                "SOL", item.getId_solicitud(), "PAR", Integer.toString(item.getParcial()) );
                listaComandoSQL.add(comandoSQL);
                totalizarCabeceraFactura(listaCabecera, subclientePorFacturar, documento, numeroCuota, valor, 0,"",0);

            }


            // Item iva comision interventoria
            valor = Util.redondear2(item.getIva_comision_interventoria()/subclientePorFacturar.getPeriodo(),0);
            if (valor != 0 && (!subclientePorFacturar.getNum_os().subSequence(0, 4).equals("ACON") && !subclientePorFacturar.getTipo_distribucion().trim().equals("43"))) {
                valor = totalesConsorcio.getValor(valor,"item_iva_interventoria",subclientePorFacturar.getPeriodo(), numeroCuota)   ;
                String descripcion = "Iva interventoria ";
                comandoSQL = setFacturaDetalleCxC( "FINV", "FAC",  documento,
                                                getNumeroItemFactura(listaCabecera,subclientePorFacturar.getId_cliente(), documento, numeroCuota) + 1,
                                                nit,
                                                CONCEPTO_IVA_INTERVENTORIA, descripcion + "   Accion: " + item.getId_accion() +
                                                "  " + descripcion_factura + "   " + observacionCambioNit + item.getNombre_contratista(),
                                                CUENTA_CONCEPTO_IVA_INTERVENTORIA, 1.0,
                                                valor , valor , valor,
                                                valor , 1.0, "PES",
                                                creation_date, login, creation_date,
                                                login, "COL", item.getContratista(), "ACC", item.getId_accion(),
                                                "SOL", item.getId_solicitud(), "PAR", Integer.toString(item.getParcial()) );
                listaComandoSQL.add(comandoSQL);
                totalizarCabeceraFactura(listaCabecera, subclientePorFacturar, documento, numeroCuota, valor, 0,"",0);

            }


            // Item iva comision provintegral
            valor = Util.redondear2(item.getIva_comision_provintegral()/subclientePorFacturar.getPeriodo(),0);
            if (valor != 0 && (!subclientePorFacturar.getNum_os().subSequence(0, 4).equals("ACON") && !subclientePorFacturar.getTipo_distribucion().trim().equals("43"))) {
                valor = totalesConsorcio.getValor(valor,"item_iva_provintegral",subclientePorFacturar.getPeriodo(), numeroCuota)   ;
                String descripcion = "Iva provintegral ";
                comandoSQL = setFacturaDetalleCxC( "FINV", "FAC",  documento,
                                                getNumeroItemFactura(listaCabecera,subclientePorFacturar.getId_cliente(), documento, numeroCuota) + 1,
                                                nit,
                                                CONCEPTO_IVA_PROVINTEGRAL, descripcion + "   Accion: " + item.getId_accion() +
                                                "  " + descripcion_factura + "   " + observacionCambioNit + item.getNombre_contratista(),
                                                CUENTA_CONCEPTO_IVA_PROVINTEGRAL, 1.0,
                                                valor , valor , valor,
                                                valor , 1.0, "PES",
                                                creation_date, login, creation_date,
                                                login, "COL", item.getContratista(), "ACC", item.getId_accion(),
                                                "SOL", item.getId_solicitud(), "PAR", Integer.toString(item.getParcial()) );
                listaComandoSQL.add(comandoSQL);
                totalizarCabeceraFactura(listaCabecera, subclientePorFacturar, documento, numeroCuota, valor, 0,"",0);

            }


            // Item iva comision eca
            valor = Util.redondear2(item.getIva_comision_eca()/subclientePorFacturar.getPeriodo(),0);
            if (valor != 0) {
                valor = totalesConsorcio.getValor(valor,"item_iva_eca",subclientePorFacturar.getPeriodo(), numeroCuota)   ;
                String descripcion = "Iva eca ";
                comandoSQL = setFacturaDetalleCxC( "FINV", "FAC",  documento,
                                                getNumeroItemFactura(listaCabecera,subclientePorFacturar.getId_cliente(), documento, numeroCuota) + 1,
                                                nit,
                                                CONCEPTO_IVA_ECA, descripcion + "   Accion: " + item.getId_accion() +
                                                "  " + descripcion_factura + "   " + observacionCambioNit + item.getNombre_contratista(),
                                                CUENTA_CONCEPTO_IVA_ECA, 1.0,
                                                valor , valor , valor,
                                                valor , 1.0, "PES",
                                                creation_date, login, creation_date,
                                                login, "COL", item.getContratista(), "ACC", item.getId_accion(),
                                                "SOL", item.getId_solicitud(), "PAR", Integer.toString(item.getParcial()) );
                listaComandoSQL.add(comandoSQL);
                totalizarCabeceraFactura(listaCabecera, subclientePorFacturar, documento, numeroCuota, valor, 0,"",0);


            }

            // Item valor extemporaneo 2
            valor = Util.redondear2(item.getValor_extemporaneo_2()/subclientePorFacturar.getPeriodo(),0);
            if (valor != 0 && (!subclientePorFacturar.getNum_os().subSequence(0, 4).equals("ACON") && !subclientePorFacturar.getTipo_distribucion().trim().equals("43"))) {
                valor = totalesConsorcio.getValor(valor,"item_extemporaneo_2",subclientePorFacturar.getPeriodo(), numeroCuota)   ;
                String descripcion = "Valor extemporaneo 2 ";
                comandoSQL = setFacturaDetalleCxC( "FINV", "FAC",  documento,
                                                getNumeroItemFactura(listaCabecera,subclientePorFacturar.getId_cliente(), documento, numeroCuota) + 1,
                                                nit,
                                                CONCEPTO_EXTEMPORANEO_2, descripcion + "   Accion: " + item.getId_accion() +
                                                "  " + descripcion_factura + "   " + observacionCambioNit + item.getNombre_contratista(),
                                                CUENTA_CONCEPTO_EXTEMPORANEO_2, 1.0,
                                                valor , valor , valor,
                                                valor , 1.0, "PES",
                                                creation_date, login, creation_date,
                                                login, "COL", item.getContratista(), "ACC", item.getId_accion(),
                                                "SOL", item.getId_solicitud(), "PAR", Integer.toString(item.getParcial()) );
                listaComandoSQL.add(comandoSQL);
                totalizarCabeceraFactura(listaCabecera, subclientePorFacturar, documento, numeroCuota, valor, 0,"",0);

            }


            // Item valor intereses
            valor = Util.redondear2(item.getIntereses()/subclientePorFacturar.getPeriodo(),0);
            if (valor != 0) {
                valor = totalesConsorcio.getValor(valor,"item_intereses",subclientePorFacturar.getPeriodo(), numeroCuota)   ;
                String descripcion = "Intereses ";
                int item_interes =  getNumeroItemFactura(listaCabecera,subclientePorFacturar.getId_cliente(), documento, numeroCuota) + 1;
                comandoSQL = setFacturaDetalleCxC( "FINV", "FAC",  documento,
                                                item_interes,
                                                nit,
                                                CONCEPTO_INTERES, descripcion + "   Accion: " + item.getId_accion() +
                                                "  " + descripcion_factura + "   " + observacionCambioNit + item.getNombre_contratista(),
                                                CUENTA_CONCEPTO_INTERES, 1.0,
                                                valor , valor , valor,
                                                valor , 1.0, "PES",
                                                creation_date, login, creation_date,
                                                login, "COL", item.getContratista(), "ACC", item.getId_accion(),
                                                "SOL", item.getId_solicitud() , "PAR", Integer.toString(item.getParcial()) );
                listaComandoSQL.add(comandoSQL);
                totalizarCabeceraFactura(listaCabecera, subclientePorFacturar, documento, numeroCuota, valor,valor,"127",item_interes);

            }



        }
        catch ( Exception e) {
            error = "SI";
        }

        return error;
    }




    public void totalizarCabeceraFactura(Vector listaCabecera, SubclientePorFacturar subclientePorFacturar,
                                         String documento, int numeroCuota, double valor,
                                         double valor_intereses,
                                         String concepto, int item_concepto) {



        int longitud = listaCabecera.size();
        if (longitud > 0) {
            String facturaLocalizada = "NO";
            for (int i= 0; i<longitud; i++) {
                FacturaClienteCuota facturaClienteCuota = (FacturaClienteCuota) listaCabecera.elementAt(i);
                if (  (facturaClienteCuota.getId_cliente().equalsIgnoreCase(subclientePorFacturar.getId_cliente()) ) &
                      (facturaClienteCuota.getFactura().equalsIgnoreCase(documento) ) &
                      (facturaClienteCuota.getCuota() == numeroCuota)  )  {

                    facturaLocalizada = "SI";
                    facturaClienteCuota.setValor_factura(facturaClienteCuota.getValor_factura() + valor);
                    facturaClienteCuota.setValor_intereses(facturaClienteCuota.getValor_intereses() + valor_intereses);
                    facturaClienteCuota.setItem(facturaClienteCuota.getItem() + 1);


                    // Registra el valor total de la comision opav , el ultimo numero de item donde se registra el valor del opav, y el del interes.
                    if (concepto.equalsIgnoreCase("112")) {
                        facturaClienteCuota.setValor_opav(facturaClienteCuota.getValor_opav() + valor);
                        facturaClienteCuota.setItem_opav(item_concepto);
                    }
                    if (concepto.equalsIgnoreCase("127")) {
                        facturaClienteCuota.setItem_interes(item_concepto);
                    }







                    listaCabecera.setElementAt(facturaClienteCuota, i);
                    break;
                }
            }
            if(facturaLocalizada.equalsIgnoreCase("NO")) {
                FacturaClienteCuota nuevaFactura = new FacturaClienteCuota();
                nuevaFactura.setId_cliente(subclientePorFacturar.getId_cliente());
                nuevaFactura.setFactura(documento);
                nuevaFactura.setCuota(numeroCuota);
                nuevaFactura.setValor_factura(valor);
                nuevaFactura.setValor_intereses(valor_intereses);


                nuevaFactura.setItem(1);
                nuevaFactura.setNit(subclientePorFacturar.getNit());
                nuevaFactura.setSimbolo_variable(subclientePorFacturar.getSimbolo_variable());
                nuevaFactura.setFecha_financiacion(subclientePorFacturar.getFecha_financiacion());
                nuevaFactura.setNum_os(subclientePorFacturar.getNum_os());
                nuevaFactura.setId_solicitud( subclientePorFacturar.getId_solicitud() );
                nuevaFactura.setParcial( subclientePorFacturar.getParcial());

                // Registra el valor total de la comision opav , el ultimo numero de item donde se registra el valor del opav, y el del interes.
                if (concepto.equalsIgnoreCase("112")) {
                    nuevaFactura.setValor_opav(valor);
                    nuevaFactura.setItem_opav(item_concepto);
                }
                if (concepto.equalsIgnoreCase("127")) {
                    nuevaFactura.setItem_interes(item_concepto);
                }




                listaCabecera.addElement(nuevaFactura);
            }
        }
        else {
            FacturaClienteCuota nuevaFactura = new FacturaClienteCuota();
            nuevaFactura.setId_cliente(subclientePorFacturar.getId_cliente());
            nuevaFactura.setFactura(documento);
            nuevaFactura.setCuota(numeroCuota);
            nuevaFactura.setValor_factura(valor);
            nuevaFactura.setValor_intereses(valor_intereses);
            nuevaFactura.setItem(1);

            nuevaFactura.setNit(subclientePorFacturar.getNit());
            nuevaFactura.setSimbolo_variable(subclientePorFacturar.getSimbolo_variable());
            nuevaFactura.setFecha_financiacion(subclientePorFacturar.getFecha_financiacion());
            nuevaFactura.setNum_os(subclientePorFacturar.getNum_os());
            nuevaFactura.setId_solicitud( subclientePorFacturar.getId_solicitud() );
            nuevaFactura.setParcial( subclientePorFacturar.getParcial());

            // Registra el valor total de la comision opav , el ultimo numero de item donde se registra el valor del opav, y el del interes.
            if (concepto.equalsIgnoreCase("112")) {
                nuevaFactura.setValor_opav(valor);
                nuevaFactura.setItem_opav(item_concepto);
            }
            if (concepto.equalsIgnoreCase("127")) {
                nuevaFactura.setItem_interes(item_concepto);
            }




            listaCabecera.addElement(nuevaFactura);

        }

    }







    public int getNumeroItemFactura(Vector listaCabecera, String id_cliente, String documento, int numeroCuota) {

        int numeroItem = 0;


        int longitud = listaCabecera.size();
        if (longitud > 0) {
            String facturaLocalizada = "NO";
            for (int i= 0; i<longitud; i++) {
                FacturaClienteCuota facturaClienteCuota = (FacturaClienteCuota) listaCabecera.elementAt(i);
                if (  (facturaClienteCuota.getId_cliente().equalsIgnoreCase(id_cliente) ) &
                      (facturaClienteCuota.getFactura().equalsIgnoreCase(documento) ) &
                      (facturaClienteCuota.getCuota() == numeroCuota)  )  {

                    facturaLocalizada = "SI";
                    numeroItem = facturaClienteCuota.getItem();
                    break;
                }
            }
            if(facturaLocalizada.equalsIgnoreCase("NO")) {
                numeroItem = 0;
            }
        }
        else {
            numeroItem = 0;
        }
        return numeroItem;
    }





    public void generaAjusteFactura(String facturaCreada, Vector listaCabecera, LogWriter logWriter)  throws SQLException {

        if (!facturaCreada.equalsIgnoreCase("")) {

            String comandoSQL = "";
            Vector comandos_sql =new Vector();

            Vector valoresFiducia = new Vector();
            int numeroCabeceras = listaCabecera.size();

            for (int k= 0 ; k<numeroCabeceras ; k++) {
                FacturaClienteCuota facturaClienteCuota = (FacturaClienteCuota) listaCabecera.elementAt(k);

                try {
                    valoresFiducia.addAll( consorcioDao.buscaTotalFacturaFiducia( facturaCreada+"_"+facturaClienteCuota.getCuota() ) ) ;
                }
                catch (SQLException e) {
                    Util.imprimirTrace(e);
                }


                double  valorFacturaFiducia   =  (Double) valoresFiducia.elementAt(0);
                double  valorCapitalFiducia   =  (Double) valoresFiducia.elementAt(1);
                double  valorInteresFiducia   =  (Double) valoresFiducia.elementAt(2);


                if (valorFacturaFiducia != facturaClienteCuota.getValor_factura() ) {

                    double diferencia = valorFacturaFiducia - facturaClienteCuota.getValor_factura() ;

                    // Actualizar cabecera
                    comandoSQL = consorcioDao.actualizarCabecera(facturaClienteCuota.getFactura(), diferencia );
                    comandos_sql.add(comandoSQL);

                    // Actualizar interes
                    double diferenciaInteres = valorInteresFiducia - facturaClienteCuota.getValor_intereses();
                    if(diferenciaInteres != 0) {
                        comandoSQL = consorcioDao.actualizarItem(facturaClienteCuota.getFactura(), diferenciaInteres, facturaClienteCuota.getItem_interes() );
                        comandos_sql.add(comandoSQL);
                    }


                    // Actualizar opav
                    diferencia = diferencia - diferenciaInteres;
                    if(diferencia != 0 ) {
                        comandoSQL = consorcioDao.actualizarItem(facturaClienteCuota.getFactura(), diferencia, facturaClienteCuota.getItem_opav() );
                        comandos_sql.add(comandoSQL);
                    }


                    String estado = this.ejecutarBatchSQL(comandos_sql , logWriter);

                    if (estado.equalsIgnoreCase("ERROR")){
                        logWriter.log("ERROR: Se genero error al ejecutar el Batch de SQL de ajuste . Se efectuo rollback. No se proceso solicitud: " +
                                      facturaClienteCuota.getId_solicitud() + " \n",LogWriter.INFO);
                    }
                    else{
                        // logWriter.log("    Registra en base de datos sin error \n",LogWriter.INFO);
                    }
                    logWriter.log("Valores de los ajustes : Diferencia total factura : " +  ( valorFacturaFiducia - facturaClienteCuota.getValor_factura() ) +  " \n",LogWriter.INFO);
                    logWriter.log("Valores de los ajustes : Diferencia interes       : " +  diferenciaInteres +  " \n",LogWriter.INFO);
                    logWriter.log("Valores de los ajustes : Diferencia capital (opav): " +  diferencia  +  " \n",LogWriter.INFO);


                }
                valoresFiducia.clear();
                comandos_sql.clear();
            }


        }


    }


//* se crea este metodo para permitir prefacturar si hay mas de una accion que se debe prefcaturar de forma individual*//
    public void ajustesProyectosEspeciales(String id_solicitud, LogWriter logWriter) throws SQLException {
        Vector comandos_sql = new Vector();
        comandos_sql.addAll(consorcioDao.AjusteSolicitudesEspeciales(id_solicitud));

        String estado = this.ejecutarBatchSQL(comandos_sql, logWriter);
        
        if (estado.equalsIgnoreCase("ERROR")) {
            logWriter.log("ERROR: se genero un error en la actualizacion para ajustes de proyectos especiales:  \n", LogWriter.INFO);
        }
    }





    public String generaCabecera(SubclientePorFacturar subclientePorFacturar, Vector listaCabecera,
                                 Vector listaComandoSQL, String creation_date, String login,
                                 LogWriter logWriter) {

        String error = "NO";
        String comandoSQL = "";
        String numeroFacturaCreada = "";

        String tipo_referencia_3 = "";
        String referencia_3 = "";

        int numeroCabeceras = listaCabecera.size();
        
        for (int k= 0 ; k<numeroCabeceras ; k++) {

            try {


                FacturaClienteCuota facturaClienteCuota = (FacturaClienteCuota) listaCabecera.elementAt(k);


                numeroFacturaCreada = this.buscaNumeroFactura(facturaClienteCuota.getId_solicitud(),
                                                              facturaClienteCuota.getParcial(),
                                                              facturaClienteCuota.getId_cliente());


                String documento            = facturaClienteCuota.getFactura();
                String id_cliente           = facturaClienteCuota.getId_cliente();
                String nit                  = facturaClienteCuota.getNit();
                String id_solicitud         = facturaClienteCuota.getId_solicitud();
                int parcial                 = facturaClienteCuota.getParcial();


                // Si la oferta es valor agregado se le debe facturar a Electricaribe
                String clienteValorAgregado = this.getClienteValorAgregado(id_solicitud);
                if (clienteValorAgregado.equalsIgnoreCase("10838")) {
                    id_cliente = "10838" ;
                    nit = "8020076706";
                }


                double valor_factura        = facturaClienteCuota.getValor_factura();
                int numeroCuota             = facturaClienteCuota.getCuota();
                String fecha_financiacion   = facturaClienteCuota.getFecha_financiacion();
                String num_os               = facturaClienteCuota.getNum_os();
                String simbolo_variable     = facturaClienteCuota.getSimbolo_variable();
                int cantidad_items          = facturaClienteCuota.getItem();

                String descripcion_factura  = "Factura Eca   NUMERO OS: " + num_os + "   SOLICITUD: " + id_solicitud +
                                              "   SIMBOLO VARIABLE: " + simbolo_variable;


                // Calculo de la fecha de vencimiento de la factura

                DateFormat formato_fecha;
                formato_fecha = new SimpleDateFormat("yyyy-MM-dd");

                String fecha_inicio_pago = subclientePorFacturar.getFecha_inicio_pago();
                String cmc=subclientePorFacturar.getTipo_distribucion().trim().equals("43")?"AS":"FM";

                Calendar calendarFechaVencimiento = Calendar.getInstance();
                String ano = fecha_inicio_pago.substring(0,4);
                String mes = fecha_inicio_pago.substring(5,7);
                String dia = fecha_inicio_pago.substring(8,10);

                calendarFechaVencimiento.set(Integer.parseInt(ano),Integer.parseInt(mes)-2,Integer.parseInt(dia),8,0,0);
                calendarFechaVencimiento.add(Calendar.MONTH, numeroCuota);

                String fecha_vencimiento = formato_fecha.format(calendarFechaVencimiento.getTime()) ;







                // Cabecera de la factura

                if(numeroFacturaCreada.isEmpty()){
                    tipo_referencia_3 = "";
                    referencia_3 = "";
                }
                else {
                    tipo_referencia_3 = "FAC";
                    referencia_3 = numeroFacturaCreada+"_"+facturaClienteCuota.getCuota();
                }

                comandoSQL = setFacturaCxC("FINV", "FAC", documento,
                             nit , id_cliente,"MU",
                             fecha_financiacion,
                             fecha_vencimiento, fecha_financiacion,
                             "Cuota " + Integer.toString(numeroCuota) + ":  " + descripcion_factura, "",
                             valor_factura, 0.0, valor_factura, valor_factura,
                             0.0, valor_factura, 1.0, "PES", cantidad_items,
                             "CREDITO", "OP", "OP",
                             "", "COL", creation_date, login, creation_date,
                             login ,cmc,
                              "", "OP"
                             ,"MS",num_os,"SV",
                             simbolo_variable,"SOL",id_solicitud,
                             "PAR",Integer.toString(parcial),tipo_referencia_3,referencia_3);

                listaComandoSQL.add(comandoSQL);



                // Genera registro en tabla factura_consorcio

                String numeroFacturaCreadaLocalizada = "";
                numeroFacturaCreadaLocalizada = buscaNumeroFactura(id_solicitud, parcial, facturaClienteCuota.getId_cliente() );

                if (numeroFacturaCreadaLocalizada.equalsIgnoreCase("") ){
                    // No esta en factura consorcio, asi que se registra
                    comandoSQL =  insertarFacturaConsorcio(facturaClienteCuota, fecha_vencimiento );
                    listaComandoSQL.add(comandoSQL);
                    // logWriter.log("    Factura consorcio agregada al batch de SQL \n",LogWriter.INFO);
                }
                else {
                    // Si esta en factura consorcio, asi que se actualiza el campo de factura_consorcio
                    comandoSQL =  actualizarFacturaConsorcio(facturaClienteCuota, numeroFacturaCreadaLocalizada+"_"+facturaClienteCuota.getCuota() );
                    listaComandoSQL.add(comandoSQL);
                    // logWriter.log("       Factura ya que existe, se actualiza. Factura: " + numeroFacturaCreada + "-" + numeroCuota +  "  Cliente: " + id_cliente + " \n",LogWriter.INFO);
                }



            }
            catch (Exception e) {

                error = "SI";

            }
        }



        return error;
    }


    public String getClienteValorAgregado(String id_solicitud)throws SQLException{
        return consorcioDao.getClienteValorAgregado(id_solicitud);
    }




/**
 *
 * Actualiza en la tabla de sublcientes ofertas el campo de factura cliente, para evitar dobles facturaciones al cliente.
 * La actualizacion se hace para cada subcliente de la solicitud, ya que cada subcliente tiene un numero de factura diferente
 * @param subclientePorFacturar Objeto que contiene informacion de la solicitud facturada a un subcliente.
 * Contiene el numero de id_solicitud, parcial y cliente al que se le facturo
 * @param factura Numero de factura al cliente
 * @return String sql que permite la actualizacion en batch
 * @throws SQLException
 */
    public String setSolicitudSubcliente(SubclientePorFacturar subclientePorFacturar, String factura) throws SQLException {
        return consorcioDao.setSolicitudSubcliente(subclientePorFacturar, factura ) ;
    }

    public String setActulizarAccion(String id_solicitud)throws SQLException{
        return consorcioDao.setActulizarAccion( id_solicitud);
    }



    public TotalAcciones buscaTotalAcciones(String id_solicitud, int parcial)throws SQLException{
        return consorcioDao.buscaTotalAcciones(id_solicitud, parcial);
    }


    /**
     * Busca el numero de factura para una ya creada en Factura Consorcio
     */
    public String buscaNumeroFactura(String id_solicitud, int parcial, String id_cliente)throws SQLException{
        return consorcioDao.buscaNumeroFactura(id_solicitud, parcial, id_cliente);
    }

    /**
     * Busca el numero de factura para una ya creada en Factura Consorcio
     */
    public String buscaNumeroFacturaFintra(String id_solicitud, int parcial, String id_cliente)throws SQLException{
        return consorcioDao.buscaNumeroFactura(id_solicitud, parcial, id_cliente);
    }


    /**
     * Busca el numero de factura para una ya creada en Factura Consorcio   // 2010-07-08
     */
    public String getNumeroFacturaConsorcio(String id_solicitud, int parcial, String id_cliente)throws SQLException{
        return consorcioDao.getNumeroFacturaConsorcio(id_solicitud, parcial, id_cliente);
    }










    public String setFacturaConsorcio(String facturaConsorcio, String facturaCorficolombiana) throws SQLException {
        return consorcioDao.setFacturaConsorcio(facturaConsorcio, facturaCorficolombiana);
    }













    /** Crea una lista de acciones de la solicitud pendientes de facturar al cliente */
    public void buscaDetalleAcciones(String id_solicitud, int parcial)throws SQLException{
        listaDetalleAcciones = consorcioDao.buscaDetalleAcciones(id_solicitud, parcial);
    }

    public List getDetalleAcciones() throws SQLException{
        return listaDetalleAcciones;
    }


    public List generaItem(SubclientePorFacturar subcliente,
                           String documento_inicial,
                           TotalesConsorcio totalesConsorcio ) throws SQLException{



        List listaAcciones = new LinkedList();
        DetalleAcciones accion = new DetalleAcciones();
        ItemFacturaCliente item = new ItemFacturaCliente();
        List listaItem = new LinkedList();



        try {

            buscaDetalleAcciones(subcliente.getId_solicitud(),
                                 subcliente.getParcial());

            listaAcciones = getDetalleAcciones();
            int totalAcciones = listaAcciones.size();



            totalesConsorcio.setInicializar(subcliente.getBase_1(),"subtotal_sin_iva","TOTAL") ;
            totalesConsorcio.setInicializar(subcliente.getIva_base(),"subtotal_iva","TOTAL") ;
            totalesConsorcio.setInicializar(subcliente.getVal_extemporaneo_1(),"valor_extemporaneo_1","TOTAL") ;
            totalesConsorcio.setInicializar(-subcliente.getVal_cuota_inicial(),"cuota_inicial","TOTAL") ;
            totalesConsorcio.setInicializar(subcliente.getVal_extemporaneo_2(),"valor_extemporaneo_2","TOTAL") ;
            totalesConsorcio.setInicializar(subcliente.getIntereses(),"intereses","TOTAL") ;
            totalesConsorcio.setInicializar(subcliente.getBonificacion(),"bonificacionSubcliente","TOTAL") ;


            double valor = 0.00;

            int k = 0;
            Iterator it = listaAcciones.iterator();
            while (it.hasNext()) {
                 accion = (DetalleAcciones)it.next();
                 k++;                                                               // k esima accion

                 double porcentajeBase = subcliente.getPorcentaje_base()/100;

                 item = new ItemFacturaCliente ();

                 item.setId_accion(accion.getId_accion() );
                 item.setId_solicitud(accion.getId_solicitud());
                 item.setParcial(accion.getParcial());
                 item.setContratista(accion.getContratista());
                 item.setNombre_contratista(accion.getNombre_contratista());

                 valor = Util.redondear2( porcentajeBase*accion.getValor_a_financiar_sin_iva(),0);
                 item.setValor_a_financiar_sin_iva(totalesConsorcio.getValor(valor,"subtotal_sin_iva",totalAcciones, k)  ) ;

                 // Calculo del costo contratista y comisiones

                 item.setCosto_contratista(Util.redondear2(porcentajeBase*accion.getCosto_contratista(),0 ) ) ;
                 item.setComision_opav(Util.redondear2( porcentajeBase*accion.getComision_opav(),0 ) );
                 item.setComision_fintra(Util.redondear2( porcentajeBase*accion.getComision_fintra(),0 ) );
                 item.setComision_interventoria(Util.redondear2( porcentajeBase*accion.getComision_interventoria(),0 ) );
                 item.setComision_provintegral(Util.redondear2( porcentajeBase*accion.getComision_provintegral(),0 ) );

                 double comisionEca =   item.getValor_a_financiar_sin_iva() -
                                        item.getCosto_contratista() -
                                        item.getComision_opav() -
                                        item.getComision_fintra() -
                                        item.getComision_interventoria() -
                                        item.getComision_provintegral() ;

                 item.setComision_eca(comisionEca);

                 // Calculos de los Ivas

                 valor = Util.redondear2( porcentajeBase*accion.getSubtotal_iva(),0);
                 item.setSubtotal_iva(totalesConsorcio.getValor(valor,"subtotal_iva",totalAcciones, k)  ) ;


                 item.setIva_contratista(Util.redondear2(porcentajeBase*accion.getIva_contratista(),0));
                 item.setIva_bonificacion(Util.redondear2(porcentajeBase*accion.getIva_bonificacion(),0));
                 item.setIva_comision_opav(Util.redondear2(porcentajeBase*accion.getIva_comision_opav(),0));
                 item.setIva_comision_fintra(Util.redondear2(porcentajeBase*accion.getIva_comision_fintra(),0));
                 item.setIva_comision_interventoria(Util.redondear2(porcentajeBase*accion.getIva_comision_interventoria(),0));
                 item.setIva_comision_provintegral(Util.redondear2(porcentajeBase*accion.getIva_comision_provintegral(),0));

                 double iva_comision_eca =  item.getSubtotal_iva() -
                                            item.getIva_contratista() -
                                            item.getIva_bonificacion() -
                                            item.getIva_comision_opav() -
                                            item.getIva_comision_fintra() -
                                            item.getIva_comision_interventoria() -
                                            item.getIva_comision_provintegral();
                 item.setIva_comision_eca(iva_comision_eca) ;

                 // Calculo del prorrateo del valor extemporaneo 1


                 double porcentaje = Util.redondear2(item.getValor_a_financiar_sin_iva()/subcliente.getBase_1()*100,0 );
                 valor = Util.redondear2(porcentaje * subcliente.getVal_extemporaneo_1()/100, 0) ;
                 item.setValor_extemporaneo_1(totalesConsorcio.getValor(valor,"valor_extemporaneo_1",totalAcciones, k)  ) ;


                 // Calculo del prorrateo del valor de la cuota inicial


                 valor = -Util.redondear2(porcentaje * subcliente.getVal_cuota_inicial()/100, 0) ;
                 item.setCuota_inicial(totalesConsorcio.getValor(valor,"cuota_inicial",totalAcciones, k)  ) ;


                 // Calculo del prorrateo del valor extemporaneo 2

                 valor = Util.redondear2(porcentaje * subcliente.getVal_extemporaneo_2()/100, 0) ;
                 item.setValor_extemporaneo_2(totalesConsorcio.getValor(valor,"valor_extemporaneo_2",totalAcciones, k)  ) ;


                 // Calculo del prorrateo de los intereses

                 valor = Util.redondear2(porcentaje * subcliente.getIntereses()/100, 0) ;
                 item.setIntereses(totalesConsorcio.getValor(valor,"intereses",totalAcciones, k)  ) ;


                 // Calculo de la bonificacion por item

                 // valor = Util.redondear2(porcentaje * subcliente.getBonificacion()/100, 0) ;
                 // item.setBonificacion(totalesConsorcio.getValor(valor,"bonificacionSubcliente",totalAcciones, k)  ) ;

                 valor = Util.redondear2( porcentajeBase*accion.getBonificacion(),0 ) ;
                 item.setBonificacion(totalesConsorcio.getValor(valor,"bonificacionSubcliente",totalAcciones, k)  ) ;



                 listaItem.add(item);


            }
        }

        catch (Exception e){
            listaItem = null;
        }

    return listaItem;
    }




    public String setFacturaDetalleCxC(String dstrct, String tipo_documento, String documento,
                                    int item, String nit, String concepto,
                                    String descripcion, String codigo_cuenta_contable,
                                    double cantidad, double valor_unitario , double valor_unitariome , double valor_item,
                                    double valor_itemme , double valor_tasa, String moneda,
                                    String last_update, String user_update, String creation_date,
                                    String creation_user, String base, String auxiliar,
                                    String tipo_referencia_1, String referencia_1,
                                    String tipo_referencia_2, String referencia_2,
                                    String tipo_referencia_3, String referencia_3
                                    )throws SQLException{

        return consorcioDao.setFacturaDetalleCxC(dstrct, tipo_documento, documento, item,
                                           nit, concepto, descripcion, codigo_cuenta_contable,
                                           cantidad, valor_unitario, valor_unitariome,
                                           valor_item, valor_itemme, valor_tasa,
                                           moneda, last_update, user_update, creation_date,
                                           creation_user, base, auxiliar, tipo_referencia_1, referencia_1,
                                           tipo_referencia_2,  referencia_2, tipo_referencia_3, referencia_3);


    }




   public String  setFacturaCxC(String dstrct, String tipo_documento, String documento,
                             String nit, String codcli, String concepto,
                             String fecha_factura, String fecha_vencimiento,
                             String fecha_impresion,String descripcion, String observacion,
                             Double valor_factura, Double valor_abono, Double valor_saldo,
                             Double valor_facturame, Double valor_abonome, Double valor_saldome,
                             Double valor_tasa, String moneda,
                             int cantidad_items, String forma_pago, String agencia_facturacion,
                             String agencia_cobro,String zona, String base,String last_update, String user_update,
                             String creation_date, String creation_user, String cmc,
                             String formato, String agencia_impresion,
                             String tipo_ref1,String ref1,String tipo_ref2,String ref2,
                             String tipo_referencia_1, String referencia_1,
                             String tipo_referencia_2, String referencia_2,
                             String tipo_referencia_3, String referencia_3)throws SQLException{


       return consorcioDao.setFacturaCxC(dstrct, tipo_documento, documento, nit, codcli,
                                   concepto, fecha_factura, fecha_vencimiento, fecha_impresion,
                                   descripcion, observacion, valor_factura, valor_abono, valor_saldo, valor_facturame,
                                   valor_abonome, valor_saldome, valor_tasa, moneda, cantidad_items,
                                   forma_pago, agencia_facturacion, agencia_cobro,
                                   zona, base, last_update, user_update, creation_date,
                                   creation_user, cmc, formato, agencia_impresion,
                                   tipo_ref1, ref1, tipo_ref2,  ref2, tipo_referencia_1, referencia_1,
                                   tipo_referencia_2,  referencia_2, tipo_referencia_3, referencia_3);
   }



   /**
    * Elabora una Nota credito por todas las facturas que el Consorcio le cobra al cliente
    * @param login String usuario en sesion
    * @param model Objeto Model que identifica la variable en sesion para cada usuario
    * @param logWriter Objeto LogWriter utilizado para manejar el log del proceso
    * @throws SQLException
    * @Author Alvaro Pabon Martinez
    * @Version 1.0
    */
   public void generaNcTraslado(String login, ModelOpav model, LogWriter logWriter) throws SQLException {


       // Definicion de constantes

       String TIPO_DOCUMENTO_NC         = constanteService.getValor("FINV", "TIPO_DOCUMENTO_NC", "");
       String TIPO_DOCUMENTO_FAC        = constanteService.getValor("FINV", "TIPO_DOCUMENTO_FAC", "");
       String SIGLA_SERIE_DOCUMENTO_NC  = constanteService.getValor("FINV", "SIGLA_SERIE_DOCUMENTO_NC", "");
       String OFICINA_PRINCIPAL         = constanteService.getValor("FINV", "OFICINA_PRINCIPAL", "");
       String BASE_LOCAL                = constanteService.getValor("FINV", "BASE_LOCAL", "");

       String CONCEPTO_NC_TRASLADO      = constanteService.getValor("FINV", "CONCEPTO_NC_TRASLADO", "");
       String CODIGO_TIPO_INGRESO       = constanteService.getValor("FINV", "CODIGO_TIPO_INGRESO", "");
       String MONEDA_LOCAL              = constanteService.getValor("FINV", "MONEDA_LOCAL", "");
       String FECHA_STANDARD_POSTGRES   = constanteService.getValor("FINV", "FECHA_STANDARD_POSTGRES", "");
       String NIT_FINTRA                = constanteService.getValor("FINV", "NIT_FINTRA", "");
       String CUENTA_NC_CXC_DETALLE     = constanteService.getValor("FINV", "CUENTA_NC_CXC_DETALLE", "");  // 2010-06-12


       String CUENTA_TEMPORAL_TRASLADO_FACTURA_CLIENTE   = constanteService.getValor("FINV", "CUENTA_TEMPORAL_TRASLADO_FACTURA_CLIENTE", "");


       String comandoSQL = "";
       Vector comandos_sql =new Vector();
       SerieGeneral serie =  new SerieGeneral();
       String num_ingreso = "";
       String estado = "";

       java.util.Date fechaActual = new Date();
       String creation_date = fechaActual.toString();

       Date fecha_hoy = new Date();
       DateFormat formato_fecha;
       formato_fecha = new SimpleDateFormat("yyyy-MM-dd");
       String fechaProceso = formato_fecha.format(fecha_hoy);

       IngresoDetalle ingresoDetalle = new IngresoDetalle();
       IngresoCabecera ingresoCabecera = new IngresoCabecera();


       try {

           // Elabora una lista de facturas ha hacerle la nota credito a cada una de ellas
           List listaNcDetallePorTrasladar;
           listaNcDetallePorTrasladar =  consorcioDao.buscaNcDetallePorTrasladar() ;

           if (listaNcDetallePorTrasladar.size() > 0) {

               logWriter.tituloInicial("GENERACION DE NOTAS CREDITO PARA TRASLADO");

               ListIterator it = listaNcDetallePorTrasladar.listIterator();
               while (it.hasNext()) {
                  ingresoDetalle  = (IngresoDetalle) it.next();
                

                 serie = serieGeneralService.getSerie("FINV",OFICINA_PRINCIPAL, SIGLA_SERIE_DOCUMENTO_NC);
                 serieGeneralService.setSerie("FINV",OFICINA_PRINCIPAL, SIGLA_SERIE_DOCUMENTO_NC);
                  num_ingreso = serie.getUltimo_prefijo_numero();

                  // Arma el detalle
                  ingresoDetalle.setUser_update(login);
                  ingresoDetalle.setLast_update(creation_date);

                  ingresoDetalle.setCreation_user(login);
                  ingresoDetalle.setCreation_date(creation_date);

                  ingresoDetalle.setTipo_documento(TIPO_DOCUMENTO_NC);
                  ingresoDetalle.setTipo_doc(TIPO_DOCUMENTO_FAC);
                  ingresoDetalle.setBase(BASE_LOCAL);
                  ingresoDetalle.setNum_ingreso(num_ingreso);
                  ingresoDetalle.setCuenta(CUENTA_NC_CXC_DETALLE);

                  comandoSQL = consorcioDao.setInsertarIngresoDetalle(ingresoDetalle);
                  comandos_sql.add(comandoSQL);


                  // Arma la cabecera
                  ingresoCabecera.setReg_status("");
                  ingresoCabecera.setDstrct("FINV") ;
                  ingresoCabecera.setTipo_documento(TIPO_DOCUMENTO_NC) ;
                  ingresoCabecera.setNum_ingreso(num_ingreso) ;
                  ingresoCabecera.setCodcli(ingresoDetalle.getCodcli()) ;
                  ingresoCabecera.setNitcli(ingresoDetalle.getNitcli()) ;
                  ingresoCabecera.setConcepto(CONCEPTO_NC_TRASLADO) ;
                  ingresoCabecera.setTipo_ingreso(CODIGO_TIPO_INGRESO) ;

                  ingresoCabecera.setFecha_consignacion (fechaProceso) ;
                  ingresoCabecera.setFecha_ingreso(fechaProceso) ;
                  ingresoCabecera.setBranch_code("") ;
                  ingresoCabecera.setBank_account_no("") ;
                  ingresoCabecera.setCodmoneda(MONEDA_LOCAL) ;
                  ingresoCabecera.setAgencia_ingreso(OFICINA_PRINCIPAL) ;




                  ingresoCabecera.setDescripcion_ingreso("CANCELA FACTURA: " + ingresoDetalle.getFactura()+ " PARA TRASLADARLA A FINTRA") ;
                  ingresoCabecera.setPeriodo("") ;

                  ingresoCabecera.setVlr_ingreso(ingresoDetalle.getValor_ingreso());
                  ingresoCabecera.setVlr_ingreso_me(ingresoDetalle.getValor_ingreso()) ;
                  ingresoCabecera.setVlr_tasa(1.00) ;
                  ingresoCabecera.setFecha_tasa(fechaProceso) ;
                  ingresoCabecera.setCant_item (1) ;
                  ingresoCabecera.setTransaccion (0) ;
                  ingresoCabecera.setTransaccion_anulacion(0) ;
                  ingresoCabecera.setFecha_impresion(FECHA_STANDARD_POSTGRES) ;
                  ingresoCabecera.setFecha_contabilizacion(FECHA_STANDARD_POSTGRES) ;
                  ingresoCabecera.setFecha_anulacion_contabilizacion(FECHA_STANDARD_POSTGRES) ;
                  ingresoCabecera.setFecha_anulacion (FECHA_STANDARD_POSTGRES) ;
                  ingresoCabecera.setCreation_user (login) ;
                  ingresoCabecera.setCreation_date (creation_date) ;
                  ingresoCabecera.setUser_update (login) ;
                  ingresoCabecera.setLast_update (creation_date) ;
                  ingresoCabecera.setBase (BASE_LOCAL) ;
                  ingresoCabecera.setNro_consignacion ("") ;
                  ingresoCabecera.setPeriodo_anulacion ("") ;
                  ingresoCabecera.setCuenta (CUENTA_TEMPORAL_TRASLADO_FACTURA_CLIENTE) ;
                  ingresoCabecera.setAuxiliar(NIT_FINTRA);
                  ingresoCabecera.setAbc ("") ;
                  ingresoCabecera.setTasa_dol_bol (0.00) ;
                  ingresoCabecera.setSaldo_ingreso (0.00) ;
                  ingresoCabecera.setCmc (ingresoDetalle.getCmc()) ;

                  ingresoCabecera.setCorficolombiana ("") ;
                  ingresoCabecera.setFec_envio_fiducia (FECHA_STANDARD_POSTGRES) ;
                  ingresoCabecera.setTipo_referencia_1 (TIPO_DOCUMENTO_FAC) ;
                  ingresoCabecera.setReferencia_1 (ingresoDetalle.getFactura()) ;
                  ingresoCabecera.setTipo_referencia_2 ("") ;
                  ingresoCabecera.setReferencia_2 ("") ;
                  ingresoCabecera.setTipo_referencia_3 ("") ;
                  ingresoCabecera.setReferencia_3 ("") ;

                  comandoSQL = consorcioDao.setInsertarIngresoCabecera(ingresoCabecera);
                  comandos_sql.add(comandoSQL);

                  comandoSQL = consorcioDao.setActualizarFacturaCliente(ingresoCabecera);
                  comandos_sql.add(comandoSQL);


                  // Actualiza en la base de datos
                  estado = model.consorcioService.ejecutarBatchSQL(comandos_sql, logWriter);

                  if (estado.equalsIgnoreCase("ERROR")){
                      logWriter.log("ERROR: Se genero error al ejecutar el Batch de SQL. Se efectuo rollback. No se cancelo factura: " + ingresoDetalle.getFactura() + " \n",LogWriter.INFO);
                  }
                  else{
                      // logWriter.log("    Registra en base de datos sin error \n",LogWriter.INFO);
                  }
                  comandos_sql.clear();

               }


               logWriter.log("FINAL  PROCESO GENERACION DE NOTA CREDITOS DE TRASLADO \n",LogWriter.INFO);
           }
           // No hay facturas a las que elaborarle notas credito
           else {
               logWriter.log("NO EXISTEN FACTURAS A LA QUE ELABORARLE LAS NOTAS CREDITO \n",LogWriter.INFO);
           }

       }
       catch (Exception e) {
           logWriter.log("ERROR: Se presento el siguiente error:  \n"  ,LogWriter.INFO);
           logWriter.log(e.getMessage());
       }


   }




   /**
    * Elabora una Nota credito en Fintra para aplicar el valor de factoring y formulas a las facturas trasladadas a Fintra
    * @param login String usuario en sesion
    * @param model Objeto Model que identifica la variable en sesion para cada usuario
    * @param logWriter Objeto LogWriter utilizado para manejar el log del proceso
    * @throws SQLException
    * @Author Alvaro Pabon Martinez
    * @Version 1.0
    */
   public void generarNcFactoringFormula(String login, ModelOpav model, LogWriter logWriter) throws SQLException {


       // Definicion de constantes


       // Falta grabarlo en constante y hay que grabar en cmc el nuevo documento con cmc  NF
       String TIPO_DOCUMENTO_NC_CXP     = constanteService.getValor("FINV", "TIPO_DOCUMENTO_NC_CXP", "");

       String AGENCIA_PRINCIPAL         = constanteService.getValor("FINV", "AGENCIA_PRINCIPAL", "");





       String CUENTA_NC_FACTORING       = constanteService.getValor("FINV", "CUENTA_NC_FACTORING", "");
       String CUENTA_NC_FORMULA         = constanteService.getValor("FINV", "CUENTA_NC_FORMULA", "");
       String SIGLA_ACCION              = constanteService.getValor("FINV", "SIGLA_ACCION", "");
       String MONEDA_LOCAL              = constanteService.getValor("FINV", "MONEDA_LOCAL", "");
       String BASE_LOCAL                = constanteService.getValor("FINV", "BASE_LOCAL", "");
       String CLASE_DOCUMENTO           = constanteService.getValor("FINV", "CLASE_DOCUMENTO", "");
       String APROBADOR_AUTOMATICO      = constanteService.getValor("FINV", "APROBADOR_AUTOMATICO", "");
       String BANCO_POR_DEFECTO         = constanteService.getValor("FINV", "BANCO_POR_DEFECTO", "");
       String SUCURSAL_POR_DEFECTO      = constanteService.getValor("FINV", "SUCURSAL_POR_DEFECTO", "");
       String HANDLE_CODE_CD                       = constanteService.getValor("FINV", "HANDLE_CODE_CD", "");






       String comandoSQL = "";
       Vector comandos_sql = new Vector();
       Vector comandos_sql_consorcio = new Vector();
       String estado = "";

       java.util.Date fechaActual = new Date();
       String creation_date = fechaActual.toString();

       FacturaFactoringFormula facturaFactoringFormula = new FacturaFactoringFormula();
       AccionFactoringFormula   accionFactoringFormula = new AccionFactoringFormula();
       FacturaDetalleCxP facturaDetalleCxP   = new FacturaDetalleCxP();
       FacturaCabeceraCxP facturaCabeceraCxP = new FacturaCabeceraCxP();

       List listaFacturaCabecera =  new LinkedList();


       String condicion = "";
       String acciones = "";
       double total_aplicaciones = 0.00;



       try {


           List listaFactura;
           List listaAccion;


           // PASO 1 Localizar las facturas a las cuales hay que hacerle nota credito por factoring y formula
           listaFactura =  consorcioDao.buscaFacturaFactoringFormula() ;

           if (listaFactura.size() > 0) {

               logWriter.tituloInicial(" PROCESO GENERACION DE APLICACION DEL FACTORING Y/O FORMULA PARA FACTURAS DE PROVEEDORES EN FINTRA");

               ListIterator it = listaFactura.listIterator();
               while (it.hasNext()) {

                   facturaFactoringFormula  = (FacturaFactoringFormula) it.next();

                   logWriter.log("INICIA   proceso factura : " + facturaFactoringFormula.getDocumento() + " \n",LogWriter.INFO);


                   condicion =  " dstrct = '" + facturaFactoringFormula.getDstrct() + "' and ";
                   condicion +=  "proveedor = '" + facturaFactoringFormula.getProveedor() + "' and ";
                   condicion +=  "tipo_documento = '" + facturaFactoringFormula.getTipo_documento() + "' and ";
                   condicion +=  "documento = '" + facturaFactoringFormula.getDocumento() + "'" ;

                   // PASO 1A  Buscar factura en Fintra

                   listaFacturaCabecera = consorcioDao.buscaFacturaCabecera(condicion, "fintra");

                   facturaCabeceraCxP = (FacturaCabeceraCxP) listaFacturaCabecera.get(0);

                   if(!listaFacturaCabecera.isEmpty()) {

                       // Existe la factura en Fintra

                       if(facturaCabeceraCxP.getFactoring_formula_aplicada().equalsIgnoreCase("N")) {



                           // PASO 1 Arma la cabecera de la nota credito que cancela la factura del contratista

                           String proveedor = facturaCabeceraCxP.getProveedor();
                           String tipo_documento = TIPO_DOCUMENTO_NC_CXP;
                           String factura_contratista = facturaCabeceraCxP.getDocumento();

                           double totalFactura = facturaFactoringFormula.getTotal_nota_credito();
                           String fechaVencimiento = facturaCabeceraCxP.getFecha_vencimiento();
                           String tipoDocumentoRelacionado = facturaCabeceraCxP.getTipo_documento();




                            // PASO 1B Inicializa los items comunes en el detalle del ingreso

                            int item = 0;
                            acciones = "";
                            total_aplicaciones = 0.00;


                            facturaDetalleCxP.setReg_status( "" );
                            facturaDetalleCxP.setDstrct(facturaCabeceraCxP.getDstrct() );
                            facturaDetalleCxP.setProveedor(proveedor );
                            facturaDetalleCxP.setTipo_documento(tipo_documento );
                            facturaDetalleCxP.setDocumento(factura_contratista );
                            facturaDetalleCxP.setCodigo_abc("");
                            facturaDetalleCxP.setPlanilla("");
                            facturaDetalleCxP.setLast_update(creation_date);
                            facturaDetalleCxP.setUser_update(login);
                            facturaDetalleCxP.setCreation_date(creation_date);
                            facturaDetalleCxP.setCreation_user(login);
                            facturaDetalleCxP.setBase(BASE_LOCAL);
                            facturaDetalleCxP.setCodcliarea("");
                            facturaDetalleCxP.setTipcliarea("");
                            facturaDetalleCxP.setConcepto("");
                            facturaDetalleCxP.setAuxiliar(proveedor);
                            facturaDetalleCxP.setTipo_referencia_1(SIGLA_ACCION);

                            facturaDetalleCxP.setTipo_referencia_2("");
                            facturaDetalleCxP.setReferencia_2("" );
                            facturaDetalleCxP.setTipo_referencia_3("" );
                            facturaDetalleCxP.setReferencia_3("");


                            double valorSaldoFactura = facturaCabeceraCxP.getVlr_saldo();


                            // PASO 2 Localizar las acciones de la factura que tienen valor factoring y/o formula
                            listaAccion = consorcioDao. buscaAccionFactoringFormula(facturaFactoringFormula.getDstrct(),
                                                                                    facturaFactoringFormula.getProveedor(),
                                                                                    facturaFactoringFormula.getTipo_documento(),
                                                                                    facturaFactoringFormula.getDocumento());

                            Iterator it2 = listaAccion.iterator();

                            while (it2.hasNext()  ) {

                               accionFactoringFormula = (AccionFactoringFormula)it2.next();

                               acciones = acciones + accionFactoringFormula.getId_accion() + " ";

                               // PASO 3  Generar los items de la nota credito

                               if(accionFactoringFormula.getVal_factoring_contratista() != 0.00) {

                                   item++;
                                   String stringItem =   Util.llenarConCerosALaIzquierda( item , 3 ) ;


                                   total_aplicaciones = total_aplicaciones + accionFactoringFormula.getVal_factoring_contratista();

                                   valorSaldoFactura = valorSaldoFactura - accionFactoringFormula.getVal_factoring_contratista();

                                   facturaDetalleCxP.setItem(stringItem);
                                   facturaDetalleCxP.setDescripcion("Aplica factoring de la accion: " + accionFactoringFormula.getId_accion() );
                                   facturaDetalleCxP.setVlr(accionFactoringFormula.getVal_factoring_contratista());
                                   facturaDetalleCxP.setVlr_me(accionFactoringFormula.getVal_factoring_contratista());
                                   facturaDetalleCxP.setCodigo_cuenta(CUENTA_NC_FACTORING);
                                   facturaDetalleCxP.setReferencia_1(accionFactoringFormula.getId_accion());

                                   facturaDetalleCxP.setTipo_documento("NC");
                                   comandoSQL = model.consorcioService.setFacturaDetalleCxP(facturaDetalleCxP, login, creation_date);
                                   comandos_sql.add(comandoSQL);
                               }


                               if(accionFactoringFormula.getVal_formula_contratista() != 0.00) {

                                   item++;
                                   String stringItem =   Util.llenarConCerosALaIzquierda( item , 3 ) ;

                                   total_aplicaciones = total_aplicaciones + accionFactoringFormula.getVal_formula_contratista();

                                   valorSaldoFactura = valorSaldoFactura - accionFactoringFormula.getVal_formula_contratista();

                                   facturaDetalleCxP.setItem(stringItem);
                                   facturaDetalleCxP.setDescripcion("Aplica formula de la accion: " + accionFactoringFormula.getId_accion() );
                                   facturaDetalleCxP.setVlr(accionFactoringFormula.getVal_formula_contratista());
                                   facturaDetalleCxP.setVlr_me(accionFactoringFormula.getVal_formula_contratista());
                                   facturaDetalleCxP.setCodigo_cuenta(CUENTA_NC_FORMULA);
                                   facturaDetalleCxP.setReferencia_1(accionFactoringFormula.getId_accion());
                                   facturaDetalleCxP.setTipo_documento("NC");

                                   comandoSQL = model.consorcioService.setFacturaDetalleCxP(facturaDetalleCxP, login, creation_date);
                                   comandos_sql.add(comandoSQL);
                               }
                           }


                                            comandoSQL = model.consorcioService.setCxp_doc(facturaCabeceraCxP.getDstrct(), proveedor, "NC", factura_contratista,
                                              "Aplica factoring y/o formula de la accion: " + acciones,
                                              AGENCIA_PRINCIPAL, HANDLE_CODE_CD,  creation_date,  APROBADOR_AUTOMATICO, APROBADOR_AUTOMATICO, BANCO_POR_DEFECTO,
                                              SUCURSAL_POR_DEFECTO, MONEDA_LOCAL, totalFactura, 0.00, totalFactura, totalFactura,
                                              0.00, totalFactura, 1.00, "", login,
                                              login, BASE_LOCAL, CLASE_DOCUMENTO, MONEDA_LOCAL,
                                              facturaCabeceraCxP.getFecha_documento(), fechaVencimiento, "",
                                              creation_date, creation_date, "","","",
                                              "", tipoDocumentoRelacionado, factura_contratista, "","","N");
                           comandos_sql.add(0, comandoSQL);




                           // PAOS 6  Aplicar la nota credito a la factura PM en Fintra

                           comandoSQL = consorcioDao.setAbonaCxP(facturaCabeceraCxP.getDstrct(), proveedor,
                                                                 accionFactoringFormula.getTipo_documento(), factura_contratista, total_aplicaciones);
                           comandos_sql.add(comandoSQL);



                           // PASO 7  Marcar la factura CF en consorcio marcada como procesada




                           String datos = " factoring_formula_aplicada = 'S' ";

                           comandoSQL = consorcioDao.setActualizaCxP(facturaCabeceraCxP.getDstrct(), proveedor,
                                                                     accionFactoringFormula.getTipo_documento(), factura_contratista, datos);
                           comandos_sql_consorcio.add(comandoSQL);




                           // Actualiza en la base de datos de Fintra
                           estado = model.consorcioService.ejecutarBatchSQL(comandos_sql,logWriter,"selectrik");

                           if (estado.equalsIgnoreCase("ERROR")){
                               logWriter.log("ERROR: Se genero error al ejecutar el Batch de SQL. Se efectuo rollback. La factura en Consorcio no pudo ser procesada: " + facturaFactoringFormula.getDocumento() + " \n",LogWriter.INFO);
                           }
                           else{
                               // logWriter.log("    Registra en base de datos de fintra sin error \n",LogWriter.INFO);

                               // Actualiza en la base de datos de Consorcio
                               estado = model.consorcioService.ejecutarBatchSQL(comandos_sql_consorcio,logWriter);
                               estado="";

                               if (estado.equalsIgnoreCase("ERROR")){
                                   logWriter.log("ERROR: Se genero error al ejecutar el Batch de SQL. Se efectuo rollback. \n",LogWriter.INFO);
                                   logWriter.log("       La factura PM en Fintra analoga a la factura en el Consorcio se le aplico correctamente el factoring y/o formula \n",LogWriter.INFO);
                                   logWriter.log("       La factura CF en Consorcio no qued� marcada como aplicado el factoring. Factura Consorcio : " + facturaFactoringFormula.getDocumento() + " \n",LogWriter.INFO);
                               }
                               else{
                                   // logWriter.log("    Registra en base de datos de consorcio sin error \n",LogWriter.INFO);

                                   logWriter.log("FINALIZA proceso factura : " + facturaFactoringFormula.getDocumento() + " \n",LogWriter.INFO);

                               }

                           }

                       }
                       else {

                           // La factura ya tiene aplicada un NC por factoring

                           logWriter.log("ERROR : La factura " + facturaCabeceraCxP.getDocumento() + " esta marcada en Fintra como si ya estuviera aplicada la nota credito. \n",LogWriter.INFO);
                           logWriter.log("        Consulte la factura y verifique si existe una nota credito por este concepto y si ya la factura tiene aplicada la NC \n",LogWriter.INFO);

                       }

                   }

                   else {
                       // La factura de tipo PM no se encuentra en Fintra por tanto no se le puede hacer la NC de factoring

                        logWriter.log("ERROR : La factura " + facturaCabeceraCxP.getDocumento() + " no se encuentra registrada en Fintra. \n",LogWriter.INFO);
                        logWriter.log("        No se elaboro la nota credito de factoring y formula \n",LogWriter.INFO);

                   }

                   comandos_sql.clear();
                   comandos_sql_consorcio.clear();

               } // Fin de while recorriendo las facturas a las que se le hay que aplicar NC por factgoring y/o formula


               logWriter.log("FINAL  PROCESO GENERACION DE NOTA CREDITOS DE VALOR FACTORING Y VALOR FORMULA PARA FACTURA A FINTRA \n",LogWriter.INFO);
           }
           // No hay facturas a las que elaborarle notas credito
           else {
               logWriter.log("NO EXISTEN FACTURAS A LA QUE ELABORARLE LAS NOTAS CREDITO \n",LogWriter.INFO);
           }

       }
       catch (Exception e) {
           logWriter.log("ERROR: Se presento el siguiente error:  \n"  ,LogWriter.INFO);
           logWriter.log(e.getMessage());
       }


   }
   
    /**
     * Metodo que ejecuta una rutina en la bd para realizar el traslado de facturas...
     * @param user
     * @param model
     * @param logWriter
     * @throws SQLException
     * @autor egonzalez
     */
    public void generarTrasladoFacturas(Usuario user, ModelOpav model, LogWriter logWriter) throws SQLException {    
          consorcioDao.generarTrasladoFacturas(user);
    }







    public void generaFacturaPorCobrarFintra(String login, ModelOpav model, LogWriter logWriter) throws SQLException {

       // Definicion de constantes

       String TIPO_DOCUMENTO_FAC           = constanteService.getValor("FINV", "TIPO_DOCUMENTO_FAC", "");
       String SIGLA_SERIE_FACTURA_CLIENTE  = constanteService.getValor("FINV", "SIGLA_SERIE_FACTURA_CLIENTE", "");
       String OFICINA_PRINCIPAL            = constanteService.getValor("FINV", "OFICINA_PRINCIPAL", "");
       String BASE_LOCAL                   = constanteService.getValor("FINV", "BASE_LOCAL", "");
       String MONEDA_LOCAL                 = constanteService.getValor("FINV", "MONEDA_LOCAL", "");
       String CONCEPTO_CXC_FINTRA          = constanteService.getValor("FINV", "CONCEPTO_CXC_FINTRA", "");
       String CUENTA_TEMPORAL_TRASLADO_FACTURA_CLIENTE   = constanteService.getValor("FINV", "CUENTA_TEMPORAL_TRASLADO_FACTURA_CLIENTE", "");

       String CONCEPTO_CUOTA_INICIAL       = constanteService.getValor("FINV", "CONCEPTO_CUOTA_INICIAL", "");
       String CONCEPTO_EXTEMPORANEO_1      = constanteService.getValor("FINV", "CONCEPTO_EXTEMPORANEO_1", "");
       String CONCEPTO_EXTEMPORANEO_2      = constanteService.getValor("FINV", "CONCEPTO_EXTEMPORANEO_2", "");
       String CONCEPTO_INTERES             = constanteService.getValor("FINV", "CONCEPTO_INTERES", "");
       String CONCEPTO_CAPITAL             = constanteService.getValor("FINV", "CONCEPTO_CAPITAL", "");
       String CONCEPTO_EXTEMPORANEO        = constanteService.getValor("FINV", "CONCEPTO_EXTEMPORANEO", "");

       String CUENTA_INTERES_FINTRA        = constanteService.getValor("FINV", "CUENTA_INTERES_FINTRA", "");
       String CUENTA_CAPITAL_FINTRA        = constanteService.getValor("FINV", "CUENTA_CAPITAL_FINTRA", "");
       String CUENTA_EXTEMPORANEO_FINTRA   = constanteService.getValor("FINV", "CUENTA_EXTEMPORANEO_FINTRA", "");
       String CUENTA_CUOTA_INICIAL_FINTRA  = constanteService.getValor("FINV", "CUENTA_CUOTA_INICIAL_FINTRA", "");
       String DISTRICTO                    = constanteService.getValor("FINV", "DISTRICTO", "");
       String NIT_FINTRA                   = constanteService.getValor("FINV", "NIT_FINTRA", "");


       String comandoSQL = "";
       Vector comandos_sql = new Vector();
       SerieGeneral serie  = new SerieGeneral();
       String documento = "";

       String estado = "";
       double valor_total_factura = 0.00;


       java.util.Date fechaActual = new Date();
       String creation_date = fechaActual.toString();

       Date fecha_hoy = new Date();
       DateFormat formato_fecha;
       formato_fecha = new SimpleDateFormat("yyyy-MM-dd");
       String fechaProceso = formato_fecha.format(fecha_hoy);


       try {


           List listaFacturaPorCobrarFintra;
           listaFacturaPorCobrarFintra =  consorcioDao.buscaFacturaPorCobrarFintra() ;


           if (listaFacturaPorCobrarFintra.size() > 0) {



               FacturaPorCobrarFintra facturaPorCobrarFintra = new FacturaPorCobrarFintra();
               FacturaDetalle facturaDetalle = new FacturaDetalle();
               FacturaCabecera facturaCabecera = new FacturaCabecera();

               // Armando los campos fijos para todos los items de la factura
               facturaDetalle.setDstrct(DISTRICTO);
               facturaDetalle.setTipo_documento(TIPO_DOCUMENTO_FAC);
               facturaDetalle.setConcepto(CONCEPTO_CXC_FINTRA);
               facturaDetalle.setDescripcion("Cobro de factura a Fintra" );
               facturaDetalle.setCantidad(1.00);
               facturaDetalle.setValor_tasa(1.00);
               facturaDetalle.setMoneda(MONEDA_LOCAL);
               facturaDetalle.setLast_update(creation_date);
               facturaDetalle.setUser_update(login);
               facturaDetalle.setCreation_date(creation_date);
               facturaDetalle.setCreation_user(login);
               facturaDetalle.setBase(BASE_LOCAL);
               facturaDetalle.setTipo_referencia_1(TIPO_DOCUMENTO_FAC);



               int item = 0;


               logWriter.tituloInicial("GENERACION DE FACTURAS POR COBRAR A FINTRA");

               ListIterator it = listaFacturaPorCobrarFintra.listIterator();
               while (it.hasNext()) {
                  facturaPorCobrarFintra  = (FacturaPorCobrarFintra) it.next();


                  facturaDetalle.setNit(facturaPorCobrarFintra.getNit());
                  facturaDetalle.setReferencia_1(facturaPorCobrarFintra.getDocumento());
                  serie = serieGeneralService.getSerie(DISTRICTO,OFICINA_PRINCIPAL, SIGLA_SERIE_FACTURA_CLIENTE);
                  serieGeneralService.setSerie(DISTRICTO,OFICINA_PRINCIPAL, SIGLA_SERIE_FACTURA_CLIENTE);
                  documento = serie.getUltimo_prefijo_numero();
                  facturaDetalle.setTipo_documento(documento);



                  List listaFacturaDetalle = new LinkedList();
                  FacturaDetalle facturaDetalle_general = new FacturaDetalle() ;

                  double totalCapital      = 0.00 ;
                  double totalInteres      = 0.00 ;
                  double totalCuotaInicial = 0.00;
                  double totalExtemporaneo = 0.00 ;



                  listaFacturaDetalle = consorcioDao.buscaFacturaPorCobrarDetalle(facturaPorCobrarFintra.getDocumento());

                  ListIterator it1 = listaFacturaDetalle.listIterator();
                  while (it1.hasNext()) {

                      facturaDetalle_general  = (FacturaDetalle) it1.next();


                      if(facturaDetalle_general.getConcepto().equalsIgnoreCase(CONCEPTO_CUOTA_INICIAL)) {
                          totalCuotaInicial += facturaDetalle_general.getValor_item() ;
                          valor_total_factura  += facturaDetalle_general.getValor_item();

                      }

                      else if(facturaDetalle_general.getConcepto().equalsIgnoreCase(CONCEPTO_INTERES)) {
                          totalInteres += facturaDetalle_general.getValor_item() ;
                          valor_total_factura  += facturaDetalle_general.getValor_item();
                      }

                      else if( (facturaDetalle_general.getConcepto().equalsIgnoreCase(CONCEPTO_EXTEMPORANEO_1)) ||
                               (facturaDetalle_general.getConcepto().equalsIgnoreCase(CONCEPTO_EXTEMPORANEO_2)) )  {
                          totalExtemporaneo += facturaDetalle_general.getValor_item() ;
                          valor_total_factura  +=facturaDetalle_general.getValor_item();
                      }
                      else {
                           totalCapital += facturaDetalle_general.getValor_item() ;
                           valor_total_factura  += facturaDetalle_general.getValor_item();
                      }

                  }



                  // A. ARMAR LOS ITEMS DE LA FACTURA DETALLE

                  // Armando el item de capital
                  facturaDetalle.setItem(++item);
                  facturaDetalle.setValor_unitario(totalCapital);
                  facturaDetalle.setValor_unitariome(totalCapital);
                  facturaDetalle.setValor_item(totalCapital);
                  facturaDetalle.setValor_itemme(totalCapital);
                  facturaDetalle.setReferencia_1(facturaPorCobrarFintra.getDocumento());
                  facturaDetalle.setConcepto(CONCEPTO_CAPITAL);
                  facturaDetalle.setCodigo_cuenta_contable(CUENTA_CAPITAL_FINTRA);

                  comandoSQL = consorcioDao.insertarDetalleCXC(facturaDetalle, "");
                  comandos_sql.add(comandoSQL);



                  // Armando el item de interes
                  facturaDetalle.setItem(++item);
                  facturaDetalle.setValor_unitario(totalInteres);
                  facturaDetalle.setValor_unitariome(totalInteres);
                  facturaDetalle.setValor_item(totalInteres);
                  facturaDetalle.setValor_itemme(totalInteres);
                  facturaDetalle.setReferencia_1(facturaPorCobrarFintra.getDocumento());
                  facturaDetalle.setConcepto(CONCEPTO_INTERES);
                  facturaDetalle.setCodigo_cuenta_contable(CUENTA_INTERES_FINTRA);

                  comandoSQL = consorcioDao.insertarDetalleCXC(facturaDetalle, "");
                  comandos_sql.add(comandoSQL);


                  // Armando el item de extemporaneo
                  facturaDetalle.setItem(++item);
                  facturaDetalle.setValor_unitario(totalExtemporaneo);
                  facturaDetalle.setValor_unitariome(totalExtemporaneo);
                  facturaDetalle.setValor_item(totalExtemporaneo);
                  facturaDetalle.setValor_itemme(totalExtemporaneo);
                  facturaDetalle.setReferencia_1(facturaPorCobrarFintra.getDocumento());
                  facturaDetalle.setConcepto(CONCEPTO_EXTEMPORANEO);
                  facturaDetalle.setCodigo_cuenta_contable(CUENTA_EXTEMPORANEO_FINTRA);

                  comandoSQL = consorcioDao.insertarDetalleCXC(facturaDetalle, "");
                  comandos_sql.add(comandoSQL);



                  // Armando el item de cuota inicial
                  facturaDetalle.setItem(++item);
                  facturaDetalle.setValor_unitario(totalCuotaInicial);
                  facturaDetalle.setValor_unitariome(totalCuotaInicial);
                  facturaDetalle.setValor_item(totalCuotaInicial);
                  facturaDetalle.setValor_itemme(totalCuotaInicial);
                  facturaDetalle.setReferencia_1(facturaPorCobrarFintra.getDocumento());
                  facturaDetalle.setConcepto(CONCEPTO_CUOTA_INICIAL);
                  facturaDetalle.setCodigo_cuenta_contable(CUENTA_CUOTA_INICIAL_FINTRA);

                  comandoSQL = consorcioDao.insertarDetalleCXC(facturaDetalle, "");
                  comandos_sql.add(comandoSQL);


                  comandoSQL = consorcioDao.actualizarFacturaTrasladada(facturaDetalle);
                  comandos_sql.add(comandoSQL);


                  // B. ARMAR LA CABECERA DE LA FACTURA


                  facturaCabecera.setDstrct(DISTRICTO);
                  facturaCabecera.setTipo_documento(TIPO_DOCUMENTO_FAC);
                  facturaCabecera.setDocumento(documento);
                  facturaCabecera.setNit(NIT_FINTRA);


                  // facturaCabecera.setCodcli();

                  facturaCabecera.setConcepto(CONCEPTO_CXC_FINTRA);
                  facturaCabecera.setFecha_factura(fechaProceso);
                  facturaCabecera.setFecha_vencimiento(fechaProceso);
                  facturaCabecera.setDescripcion("Cobro facturas trasladadas a Fintra de: " + creation_date.substring(0, 16 ) );
                  facturaCabecera.setValor_factura(valor_total_factura) ;
                  facturaCabecera.setValor_abono(valor_total_factura);
                  facturaCabecera.setValor_saldo(valor_total_factura);

                  facturaCabecera.setValor_facturame(valor_total_factura);
                  facturaCabecera.setValor_abonome(valor_total_factura);
                  facturaCabecera.setValor_saldome(valor_total_factura);

                  facturaCabecera.setValor_tasa(1.00);
                  facturaCabecera.setMoneda(MONEDA_LOCAL);
                  facturaCabecera.setCantidad_items(item);
                  facturaCabecera.setForma_pago("CREDITO");
                  facturaCabecera.setAgencia_facturacion(OFICINA_PRINCIPAL);
                  facturaCabecera.setAgencia_cobro(OFICINA_PRINCIPAL);
                  facturaCabecera.setBase (BASE_LOCAL);
                  facturaCabecera.setLast_update(creation_date);

                  facturaCabecera.setUser_update(login);
                  facturaCabecera.setCreation_date(creation_date);
                  facturaCabecera.setCreation_user(login);
                  facturaCabecera.setCmc("CM");


                  comandoSQL = consorcioDao.insertarCabeceraCXC(facturaCabecera, "");
                  comandos_sql.add(comandoSQL);


                  // Actualizar todo el lote de comandos SQL


                  estado = model.consorcioService.ejecutarBatchSQL(comandos_sql, logWriter);

                  if (estado.equalsIgnoreCase("ERROR")){
                      logWriter.log("ERROR: Se genero error al ejecutar el Batch de SQL. Se efectuo rollback. No se realizo la  factura. \n",LogWriter.INFO);
                  }
                  else{
                      // logWriter.log("    Registra en base de datos sin error \n",LogWriter.INFO);
                  }
                  comandos_sql.clear();


               } // Final del while que recorre la lista de facturas



               // Actualizar todo el lote de comandos SQL


               estado = model.consorcioService.ejecutarBatchSQL(comandos_sql, logWriter);

               if (estado.equalsIgnoreCase("ERROR")){
                   logWriter.log("ERROR: Se genero error al ejecutar el Batch de SQL. Se efectuo rollback. No se realizo la  factura. \n",LogWriter.INFO);
               }
               else{
                   // logWriter.log("    Registra en base de datos sin error \n",LogWriter.INFO);
               }
               comandos_sql.clear();

           }
           // No existen facturas con notas credito
           else {
               logWriter.log("NO EXITEN FACTURAS CON NOTA CREDITO PARA ELABORARLE LAS FACTURAS A COBRARLE A FINTRA \n",LogWriter.INFO);
           }


           logWriter.log("FINAL  PROCESO GENERACION DE APLICACION DEL FACTORING Y/O FORMULA \n",LogWriter.INFO);


       }
       catch (Exception e) {

           logWriter.log("ERROR: Se presento el siguiente error:  \n"  ,LogWriter.INFO);
           logWriter.log(e.getMessage());

       }
    }


    /**
     * Metodo que crea facturas de cobro al cliente en Fintra basada en la factura de cobro al cliente del Consorcio
     * La factura creada tiene 5 items : Capital, Financiacion, Cuota Inicial, Extemporaneidad, Consorcio Opav
     * @author Alvaro Pabon Martinez
     * @version 1.0
     * @param login String Es el usuario de la sesion que ingreso al sistema
     * @param model Objeto Model que identifica los objetos y variables exclusivas de cada usuario en la sesion
     * @param logWriter Objeto LogWriter que permite escribir en un log el control del proceso
     * @throws SQLException
     * Modificaciones :
     * 2010-05-07 Corrige error de longitud de substring en la busqueda del numero de la factura
     * 2010-06-09 Corrige el numero de factura que se traslada a Fintra ya que colocaba la misma CF en vez de NM
     * 2010-06-11 Corrige el nombre de la base de datos donde se debe actualizar la factura de traslado, para que no se vuelva a trasladar a fintra
     * 2010-06-14 Corrige en el item la descripcion para que se estaba acumulando con la de los otros items
     */


    public void generarFacturaTraslado(String login, ModelOpav model, LogWriter logWriter) throws SQLException    {

       // Definicion de constantes

       String TIPO_DOCUMENTO_FAC           = constanteService.getValor("FINV", "TIPO_DOCUMENTO_FAC", "");
       String SIGLA_SERIE_FACTURA_CLIENTE_FINTRA  = constanteService.getValor("FINV", "SIGLA_SERIE_FACTURA_CLIENTE_FINTRA", "");
       String OFICINA_PRINCIPAL            = constanteService.getValor("FINV", "OFICINA_PRINCIPAL", "");
       String BASE_LOCAL                   = constanteService.getValor("FINV", "BASE_LOCAL", "");
       String MONEDA_LOCAL                 = constanteService.getValor("FINV", "MONEDA_LOCAL", "");
       String CONCEPTO_CXC_FINTRA          = constanteService.getValor("FINV", "CONCEPTO_CXC_FINTRA", "");

       String CONCEPTO_INTERES             = constanteService.getValor("FINV", "CONCEPTO_INTERES", "");
       String CONCEPTO_EXTEMPORANEO_1      = constanteService.getValor("FINV", "CONCEPTO_EXTEMPORANEO_1", "");
       String CONCEPTO_EXTEMPORANEO_2      = constanteService.getValor("FINV", "CONCEPTO_EXTEMPORANEO_2", "");
       String CONCEPTO_CUOTA_INICIAL       = constanteService.getValor("FINV", "CONCEPTO_CUOTA_INICIAL", "");
       String DISTRICTO                    = constanteService.getValor("FINV", "DISTRICTO", "");
       String CUENTA_INTERES_FINTRA        = constanteService.getValor("FINV", "CUENTA_INTERES_FINTRA", "");
       String CONCEPTO_EXTEMPORANEO        = constanteService.getValor("FINV", "CONCEPTO_EXTEMPORANEO", "");
       String CUENTA_EXTEMPORANEO_FINTRA   = constanteService.getValor("FINV", "CUENTA_EXTEMPORANEO_FINTRA", "");
       String CUENTA_CUOTA_INICIAL_FINTRA  = constanteService.getValor("FINV", "CUENTA_CUOTA_INICIAL_FINTRA", "");
       String CUENTA_CAPITAL_FINTRA        = constanteService.getValor("FINV", "CUENTA_CAPITAL_FINTRA", "");
       String CONCEPTO_CAPITAL             = constanteService.getValor("FINV", "CONCEPTO_CAPITAL", "");


       // Definicion de variables

       String estado = "";
       String comandoSQL = "";
       Vector comandos_sql =new Vector();

       Vector comandos_sql_consorcio =new Vector();//20100610

       SerieGeneral serie = new SerieGeneral();

       java.util.Date fechaActual = new Date();
       String creation_date = fechaActual.toString();

       Date fecha_hoy = new Date();
       DateFormat formato_fecha;
       formato_fecha = new SimpleDateFormat("yyyy-MM-dd");
       String fechaProceso = formato_fecha.format(fecha_hoy);


       String condicion = "";
       double valor = 0.00;
       String documento = "";



       try {


           List listaFacturaPorTrasladar;

           // PASO 1. Ubica en una lista las facturas a trasladar a Fintra. Los objetos de la lista son de tipo FacturaCabecera
           listaFacturaPorTrasladar =  consorcioDao.buscaFacturaPorTrasladar() ;

           // PASO 2. Generacion de las facturas a trasladar a Fintra
           if (listaFacturaPorTrasladar.size() > 0) {

               FacturaCabecera facturaCabecera =  new FacturaCabecera();
               FacturaDetalle facturaDetalle = new FacturaDetalle();

               logWriter.tituloInicial("GENERACION DE FACTURAS POR TRASLADAR A FINTRA");

               ListIterator it = listaFacturaPorTrasladar.listIterator();
               while (it.hasNext()) {

                   // PASO 2.1 Construye la cabecera de la factura
                   facturaCabecera  = (FacturaCabecera) it.next();

                   // Determinar el numero de la factura

                   String idSolicitud             = facturaCabecera.getReferencia_1();
                   int parcial                    = Integer.parseInt(facturaCabecera.getReferencia_2() ) ;
                   String facturaFintra           = facturaCabecera.getReferencia_3(); // Si diferente de vacio debe ser una factura de tipo NM
                   String facturaConsorcio        = facturaCabecera.getDocumento();    // Es una factura de tipo CF

                   // Consigue el codigo del cliente en Fintra
                   String idCliente               = facturaCabecera.getCodcli();
                   Equivalencia  codigoEquivalente = getEquivalencia("CONSORCIO","FINTRA","CLIENTE",idCliente);
                   if ( (codigoEquivalente != null) && (!codigoEquivalente.getCodigo_final().isEmpty()) ) {

                       // Consigue el nit del cliente en
                       String nitFintra = consorcioDao.getNit(codigoEquivalente.getCodigo_final(),"fintra");
                       if (nitFintra != "") {

                           // La factura del consorcio a trasladar a fintra no tiene una factura ya asignada

                           // Se busca en factura_consorcio si la solicitud con su parcial tiene ya una factura CF o NM registrada

                           String documentoCF_NM = this.getNumeroFacturaConsorcio(idSolicitud, parcial, idCliente);


                           logWriter.log("INFO : INICIA DEFINICION DEL NUMERO DE DOCUMENTO EN FINTRA \n"       ,LogWriter.INFO);

                           logWriter.log("       Procesa documento : idSolicitud      = " + idSolicitud + " \n"       ,LogWriter.INFO);
                           logWriter.log("                           parcial          = " + parcial     + " \n"       ,LogWriter.INFO);
                           logWriter.log("                           facturaFintra    = " + facturaFintra     + " \n"       ,LogWriter.INFO);
                           logWriter.log("                           facturaConsorcio = " + facturaConsorcio  + " \n"       ,LogWriter.INFO);

                           logWriter.log("                           documentoCF_NM   = " + documentoCF_NM + " \n"       ,LogWriter.INFO);


                           if (documentoCF_NM.isEmpty() )   {

                               // CASO 1:  NO EXISTE UN REGISTRO EN FACTURA CONSORCIO

                               logWriter.log("       Entra en CASO 1 \n"       ,LogWriter.INFO);
                               

                               // Se debe conseguir un numero de serie nuevo
                               
                               serie = serieGeneralService.getSerie("FINV", OFICINA_PRINCIPAL, SIGLA_SERIE_FACTURA_CLIENTE_FINTRA);
                               serieGeneralService.setSerie("FINV",OFICINA_PRINCIPAL, SIGLA_SERIE_FACTURA_CLIENTE_FINTRA);
                             
                               
                               documento = serie.getUltimo_prefijo_numero() + "_" +  (facturaCabecera.getDocumento().split("_"))[1];

                               logWriter.log("       documento = " + documento + " \n"       ,LogWriter.INFO);

                           }
                           else {

                               // CASO 2: EXISTE UN REGISTRO EN FACTURA CONSORCIO

                               logWriter.log("       Entra en CASO 2 \n"       ,LogWriter.INFO);


                               String[] campoNumeroFacturas = documentoCF_NM.split("%");
                               String documentoInicial = campoNumeroFacturas[0];
                               String documentoCorficolombiana = "";

                               boolean existeDocumentoCorficolombiana = true;

                               if(campoNumeroFacturas.length != 2) {
                                   existeDocumentoCorficolombiana = false;
                               }
                               else {
                                 documentoCorficolombiana = campoNumeroFacturas[1];
                                 if (documentoCorficolombiana.isEmpty())  {
                                     existeDocumentoCorficolombiana = false;
                                 }
                               }


                               if (existeDocumentoCorficolombiana){

                                   logWriter.log("       Entra en CASO 2A \n"       ,LogWriter.INFO);

                                   // CASO 2A : EL REGISTRO CONTIENE UN DOCUMENTO CORFICOLOMBIANA DE TIPO NM

                                   // El numero de la factura a Fintra es el numero de factura Corficolombiana sin la raya y numero de cuota
                                   // La cuota es la que venga en el registro.


                                   String facturaCorficolombianaGeneral = documentoCorficolombiana.substring(0, 7);
                                   documento = facturaCorficolombianaGeneral + "_" +  (facturaCabecera.getDocumento().split("_"))[1];

                                   logWriter.log("       documento = " + documento + " \n"       ,LogWriter.INFO);

                                   comandoSQL = this.setFacturaConsorcio(facturaConsorcio, documento);
                                   comandos_sql_consorcio.add(comandoSQL);

                               }
                               else  {
                                   // CASO 2B : EL REGISTRO NO CONTIENE UN DOCUMENTO CORFICOLOMBIANA DE TIPO NM

                                   logWriter.log("       Entra en CASO 2B \n"       ,LogWriter.INFO);

                                   // Revisar si el documentoInicial es de tipo CF o NM

                                   if(documentoInicial.substring(0,2).equalsIgnoreCase("CF")) {


                                       logWriter.log("       Entra en CASO 2B1 \n"       ,LogWriter.INFO);

                                       // Se debe conseguir un numero de serie nuevo
                                       serie = serieGeneralService.getSerie("FINV",OFICINA_PRINCIPAL, SIGLA_SERIE_FACTURA_CLIENTE_FINTRA);
                                       serieGeneralService.setSerie("FINV",OFICINA_PRINCIPAL, SIGLA_SERIE_FACTURA_CLIENTE_FINTRA);
                                       documento = serie.getUltimo_prefijo_numero() + "_" +  (facturaCabecera.getDocumento().split("_"))[1];//20100609

                                       logWriter.log("       documento = " + documento + " \n"       ,LogWriter.INFO);

                                       comandoSQL = this.setFacturaConsorcio(facturaConsorcio, documento);
                                       comandos_sql_consorcio.add(comandoSQL);

                                   } else {

                                       logWriter.log("       Entra en CASO 2B2 \n"       ,LogWriter.INFO);

                                       documento = documentoInicial;

                                       logWriter.log("       documento = " + documento + " \n"       ,LogWriter.INFO);

                                       comandoSQL = this.setFacturaConsorcio(facturaConsorcio, documento);
                                       comandos_sql_consorcio.add(comandoSQL);

                                   }

                               }

                           }

                           logWriter.log("INFO : FINALIZA DEFINICION DEL NUMERO DE DOCUMENTO EN FINTRA \n"       ,LogWriter.INFO);


                           String numeroFacturaTrasladada =  facturaCabecera.getDocumento();
                           facturaCabecera.setDocumento(documento);
                           facturaCabecera.setNit(nitFintra);

                           facturaCabecera.setTipo_documento(TIPO_DOCUMENTO_FAC);
                           facturaCabecera.setLast_update(creation_date);
                           facturaCabecera.setUser_update(login);
                           facturaCabecera.setCreation_date(creation_date);
                           facturaCabecera.setCreation_user(login);
                           facturaCabecera.setNit_enviado_fiducia("");
                           facturaCabecera.setCorficolombiana("");
                           facturaCabecera.setPagado_fenalco("");
                           facturaCabecera.setDescripcion(facturaCabecera.getDescripcion() + " Factura Consorcio: " + facturaConsorcio);
                           facturaCabecera.setValor_abono(0.00);
                           facturaCabecera.setValor_saldo(facturaCabecera.getValor_factura());
                           facturaCabecera.setValor_abonome(0.00);
                           facturaCabecera.setValor_saldome(facturaCabecera.getValor_factura());
                           facturaCabecera.setCmc("OV");
                           // Se convierte el codigo del cliente del Consorcio en el codigo del cliente de Fintra
                           facturaCabecera.setCodcli(codigoEquivalente.getCodigo_final());
                           facturaCabecera.setFecha_contabilizacion("0099-01-01 00:00:00");
                           facturaCabecera.setPeriodo("");
                           facturaCabecera.setTransaccion(0);




                           // PASO 2.2 Construye los detalles de la factura

                           // Armando los campos fijos para todos los items de la factura
                           facturaDetalle.setDstrct(DISTRICTO);
                           facturaDetalle.setTipo_documento(TIPO_DOCUMENTO_FAC);
                           facturaDetalle.setDocumento(documento);
                           facturaDetalle.setNit(nitFintra);
                           facturaDetalle.setConcepto(CONCEPTO_CXC_FINTRA);

                           String descripcion = "Factura " + numeroFacturaTrasladada + " cedida por el Consorcio"; // 2010-06-14
                           facturaDetalle.setDescripcion(descripcion ); // 2010-06-14
                           facturaDetalle.setCantidad(1.00);
                           facturaDetalle.setValor_tasa(1.00);
                           facturaDetalle.setMoneda(MONEDA_LOCAL);
                           facturaDetalle.setLast_update(creation_date);
                           facturaDetalle.setUser_update(login);
                           facturaDetalle.setCreation_date(creation_date);
                           facturaDetalle.setCreation_user(login);
                           facturaDetalle.setBase(BASE_LOCAL);
                           facturaDetalle.setTipo_referencia_1(TIPO_DOCUMENTO_FAC);


                           int numeroItems = 0;

                           // Intereses
                           condicion = "b.concepto = '" + CONCEPTO_INTERES + "' ";
                           valor     = consorcioDao.buscaFacturaDetalle( numeroFacturaTrasladada, condicion);
                           if (valor != 0.00) {
                               numeroItems++;

                               facturaDetalle.setItem(numeroItems);
                               facturaDetalle.setValor_unitario(valor);
                               facturaDetalle.setValor_unitariome(valor);
                               facturaDetalle.setValor_item(valor);
                               facturaDetalle.setValor_itemme(valor);
                               facturaDetalle.setReferencia_1(numeroFacturaTrasladada);
                               facturaDetalle.setConcepto(CONCEPTO_INTERES);
                               facturaDetalle.setCodigo_cuenta_contable(CUENTA_INTERES_FINTRA);
                               facturaDetalle.setDescripcion(descripcion + " Item Financiacion"); // 2010-06-14

                               comandoSQL = consorcioDao.insertarDetalleCXC(facturaDetalle, "fintra");
                               comandos_sql.add(comandoSQL);

                           }



                           // Extemporaneo
                           condicion = "b.concepto in ('" + CONCEPTO_EXTEMPORANEO_1 + "', '" + CONCEPTO_EXTEMPORANEO_2 + "' )"    ;
                           valor     = consorcioDao.buscaFacturaDetalle( numeroFacturaTrasladada, condicion);
                           if (valor != 0.00) {
                               numeroItems++;
                               facturaDetalle.setItem(numeroItems);
                               facturaDetalle.setValor_unitario(valor);
                               facturaDetalle.setValor_unitariome(valor);
                               facturaDetalle.setValor_item(valor);
                               facturaDetalle.setValor_itemme(valor);
                               facturaDetalle.setReferencia_1(numeroFacturaTrasladada);
                               facturaDetalle.setConcepto(CONCEPTO_EXTEMPORANEO);
                               facturaDetalle.setCodigo_cuenta_contable(CUENTA_EXTEMPORANEO_FINTRA);
                               facturaDetalle.setDescripcion(descripcion + " Item Extemporaneo"); // 2010-06-14

                               comandoSQL = consorcioDao.insertarDetalleCXC(facturaDetalle, "fintra");
                               comandos_sql.add(comandoSQL);

                           }

                           // Cuota inicial
                           condicion = "b.concepto = '" + CONCEPTO_CUOTA_INICIAL + "' ";
                           valor     = consorcioDao.buscaFacturaDetalle( numeroFacturaTrasladada, condicion);
                           if (valor != 0.00) {
                               numeroItems++;
                               facturaDetalle.setItem(numeroItems);
                               facturaDetalle.setValor_unitario(valor);
                               facturaDetalle.setValor_unitariome(valor);
                               facturaDetalle.setValor_item(valor);
                               facturaDetalle.setValor_itemme(valor);
                               facturaDetalle.setReferencia_1(numeroFacturaTrasladada);
                               facturaDetalle.setConcepto(CONCEPTO_CUOTA_INICIAL);
                               facturaDetalle.setCodigo_cuenta_contable(CUENTA_CUOTA_INICIAL_FINTRA);
                               facturaDetalle.setDescripcion(descripcion + " Item Cuota Inicial"); // 2010-06-14

                               comandoSQL = consorcioDao.insertarDetalleCXC(facturaDetalle, "fintra");
                               comandos_sql.add(comandoSQL);

                           }


                           // Capital sin tener en cuenta Interes, Cuota Inicial, Opav, Interes Opav, Valor extemporaneo 1 y 2

                           condicion  = "b.concepto not in ('" + CONCEPTO_INTERES + "', '"  ;
                           condicion += CONCEPTO_EXTEMPORANEO_1 + "', '";
                           condicion += CONCEPTO_EXTEMPORANEO_2 + "', '";
                           condicion += CONCEPTO_CUOTA_INICIAL  + "') ";
                           valor     = consorcioDao.buscaFacturaDetalle( numeroFacturaTrasladada, condicion);
                           if (valor != 0.00) {
                               numeroItems++;
                               facturaDetalle.setItem(numeroItems);
                               facturaDetalle.setValor_unitario(valor);
                               facturaDetalle.setValor_unitariome(valor);
                               facturaDetalle.setValor_item(valor);
                               facturaDetalle.setValor_itemme(valor);
                               facturaDetalle.setReferencia_1(numeroFacturaTrasladada);
                               facturaDetalle.setConcepto(CONCEPTO_CAPITAL);
                               facturaDetalle.setCodigo_cuenta_contable(CUENTA_CAPITAL_FINTRA);
                               facturaDetalle.setDescripcion(descripcion + " Item Capital");

                               comandoSQL = consorcioDao.insertarDetalleCXC(facturaDetalle, "fintra");
                               comandos_sql.add(comandoSQL);
                           }



                           // PASO 2.3  Graba en la base de datos de fintra la factura creada


                           facturaCabecera.setCantidad_items(numeroItems);
                           facturaCabecera.setReferencia_3(facturaConsorcio);
                           comandoSQL = consorcioDao.insertarCabeceraCXC(facturaCabecera,"fintra");
                           comandos_sql.add(comandoSQL);



                           // PASO 3. Actualizar todo el lote de comandos SQL


                           estado = model.consorcioService.ejecutarBatchSQL(comandos_sql, logWriter,"fintra");

                           if (estado.equalsIgnoreCase("ERROR")){
                               logWriter.log("ERROR: Se genero error al ejecutar el Batch de SQL. Se efectuo rollback. No se realizo la  factura en Fintra. \n" +
                                             "       Solicitud = " + idSolicitud + "  Parcial = " + facturaCabecera.getReferencia_2() + "  Cliente = " + idCliente +
                                             "       Factura consorcio = " + facturaConsorcio + " \n"       ,LogWriter.INFO);
                           }
                           else{
                               // logWriter.log("    Registra en base de datos sin error \n",LogWriter.INFO);
                           }
                           comandos_sql.clear();


                           // PASO 4. Actualizar en el Consorcio la cabecera de la factura CF con el numero NM de Fintra

                           comandoSQL = consorcioDao.actualizarFacturaCabeceraTrasladada( "FINV",  TIPO_DOCUMENTO_FAC, numeroFacturaTrasladada,  documento);//20100611
                           comandos_sql.add(comandoSQL);
                           estado = model.consorcioService.ejecutarBatchSQL(comandos_sql, logWriter,"consorcio");//20100611

                           if (estado.equalsIgnoreCase("ERROR")){
                               logWriter.log("ERROR: Se genero error al ejecutar el Batch de SQL. Se efectuo rollback. \n" +
                                             "       No se realizo actualizacion en la factura del Consorcio con la factura traslada a Fintra. \n" +
                                             "       Factura Consorcio = " + documento + " Factura Fintra = " + numeroFacturaTrasladada + " \n" +
                                             "       Debe actualizarse la columna consorcio.con.factura.factura_traslado = " + numeroFacturaTrasladada + "\n" ,LogWriter.INFO);

                           }
                           else{
                               estado = model.consorcioService.ejecutarBatchSQL(comandos_sql_consorcio, logWriter, "consorcio");//20100610
                               if (estado.equalsIgnoreCase("ERROR")){//20100610
                                   logWriter.log("ERROR2: Se genero error al ejecutar el Batch de SQL. Se efectuo rollback. \n" +
                                                 "       No se realizo actualizacion en la factura del Consorcio con la factura traslada a Fintra. \n" +
                                                 "       Factura Consorcio = " + documento + " Factura Fintra = " + numeroFacturaTrasladada + " \n" +
                                                 "       Debe actualizarse la columna consorcio.con.factura.factura_traslado = " + numeroFacturaTrasladada + "\n" ,LogWriter.INFO);//20100610
                                   throw new Exception(" algo paso en el consorcio...");//20100610
                               }else{//20100610
                                    // logWriter.log("    en el consorcio tambien. \n",LogWriter.INFO);//20100610
                               }//20100610

                               //logWriter.log("    Registra en base de datos sin error \n",LogWriter.INFO);
                           }
                           comandos_sql.clear();
                           comandos_sql_consorcio.clear();//20100610

                       }  // fin de la existencia del nit
                       else {
                               // En este caso no existe un nit en la base de datos de Fintra para un codigo equivalente
                               logWriter.log("ERROR: No existe nit en Fintra para el codigo cliente = " +  codigoEquivalente.getCodigo_final() + "\n" +
                                             "       El codigo del cliente en el Consorcio = " + idCliente + "\n" +
                                             "       No se genero factura para la id_solicitud = " + idSolicitud + "-" + parcial + "\n" +
                                             "       Factura Consorcio = " + facturaConsorcio + "\n" +
                                             "       Factura Fintra    = " + facturaFintra + "\n" +
                                             "       Cliente Consorcio = " + idCliente + "\n"  ,LogWriter.INFO);
                       }


                   }  // fin de la existencia codigo equivalente
                   else

                   {
                       // En este caso no existe un codigo equivalente en fintra del codigo del cliente y por lo tanto  no se genera factura
                       logWriter.log("ERROR: No existe codigo equivalente del cliente en Fintra  \n" +
                                     "       No se genero factura para la id_solicitud = " + idSolicitud + "-" + parcial + " \n" +
                                     "       Factura Consorcio = " + facturaConsorcio + " \n" +
                                     "       Factura Fintra    = " + facturaFintra + " \n" +
                                     "       Cliente Consorcio = " + idCliente + " \n"  ,LogWriter.INFO);
                   }

               } // Fin de la lista de facturas a trasladar

           } // Fin de comparativo de que la lista sea mayor que cero

           // No existen facturas con NC elaboradas para realizarle la factura de traslado a Fintra
           else {
               logWriter.log("NO EXISTEN FACTURAS CON NC PARA TRASLADARLE A FINTRA  \n"  ,LogWriter.INFO);
           }
       }
       catch (Exception e) {

           logWriter.log("ERROR: Se presento el siguiente error:  \n"  ,LogWriter.INFO);
           logWriter.log(e.getMessage());
           Util.imprimirTrace(e);//20100610

       }




    }
    
    /**
     *Metodo que genera la factura de traslado de selectric a fintra.
     * @author Ing.Edgar Gonzalez Mendoza.
     * @version 1.0
     * @param usuario
     * @throws java.sql.SQLException
     */
    public void generarCxcFintra(Usuario usuario)throws SQLException{
        consorcioDao.generarCxcFintra(usuario);
    }



    public String actualizarFacturaCabeceraTrasladada(String dstrct, String tipoDocumento, String documento, String facturaFintra) throws SQLException{

        return consorcioDao.actualizarFacturaCabeceraTrasladada(dstrct, tipoDocumento, documento, facturaFintra);
    }



    /**
     * Convierte un codigo en otro codigo equivalente
     * @param ciaInicial Compania inicial desde donde se envia el codigo
     * @param ciaFinal   Comapania final en donde queda el codigo nuevo
     * @param claseInicial  Tipo de equivalencia a realizar
     * @param codigoInicial  Codigo a convertir en un codigo equivalente
     * @return
     * @throws SQLException
     */
    public Equivalencia getEquivalencia(String ciaInicial, String ciaFinal, String claseInicial, String codigoInicial)throws SQLException{

        return consorcioDao.getEquivalencia( ciaInicial,  ciaFinal,  claseInicial,  codigoInicial);
    }







    // *************************************************************************
    // *
    // *  AREA DE METODOS DEL PROCESO DE CONTRATISTA
    // *
    // *************************************************************************


    public void generaCxpInterna(String usuario, String dstrct,  ModelOpav model, LogWriter logWriter) {





        try{

            logWriter.tituloInicial("GENERACION DE PREFACTURAS DE CONTRATISTAS");
            
                    String  CUENTA_MATERIAL_CONTRATISTA         = "";
                    String  CUENTA_MANO_OBRA_CONTRATISTA        = "";
                    String  CUENTA_OTROS_CONTRATISTA            = "";
                    String  CUENTA_ADMINISTRACION_CONTRATISTA   = "";
                    String  CUENTA_IMPREVISTO_CONTRATISTA       = "";
                    String  CUENTA_UTILIDAD_CONTRATISTA         = "";
                    String  CUENTA_BONIFICACION_CONTRATISTA     = "";

                    String  CUENTA_IVA_CONTRATISTA              = "";
                    String  CUENTA_RET_MATERIAL_CONTRATISTA     = "";
                    String  CUENTA_RET_MO_CONTRATISTA           = "";
                    String  CUENTA_RET_OTROS_CONTRATISTA        = "";
                    String  CUENTA_RET_AIU_CONTRATISTA          = "";
                    String  CUENTA_RETEICA_CONTRATISTA          = "";
                    String  CUENTA_RETEIVA_CONTRATISTA          = "";

                    String  CONCEPTO_MATERIAL_CONTRATISTA       = "";
                    String  CONCEPTO_MANO_OBRA_CONTRATISTA      = "";
                    String  CONCEPTO_OTROS_CONTRATISTA          = "";
                    String  CONCEPTO_ADMINISTRACION_CONTRATISTA = "";
                    String  CONCEPTO_IMPREVISTO_CONTRATISTA     = "";
                    String  CONCEPTO_UTILIDAD_CONTRATISTA       = "";
                    String  CONCEPTO_BONIFICACION_CONTRATISTA   = "";

                    String  CONCEPTO_IVA_CONTRATISTA            = "";
                    String  CONCEPTO_RET_MATERIAL_CONTRATISTA   = "";
                    String  CONCEPTO_RET_MO_CONTRATISTA         = "";
                    String  CONCEPTO_RET_OTROS_CONTRATISTA      = "";
                    String  CONCEPTO_RET_AIU_CONTRATISTA        = "";
                    String  CONCEPTO_RETEICA_CONTRATISTA        = "";
                    String  CONCEPTO_RETEIVA_CONTRATISTA        = "";

                    String SIGLA_ACCION                         = "";
                    String SIGLA_PREFACTURA_CONTRATISTA         = "";
                    String MONEDA_LOCAL                         = "";
                    String BASE_LOCAL                           = "";
                    String CLASE_DOCUMENTO                      = "";
                    String TIPO_DOCUMENTO_FAP                   = "";
                    String HANDLE_CODE_CI                       = "";
                    String APROBADOR_AUTOMATICO                 = "";
                    String AGENCIA_PRINCIPAL                    = "";
            
            int totalGrabados = 0;
            String estado = "";
            Vector comandos_sql =new Vector();
            java.util.Date fechaActual = new Date();
            String creation_date = fechaActual.toString();

            List listaFacturaGeneral =  model.consorcioService.getFacturaGeneral();
            
            if (listaFacturaGeneral.size() != 0){

                
                int totalPrefacturas = listaFacturaGeneral.size();

                logWriter.log("LISTA DE PREFACTURAS DE CONTRATISTA CREADA \n",LogWriter.INFO);
                logWriter.log("Total prefacturas leidas : " + totalPrefacturas + " \n",LogWriter.INFO);


                FacturaGeneral facturaGeneral = new FacturaGeneral();
                Iterator it = listaFacturaGeneral.iterator();

                String comandoSQL = "";

                while (it.hasNext()  ) {

                    facturaGeneral = (FacturaGeneral)it.next();
                    
                    //preguntamos si es aires.
                    if(facturaGeneral.getNum_os().subSequence(0, 4).equals("ACON") && facturaGeneral.getTipo_distribucion().trim().equals("43")){
                    
                    CUENTA_MATERIAL_CONTRATISTA         = constanteService.getValor("FINV", "AIRES_CUENTA_MATERIAL_CONTRATISTA", "");
                    CUENTA_MANO_OBRA_CONTRATISTA        = constanteService.getValor("FINV", "AIRES_CUENTA_MANO_OBRA_CONTRATISTA", "");
                    CUENTA_OTROS_CONTRATISTA            = constanteService.getValor("FINV", "AIRES_CUENTA_OTROS_CONTRATISTA", "");
                    CUENTA_IVA_CONTRATISTA              = constanteService.getValor("FINV", "AIRES_CUENTA_IVA_CONTRATISTA", "");
                    
                    CONCEPTO_MATERIAL_CONTRATISTA       = constanteService.getValor("FINV", "AIRES_CONCEPTO_MATERIAL_CONTRATISTA", "");
                    CONCEPTO_MANO_OBRA_CONTRATISTA      = constanteService.getValor("FINV", "AIRES_CONCEPTO_MANO_OBRA_CONTRATISTA", "");
                    CONCEPTO_OTROS_CONTRATISTA          = constanteService.getValor("FINV", "AIRES_CONCEPTO_OTROS_CONTRATISTA", "");
                    
                    CONCEPTO_IVA_CONTRATISTA            = constanteService.getValor("FINV", "AIRES_CONCEPTO_IVA_CONTRATISTA", "");                                       
                    
                    SIGLA_ACCION                         = constanteService.getValor("FINV", "SIGLA_ACCION", "");
                    SIGLA_PREFACTURA_CONTRATISTA         = constanteService.getValor("FINV", "SIGLA_PREFACTURA_CONTRATISTA", "");
                    MONEDA_LOCAL                         = constanteService.getValor("FINV", "MONEDA_LOCAL", "");
                    BASE_LOCAL                           = constanteService.getValor("FINV", "BASE_LOCAL", "");
                    CLASE_DOCUMENTO                      = constanteService.getValor("FINV", "CLASE_DOCUMENTO", "");
                    TIPO_DOCUMENTO_FAP                   = constanteService.getValor("FINV", "TIPO_DOCUMENTO_FAP", "");
                    HANDLE_CODE_CI                       = constanteService.getValor("FINV", "HANDLE_CODE_AS", ""); //crearlo
                    APROBADOR_AUTOMATICO                 = constanteService.getValor("FINV", "APROBADOR_AUTOMATICO", "");
                    AGENCIA_PRINCIPAL                    = constanteService.getValor("FINV", "AGENCIA_PRINCIPAL", "");
                    
                    }else{
                        
                        
                        // Definicion de las cuentas para cada concepto
                    CUENTA_MATERIAL_CONTRATISTA         = constanteService.getValor("FINV", "CUENTA_MATERIAL_CONTRATISTA", "");
                    CUENTA_MANO_OBRA_CONTRATISTA        = constanteService.getValor("FINV", "CUENTA_MANO_OBRA_CONTRATISTA", "");
                    CUENTA_OTROS_CONTRATISTA            = constanteService.getValor("FINV", "CUENTA_OTROS_CONTRATISTA", "");
                    CUENTA_ADMINISTRACION_CONTRATISTA   = constanteService.getValor("FINV", "CUENTA_ADMINISTRACION_CONTRATISTA", "");
                    CUENTA_IMPREVISTO_CONTRATISTA       = constanteService.getValor("FINV", "CUENTA_IMPREVISTO_CONTRATISTA", "");
                    CUENTA_UTILIDAD_CONTRATISTA         = constanteService.getValor("FINV", "CUENTA_UTILIDAD_CONTRATISTA", "");
                    CUENTA_BONIFICACION_CONTRATISTA     = constanteService.getValor("FINV", "CUENTA_BONIFICACION_CONTRATISTA", "");

                    CUENTA_IVA_CONTRATISTA              = constanteService.getValor("FINV", "CUENTA_IVA_CONTRATISTA", "");
                    CUENTA_RET_MATERIAL_CONTRATISTA     = constanteService.getValor("FINV", "CUENTA_RET_MATERIAL_CONTRATISTA", "");
                    CUENTA_RET_MO_CONTRATISTA           = constanteService.getValor("FINV", "CUENTA_RET_MO_CONTRATISTA", "");
                    CUENTA_RET_OTROS_CONTRATISTA        = constanteService.getValor("FINV", "CUENTA_RET_OTROS_CONTRATISTA", "");
                    CUENTA_RET_AIU_CONTRATISTA          = constanteService.getValor("FINV", "CUENTA_RET_AIU_CONTRATISTA", "");
                    CUENTA_RETEICA_CONTRATISTA          = constanteService.getValor("FINV", "CUENTA_RETEICA_CONTRATISTA", "");
                    CUENTA_RETEIVA_CONTRATISTA          = constanteService.getValor("FINV", "CUENTA_RETEIVA_CONTRATISTA", "");

                    CONCEPTO_MATERIAL_CONTRATISTA       = constanteService.getValor("FINV", "CONCEPTO_MATERIAL_CONTRATISTA", "");
                    CONCEPTO_MANO_OBRA_CONTRATISTA      = constanteService.getValor("FINV", "CONCEPTO_MANO_OBRA_CONTRATISTA", "");
                    CONCEPTO_OTROS_CONTRATISTA          = constanteService.getValor("FINV", "CONCEPTO_OTROS_CONTRATISTA", "");
                    CONCEPTO_ADMINISTRACION_CONTRATISTA = constanteService.getValor("FINV", "CONCEPTO_ADMINISTRACION_CONTRATISTA", "");
                    CONCEPTO_IMPREVISTO_CONTRATISTA     = constanteService.getValor("FINV", "CONCEPTO_IMPREVISTO_CONTRATISTA", "");
                    CONCEPTO_UTILIDAD_CONTRATISTA       = constanteService.getValor("FINV", "CONCEPTO_UTILIDAD_CONTRATISTA", "");
                    CONCEPTO_BONIFICACION_CONTRATISTA   = constanteService.getValor("FINV", "CONCEPTO_BONIFICACION_CONTRATISTA", "");

                    CONCEPTO_IVA_CONTRATISTA            = constanteService.getValor("FINV", "CONCEPTO_IVA_CONTRATISTA", "");
                    CONCEPTO_RET_MATERIAL_CONTRATISTA   = constanteService.getValor("FINV", "CONCEPTO_RET_MATERIAL_CONTRATISTA", "");
                    CONCEPTO_RET_MO_CONTRATISTA         = constanteService.getValor("FINV", "CONCEPTO_RET_MO_CONTRATISTA", "");
                    CONCEPTO_RET_OTROS_CONTRATISTA      = constanteService.getValor("FINV", "CONCEPTO_RET_OTROS_CONTRATISTA", "");
                    CONCEPTO_RET_AIU_CONTRATISTA        = constanteService.getValor("FINV", "CONCEPTO_RET_AIU_CONTRATISTA", "");
                    CONCEPTO_RETEICA_CONTRATISTA        = constanteService.getValor("FINV", "CONCEPTO_RETEICA_CONTRATISTA", "");
                    CONCEPTO_RETEIVA_CONTRATISTA        = constanteService.getValor("FINV", "CONCEPTO_RETEIVA_CONTRATISTA", "");

                    SIGLA_ACCION                         = constanteService.getValor("FINV", "SIGLA_ACCION", "");
                    SIGLA_PREFACTURA_CONTRATISTA         = constanteService.getValor("FINV", "SIGLA_PREFACTURA_CONTRATISTA", "");
                    MONEDA_LOCAL                         = constanteService.getValor("FINV", "MONEDA_LOCAL", "");
                    BASE_LOCAL                           = constanteService.getValor("FINV", "BASE_LOCAL", "");
                    CLASE_DOCUMENTO                      = constanteService.getValor("FINV", "CLASE_DOCUMENTO", "");
                    TIPO_DOCUMENTO_FAP                   = constanteService.getValor("FINV", "TIPO_DOCUMENTO_FAP", "");
                    HANDLE_CODE_CI                       = constanteService.getValor("FINV", "HANDLE_CODE_CI", "");
                    APROBADOR_AUTOMATICO                 = constanteService.getValor("FINV", "APROBADOR_AUTOMATICO", "");
                    AGENCIA_PRINCIPAL                    = constanteService.getValor("FINV", "AGENCIA_PRINCIPAL", "");
                      
                        
                        
                    }
                    

                    // Datos comunes a la cabecera y a los items


                    String id_contratista = facturaGeneral.getId_contratista();
                    String prefactura     = facturaGeneral.getPrefactura();

                    model.consorcioService.setContratista(id_contratista);
                    Contratista contratista = model.consorcioService.getContratista();

                    String proveedor = contratista.getNit();
                    String tipo_documento = TIPO_DOCUMENTO_FAP;
                    String documento      = facturaGeneral.getPrefactura();
                    String user_update = usuario;
                    String creation_user = user_update;
                    String base = BASE_LOCAL;

                    String id_accion = facturaGeneral.getId_accion();



                    // CREACION DE LA CABECERA EN CXP_DOC

                    // Datos de la cabecera


                    String descripcion    = "Factura correspondiente a prefactura : " + documento;
                    String agencia        = AGENCIA_PRINCIPAL;
                    Proveedor objeto_proveedor = proveedorService.obtenerProveedorPorNit(proveedor);

                    //String handle_code = objeto_proveedor.getC_hc();

                  String handle_code = HANDLE_CODE_CI;

                    String aprobador   = APROBADOR_AUTOMATICO;
                    String usuario_aprobacion = APROBADOR_AUTOMATICO;
                    String banco = "ABONO PRESTAMOS";
                    String sucursal = "CC";
                    String moneda = MONEDA_LOCAL;

                    double vlr_mat = facturaGeneral.getVlr_mat();
                    double vlr_mob = facturaGeneral.getVlr_mob();
                    double vlr_otr = facturaGeneral.getVlr_otr();
                    double vlr_administracion = facturaGeneral.getVlr_administracion();
                    double vlr_imprevisto     = facturaGeneral.getVlr_imprevisto();
                    double vlr_utilidad       = facturaGeneral.getVlr_utilidad();
                    double vlr_bonificacion   = -facturaGeneral.getVlr_Bonificacion();


                    double vlr_iva            = facturaGeneral.getVlr_iva();

                    double vlr_rmat           = facturaGeneral.getVlr_rmat();
                    double vlr_rmob           = facturaGeneral.getVlr_rmob();
                    double vlr_rotr           = facturaGeneral.getVlr_rotr();
                    double vlr_factoring      = facturaGeneral.getVlr_factoring();
                    double vlr_formula        = facturaGeneral.getVlr_formula();


                    double vlr_base_iva       = facturaGeneral.getVlr_base_iva();


                    double vlr_formula_provintegral=facturaGeneral.getVlr_formula_provintegral();
                    double val_ret_aiu_contratista        = facturaGeneral.getVal_ret_aiu_contratista();
                    double val_ret_ica_contratista        = facturaGeneral.getVal_ret_ica_contratista();
                    double val_ret_iva_contratista        = facturaGeneral.getVal_ret_iva_contratista();






                    double vlr_neto  = vlr_mat + vlr_mob + vlr_otr +
                                       vlr_administracion + vlr_imprevisto + vlr_utilidad +
                                       vlr_bonificacion + vlr_iva + vlr_rmat + vlr_rmob + vlr_rotr +
                                       val_ret_ica_contratista + val_ret_aiu_contratista + val_ret_iva_contratista;


                    double vlr_base_reteica = vlr_mat + vlr_mob + vlr_otr +
                                              vlr_administracion + vlr_imprevisto + vlr_utilidad + vlr_bonificacion;

                    double vlr_total_abonos = 0;
                    double vlr_saldo = vlr_neto;
                    double vlr_neto_me = vlr_neto;
                    double vlr_total_abonos_me = 0;
                    double vlr_saldo_me = vlr_neto;
                    double tasa = 1;

                    String observacion = "";
                    String clase_documento = CLASE_DOCUMENTO;
                    // String moneda_banco = objeto_proveedor.getC_currency_bank();
                    String moneda_banco = MONEDA_LOCAL;
                    String clase_documento_rel = CLASE_DOCUMENTO;
                    Date fecha_hoy = new Date();
                    DateFormat formato;
                    formato = new SimpleDateFormat("yyyy-MM-dd");
                    String fecha_documento = formato.format(fecha_hoy);
                    Date fecha_pago = fecha_hoy ;
                    String fecha_vencimiento = fecha_documento;

                    comandoSQL = model.consorcioService.setCxp_doc(dstrct, proveedor, tipo_documento, documento, descripcion,
                            agencia, handle_code, creation_date, aprobador, usuario_aprobacion, banco, sucursal,
                            moneda, vlr_neto, vlr_total_abonos, vlr_saldo, vlr_neto_me,
                            vlr_total_abonos_me, vlr_saldo_me, tasa, observacion, user_update,
                            creation_user, base, clase_documento, moneda_banco,
                            fecha_documento, fecha_vencimiento, clase_documento_rel,
                            creation_date, creation_date, SIGLA_ACCION,id_accion,SIGLA_PREFACTURA_CONTRATISTA, documento, "","","","","N");

                    comandos_sql.add(comandoSQL);


                    // CREACION DE LOS ITEMS EN CXP_ITEMS_DOC

                    // Datos comunes a todos los items

                    String auxiliar = proveedor;
                    int item = 0;

                    String ceros1="";
                    if (item==0){ ceros1="00";}else{ ceros1="";}

                    // Item de material

                    if (vlr_mat != 0) {
                        descripcion = "Material Accion : " + id_accion;
                        comandoSQL  = model.consorcioService.setCxp_items_doc(dstrct, proveedor,
                                         tipo_documento, documento, Util.llenarConCerosALaIzquierda(++item, 3 ),
                                         descripcion, vlr_mat, vlr_mat, CUENTA_MATERIAL_CONTRATISTA,
                                         user_update, creation_user, base, CONCEPTO_MATERIAL_CONTRATISTA, auxiliar,
                                         creation_date, creation_date, SIGLA_ACCION,id_accion,SIGLA_PREFACTURA_CONTRATISTA, documento, "","");
                        comandos_sql.add(comandoSQL);
                    }


                    //Mano de obra
                    if (vlr_mob != 0) {
                        descripcion = "Mano de obra Accion : " + id_accion;
                        comandoSQL  = model.consorcioService.setCxp_items_doc(dstrct, proveedor,
                                         tipo_documento, documento,Util.llenarConCerosALaIzquierda(++item, 3 ),
                                         descripcion, vlr_mob , vlr_mob , CUENTA_MANO_OBRA_CONTRATISTA,
                                         user_update, creation_user, base, CONCEPTO_MANO_OBRA_CONTRATISTA, auxiliar,
                                         creation_date, creation_date, SIGLA_ACCION,id_accion,SIGLA_PREFACTURA_CONTRATISTA, documento, "","");
                        comandos_sql.add(comandoSQL);
                    }


                    // Otros
                    if (vlr_otr != 0) {
                        descripcion = "Otros Accion : " + id_accion;
                        comandoSQL  = model.consorcioService.setCxp_items_doc(dstrct, proveedor,
                                         tipo_documento, documento,Util.llenarConCerosALaIzquierda(++item, 3 ),
                                         descripcion, vlr_otr, vlr_otr, CUENTA_OTROS_CONTRATISTA ,
                                         user_update, creation_user, base,CONCEPTO_OTROS_CONTRATISTA, auxiliar,
                                         creation_date, creation_date, SIGLA_ACCION,id_accion,SIGLA_PREFACTURA_CONTRATISTA, documento, "","");
                        comandos_sql.add(comandoSQL);
                    }





                    // Item de administracion

                    if ((vlr_administracion) != 0) {

                        descripcion = "Administracion Accion : " + id_accion;

                        comandoSQL = model.consorcioService.setCxp_items_doc(dstrct, proveedor,
                                         tipo_documento, documento,Util.llenarConCerosALaIzquierda(++item, 3 ),
                                         descripcion , vlr_administracion  ,
                                         vlr_administracion ,CUENTA_ADMINISTRACION_CONTRATISTA,
                                         user_update, creation_user, base, CONCEPTO_ADMINISTRACION_CONTRATISTA, auxiliar,
                                         creation_date, creation_date, SIGLA_ACCION,id_accion,SIGLA_PREFACTURA_CONTRATISTA, documento, "","");
                        comandos_sql.add(comandoSQL);
                    }


                    // Item imprevisto

                    if ((vlr_imprevisto) != 0) {

                        descripcion = "Imprevisto Accion : " + id_accion;

                        comandoSQL = model.consorcioService.setCxp_items_doc(dstrct, proveedor,
                                         tipo_documento, documento,Util.llenarConCerosALaIzquierda(++item, 3 ),
                                         descripcion , vlr_imprevisto ,
                                         vlr_imprevisto , CUENTA_IMPREVISTO_CONTRATISTA,
                                         user_update, creation_user, base,  CONCEPTO_IMPREVISTO_CONTRATISTA, auxiliar,
                                         creation_date, creation_date, SIGLA_ACCION,id_accion,SIGLA_PREFACTURA_CONTRATISTA, documento, "","");
                        comandos_sql.add(comandoSQL);
                    }


                    // Item de utilidad
                    if (vlr_utilidad != 0) {
                        comandoSQL = model.consorcioService.setCxp_items_doc(dstrct, proveedor,
                                         tipo_documento, documento,Util.llenarConCerosALaIzquierda(++item, 3 ),
                                         "Utilidad Accion : " + id_accion, vlr_utilidad, vlr_utilidad, CUENTA_UTILIDAD_CONTRATISTA,
                                         user_update, creation_user, base,  CONCEPTO_UTILIDAD_CONTRATISTA, auxiliar,
                                         creation_date, creation_date,  SIGLA_ACCION,id_accion,SIGLA_PREFACTURA_CONTRATISTA, documento, "","");
                        comandos_sql.add(comandoSQL);
                    }


                    // Item de bonificacion
                    if (vlr_bonificacion != 0) {
                        comandoSQL = model.consorcioService.setCxp_items_doc(dstrct, proveedor,
                                         tipo_documento, documento,Util.llenarConCerosALaIzquierda(++item, 3 ),
                                         "Bonificacion Accion : " + id_accion, vlr_bonificacion, vlr_bonificacion, CUENTA_BONIFICACION_CONTRATISTA,
                                         user_update, creation_user, base, CONCEPTO_BONIFICACION_CONTRATISTA, auxiliar,
                                         creation_date, creation_date,  SIGLA_ACCION,id_accion,SIGLA_PREFACTURA_CONTRATISTA, documento, "","");
                        comandos_sql.add(comandoSQL);
                    }



                    // Item de iva
                    descripcion = "Iva Accion : " + id_accion ;
                    comandoSQL = model.consorcioService.setCxp_items_doc(dstrct, proveedor,
                                     tipo_documento, documento,Util.llenarConCerosALaIzquierda(++item, 3 ),
                                     descripcion, vlr_iva, vlr_iva, CUENTA_IVA_CONTRATISTA,
                                     user_update, creation_user, base, CONCEPTO_IVA_CONTRATISTA, auxiliar,
                                     creation_date, creation_date,  SIGLA_ACCION,id_accion,SIGLA_PREFACTURA_CONTRATISTA, documento, "","");
                    comandos_sql.add(comandoSQL);


                    // Item de valor retencion material

                    if (vlr_rmat != 0) {
                        descripcion = "Retencion por material Accion : " + id_accion  + " Base retencion : " + Util.FormatoMiles(vlr_mat);
                        comandoSQL = model.consorcioService.setCxp_items_doc(dstrct, proveedor,
                                         tipo_documento, documento,Util.llenarConCerosALaIzquierda(++item, 3 ),
                                         descripcion, vlr_rmat, vlr_rmat, CUENTA_RET_MATERIAL_CONTRATISTA,
                                         user_update, creation_user, base, CONCEPTO_RET_MATERIAL_CONTRATISTA, auxiliar,
                                         creation_date, creation_date, SIGLA_ACCION,id_accion,SIGLA_PREFACTURA_CONTRATISTA, documento, "","");
                        comandos_sql.add(comandoSQL);
                    }

                     // Item de valor retencion mano de obra
                    if (vlr_rmob != 0) {
                        descripcion = "Retencion por mano de obra Accion : " + id_accion  + " Base retencion : " + Util.FormatoMiles(vlr_mob);
                        comandoSQL = model.consorcioService.setCxp_items_doc(dstrct, proveedor,
                                         tipo_documento, documento,Util.llenarConCerosALaIzquierda(++item, 3 ),
                                         descripcion, vlr_rmob, vlr_rmob, CUENTA_RET_MO_CONTRATISTA,
                                         user_update, creation_user, base, CONCEPTO_RET_MO_CONTRATISTA, auxiliar,
                                         creation_date, creation_date, SIGLA_ACCION,id_accion,SIGLA_PREFACTURA_CONTRATISTA, documento, "","");
                        comandos_sql.add(comandoSQL);
                    }

                    // Item de valor retencion otros
                    if (vlr_rotr != 0) {
                        descripcion = "Retencion por otros Accion : " + id_accion  + "  Base retencion : " + Util.FormatoMiles(vlr_otr);
                        comandoSQL = model.consorcioService.setCxp_items_doc(dstrct, proveedor,
                                         tipo_documento, documento, Util.llenarConCerosALaIzquierda(++item, 3 ),
                                         descripcion, vlr_rotr, vlr_rotr, CUENTA_RET_OTROS_CONTRATISTA,
                                         user_update, creation_user, base, CONCEPTO_RET_OTROS_CONTRATISTA, auxiliar,
                                         creation_date, creation_date, SIGLA_ACCION,id_accion,SIGLA_PREFACTURA_CONTRATISTA, documento, "","");
                        comandos_sql.add(comandoSQL);
                    }


                    // Item de valor retencion aiu

                    if (val_ret_aiu_contratista != 0) {
                        descripcion = "Retencion por aiu Accion : " + id_accion  + " Base retencion : " + Util.FormatoMiles(vlr_administracion + vlr_imprevisto + vlr_utilidad);
                        comandoSQL = model.consorcioService.setCxp_items_doc(dstrct, proveedor,
                                         tipo_documento, documento,Util.llenarConCerosALaIzquierda(++item, 3 ),
                                         descripcion, val_ret_aiu_contratista, val_ret_aiu_contratista, CUENTA_RET_AIU_CONTRATISTA,
                                         user_update, creation_user, base, CONCEPTO_RET_AIU_CONTRATISTA, auxiliar,
                                         creation_date, creation_date, SIGLA_ACCION,id_accion,SIGLA_PREFACTURA_CONTRATISTA, documento, "","");
                        comandos_sql.add(comandoSQL);
                    }

                    // Item de valor reteica


                    if (val_ret_ica_contratista != 0) {
                        descripcion = "Reteica Accion : " + id_accion + " Base retencion : " + Util.FormatoMiles(vlr_base_reteica);
                        comandoSQL = model.consorcioService.setCxp_items_doc(dstrct, proveedor,
                                         tipo_documento, documento,Util.llenarConCerosALaIzquierda(++item, 3 ),
                                         descripcion, val_ret_ica_contratista, val_ret_ica_contratista, CUENTA_RETEICA_CONTRATISTA,
                                         user_update, creation_user, base, CONCEPTO_RETEICA_CONTRATISTA, auxiliar,
                                         creation_date, creation_date, SIGLA_ACCION,id_accion,SIGLA_PREFACTURA_CONTRATISTA, documento, "","");
                        comandos_sql.add(comandoSQL);
                    }


                    // Item de valor reteiva

                    if (val_ret_iva_contratista != 0) {
                        descripcion = "Reteiva Accion : " + id_accion  + " Base retencion : " + Util.FormatoMiles(vlr_base_iva);;
                        comandoSQL = model.consorcioService.setCxp_items_doc(dstrct, proveedor,
                                         tipo_documento, documento,Util.llenarConCerosALaIzquierda(++item, 3 ),
                                         descripcion, val_ret_iva_contratista, val_ret_iva_contratista, CUENTA_RETEIVA_CONTRATISTA,
                                         user_update, creation_user, base, CONCEPTO_RETEIVA_CONTRATISTA, auxiliar,
                                         creation_date, creation_date, SIGLA_ACCION,id_accion,SIGLA_PREFACTURA_CONTRATISTA, documento, "","");
                        comandos_sql.add(comandoSQL);
                    }




                    // ACTUALIZA LAS ACCIONES CON EL NUMERO DE FACTURA Y FECHA DE FACTURA DEL CONTRATISTA

                    comandoSQL = model.consorcioService.setFacturaContratista(documento, id_accion );
                    comandos_sql.add(comandoSQL);



                    estado = model.consorcioService.ejecutarBatchSQL(comandos_sql, logWriter);

                    if (estado.equalsIgnoreCase("ERROR")){
                        logWriter.log("ERROR: Se genero error al ejecutar el Batch de SQL. "+
                                      "Se efectuo rollback. \n" +
                                      "                                            No se proceso accion: " + id_accion + "\n" +
                                      "                                            Prefactura          : " + prefactura + "\n" ,LogWriter.INFO);
                    }
                    else{
                        // logWriter.log("    Registra en base de datos sin error \n",LogWriter.INFO);
                        totalGrabados++;
                    }


                    // Inicializa los vectores de comandos para una proxima solicitud

                    comandos_sql.clear();

                }

                logWriter.log("Total facturas grabadas : " + totalGrabados + " \n",LogWriter.INFO);
                logWriter.tituloFinal();
            }
            else {
                logWriter.log("ADVERTENCIA: No existen solicitudes por facturar. \n",LogWriter.INFO);
            }

            //model.LogProcesosSvc.finallyProceso(this.processName, this.hashCode(), usuario.getLogin(), "PROCESO EXITOSO");
        }catch (Exception ex){
            Util.imprimirTrace(ex);
            try{
                //model.LogProcesosSvc.finallyProceso(this.processName, this.hashCode(), usuario.getLogin(), "Finalizado con error ...\n" + ex.getMessage());
            }catch (Exception e){
                System.out.println("Error HFacturaContratista ...\n" + e.getMessage());
            }
        }





    }






public void generarFacturaInternaProvintegral(String usuario,  ModelOpav model, LogWriter logWriter) {



        try{

            logWriter.tituloInicial("GENERACION DE FACTURAS INTERNAS PROVINTEGRAL");

            // Definicion de las cuentas para cada concepto

            String  CUENTA_COMISION_PROVINTEGRAL       = constanteService.getValor("FINV", "CUENTA_COMISION_PROVINTEGRAL", "");


            String  CONCEPTO_COMISION_PROVINTEGRAL     = constanteService.getValor("FINV", "CONCEPTO_COMISION_PROVINTEGRAL", "");
            String  CONCEPTO_IVA_FACTURA_PROVINTEGRAL  = constanteService.getValor("FINV", "CONCEPTO_IVA_FACTURA_PROVINTEGRAL", "");
            //String  CONCEPTO_RETENCION_PROVINTEGRAL    = constanteService.getValor("FINV", "CONCEPTO_RETENCION_PROVINTEGRAL", "");
            //String  CONCEPTO_RETEICA_PROVINTEGRAL      = constanteService.getValor("FINV", "CONCEPTO_RETEICA_PROVINTEGRAL", "");
            //String  CONCEPTO_RETEIVA_PROVINTEGRAL      = constanteService.getValor("FINV", "CONCEPTO_RETEIVA_PROVINTEGRAL", "");

            String  CODIGO_IVA_PROVINTEGRAL            = constanteService.getValor("FINV", "CODIGO_IVA_PROVINTEGRAL", "");
            //String  CODIGO_RETENCION_PROVINTEGRAL      = constanteService.getValor("FINV", "CODIGO_RETENCION_PROVINTEGRAL", "");
            //String  CODIGO_RETEICA_PROVINTEGRAL        = constanteService.getValor("FINV", "CODIGO_RETEICA_PROVINTEGRAL", "");
            //String  CODIGO_RETEIVA_PROVINTEGRAL        = constanteService.getValor("FINV", "CODIGO_RETEIVA_PROVINTEGRAL", "");

            String  CUENTA_IVA_PROVINTEGRAL            = constanteService.getValor("FINV", "CUENTA_IVA_PROVINTEGRAL", "");
            //String  CUENTA_RETENCION_PROVINTEGRAL      = constanteService.getValor("FINV", "CUENTA_RETENCION_PROVINTEGRAL", "");
            //String  CUENTA_RETEICA_PROVINTEGRAL        = constanteService.getValor("FINV", "CUENTA_RETEICA_PROVINTEGRAL", "");
            //String  CUENTA_RETEIVA_PROVINTEGRAL        = constanteService.getValor("FINV", "CUENTA_RETEIVA_PROVINTEGRAL", "");






            String SIGLA_ACCION                         = constanteService.getValor("FINV", "SIGLA_ACCION", "");
            String MONEDA_LOCAL                         = constanteService.getValor("FINV", "MONEDA_LOCAL", "");
            String BASE_LOCAL                           = constanteService.getValor("FINV", "BASE_LOCAL", "");
            String CLASE_DOCUMENTO                      = constanteService.getValor("FINV", "CLASE_DOCUMENTO", "");
            String TIPO_DOCUMENTO_FAP                   = constanteService.getValor("FINV", "TIPO_DOCUMENTO_FAP", "");
            String HANDLE_CODE_PI                       = constanteService.getValor("FINV", "HANDLE_CODE_PI", "");
            String APROBADOR_AUTOMATICO                 = constanteService.getValor("FINV", "APROBADOR_AUTOMATICO", "");
            String AGENCIA_PRINCIPAL                    = constanteService.getValor("FINV", "AGENCIA_PRINCIPAL", "");

            String dstrct = "FINV";



            int totalGrabados = 0;
            String estado = "";
            Vector comandos_sql =new Vector();
            java.util.Date fechaActual = new Date();
            String creation_date = fechaActual.toString();


            DateFormat formato_fecha;
            formato_fecha = new SimpleDateFormat("yyyy");
            String fechaVigencia = formato_fecha.format(fechaActual)+ "-12-31";



            List listaAcciones =  model.consorcioService.getAccionesProvintegral("FINV",logWriter);


            //Tipo_impuesto impuestoIva = getTipoImpuesto( dstrct,  CODIGO_IVA_PROVINTEGRAL,  fechaVigencia,  CODIGO_RETEIVA_PROVINTEGRAL,  "OP" );
            //Tipo_impuesto impuestoRetencion = getTipoImpuesto( dstrct,  CODIGO_RETENCION_PROVINTEGRAL,  fechaVigencia,  "",  "OP" );
            //Tipo_impuesto impuestoRica = getTipoImpuesto( dstrct,  CODIGO_RETEICA_PROVINTEGRAL,  fechaVigencia,  "",  "BQ" );
            //Tipo_impuesto impuestoRiva = getTipoImpuesto( dstrct,  CODIGO_RETEIVA_PROVINTEGRAL,  fechaVigencia,  "",  "OP" );

            String observacionImpuesto = "";
            /*
            if(impuestoIva == null) {
                observacionImpuesto += "El impuesto de Iva : " + CODIGO_IVA_PROVINTEGRAL + " no pudo ser leido en la Tabla de Impuesto \n" ;
            }
            else if(impuestoIva.getCod_cuenta_contable().isEmpty() ) {
                observacionImpuesto += "El impuesto de Iva : " + CODIGO_IVA_PROVINTEGRAL + " no tiene un codigo de cuenta contable \n" ;
            }


            if(impuestoRetencion == null) {
                observacionImpuesto += "El impuesto de Retencion : " + CODIGO_RETENCION_PROVINTEGRAL + " no pudo ser leido en la Tabla de Impuesto \n" ;
            }
            else if(impuestoRetencion.getCod_cuenta_contable().isEmpty() ) {
                observacionImpuesto += "El impuesto de Retencion : " + CODIGO_RETENCION_PROVINTEGRAL + " no tiene un codigo de cuenta contable \n" ;
            }


            if(impuestoRica == null) {
                observacionImpuesto += "El impuesto de Rica : " + CODIGO_RETEICA_PROVINTEGRAL + " no pudo ser leido en la Tabla de Impuesto \n" ;
            }
            else if(impuestoRica.getCod_cuenta_contable().isEmpty() ) {
                observacionImpuesto += "El impuesto de Rica : " + CODIGO_RETEICA_PROVINTEGRAL + " no tiene un codigo de cuenta contable \n" ;
            }


            if(impuestoRiva == null) {
                observacionImpuesto += "El impuesto de Riva : " + CODIGO_RETEIVA_PROVINTEGRAL + " no pudo ser leido en la Tabla de Impuesto \n" ;
            }
            else if(impuestoRiva.getCod_cuenta_contable().isEmpty() ) {
                observacionImpuesto += "El impuesto de Riva : " + CODIGO_RETEIVA_PROVINTEGRAL + " no tiene un codigo de cuenta contable \n" ;
            }
            */

            if(observacionImpuesto.isEmpty()) {

                if (!listaAcciones.isEmpty() ){


                    int totalAcciones = listaAcciones.size();

                    logWriter.log("LISTA DE ACCIONES DE PROVINTEGRAL  CREADA \n",LogWriter.INFO);
                    logWriter.log("Total acciones leidas : " + totalAcciones + " \n",LogWriter.INFO);


                    AccionProvintegral accionProvintegral = new AccionProvintegral();
                    Iterator it = listaAcciones.iterator();

                    String comandoSQL = "";

                    while (it.hasNext()  ) {

                        accionProvintegral = (AccionProvintegral)it.next();

                        // Datos comunes a la cabecera y a los items


                        String proveedor_original = accionProvintegral.getNit();
                        String proveedor = "9000742958";

                        String tipo_documento = TIPO_DOCUMENTO_FAP;
                        String documento      = accionProvintegral.getId_accion();
                        String user_update = usuario;
                        String creation_user = user_update;
                        String base = BASE_LOCAL;

                        String id_accion = accionProvintegral.getId_accion();



                        // CREACION DE LA CABECERA EN CXP_DOC

                        // Datos de la cabecera


                        String descripcion    = "Factura interna Provintegral correspondiente a accion : " + accionProvintegral.getId_accion() +
                                                 "  del Proveedor : " + proveedor_original;
                        String agencia        = AGENCIA_PRINCIPAL;


                        String handle_code = HANDLE_CODE_PI;

                        String aprobador   = APROBADOR_AUTOMATICO;
                        String usuario_aprobacion = APROBADOR_AUTOMATICO;
                        String banco = "ABONO PRESTAMOS";
                        String sucursal = "CC";
                        String moneda = MONEDA_LOCAL;

                        double vlr_comision = accionProvintegral.getVlr_provintegral();
                        double vlr_iva = accionProvintegral.getIva_provintegral();



                        // Calculo de los valores de los items para poder sumar o restar alvalor neto

                        double vlrRetefuente = 0.00;
                        double vlrRica = 0.00;
                        double vlrRiva = 0.00;



                        //double vlrRetefuente = -Util.redondear2(vlr_comision * impuestoRetencion.getPorcentaje1()/100  , 0) ;
                        //double vlrRica = -Util.redondear2(vlr_comision * impuestoRica.getPorcentaje1()/100  , 0) ;
                        //double vlrRiva = -Util.redondear2(vlr_comision * impuestoRiva.getPorcentaje1()/100  , 0) ;


                        double vlr_neto = vlr_comision + vlr_iva + vlrRetefuente + vlrRica + vlrRiva;


                        double vlr_total_abonos = 0;
                        double vlr_saldo = vlr_neto;
                        double vlr_neto_me = vlr_neto;
                        double vlr_total_abonos_me = 0;
                        double vlr_saldo_me = vlr_neto;
                        double tasa = 1;

                        String observacion = "Solicitud : " + accionProvintegral.getId_solicitud() + " Facturada en : " + accionProvintegral.getFactura_cliente();


                        String clase_documento = CLASE_DOCUMENTO;

                        String moneda_banco = MONEDA_LOCAL;
                        String clase_documento_rel = CLASE_DOCUMENTO;


                        String fecha_documento = accionProvintegral.getFecha_factura();

                        String fecha_vencimiento = fecha_documento;

                        comandoSQL = model.consorcioService.setCxp_doc(dstrct, proveedor, tipo_documento, documento, descripcion,
                                agencia, handle_code, creation_date,  aprobador, usuario_aprobacion, banco, sucursal,
                                moneda, vlr_neto, vlr_total_abonos, vlr_saldo, vlr_neto_me,
                                vlr_total_abonos_me, vlr_saldo_me, tasa, observacion, user_update,
                                creation_user, base, clase_documento, moneda_banco,
                                fecha_documento, fecha_vencimiento, clase_documento_rel,
                                creation_date, creation_date, SIGLA_ACCION,id_accion,"", "", "","","","","N");

                        comandos_sql.add(comandoSQL);


                        // CREACION DE LOS ITEMS EN CXP_ITEMS_DOC

                        // Datos comunes a todos los items

                        String auxiliar = proveedor;
                        int item = 0;


                        // Item de comision

                        if (vlr_comision != 0) {
                            descripcion = "Valor comision provintegral Accion : " +id_accion;
                            comandoSQL  = model.consorcioService.setCxp_items_doc(dstrct, proveedor,
                                             tipo_documento, documento, Util.llenarConCerosALaIzquierda(++item, 3 ),
                                             descripcion, vlr_comision, vlr_comision, CUENTA_COMISION_PROVINTEGRAL,
                                             user_update, creation_user, base, CONCEPTO_COMISION_PROVINTEGRAL, auxiliar,
                                             creation_date, creation_date, SIGLA_ACCION,id_accion,"", "", "","");
                            comandos_sql.add(comandoSQL);
                        }


                        //Iva de la comision
                        if (vlr_iva != 0) {
                            descripcion = "Valor iva comision Provintegral Accion : " + id_accion + " Valor Base : " + vlr_comision;
                            comandoSQL  = model.consorcioService.setCxp_items_doc(dstrct, proveedor,
                                             tipo_documento, documento,Util.llenarConCerosALaIzquierda(++item, 3 ),
                                             descripcion, vlr_iva , vlr_iva , CUENTA_IVA_PROVINTEGRAL,
                                             user_update, creation_user, base, CONCEPTO_IVA_FACTURA_PROVINTEGRAL, auxiliar,
                                             creation_date, creation_date, SIGLA_ACCION,id_accion,"BAS", ""+vlr_comision, "","");
                            comandos_sql.add(comandoSQL);
                        }


                        /*
                        //Retefuente

                        if (vlrRetefuente != 0) {
                            descripcion = "Valor retefuente comision Provintegral Accion : " + id_accion + " Valor Base : " + vlr_comision;
                            comandoSQL  = model.consorcioService.setCxp_items_doc(dstrct, proveedor,
                                             tipo_documento, documento,Util.llenarConCerosALaIzquierda(++item, 3 ),
                                             descripcion, vlrRetefuente , vlrRetefuente , CUENTA_RETENCION_PROVINTEGRAL,
                                             user_update, creation_user, base, CONCEPTO_RETENCION_PROVINTEGRAL, auxiliar,
                                             creation_date, creation_date, SIGLA_ACCION,id_accion,"BAS", ""+vlr_comision, "","");
                            comandos_sql.add(comandoSQL);
                        }

                        //Rica

                        if (vlrRica != 0) {
                            descripcion = "Valor rica comision Provintegral Accion : " + id_accion + " Valor Base : " + vlr_comision;
                            comandoSQL  = model.consorcioService.setCxp_items_doc(dstrct, proveedor,
                                             tipo_documento, documento,Util.llenarConCerosALaIzquierda(++item, 3 ),
                                             descripcion, vlrRica , vlrRica ,CUENTA_RETEICA_PROVINTEGRAL,
                                             user_update, creation_user, base, CONCEPTO_RETEICA_PROVINTEGRAL, auxiliar,
                                             creation_date, creation_date, SIGLA_ACCION,id_accion,"BAS", ""+vlr_comision, "","");
                            comandos_sql.add(comandoSQL);
                        }

                        //Riva

                        if (vlrRiva != 0) {
                            descripcion = "Valor riva comision Provintegral Accion : " + id_accion + " Valor Base : " + vlr_comision;
                            comandoSQL  = model.consorcioService.setCxp_items_doc(dstrct, proveedor,
                                             tipo_documento, documento,Util.llenarConCerosALaIzquierda(++item, 3 ),
                                             descripcion, vlrRiva , vlrRiva ,CUENTA_RETEIVA_PROVINTEGRAL,
                                             user_update, creation_user, base, CONCEPTO_RETEIVA_PROVINTEGRAL, auxiliar,
                                             creation_date, creation_date, SIGLA_ACCION,id_accion,"BAS", ""+vlr_comision, "","");
                            comandos_sql.add(comandoSQL);
                        }
                        */



                        // ACTUALIZA LAS ACCIONES CON EL NUMERO DE FACTURA Y FECHA DE FACTURA DE PROVINTEGRAL

                        comandoSQL = model.consorcioService.setFacturaProvintegral( id_accion );
                        comandos_sql.add(comandoSQL);



                        estado = model.consorcioService.ejecutarBatchSQL(comandos_sql, logWriter);

                        if (estado.equalsIgnoreCase("ERROR")){
                            logWriter.log("ERROR: Se genero error al ejecutar el Batch de SQL. "+
                                          "Se efectuo rollback. \n" +
                                          "                                            No se proceso accion: " + id_accion + "\n" ,LogWriter.INFO);
                        }
                        else{
                            // logWriter.log("    Registra en base de datos sin error \n",LogWriter.INFO);
                            totalGrabados++;
                        }


                        // Inicializa los vectores de comandos para una proxima solicitud

                        comandos_sql.clear();

                    }

                    logWriter.log("Total facturas grabadas : " + totalGrabados + " \n",LogWriter.INFO);
                    logWriter.tituloFinal();
                }
                else {
                    logWriter.log("ADVERTENCIA: No existen acciones para realizarles la factura interna a Provintegral. \n",LogWriter.INFO);
                }

            }
            else {
                logWriter.log("ADVERTENCIA: No se procesaran las facturas internas de Provintegral debido a:  \n",LogWriter.INFO);
                logWriter.log("             " + observacionImpuesto +"\n",LogWriter.INFO);
            }

            //model.LogProcesosSvc.finallyProceso(this.processName, this.hashCode(), usuario.getLogin(), "PROCESO EXITOSO");
        }catch (Exception ex){
            Util.imprimirTrace(ex);
            try{
                //model.LogProcesosSvc.finallyProceso(this.processName, this.hashCode(), usuario.getLogin(), "Finalizado con error ...\n" + ex.getMessage());
            }catch (Exception e){
                System.out.println("Error HFacturaContratista ...\n" + e.getMessage());
            }
        }

    }





    public void generaNcInternaProvintegral(String usuario,  ModelOpav model, LogWriter logWriter) {


        String registro = "";

        int totalGrabados = 0;
        String estado = "";
        Vector comandos_sql =new Vector();

        java.util.Date fechaActual = new Date();
        String creation_date = fechaActual.toString();


        DateFormat formato_fecha;
        formato_fecha = new SimpleDateFormat("yyyy-MM-dd");
        String fechaDocumento = formato_fecha.format(fechaActual);

        try{


            String BANCO_POR_DEFECTO                    = constanteService.getValor("FINV", "BANCO_POR_DEFECTO", "");
            String SUCURSAL_POR_DEFECTO                 = constanteService.getValor("FINV", "SUCURSAL_POR_DEFECTO", "");
            String MONEDA_LOCAL                         = constanteService.getValor("FINV", "MONEDA_LOCAL", "");
            String BASE_LOCAL                           = constanteService.getValor("FINV", "BASE_LOCAL", "");
            String CLASE_DOCUMENTO                      = constanteService.getValor("FINV", "CLASE_DOCUMENTO", "");
            String TIPO_DOCUMENTO_FAP                   = constanteService.getValor("FINV", "TIPO_DOCUMENTO_FAP", "");
            String HANDLE_CODE_PI                       = constanteService.getValor("FINV", "HANDLE_CODE_PI", "");
            String APROBADOR_AUTOMATICO                 = constanteService.getValor("FINV", "APROBADOR_AUTOMATICO", "");
            String AGENCIA_PRINCIPAL                    = constanteService.getValor("FINV", "AGENCIA_PRINCIPAL", "");
            String TIPO_DOCUMENTO_NC_CXP                = constanteService.getValor("FINV", "TIPO_DOCUMENTO_NC_CXP", "");
           String CUENTA_ITEM_NC_PROVINTEGRAL   = constanteService.getValor("FINV", "CUENTA_ITEM_NC_PROVINTEGRAL", "");


            logWriter.tituloInicial("GENERACION DE NOTAS CREDITO PARA FACTURAS INTERNAS PROVINTEGRAL");

            // Busca las diferentes facturas conformadas que existan sin que tengan NC
            List listaFacturaProvintegral =  consorcioDao.getFacturaProvintegral( logWriter);

            if (!listaFacturaProvintegral.isEmpty()){


                // Hay facturas
                String comandoSQL = "";
                String documento = "";
                String proveedor = "";
                String condicion = "";
                List listaFacturaDetalleCxP = null;

                FacturaCabeceraCxP facturaCabeceraCxP =  new FacturaCabeceraCxP();


                FacturaDetalleCxP facturaDetalleCxP = null;


                Iterator it = listaFacturaProvintegral.iterator();
                while (it.hasNext()  ) {

                    // Recorre la lista de las facturas conformadas de provintegral

                    String facturaConformada = (String)it.next();



                    // Busca las diferentes facturas internas de provintegral de una factura conformada
                    List listaFacturaInternaProvintegral =  consorcioDao.getFacturaInternaProvintegral( facturaConformada,logWriter);

                    if (!listaFacturaInternaProvintegral.isEmpty()){




                        Iterator it2 = listaFacturaInternaProvintegral.iterator();
                        while (it2.hasNext()  ) {

                            // Recorre las facturas internas de una factura conformada de provintegral

                            registro = (String)it2.next();

                            String[] campo = registro.split("%");
                            documento  = campo[0];       // Numero de la factura interna = id_accion
                            proveedor  = campo[1];

                            int item = 0;
                            double valorTotal = 0.00;
                            String observacion = "";


                            String nit = "";

                            observacion += documento + " ";

                            condicion =  " dstrct = '" + "FINV" + "' and ";
                            condicion +=  "proveedor = '" + proveedor + "' and ";
                            condicion +=  "tipo_documento = '" + "FAP" + "' and ";
                            condicion +=  "documento = '" + documento + "' and ";
                            condicion +=  "reg_status != 'A' ";

                            // Busca el detalle de cada factura interna
                            listaFacturaDetalleCxP =  consorcioDao.getFacturaCxPDetalle(condicion);

                            Iterator it3 = listaFacturaDetalleCxP.iterator();
                            while (it3.hasNext()  ) {

                                // Recorre el detalle de cada facturas interna de una factura conformada de provintegral

                                facturaDetalleCxP = (FacturaDetalleCxP)it3.next();

                                nit = facturaDetalleCxP.getProveedor();
                                String documentoInicial = facturaDetalleCxP.getDocumento();

                                facturaDetalleCxP.setProveedor("9000742958") ;
                                facturaDetalleCxP.setDocumento(documento) ;
                                facturaDetalleCxP.setTipo_documento(TIPO_DOCUMENTO_NC_CXP);
                                facturaDetalleCxP.setItem( Util.llenarConCerosALaIzquierda(++item, 4 ) );
                                facturaDetalleCxP.setTipo_referencia_1(TIPO_DOCUMENTO_FAP) ;
                                facturaDetalleCxP.setReferencia_1(documentoInicial);
                                facturaDetalleCxP.setTipo_referencia_3("NIT") ;
                                facturaDetalleCxP.setReferencia_3(nit);

                                valorTotal += facturaDetalleCxP.getVlr();

                                facturaDetalleCxP.setDescripcion("Cancela factura interna provintegral: " +
                                                                  documentoInicial + "Concepto: " +
                                                                  facturaDetalleCxP.getDescripcion());
                                facturaDetalleCxP.setTipo_documento("NC");
                                facturaDetalleCxP.setCodigo_cuenta(CUENTA_ITEM_NC_PROVINTEGRAL);

                                comandoSQL = model.consorcioService.setFacturaDetalleCxP(facturaDetalleCxP, usuario, creation_date);
                                comandos_sql.add(comandoSQL);

                            } // Fin del while que recorre los items de una factura interna

                            // Actualizar todas las acciones con el mismo numero de la nota credito factura conformada

                            comandoSQL = model.consorcioService.setNcProvintegral(documento,  logWriter);
                            comandos_sql.add(comandoSQL);



                            // Armar la cabecera

                            facturaCabeceraCxP.clear();

                            facturaCabeceraCxP.setDstrct("FINV");
                            facturaCabeceraCxP.setProveedor(facturaDetalleCxP.getProveedor() ) ;
                            facturaCabeceraCxP.setTipo_documento(facturaDetalleCxP.getTipo_documento()) ;
                            facturaCabeceraCxP.setDocumento(documento) ;
                            facturaCabeceraCxP.setDescripcion("Cancela factura interna " + documento + " de provintegral") ;

                            facturaCabeceraCxP.setTipo_documento_rel("FAP");
                            facturaCabeceraCxP.setDocumento_relacionado(documento);
                            facturaCabeceraCxP.setTipo_referencia_1("FAP");
                            facturaCabeceraCxP.setReferencia_1(documento);


                            facturaCabeceraCxP.setAgencia(AGENCIA_PRINCIPAL) ;
                            facturaCabeceraCxP.setHandle_code(HANDLE_CODE_PI) ;

                            facturaCabeceraCxP.setFecha_aprobacion (creation_date) ;
                            facturaCabeceraCxP.setAprobador(APROBADOR_AUTOMATICO) ;
                            facturaCabeceraCxP.setUsuario_aprobacion(APROBADOR_AUTOMATICO) ;
                            facturaCabeceraCxP.setBanco(BANCO_POR_DEFECTO) ;
                            facturaCabeceraCxP.setSucursal(SUCURSAL_POR_DEFECTO) ;
                            facturaCabeceraCxP.setMoneda(MONEDA_LOCAL) ;
                            facturaCabeceraCxP.setVlr_neto(valorTotal) ;
                            facturaCabeceraCxP.setVlr_saldo(valorTotal) ;
                            facturaCabeceraCxP.setVlr_neto_me(valorTotal) ;
                            facturaCabeceraCxP.setVlr_saldo_me(valorTotal) ;
                            facturaCabeceraCxP.setTasa(1.00) ;
                            facturaCabeceraCxP.setObservacion("Facturas internas : " + observacion) ;
                            facturaCabeceraCxP.setLast_update(creation_date) ;
                            facturaCabeceraCxP.setUser_update(usuario) ;
                            facturaCabeceraCxP.setCreation_date(creation_date) ;
                            facturaCabeceraCxP.setCreation_user(usuario) ;
                            facturaCabeceraCxP.setBase(BASE_LOCAL) ;
                            facturaCabeceraCxP.setClase_documento(CLASE_DOCUMENTO) ;
                            facturaCabeceraCxP.setMoneda_banco(MONEDA_LOCAL) ;
                            facturaCabeceraCxP.setFecha_documento(fechaDocumento) ;
                            facturaCabeceraCxP.setFecha_vencimiento(fechaDocumento)  ;

                            comandoSQL = model.consorcioService.setFacturaCabeceraCxP(facturaCabeceraCxP, usuario, creation_date);
                            comandos_sql.add(comandoSQL);


                            estado = model.consorcioService.ejecutarBatchSQL(comandos_sql, logWriter);

                            if (estado.equalsIgnoreCase("ERROR")){
                                logWriter.log("ERROR: Se genero error al ejecutar el Batch de SQL. "+
                                              "Se efectuo rollback. \n" +
                                              "                                            No se proceso factura conformada de provintegral : " + facturaConformada + "\n" ,LogWriter.INFO);
                            }
                            else{
                                // logWriter.log("    Registra en base de datos sin error \n",LogWriter.INFO);
                                totalGrabados++;
                            }


                            // Inicializa los vectores de comandos para una proxima solicitud

                            comandos_sql.clear();






                        } // Fin while que recorre las facturas internas que tienen la misma factura conformada de provintegral



                    }

                    else {

                        // No existen facturas internas para una factura conformada de Provintegral
                    }

                }  // Final while que recorre la lista de facturas conformadas


                logWriter.log("Total notas credito grabadas : " + totalGrabados + " \n",LogWriter.INFO);
                logWriter.tituloFinal();

            }
            else {

                // No existen facturas conformadas para Provintegral
                logWriter.log("ADVERTENCIA: No existen acciones marcadas con un numero de factura conformada de provintegral para realizarles la nota credito interna. \n",LogWriter.INFO);

            }

        }catch (Exception ex){
            logWriter.log(ex.getMessage());
            try{
                //model.LogProcesosSvc.finallyProceso(this.processName, this.hashCode(), usuario.getLogin(), "Finalizado con error ...\n" + ex.getMessage());
            }catch (Exception e){
                logWriter.log("Error en generaNcInternaProvintegral ...\n");
                logWriter.log(e.getMessage());
            }
        }

    }




    public void generaCxpProvintegral(String usuario, ModelOpav  model, LogWriter logWriter) {


        String registro = "";

        int totalGrabados = 0;
        String estado = "";
        Vector comandos_sql =new Vector();

        java.util.Date fechaActual = new Date();
        String creation_date = fechaActual.toString();


        DateFormat formato_fecha;
        formato_fecha = new SimpleDateFormat("yyyy-MM-dd");
        String fechaDocumento = formato_fecha.format(fechaActual);

        try{


            String BANCO_POR_DEFECTO                    = constanteService.getValor("FINV", "BANCO_POR_DEFECTO", "");
            String SUCURSAL_POR_DEFECTO                 = constanteService.getValor("FINV", "SUCURSAL_POR_DEFECTO", "");
            String MONEDA_LOCAL                         = constanteService.getValor("FINV", "MONEDA_LOCAL", "");
            String BASE_LOCAL                           = constanteService.getValor("FINV", "BASE_LOCAL", "");
            String HANDLE_CODE_PF                       = constanteService.getValor("FINV", "HANDLE_CODE_PF", "");
            String APROBADOR_AUTOMATICO                 = constanteService.getValor("FINV", "APROBADOR_AUTOMATICO", "");
            String AGENCIA_PRINCIPAL                    = constanteService.getValor("FINV", "AGENCIA_PRINCIPAL", "");
            String CUENTA_ITEM_FACTURA_CXP_FINAL_PROVINTEGRAL = constanteService.getValor("FINV", "CUENTA_FACTURA_CXP_FINAL_PROVINTEGRAL", "");


            logWriter.tituloInicial("GENERACION DE FACTURA CONFORMADA DE PROVINTEGRAL");

            // Busca las diferentes facturas conformadas que existan  que tengan NC sin fecha de factura conformada
            List listaFacturaProvintegral =  consorcioDao.getFacturaProvintegralConformada( );

            if (!listaFacturaProvintegral.isEmpty()){


                // Hay facturas
                String comandoSQL = "";
                String documento = "";
                String proveedor = "";
                String condicion = "";
                List listaFacturaDetalleCxP = null;

                FacturaCabeceraCxP facturaCabeceraCxP =  new FacturaCabeceraCxP();


                FacturaDetalleCxP facturaDetalleCxP = null;


                Iterator it = listaFacturaProvintegral.iterator();
                while (it.hasNext()  ) {

                    // Recorre la lista de las facturas conformadas de provintegral

                    String facturaConformada = (String)it.next();

                    int item = 0;
                    double valorTotal = 0.00;
                    String observacion = "";




                    // Busca las diferentes facturas internas de provintegral de una factura conformada
                    List listaFacturaInternaProvintegral =  consorcioDao.getFacturaInternaProvintegral( facturaConformada,logWriter);

                    if (!listaFacturaInternaProvintegral.isEmpty()){




                        Iterator it2 = listaFacturaInternaProvintegral.iterator();
                        while (it2.hasNext()  ) {

                            // Recorre las facturas internas de una factura conformada de provintegral

                            registro = (String)it2.next();

                            String[] campo = registro.split("%");
                            documento  = campo[0];       // Numero de la factura interna = id_accion
                            proveedor  = campo[1];


                            String nit = "";

                            observacion += documento + " ";

                            condicion =  " dstrct = '" + "FINV" + "' and ";
                            condicion +=  "proveedor = '" + proveedor + "' and ";
                            condicion +=  "tipo_documento = '" + "FAP" + "' and ";
                            condicion +=  "documento = '" + documento + "' and ";
                            condicion +=  "reg_status != 'A' ";

                            // Busca el detalle de cada factura interna
                            listaFacturaDetalleCxP =  consorcioDao.getFacturaCxPDetalle(condicion);

                            Iterator it3 = listaFacturaDetalleCxP.iterator();
                            while (it3.hasNext()  ) {

                                // Recorre el detalle de cada facturas interna de una factura conformada de provintegral

                                facturaDetalleCxP = (FacturaDetalleCxP)it3.next();

                                nit = facturaDetalleCxP.getProveedor();
                                String documentoInicial = facturaDetalleCxP.getDocumento();

                                facturaDetalleCxP.setProveedor("9000742958") ;
                                facturaDetalleCxP.setDocumento(facturaConformada) ;
                                facturaDetalleCxP.setTipo_documento("FAP");
                                facturaDetalleCxP.setItem( Util.llenarConCerosALaIzquierda(++item, 4 ) );

                                /*if (facturaDetalleCxP.getConcepto().equals("240")) {
                                    facturaDetalleCxP.setCodigo_cuenta("28151002");
                                }

                                else if (facturaDetalleCxP.getConcepto().equals("241")) {
                                    facturaDetalleCxP.setCodigo_cuenta("28151002");
                                }
                                else if (facturaDetalleCxP.getConcepto().equals("242")) {
                                    facturaDetalleCxP.setCodigo_cuenta("26150501");
                                }
                                else if (facturaDetalleCxP.getConcepto().equals("243")) {
                                    facturaDetalleCxP.setCodigo_cuenta("26150502");
                                }
                                else if (facturaDetalleCxP.getConcepto().equals("244")) {
                                    facturaDetalleCxP.setCodigo_cuenta("26150503");
                                }
                                else {
                                    facturaDetalleCxP.setCodigo_cuenta("");
                                }*/

                                facturaDetalleCxP.setCodigo_cuenta(CUENTA_ITEM_FACTURA_CXP_FINAL_PROVINTEGRAL);


                                valorTotal += facturaDetalleCxP.getVlr();

                                facturaDetalleCxP.setDescripcion("Facturas interna de provintegral " + facturaDetalleCxP.getReferencia_1());

                                facturaDetalleCxP.setTipo_documento("FAP");

                                comandoSQL = model.consorcioService.setFacturaDetalleCxP(facturaDetalleCxP, usuario, creation_date);
                                comandos_sql.add(comandoSQL);

                            } // Fin del while que recorre los items de una factura interna

                            // Actualizar todas las acciones con el mismo numero de la nota credito factura conformada

                            comandoSQL = consorcioDao.setFechaFacturaConformadaProvintegral(documento, fechaDocumento,  logWriter);
                            comandos_sql.add(comandoSQL);



                        } // Fin while que recorre las facturas internas que tienen la misma factura conformada de provintegral

                        // Armar la cabecera

                        facturaCabeceraCxP.clear();

                        facturaCabeceraCxP.setDstrct("FINV");
                        facturaCabeceraCxP.setProveedor(facturaDetalleCxP.getProveedor() ) ;
                        facturaCabeceraCxP.setTipo_documento(facturaDetalleCxP.getTipo_documento()) ;
                        facturaCabeceraCxP.setDocumento(facturaConformada) ;
                        facturaCabeceraCxP.setDescripcion("Factura interna " + documento + " de provintegral") ;


                        facturaCabeceraCxP.setTipo_referencia_1("FAP");
                        facturaCabeceraCxP.setReferencia_1(documento);


                        facturaCabeceraCxP.setAgencia(AGENCIA_PRINCIPAL) ;
                        facturaCabeceraCxP.setHandle_code(HANDLE_CODE_PF) ;

                        facturaCabeceraCxP.setFecha_aprobacion (creation_date) ;
                        facturaCabeceraCxP.setAprobador(APROBADOR_AUTOMATICO) ;
                        facturaCabeceraCxP.setUsuario_aprobacion(APROBADOR_AUTOMATICO) ;
                        facturaCabeceraCxP.setBanco(BANCO_POR_DEFECTO) ;
                        facturaCabeceraCxP.setSucursal(SUCURSAL_POR_DEFECTO) ;
                        facturaCabeceraCxP.setMoneda(MONEDA_LOCAL) ;
                        facturaCabeceraCxP.setVlr_neto(valorTotal) ;
                        facturaCabeceraCxP.setVlr_saldo(valorTotal) ;
                        facturaCabeceraCxP.setVlr_neto_me(valorTotal) ;
                        facturaCabeceraCxP.setVlr_saldo_me(valorTotal) ;
                        facturaCabeceraCxP.setTasa(1.00) ;
                        facturaCabeceraCxP.setObservacion("Facturas internas : " + observacion) ;
                        facturaCabeceraCxP.setLast_update(creation_date) ;
                        facturaCabeceraCxP.setUser_update(usuario) ;
                        facturaCabeceraCxP.setCreation_date(creation_date) ;
                        facturaCabeceraCxP.setCreation_user(usuario) ;
                        facturaCabeceraCxP.setBase(BASE_LOCAL) ;
                        facturaCabeceraCxP.setClase_documento("4") ;
                        facturaCabeceraCxP.setMoneda_banco(MONEDA_LOCAL) ;
                        facturaCabeceraCxP.setFecha_documento(fechaDocumento) ;
                        facturaCabeceraCxP.setFecha_vencimiento(fechaDocumento)  ;

                        comandoSQL = model.consorcioService.setFacturaCabeceraCxP(facturaCabeceraCxP, usuario, creation_date);
                        comandos_sql.add(comandoSQL);


                        estado = model.consorcioService.ejecutarBatchSQL(comandos_sql, logWriter);

                        if (estado.equalsIgnoreCase("ERROR")){
                            logWriter.log("ERROR: Se genero error al ejecutar el Batch de SQL. "+
                                          "Se efectuo rollback. \n" +
                                          "                                            No se proceso factura conformada de provintegral : " + facturaConformada + "\n" ,LogWriter.INFO);
                        }
                        else{
                            // logWriter.log("    Registra en base de datos sin error \n",LogWriter.INFO);
                            totalGrabados++;
                        }


                        // Inicializa los vectores de comandos para una proxima solicitud

                        comandos_sql.clear();


                    }

                    else {

                        // No existen facturas internas para una factura conformada de Provintegral
                    }

                }  // Final while que recorre la lista de facturas conformadas


                logWriter.log("Total facturas conformadas de Provintegral grabadas : " + totalGrabados + " \n",LogWriter.INFO);
                logWriter.tituloFinal();

            }
            else {

                // No existen facturas conformadas para Provintegral
                logWriter.log("ADVERTENCIA: No existen acciones marcadas con un numero de factura conformada de provintegral para realizarles la factura definitiva. \n",LogWriter.INFO);

            }

        }catch (Exception ex){
            logWriter.log(ex.getMessage());
            try{
                //model.LogProcesosSvc.finallyProceso(this.processName, this.hashCode(), usuario.getLogin(), "Finalizado con error ...\n" + ex.getMessage());
            }catch (Exception e){
                logWriter.log("Error en generaCxpProvintegral ...\n");
                logWriter.log(e.getMessage());
            }
        }

    }



















    /**
     * Crea una  lista de objetos  AccionProvintegral que son a las cuales se les va a realizar las facturas internas
     * @param distrito String Sigla de la empresa
     * @param logWriter LogWriter Objeto para manejar un log de usuario
     * @throws SQLException
     * @see
     */

    public List getAccionesProvintegral(String distrito, LogWriter logWriter)throws SQLException{
        return consorcioDao.getAccionesProvintegral( distrito,  logWriter);
    }













    /**
     * Crea una  lista: listaAccionesContratista de objetos AccionContratista que son las acciones sin prefacturar de un contratista
     * @param id_contratista String Identificacion del contratista
     * @throws SQLException
     * @see
     */

    public void setAccionesContratista(String id_contratista,int tipo)throws SQLException{
        listaAccionesContratista = consorcioDao.getAccionesContratista(id_contratista,tipo);
    }


    /**
     * Retorna una lista construida con setPrefacturaContratista
     * @return una lista: listaPrefacturaContratista de objetos PrefacturaContratista que son las acciones sin prefacturar de un contratista
     * @throws SQLException
     */
    public List getAccionesContratista() throws SQLException{
        return listaAccionesContratista;
    }


    /**
     * Crea un objeto Contratista
     * @param id_contratista Identificacion del contratista
     * @return Retorna un objeto contratista
     * @throws SQLException
     */
    public void setContratista(String id_contratista) throws SQLException{
        contratista = consorcioDao.getContratista(id_contratista);
    }


    /**
     * Retorna la informacion de un  objeto Contratista
     * @param id_contratista Identificacion del contratista
     * @return Retorna un objeto contratista
     * @throws SQLException
     */
    public Contratista getContratista() throws SQLException{
        return contratista;
    }


    public void buscaImpuestoContrato(String codigo_general_impuesto,String distrito,String tipo_impuesto )throws SQLException{

        consorcioDao.buscaImpuestoContrato(codigo_general_impuesto, distrito, tipo_impuesto);
    }

    /**
     *
     * @param distrito
     * @param contratista
     * @param tipoImpuesto
     * @param anos Entero que indica el numero de a�os que se desean cargar impuestos, comenzando en el ano actual hacia atras
     *        Para un ano actual del 2010 y anos = 2, significa que se cargaran los impuestos para los anos 2010 y 2009
     * @param anioImpuesto se agrego el a�o de impuesto para q se pueda escoger debido al cambio de impuesto 
     * @throws SQLException
     */
    public void buscaImpuesto(String distrito, Contratista contratista, String tipoImpuesto, int anos ,String anioImpuesto )throws SQLException{

        // Determina el ano actual : int AAAA
        Calendar ahoraCal = Calendar.getInstance();
        int ano = ahoraCal.get(Calendar.YEAR);


        // DETERMINACION DEL IVA

        for (int i=0 ; i < anos; i++) {

            int anoVigencia = ano - i;
            //String fechaVigencia = Integer.toString(anoVigencia) + "-12-31";
            String fechaVigencia = anioImpuesto + "-12-31";

            ImpuestoContrato impuestoEspecifico = new ImpuestoContrato();
            if(tipoImpuesto.equalsIgnoreCase("IVA")) {
                    impuestoEspecifico = consorcioDao.buscaIva(contratista.getRegimen(), distrito, fechaVigencia );
            }
            impuestoContratista.setImpuesto(impuestoEspecifico, "IVA", i);
        }
    }



    public void buscaImpuesto(String distrito, Contratista contratista, String tipoImpuesto, String tipoImpuestoRelacionado, int anos )throws SQLException{

        // Determina el ano actual : int AAAA
        Calendar ahoraCal = Calendar.getInstance();
        int ano = ahoraCal.get(Calendar.YEAR);

        // DETERMINACION DEL RIVA

        for (int i=0 ; i < anos; i++) {

            int anoVigencia = ano - i;
            String fechaVigencia = Integer.toString(anoVigencia) + "-12-31";

            ImpuestoContrato impuestoEspecifico = new ImpuestoContrato();
            if(tipoImpuestoRelacionado.equalsIgnoreCase("RIVA")) {
                    impuestoEspecifico = consorcioDao.buscaRiva(contratista.getRegimen(), distrito, fechaVigencia );
            }
            impuestoContratista.setImpuesto(impuestoEspecifico, "RIVA", i);
        }
    }









    public void buscaImpuesto(String distrito, Contratista contratista, String aiu, String tipoImpuesto, String claseImpuestoEspecifico , int anos  )throws SQLException{


        // DETERMINACION DEL RETEICA

        // Determina el ano actual : int AAAA
        Calendar ahoraCal = Calendar.getInstance();
        int ano = ahoraCal.get(Calendar.YEAR);

        for (int i=0 ; i < anos; i++) {

            int anoVigencia = ano - i;
            String fechaVigencia = Integer.toString(anoVigencia) + "-12-31";


            ImpuestoContrato impuestoEspecifico = new ImpuestoContrato();

            if(tipoImpuesto.equalsIgnoreCase("RETEICA")) {


                if(contratista.getGran_contribuyente().equalsIgnoreCase("NO")    ) {

                    impuestoEspecifico = consorcioDao.buscaReteica(contratista.getDomicilio_comercial(),
                                                                 contratista.getActividad(), distrito, aiu, fechaVigencia );

                }
                else {
                    impuestoEspecifico.setCodigo_impuesto("");
                    impuestoEspecifico.setDescripcion("Sin impuesto por ser grancontribuyente");
                    impuestoEspecifico.setPorcentaje1(0.00);
                }
            }


            impuestoContratista.setImpuesto(impuestoEspecifico, claseImpuestoEspecifico, i);

        }

    }





    public void buscaImpuesto(String distrito, Contratista contratista, String concepto, String aiu,
                              String tipoImpuesto, String claseImpuestoEspecifico,
                              int anos  )throws SQLException{


        // DETERMINACION DEL RETEFUENTE

        // Determina el ano actual : int AAAA
        Calendar ahoraCal = Calendar.getInstance();
        int ano = ahoraCal.get(Calendar.YEAR);


        // DETERMINACION DEL IVA

        for (int i=0 ; i < anos; i++) {

            int anoVigencia = ano - i;
            String fechaVigencia = Integer.toString(anoVigencia) + "-12-31";



            ImpuestoContrato impuestoEspecifico = new ImpuestoContrato();

            if(tipoImpuesto.equalsIgnoreCase("RETEFUENTE")) {

                if(contratista.getAutoretenedor().equalsIgnoreCase("NO")    ) {

                    impuestoEspecifico = consorcioDao.buscaRetefuente(distrito, concepto, aiu, fechaVigencia );
                }
                else {
                    impuestoEspecifico.setCodigo_impuesto("");
                    impuestoEspecifico.setDescripcion("Sin impuesto por ser autoretenedor");
                    impuestoEspecifico.setPorcentaje1(0.00);
                }
            }

            impuestoContratista.setImpuesto(impuestoEspecifico, claseImpuestoEspecifico, i);

        }



    }




    public String getFechaFactura(String distrito, String tipoDocumento , String facturaCliente ) throws SQLException{

        return consorcioDao.getFechaFactura( distrito,  tipoDocumento ,  facturaCliente );

    }






    public ImpuestoContrato getImpuestoContrato() throws SQLException{
        return consorcioDao.getImpuestoContrato();
    }

    public void setImpuesto(ImpuestoContrato impuestoContrato,String tipoImpuesto, int indiceAno) {
        impuestoContratista.setImpuesto(impuestoContrato, tipoImpuesto, indiceAno);
    }

    public double getPorcentaje(String tipoImpuesto, String anoVigencia){
        return impuestoContratista.getPorcentaje(tipoImpuesto, anoVigencia);
    }

    public String setSecuenciaPrefactura(String id_contratista)throws SQLException{
        return consorcioDao.setSecuenciaPrefactura (id_contratista);
    }

    public String getCodigoImpuesto(String tipoImpuesto, String anoVigencia){
        return impuestoContratista.getCodigo(tipoImpuesto, anoVigencia);
    }

    public String getDescripcion(String tipoImpuesto, String anoVigencia){
        return impuestoContratista.getDescripcion(tipoImpuesto, anoVigencia);
    }



    public String  setAccionContratista(String accion, String prefactura_contratista, String usuario_prefactura_contratista,
                                     String cod_iva_contratista, double  por_iva_contratista, double val_base_iva_contratista, double val_iva_contratista,
                                     String cod_ret_material_contratista, double  por_ret_material_contratista, double val_ret_material_contratista,
                                     String  cod_ret_mano_obra_contratista,double  por_ret_mano_obra_contratista,double  val_ret_mano_obra_contratista,
                                     String  cod_ret_otros_contratista,double  por_ret_otros_contratista, double  val_ret_otros_contratista,
                                     String  cod_ret_aiu_contratista, double por_ret_aiu_contratista, double val_ret_aiu_contratista,

                                     double  por_factoring_contratista,double  val_factoring_contratista,
                                     double  por_formula_contratista, double  val_formula_contratista,
                                     String  cod_ret_ica_contratista, double por_ret_ica_contratista, double val_ret_ica_contratista,
                                     String  cod_ret_iva_contratista, double  por_ret_iva_contratista, double val_ret_iva_contratista)throws SQLException{
       return  consorcioDao.setAccionContratista (accion, prefactura_contratista, usuario_prefactura_contratista,
                                                  cod_iva_contratista, por_iva_contratista, val_base_iva_contratista, val_iva_contratista,
                                                  cod_ret_material_contratista, por_ret_material_contratista, val_ret_material_contratista,
                                                  cod_ret_mano_obra_contratista, por_ret_mano_obra_contratista, val_ret_mano_obra_contratista,
                                                  cod_ret_otros_contratista, por_ret_otros_contratista, val_ret_otros_contratista,

                                                  cod_ret_aiu_contratista, por_ret_aiu_contratista, val_ret_aiu_contratista,

                                                  por_factoring_contratista, val_factoring_contratista,
                                                  por_formula_contratista, val_formula_contratista,
                                                  cod_ret_ica_contratista, por_ret_ica_contratista, val_ret_ica_contratista,
                                                  cod_ret_iva_contratista, por_ret_iva_contratista, val_ret_iva_contratista);
    }



    public List getFacturaGeneral() throws SQLException{
        return consorcioDao.getFacturaGeneral();
    }



    public String setCxp_doc (String dstrct,String proveedor, String tipo_documento,String documento,
                              String descripcion,String agencia,
                              String handle_code, String fecha_aprobacion,  String aprobador,String usuario_aprobacion,
                              String banco,String sucursal,String moneda, Double vlr_neto,
                              Double vlr_total_abonos,Double vlr_saldo,Double vlr_neto_me,Double vlr_total_abonos_me,
                              Double vlr_saldo_me, Double tasa,String observacion,String user_update,
                              String creation_user,String base,String clase_documento,String moneda_banco,
                              String fecha_documento,String fecha_vencimiento,String clase_documento_rel,
                              String last_update, String creation_date,
                              String tipo_referencia_1, String referencia_1, String tipo_referencia_2, String referencia_2,
                              String tipo_documento_rel,  String documento_relacionado,
                              String tipo_referencia_3, String referencia_3, String indicador_traslado_fintra)throws SQLException{

        return consorcioDao.setCxp_doc(dstrct, proveedor, tipo_documento, documento, descripcion,
                                    agencia, handle_code, fecha_aprobacion, aprobador, usuario_aprobacion, banco, sucursal,
                                    moneda, vlr_neto, vlr_total_abonos, vlr_saldo, vlr_neto_me,
                                    vlr_total_abonos_me, vlr_saldo_me, tasa, observacion, user_update,
                                    creation_user, base, clase_documento, moneda_banco,
                                    fecha_documento, fecha_vencimiento, clase_documento_rel,
                                    last_update, creation_date, tipo_referencia_1, referencia_1, tipo_referencia_2, referencia_2,
                                    tipo_documento_rel, documento_relacionado,
                                    tipo_referencia_3, referencia_3, indicador_traslado_fintra);
    }




    public String setCxp_items_doc (String dstrct, String proveedor, String tipo_documento,
                                   String documento, String item,String descripcion,
                                   double vlr, double vlr_me, String codigo_cuenta,
                                   String user_update,
                                   String creation_user, String base,
                                   String concepto, String auxiliar,
                                   String last_update, String creation_date,
                                   String tipo_referencia_1, String referencia_1, String tipo_referencia_2, String referencia_2,
                                   String tipo_referencia_3, String referencia_3)throws SQLException{

        return consorcioDao.setCxp_items_doc(dstrct, proveedor, tipo_documento, documento,
                                          item, descripcion, vlr, vlr_me, codigo_cuenta,
                                          user_update,
                                          creation_user, base, concepto, auxiliar,
                                          last_update, creation_date,
                                          tipo_referencia_1, referencia_1, tipo_referencia_2, referencia_2,  tipo_referencia_3, referencia_3);
    }



   public String setFacturaContratista (String factura_contratista_fintra, String id_accion) throws SQLException {
       return consorcioDao.setFacturaContratista( factura_contratista_fintra,  id_accion);

   }

   public String setFacturaProvintegral (String id_accion) throws SQLException {
       return consorcioDao.setFacturaProvintegral(  id_accion);

   }




   public String setNcProvintegral( String documento, LogWriter logWriter)throws SQLException {
      return consorcioDao.setNcProvintegral( documento,  logWriter);
   }



    public void generaNcCxpInterna_Contratista(String usuario, String dstrct, ModelOpav  model, LogWriter logWriter) {

        String registro = "";

        int totalGrabados = 0;
        String estado = "";
        Vector comandos_sql =new Vector();

        java.util.Date fechaActual = new Date();
        String creation_date = fechaActual.toString();


        try{

            logWriter.tituloInicial("GENERACION DE NOTAS CREDITO PARA CANCELAR FACTURAS INTERNAS DE CONTRATISTAS");

            List listaFacturaContratistaFintra =  consorcioDao.getFacturaContratistaFintra();

            if (!listaFacturaContratistaFintra.isEmpty()){



                String comandoSQL = "";
                String contratista = "";
                String nit = "";
                String factura_contratista_fintra = "";
                String condicion = "";
                List listaFacturaCabeceraCxP = null;
                List listaFacturaDetalleCxP = null;

                FacturaCabeceraCxP facturaCabeceraCxP = null;
                FacturaDetalleCxP facturaDetalleCxP = null;


                Iterator it = listaFacturaContratistaFintra.iterator();
                while (it.hasNext()  ) {

                    registro = (String)it.next();

                    String[] campo = registro.split("%");
                    contratista = campo[0];
                    nit = campo[1];
                    factura_contratista_fintra = campo[2];

                       condicion =  " dstrct = '" + dstrct + "' and ";
                        condicion +=  "proveedor = '" + nit + "' and ";
                        // condicion +=  "a.proveedor = '8020081907' and ";
                        condicion +=  "tipo_documento = '" + "FAP" + "' and ";
                        condicion +=  "documento = '" + factura_contratista_fintra + "' and ";
                        // condicion +=  "a.documento = 'CC002-000288' and ";
                        condicion +=  "reg_status != 'A' ";

                    listaFacturaCabeceraCxP =  consorcioDao.getFacturaCxPCabecera(condicion);

                    if (listaFacturaCabeceraCxP != null && !listaFacturaCabeceraCxP.isEmpty()) {
                        facturaCabeceraCxP = (FacturaCabeceraCxP) listaFacturaCabeceraCxP.get(0);
                        facturaCabeceraCxP.setIndicador_traslado_fintra("N");
                        comandoSQL = model.consorcioService.setFacturaCabeceraCxpContratista(facturaCabeceraCxP, usuario, creation_date );
                        comandos_sql.add(comandoSQL);

                        condicion =  " a.dstrct = '" + dstrct + "' and ";
                        condicion +=  "a.proveedor = '" + nit + "' and ";
                        // condicion +=  "a.proveedor = '8020081907' and ";
                        condicion +=  "a.tipo_documento = '" + "FAP" + "' and ";
                        condicion +=  "a.documento = '" + factura_contratista_fintra + "' and ";
                        // condicion +=  "a.documento = 'CC002-000288' and ";
                        condicion +=  "a.reg_status != 'A' ";

                        listaFacturaDetalleCxP =  consorcioDao.getFacturaCxPDetalleInterna(condicion, facturaCabeceraCxP.getHandle_code());

                        Iterator it2 = listaFacturaDetalleCxP.iterator();
                        while (it2.hasNext()  ) {
                            facturaDetalleCxP = (FacturaDetalleCxP)it2.next();
                            facturaDetalleCxP.setTipo_documento("NC");
                            facturaDetalleCxP.setDescripcion("Cancela factura interna contratista: " +
                                                              facturaDetalleCxP.getDocumento() + "Concepto: " +
                                                              facturaDetalleCxP.getDescripcion());

                             comandoSQL = model.consorcioService.setFacturaDetalleCxP(facturaDetalleCxP, usuario, creation_date);
                             comandos_sql.add(comandoSQL);
                        }
                        
                            comandoSQL = consorcioDao.setAccionNC(facturaCabeceraCxP.getDocumento(), facturaCabeceraCxP.getReferencia_1());
                            comandos_sql.add(comandoSQL);

                        comandoSQL = consorcioDao.setCancelaCxP(facturaCabeceraCxP.getDstrct(), facturaCabeceraCxP.getProveedor(),
                                                                facturaCabeceraCxP.getTipo_documento(), facturaCabeceraCxP.getDocumento());
                        comandos_sql.add(comandoSQL);



                    }
                    else {
                        // No se ha creado la factura interna

                    }


                   // GRABAR TODO A LA BASE DE DATOS
                   estado = model.consorcioService.ejecutarBatchSQL(comandos_sql, logWriter);

                    if (estado.equalsIgnoreCase("ERROR")){
                        logWriter.log("ERROR: Se genero error al ejecutar el Batch de SQL. "+
                                      "Se efectuo rollback. \n" +
                                      "                                            Factura interna     : " + factura_contratista_fintra + "\n" ,LogWriter.INFO);
                    }
                    else{
                        // logWriter.log("    Registra en base de datos sin error \n",LogWriter.INFO);
                        totalGrabados++;
                    }

                    // Inicializa los vectores de comandos para una proxima solicitud

                    comandos_sql.clear();

                }

                logWriter.log("Total facturas grabadas : " + totalGrabados + " \n",LogWriter.INFO);
                logWriter.tituloFinal();

            }

            else {
                logWriter.log("ADVERTENCIA: No existen facturas para elaborarles las Notas creditos. \n",LogWriter.INFO);
            }

        }
        catch (Exception e){
            Util.imprimirTrace(e);
            logWriter.log("ERROR: Se genero error de Exception al ejecutar el Metodo : generaNcCxpInterna_Contratista. " +
                          "                                 Mensaje : " + e.getMessage() + "\n" ,LogWriter.INFO) ;
        }


    }



    public String setFacturaCabeceraCxpContratista(FacturaCabeceraCxP fc, String usuario, String fechaCreacion) throws SQLException  {

        java.util.Date fechaActual = new Date();
        DateFormat formato_fecha;
        formato_fecha = new SimpleDateFormat("yyyy-MM-dd");
        String fechaProceso = formato_fecha.format(fechaActual);

        String comandoSQL = "";


        comandoSQL =consorcioDao.setCxp_doc(fc.getDstrct(), fc.getProveedor(), "NC", fc.getDocumento(),
                             "Cancela factura interna contratista: "+ fc.getDocumento() ,
                             fc.getAgencia(), fc.getHandle_code(), fechaCreacion,  fc.getAprobador() ,
                             fc.getUsuario_aprobacion(), fc.getBanco(), fc.getSucursal(), fc.getMoneda(),
                             fc.getVlr_saldo(), 0.00, fc.getVlr_saldo(),
                             fc.getVlr_saldo_me(), 0.00, fc.getVlr_saldo_me(),
                             fc.getTasa(), fc.getObservacion(), usuario,
                             usuario, fc.getBase(), fc.getClase_documento(), fc.getMoneda_banco(),
                             fechaProceso , fechaProceso, fc.getClase_documento_rel(),
                             fechaCreacion, fechaCreacion,
                             fc.getTipo_referencia_1(), fc.getReferencia_1(), fc.getTipo_referencia_2(), fc.getReferencia_2(),
                             "FAP", fc.getDocumento(),"FAP", fc.getDocumento(), fc.getIndicador_traslado_fintra() );

        return comandoSQL;




    }






    public String setFacturaCabeceraCxP(FacturaCabeceraCxP fc, String usuario, String fechaCreacion) throws SQLException  {

        java.util.Date fechaActual = new Date();
        DateFormat formato_fecha;
        formato_fecha = new SimpleDateFormat("yyyy-MM-dd");
        String fechaProceso = formato_fecha.format(fechaActual);

        String comandoSQL = "";


        comandoSQL =consorcioDao.setCxp_doc(fc.getDstrct(), fc.getProveedor(), fc.getTipo_documento(), fc.getDocumento(),
                             fc.getDescripcion() ,
                             fc.getAgencia(), fc.getHandle_code(), fechaCreacion,  fc.getAprobador() ,
                             fc.getUsuario_aprobacion(), fc.getBanco(), fc.getSucursal(), fc.getMoneda(),
                             fc.getVlr_saldo(), 0.00, fc.getVlr_saldo(),
                             fc.getVlr_saldo_me(), 0.00, fc.getVlr_saldo_me(),
                             fc.getTasa(), fc.getObservacion(), usuario,
                             usuario, fc.getBase(), fc.getClase_documento(), fc.getMoneda_banco(),
                             fechaProceso , fechaProceso, fc.getClase_documento_rel(),
                             fechaCreacion, fechaCreacion,
                             fc.getTipo_referencia_1(), fc.getReferencia_1(), fc.getTipo_referencia_2(), fc.getReferencia_2(),
                             fc.getTipo_documento_rel(), fc.getDocumento_relacionado(), fc.getTipo_referencia_3(), fc.getReferencia_3(), fc.getIndicador_traslado_fintra() );

        return comandoSQL;




    }









    public String setFacturaDetalleCxP(FacturaDetalleCxP fd, String usuario, String fechaCreacion) throws SQLException  {

        String comandoSQL = "";


        comandoSQL =consorcioDao.setCxp_items_doc(

                             fd.getDstrct(), fd.getProveedor(), fd.getTipo_documento(),
                             fd.getDocumento(), fd.getItem(),
                             fd.getDescripcion() ,
                             fd.getVlr(),fd.getVlr(), fd.getCodigo_cuenta(), // 20100623
                             usuario, usuario, fd.getBase(),
                             fd.getConcepto(), fd.getProveedor(),
                             fechaCreacion, fechaCreacion,
                             fd.getTipo_referencia_1(), fd.getReferencia_1(), fd.getTipo_referencia_2(), fd.getReferencia_2(),
                             "FAP", fd.getDocumento() );
        return comandoSQL;


    }



   public ArrayList getFacturaContratistaDefinitiva () throws SQLException {

       return consorcioDao.getFacturaContratistaDefinitiva();
   }






    public void generaCxpFinalContratista(String usuario, String dstrct, ModelOpav  model, LogWriter logWriter) {


        java.util.Date fechaActual = new Date();
        String creation_date = fechaActual.toString();
        DateFormat formato_fecha;
        formato_fecha = new SimpleDateFormat("yyyy-MM-dd");
        String fechaDocumento = formato_fecha.format(fechaActual);

        String comandoSQL = "";
        Vector comandos_sql = new Vector();

        int totalGrabados = 0;
        String estado = "";


        try{


            String  CUENTA_ITEM_CXP_FINAL               = constanteService.getValor("FINV", "CUENTA_ITEM_CXP_FINAL", "");  // 28151099 28151006
            
            String MONEDA_LOCAL                         = constanteService.getValor("FINV", "MONEDA_LOCAL", "");
            String BASE_LOCAL                           = constanteService.getValor("FINV", "BASE_LOCAL", "");
            String CLASE_DOCUMENTO                      = constanteService.getValor("FINV", "CLASE_DOCUMENTO", "");
            String HANDLE_CODE_CD                       = constanteService.getValor("FINV", "HANDLE_CODE_CD", "");
            String APROBADOR_AUTOMATICO                 = constanteService.getValor("FINV", "APROBADOR_AUTOMATICO", "");
            String AGENCIA_PRINCIPAL                    = constanteService.getValor("FINV", "AGENCIA_PRINCIPAL", "");





            logWriter.tituloInicial("GENERACION DE FACTURAS FINALES DE CONTRATISTAS");


            // Lee las facturas de un contratista que requieren una factura final
            ArrayList listaFacturaContratista =  consorcioDao.getFacturaContratistaDefinitiva();

            if (!listaFacturaContratista.isEmpty()){

                String registro = "";
                String factura_contratista = "";
                String codigo_contratista = "";
                String tipo_distribucion = "";
                String multiservicio = "";

                String condicion = "";

                ArrayList listaFacturaCxPInterna = null;

                // Inicia el recorrido para leer las facturas internas de cada factura de contratista
                for (int i=0; i < listaFacturaContratista.size() ; i++) {

                    registro = (String) listaFacturaContratista.get(i);

                    String[] campo = registro.split("%");
                    codigo_contratista = campo[0];
                    factura_contratista = campo[1];
                    tipo_distribucion = campo[2];
                    multiservicio = campo[3];
                    
                    if(multiservicio.subSequence(0, 4).equals("ACON") && tipo_distribucion.trim().equals("43")){
                        CUENTA_ITEM_CXP_FINAL = constanteService.getValor("FINV", "AIRES_CUENTA_ITEM_CXP_FINAL", "");  // 28151099 28151006
                        HANDLE_CODE_CD = constanteService.getValor("FINV", "HANDLE_CODE_AJ", "");
                    }else{
                        CUENTA_ITEM_CXP_FINAL = constanteService.getValor("FINV", "CUENTA_ITEM_CXP_FINAL", "");  // 28151099 28151006
                        HANDLE_CODE_CD = constanteService.getValor("FINV", "HANDLE_CODE_CD", "");
                    
                    }


                    // Lee las facturas internas correspondientes a la factura conformada del contratista del contratista
                    listaFacturaCxPInterna = consorcioDao.getFacturaCxpInterna(codigo_contratista, factura_contratista);

                    String nit = "";
                    String factura_contratista_fintra = "";
                    List listaFacturaDetalleCxP = new LinkedList();

                    double totalFactura = 0;

                    String conjuntoFacturas = "";

                    List listaDetalle = new LinkedList();

                    for (int j=0; j < listaFacturaCxPInterna.size() ; j++) {


                        registro = (String) listaFacturaCxPInterna.get(j);
                        campo = registro.split("%");
                        nit  = campo[0];
                        factura_contratista_fintra = campo[1];

                        conjuntoFacturas += factura_contratista_fintra + " ";


                        condicion =  " dstrct = '" + dstrct + "' and ";
                        condicion +=  "proveedor = '" + nit + "' and ";
                        condicion +=  "tipo_documento = '" + "FAP" + "' and ";
                        condicion +=  "documento = '" + factura_contratista_fintra + "' and ";
                        condicion +=  "reg_status != 'A' ";

                        // Lee los items de la factura interna y los adiciona en una lista total de facturas



                        listaDetalle = (List) consorcioDao.getFacturaCxPDetalle(condicion);


                        listaFacturaDetalleCxP.addAll(listaDetalle );


                    }

                    
                    Proveedor objeto_proveedor = proveedorService.obtenerProveedorPorNit(nit);



                    FacturaDetalleCxP facturaDetalleCxP = null;
                    int item = 0;
                    Iterator it = listaFacturaDetalleCxP.iterator();

                    while (it.hasNext()  ) {

                        facturaDetalleCxP = (FacturaDetalleCxP)it.next();
                        item++;
                        String stringItem =   Util.llenarConCerosALaIzquierda( item , 3 ) ;

                        // Actualiza campos del item

                        String documentoInterno = facturaDetalleCxP.getDocumento();
                        facturaDetalleCxP.setDocumento(factura_contratista);
                        facturaDetalleCxP.setItem(stringItem);
                        String concepto = facturaDetalleCxP.getConcepto();


                        String constante = "CP_AL_CONCEPTO_" + concepto;  // 20100623
                        String  CONCEPTO = constanteService.getValor("FINV", constante, "");
                        facturaDetalleCxP.setConcepto(CONCEPTO);
                        constante = "CP_A_LA_CUENTA_DEL_CONCEPTO_" + concepto;  // 20100623
                        String  CUENTA = constanteService.getValor("FINV", constante, "");

                        facturaDetalleCxP.setCodigo_cuenta(CUENTA_ITEM_CXP_FINAL);

                        facturaDetalleCxP.setLast_update(creation_date);
                        facturaDetalleCxP.setCreation_date(creation_date);
                        facturaDetalleCxP.setUser_update(usuario);
                        facturaDetalleCxP.setCreation_user(usuario);

                        facturaDetalleCxP.setTipo_referencia_2(facturaDetalleCxP.getTipo_documento());
                        facturaDetalleCxP.setReferencia_2(documentoInterno);

                        totalFactura += facturaDetalleCxP.getVlr();

                        comandoSQL = model.consorcioService.setFacturaDetalleCxpFinalContratista(facturaDetalleCxP, usuario, creation_date);
                        comandos_sql.add(comandoSQL);
                    }


                    // Arma la cabecera de la factura final al contratista

                    String proveedor = facturaDetalleCxP.getProveedor();
                    String tipo_documento = facturaDetalleCxP.getTipo_documento();
                    String descripcion = "Reunifica facturas internas: " + conjuntoFacturas;

                    // Calculo de la fecha de vencimiento de la factura

                    int plazo = objeto_proveedor.getPlazo();
                    Calendar calendarFechaVencimiento = Calendar.getInstance();
                    String ano = fechaDocumento.substring(0,4);
                    String mes = fechaDocumento.substring(5,7);
                    String dia = fechaDocumento.substring(8,10);
                    calendarFechaVencimiento.set(Integer.parseInt(ano),Integer.parseInt(mes)-1,Integer.parseInt(dia),8,0,0);
                    calendarFechaVencimiento.add(Calendar.DAY_OF_YEAR,  plazo);
                    String fecha_vencimiento = formato_fecha.format(calendarFechaVencimiento.getTime()) ;

                    comandoSQL = model.consorcioService.setCxp_doc(dstrct, proveedor, tipo_documento, factura_contratista, descripcion,
                                                                   AGENCIA_PRINCIPAL, HANDLE_CODE_CD, "0099-01-01", APROBADOR_AUTOMATICO, "", "ABONO PRESTAMOS", "CC",
                                                                   MONEDA_LOCAL, totalFactura, 0.00, totalFactura, totalFactura,
                                                                   0.00, totalFactura, 1.00, conjuntoFacturas, usuario,
                                                                   usuario, BASE_LOCAL, CLASE_DOCUMENTO, MONEDA_LOCAL,
                                                                   fechaDocumento, fecha_vencimiento, "",
                                                                   creation_date, creation_date, "","","",
                                                                   "", "","","","","S");
                    comandos_sql.add(0, comandoSQL);


                    // Actualiza las acciones con la fecha de factura del contratista final
                    comandoSQL = consorcioDao.setAccionFacturaContratista(factura_contratista, creation_date );
                    comandos_sql.add(comandoSQL);



                   // GRABAR TODO A LA BASE DE DATOS
                   estado = model.consorcioService.ejecutarBatchSQL(comandos_sql, logWriter);

                    if (estado.equalsIgnoreCase("ERROR")){
                        logWriter.log("ERROR: Se genero error al ejecutar el Batch de SQL. "+
                                      "Se efectuo rollback. \n" +
                                      "                                            Factura interna     : " + factura_contratista_fintra + "\n" ,LogWriter.INFO);
                    }
                    else{
                        // logWriter.log("    Registra en base de datos sin error \n",LogWriter.INFO);
                        totalGrabados++;
                    }

                    // Inicializa los vectores de comandos para una proxima solicitud

                    comandos_sql.clear();


                }


                logWriter.log("Total facturas grabadas : " + totalGrabados + " \n",LogWriter.INFO);
                logWriter.tituloFinal();

            }

            else {
                logWriter.log("ADVERTENCIA: No existen facturas para elaborarle facturas finales al contratista. \n",LogWriter.INFO);
            }

        }
        catch (Exception e){
            Util.imprimirTrace(e);
            logWriter.log("ERROR: Se genero error de Exception al ejecutar el Metodo : generaCxpFinalContratista. " +
                          "                                 Mensaje : " + e.getMessage() + "\n" ,LogWriter.INFO) ;
        }


    }



    public String setFacturaDetalleCxpFinalContratista(FacturaDetalleCxP fd, String usuario, String fechaCreacion) throws SQLException  {

        String comandoSQL = "";


        comandoSQL =consorcioDao.setCxp_items_doc(

                             fd.getDstrct(), fd.getProveedor(), "FAP",
                             fd.getDocumento(), fd.getItem(),
                             fd.getDescripcion() ,
                             fd.getVlr(),fd.getVlr(), fd.getCodigo_cuenta(),  // 20100623
                             usuario, usuario, fd.getBase(),
                             fd.getConcepto(), fd.getProveedor(),
                             fechaCreacion, fechaCreacion,
                             fd.getTipo_referencia_1(), fd.getReferencia_1(), fd.getTipo_referencia_2(), fd.getReferencia_2(),
                             "FAP", fd.getDocumento() );
        return comandoSQL;


    }





    public void generaNcTrasladoContratista(String usuario, String dstrct, ModelOpav  model, LogWriter logWriter) {


        int totalGrabados = 0;
        String estado = "";
        Vector comandos_sql =new Vector();
        String comandoSQL = "";

        java.util.Date fechaActual = new Date();
        String creation_date = fechaActual.toString();

        Date fecha_hoy = new Date();
        DateFormat formato_fecha = new SimpleDateFormat("yyyy-MM-dd");
        String fechaDocumento = formato_fecha.format(fecha_hoy);

        String condicion = "";
        FacturaCabeceraCxP facturaCabeceraCxP = new FacturaCabeceraCxP();
        FacturaDetalleCxP  facturaDetalleCxP  = new FacturaDetalleCxP();

        try{

            String MONEDA_LOCAL                         = constanteService.getValor("FINV", "MONEDA_LOCAL", "");
            String BASE_LOCAL                           = constanteService.getValor("FINV", "BASE_LOCAL", "");
            String CLASE_DOCUMENTO                      = constanteService.getValor("FINV", "CLASE_DOCUMENTO", "");
            String APROBADOR_AUTOMATICO                 = constanteService.getValor("FINV", "APROBADOR_AUTOMATICO", "");
            String AGENCIA_PRINCIPAL                    = constanteService.getValor("FINV", "AGENCIA_PRINCIPAL", "");
            String TIPO_DOCUMENTO_NC_CXP                = constanteService.getValor("FINV", "TIPO_DOCUMENTO_NC_CXP", "");
            String CUENTA_NC_CXP_TRASLADO               = constanteService.getValor("FINV", "CUENTA_NC_CXP_TRASLADO", "");
            String HANDLE_CODE_NC_CXP_TRASLADO          = constanteService.getValor("FINV", "HANDLE_CODE_NC_CXP_TRASLADO", "");
            String BANCO_POR_DEFECTO                    = constanteService.getValor("FINV", "BANCO_POR_DEFECTO", "");
            String SUCURSAL_POR_DEFECTO                 = constanteService.getValor("FINV", "SUCURSAL_POR_DEFECTO", "");



            logWriter.tituloInicial("GENERACION DE NOTAS CREDITO PARA TRASLADO DE FACTURA CONTRATISTA A FINTRA");

            condicion =  " dstrct = '" + dstrct + "' and ";
            condicion +=  "tipo_documento = '" + "FAP" + "' and ";
            condicion +=  "reg_status != 'A' and ";
            condicion +=  "indicador_traslado_fintra = 'S' and " ;
            condicion +=  "referencia_2 = '' ";



            List listaFacturaCabeceraCxP =  consorcioDao.getFacturaCxPCabecera(condicion);

            if (listaFacturaCabeceraCxP != null) {

                logWriter.log("Total facturas leidas : " + listaFacturaCabeceraCxP.size() + " \n",LogWriter.INFO);

                Iterator it = listaFacturaCabeceraCxP.iterator();
                while (it.hasNext()  ) {

                   facturaCabeceraCxP = (FacturaCabeceraCxP)it.next();

                   // PASO 1 Arma la cabecera de la nota credito que cancela la factura del contratista

                   String proveedor = facturaCabeceraCxP.getProveedor();
                   String tipo_documento = TIPO_DOCUMENTO_NC_CXP;
                   String factura_contratista = facturaCabeceraCxP.getDocumento();
                   String descripcion = "Cancela factura contratista por traslado a Fintra: " + facturaCabeceraCxP.getDocumento();
                   double totalFactura = facturaCabeceraCxP.getVlr_saldo();
                   String fechaVencimiento = fechaDocumento;
                   String tipoDocumentoRelacionado = facturaCabeceraCxP.getTipo_documento();

                   comandoSQL = model.consorcioService.setCxp_doc(dstrct, proveedor, tipo_documento, factura_contratista, descripcion,
                                                                  AGENCIA_PRINCIPAL, HANDLE_CODE_NC_CXP_TRASLADO, creation_date,  APROBADOR_AUTOMATICO, APROBADOR_AUTOMATICO, BANCO_POR_DEFECTO,
                                                                  SUCURSAL_POR_DEFECTO, MONEDA_LOCAL, totalFactura, 0.00, totalFactura, totalFactura,
                                                                  0.00, totalFactura, 1.00, "", usuario,
                                                                  usuario, BASE_LOCAL, CLASE_DOCUMENTO, MONEDA_LOCAL,
                                                                  fechaDocumento, fechaVencimiento, "",
                                                                  creation_date, creation_date, "","","",
                                                                  "", tipoDocumentoRelacionado, factura_contratista, "","","N");
                    comandos_sql.add(comandoSQL);

                    // PASO 2 Arma detalle de la nota credito


                    int item = 0;


                    item++;
                    String stringItem =   Util.llenarConCerosALaIzquierda( item , 3 ) ;

                    // Actualiza campos del item

                    facturaDetalleCxP.setReg_status("");
                    facturaDetalleCxP.setDstrct(dstrct);
                    facturaDetalleCxP.setProveedor(proveedor);
                    facturaDetalleCxP.setTipo_documento(TIPO_DOCUMENTO_NC_CXP);
                    facturaDetalleCxP.setDocumento(factura_contratista);
                    facturaDetalleCxP.setItem(stringItem);
                    facturaDetalleCxP.setDescripcion(descripcion);
                    facturaDetalleCxP.setVlr(totalFactura);
                    facturaDetalleCxP.setVlr_me(totalFactura);
                    facturaDetalleCxP.setCodigo_cuenta(CUENTA_NC_CXP_TRASLADO);
                    facturaDetalleCxP.setCodigo_abc("");
                    facturaDetalleCxP.setPlanilla("");
                    facturaDetalleCxP.setLast_update(creation_date);
                    facturaDetalleCxP.setUser_update(usuario);
                    facturaDetalleCxP.setCreation_date(creation_date);
                    facturaDetalleCxP.setCreation_user(usuario);
                    facturaDetalleCxP.setBase(BASE_LOCAL);
                    facturaDetalleCxP.setCodcliarea("");
                    facturaDetalleCxP.setTipcliarea("");
                    facturaDetalleCxP.setConcepto("");
                    facturaDetalleCxP.setAuxiliar(proveedor);
                    facturaDetalleCxP.setTipo_referencia_1(tipo_documento);
                    facturaDetalleCxP.setReferencia_1(factura_contratista);
                    facturaDetalleCxP.setTipo_referencia_2("");
                    facturaDetalleCxP.setReferencia_2("");
                    facturaDetalleCxP.setTipo_referencia_3("");
                    facturaDetalleCxP.setReferencia_3("");


                    comandoSQL = model.consorcioService. setFacturaDetalle(facturaDetalleCxP, usuario, creation_date);
                    comandos_sql.add(comandoSQL);


                    // PASO 3 Actualiza la factura del contratista

                    String camposAdicionales = "tipo_referencia_2 = '" + tipo_documento + "', ";
                    camposAdicionales += "referencia_2 = '" + factura_contratista + "' , ";

                    comandoSQL = consorcioDao.setCancelaFacturaCxP(dstrct, proveedor,facturaCabeceraCxP.getTipo_documento(),
                                                                   factura_contratista, camposAdicionales );
                    comandos_sql.add(comandoSQL);



                   // PASO 4 GRABAR TODO A LA BASE DE DATOS
                   estado = model.consorcioService.ejecutarBatchSQL(comandos_sql, logWriter);

                    if (estado.equalsIgnoreCase("ERROR")){
                        logWriter.log("ERROR: Se genero error al ejecutar el Batch de SQL. "+
                                      "Se efectuo rollback. \n" +
                                      "                                            Factura contratista : " + factura_contratista + "\n" ,LogWriter.INFO);
                    }
                    else{
                        // logWriter.log("    Registra en base de datos sin error \n",LogWriter.INFO);
                        totalGrabados++;
                    }

                    // Inicializa los vectores de comandos para una proxima solicitud

                    comandos_sql.clear();

                }

                logWriter.log("Total facturas grabadas : " + totalGrabados + " \n",LogWriter.INFO);
                logWriter.tituloFinal();

            }

            else {
                logWriter.log("ADVERTENCIA: No existen facturas para elaborarles las Notas creditos a las CxP del contratista. \n",LogWriter.INFO);
            }

        }
        catch (Exception e){
            Util.imprimirTrace(e);
            logWriter.log("ERROR: Se genero error de Exception al ejecutar el Metodo : generaNcTrasladoContratista. " +
                          "                                 Mensaje : " + e.getMessage() + "\n" ,LogWriter.INFO) ;
        }


    }



    public String setFacturaDetalle(FacturaDetalleCxP fd, String usuario, String fechaCreacion) throws SQLException  {

        String comandoSQL = "";


        comandoSQL =consorcioDao.setCxp_items_doc(

                             fd.getDstrct(), fd.getProveedor(), fd.getTipo_documento(),
                             fd.getDocumento(), fd.getItem(),
                             fd.getDescripcion() ,
                             fd.getVlr(),fd.getVlr(), fd.getCodigo_cuenta(),  // 20100623
                             usuario, usuario, fd.getBase(),
                             fd.getConcepto(), fd.getProveedor(),
                             fechaCreacion, fechaCreacion,
                             fd.getTipo_referencia_1(), fd.getReferencia_1(), fd.getTipo_referencia_2(), fd.getReferencia_2(),
                             fd.getTipo_referencia_3(), fd.getReferencia_3() );
        return comandoSQL;


    }





    public void generaTrasladoContratista(String usuario, String dstrct, ModelOpav  model, LogWriter logWriter) {


        int totalGrabados = 0;
        String estado = "";
        Vector comandos_sql =new Vector();
        String comandoSQL = "";

        java.util.Date fechaActual = new Date();
        String creation_date = fechaActual.toString();


        String condicion = "";
        FacturaCabeceraCxP facturaCabeceraCxP = new FacturaCabeceraCxP();
        FacturaDetalleCxP  facturaDetalleCxP  = new FacturaDetalleCxP();
        ItemCxPContratista itemCxPContratista = new ItemCxPContratista();

        try{

            String MONEDA_LOCAL                         = constanteService.getValor("FINV", "MONEDA_LOCAL", "");
            String BASE_LOCAL                           = constanteService.getValor("FINV", "BASE_LOCAL", "");
            String CLASE_DOCUMENTO                      = constanteService.getValor("FINV", "CLASE_DOCUMENTO", "");
            String APROBADOR_AUTOMATICO                 = constanteService.getValor("FINV", "APROBADOR_AUTOMATICO", "");
            String AGENCIA_PRINCIPAL                    = constanteService.getValor("FINV", "AGENCIA_PRINCIPAL", "");
            String TIPO_DOCUMENTO_FAP                   = constanteService.getValor("FINV", "TIPO_DOCUMENTO_FAP", "");
            String HANDLE_CODE_OM                       = constanteService.getValor("FINV", "HANDLE_CODE_OM", "");
            String BANCO_POR_DEFECTO                    = constanteService.getValor("FINV", "BANCO_POR_DEFECTO", "");
            String SUCURSAL_POR_DEFECTO                 = constanteService.getValor("FINV", "SUCURSAL_POR_DEFECTO", "");


            logWriter.tituloInicial("GENERACION DE FACTURAS TRASLADADAS A FINTRA");

            condicion =   " reg_status != 'A' and ";
            condicion +=  " indicador_traslado_fintra = 'S' and ";
            condicion +=  " referencia_2 != '' and ";
            condicion +=  " referencia_3  = '' ";


            List listaFacturaCabeceraCxP =  consorcioDao.getFacturaCxPCabecera(condicion);

            if (listaFacturaCabeceraCxP != null) {

                logWriter.log("Total facturas leidas : " + listaFacturaCabeceraCxP.size() + " \n",LogWriter.INFO);

                Iterator it = listaFacturaCabeceraCxP.iterator();
                while (it.hasNext()  ) {

                   facturaCabeceraCxP = (FacturaCabeceraCxP)it.next();

                   // PASO 1 ARMA LA CABECERA DE LA FACTURA DEL CONTRATISTA A SER TRASLADADA

                   String proveedor = facturaCabeceraCxP.getProveedor();
                   String tipo_documento = TIPO_DOCUMENTO_FAP;
                   String factura_contratista = facturaCabeceraCxP.getDocumento();
                   String descripcion = "Factura trasladada del Consorcio: " + facturaCabeceraCxP.getDocumento() + "  " +
                                        facturaCabeceraCxP.getDescripcion();
                   double totalFactura = facturaCabeceraCxP.getVlr_neto();
                   String fechaDocumento = facturaCabeceraCxP.getFecha_documento();
                   String fechaVencimiento = facturaCabeceraCxP.getFecha_vencimiento();


                   comandoSQL = model.consorcioService.setCxp_doc(dstrct, proveedor, tipo_documento, factura_contratista, descripcion,
                                                                  AGENCIA_PRINCIPAL, HANDLE_CODE_OM, "0099-01-01 00:00:00",  APROBADOR_AUTOMATICO, APROBADOR_AUTOMATICO, BANCO_POR_DEFECTO,
                                                                  SUCURSAL_POR_DEFECTO, MONEDA_LOCAL, totalFactura, 0.00, totalFactura, totalFactura,
                                                                  0.00, totalFactura, 1.00, "", usuario,
                                                                  usuario, BASE_LOCAL, CLASE_DOCUMENTO, MONEDA_LOCAL,
                                                                  fechaDocumento, fechaVencimiento, "",
                                                                  creation_date, creation_date, "","","",
                                                                  "", "", "", "","","N");
                    comandos_sql.add(comandoSQL);

                    // PASO 2 ARMA DETALLE DE LA FACTURA TRASLADADA A FINTRA


                    int item = 0;


                    ArrayList listaItem = consorcioDao.getFacturaDetalleContratista(dstrct,  proveedor,  tipo_documento,  factura_contratista);


                    Iterator it2 = listaItem.iterator();
                    while (it2.hasNext()  ) {

                        item++;
                        String stringItem =   Util.llenarConCerosALaIzquierda( item , 3 ) ;

                        itemCxPContratista = (ItemCxPContratista)it2.next();

                        String constanteConceptoCuenta = "CUENTA_CONCEPTO_" + itemCxPContratista.getReferencia();
                        String cuentaItem = constanteService.getValor("FINV", constanteConceptoCuenta, "");

                        facturaDetalleCxP.setReg_status( "" );
                        facturaDetalleCxP.setDstrct( dstrct );
                        facturaDetalleCxP.setProveedor(proveedor );
                        facturaDetalleCxP.setTipo_documento( tipo_documento );
                        facturaDetalleCxP.setDocumento(factura_contratista );
                        facturaDetalleCxP.setItem(stringItem );
                        facturaDetalleCxP.setDescripcion(itemCxPContratista.getDescripcion() );
                        facturaDetalleCxP.setVlr(itemCxPContratista.getTotal_vlr());
                        facturaDetalleCxP.setVlr_me(itemCxPContratista.getTotal_vlr());
                        facturaDetalleCxP.setCodigo_cuenta(cuentaItem);
                        facturaDetalleCxP.setCodigo_abc("");
                        facturaDetalleCxP.setPlanilla("");
                        facturaDetalleCxP.setLast_update( creation_date );
                        facturaDetalleCxP.setUser_update(usuario );
                        facturaDetalleCxP.setCreation_date(creation_date );
                        facturaDetalleCxP.setCreation_user(usuario );
                        facturaDetalleCxP.setBase( BASE_LOCAL );
                        facturaDetalleCxP.setCodcliarea("" );
                        facturaDetalleCxP.setTipcliarea("" );
                        facturaDetalleCxP.setConcepto( itemCxPContratista.getReferencia() );
                        facturaDetalleCxP.setAuxiliar("" );
                        facturaDetalleCxP.setTipo_referencia_1("" );
                        facturaDetalleCxP.setReferencia_1( "" );
                        facturaDetalleCxP.setTipo_referencia_2( "" );
                        facturaDetalleCxP.setReferencia_2( "" );
                        facturaDetalleCxP.setTipo_referencia_3( "" );
                        facturaDetalleCxP.setReferencia_3( "");



                        comandoSQL = model.consorcioService.setFacturaDetalle(facturaDetalleCxP, usuario, creation_date);
                        comandos_sql.add(comandoSQL);
                    }



                   // PASO 3 GRABAR TODO A LA BASE DE DATOS


                   estado = model.consorcioService.ejecutarBatchSQL(comandos_sql, logWriter, "fintra");

                    if (estado.equalsIgnoreCase("ERROR")){
                        logWriter.log("ERROR: Se genero error al ejecutar el Batch de SQL. "+
                                      "Si el error es de llave duplicada, significa que la factura ya esta registrada en Fintra y no esta marcada como trasladada en el Consorcio. \n" +
                                      "Verifique en Fintra si es la misma factura en cuyo caso marque la factura en consorcio en referencia3, para que error no se repita. \n" +
                                      "Se efectuo rollback. \n" +
                                      "                                            Factura contratista : " + factura_contratista + "\n" ,LogWriter.INFO);
                    }
                    else{
                        // logWriter.log("    Registra en base de datos sin error \n",LogWriter.INFO);
                        totalGrabados++;
                    }

                    // Inicializa los vectores de comandos para una proxima solicitud

                    comandos_sql.clear();


                    // PASO 4 ACTUALIZA LA FACTURA DEL CONTRATISTA EN CONSORCIO PARA MARCARLA COMO TRASLADADA


                     comandoSQL = consorcioDao.setReferencia3(tipo_documento, factura_contratista, dstrct,
                                                              proveedor,tipo_documento, factura_contratista  );
                     comandos_sql.add(comandoSQL);

                     estado = model.consorcioService.ejecutarBatchSQL(comandos_sql, logWriter);
                     if (estado.equalsIgnoreCase("ERROR")){
                         logWriter.log("ERROR: Se genero error al ejecutar el Batch de SQL, al tratar de actualizar la factura en Consorcio con la factura de traslado a Fintra"+
                                       "Se efectuo rollback. \n" +
                                       "                                            Factura contratista : " + factura_contratista + "\n" ,LogWriter.INFO);
                     }




                }

                logWriter.log("Total facturas grabadas : " + totalGrabados + " \n",LogWriter.INFO);
                logWriter.tituloFinal();

            }

            else {
                logWriter.log("ADVERTENCIA: No existen facturas para elaborarles el traslado a Fintra. \n",LogWriter.INFO);
            }

        }
        catch (Exception e){
            Util.imprimirTrace(e);
            logWriter.log("ERROR: Se genero error de Exception al ejecutar el Metodo : generaTrasladoContratista. " +
                          "                                 Mensaje : " + e.getMessage() + "\n" ,LogWriter.INFO) ;
        }


    }








    // *************************************************************************
    // *
    // *  FIN DEL AREA DE METODOS DEL PROCESO DE CONTRATISTA
    // *
    // *************************************************************************








    // *************************************************************************
    // *
    // *  AREA DE METODOS DEL PROCESO DE RECAUDOS
    // *
    // *************************************************************************



     /*public void setInsertarRecaudo( Recaudo recaudo ,String dstrct,  String  usuario , String fechaCreacion, LogWriter logWriter ) throws SQLException {



        consorcioDao.setInsertarRecaudo( recaudo , dstrct,  usuario,  fechaCreacion,  logWriter);


     }*/


     /*public void creaTablaRecaudo( LogWriter logWriter ) throws SQLException {

        consorcioDao.creaTablaRecaudo(logWriter);

     }*/




     /*public ArrayList  getRecaudoProceso(LogWriter logWriter )throws SQLException{

           return  consorcioDao.getRecaudoProceso(logWriter);

     }*/





     /*public void procesarRecaudo(Model model, String login, String fechaCreacion, LogWriter logWriter) {



        // Constantes
        final String  NOMBRE_LIBRO_1 = "Estado Informe_de_Recaudo_MS_Junio_2010";
        final String  NOMBRE_LIBRO_2 = "Cruce Recaudos Electricaribe";
        final String  ETIQUETA_HOJA_1 = "Recaudo_Multiservicios";
        final String  ETIQUETA_HOJA_2 = "Cruce Recaudos";
        final String  ETIQUETA_HOJA_3 = "Recaudos no cruzados";
        final String  ETIQUETA_HOJA_4 = "NC Recaudos no cruzados";
        final String  ETIQUETA_HOJA_5 = "Recaudos menores no cruzados";
        final int     LINEA_TITULO_COLUMNA = 3;




        // -------------------------------------------------------------------------
        // DEFINICION PARA USAR EXCEL
        // Variables del archivo de excel

        AsposeUtil xlsUtil = new AsposeUtil();
        Workbook   wb;
        String     rutaInformes;
        String     nombre;

        int fila  = 0;

        int fila3 = 0;
        // FIN DEFINICION PARA USAR EXCEL
        // -------------------------------------------------------------------------


        try{

            rutaInformes = this.generarRUTA(login);   // crear el directorio donde va a incluirse el archivo excell
            String nombreLibro = xlsUtil.crearNombreLibro(NOMBRE_LIBRO_2, FileFormatType.EXCEL2003, true);
            wb = xlsUtil.crearLibro(nombreLibro);
            inicializarEstilos(wb);

            Worksheet sheet  = inicializarHoja( wb,ETIQUETA_HOJA_2, login, "Cruce Recaudos de Electrificadora", 0);
            Worksheet sheet3 = inicializarHoja( wb,ETIQUETA_HOJA_3, login, "Recaudos no cruzados", 1);
            Worksheet sheet4 = inicializarHoja( wb,ETIQUETA_HOJA_4, login, "NC Recaudos no cruzados", 2);
            Worksheet sheet5 = inicializarHoja( wb,ETIQUETA_HOJA_5, login, "Recaudos menores no cruzados", 3);

            // ENCABEZADO HOJA 1 : titulos de las columnas para la hoja Cruce Recaudos de Electrificadora
            String [] cabecera = {"REFERENCIA 1","SIMBOLO VARIABLE","DOCUMENTO","NOMBRE CLIENTE" ,
                                  "FECHA FACTURA", "FECHA VENCIMIENTO", "VALOR FACTURA", "VALOR ABONO", "VALOR SALDO",
                                  "VALOR INGRESO INICIAL", "VALOR APLICADO", "DIFERENCIA INICIAL", "VALOR SOBRANTE"};

            // ancho de las columnas
            float  [] dimensiones = new float [] {
                15,15, 14,50,   15, 15, 11, 11, 11,    15, 11, 12,13
            };
            // arma los tiulos en el excell
            xlsUtil.generarTitulos(sheet,LINEA_TITULO_COLUMNA,titulo2, cabecera, dimensiones);



            // ENCABEZADO HOJA 2 : titulos de las columnas para la hoja Recaudos no cruzados


            String [] cabecera3 = {"PERIODO RECAUDO","NOM EMPRESA","NOM UNICOM","COD UNICOM" ,
                                  "GESTOR", "NOM CLI", "NIC", "NIS RAD", "NUM ACU",
                                  "SIMBOLO VAR", "F FACT", "F PUESTA", "CO CONCEPTO" ,"DESC CONCEPTO",
                                  "IMP FACTURADO CONCEPTO", "IMP PAGADO CONCEPTO", "IMP RECAUDO"};


            // ancho de las columnas
            float  [] dimensiones3 = new float [] {
                16, 35, 15, 12, 35, 35, 12, 12, 12, 12, 12, 12, 12, 35, 18, 16, 15
            };
            // arma los tiulos en el excell
            xlsUtil.generarTitulos(sheet3,LINEA_TITULO_COLUMNA,titulo2, cabecera3, dimensiones3);


            // ENCABEZADO HOJA 3 : titulos de las columnas para la hoja NC Recaudos no cruzados
            // arma los tiulos en el excell

            xlsUtil.generarTitulos(sheet4,LINEA_TITULO_COLUMNA,titulo2, cabecera3, dimensiones3);


            // ENCABEZADO HOJA 4 : titulos de las columnas para la hoja Recaudos menores no cruzados
            // arma los tiulos en el excell

            xlsUtil.generarTitulos(sheet5,LINEA_TITULO_COLUMNA,titulo2, cabecera3, dimensiones3);


            Style letraColor = letra;
            letraColor = letraAzul;
            Style dineroColor = dinero;
            dineroColor = dineroAzul;

            int COLUMNA_PRIMERA_FACTURA = 12;


            // Seleccionar los recaudos que se procesaran

            ArrayList listaRecaudoProceso = model.consorcioService.getRecaudoProceso(logWriter);

            if (listaRecaudoProceso.isEmpty()) {

                // No hay recaudos para procesar
                logWriter.log("ADVERTENCIA: No existen recaudos para procesar. \n",LogWriter.INFO);
            }
            else {

                // Hay recaudos

                List listaFacturaCabecera =  new LinkedList();

                String condicion = "";
                String ordenamiento = "";
                int encontradas = 0;

                fila = 3;
                fila3 = 3;

                for( int i= 1; i<=listaRecaudoProceso.size() ; i++ ) {

                    RecaudoProceso recaudoProceso = (RecaudoProceso) listaRecaudoProceso.get(i-1);

                    condicion =  " dstrct = 'FINV' and ";
                    condicion += " tipo_documento = 'FAC' and ";
                    condicion += " documento like 'PM%' and " ;
                    condicion += " ref2 = '" + recaudoProceso.getSimbolo_var() + "' and ";
                    condicion += " valor_saldo >= 10 and " ;
                    condicion += " reg_status != 'A' ";

                    ordenamiento  = " order by  ref2, ";
                    ordenamiento += " fecha_vencimiento ";


                    listaFacturaCabecera = consorcioDao.buscaFacturaCabeceraCxC(condicion + ordenamiento , "fintra");

                    if (!listaFacturaCabecera.isEmpty()) {


                        // Se encontraron facturas con simbolo variable que cruzan con el ingreso


                        // Contando las encontradas

                        encontradas++;

                        // Determinando el color a sombrear las lineas del ingreso
                        if (Util.esImpar(encontradas)) {
                            letraColor = letraAzul;
                            dineroColor = dineroAzul;
                        }
                        else {
                            letraColor = letraVerde;
                            dineroColor = dineroVerde;
                        }

                        FacturaCabecera facturaCabecera = new FacturaCabecera();
                        Iterator it = listaFacturaCabecera.iterator();

                        double valorIngreso = recaudoProceso.getImp_recaudo();


                        int itemFactura = 0;
                        int filaPrimeraFactura = 0;


                        while (it.hasNext()  ) {

                           facturaCabecera = (FacturaCabecera)it.next();

                           itemFactura++;

                           String nombreCliente = consorcioDao.getNombreCliente(facturaCabecera.getCodcli());

                           double valorSaldo = facturaCabecera.getValor_saldo();
                           double valorAplicado = 0.00;
                           double diferenciaInicial = 0.00;
                           double valorIngresoInicial = 0.00;

                           if (valorIngreso >= valorSaldo) {
                               valorAplicado = valorSaldo;
                               valorIngreso  = valorIngreso - valorAplicado;
                           }
                           else {
                               valorAplicado = valorIngreso;
                               valorIngreso  = 0.00;
                           }

                           if(itemFactura == 1){
                               diferenciaInicial = recaudoProceso.getImp_recaudo() - facturaCabecera.getValor_saldo();
                               valorIngresoInicial =  recaudoProceso.getImp_recaudo();
                           }
                           else {
                               diferenciaInicial   = 0.00;
                               valorIngresoInicial = 0.00;
                           }


                           if (valorAplicado != 0.00) {

                               fila++;
                               int col = 0;

                               if(itemFactura == 1) {
                                   filaPrimeraFactura = fila;
                               }

                               xlsUtil.setCelda(sheet, fila  , col++ , facturaCabecera.getRef1(), letraColor  );
                               xlsUtil.setCelda(sheet, fila  , col++ , facturaCabecera.getRef2(), letraColor  );
                               xlsUtil.setCelda(sheet, fila  , col++ , facturaCabecera.getDocumento(), letraColor  );
                               xlsUtil.setCelda(sheet, fila  , col++ , nombreCliente, letraColor  );
                               xlsUtil.setCelda(sheet, fila  , col++ , facturaCabecera.getFecha_factura(), letraColor  );
                               xlsUtil.setCelda(sheet, fila  , col++ , facturaCabecera.getFecha_vencimiento(), letraColor );
                               xlsUtil.setCelda(sheet, fila  , col++ , facturaCabecera.getValor_factura(), dineroColor  );
                               xlsUtil.setCelda(sheet, fila  , col++ , facturaCabecera.getValor_abono(), dineroColor  );
                               xlsUtil.setCelda(sheet, fila  , col++ , facturaCabecera.getValor_saldo(), dineroColor  );


                               if(itemFactura == 1) {
                                   xlsUtil.setCelda(sheet, fila  , col++ , valorIngresoInicial, dineroColor  );
                               }
                               else {
                                   xlsUtil.setCelda(sheet, fila  , col++ , "", letraColor  );
                               }

                               xlsUtil.setCelda(sheet, fila  , col++ , valorAplicado, dineroColor  );

                               if(itemFactura == 1) {
                                   xlsUtil.setCelda(sheet, fila  , col++ , diferenciaInicial, dineroColor  );
                               }
                               else {
                                   xlsUtil.setCelda(sheet, fila  , col++ , "", letraColor  );
                               }

                               xlsUtil.setCelda(sheet, fila  , col , "", letraColor  );

                            }

                        } // Final del while que recorre las facturas a aplicar

                        if(valorIngreso != 0) {
                            xlsUtil.setCelda(sheet, filaPrimeraFactura  , COLUMNA_PRIMERA_FACTURA , valorIngreso, dineroColor  );
                        }
                        else {
                             xlsUtil.setCelda(sheet, filaPrimeraFactura  , COLUMNA_PRIMERA_FACTURA , "", letraColor  );
                        }

                    }
                    else {
                        // El ingreso no encontro facturas con la cual cruzar

                        fila3 = this.registrarNoCruce(sheet3, fila3, recaudoProceso.getSimbolo_var(), logWriter);



                    }
                }  // Final del for que recorre los ingresos a procesar


                this.registrarNCRecaudo(sheet4,logWriter);
                this.registrarRecaudosMenores(sheet5,logWriter);


            }  // Final de Hay Recaudos

            // Salvar la hoja electronica

            xlsUtil.cerrarLibro(wb, rutaInformes, nombreLibro, FileFormatType.EXCEL2003);
            wb = null;
            sheet = null;
            sheet3 = null;
            sheet4 = null;
            sheet5 = null;


        }
        catch (Exception e){
            logWriter.log(e.getMessage());
            logWriter.log("ERROR: Se genero error de Exception al ejecutar el Metodo : procesarRecaudo. " +
                          "                                 Mensaje : " + e.getMessage() + "\n" ,LogWriter.INFO) ;
        }
     }*/




     /*public int   registrarNoCruce(Worksheet sheet, int fila,  String simbolo_var, LogWriter logWriter )  {


        ArrayList listaRecaudoDetalle = new ArrayList();


        Style letraColor = letra;
        letraColor = letraAzul;
        Style dineroColor = dinero;
        dineroColor = dineroAzul;


         try {

            String condicion    =  " simbolo_var = '" + simbolo_var + "' ";
            String ordenamiento  = " order by  co_concepto ";

            listaRecaudoDetalle = this.getRecaudoDetalle(condicion + ordenamiento );



            for( int i= 1; i<=listaRecaudoDetalle.size() ; i++ ) {

               Recaudo recaudo = (Recaudo) listaRecaudoDetalle.get(i-1);

               fila++;

               int col = 0;

               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getPeriodo_recaudo(), letraColor  );
               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getNom_empresa(), letraColor  );
               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getNom_unicom(), letraColor  );
               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getCod_unicom(), letraColor  );
               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getGestor(), letraColor  );
               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getNom_cli(), letraColor );
               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getNic(), letraColor  );
               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getNis_rad(), letraColor  );
               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getNum_acu(), dineroSinCentavoAzul  );

               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getSimbolo_var(), letraColor  );
               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getF_fact(), letraColor  );
               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getF_puesta(), letraColor  );
               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getCo_concepto(), letraColor  );
               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getDesc_concepto(), letraColor  );
               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getImp_facturado_concepto(), dineroColor  );
               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getImp_pagado_concepto(), dineroColor  );
               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getImp_recaudo(), dineroColor  );

            }



         }
         catch (Exception e){
             logWriter.log(e.getMessage());
             logWriter.log("ERROR: Se genero error de Exception al ejecutar el Metodo : registrarNoCruce. " +
                          "                                 Mensaje : " + e.getMessage() + "\n" ,LogWriter.INFO) ;
         }

        return fila;

     }*/





     /*public void registrarNCRecaudo(Worksheet sheet, LogWriter logWriter )  {


        ArrayList listaRecaudoDetalle = new ArrayList();


        Style letraColor = letra;
        letraColor = letraAzul;
        Style dineroColor = dinero;
        dineroColor = dineroAzul;
        Style dineroSinCentavoColor = dinero;
        dineroSinCentavoColor = dineroSinCentavoAzul;


        int fila = 3;
        String simboloVarAnterior = "";
        int numeroSimbolo = 0;


         try {


            String condicion    =  " nic in (select distinct  nic  from con.recaudo where imp_recaudo < 0) ";
            String ordenamiento  = " order by nic, simbolo_var, co_concepto ";

            listaRecaudoDetalle = this.getRecaudoDetalle(condicion + ordenamiento );



            for( int i= 1; i<=listaRecaudoDetalle.size() ; i++ ) {

               Recaudo recaudo = (Recaudo) listaRecaudoDetalle.get(i-1);

               if(!recaudo.getSimbolo_var().equalsIgnoreCase(simboloVarAnterior)) {
                   numeroSimbolo++;
                   simboloVarAnterior = recaudo.getSimbolo_var();
               }


               // Determinando el color a sombrear las lineas del ingreso
               if (Util.esImpar(numeroSimbolo)) {
                   letraColor = letraAzul;
                   dineroColor = dineroAzul;
                   dineroSinCentavoColor = dineroSinCentavoAzul;
               }
               else {
                   letraColor = letraVerde;
                   dineroColor = dineroVerde;
                   dineroSinCentavoColor = dineroSinCentavoVerde;
               }

               fila++;

               int col = 0;

               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getPeriodo_recaudo(), letraColor  );
               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getNom_empresa(), letraColor  );
               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getNom_unicom(), letraColor  );
               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getCod_unicom(), letraColor  );
               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getGestor(), letraColor  );
               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getNom_cli(), letraColor );
               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getNic(), letraColor  );
               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getNis_rad(), letraColor  );
               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getNum_acu(), dineroSinCentavoColor  );

               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getSimbolo_var(), letraColor  );
               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getF_fact(), letraColor  );
               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getF_puesta(), letraColor  );
               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getCo_concepto(), letraColor  );
               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getDesc_concepto(), letraColor  );
               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getImp_facturado_concepto(), dineroColor  );
               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getImp_pagado_concepto(), dineroColor  );
               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getImp_recaudo(), dineroColor  );

            }
         }
         catch (Exception e){
             logWriter.log(e.getMessage());
             logWriter.log("ERROR: Se genero error de Exception al ejecutar el Metodo : registrarNoCruce. " +
                          "                                 Mensaje : " + e.getMessage() + "\n" ,LogWriter.INFO) ;
         }
     }*/






     /*public void registrarRecaudosMenores(Worksheet sheet, LogWriter logWriter )  {


        ArrayList listaRecaudoDetalle = new ArrayList();


        Style letraColor = letra;
        letraColor = letraAzul;

        Style dineroColor = dinero;
        dineroColor = dineroAzul;

        Style dineroSinCentavoColor = dinero;
        dineroSinCentavoColor = dineroSinCentavoAzul;

        int fila = 3;
        String simboloVarAnterior = "";
        int numeroSimbolo = 0;


         try {

            String condicion    =  " simbolo_var in  (select simbolo_var  ";
            condicion += " from con.recaudo ";
            condicion += " where nic not in (select distinct  nic  from con.recaudo where imp_recaudo < 0) ";
            condicion += " group by simbolo_var ";
            condicion += " having sum(imp_recaudo) <=  10 and sum(imp_recaudo) !=  0 ) and imp_recaudo != 0 ";

            String ordenamiento  = " order by nic, simbolo_var, co_concepto ";

            listaRecaudoDetalle = this.getRecaudoDetalle(condicion + ordenamiento );

            for( int i= 1; i<=listaRecaudoDetalle.size() ; i++ ) {

               Recaudo recaudo = (Recaudo) listaRecaudoDetalle.get(i-1);

               if(!recaudo.getSimbolo_var().equalsIgnoreCase(simboloVarAnterior)) {
                   numeroSimbolo++;
                   simboloVarAnterior = recaudo.getSimbolo_var();
               }

               // Determinando el color a sombrear las lineas del ingreso
               if (Util.esImpar(numeroSimbolo)) {
                   letraColor = letraAzul;
                   dineroColor = dineroAzul;
                   dineroSinCentavoColor = dineroSinCentavoAzul;
               }
               else {
                   letraColor = letraVerde;
                   dineroColor = dineroVerde;
                   dineroSinCentavoColor = dineroSinCentavoVerde;
               }

               fila++;

               int col = 0;

               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getPeriodo_recaudo(), letraColor  );
               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getNom_empresa(), letraColor  );
               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getNom_unicom(), letraColor  );
               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getCod_unicom(), letraColor  );
               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getGestor(), letraColor  );
               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getNom_cli(), letraColor );
               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getNic(), letraColor  );
               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getNis_rad(), letraColor  );
               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getNum_acu(), dineroSinCentavoColor  );

               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getSimbolo_var(), letraColor  );
               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getF_fact(), letraColor  );
               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getF_puesta(), letraColor  );
               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getCo_concepto(), letraColor  );
               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getDesc_concepto(), letraColor  );
               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getImp_facturado_concepto(), dineroColor  );
               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getImp_pagado_concepto(), dineroColor  );
               xlsUtil.setCelda(sheet, fila  , col++ , recaudo.getImp_recaudo(), dineroColor  );

            }
         }
         catch (Exception e){
             logWriter.log(e.getMessage());
             logWriter.log("ERROR: Se genero error de Exception al ejecutar el Metodo : registrarRecaudosMenores. " +
                          "                                 Mensaje : " + e.getMessage() + "\n" ,LogWriter.INFO) ;
         }
     }*/







    /*public ArrayList  getRecaudoDetalle(String condicion)throws SQLException{

        return consorcioDao.getRecaudoDetalle( condicion);
    }*/






    // *************************************************************************
    // *
    // *  FIN DEL AREA DE METODOS DEL PROCESO DE RECAUDOS
    // *
    // *************************************************************************






    public Tipo_impuesto getTipoImpuesto(String dstrct, String codigo, String fecha, String concepto, String agencia ) throws SQLException {

        return consorcioDao.getTipoImpuesto(dstrct, codigo, fecha, concepto, agencia);

    }



    public double get_dtf_mv (double r) {

        double ipa = (r/100)/4;                            // trimestre anticipado
        double ipv = ipa/(1-ipa);                          // trimestre vencido
        double iev =  Math.pow((1+ipv),4) - 1;             // efectivo anual
        double iea =  Math.pow((1-ipa),-4) - 1;            // efectivo anual
        double rn = 1/12;
        double ip  =  Math.pow((1+ iea),0.083333333) - 1;  // Tasa peri�dica a partir de la tasa efectiva anual

        return ip * 100;
    }


    public double get_anualidad_dtf(double vp , double dtf , double n ) {

        // Sirve para pasar conseguir el valor de una anualidad especificando una DTF + puntos que sera convertida en Mensual Vencida
        // (DTF dada en TA+puntos a una MV)

        // r = anual trimestre anticipado.

        double i = get_dtf_mv(dtf);
        double r = Math.pow((1+(i/100)),n);
        double factor =  (r-1)/(i/100*r);
        double a = vp / factor;

        return a;
    }


    public double get_anualidad(double vp , double i , double n ) {

        // Sirve para pasar conseguir el valor de una anualidad especificando un interes en  Mensual Vencida

        // r = anual trimestre anticipado.

        double r = Math.pow((1+(i/100)),n);
        double factor =  (r-1)/(i/100*r);
        double a = vp / factor;

        return a;
    }



    public PuntosFinanciacion getPuntosFinanciacion(String esquema, String regulacion, double valor, String ano,
                                                    String trimestre, int cuotas,String edificio)throws SQLException{
        return consorcioDao.getPuntosFinanciacion(esquema, regulacion, valor,  ano,
                                               trimestre, cuotas,edificio);
    }



    public String getTrimestre(String mes) {

        int mesNumerico = Integer.parseInt(mes);
        String trimestre   = "1";

        if (mesNumerico <= 3) {
            trimestre = "1";
        }
        else if (mesNumerico <= 6){
            trimestre = "2";
        }
        else if (mesNumerico <= 9){
            trimestre = "3";
        }
        else {
            trimestre = "4";
        }
        return trimestre;
    }







        /**
         * Ubica la informacion de la ruta donde quedara el informe
         * Si no existe crea el directorio
         *
         * @author  Alvaro Pabon Martinez
         * @version %I%, %G%
         * @since   1.0
         *
         */

        public String  generarRUTA( String login) throws Exception{

            String ruta = "";

            try{



                ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
                ruta = rb.getString("ruta") + "/exportar/migracion/" + login;
                File archivo = new File( ruta );
                if (!archivo.exists()) archivo.mkdirs();

            }catch (Exception ex){
                Util.imprimirTrace(ex);
                throw new Exception(ex.getMessage());
            }
            return ruta;

        }





    /**
     * Metodo para definir y etiquetar la hoja de excell
     * @autor apabon
     * @param wb Libro de excell
     * @param etiqueta Nombre para etiquetar la hoja
     * @throws Exception.
     */
    private Worksheet inicializarHoja(Workbook wb, String etiqueta, String login, String titulo, int numeroHoja) throws Exception{
        try{


            Worksheets worksheets = wb.getWorksheets();
            Worksheet sheet = worksheets.addSheet();

            sheet = wb.getWorksheets().getSheet(numeroHoja);


            // sheet = wb.getWorksheets().getActiveSheet() ;

            fmt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            xlsUtil.etiquetarHoja(sheet, etiqueta);
            titularHoja(sheet,titulo, login);

            return sheet;

        }catch (Exception e){
            Util.imprimirTrace(e);
            throw new Exception( "Error en inicializar Hoja ....\n"  + e.getMessage() );
        }


    }


    /**
     * Metodo para crear el  archivo de excel
     * @autor apabon
     * @param sheet  Nombre de la hoja donde queda el informe
     * @param titulo Titulo en la hoja del informe
     * @throws Exception.
     */
    private void titularHoja(Worksheet sheet, String titulo, String login) throws Exception{
        try{
            fmt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

            xlsUtil.combinarCeldas(sheet, 0, 0, 0, 8);
            xlsUtil.setCelda(sheet, 0,0, titulo, header);
            xlsUtil.setCelda(sheet, 1,0, "FECHA" , titulo1);
            xlsUtil.setCelda(sheet, 1,1, fmt.format( new Date())  , titulo1 );
            xlsUtil.setCelda(sheet, 2,0, "USUARIO", titulo1);
            xlsUtil.setCelda(sheet, 2,1, login , titulo1);


        }catch (Exception ex){
            Util.imprimirTrace(ex);
            throw new Exception( "Error en crearArchivo ....\n"  + ex.getMessage() );
        }
    }



    /**
     * Metodo para Incializar los colores propios y los estilos
     * @autor apabon
     * @param wb Libro de excell especifico para el cual se definen los colores y estilos propios
     * @throws Exception.
     */
    private void inicializarEstilos(Workbook wb) throws Exception{
        try{

            // colores
            xlsUtil.crearColor(wb,colorPersonalizado, "AZUL_CLARO_FINTRA", 55, 149,179,215);
            xlsUtil.crearColor(wb,colorPersonalizado, "VERDE_CLARO_FINTRA", 52, 194,214,154);
            xlsUtil.crearColor(wb,colorPersonalizado, "VERDE_FINTRA", 53, 51,153,102);
            xlsUtil.crearColor(wb,colorPersonalizado, "MARRON_CLARO_FINTRA", 51, 221,217,195);
            xlsUtil.crearColor(wb,colorPersonalizado, "GRIS", 54, 192,192,192);
            xlsUtil.crearColor(wb,colorPersonalizado, "NONE", 50, 0,0,0);
            xlsUtil.crearColor(wb,colorPersonalizado, "MEDIO", 49, 127,127,127);
            xlsUtil.crearColor(wb,colorPersonalizado, "AZUL_FORMULA", 48, 23,55,93);

            // estilos

            header  = xlsUtil.crearEstilo(wb, "Tahoma", 10, true, false, 0, "49",  com.aspose.cells.Color.GREEN, false,xlsUtil.getColorPersonalizado(wb, colorPersonalizado, "NONE"), TextAlignmentType.LEFT);
            titulo1 = xlsUtil.crearEstilo(wb, "Tahoma", 8, true, false,  0, "49", com.aspose.cells.Color.BLACK, false,xlsUtil.getColorPersonalizado(wb, colorPersonalizado, "NONE"), TextAlignmentType.LEFT);
            titulo2 = xlsUtil.crearEstilo(wb, "Calibri", 8, true, false,  0,"49", com.aspose.cells.Color.WHITE, true,xlsUtil.getColorPersonalizado(wb, colorPersonalizado, "VERDE_FINTRA"), TextAlignmentType.CENTER);
            titulo3 = xlsUtil.crearEstilo(wb, "Tahoma", 8, true, false,  0,"49", com.aspose.cells.Color.WHITE, true,xlsUtil.getColorPersonalizado(wb, colorPersonalizado, "NONE"), TextAlignmentType.CENTER);
            titulo4 = xlsUtil.crearEstilo(wb, "Tahoma", 8, true, false,  0,"49", com.aspose.cells.Color.WHITE, true,com.aspose.cells.Color.OLIVE, TextAlignmentType.CENTER);
            titulo5 = xlsUtil.crearEstilo(wb, "Tahoma", 14, true, false,  0,"49", com.aspose.cells.Color.BLACK, false,xlsUtil.getColorPersonalizado(wb, colorPersonalizado, "NONE"), TextAlignmentType.LEFT);

            letra   = xlsUtil.crearEstilo(wb, "Calibri", 8, false, false, 0, "49", com.aspose.cells.Color.BLACK, false, com.aspose.cells.Color.OLIVE , TextAlignmentType.LEFT);

            letraMarron = xlsUtil.crearEstilo(wb, "Calibri", 8, false, false,  0,"49", com.aspose.cells.Color.BLACK,  true, xlsUtil.getColorPersonalizado(wb, colorPersonalizado, "MARRON_CLARO_FINTRA"), TextAlignmentType.LEFT);
            letraAzul   = xlsUtil.crearEstilo(wb, "Calibri", 8, false, false,  0,"49", com.aspose.cells.Color.BLACK,  true, xlsUtil.getColorPersonalizado(wb, colorPersonalizado, "AZUL_CLARO_FINTRA"),   TextAlignmentType.LEFT);
            letraVerde  = xlsUtil.crearEstilo(wb, "Calibri", 8, false, false,  0,"49", com.aspose.cells.Color.BLACK,  true, xlsUtil.getColorPersonalizado(wb, colorPersonalizado, "VERDE_CLARO_FINTRA"),  TextAlignmentType.LEFT);

            letraCentrada    = xlsUtil.crearEstilo(wb, "Tahoma", 8, false, false,  0,"49", com.aspose.cells.Color.BLACK, false,xlsUtil.getColorPersonalizado(wb, colorPersonalizado, "NONE"), TextAlignmentType.CENTER);
            numero           = xlsUtil.crearEstilo(wb, "Tahoma", 8, false, false,  0,"#", com.aspose.cells.Color.BLACK, false,xlsUtil.getColorPersonalizado(wb, colorPersonalizado, "NONE"), TextAlignmentType.RIGHT);
            numeroCentrado   = xlsUtil.crearEstilo(wb, "Tahoma", 8, false, false,  0,"#", com.aspose.cells.Color.BLACK, false,xlsUtil.getColorPersonalizado(wb, colorPersonalizado, "NONE"), TextAlignmentType.CENTER);
            dinero           = xlsUtil.crearEstilo(wb, "Calibri", 8, false, false,  0,"#,##0.00", com.aspose.cells.Color.BLACK, false,xlsUtil.getColorPersonalizado(wb, colorPersonalizado, "NONE"), TextAlignmentType.RIGHT);

            dineroMarron     = xlsUtil.crearEstilo(wb, "Calibri", 8, false, false,  0,"#,##0.00", com.aspose.cells.Color.BLACK, true,xlsUtil.getColorPersonalizado(wb, colorPersonalizado, "MARRON_CLARO_FINTRA"), TextAlignmentType.RIGHT);
            dineroAzul       = xlsUtil.crearEstilo(wb, "Calibri", 8, false, false,  0,"#,##0.00", com.aspose.cells.Color.BLACK, true,xlsUtil.getColorPersonalizado(wb, colorPersonalizado, "AZUL_CLARO_FINTRA"), TextAlignmentType.RIGHT);
            dineroVerde      = xlsUtil.crearEstilo(wb, "Calibri", 8, false, false,  0,"#,##0.00", com.aspose.cells.Color.BLACK, true,xlsUtil.getColorPersonalizado(wb, colorPersonalizado, "VERDE_CLARO_FINTRA"), TextAlignmentType.RIGHT);

            dineroSinCentavo = xlsUtil.crearEstilo(wb, "Calibri", 8, false, false,  0,"#,##0", com.aspose.cells.Color.BLACK, false,xlsUtil.getColorPersonalizado(wb, colorPersonalizado, "MARRON_CLARO_FINTRA"), TextAlignmentType.RIGHT);

            dineroSinCentavoMarron = xlsUtil.crearEstilo(wb, "Calibri", 8, false, false,  0,"#,##0", com.aspose.cells.Color.BLACK, true,xlsUtil.getColorPersonalizado(wb, colorPersonalizado, "MARRON_CLARO_FINTRA"), TextAlignmentType.RIGHT);
            dineroSinCentavoAzul   = xlsUtil.crearEstilo(wb, "Calibri", 8, false, false,  0,"#,##0", com.aspose.cells.Color.BLACK, true,xlsUtil.getColorPersonalizado(wb, colorPersonalizado, "AZUL_CLARO_FINTRA"),   TextAlignmentType.RIGHT);
            dineroSinCentavoVerde  = xlsUtil.crearEstilo(wb, "Calibri", 8, false, false,  0,"#,##0", com.aspose.cells.Color.BLACK, true,xlsUtil.getColorPersonalizado(wb, colorPersonalizado, "VERDE_CLARO_FINTRA"),  TextAlignmentType.RIGHT);

            idMarron  = xlsUtil.crearEstilo(wb, "Calibri", 8, false, false,  0,"###0.00_____)", com.aspose.cells.Color.GRAY, false,xlsUtil.getColorPersonalizado(wb, colorPersonalizado, "MARRON_CLARO_FINTRA"), TextAlignmentType.RIGHT);
            idAzul    = xlsUtil.crearEstilo(wb, "Calibri", 8, false, false,  0,"###0.00_____)", com.aspose.cells.Color.GRAY, false,xlsUtil.getColorPersonalizado(wb, colorPersonalizado, "AZUL_CLARO_FINTRA"), TextAlignmentType.RIGHT);
            idVerde   = xlsUtil.crearEstilo(wb, "Calibri", 8, false, false,  0,"###0.00_____)", com.aspose.cells.Color.GRAY, false,xlsUtil.getColorPersonalizado(wb, colorPersonalizado, "VERDE_CLARO_FINTRA"), TextAlignmentType.RIGHT);
            idLetra   = xlsUtil.crearEstilo(wb, "Calibri", 8, false, false,  0,"###0.00_____)", com.aspose.cells.Color.GRAY, false,xlsUtil.getColorPersonalizado(wb, colorPersonalizado, "MARRON_CLARO_FINTRA"), TextAlignmentType.RIGHT);

            gtLetra             = xlsUtil.crearEstilo(wb, "Calibri", 8, true, false,  0,"49", com.aspose.cells.Color.WHITE, true,xlsUtil.getColorPersonalizado(wb, colorPersonalizado, "MEDIO"), TextAlignmentType.LEFT);
            gtDineroSinCentavo  = xlsUtil.crearEstilo(wb, "Calibri", 8, true, false, 0, "#,##0", com.aspose.cells.Color.WHITE, true,xlsUtil.getColorPersonalizado(wb, colorPersonalizado, "MEDIO"),  TextAlignmentType.RIGHT);
            gtId                = xlsUtil.crearEstilo(wb, "Calibri", 8, true, false, 0, "###0.00_____)", com.aspose.cells.Color.GRAY, false,xlsUtil.getColorPersonalizado(wb, colorPersonalizado, "MEDIO"),  TextAlignmentType.RIGHT);

            fLetra              = xlsUtil.crearEstilo(wb, "Calibri", 8, true, false, 0, "49", com.aspose.cells.Color.WHITE, true,xlsUtil.getColorPersonalizado(wb, colorPersonalizado, "AZUL_FORMULA"), TextAlignmentType.LEFT);
            fDineroSinCentavo   = xlsUtil.crearEstilo(wb, "Calibri", 8, true, false, 0, "#,##0", com.aspose.cells.Color.WHITE, true,xlsUtil.getColorPersonalizado(wb, colorPersonalizado, "AZUL_FORMULA"),  TextAlignmentType.RIGHT);
            fId                 = xlsUtil.crearEstilo(wb, "Calibri", 8, true, false, 0, "###0.00_____)", com.aspose.cells.Color.GRAY, false,xlsUtil.getColorPersonalizado(wb, colorPersonalizado, "AZUL_FORMULA"),  TextAlignmentType.RIGHT);
            T1Letra             = xlsUtil.crearEstilo(wb, "Calibri", 8, true, false, 1, "49", com.aspose.cells.Color.BLACK, false,xlsUtil.getColorPersonalizado(wb, colorPersonalizado, "AZUL_FORMULA"), TextAlignmentType.LEFT);
            numeroNegrita  = xlsUtil.crearEstilo(wb, "Tahoma", 8, true, false, 0, "#", com.aspose.cells.Color.BLACK, false,xlsUtil.getColorPersonalizado(wb, colorPersonalizado, "NONE"), TextAlignmentType.RIGHT);
            porcentaje     = xlsUtil.crearEstilo(wb, "Tahoma", 8, false, false, 0, "0.00%", com.aspose.cells.Color.BLACK, false,xlsUtil.getColorPersonalizado(wb, colorPersonalizado, "NONE"), TextAlignmentType.RIGHT);
            ftofecha       = xlsUtil.crearEstilo(wb, "Tahoma", 8, false, false, 0, "yyyy-mm-dd", com.aspose.cells.Color.BLACK, false,xlsUtil.getColorPersonalizado(wb, colorPersonalizado, "NONE"), TextAlignmentType.CENTER);

        }catch (Exception e){
            Util.imprimirTrace(e);
            throw new Exception("Error en Inicializar Estilos .... \n" + e.getMessage());
        }

   }


    
    // 2012-03-14
    /* *********************************************************************************************************************************************/
    /* INICIO REFINANCIACION */ 
    /* *********************************************************************************************************************************************/


    
    
    
    public double getIvaNM(String documentoNM) throws SQLException  {
        
        double valorIva = 0.00; 
        
        String documentoPM = "PM"+ documentoNM.substring(2);
        
        valorIva = consorcioDao.getIvaCF(documentoPM);
        if (valorIva < 0.00) {
            valorIva =  consorcioDao.getIvaNM(documentoNM);
        }
                
        return valorIva;
    }
        
    
    
    
    
    

    /**
     * Retorna un objeto con la informacion de la refinanciacin de PM
     */
    
    public FinanciacionPM getFinanciacionPM() {
        return consorcioDao.getFinanciacionPM();
    }


    public void inicializarFinanciacionPM() {
        consorcioDao.inicializarFinanciacionPM();
    }
    

   

    

    /**
     * Extrae una lista de las facturas PM de un cliente que no esten canceladas
     * @param idCliente Codigo del cliente
     * @throws SQLException
     */
    public void buscaFacturaCliente(String idCliente) throws SQLException {     //20101115

        consorcioDao.buscaFacturaCliente(idCliente);
    }


    

    /**
     * Extrae una lista de las facturas PM de un multiservicio que no esten canceladas
     * @param idMultiservicio Numero del multiservicio
     * @throws SQLException
     */
    public void buscaFacturaClienteMS(String idMultiservicio) throws SQLException {     //20101115

        consorcioDao.buscaFacturaClienteMS(idMultiservicio);
    }
    
    
    
    
    /**
     *
     * @return Una lista de facturas del cliente con saldo para refinanciar
     * @see buscaFacturaCliente
     */
    public List getFacturaCliente() {                                           //20101115
        return consorcioDao.getFacturaCliente();
    }




    /**
     *
     * @return Una lista de objetos FacturaPM de la tabla Factura
     * @see buscaFacturaCabeceraCxC
     * @see FacturaPM.java
     */
    public List getFacturaPM() {                                                //20101115
        return consorcioDao.getFacturaPM();
    }






    /**
     * Extrae el nombre del cliente
     * @param codigoCliente
     * @return String del nombre del cliente
     * @throws SQLException
     */
    public String getNombreCliente(String codigoCliente )throws SQLException{   // 20101115
        return consorcioDao.getNombreCliente(codigoCliente);
    }





    
    
    public String crearIngresoCabecera( String reg_status, String dstrct, String tipo_documento,String  num_ingreso, String codcli, String nitcli,
                                        String concepto, String tipo_ingreso, String fecha_consignacion, String fecha_ingreso, String branch_code,
                                        String bank_account_no, String codmoneda, String agencia_ingreso, String descripcion_ingreso,
                                        String periodo, double vlr_ingreso, double vlr_ingreso_me, double vlr_tasa, String fecha_tasa, int cant_item,
                                        int transaccion, int transaccion_anulacion, String fecha_impresion, String fecha_contabilizacion,
                                        String fecha_anulacion_contabilizacion, String fecha_anulacion, String creation_user,
                                        String creation_date, String user_update, String last_update, String base, String nro_consignacion,
                                        String periodo_anulacion, String cuenta, String auxiliar, String abc, double tasa_dol_bol, double saldo_ingreso,
                                        String cmc, String corficolombiana, String fec_envio_fiducia, String tipo_referencia_1, String referencia_1,
                                        String tipo_referencia_2, String referencia_2,String  tipo_referencia_3,String  referencia_3,  LogWriter logWriter)throws SQLException{   
        
        
        return consorcioDao.crearIngresoCabecera(reg_status, dstrct, tipo_documento, num_ingreso, codcli, nitcli,
                                                 concepto, tipo_ingreso, fecha_consignacion, fecha_ingreso, branch_code,
                                                 bank_account_no, codmoneda, agencia_ingreso, descripcion_ingreso,
                                                 periodo, vlr_ingreso, vlr_ingreso_me, vlr_tasa, fecha_tasa, cant_item,
                                                 transaccion, transaccion_anulacion, fecha_impresion, fecha_contabilizacion,
                                                 fecha_anulacion_contabilizacion, fecha_anulacion, creation_user,
                                                 creation_date, user_update, last_update, base, nro_consignacion,
                                                 periodo_anulacion, cuenta, auxiliar, abc, tasa_dol_bol, saldo_ingreso,
                                                 cmc, corficolombiana, fec_envio_fiducia, tipo_referencia_1, referencia_1,
                                                 tipo_referencia_2, referencia_2, tipo_referencia_3, referencia_3, logWriter );

    }    
    
    
    
    public String crearIngresoDetalle( String reg_status, String dstrct, String tipo_documento, String num_ingreso, int item, String nitcli,
                                        double valor_ingreso, double valor_ingreso_me, String factura, String fecha_factura, String codigo_retefuente,
                                        double valor_retefuente, double valor_retefuente_me, String tipo_doc, String documento, String  codigo_reteica,
                                        double valor_reteica, double valor_reteica_me, double valor_diferencia_tasa, String creation_user,
                                        String creation_date, String user_update, String last_update, String base, String cuenta, String auxiliar,
                                        String fecha_contabilizacion, String fecha_anulacion_contabilizacion, String periodo,
                                        String fecha_anulacion, String periodo_anulacion, int transaccion, int transaccion_anulacion,
                                        String descripcion, double valor_tasa, double saldo_factura, String procesado,  String ref1,
                                        String tipo_referencia_1, String referencia_1, String tipo_referencia_2,String  referencia_2,
                                        String tipo_referencia_3, String referencia_3, LogWriter logWriter)throws SQLException{   // 20101115
    

        
        return consorcioDao.crearIngresoDetalle (reg_status, dstrct, tipo_documento, num_ingreso, item, nitcli,
                                                 valor_ingreso, valor_ingreso_me, factura, fecha_factura, codigo_retefuente,
                                                 valor_retefuente, valor_retefuente_me, tipo_doc, documento, codigo_reteica,
                                                 valor_reteica, valor_reteica_me, valor_diferencia_tasa, creation_user,
                                                 creation_date, user_update, last_update, base, cuenta, auxiliar,
                                                 fecha_contabilizacion, fecha_anulacion_contabilizacion, periodo,
                                                 fecha_anulacion, periodo_anulacion, transaccion, transaccion_anulacion,
                                                 descripcion, valor_tasa, saldo_factura, procesado, ref1,
                                                 tipo_referencia_1, referencia_1, tipo_referencia_2, referencia_2,
                                                 tipo_referencia_3, referencia_3,logWriter);
        
        
        
    }
        
        
        
    public String  actualizarCabeceraCxC(String factura, double valor_ajuste )throws SQLException {
        
       return  consorcioDao.actualizarCabeceraCxC(factura, valor_ajuste);
    }
    
    
    
    public double getTasaMaxima(String periodo)throws SQLException{
         return  consorcioDao.getTasaMaxima(periodo);
    }  
    
    
    
    
    public double getValorIPM( String comprobanteIPM )  throws SQLException{
         return  consorcioDao.getValorIPM(comprobanteIPM);
    }  
                   
    
    
    
       
    public String getCuentaCabeceraPM( String pm )  throws SQLException{
         return  consorcioDao.getCuentaCabeceraPM( pm );
    }  
     
    
    
    
    
    
    
    
    
    
    
    
    
    
    

    /* *********************************************************************************************************************************************/
    /* FINAL REFINANCIACION */ 
    /* *********************************************************************************************************************************************/

            

    
    
    
    
    
    


}

