/*
 * NegociosApplusService.java
 * Created on 2 de febrero de 2009, 17:33
 */
package com.tsp.opav.model.services;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.tsp.opav.model.DAOS.NegociosApplusDAO;
import java.util.ArrayList;
import java.util.List;
import java.io.ByteArrayInputStream;
import java.util.Dictionary;
import com.tsp.util.Utility;
import java.util.ResourceBundle;
import com.tsp.operation.model.beans.Imagen;
import com.tsp.operation.model.beans.Accord;
import java.util.TreeMap;
import com.tsp.opav.model.beans.NegocioApplus;
import com.tsp.opav.model.beans.Material;
import com.tsp.operation.model.beans.BeanGeneral;//20100219
/**
 * @author  Fintra
 */
public class NegociosApplusService {
    /** Creates a new instance of NegociosApplusService */
    private ArrayList lista;
    NegociosApplusDAO negociosApplusDAO;
    private List      archivitos;
    private BeanGeneral data_recep_obra;//20100219

    public NegociosApplusService() {
        negociosApplusDAO= new NegociosApplusDAO();
    }
    public NegociosApplusService(String dataBaseName) {
        negociosApplusDAO= new NegociosApplusDAO(dataBaseName);
    }

    public ArrayList getNegociosApplus(String estado,String contratista,String numosxi,String factconformed,String loginx) throws Exception{
                     //ystem.out.println("estado en service"+estado);
        return negociosApplusDAO.getNegociosApplus(estado,contratista,numosxi,factconformed,loginx);
    }
    public String CambiarEstado(String[] ofertas,String nuevoestado,String observacion,String esquema_comision,String svx,String contratista_consultar,String fact_conformed,String cuoticas,String fecfaccli,String fact_conformada_consultar,String esquema_financiacion,String userx) throws Exception{
        return negociosApplusDAO.CambiarEstado(ofertas ,nuevoestado,observacion,esquema_comision,svx,contratista_consultar,fact_conformed,cuoticas,fecfaccli,fact_conformada_consultar,esquema_financiacion,userx);
    }

    public ArrayList getEstadosApplus() throws Exception{
        return negociosApplusDAO.getEstadosApplus();
    }

    public void insertImagen( ByteArrayInputStream bfin, int longitud,  Dictionary fields,String idxx4,String tipito) throws Exception{
        try{
            this.negociosApplusDAO.insertImagen(bfin,longitud,fields,idxx4,tipito);
        }catch(Exception e){  throw new Exception( e.getMessage()); }
    }

    public String[] getArchivo(String numosx, String loginx,String tipito)
    {
        String respuesta[]=new String[0] ;
        //respuesta[0]="";
        try
        {
            //ImagenService imagenSvc = new ImagenService();
            java.util.List lista= searchArchivos(numosx, loginx,tipito);

            //ystem.out.println("lista"+lista);
            Imagen img;
            if(lista != null && lista.size() > 0){
                respuesta=new String[lista.size()];
                for (int k=0;k<lista.size();k++){
                    img = (Imagen)lista.get(k);
                    respuesta[k] = img.getActividad()+"__"+img.getNameImagen();
                }
            }
            img = null;
            lista = null;

        }
        catch(Exception e)
        {
            System.out.println("errorrr::"+e.toString()+"__"+e.getMessage());
        }
        return respuesta;
    }


    public List getNombresArchivos(String numosx,String loginx,String tipito)
    {
       java.util.List lista =null;
        try
        {
            //ImagenService imagenSvc = new ImagenService();
            lista= negociosApplusDAO.searchNombresArchivos(numosx, loginx,tipito);
            //ystem.out.println("lista"+lista);

        }
        catch(Exception e)
        {
            System.out.println("errorrr::"+e.toString()+"__"+e.getMessage());
        }
        return lista;
    }

    public ArrayList getContratistas()
    {
       java.util.ArrayList lista =null;
        try
        {
            //ImagenService imagenSvc = new ImagenService();
            lista= negociosApplusDAO.getContratistas();
            //ystem.out.println("lista"+lista);

        }
        catch(Exception e)
        {
            System.out.println("errorrr::"+e.toString()+"__"+e.getMessage());
        }
        return lista;
    }


    public List searchArchivos(String documento        ,String loginx,String tipito) throws Exception{
        try{
             ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
             String ruta  = rb.getString("ruta")+ "/images/multiservicios/"+loginx+"/";
             //ystem.out.println("en service doc:"+documento+" y ruta:"+ruta);
             return  this.negociosApplusDAO.searcArchivos(documento,(""+ruta ),tipito);
        }catch(Exception e){ throw new Exception( e.getMessage());}
    }


    public String[] getExPrefacturaEca(String id_orde)  throws Exception{
        return this.negociosApplusDAO.getExPrefacturaEca(id_orde);

    }

    public String[] getExPrefacturaContratista(String id_orde,String id_accio)  throws Exception{
        return this.negociosApplusDAO.getExPrefacturaContratista(id_orde,id_accio);

    }

    public String getEsquemaComision (String id_orden) throws Exception{
        return this.negociosApplusDAO.getEsquemaComision(id_orden);
    }

    public List  obtainCxpsContratista()throws Exception{
        return negociosApplusDAO.obtainCxpsContratista();
    }

    public String updateAccord (String factura_contratista,String factura_formula_prov) throws Exception{
        return this.negociosApplusDAO.updateAccord(factura_contratista, factura_formula_prov);
    }

    public ArrayList obtainAccords(String[] negocios) throws Exception{
        return this.negociosApplusDAO.obtainAccords(negocios);
    }

    public String validarCambioEstado(String[] acciones,String nuevo_estado) throws Exception{
        return this.negociosApplusDAO.validarCambioEstado(acciones,nuevo_estado);
    }

    public ArrayList getEstadosApplusUser(String userx) throws Exception{
        return negociosApplusDAO.getEstadosApplusUser(userx);
    }

    public ArrayList getNegociosApplus2(String estado,String contratista,String numosxi,String factconformed,String loginx,String id_solici,
            String nicc ,String nomclie,String id_cliente,String id_conse, String interventor) throws Exception{
        return negociosApplusDAO.getNegociosApplus2(estado,contratista,numosxi,factconformed,loginx,id_solici,nicc ,nomclie, id_cliente,id_conse, interventor);//------INTERVENTOR JJCASTRO
    }

    public ArrayList getEstadosApplus2() throws Exception{
        return negociosApplusDAO.getEstadosApplus2();
    }

    public NegocioApplus obtainAccione(String solicitud_consultable,String id_accion,String loginx) throws Exception{
        return negociosApplusDAO.obtainAccione(solicitud_consultable,id_accion,loginx);
    }
    public java.util.TreeMap getClientesEca()throws Exception    {
        return  negociosApplusDAO.getClientesEca();
    }

    public String getClientesEca(String cl,String nomselect,String def)throws Exception    {
        return  negociosApplusDAO.getClientesEca(cl, nomselect,def);
    }

    public String getNomClientepadre(String cl)throws Exception    {
    return  negociosApplusDAO.getNomClientepadre(cl);
    }

    public String marcarTipoTrabajo(String solicitud_consultable,String id_accion,String loginx,String tipotraba,String observaci,String extipotraba,String fecvisitaplan,String fecvisitareal,String estadinho) throws Exception{//20100513
        return this.negociosApplusDAO.marcarTipoTrabajo(solicitud_consultable,id_accion,loginx,tipotraba,observaci,extipotraba,fecvisitaplan,fecvisitareal,estadinho);//20100513
    }

    public String asignar(String loginx, String fecof,String solicitudes[]) throws Exception{
        return this.negociosApplusDAO.asignar(loginx,fecof,solicitudes);
    }

    public String validacionSeguridadSolicitud(String id_solicitudx,String id_accionx,String loginxx,String pasoadar){
        return this.negociosApplusDAO.validacionSeguridadSolicitud( id_solicitudx, id_accionx, loginxx, pasoadar);
    }

    public String getIdClie(String nic) throws Exception{
        return this.negociosApplusDAO.getIdClie( nic );
    }

    public NegocioApplus obtainAlcanc(String solicitud_consultable,String id_accion,String loginx) throws Exception{//091016
        return negociosApplusDAO.obtainAlcanc(solicitud_consultable,id_accion,loginx);
    }

    public String marcarAvance(String solicitud_consultable,String id_accion,String loginx,String alcanci,String adicioni,String trabaji)  throws Exception{
        return negociosApplusDAO.marcarAvance(solicitud_consultable,id_accion,loginx,alcanci,adicioni,trabaji);
    }

    public String modificarProducto(String reg_status, String exproducto,String consec,String descr,double pre,String tipo,String medida,String loginx,int cat, int subcat, int tiposubcat, String empaque, String alcance, String  certificado, String ente_certificador,double pre_compra)  throws Exception{
        return negociosApplusDAO.modificarProducto(reg_status, exproducto, consec, descr, pre, tipo, medida, loginx, cat, subcat, tiposubcat, empaque, alcance, certificado, ente_certificador,pre_compra);//091222
    }

    public Material getMaterial(String pk_material) throws Exception{
        return negociosApplusDAO.getMaterial(pk_material);
    }

    public String asignarEstado(String loginx, String estado_asignable,String acciones[]) throws Exception{
        return this.negociosApplusDAO.asignarEstado(loginx,estado_asignable,acciones);
    }

    public BeanGeneral obtainDatRecepObra(String solicitud) throws Exception{
        data_recep_obra=this.negociosApplusDAO.getDatRecepObra(solicitud);;
        return data_recep_obra;
    }

    public BeanGeneral getDatRecepObra() {
        return data_recep_obra;
    }

    /**
 *
 * @param solicitud
 * @param observaciones
 * @param acciones
 * @param fecFin
 * @param fecInterventor
 * @param loginx
 * @return
 * @throws Exception
 * Modificado: Jcastro 28/06/2010
 */
    public String aceptarRecepObra(String solicitud,String[] observaciones,String[] acciones,String[] fecFin,String[] fecInterventor,String loginx, String [] fecDocumentacion, String[] liquidacion, String[] informe, String[] acta, String opcion) throws Exception{
        return this.negociosApplusDAO.aceptarRecepObra(solicitud,observaciones,acciones,fecFin,fecInterventor,loginx, fecDocumentacion, liquidacion, informe, acta, opcion);
    }

    /**
    * metodo para obtener el listado de los clientes con el nic asociado
     * @param cl filtro de busqueda
     * @param nomselect  nombre del objeto html
     * @param def definicion
     * @author MGarizao - GEOTECH
     * @date 07/04/2010
     * @version 1.0
     * @return cadena con las descripcion de un objeto select
     * @throws Exception
     */
     public String getClientesEca_nics(String cl,String nomselect,String def)throws Exception    {
        return  negociosApplusDAO.getClientesEca_nics(cl, nomselect,def);
    }


    public void setLista(ArrayList array){
        lista = array;
    }

    public ArrayList getLista(){
        return lista;
    }

    public void consultarSolicitudes(String estado, String vencido, String solicitud, String cliente, String ejecutivo, String contratista, String departamento, String consecutivo, String nic, String fechaInicial, String fechaFinal, String tipo_solicitud, String responsable) throws Exception {
        setLista(negociosApplusDAO.consultarSolicitudes(estado, vencido, solicitud, cliente, ejecutivo, contratista, departamento, consecutivo, nic, fechaInicial, fechaFinal, tipo_solicitud, responsable));
    }


    /**
     * Obtiene los dias de vencimiento de las solicitudes de la tablagen venci_sol
     * @return TreeMap con los dias de vencimiento
     * @throws SQLException
     */
    public TreeMap<String,Integer> obtenerVencimientoSolicitudes() throws Exception{
        return negociosApplusDAO.obtenerVencimientoSolicitudes();
    }

    /**
     * Obtiene la lista de tipos de trabajo
     * @return ArrayList con los tipos de trabajo
     * @throws SQLException
     */
    public ArrayList listarTiposTrabajo() throws Exception{
        return negociosApplusDAO.listarTiposTrabajo();
    }

    /**
     * Comunica el action con el dao para actualizar los registros.
     * Le asigna un numero de factura conformada a un grupo de acciones
     * @param acciones String de acciones separadas por la cadena ;_;
     * <br>ej: "12003;_;85221;_;95523"
     * @param factura La factura a asignar
     * @return Cadena con un mensaje de confirmacion de la actualizacion o reportando que no fue posible
     * @throws Exception cuando ocurre un error
     */
    public String facturaConformada(String acciones,String factura,String login, String fecfactconf) throws Exception{//20100825
        String resp = "Datos ingresados correctamente";
        try {
            resp = this.negociosApplusDAO.facturaConformada(acciones, factura,login,fecfactconf);//20100825
        }
        catch (Exception e) {
            resp = "No fue posible ingresar los datos ... ocurrio un error!";
            System.out.println("Error al asignar facturas conformadas: "+e.toString());
        }
        return resp;
    }

    /**
     * Obtiene el listado de acciones de una solicitud
     * @param id_solicitud
     * @return ArrayList de beans NegocioApplus
     * @throws Exception
     */
    public ArrayList listarAccionesSolicitud(String id_solicitud) throws Exception{
        return negociosApplusDAO.listarAccionesSolicitud(id_solicitud);
    }

        /**
 *
 * @param cod_material
 * @return
 * @throws Exception
 * MATERIAL JOSE CASTRO
 */
    public String getId(String cod_material)  throws Exception{
      return this.negociosApplusDAO.getId(cod_material);
    }

    public void consultarSolicitudesInforme(String estado, String vencido, String solicitud, String cliente, String ejecutivo, String contratista, String departamento, String consecutivo, String nic, String fechaInicial, String fechaFinal, String tipo_solicitud, String responsable) throws Exception {
        setLista(negociosApplusDAO.consultarSolicitudesInforme(estado, vencido, solicitud, cliente, ejecutivo, contratista, departamento, consecutivo, nic, fechaInicial, fechaFinal, tipo_solicitud, responsable));
    }
    public ArrayList getNegociosApplusEquipos(String estado, String contratista, String numosxi, String factconformed, String loginx, String id_solici,
            String nicc, String nomclie, String id_cliente, String id_conse, String interventor) throws Exception {
        return negociosApplusDAO.getNegociosApplusEquipos(estado, contratista, numosxi, factconformed, loginx, id_solici, nicc, nomclie, id_cliente, id_conse, interventor);
 }

 public String getDescTipoSolc(String id_solicitud) throws Exception {
	return negociosApplusDAO.getDescTipoSolc(id_solicitud);
 }
 
 /**
     * Genera una listado por referencia de la tabla especificada
     * @return lista con los datos encontrados
     * @throws Exception cuando hay error
     */
    public ArrayList busquedaGeneralRef(String tabletype, String referencia) throws Exception {
         return negociosApplusDAO.busquedaGeneralRef(tabletype, referencia);
    }


     /**
     * Busca dato en tablagen
     * @param dato table_type a buscar
     * @return listado con datos coincidentes
     * @throws Exception cuando hay error
     */
    public ArrayList busquedaGeneral(String dato) throws Exception {
        return negociosApplusDAO.busquedaGeneral(dato);
    }

    public ArrayList getEstadosApplusAAAE() throws Exception{
        return negociosApplusDAO.getEstadosApplusAAAE();
    }
    
    public String insertarEncuesta(JsonObject informacion) {
        return negociosApplusDAO.insertarEncuesta(informacion);
    }
    
    public String buscarEncuesta(String solicitud) {
        return (new Gson()).toJson(negociosApplusDAO.buscarEncuesta(solicitud));
    }
    
    public boolean existeEncuesta(String solicitud) {
        JsonObject jo = negociosApplusDAO.buscarEncuesta(solicitud);
        return (!jo.get("creation_user").getAsString().equalsIgnoreCase(""));
    }
}