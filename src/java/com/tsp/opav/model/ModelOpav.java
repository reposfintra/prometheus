package com.tsp.opav.model;
import com.tsp.opav.model.services.*;



public class ModelOpav {
    public transient ClientesVerService ClientesVerService;
    public transient  NegociosApplusService  NegociosApplusService;
    public transient ElectricaribeOfertaService ElectricaribeOfertaSvc;
    public transient ElectricaribeOfertaService_anterior electricaribeOfertaSvc_ant;
    public transient ElectricaribeOtService ElectricaribeOtSvc;
    public transient ElectricaribeVerService electricaribeVerSvc;
    public transient SeguimientoEjecucionService seguimientoEjecucionService;
    public transient  ApplusService  applusService;
    public transient  ConsorcioService  consorcioService;
    public transient  ConstanteService  constanteService;





    /** Creates a new instance of ModelOpav */
    public  ModelOpav() {
        try{/*
            ClientesVerService=new ClientesVerService();
            NegociosApplusService=new NegociosApplusService();
            ElectricaribeOfertaSvc= new ElectricaribeOfertaService();
            electricaribeVerSvc= new ElectricaribeVerService();
            ElectricaribeOtSvc=new ElectricaribeOtService();   
            electricaribeOfertaSvc_ant=new ElectricaribeOfertaService_anterior();
            seguimientoEjecucionService = new SeguimientoEjecucionService(); //darrieta
            applusService = new ApplusService();
            consorcioService = new ConsorcioService();
            constanteService = new ConstanteService();
        */
        }catch(Exception e){
        }
    }
    
    public  ModelOpav(String dataBaseName) {
        try{
            ClientesVerService=new ClientesVerService(dataBaseName);
            NegociosApplusService=new NegociosApplusService(dataBaseName);
            ElectricaribeOfertaSvc= new ElectricaribeOfertaService(dataBaseName);
            electricaribeVerSvc= new ElectricaribeVerService(dataBaseName);
            ElectricaribeOtSvc=new ElectricaribeOtService(dataBaseName);   
            electricaribeOfertaSvc_ant=new ElectricaribeOfertaService_anterior(dataBaseName);
            seguimientoEjecucionService = new SeguimientoEjecucionService(dataBaseName); //darrieta
            applusService = new ApplusService(dataBaseName);
            consorcioService = new ConsorcioService(dataBaseName);
            constanteService = new ConstanteService(dataBaseName);

        }catch(Exception e){
        }
    }
}