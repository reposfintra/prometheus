/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.opav.controller;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.tsp.exceptions.InformationException;
import com.tsp.opav.model.DAOS.CarteraSlDAO;
import com.tsp.opav.model.DAOS.impl.CarteraSlImpl;

import com.tsp.operation.model.beans.Usuario;
import java.io.IOException;
import java.net.URLDecoder;
import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;

/**
 *
 * @author user
 */
public class CarteraSlAction extends Action {

    private final int ISERTAR_TASA = 0;
    private final int CARGAR_AUDITORIA_TASAS = 1;

    

    private JsonObject respuesta;
    private CarteraSlDAO dao;
    Usuario usuario = null;
    private String txtResp;

    @Override
    public void run() throws ServletException, InformationException {

        try {
            HttpSession session = request.getSession();
            usuario = (Usuario) session.getAttribute("Usuario");
            dao = new CarteraSlImpl(usuario.getBd());
            int opcion = (request.getParameter("opcion") != null)
                    ? Integer.parseInt(request.getParameter("opcion"))
                    : -1;
            switch (opcion) {
                case CARGAR_AUDITORIA_TASAS:
                    this.cargar_auditoria_tasas();
                    break;
                case ISERTAR_TASA:
                    this.isertar_tasa();
                    break;

            }
            
            response.setContentType("application/json; charset=utf-8");
            response.setHeader("Cache-Control", "no-cache");
            response.getWriter().println((new Gson()).toJson(respuesta));

        } catch (Exception e) {
            e.printStackTrace();
        }
        

    }

    private void cargar_auditoria_tasas() throws IOException {
        //String id_usuario = request.getParameter("id_usuario");
        
        respuesta = dao.cargar_auditoria_tasas();
        
    }

    private void isertar_tasa() throws IOException {
        
        String tasa = request.getParameter("tasa") != null ? URLDecoder.decode(request.getParameter("tasa"), "UTF-8") : "";
        
        respuesta = dao.isertar_tasa(tasa , usuario.getLogin());
        
    }
    
    
    


}
