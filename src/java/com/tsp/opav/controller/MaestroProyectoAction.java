/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.opav.controller;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.tsp.exceptions.InformationException;
import com.tsp.opav.model.DAOS.MaestroProyectoDAO;
import com.tsp.opav.model.DAOS.impl.MaestroProyectoImpl;
import com.tsp.operation.controller.EmailSenderService;
import com.tsp.operation.model.TransaccionService;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.services.NegocioTrazabilidadService;
import com.tsp.util.Util;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mcastillo
 */
public class MaestroProyectoAction extends Action {

    private final int CARGAR_WBS = 0;
    private final int LISTAR_AREAS = 1;
    private final int LISTAR_DISCIPLINAS = 2;
    private final int GUARDAR_AREA_PROYECTO = 3;
    private final int GUARDAR_DISCIPLINA_AREA = 4;
    private final int GUARDAR_CAPITULO = 5;
    private final int GUARDAR_ACTIVIDAD_CAPITULO = 6;
    private final int ELIMINAR_NODO_MENU = 7;
    private final int LISTAR_ACTIVIDADES = 8;
    private final int CARGAR_GRILLA_ACTIVIDADES_CAPITULO = 9;
    private final int CARGAR_GRILLA_APU_ACTIVIDADES = 10;
    private final int DESASIGNAR_ACTIVIDAD_CAPITULO = 11;
    private final int DESASIGNAR_APU_ACTIVIDAD = 12;
    private final int CARGAR_GRILLA_ACTIVIDADES = 13;
    private final int INSERTAR_ACTIVIDAD = 14;
    private final int ACTUALIZAR_ACTIVIDAD = 15;
    private final int ACTIVAR_INACTIVAR_ACTIVIDAD = 16;
    private final int CARGAR_GRILLA_AREAS = 17;
    private final int INSERTAR_AREA = 18;
    private final int ACTUALIZAR_AREA = 19;
    private final int ACTIVAR_INACTIVAR_AREA = 20;
    private final int CARGAR_GRILLA_DISCIPLINAS = 21;
    private final int INSERTAR_DISCIPLINA = 22;
    private final int ACTUALIZAR_DISCIPLINA = 23;
    private final int ACTIVAR_INACTIVAR_DISCIPLINA = 24;
    private final int CARGAR_APU_X_GRUPO = 25;
    private final int CARGAR_APU_ASOC = 26;
    private final int ASOCIAR_APU_ACTIVIDAD = 27;
    private final int DESASOCIAR_APU_ACTIVIDAD = 28;
    private final int LISTAR_TIPO_ACTORES = 29;
    private final int GUARDAR_TIPO_ACTOR = 30;
    private final int ACTUALIZAR_TIPO_ACTOR = 31;
    private final int ACTIVAR_INACTIVAR_TIPO_ACTOR = 32;
    private final int CARGAR_CBO_TIPO_ACTORES = 33;
    private final int CARGAR_CBO_TIPOS_IDENTIFICACION = 34;
    private final int LISTAR_ACTORES = 35;
    private final int GUARDAR_ACTOR = 36;
    private final int ACTUALIZAR_ACTOR = 37;
    private final int ACTIVAR_INACTIVAR_ACTOR = 38;
    private final int GUARDAR_APU_EDIT = 39;
    private final int OBTENER_ACCION = 40;
    private final int ACTUALIZAR_CANTIDAD_APU = 41;
    private final int CARGAR_POLIZAS = 42;
    private final int GUARDAR_POLIZA = 43;
    private final int ACTUALIZAR_POLIZA = 44;
    private final int ACTIVAR_INACTIVAR_POLIZA = 45;
    private final int LISTAR_BROKER = 46;
    private final int GUARDAR_BROKER = 47;
    private final int ACTUALIZAR_BROKER = 48;
    private final int ACTIVAR_INACTIVAR_BROKER = 49;
    private final int ACTUALIZAR_ESTADO_REL = 50;
    private final int CARGAR_GRID_INSUMOS = 51;
    private final int LISTAR_CORRESPONDENCIA = 52;
    private final int CARGAR_EQUIVALENCIAS = 53;
    private final int CAMBIAR_ESTADO_CORRESPONDENCIA = 54;
    private final int GUARDAR_CORRESPONDENCIA= 55;
    private final int ACTUALIZAR_CORRESPONDENCIA = 56;
    private final int LISTAR_CATEGORIAS_ADMON = 57;
    private final int GUARDAR_CATEGORIA_ADMON = 58;
    private final int ACTUALIZAR_CATEGORIA_ADMON = 59;
    private final int ACTIVAR_INACTIVAR_ITEM = 60;
    private final int LISTAR_ITEMS_CATEGORIA = 61;
    private final int GUARDAR_ITEM_CATEGORIA = 62;
    private final int ACTUALIZAR_ITEM_CATEGORIA = 63;
    private final int CARGAR_GRILLA_UNIDADES_ADMON = 64;
    private final int INSERTAR_UNIDAD_ADMON = 65;
    private final int ACTUALIZAR_UNIDAD_ADMON = 66;
    private final int ACTIVAR_INACTIVAR_UNIDAD_ADMON = 67;
    private final int CARGAR_ASEGURADORAS = 68;
    private final int GUARDAR_ASEGURADORA = 69;
    private final int ACTUALIZAR_ASEGURADORA = 70;
    private final int ACTIVAR_INACTIVAR_ASEGURADORA = 71;
    private final int EXISTE_ASEGURADORA_EN_PROVEEDOR = 72;
    private final int FILTRO_APUS = 73;
    private final int FILTRO_INSUMO = 74;

    private JsonObject respuesta;

    MaestroProyectoDAO dao;
    Usuario usuario = null;
    NegocioTrazabilidadService trazaService;
    String reponseJson = "{}";
    String typeResponse = "application/json;";

    @Override
    public void run() throws ServletException, InformationException {
        try {

            HttpSession session = request.getSession();
            usuario = (Usuario) session.getAttribute("Usuario");
            trazaService = new NegocioTrazabilidadService(usuario.getBd());
            int opcion = Integer.parseInt(request.getParameter("opcion"));
            dao = new MaestroProyectoImpl(usuario.getBd());

            switch (opcion) {
                case CARGAR_WBS:
                    cargarWbs();
                    break;
                case LISTAR_AREAS:
                    this.cargarAreas();
                    break;
                case LISTAR_DISCIPLINAS:
                    this.listarDisciplinas();
                    break;
                case GUARDAR_AREA_PROYECTO:
                    guardarAreaProyecto();
                    break;
                case GUARDAR_DISCIPLINA_AREA:
                    guardarDisciplinaArea();
                    break;
                case GUARDAR_CAPITULO:
                    guardarCapitulo();
                    break;
                case GUARDAR_ACTIVIDAD_CAPITULO:
                    guardarActividadCapitulo();
                    break;
                case ELIMINAR_NODO_MENU:
                    eliminarItem();
                    break;
                case LISTAR_ACTIVIDADES:
                    this.cargarActividades();
                    break;
                case CARGAR_GRILLA_ACTIVIDADES_CAPITULO:
                    this.cargarGridActividadesCapitulo();
                    break;
                case CARGAR_GRILLA_APU_ACTIVIDADES:
                    this.cargarGridApuActividades();
                    break;
                case DESASIGNAR_ACTIVIDAD_CAPITULO:
                    desasignarActividadCapitulo();
                    break;
                case DESASIGNAR_APU_ACTIVIDAD:
                    desasignarApuActividad();
                    break;
                case CARGAR_GRILLA_ACTIVIDADES:
                    this.cargarGridActividades();
                    break;
                case INSERTAR_ACTIVIDAD:
                    insertarActividad();
                    break;
                case ACTUALIZAR_ACTIVIDAD:
                    actualizarActividad();
                    break;
                case ACTIVAR_INACTIVAR_ACTIVIDAD:
                    activaInactivaActividad();
                    break;
                case CARGAR_GRILLA_AREAS:
                    this.cargarGridAreas();
                    break;
                case INSERTAR_AREA:
                    insertarArea();
                    break;
                case ACTUALIZAR_AREA:
                    actualizarArea();
                    break;
                case ACTIVAR_INACTIVAR_AREA:
                    activaInactivaArea();
                    break;
                case CARGAR_GRILLA_DISCIPLINAS:
                    this.cargarGridDisciplinas();
                    break;
                case INSERTAR_DISCIPLINA:
                    insertarDisciplina();
                    break;
                case ACTUALIZAR_DISCIPLINA:
                    actualizarDisciplina();
                    break;
                case ACTIVAR_INACTIVAR_DISCIPLINA:
                    activaInactivaDisciplina();
                    break;
                case CARGAR_APU_X_GRUPO:
                    this.getAPUXGrupo();
                    break;
                case CARGAR_APU_ASOC:
                    this.getAPUAsoc();
                    break;
                case ASOCIAR_APU_ACTIVIDAD:
                    this.AsocApuActividad();
                    break;
                case DESASOCIAR_APU_ACTIVIDAD:
                    this.DesAsocApuActividad();
                    break;
                case LISTAR_TIPO_ACTORES:
                    listarTipoActores();
                    break;
                case GUARDAR_TIPO_ACTOR:
                    insertarTipoActor();
                    break;
                case ACTUALIZAR_TIPO_ACTOR:
                    actualizarTipoActor();
                    break;
                case ACTIVAR_INACTIVAR_TIPO_ACTOR:
                    activaInactivaTipoActor();
                    break;
                case CARGAR_CBO_TIPO_ACTORES:
                    cargarCboTipoActores();
                    break;
                case CARGAR_CBO_TIPOS_IDENTIFICACION:
                    cargarTiposIdentificacion();
                    break;
                case LISTAR_ACTORES:
                    listarActores();
                    break;
                case GUARDAR_ACTOR:
                    insertarActor();
                    break;
                case ACTUALIZAR_ACTOR:
                    actualizarActor();
                    break;
                case ACTIVAR_INACTIVAR_ACTOR:
                    activaInactivaActor();
                    break;
                case GUARDAR_APU_EDIT:
                    this.guardar_armado_apu_edit();
                    break;
                case OBTENER_ACCION:
                    this.obtenerAccion();
                    break;
                case ACTUALIZAR_CANTIDAD_APU:
                    this.ActualizarCantidadAPU();
                    break;
                case ACTUALIZAR_ESTADO_REL:
                    this.ActualizarEstadoRel();
                    break;
                case CARGAR_POLIZAS:
                    cargarPolizas();
                    break;
                case GUARDAR_POLIZA:
                    guardarPoliza();
                    break;
                case ACTUALIZAR_POLIZA:
                    actualizarPoliza();
                    break;
                case ACTIVAR_INACTIVAR_POLIZA:
                    activaInactivaPoliza();
                    break;
                case LISTAR_BROKER:
                    listarBroker();
                    break;
                case GUARDAR_BROKER:
                    insertarBroker();
                    break;
                case ACTUALIZAR_BROKER:
                    actualizarBroker();
                    break;
                case ACTIVAR_INACTIVAR_BROKER:
                    activaInactivaBroker();
                    break;
                case CARGAR_GRID_INSUMOS:
                    this.cargar_grid_insumos_edit();
                    break;
                case LISTAR_CORRESPONDENCIA:
                    listarCorrespondencia();
                    break;
                case CARGAR_EQUIVALENCIAS:
                    cargarEquivalencias();
                    break;
                case CAMBIAR_ESTADO_CORRESPONDENCIA:
                    cambiarEstadoCorrespondencia();
                    break;
                case GUARDAR_CORRESPONDENCIA:
                    guardarCorrespondencia();
                    break;
                case ACTUALIZAR_CORRESPONDENCIA:
                    actualizarCorrespondencia();
                    break;  
                case LISTAR_CATEGORIAS_ADMON:
                    listarCategoriasAdmon();
                    break;
                case GUARDAR_CATEGORIA_ADMON:
                    guardarCategoriaAdmon();
                    break;
                case ACTUALIZAR_CATEGORIA_ADMON:
                    actualizarCategoriaAdmon();
                    break;
                case ACTIVAR_INACTIVAR_ITEM:
                    activaInactivaItem();
                    break;
                case LISTAR_ITEMS_CATEGORIA:
                    listarItemsCategoria();
                    break;
                case GUARDAR_ITEM_CATEGORIA:
                    guardarItemCategoria();
                    break;
                case ACTUALIZAR_ITEM_CATEGORIA:
                    actualizarItemCategoria();
                    break;
                case CARGAR_GRILLA_UNIDADES_ADMON:
                    this.cargarGridUndsAdmon();
                    break;
                case INSERTAR_UNIDAD_ADMON:
                    insertarUndAdmon();
                    break;
                case ACTUALIZAR_UNIDAD_ADMON:
                    actualizarUndAdmon();
                    break;
                case ACTIVAR_INACTIVAR_UNIDAD_ADMON:
                    activaInactivaUndAdmon();
                    break;
                case CARGAR_ASEGURADORAS:
                    cargarAseguradoras();
                    break;
                case GUARDAR_ASEGURADORA:
                    guardarAseguradora();
                    break;
                case ACTUALIZAR_ASEGURADORA:
                    actualizarAseguradora();
                    break;
                case ACTIVAR_INACTIVAR_ASEGURADORA:
                    activaInactivaAseguradora();
                    break;
                case EXISTE_ASEGURADORA_EN_PROVEEDOR:
                    existeNitAseguradoraEnProveedor();
                    break;
                case FILTRO_APUS:
                    filtro_Apus();
                    break;
                case FILTRO_INSUMO:
                    filtro_Insumo();
                    break;
                default:
                    this.printlnResponseAjax("{\"error\":\"Peticion invalida 404 recurso no encontrado\"}", "application/json;");
                    break;
            }

        } catch (Exception ex) {
            reponseJson = "{\"error\":\"Algo salio mal al procesar su solicitud\",\"exception\":\"" + ex.getMessage() + "\"}";
            ex.printStackTrace();
        } finally {
            try {
                this.printlnResponseAjax(reponseJson, typeResponse);
            } catch (Exception ex1) {
                Logger.getLogger(MaestroProyectoAction.class.getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }

    private void cargarWbs() {
        try {
            String id_solicitud = request.getParameter("id_solicitud") != null ? request.getParameter("id_solicitud") : "";
            this.reponseJson = dao.getInfoWbs(id_solicitud);
        } catch (Exception ex) {
            Logger.getLogger(MaestroProyectoAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void cargarAreas() {
        try {
            String q = request.getParameter("q") != null ? Util.setCodificacionCadena(request.getParameter("q"),"ISO-8859-1") : "";
            this.reponseJson = dao.cargarAreas(q);
        } catch (Exception ex) {
            Logger.getLogger(MaestroProyectoAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void listarDisciplinas() {
        try {
            this.reponseJson = dao.cargarDisciplinas();
        } catch (Exception ex) {
            Logger.getLogger(MaestroProyectoAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void guardarAreaProyecto() {
        try {
            String id_area_proyecto = (request.getParameter("id_area_proyecto") != null) ? request.getParameter("id_area_proyecto") : "0";
            String id_solicitud = (request.getParameter("id_solicitud") != null) ? request.getParameter("id_solicitud") : "";
            String nombre_area = (request.getParameter("area") != null) ? Util.setCodificacionCadena(request.getParameter("area"),"ISO-8859-1") : "0";
            this.reponseJson = (id_area_proyecto.equals("0")) ? dao.insertarAreaProyecto(id_solicitud, nombre_area, usuario) : dao.actualizarAreaProyecto(id_area_proyecto, nombre_area, usuario);
        } catch (Exception ex) {
            Logger.getLogger(MaestroProyectoAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void guardarDisciplinaArea() {
        try {
            String id_disciplina_area = (request.getParameter("id_disciplina_area") != null) ? request.getParameter("id_disciplina_area") : "0";
            String id_area_proyecto = (request.getParameter("id_area_proyecto") != null) ? request.getParameter("id_area_proyecto") : "0";
            String id_disciplina = (request.getParameter("id_disciplina") != null) ? request.getParameter("id_disciplina") : "0";
            this.reponseJson = (id_disciplina_area.equals("0")) ? dao.insertarDisciplinaArea(id_area_proyecto, id_disciplina, usuario) : dao.actualizarDisciplinaArea(id_disciplina_area, id_disciplina, usuario);
        } catch (Exception ex) {
            Logger.getLogger(MaestroProyectoAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void guardarCapitulo() {
        try {
            String id_capitulo = (request.getParameter("id_capitulo_disciplina") != null) ? request.getParameter("id_capitulo_disciplina") : "0";
            String id_disciplina = (request.getParameter("id_disciplina") != null) ? request.getParameter("id_disciplina") : "0";
            String descripcion = (request.getParameter("descripcion") != null) ? Util.setCodificacionCadena(request.getParameter("descripcion"),"ISO-8859-1") : "";
            this.reponseJson = (id_capitulo.equals("0")) ? dao.insertarCapitulo(id_disciplina, descripcion, usuario) : dao.actualizarCapitulo(id_capitulo, descripcion, usuario);
        } catch (Exception ex) {
            Logger.getLogger(MaestroProyectoAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void guardarActividadCapitulo() {
        try {
            String id_capitulo = (request.getParameter("id_capitulo") != null) ? request.getParameter("id_capitulo") : "0";
            String id_actividad = (request.getParameter("id_actividad") != null) ? request.getParameter("id_actividad") : "0";
            this.reponseJson = dao.insertarActividadCapitulo(id_capitulo, id_actividad, usuario);
        } catch (Exception ex) {
            Logger.getLogger(MaestroProyectoAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void eliminarItem() {
        try {
            String id = (request.getParameter("id") != null) ? request.getParameter("id") : "0";
            String nivel = (request.getParameter("nivel") != null) ? request.getParameter("nivel") : "";
            String query = (nivel.equals("1") ? "SQL_DELETE_AREA_PROYECTO" : nivel.equals("2") ? "SQL_DELETE_DISCIPLINA_AREA" : "SQL_DELETE_CAPITULO");
            String filtro = (nivel.equals("1") ? " AND a.id =" + id : nivel.equals("2") ? " AND d.id =" + id : " AND cap.id =" + id);
            if (!dao.existeApuAsociado(filtro)) {
                this.reponseJson = dao.eliminarItem(id, query);
            } else {
                this.reponseJson = "{\"error\":\" No es posible eliminar el item seleccionado, tiene APU's asociados\"}";
            }
        } catch (Exception ex) {
            Logger.getLogger(MaestroProyectoAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void desasignarActividadCapitulo() {
        try {
            String id = (request.getParameter("id") != null) ? request.getParameter("id") : "0";
            String query = "SQL_UPDATE_ACTIVIDAD_CAPITULO";
             String filtro = " AND act.id =" + id;
            if (!dao.existeApuAsociado(filtro)) {
                this.reponseJson = dao.eliminarItem(id, query);
            } else {
                this.reponseJson = "{\"error\":\" No es posible eliminar el item seleccionado, tiene APU's asociados\"}";
            }
        } catch (Exception ex) {
            Logger.getLogger(MaestroProyectoAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void desasignarApuActividad() {
        try {
            String id = (request.getParameter("id") != null) ? request.getParameter("id") : "0";
            String query = "SQL_DELETE_APU_ACTIVIDAD";
            this.reponseJson = dao.eliminarItem(id, query);
        } catch (Exception ex) {
            Logger.getLogger(MaestroProyectoAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void cargarActividades() {
        try {
            String q = request.getParameter("q") != null ? Util.setCodificacionCadena(request.getParameter("q"),"ISO-8859-1") : "";
            this.reponseJson = dao.cargarActividades(q);
        } catch (Exception ex) {
            Logger.getLogger(MaestroProyectoAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void cargarGridActividadesCapitulo() {
        try {
            String id_capitulo = request.getParameter("id_capitulo") != null ? request.getParameter("id_capitulo") : "";
            this.reponseJson = dao.cargarGridActividades(id_capitulo);
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(MaestroProyectoAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void cargarGridApuActividades() {
        try {
            String id_actividad = request.getParameter("id_actividad") != null ? request.getParameter("id_actividad") : "";
            this.reponseJson = dao.cargarGridAPUsActividades(id_actividad);
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(MaestroProyectoAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void cargarGridActividades() {
        try {
            this.reponseJson = dao.cargarGridActividades();
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(MaestroProyectoAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void insertarActividad() {
        try {
            String descripcion = (request.getParameter("descripcion") != null) ? Util.setCodificacionCadena(request.getParameter("descripcion"),"ISO-8859-1") : "";
            if (!dao.existeActividad(descripcion, "0")) {
                this.reponseJson = dao.insertarActividad(descripcion, usuario);
            } else {
                this.reponseJson = "{\"error\":\" No se cre� la actividad, ya existe una actividad con ese nombre\"}";
            }
        } catch (Exception ex) {
            Logger.getLogger(MaestroProyectoAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void actualizarActividad() {

        try {
            String id = request.getParameter("id") != null ? request.getParameter("id") : "";
            String descripcion = request.getParameter("descripcion") != null ? Util.setCodificacionCadena(request.getParameter("descripcion"),"ISO-8859-1") : "";
            if (!dao.existeActividad(descripcion, id)) {
                this.reponseJson = dao.actualizarActividad(id, descripcion, usuario);
            } else {
                this.reponseJson = "{\"error\":\" No se cre� la actividad, ya existe una actividad con ese nombre\"}";
            }
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(MaestroProyectoAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void activaInactivaActividad() {

        try {
            String id = request.getParameter("id") != null ? request.getParameter("id") : "";
            this.reponseJson = dao.activaInactivaActividad(id, usuario);
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(MaestroProyectoAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void cargarGridAreas() {
        try {
            this.reponseJson = dao.cargarGridAreas();
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(MaestroProyectoAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void insertarArea() {
        try {
            String nombre = (request.getParameter("nombre") != null) ? Util.setCodificacionCadena(request.getParameter("nombre"),"ISO-8859-1") : "";
            if (!dao.existeArea(nombre, "0")) {
                this.reponseJson = dao.insertarArea(nombre, usuario);
            } else {
                this.reponseJson = "{\"error\":\" No se cre� el area, ya existe un area con ese nombre\"}";
            }
        } catch (Exception ex) {
            Logger.getLogger(MaestroProyectoAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void actualizarArea() {

        try {
            String id = request.getParameter("id") != null ? request.getParameter("id") : "";
            String nombre = request.getParameter("nombre") != null ? Util.setCodificacionCadena(request.getParameter("nombre"),"ISO-8859-1") : "";
            if (!dao.existeArea(nombre, id)) {
                this.reponseJson = dao.actualizarArea(id, nombre, usuario);
            } else {
                this.reponseJson = "{\"error\":\" No se cre� el area, ya existe un area con ese nombre\"}";
            }

        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(MaestroProyectoAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void activaInactivaArea() {
        try {
            String id = request.getParameter("id") != null ? request.getParameter("id") : "";
            this.reponseJson = dao.activaInactivaArea(id, usuario);
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(MaestroProyectoAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void cargarGridDisciplinas() {
        try {
            this.reponseJson = dao.cargarGridDisciplinas();
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(MaestroProyectoAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void insertarDisciplina() {
        try {
            String nombre = (request.getParameter("nombre") != null) ? Util.setCodificacionCadena(request.getParameter("nombre"),"ISO-8859-1") : "";
            if (!dao.existeDisciplina(nombre, "0")) {
                this.reponseJson = dao.insertarDisciplina(nombre, usuario);
            } else {
                this.reponseJson = "{\"error\":\" No se cre� la disciplina, ya existe una disciplina con ese nombre\"}";
            }
        } catch (Exception ex) {
            Logger.getLogger(MaestroProyectoAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void actualizarDisciplina() {

        try {
            String id = request.getParameter("id") != null ? request.getParameter("id") : "";
            String nombre = request.getParameter("nombre") != null ? Util.setCodificacionCadena(request.getParameter("nombre"),"ISO-8859-1") : "";
            if (!dao.existeDisciplina(nombre, id)) {
                this.reponseJson = dao.actualizarDisciplina(id, nombre, usuario);
            } else {
                this.reponseJson = "{\"error\":\" No se cre� la disciplina, ya existe una disciplina con ese nombre\"}";
            }
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(MaestroProyectoAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void activaInactivaDisciplina() {
        try {
            String id = request.getParameter("id") != null ? request.getParameter("id") : "";
            this.reponseJson = dao.activaInactivaDisciplina(id, usuario);
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(MaestroProyectoAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void getAPUXGrupo() {
        try {
            String grupo_apu = request.getParameter("grupo_apu");
            String id = request.getParameter("id");
            this.reponseJson = dao.cargarAPUXGrupo(grupo_apu, id);
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(MaestroProyectoAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void getAPUAsoc() {
        try {
                String id = request.getParameter("id");
            this.reponseJson = dao.cargarAPUAsoc(id);
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(MaestroProyectoAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void AsocApuActividad() {
        String listado[] = request.getParameter("listado").split(",");
        int id = Integer.parseInt(request.getParameter("id"));

        try {
            TransaccionService tService = new TransaccionService(usuario.getBd());
            tService.crearStatement();

            for (int i = 0; i < listado.length; i++) {

                tService.getSt().addBatch(dao.insertarRelAPUActividad(id, listado[i], usuario));
            }
            tService.execute();
            String resp = "{\"respuesta\":\"OK\"}";
            this.reponseJson = resp;
            //this.printlnResponse(resp, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void DesAsocApuActividad() {
        String listado[] = request.getParameter("listado").split(",");
        int id = Integer.parseInt(request.getParameter("id"));

        try {
            TransaccionService tService = new TransaccionService(usuario.getBd());
            tService.crearStatement();

            for (int i = 0; i < listado.length; i++) {

                tService.getSt().addBatch(dao.EliminarRelAPUActividad(id, listado[i]));

//                tService.getSt().addBatch("delete from opav.sl_rel_grupo_apu where id_apu=(select id FROM opav.sl_apu where id= " + listado[i] + " and tipo_apu='PROYECTO');");
//                tService.getSt().addBatch("delete from opav.sl_apu_det where id_apu=(select id FROM opav.sl_apu where id= " + listado[i] + " and tipo_apu='PROYECTO');");
//                tService.getSt().addBatch("delete from opav.sl_apu where id= " + listado[i] + " and tipo_apu='PROYECTO';");

            }
            tService.execute();
            String resp = "{\"respuesta\":\"OK\"}";
            this.reponseJson = resp;
            //this.printlnResponse(resp, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void listarTipoActores() {
        try {
            String mostrarTodos = request.getParameter("mostrarTodos") != null ? request.getParameter("mostrarTodos") : "N";
            this.reponseJson = dao.listarTipoActores(mostrarTodos);
        } catch (Exception ex) {
            Logger.getLogger(MaestroProyectoAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void insertarTipoActor() {

        try {
            String descripcion = request.getParameter("descripcion") != null ? Util.setCodificacionCadena(request.getParameter("descripcion"),"ISO-8859-1") : "";
            this.reponseJson = dao.guardarTipoActor(descripcion, usuario);
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(MaestroProyectoAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void actualizarTipoActor() {

        try {
            String id = request.getParameter("id") != null ? request.getParameter("id") : "";
            String descripcion = request.getParameter("descripcion") != null ? Util.setCodificacionCadena(request.getParameter("descripcion"),"ISO-8859-1") : "";
            this.reponseJson = dao.actualizarTipoActor(id, descripcion, usuario);
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(MaestroProyectoAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void activaInactivaTipoActor() {

        try {
            String id = request.getParameter("id") != null ? request.getParameter("id") : "";
            this.reponseJson = dao.activaInactivaTipoActor(id, usuario);
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(MaestroProyectoAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void cargarCboTipoActores() {
        try {
            this.reponseJson = dao.cargarCboTipoActores();
        } catch (Exception ex) {
            Logger.getLogger(MaestroProyectoAction.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void cargarTiposIdentificacion() {
        try {
            this.reponseJson = dao.cargarTiposIdentificacion();
        } catch (Exception ex) {
            Logger.getLogger(MaestroProyectoAction.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void listarActores() {
        try {
            String mostrarTodos = request.getParameter("mostrarTodos") != null ? request.getParameter("mostrarTodos") : "N";
            this.reponseJson = dao.cargarGridActores(mostrarTodos);
        } catch (Exception ex) {
            Logger.getLogger(MaestroProyectoAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void insertarActor() {

        try {
            String tipoActor = (request.getParameter("tipo_actor") != null) ? request.getParameter("tipo_actor") : "";
            String tipoDoc = (request.getParameter("tipo_doc") != null) ? request.getParameter("tipo_doc") : "";
            String documento = (request.getParameter("documento") != null) ? request.getParameter("documento") : "";
            String nombre = (request.getParameter("nombre") != null) ? Util.setCodificacionCadena(request.getParameter("nombre"),"ISO-8859-1") : "";
            String direccion = (request.getParameter("direccion") != null) ? Util.setCodificacionCadena(request.getParameter("direccion"),"ISO-8859-1") : "";
            String email = (request.getParameter("email") != null) ? Util.setCodificacionCadena(request.getParameter("email"),"ISO-8859-1") : "";
            String celular = (request.getParameter("celular") != null) ? request.getParameter("celular") : "";
            String telefono = (request.getParameter("telefono") != null) ? request.getParameter("telefono") : "";
            String extension = (request.getParameter("ext") != null) ? request.getParameter("ext") : "";
            String pais = (request.getParameter("pais") != null) ? request.getParameter("pais") : "";
            String departamento = (request.getParameter("departamento") != null) ? request.getParameter("departamento") : "";
            String ciudad = (request.getParameter("ciudad") != null) ? request.getParameter("ciudad") : "";
            String tarj_prof = (request.getParameter("tarjeta_prof") != null) ? Util.setCodificacionCadena(request.getParameter("tarjeta_prof"),"ISO-8859-1") : "";
            String lugarExpCed = (request.getParameter("lugar_expedicion") != null) ? Util.setCodificacionCadena(request.getParameter("lugar_expedicion"),"ISO-8859-1") : "";
            this.reponseJson = dao.guardarActor(Integer.parseInt(tipoActor), tipoDoc, documento, nombre, ciudad, departamento, pais, direccion, telefono, extension, celular, email, tarj_prof, lugarExpCed, usuario);
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(MaestroProyectoAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void actualizarActor() {

        try {
            String id = request.getParameter("id") != null ? request.getParameter("id") : "";
            String tipoActor = (request.getParameter("tipo_actor") != null) ? request.getParameter("tipo_actor") : "";
            String tipoDoc = (request.getParameter("tipo_doc") != null) ? request.getParameter("tipo_doc") : "";
            String documento = (request.getParameter("documento") != null) ? request.getParameter("documento") : "";
            String nombre = (request.getParameter("nombre") != null) ? Util.setCodificacionCadena(request.getParameter("nombre"),"ISO-8859-1") : "";
            String direccion = (request.getParameter("direccion") != null) ? Util.setCodificacionCadena(request.getParameter("direccion"),"ISO-8859-1") : "";
            String email = (request.getParameter("email") != null) ? Util.setCodificacionCadena(request.getParameter("email"),"ISO-8859-1") : "";
            String celular = (request.getParameter("celular") != null) ? request.getParameter("celular") : "";
            String telefono = (request.getParameter("telefono") != null) ? request.getParameter("telefono") : "";
            String extension = (request.getParameter("ext") != null) ? request.getParameter("ext") : "";
            String pais = (request.getParameter("pais") != null) ? request.getParameter("pais") : "";
            String departamento = (request.getParameter("departamento") != null) ? request.getParameter("departamento") : "";
            String ciudad = (request.getParameter("ciudad") != null) ? request.getParameter("ciudad") : "";
            String tarj_prof = (request.getParameter("tarjeta_prof") != null) ? Util.setCodificacionCadena(request.getParameter("tarjeta_prof"),"ISO-8859-1") : "";
            String lugarExpCed = (request.getParameter("lugar_expedicion") != null) ? Util.setCodificacionCadena(request.getParameter("lugar_expedicion"),"ISO-8859-1") : "";
            this.reponseJson = dao.actualizarActor(Integer.parseInt(id), Integer.parseInt(tipoActor), tipoDoc, documento, nombre, ciudad, departamento, pais, direccion, telefono, extension, celular, email, tarj_prof, lugarExpCed, usuario);
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(MaestroProyectoAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void activaInactivaActor() {

        try {
            String id = request.getParameter("id") != null ? request.getParameter("id") : "";
            this.reponseJson = dao.activaInactivaActor(id, usuario);
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(MaestroProyectoAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void guardar_armado_apu_edit() throws IOException {
       
        try {
            JsonObject info = (JsonObject) (new JsonParser()).parse(request.getParameter("informacion"));
            info.addProperty("usuario", ((Usuario) request.getSession().getAttribute("Usuario")).getLogin());
            info.addProperty("dstrct", ((Usuario) request.getSession().getAttribute("Usuario")).getDstrct());
            info.addProperty("nit_propietario", ((Usuario) request.getSession().getAttribute("Usuario")).getNitPropietario());
            info.addProperty("grupo_apu", (request.getParameter("grupo_apu")));
            info.addProperty("nomapu", (Util.setCodificacionCadena(request.getParameter("nomapu"),"ISO-8859-1")));
            info.addProperty("ids", (request.getParameter("ids")));
            info.addProperty("id_actividad", (request.getParameter("id_actividad")));
            info.addProperty("tipo", (Util.setCodificacionCadena(request.getParameter("tipo"),"ISO-8859-1")));
            info.addProperty("cantidad", (request.getParameter("cantidad")));
            info.addProperty("tipo_apu", (request.getParameter("tipo_apu")));
            info.addProperty("unidad_medida", (request.getParameter("unidad_medida")));

            String resp = "{}";
            
            if (request.getParameter("tipo").equals("0")) {
                resp = dao.guardarAPUEdit(info);
            } else {
                resp = dao.guardarAPUEditEspejo(info);
            }
            
            this.reponseJson = resp;
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void obtenerAccion() {
        try {

            String id = request.getParameter("id_solicitud");

            this.reponseJson = dao.ObtenerAccion(id);
        } catch (Exception ex) {
            Logger.getLogger(MaestroProyectoAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void ActualizarCantidadAPU() {

        int id_apu = Integer.parseInt(request.getParameter("id_apu"));
        int id_actividad = Integer.parseInt(request.getParameter("id_actividad"));
        int cantidad = Integer.parseInt(request.getParameter("cantidad"));
        int posicion = Integer.parseInt(request.getParameter("posicion"));

        try {
            this.reponseJson = dao.ActualizarCantidadAPU(id_apu, id_actividad, cantidad, posicion);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void ActualizarEstadoRel() {

        int id_apu = Integer.parseInt(request.getParameter("id_apu"));
        int id_actividad = Integer.parseInt(request.getParameter("id_actividad"));

        try {
            this.reponseJson = dao.ActualizarEstadoRel(id_apu, id_actividad);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void cargarPolizas() {
        try {
            this.reponseJson = dao.CargarPolizas();
        } catch (Exception e) {
            Logger.getLogger(MaestroProyectoAction.class.getName()).log(Level.SEVERE, null, e);
            e.printStackTrace();
        }
    }

    public void guardarPoliza() {
        String json = "";
        try {
            String nombre_poliza = (request.getParameter("nombre") != null) ? Util.setCodificacionCadena(request.getParameter("nombre"),"ISO-8859-1") : "";
            String descripcion = (request.getParameter("desc") != null) ? Util.setCodificacionCadena(request.getParameter("desc"),"ISO-8859-1") : "";
            this.reponseJson = dao.guardarPoliza(nombre_poliza, descripcion, usuario);
        } catch (Exception e) {
            e.printStackTrace();
            this.reponseJson = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        }
    }

    private void actualizarPoliza() {
        String json = "";
        try {
            String id = (request.getParameter("id") != null) ? request.getParameter("id") : "";
            String nombre_poliza = (request.getParameter("nombre") != null) ? Util.setCodificacionCadena(request.getParameter("nombre"),"ISO-8859-1") : "";
            String descripcion = (request.getParameter("desc") != null) ? Util.setCodificacionCadena(request.getParameter("desc"),"ISO-8859-1") : "";
            this.reponseJson = dao.actualizarPoliza(id, nombre_poliza, descripcion, usuario);
        } catch (Exception e) {
            e.printStackTrace();
            this.reponseJson = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        }
    }

    public void activaInactivaPoliza() {

        try {
            String id = request.getParameter("id") != null ? request.getParameter("id") : "";
            this.reponseJson = dao.activaInactivaPoliza(id, usuario);
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(MaestroProyectoAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void listarBroker() {
        try {
            this.reponseJson = dao.cargarGridBroker();
        } catch (Exception ex) {
            Logger.getLogger(MaestroProyectoAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void insertarBroker() {

        try {
            String tipoDoc = (request.getParameter("tipo_doc") != null) ? request.getParameter("tipo_doc") : "";
            String documento = (request.getParameter("documento") != null) ? request.getParameter("documento") : "";
            String nombre = (request.getParameter("nombre") != null) ? Util.setCodificacionCadena(request.getParameter("nombre"),"ISO-8859-1") : "";          
            String pais = (request.getParameter("pais") != null) ? request.getParameter("pais") : "";
            String departamento = (request.getParameter("departamento") != null) ? request.getParameter("departamento") : "";
            String ciudad = (request.getParameter("ciudad") != null) ? request.getParameter("ciudad") : "";    
            String direccion = (request.getParameter("direccion") != null) ? Util.setCodificacionCadena(request.getParameter("direccion"),"ISO-8859-1") : "";
            String idusuario = (request.getParameter("idusuario") != null) ? Util.setCodificacionCadena(request.getParameter("idusuario"),"ISO-8859-1") : ""; 
            String nombre_contacto = (request.getParameter("nombre_contacto") != null) ? Util.setCodificacionCadena(request.getParameter("nombre_contacto"),"ISO-8859-1") : "";           
            String email = (request.getParameter("email") != null) ? Util.setCodificacionCadena(request.getParameter("email"),"ISO-8859-1") : "";
            String celular = (request.getParameter("celular") != null) ? request.getParameter("celular") : "";
            String telefono = (request.getParameter("telefono") != null) ? request.getParameter("telefono") : "";
            String enviar_correo_contacto = (request.getParameter("enviar_correo_contacto") != null) ? request.getParameter("enviar_correo_contacto") : "N";
            String nombre_asistente = (request.getParameter("nombre_asistente") != null) ? Util.setCodificacionCadena(request.getParameter("nombre_asistente"),"ISO-8859-1") : "";         
            String email_asistente = (request.getParameter("email_asistente") != null) ? Util.setCodificacionCadena(request.getParameter("email_asistente"),"ISO-8859-1") : "";
            String celular_asistente = (request.getParameter("celular_asistente") != null) ? request.getParameter("celular_asistente") : "";
            String telefono_asistente = (request.getParameter("telefono_asistente") != null) ? request.getParameter("telefono_asistente") : "";
            String enviar_correo_asistente = (request.getParameter("enviar_correo_asistente") != null) ? request.getParameter("enviar_correo_asistente") : "N";
            //Envio de email al broker
            JsonObject jobj = dao.getInfoCuentaEnvioCorreo();           
            String MyMessage = "Creado Broker " + nombre + ", por el usuario " + usuario.getLogin() + ". Nombre del contacto: " + nombre_contacto + ".<br>Login:" + idusuario+" <br>Password:1234<br>Recuerde cambiar el password al iniciar sesi�n.";
            String mailsToSend = "";
            if(enviar_correo_contacto.equals("S")) mailsToSend +=  email;
            mailsToSend +=(enviar_correo_asistente.equals("S") && !mailsToSend.equals("")) ? ","+email_asistente:(enviar_correo_asistente.equals("S"))?email_asistente:"";
            String asunto = "Datos de acceso broker " + nombre + " al sistema de Fintra";
            Thread hiloEmail = new Thread(new EmailSenderService(jobj, "MAILBROKER", MyMessage, mailsToSend, asunto, usuario), "hilo");
            hiloEmail.start();
            this.reponseJson = dao.guardarBroker(tipoDoc, documento, nombre, ciudad, departamento, pais, direccion, nombre_contacto, telefono, celular, email, enviar_correo_contacto, idusuario, nombre_asistente, telefono_asistente, celular_asistente, email_asistente, enviar_correo_asistente, usuario);      
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(MaestroProyectoAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void actualizarBroker() {

        try {
            String id = request.getParameter("id") != null ? request.getParameter("id") : "";
            String tipoDoc = (request.getParameter("tipo_doc") != null) ? request.getParameter("tipo_doc") : "";
            String documento = (request.getParameter("documento") != null) ? request.getParameter("documento") : "";
            String nombre = (request.getParameter("nombre") != null) ? Util.setCodificacionCadena(request.getParameter("nombre"),"ISO-8859-1") : "";
            String direccion = (request.getParameter("direccion") != null) ? Util.setCodificacionCadena(request.getParameter("direccion"),"ISO-8859-1") : "";
            String email = (request.getParameter("email") != null) ? Util.setCodificacionCadena(request.getParameter("email"),"ISO-8859-1") : "";
            String celular = (request.getParameter("celular") != null) ? request.getParameter("celular") : "";
            String telefono = (request.getParameter("telefono") != null) ? request.getParameter("telefono") : "";
            String pais = (request.getParameter("pais") != null) ? request.getParameter("pais") : "";
            String departamento = (request.getParameter("departamento") != null) ? request.getParameter("departamento") : "";
            String ciudad = (request.getParameter("ciudad") != null) ? request.getParameter("ciudad") : "";
            String nombre_contacto = (request.getParameter("nombre_contacto") != null) ? Util.setCodificacionCadena(request.getParameter("nombre_contacto"),"ISO-8859-1") : "";
            String enviar_correo_contacto = (request.getParameter("enviar_correo_contacto") != null) ? request.getParameter("enviar_correo_contacto") : "N";
            String nombre_asistente = (request.getParameter("nombre_asistente") != null) ? Util.setCodificacionCadena(request.getParameter("nombre_asistente"),"ISO-8859-1") : "";         
            String email_asistente = (request.getParameter("email_asistente") != null) ? Util.setCodificacionCadena(request.getParameter("email_asistente"),"ISO-8859-1") : "";
            String celular_asistente = (request.getParameter("celular_asistente") != null) ? request.getParameter("celular_asistente") : "";
            String telefono_asistente = (request.getParameter("telefono_asistente") != null) ? request.getParameter("telefono_asistente") : "";
            String enviar_correo_asistente = (request.getParameter("enviar_correo_asistente") != null) ? request.getParameter("enviar_correo_asistente") : "N";
            this.reponseJson = dao.actualizarBroker(Integer.parseInt(id), tipoDoc, documento, nombre, ciudad, departamento, pais, direccion, nombre_contacto, telefono, celular, email, enviar_correo_contacto, nombre_asistente, telefono_asistente, celular_asistente, email_asistente, enviar_correo_asistente, usuario);
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(MaestroProyectoAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void activaInactivaBroker() {

        try {
            String id = request.getParameter("id") != null ? request.getParameter("id") : "";
            this.reponseJson = dao.activaInactivaBroker(id, usuario);
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(MaestroProyectoAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }
    
    private void cargar_grid_insumos_edit() throws IOException {
        try {

            String ids = request.getParameter("ids");
            int tipo = Integer.parseInt(request.getParameter("tipo"));
            String id_actividad = request.getParameter("id");
            
            this.reponseJson = dao.cargarGridInsumosEdit(ids, tipo, id_actividad);
        } catch (Exception ex) {
            Logger.getLogger(MaestroProyectoAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
        
    }
    private void listarCorrespondencia() {
        try {
            this.reponseJson = dao.listarCorrespondencia();
        } catch (Exception e) {
           Logger.getLogger(MaestroProyectoAction.class.getName()).log(Level.SEVERE, null, e);
           e.printStackTrace();
        }
    }
  
 private void cargarEquivalencias() {
        try {
            this.reponseJson = dao.cargarEquivalencias();
            //this.printlnResponseAjax(dao.cargarEquivalencias(), "application/json");
        } catch (Exception e) {
           Logger.getLogger(MaestroProyectoAction.class.getName()).log(Level.SEVERE, null, e);
           e.printStackTrace();
        }
     
    }

    public void cambiarEstadoCorrespondencia() {

        try {
            String id = request.getParameter("id") != null ? request.getParameter("id") : "";
            this.reponseJson = dao.cambiarEstadoCorrespondencia(id, usuario);
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(MaestroProyectoAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void guardarCorrespondencia() {
       try {
           
            String descripcion = (request.getParameter("descripcion") != null) ? Util.setCodificacionCadena(request.getParameter("descripcion"),"ISO-8859-1") : "";
            int id_procedencia = Integer.parseInt(request.getParameter("id_procedencia"));
            //String id_procedencia = (request.getParameter("id_prodecencia") != null) ? request.getParameter("id_prodecencia") : "";        
            this.reponseJson = dao.guardarCorrespondencia(descripcion, id_procedencia, usuario);           
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(MaestroProyectoAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    } 

    private void actualizarCorrespondencia() {
        try {
            String id = (request.getParameter("id") != null) ? request.getParameter("id") : "";
            String descripcion = (request.getParameter("descripcion") != null) ? Util.setCodificacionCadena(request.getParameter("descripcion"),"ISO-8859-1") : "";
            int id_procedencia = Integer.parseInt(request.getParameter("id_procedencia"));   
            this.reponseJson = dao.actualizarCorrespondencia(id, descripcion, id_procedencia, usuario);           
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(MaestroProyectoAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }
    
    private void listarCategoriasAdmon() {
        try {
            this.reponseJson = dao.cargarGridCategoriasAdmon();
        } catch (Exception ex) {
            Logger.getLogger(MaestroProyectoAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }
    
    private void guardarCategoriaAdmon() {
        try {
            String nombre = (request.getParameter("nombre") != null) ? Util.setCodificacionCadena(request.getParameter("nombre"),"UTF-8") : "";
            String descripcion = (request.getParameter("descripcion") != null) ? Util.setCodificacionCadena(request.getParameter("descripcion"),"UTF-8") : "";        
            if (!dao.existeCategoriaAdmon(nombre)) {
                this.reponseJson = dao.guardarCategoria(nombre, descripcion, usuario);
            } else {
                this.reponseJson = "{\"error\":\" No se cre� la categor�a, puede que el nombre ya exista\"}";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private void actualizarCategoriaAdmon() {
        try {
            String nombre = (request.getParameter("nombre") != null) ? Util.setCodificacionCadena(request.getParameter("nombre"), "UTF-8") : "";
            String descripcion = (request.getParameter("descripcion") != null) ? Util.setCodificacionCadena(request.getParameter("descripcion"), "UTF-8") : "";
            String idCategoria = (request.getParameter("idCategoria") != null) ? request.getParameter("idCategoria") : "";
            if (!dao.existeCategoriaAdmon(idCategoria,nombre)) {
                this.reponseJson = dao.actualizarCategoria(nombre, descripcion, idCategoria, usuario);
            } else {
                this.reponseJson = "{\"error\":\" No se actualiz� la categor�a, puede que el nombre ya exista\"}";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
     private void activaInactivaItem() {
        try {
             String id = (request.getParameter("id") != null) ? request.getParameter("id") : "";
             String query = (request.getParameter("query") != null) ? request.getParameter("query") : "";
             this.reponseJson = dao.activaInactivaItem(id, query, usuario);           
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
     
    private void listarItemsCategoria() {
        try {
            String idCategoria = (request.getParameter("idCategoria") != null) ? request.getParameter("idCategoria") : "";
            this.reponseJson = dao.cargarGridItemsCategoria(idCategoria);
        } catch (Exception ex) {
            Logger.getLogger(MaestroProyectoAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }
    
    private void guardarItemCategoria() {
        try {
            String idCategoria = (request.getParameter("idCategoria") != null) ? request.getParameter("idCategoria") : "";
            String nombre = (request.getParameter("nombre") != null) ? Util.setCodificacionCadena(request.getParameter("nombre"),"UTF-8") : "";
            String valor = (request.getParameter("valor") != null) ? request.getParameter("valor") : "0";
            String is_default = (request.getParameter("is_default") != null) ? request.getParameter("is_default") : "N";                 
            this.reponseJson = dao.guardarItemCategoria(idCategoria, nombre, valor, is_default, usuario);          
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private void actualizarItemCategoria() {
        try {
            String idItem = (request.getParameter("idItem") != null) ? request.getParameter("idItem") : "";
            String nombre = (request.getParameter("nombre") != null) ? Util.setCodificacionCadena(request.getParameter("nombre"),"UTF-8") : "";
            String valor = (request.getParameter("valor") != null) ? request.getParameter("valor") : "0";
            String is_default = (request.getParameter("is_default") != null) ? request.getParameter("is_default") : "N";   
            this.reponseJson = dao.actualizarItemCategoria(idItem, nombre, valor, is_default, usuario);            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void cargarGridUndsAdmon() {
        try {
            this.reponseJson = dao.cargarGridUndsAdmon();
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(MaestroProyectoAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void insertarUndAdmon() {
        try {
            String nombre = request.getParameter("nombre") != null ? Util.setCodificacionCadena(request.getParameter("nombre"),"ISO-8859-1") : "";
            String descripcion = request.getParameter("descripcion") != null ? Util.setCodificacionCadena(request.getParameter("descripcion"),"ISO-8859-1") : "";
            this.reponseJson = dao.insertarUndAdmon(nombre, descripcion, usuario);
            /*if (!dao.existeDisciplina(nombre, "0")) {
                this.reponseJson = dao.insertarDisciplina(nombre, usuario);
            } else {
                this.reponseJson = "{\"error\":\" No se cre� la disciplina, ya existe una disciplina con ese nombre\"}";
            }*/
        } catch (Exception ex) {
            Logger.getLogger(MaestroProyectoAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void actualizarUndAdmon() {

        try {
            String id = request.getParameter("id") != null ? request.getParameter("id") : "";
            String nombre = request.getParameter("nombre") != null ? Util.setCodificacionCadena(request.getParameter("nombre"),"ISO-8859-1") : "";
            String descripcion = request.getParameter("descripcion") != null ? Util.setCodificacionCadena(request.getParameter("descripcion"),"ISO-8859-1") : "";
            this.reponseJson = dao.actualizarUndAdmon(id, nombre, descripcion, usuario);
            /*if (!dao.existeDisciplina(nombre, id)) {
                this.reponseJson = dao.actualizarDisciplina(id, nombre, usuario);
            } else {
                this.reponseJson = "{\"error\":\" No se cre� la disciplina, ya existe una disciplina con ese nombre\"}";
            }*/
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(MaestroProyectoAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void activaInactivaUndAdmon() {
        try {
            String id = request.getParameter("id") != null ? request.getParameter("id") : "";
            this.reponseJson = dao.activaInactivaUndAdmon(id, usuario);
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(MaestroProyectoAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }
    
    private void cargarAseguradoras() {
        try {
            this.reponseJson = dao.CargarAseguradoras();
        } catch (Exception e) {
            Logger.getLogger(MaestroProyectoAction.class.getName()).log(Level.SEVERE, null, e);
            e.printStackTrace();
        }
    }

    public void guardarAseguradora() {
        try {
            String nit_aseguradora = (request.getParameter("nit_aseguradora") != null) ? request.getParameter("nit_aseguradora") : "";
            String nombre_aseguradora = (request.getParameter("nombre_aseguradora") != null) ? Util.setCodificacionCadena(request.getParameter("nombre_aseguradora"),"ISO-8859-1") : "";
            this.reponseJson = dao.guardarAseguradora(nit_aseguradora, nombre_aseguradora, usuario);
        } catch (Exception e) {
            e.printStackTrace();
            this.reponseJson = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        }
    }

    private void actualizarAseguradora() {
        try {
            String id = (request.getParameter("id") != null) ? request.getParameter("id") : "";
            String nit_aseguradora = (request.getParameter("nit_aseguradora") != null) ? request.getParameter("nit_aseguradora") : "";
            String nombre_aseguradora = (request.getParameter("nombre_aseguradora") != null) ? Util.setCodificacionCadena(request.getParameter("nombre_aseguradora"),"ISO-8859-1") : "";
            this.reponseJson = dao.actualizarAseguradora(id, nit_aseguradora, nombre_aseguradora, usuario);
        } catch (Exception e) {
            e.printStackTrace();
            this.reponseJson = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        }
    }

    public void activaInactivaAseguradora() {

        try {
            String id = request.getParameter("id") != null ? request.getParameter("id") : "";
            this.reponseJson = dao.activaInactivaAseguradora(id, usuario);
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(MaestroProyectoAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }
    
    private void existeNitAseguradoraEnProveedor() {
        try {
            String nit_aseguradora = (request.getParameter("nit") != null) ? request.getParameter("nit") : "";
            if (dao.existeNitEnProveedor(nit_aseguradora)) {
                this.reponseJson =  "{\"respuesta\":\"SI\"}";
            } else {
                this.reponseJson =  "{\"respuesta\":\"NO\"}";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void filtro_Apus() {
        try {
            String buscar = request.getParameter("buscar") != null ? request.getParameter("buscar") : "";
            String id = request.getParameter("id") != null ? request.getParameter("id") : "";
            String apu_proyecto = request.getParameter("apu_proyecto") != null ? request.getParameter("apu_proyecto") : "";
            String id_solicitud = request.getParameter("id_solicitud") != null ? request.getParameter("id_solicitud") : "";
            String grupo_apus = request.getParameter("grupo_apus") != null ? request.getParameter("grupo_apus") : "";
            if(apu_proyecto.equals("true")){
                this.reponseJson = dao.filtro_Apus(buscar,id, id_solicitud,grupo_apus);
            }else{
                this.reponseJson = dao.filtro_Apus(buscar,id, grupo_apus);
            }
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(MaestroProyectoAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }
    
    private void filtro_Insumo() {
        try {
            String buscar = request.getParameter("buscar") != null ? request.getParameter("buscar") : "";
            String categoria = request.getParameter("categoria") != null ? request.getParameter("categoria") : "";
            String subcategoria = request.getParameter("subcategoria") != null ? request.getParameter("subcategoria") : "";
            String id_tipo_insumo = request.getParameter("id_tipo_insumo") != null ? request.getParameter("id_tipo_insumo") : "";
            this.reponseJson = dao.filtro_Insumo(buscar, categoria , subcategoria, id_tipo_insumo);
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(MaestroProyectoAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

}
