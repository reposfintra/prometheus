/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.opav.controller;
import com.tsp.operation.model.TransaccionService;
import com.tsp.operation.model.beans.Usuario;
import javax.servlet.http.*;
import java.util.*;
import com.tsp.opav.model.services.*;
import com.tsp.util.ExcelApplication;
import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import com.tsp.opav.model.beans.*;


/**
 *
 * @author jose
 */
public class CategoriaOrdenAction extends Action {


   //Opciones dentro de la clase:

    private final int CARGAR_CATEGORIAS = 0;
    private final int CARGAR_CATEGORIAS_ACCIONES = 1;
    private final int CARGAR_SUBCATEGORIAS = 2;
    private final int CARGAR_TIPOSUBCATEGORIAS = 3;
    private final int AGREGAR_FILA = 4;
    private final int ELIMINAR_FILA = 5;
    private final int AGREGAR_SUBCATEGORIA = 6;
    private final int ELIMINAR_SUBCATEGORIA = 7;
    private final int NUEVA_CATEGORIA = 8;
    private final int GUARDAR_DATOS = 9;
    private final int MODIFICAR_DATOS = 10;
    private final int LISTAR_CATEGORIAS = 11;
    private final int LISTAR_SUBCATEGORIAS = 12;
    private final int LISTAR_TIPOSUBCATEGORIAS = 13;
    private final int LISTADO_MATERIALES = 14;
    private final int GENERAR_XLS = 15;
    private final int LISTADOSUBCATEGORIASYTIPOSUBCATEGORIAS = 16;


    CategoriaService    categoriaService;//modificar
    TransaccionService transaccionService;


    String next;
    String loginx;
    Usuario usuario;
    public CategoriaOrdenAction(){
     this.next = "/jsp/opav/cotizacion/listadocategoriaaux.jsp";
    }


    public void run() throws javax.servlet.ServletException {

                    try {
                        int op = (request.getParameter("opcion") != null) ? Integer.parseInt(request.getParameter("opcion").toString()) : -1;
                        HttpSession session = request.getSession();
                        com.tsp.util.Util.logController(((com.tsp.operation.model.beans.Usuario) session.getAttribute("Usuario")).getLogin(), this.getClass().getName());
                        
                        usuario = (Usuario) request.getSession().getAttribute("Usuario");
                        categoriaService= new CategoriaService(usuario.getBd());
                        transaccionService = new TransaccionService(usuario.getBd());
                        loginx=usuario.getLogin();

                        String idcat = request.getParameter("idcat") == null ? "" : request.getParameter("idcat");

                        switch (op) {
                            case CARGAR_CATEGORIAS:
                                    CategoriaService clvsrv = new CategoriaService(usuario.getBd());
                                    ArrayList listadoCategorias = clvsrv.listCategorias();
                                    session.setAttribute("listadoCategorias", listadoCategorias);
                                    this.next = "/jsp/opav/cotizacion/listadocategoriaaux.jsp";
                                    this.dispatchRequest(this.next + "?op=0");
                            break;
                            case CARGAR_CATEGORIAS_ACCIONES:


                                     String est = request.getParameter("est") == null ? "" : request.getParameter("est");
                                     session.setAttribute("estado", est);
                                     clvsrv = new CategoriaService(usuario.getBd());
                                     listadoCategorias = clvsrv.listCategoria(idcat);
                                     if(idcat.equals("0")){
                                     if(listadoCategorias.size()<=0){
                                        int tam =  listadoCategorias.size();
                                        String val = tam+"";
                                        listadoCategorias.add(new String[]{val, "Digite la Descripción", "''"});
                                      }

                                     }

                                    session.setAttribute("listadoCategorias", listadoCategorias);
                                    this.next = "/jsp/opav/cotizacion/listadocategoriaaccionesaux.jsp";
                                    this.dispatchRequest(this.next + "?op=0&est="+est);
                            break;
                            case CARGAR_SUBCATEGORIAS:
                                    this.consultarsubCategorias();
                            break;
                            case CARGAR_TIPOSUBCATEGORIAS:
                                    this.consultartiposubCategorias();
                            break;
                            case AGREGAR_FILA://opcion 4
                                    this.agregarFila();
                            break;
                            case ELIMINAR_FILA:
                                    this.eliminarFila();
                            break;
                            case AGREGAR_SUBCATEGORIA:
                                    this.agregarSubcategoria(); //opcion 6
                            break;
                            case ELIMINAR_SUBCATEGORIA:
                                    this.eliminarSubcategoria();//opcion 7
                            break;
                            case NUEVA_CATEGORIA:
                                    this.nuevaCategoria();//opcion 8
                            break;
                            case GUARDAR_DATOS:
                                    this.guardarDatos();//opcion 9
                            break;
                            case MODIFICAR_DATOS:
                                    this.modificarDatos();//opcion 10
                            break;
                            case LISTAR_CATEGORIAS:
                                    this.buscarCategorias();//opcion 11
                            break;
                            case LISTAR_SUBCATEGORIAS:
                                    this.buscarSubCategorias();//opcion 12
                            break;
                            case LISTAR_TIPOSUBCATEGORIAS:
                                    this.buscarTipoSubCategorias();//opcion 13
                            break;
                            case LISTADO_MATERIALES:
                                    this.listadoMateriales();//opcion 14
                            break;
                            case GENERAR_XLS://opcion 15
                                this.exportarXls();
                                break;
                            case LISTADOSUBCATEGORIASYTIPOSUBCATEGORIAS://opcion 16
                                this.listadoSubcategoriasyTiposub();
                                break;


                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }





    }

/**
 * Dada un id_categoria devuelve las subcategorias
 * @throws Exception
 */
        private void consultarsubCategorias() throws Exception{
        HttpSession session = request.getSession();
        CategoriaService clvsrv = new CategoriaService(usuario.getBd());
        int index = Integer.parseInt(request.getParameter("index").toString());
        String idcat = request.getParameter("idcat");
        String est = request.getParameter("est");
         ArrayList detalles = clvsrv.listSubcategorias(idcat);
         //
         if(detalles.size()<=0){
          int tam =  detalles.size();
          String val = tam+"";
          detalles.add(new String[]{val, "Digite la Descripción", "''"});
         }
          //

         session.setAttribute("subcategorias", detalles);
        response.setContentType("text/plain; charset=utf-8");
        response.setHeader("Cache-Control", "no-cache");
        this.next = "/jsp/opav/cotizacion/listadocategoriaaccionesaux.jsp";
        this.dispatchRequest(next+"?op=1&index="+index+"&id_accion="+idcat+"&estadoCategoria="+est);
    }


/**
 * Devuelve los tiposubcategorias de una subcategoria
 * @throws Exception
 */
         private void consultartiposubCategorias() throws Exception{
        HttpSession session = request.getSession();
        CategoriaService clvsrv = new CategoriaService(usuario.getBd());
        int index = Integer.parseInt(request.getParameter("index").toString());
        int ind = Integer.parseInt(request.getParameter("ind").toString());
        String est = request.getParameter("est");
        String idsubcat = request.getParameter("idsubcat");

         ArrayList detalles = clvsrv.listtipoSubcategorias(idsubcat);

         //
         if(detalles.size()<=0){
          int tam =  detalles.size()+1;
          String val = tam+"";
          detalles.add(new String[]{"0", "Digite la Descripción", "''"});
         }
          //
        session.setAttribute("tiposubcategorias"+index+"_"+ind, detalles);
        response.setContentType("text/plain; charset=utf-8");
        response.setHeader("Cache-Control", "no-cache");
        this.next = "/jsp/opav/cotizacion/listadocategoriaaccionesaux.jsp";
        this.dispatchRequest(next+"?op=2&index="+index+"&id_accion="+idsubcat+"&ind="+ind+"&estadosubCategoria="+est);
    }


/**
 * Permite agregar tiposubcategorias a una lista
 * @throws Exception
 */
     private void agregarFila() throws Exception {
        try {
            HttpSession session = request.getSession();
            int index = Integer.parseInt(request.getParameter("index").toString());
            int ind = Integer.parseInt(request.getParameter("ind").toString());
            String idsubcat = request.getParameter("idsubcat");
            String est = request.getParameter("est");
            ArrayList listadotipoSubcategorias = (ArrayList) session.getAttribute("tiposubcategorias"+index+"_"+ind);

            int tam =  listadotipoSubcategorias.size()+1;
            String val = tam+"";


            //listadotipoSubcategorias.add(new String[]{val, "Digite la Descripción", "''"});
            listadotipoSubcategorias.add(new String[]{"0", "Digite la Descripción", "''"});//Free 51

            session.setAttribute("tiposubcategorias"+index+"_"+ind, listadotipoSubcategorias);
            this.next = "/jsp/opav/cotizacion/material/listadocategoriaaccionesaux.jsp";
            this.dispatchRequest(next + "?op=2&index=" + index + "&id_accion=" + idsubcat + "&ind=" + ind+"&estadosubCategoria="+est);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
     }

/**
 * Permite eliminar filas de tiposubcategorias de un listado
 * @throws Exception
 */
     private void eliminarFila() throws Exception{
         try{

            HttpSession session = request.getSession();
            int index = Integer.parseInt(request.getParameter("index").toString());
            int ind = Integer.parseInt(request.getParameter("ind").toString());
            int borrar = Integer.parseInt(request.getParameter("borrar").toString());
            String idsubcat = request.getParameter("idsubcat");
            String est = request.getParameter("est");
            ArrayList listadotipoSubcategorias = (ArrayList) session.getAttribute("tiposubcategorias"+index+"_"+ind);
            if(listadotipoSubcategorias.size()>1){
            listadotipoSubcategorias.remove(borrar);
            }
            this.next = "/jsp/opav/cotizacion/material/listadocategoriaaccionesaux.jsp";
            this.dispatchRequest(next + "?op=2&index=" + index + "&id_accion=" + idsubcat + "&ind=" + ind+"&estadosubCategoria="+est);

         }catch(Exception ex){
             ex.printStackTrace();
         }
     }

/**
 * Permite agregar una fila a un listado de Subcategorias
 * @throws Exception
 */
     private void agregarSubcategoria() throws Exception {
        try {
            HttpSession session = request.getSession();
            int index = Integer.parseInt(request.getParameter("index").toString());
            String idsubcat = request.getParameter("idsubcat");
            String est = request.getParameter("est");
            ArrayList listadoSubcategorias = (ArrayList) session.getAttribute("subcategorias");
            int tam =  listadoSubcategorias.size()+1;
            String val = tam+"";

            listadoSubcategorias.add(new String[]{"0", "Digite la Descripción", "''"});
            session.setAttribute("subcategorias"+index, listadoSubcategorias);
            this.next = "/jsp/opav/cotizacion/material/listadocategoriaaccionesaux.jsp";
            this.dispatchRequest(next + "?op=1&index=" + index + "&id_accion=" + idsubcat+ "&estadoCategoria=" + est);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
     }



/**
 * Permite eliminar una fila de un listado de subcategorias
 * @throws Exception
 * //opcion 7
 */
     private void eliminarSubcategoria() throws Exception{
         try{

            HttpSession session = request.getSession();
            int index = Integer.parseInt(request.getParameter("index").toString());
            int borrar = Integer.parseInt(request.getParameter("borrar").toString());
            String idsubcat = request.getParameter("idsubcat");
            String est = request.getParameter("est");
            ArrayList listadoSubcategorias = (ArrayList) session.getAttribute("subcategorias");
            if(listadoSubcategorias.size()>1){
            listadoSubcategorias.remove(borrar);
            }

            this.next = "/jsp/opav/cotizacion/material/listadocategoriaaccionesaux.jsp";
            this.dispatchRequest(next + "?op=1&index=" + index + "&id_accion=" + idsubcat + "&estadoCategoria=" + est);

         }catch(Exception ex){
             ex.printStackTrace();
         }
     }


/**
 * Permite agregar una nueva Categoria
 * @throws Exception
 */
    private void nuevaCategoria() throws Exception {
        try {
            HttpSession session = request.getSession();
            int index = Integer.parseInt(request.getParameter("index").toString());
            int ind = Integer.parseInt(request.getParameter("ind").toString());
            String idsubcat = request.getParameter("idsubcat");
            String est = request.getParameter("est");
            ArrayList listadotipoSubcategorias = (ArrayList) session.getAttribute("tiposubcategorias"+index+"_"+ind);

            int tam =  listadotipoSubcategorias.size()+1;
            String val = tam+"";


            listadotipoSubcategorias.add(new String[]{val, "Digite la Descripción", "''"});
            session.setAttribute("tiposubcategorias"+index+"_"+ind, listadotipoSubcategorias);
            this.next = "/jsp/opav/cotizacion/material/listadocategoriaaccionesaux.jsp";
            this.dispatchRequest(next + "?op=2&index=" + index + "&id_accion=" + idsubcat + "&ind=" + ind+"&estadosubCategoria="+est);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
     }


/**
 * Almacena los datos digitados en pantalla en la creacion de Categorias
 * @throws Exception
 */
    private void guardarDatos() throws Exception {

        try {
            HttpSession session = request.getSession();
            int cant_c = Integer.parseInt(request.getParameter("cant_c").toString());
            int cat = Integer.parseInt(request.getParameter("cat").toString());
            CategoriaService clvsrv = new CategoriaService(usuario.getBd());
            String idcatsub = "";
            String statussub = "";
            String sql = "";

            for (int i = 0; i < cant_c; i++) {
                //Inserto primero la Categoría
                String idcat = request.getParameter("idcat" + cat);
                String status = request.getParameter("status" + cat);

                int idcategoria = clvsrv.insertarCategoria(idcat, status, loginx);

                //Proceso para insertar las Subcategorias
                String cs = request.getParameter("cant_s") != null ? request.getParameter("cant_s").toString() : "-1";//Obtengo la Cantidad de subcategorias
                int cant_s = Integer.parseInt(cs);

                for (int j = 0; j < cant_s; j++) {
                    idcatsub = request.getParameter("idcat" + cat + "_" + j);
                    statussub = request.getParameter("status" + cat + "_" + j);
                    int idsubcategoria = clvsrv.insertarSubCategoria(idcategoria, idcatsub, statussub, loginx);

                    String ct = request.getParameter("cant_t" + cat + "_" + j) != null ? request.getParameter("cant_t" + cat + "_" + j).toString() : "-1";//Obtengo la Cantidad de subcategorias
                    int cant_t = Integer.parseInt(ct);

                    for (int t = 0; t < cant_t; t++) {
                        idcatsub = request.getParameter("idcatts" + j + "_" + t);
                        statussub = request.getParameter("statust" + j + "_" + t);
                                sql += clvsrv.insertartipoSubCategoria(idcategoria, idsubcategoria, idcatsub, statussub, loginx);
                    }
                }
            }

            //Ingresa Las tiposubcategorias
                                System.out.println("SQL-------->"+ sql);
                                TransaccionService tService = new TransaccionService(usuario.getBd());
                                tService.crearStatement();
                                tService.getSt().addBatch(sql);
                                tService.execute();


            this.next = "/jsp/opav/cotizacion/listadocategoria.jsp";
            response.getWriter().println(next);


        } catch (Exception ex) {
            ex.printStackTrace();

        }

    }



/**
 * Permite modificar la información de una categoria
 * @throws Exception
 */
        private void modificarDatos() throws Exception {

        try {
            HttpSession session = request.getSession();
            int cant_c = Integer.parseInt(request.getParameter("cant_c").toString());
            int cat = Integer.parseInt(request.getParameter("cat").toString());
            CategoriaService clvsrv = new CategoriaService(usuario.getBd());
            String idcatsub = "";
            String statussub = "";
            String codcatsub = "";
            String codcatts = "";
            int idsubcategoria = 0;
            int idtiposubcategoria = 0;
            String sql = "";

            for (int i = 0; i < cant_c; i++) {
                //Actualizo primero la Categoría
                String idcat = request.getParameter("idcat" + cat);
                int idcategoria = Integer.parseInt(request.getParameter("codcat"+cat).toString());
                String status = request.getParameter("status" + cat);
                clvsrv.updateCategoria(idcat, status, loginx, idcategoria);//Actualiza la Categoria

                //Proceso para insertar las Subcategorias
                String cs = request.getParameter("cant_s") != null ? request.getParameter("cant_s").toString() : "-1";//Obtengo la Cantidad de subcategorias
                int cant_s = Integer.parseInt(cs);

                for (int j = 0; j < cant_s; j++) {
                    idcatsub = request.getParameter("idcat" + cat + "_" + j);
                    codcatsub = request.getParameter("codcatsub" + cat + "_" + j);
                    statussub = request.getParameter("status" + cat + "_" + j);

                    if(codcatsub.equals("0")){
                    idsubcategoria = clvsrv.insertarSubCategoria(idcategoria, idcatsub, statussub, loginx);//Inserta la Subcategoria si no existe
                    }else{
                    idsubcategoria =  Integer.parseInt(codcatsub.toString());
                    //clvsrv.updateSubCategoria(idcat, status, loginx, idcategoria, idsubcategoria);//Actualiza la subcategoria
                    clvsrv.updateSubCategoria(idcatsub, status, loginx, idcategoria, idsubcategoria);//FREE 52
                    }

                    String ct = request.getParameter("cant_t" + cat + "_" + j) != null ? request.getParameter("cant_t" + cat + "_" + j).toString() : "-1";//Obtengo la Cantidad de subcategorias
                    int cant_t = Integer.parseInt(ct);

                    for (int t = 0; t < cant_t; t++) {
                        idcatsub = request.getParameter("idcatts" + j + "_" + t);//Descripción
                        codcatts = request.getParameter("codcatts" + j + "_" + t);
                        statussub = request.getParameter("statust" + j + "_" + t);

                        if(codcatts.equals("0")){
                        sql += clvsrv.insertartipoSubCategoria(idcategoria, idsubcategoria, idcatsub, statussub, loginx);//inserta el tiposubcategoria
                        }else{
                        idtiposubcategoria = Integer.parseInt(codcatts.toString());
                        sql += clvsrv.updatetipoSubCategoria(idcatsub, status, loginx, idcategoria, idsubcategoria, idtiposubcategoria);//Actualiza el tiposubcategoria
                        }


                    }
                }
            }

            //Ingresa Las tiposubcategorias
                                System.out.println("SQL-------->"+ sql);
                                TransaccionService tService = new TransaccionService(usuario.getBd());
                                tService.crearStatement();
                                tService.getSt().addBatch(sql);
                                tService.execute();


            this.next = "/jsp/opav/cotizacion/listadocategoria.jsp";
            response.getWriter().println(next);


        } catch (Exception ex) {
            ex.printStackTrace();

        }

    }




  private void buscarCategorias() throws Exception {
        String xml = "";
        CategoriaService clvsrv = new CategoriaService(usuario.getBd());
        clvsrv.listadoCategorias();
        Map categorias = clvsrv.getListadoCategorias();

        xml = "<?xml version=\"1.0\" encoding=\"iso-8859-1\"?>";
        xml += "<mensaje>";
        xml += this.getXMLCategorias(categorias);
        xml += "</mensaje>";

        response.setContentType("text/xml");
        response.setHeader("Cache-Control", "no-cache");
        response.getWriter().println(xml);
    }


   private String getXMLCategorias(Map map)throws Exception{
        String xml = "";
        Iterator categorias = map.keySet().iterator();
        while (categorias.hasNext()){
            String cat = categorias.next().toString();
            xml += "<categoria codcat=\""+map.get(cat).toString()+"\">"+cat+"</categoria>";
        }
        return xml;
    }



  private void buscarSubCategorias() throws Exception {
        String xml = "";
        int codcat = request.getParameter("cat")!=null?(!request.getParameter("cat").equals("")?Integer.parseInt(request.getParameter("cat").toString()):0):0;
        CategoriaService clvsrv = new CategoriaService(usuario.getBd());
        clvsrv.listadoSubCategorias(codcat);
        Map categorias = clvsrv.getListadoSubCategorias();


        xml = "<?xml version=\"1.0\" encoding=\"iso-8859-1\"?>";
        if(categorias.size()>0){
        xml += "<mensaje>";
        xml += this.getXMLSubCategorias(categorias);
        xml += "</mensaje>";
        }else{
        xml += "<mensaje>";
        xml +="<subcategoria codsubcat= '0' >No hay Subcategorias</subcategoria>";
        xml += "</mensaje>";
        }
        response.setContentType("text/xml");
        response.setHeader("Cache-Control", "no-cache");
        response.getWriter().println(xml);
    }


   private String getXMLSubCategorias(Map map)throws Exception{
        String xml = "";
        Iterator categorias = map.keySet().iterator();
        while (categorias.hasNext()){
            String cat = categorias.next().toString();
            xml += "<subcategoria codsubcat=\""+map.get(cat).toString()+"\">"+cat+"</subcategoria>";
        }
        return xml;
    }



     private void buscarTipoSubCategorias() throws Exception {
        String xml = "";
        int codcat = Integer.parseInt(request.getParameter("cat").toString());
        int codsubcat = Integer.parseInt(request.getParameter("subcat").toString());

        CategoriaService clvsrv = new CategoriaService(usuario.getBd());
        clvsrv.listadoTipoSubCategorias(codcat, codsubcat);
        Map categorias = clvsrv.getListadoTipoSubCategorias();


        xml = "<?xml version=\"1.0\" encoding=\"iso-8859-1\"?>";
        if(categorias.size()>0){
        xml += "<mensaje>";
        xml += this.getXMLTipoSubCategorias(categorias);
        xml += "</mensaje>";
        }else{
        xml += "<mensaje>";
        xml +="<tiposubcategoria codsubcat= '0' >No hay TipoSubcategorias</tiposubcategoria>";
        xml += "</mensaje>";
        }
        response.setContentType("text/xml");
        response.setHeader("Cache-Control", "no-cache");
        response.getWriter().println(xml);
    }


   private String getXMLTipoSubCategorias(Map map)throws Exception{
        String xml = "";
        Iterator categorias = map.keySet().iterator();
        while (categorias.hasNext()){
            String cat = categorias.next().toString();
            xml += "<tiposubcategoria codtiposubcat=\""+map.get(cat).toString()+"\">"+cat+"</tiposubcategoria>";
        }
        return xml;
    }


   private void listadoMateriales () throws Exception{
       String parametro = "";
       int filtro = 0;
       HttpSession session = request.getSession();
       this.next = "/jsp/opav/cotizacion/material/listadomaterialaux.jsp?op=0";
       request.setAttribute("msg", "Producto buscado!");
       MaterialesService pserv = new MaterialesService(usuario.getBd());
       try {
           parametro = request.getParameter("parametro") != null ? request.getParameter("parametro") : "";
           filtro = request.getParameter("filtro") != null ? Integer.parseInt(request.getParameter("filtro")) : 0;
           int idcat = (request.getParameter("cat") != null || !request.getParameter("cat").equals("")) ? Integer.parseInt(request.getParameter("cat")) : 0;
           int idsubcat = (request.getParameter("subcat") != null || !request.getParameter("subcat").equals("")) ? Integer.parseInt(request.getParameter("subcat")) : 0;
           int idtiposubcat = (request.getParameter("tscat") != null || !request.getParameter("tscat").equals("")) ? Integer.parseInt(request.getParameter("tscat")) : 0;
           String tipomat = request.getParameter("tipomat") != null ? request.getParameter("tipomat") : "M";

           session.setAttribute("resultado", pserv.buscarPorMatCat(filtro, parametro, idcat, idsubcat, idtiposubcat,tipomat));

           this.dispatchRequest(this.next);
       } catch (Exception ec) {
           System.out.println("Error en el run del action: " + ec.toString());
       }

   }




       public void exportarXls() throws Exception{
        try {
            ///
            HttpSession session = request.getSession();
            CategoriaService clvsrv = new CategoriaService(usuario.getBd());
            ArrayList listadoCategorias = clvsrv.listCategorias();
            session.setAttribute("listadoCategorias", listadoCategorias);
            this.next = "/jsp/opav/cotizacion/listadocategoria.jsp";
           ////
            session.setAttribute("msg", "Excel Generado!");
            session.setAttribute("list_categorias", clvsrv.categoriasExcel());
            this.generarXls();

            //this.dispatchRequest(this.next + "?op=0");

            response.getWriter().println(next);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }




        /**
     * Genera el xls del estado de cuenta
     * @param user Usuario que lo genera
     * @param listafacts Lista de facturas
     * @param listaing Lista de ingresos
     * @param listahead Datos para el encabezado
     * @param sal Saldo del negocio
     * @param vec Otros datos del cliente que se incluyen
     * @throws Exception Cuando hay un error
     */
    private void generarXls() throws Exception{
        String directorio = "";
        System.out.println("inicia generacion de xls");
        HttpSession session = request.getSession();
        ArrayList ver = (ArrayList) session.getAttribute("list_categorias");


        try {
            directorio = this.directorioArchivo(loginx, "_Categorias_", "xls");
            System.out.println("Elaborando directorio");
            ExcelApplication excel = this.instanciar("Listado Categorias");
            excel.setDataCell(0, 0, "Listado de Categorias:");
            excel.setCellStyle(0, 0, excel.getStyle("estilo2"));
            excel.setDataCell(1, 0, "id Categoria");
            excel.setCellStyle(1, 0, excel.getStyle("estilo2"));
            excel.setDataCell(1, 1, "Descripcion Categoria");
            excel.setCellStyle(1, 1, excel.getStyle("estilo2"));
            excel.setDataCell(1, 2, "id SubCategoria");
            excel.setCellStyle(1, 2, excel.getStyle("estilo2"));
            excel.setDataCell(1, 3, "Descripcion SubCategoria");
            excel.setCellStyle(1, 3, excel.getStyle("estilo2"));
            excel.setDataCell(1, 4, "id TipoSubCategoria");
            excel.setCellStyle(1, 4, excel.getStyle("estilo2"));
            excel.setDataCell(1, 5, "Descripcion TipoSubCategoria");
            excel.setCellStyle(1, 5, excel.getStyle("estilo2"));

            System.out.println("cabecera lista");
            int filaAct = 3;

            Material spl = null;
            //parte 1 ingresos
            for(int i=0;i<ver.size();i++){

                spl = (Material) ver.get(i);
                excel.setDataCell(filaAct, 0, spl.getIdcategoria());
                excel.setCellStyle(filaAct, 0, excel.getStyle("estilo3"));

                excel.setDataCell(filaAct, 1, spl.getDesc_categoria());
                excel.setCellStyle(filaAct, 1, excel.getStyle("estilo3"));

                excel.setDataCell(filaAct, 2, spl.getIdsubcategoria());
                excel.setCellStyle(filaAct, 2, excel.getStyle("estilo3"));

                excel.setDataCell(filaAct, 3, spl.getDesc_subcat());
                excel.setCellStyle(filaAct, 3, excel.getStyle("estilo3"));

                excel.setDataCell(filaAct, 4, spl.getIdtiposubcategoria());
                excel.setCellStyle(filaAct, 4, excel.getStyle("estilo3"));

                excel.setDataCell(filaAct, 5, spl.getDesc_tiposub());
                excel.setCellStyle(filaAct, 5, excel.getStyle("estilo3"));


                spl = null;
                filaAct += 1;
            }

            excel.saveToFile(directorio);
            System.out.println("guardado listo");
        }
        catch (Exception e) {
            throw new Exception("Error al generar el archivo xls: "+e.toString());
        }
        System.out.println("fin proceso xls");
    }

    /**
     * Genera la ruta en que se guardara el archivo
     * @param user usuario que genera el archivo
     * @param cons consecutivo de la cotizacion
     * @param extension La extension del archivo
     * @return String con la ruta en la que queda el archivo
     * @throws Exception cuando ocurre algun error
     */
    private String directorioArchivo(String user,String cons,String extension) throws Exception{
        String ruta = "";
        try {
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            ruta = rb.getString("ruta") + "/exportar/migracion/" + user;
            SimpleDateFormat fmt = new SimpleDateFormat("yyyMMdd_hhmmss");
            File archivo = new File( ruta );
            if (!archivo.exists()) archivo.mkdirs();
            ruta = ruta + "/" + "listado_categorias_" + cons +"_" + fmt.format( new Date() ) +"."+extension;
        }
        catch (Exception e) {
            throw new Exception("Error al generar el directorio: "+e.toString());
        }
        return ruta;
    }

    /**
     * Crea un objeto tipo ExcelApplication
     * @param descripcion Nombre de la hoja principal del libro
     * @return Objeto ExcelApplication creado
     * @throws Exception Cuando hay error
     */
    private ExcelApplication instanciar(String descripcion) throws Exception{
        ExcelApplication excel = new ExcelApplication();
        try{
            excel.createSheet(descripcion);
            excel.createFont("Titulo", "Arial", (short)1, true, (short)12);
            excel.createFont("Subtitulo", "Verdana", (short)0, true, (short)10);
            excel.createFont("Contenido", "Verdana", (short)0, false, (short)10);

            excel.createColor((short)11, (byte)255, (byte)255, (byte)255);//Blanco
            excel.createColor((short)9, (byte)26, (byte)126, (byte)0);//Verde dark
            excel.createColor((short)10, (byte)37, (byte)69, (byte)255);//Azul oscuro

            excel.createStyle("estilo1", excel.getFont("Titulo"), (short)10, true, "@");
            excel.createStyle("estilo2", excel.getFont("Subtitulo"), (short)9, true, "@");
            excel.createStyle("estilo3", excel.getFont("Contenido"), (short)11, true, "@");

        }
        catch(Exception e){
            throw new Exception("Error al instanciar el objeto: "+e.toString());
        }
        return excel;
    }





     private void listadoSubcategoriasyTiposub () throws Exception{
       HttpSession session = request.getSession();
       String ind = request.getParameter("ind") != null ? request.getParameter("ind") : "";
       String tipo = request.getParameter("tipo") != null ? request.getParameter("tipo") : "";
       this.next = "/jsp/opav/cotizacion/material/filtrocotizacion/listadosubtiposubaux.jsp?op=0&ind="+ind+"&tipo="+tipo;
       request.setAttribute("msg", "Datos buscados!");
       CategoriaService clvsrv = new CategoriaService(usuario.getBd());
       try {
           int idcat = (request.getParameter("cat") != null || !request.getParameter("cat").equals("")) ? Integer.parseInt(request.getParameter("cat")) : 0;

           session.setAttribute("resultado", clvsrv.listadoSubTiposub(idcat));

           this.dispatchRequest(this.next);
       } catch (Exception ec) {
           System.out.println("Error en el run del action: " + ec.toString());
       }

   }





}