package com.tsp.opav.controller;

import java.io.IOException;
import javax.servlet.http.HttpSession;
import com.tsp.opav.model.beans.*;
import com.tsp.opav.model.services.ElectricaribeOfertaService;
import com.tsp.opav.model.services.InterventorService;
import com.tsp.operation.model.beans.Usuario;
import java.util.Date;
import com.tsp.util.Util;
//import org.apache.jasper.tagplugins.jstl.Util;

public class ElectricaribeOfertaAction extends Action{
    InterventorService interventorService;//interventor jjcastro
    public ElectricaribeOfertaAction(){
        interventorService = new InterventorService();//interventor jjcastro
    }

    public void run(){

        HttpSession session     = request.getSession();
        Usuario     usuario     = (Usuario) session.getAttribute("Usuario");
        String      msjGenerar  = "";
        String      msjPdf      = "PDF creado exitosamente";
        String      msjAnular   = "";
        String      num         = "";
        OfertaElca  oelca;
        String msn= "";//20100513
        try {
            if(request.getParameter("opcion").equals("1")){
                
                try {
                    String globales = request.getParameter("globales");
                    num = request.getParameter("num_oferta");

                    ElectricaribeOfertaService eos = new ElectricaribeOfertaService(usuario.getBd());
                    DatosOferta ofertaInfo = eos.ofertaInfo(num);
                    Date a = Util.ConvertiraDate1(ofertaInfo.getFechaGeneracion().substring(0, 10));
                    Date b = Util.ConvertiraDate1("2012-04-23");
                    String tipo2="";
                    String tipo="";
                    
                    // Se verifica si la fecha de creacion de la oferta es menor a '2012-04-23'
                    // Si cumple entonces utiliza el formato de documento anterior y si no lo cumple
                    // usa el nuevo formato.
                    if (num != null) {                       

                        if (a.before(b)) {
                            model.electricaribeOfertaSvc_ant.iniciar(request.getParameter("num_oferta"), usuario.getLogin());
                            model.electricaribeOfertaSvc_ant.setGlobales(globales);
                            tipo2 = model.electricaribeOfertaSvc_ant.getOfferDao().getOferta().getTipo_solicitud();
                        } else {
                            if (a.after(b)) {
                                model.ElectricaribeOfertaSvc.iniciar(request.getParameter("num_oferta"), usuario.getLogin());
                                model.ElectricaribeOfertaSvc.setGlobales(globales);
                                
                            }else{
                                model.ElectricaribeOfertaSvc.iniciar(request.getParameter("num_oferta"), usuario.getLogin());
                                model.ElectricaribeOfertaSvc.setGlobales(globales);
                            }
                            tipo = model.ElectricaribeOfertaSvc.getOfferDao().getOferta().getTipo_solicitud() ;
                        }

                        
                        

                        if (tipo.equals("Emergencia")||tipo2.equals("Emergencia")) {
                            if (a.before(b)) {
                                model.electricaribeOfertaSvc_ant.doEmergencyPDF();
                            } else {
                                if (b.before(a)) {
                                    model.ElectricaribeOfertaSvc.PDFEmergencia();
                                }else{
                                    model.ElectricaribeOfertaSvc.PDFEmergencia();
                                }
                            }
                        } else {
                            if (a.before(b)) {
                                model.electricaribeOfertaSvc_ant.doOfferPDF();
                            } else {
                                if (b.before(a)) {
                                    model.ElectricaribeOfertaSvc.PDF();
                                }else{
                                    model.ElectricaribeOfertaSvc.PDF();
                                }
                            }
                        }

                        if (a.before(b)) {
                            model.electricaribeOfertaSvc_ant.cerrar();
                        } else {
                            if (b.before(a)) {
                                model.ElectricaribeOfertaSvc.cerrar();
                            }else{
                                model.ElectricaribeOfertaSvc.cerrar();
                            }
                        }
                    }
                } catch (Exception e) {
                    msjPdf = model.ElectricaribeOfertaSvc.returnNext() + model.ElectricaribeOfertaSvc.returnNext();
                }
            }

            if(request.getParameter("opcion").equals("2")){
                try {
                    if ((request.getParameter("id")!=null) &&
                        (request.getParameter("nom_oferta")!=null) &&
                        (request.getParameter("text_consi")!=null) &&
                        (request.getParameter("consideraciones")!=null) ){

                        /*interventor jjcastro
                        String interventor = "";
                        interventor = interventorService.obtenerInterventor(request.getParameter("id"), "Oferta");
                        if (!interventor.equals("")) {
                        this.interventorService.updateOfertasInteventor(request.getParameter("id"), interventor, usuario.getLogin());
                        }
                        *///interventor jjcastro

                        //JJCASTRO EMERGENCIA
                       String tipo = "";
                        num = request.getParameter("num_oferta")!=null?request.getParameter("num_oferta"):"";
                        if (!num.equals("")) {
                            tipo = model.ElectricaribeOfertaSvc.tipoOferta(num);
                        }
                        //JJCASTRO EMERGENCIA


                        oelca = new OfertaElca();
                        oelca.setId_solicitud(request.getParameter("id"));
                        oelca.setNombre_solicitud(request.getParameter("nom_oferta"));
                        oelca.setOtras_consideraciones(request.getParameter("text_consi"));
                        oelca.setConsideraciones(request.getParameter("consideraciones"));
                        oelca.setOficial(request.getParameter("meses_mora"));
                        oelca.setConsecutivo_oferta(request.getParameter("consecutivo"));
                        int caso = 0;

                        if(request.getParameter("valh").length() > 0 && !oelca.getConsecutivo_oferta().endsWith("R")){

                            caso = 1;

                        }
                        model.ElectricaribeOfertaSvc.actualizarOferta(usuario, oelca, caso, tipo);

                        msjGenerar = model.ElectricaribeOfertaSvc.returnNext();

                        oelca = null;
                    }
                }
                catch (Exception e){
                    msjGenerar = "Error generando la oferta";
                }
            }

            if(request.getParameter("opcion").equals("3")){
                try {
                    this.dispatchRequest("/jsp/delectricaribe/pdf_electricaribe.jsp");
                }
                catch (Exception Exception){
                }
            }

            if(request.getParameter("opcion").equals("4")){
                try {
                    String[] solicitudes = request.getParameter("solicitudes").split(",");
                    boolean sw=true;
                    if (solicitudes.length > 0
                            && (request.getParameter("text_consi") != null)) {
                        for (int i = 0; i < solicitudes.length; i++) {
                            String tem="";
                            oelca = new OfertaElca();
                            oelca.setId_solicitud(solicitudes[i]);
                            oelca.setOtras_consideraciones(request.getParameter("text_consi"));
                            oelca.setConsideraciones(request.getParameter("anulaciones"));
                            tem=model.ElectricaribeOfertaSvc.actualizarAnulacionOferta(oelca, false, usuario.getLogin());
                            if(!tem.substring(0, 1).equals("1")){
                                msjAnular=msjAnular+"\n"+tem;
                                sw=false;
                            }else{
                                if(!tem.substring(1).trim().equals("")){
                                msjAnular=msjAnular+"\n"+tem.substring(1);
                                }
                            }
                            oelca = null;
                        }
                        if(sw){
                            msjAnular="Las Solicitudes fueron denegadas con exito\n"+msjAnular;
                        }
                    }
                } catch (Exception e) {
                    System.out.println("Error anulando oferta" + e.toString() + "_" + e.getMessage());
                    msjAnular = "Error denegando la oferta";
                }
            }

            if(request.getParameter("opcion").equals("5")){


                try {
                    if ((request.getParameter("id_solici")!=null) &&
                         (request.getParameter("idnics")!=null)){

                        String idsol = request.getParameter("id_solici");
                        String arg[] = request.getParameter("idnics").split("_");


                        msn = model.ElectricaribeOfertaSvc.actualizarCambioClienteSol(arg[0],idsol , arg[1]);


                        int estado = Integer.valueOf(request.getParameter("est"));

                        if(estado > 60){

                           msn += "\n Se debe volver a realizar la aceptación de pagos para actualizar los nics. \n";
                        }

                        if(estado > 40){

                            msn += "Se debe volver a generar el pdf de la oferta. \n";
                        }

                        if(estado > 70){

                            msn += "Se debe volver a generar el pdf de la orden de trabajo. \n";
                        }



                    }
                } catch (Exception e){
                  e.printStackTrace();
                  msn = "Transaccion no exitosa";
                }
            }
	   if (request.getParameter("opcion").equals("7")) {
                boolean ok = false;
                String res = "no";
                try {

                    num = request.getParameter("num_oferta");
                    ok = model.ElectricaribeOfertaSvc.reevaluarOferta(num, usuario.getLogin());
                    System.out.print("respuesta " + ok);
                    if (ok) {
                        res = "si";
                    }
                    response.setContentType("text/plain");
                    response.setHeader("Cache-Control", "no-cache");
                    response.getWriter().println(res);

                } catch (Exception Exception) {
                    response.setContentType("text/plain");
                    response.setHeader("Cache-Control", "no-cache");
                    response.getWriter().println(res);
                }
            }

        } catch (Exception ex) {
            msjGenerar = "Error, hay una excepcion";
            msjPdf = "Error, hay una excepcion";
            msjAnular = "Error, hay una excepcion";
        }


        /*
         * Lo siguiente es para mandar las respuestas
         * al metodo Ajax.Request que esta en la pagina.
         */

        if(request.getParameter("opcion").equals("1")){
            try {
                response.setContentType("text/plain");
                response.setHeader("Cache-Control", "no-cache");
                response.getWriter().println(msjPdf);
            }
            catch (IOException ex) {
            }
        }

        if(request.getParameter("opcion").equals("2")){
            try {
                response.setContentType("text/plain");
                response.setHeader("Cache-Control", "no-cache");
                response.getWriter().println(msjGenerar);
            }
            catch (IOException ex) {
            }
        }

         if(request.getParameter("opcion").equals("4")){
            try {
                response.setContentType("text/plain");
                response.setHeader("Cache-Control", "no-cache");
                response.getWriter().println(msjAnular);
            }
            catch (IOException ex) {
                System.out.println("error raro: "+ex.toString()+"_"+ex.getMessage());
            }
        }

        if(request.getParameter("opcion").equals("5")){
            try {
                response.setContentType("text/plain");
                response.setHeader("Cache-Control", "no-cache");
                response.getWriter().println(msn);
            }
            catch (IOException ex) {
                System.out.println("error raro: "+ex.toString()+"_"+ex.getMessage());
            }
        }

    }

}