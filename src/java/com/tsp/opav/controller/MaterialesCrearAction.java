/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.opav.controller;

import com.tsp.exceptions.InformationException;
import java.util.*;
import com.tsp.util.Util;
import com.tsp.opav.model.beans.*;
import com.tsp.opav.model.services.*;
import com.tsp.operation.model.beans.Email2;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.services.EmailSendingEngineService;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import com.tsp.util.ExcelApplication;
import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.util.Region;

import java.io.*;
import java.sql.Timestamp;
import javax.swing.GroupLayout.Alignment;

/**
 *
 * @author Rhonalf
 * @modificado Pablo Bassil
 * Organizado por Ing. Jose Castro
 */
public class MaterialesCrearAction extends Action {

    private final int CREAR_MATERIAL = 0;
    private final int MODIFICAR_MATERIAL = 1;
    private final int CONSULTAR_MATERIAL = 2;
    private final int CREAR_NUEVO = 3;
    private final int GENERAR_XLS = 4;
    private final int CARGAR_CERTIFICADORES = 5;
    private final int ACTUALIZAR_LISTA_PRECIOS = 6;
    private final int ACTUALIZAR_LISTA_PRECIOS_INACTIVIDAD = 7;

    String descr = "";
    double pre = 0.0;
    double pre_compra = 0.0;
    String consec = "";
    String medida = "UNIDADES";
    String tipo = "M";
    String cat = "";//091222
    MaterialesService pserv;
    String loginx;
    String next;
    String uni_empaque;
    NegociosApplusService napse;
    String reg_status = "";
    String alcance = "";
    String conse_mat = "";

    String certificado = "";
    String ente_certificador = "";
    HttpSession session=null;
    String  directorio = "";
    String ruta="";
    String  archivo="";
    Usuario usuario;

    public MaterialesCrearAction() {
        next = "/jsp/opav/cotizacion/material/buscar_producto.jsp";
    }

    public void run() throws ServletException, InformationException {

        try {
            directorio = Util.directorioArchivo("DOCUMENTOS");
            ruta=directorio+"/";
            archivo="ListadoMaterial.xls";
            session = request.getSession();
            usuario = (Usuario) session.getAttribute("Usuario");
            pserv = new MaterialesService(usuario.getBd());
            napse = new NegociosApplusService(usuario.getBd());
            loginx = usuario.getLogin();
            int op = (request.getParameter("opcion") != null) ? Integer.parseInt(request.getParameter("opcion").toString()) : -1;

            switch (op) {
                case CREAR_MATERIAL:
                    this.crearMaterial();
                    break;
                case MODIFICAR_MATERIAL:
                    this.updateMaterial();
                    break;
                case CONSULTAR_MATERIAL:
                    this.consultarMaterial();
                    break;
                case CREAR_NUEVO:
                    this.crearNuevo();
                    break;
                case GENERAR_XLS:
                    this.exportarXls();
                    break;
                case CARGAR_CERTIFICADORES:
                    this.cargarCertificadores();//opcion 5
                    break;

                case ACTUALIZAR_LISTA_PRECIOS:
                    this.updateListaPreciosMaterial();//opcion
                    break;

                case ACTUALIZAR_LISTA_PRECIOS_INACTIVIDAD:
                    this.updateListaPreciosMaterialInactividad();//opcion
                    break;

            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     *
     * @throws Exception
     */
    public void crearMaterial() throws Exception {

        try {
            request.setAttribute("msg", "Material ingresado correctamente");
            descr = request.getParameter("descripcion");
            reg_status = request.getParameter("status");
            pre = Double.parseDouble(request.getParameter("precio"));
            pre_compra = Double.parseDouble(request.getParameter("precio_compra")!=null ? request.getParameter("precio_compra"):"0"    );
            consec = request.getParameter("producto");
            consec = pserv.contarProductos();//20100212temporal
            medida = request.getParameter("medida");
            tipo = request.getParameter("tipo");
            alcance = request.getParameter("alcance")!=null?request.getParameter("alcance"):"";
            uni_empaque = request.getParameter("uni_empaque")!=null?request.getParameter("uni_empaque"):"";


            //-------------ENTE CERFIFICADOR
            certificado = request.getParameter("certificado")!=null?(request.getParameter("certificado").equals("N")?"N":"S"):"N";//CAMBIO TROCHA
            ente_certificador = request.getParameter("ente_certi")!=null?request.getParameter("ente_certi"):"";




            HttpSession session = request.getSession();
            session.setAttribute("datosMaterial", null);

            int codcat = 0;
            int codsubcat = 0;
            int codtiposubcat = 0;



            if(!tipo.equals("D")){
            /*
            codcat = Integer.parseInt(request.getParameter("categoria").toString());
            codsubcat = Integer.parseInt(request.getParameter("subcategoria").toString());
            codtiposubcat = Integer.parseInt(request.getParameter("tiposubcategoria").toString());*/

            codcat = Integer.parseInt(request.getParameter("categoria")!=null&&request.getParameter("categoria").equals("")==false?request.getParameter("categoria"):"0");//TROCHA 2
            codsubcat = Integer.parseInt(request.getParameter("subcategoria")!=null&&request.getParameter("subcategoria").equals("")==false?request.getParameter("subcategoria"):"0");//TROCHA 2
            codtiposubcat = Integer.parseInt(request.getParameter("tiposubcategoria")!=null&&request.getParameter("tiposubcategoria").equals("")==false?request.getParameter("tiposubcategoria"):"0");//TROCHA2

            }



                pserv.insertarProducto(reg_status, descr, pre, consec, tipo, medida, loginx, codcat, codsubcat, codtiposubcat, uni_empaque, alcance, certificado, ente_certificador,pre_compra);//0912226  //3 Agosto 2010 //28 08 2010
                //request.setAttribute("msg", "Información Ingresada Correctamente");


            this.next = "/jsp/opav/cotizacion/material/crear_producto2.jsp" ;
            //this.next += "?tipo="+tipo+"&idcat="+codcat+"&idsubcat="+codsubcat+"&idts="+codtiposubcat+"&pag=consulta";
            this.next += "?tipo="+tipo+"&idcat="+codcat+"&idsubcat="+codsubcat+"&idts="+codtiposubcat+"&pag=consulta"+"&msg=Material Creado con Exito...";


            this.dispatchRequest(this.next);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    /**
     *
     * @throws Exception
     *
     */
    public void updateMaterial() throws Exception {

        try {

            request.setAttribute("msg", "Material actualizado correctamente");
            descr = request.getParameter("descripcion");
            reg_status = request.getParameter("status");
            pre = Double.parseDouble(request.getParameter("precio"));
             pre_compra = Double.parseDouble(request.getParameter("precio_compra")!=null ? request.getParameter("precio_compra"):"0"    );
            consec = request.getParameter("producto");
           // consec = pserv.contarProductos();//20100212temporal
            medida = request.getParameter("medida");
            tipo = request.getParameter("tipo");
            alcance = request.getParameter("alcance")!=null?request.getParameter("alcance"):"";//CAMBIO TROCHA
            uni_empaque = request.getParameter("uni_empaque")!=null?request.getParameter("uni_empaque"):"";
            String exproducto = request.getParameter("exproducto");

                        //-------------ENTE CERFIFICADOR
            certificado = request.getParameter("certificado")!=null?(request.getParameter("certificado").equals("N")?"N":"S"):"N";//CAMBIO TROCHA
            ente_certificador = request.getParameter("ente_certi")!=null?request.getParameter("ente_certi"):"";

            int codcat = 0;
            int codsubcat = 0;
            int codtiposubcat = 0;

            if(!tipo.equals("D")){

//            codcat = Integer.parseInt(request.getParameter("categoria").toString());

            codcat = Integer.parseInt(request.getParameter("categoria")!=null&&request.getParameter("categoria").equals("")==false?request.getParameter("categoria"):"0");//TROCHA 2

            codsubcat = Integer.parseInt(request.getParameter("subcategoria")!=null&&request.getParameter("subcategoria").equals("")==false?request.getParameter("subcategoria"):"0");
            codtiposubcat = Integer.parseInt(request.getParameter("tiposubcategoria")!=null&&request.getParameter("tiposubcategoria").equals("")==false?request.getParameter("tiposubcategoria"):"0");//TROCHA2

            }

            napse.modificarProducto(reg_status, exproducto, consec, descr, pre, tipo, medida, loginx, codcat, codsubcat, codtiposubcat, uni_empaque, alcance , certificado, ente_certificador,pre_compra); // 28 08 2010


            this.conse_mat = exproducto;
            this.consultarMaterial();

           
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }



    public void consultarMaterial() throws Exception {
        try {
            HttpSession session = request.getSession();
            request.setAttribute("msg", "");
            String opcion_pagina = request.getParameter("opcion_pagina")!=null?request.getParameter("opcion_pagina"):"";


            String idmat="";
            if(!conse_mat.equals("")){

                idmat = model.NegociosApplusService.getId(conse_mat);
                idmat= conse_mat;

            }else{
            idmat = request.getParameter("idmat") != null ? request.getParameter("idmat") : "";
            }
            //this.next = "/jsp/opav/cotizacion/material/crear_producto2.jsp?opcion_pagina="+opcion_pagina+"&idmat="+idmat ;
            this.next = "/jsp/opav/cotizacion/material/crear_producto2.jsp?opcion_pagina="+opcion_pagina+"&idmat="+idmat +"&msg=Material Creado con Exito..."; //-- MOD MATERIAL JCASTRO

            Material mat = model.NegociosApplusService.getMaterial(idmat);
            session.setAttribute("datosMaterial", mat);
            this.conse_mat = "";

            this.dispatchRequest(this.next);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    public void crearNuevo() throws Exception {
        try {
            HttpSession session = request.getSession();
            request.setAttribute("msg", "");
            String idmat = request.getParameter("idmat") != null ? request.getParameter("idmat") : "";


            Material mat = model.NegociosApplusService.getMaterial(idmat);
            session.setAttribute("datosMaterial", null);

            this.next = "/jsp/opav/cotizacion/material/crear_producto2.jsp" ;
           //this.next += "&tipo=M&idcat=33&idsubcat=12&idts=9";
            response.getWriter().println(next);

        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }



    public void exportarXls() throws Exception{

       String parametro = "";
       int filtro = 0;
       HttpSession session = request.getSession();
       this.next = "/jsp/opav/cotizacion/material/listadomaterialaux.jsp?op=0";
       request.setAttribute("msg", "Excel Generado!");
       //MaterialesService pserv = new MaterialesService(usuario.getBd());
       try {
           parametro = request.getParameter("parametro") != null ? request.getParameter("parametro") : "";
           filtro = request.getParameter("filtro") != null ? Integer.parseInt(request.getParameter("filtro")) : 0;
           int idcat = (request.getParameter("cat") != null || !request.getParameter("cat").equals("")) ? Integer.parseInt(request.getParameter("cat")) : 0;
           int idsubcat = (request.getParameter("subcat") != null || !request.getParameter("subcat").equals("")) ? Integer.parseInt(request.getParameter("subcat")) : 0;
           int idtiposubcat = (request.getParameter("tscat") != null || !request.getParameter("tscat").equals("")) ? Integer.parseInt(request.getParameter("tscat")) : 0;
           session.setAttribute("resultado", pserv.buscarPorMatCat(filtro, parametro, idcat, idsubcat, idtiposubcat));

           this.generarXls();

           this.dispatchRequest(this.next);

       }catch (Exception e){
           e.printStackTrace();
       }

    }




        /**
     * Genera el xls del estado de cuenta
     * @param user Usuario que lo genera
     * @param listafacts Lista de facturas
     * @param listaing Lista de ingresos
     * @param listahead Datos para el encabezado
     * @param sal Saldo del negocio
     * @param vec Otros datos del cliente que se incluyen
     * @throws Exception Cuando hay un error
     */
    private void generarXls() throws Exception{
        String directorio = "";
        System.out.println("inicia generacion de xls");
        HttpSession session = request.getSession();
        ArrayList ver = (ArrayList) session.getAttribute("resultado");


        try {
            directorio = this.directorioArchivo(loginx, "_Mat_", "xls");
            System.out.println("Elaborando directorio");
            ExcelApplication excel = this.instanciar("Listado Materiales");
            excel.setDataCell(0, 0, "Listado de Materiales Segun Filtro:");
            excel.setCellStyle(0, 0, excel.getStyle("estilo2"));
            excel.setDataCell(1, 0, "Consecutivo");
            excel.setCellStyle(1, 0, excel.getStyle("estilo2"));
            excel.setDataCell(1, 1, "Descripcion del Producto");
            excel.setCellStyle(1, 1, excel.getStyle("estilo2"));
            excel.setDataCell(1, 2, "Precio");
            excel.setCellStyle(1, 2, excel.getStyle("estilo2"));
            excel.setDataCell(1, 3, "Tipo");
            excel.setCellStyle(1, 3, excel.getStyle("estilo2"));
            excel.setDataCell(1, 4, "Categoria");
            excel.setCellStyle(1, 4, excel.getStyle("estilo2"));
            excel.setDataCell(1, 5, "SubCategoria");
            excel.setCellStyle(1, 5, excel.getStyle("estilo2"));
            excel.setDataCell(1, 6, "TipoSubCategoria");
            excel.setCellStyle(1, 6, excel.getStyle("estilo2"));
            excel.setDataCell(1, 7, "Unidad Empaque");
            excel.setCellStyle(1, 7, excel.getStyle("estilo2"));
            excel.setDataCell(1, 8, "Estado");
            excel.setCellStyle(1, 8, excel.getStyle("estilo2"));
            System.out.println("cabecera lista");
            int filaAct = 3;

            Material spl = null;
            //parte 1 ingresos
            for(int i=0;i<ver.size();i++){

                spl = (Material) ver.get(i);
                excel.setDataCell(filaAct, 0, spl.getCodigo());
                excel.setCellStyle(filaAct, 0, excel.getStyle("estilo3"));

                excel.setDataCell(filaAct, 1, spl.getDescripcion());
                excel.setCellStyle(filaAct, 1, excel.getStyle("estilo3"));

                excel.setDataCell(filaAct, 2, spl.getValor());
                excel.setCellStyle(filaAct, 2, excel.getStyle("estilo3"));

                excel.setDataCell(filaAct, 3, spl.getTipo());
                excel.setCellStyle(filaAct, 3, excel.getStyle("estilo3"));


                excel.setDataCell(filaAct, 4, spl.getDesc_categoria());
                excel.setCellStyle(filaAct, 4, excel.getStyle("estilo3"));

                excel.setDataCell(filaAct, 5, spl.getDesc_subcat());
                excel.setCellStyle(filaAct, 5, excel.getStyle("estilo3"));

                excel.setDataCell(filaAct, 6, spl.getDesc_tiposub());
                excel.setCellStyle(filaAct, 6, excel.getStyle("estilo3"));

                excel.setDataCell(filaAct, 7, spl.getUnidad_empaque());
                excel.setCellStyle(filaAct, 7, excel.getStyle("estilo3"));

                excel.setDataCell(filaAct, 8, spl.getRegStatus());
                excel.setCellStyle(filaAct, 8, excel.getStyle("estilo3"));

                spl = null;
                filaAct += 1;
            }

            excel.saveToFile(directorio);
            System.out.println("guardado listo");
        }
        catch (Exception e) {
            throw new Exception("Error al generar el archivo xls: "+e.toString());
        }
        System.out.println("fin proceso xls");
    }

    /**
     * Genera la ruta en que se guardara el archivo
     * @param user usuario que genera el archivo
     * @param cons consecutivo de la cotizacion
     * @param extension La extension del archivo
     * @return String con la ruta en la que queda el archivo
     * @throws Exception cuando ocurre algun error
     */
    private String directorioArchivo(String user,String cons,String extension) throws Exception{
        String ruta = "";
        try {
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            ruta = rb.getString("ruta") + "/exportar/migracion/" + user;
            SimpleDateFormat fmt = new SimpleDateFormat("yyyMMdd_hhmmss");
            File archivo = new File( ruta );
            if (!archivo.exists()) archivo.mkdirs();
            ruta = ruta + "/" + "listado_materiales_" + cons +"_" + fmt.format( new Date() ) +"."+extension;
        }
        catch (Exception e) {
            throw new Exception("Error al generar el directorio: "+e.toString());
        }
        return ruta;
    }

    /**
     * Crea un objeto tipo ExcelApplication
     * @param descripcion Nombre de la hoja principal del libro
     * @return Objeto ExcelApplication creado
     * @throws Exception Cuando hay error
     */
    private ExcelApplication instanciar(String descripcion) throws Exception{
        ExcelApplication excel = new ExcelApplication();

        try{
            excel.createSheet(descripcion);
            
            excel.createColor((short)11, (byte)255, (byte)255, (byte)255);//Blanco
            excel.createColor((short)9, (byte)26, (byte)126, (byte)0);//Verde dark
            excel.createColor((short)10, (byte)37, (byte)69, (byte)255);//Azul oscuro

            excel.createFont("Titulo", "Arial", (short)11, true, (short)12);
            excel.createFont("Subtitulo", "Verdana", (short)11, true, (short)10);
            excel.createFont("Contenido", "Verdana", (short)0, false, (short)10);
            




            excel.createStyle("estilo1", excel.getFont("Titulo"), (short)10, true, "@");
            excel.createStyle("estilo2", excel.getFont("Subtitulo"), (short)9, true, "@");
            excel.createStyle("estilo3", excel.getFont("Contenido"), (short)11, true, "@");
            excel.createStyle("estilo4", excel.getFont("Contenido"), (short)11, true, "@",(short)2);
            

            

        }
        catch(Exception e){
            throw new Exception("Error al instanciar el objeto: "+e.toString());
        }
        return excel;
    }


    private void cargarCertificadores() throws Exception {
       String xml = "";
       //MaterialesService pserv = new MaterialesService();
       ArrayList listadoCertificadores = pserv.buscarCerficadores();


        xml = "<?xml version=\"1.0\" encoding=\"iso-8859-1\"?>";
        xml += "<mensaje>";
        xml += this.getXMLCertificadores(listadoCertificadores);
        xml += "</mensaje>";

        response.setContentType("text/xml");
        response.setHeader("Cache-Control", "no-cache");
        response.getWriter().println(xml);
    }


   private String getXMLCertificadores(ArrayList listadoCertificadores)throws Exception{
        String xml = "";
             Material spl = null;
            //parte 1 ingresos
            for(int i=0;i<listadoCertificadores.size();i++){
                spl = (Material) listadoCertificadores.get(i);
                xml += "<certificador codente=\""+spl.getCod_ente()+"\">"+spl.getDesc_ente()+"</certificador>";
            }
        return xml;
    }


    /**
     * jpinedo
     * @throws Exception
     *
     */
        public void updateListaPreciosMaterial() throws Exception {

        try {
             next = "/jsp/opav/cotizacion/material/reporte_materiales.jsp";
             //MaterialesService mserv = new MaterialesService();
             String [] cod=request.getParameterValues("cod_producto");
             String [] obss=request.getParameterValues("obs");
             String [] act_precio=new String [cod.length];
             for (int i=0;i<cod.length;i++)
             {
                 act_precio[i]=request.getParameter("act_precio"+i)!=null ? request.getParameter("act_precio"+i):"N";
             }
             ArrayList lista_materiales=new ArrayList();
             lista_materiales= (ArrayList)session.getAttribute("lista_materiales");
             pserv.ActualizarlistaPrecios(cod, obss, act_precio);
             if(this.generarXlsListadoMat(lista_materiales,act_precio,obss))
             {
                 this.envia_correo("sistemas@fintra.co", "jpinedo@fintra.co", "", "Actualizacion Lista Precios Materiales", this.getMensaje(), ruta, archivo);
                 
              }
            this.dispatchRequest(this.next+"?msj=Materiales Actualizados Correctamente");
        } catch (Exception ex) {
            this.dispatchRequest(this.next+"?msj=Error");
            ex.printStackTrace();
        }
    }




            /**
     * jpinedo
     * @throws Exception
     *
     */
        public void updateListaPreciosMaterialInactividad() throws Exception {

        try {
             next = "/jsp/opav/cotizacion/material/reporte_inactividad_materiales.jsp";
             //MaterialesService mserv = new MaterialesService();
             String [] cod=request.getParameterValues("cod_producto");
             String [] precio=new String [cod.length];
             String [] act_precio=new String [cod.length];
             for (int i=0;i<cod.length;i++)
             {
                 act_precio[i]=request.getParameter("act_precio"+i)!=null ? request.getParameter("act_precio"+i):"N";
                 precio[i]=request.getParameter("precio"+i)!=null ? request.getParameter("precio"+i):"";
             }
             ArrayList lista_materiales=new ArrayList();
             lista_materiales= (ArrayList)session.getAttribute("lista_materiales_inactividad");
             pserv.ActualizarlistaPreciosInactividad(cod, precio, act_precio);
             if(this.generarXlsListadoMatInactividad(lista_materiales,act_precio,precio))
             {                 
                 this.envia_correo("sistemas@fintra.co", "jpinedo@fintra.co", "", "Actualizacion Lista Precios Materiales Por Inactividad", this.getMensaje(), ruta, archivo);

             }
            this.dispatchRequest(this.next+"?msj=Materiales Actualizados Correctamente");
        } catch (Exception ex) {
            this.dispatchRequest(this.next+"?msj=Error");
            ex.printStackTrace();
        }
    }














                /**
     * Genera el xls del estado de cuenta
     * @param user Usuario que lo genera
     * @param listafacts Lista de facturas
     * @param listaing Lista de ingresos
     * @param listahead Datos para el encabezado
     * @param sal Saldo del negocio
     * @param vec Otros datos del cliente que se incluyen
     * @throws Exception Cuando hay un error
     */
    private boolean generarXlsListadoMat(ArrayList ver,String [] act_precio,String [] obs) throws Exception{
        String directorio = "";
        boolean generado=true;
        System.out.println("inicia generacion de xls");
        HttpSession session = request.getSession();
        //ArrayList ver = (ArrayList) session.getAttribute("resultado");

        try {
            directorio = this.ruta+this.archivo;
            System.out.println("Elaborando directorio");
            ExcelApplication excel = this.instanciar("Listado Materiales");
            excel.setDataCell(0, 0, "Listado de Materiales:");
            excel.cambiarAnchoColumna(0, 4000);
            excel.setCellStyle(0, 0, excel.getStyle("estilo2"));
            excel.setDataCell(1, 0, "Codigo");


           // HSSFCellStyle hs =new HSSFCellStyle();
            
            excel.cambiarAnchoColumna(1, 11000);
            excel.setCellStyle(1, 0, excel.getStyle("estilo2"));
            excel.setDataCell(1, 1, "Descripcion del Producto");


            excel.cambiarAnchoColumna(2, 4000);
            excel.setCellStyle(1, 1, excel.getStyle("estilo2"));
            excel.setDataCell(1, 2, "$ Compra");

            excel.cambiarAnchoColumna(3, 4000);
            excel.setCellStyle(1, 2, excel.getStyle("estilo2"));
            excel.setDataCell(1, 3, "$ Contratista");

            excel.cambiarAnchoColumna(4, 4000);
            excel.setCellStyle(1, 3, excel.getStyle("estilo2"));
            excel.setDataCell(1, 4, "$ Ultima Compra");
            excel.setCellStyle(1, 4, excel.getStyle("estilo2"));

            excel.cambiarAnchoColumna(5, 5000);
            excel.setDataCell(1, 5, "Fecha Ultima Compra");
            excel.setCellStyle(1, 5, excel.getStyle("estilo2"));

            excel.cambiarAnchoColumna(6, 5000);
            excel.setDataCell(1, 6, "Ultima Actualizacion");
            excel.setCellStyle(1, 6, excel.getStyle("estilo2"));

            excel.cambiarAnchoColumna(7, 5000);
            excel.setDataCell(1, 7, "Se Actualiza Precio");
            excel.setCellStyle(1, 7, excel.getStyle("estilo2"));

            excel.cambiarAnchoColumna(8, 6000);
            excel.setDataCell(1, 8, "Observacion ");
            excel.setCellStyle(1, 8, excel.getStyle("estilo2"));            
            int filaAct = 3;

            Material m = null;
            //parte 1 ingresos
            for(int i=0;i<ver.size();i++){

                m = (Material) ver.get(i);
                m.setObservacion(obs[i]);


                excel.setDataCell(filaAct, 0, m.getCodigo());
                excel.setCellStyle(filaAct, 0, excel.getStyle("estilo4"));

                excel.setDataCell(filaAct, 1, m.getDescripcion());
                excel.setCellStyle(filaAct, 1, excel.getStyle("estilo4"));

                excel.setDataCell(filaAct, 2, Util.customFormat(m.getValorCompra()));
                excel.setCellStyle(filaAct, 2, excel.getStyle("estilo3"));

                excel.setDataCell(filaAct, 3, Util.customFormat(m.getValor()));
                excel.setCellStyle(filaAct, 3, excel.getStyle("estilo3"));


                excel.setDataCell(filaAct, 4, Util.customFormat(m.getPrecio_ultima_compra()));
                excel.setCellStyle(filaAct, 4, excel.getStyle("estilo3"));

                excel.setDataCell(filaAct, 5, m.getFecha_ultima_compra());
                excel.setCellStyle(filaAct, 5, excel.getStyle("estilo3"));

                excel.setDataCell(filaAct, 6, m.getFecha_actualizacion_precio_compra());
                excel.setCellStyle(filaAct, 6, excel.getStyle("estilo3"));

                excel.setDataCell(filaAct, 7, act_precio[i]);
                excel.setCellStyle(filaAct, 7, excel.getStyle("estilo4"));

                excel.setDataCell(filaAct, 8, m.getObservacion());
                excel.setCellStyle(filaAct, 8, excel.getStyle("estilo4"));

                m = null;
                filaAct += 1;
            }

            excel.saveToFile(directorio);
            System.out.println("guardado listo");
        }
        catch (Exception e) {
            generado=false;
            throw new Exception("Error al generar el archivo xls: "+e.toString());

        }


        return generado;
    }




               /**
     * Genera el xls del estado de cuenta
     * @param user Usuario que lo genera
     * @param listafacts Lista de facturas
     * @param listaing Lista de ingresos
     * @param listahead Datos para el encabezado
     * @param sal Saldo del negocio
     * @param vec Otros datos del cliente que se incluyen
     * @throws Exception Cuando hay un error
     */
    private boolean generarXlsListadoMatInactividad(ArrayList ver,String [] act_precio,String [] precio) throws Exception{
        String directorio = "";
        this.archivo="listado_materiales_inactividad.xls";
        boolean generado=true;
        HttpSession session = request.getSession();
        //ArrayList ver = (ArrayList) session.getAttribute("resultado");

        try {
            directorio = this.ruta+this.archivo;
            System.out.println("Elaborando directorio");
            ExcelApplication excel = this.instanciar("Listado Materiales");
            excel.setDataCell(0, 0, "Listado de Materiales:");
            excel.cambiarAnchoColumna(0, 4000);
            excel.setCellStyle(0, 0, excel.getStyle("estilo2"));
            excel.setDataCell(1, 0, "Codigo");


           // HSSFCellStyle hs =new HSSFCellStyle();

            excel.cambiarAnchoColumna(1, 11000);
            excel.setCellStyle(1, 0, excel.getStyle("estilo2"));
            excel.setDataCell(1, 1, "Descripcion del Producto");


            excel.cambiarAnchoColumna(2, 4000);
            excel.setCellStyle(1, 1, excel.getStyle("estilo2"));
            excel.setDataCell(1, 2, "$ Compra");

            excel.cambiarAnchoColumna(3, 4000);
            excel.setCellStyle(1, 2, excel.getStyle("estilo2"));
            excel.setDataCell(1, 3, "$ Contratista");

            excel.cambiarAnchoColumna(4, 4000);
            excel.setCellStyle(1, 3, excel.getStyle("estilo2"));
            excel.setDataCell(1, 4, "$ Ultima Compra");
            excel.setCellStyle(1, 4, excel.getStyle("estilo2"));

            excel.cambiarAnchoColumna(5, 5000);
            excel.setDataCell(1, 5, "Fecha Ultima Compra");
            excel.setCellStyle(1, 5, excel.getStyle("estilo2"));

            excel.cambiarAnchoColumna(6, 5000);
            excel.setDataCell(1, 6, "Ultima Actualizacion");
            excel.setCellStyle(1, 6, excel.getStyle("estilo2"));

            excel.cambiarAnchoColumna(7, 5000);
            excel.setDataCell(1, 7, "Max Dias. Inac");
            excel.setCellStyle(1, 7, excel.getStyle("estilo2"));

            excel.cambiarAnchoColumna(8, 3000);
            excel.setDataCell(1, 8, "Dias. Inac ");
            excel.setCellStyle(1, 8, excel.getStyle("estilo2"));

            excel.cambiarAnchoColumna(9, 3000);
            excel.setDataCell(1, 9, "Precio Actual ");
            excel.setCellStyle(1, 9, excel.getStyle("estilo2"));

            int filaAct = 3;

            Material m = null;
            //parte 1 ingresos

            for(int i=0;i<ver.size();i++)
            {
                if(act_precio[i].equals("S"))
                {

                m = (Material) ver.get(i);            
                excel.setDataCell(filaAct, 0, m.getCodigo());
                excel.setCellStyle(filaAct, 0, excel.getStyle("estilo4"));

                excel.setDataCell(filaAct, 1, m.getDescripcion());
                excel.setCellStyle(filaAct, 1, excel.getStyle("estilo4"));

                excel.setDataCell(filaAct, 2, Util.customFormat(m.getValorCompra()));
                excel.setCellStyle(filaAct, 2, excel.getStyle("estilo3"));

                excel.setDataCell(filaAct, 3, Util.customFormat(m.getValor()));
                excel.setCellStyle(filaAct, 3, excel.getStyle("estilo3"));


                excel.setDataCell(filaAct, 4, Util.customFormat(m.getPrecio_ultima_compra()));
                excel.setCellStyle(filaAct, 4, excel.getStyle("estilo3"));

                excel.setDataCell(filaAct, 5, m.getFecha_ultima_compra());
                excel.setCellStyle(filaAct, 5, excel.getStyle("estilo3"));

                excel.setDataCell(filaAct, 6, m.getFecha_actualizacion_precio_compra());
                excel.setCellStyle(filaAct, 6, excel.getStyle("estilo3"));

                excel.setDataCell(filaAct, 7,m.getMax_dias_inactividad() );
                excel.setCellStyle(filaAct, 7, excel.getStyle("estilo4"));

                excel.setDataCell(filaAct, 8, m.getDias_inactividad());
                excel.setCellStyle(filaAct, 8, excel.getStyle("estilo4"));

                excel.setDataCell(filaAct, 9, Util.customFormat(Integer.parseInt(precio[i])));
                excel.setCellStyle(filaAct, 9, excel.getStyle("estilo4"));
                m = null;
                filaAct += 1;
                }
            }

            excel.saveToFile(directorio);         
        }
        catch (Exception e) {
            generado=false;
            throw new Exception("Error al generar el archivo xls: "+e.toString());

        }


        return generado;
    }












      public boolean envia_correo(String from,String to,String copia,String asunto,String msg,String ruta,String archivo)
    {
     boolean enviado=false;
     try
       {

         /************************* Envio de Correo*************************/
           Email2 emailData = null;
           String ahora = new java.util.Date().toString();
            emailData=new Email2();
            emailData.setEmailId(12345);
            emailData.setRecstatus("A");//A
            emailData.setEmailcode( "emailcode");
            emailData.setEmailfrom(from);
            emailData.setEmailto(to );
            emailData.setEmailcopyto(copia);
            emailData.setEmailHiddencopyto("");
            emailData.setEmailsubject(asunto);//"WebServiceMultiple_Fintra2" );
            emailData.setEmailbody(msg);
            emailData.setLastupdat(new Timestamp(System.currentTimeMillis()) );
            emailData.setSenderName( "sender name" );
            emailData.setRemarks("remark2");
            emailData.setTipo("E");
            emailData.setRutaArchivo(ruta);
            emailData.setNombreArchivo(archivo);


           // EmailSendingEngineService emailSendingEngineService=new EmailSendingEngineService("smtpbar.une.net.co","fintravalores@geotechsa.com","fintra21");
              EmailSendingEngineService emailSendingEngineService=new EmailSendingEngineService();
              enviado= emailSendingEngineService.send(emailData);
              emailSendingEngineService=null;//091206
            /*************************Fin Envio de Correo*************************/
           }
           catch(Exception e)
           {

               System.out.println("Error  "+e.getMessage());
           }

     return enviado;
    }



    public String getMensaje()
    {
        String mensaje="<span style='font-family: ; font-size:14px; line-height:25px '; >"
                + "</br>Adjunto se encuentra el reporte de los materiales actualizados.<br /></br >"
                + "</span>";

        return mensaje;
    }


}