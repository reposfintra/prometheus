/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.opav.controller;

/**
 *
 * @author Ing.William Siado T
 */
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.tsp.exceptions.InformationException;
import com.tsp.opav.model.DAOS.ModuloEjecucionDAO;
import com.tsp.opav.model.DAOS.impl.ModuloEjecucionImpl;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.util.Util;
import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ModuloEjecucionAction extends Action {

    private final int CARGAR_WBS_EJECUCION = 0;
    private final int CARGAR_COMBO_GENERICO = 1;
    private final int CARGAR_RESPONSABLES_EJECUCION = 2;
    private final int INSERTAR_RESPONSABLES_EJECUCION = 3;
    private final int ACTUALIZAR_RESPONSABLES_EJECUCIONS = 4;
    private final int ANULAR_RESPONSABLES_EJECUCION = 5;
    private final int CARGAR_PROYECTOS_ASIGNADOS = 6;
    private final int CARGAR_PROYECTO_EJECUCION = 7;
    private final int CARGAR_INSUMOS_APU = 8;
    private final int GUARDAR_CANTIDADES_INSUMOS_ACTUAL = 9;
    private final int CARGAR_LOTES_PROYECTO_USUARIO = 10;
    private final int SET_LOTE_WBS_EJECUCION = 11;
    private final int CARGAR_GRILLA_FACTURACION = 12;
    private final int PREPARAR_SOLICITUD_PARA_FACTURACION = 13;
    private final int GUARDAR_LOTE_EJECUCION = 14;
    private final int EDITAR_LOTE_EJECUCION = 15;
    private final int NUEVO_LOTE_EJECUCION = 16;
    private final int CREAR_WBS_EJECUCION = 17;
    private final int IMPRESIONES = 18;
    private final int INSERTAR_INSUMO_ADICIONAL = 19;
    private final int INSERTAR_APU_WBS_EJECUCION= 20;

    private ModuloEjecucionDAO dao;
    Usuario usuario = null;
    String reponseJson = "{}";
    String typeResponse = "application/json;";

    @Override
    public void run() throws ServletException, InformationException {
        try {

            HttpSession session = request.getSession();
            usuario = (Usuario) session.getAttribute("Usuario");
            dao = new ModuloEjecucionImpl(usuario.getBd());
            int opcion = (request.getParameter("opcion") != null)
                    ? Integer.parseInt(request.getParameter("opcion"))
                    : -1;
            switch (opcion) {
                case CARGAR_WBS_EJECUCION:
                    this.cargar_Wbs_Ejecucion();
                    break;
                case CARGAR_COMBO_GENERICO:
                    this.cargarComboGenerico();
                    break;
                case CARGAR_GRILLA_FACTURACION:
                    this.cargarGridFacturacion();
                    break;
                case PREPARAR_SOLICITUD_PARA_FACTURACION:
                    this.prepararSolicitudPendienteFacturar();
                    break;
                case CARGAR_RESPONSABLES_EJECUCION:
                    this.cargar_Responsables_Ejecucion();
                    break;

                case INSERTAR_RESPONSABLES_EJECUCION:
                    this.insertar_Responsables_Ejecucion();
                    break;

                case ACTUALIZAR_RESPONSABLES_EJECUCIONS:
                    this.actualizar_Responsables_Ejecucions();
                    break;

                case ANULAR_RESPONSABLES_EJECUCION:
                    this.anular_Responsables_Ejecucion();
                    break;

                case CARGAR_PROYECTOS_ASIGNADOS:
                    this.cargar_Proyectos_Asignados();
                    break;

                case CARGAR_PROYECTO_EJECUCION:
                    this.cargar_Proyecto_Ejecucion();
                    break;

                case CARGAR_INSUMOS_APU:
                    this.cargar_Insumos_Apu();
                    break;

                case GUARDAR_CANTIDADES_INSUMOS_ACTUAL:
                    this.guardar_Cantidades_Insumos_Actual();
                    break;
                    
                case INSERTAR_APU_WBS_EJECUCION:
                    this.insertar_Apu_Wbs_Ejecucion();
                    break;

                case CARGAR_LOTES_PROYECTO_USUARIO:
                    this.cargar_Lotes_Proyecto_Usuario();
                    break;

                case SET_LOTE_WBS_EJECUCION:
                    this.set_Lote_Wbs_Ejecucion();
                    break;

                case GUARDAR_LOTE_EJECUCION:
                    this.guardar_Lote_Ejecucion();
                    break;

                case EDITAR_LOTE_EJECUCION:
                    this.editar_Lote_Ejecucion();
                    break;

                case NUEVO_LOTE_EJECUCION:
                    this.nuevo_Lote_Ejecucion();
                    break;

                case CREAR_WBS_EJECUCION:
                    this.crear_WBS_Ejecucion();
                    break;

                case IMPRESIONES:
                    this.impresiones();
                    break;

                case INSERTAR_INSUMO_ADICIONAL:
                    this.insertar_Insumo_Adicional();
                    break;

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                this.printlnResponseAjax(reponseJson, typeResponse);
            } catch (Exception ex1) {
                Logger.getLogger(MaestroProyectoAction.class.getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }

    private void cargar_Wbs_Ejecucion() {
        try {
            String id_solicitud = request.getParameter("id_solicitud") != null ? request.getParameter("id_solicitud") : "";
            this.reponseJson = dao.cargar_Wbs_Ejecucion(id_solicitud);
        } catch (Exception ex) {
            Logger.getLogger(MaestroProyectoAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void cargarComboGenerico() {
        try {
            String op = request.getParameter("op") != null ? request.getParameter("op") : "";
            String param = request.getParameter("param") != null ? request.getParameter("param") : "";
            this.reponseJson = dao.cargarComboGenerico(op, param);
        } catch (Exception ex) {
            Logger.getLogger(MaestroProyectoAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void cargar_Responsables_Ejecucion() {
        try {
            String id_solicitud = request.getParameter("id_solicitud") != null ? Util.setCodificacionCadena(request.getParameter("id_solicitud"), "ISO-8859-1") : "";
            this.reponseJson = dao.cargar_Responsables_Ejecucion(id_solicitud);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void insertar_Responsables_Ejecucion() {
        try {
            String id_solicitud = request.getParameter("id_solicitud") != null ? Util.setCodificacionCadena(request.getParameter("id_solicitud"), "ISO-8859-1") : "";
            String id_usuario_ = request.getParameter("id_usuario_") != null ? request.getParameter("id_usuario_") : "";
            this.reponseJson = dao.insertar_Responsables_Ejecucion(id_solicitud, id_usuario_, usuario);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void actualizar_Responsables_Ejecucions() {
        try {
            String id = request.getParameter("id") != null ? request.getParameter("id") : "";
            String nombre = request.getParameter("nombre") != null ? Util.setCodificacionCadena(request.getParameter("nombre"), "ISO-8859-1") : "";
            String id_usuario_ = request.getParameter("id_usuario_") != null ? request.getParameter("id_usuario_") : "";
//            this.reponseJson = dao.actualizar_Responsables_Ejecucions(id, nombre,id_usuario_, usuario);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void anular_Responsables_Ejecucion() {
        try {
            String id = request.getParameter("id") != null ? request.getParameter("id") : "";
            this.reponseJson = dao.anular_Responsables_Ejecucion(id, usuario);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    private void cargar_Proyectos_Asignados() {
        try {
            this.reponseJson = dao.cargar_Proyectos_Asignados(usuario);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void cargar_Proyecto_Ejecucion() {
        try {
            String id_solicitud = request.getParameter("id_solicitud") != null ? request.getParameter("id_solicitud") : "";
            String opc = request.getParameter("opc") != null ? Util.setCodificacionCadena(request.getParameter("opc"), "ISO-8859-1") : "";
            String cond = request.getParameter("cond") != null ? request.getParameter("cond") : "";
            String proceso = request.getParameter("proceso") != null ? request.getParameter("proceso") : "";
            String id_lote = request.getParameter("id_lote") != null ? request.getParameter("id_lote") : "";

            //se pasa por parametro el usuario por que mas adelante se asignara apus por usuario y no por proyecto
            this.reponseJson = dao.cargar_Proyecto_Ejecucion(id_solicitud, proceso, opc, cond, id_lote ,usuario);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void cargar_Insumos_Apu() {
        try {
            String id_solicitud = request.getParameter("id_solicitud") != null ? request.getParameter("id_solicitud") : "";
            String id_rel_actividades_apu = request.getParameter("id_rel_actividades_apu") != null ? Util.setCodificacionCadena(request.getParameter("id_rel_actividades_apu"), "ISO-8859-1") : "";
            String id_apu = request.getParameter("id_apu") != null ? request.getParameter("id_apu") : "";
            String unidad_medida_apu = request.getParameter("unidad_medida_apu") != null ? request.getParameter("unidad_medida_apu") : "";

            String master = request.getParameter("master") != null ? request.getParameter("master") : "";
            
            if(master.equals("")){
                this.reponseJson = dao.cargar_Insumos_Apu(id_solicitud, id_rel_actividades_apu, id_apu, unidad_medida_apu, usuario.getLogin());
            }else{
                this.reponseJson = dao.cargar_Insumos_Apu_Master(id_solicitud, id_rel_actividades_apu, id_apu, unidad_medida_apu, "MASTER");
            }
            
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void guardar_Cantidades_Insumos_Actual() {
        try {
            JsonObject rows = (JsonObject) (new JsonParser()).parse(request.getParameter("informacion"));
             String master = request.getParameter("master") != null ? request.getParameter("master") : "";
            
            if(master.equals("")){
                this.reponseJson = dao.guardar_Cantidades_Insumos_Actual(rows, usuario.getLogin());
            }else{
                this.reponseJson = dao.guardar_Cantidades_Insumos_Actual_Master(rows, usuario.getLogin());
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private void insertar_Apu_Wbs_Ejecucion() {
        try {
            JsonObject json = (JsonObject) (new JsonParser()).parse(request.getParameter("informacion"));
            this.reponseJson = dao.insertar_Apu_Wbs_Ejecucion(json, usuario);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void cargar_Lotes_Proyecto_Usuario() {
        try {
            String id_solicitud = request.getParameter("id_solicitud") != null ? request.getParameter("id_solicitud") : "";
            this.reponseJson = dao.cargar_Lotes_Proyecto_Usuario(id_solicitud, usuario);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void set_Lote_Wbs_Ejecucion() {
        try {
            String id_solicitud = request.getParameter("id_solicitud") != null ? request.getParameter("id_solicitud") : "";
            String id_lote = request.getParameter("id_lote") != null ? request.getParameter("id_lote") : "";
            this.reponseJson = dao.set_Lote_Wbs_Ejecucion(id_solicitud, id_lote, usuario);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void cargarGridFacturacion() {
        try {
            String id_solicitud = request.getParameter("id_solicitud") != null ? request.getParameter("id_solicitud") : "";
            this.reponseJson = dao.cargarGridFacturacion(id_solicitud);
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(ModuloEjecucionAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void prepararSolicitudPendienteFacturar() throws Exception {
        try {
            String id_solicitud = request.getParameter("id_solicitud") != null ? request.getParameter("id_solicitud") : "";
            String valor_facturar = request.getParameter("valor_facturar") != null ? request.getParameter("valor_facturar") : "0";
            String valor_material = request.getParameter("valor_material") != null ? request.getParameter("valor_material") : "0";
            this.reponseJson = dao.prepararSolicitudParaFacturar(id_solicitud, valor_facturar, valor_material, usuario);
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(MinutasContratacionAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void guardar_Lote_Ejecucion() {

        try {
            JsonObject info = (JsonObject) (new JsonParser()).parse(request.getParameter("informacion"));
            String id_solicitud = request.getParameter("id_solicitud") != null ? request.getParameter("id_solicitud") : "";
            String descripcion = request.getParameter("descripcion") != null ? request.getParameter("descripcion") : "";
            this.reponseJson = dao.guardar_Lote_Ejecucion(id_solicitud, descripcion, info, usuario);

        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(MinutasContratacionAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void editar_Lote_Ejecucion() {
        try {
            JsonObject info = (JsonObject) (new JsonParser()).parse(request.getParameter("informacion"));
            String id_lote = request.getParameter("id_lote") != null ? request.getParameter("id_lote") : "";
            String id_solicitud = request.getParameter("id_solicitud") != null ? request.getParameter("id_solicitud") : "";
            String descripcion = request.getParameter("descripcion") != null ? request.getParameter("descripcion") : "";
            this.reponseJson = dao.editar_Lote_Ejecucion(id_solicitud, id_lote, descripcion, info, usuario);

        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(MinutasContratacionAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void nuevo_Lote_Ejecucion() {
        try {
            String id_solicitud = request.getParameter("id_solicitud") != null ? request.getParameter("id_solicitud") : "";
            this.reponseJson = dao.nuevo_Lote_Ejecucion(id_solicitud, usuario);

        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(MinutasContratacionAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void crear_WBS_Ejecucion() {
        try {
            String id_solicitud = request.getParameter("id_solicitud") != null ? request.getParameter("id_solicitud") : "";
            String copia = request.getParameter("copia") != null ? request.getParameter("copia") : "";
            if(copia.equals("")){
                this.reponseJson = dao.crear_WBS_Ejecucion(id_solicitud);
            }else{
                this.reponseJson = dao.crear_WBS_Ejecucion(id_solicitud, usuario);
            }
            

        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(MinutasContratacionAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void impresiones() {
        try {
                String ids_actas = request.getParameter("ids_actas") != null ? request.getParameter("ids_actas") : "";
            String id_solicitud = request.getParameter("id_solicitud") != null ? request.getParameter("id_solicitud") : "";
            String opc_impresion = request.getParameter("opc_impresion") != null ? request.getParameter("opc_impresion") : "";
            if(opc_impresion.equals("0")){
                this.reponseJson = dao.imprimir_Actas(id_solicitud, ids_actas, usuario);
            }else{
                this.reponseJson = dao.imprimir_Liquidacion(id_solicitud, ids_actas, usuario);
            }
            

        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(MinutasContratacionAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void insertar_Insumo_Adicional() {
        try {
            String id_insumo = request.getParameter("id_insumo") != null ? request.getParameter("id_insumo") : "";
            String id_wbs_ejecucion = request.getParameter("id_wbs_ejecucion") != null ? request.getParameter("id_wbs_ejecucion") : "";
            String id_unidad_medida = request.getParameter("id_unidad_medida") != null ? request.getParameter("id_unidad_medida") : "";
            String cantidad = request.getParameter("cantidad") != null ? request.getParameter("cantidad") : "";
            this.reponseJson = dao.insertar_Insumo_Adicional(id_insumo,id_unidad_medida,id_wbs_ejecucion, cantidad, usuario);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
