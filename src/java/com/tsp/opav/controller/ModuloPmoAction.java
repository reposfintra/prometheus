/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.opav.controller;

/**
 *
 * @author Ing.William Siado T
 */
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.tsp.exceptions.InformationException;
import com.tsp.opav.model.DAOS.ModuloPmoDAO;
import com.tsp.opav.model.DAOS.impl.ModuloPmoImpl;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.util.Util;
import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ModuloPmoAction extends Action {

    private final int CARGAR_WBS_EJECUCION = 0;

    private ModuloPmoDAO dao;
    Usuario usuario = null;
    String reponseJson = "{}";
    String typeResponse = "application/json;";

    @Override
    public void run() throws ServletException, InformationException {
        try {

            HttpSession session = request.getSession();
            usuario = (Usuario) session.getAttribute("Usuario");
            dao = new ModuloPmoImpl(usuario.getBd());
            int opcion = (request.getParameter("opcion") != null)
                    ? Integer.parseInt(request.getParameter("opcion"))
                    : -1;
            switch (opcion) {
                case CARGAR_WBS_EJECUCION:
                    this.cargar_Wbs_Ejecucion();
                    break;

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                this.printlnResponseAjax(reponseJson, typeResponse);
            } catch (Exception ex1) {
                Logger.getLogger(MaestroProyectoAction.class.getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }

    private void cargar_Wbs_Ejecucion() {
        try {
            String id_solicitud = request.getParameter("id_solicitud") != null ? request.getParameter("id_solicitud") : "";
            this.reponseJson = dao.cargar_Wbs_Ejecucion(id_solicitud);
        } catch (Exception ex) {
            Logger.getLogger(MaestroProyectoAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

}
