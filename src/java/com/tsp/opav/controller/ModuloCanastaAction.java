/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.opav.controller;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.tsp.exceptions.InformationException;
import com.tsp.opav.model.DAOS.ModuloCanastaDAO;
import com.tsp.opav.model.DAOS.impl.ModuloCanastaImpl;

import com.tsp.operation.model.beans.Usuario;
import com.tsp.util.Util;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Ing.William A. Siado
 */
public class ModuloCanastaAction extends Action {

    private final int CARGAR_COMBO_GENERICO = 1;
    private final int CARGAR_PROYECTO_EJECUCION = 2;
    private final int GET_APUS_WBS = 3;
    private final int BLOQUER_APUS = 4;
    private final int CARGAR_CAUSALES_CANASTA = 5;
    private final int GUARDAR_CAUSALES_CANASTA = 6;
    private final int EDITAR_CAUSALES_CANASTA = 7;
    private final int ELIMINAR_CAUSALES_CANASTA = 8;
    private final int CARGAR_INSUMOS_APU = 9;
    private final int CARGAR_INSUMOS_PROYECTO = 10;
    private final int CARGAR_APUS_INSUMO = 11;
    private final int BLOQUEAR_APUS_INSUMO = 12;
    

    private ModuloCanastaDAO dao;
    Usuario usuario = null;
    String reponseJson = "{}";
    String typeResponse = "application/json;";

    @Override
    public void run() throws ServletException, InformationException {
        try {
            HttpSession session = request.getSession();
            usuario = (Usuario) session.getAttribute("Usuario");
            dao = new ModuloCanastaImpl(usuario.getBd());
            int opcion = (request.getParameter("opcion") != null)
                    ? Integer.parseInt(request.getParameter("opcion"))
                    : -1;
            switch (opcion) {
                case CARGAR_COMBO_GENERICO:
                    this.cargarComboGenerico();
                    break;
                case CARGAR_PROYECTO_EJECUCION:
                    this.cargar_Proyecto_Ejecucion();
                    break;
                case GET_APUS_WBS:
                    this.get_Apus_Wbs();
                    break;
                case BLOQUER_APUS:
                    this.bloquer_Apus();
                    break;
                case CARGAR_CAUSALES_CANASTA:
                    this.cargar_Causales_Canasta();
                    break;
                case GUARDAR_CAUSALES_CANASTA:
                    this.guardar_Causales_Canasta();
                    break;
                case EDITAR_CAUSALES_CANASTA:
                    this.editar_Causales_Canasta();
                    break;
                case ELIMINAR_CAUSALES_CANASTA:
                    this.eliminar_Causales_Canasta();
                    break;
                case CARGAR_INSUMOS_APU:
                    this.cargar_Insumos_Apu();
                    break;
                case CARGAR_INSUMOS_PROYECTO:
                    this.cargar_Insumos_Proyecto();
                    break;
                case CARGAR_APUS_INSUMO:
                    this.cargar_Apus_Insumo();
                    break;
                case BLOQUEAR_APUS_INSUMO:
                    this.bloquear_Apus_Insumo();
                    break;

            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                this.printlnResponseAjax(reponseJson, typeResponse);
            } catch (Exception ex1) {
                Logger.getLogger(MaestroProyectoAction.class.getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }

    private void cargarComboGenerico() {
        try {
            String op = request.getParameter("op") != null ? request.getParameter("op") : "";
            String param = request.getParameter("param") != null ? request.getParameter("param") : "";
            this.reponseJson = dao.cargarComboGenerico(op, param);
        } catch (Exception ex) {
            Logger.getLogger(MaestroProyectoAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void cargar_Proyecto_Ejecucion() {
        try {
            String id_solicitud = request.getParameter("id_solicitud") != null ? request.getParameter("id_solicitud") : "";
            String opc = request.getParameter("opc") != null ? Util.setCodificacionCadena(request.getParameter("opc"), "ISO-8859-1") : "";
            String cond = request.getParameter("cond") != null ? request.getParameter("cond") : "";
            String proceso = request.getParameter("proceso") != null ? request.getParameter("proceso") : "";

            this.reponseJson = dao.cargar_Proyecto_Ejecucion(id_solicitud, proceso, opc, cond, usuario);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void get_Apus_Wbs() {
        try {
            String id_solicitud = request.getParameter("id_solicitud") != null ? request.getParameter("id_solicitud") : "";
            String id_apu = request.getParameter("id_apu") != null ? Util.setCodificacionCadena(request.getParameter("id_apu"), "ISO-8859-1") : "";

            this.reponseJson = dao.get_Apus_Wbs(id_solicitud, id_apu);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void bloquer_Apus() {
        try {
            JsonObject informacion = (JsonObject) (new JsonParser()).parse(request.getParameter("informacion"));
            this.reponseJson = dao.bloquer_Apus(informacion, usuario);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void cargar_Causales_Canasta() {
        try {
            this.reponseJson = dao.cargar_Causales_Canasta();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void guardar_Causales_Canasta() {

        try {
            String nombre = request.getParameter("nombre") != null ? Util.setCodificacionCadena(request.getParameter("nombre"), "ISO-8859-1") : "";
            String descripcion = request.getParameter("descripcion") != null ? Util.setCodificacionCadena(request.getParameter("descripcion"), "ISO-8859-1") : "";

            this.reponseJson = dao.guardar_Causales_Canasta(nombre, descripcion, usuario);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    private void editar_Causales_Canasta() {
        try {
            String id = request.getParameter("id") != null ? Util.setCodificacionCadena(request.getParameter("id"), "ISO-8859-1") : "";
            String nombre = request.getParameter("nombre") != null ? Util.setCodificacionCadena(request.getParameter("nombre"), "ISO-8859-1") : "";
            String descripcion = request.getParameter("descripcion") != null ? Util.setCodificacionCadena(request.getParameter("descripcion"), "ISO-8859-1") : "";

            this.reponseJson = dao.editar_Causales_Canasta(id, nombre, descripcion, usuario);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    private void eliminar_Causales_Canasta() {
        try{
            String id = request.getParameter("id") != null ? Util.setCodificacionCadena(request.getParameter("id"), "ISO-8859-1") : "";
            this.reponseJson = dao.eliminar_Causales_Canasta(id, usuario);
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }

    private void cargar_Insumos_Apu() {
        try {
            String id_solicitud = request.getParameter("id_solicitud") != null ? request.getParameter("id_solicitud") : "";
            String id_rel_actividades_apu = request.getParameter("id_rel_actividades_apu") != null ? Util.setCodificacionCadena(request.getParameter("id_rel_actividades_apu"), "ISO-8859-1") : "";
            String id_apu = request.getParameter("id_apu") != null ? request.getParameter("id_apu") : "";
            String unidad_medida_apu = request.getParameter("unidad_medida_apu") != null ? request.getParameter("unidad_medida_apu") : "";

            this.reponseJson = dao.cargar_Insumos_Apu(id_solicitud, id_rel_actividades_apu, id_apu, unidad_medida_apu);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void cargar_Insumos_Proyecto() {
        try {
            String id_solicitud = request.getParameter("id_solicitud") != null ? request.getParameter("id_solicitud") : "";
            String nom_insumo = request.getParameter("nom_insumo") != null ? Util.setCodificacionCadena(request.getParameter("nom_insumo"), "ISO-8859-1") : "";

            this.reponseJson = dao.cargar_Insumos_Proyecto(id_solicitud, nom_insumo);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void cargar_Apus_Insumo() {
        try {
            String id_solicitud = request.getParameter("id_solicitud") != null ? request.getParameter("id_solicitud") : "";
            String id_insumo = request.getParameter("id_insumo") != null ? request.getParameter("id_insumo") : "";
            String id_unidad_medida_insumo = request.getParameter("id_unidad_medida_insumo") != null ? request.getParameter("id_unidad_medida_insumo") : "";

            this.reponseJson = dao.cargar_Apus_Insumo(id_solicitud, id_insumo, id_unidad_medida_insumo );
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void bloquear_Apus_Insumo() {
        try {
            JsonObject informacion = (JsonObject) (new JsonParser()).parse(request.getParameter("informacion"));
            this.reponseJson = dao.bloquear_Apus_Insumo(informacion, usuario);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}
