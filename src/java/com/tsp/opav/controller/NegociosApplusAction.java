/*
 * NegociosApplusAction.java
 * Created on 2 de febrero de 2009, 17:32
 */
package com.tsp.opav.controller;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.tsp.exceptions.*;
import javax.servlet.*;

import com.tsp.operation.model.beans.Usuario;

import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.FileItem;
import com.tsp.opav.model.beans.NegocioApplus;
import java.io.*;
import java.util.LinkedList;
import java.util.*;
import com.tsp.operation.model.threads.HSendMail2;
import com.tsp.util.Util;

import com.tsp.operation.model.threads.HGenerarFacturaFormulaPro;
import com.tsp.operation.model.beans.BeanGeneral;//20100219
import com.tsp.opav.model.services.ClientesVerService;

import javax.servlet.http.*;

import com.tsp.opav.model.services.NegociosApplusService;//20100618
import com.tsp.util.ExcelApiUtil;

/** * @author  Fintra */
public class NegociosApplusAction  extends Action  {
    /** Creates a new instance of NegociosApplusAction */
    String loginx;
    String next;
    Usuario usuario;
    public NegociosApplusAction() {

    }

    @Override
    public void run() throws ServletException, InformationException {


            String opcion = request.getParameter("opcion")!=null?request.getParameter("opcion"):"";
        String cuadro_informe = request.getParameter("informe")!=null?request.getParameter("informe"):"";
        try{
        if(opcion.equals("responsable")){




        this.cargarResponsables();
        }else{


        //ystem.out.println("cambiarEstado"+request.getParameter("opcion"));
        HttpSession session = request.getSession();
        com.tsp.util.Util.logController(((com.tsp.operation.model.beans.Usuario)session.getAttribute("Usuario")).getLogin(),this.getClass().getName());
        usuario = (Usuario) session.getAttribute("Usuario");
        String dstrct = session.getAttribute("Distrito").toString();
        String                      msjRecepcionObra = "Recepcion pendiente.";
        boolean redirect = true;
        String strRespuesta = "";
        if(request.getParameter("opcion").equals("clselect"))
        {   String filtro=request.getParameter("filtro");
            String nomselect=request.getParameter("nomselect");

            try{
                response.setContentType("text/plain");
                response.setHeader("Cache-Control", "no-cache");
                response.getWriter().println(model.NegociosApplusService.getClientesEca(filtro,nomselect,((request.getParameter("idclie")!=null)?request.getParameter("idclie"):"")));
            }
            catch(Exception e)
            {   e.printStackTrace();
                System.out.println(e.getMessage());

            }
        }
        else
        {

            if(request.getParameter("opcion").equals("clnics"))
            {   String filtro=request.getParameter("filtro");
                String nomselect=request.getParameter("nomselect");

                try{
                    response.setContentType("text/plain");
                    response.setHeader("Cache-Control", "no-cache");
                    response.getWriter().println(model.NegociosApplusService.getClientesEca_nics(filtro,nomselect,((request.getParameter("idclie")!=null)?request.getParameter("idclie"):"")));
                }
                catch(Exception e)
                {   e.printStackTrace();
                    System.out.println(e.getMessage());

                }
            }
            else if(request.getParameter("opcion").equals("conformar")){
                next="/jsp/opav/cambiar_estado_negocios2.jsp";
                String resp = "Datos ingresados correctamente";//2010-06-18 rhonalf
                try {
                    String acciones = request.getParameter("datos")!=null? request.getParameter("datos") : "";
                    String factura = request.getParameter("factura")!=null? request.getParameter("factura") : "";
                    String usua = request.getParameter("usuario")!=null? request.getParameter("usuario") : "";//2010-06-18 rhonalf

                    String fecfactconf= request.getParameter("fecfactconf");//20100825

                    NegociosApplusService naps = new NegociosApplusService(usuario.getBd());
                    resp = naps.facturaConformada(acciones, factura,usua,fecfactconf );//2010-06-18 rhonalf//20100825
                    naps=null;
                }
                catch (Exception e) {
                    resp = "No fue posible ingresar los datos ... ocurrio un error!";//2010-06-18 rhonalf
                    System.out.println("Error al asignar factura conformada en NegociosApplusAction.java: "+e.toString());
                    e.printStackTrace();
                }
                finally{
                    try {//2010-06-18 rhonalf
                        response.setContentType("text/plain; charset=utf-8");
                        response.setHeader("Cache-Control", "no-cache");
                        response.getWriter().println(resp);
                    }
                    catch (Exception e) {
                        System.out.println("Error al escribir la respuesta: "+e.toString());
                    }
                }

            }
            else{//aqui viene el sino que estaba luego de la condicion opcion: clselect

                System.out.println("request.getParameter"+request.getParameter("opcion"));

                loginx=usuario.getLogin();

                RequestDispatcher rd ;
                next = "";
                String estado_consultar=request.getParameter("estado_consultar");
                String estado_asignar=request.getParameter("estado_asignar");
                String observacion=request.getParameter("observacion");
                String esquema_comision=request.getParameter("esquema_comision");
                String svx=request.getParameter("svx");
                String contratista_consultar=request.getParameter("contratista_consultar");
                String fact_conformed=request.getParameter("fact_conformed");
                String num_osx=request.getParameter("num_osx");
                String cuoticas=request.getParameter("cuoticas");
                String fecfaccli=request.getParameter("fecfaccli");
                String fact_conformada_consultar=request.getParameter("fact_conformada_consultar");
                String esquema_financiacion=request.getParameter("esquema_financiacion");

                //ystem.out.println("en actionnnnnnnnnnnnnn"+contratista_consultar);
                try{
                    if (request.getParameter("opcion")!=null && request.getParameter("opcion").equals("cambiarEstado")){

                        HSendMail2 hSendMail3=new HSendMail2();//090919
                        ArrayList archivillos=new ArrayList();//090919
                        String[] archi1={"C:\\test.gif","archix1"};//090919
                        String[] archi2={"C:\\test2.gif","archix2"};//090919
                        archivillos.add(archi1);//090919
                        archivillos.add(archi2);//090919
        /*                hSendMail3.start("imorales@fintravalores.com",
                            "imorales@fintravalores.com", "", "",
                            "asuntillo",
                            "textillo \n remix",loginx,archivillos);//090919*/

                        String[] negocios  =  request.getParameterValues("idNegocio");

                        //inicio de 090715
                        ArrayList accords=model.NegociosApplusService.obtainAccords(negocios);

                        for (int k=0;k<accords.size();k++){
                            NegocioApplus na=(NegocioApplus)accords.get(k);
                            if (!(na.getIdEstado().equals(estado_asignar))){

                                if (estado_asignar.equals("96")){
                                    String[] dat_cli= model.electricaribeVerSvc.getDatosClient(na.getIdCliente());
                                    HSendMail2 hSendMail2=new HSendMail2();
                                    hSendMail2.start("imorales@fintravalores.com",
                                        model.electricaribeVerSvc.getEmailContratistaApp(na.getIdContratista()), "", "",
                                        "entrega al contratista",
                                            //"Se ha cambiado el estado del multiservicio "+na.getNumOs()+" del cliente "+na.getNombreClient()+" ( "+na.getIdCliente()+" ) ; Se pas� de '"+na.getEstado()+"' a '"+getUnEstadoApp(estado_asignar)+"'. Descripci�n: "+na.getAcciones()+". "+getUnContratistaApp(na.getIdContratista())+" ("+na.getIdContratista()+ "). Por favor consultar en http://www.fintra.co",loginx);
                                        ""+model.electricaribeVerSvc.getNombreContratistaApp(na.getIdContratista())+": Se debe realizar visita al cliente "+na.getNombreCliente()+" "+na.getIdCliente()+" ("+dat_cli[7]+" "+dat_cli[5]+") del " +
                                        "multiservicio "+na.getNumOs()+" . Observaciones: \n"+na.getAcciones()+" .",loginx,"","");
                                }

                                if (estado_asignar.equals("4") && k==0){
                                    //String[] dat_cli= model.electricaribeVerSvc.getDatosClient(na.getIdCliente());
                                    HSendMail2 hSendMail2=new HSendMail2();
                                    hSendMail2.start("imorales@fintravalores.com",
                                        model.electricaribeVerSvc.getEmailEntidad("APPLUS"), "", "",
                                        "aprobado financiado",
                                            //"Se ha cambiado el estado del multiservicio "+na.getNumOs()+" del cliente "+na.getNombreClient()+" ( "+na.getIdCliente()+" ) ; Se pas� de '"+na.getEstado()+"' a '"+getUnEstadoApp(estado_asignar)+"'. Descripci�n: "+na.getAcciones()+". "+getUnContratistaApp(na.getIdContratista())+" ("+na.getIdContratista()+ "). Por favor consultar en http://www.fintra.co",loginx);
                                        "APPLUS: Ha sido aprobada la financiaci�n de "+na.getNumOs()+" . Favor consultar Aplicativo de Consorcio Multiservicios en http://200.30.77.38/consorcio  .",loginx,"","");
                                            //+"Se debe realizar visita al cliente "+na.getNombreClient()+" "+na.getIdCliente()+" ("+dat_cli[7]+") del " +"multiservicio "+na.getNumOs()+" . Observaciones: \n"+na.getAcciones()+" .",loginx);
                                }

                                if (estado_asignar.equals("6")){
                                    //String[] dat_cli= model.electricaribeVerSvc.getDatosClient(na.getIdCliente());

                                    HSendMail2 hSendMail2=new HSendMail2();
                                    hSendMail2.start("imorales@fintravalores.com",
                                        model.electricaribeVerSvc.getEmailContratistaApp(na.getIdContratista()), "", "",
                                        "orden de trabajo",
                                            //"Se ha cambiado el estado del multiservicio "+na.getNumOs()+" del cliente "+na.getNombreClient()+" ( "+na.getIdCliente()+" ) ; Se pas� de '"+na.getEstado()+"' a '"+getUnEstadoApp(estado_asignar)+"'. Descripci�n: "+na.getAcciones()+". "+getUnContratistaApp(na.getIdContratista())+" ("+na.getIdContratista()+ "). Por favor consultar en http://www.fintra.co",loginx);
                                        ""+model.electricaribeVerSvc.getNombreContratistaApp(na.getIdContratista())+": Se ha pasado a estado 'orden de trabajo' de "+na.getNumOs()+" . Favor consultar Aplicativo de Consorcio Multiservicios en http://200.30.77.38/consorcio  .",loginx,"","");
                                            //+"Se debe realizar visita al cliente "+na.getNombreClient()+" "+na.getIdCliente()+" ("+dat_cli[7]+") del " +"multiservicio "+na.getNumOs()+" . Observaciones: \n"+na.getAcciones()+" .",loginx);
                                }

                                if (estado_asignar.equals("7") && k==0){
                                    //String[] dat_cli= model.electricaribeVerSvc.getDatosClient(na.getIdCliente());
                                    HSendMail2 hSendMail2=new HSendMail2();
                                    hSendMail2.start("imorales@fintravalores.com",
                                        model.electricaribeVerSvc.getEmailEntidad("ECA"), "", "",
                                        "recepcion de obra",
                                            //"Se ha cambiado el estado del multiservicio "+na.getNumOs()+" del cliente "+na.getNombreClient()+" ( "+na.getIdCliente()+" ) ; Se pas� de '"+na.getEstado()+"' a '"+getUnEstadoApp(estado_asignar)+"'. Descripci�n: "+na.getAcciones()+". "+getUnContratistaApp(na.getIdContratista())+" ("+na.getIdContratista()+ "). Por favor consultar en http://www.fintra.co",loginx);
                                        "ECA: Se ha cambiado el "+na.getNumOs()+" al estado Recepci�n de Obra. Favor consultar Aplicativo de Consorcio Multiservicios en http://200.30.77.38/consorcio  .",loginx,"","");
                                            //+"Se debe realizar visita al cliente "+na.getNombreClient()+" "+na.getIdCliente()+" ("+dat_cli[7]+") del " +"multiservicio "+na.getNumOs()+" . Observaciones: \n"+na.getAcciones()+" .",loginx);
                                }

                                if ((estado_asignar.equals("1") || estado_asignar.equals("2")) && k==0){
                                    HSendMail2 hSendMail2=new HSendMail2();
                                    hSendMail2.start("imorales@fintravalores.com",
                                        model.electricaribeVerSvc.getEmailEntidad("FINTRA"), "", "",
                                        "en estudio",
                                        "FINTRA: Se ha cambiado el "+na.getNumOs()+" al estado En Estudio. Favor consultar Aplicativo de Consorcio Multiservicios en http://200.30.77.38/consorcio  .",loginx,"","");

                                }

                            }
                        }
                        //fin de 090715

                        model.NegociosApplusService.CambiarEstado(negocios,estado_asignar,observacion,esquema_comision,svx,contratista_consultar,fact_conformed,cuoticas,fecfaccli,fact_conformada_consultar,esquema_financiacion,loginx);
                        //ystem.out.println("contratista_consultar"+contratista_consultar);
                        next="/jsp/opav/cambiar_estado_negocios.jsp?estadito=" + estado_consultar+"&contratistica="+contratista_consultar;
                    }
                    if (request.getParameter("opcion")!=null && request.getParameter("opcion").equals("consultarEstado")){
                        if (estado_consultar==null || estado_consultar.equals("")){
                            estado_consultar="2";
                        }

                        next="/jsp/opav/cambiar_estado_negocios.jsp?estadito=" + estado_consultar+"&contratistica="+contratista_consultar+"&fact_conformada_consultar="+fact_conformada_consultar;
                        if (request.getParameter("caso").equals("applus")){//090428
                            next="/jsp/opav/partedcambiar_estado_negocios.jsp?estadito=" + estado_consultar+"&contratistica="+contratista_consultar+"&fact_conformada_consultar="+fact_conformada_consultar;
                        }
                    }

                    if (request.getParameter("opcion")!=null && request.getParameter("opcion").equals("asignarEstado")){ //
                        if (estado_consultar==null ){
                            estado_consultar="0";
                        }
                        String nic_consultable=request.getParameter("nicc");
                        String nomcli_consultable=request.getParameter("nomclie");
                        String solici_consultable=request.getParameter("id_solici");
                        String estado_asignable=request.getParameter("estado_asignable");

                        String[] acciones  =  request.getParameterValues("idAcc");
                        String tipo = model.ElectricaribeOfertaSvc.tipoOferta(solici_consultable);//JJCASTRO EMERGENCIA

                        estado_asignable = tipo.equals("Emergencia")?"070":estado_asignable;//JJCASTRO EMERGENCIA

                        String asignacionEstado=model.NegociosApplusService.asignarEstado(loginx,estado_asignable,acciones);

                        next="/jsp/opav/cambiar_estado_negocios2.jsp?estadito=" + estado_consultar+"&contratistica="+contratista_consultar+"&fact_conformada_consultar="+fact_conformada_consultar+
                            "&id_solici="+solici_consultable+"&nicc="+nic_consultable+"&nomclie="+nomcli_consultable;
                    }



                    if (request.getParameter("opcion")!=null && request.getParameter("opcion").equals("subirArchivo")){
                        //ystem.out.println("numerito"+request.getParameter("num_osx"));
                        next=guardarArchivo( );
                        //next="/jsp/opav/importar.jsp";
                    }

                    if (request.getParameter("opcion")!=null && request.getParameter("opcion").equals("nombres")){
                        String numerit_os=request.getParameter("numerit_os");
                        String tipito=request.getParameter("tipito");
                        //ystem.out.println("numeritooo"+numerit_os);

                        next="/jsp/opav/mostrar_archivos.jsp?num_osx="+numerit_os+"&tipito="+tipito;
                    }
                    if (request.getParameter("opcion")!=null && request.getParameter("opcion").equals("consultarCodigo")){
                        ServletFileUpload servletFileUpload = new ServletFileUpload(new DiskFileItemFactory());
                        List fileItemsList = servletFileUpload.parseRequest(request);

                        //// Itero para obtener todos los FileItem
                        Iterator it = fileItemsList.iterator();
                        String nombre_archivo;
                        String fieldtem="",valortem="",idxx="na";
                        String secuenciax="na";
                        String tipitox="na";
                        while (it.hasNext()){
                            FileItem fileItem = (FileItem)it.next();
                            if (fileItem.isFormField()){

                                fieldtem = fileItem.getFieldName();
                                //ystem.out.println("fieldtem"+fieldtem);
                                valortem = fileItem.getString();
                                if (fieldtem.equals("numerit_osx")){
                                    idxx=valortem;//request.getParameter("");
                                }
                                if (fieldtem.equals("tipito")){
                                    tipitox=valortem;
                                }
                                if (fieldtem.equals("tipoconv")) {
                                    next = next + "&tipoconv=" + valortem;
                                }
                                if (fieldtem.equals("vista")) {
                                    next = next + "&vista=" + valortem;
                                }
                                if (fieldtem.equals("form")) {
                                    next = next + "&form=" + valortem;
                                }
                                if (fieldtem.equals("secuenciax")){
                                    secuenciax=valortem;//request.getParameter("");
                                    //ystem.out.println("secuenciax"+secuenciax);
                                }


                            }
                        }
                        String[] archivosx=model.NegociosApplusService.getArchivo(secuenciax,loginx,tipitox);
                        //ystem.out.println("numerit_osxremix"+request.getParameter("numerit_os")+"idxx"+idxx);

                        if (archivosx!=null && archivosx.length>0){
                            next="/jsp/opav/mostrar_archivos.jsp?archivolisto="+archivosx[0]+"&secuenciax="+secuenciax;
                        }else{
                            next="/jsp/opav/mostrar_archivos.jsp?num_osx="+idxx+"&tipito="+tipitox;
                        }


                    }

                    if (request.getParameter("opcion")!=null && request.getParameter("opcion").equals("borrar")){
                        String carpeta="images/multiservicios";
                        ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");//se consiguen los datos de db.properties

                        String directorioArchivos = rb.getString("ruta")+"/"+carpeta+"/"+loginx;//se establece la ruta de la imagen

                        File carpetica =new File (directorioArchivos)    ;

                        deleteDirectory(carpetica);

                        next="/jsp/opav/mostrar_archivos.jsp?cerrar=si";
                    }

                    if (request.getParameter("opcion")!=null && request.getParameter("opcion").equals("borrar2")){
                        String carpeta="images/multiservicios";
                        ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");//se consiguen los datos de db.properties

                        String directorioArchivos = rb.getString("ruta")+"/"+carpeta+"/"+loginx;//se establece la ruta de la imagen

                        File carpetica =new File (directorioArchivos)    ;

                        deleteDirectory(carpetica);

                        next="/jsp/opav/importar.jsp?cerrar=si";
                    }


                    if (request.getParameter("opcion")!=null && request.getParameter("opcion").equals("cxp_formula_prov")){

                        HGenerarFacturaFormulaPro hilo = new HGenerarFacturaFormulaPro();
                       // hilo.start(model, usuario, dstrct);

                        next="/jsp/opav/facturaFormulaCrearPro.jsp?msj=Proceso Iniciado.";
                    }

                    if (request.getParameter("opcion")!=null && request.getParameter("opcion").equals("consultarEstado2")){ //090922
                        if (estado_consultar==null ){
                            estado_consultar="0";
                        }
                        String nic_consultable=request.getParameter("nicc");
                        String nomcli_consultable=request.getParameter("nomclie");
                        String idclie=request.getParameter("idclie");
                        String id_conse=request.getParameter("id_conse");
                        String solici_consultable=request.getParameter("id_solici");

                        String vista = request.getParameter("vista")!=null ? request.getParameter("vista") : "1";
                        next="/jsp/opav/cambiar_estado_negocios2.jsp?estadito=" + estado_consultar+"&contratistica="+contratista_consultar+"&fact_conformada_consultar="+fact_conformada_consultar+
                            "&id_solici="+solici_consultable+"&nicc="+nic_consultable+"&nomclie="+nomcli_consultable+"&id_cliente="+idclie+"&id_conse="+id_conse+"&vista="+vista;

                    }

                    if (request.getParameter("opcion")!=null && request.getParameter("opcion").equals("consultarAccion")){ //
                        String solicitud_consultable=request.getParameter("id_solici");
                        String id_accion=request.getParameter("id_accion");
                        //model.negociosApplusService.obtainAccione(solicitud_consultable,id_accion,loginx);

                        //next="/jsp/opav/acciones2.jsp?id_solici=" + solicitud_consultable+"&id_acci="+id_accion+"&usuari="+loginx;
                        next="/jsp/opav/alcance_acciones.jsp?id_solici=" + solicitud_consultable+"&id_acci="+id_accion+"&usuari="+loginx;
                    }

                    if (request.getParameter("opcion")!=null && request.getParameter("opcion").equals("marcarTipoTrabajo")){ //
                        String solicitud_consultable=request.getParameter("id_solici");
                        String id_accion=request.getParameter("id_accion");
                        String tipotraba=request.getParameter("tipotraba");
                        String observaci=request.getParameter("observaci");

                        String extipotraba=request.getParameter("extipotraba");

                        String fecvisitaplan=request.getParameter("fecvisitaplan");//091030
                        String fecvisitareal=request.getParameter("fecvisitareal");//091030

                        String estadinho=request.getParameter("estadinho");//20100513

                        String marcaTipoTrabajo=model.NegociosApplusService.marcarTipoTrabajo(solicitud_consultable,id_accion,loginx,tipotraba,observaci,extipotraba,fecvisitaplan,fecvisitareal,estadinho);//20100513
                        String auxiliar = "on";//Agregado jpena 07-04-2010

                        next="/jsp/opav/alcance_acciones.jsp?id_solici=" + solicitud_consultable+
                            "&id_acci="+id_accion+"&usuari="+loginx+"&auxiliar="+auxiliar;

                    }

                    if (request.getParameter("opcion")!=null && request.getParameter("opcion").equals("asignar")){ //
                        if (estado_consultar==null ){
                            estado_consultar="0";
                        }
                        String nic_consultable=request.getParameter("nicc");
                        String nomcli_consultable=request.getParameter("nomclie");
                        String solici_consultable=request.getParameter("id_solici");
                        String fecof=request.getParameter("fecof");

                        String[] solicitudes  =  request.getParameterValues("idNegocio");

                        String asignacion=model.NegociosApplusService.asignar(loginx,fecof,solicitudes);

                        next="/jsp/opav/cambiar_estado_negocios2.jsp?estadito=" + estado_consultar+"&contratistica="+contratista_consultar+"&fact_conformada_consultar="+fact_conformada_consultar+
                            "&id_solici="+solici_consultable+"&nicc="+nic_consultable+"&nomclie="+nomcli_consultable;
                    }

                    if (request.getParameter("opcion")!=null && request.getParameter("opcion").equals("denegar")){ //Ivargas
                        if (estado_consultar==null ){
                            estado_consultar="0";
                        }
                        String nic_consultable=request.getParameter("nicc");
                        String nomcli_consultable=request.getParameter("nomclie");
                        String solici_consultable=request.getParameter("id_solici");
                        String[] solicitudes  =  request.getParameterValues("idNegocio");
                        String solic="";
                        if(solicitudes.length>0){
                        for(int i=0;i<solicitudes.length;i++){
                            solic=solic+solicitudes[i]+",";
                        }
                        solic=solic.substring(0, solic.length()-1);
                        }

                        next="/jsp/opav/anular_oferta.jsp?estadito=" + estado_consultar+"&contratistica="+contratista_consultar+"&fact_conformada_consultar="+fact_conformada_consultar+
                            "&id_solici="+solici_consultable+"&nicc="+nic_consultable+"&nomclie="+nomcli_consultable+"&solicitudes="+solic;
                    }

                    if (request.getParameter("opcion")!=null && request.getParameter("opcion").equals("consultarAlcances")){ //
                        String solicitud_consultable=request.getParameter("id_solici");
                        String id_accion=request.getParameter("id_accion");
                        String auxiliar = request.getParameter("auxiliar");//Agregado jpena 07-04-2010

                        next="/jsp/opav/alcance_acciones.jsp?id_solici=" + solicitud_consultable+"&id_acci="+id_accion+"&usuari="+loginx+"&auxiliar="+auxiliar;//Modificado jpena 07-04-2010
                    }

                    if (request.getParameter("opcion")!=null && request.getParameter("opcion").equals("marcarAlcances")){ //
                        String solicitud_consultable=request.getParameter("id_solici");
                        String id_accion=request.getParameter("id_accion");

                        String alcanci=request.getParameter("alcanci");
                        String adicioni=request.getParameter("adicioni");
                        String trabaji=request.getParameter("trabaji");

                        String marcaAvance=model.NegociosApplusService.marcarAvance(solicitud_consultable,id_accion,loginx,alcanci,adicioni,trabaji);
                        String auxiliar = "off";//Agregado jpena 07-04-2010

                        next="/jsp/opav/alcance_acciones.jsp?id_solici=" + solicitud_consultable+"&id_acci="+id_accion+"&usuari="+loginx+"&auxiliar="+auxiliar;//Modificado jpena 07-04-2010
                    }

                    if (request.getParameter("opcion")!=null && request.getParameter("opcion").equals("consultarprod")){ //
                        String idmat=request.getParameter("idmat");
                        next="/jsp/opav/cotizacion/modificar_producto.jsp?materiale="+idmat;
                    }

                    if (request.getParameter("opcion")!=null && request.getParameter("opcion").equals("recibirObra")){ //
                        String solicitud=request.getParameter("solicitud");
                        BeanGeneral dat_recep_obra=model.NegociosApplusService.obtainDatRecepObra(solicitud);


                        next="/jsp/opav/recepcion_obra.jsp?nomcli="+dat_recep_obra.getValor_01()+"&consecutivo="+dat_recep_obra.getValor_02()+
                                "&num_os="+dat_recep_obra.getValor_03()+"&solicitud="+solicitud+"&fec_ult_seg="+dat_recep_obra.getValor_04()+"&por_ava_glo="+dat_recep_obra.getValor_05();//JJCASTRO RECEPCION OBRA


                    }

                    if (request.getParameter("opcion")!=null && request.getParameter("opcion").equals("aceptarrecibirObra")){ //

                        try{

                            String solicitud=request.getParameter("solicitud");

                            String[] observaciones =request.getParameterValues("observ");
                            String[] acciones =request.getParameterValues("accione");

                            String[] fecFin =request.getParameterValues("fecFin");
                            String[] fecInterventor =request.getParameterValues("fecInterventor");
                            //jjcastro
                            String[] fecDocumentacion =request.getParameterValues("fecDocumentacion");
                            String[] liquidacion =request.getParameterValues("liquidacion");
                            String[] informe =request.getParameterValues("informe");
                            String[] acta =request.getParameterValues("acta");
                            String opcionsol =request.getParameter("opcionsol");
                            //jjcastro

                            for (int i=0;i<observaciones.length;i++){
                                System.out.println("observaciones:"+observaciones[i]+"_acciones:"+acciones[i]+"_fecFin:"+fecFin[i]+"_fecInterventor:"+fecInterventor[i]);
                            }

                            String respuesta=model.NegociosApplusService.aceptarRecepObra(solicitud,observaciones,acciones,fecFin,fecInterventor,loginx, fecDocumentacion, liquidacion, informe, acta, opcionsol);//jjcastro recepcion obra
                            msjRecepcionObra=respuesta;

                            BeanGeneral dat_recep_obra=model.NegociosApplusService.obtainDatRecepObra(solicitud);
                            next="/jsp/opav/recepcion_obra.jsp?nomcli="+dat_recep_obra.getValor_01()+"&consecutivo="+dat_recep_obra.getValor_02()+
                                    "&num_os="+dat_recep_obra.getValor_03()+"&solicitud="+solicitud;
                        }catch(Exception e){
                            msjRecepcionObra="Error recibiendo.";
                            System.out.println("error en acion recibiendo obra:"+e.toString()+"__"+e.getMessage());
                            e.printStackTrace();
                        }
                    }
                    
                    if (request.getParameter("opcion")!=null && request.getParameter("opcion").equals("registrarEncuestaObra")){
                        JsonObject obj = (JsonObject) (new JsonParser()).parse(request.getParameter("informacion"));
                        obj.addProperty("usuario",((Usuario) request.getSession().getAttribute("Usuario")).getLogin());
                        msjRecepcionObra = model.NegociosApplusService.insertarEncuesta(obj);
                        response.setContentType("text/plain; charset=utf-8");
                        response.setHeader("Cache-Control", "no-cache");
                        response.getWriter().println(msjRecepcionObra);
                        redirect=false;
                    }
                    
                    if(request.getParameter("opcion")!=null && request.getParameter("opcion").equals("buscarEncuestaObra")) {
                        String solicitud = request.getParameter("solicitud");
                        solicitud = model.NegociosApplusService.buscarEncuesta(solicitud);
                        response.setContentType("application/json; charset=utf-8");
                        response.setHeader("Cache-Control", "no-cache");
                        response.getWriter().println(solicitud);
                        redirect=false;
                    }

                if (request.getParameter("opcion")!=null && request.getParameter("opcion").equals("buscarCliente")){
                    redirect = false;
                    String nomCliente = request.getParameter("txtCliente");
                    ClientesVerService clvsrv = new ClientesVerService(usuario.getBd());
                    ArrayList lista = clvsrv.buscarCliente(nomCliente);
                    strRespuesta="<ul>";
                    for (int i = 0; i < lista.size(); i++) {
                        String[] cliente = (String[])lista.get(i);
                        strRespuesta+="<li id='"+cliente[0]+"'>"+cliente[1]+"</li>";
                    }
                    strRespuesta+="</ul>";
                    response.setContentType("text/plain");
                }

                if (request.getParameter("opcion")!=null && request.getParameter("opcion").equals("buscarEjecutivo")){
                    redirect = false;
                    String nomEjecutivo = request.getParameter("txtEjecutivo");
                    ClientesVerService clvsrv = new ClientesVerService(usuario.getBd());
                    ArrayList lista = clvsrv.buscarEjecutivo(nomEjecutivo);
                    strRespuesta="<ul>";
                    for (int i = 0; i < lista.size(); i++) {
                        String[] ejecutivo = (String[])lista.get(i);
                        strRespuesta+="<li id='"+ejecutivo[0]+"'>"+ejecutivo[1]+"</li>";
                    }
                    strRespuesta+="</ul>";
                    response.setContentType("text/plain");
                }

                if (request.getParameter("opcion")!=null && request.getParameter("opcion").equals("consultarSolicitudes") && !cuadro_informe.equals("informe") ){//----------responsable opav jcastro

                    String estado = request.getParameter("cmbEstado");
                    String vencido = request.getParameter("chkVencido");
                    String cliente = request.getParameter("txtCliente");
                    String ejecutivo = request.getParameter("txtEjecutivo");
                    String contratista = request.getParameter("cmbContratista");
                    String departamento = request.getParameter("cmbDepartamento");
                    String consecutivo = request.getParameter("txtConsecutivo");
                    String nic = request.getParameter("txtNic");
                    String fechaInicial = request.getParameter("txtFechaInicial");
                    String fechaFinal = request.getParameter("txtFechaFinal");
                    String solicitud = request.getParameter("txtSolicitud");
                    //---- RESPONSABLE OPAV
                    String responsable = request.getParameter("responsable")!=null?request.getParameter("responsable"):"";
                    String tipo_solicitud = request.getParameter("tipo_solicitud")!=null?request.getParameter("tipo_solicitud"):"";
                    //---- RESPONSABLE OPAV

                    ClientesVerService clvsrv = new ClientesVerService(usuario.getBd());
                    String perfiles = clvsrv.getPerfil(usuario.getLogin());
                    if(clvsrv.ispermitted(perfiles, "37")){
                        model.NegociosApplusService.consultarSolicitudes(estado, vencido, solicitud, cliente, ejecutivo, contratista, departamento, consecutivo, nic, fechaInicial, fechaFinal, tipo_solicitud, responsable);
                    }else{
                        if(contratista.equals("-1") || contratista.equals(usuario.getNitPropietario())){
                            contratista = usuario.getNitPropietario();
                            model.NegociosApplusService.consultarSolicitudes(estado, vencido, solicitud, cliente, ejecutivo, contratista, departamento, consecutivo, nic, fechaInicial, fechaFinal, tipo_solicitud, responsable);
                        }
                    }

                    next = "/jsp/opav/seguimientoSolicitudes.jsp?resultado=ok&estadoSol="+estado+"&vencido="+vencido+"&solicitud="+solicitud+
                            "&cliente="+cliente+"&ejecutivo="+ejecutivo+"&contratista="+contratista+"&departamento="+departamento+
                            "&consecutivo="+consecutivo+"&nic="+nic+"&fechaInicial="+fechaInicial+"&fechaFinal="+fechaFinal;
                }

                //----------------- RESPONSABLE OPAV JCASTRO


                if (request.getParameter("opcion")!=null && request.getParameter("opcion").equals("consultarSolicitudes") && cuadro_informe.equals("informe") ){

                    String estado = request.getParameter("cmbEstado");
                    String vencido = request.getParameter("chkVencido");
                    String cliente = request.getParameter("txtCliente");
                    String ejecutivo = request.getParameter("txtEjecutivo");
                    String contratista = request.getParameter("cmbContratista");
                    String departamento = request.getParameter("cmbDepartamento");
                    String consecutivo = request.getParameter("txtConsecutivo");
                    String nic = request.getParameter("txtNic");
                    String fechaInicial = request.getParameter("txtFechaInicial");
                    String fechaFinal = request.getParameter("txtFechaFinal");
                    String solicitud = request.getParameter("txtSolicitud");


                    String responsable = request.getParameter("responsable")!=null?request.getParameter("responsable"):"";
                    String tipo_solicitud = request.getParameter("tipo_solicitud")!=null?request.getParameter("tipo_solicitud"):"";

                    ClientesVerService clvsrv = new ClientesVerService(usuario.getBd());
                    String perfiles = clvsrv.getPerfil(usuario.getLogin());
                    if(clvsrv.ispermitted(perfiles, "37")){
                        model.NegociosApplusService.consultarSolicitudesInforme(estado, vencido, solicitud, cliente, ejecutivo, contratista, departamento, consecutivo, nic, fechaInicial, fechaFinal, tipo_solicitud, responsable);
                    }else{
                        if(contratista.equals("-1") || contratista.equals(usuario.getNitPropietario())){
                            contratista = usuario.getNitPropietario();
                            model.NegociosApplusService.consultarSolicitudesInforme(estado, vencido, solicitud, cliente, ejecutivo, contratista, departamento, consecutivo, nic, fechaInicial, fechaFinal, tipo_solicitud, responsable );
                        }
                    }

                    next = "/jsp/opav/seguimientoSolicitudes.jsp?resultado=ok&estadoSol="+estado+"&vencido="+vencido+"&solicitud="+solicitud+
                            "&cliente="+cliente+"&ejecutivo="+ejecutivo+"&contratista="+contratista+"&departamento="+departamento+
                            "&consecutivo="+consecutivo+"&nic="+nic+"&fechaInicial="+fechaInicial+"&fechaFinal="+fechaFinal+"&cuadro_informe=mostrar";
                }




                //----------------- RESPONSABLE OPAV JCASTRO


                if (request.getParameter("opcion")!=null && request.getParameter("opcion").equals("consultarSolicitudesEstado")){

                    String estado = request.getParameter("cmbEstado");
                    String solicitud = request.getParameter("txtSolicitud");
                    //---- RESPONSABLE OPAV
                    String responsable = request.getParameter("responsable")!=null?request.getParameter("responsable"):"";
                    String tipo_solicitud = request.getParameter("tipo_solicitud")!=null?request.getParameter("tipo_solicitud"):"";
                    //---- RESPONSABLE OPAV

                    String cliente = request.getParameter("txtCliente");
                    String contratista = request.getParameter("cmbContratista");
                    String consecutivo = request.getParameter("txtConsecutivo");
                    String nic = request.getParameter("txtNic");
                    String multiservicio = request.getParameter("txtMultiservicio");

                    String interventor = !request.getParameter("interventor").equals("0")?request.getParameter("interventor"):"";//----INTERVENTOR JJCASTRO

                    /*next = "/jsp/opav/consultaSolicitudes.jsp?resultado=ok&estadoSolicitud="+estado+"&solicitud="+solicitud+
                           "&cliente="+cliente+"&contratista="+contratista+"&consecutivo="+consecutivo+"&nic="+nic+"&multiservicio="+multiservicio;*/

                    next = "/jsp/opav/consultaSolicitudes.jsp?resultado=ok&estadoSolicitud="+estado+"&solicitud="+solicitud+
                           "&cliente="+cliente+"&contratista="+contratista+"&consecutivo="+consecutivo+"&nic="+nic+"&multiservicio="+multiservicio+"&interventor="+interventor;

                    ClientesVerService clvsrv = new ClientesVerService(usuario.getBd());
                    String perfiles = clvsrv.getPerfil(usuario.getLogin());
                    String tipo_sol = model.NegociosApplusService.getDescTipoSolc(solicitud);
                    ArrayList negocios = null;
                    if(clvsrv.ispermitted(perfiles, "37")){
                        if(!tipo_sol.equals("AAAE")){
                        negocios = model.NegociosApplusService.getNegociosApplus2(estado,contratista,multiservicio,"","",solicitud,nic,cliente,"",consecutivo, interventor);
                        }else{
                            negocios = model.NegociosApplusService.getNegociosApplusEquipos(estado, contratista, multiservicio, fact_conformada_consultar, loginx, solicitud, nic, "", cliente, consecutivo, "");
                        }
                    }else{
                        if(contratista.equals("-1") || contratista.equals(usuario.getNitPropietario())){
                            contratista = usuario.getNitPropietario();
                             if(!tipo_sol.equals("AAAE")){
                            negocios = model.NegociosApplusService.getNegociosApplus2(estado,contratista,multiservicio,"","",solicitud,nic,cliente,"",consecutivo, interventor);
                             }else{
                            negocios = model.NegociosApplusService.getNegociosApplusEquipos(estado, contratista, multiservicio, fact_conformada_consultar, loginx, solicitud, nic, "", cliente, consecutivo, "");
                             }
                        }
                    }
                    model.NegociosApplusService.setLista(negocios);

                }
                
                //metodo para exportar excel 
                    if (request.getParameter("opcion") != null && request.getParameter("opcion").equals("exportarExcel")) { //090922
                       
                        if (estado_consultar == null) {
                            estado_consultar = "0";
                        }
                        String nic_consultable = request.getParameter("nicc");
                        String nomcli_consultable = request.getParameter("nomclie");
                        String idclie = request.getParameter("idclie");
                        String id_conse = request.getParameter("id_conse");
                        String solici_consultable = request.getParameter("id_solici");
                        String num_oxs = request.getParameter("num_osx");

                        // model.NegociosApplusService.getNegociosApplus2(estado_consultar, contratista_consultar, num_osx, fact_conformed, loginx, id_conse, next, idclie, id_conse, id_conse, observacion);
                        ArrayList negociosApplus2 = model.NegociosApplusService.getNegociosApplus2(estado_consultar, contratista_consultar, num_oxs, fact_conformada_consultar, loginx, solici_consultable, nic_consultable, "", idclie, id_conse, "");
                       
                        JsonArray jsonArray = new JsonArray();
                       
                        for (Object negociosApplus21 : negociosApplus2) {
                            NegocioApplus negocioApplus = (NegocioApplus) negociosApplus21;
                            JsonObject jsonObject = new JsonObject();
                            jsonObject.addProperty("id_solicitud", negocioApplus.getIdSolicitud());
                            jsonObject.addProperty("consecutivo", negocioApplus.getConsecutivo_oferta());
                            jsonObject.addProperty("nro_orden", negocioApplus.getNumOs());
                            jsonObject.addProperty("accion", negocioApplus.getIdAccion());
                            jsonObject.addProperty("estado", negocioApplus.getEstado());
                            jsonObject.addProperty("en_oferta", negocioApplus.getCreacionFechaEntregaOferta());
                            jsonObject.addProperty("id_cliente", negocioApplus.getIdCliente());
                            jsonObject.addProperty("nic", negocioApplus.getNicClient());
                            jsonObject.addProperty("nit", negocioApplus.getNitClient());
                            jsonObject.addProperty("na_cliente", negocioApplus.getNombreCliente());
                            jsonObject.addProperty("tipo_cliente", negocioApplus.getTipoCliente());
                            jsonObject.addProperty("cliente_padre",  model.NegociosApplusService.getNomClientepadre(negocioApplus.getIdCliente()));//
                            jsonObject.addProperty("na_contratista", negocioApplus.getNombreContratista());
                            jsonObject.addProperty("vlr_material", negocioApplus.getMaterial());
                            jsonObject.addProperty("vlr_mano_obra", negocioApplus.getMano_obra());
                            
                            jsonObject.addProperty("vlr_otros", negocioApplus.getOtros());
                            jsonObject.addProperty("administracion", negocioApplus.getAdministracion());
                            jsonObject.addProperty("imprevistos", negocioApplus.getImprevisto());
                            jsonObject.addProperty("utilidad", negocioApplus.getUtilidad());
                            jsonObject.addProperty("porc_admin", negocioApplus.getAdministracion());
                            jsonObject.addProperty("porc_imprevisto", negocioApplus.getPorc_i());
                            jsonObject.addProperty("porc_utilidad", negocioApplus.getPorc_u());
                            jsonObject.addProperty("costo_contratista", negocioApplus.getVlr());
                            jsonObject.addProperty("precio_venta", negocioApplus.getEcaOferta());
                            
                            jsonObject.addProperty("fac_conformada", negocioApplus.getFacturaConformada());
                            jsonObject.addProperty("fecha_factura_conformada", negocioApplus.getFecFactConformed());
                            jsonObject.addProperty("prefactura", negocioApplus.getPrefactura());
                            jsonObject.addProperty("fecha_cxp_contratista", negocioApplus.getFecFacContratistaFin());
                            jsonObject.addProperty("nc_contratista", negocioApplus.getPrefactura());
                            jsonObject.addProperty("acciones", negocioApplus.getAcciones());
                            
                            jsonArray.add(jsonObject);
                        }
                        
                       // JsonArray jsonArray = (JsonArray) new JsonParser().parse(jsonArray);

                        String[] cabecera = null;
                        short[] dimensiones = null;
                        String nombreArchivo = "REPORTE", titulo = "REPORTE SOLICITUD";

                        cabecera = new String[]{"Id solicitud", "Consecutivo", "No orden de trabajo", "Accion", "Estado", "Entrega oferta", "Id cliente", "Nic",
                            "Nit", "Nombre cliente", "Tipo cliente", "Cliente padre", "Nombre contratista", "Valor material", "Valor mano de obra","Valor otros",
                            "Administracion","Imprevistos","Utilidad","% Administracion","% Imprevisto", "% Utilidad", "Costo contratista","Precio venta",
                            "Factura conformada","Fecha factura conformada","Prefactura","Fecha cxp contratista","Nc contratista","Acciones"};

                        dimensiones = new short[]{
                            5000, 5000, 7000, 5000, 9000, 5000, 5000, 5000, 5000, 9000, 5000, 5000, 5000, 5000, 5000,
                            5000,5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 10000
                        };

                        ExcelApiUtil apiUtil = new ExcelApiUtil(usuario);
                        String crearArchivoExcel = apiUtil.crearArchivoExcel(nombreArchivo, titulo, jsonArray, dimensiones, cabecera, request);

                        this.printlnResponseAjax(crearArchivoExcel, "text/plain;");

                        redirect = false;

                    }

                
                    //inicio de 20100222
                    try {
                        if (request.getParameter("opcion")!=null && request.getParameter("opcion").equals("aceptarrecibirObra")){
                            response.setContentType("text/plain; charset=utf-8");//20100520
                            response.setHeader("Cache-Control", "no-cache");//20100520
                            response.getWriter().println(msjRecepcionObra);
                        }
                    }
                    catch (Exception ex) {
                        System.out.println("error en negapplusaction:"+ex.toString()+"__"+ex.getMessage());
                        ex.printStackTrace();
                    }
                    //fin de 20100222

                    
                    
                }catch(Exception ee){
                    System.out.println("eee"+ee.toString()+ee.getMessage());
                }
                try{
                if(redirect){//darrieta
                //if (!(next.equals("na"))){
                    System.out.println("nexttty"+next);
                        rd = application.getRequestDispatcher(next);

                        if(rd == null){
                            System.out.println("no se encontro"+next);
                            throw new ServletException("No se pudo encontrar "+ next);
                        }
                        System.out.println("request"+request+"response"+response);
                        if (!(request.getParameter("opcion")!=null && request.getParameter("opcion").equals("aceptarrecibirObra"))){ //20100222
                            rd.forward(request, response);
                        }//20100222

                }else{//darrieta
                    response.setHeader("Cache-Control", "no-store");
                    response.setDateHeader("Expires", 0);
                    response.getWriter().print(strRespuesta);
                }
                } catch ( Exception e){
                    System.out.println("error en run de action..."+e.toString()+"__"+e.getMessage());
                    e.printStackTrace();
                    throw new ServletException("Error en NegociosApplusAction en  run.....\n"+e.getMessage());
               }
            }//20100513
        }
        }
                } catch ( Exception e){
                     e.printStackTrace();
                    throw new ServletException("Error en NegociosApplusAction en  run.....\n"+e.getMessage());
               }
    }





  public String guardarArchivo(){
    String num_osx="";
    String respuesta="";
    try{

        String carpeta="images/multiservicios";
        ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");//se consiguen los datos de db.properties

        String directorioArchivos = rb.getString("ruta")+"/"+carpeta+"/"+loginx;//se establece la ruta de la imagen

        File carpetica =new File (directorioArchivos)    ;

        //boolean borrar =carpetica.delete();
        deleteDirectory(carpetica);
        //ystem.out.println("borrar::"+borrar);

        this.createDir(directorioArchivos );
        if (ServletFileUpload.isMultipartContent(request)){

            ServletFileUpload servletFileUpload = new ServletFileUpload(new DiskFileItemFactory());
            List fileItemsList = servletFileUpload.parseRequest(request);

            //// Itero para obtener todos los FileItem
            Iterator it = fileItemsList.iterator();
            String nombre_archivo;
            String fieldtem="",valortem="",idxx="";
            String tipitoxx="";

            String multixx="",contratixx="",finarchivo="";//090715

            while (it.hasNext()){
                FileItem fileItem = (FileItem)it.next();
                if (fileItem.isFormField()){

                    fieldtem = fileItem.getFieldName();
                    //ystem.out.println("fieldtem"+fieldtem);
                    valortem = fileItem.getString();
                            if (fieldtem.equals("numerit_osx")){
                                num_osx=valortem;//request.getParameter("conductor_foto");
                                next="/jsp/opav/importar.jsp?num_osx="+num_osx+"&Mensaje=Archivo Cargado.";
                                //next=next+"?num_osx="+num_osx;

                                //ystem.out.println("nexttyyy"+next);
                            }
                            if (fieldtem.equals("idxx")){
                                idxx=valortem;//request.getParameter("");
                            }

                            if (fieldtem.equals("tipito")){
                                tipitoxx=valortem;//request.getParameter("");
                                next=next+"&tipito="+tipitoxx;
                            }


                            if (fieldtem.equals("multixx")){//090715
                                multixx=valortem;
                            }

                            if (fieldtem.equals("contratixx")){//090715
                                contratixx=valortem;
                            }

                            if (fieldtem.equals("finarchivo")){//090715
                                finarchivo=valortem;
                            }

                            if (fieldtem.equals("swmsgx")){//090715
                                //System.out.println("valortem22"+valortem+"contratixx"+contratixx);
                                if (valortem.equals("si") && !(contratixx.equals("")) && finarchivo.equals("OFERTA")){

                                    HSendMail2 hSendMail20=new HSendMail2();
                                    hSendMail20.start("imorales@fintravalores.com", model.electricaribeVerSvc.getEmailEntidad("APPLUS"), "", "",
                                    "oferta",
                                    "APPLUS:  Se ha montado oferta de "+multixx +" , contratista "+contratixx+
                                                        " . Favor consultar Aplicativo de Consorcio Multiservicios en http://200.30.77.38/consorcio  .",loginx,"","");

                                }


                                if (valortem.equals("si") && !(contratixx.equals("")) && finarchivo.equals("FINAL")){

                                    HSendMail2 hSendMail20=new HSendMail2();
                                    hSendMail20.start("imorales@fintravalores.com", model.electricaribeVerSvc.getEmailEntidad("APPLUS"), "", "",
                                    "acta final",
                                    "APPLUS:  Se ha montado acta de finalizaci�n de "+multixx +
                                                        " . Favor consultar Aplicativo de Consorcio Multiservicios en http://200.30.77.38/consorcio  .",loginx,"","");

                                }

                                if (valortem.equals("si") && !(contratixx.equals("")) && finarchivo.equals("CREDITO")){

                                    HSendMail2 hSendMail20=new HSendMail2();
                                    hSendMail20.start("imorales@fintravalores.com", model.electricaribeVerSvc.getEmailEntidad("APPLUS"), "", "",
                                    "credito",
                                    "APPLUS:  Se ha montado el contrato de credito de "+multixx +
                                                        " . Favor consultar Aplicativo de Consorcio Multiservicios en http://200.30.77.38/consorcio  .",loginx,"","");

                                }

                            }

                }
                if (!(fileItem.isFormField())){
                    String nombreCampo = fileItem.getFieldName();
                    //// Tama�o de archivo en bytes
                    long tamanioArchivo = fileItem.getSize();

                    //// Nombre del archivo en el cliente. Algunos navegadores (por ej. IE 6)
                    //// incluyen el path completo, lo que puede implicar separar path
                    //// de nombre.
                    String nombreArchivo = fileItem.getName();

                    //// Content type (tipo MIME) del archivo.
                    //// Esta informaci�n la proporciona el navegador del cliente.
                    //// Algunos ejemplos: .jpg = image/jpeg, .txt = text/plain
                    String contentType = fileItem.getContentType();

                    //// Obtengo caracteristicas de campo y archivo
                    //ystem.out.println( "<p>--> Name:" + nombreCampo + "</p>");
                    //ystem.out.println( "<p>--> Tama�o archivo:" + tamanioArchivo + "</p>");
                    //ystem.out.println( "<p>--> Nombre archivo del cliente:" + nombreArchivo + "</p>");
                    //ystem.out.println( "<p>--> contentType:" + contentType + "</p>");

                    //// Obtengo extensi�n del archivo de cliente
                    String extension = nombreArchivo.substring(nombreArchivo.indexOf("."));
                    //ystem.out.println( "<p>--> Extensi�n del archivo:" + extension + "</p>");

                    //// Guardo archivo del cliente en servidor, con un nombre 'fijo' y la
                    //// extensi�n que me manda el cliente
                    //directorioArchivos=rutica;
                    Date ahora=new Date();
                    String ahorax= Util.dateFormat(ahora);
                    //String soloNombreArchivo="cc"+ num_osx +"fec"+ahorax+"usu"+loginx+"x"+""+ extension;
                    //model.AnticiposGasolinaService.setNombreNewFotoBd(soloNombreArchivo);
                    //ystem.out.println("nombreArchivo::::::hjb::::"+nombreArchivo);
                    String[] nombrearreglo=nombreArchivo.split("\\\\");
                    //ystem.out.println("nombrearreglo[(nombrearreglo.length-1)]"+nombrearreglo[0]);
                    String nombreArchivoremix=nombrearreglo[(nombrearreglo.length-1)];
                    nombre_archivo=directorioArchivos + "/"+ nombreArchivoremix;

                    File archivo = new File(nombre_archivo);

                    //ystem.out.println("nombre_archivo"+nombre_archivo);
                    fileItem.write(archivo);

                    if ( archivo.exists() ){

                            //ystem.out.println( "<p>--> GUARDADO " + archivo.getAbsolutePath() + "</p>");
                            respuesta="&respuesta=Archivo Guardado";
                            //model.AnticiposGasolinaService.setEstado_foto(true);
                            //model.AnticiposGasolinaService.setNombre_archivo(nombre_archivo);
                            //ystem.out.println("idxx"+idxx);
                            guardarArchivoEnBd(num_osx,directorioArchivos,idxx,tipitoxx);
                    }else{
                            //ystem.out.println( "<p>--> FALLO AL GUARDAR. NO EXISTE " + archivo.getAbsolutePath() + "</p>");
                            respuesta="&respuesta=Archivo Rechazado";
                            //model.AnticiposGasolinaService.setEstado_foto(false);
                    }


                }

            }
         }


    }catch(Exception ee){
        System.out.println("eroror:"+ee.toString()+"__"+ee.getMessage());
    }
    return next;
  }

  public void guardarArchivoEnBd(String num_osx,String namecarpeta,String idxx2,String tipito){
        try{
            //ystem.out.println("num_osx"+num_osx+"namecarpeta"+namecarpeta+"loginx"+loginx);

            List archivos   = new LinkedList();

            //ystem.out.println("model.AnticiposGasolinaService.getNombre_archivo()"+model.AnticiposGasolinaService.getNombre_archivo());

            Dictionary fields   = new Hashtable();
            fields.put("actividad",     "023" );
            fields.put("tipoDocumento", "031"   );
            fields.put("documento",     num_osx);
            fields.put("agencia",       "OP"   );
            fields.put("usuario",       loginx);

            File  file                = new File( namecarpeta );

            String comentario="";
            //ystem.out.println("file"+file+"namearchivo "+namecarpeta );
            if(file.exists()){
               //ystem.out.println("existe");
               File []arc =  file.listFiles();
               //ystem.out.println("arc"+arc);
               for (int i=0;i< arc.length;i++){
                   //ystem.out.println("in for"+arc[i].getAbsolutePath()+"__"+arc[i].getName()+"__"+arc[i].getPath());
                     if( ! arc[i].isDirectory()){
                           //ystem.out.println("no dir");
                           String name  = arc[i].getName();
                           //ystem.out.println("name"+name);
                           //if(  model.ImagenSvc.isLoad( model.ImagenSvc.getImagenes(), name)   && model.ImagenSvc.ext(name) ){
                                  //ystem.out.println("isload_"+file+"_"+name);
                                  FileInputStream       in  = new FileInputStream(file  +"/"+  name);
                                  //ystem.out.println("in");
                                  ByteArrayOutputStream out = new ByteArrayOutputStream();
                                  ByteArrayInputStream  bfin;
                                  //ystem.out.println("antes de read");
                                  int input                 = in.read();
                                  //ystem.out.println("despues de read");
                                  byte[] savedFile          = null;
                                  while(input!=-1){
                                      //ystem.out.println("while");
                                      out.write(input);
                                      input  = in.read();
                                      //ystem.out.println("fin de while");
                                  }
                                  in.close();
                                  //ystem.out.println("antes de close");
                                  out.close();
                                  //ystem.out.println("despues de close");
                                  savedFile  = out.toByteArray();
                                  //ystem.out.println("saved");
                                  bfin       = new ByteArrayInputStream( savedFile );
                                  //ystem.out.println("bfin ya");
                                  fields.put("archivo", name );
                                  //ystem.out.println("antes de comentario");
                              // Insertar imagen
                                 comentario += this.insertImagen(bfin, savedFile.length ,  fields,idxx2,tipito);
                                 //ystem.out.println("insertada");
                           //}
                     }
                }
            }

            //model.ImagenSvc.reset();

        }catch(Exception ee){
            System.out.println("eeeche"+ee.toString()+"_"+ee.getMessage());
        }
    }


    public String insertImagen(ByteArrayInputStream bfin, int longitud,  Dictionary fields,String idxx3,String tipito) throws ServletException{
        String estado   = "";
        String fileName = (String)fields.get("archivo");
        //ystem.out.println("longitud"+longitud+"bfin"+bfin.toString());

        try{

        //model.ImagenSvc.setEstado(false);
        model.NegociosApplusService.insertImagen(bfin,longitud,fields,idxx3,tipito);
        estado = "Archivo "+ fileName +" ha sido guardado <br>";

        }catch(Exception e){
            System.out.println("errorrcito"+e.toString()+"__"+e.getMessage());
         estado =  "No se pudo guardar la imagen "+  fileName  +"<br>" + e.getMessage() ;
        }
        return estado;
  }
    public void createDir(String dir) throws Exception{
        try{
        File f = new File(dir);
        if(! f.exists() ) f.mkdir();
     /*   else{
            File []arc =  f.listFiles();
            while( arc.length >0  ){
               File  imagen = arc[0];
               imagen.delete();
               arc =  f.listFiles();
            }
        }*/
        }catch(Exception e){ throw new Exception ( e.getMessage());}
   }

   static public boolean deleteDirectory(File path) {
    if( path.exists() ) {
      File[] files = path.listFiles();
      for(int i=0; i<files.length; i++) {
         if(files[i].isDirectory()) {
           deleteDirectory(files[i]);
         }
         else {
           files[i].delete();
         }
      }
    }
    return( path.delete() );
  }

       private void cargarResponsables() throws Exception {
        String xml = "";
        ClientesVerService clserv = new ClientesVerService(usuario.getBd());
        ArrayList listadoResponsables = clserv.listadoResponsables();

        xml = "<?xml version=\"1.0\" encoding=\"iso-8859-1\"?>";
        xml += "<mensaje>";
        xml += this.getXMLCertificadores(listadoResponsables);
        xml += "</mensaje>";

        response.setContentType("text/xml");
        response.setHeader("Cache-Control", "no-cache");
        response.getWriter().println(xml);
    }


    private String getXMLCertificadores(ArrayList listadoResponsables) throws Exception {
        String xml = "";
        //parte 1 ingresos
        xml += "<responsable codente=\"" +""+ "\">" + "..." + "</responsable>";
        for (int i = 0; i < listadoResponsables.size(); i++) {

            xml += "<responsable codente=\"" + ((String[]) listadoResponsables.get(i))[0] + "\">" + ((String[]) listadoResponsables.get(i))[1] + "</responsable>";
        }
        return xml;
    }

}