/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.opav.controller;
import javax.servlet.http.*;
import com.tsp.opav.model.beans.*;
import com.tsp.opav.model.services.*;
import com.tsp.operation.model.beans.Usuario;
/**
 *
 * @author Rhonalf
 */
public class CotizacionAIUAction extends Action {

    public CotizacionAIUAction(){

    }

    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        String next  = "/jsp/opav/cotizacion/aiu_cotizacion.jsp";
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        CotizacionService csr = new CotizacionService(usuario.getBd());
        String loginx=usuario.getLogin();
        try{
            double porc_a = Double.parseDouble(request.getParameter("porc_a"));
            double porc_i = Double.parseDouble(request.getParameter("porc_i"));
            double porc_u = Double.parseDouble(request.getParameter("porc_u"));
            double val_a = Double.parseDouble(request.getParameter("val_a2"));
            double val_i = Double.parseDouble(request.getParameter("val_i2"));
            double val_u = Double.parseDouble(request.getParameter("val_u2"));
            
            double materiales = Double.parseDouble(request.getParameter("materiales"));
            double manos_obra = Double.parseDouble(request.getParameter("mano"));
            double otros = Double.parseDouble(request.getParameter("otros"));
            String accion = request.getParameter("idaccion");
            csr.valorizarAIU(porc_a, porc_i, porc_u, val_a, val_i, val_u, loginx, accion, materiales, manos_obra, otros);
            request.setAttribute("msg", "");
            next = next + "?idaccion="+accion;
        }
        catch(Exception e){
            System.out.println("Error en el run del action cotizacion aiu: "+e.toString());
            request.setAttribute("msg", "Error en creacion de aiu");
        }
        this.dispatchRequest(next);
    }

}