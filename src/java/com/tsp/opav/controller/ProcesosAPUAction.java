/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.opav.controller;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.tsp.exceptions.InformationException;
import com.tsp.opav.model.DAOS.ProcesosAPUDAO;
import com.tsp.opav.model.DAOS.impl.ProcesosAPUImpl;

import com.tsp.operation.model.beans.Usuario;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;

/**
 *
 * @author user
 */
public class ProcesosAPUAction extends Action {

    private final int CARGAR_COMBO_GRUPO = 1;
    private final int CARGAR_COMBO = 2;
    private final int GUARDAR_GRUPO_APU = 3;
    private final int GET_INSUMOS = 4;
    private final int TIPO_INSUMOS = 5;
    private final int INSUMOS_X_FILTRO = 6;
    private final int GUARDAR_APU = 7;
    private final int CARGAR_APU_X_GRUPO = 8;
    private final int CARGAR_GRID_INSUMOS_EDIT = 9;
    private final int GUARDAR_APU_EDIT = 10;
    private final int CARGAR_GRID_MATERIALES = 11;
    private final int GUARDAR_UNIDAD_MEDIDA = 12;
    private final int CARGAR_COMBO_UNIDADM = 13;
    private final int CARGAR_GRID_SOLICITUDES = 14;
    private final int CARGAR_SUBGRID_SOLICITUDES = 15;
    private final int CARGAR_GRID_APU_ACCIONES = 16;
    private final int CARGAR_SUB_GRID_APU_ACCIONES = 17;
    private final int CLONAR_APU = 18;
    private final int CLONAR_APU2 = 19;

    private JsonObject respuesta;
    private ProcesosAPUDAO dao;
    Usuario usuario = null;
    private String txtResp;

    @Override
    public void run() throws ServletException, InformationException {

        try {
            HttpSession session = request.getSession();
            usuario = (Usuario) session.getAttribute("Usuario");
            dao = new ProcesosAPUImpl(usuario.getBd()) {
            };
            int opcion = (request.getParameter("opcion") != null)
                    ? Integer.parseInt(request.getParameter("opcion"))
                    : -1;
            switch (opcion) {
                case CARGAR_COMBO_GRUPO:
                    this.cargar_combo_grupo();
                    break;
                case CARGAR_COMBO:
                    this.cargar_combo();
                    break;
                case GUARDAR_GRUPO_APU:
                    this.guardar_grupo_apu();
                    break;
                case GET_INSUMOS:
                    this.getInsumos();
                    break;
                case TIPO_INSUMOS:
                    this.getTiposMaterial();
                    break;
                case INSUMOS_X_FILTRO:
                    this.getInsumosXFiltro();
                    break;
                case GUARDAR_APU:
                    this.guardar_armado_apu();
                    break;
                case CARGAR_APU_X_GRUPO:
                    this.getAPUXGrupo();
                    break;
                case CARGAR_GRID_INSUMOS_EDIT:
                    this.cargar_grid_insumos_edit();
                    break;
                case GUARDAR_APU_EDIT:
                    this.guardar_armado_apu_edit();
                    break;
                case CARGAR_GRID_MATERIALES:
                    this.cargar_grid_materiales();
                    break;
                case GUARDAR_UNIDAD_MEDIDA:
                    this.guardar_unidad_medida();
                    break;
                case CARGAR_COMBO_UNIDADM:
                    this.cargar_combo_unidad();
                    break;
                case CARGAR_GRID_SOLICITUDES:
                    this.cargar_grid_solicitudes();
                    break;
                case CARGAR_SUBGRID_SOLICITUDES:
                    this.cargar_subgrid_solicitudes();
                    break;
                case CARGAR_GRID_APU_ACCIONES:
                    this.cargar_grid_apu_solicitudes();
                    break;
                case CARGAR_SUB_GRID_APU_ACCIONES:
                    this.cargar_sub_grid_apu_solicitudes();
                    break;
                case CLONAR_APU:
                    this.clonar_apu();
                    break;
                case CLONAR_APU2:
                    this.clonar_apu2();
                    break;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    
    private void cargar_combo_unidad() throws IOException {
        JsonObject info = (JsonObject) (new JsonParser()).parse(request.getParameter("informacion"));
        respuesta = dao.cargar_combo_grupo(info);
        response.setContentType("application/json; charset=utf-8");
        response.setHeader("Cache-Control", "no-cache");
        response.getWriter().println((new Gson()).toJson(respuesta));
    }
    
    private void guardar_unidad_medida() {
        try {
            String nombre = request.getParameter("nombre");
            String empresa = usuario.getDstrct();

            String resp = "{}";
            if (!dao.existeUnidadMedida(empresa, nombre)) {
                resp = dao.guardarUnidadMedida(empresa, nombre, usuario.getLogin());
            } else {
                resp = "{\"error\":\" No se creo el meta proceso, puede que el nombre ya exista\"}";
            }
            this.printlnResponse(resp, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private void cargar_grid_materiales() throws IOException {
        String ids = request.getParameter("ids");
        String tipo = request.getParameter("tipo");
        respuesta = dao.cargarGridMateriales(ids,tipo);
        response.setContentType("application/json; charset=utf-8");
        response.setHeader("Cache-Control", "no-cache");
        response.getWriter().println((new Gson()).toJson(respuesta));
    }
    
    private void cargar_grid_insumos_edit() throws IOException {
        String ids = request.getParameter("ids");;
        respuesta = dao.cargarGridInsumosEdit(ids);
        response.setContentType("application/json; charset=utf-8");
        response.setHeader("Cache-Control", "no-cache");
        response.getWriter().println((new Gson()).toJson(respuesta));
    }

    private void cargar_combo_grupo() throws IOException {
        JsonObject info = (JsonObject) (new JsonParser()).parse(request.getParameter("informacion"));
        respuesta = dao.cargar_combo_grupo(info);
        response.setContentType("application/json; charset=utf-8");
        response.setHeader("Cache-Control", "no-cache");
        response.getWriter().println((new Gson()).toJson(respuesta));
    }

    private void cargar_combo() throws IOException {
        JsonObject info = (JsonObject) (new JsonParser()).parse(request.getParameter("informacion"));
        respuesta = dao.cargar_combo(info);
        response.setContentType("application/json; charset=utf-8");
        response.setHeader("Cache-Control", "no-cache");
        response.getWriter().println((new Gson()).toJson(respuesta));
    }

    private void guardar_grupo_apu() {
        try {
            String nombre = request.getParameter("nombre");
            String descripcion = request.getParameter("descripcion");
            String empresa = usuario.getDstrct();

            String resp = "{}";
            if (!dao.existeGrupoApu(empresa, nombre)) {
                resp = dao.guardarGrupoApu(empresa, nombre, descripcion, usuario.getLogin());
            } else {
                resp = "{\"error\":\" No se creo el meta proceso, puede que el nombre ya exista\"}";
            }
            this.printlnResponse(resp, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void printlnResponse(String respuesta, String contentType) throws Exception {
        response.setContentType(contentType + " charset=UTF-8;");
        response.setHeader("Cache-Control", "no-store");
        response.setDateHeader("Expires", 0);
        response.getWriter().println(respuesta);

    }

    private void getInsumos() {
        try {
            String id = (request.getParameter("id") != null) ? request.getParameter("id") : "0";
            txtResp = dao.cargarInsumosXTipo(id);
            this.printlnResponse(txtResp, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void getTiposMaterial() {
        try {
            txtResp = dao.cargarTiposMaterial();
            this.printlnResponse(txtResp, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private void getInsumosXFiltro() throws IOException {
        String idsubcategoria = request.getParameter("subcategoria");;
        respuesta = dao.cargarInsumosXFiltro(idsubcategoria);
        response.setContentType("application/json; charset=utf-8");
        response.setHeader("Cache-Control", "no-cache");
        response.getWriter().println((new Gson()).toJson(respuesta));
    }
    
    private void guardar_armado_apu() throws IOException {
        JsonObject info = (JsonObject) (new JsonParser()).parse(request.getParameter("informacion"));
        info.addProperty("usuario", ((Usuario) request.getSession().getAttribute("Usuario")).getLogin());
        info.addProperty("dstrct", ((Usuario) request.getSession().getAttribute("Usuario")).getDstrct());
        info.addProperty("nit_propietario", ((Usuario) request.getSession().getAttribute("Usuario")).getNitPropietario());
        info.addProperty("grupo_apu", (request.getParameter("grupo_apu")));
        info.addProperty("unidadm", (request.getParameter("unidadm")));
        info.addProperty("nomapu", (request.getParameter("nomapu")));
        respuesta = dao.guardarAPU(info);
        response.setContentType("application/json; charset=utf-8");
        response.setHeader("Cache-Control", "no-cache");
        response.getWriter().println((new Gson()).toJson(respuesta));
    }
    
    private void getAPUXGrupo() throws IOException {
        String grupo_apu = request.getParameter("grupo_apu");
        respuesta = dao.cargarAPUXGrupo(grupo_apu);
        response.setContentType("application/json; charset=utf-8");
        response.setHeader("Cache-Control", "no-cache");
        response.getWriter().println((new Gson()).toJson(respuesta));
    }
    
    private void guardar_armado_apu_edit() throws IOException {
        JsonObject info = (JsonObject) (new JsonParser()).parse(request.getParameter("informacion"));
        info.addProperty("usuario", ((Usuario) request.getSession().getAttribute("Usuario")).getLogin());
        info.addProperty("dstrct", ((Usuario) request.getSession().getAttribute("Usuario")).getDstrct());
        info.addProperty("nit_propietario", ((Usuario) request.getSession().getAttribute("Usuario")).getNitPropietario());
        info.addProperty("grupo_apu", (request.getParameter("grupo_apu")));
        info.addProperty("nomapu", (request.getParameter("nomapu")));
        info.addProperty("ids", (request.getParameter("ids")));
        respuesta = dao.guardarAPUEdit(info);
        response.setContentType("application/json; charset=utf-8");
        response.setHeader("Cache-Control", "no-cache");
        response.getWriter().println((new Gson()).toJson(respuesta));
    }
    
    private void cargar_grid_solicitudes() throws IOException {
        String nit_proveedor = ((Usuario) request.getSession().getAttribute("Usuario")).getNitPropietario();
        respuesta = dao.cargarGridSolicitudes(nit_proveedor);
        response.setContentType("application/json; charset=utf-8");
        response.setHeader("Cache-Control", "no-cache");
        response.getWriter().println((new Gson()).toJson(respuesta));
    }
    
    private void cargar_subgrid_solicitudes() throws IOException {
        String nit_proveedor = ((Usuario) request.getSession().getAttribute("Usuario")).getNitPropietario();
        String id =request.getParameter("id");
        respuesta = dao.cargarSubGridSolicitudes(nit_proveedor, id);
        response.setContentType("application/json; charset=utf-8");
        response.setHeader("Cache-Control", "no-cache");
        response.getWriter().println((new Gson()).toJson(respuesta));
    }
    
    private void cargar_grid_apu_solicitudes() throws IOException {
        String id =request.getParameter("id");
        respuesta = dao.cargarGridApuSolicitudes(id);
        response.setContentType("application/json; charset=utf-8");
        response.setHeader("Cache-Control", "no-cache");
        response.getWriter().println((new Gson()).toJson(respuesta));
    }
    
    private void cargar_sub_grid_apu_solicitudes() throws IOException {
        String id_accion =request.getParameter("id");
        respuesta = dao.cargarSubGridAPUSolicitudes(id_accion);
        response.setContentType("application/json; charset=utf-8");
        response.setHeader("Cache-Control", "no-cache");
        response.getWriter().println((new Gson()).toJson(respuesta));
    }

    private void clonar_apu() throws IOException {
        String id_apu =request.getParameter("id_apu");
        respuesta = dao.clonar_apu(id_apu,usuario);
        response.setContentType("application/json; charset=utf-8");
        response.setHeader("Cache-Control", "no-cache");
        response.getWriter().println((new Gson()).toJson(respuesta));
    }

    private void clonar_apu2() throws IOException {
        String id_apu =request.getParameter("id_apu");
        String id_actividad_capitulo = request.getParameter("id_actividad_capitulo");
        respuesta = dao.clonar_apu2(id_apu,id_actividad_capitulo,usuario);
        response.setContentType("application/json; charset=utf-8");
        response.setHeader("Cache-Control", "no-cache");
        response.getWriter().println((new Gson()).toJson(respuesta));
    }
    
}
