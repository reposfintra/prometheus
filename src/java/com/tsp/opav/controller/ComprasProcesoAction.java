/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.

 */
package com.tsp.opav.controller;

/**
 *
 * @author user
 */
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.tsp.exceptions.InformationException;
import com.tsp.opav.model.DAOS.ComprasProcesoDAO;
//import com.tsp.opav.model.DAOS.impl.ProcesosClienteImpl;
import com.tsp.opav.model.DAOS.impl.ComprasProcesoImpl;
        


import com.tsp.opav.model.beans.AccionesEca;
import com.tsp.opav.model.beans.CotizacionSl;
import com.tsp.operation.model.beans.BeanGeneral;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.beans.Cliente;
import com.tsp.opav.model.beans.OfertaElca;
import com.tsp.operation.controller.EmailSenderService;
import com.tsp.operation.model.TransaccionService;


import com.tsp.util.ExcelApiUtil;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;

import java.util.logging.Level;
import com.tsp.util.Util;
import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;

public class ComprasProcesoAction extends Action {

    private final int cargarInsumosProyecto = 0;
    private final int GuardarPreInsumos = 1;
    private final int CrearSolicitudOCS = 2;
    private final int EstadoSolicitudes = 3;
    private final int VisualizarSolicitudes = 4;
    private final int EliminarSolicitudes = 5;
    private final int PasarSolicitudaCompras = 6;
    private final int TodasLasSolicitudes = 7;
    private final int SolicitudAorden = 8;
    private final int AddElemToOrderOCS = 9;
    private final int GuardarOCS = 10;
    private final int GuardarDetalleOCS = 11;
    private final int VisualizarProveedores = 12;
    private final int LoadOCS = 13;
    private final int VisualizarOrdenesCS = 14;
    private final int VerificarPreOCS = 15;
    private final int IMPRIMIROCS = 16;
    private final int SaveCatalogoInsumos = 17;
    private final int InfoPreSolicitud = 18;
    private final int EliminarPreOCS = 19;
    private final int GuardarRechazarSolicitud = 20;
    private final int ActualizarSolicitudes = 21;
    private final int DevolverSolicitud = 22;
    private final int EliminarOCS = 23;
    private final int InventarioListado = 24;
    private final int InventarioDetalle = 25;
    private final int GuardarDespacho = 26;
    private final int GuardarDetalleDespacho = 27;
    private final int InventarioDespacho = 28;
    private final int DespachosListado = 29;
    private final int DespachosDetalles = 30;
    private final int VisualizarProyectos = 31;
    private final int VisualizarItemsBodega = 32;
    private final int CargarListaInsumos = 33;
    private final int GuardarMovimiento = 34;
    private final int ImprimirPDFMovimiento = 35;
    private final int CargarCombo = 36;
    private final int CargarProyectosTraslado = 37;
    private final int CargarHistoricoMaterialEnBodega = 38;
    private final int VerificarOCS = 39;
    private final int cargarSolicitudesEjecucion = 40;
    private final int cargarDetalleSolicitudEjecucion = 41;
    private final int guardarDetalleSolicitudEjecucion = 42;
    private final int cargarSolicitudesEjecucionParaDespacho = 43;
    private final int convertirSolicitudEnMovimiento = 44;
    private final int rechazarSolicitud = 45;
    private final int borrarSolicitud = 46;
    private final int obternerCotizacionTerc = 47;
    

        
    private ComprasProcesoDAO dao;
    Usuario usuario = null;
    String reponseJson = "{}";
            
    @Override
    public void run() throws ServletException, InformationException {
        try {

            HttpSession session = request.getSession();
            usuario = (Usuario) session.getAttribute("Usuario");
            dao = new ComprasProcesoImpl(usuario.getBd());
            int opcion = (request.getParameter("opcion") != null)
                    ? Integer.parseInt(request.getParameter("opcion"))
                    : -1;
            switch (opcion) {
                
                case cargarInsumosProyecto:
                    this.cargarSolicitudes(request.getParameter("usuario"),request.getParameter("id_solicitud"),request.getParameter("ano"),request.getParameter("mes"),request.getParameter("tiposolicitud"),request.getParameter("EstadoSolicitud"));
                    break;

                case GuardarPreInsumos:
                    this.PreGuardarInsumos(request.getParameter("usuario"),request.getParameter("id_solicitud"));
                    break;
                    
                case CrearSolicitudOCS:
                    //this.WhatIdo(request.getParameter("usuario"), request.getParameter("id_solicitud"), request.getParameter("tiposolicitud"), request.getParameter("tipo_bodega"), request.getParameter("descripcion"), request.getParameter("fecha_actual"), request.getParameter("fecha_entrega"), request.getParameter("direccion_entrega"), request.getParameter("id_bodega"));
                    //por defecto todas las solicitudes de compra van a llegar a Bodega Principal
                    this.WhatIdo(request.getParameter("usuario"), request.getParameter("id_solicitud"), request.getParameter("tiposolicitud"), "1" , request.getParameter("descripcion"), request.getParameter("fecha_actual"), request.getParameter("fecha_entrega"), request.getParameter("direccion_entrega"), "1");
                    break;
                    
                case EstadoSolicitudes:
                    this.EstadoSolicitud(request.getParameter("usuario"), request.getParameter("id_solicitud"));
                    break;
                    
                case VisualizarSolicitudes:
                    this.VisualizarSolicitud(request.getParameter("cod_solicitud"));
                    break;

                case EliminarSolicitudes:
                    this.DeleteSolicitud(request.getParameter("usuario"), request.getParameter("id_solicitud"),request.getParameter("CodSol"));
                    break;
                    
                case PasarSolicitudaCompras:
                    this.PassSolicitudaCompras(request.getParameter("usuario"), request.getParameter("id_solicitud"),request.getParameter("CodSol"));
                    break;
                    
                case TodasLasSolicitudes:
                    this.SolicitudesDeTodos(request.getParameter("ano"),request.getParameter("mes"),request.getParameter("tiposolicitud"));
                    break;
                    
                case SolicitudAorden:
                    this.SolicitudToOrden(request.getParameter("consultaDB"), request.getParameter("ano"),request.getParameter("mes"),request.getParameter("tiposolicitud"));
                    break;
                    
                case AddElemToOrderOCS:
                    this.AddElemToOrderOCS(request.getParameter("modo_compra"), request.getParameter("usuario"), request.getParameter("cod_solicitud"), request.getParameter("cod_insumo"));
                    break;
                    
                case GuardarOCS:
                    this.GuardarOCS(request.getParameter("usuario"), request.getParameter("tipo_solicitud"), request.getParameter("proveedor"), request.getParameter("descripcion"), request.getParameter("fecha_entrega"), request.getParameter("direccion_entrega"), request.getParameter("f_pago") , request.getParameter("id_bodega"));
                    break;

                case GuardarDetalleOCS:
                    this.GuardarDetalleOCS(request.getParameter("oSC") );
                    break;
                    
                case VisualizarProveedores:
                    this.VisualizarProveedores();
                    break;
                    
                case LoadOCS:
                    this.LoadOCS(request.getParameter("consultaDB"), request.getParameter("ano"),request.getParameter("mes"),request.getParameter("tiposolicitud"));
                    break;
                    
                case VisualizarOrdenesCS:
                    this.VisualizarOrdenesCS(request.getParameter("codOCS"));
                    break;
                    
                case VerificarPreOCS:
                    this.VerificarPreOCS(request.getParameter("usuario"));
                    break;
                case IMPRIMIROCS:
                    imprimirOCS();
                    break;
                    
                case SaveCatalogoInsumos:
                    this.SaveCatalogoInsumos(request.getParameter("usuario"), request.getParameter("id_solicitud"), request.getParameter("TipoInsumo"), request.getParameter("CodigoInsumo"), request.getParameter("DescripcionInsumo"), request.getParameter("NombreUnidadInsumo"), request.getParameter("Cantidad"), request.getParameter("CdSol"));
                    break;
                    
                case InfoPreSolicitud:
                    this.InfoPreSolicitud(request.getParameter("RqSol"));
                    break;

                case EliminarPreOCS:
                    this.EliminarPreOCS(request.getParameter("usuario"));
                    break;
                    
                case GuardarRechazarSolicitud:
                    this.GuardarRechazarSolicitud(request.getParameter("usuario"),request.getParameter("sol_aprobar"),request.getParameter("opcion_accion"),request.getParameter("DescripcionAprobacion"));
                    break;
                    
                case ActualizarSolicitudes:
                    this.ActualizarSolicitudes(request.getParameter("usuario"),request.getParameter("oSC"));
                    break;                    

                case DevolverSolicitud:
                    this.DevolverSolicitud(request.getParameter("cod_solicitud"));
                    break;                    

                case EliminarOCS:
                    this.EliminarOCS(request.getParameter("cod_OCS"), request.getParameter("usuario"));
                    break;                    
                    
                case InventarioListado:
                    this.InventarioTodos(usuario.getLogin(), request.getParameter("ano"),request.getParameter("mes"),request.getParameter("tipomovimiento"));
                    break;

                case InventarioDetalle:
                    this.InventarioDetalle(request.getParameter("cod_movimiento"));
                    break;

                case GuardarDespacho:
                    this.GuardarDespacho(request.getParameter("usuario"), request.getParameter("tipo_solicitud"), request.getParameter("proveedor"), request.getParameter("descripcion"), request.getParameter("fecha_entrega"), request.getParameter("direccion_entrega"), request.getParameter("OrdenCompDespacho"));
                    break;

                case GuardarDetalleDespacho:
                    this.GuardarDetalleDespacho(request.getParameter("NoDsptch") );
                    break;
                    
                case InventarioDespacho:
                    this.InventarioDespacho(request.getParameter("NoDsptch") );
                    break;
                    
                case DespachosListado:
                    this.DespachosListado(request.getParameter("ano"),request.getParameter("mes"));
                    break;

                case DespachosDetalles:
                    this.DespachosDetalles(request.getParameter("cod_movimiento"));
                    break;
                    
                case VisualizarProyectos:
                    this.VisualizarProyectos();
                    break;
                    
                case VisualizarItemsBodega:
                    this.VisualizarItemsBodega(request.getParameter("id_bodega"), request.getParameter("tipo_movimiento"), request.getParameter("id_solicitud"));
                    break;   
                    
                case CargarListaInsumos:
                    this.CargarListaInsumos();
                    break;

                case GuardarMovimiento:     
                    this.GuardarMovimiento(usuario, request.getParameter("fecha_transaccion"),request.getParameter("responsable"),request.getParameter("descripcion"),request.getParameter("id_solicitud_proyecto"),request.getParameter("id_bodega"), request.getParameter("detalle"), request.getParameter("tipo_movimiento"), request.getParameter("id_solicitud_destino"), request.getParameter("id_bodega_destino"));
                    break;
                    
                case ImprimirPDFMovimiento:     
                    imprimirPDFMovimiento();
                    break;                        
                                                                                
                case CargarCombo:     
                    CargarCombo();
                    break;
                    
                case CargarProyectosTraslado:     
                    CargarProyectosTraslado();
                    break;

                case CargarHistoricoMaterialEnBodega:
                    this.CargarHistoricoMaterialEnBodega(request.getParameter("id_bodega"), request.getParameter("cod_material"), request.getParameter("id_solicitud"));
                    break;  

                case VerificarOCS:
                    this.VerificarOCS(request.getParameter("orden_compra"));
                    break;
                
                case cargarSolicitudesEjecucion:     
                    cargarSolicitudesEjecucion();
                    break;  

                case cargarDetalleSolicitudEjecucion:     
                    cargarDetalleSolicitudEjecucion(request.getParameter("id_solicitud_ejecucion"));
                    break;
                    
                case guardarDetalleSolicitudEjecucion:
                    guardarDetalleSolicitudEjecucion();
                    break;

                case cargarSolicitudesEjecucionParaDespacho:     
                    cargarSolicitudesEjecucionParaDespacho();
                    break;      
                    
                case convertirSolicitudEnMovimiento:     
                    convertirSolicitudEnMovimiento();
                    break;
                    
                case rechazarSolicitud:     
                    rechazarSolicitud();
                    break; 
                    
                case borrarSolicitud:     
                    borrarSolicitud();
                    break;                        
                    
                case obternerCotizacionTerc:     
                    obternerCotizacionTerc();
                    break;                        
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }    
    private void cargarSolicitudes(String Usuario, String IdSolicitud, String ano, String mes, String tiposolicitud, String EstadoSolicitud) {
        try {
            String json = dao.cargarSolicitudes(Usuario, IdSolicitud, ano, mes, tiposolicitud, EstadoSolicitud);
            this.printlnResponseAjax(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void PreGuardarInsumos(String Usuario, String IdSolicitud) {
        String json = "";
        try {
            JsonObject informacion = (JsonObject) (new JsonParser()).parse(request.getParameter("informacion"));
            this.printlnResponseAjax(dao.PreSaveInsumos(informacion, Usuario, IdSolicitud), "application/json;");

        } catch (Exception ex) {
            ex.printStackTrace();
            json = "{\"respuesta\":\"ERROR:)\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }
    
    public void WhatIdo(String Usuario, String IdSolicitud, String tiposolicitud, String tipo_bodega, String descripcion, String fecha_actual, String fecha_entrega, String direccion_entrega, String id_bodega) {
        String json = "";
        try {
            //JsonObject informacion = (JsonObject) (new JsonParser()).parse(request.getParameter("informacion"));
            this.printlnResponseAjax(dao.sp_WhatIdo(Usuario, IdSolicitud, tiposolicitud, tipo_bodega, descripcion, fecha_actual, fecha_entrega, direccion_entrega, id_bodega), "application/json;");

        } catch (Exception ex) {
            ex.printStackTrace();
            json = "{\"respuesta\":\"ERROR:)\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }    
    
    public void EstadoSolicitud(String Usuario, String IdSolicitud) {
        String json = "";
        try {
            //JsonObject informacion = (JsonObject) (new JsonParser()).parse(request.getParameter("informacion"));
            this.printlnResponseAjax(dao.EstadoSolicitud(Usuario, IdSolicitud), "application/json;");

        } catch (Exception ex) {
            ex.printStackTrace();
            json = "{\"respuesta\":\"ERROR:)\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }    


    public void VisualizarSolicitud(String CodSolicitud) {
        String json = "";
        try {
            //JsonObject informacion = (JsonObject) (new JsonParser()).parse(request.getParameter("informacion"));
            this.printlnResponseAjax(dao.VisualizarSolicitudes(CodSolicitud), "application/json;");

        } catch (Exception ex) {
            ex.printStackTrace();
            json = "{\"respuesta\":\"ERROR:)\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }     
    
    public void DeleteSolicitud(String Usuario, String IdSolicitud, String CodigoSolicitud) {
        String json = "";
        try {
            //JsonObject informacion = (JsonObject) (new JsonParser()).parse(request.getParameter("informacion"));
            this.printlnResponseAjax(dao.DeleteSolicitud(Usuario, IdSolicitud, CodigoSolicitud), "application/json;");

        } catch (Exception ex) {
            ex.printStackTrace();
            json = "{\"respuesta\":\"ERROR:)\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }     

    public void PassSolicitudaCompras(String Usuario, String IdSolicitud, String CodigoSolicitud) {
        String json = "";
        try {
            //JsonObject informacion = (JsonObject) (new JsonParser()).parse(request.getParameter("informacion"));
            this.printlnResponseAjax(dao.PassSolicitudaCompras(Usuario, IdSolicitud, CodigoSolicitud), "application/json;");

        } catch (Exception ex) {
            ex.printStackTrace();
            json = "{\"respuesta\":\"ERROR:)\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }
    
    
    private void SolicitudesDeTodos(String ano, String mes, String tiposolicitud) {
        try {
            String json = dao.SolicitudesDeTodos(ano, mes, tiposolicitud);
            this.printlnResponseAjax(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private void SolicitudToOrden(String consultaDB, String ano, String mes, String tiposolicitud) {
        try {
            String json = dao.SolicitudToOrden(consultaDB, ano, mes, tiposolicitud);
            this.printlnResponseAjax(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void AddElemToOrderOCS(String modo_compra, String Usuario, String CodigoSolicitud, String cod_insumo) {
        String json = "";
        try {
            //JsonObject informacion = (JsonObject) (new JsonParser()).parse(request.getParameter("informacion"));
            this.printlnResponseAjax(dao.AddElemToOrderOCS(modo_compra, Usuario, CodigoSolicitud, cod_insumo), "application/json;");

        } catch (Exception ex) {
            ex.printStackTrace();
            json = "{\"respuesta\":\"ERROR:)\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }     
    
    public void GuardarOCS(String Usuario, String tipo_solicitud, String proveedor, String descripcion, String fecha_entrega, String direccion_entrega, String f_pago, String id_bodega) {
        String json = "";
        try {
            
            this.printlnResponseAjax(dao.SaveOCS(Usuario, tipo_solicitud, proveedor, descripcion, fecha_entrega, direccion_entrega, f_pago , id_bodega), "application/json;");

        } catch (Exception ex) {
            ex.printStackTrace();
            json = "{\"respuesta\":\"ERROR:)\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    public void GuardarDetalleOCS(String oSC) {
        String json = "";
        try {
            JsonObject informacion = (JsonObject) (new JsonParser()).parse(request.getParameter("informacion"));
            this.printlnResponseAjax(dao.SaveDetailsOCS(informacion, oSC), "application/json;");

        } catch (Exception ex) {
            ex.printStackTrace();
            json = "{\"respuesta\":\"ERROR:)\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }
    
    public void VisualizarProveedores() {
        String json = "";
        try {
            //JsonObject informacion = (JsonObject) (new JsonParser()).parse(request.getParameter("informacion"));
            this.printlnResponseAjax(dao.VisualizarProveedor(), "application/json;");

        } catch (Exception ex) {
            ex.printStackTrace();
            json = "{\"respuesta\":\"ERROR:)\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }       
    
    private void LoadOCS(String consultaDB, String ano, String mes, String tiposolicitud) {
        try {
            String json = dao.LoadOrdenCS(usuario.getLogin(), consultaDB, ano, mes, tiposolicitud);
            this.printlnResponseAjax(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void VisualizarOrdenesCS(String CodOrden) {
        String json = "";
        try {
            //JsonObject informacion = (JsonObject) (new JsonParser()).parse(request.getParameter("informacion"));
            this.printlnResponseAjax(dao.VisualizarOrdenCS(CodOrden), "application/json;");

        } catch (Exception ex) {
            ex.printStackTrace();
            json = "{\"respuesta\":\"ERROR:)\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }     
    
    
    
    public void VerificarPreOCS(String Usuario) {
        String json = "";
        try {
            //JsonObject informacion = (JsonObject) (new JsonParser()).parse(request.getParameter("informacion"));
            this.printlnResponseAjax(dao.VerificarPreOCS(Usuario), "application/json;");

        } catch (Exception ex) {
            ex.printStackTrace();
            json = "{\"respuesta\":\"ERROR:)\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }  
    
    public void imprimirOCS() {
        String json = "";
        try {
            String orden_compra = request.getParameter("orden_compra") == null ? "" : request.getParameter("orden_compra");
            if (usuario.getBd().equals("provintegral")) {
                json = "{\"respuesta\":\"" + dao.imprimirOCS_Provintegral(orden_compra, usuario) + "\"}";
            }else{
                json = "{\"respuesta\":\"" + dao.imprimirOCS(orden_compra, usuario) + "\"}";
            }
            
            this.printlnResponseAjax(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        }
    }
    
    public void SaveCatalogoInsumos(String Usuario, String id_solicitud, String TipoInsumo, String CodigoInsumo, String DescripcionInsumo, String NombreUnidadInsumo, String Cantidad, String CdSol) {
        String json = "";
        try {
            
            this.printlnResponseAjax(dao.SalvarCatalogoInsumos(Usuario, id_solicitud, TipoInsumo, CodigoInsumo, DescripcionInsumo, NombreUnidadInsumo, Cantidad, CdSol), "application/json;");

        } catch (Exception ex) {
            ex.printStackTrace();
            json = "{\"respuesta\":\"ERROR:)\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }
    
    public void InfoPreSolicitud(String ReqSolicitud) {
        String json = "";
        try {
            
            this.printlnResponseAjax(dao.InfoPreSolicitud(ReqSolicitud), "application/json;");

        } catch (Exception ex) {
            ex.printStackTrace();
            json = "{\"respuesta\":\"ERROR:)\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }    
    
    public void EliminarPreOCS(String uSer) {
        String json = "";
        try {
            
            this.printlnResponseAjax(dao.EliminarPreOCS(uSer), "application/json;");

        } catch (Exception ex) {
            ex.printStackTrace();
            json = "{\"respuesta\":\"ERROR:)\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }    
    
    
    public void GuardarRechazarSolicitud(String uSer, String SolAprobar, String OpcionAccion, String DescripcionAprobacion) {
        String json = "";
        try {
            
            this.printlnResponseAjax(dao.GuardarRechazarSolicitud(uSer, SolAprobar, OpcionAccion, DescripcionAprobacion), "application/json;");

        } catch (Exception ex) {
            ex.printStackTrace();
            json = "{\"respuesta\":\"ERROR:)\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    public void ActualizarSolicitudes(String uSers, String oSC) {
        String json = "";
        try {
            
            this.printlnResponseAjax(dao.ActualizarSolicitudes(uSers, oSC), "application/json;");

        } catch (Exception ex) {
            ex.printStackTrace();
            json = "{\"respuesta\":\"ERROR:)\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }
    
    public void DevolverSolicitud(String CodSolicitud) {
        String json = "";
        try {
        String respuesta = dao.DevolverSolicitud(CodSolicitud); 
        
        //Envio de Email
        if (respuesta.equals("POSITIVO") ){
            JsonObject jobj = dao.getInfoCuentaEnvioCorreo();
            String MyMessage = "Su solicitud de compra No." + CodSolicitud + " ha sido rechazada por el area de compras.";
            String mailsToSend = dao.getInfoCuentaEnvioCorreoDestino(CodSolicitud);
            String asunto = "Solicitud de Compra Rechazada";
            Thread hiloEmail = new Thread(new EmailSenderService(jobj, "COMPRAS", MyMessage, mailsToSend, asunto, usuario), "hilo");
            hiloEmail.start();
        }
        this.printlnResponseAjax( "{\"respta\":\""+respuesta+"\"}", "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
            json = "{\"respta\":\"ERROR:)\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }    
    
    public void EliminarOCS(String codOCS, String uSer) {
        String json = "";
        try {
            
            this.printlnResponseAjax(dao.EliminarOCS(codOCS, uSer), "application/json;");

        } catch (Exception ex) {
            ex.printStackTrace();
            json = "{\"respuesta\":\"ERROR:)\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }    
    
    private void InventarioTodos(String usuario, String ano, String mes, String tipomovimiento) {
        try {
            String json = dao.InventarioTodos(usuario, ano, mes, tipomovimiento);
            this.printlnResponseAjax(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void InventarioDetalle(String CodMovimiento) {
        String json = "";
        try {
            //JsonObject informacion = (JsonObject) (new JsonParser()).parse(request.getParameter("informacion"));
            this.printlnResponseAjax(dao.InventarioDetalle(CodMovimiento), "application/json;");

        } catch (Exception ex) {
            ex.printStackTrace();
            json = "{\"respuesta\":\"ERROR:)\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }    

    public void GuardarDespacho(String Usuario, String tipo_solicitud, String proveedor, String descripcion, String fecha_entrega, String direccion_entrega, String OrdenCompDespacho) {
        String json = "";
        try {
            
            this.printlnResponseAjax(dao.SaveDespacho(Usuario, tipo_solicitud, proveedor, descripcion, fecha_entrega, direccion_entrega, OrdenCompDespacho), "application/json;");

        } catch (Exception ex) {
            ex.printStackTrace();
            json = "{\"respuesta\":\"ERROR:)\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    public void GuardarDetalleDespacho(String dSPTCH) {
        String json = "";
        try {
            JsonObject informacion = (JsonObject) (new JsonParser()).parse(request.getParameter("InfoDespacho"));
            this.printlnResponseAjax(dao.SaveDetailsDispatch(informacion, dSPTCH), "application/json;");

        } catch (Exception ex) {
            ex.printStackTrace();
            json = "{\"respuesta\":\"ERROR:)\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }
    
    public void InventarioDespacho(String dSPTCH) {
        String json = "";
        try {
            
            this.printlnResponseAjax(dao.InventarioDespacho(dSPTCH), "application/json;");

        } catch (Exception ex) {
            ex.printStackTrace();
            json = "{\"respuesta\":\"ERROR:)\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }    
    
    private void DespachosListado(String ano, String mes) {
        try {
            String json = dao.DespachosTodos(ano, mes);
            this.printlnResponseAjax(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    
    public void DespachosDetalles(String CodDespacho) {
        String json = "";
        try {
            //JsonObject informacion = (JsonObject) (new JsonParser()).parse(request.getParameter("informacion"));
            this.printlnResponseAjax(dao.DespachosDetalles(CodDespacho), "application/json;");

        } catch (Exception ex) {
            ex.printStackTrace();
            json = "{\"respuesta\":\"ERROR:)\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }    

    private void VisualizarProyectos() {
        String json = "";
        try {
            this.printlnResponseAjax(dao.VisualizarProyectos(usuario.getLogin()), "application/json;");

        } catch (Exception ex) {
            ex.printStackTrace();
            json = "{\"respuesta\":\"ERROR:)\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    private void VisualizarItemsBodega(String id_bodega, String tipo_movimiento, String id_solicitud) {
        String json = "";
        try {
            this.printlnResponseAjax(dao.VisualizarItemsBodega(id_bodega, tipo_movimiento, usuario.getLogin(), id_solicitud), "application/json;");

        } catch (Exception ex) {
            ex.printStackTrace();
            json = "{\"respuesta\":\"ERROR:)\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    private void CargarListaInsumos() {
        String json = "";
        try {
            this.printlnResponseAjax(dao.CargarListaInsumos(), "application/json;");

        } catch (Exception ex) {
            ex.printStackTrace();
            json = "{\"respuesta\":\"ERROR:)\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }        
    }
    
    private void GuardarMovimiento(Usuario usuario, String fecha_transaccion, String responsable, String descripcion, String id_solicitud_proyecto, String id_bodega, String detalle, String tipo_movimiento, String id_solicitud_destino, String id_bodega_destino) {        
                
        String json = "";
        try {
            this.printlnResponseAjax(dao.GuardarMovimiento(usuario, fecha_transaccion, responsable, descripcion, id_solicitud_proyecto, id_bodega, detalle, tipo_movimiento, id_solicitud_destino, id_bodega_destino), "application/json;");

        } catch (Exception ex) {
            ex.printStackTrace();
            json = "{\"respuesta\":\"ERROR:)\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }        
    }    

    private void imprimirPDFMovimiento() {
        String json = "";
        try {
            String cod_movimiento = request.getParameter("cod_movimiento");
            json = "{\"respuesta\":\"" + dao.imprimirPDFMovimiento(cod_movimiento, usuario) + "\"}";            
            this.printlnResponseAjax(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        }        
    }

    private void CargarCombo() {
        try {
            String op = request.getParameter("op") == null ? "" : request.getParameter("op");
            String json = dao.cargar_Combo(usuario.getLogin(), op);
            this.printlnResponseAjax(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void CargarProyectosTraslado() {
        String json = "";
        try {
            this.printlnResponseAjax(dao.CargarProyectosTraslado(usuario.getLogin()), "application/json;");

        } catch (Exception ex) {
            ex.printStackTrace();
            json = "{\"respuesta\":\"ERROR:)\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    private void CargarHistoricoMaterialEnBodega(String id_bodega, String cod_material, String id_solicitud) {
        String json = "";
        try {
            this.printlnResponseAjax(dao.CargarHistoricoMaterialEnBodega(id_bodega, cod_material, id_solicitud), "application/json;");

        } catch (Exception ex) {
            ex.printStackTrace();
            json = "{\"respuesta\":\"ERROR:)\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }        
    }

    private void VerificarOCS(String ocs) {
        String json = "";
        try {            
            this.printlnResponseAjax(dao.VerificarOCS(ocs), "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
            json = "{\"respuesta\":\"ERROR:)\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }
    
    private void cargarSolicitudesEjecucion() {
        try {           
            String json = dao.cargarSolicitudesEjecucion(usuario.getLogin());
            this.printlnResponseAjax(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    
    
    private void cargarDetalleSolicitudEjecucion(String id_solicitud_ejecucion) {
        try {           
            String json = dao.cargarDetalleSolicitudEjecucion(id_solicitud_ejecucion);
            this.printlnResponseAjax(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }    

    private void guardarDetalleSolicitudEjecucion() {
        try {           
            String json = dao.guardarDetalleSolicitudEjecucion(request.getParameter("id_solicitud"),
                                                               request.getParameter("id_bodega_ejecucion"),
                                                               request.getParameter("id_bodega_proyecto"),
                                                               request.getParameter("fecha_esperada_entrega"),
                                                               request.getParameter("observaciones"),
                                                               usuario.getLogin(),
                                                               request.getParameter("detalle"));
            this.printlnResponseAjax(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }         
    }
    
    private void cargarSolicitudesEjecucionParaDespacho() {
        try {           
            String json = dao.cargarSolicitudesEjecucionParaDespacho(usuario.getLogin());
            this.printlnResponseAjax(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }    
    
    private void convertirSolicitudEnMovimiento() {
        try {           
            String json = dao.convertirSolicitudEnMovimiento(usuario.getLogin(), request.getParameter("solicitud_ejecucion"));
            this.printlnResponseAjax(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }        
    
    private void rechazarSolicitud() {
        try {           
            String json = dao.rechazarSolicitud(usuario.getLogin(), request.getParameter("solicitud_ejecucion"));
            this.printlnResponseAjax(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }       

    private void borrarSolicitud() {
        try {           
            String json = dao.borrarSolicitud(usuario.getLogin(), request.getParameter("solicitud_ejecucion"));
            this.printlnResponseAjax(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }      

    private void obternerCotizacionTerc() {
        try {           
            String json = dao.obternerCotizacionTerc(request.getParameter("id_solicitud"));
            this.printlnResponseAjax(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }      
    
}


