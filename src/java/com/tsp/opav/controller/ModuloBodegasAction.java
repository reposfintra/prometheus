/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.opav.controller;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.tsp.exceptions.InformationException;
import com.tsp.opav.model.DAOS.ModuloBodegasDAO;
import com.tsp.opav.model.DAOS.impl.ModuloBodegasImpl;

import com.tsp.operation.model.beans.Usuario;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Ing.William A. Siado
 */
public class ModuloBodegasAction extends Action {

    private final int CREAR_BODEGA = 1;
    private final int LISTAR_CONTRATISTAS = 2;
    private final int LISTAR_BODEGAS = 3;
    private final int CREAR_BODEGA_TERCERIZADA = 4;

    private JsonObject respuesta;
    private ModuloBodegasDAO dao;
    Usuario usuario = null;
    String reponseJson = "{}";
    String typeResponse = "application/json;";

    @Override
    public void run() throws ServletException, InformationException {
        try {
            HttpSession session = request.getSession();
            usuario = (Usuario) session.getAttribute("Usuario");
            dao = new ModuloBodegasImpl(usuario.getBd());
            int opcion = (request.getParameter("opcion") != null)
                    ? Integer.parseInt(request.getParameter("opcion"))
                    : -1;
            switch (opcion) {
                case CREAR_BODEGA:
                    this.crear_Bodega();
                    break;
                case LISTAR_CONTRATISTAS:
                    this.listar_Contratistas();
                    break;
                case LISTAR_BODEGAS:
                    this.listar_Bodegas();
                    break;
                case CREAR_BODEGA_TERCERIZADA:
                    this.crear_Bodega_Tercerizada();
                    break;
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                this.printlnResponseAjax(reponseJson, typeResponse);
            } catch (Exception ex1) {
                Logger.getLogger(MaestroProyectoAction.class.getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }

    private void crear_Bodega() {
        try {
            JsonObject informacion = (JsonObject) (new JsonParser()).parse(request.getParameter("informacion"));
            this.reponseJson = dao.crear_Bodega(informacion, usuario);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void listar_Contratistas() {
        try {
            this.reponseJson = dao.listar_Contratistas();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void listar_Bodegas() {
        try {
            String codigo_contratista = (request.getParameter("codigo_contratista") != null) ? request.getParameter("codigo_contratista") : "";
            this.reponseJson = dao.listar_Bodegas(codigo_contratista);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void crear_Bodega_Tercerizada() {
        try {
            JsonObject informacion = (JsonObject) (new JsonParser()).parse(request.getParameter("informacion"));
            this.reponseJson = dao.crear_Bodega_Tercerizada(informacion, usuario);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}