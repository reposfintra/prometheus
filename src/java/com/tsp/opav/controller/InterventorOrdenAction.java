/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.opav.controller;
import com.tsp.operation.model.beans.Usuario;
import javax.servlet.http.*;
import java.util.*;
import com.tsp.opav.model.services.*;
import com.tsp.opav.model.beans.*;

/**
 *
 * @author Ing. Jose Castro
 */
public class InterventorOrdenAction extends Action{

        private final int CARGAR_INTERVENTORES    = 0;
        private final int DEPARTAMENTOS_ASOCIADOS = 1;
        private final int GUARDAR_INTERVENTOR_DPTO = 2;
        private final int SOLICITUDES_ASOCIADAS = 3;
        private final int ASOCIAR_SOLICITUDES_INTERVENTOR=4;
        private final int ASIGNAR_INTERVENTOR_OFERTA=5;


    InterventorService interventorService;


    String next;
    String loginx;
    Usuario usuario;
    public InterventorOrdenAction(){
     this.next = "/jsp/delectricaribe/cotizacion/listadocotizacionesaux.jsp";
    }


        public void run() throws javax.servlet.ServletException {

                    try {
                        int op = (request.getParameter("opcion") != null) ? Integer.parseInt(request.getParameter("opcion").toString()) : -1;
                        HttpSession session = request.getSession();
                        com.tsp.util.Util.logController(((com.tsp.operation.model.beans.Usuario) session.getAttribute("Usuario")).getLogin(), this.getClass().getName());
                        usuario = (Usuario) session.getAttribute("Usuario");
                        interventorService =  new InterventorService(usuario.getBd());
                        loginx=usuario.getLogin();
                        switch (op) {
                            case CARGAR_INTERVENTORES:
                                    this.cargarInterventores();
                            break;
                            case DEPARTAMENTOS_ASOCIADOS:
                                    this.cargarDepartamentos();
                            break;
                            case GUARDAR_INTERVENTOR_DPTO:
                                    this.guardarDptos();
                            break;
                            case SOLICITUDES_ASOCIADAS:
                                    this.cargarSolicitudes();
                            break;
                            case ASOCIAR_SOLICITUDES_INTERVENTOR:
                                    this.guardarSolicitudesInteventor();
                            break;
                            case ASIGNAR_INTERVENTOR_OFERTA:
                                    this.asignarInterventorOferta();
                            break;

                        }
                    } catch (Exception ex) {
                        System.out.println("errorcito:"+ex.toString());
                        ex.printStackTrace();
                    }

        }



       private void cargarInterventores() throws Exception {
       String xml = "";
       //InterventorService iserv = new InterventorService(usuario.getBd());
       ArrayList listadoInterventores = interventorService.listadoInterventores();

        xml = "<?xml version=\"1.0\" encoding=\"iso-8859-1\"?>";
        xml += "<mensaje>";
        xml += this.getXMLCertificadores(listadoInterventores);
        xml += "</mensaje>";

        response.setContentType("text/xml");
        response.setHeader("Cache-Control", "no-cache");
        response.getWriter().println(xml);
    }


   private String getXMLCertificadores(ArrayList listadoInterventores)throws Exception{
        String xml = "";
             Interventor spl = null;
            //parte 1 ingresos
            for(int i=0;i<listadoInterventores.size();i++){
                spl = (Interventor) listadoInterventores.get(i);
                xml += "<interventor codente=\""+spl.getCodigo()+"\">"+spl.getNombre()+"</interventor>";
            }
        return xml;
    }

/**
 *
 * @throws Exception
 */
       private void cargarDepartamentos() throws Exception {
       String xml = "";
       String  cod_interventor = request.getParameter("cod_inter")!=null?request.getParameter("cod_inter"):"";
       String  opcion = request.getParameter("op")!=null?request.getParameter("op"):"";
       //InterventorService iserv = new InterventorService(usuario.getBd());
       ArrayList listadoDepartamentos = interventorService.listadoDepartamentos(cod_interventor, opcion);

        xml = "<?xml version=\"1.0\" encoding=\"iso-8859-1\"?>";
        xml += "<mensaje>";
        xml += this.getXMLDepartamentos(listadoDepartamentos);
        xml += "</mensaje>";

        response.setContentType("text/xml");
        response.setHeader("Cache-Control", "no-cache");
        response.getWriter().println(xml);
    }


    private String getXMLDepartamentos(ArrayList listadoDepartamentos) throws Exception {
        String xml = "";
        Interventor spl = null;
        //parte 1 ingresos
        if (listadoDepartamentos.size() > 0) {

            for (int i = 0; i < listadoDepartamentos.size(); i++) {
                spl = (Interventor) listadoDepartamentos.get(i);
                xml += "<departamento codente=\"" + spl.getCodigo() + "\">" + spl.getNombre() + "</departamento>";
            }
        } else {
            xml += "<departamento codente=\"0\">No tiene Departamentos</departamento>";
        }


        return xml;
    }


private void guardarDptos() throws Exception{

String dpto = "";
Interventor inter = null;
ArrayList departamentos = new ArrayList();
String cod_inter = request.getParameter("cod_inter")!=null?request.getParameter("cod_inter"):"";

int tam_items = (request.getParameter("tam_items") != null) ? Integer.parseInt(request.getParameter("tam_items").toString()) : -1;

for (int i = 0; i < tam_items; i++) {
      dpto = request.getParameter("iddpto" + i) != null ? request.getParameter("iddpto" + i) : "";
      departamentos.add(dpto);
 }
         interventorService.insertarRelacionInterventor(departamentos,  cod_inter, loginx);
         this.next = "/jsp/delectricaribe/interventor/asignacion_interventordpto.jsp?cod_inter="+cod_inter ;
         response.getWriter().println(next);

}



/**
 *
 * @throws Exception
 */
       private void cargarSolicitudes() throws Exception {
       String xml = "";
       String  cod_interventor = request.getParameter("cod_inter")!=null?request.getParameter("cod_inter"):"";
       String  opcion = request.getParameter("op")!=null?request.getParameter("op"):"";
      //InterventorService iserv = new InterventorService(usuario.getBd());
       ArrayList listadoSolicitudes = interventorService.listadoSolicitudes(cod_interventor, opcion);

        xml = "<?xml version=\"1.0\" encoding=\"iso-8859-1\"?>";
        xml += "<mensaje>";
        xml += this.getXMLSolicitudes(listadoSolicitudes);
        xml += "</mensaje>";

        response.setContentType("text/xml");
        response.setHeader("Cache-Control", "no-cache");
        response.getWriter().println(xml);
    }


    private String getXMLSolicitudes(ArrayList listadoSolicitudes) throws Exception {
        String xml = "";
        Interventor spl = null;
        //parte 1 ingresos
        if (listadoSolicitudes.size() > 0) {

            for (int i = 0; i < listadoSolicitudes.size(); i++) {
                spl = (Interventor) listadoSolicitudes.get(i);
                xml += "<sol_cliente codente=\"" + spl.getCodigo() + "\">" + spl.getCodigo()+"_"+spl.getNombre() + "</sol_cliente>";
                System.out.println("xml_____<sol_cliente codente=\"" + spl.getCodigo() + "\">" + spl.getCodigo()+"_"+spl.getNombre() + "</sol_cliente>");
            }
        } else {
            xml += "<sol_cliente codente=\"0\">No tiene Solicitudes</sol_cliente>";
        }

       System.out.println("xml_____"+xml);
        return xml;
    }



private void guardarSolicitudesInteventor() throws Exception{

String dpto = "";
Interventor inter = null;
ArrayList departamentos = new ArrayList();
String cod_inter = request.getParameter("cod_inter")!=null?request.getParameter("cod_inter"):"";

int tam_items = (request.getParameter("tam_items") != null) ? Integer.parseInt(request.getParameter("tam_items").toString()) : -1;

for (int i = 0; i < tam_items; i++) {
      dpto = request.getParameter("iddpto" + i) != null ? request.getParameter("iddpto" + i) : "";
      departamentos.add(dpto);
 }
         interventorService.insertarSolicitudInterventor(departamentos,  cod_inter, loginx);
         this.next = "/jsp/delectricaribe/interventor/asignacion_solicitud.jsp?cod_inter="+cod_inter ;
         //this.dispatchRequest(this.next);
         response.getWriter().println(next);

}



private void asignarInterventorOferta() throws Exception{

       String id_solicitud = request.getParameter("id") != null ? request.getParameter("id") : "";
       String tipo_accion = request.getParameter("tipo_accion") != null ? request.getParameter("tipo_accion") : "";
       String interventor = "";
       String ejecutar = request.getParameter("ejecutar") != null ? request.getParameter("ejecutar") : "";
        interventor = interventorService.obtenerInterventor(id_solicitud, ejecutar);
        if (!interventor.equals("")) {
            this.interventorService.updateOfertasInteventor(id_solicitud, interventor, loginx);
        }


    }








}