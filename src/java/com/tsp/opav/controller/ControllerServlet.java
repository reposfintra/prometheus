/**
*  Copyright (c) 2004
*  Transportes Sanchez Polo S.A.
*
*  Clase Controladora (CONTROLLER)
*
*/
package com.tsp.opav.controller;

import com.tsp.exceptions.InformationException;
import com.tsp.exceptions.SessionExpiredException;
import com.tsp.opav.model.ModelOpav;
import com.tsp.operation.model.Model;
import com.tsp.operation.model.beans.Usuario;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
* Componente controller para el programa operativo
* de Transportes Sanchez Polo S.A.
*/
public class ControllerServlet extends HttpServlet
{

        /**
     * Terminates the active session when this one expires.
     */
    private void logOut(HttpServletRequest request, HttpServletResponse response)
    throws ServletException {
        ServletContext context = getServletContext();
        RequestDispatcher rd = context.getRequestDispatcher("/logout.jsp");
        try {
            rd.forward(request, response);
        }catch (Exception E){
            E.printStackTrace();
        }
    }
   /**
   * Handles an HTTP GET request
   */
   public void doGet(
         HttpServletRequest request,
         HttpServletResponse response)
      throws ServletException, IOException
   {
      doPost(request, response);
   }

   /**
   * Handles an HTTP POST request
   */
   public void doPost(
         HttpServletRequest request,
         HttpServletResponse response)
      throws ServletException, IOException
   {
      HttpSession session = request.getSession();
      Map actionMap = (Map) session.getAttribute("actionMapopav");
      if (actionMap == null) {
         actionMap = new HashMap();
         session.setAttribute("actionMapopav", actionMap);
      }
      ServletContext context = getServletContext();
      try {

         // Get the state and event from the path info

//         String pathInfo = request.getPathInfo();
         String pathInfo = "/"+request.getParameter("estado").trim() +
                           "/"+request.getParameter("accion").trim();
         if (pathInfo == null)
            throw new ServletException
            ("Invalid internal state - no path info");

         // Load the action object that handles
         // this state and event

         Action action = (Action) actionMap.get(pathInfo);
         if (action == null) {

            // This is the first time the servlet has seen
            // this action.  Get the state and event name
            // from pathInfo.

            StringTokenizer st =
               new StringTokenizer(pathInfo, "/");

            if (st.countTokens() != 2)
               throw new ServletException
               ("Invalid internal state - invalid path info ["
               + pathInfo + "]");

            String state = st.nextToken();
            String event = st.nextToken();

            // Form the class name from the state and event

            String className =
               "com.tsp.opav.controller."
               + state + event + "Action";

            // Load the class and create an instance

            try {
               Class actionClass = Class.forName(className);
               action = (Action) actionClass.newInstance();
            }
            catch (ClassNotFoundException e) {
               throw new ServletException
               ("Could not load class " + className
               + ": " + e.getMessage());
            }
            catch (InstantiationException e) {
               throw new ServletException
               ("Could not create an instance of "
               + className + ": " + e.getMessage());
            }
            catch (IllegalAccessException e) {
               throw new ServletException
               (className + ": " + e.getMessage());
            }

            // Cache the instance in the action map

            actionMap.put(pathInfo, action);
         }

         // Ensure that a model exists in the session.
         ModelOpav model = (ModelOpav) session.getAttribute("modelopav");
         Usuario usuario = (Usuario) session.getAttribute("Usuario");
         if(model==null){
            if(model == null &&  usuario == null  )
                throw new SessionExpiredException();
                model = new ModelOpav(usuario.getBd());
                session.setAttribute("modelopav", model);
         }
            

            
         // INICIO IMPLEMENTACION DE CONTROLLER MANEJANDO MULTIPLES MODELOS   
         // Alvaro Pabon Martinez  2012-03-21 
         // Ensure that a model exists in the session.
         Model modelOperation = (Model) session.getAttribute("model");
         if(modelOperation == null &&  usuario == null  )
                throw new SessionExpiredException();
         // FINAL IMPLEMENTACION DE CONTROLLER MANEJANDO MULTIPLES MODELOS   
            
            
            

         // Now execute the action.  The action should perform
         // a RequestDispatcher.forward() when it completes

         action.setRequest(request);
         action.setResponse(response);
         action.setApplication(context);
         action.setModel(model);

         
         // INICIO IMPLEMENTACION DE CONTROLLER MANEJANDO MULTIPLES MODELOS   
         // Alvaro Pabon Martinez  2012-03-21          
         action.setModelOperation(modelOperation);
         // FINAL IMPLEMENTACION DE CONTROLLER MANEJANDO MULTIPLES MODELOS 
         
         
         action.run();
      } catch (InformationException e) {

         // Use the JSP error page for all servlet errors

         request.setAttribute("javax.servlet.jsp.jspException", e);
         RequestDispatcher rd = context.getRequestDispatcher("/error/ErrorPage.jsp");

         if (response.isCommitted())
            rd.include(request, response);
         else
            rd.forward(request, response);
        } catch (IllegalStateException IllegalStateE) {
            this.logOut(request, response);
        } catch (SessionExpiredException SessionExpiredE) {
            this.logOut(request, response);
      }
   }
}
