/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * EjecutivoGestionAction.java :
 * Copyright 2010 Rhonalf Martinez Villa <rhonaldomaster@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.tsp.opav.controller;
import java.util.*;
import com.tsp.opav.model.beans.*;
import com.tsp.opav.model.services.*;
import com.tsp.operation.model.beans.Usuario;
import javax.servlet.http.*;
/**
 *
 * @author rhonalf
 */
public class EjecutivoGestionAction extends Action {

    private EjecutivoService ejserv;

    public EjecutivoGestionAction(){

    }

    public void run(){
        Ejecutivo ejec = null;
        String next = "/jsp/opav/gestion_ejecutivos.jsp";
        try {
            String opcion = request.getParameter("opcion")!=null ? request.getParameter("opcion") : "search";
            String id = request.getParameter("idx")!=null ? request.getParameter("idx"):"0";
            HttpSession misesion = request.getSession();
            Usuario user = (Usuario)misesion.getAttribute("Usuario");
            ejserv = new EjecutivoService(user.getBd());
            String login = user.getLogin();
            ejec = ejserv.getDatosEjec(id);
            //next = next + "?id=" + id;
            if(opcion.equals("insert")){
                this.insertar(this.crearObjeto(), login);
            }
            else if(opcion.equals("delete")){
                this.borrar(id, login);
            }
            else if(opcion.equals("search")){
                String param = request.getParameter("param")!=null? request.getParameter("param") : "id_ejecutivo";
                String cadena = request.getParameter("cadena")!=null? request.getParameter("cadena") : "";
                ArrayList<Ejecutivo> listado = this.buscarEjecs(param, cadena);
                String cadenaTabla = "<table border='1' style='border-collapse:collapse;' width='100%'>";
                cadenaTabla = cadenaTabla + "<thead>" +
                        "<tr class='subtitulo1'>" +
                        "<th>Codigo</th>" +
                        "<th>Nit</th>" +
                        "<th>Nombre</th>" +
                        "<th>Direccion</th>" +
                        "<th>Telefono</th>" +
                        "<th>Ciudad</th>" +
                        "<th>Departamento</th>" +
                        "<th>Correo electronico</th>" +
                        "<th>Cargo</th>" +
                        "<th>Mercado</th>" +
                        "</tr></thead>";
                cadenaTabla = cadenaTabla + "<tbody>";
                Ejecutivo ejx = null;
                for (int i = 0; i < listado.size(); i++) {
                    ejx = listado.get(i);
                    cadenaTabla = cadenaTabla + "<tr class='fila'>" +
                            "<td>"+ejx.getId()+"</td>" +
                            "<td>"+ejx.getNit()+"</td>" +
                            "<td>"+ejx.getNombre()+"</td>" +
                            "<td>"+ejx.getDireccion()+"</td>" +
                            "<td>"+ejx.getTelefono()+"</td>" +
                            "<td>"+ejx.getCiudad()+"</td>" +
                            "<td>"+ejx.getDepartamento()+"</td>" +
                            "<td>"+ejx.getEmail()+"</td>" +
                            "<td>"+ejx.getCargo()+"</td>" +
                            "<td>"+ejx.getMercado()+"</td>" +
                            "</tr>";
                }
                cadenaTabla = cadenaTabla + "</tbody></table>";
                response.setContentType("text/plain; charset=utf-8");
                response.setHeader("Cache-Control", "no-cache");
                response.getWriter().println(cadenaTabla);
            }
            else if(opcion.equals("listar")){
                Ejecutivo ejx = null;
                String resp = "";
                ejx = this.buscarEjecutivo(id);
                resp =  ejx.getNit()+";_;" +
                            ejx.getNombre()+";_;" +
                            ejx.getDireccion()+";_;" +
                            ejx.getTelefono()+";_;" +
                            ejx.getCiudad()+";_;" +
                            ejx.getDepartamento()+";_;" +
                            ejx.getEmail()+";_;" +
                            ejx.getCargo()+";_;" +
                            ejx.getMercado();
                System.out.println("resp: "+resp);
                response.setContentType("text/plain; charset=utf-8");
                response.setHeader("Cache-Control", "no-cache");
                response.getWriter().println(resp);
            }
            else if(opcion.equals("modify")){
                Ejecutivo ejz = new Ejecutivo();
                ejz.setCargo(request.getParameter("cargo"));
                ejz.setCiudad(request.getParameter("ciudad"));
                ejz.setDepartamento(request.getParameter("departamento"));
                ejz.setDireccion(request.getParameter("direccion"));
                ejz.setEmail(request.getParameter("email"));
                ejz.setMercado(request.getParameter("mercado"));
                ejz.setNit(request.getParameter("nit"));
                ejz.setNombre(request.getParameter("nombre"));
                ejz.setTelefono(request.getParameter("telefono"));
                this.actualizar(ejz, login, id);
            }
            else{
                System.out.println("Aun no hay nada aca ... no deberia llegar hasta aca");
            }
            if(opcion.equals("listar") || opcion.equals("search")){
                System.out.println("enviando datos al jsp ...");
            }
            else{
                this.dispatchRequest(next);
            }
        }
        catch (Exception e) {
            System.out.println("Error en el run del action EjecutivoGestionAction: "+e.toString());
            e.printStackTrace();
        }
    }

    /**
     * Gestiona la insercion de ejecutivos
     * @param ejec Objeto Ejecutivo con los datos
     * @param user Usuario que hace la insercion
     * @throws Exception Cuando hay un error
     */
    private void insertar(Ejecutivo ejec,String user) throws Exception{
        try {
            ejserv.insertarEjecutivo(ejec, user);
        }
        catch (Exception e) {
            System.out.println("Error al insertar ejecutivo : "+e.toString()+" : "+e.getMessage());
            e.printStackTrace();
            throw new Exception("Error al insertar ejecutivo : "+e.toString()+" : "+e.getMessage());
        }
    }

    /**
     * Gestiona la anulacion de registros de la tabla ejecutivo
     * @param id Id del registro a anular
     * @param user Usuario que hace la anulacion
     * @throws Exception Cuando hay un error
     */
    private void borrar(String id,String user) throws Exception{
        try {
            ejserv.eliminarEjecutivo(id, user);
        }
        catch (Exception e) {
            System.out.println("Error al anular ejecutivo : "+e.toString()+" : "+e.getMessage());
            e.printStackTrace();
            throw new Exception("Error al anular ejecutivo : "+e.toString()+" : "+e.getMessage());
        }
    }

    /**
     * Gestiona la busqueda de ejecutivos
     * @param param Columna donde se debe buscar
     * @param cadena Dato a buscar
     * @return ArrayList de objetos Ejecutivo con las coincidencias encontradas
     * @throws Exception Cuando hay un error
     */
    private ArrayList<Ejecutivo> buscarEjecs(String param,String cadena) throws Exception{
        ArrayList<Ejecutivo> lista = null;
        try {
            lista = ejserv.buscarEjecutivos(param, cadena);
        }
        catch (Exception e) {
            System.out.println("Error al buscar datos de ejecutivo : "+e.toString()+" : "+e.getMessage());
            e.printStackTrace();
            throw new Exception("Error al buscar datos de ejecutivo : "+e.toString()+" : "+e.getMessage());
        }
        return lista;
    }

    /**
     * Busca un ejecutivo dado el id
     * @param id Codigo del ejecutivo
     * @return Objeto Ejecutivo con los datos encontrados
     * @throws Exception Cuando hay un error
     */
    private Ejecutivo buscarEjecutivo(String id) throws Exception{
        Ejecutivo ejx = null;
        try {
            ejx = ejserv.getDatosEjec(id);
        }
        catch (Exception e) {
            System.out.println("Error al buscar datos de ejecutivo por id : "+e.toString()+" : "+e.getMessage());
            e.printStackTrace();
            throw new Exception("Error al buscar datos de ejecutivo por id : "+e.toString()+" : "+e.getMessage());
        }
        return ejx;
    }

    /**
     * Crea un objeto Ejecutivo a partir de los parametros
     * @return Objeto creado de los parametros
     */
    private Ejecutivo crearObjeto(){
        Ejecutivo ejecx = new Ejecutivo();
        ejecx.setCargo(request.getParameter("cargo"));
        ejecx.setCiudad(request.getParameter("ciudad"));
        ejecx.setDepartamento(request.getParameter("departamento"));
        ejecx.setDireccion(request.getParameter("direccion"));
        ejecx.setEmail(request.getParameter("email"));
        ejecx.setMercado(request.getParameter("mercado"));
        ejecx.setNit(request.getParameter("nit"));
        ejecx.setNombre(request.getParameter("nombre"));
        ejecx.setTelefono(request.getParameter("telefono"));
        return ejecx;
    }

    /**
     * Gestiona la actualizacion de datos
     * @param ej Objeto con los datos
     * @param user Usuario que actualiza
     * @param id Codigo del registro a actualizar
     * @throws Exception Cuando hay un error
     */
    private void actualizar(Ejecutivo ej,String user,String id) throws Exception{
        try {
            ejserv.actualizarEjecutivo(ej, user, id);
        }
        catch (Exception e) {
            System.out.println("Error al actualizar datos de ejecutivo: "+e.toString()+" : "+e.getMessage());
            e.printStackTrace();
            throw new Exception("Error al actialuzar datos de ejecutivo: "+e.toString()+" : "+e.getMessage());
        }
    }

}