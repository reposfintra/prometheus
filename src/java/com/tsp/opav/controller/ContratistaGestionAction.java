/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * ContratistaGestionAction.java :
 * Copyright 2010 Rhonalf Martinez Villa <rhonaldomaster@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.tsp.opav.controller;
import java.util.*;
import com.tsp.opav.model.beans.*;
import com.tsp.opav.model.services.*;
import com.tsp.operation.model.beans.Usuario;
import javax.servlet.http.*;
/**
 *
 * @author rhonalf
 */
public class ContratistaGestionAction extends Action {

    private ContratistaService csr;
    private Usuario usuario;
    /**
     * Constructor de la clase
     */
    public ContratistaGestionAction(){

    }

    @Override
    public void run(){
        String next = "/jsp/opav/gestion_contratistas.jsp";
        usuario = (Usuario)request.getSession().getAttribute("Usuario");
        Contratista contratista = null;
        String codigo="";
        boolean redirect = false;
        try {
            String opcion = request.getParameter("opcion")!=null ? request.getParameter("opcion") : "search";
            csr = new ContratistaService(usuario.getBd());
            if(opcion.equals("insert")){
                contratista = this.crearObjeto();
                try {
                    csr.insertar(contratista);
                }
                catch (Exception e) {
                    System.out.println("Error al insertar el contratista: "+e.toString());
                    e.printStackTrace();
                }
                redirect = true;
            }
            else if(opcion.equals("delete")){
                codigo = request.getParameter("codigo")!=null?request.getParameter("codigo"):"";
                try {
                    csr.anular(codigo);
                }
                catch (Exception e) {
                    System.out.println("Error al eliminar el contratista: "+e.toString());
                    e.printStackTrace();
                }
                redirect = true;
            }
            else if(opcion.equals("search")){
                String tabla="";
                tabla = "<table border='1' style='border-collapse:collapse;border-color: black;' width='100%'>";
                tabla = tabla + "<thead><tr class='subtitulo1'>" +
                                        "<th>Codigo del contratista</th>" +
                                        "<th>Nit</th>" +
                                        "<th>Nombre</th>" +
                                        "<th>E-mail</th>" +
                                        "<thead>";
                tabla = tabla + "<tbody>";
                String filtro = request.getParameter("param")!=null?request.getParameter("param"):"descripcion";
                String param = request.getParameter("cadena")!=null?request.getParameter("cadena"):"";
                ArrayList lista = null;
                try {
                    lista = csr.buscar(filtro, param);
                }
                catch (Exception e) {
                    lista = null;
                    System.out.println("Error al buscar datos de contratistas: "+e.toString());
                    e.printStackTrace();
                }
                if(lista!=null && lista.size()>0){
                    Contratista cont = null;
                    for (int i = 0; i < lista.size(); i++) {
                        cont = (Contratista)lista.get(i);
                        tabla = tabla + "<tr class='fila'>";
                        tabla = tabla + "<td>"+cont.getId_contratista()+"</td>" +
                                    "<td>"+cont.getNit()+"</td>" +
                                    "<td>"+cont.getNombre_contratista()+"</td>" +
                                    "<td>"+cont.getEmail()+"</td>";
                        tabla = tabla + "</tr>";
                    }
                }
                else{
                    tabla = tabla + "<tr class='fila'><td colspan='4' align='center'>No se encontraron resultados</td></tr>";
                }
                tabla = tabla + "</tbody></table>";
                this.escribirResponse(tabla);
            }
            else if(opcion.equals("modify")){
                contratista = this.crearObjeto();
                try {
                    csr.actualizar(contratista);
                }
                catch (Exception e) {
                    System.out.println("Error al modificar el contratista: "+e.toString());
                    e.printStackTrace();
                }
                redirect = true;
            }
            else if(opcion.equals("listar")){
                codigo = request.getParameter("codigo")!=null?request.getParameter("codigo"):"CC000";
                String datos = "";
                try {
                    contratista = (Contratista)csr.buscar("id_contratista", codigo).get(0);
                }
                catch (Exception e) {
                    contratista=null;
                    System.out.println("Error al buscar los datos del codigo "+codigo+": "+e.toString());
                }
                if(contratista!=null){
                    datos = ""+contratista.getActividad()+";_;"+contratista.getAutoretenedor()+";_;"+contratista.getClave();
                    datos = datos + ";_;"+contratista.getCodigo_reteica()+";_;"+contratista.getDomicilio_comercial()+";_;"+contratista.getEmail();
                    datos = datos + ";_;"+contratista.getGran_contribuyente()+";_;"+contratista.getNit()+";_;"+contratista.getNombre_contratista();
                    datos = datos + ";_;"+contratista.getRegimen()+";_;"+contratista.getSecuencia_prefactura()+";_;";
                    datos += contratista.getResponsable()+";_;"+contratista.getDireccion()+";_;"+contratista.getTelefono()+";_;"+contratista.getCelular()+";_;";
                }
                this.escribirResponse(datos);
            }
            else{
                redirect = true;
            }
        }
        catch (Exception e) {
            System.out.println("Error en ContratistaGestionAction.java: "+e.toString());
            e.printStackTrace();
        }
        finally{
            try {
                if(redirect==true){
                    this.dispatchRequest(next);
                }
            }
            catch (Exception e) {
                System.out.println("Error en ContratistaGestionAction.java al hacer forward: "+e.toString());
                e.printStackTrace();
            }
        }
    }

    /**
     * Le entrega los datos a la peticion ajax
     * @param resp lo que se quiere entregar
     * @throws Exception cuando ocurre un error
     */
    private void escribirResponse(String resp) throws Exception{
        try {
            response.setContentType("text/plain; charset=utf-8");
            response.setHeader("Cache-Control", "no-cache");
            response.getWriter().println(resp);
        } catch (Exception e) {
            System.out.println("Error al escribir la respuesta: "+e.toString());
            throw new Exception("Error al escribir la respuesta: "+e.toString());
        }
    }

    /**
     * Crea un objeto contratista basado en los parametros del request
     * @return objeto contratista con datos
     */
    private Contratista crearObjeto(){
        Contratista cont = null;
        try {
            cont = new Contratista();
            cont.setActividad(request.getParameter("actividad")!=null?request.getParameter("actividad"):"");
            cont.setAutoretenedor(request.getParameter("autoretenedor")!=null?request.getParameter("autoretenedor"):"");
            cont.setClave(request.getParameter("clave")!=null?request.getParameter("clave"):"");
            cont.setCodigo_reteica(request.getParameter("reteica")!=null?request.getParameter("reteica"):"");
            cont.setDomicilio_comercial(request.getParameter("domicilio")!=null?request.getParameter("domicilio"):"");
            cont.setEmail(request.getParameter("email")!=null?request.getParameter("email"):"");
            cont.setGran_contribuyente(request.getParameter("contribuyente")!=null?request.getParameter("contribuyente"):"");
            cont.setId_contratista(request.getParameter("codigo")!=null?request.getParameter("codigo"):"");
            cont.setNit(request.getParameter("nit")!=null?request.getParameter("nit"):"");
            cont.setNombre_contratista(request.getParameter("nombre")!=null?request.getParameter("nombre"):"");
            cont.setRegimen(request.getParameter("regimen")!=null?request.getParameter("regimen"):"");
            cont.setSecuencia_prefactura(request.getParameter("secuencia")!=null?Integer.parseInt(request.getParameter("secuencia")):0);
            cont.setResponsable(request.getParameter("responsable")!=null?request.getParameter("responsable"):"");
            cont.setDireccion(request.getParameter("direccion")!=null?request.getParameter("direccion"):"");
            cont.setTelefono(request.getParameter("telefono")!=null?request.getParameter("telefono"):"");
            cont.setCelular(request.getParameter("celular")!=null?request.getParameter("celular"):"");
        }
        catch (Exception e) {
            System.out.println("Error al crear objeto en contratistagestionaction: "+e.toString());
        }
        return cont;
    }

}