/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.opav.controller;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.tsp.exceptions.InformationException;
import com.tsp.opav.model.DAOS.MinutasContratacionDAO;
import com.tsp.opav.model.DAOS.impl.MinutasContratacionImpl;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.util.Util;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

/**
 *
 * @author user
 */
public class MinutasContratacionAction extends Action{

    private final int CARGAR_SOLICITUDES_PEND_POR_CONTRATACION = 0;
    private final int OBTENER_INFO_SOLICITUD = 1;
    private final int CARGAR_ARCHIVO = 2;
    private final int LISTAR_DOCUMENTOS_CARGADOS = 3;
    private final int CONSULTAR_ARCHIVO = 4;
    private final int ELIMINAR_ARCHIVO = 5;
    private final int GUARDAR_CONTRATO = 6;
    private final int CARGAR_FACTURAS_PARCIALES = 7;
    private final int CARGAR_GARANTIAS = 8;
    private final int LISTAR_DIAS_PAGO = 9;
    private final int LISTAR_POLIZAS = 10;
    private final int CARGAR_CONFIG_DOCS_MINUTA = 11;
    private final int GUARDAR_CONFIG_DOCS_MINUTA = 12;
    private final int CARGAR_GRILLA_EQUIVALENCIAS = 13;
    private final int LISTAR_CLASIFICACION_PERSONA = 14;
    private final int GENERAR_CARTA_ACEPTACION_CLIENTE = 15;
    private final int GENERAR_CONTRATO = 16;
    private final int LISTAR_TIPO_DOCUMENTOS = 17;
    private final int GUARDAR_TIPO_DOCUMENTO = 18;
    private final int ACTUALIZAR_TIPO_DOCUMENTO = 19;
    private final int ACTIVAR_INACTIVAR_TIPO_DOCUMENTO = 20;
    private final int CREAR_DOCUMENTOS_CONTRATO = 21;
    private final int EXISTE_DOCUMENTO_REL_CONTRATO = 22;
    private final int CARGAR_DOCUMENTOS_CONTRATO = 23;
    private final int ACTUALIZAR_INFO_CONTRATO_DOC = 24;
    private final int CARGAR_GRILLA_ASIGNACION_BROKER = 25;
    private final int LISTAR_BROKER = 26;
    private final int ASOCIAR_BROKER_CONTRATO = 27;
    private final int CARGAR_GRILLA_SOLICITUDES_BROKER = 28;
    private final int LISTAR_ASEGURADORAS = 29;
    private final int CARGAR_COTIZACION_ASEGURADORA = 30;
    private final int GUARDAR_COTIZACION_ASEGURADORA = 31;
    private final int LISTAR_TOTALES_COT_ASEGURADORA = 32;
    private final int LISTAR_DETALLE_COT_ASEGURADORA = 33;
    private final int ENVIAR_COTIZACION_BROKER = 34;
    private final int ACEPTAR_COTIZACION_BROKER = 35;
    private final int ANULAR_GARANTIA_CONTRATO = 36;
    private final int OBTENER_OTROS_COSTOS_ASEGURADORA = 37;
    private final int GENERAR_CXP_ASEGURADORA = 38;
    private final int OBTENER_INFO_ASEGURADORA_ACEPTADA = 39;
    private final int LISTAR_COTIZ_PENDIENTES_GENERAR_CXP = 40;
    private final int LISTAR_BENEFICIARIOS_POLIZA = 41;
    private final int LISTAR_CAUSALES_POLIZA = 42;
    private final int LISTAR_BENEFICIARIOS_ACEPTADOS_CXP = 43;
    private final int LISTAR_BENEFICIARIOS_CONTRATO = 44;
    private final int LISTAR_TIPOS_BENEFICIARIO_ASIGNADOS_BROKER = 45;
    private final int COTIZACION_PENDIENTE_BROKER = 46;
    private final int CARGAR_GRILLA_OTROS_SI = 47;
    private final int INSERTAR_OTRO_SI = 48;
    private final int GUARDAR_INFO_OTRO_SI = 49;
    private final int ANULAR_OTRO_SI = 50;
    private final int LISTAR_OTROS_SI_CONTRATO = 51;
    private final int LISTAR_OTROS_SI_PENDIENTES_CXP = 52;
       
    MinutasContratacionDAO dao;
    Usuario usuario = null;
    String reponseJson = "{}";
    String typeResponse = "application/json;";
    
    @Override
    public void run() throws ServletException, InformationException {
          try {

            HttpSession session = request.getSession();
            usuario = (Usuario) session.getAttribute("Usuario");
            int opcion = Integer.parseInt(request.getParameter("opcion"));         
            dao = new MinutasContratacionImpl(usuario.getBd());

            switch (opcion) {
                case CARGAR_SOLICITUDES_PEND_POR_CONTRATACION:
                    this.cargarGridSolicitudesPendientes();
                    break;   
                case OBTENER_INFO_SOLICITUD:
                    this.cargarInfoSolicitud();
                    break;
                case CARGAR_ARCHIVO:
                    this.cargarArchivo();
                    break;  
                case LISTAR_DOCUMENTOS_CARGADOS:
                    this.getNombresArchivos();
                    break;
                case CONSULTAR_ARCHIVO:
                    this.almacenarArchivoEnCarpetaUsuario();
                    break;
                case ELIMINAR_ARCHIVO:
                    this.eliminarArchivo();
                    break;
                case GUARDAR_CONTRATO:
                    this.guardarContrato();
                    break;  
                case CARGAR_FACTURAS_PARCIALES:
                    this.listarFacturasParciales();
                    break;   
                case CARGAR_GARANTIAS:
                    this.listarGarantias();
                    break;   
                case LISTAR_DIAS_PAGO:
                    this.listarDiasPago();
                    break;
                case LISTAR_POLIZAS:
                    this.listarPolizas();
                    break;
                case CARGAR_CONFIG_DOCS_MINUTA:
                    cargarConfigDocsMinuta();
                    break;
                case GUARDAR_CONFIG_DOCS_MINUTA:
                    guardarConfigDocsMinuta();
                    break;
                case CARGAR_GRILLA_EQUIVALENCIAS:
                    this.cargarEquivalencias();
                    break;
                case LISTAR_CLASIFICACION_PERSONA:
                    this.listarClasificacionPersona();
                    break;
                case GENERAR_CARTA_ACEPTACION_CLIENTE:
                    generarCartaAceptacion();
                    break;
                case GENERAR_CONTRATO:
                    generarContrato();
                    break;
                case LISTAR_TIPO_DOCUMENTOS:
                    listarTipoDocumentos();
                    break;
                case GUARDAR_TIPO_DOCUMENTO:
                    insertarTipoDocumento();
                    break;
                case ACTUALIZAR_TIPO_DOCUMENTO:
                    actualizarTipoDocumento();
                    break;
                case ACTIVAR_INACTIVAR_TIPO_DOCUMENTO:
                    activaInactivaTipoDocumento();
                    break;
                case CREAR_DOCUMENTOS_CONTRATO:
                    crearDocsContrato();
                    break;
                case EXISTE_DOCUMENTO_REL_CONTRATO:
                    existenDocumentoRelContrato();
                    break;
                case CARGAR_DOCUMENTOS_CONTRATO:
                    cargarInfoContratoDocs();
                    break;
                case ACTUALIZAR_INFO_CONTRATO_DOC:
                    guardarContratoDocs();
                    break;
                case CARGAR_GRILLA_ASIGNACION_BROKER:
                    this.cargarGridAsignacionBroker();
                    break; 
                case LISTAR_BROKER:
                    this.listarBroker();
                    break; 
                case ASOCIAR_BROKER_CONTRATO:
                    this.asignarBrokerContrato();
                    break; 
                case CARGAR_GRILLA_SOLICITUDES_BROKER:
                    this.cargarGridSolicitudesBroker();
                    break; 
                case LISTAR_ASEGURADORAS:
                    this.listarAseguradoras();
                    break; 
                case CARGAR_COTIZACION_ASEGURADORA:
                    this.listarCotizacionAseguradora();
                    break;   
                case GUARDAR_COTIZACION_ASEGURADORA:
                    guardarCotizacionAseguradora();
                    break;
                case LISTAR_TOTALES_COT_ASEGURADORA:
                    this.listarTotalesCotAseguradora();
                    break; 
                case LISTAR_DETALLE_COT_ASEGURADORA:
                    this.listarDetalleCotAseguradora();
                    break; 
                case ENVIAR_COTIZACION_BROKER:
                    enviarCotizacionBroker();
                    break;
                case ACEPTAR_COTIZACION_BROKER:
                    aceptarCotizacionBroker();
                    break;
                case ANULAR_GARANTIA_CONTRATO:
                    anularGarantiaContrato();
                    break;
                case OBTENER_OTROS_COSTOS_ASEGURADORA:
                    this.cargarOtrosCostosAseguradora();
                    break;
                case GENERAR_CXP_ASEGURADORA:
                    generarCXPAseguradora();
                    break;
                case OBTENER_INFO_ASEGURADORA_ACEPTADA:
                    this.cargarInfoAseguradoraAceptada();
                    break;
                case LISTAR_COTIZ_PENDIENTES_GENERAR_CXP:
                    this.listarCotizPendientesCxPAseguradora();
                    break;
                case LISTAR_BENEFICIARIOS_POLIZA:
                    this.listarBeneficiariosPoliza();
                    break;
                case LISTAR_CAUSALES_POLIZA:
                    this.listarCausalesPoliza();
                    break;
                case LISTAR_BENEFICIARIOS_ACEPTADOS_CXP:
                    this.cargarCboBeneficiarioAceptadoCxP();
                    break;
                case LISTAR_BENEFICIARIOS_CONTRATO:
                    this.cargarCboBeneficiariosContrato();
                    break;
                case LISTAR_TIPOS_BENEFICIARIO_ASIGNADOS_BROKER:
                    this.cargarCboBeneficiariosAsignadosBroker();
                    break;
                case COTIZACION_PENDIENTE_BROKER:
                     this.cotizacionPendienteBroker();
                     break;
                case CARGAR_GRILLA_OTROS_SI:
                    this.listarOtrosSi();
                    break;   
                case INSERTAR_OTRO_SI:
                    this.insertarOtroSi();
                    break;   
                case GUARDAR_INFO_OTRO_SI:
                    this.guardarInfoOtroSi();
                    break;   
                case ANULAR_OTRO_SI:
                    this.anularOtroSi();
                    break;   
                case LISTAR_OTROS_SI_CONTRATO:
                    this.cargarCboOtrosSiContrato();
                    break;   
                case LISTAR_OTROS_SI_PENDIENTES_CXP:
                    this.cargarCboOtrosSiAceptadoCxP();
                    break;
                    default:             
                    this.printlnResponseAjax("{\"error\":\"Peticion invalida 404 recurso no encontrado\"}", "application/json;");
                    break;

            }

        } catch (Exception ex) {
            reponseJson = "{\"error\":\"Algo salio mal al procesar su solicitud\",\"exception\":\"" + ex.getMessage() + "\"}";
            ex.printStackTrace();
        } finally {
            try {
                this.printlnResponseAjax(reponseJson, typeResponse);
            } catch (Exception ex1) {
                Logger.getLogger(MinutasContratacionAction.class.getName()).log(Level.SEVERE, null, ex1);
            }
        }

    }
    
    public void cargarGridSolicitudesPendientes() {
        try {
            String fechainiApprov = request.getParameter("fecha_inicial") != null ? request.getParameter("fecha_inicial") : "";
            String fechafinApprov = request.getParameter("fecha_final") != null ? request.getParameter("fecha_final") : "";         
            String num_solicitud = request.getParameter("num_solicitud") != null ? request.getParameter("num_solicitud") : "";
            String lineaNegocio = request.getParameter("lineaNegocio") == null ? "" : request.getParameter("lineaNegocio");
            String responsable = request.getParameter("responsable") == null ? "" : Util.setCodificacionCadena(request.getParameter("responsable"),"ISO-8859-1");
            String etapaActual = request.getParameter("etapaActual") == null ? "" : Util.setCodificacionCadena(request.getParameter("etapaActual"),"ISO-8859-1");
            String id_cliente = request.getParameter("id_cliente") == null ? "" : Util.setCodificacionCadena(request.getParameter("id_cliente"),"ISO-8859-1");
            String nom_proyecto = request.getParameter("nom_proyecto") == null ? "" : Util.setCodificacionCadena(request.getParameter("nom_proyecto"),"ISO-8859-1");
            String foms = request.getParameter("foms") == null ? "" : Util.setCodificacionCadena(request.getParameter("foms"),"ISO-8859-1");
            String tipo_proyecto = request.getParameter("tipo_proyecto") == null ? "" : Util.setCodificacionCadena(request.getParameter("tipo_proyecto"),"ISO-8859-1");
            this.reponseJson = dao.cargarGridSolicitudesPendientes(fechainiApprov, fechafinApprov, num_solicitud, lineaNegocio, responsable, etapaActual, id_cliente, nom_proyecto,foms, tipo_proyecto );
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(MinutasContratacionAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }
    
    public void cargarArchivo() {
        String num_solicitud="";
        try {
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");//se consiguen los datos de db.properties

            String directorioArchivos = rb.getString("rutaImagenes") + "gestionadministrativa/minutas/";//se establece la ruta de la imagen
            
            //this.createDir(directorioArchivos );
            if (ServletFileUpload.isMultipartContent(request)) {

                ServletFileUpload servletFileUpload = new ServletFileUpload(new DiskFileItemFactory());
                List fileItemsList = servletFileUpload.parseRequest(request);

                //// Itero para obtener todos los FileItem
                Iterator it = fileItemsList.iterator();
                String nombre_archivo;
                String fieldtem = "", valortem = "", idxx = "";
                        
                while (it.hasNext()) {
                    FileItem fileItem = (FileItem) it.next();
                    
                    if ( (fileItem.isFormField()) ) {
                        
                        fieldtem = fileItem.getFieldName();                   
                        valortem = fileItem.getString();
                        
                        if (fieldtem.equals("num_solicitud")){
                            num_solicitud = valortem;
                        }                
                       
                    }
                    
                    if (!(fileItem.isFormField())) {

                        //// Nombre del archivo en el cliente. Algunos navegadores (por ej. IE 6)
                        //// incluyen el path completo, lo que puede implicar separar path
                        //// de nombre.
                        if (fileItem.getName() != "" && !fileItem.getName().isEmpty() && fileItem.getSize() > 0) {

                            String nombreArchivo = fileItem.getName();

                            String[] nombrearreglo = nombreArchivo.split("\\\\");
                            String nombreArchivoremix = nombrearreglo[(nombrearreglo.length - 1)];
                            String rutaArchivo = directorioArchivos + num_solicitud;
                            nombre_archivo = nombreArchivoremix;

                            this.createDir(rutaArchivo);

                            File archivo = new File(rutaArchivo + "/" + nombre_archivo);
                            fileItem.write(archivo);
                        }

                    }
                    
                    
                    
                }
            }          
            this.reponseJson =  "{\"respuesta\":\"OK\"}";          
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(MinutasContratacionAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }  
    }

    
    public void guardarContrato() {
       
        try {
            String num_solicitud = request.getParameter("num_solicitud") != null ? request.getParameter("num_solicitud") : "";
            String num_contrato = request.getParameter("num_contrato") != null ? request.getParameter("num_contrato") : "";
            String tipo_contrato = request.getParameter("tipo_contrato") != null ? request.getParameter("tipo_contrato") : "";
            
            String vlr_antes_iva = (request.getParameter("vlr_antes_iva") != null) ? request.getParameter("vlr_antes_iva") : "0";
            String porc_admon = (request.getParameter("porc_admon") != null) ? request.getParameter("porc_admon") : "0";
            String valor_admon = (request.getParameter("valor_admon") != null) ? request.getParameter("valor_admon") : "0";
            String porc_imprevisto = (request.getParameter("porc_imprevisto") != null) ? request.getParameter("porc_imprevisto") : "0";
            String valor_imprevisto = (request.getParameter("valor_imprevisto") != null) ? request.getParameter("valor_imprevisto") : "0";
            String porc_utilidad = (request.getParameter("porc_utilidad") != null) ? request.getParameter("porc_utilidad") : "0";
            String valor_utilidad = (request.getParameter("valor_utilidad") != null) ? request.getParameter("valor_utilidad") : "0";
            String porc_aiu = (request.getParameter("porc_aiu") != null) ? request.getParameter("porc_aiu") : "0";
            String valor_aiu = (request.getParameter("valor_aiu") != null) ? request.getParameter("valor_aiu") : "0";
            String porc_iva = (request.getParameter("porc_iva") != null) ? request.getParameter("porc_iva") : "0";         
            String valor_iva = (request.getParameter("valor_iva") != null) ? request.getParameter("valor_iva") : "0";
         
            String valor_total = (request.getParameter("valor_total") != null) ? request.getParameter("valor_total") : "0";
            String porc_anticipo = (request.getParameter("porc_anticipo") != null) ? request.getParameter("porc_anticipo") : ""; 
            String nit_empresa = request.getParameter("nit_empresa") != null ? request.getParameter("nit_empresa") : "";
            String cedula_rep_empresa = request.getParameter("cedula_rep_empresa") != null ? request.getParameter("cedula_rep_empresa") : "";
                       
            JsonObject garantias = (JsonObject) (new JsonParser()).parse(request.getParameter("listadogarantias"));
            //JsonObject garantias_otro_si = (JsonObject) (new JsonParser()).parse(request.getParameter("listadogarantias_otro_si"));
            JsonObject garantias_extra = (JsonObject) (new JsonParser()).parse(request.getParameter("listadogarantias_extra"));
            JsonObject infoProveedor = (JsonObject) (new JsonParser()).parse(request.getParameter("infoProveedor"));
            JsonObject infoFormaPago = (JsonObject) (new JsonParser()).parse(request.getParameter("infoFormasPago"));
                 
            this.reponseJson =  dao.guardarContrato(num_solicitud, tipo_contrato, num_contrato, vlr_antes_iva, porc_admon, valor_admon, porc_imprevisto, valor_imprevisto, porc_utilidad, valor_utilidad, porc_aiu, valor_aiu,  porc_iva, valor_iva, valor_total, porc_anticipo, nit_empresa, cedula_rep_empresa, usuario, garantias, garantias_extra, infoProveedor, infoFormaPago);          
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(MinutasContratacionAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }  
    }
    
    public void createDir(String dir) throws Exception {
        try {
            File f = new File(dir);
            if (!f.exists()) {
                f.mkdir();
            }
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }
    
    private void cargarInfoSolicitud() {
        try {
            String id_solicitud = (request.getParameter("id_solicitud") != null) ? request.getParameter("id_solicitud") : "";
            this.reponseJson = dao.getInfoSolicitud(id_solicitud);
        } catch (Exception ex) {
            Logger.getLogger(MinutasContratacionAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }
    
    private void listarDiasPago() {
        try {           
            this.reponseJson = dao.listarDiasPago();
        } catch (Exception ex) {
            Logger.getLogger(MinutasContratacionAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }
    
    private void listarPolizas() {
        try {           
            this.reponseJson = dao.listarPolizas();
        } catch (Exception ex) {
            Logger.getLogger(MinutasContratacionAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }
        
    public void getNombresArchivos() {
        java.util.List lista = null;
         Gson gson = new Gson();
        try {
            String id_solicitud = (request.getParameter("id_solicitud") != null) ? request.getParameter("id_solicitud") : "";
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String rutaOrigen = rb.getString("rutaImagenes") + "gestionadministrativa/minutas/";
            this.reponseJson = gson.toJson(dao.searchNombresArchivos(rutaOrigen, id_solicitud));
        } catch (Exception e) {
            System.out.println("errorrr::" + e.toString() + "__" + e.getMessage());
        }       
    }
    
    public void almacenarArchivoEnCarpetaUsuario() throws Exception {
        try {
            String documento = (request.getParameter("num_solicitud") != null) ? request.getParameter("num_solicitud") : "";
            String nomarchivo = (request.getParameter("nomarchivo") != null) ? Util.setCodificacionCadena(request.getParameter("nomarchivo"),"ISO-8859-1") : "";
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");            
            String rutaOrigen = rb.getString("rutaImagenes") + "gestionadministrativa/minutas/";
            String rutaDestino = rb.getString("ruta") + "/images/multiservicios/" + usuario.getLogin() + "/";
            if (dao.almacenarArchivoEnCarpetaUsuario(documento, ("" + rutaOrigen+documento), ("" + rutaDestino), nomarchivo)) {
                this.reponseJson = "{\"respuesta\":\"SI\",\"login\":\"" + usuario.getLogin() + "\"}";
            } else {
                this.reponseJson = "{\"respuesta\":\"NO\"}";
            }           
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }
    
    public void eliminarArchivo() throws Exception {
        try {
            String documento = (request.getParameter("num_solicitud") != null) ? request.getParameter("num_solicitud") : "";
            String nomarchivo = (request.getParameter("nomarchivo") != null) ? Util.setCodificacionCadena(request.getParameter("nomarchivo"),"ISO-8859-1") : "";
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String rutaOrigen = rb.getString("rutaImagenes") + "gestionadministrativa/minutas/"; 
            if (dao.eliminarArchivo(("" + rutaOrigen), documento,  nomarchivo)) {
                this.reponseJson = "{\"respuesta\":\"OK\"}";               
            } else {
                this.reponseJson = "{\"respuesta\":\"ERROR\"}";               
            }        
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }
    
    private void listarFacturasParciales() {
                 
        try{
            String num_solicitud = (request.getParameter("num_solicitud") != null) ? request.getParameter("num_solicitud") : "";
            this.reponseJson = dao.cargarFacturasParciales(num_solicitud);   
           
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
    private void listarGarantias() {

        try {
            String num_solicitud = (request.getParameter("num_solicitud") != null) ? request.getParameter("num_solicitud") : "";
            String extracontractual = (request.getParameter("extracontractual") != null) ? request.getParameter("extracontractual") : "N";
            String otro_si = (request.getParameter("otro_si") != null) ? request.getParameter("otro_si") : "N";
            String secuencia = (request.getParameter("secuencia") != null) ? request.getParameter("secuencia") : "";
            this.reponseJson = dao.cargarGarantias(num_solicitud, extracontractual, otro_si, secuencia);          

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private void cargarConfigDocsMinuta() {
        try {
            //int tipo_doc = Integer.parseInt(request.getParameter("tipo_doc"));
            this.reponseJson =  dao.cargarConfigDocs();          
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public void guardarConfigDocsMinuta() {
       
        try {
            String tipo_doc = request.getParameter("tipo_doc") != null ? request.getParameter("tipo_doc") : "";
            String content = request.getParameter("content") != null ? Util.setCodificacionCadena(request.getParameter("content"),"ISO-8859-1") : "";
            this.reponseJson =  dao.guardarConfigDocs(tipo_doc, content, usuario);          
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(MinutasContratacionAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }  
    }   

    private void cargarEquivalencias() {
        try {
            this.reponseJson = dao.cargarEquivalencias();          
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private void listarClasificacionPersona() {
        try {           
            this.reponseJson = dao.listarClasificacionPersona();
        } catch (Exception ex) {
            Logger.getLogger(MinutasContratacionAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void generarCartaAceptacion() {

        try {      
            String num_solicitud = (request.getParameter("num_solicitud") != null) ? request.getParameter("num_solicitud") : "";
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");//se consiguen los datos de db.properties
            String directorioOrigen = rb.getString("ruta") + "/images/multiservicios/" + usuario.getLogin() + "/";//se establece la ruta de la imagen 
            String directorioDestino = rb.getString("rutaImagenes") + "gestionadministrativa/minutas/"; 
            /*String ruta = directorioArchivos + "actas/";   */
             this.createDir(directorioOrigen);
             this.reponseJson =  dao.generarCartaAceptacion(num_solicitud, directorioOrigen, directorioDestino, usuario.getLogin());          
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    
    private void generarContrato() {

        try {      
            String num_solicitud = (request.getParameter("num_solicitud") != null) ? request.getParameter("num_solicitud") : "";
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");//se consiguen los datos de db.properties
            String directorioOrigen = rb.getString("ruta") + "/images/multiservicios/" + usuario.getLogin() + "/";//se establece la ruta de la imagen 
            String directorioDestino = rb.getString("rutaImagenes") + "gestionadministrativa/minutas/"; 
            /*String ruta = directorioArchivos + "actas/";   */
             this.createDir(directorioOrigen);
             this.reponseJson =  dao.generarContrato(num_solicitud, directorioOrigen, directorioDestino, usuario.getLogin());          
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    
    private void listarTipoDocumentos() {
        try {
            String mostrarTodos = request.getParameter("mostrarTodos") != null ? request.getParameter("mostrarTodos") : "N";
            this.reponseJson = dao.listarTipoDocumentos(mostrarTodos);
        } catch (Exception ex) {
            Logger.getLogger(MinutasContratacionAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }
    
    public void insertarTipoDocumento() {

        try {
            String nombre = request.getParameter("nombre") != null ? Util.setCodificacionCadena(request.getParameter("nombre"),"ISO-8859-1") : "";
            String descripcion = request.getParameter("descripcion") != null ? Util.setCodificacionCadena(request.getParameter("descripcion"),"ISO-8859-1") : "";
            this.reponseJson = dao.guardarTipoDocumento(nombre, descripcion, usuario);
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(MinutasContratacionAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void actualizarTipoDocumento() {

        try {
            String id = request.getParameter("id") != null ? request.getParameter("id") : "";
            String nombre = request.getParameter("nombre") != null ? Util.setCodificacionCadena(request.getParameter("nombre"),"ISO-8859-1") : "";
            String descripcion = request.getParameter("descripcion") != null ? Util.setCodificacionCadena(request.getParameter("descripcion"),"ISO-8859-1") : "";
            this.reponseJson = dao.actualizarTipoDocumento(id, nombre, descripcion, usuario);
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(MinutasContratacionAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    } 
    
    public void activaInactivaTipoDocumento() {
 
        try {
            String id = request.getParameter("id") != null ? request.getParameter("id") : "";          
            this.reponseJson = dao.activaInactivaTipoDocumento(id, usuario);
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(MinutasContratacionAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    } 
    
    private void crearDocsContrato() throws Exception {
         try {
             JsonObject contratos = (JsonObject) (new JsonParser()).parse(request.getParameter("listadoContratos"));
             this.reponseJson = dao.crearDocsContrato(contratos, usuario);
         }catch (Exception ex) {
            java.util.logging.Logger.getLogger(MinutasContratacionAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }       
    }
    
    private void existenDocumentoRelContrato() {
        try {
            String tipo_contrato = request.getParameter("tipo_contrato") != null ? request.getParameter("tipo_contrato") : "";
            String num_contrato = request.getParameter("num_contrato") != null ? request.getParameter("num_contrato") : "";            
            if (dao.existeContratoDoc(num_contrato,tipo_contrato)) {
                this.reponseJson = "{\"respuesta\":\"SI\"}";              
            } else {
                this.reponseJson = "{\"respuesta\":\"NO\"}";             
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private void cargarInfoContratoDocs() {
        try {
            String tipo_contrato = request.getParameter("tipo_contrato") != null ? request.getParameter("tipo_contrato") : "";
            String num_contrato = request.getParameter("num_contrato") != null ? request.getParameter("num_contrato") : "";  
            this.reponseJson = dao.getInfoContratoDocs(num_contrato,tipo_contrato);
        } catch (Exception ex) {
            Logger.getLogger(MinutasContratacionAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }
    
    public void guardarContratoDocs() {

        try {
            String tipo_contrato = request.getParameter("tipo_contrato") != null ? request.getParameter("tipo_contrato") : "";
            String num_contrato = request.getParameter("num_contrato") != null ? request.getParameter("num_contrato") : "";  
            String content = request.getParameter("content") != null ? Util.setCodificacionCadena(request.getParameter("content"),"ISO-8859-1") : "";
            this.reponseJson = dao.actualizarContratoDocs(tipo_contrato, num_contrato, content, usuario);
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(MinutasContratacionAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }
    
    public void cargarGridAsignacionBroker() {
        try {
            String fechainiContrato = request.getParameter("fecha_inicial") != null ? request.getParameter("fecha_inicial") : "";
            String fechafinContrato = request.getParameter("fecha_final") != null ? request.getParameter("fecha_final") : "";
            String num_solicitud = request.getParameter("num_solicitud") != null ? request.getParameter("num_solicitud") : "";
            String nombre_cliente = request.getParameter("nombre_cliente") != null ? request.getParameter("nombre_cliente") : "";
            String lineaNegocio = request.getParameter("lineaNegocio") == null ? "" : request.getParameter("lineaNegocio");
            String responsable = request.getParameter("responsable") == null ? "" : Util.setCodificacionCadena(request.getParameter("responsable"),"ISO-8859-1");
            String etapaActual = request.getParameter("etapaActual") == null ? "" : Util.setCodificacionCadena(request.getParameter("etapaActual"),"ISO-8859-1");
            String id_cliente = request.getParameter("id_cliente") == null ? "" : Util.setCodificacionCadena(request.getParameter("id_cliente"),"ISO-8859-1");
            String nom_proyecto = request.getParameter("nom_proyecto") == null ? "" : Util.setCodificacionCadena(request.getParameter("nom_proyecto"),"ISO-8859-1");
            String foms = request.getParameter("foms") == null ? "" : Util.setCodificacionCadena(request.getParameter("foms"),"ISO-8859-1");
            String tipo_proyecto = request.getParameter("tipo_proyecto") == null ? "" : Util.setCodificacionCadena(request.getParameter("tipo_proyecto"),"ISO-8859-1");
            this.reponseJson = dao.cargarGridAsignacionBroker(fechainiContrato, fechafinContrato, num_solicitud, nombre_cliente, lineaNegocio, responsable, etapaActual, id_cliente, nom_proyecto,foms, tipo_proyecto );
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(MinutasContratacionAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }
    
    private void listarBroker() {
        try {
            this.reponseJson = dao.listarBroker();
        } catch (Exception ex) {
            Logger.getLogger(MinutasContratacionAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }   
    
    public void asignarBrokerContrato() {

        try {          
            String num_contrato = request.getParameter("num_contrato") != null ? request.getParameter("num_contrato") : ""; 
            String id_broker = request.getParameter("id_broker") != null ? request.getParameter("id_broker") : "0";      
            String id_beneficiario = request.getParameter("id_beneficiario") != null ? request.getParameter("id_beneficiario") : "0";      
            this.reponseJson = (!dao.existeBrokerRelacionado(num_contrato,id_beneficiario)) ? dao.insertarRelBrokerMinuta(num_contrato, id_broker, id_beneficiario, usuario) : dao.actualizarRelBrokerMinuta(num_contrato, id_broker, id_beneficiario, usuario);
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(MinutasContratacionAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }
    
     public void cargarGridSolicitudesBroker() {
        try {
            String fechainiContrato = request.getParameter("fecha_inicial") != null ? request.getParameter("fecha_inicial") : "";
            String fechafinContrato = request.getParameter("fecha_final") != null ? request.getParameter("fecha_final") : "";
            String num_solicitud = request.getParameter("num_solicitud") != null ? request.getParameter("num_solicitud") : "";
            String nombre_cliente = request.getParameter("nombre_cliente") != null ? request.getParameter("nombre_cliente") : "";
            String lineaNegocio = request.getParameter("lineaNegocio") == null ? "" : request.getParameter("lineaNegocio");
            String responsable = request.getParameter("responsable") == null ? "" : Util.setCodificacionCadena(request.getParameter("responsable"),"ISO-8859-1");
            this.reponseJson = dao.cargarGridSolicitudesBroker(fechainiContrato, fechafinContrato, num_solicitud, nombre_cliente, lineaNegocio, responsable, usuario);
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(MinutasContratacionAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }
     
    private void listarAseguradoras() {
        try {
            this.reponseJson = dao.listarAseguradoras();
        } catch (Exception ex) {
            Logger.getLogger(MinutasContratacionAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }   
    
     private void listarCotizacionAseguradora() {

        try {
            String num_solicitud = (request.getParameter("num_solicitud") != null) ? request.getParameter("num_solicitud") : "";
            String id_aseguradora = (request.getParameter("id_aseguradora") != null) ? request.getParameter("id_aseguradora") : "";
            String id_beneficiario = (request.getParameter("id_beneficiario") != null) ? request.getParameter("id_beneficiario") : "";
            String secuencia = (request.getParameter("secuencia") != null) ? request.getParameter("secuencia") : "0";
            this.reponseJson = dao.cargarCotizacionAseguradora(num_solicitud,id_aseguradora,id_beneficiario,secuencia);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
     
    public void guardarCotizacionAseguradora() {
       
        try {
           
            String num_contrato = request.getParameter("num_contrato") != null ? request.getParameter("num_contrato") : "";  
            String id_aseguradora = (request.getParameter("id_aseguradora") != null) ? request.getParameter("id_aseguradora") : "";  
            String id_beneficiario = (request.getParameter("id_beneficiario") != null) ? request.getParameter("id_beneficiario") : "";  
            String secuencia = (request.getParameter("secuencia") != null) ? request.getParameter("secuencia") : "0";  
            JsonObject cotizaciones = (JsonObject) (new JsonParser()).parse(request.getParameter("listadoaseguradora"));    
            String otros_gastos = (request.getParameter("otros_gastos") != null) ? request.getParameter("otros_gastos") : "0";
            String porc_iva = (request.getParameter("porc_iva") != null) ? request.getParameter("porc_iva") : "0";
            String valor_iva = (request.getParameter("valor_iva") != null) ? request.getParameter("valor_iva") : "0";
                 
            this.reponseJson =  dao.guardarCotizacionAseguradora(num_contrato,id_aseguradora,id_beneficiario,secuencia,cotizaciones,otros_gastos,porc_iva,valor_iva,usuario);          
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(MinutasContratacionAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }  
    }
    
    private void listarTotalesCotAseguradora() {

        try {
            
            String num_contrato = request.getParameter("num_contrato") != null ? request.getParameter("num_contrato") : "";  
            String cotizado_broker = request.getParameter("cotizado_broker") != null ? request.getParameter("cotizado_broker") : "N";  
            this.reponseJson = dao.cargarGridTotalesAseguradora(num_contrato,cotizado_broker, usuario);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private void listarDetalleCotAseguradora() {

        try {
            
            String num_contrato = request.getParameter("num_contrato") != null ? request.getParameter("num_contrato") : ""; 
            String id_aseguradora = (request.getParameter("id_aseguradora") != null) ? request.getParameter("id_aseguradora") : "";  
            String id_beneficiario = (request.getParameter("id_beneficiario") != null) ? request.getParameter("id_beneficiario") : ""; 
            String secuencia = (request.getParameter("secuencia") != null) ? request.getParameter("secuencia") : "0"; 
            this.reponseJson = dao.cargarDetalleCotizacionAseguradora(num_contrato, id_aseguradora, id_beneficiario, secuencia);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void enviarCotizacionBroker() {
 
        try {
            String num_contrato = request.getParameter("num_contrato") != null ? request.getParameter("num_contrato") : "";           
            this.reponseJson = dao.enviarCotizacionBroker(num_contrato, usuario);
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(MinutasContratacionAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    } 
    
    public void aceptarCotizacionBroker() {
 
        try {
            String num_contrato = request.getParameter("num_contrato") != null ? request.getParameter("num_contrato") : ""; 
            String id_aseguradora = (request.getParameter("id_aseguradora") != null) ? request.getParameter("id_aseguradora") : ""; 
            String id_beneficiario = (request.getParameter("id_beneficiario") != null) ? request.getParameter("id_beneficiario") : ""; 
            String secuencia = (request.getParameter("secuencia") != null) ? request.getParameter("secuencia") : "0"; 
            this.reponseJson = dao.seleccionarCotizacionBroker(num_contrato, id_aseguradora, id_beneficiario, secuencia, usuario);
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(MinutasContratacionAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }
    
    public void anularGarantiaContrato() {
 
        try {
            String id = request.getParameter("id") != null ? request.getParameter("id") : "";           
            this.reponseJson = dao.anularGarantiaMinuta(id, usuario);
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(MinutasContratacionAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    } 
    
    private void cargarOtrosCostosAseguradora() {
        try {          
            String num_contrato = (request.getParameter("id_contrato") != null) ? request.getParameter("id_contrato") : "";
            String id_aseguradora = (request.getParameter("id_aseguradora") != null) ? request.getParameter("id_aseguradora") : "";
            String id_beneficiario = (request.getParameter("id_beneficiario") != null) ? request.getParameter("id_beneficiario") : "";
            String secuencia = (request.getParameter("secuencia") != null) ? request.getParameter("secuencia") : "0";
            this.reponseJson = dao.getInfoOtrosCostosAseguradora(num_contrato, id_aseguradora, id_beneficiario, secuencia);
        } catch (Exception ex) {
            Logger.getLogger(MinutasContratacionAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }
    
    private void generarCXPAseguradora() {

        try {      
            
            String listadoPolizas = request.getParameter("listadoPolizas");           
            JsonParser jsonParser = new JsonParser();
            JsonObject jpol = (JsonObject) jsonParser.parse(listadoPolizas);
            JsonArray jsonArrPol = jpol.getAsJsonArray("polizas");            
            String num_contrato = request.getParameter("num_contrato") != null ? request.getParameter("num_contrato") : "";
            String valor_gastos = request.getParameter("valor_gastos") != null ? request.getParameter("valor_gastos") : "0";
            String concepto_iva = request.getParameter("concepto_iva") != null ? request.getParameter("concepto_iva") : "";     
            String valor_iva = request.getParameter("valor_iva") != null ? request.getParameter("valor_iva") : "0";         
            String num_factura = request.getParameter("num_factura") != null ? request.getParameter("num_factura") : "";
            List<String> x = new ArrayList<String>();
            for (int i = 0; i < jsonArrPol.size(); i++) {
                String id_aseguradora = jsonArrPol.get(i).getAsJsonObject().get("id").getAsJsonPrimitive().getAsString();
                x.add(id_aseguradora);               
            }
            String[] Array = x.toArray(new String[0]);
            String[] split = dao.generarCxPAseguradora(num_factura,num_contrato, Array, valor_gastos, concepto_iva, usuario.getLogin()).split(";");
            this.reponseJson =  "{\"respuesta\":\"" + split[0] + "\"}";  
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    
    private void cargarInfoAseguradoraAceptada() {
        try {
            String num_contrato = (request.getParameter("num_contrato") != null) ? request.getParameter("num_contrato") : "";
            this.reponseJson = dao.getInfoAseguradoraAceptada(num_contrato);
        } catch (Exception ex) {
            Logger.getLogger(MinutasContratacionAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }
    
    private void listarCotizPendientesCxPAseguradora() {

        try {                  
            String num_contrato = request.getParameter("num_contrato") != null ? request.getParameter("num_contrato") : "";           
            String id_aseguradora = (request.getParameter("id_aseguradora") != null) ? request.getParameter("id_aseguradora") : "";     
            String id_beneficiario = (request.getParameter("id_beneficiario") != null) ? request.getParameter("id_beneficiario") : ""; 
            String secuencia = request.getParameter("secuencia") != null ? request.getParameter("secuencia") : "0";
            this.reponseJson = dao.cargarCotizacionPendienteCxPAseguradora(num_contrato, id_aseguradora, id_beneficiario, secuencia);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private void listarBeneficiariosPoliza() {
        try {        
            String Excluirid = request.getParameter("param") != null ? request.getParameter("param") : ""; 
            this.reponseJson = dao.listarBeneficiariosPoliza(Excluirid);
        } catch (Exception ex) {
            Logger.getLogger(MinutasContratacionAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }
    
    private void listarCausalesPoliza() {
        try {
            this.reponseJson = dao.listarCausalesPoliza();
        } catch (Exception ex) {
            Logger.getLogger(MinutasContratacionAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }
    
    private void cargarCboBeneficiarioAceptadoCxP() {
        try {
            String num_contrato = (request.getParameter("num_contrato") != null) ? request.getParameter("num_contrato") : "";
            this.reponseJson = dao.cargarCboBeneficiarioCxP(num_contrato);
        } catch (Exception ex) {
            Logger.getLogger(MinutasContratacionAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }
    
    private void cargarCboBeneficiariosContrato() {
        try {
            String num_contrato = (request.getParameter("num_contrato") != null) ? request.getParameter("num_contrato") : "";
            this.reponseJson = dao.listarBeneficiariosContrato(num_contrato);
        } catch (Exception ex) {
            Logger.getLogger(MinutasContratacionAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }
    
    private void cargarCboBeneficiariosAsignadosBroker() {
        try {
            String num_contrato = (request.getParameter("num_contrato") != null) ? request.getParameter("num_contrato") : "";
            this.reponseJson = dao.listarBeneficiariosAsignadosBroker(num_contrato, usuario);
        } catch (Exception ex) {
            Logger.getLogger(MinutasContratacionAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }
    
     private void cotizacionPendienteBroker() {
        try {
            String num_contrato = request.getParameter("num_contrato") != null ? request.getParameter("num_contrato") : "";            
            if (dao.cotizacionPendienteBroker(num_contrato,usuario)) {
                this.reponseJson = "{\"respuesta\":\"SI\"}";              
            } else {
                this.reponseJson = "{\"respuesta\":\"NO\"}";             
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
     
    private void listarOtrosSi() {

        try {
            String num_contrato = (request.getParameter("num_contrato") != null) ? request.getParameter("num_contrato") : "";           
            this.reponseJson = dao.cargarOtrosSi(num_contrato);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private void insertarOtroSi() {
        try {
            String num_contrato = request.getParameter("num_contrato") != null ? request.getParameter("num_contrato") : "";
            this.reponseJson = dao.insertarGarantiaOtroSi(num_contrato, usuario);       
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void guardarInfoOtroSi() {
       
        try {          
            String num_contrato = request.getParameter("num_contrato") != null ? request.getParameter("num_contrato") : "";
            JsonObject garantias_otro_si = (JsonObject) (new JsonParser()).parse(request.getParameter("listadogarantias_otro_si"));
          
            this.reponseJson =  dao.guardarOtroSi(num_contrato,usuario, garantias_otro_si);          
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(MinutasContratacionAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }  
    }
    
    public void anularOtroSi() {
       
        try {          
            String num_contrato = request.getParameter("num_contrato") != null ? request.getParameter("num_contrato") : "";
            String secuencia = request.getParameter("secuencia") != null ? request.getParameter("secuencia") : "";
         
            this.reponseJson =  dao.anularOtroSi(num_contrato, secuencia, usuario);          
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(MinutasContratacionAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }  
    }
    
    private void cargarCboOtrosSiContrato() {
        try {
            String num_contrato = (request.getParameter("num_contrato") != null) ? request.getParameter("num_contrato") : "";
            String id_beneficiario = (request.getParameter("id_beneficiario") != null) ? request.getParameter("id_beneficiario") : "";
            this.reponseJson = dao.cargarCboOtrosSiBeneficiario(num_contrato, id_beneficiario);
        } catch (Exception ex) {
            Logger.getLogger(MinutasContratacionAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }
    
    private void cargarCboOtrosSiAceptadoCxP() {
        try {
            String num_contrato = (request.getParameter("num_contrato") != null) ? request.getParameter("num_contrato") : "";
            this.reponseJson = dao.cargarCboOtrosSiCxP(num_contrato);
        } catch (Exception ex) {
            Logger.getLogger(MinutasContratacionAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }
    
}