/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * ActividadGestionAction.java :
 * Copyright 2010 Rhonalf Martinez Villa <rhonaldomaster@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.tsp.opav.controller;
import java.util.*;
import com.tsp.opav.model.beans.*;
import com.tsp.opav.model.services.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.services.SerieGeneralService;
import com.tsp.operation.model.services.TablaGenManagerService;

/**
 *
 * @author rhonalf
 */
public class ActividadGestionAction extends Action {

    Usuario usuario;
    public ActividadGestionAction() {
    }

    @Override
    public void run(){
        ActividadAccionService aserv;
        String next = "/jsp/opav/gestion_actividades.jsp";
        ActividadAccion actividad = null;
        String resp="";
        boolean redirect = false;
        try {
            String opcion = request.getParameter("opcion")!=null ? request.getParameter("opcion") : "search";
            String user = request.getParameter("usuario")!=null ? request.getParameter("usuario") : ""; 
            usuario = (Usuario) request.getSession().getAttribute("Usuario");
            aserv = new ActividadAccionService(usuario.getBd());
             if(opcion.equals("insert_tipo")){
                try {
                    TablaGenManagerService tbs = new TablaGenManagerService(usuario.getBd());
                    SerieGeneralService  serie = new SerieGeneralService(usuario.getBd());
                    //dstrct,agency_id,document_type
                     int codigo = serie.getSerie("FINV", "OP", "TIPO_ACTIVIDAD").getUltimo_numero_entero();
                     String descripcion = "Tipos Actividades";
                     String dato = request.getParameter("tipo")!=null ? request.getParameter("tipo") : "";
                     tbs.agregarRegistro("TIPO_ACT",String.valueOf(codigo), "", descripcion, user, dato);
                     serie.setSerie("FINV", "OP", "TIPO_ACTIVIDAD");
                     resp="EL Tipo De Actividad "+ dato + " Se Registro Con Exito";
                      
                }
                catch (Exception e) {
                    resp=e.toString();
                    e.printStackTrace();
                }
                this.escribirResponse(resp);
                redirect = false;
            }
           else
               if(opcion.equals("update_tipo")){
                try {
                    TablaGenManagerService tbs = new TablaGenManagerService(usuario.getBd());
                    //SerieGeneralService  serie = new SerieGeneralService();
                    //dstrct,agency_id,document_type
                     String oid = request.getParameter("oid")!=null ? request.getParameter("oid") : "";
                     String codigo = request.getParameter("codigo")!=null ? request.getParameter("codigo") : "";
                     String descripcion = "Tipos Actividades";
                     String dato = request.getParameter("tipo")!=null ? request.getParameter("tipo") : "";
                     tbs.actualizarRegistro(oid,"TIPO_ACT",codigo, "", descripcion, user, dato);
                    
                     resp="EL Tipo De Actividad "+ dato + " Se Actualizo Con Exito";
                     
                }
                catch (Exception e) {
                    resp=e.toString();
                    e.printStackTrace();
                }
                this.escribirResponse(resp);
                redirect = false;
            }
             else
               {






            if(opcion.equals("insert")){
                actividad = this.crearObjeto();
                try {
                    aserv.insertar(actividad,user);
                }
                catch (Exception e) {
                    System.out.println("Error al insertar la actividad: "+e.toString());
                    e.printStackTrace();
                }
                redirect = true;
            }
            else if(opcion.equals("delete")){
                String codigo = request.getParameter("codigo")!=null?request.getParameter("codigo"):"";
                try {
                    aserv.anular(codigo,user);
                }
                catch (Exception e) {
                    System.out.println("Error al anular la actividad: "+e.toString());
                    e.printStackTrace();
                }
                redirect = true;
            }
            else if(opcion.equals("search")){
                redirect = false;
                String tabla="";
                tabla = "<table border='1' style='border-collapse:collapse;border-color: black;' width='100%'>";
                tabla = tabla + "<thead><tr class='subtitulo1'>" +
                                        "<th>Codigo</th>" +
                                        "<th>Tipo</th>" +
                                        "<th>Peso Predeterminado</th>" +
                                        "<th>Orden Predeterminado</th>" +
                                        "<th>Descripcion</th>" +

                                        "<th>Responsable Predeterminado</th>" +

                                        "</tr><thead>";
                tabla = tabla + "<tbody>";
                String filtro = request.getParameter("param")!=null?request.getParameter("param"):"descripcion";
                String param = request.getParameter("cadena")!=null?request.getParameter("cadena"):"";
                ArrayList lista = null;
                try {
                    lista = aserv.buscar(filtro, param);
                }
                catch (Exception e) {
                    lista = null;
                    System.out.println("Error al buscar datos de actividades: "+e.toString());
                    e.printStackTrace();
                }
                if(lista!=null && lista.size()>0){
                    for (int i = 0; i < lista.size(); i++) {
                        actividad = (ActividadAccion)lista.get(i);
                        tabla = tabla + "<tr class='fila'>";
                        tabla = tabla + "<td>"+actividad.getId()+"</td>" +
                                    "<td>"+actividad.getTipo()+"</td>" +
                                    "<td>"+actividad.getPeso_predeterminado()+"</td>" +
                                    "<td>"+actividad.getOrden_predeterminado()+"</td>" +
                                    "<td>"+actividad.getDescripcion()+"</td>" +
                                      "<td>"+aserv.nomDat(actividad.getResponsable_predeterminado())+"</td>";
                        tabla = tabla + "</tr>";
                    }
                }
                else{
                    tabla = tabla + "<tr class='fila'><td colspan='6' align='center'>No se encontraron resultados</td></tr>";
                }
                tabla = tabla + "</tbody></table>";
                this.escribirResponse(tabla);
            }
            else if(opcion.equals("modify")){
                actividad = this.crearObjeto();
                try {
                    aserv.actualizar(actividad,user);
                }
                catch (Exception e) {
                    System.out.println("Error al modificar la actividad: "+e.toString());
                    e.printStackTrace();
                }
                redirect = true;
            }
            else if(opcion.equals("listar")){
                redirect = false;
                String codigo = request.getParameter("id")!=null?request.getParameter("id"):"0";
                String datos = "";
                try {
                    actividad = (ActividadAccion)( ( aserv.buscar("id", codigo) ).get(0) );
                }
                catch (Exception e) {
                    actividad=null;
                    System.out.println("Error al buscar los datos del codigo "+codigo+": "+e.toString());
                }
                if(actividad!=null){
                    datos = ""+actividad.getDescripcion()+
                            ";_;"+actividad.getTipo()+
                            ";_;"+actividad.getOrden_predeterminado()+
                            ";_;"+actividad.getPeso_predeterminado()+
                            ";_;"+actividad.getResponsable_predeterminado()+";_;" ;
                }
                else{
                    datos = ";_;;_;;_;;_;;_;";
                }
                this.escribirResponse(datos);
            }

            else{
                redirect = true;
            }
           }
        }
        catch (Exception e) {
            System.out.println("Error en ActividadGestionAction.java: "+e.toString());
            e.printStackTrace();
        }
        finally{
            try {
                if(redirect==true){
                    this.dispatchRequest(next);
                }
            }
            catch (Exception e) {
                System.out.println("Error en ActividadGestionAction.java al hacer forward: "+e.toString());
                e.printStackTrace();
            }
        }
    }

    /**
     * Le entrega los datos a la peticion ajax
     * @param resp lo que se quiere entregar
     * @throws Exception cuando ocurre un error
     */
    private void escribirResponse(String resp) throws Exception{
        try {
            response.setContentType("text/plain; charset=utf-8");
            response.setHeader("Cache-Control", "no-cache");
            response.getWriter().println(resp);
        } catch (Exception e) {
            System.out.println("Error al escribir la respuesta: "+e.toString());
            throw new Exception("Error al escribir la respuesta: "+e.toString());
        }
    }

    /**
     * Crea un objeto de tipo ActividadAccion basado en los parametros que envia la pagina
     * @return objeto de tipo ActividadAccion con datos
     */
    private ActividadAccion crearObjeto(){
        ActividadAccion act = new ActividadAccion();
        try {
            act.setDescripcion(request.getParameter("descripcion")!=null?request.getParameter("descripcion"):"");
            act.setTipo(request.getParameter("tipo")!=null?request.getParameter("tipo"):"");
            act.setId((request.getParameter("codigo")!=null)&&(!(request.getParameter("codigo").equals("")))?Integer.parseInt((request.getParameter("codigo")).trim()):0);
            act.setOrden_predeterminado((request.getParameter("orden")!=null)&&(!(request.getParameter("orden").equals("")))?Integer.parseInt((request.getParameter("orden")).trim()):0);
            act.setPeso_predeterminado((request.getParameter("peso")!=null)&&(!(request.getParameter("peso").equals("")))?Integer.parseInt((request.getParameter("peso")).trim()):0);
            act.setResponsable_predeterminado(request.getParameter("resp_pred")!=null?request.getParameter("resp_pred"):"");
        }
        catch (Exception e) {
            System.out.println("Error al crear el objeto en ActividadGestionAction.java: "+e.toString());
            e.printStackTrace();
        }
        return act;
    }

}