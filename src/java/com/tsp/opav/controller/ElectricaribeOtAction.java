package com.tsp.opav.controller;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.tsp.opav.model.DAOS.ProcesosClienteDAO;
import com.tsp.opav.model.DAOS.impl.ProcesosClienteImpl;
import com.tsp.opav.model.beans.*;
import com.tsp.operation.model.beans.Usuario;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.StringTokenizer;
import com.tsp.operation.model.TransaccionService;
import com.tsp.operation.model.beans.BeanGeneral;


public class ElectricaribeOtAction extends Action{
    
    private ProcesosClienteDAO dao;

    public ElectricaribeOtAction(){
    }

    public void run(){
        HttpSession                 session     = request.getSession();
        Usuario                     usuario     = (Usuario) session.getAttribute("Usuario");
        String                      msjGenerar  = "OT generada exitosamente";
        String                      msjAceptar  = "Pagos Aceptados";
        String                      msjImprimir = "PDF Generado";
        String                      msjFinanciacion = "Financiacion hecha";
        ArrayList<SubclienteEca>    subs        = new ArrayList<SubclienteEca>();
        OrdenTrabajo                ot;
        SubclienteEca               sub;

        String id           = "";
        String cuotas       = "";
        String meses        = "";
        String val_cuota    = "";
        String porc_cuotas  = "";
        String porc_base    = "";
        String nics         = "";        
        String id_solicitud = "";
        String tipo         = "";
        
        try {
            
            /* Esta parte para la aceptacion de los pagos
             * de los clientes de un cliente padre.
             */
            if(request.getParameter("opcion").equals("1")){
                try {
                    if ((request.getParameter("id") != null) &&
                        (request.getParameter("cuotas") != null) &&
                        (request.getParameter("porc_cuotas") != null) &&
                        (request.getParameter("porc_base") != null) &&
                        (request.getParameter("meses") != null) &&
                        (request.getParameter("nics") != null)){

                        id              = request.getParameter("id");
                        cuotas          = request.getParameter("cuotas");
                        meses           = request.getParameter("meses");
                        val_cuota       = request.getParameter("val_ci");
                        porc_cuotas     = request.getParameter("porc_cuotas");
                        porc_base       = request.getParameter("porc_base");
                        nics            = request.getParameter("nics");
                        tipo            = request.getParameter("tipo");

                        id_solicitud    = request.getParameter("id_solicitud");

                        if (model.ElectricaribeOtSvc.isPaymentReady(id_solicitud)){
                            StringTokenizer idTokens = new StringTokenizer(id, ";");
                            StringTokenizer cuTokens = new StringTokenizer(cuotas, ";");
                            StringTokenizer meTokens = new StringTokenizer(meses, ";");
                            StringTokenizer viTokens = new StringTokenizer(val_cuota, ";");
                            StringTokenizer pcTokens = new StringTokenizer(porc_cuotas, ";");
                            StringTokenizer pbTokens = new StringTokenizer(porc_base, ";");
                            StringTokenizer ncTokens = new StringTokenizer(nics, ";");
                            StringTokenizer tpTokens = new StringTokenizer(tipo, ";");

                            while (idTokens.hasMoreTokens()) {
                                sub = new SubclienteEca();
                                sub.setId_cliente(          idTokens.nextToken());
                                sub.setId_solicitud(        id_solicitud);
                                sub.setPeriodo(             cuTokens.nextToken());
                                sub.setMeses_mora(          meTokens.nextToken());
                                sub.setPorc_cuota_inicial(  pcTokens.nextToken());
                                sub.setPorc_base(           pbTokens.nextToken());
                                sub.setVal_cuota_inicial(   viTokens.nextToken());
                                sub.setNic(                 ncTokens.nextToken());
                                sub.setTipo(                 tpTokens.nextToken());
                                subs.add(sub);
                            }

                            model.ElectricaribeOtSvc.insertPayments(subs, usuario.getLogin());
                        }
                        else{
                            msjAceptar = "La solicitud no puede ser actualizada\nporque debe estar pendiente por\nACEPTACION DE CLIENTE. (060)";
                        }
                    }
                    else{
                        msjAceptar = "Faltan datos por llenar";
                    }
                }
                catch (Exception e) {
                    System.out.println("error en action de ecaot:"+e.toString()+"__"+e.getMessage());
                    e.printStackTrace();
                    msjAceptar = "Error aceptando los pagos";
                }
            }

            /* Esta parte del codigo es cuando se va a
             * ingresar una nueva OT, y enseguida genera
             * el PDF.
             */
            if (request.getParameter("opcion").equals("2")) {
                boolean sw = true;

                if ((request.getParameter("id_solicitud") != null)
                        && (request.getParameter("observaciones") != null)) {

                    ot = new OrdenTrabajo();
                    ot.setId_solicitud(request.getParameter("id_solicitud"));
                    ot.setObservaciones(request.getParameter("observaciones"));
                    ot.setUser_update(usuario.getLogin());
                    
                    String tipo_sol = model.NegociosApplusService.getDescTipoSolc(ot.getId_solicitud());
                    if (tipo_sol.equals("AAAE")) {
                        sw = model.ElectricaribeOtSvc.isPaymentReady(ot.getId_solicitud());
                    } else {
                        sw = model.ElectricaribeOtSvc.isOtReady(ot.getId_solicitud());
                    }
                    if (sw) {
                        if (tipo_sol.equals("AAAE")) {
                            BeanGeneral info = model.ElectricaribeOtSvc.getInfoAceptarPagos(ot.getId_solicitud());
                            if (info.getValor_02().equals("A")) {
                                TransaccionService tService = new TransaccionService(usuario.getBd());
                                tService.crearStatement();
                                info.setValor_06(ot.getId_solicitud());
                                info.setValor_07(usuario.getLogin());
                                if(!model.ElectricaribeOtSvc.isOtReady(ot.getId_solicitud())){
                                    tService.getSt().addBatch(model.ElectricaribeOtSvc.aceptarPagos(info));
                                }
                                if (request.getParameter("cambiarcons").equals("true")) {
                                    tService.getSt().addBatch(model.ElectricaribeOtSvc.sqlIngresarOT(ot, true, usuario.getLogin()));
                                } else {
                                    String[] globalsql = model.ElectricaribeOtSvc.sqlIngresarOT(ot, false, usuario.getLogin()).split(";");
                                    for(int i = 0; i < globalsql.length; i++)  {
                                        tService.getSt().addBatch(globalsql[i]);
                                    }
                                }
				try {
                                    tService.execute();
                                } catch (Exception e) {
                                    e.printStackTrace();
                                    msjGenerar = e.getMessage();
                                }
                            } else {
                                msjGenerar = "La solicitud no se encuentra aprobada por el area de riesgos";
                            }

                        } else {


                            if (request.getParameter("cambiarcons").equals("true")) {
                                model.ElectricaribeOtSvc.insertarOT(ot, true, usuario.getLogin());
                            } else {
                                model.ElectricaribeOtSvc.insertarOT(ot, false, usuario.getLogin());
                            }
                            model.ElectricaribeOtSvc.insertarCotizacionOrdenCompra(ot, usuario.getLogin());
                        }

                        model.ElectricaribeOtSvc.setBeans(ot.getId_solicitud());
                        model.ElectricaribeOtSvc.getContratistas(ot.getId_solicitud());

                        model.ElectricaribeOtSvc.doPDF(usuario.getLogin(), ot.getId_solicitud());
                    } else {
                        if (tipo_sol.equals("AAAE")) {
                            msjGenerar = "La solicitud necesita estar PENDIENTE POR ACEPTACION DEL CLIENTE. (060)";
                        } else {
                            msjGenerar = "La solicitud necesita estar PENDIENTE POR GENERAR OT. (070)";
                        }
                    }
                } else {
                    msjGenerar = "Faltan datos por llenar";
                }
                
                dao = new ProcesosClienteImpl(usuario.getBd());
                JsonObject Centro_costo = (JsonObject) new JsonParser().parse(dao.generar_Centros_Costos(request.getParameter("id_solicitud"), usuario, 2));

                if (Centro_costo.has("error")) {
                    msjGenerar = Centro_costo.get("error").getAsString();
                }
            }

            /* Esta parte es solamente para imprimir
             * el PDF de la OT.
             */
            if(request.getParameter("opcion").equals("3")){

                if((request.getParameter("id_solicitud")!=null) &&
                   (request.getParameter("observaciones")!=null)){

                    ot = new OrdenTrabajo();
                    ot.setId_solicitud(request.getParameter("id_solicitud"));
                    ot.setObservaciones(request.getParameter("observaciones"));
                    ot.setUser_update(usuario.getLogin());

                    if (model.ElectricaribeOtSvc.isOtReadyForPDF(ot.getId_solicitud())){

                        model.ElectricaribeOtSvc.setBeans(ot.getId_solicitud());
                        model.ElectricaribeOtSvc.getContratistas(ot.getId_solicitud());

                        model.ElectricaribeOtSvc.doPDF(usuario.getLogin(), ot.getId_solicitud());
                    }
                    else{
                        msjImprimir  = "No hay orden de trabajo para esta solicitud.";
                    }
                }
                else{
                    msjImprimir  = "Faltan datos por llenar";
                }
            }

            /* Esta parte es para lo de la actualizacion
             * de la fecha de financiaci\F3n.
             */
            if(request.getParameter("opcion").equals("4")){

                if((request.getParameter("idsol")!=null) &&
                   (request.getParameter("fecha")!=null)){

                    try {
                        model.ElectricaribeOtSvc.financiacion(request.getParameter("fecha"), request.getParameter("idsol"));
                    }
                    catch (Exception e) {
                        msjFinanciacion = "Error financiando";
                    }
                }
            }
        }
        catch (Exception ex){
            msjImprimir  = model.ElectricaribeOtSvc.getNext();
        }

        try {
            response.setContentType("text/plain; charset=utf-8");//100313
            response.setHeader("Cache-Control", "no-cache");

            if(request.getParameter("opcion").equals("1")){
                response.getWriter().println(msjAceptar);
            }
            if(request.getParameter("opcion").equals("2")){
                response.getWriter().println(msjGenerar);
            }
            if(request.getParameter("opcion").equals("3")){
                response.getWriter().println(msjImprimir);
            }
            if(request.getParameter("opcion").equals("4")){
                response.getWriter().println(msjFinanciacion);
            }
        }
        catch (Exception ex) {
        }
    }
}