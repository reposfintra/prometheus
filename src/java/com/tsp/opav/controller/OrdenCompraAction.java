/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.opav.controller;

import com.tsp.exceptions.InformationException;
import com.tsp.opav.model.DAOS.OrdenCompraDAO;
import com.tsp.opav.model.DAOS.impl.OrdenCompraImpl;
import com.tsp.opav.model.DAOS.impl.ProcesosClienteImpl;
import com.tsp.operation.model.beans.Usuario;
import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;

/**
 *
 * @author rsantamaria
 */
public class OrdenCompraAction extends Action {
    
    private final int CARGAR_BODEGAS = 0;
    private final int GUARDAR_DIRECCION = 1;
    private final int OBTENER_DIRECCION_ENTREGA = 2;
    private final int CARGAR_BODEGA_PRINCIPAL = 3;
    private final int CARGAR_INFO_BODEGA_ASIGNADA = 4;
    private final int CARGAR_BODEGAS_POR_USUARIO = 5;
    private final int CARGAR_BODEGAS_POR_USUARIO_SOLICITUD_EJECUCION = 6;
    
    
    private OrdenCompraDAO dao;
    Usuario usuario = null;
    String reponseJson = "{}";

    @Override
    public void run() throws ServletException, InformationException {
      try {
            HttpSession session = request.getSession();
            usuario = (Usuario) session.getAttribute("Usuario");
            
            String bd = usuario.getBd();
            dao = new OrdenCompraImpl(bd);
            int opcion = (request.getParameter("opcion") != null)
                    ? Integer.parseInt(request.getParameter("opcion"))
                    : -1;
            switch (opcion) {
                
                case CARGAR_BODEGAS:
                    cargarBodegas(bd);
                break;
                case GUARDAR_DIRECCION:
                    gardarDireccion();
                break;
                case OBTENER_DIRECCION_ENTREGA:
                    obtenerDireccionEntrega();
                break;
                case CARGAR_BODEGA_PRINCIPAL:
                    cargarBodegaPrincipal(bd);
                break;
                case CARGAR_INFO_BODEGA_ASIGNADA:
                    cargarInfoBodegaAsignada();
                break;
                case CARGAR_BODEGAS_POR_USUARIO:
                    cargarBodegasPorUsuario();
                break;             
                case CARGAR_BODEGAS_POR_USUARIO_SOLICITUD_EJECUCION:
                    cargarBodegasPorUsuarioSolicitudEjecucion();
                break;                 
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private void cargarBodegas(String bd) {
        try {
            String id_solicitud = request.getParameter("id_solicitud");
            String json = dao.cargarBodegas(id_solicitud, bd);
            this.printlnResponseAjax(json, "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void gardarDireccion() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void obtenerDireccionEntrega() {
        try {
            String id_orden_compra = request.getParameter("orden_compra");
            String json = dao.obtenerDireccionEntrega(id_orden_compra);
            this.printlnResponseAjax(json, "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void cargarBodegaPrincipal(String bd) {
        try {
            String json = dao.cargarBodegaPrincipal(bd);
            this.printlnResponseAjax(json, "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void cargarInfoBodegaAsignada() {
        try {
            String codigo_solicitud = request.getParameter("cod_solicitud");
            String json = dao.cargarInfoBodegaAsignada(codigo_solicitud);
            this.printlnResponseAjax(json, "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }        
    }

    private void cargarBodegasPorUsuario() {
        try {
            String id_solicitud = request.getParameter("id_solicitud");
            String json = dao.cargarBodegasPorUsuario(id_solicitud, usuario.getBd(), usuario.getLogin());
            this.printlnResponseAjax(json, "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }   
    }

    private void cargarBodegasPorUsuarioSolicitudEjecucion() {
        try {
            String json = dao.cargarBodegasPorUsuarioSolicitudEjecucion(usuario.getLogin());
            this.printlnResponseAjax(json, "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }         
}
