/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.opav.controller;



/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */




import com.tsp.opav.model.beans.AccionContratista;
import com.tsp.opav.model.beans.FacturaRefinanciada;
import com.tsp.opav.model.beans.FinanciacionPM;

import com.tsp.operation.model.beans.SerieGeneral;

import com.tsp.opav.model.threads.HFacturaContratista;
import com.tsp.opav.model.threads.HGenerarFacturaEcaOpav;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.util.LogWriter;
import com.tsp.util.Util;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;



/**
 *
 * @author Alvaro
 */
public class ConsorcioAccesoAction  extends Action {



    String     rutaInformes;



    public ConsorcioAccesoAction() {
    }




    /*
     * Modificaciones :
     * 2010-06-14 Toma en cuenta si el contratista es gran contribuyente para dejar en 0 a reteica y reteiva
     */


    public void run() throws javax.servlet.ServletException {







        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String distrito = session.getAttribute("Distrito").toString();

        String evento = ( request.getParameter("evento")!= null) ? request.getParameter("evento") : "";
        String next  = "";
        String msj   = "";
        String aceptarDisable = "";

        try {


            
            
            
            
            
            
            
            //  EVENTO  :  ACCIONES_SIN_PREFACTURAR_CONTRATISTA de prefacturaContratista.jsp


            String  NUMERO_ANOS_IMPUESTOS = model.constanteService.getValor("FINV", "NUMERO_ANOS_IMPUESTOS", "");

            
            

            // 2012-03-14
            // ---------------------------------------------------------------------------------------------------------------------------------------------
            // DEFINICION DEL LOG DE ERROR
            LogWriter logWriter = new LogWriter("com/tsp/util/connectionpool/db", usuario.getLogin(), "/Consorcio Causacion ", usuario.getLogin() );
            // FIN DEFINICION DEL LOG DE ERROR
            // ---------------------------------------------------------------------------------------------------------------------------------------------

                        
            
            
            
            
            
            
            

            
            // 2012-03-14
            /* *********************************************************************************************************************************************/
            /* INICIO REFINANCIACION */ 
            /* *********************************************************************************************************************************************/



            //  EVENTO  :  REFINANCIAR_CLIENTE  de refinanciarCliente.jsp
            //  Localiza las facturas PM a refinanciar de un cliente
            if (evento.equalsIgnoreCase("REFINANCIAR_CLIENTE")) {   // 20101115

                String idCliente = ( request.getParameter("idCliente")!= null) ? request.getParameter("idCliente") : "";
                String idMultiservicio = ( request.getParameter("idMultiservicio")!= null) ? request.getParameter("idMultiservicio") : "";
                
                boolean error = false;
                
                if   ( idCliente.isEmpty() && idMultiservicio.isEmpty()  ) {
                    msj = "Debe especificar un codigo de cliente o un numero de multiservicio";
                    error = true;
                }
                else if (  (!idCliente.isEmpty() && !idMultiservicio.isEmpty() )    ) {
                    msj = "No puede especificar los dos filtros al mismo tiempo";
                    error = true;
                        
                }                    
                    
                


                if (!error) {
                
                    List listaFacturaCliente =  new LinkedList();
                    
                    if(!idCliente.isEmpty() ) {
                        model.consorcioService.buscaFacturaCliente(idCliente);
                    }
                    else if (!idMultiservicio.isEmpty()) {
                        model.consorcioService.buscaFacturaClienteMS(idMultiservicio);
                        
                    }

                    listaFacturaCliente = model.consorcioService.getFacturaCliente();


                    model.consorcioService.inicializarFinanciacionPM();
                    FinanciacionPM financiacionPM = model.consorcioService.getFinanciacionPM();

                  
                    financiacionPM.setCodcli(idCliente);
                
                    Calendar calendario = GregorianCalendar.getInstance();
                    Date fecha = calendario.getTime();

                    SimpleDateFormat formatoAno = new SimpleDateFormat("yyyy");
                    SimpleDateFormat formatoMes = new SimpleDateFormat("MM");

                    String ano = formatoAno.format(fecha);
                    String mes = formatoMes.format(fecha);
                   
                    double tasaMaxima = model.consorcioService.getTasaMaxima(ano+mes);    
                    
                    financiacionPM.setTasaMaxima(tasaMaxima);

                    
                    if(listaFacturaCliente.size()!=0) {
                        
                        FacturaRefinanciada factura = (FacturaRefinanciada) listaFacturaCliente.get(1);
                        idCliente =  factura.getId_cliente();
                        financiacionPM.setCodcli(idCliente);
                        String nombreCliente = model.consorcioService.getNombreCliente(idCliente);
                        financiacionPM.setNombreCliente(nombreCliente);


                        // PASO 2. Calculo de los valores a financiar cuando hay abonos a la factura


                        for(int i=0;i<listaFacturaCliente.size();i++) {

                            factura = (FacturaRefinanciada) listaFacturaCliente.get(i);
                      
                            String documentoNM = "NM"+ factura.getDocumento().substring(2);
                            double valorIvaNm = model.consorcioService.getIvaNM(documentoNM);
                            factura.setIvaNM(valorIvaNm);
                            
                            

                            double vlrCapital   = factura.getValorCapital();
                            double vlrIntereses = factura.getValorIntereses();
                            double vlrAbono     = factura.getValorAbono();

                            if ( (vlrAbono > 0)  && (vlrIntereses + vlrCapital) > 0 ) {
                                // Si hay dinero de abonos se aplica inicialmente para cancelar los intereses
                                if (vlrAbono >= vlrIntereses ) {
                                    vlrAbono = vlrAbono - vlrIntereses;
                                    vlrIntereses = 0.00;
                                }
                                else {
                                    vlrIntereses = vlrIntereses - vlrAbono;
                                    vlrAbono = 0.00;
                                }
                                // Si sobra dinero de abonos se aplica sobre el capital
                                if(vlrAbono >= vlrCapital) {
                                    vlrAbono = vlrAbono - vlrCapital;
                                    vlrCapital = 0.00;
                                }
                                else {
                                    vlrCapital = vlrCapital - vlrAbono;
                                    vlrAbono = 0.00;
                                }
                            }


                            factura.setValorRefinanciar(vlrCapital);            // Saldo Capital
                            factura.setValorInteresesPendientes(vlrIntereses);
                            
                            
                            if (factura.getValorAbono() != 0.00 ) {
                            
                                if (factura.getValorFactura()  != 0) {
                                    factura.setIvaNMFinal(  Util.redondear2 ( vlrCapital*valorIvaNm/  ( factura.getValorFactura()  )  , 0 )   );
                                }
                                else{
                                    factura.setIvaNMFinal(0.00);
                                }
                            }
                            else {
                               factura.setIvaNMFinal(valorIvaNm);
                                
                            }
                             
                            
                            
                        }

                        next  = "/jsp/consorcio/refinanciarLista.jsp?msj=&codigoCliente="+ idCliente + "&nombreCliente=" + nombreCliente;
                        msj   = "";
                    }
                    else {
                        msj   = "No existe la factura PM";
                        next  = "/jsp/consorcio/refinanciarLista.jsp?msj=";
                    }

                }
                else {
                        
                        next  = "/jsp/consorcio/refinanciarCliente.jsp?msj=";
                    
                }
                
                
                
                
                
            } // Fin de REFINANCIAR_CLIENTE

            


            //  EVENTO  :  REFINANCIAR_CALCULO de refinanciarlista.jsp
            //  acepta los parametros para recalcular la refinacion
            
            if (evento.equalsIgnoreCase("REFINANCIAR_CALCULO")) {   // 20101115
                
                List listaFacturaCliente =  new LinkedList();
                listaFacturaCliente = model.consorcioService.getFacturaCliente();
                
                
                FinanciacionPM financiacionPM = model.consorcioService.getFinanciacionPM();
                
                String idCliente = request.getParameter("idCliente");
                String nombreCliente = model.consorcioService.getNombreCliente(idCliente);
                
                String mensaje = "";
                boolean validacionCorrecta = true;
                String mostrarLiquidacion = "NO" ;
                
                
                // Paso 1. Validacion de que los parametros hayan sido registrados
                
                
                String [] chkSeleccion  =  request.getParameterValues("chkSeleccion");
 
                
                if(chkSeleccion!=null)  {

                    int porSeleccionar = 0;
                    int totalSeleccion = chkSeleccion.length;
                    for(int k=0; k<totalSeleccion ; k++) {
                        if(chkSeleccion[k].equalsIgnoreCase("ON")) {
                            porSeleccionar++;
                        }
                    }
                    if(porSeleccionar <= 0) {
                        validacionCorrecta = false;
                        mensaje = "No han sido seleccionadas facturas para refinanciar.   ";
                    }
                }
                else {
                    validacionCorrecta = false;
                    mensaje = "No han sido seleccionadas facturas para refinanciar.   ";
                }


                
                String cuotas =  request.getParameter("numeroCuota") ;
                if (cuotas.equals("")) {
                    validacionCorrecta = false;
                    cuotas = "0";
                }
                String interes = request.getParameter("tasaInteres");
                if (interes.equals("")) {
                    validacionCorrecta = false;
                    interes = "0.00";
                }                
                String mora = request.getParameter("tasaMora");
                if (mora.equals("")) {
                    validacionCorrecta = false;
                    mora = "0.00";
                }   
                String valorCuotaInicial = request.getParameter("cuotaInicial");
                if (valorCuotaInicial.equals("")) {
                    valorCuotaInicial = "0.00";
                }                   

                
                Calendar calendario = GregorianCalendar.getInstance();
                Date fecha = calendario.getTime();
                SimpleDateFormat formatoDeFecha = new SimpleDateFormat("yyyy-MM-dd");
                String fechaFinanciacion = request.getParameter("fechaFinanciacion");
                if (fechaFinanciacion.equals("")) {
                    fechaFinanciacion = formatoDeFecha.format(fecha);
                }  
                
                
                
                String fechaVencimientoFactura = request.getParameter("fechaVencimiento");
                if (fechaVencimientoFactura.equals("")) {
                    fechaVencimientoFactura = formatoDeFecha.format(fecha);
                }
                

                
                int numeroCuotas    = Integer.parseInt(cuotas);
                double tasaInteres  = Double.parseDouble(interes);
                double tasaMora     = Double.parseDouble(mora);
                double cuotaInicial = Double.parseDouble(valorCuotaInicial);
                


                if(numeroCuotas < 2) {
                    validacionCorrecta = false;
                    mensaje += "El numero de cuotas no puede ser menor a 2.  ";
                }
                if(tasaInteres <= 0) {
                    validacionCorrecta = false;
                    mensaje += "La tasa de interes no puede ser menor o igual a cero.  ";
                }
                if(tasaMora < 0) {
                    validacionCorrecta = false;
                    mensaje += "La tasa de mora no puede ser menor a cero.  ";
                }                   
                
  
                double  vlrTotalRefinanciar = 0.00;
                double  vlrTotalInteresesPendientes = 0.00;
                double  vlrTotalMora = 0.00;                
                

                double  interesPorCuota =  0.00 ;
                double  capitalPorCuota =  0.00 ;
                double  vlrInteresesPendientesPorCuota = 0.00 ;



                double  interesPorCuotaUltimaFactura = 0.00 ;
                double  capitalPorCuotaUltimaFactura = 0.00 ;
                double  vlrInteresesPendientesPorCuotaUltimaFactura = 0.00 ;                

                
                
                
                 if (validacionCorrecta) {
                    
                    SerieGeneral serie = null;
                    serie = new SerieGeneral();  

                    serie =  modelOperation.serieGeneralService.getSerie("FINV", "OP", "FACRM");

                    
                    
                    String documento = serie.getUltimo_prefijo_numero();
                    String numeracion = serie.getUltimo_numero_cadena();
                    
                    
                            
                            
                    financiacionPM.setDocumento(documento);
                    financiacionPM.setNumeracion(numeracion);
                    
                    
                    financiacionPM.removeListaFacturasPM();                         // Limpia la lista de las PM que fueron seleccionadas
                    financiacionPM.removeListaMultiservicio();
                    financiacionPM.removeListaSimboloVariable();
                    financiacionPM.removeListaValorCapital();
                    financiacionPM.removeListaValorInteres();
                    financiacionPM.removeListaValorIPM();
                    financiacionPM.removeListaPorcentajePM();                    
                    financiacionPM.removeListaDiasAdicionales();
                    financiacionPM.removeListaValoresAdicionales();
                    
                    
                   
                    financiacionPM.setNumeroCuotas(numeroCuotas);
                    financiacionPM.setTasaInteres(tasaInteres);
                    financiacionPM.setTasaMora(tasaMora);
                    financiacionPM.setCuotaInicial(cuotaInicial);
                    
                    financiacionPM.setFechaFactura(fechaFinanciacion);
                    financiacionPM.setFechaVencimiento(fechaVencimientoFactura);
                    
                    
                    boolean asignado = false;
                    String simboloAnterior = "";
                    String simbolo = "";
                    String multiservicioAnterior = "";
                    String multiservicio = "";
                    
                    String descripcion = "Refinanciacion de las facturas: ";
                    
                    double valorIva = 0.00;
                    double vlrTotalAdicional = 0.00;
                    double vlrDiferenciaIntereses = 0.00;
                    
                    for(int i=0;i<listaFacturaCliente.size();i++) {
                        
                        if(chkSeleccion[i].equalsIgnoreCase("ON")) {

                            FacturaRefinanciada factura = (FacturaRefinanciada) listaFacturaCliente.get(i);
                            
                            factura.setSeleccionada(true);
                            
                            valorIva = valorIva + factura.getIvaNMFinal();
                            
                            if (asignado == false) {
                                financiacionPM.setNit(factura.getNit());
                                asignado = true;
                            }
                            
                            financiacionPM.setFacturasPM(factura.getDocumento());                       // Guarda todas la PM que fueron seleccionadas
                            financiacionPM.setValorCapital(factura.getValorRefinanciar() ) ;            // Saldo capital 
                            financiacionPM.setValorInteres(factura.getValorInteresesPendientes());      // Valor de intereses pendientes
                            
                            // factura.getValorAdicional();
                            
                            descripcion = descripcion + " " + factura.getDocumento();
                            
                            
                            if (!factura.getSimboloVariable().equals(simboloAnterior) ) {
                                simbolo = simbolo + " " + factura.getSimboloVariable();
                                simboloAnterior =  factura.getSimboloVariable();
                            }
                            
                            if (!factura.getMultiservicio().equals(multiservicioAnterior) ) {
                                multiservicio = multiservicio + " " + factura.getMultiservicio();
                                multiservicioAnterior =  factura.getMultiservicio();
                            }
                            
                            
                            
                            
                            financiacionPM.setMultiservicio(factura.getMultiservicio()) ;
                            financiacionPM.setSimboloVariable(factura.getSimboloVariable() ) ;

                            // PASO 3.  Calculo del caso de dias de mora para cada factura

                            SimpleDateFormat formatoAno = new SimpleDateFormat("yyyy");
                            SimpleDateFormat formatoMes = new SimpleDateFormat("MM");
                            SimpleDateFormat formatoDia = new SimpleDateFormat("dd");
                            SimpleDateFormat formatoFecha = new SimpleDateFormat("yyyy-MM-dd");


                            String ano = formatoAno.format(formatoFecha.parse(factura.getFechaVencimiento() )   );
                            String mes = formatoMes.format(formatoFecha.parse(factura.getFechaVencimiento() )   );
                            String dia = formatoDia.format(formatoFecha.parse(factura.getFechaVencimiento() )   );

                            Calendar calendarFechaVencimiento = Calendar.getInstance();
                            calendarFechaVencimiento.set(Integer.parseInt(ano),  Integer.parseInt(mes)-1,  Integer.parseInt(dia) );  // Fecha actual


                            String anoFinanciacion = fechaFinanciacion.substring(0,4);
                            String mesFinanciacion = fechaFinanciacion.substring(5,7);
                            String diaFinanciacion = fechaFinanciacion.substring(8,10);


                            Calendar calendarFechaFinanciacion = Calendar.getInstance();
                            calendarFechaFinanciacion.set(Integer.parseInt(anoFinanciacion),  Integer.parseInt(mesFinanciacion)-1,  Integer.parseInt(diaFinanciacion)  ); 
                            
                            

                            long milisegVencimiento  = calendarFechaVencimiento.getTimeInMillis();
                            long milisegFinanciacion = calendarFechaFinanciacion.getTimeInMillis();

                            long diff = milisegFinanciacion - milisegVencimiento;

                            long diasMora = diff / (24 * 60 * 60 * 1000);  
                            double vlrMora = 0.00;
                            
                            long  diasAdicionales = 0;
                            double valorAdicional = 0.00;
                            
                            
                            if (diasMora > 0 )  {
                                
                                vlrMora =    Util.redondear2( getInteresMora(  factura.getValorRefinanciar() , (tasaMora/100) , diasMora) , 0 ) ;
                                
                                
                                
                            }
                            else {
                                
                                diasMora = 0;
                                
                                // Las facturas no estan vencidas. Ahora debe calcularse el numero de dias entre la fecha de financiacion  y la fecha de la factura recalculada
                                // Si la diferencia de dias es positiva, esos corresponderia a los dias adicionales.
                                
                                // Calculo de dias adicionales
                               
                                String fechaFacturaInicial = factura.getFechaFactura();
                                int    cuota = factura.getCuota();
                                
                                
                                String anoFactura = fechaFacturaInicial.substring(0,4);
                                String mesFactura = fechaFacturaInicial.substring(5,7);
                                String diaFactura = fechaFacturaInicial.substring(8,10);
                                Calendar calendarFechaFactura = Calendar.getInstance();
                                calendarFechaFactura.set(Integer.parseInt(anoFactura),  Integer.parseInt(mesFactura)-1 + (cuota-1),  Integer.parseInt(diaFactura)  ); 
                                long milisegFechaFactura  = calendarFechaFactura.getTimeInMillis();

                                diff = milisegFinanciacion - milisegFechaFactura;
                                diasAdicionales = diff / (24 * 60 * 60 * 1000);   
                                
                                if (diasAdicionales > 0) {
                                    valorAdicional = Util.redondear2(factura.getValorInteresesPendientes() * diasAdicionales / 30  , 0) ;
                                }
                                else {
                                    diasAdicionales = 0;
                                }
             
                            }

                            factura.setDiasAdicionales( (int) diasAdicionales);
                            factura.setValorAdicional(valorAdicional);                            
                            
                            factura.setValorMora(vlrMora);
                            factura.setDiasMora( (int) diasMora);
                            
                            financiacionPM.setDiasAdicionales ( (int) diasAdicionales);
                            financiacionPM.setValoresAdicionales( valorAdicional);
                            
                            vlrTotalAdicional = vlrTotalAdicional + valorAdicional;
                            
                            financiacionPM.setVlrTotalAdicional(financiacionPM.getVlrTotalAdicional() + valorAdicional);
                            
                            Calendar calendarFechaVencimientoNuevaFactura = Calendar.getInstance();
                            calendarFechaVencimientoNuevaFactura.set(Integer.parseInt(anoFinanciacion),  Integer.parseInt(mesFinanciacion),  Integer.parseInt(diaFinanciacion)  ); 
                            
                            Date fechaVencimientoNuevaFactura = calendario.getTime();
                            fechaVencimientoFactura = formatoDeFecha.format(fechaVencimientoNuevaFactura);
   
                            
                            if (diasMora <= 0) {
                                factura.setValorInteresesPendientes(0.00);
                            }
                            
                            vlrTotalInteresesPendientes = vlrTotalInteresesPendientes + factura.getValorInteresesPendientes() ;
                            vlrTotalMora = vlrTotalMora + factura.getValorMora();
                            
                            vlrTotalRefinanciar = vlrTotalRefinanciar + factura.getValorRefinanciar()  + factura.getValorMora() + factura.getValorInteresesPendientes() + factura.getValorAdicional() ;

                            // Calculo de la diferencia de intereses pendientes para generar un item adicional en la nota credito
                            
                            if ( ( factura.getValorInteresesPendientes() + factura.getValorAdicional() ) > 0 ) {
                                vlrDiferenciaIntereses = vlrDiferenciaIntereses + factura.getValorIntereses();
                            }
                            
                            
                            
                            
                            
                        }
                    }
                    
                    financiacionPM.setVlrDiferenciaIntereses(vlrDiferenciaIntereses);
                    
                    
                    
                    descripcion = descripcion + " con SV : " + simbolo + " con MS : " + multiservicio;
                    financiacionPM.setDescripcion(descripcion);
                    
                    
                    // Paso 4.   Calculo del nuevo valor a cancelar
                    

                    
                    // Valor del iva
                    financiacionPM.setValorIva(valorIva);
                    double valorIvaPorCuota = Util.redondear2( valorIva / numeroCuotas ,0);
                    double valorIvaUltimaCuota = valorIva - ( valorIvaPorCuota * (numeroCuotas - 1)  );
                    financiacionPM.setValorIvaPorCuota(valorIvaPorCuota);
                    financiacionPM.setValorIvaUltimaCuota(valorIvaUltimaCuota);
                    
                    
                    // Valores adicionales
                    
                    double valorAdicionalCuota = Util.redondear2( financiacionPM.getVlrTotalAdicional()  / numeroCuotas ,0);
                    double valorAdicionalUltimaCuota = financiacionPM.getVlrTotalAdicional() - ( valorAdicionalCuota * (numeroCuotas - 1)  );
                    financiacionPM.setVlrAdicionalCuota(valorAdicionalCuota);
                    financiacionPM.setVlrAdicionalUltimaCuota(valorAdicionalUltimaCuota);
                    
                    
                    vlrTotalRefinanciar = Util.redondear2(vlrTotalRefinanciar - cuotaInicial,0);
                    
                    double  nuevaCuota = Util.redondear2( get_anualidad(vlrTotalRefinanciar ,tasaInteres , numeroCuotas ) , 0);
                    
                    double  totalCuotas= nuevaCuota * numeroCuotas;
                    double  totalIntereses= totalCuotas - vlrTotalRefinanciar;   // Intereses de financiacion por todo el valor refinanciado
                    
                    interesPorCuota =  Util.redondear2( totalIntereses / numeroCuotas ,0);
                    interesPorCuotaUltimaFactura = totalIntereses - interesPorCuota * (numeroCuotas - 1) ;
                    
                    
                    capitalPorCuota =  nuevaCuota - interesPorCuota;
                    vlrInteresesPendientesPorCuota = Util.redondear2(vlrTotalInteresesPendientes / numeroCuotas, 0);
                    
                    
                    
                    
                    capitalPorCuotaUltimaFactura = vlrTotalRefinanciar - capitalPorCuota * (numeroCuotas - 1) ;
                    vlrInteresesPendientesPorCuotaUltimaFactura = vlrTotalInteresesPendientes - vlrInteresesPendientesPorCuota * (numeroCuotas - 1);
                    
                    
                    // Guardando calculos en FinanciacionPM de la refinanciacion para permitir crear luego las facturas
                    
                    financiacionPM.setVlrTotalRefinanciar(vlrTotalRefinanciar);         // Incluye saldo capital, intereses pendientes, valor mora, valor adicional
                    double vlrTotalRefinanciarPorCuota = Util.redondear2(vlrTotalRefinanciar / numeroCuotas, 0);
                    double vlrTotalRefinanciarUltimaCuota = vlrTotalRefinanciar - vlrTotalRefinanciarPorCuota *  (numeroCuotas - 1);
                    financiacionPM.setVlrTotalRefinanciarPorCuota(vlrTotalRefinanciarPorCuota);
                    financiacionPM.setVlrTotalRefinanciarUltimaCuota(vlrTotalRefinanciarUltimaCuota);
                    
                    
                    
                    
                    financiacionPM.setVlrTotalInteresesPendientes(vlrTotalInteresesPendientes);
                    financiacionPM.setVlrTotalMora(vlrTotalMora);
                    
                    double vlrMoraPorCuota = Util.redondear2( vlrTotalMora / numeroCuotas, 0);
                    double vlrMoraUltimaCuota = vlrTotalMora - vlrMoraPorCuota * (numeroCuotas - 1) ;
                    financiacionPM.setVlrMoraPorCuota(vlrMoraPorCuota);
                    financiacionPM.setVlrMoraUltimaCuota(vlrMoraUltimaCuota);
                   
                    
                    financiacionPM.setNuevaCuota(nuevaCuota);
                    financiacionPM.setTotalCuotas(totalCuotas);
                    financiacionPM.setTotal_intereses(totalIntereses);
                    financiacionPM.setInteresPorCuota(interesPorCuota);
                    financiacionPM.setCapitalPorCuota(capitalPorCuota);
                    financiacionPM.setVlrInteresesPendientesPorCuota(vlrInteresesPendientesPorCuota);
                    financiacionPM.setInteresPorCuotaUltimaFactura(interesPorCuotaUltimaFactura);
                    financiacionPM.setCapitalPorCuotaUltimaFactura(capitalPorCuotaUltimaFactura);
                    financiacionPM.setVlrInteresesPendientesPorCuotaUltimaFactura(vlrInteresesPendientesPorCuotaUltimaFactura);
                    
                   
                    
                }
        
                
                if (validacionCorrecta ) {
                    mostrarLiquidacion = "SI" ;
                }
                
                next  = "/jsp/consorcio/refinanciarLista.jsp?msj="+mensaje + "&codigoCliente="+ idCliente + "&nombreCliente=" + nombreCliente; 
                next = next + "&numeroCuota=" + numeroCuotas + "&interes=" + tasaInteres + "&mora=" + tasaMora + "&cuotaInicial=" + cuotaInicial + "&fechaFinanciacion=" + fechaFinanciacion;
                next = next + "&mostrarLiquidacion=" + mostrarLiquidacion;
                next = next + "&capitalPorCuota=" + capitalPorCuota;
                next = next + "&interesPorCuota=" + interesPorCuota;
                next = next + "&vlrInteresesPendientesPorCuota=" + vlrInteresesPendientesPorCuota;
                
                next = next + "&capitalPorCuotaUltimaFactura=" + capitalPorCuotaUltimaFactura;
                next = next + "&interesPorCuotaUltimaFactura=" + interesPorCuotaUltimaFactura;
                next = next + "&vlrInteresesPendientesPorCuotaUltimaFactura=" + vlrInteresesPendientesPorCuotaUltimaFactura;
                next = next + "&vlrTotalMora=" + vlrTotalMora;
                next = next + "&valorFinanciar=" + vlrTotalRefinanciar;
                next = next + "&fechaVencimientoFactura=" + fechaVencimientoFactura;


                
            } // Fin de REFINANCIAR_CALCULO

            
            
            


            //  EVENTO  :  GENERAR_FACTURAS  de refinanciarLista.jsp
            //  Genera las facturas RMxxxxx de Refinanciacion
            if (evento.equalsIgnoreCase("GENERAR_FACTURAS")) {             // 20101115
                
               FinanciacionPM financiacionPM = model.consorcioService.getFinanciacionPM(); 
               
               String mensaje = "";
               boolean validado = true;
               String simboloVariableFactura = request.getParameter("simboloVariableFactura");
               String simboloVariableCuota = request.getParameter("simboloVariableCuota");
               String observacion = request.getParameter("observacion");
                
               if (simboloVariableFactura.isEmpty() )  {
                   validado = false;
                   mensaje = "Debe registrar un simbolo variable para las nuevas facturas  \n";
               }

               if (financiacionPM.getCuotaInicial() != 0.00) {
                   if (simboloVariableCuota.isEmpty()) {
                       validado = false;
                       mensaje = mensaje + "Debe registrar un simbolo variable para la factura de la cuota inicial";
                   }
               }

               if (!validado) {
                    msj   = mensaje;
                    next  = "/jsp/consorcio/refinanciarLista.jsp?msj=";
               }
               
               if (validado) {
               
                    financiacionPM.setSimboloVariableFactura(simboloVariableFactura);
                    financiacionPM.setSimboloVariableCuota(simboloVariableCuota);
                    financiacionPM.setObservaciones(observacion);
                    
                   
                    String  TIPO_DOCUMENTO_FAC          = model.constanteService.getValor("FINV", "TIPO_DOCUMENTO_FAC", "");
                    String  OFICINA_PRINCIPAL = model.constanteService.getValor("FINV", "OFICINA_PRINCIPAL", "");
                    String  MONEDA_LOCAL = model.constanteService.getValor("FINV", "MONEDA_LOCAL", "");
                    String  BASE_LOCAL = model.constanteService.getValor("FINV", "BASE_LOCAL", "");



                    // Para registrar en Constantes


                    String  CUENTA_CAPITAL = model.constanteService.getValor("FINV", "CUENTA_CAPITAL", "");
                    String  CUENTA_INTERES = model.constanteService.getValor("FINV", "CUENTA_INTERES", "");
                    String  CUENTA_INTERES_PENDIENTE = model.constanteService.getValor("FINV", "CUENTA_INTERES_PENDIENTE", "");
                    String  CUENTA_MORA = model.constanteService.getValor("FINV", "CUENTA_MORA", "");
                    String  CUENTA_CUOTA_INICIAL = model.constanteService.getValor("FINV", "CUENTA_CUOTA_INICIAL", "");
                    String  HANDLE_CODE_REFINANCIACION = model.constanteService.getValor("FINV", "HANDLE_CODE_REFINANCIACION", ""); 
                    String  CUENTA_IVA = model.constanteService.getValor("FINV", "CUENTA_IVA", "");



                    String  CUENTA_CAPITAL_INGRESO = model.constanteService.getValor("FINV", "CUENTA_CAPITAL_INGRESO", "");
                    String  CUENTA_INTERES_PENDIENTE_INGRESO = model.constanteService.getValor("FINV", "CUENTA_INTERES_PENDIENTE_INGRESO", "");
                    String  HANDLE_CODE_NC_REFINANCIACION = model.constanteService.getValor("FINV", "HANDLE_CODE_NC_REFINANCIACION", ""); 
                    String  CUENTA_CABECERA_INGRESO_REFINANCIACION = model.constanteService.getValor("FINV", "CUENTA_CABECERA_INGRESO_REFINANCIACION", "");
                    String  CUENTA_INTERES_IPM = model.constanteService.getValor("FINV", "CUENTA_INTERES_IPM", "");
                    String  CUENTA_DIFERENCIA_INTERES = model.constanteService.getValor("FINV", "CUENTA_DIFERENCIA_INTERES", "");



                    Vector comandos_sql =new Vector();
                    String comandoSQL = "";



                    SerieGeneral serie = null;
                    serie = new SerieGeneral();                    


                    serie = modelOperation.serieGeneralService.getSerie("FINV","OP","FACRM");

                    modelOperation.serieGeneralService.setSerie("FINV","OP","FACRM");

                    String documento = serie.getUltimo_prefijo_numero();
                    String numeracion = serie.getUltimo_numero_cadena();





                    financiacionPM.setDocumento(documento);
                    financiacionPM.setNumeracion(numeracion);                


                    // Generar las primeras n-1 facturas

                    SimpleDateFormat formatoFecha = new SimpleDateFormat("yyyy-MM-dd");

                    Calendar calendarFechaHoy = Calendar.getInstance();


                    Date dateFechaHoy = calendarFechaHoy.getTime();
                    SimpleDateFormat formatoFechaCompleta = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    String fechaHoy = formatoFechaCompleta.format(dateFechaHoy);

                    int numeroCuotas = financiacionPM.getNumeroCuotas();


                    String fechaFactura = financiacionPM.getFechaFactura();

                    String ano = fechaFactura.substring(0, 4);
                    String mes = fechaFactura.substring(5, 7);
                    String dia = fechaFactura.substring(8, 10);                



                    String fechaInicialVencimiento = financiacionPM.getFechaVencimiento();
                    String anoVencimiento = fechaInicialVencimiento.substring(0, 4);
                    String mesVencimiento = fechaInicialVencimiento.substring(5, 7);
                    String diaVencimiento = fechaInicialVencimiento.substring(8, 10);                 



                    Calendar calendarFechaVencimiento = Calendar.getInstance();



                    // Valor de los items de las facturas exceptuando la ultima cuota

                    double itemCapital = financiacionPM.getCapitalPorCuota()  - financiacionPM.getVlrInteresesPendientesPorCuota() - financiacionPM.getValorIvaPorCuota() -
                                                                                financiacionPM.getVlrMoraPorCuota()  -  financiacionPM.getVlrAdicionalCuota () ;
                    double itemInteresesPendientes = financiacionPM.getVlrInteresesPendientesPorCuota();
                    double itemIva = financiacionPM.getValorIvaPorCuota();
                    double itemMora = financiacionPM.getVlrMoraPorCuota();
                    double itemAdicional = financiacionPM.getVlrAdicionalCuota();
                    double itemInteresesFinanciacion = financiacionPM.getInteresPorCuota();


                    // Calculo de la diferencia de intereses pendientes

                    double diferenciaInteresPendiente = financiacionPM.getVlrDiferenciaIntereses() - (financiacionPM.getVlrTotalInteresesPendientes() + financiacionPM.getVlrTotalAdicional() ) ;

                    if ( diferenciaInteresPendiente > 0 ) {
                        diferenciaInteresPendiente = - diferenciaInteresPendiente; 
                    }               



                        double valorFactura =  itemCapital + itemInteresesPendientes + itemIva + itemMora + itemAdicional + itemInteresesFinanciacion + diferenciaInteresPendiente  ;

                        // Valor de los items de la ultima factura

                        double itemCapitalUltimaCuota = financiacionPM.getCapitalPorCuotaUltimaFactura()  - financiacionPM.getVlrInteresesPendientesPorCuotaUltimaFactura() - financiacionPM.getValorIvaUltimaCuota() -
                                                                                    financiacionPM.getVlrMoraUltimaCuota()  - financiacionPM.getVlrAdicionalUltimaCuota() ;
                        double itemInteresesPendientesUltimaCuota = financiacionPM.getVlrInteresesPendientesPorCuotaUltimaFactura();
                        double itemIvaUltimaCuota = financiacionPM.getValorIvaUltimaCuota();
                        double itemMoraUltimaCuota = financiacionPM.getVlrMoraUltimaCuota();
                        double itemAdicionalUltimaCuota = financiacionPM.getVlrAdicionalUltimaCuota() ;
                        double itemInteresesFinanciacionUltimaCuota = financiacionPM.getInteresPorCuotaUltimaFactura();

                        double valorFacturaUltimaCuota =  itemCapitalUltimaCuota  + itemInteresesPendientesUltimaCuota + itemIvaUltimaCuota + itemMoraUltimaCuota + itemAdicionalUltimaCuota + itemInteresesFinanciacionUltimaCuota;



                        for (int i= 1; i <= numeroCuotas - 1; i++) {

                            calendarFechaVencimiento.set(Integer.parseInt(anoVencimiento),  Integer.parseInt(mesVencimiento)-2 + i ,  Integer.parseInt(diaVencimiento)  );

                            Date fecha = calendarFechaVencimiento.getTime();                            
                            String fechaVencimiento = formatoFecha.format(fecha);

                            int cantidadItem = 0;
                            if (financiacionPM.getCapitalPorCuota() > 0) {
                                cantidadItem = cantidadItem +1 ;
                            }
                            if (financiacionPM.getInteresPorCuota() > 0) {
                                cantidadItem = cantidadItem +1 ;
                            }
                            if (financiacionPM.getVlrInteresesPendientesPorCuota() > 0) {
                                cantidadItem = cantidadItem +1 ;
                            }                    
                            if (diferenciaInteresPendiente != 0) {
                                cantidadItem = cantidadItem +1 ;
                            }                      
                            comandoSQL = model.consorcioService.setFacturaCxC(distrito, TIPO_DOCUMENTO_FAC, financiacionPM.getDocumento()+"_"+i, 
                                                            financiacionPM.getNit(), financiacionPM.getCodcli(), "", 
                                                            financiacionPM.getFechaFactura(),  fechaVencimiento, 
                                                            "0099-01-01", financiacionPM.getDescripcion(), financiacionPM.getObservaciones(),
                                                            valorFactura, 0.00, valorFactura,
                                                            valorFactura, 0.00, valorFactura,
                                                            1.00, MONEDA_LOCAL, 
                                                            cantidadItem, "CREDITO", OFICINA_PRINCIPAL, 
                                                            OFICINA_PRINCIPAL, "", BASE_LOCAL, fechaHoy, usuario.getLogin(), 
                                                            fechaHoy, usuario.getLogin(), HANDLE_CODE_REFINANCIACION, 
                                                            "", OFICINA_PRINCIPAL,
                                                            "","","SV",financiacionPM.getSimboloVariableFactura(),
                                                            "","",
                                                            "","",
                                                            "","") ;
                            comandos_sql.add(comandoSQL);

                            int item = 0;


                            if (itemCapital > 1) {
                                item++;
                                comandoSQL = model.consorcioService.setFacturaDetalleCxC(distrito, TIPO_DOCUMENTO_FAC,financiacionPM.getDocumento()+"_"+i,
                                                                        item, financiacionPM.getNit(), "",
                                                                        financiacionPM.getDescripcion(), CUENTA_CAPITAL,
                                                                        1.00, itemCapital , itemCapital , itemCapital,
                                                                        itemCapital , 1.00 , MONEDA_LOCAL,
                                                                        fechaHoy, usuario.getLogin(), fechaHoy,
                                                                        usuario.getLogin(), BASE_LOCAL, "",
                                                                        "","",
                                                                        "","",
                                                                        "","") ;
                                comandos_sql.add(comandoSQL);
                            }


                            if ( (itemInteresesPendientes + itemAdicional) > 1) {
                                item++;
                                comandoSQL = model.consorcioService.setFacturaDetalleCxC(distrito, TIPO_DOCUMENTO_FAC, financiacionPM.getDocumento()+"_"+i,
                                                                        item, financiacionPM.getNit(), "",
                                                                        financiacionPM.getDescripcion(), CUENTA_INTERES_PENDIENTE,
                                                                        1.00, itemInteresesPendientes + itemAdicional , itemInteresesPendientes + itemAdicional , itemInteresesPendientes + itemAdicional,
                                                                        itemInteresesPendientes + itemAdicional , 1.00 , MONEDA_LOCAL,
                                                                        fechaHoy, usuario.getLogin(), fechaHoy,
                                                                        usuario.getLogin(), BASE_LOCAL, "",
                                                                        "","",
                                                                        "","",
                                                                        "","") ;
                                comandos_sql.add(comandoSQL);
                            }
                            if (itemIva > 1) {
                                item++;
                                comandoSQL = model.consorcioService.setFacturaDetalleCxC(distrito, TIPO_DOCUMENTO_FAC, financiacionPM.getDocumento()+"_"+i,
                                                                        item, financiacionPM.getNit(), "",
                                                                        financiacionPM.getDescripcion(), CUENTA_IVA,
                                                                        1.00, itemIva , itemIva , itemIva,
                                                                        itemIva , 1.00 , MONEDA_LOCAL,
                                                                        fechaHoy, usuario.getLogin(), fechaHoy,
                                                                        usuario.getLogin(), BASE_LOCAL, "",
                                                                        "","",
                                                                        "","",
                                                                        "","") ; 
                                comandos_sql.add(comandoSQL);
                            }                    


                            if (itemMora > 1) {
                                item++;
                                comandoSQL = model.consorcioService.setFacturaDetalleCxC(distrito, TIPO_DOCUMENTO_FAC, financiacionPM.getDocumento()+"_"+i,
                                                                        item, financiacionPM.getNit(), "",
                                                                        financiacionPM.getDescripcion(), CUENTA_MORA,
                                                                        1.00, itemMora , itemMora , itemMora,
                                                                        itemMora , 1.00 , MONEDA_LOCAL,
                                                                        fechaHoy, usuario.getLogin(), fechaHoy,
                                                                        usuario.getLogin(), BASE_LOCAL, "",
                                                                        "","",
                                                                        "","",
                                                                        "","") ; 
                                comandos_sql.add(comandoSQL);
                            }                        



                            if (itemInteresesFinanciacion > 1) {
                                item++;
                                comandoSQL = model.consorcioService.setFacturaDetalleCxC(distrito, TIPO_DOCUMENTO_FAC, financiacionPM.getDocumento()+"_"+i,
                                                                        item, financiacionPM.getNit(), "",
                                                                        financiacionPM.getDescripcion(), CUENTA_INTERES,
                                                                        1.00, itemInteresesFinanciacion , itemInteresesFinanciacion , itemInteresesFinanciacion,
                                                                        itemInteresesFinanciacion , 1.00 , MONEDA_LOCAL,
                                                                        fechaHoy, usuario.getLogin(), fechaHoy,
                                                                        usuario.getLogin(), BASE_LOCAL, "",
                                                                        "","",
                                                                        "","",
                                                                        "","") ; 
                                comandos_sql.add(comandoSQL);
                            }                     





                       }






                    // Generar la ultima facturas


                        calendarFechaVencimiento.set(Integer.parseInt(ano),  Integer.parseInt(mes)-1 + numeroCuotas ,  Integer.parseInt(dia)  );

                        Date fecha = calendarFechaVencimiento.getTime();                            
                        String fechaVencimiento = formatoFecha.format(fecha);

                        int cantidadItem = 0;
                        if (financiacionPM.getCapitalPorCuota() > 0) {
                            cantidadItem = cantidadItem +1 ;
                        }
                        if (financiacionPM.getInteresPorCuota() > 0) {
                            cantidadItem = cantidadItem +1 ;
                        }
                        if (financiacionPM.getVlrInteresesPendientesPorCuota() > 0) {
                            cantidadItem = cantidadItem +1 ;
                        }                    


                        comandoSQL = model.consorcioService.setFacturaCxC(distrito, TIPO_DOCUMENTO_FAC, financiacionPM.getDocumento()+"_"+numeroCuotas, 
                                                        financiacionPM.getNit(), financiacionPM.getCodcli(), "", 
                                                        financiacionPM.getFechaFactura(),  fechaVencimiento, 
                                                        "0099-01-01", financiacionPM.getDescripcion(), financiacionPM.getObservaciones(),
                                                        valorFacturaUltimaCuota, 0.00, valorFacturaUltimaCuota,
                                                        valorFacturaUltimaCuota, 0.00, valorFacturaUltimaCuota,
                                                        1.00, MONEDA_LOCAL, 
                                                        cantidadItem, "CREDITO", OFICINA_PRINCIPAL, 
                                                        OFICINA_PRINCIPAL, "", BASE_LOCAL, fechaHoy, usuario.getLogin(), 
                                                        fechaHoy, usuario.getLogin(), "RM", 
                                                        "", OFICINA_PRINCIPAL,
                                                        "","","SV",financiacionPM.getSimboloVariableFactura(),
                                                        "","",
                                                        "","",
                                                        "","") ;
                        comandos_sql.add(comandoSQL);

                        int item = 0;





                        if (itemCapitalUltimaCuota > 1) {
                            item++;
                            comandoSQL = model.consorcioService.setFacturaDetalleCxC(distrito, TIPO_DOCUMENTO_FAC, financiacionPM.getDocumento()+"_"+numeroCuotas, 
                                                                    item, financiacionPM.getNit(), "",
                                                                    financiacionPM.getDescripcion(), CUENTA_CAPITAL,
                                                                    1.00, itemCapitalUltimaCuota , itemCapitalUltimaCuota , itemCapitalUltimaCuota,
                                                                    itemCapitalUltimaCuota , 1.00 , MONEDA_LOCAL,
                                                                    fechaHoy, usuario.getLogin(), fechaHoy,
                                                                    usuario.getLogin(), BASE_LOCAL, "",
                                                                    "","",
                                                                    "","",
                                                                    "","") ; 
                            comandos_sql.add(comandoSQL);
                        }


                        if ( (itemInteresesPendientesUltimaCuota + itemAdicionalUltimaCuota ) > 1) {
                            item++;
                            comandoSQL = model.consorcioService.setFacturaDetalleCxC(distrito, TIPO_DOCUMENTO_FAC, financiacionPM.getDocumento()+"_"+numeroCuotas,
                                                                    item, financiacionPM.getNit(), "",
                                                                    financiacionPM.getDescripcion(), CUENTA_INTERES_PENDIENTE,
                                                                    1.00, itemInteresesPendientesUltimaCuota + itemAdicionalUltimaCuota , itemInteresesPendientesUltimaCuota + itemAdicionalUltimaCuota , itemInteresesPendientesUltimaCuota + itemAdicionalUltimaCuota,
                                                                    itemInteresesPendientesUltimaCuota + itemAdicionalUltimaCuota , 1.00 , MONEDA_LOCAL,
                                                                    fechaHoy, usuario.getLogin(), fechaHoy,
                                                                    usuario.getLogin(), BASE_LOCAL, "",
                                                                    "","",
                                                                    "","",
                                                                    "","") ; 
                            comandos_sql.add(comandoSQL);
                        }

                        if (itemIvaUltimaCuota > 1) {
                            item++;
                            comandoSQL = model.consorcioService.setFacturaDetalleCxC(distrito, TIPO_DOCUMENTO_FAC, financiacionPM.getDocumento()+"_"+numeroCuotas,
                                                                    item, financiacionPM.getNit(), "",
                                                                    financiacionPM.getDescripcion(), CUENTA_IVA,
                                                                    1.00, itemIvaUltimaCuota , itemIvaUltimaCuota , itemIvaUltimaCuota,
                                                                    itemIvaUltimaCuota , 1.00 , MONEDA_LOCAL,
                                                                    fechaHoy, usuario.getLogin(), fechaHoy,
                                                                    usuario.getLogin(), BASE_LOCAL, "",
                                                                    "","",
                                                                    "","",
                                                                    "","") ; 
                            comandos_sql.add(comandoSQL);

                        }                





                        if (itemMoraUltimaCuota > 1) {
                            item++;
                            comandoSQL = model.consorcioService.setFacturaDetalleCxC(distrito, TIPO_DOCUMENTO_FAC, financiacionPM.getDocumento()+"_"+numeroCuotas,
                                                                    item, financiacionPM.getNit(), "",
                                                                    financiacionPM.getDescripcion(), CUENTA_MORA,
                                                                    1.00, itemMoraUltimaCuota , itemMoraUltimaCuota , itemMoraUltimaCuota,
                                                                    itemMoraUltimaCuota , 1.00 , MONEDA_LOCAL,
                                                                    fechaHoy, usuario.getLogin(), fechaHoy,
                                                                    usuario.getLogin(), BASE_LOCAL, "",
                                                                    "","",
                                                                    "","",
                                                                    "","") ; 
                            comandos_sql.add(comandoSQL);

                        }   


                        if (itemInteresesFinanciacionUltimaCuota > 1) {
                            item++;
                            comandoSQL = model.consorcioService.setFacturaDetalleCxC(distrito, TIPO_DOCUMENTO_FAC, financiacionPM.getDocumento()+"_"+numeroCuotas,
                                                                    item, financiacionPM.getNit(), "",
                                                                    financiacionPM.getDescripcion(), CUENTA_INTERES,
                                                                    1.00, itemInteresesFinanciacionUltimaCuota , itemInteresesFinanciacionUltimaCuota , itemInteresesFinanciacionUltimaCuota,
                                                                    itemInteresesFinanciacionUltimaCuota , 1.00 , MONEDA_LOCAL,
                                                                    fechaHoy, usuario.getLogin(), fechaHoy,
                                                                    usuario.getLogin(), BASE_LOCAL, "",
                                                                    "","",
                                                                    "","",
                                                                    "","") ; 
                            comandos_sql.add(comandoSQL);

                        }                       




                        // Generar la factura de intereses de mora

                        /* Se descarto crear esta factura ya que el valor de la mora fue incluido en los intereses 
                        if (financiacionPM.getVlrTotalMora()>0)    {

                            comandoSQL = model.consorcioService.setFacturaCxC(distrito, TIPO_DOCUMENTO_FAC, "MO"+financiacionPM.getNumeracion()+"_1", 
                                                            financiacionPM.getNit(), financiacionPM.getCodcli(), "", 
                                                            financiacionPM.getFechaFactura(),  financiacionPM.getFechaFactura(), 
                                                            "0099-01-01", "Mora "+financiacionPM.getDescripcion(),
                                                            financiacionPM.getVlrTotalMora(), 0.00, financiacionPM.getVlrTotalMora(),
                                                            financiacionPM.getVlrTotalMora(), 0.00, financiacionPM.getVlrTotalMora(),
                                                            1.00, MONEDA_LOCAL, 
                                                            1, "CREDITO", OFICINA_PRINCIPAL, 
                                                            OFICINA_PRINCIPAL, "", BASE_LOCAL, fechaHoy, usuario.getLogin(), 
                                                            fechaHoy, usuario.getLogin(), "RM", 
                                                            "", OFICINA_PRINCIPAL,
                                                            "","","","",
                                                            "","",
                                                            "","",
                                                            "","") ;
                            comandos_sql.add(comandoSQL);


                            comandoSQL = model.consorcioService.setFacturaDetalleCxC(distrito, TIPO_DOCUMENTO_FAC, "MO"+financiacionPM.getNumeracion()+"_1",
                                                                    1, financiacionPM.getNit(), "",
                                                                    "Mora "+financiacionPM.getDescripcion(), CUENTA_MORA,
                                                                    1.00, financiacionPM.getVlrTotalMora() , financiacionPM.getVlrTotalMora() , financiacionPM.getVlrTotalMora(),
                                                                    financiacionPM.getVlrTotalMora() , 1.00 , MONEDA_LOCAL,
                                                                    fechaHoy, usuario.getLogin(), fechaHoy,
                                                                    usuario.getLogin(), BASE_LOCAL, "",
                                                                    "","",
                                                                    "","",
                                                                    "","") ; 
                            comandos_sql.add(comandoSQL);
                        }                    
                        */



                    // Generar la factura de cuota inicial



                        if (financiacionPM.getCuotaInicial() > 0)    {

                            comandoSQL = model.consorcioService.setFacturaCxC(distrito, TIPO_DOCUMENTO_FAC, "CU" + financiacionPM.getNumeracion()+"_1",
                                                            financiacionPM.getNit(), financiacionPM.getCodcli(), "", 
                                                            financiacionPM.getFechaFactura(),  financiacionPM.getFechaFactura(), 
                                                            "0099-01-01", "Cuota Inicial "+financiacionPM.getDescripcion(), financiacionPM.getObservaciones(),
                                                            financiacionPM.getCuotaInicial(), 0.00, financiacionPM.getCuotaInicial(),
                                                            financiacionPM.getCuotaInicial(), 0.00, financiacionPM.getCuotaInicial(),
                                                            1.00, MONEDA_LOCAL, 
                                                            1, "CREDITO", OFICINA_PRINCIPAL, 
                                                            OFICINA_PRINCIPAL, "", BASE_LOCAL, fechaHoy, usuario.getLogin(), 
                                                            fechaHoy, usuario.getLogin(), "RM", 
                                                            "", OFICINA_PRINCIPAL,
                                                            "","","SV",financiacionPM.getSimboloVariableCuota(),
                                                            "","",
                                                            "","",
                                                            "","") ;
                            comandos_sql.add(comandoSQL);


                            comandoSQL = model.consorcioService.setFacturaDetalleCxC(distrito, TIPO_DOCUMENTO_FAC, "CU" + financiacionPM.getNumeracion()+"_1",
                                                                    1, financiacionPM.getNit(), "",
                                                                    "Cuota Inicial " + financiacionPM.getDescripcion(), CUENTA_CUOTA_INICIAL,
                                                                    1.00, financiacionPM.getCuotaInicial() , financiacionPM.getCuotaInicial() , financiacionPM.getCuotaInicial(),
                                                                    financiacionPM.getCuotaInicial() , 1.00 , MONEDA_LOCAL,
                                                                    fechaHoy, usuario.getLogin(), fechaHoy,
                                                                    usuario.getLogin(), BASE_LOCAL, "",
                                                                    "","",
                                                                    "","",
                                                                    "","") ; 
                            comandos_sql.add(comandoSQL);
                        }                    



                    // Generar las notas credito para cancelar las PM que se refinanciaron



                    // Calculo de porcentaje del valor de los intereses pendientes de cada factura con respecto al total de intereses pendientes de todas las facturas PM seleccionadas
                    // El porcentaje sirve luego para calcular el valor proporcional de las IPM que se deben asociar a cada factura.

                    double porcentajePM = 0.00;

                    double totalInteresesPendientes = financiacionPM.getVlrTotalInteresesPendientes();
                    List listaValorInteres          =  financiacionPM.getValorInteres();
                    List listaValoresAdicionales    = financiacionPM.getValoresAdicionales();

                    for (int j=0; j< listaValorInteres.size();j++){
                        double valorInteres   = (Double) listaValorInteres.get(j);
                        double valorAdicional = (Double) listaValoresAdicionales.get(j);
                        if (totalInteresesPendientes != 0) {
                            porcentajePM = (valorInteres + valorAdicional)  /totalInteresesPendientes;
                        }
                        financiacionPM.setPorcentajePM(porcentajePM);
                    }


                    List listaFacturaPM = financiacionPM.getFacturasPM();           
                    double totalValorIPM = 0.00;
                    String facturasIPM = "";

                    for (int j=0; j< listaFacturaPM.size();j++){
                        String facturaPM = (String) listaFacturaPM.get(j);
                        facturasIPM = facturasIPM + "I"+facturaPM + " ";

                        double valorIPM =  model.consorcioService.getValorIPM(  "I"+facturaPM );
                        totalValorIPM = totalValorIPM + valorIPM;
                        financiacionPM.setValorIPM(valorIPM);
                    }



                    List listaValorCapital    = financiacionPM.getValorCapital();
                    listaValorInteres         = financiacionPM.getValorInteres();
                    List listaPorcentajePM    = financiacionPM.getPorcentajePM();

                    List listaMultiservicio   = financiacionPM.getMultiservicio();
                    List listaSimboloVariable = financiacionPM.getSimboloVariable();

                    double valorCuotaIPM = 0.00;
                    double totalValoresCuotaIPM = 0.00;

                    SerieGeneral serieIngreso = new SerieGeneral();                    
                    serieIngreso = modelOperation.serieGeneralService.getSerie("FINV","OP","ICAC");
                    modelOperation.serieGeneralService.setSerie("FINV","OP","ICAC");
                    String num_ingreso = serieIngreso.getUltimo_prefijo_numero();






                    item = 0;



                    for (int i= 0; i < listaFacturaPM.size(); i++) {

                        String facturaPM = (String) listaFacturaPM.get(i);
                        double valorCapital = (Double) listaValorCapital.get(i);
                        double valorInteres = (Double) listaValorInteres.get(i);
                        String multiservicio = (String) listaMultiservicio.get(i);
                        String simboloVariable = (String) listaSimboloVariable.get(i);


                        porcentajePM = (Double) listaPorcentajePM.get(i);


                        comandoSQL = model.consorcioService.crearIngresoDetalle("", distrito, "ICA", num_ingreso, ++item, financiacionPM.getNit(),
                                                                                valorCapital , valorCapital, facturaPM, financiacionPM.getFechaFactura(), "",
                                                                                0.00, 0.00, "FAC", facturaPM, "",
                                                                                0.00, 0.00, 0.00, usuario.getLogin(),
                                                                                fechaHoy, usuario.getLogin(), fechaHoy, BASE_LOCAL, CUENTA_CAPITAL_INGRESO, "",
                                                                                "0099-01-01 00:00:00", "0099-01-01 00:00:00", "",
                                                                                "0099-01-01 00:00:00", "", 0, 0,
                                                                                "Cancelacion capital de : "+ facturaPM, 1.00, 0.00, "", "",
                                                                                "MS", multiservicio, "SV", simboloVariable,
                                                                                "", "", logWriter);
                        comandos_sql.add(comandoSQL);

                        if (valorInteres > 0.00) {

                            valorCuotaIPM = Util.redondear2(porcentajePM * totalValorIPM ,0) ;



                            if (  i < listaFacturaPM.size() )  {

                                totalValoresCuotaIPM = totalValoresCuotaIPM + valorCuotaIPM;

                                    comandoSQL = model.consorcioService.crearIngresoDetalle("", distrito, "ICA", num_ingreso, ++item, financiacionPM.getNit(),
                                                                                        valorInteres - valorCuotaIPM , valorInteres  - valorCuotaIPM , facturaPM, financiacionPM.getFechaFactura(), "",
                                                                                        0.00, 0.00, "FAC", facturaPM, "",
                                                                                        0.00, 0.00, 0.00, usuario.getLogin(),
                                                                                        fechaHoy, usuario.getLogin(), fechaHoy, BASE_LOCAL, model.consorcioService.getCuentaCabeceraPM(facturaPM), "",
                                                                                        "0099-01-01 00:00:00", "0099-01-01 00:00:00", "",
                                                                                        "0099-01-01 00:00:00", "", 0, 0,
                                                                                        "Cancelacion interes de : "+ facturaPM, 1.00, 0.00, "",  "",
                                                                                        "MS", multiservicio, "SV", simboloVariable,
                                                                                        "", "", logWriter);
                                    comandos_sql.add(comandoSQL);


                                    if (valorCuotaIPM > 0.00)  {
                                        comandoSQL = model.consorcioService.crearIngresoDetalle("", distrito, "ICA", num_ingreso, ++item, financiacionPM.getNit(),
                                                                                            valorCuotaIPM , valorCuotaIPM, facturaPM, financiacionPM.getFechaFactura(), "",
                                                                                            0.00, 0.00, "FAC", facturaPM, "",
                                                                                            0.00, 0.00, 0.00, usuario.getLogin(),
                                                                                            fechaHoy, usuario.getLogin(), fechaHoy, BASE_LOCAL, CUENTA_INTERES_IPM, "",
                                                                                            "0099-01-01 00:00:00", "0099-01-01 00:00:00", "",
                                                                                            "0099-01-01 00:00:00", "", 0, 0,
                                                                                            "Valor correspondiente a las IPM " + facturasIPM , 1.00, 0.00, "",  "",
                                                                                            "MS", multiservicio, "SV", simboloVariable,
                                                                                            "", "", logWriter);
                                        comandos_sql.add(comandoSQL); 
                                    }

                            }
                            else {

                                double valorCuotaIPMUltima = totalValorIPM - totalValoresCuotaIPM;

                                    comandoSQL = model.consorcioService.crearIngresoDetalle("", distrito, "ICA", num_ingreso, ++item, financiacionPM.getNit(),
                                                                                        valorInteres - valorCuotaIPMUltima , valorInteres - valorCuotaIPMUltima , facturaPM, financiacionPM.getFechaFactura(), "",
                                                                                        0.00, 0.00, "FAC", facturaPM, "",
                                                                                        0.00, 0.00, 0.00, usuario.getLogin(),
                                                                                        fechaHoy, usuario.getLogin(), fechaHoy, BASE_LOCAL, model.consorcioService.getCuentaCabeceraPM(facturaPM), "",
                                                                                        "0099-01-01 00:00:00", "0099-01-01 00:00:00", "",
                                                                                        "0099-01-01 00:00:00", "", 0, 0,
                                                                                        "Cancelacion interes de : "+ facturaPM, 1.00, 0.00, "",  "",
                                                                                        "MS", multiservicio, "SV", simboloVariable,
                                                                                        "", "", logWriter);
                                    comandos_sql.add(comandoSQL);


                                    if (valorCuotaIPMUltima > 0.00)  {
                                        comandoSQL = model.consorcioService.crearIngresoDetalle("", distrito, "ICA", num_ingreso, ++item, financiacionPM.getNit(),
                                                                                            valorCuotaIPMUltima , valorCuotaIPMUltima, facturaPM, financiacionPM.getFechaFactura(), "",
                                                                                            0.00, 0.00, "FAC", facturaPM, "",
                                                                                            0.00, 0.00, 0.00, usuario.getLogin(),
                                                                                            fechaHoy, usuario.getLogin(), fechaHoy, BASE_LOCAL, CUENTA_INTERES_IPM, "",
                                                                                            "0099-01-01 00:00:00", "0099-01-01 00:00:00", "",
                                                                                            "0099-01-01 00:00:00", "", 0, 0,
                                                                                            "Valor correspondiente a las IPM " + facturasIPM ,  1.00, 0.00, "",  "",
                                                                                            "MS", multiservicio, "SV", simboloVariable,
                                                                                            "", "", logWriter);
                                        comandos_sql.add(comandoSQL);  
                                    }

                            }

                        }


                        // Actualizacion de cada una de las PM abonando con el valor capital y el valor de los intereses pendientes.

                        // Actualizar cabecera

                        comandoSQL = model.consorcioService.actualizarCabeceraCxC(facturaPM, valorCapital + valorInteres );
                        comandos_sql.add(comandoSQL);



                    }


                    // Adicionando el item de diferencia en intereses pendientes



                    if ( diferenciaInteresPendiente != 0 ) {

                                        comandoSQL = model.consorcioService.crearIngresoDetalle("", distrito, "ICA", num_ingreso, ++item, financiacionPM.getNit(),
                                                                                            diferenciaInteresPendiente , diferenciaInteresPendiente, "", financiacionPM.getFechaFactura(), "",
                                                                                            0.00, 0.00, "FAC", "", "",
                                                                                            0.00, 0.00, 0.00, usuario.getLogin(),
                                                                                            fechaHoy, usuario.getLogin(), fechaHoy, BASE_LOCAL, CUENTA_DIFERENCIA_INTERES, "",
                                                                                            "0099-01-01 00:00:00", "0099-01-01 00:00:00", "",
                                                                                            "0099-01-01 00:00:00", "", 0, 0,
                                                                                            "Valor correspondiente a las IPM " + facturasIPM ,  1.00, 0.00, "",  "",
                                                                                            "MS", "", "SV", "",
                                                                                            "", "", logWriter);
                                        comandos_sql.add(comandoSQL);                   


                    }

                    comandoSQL = model.consorcioService.crearIngresoCabecera("", distrito, "ICA", num_ingreso, financiacionPM.getCodcli(), financiacionPM.getNit(),
                                                        "FR","C", financiacionPM.getFechaFactura(), financiacionPM.getFechaFactura(), "",
                                                        "", MONEDA_LOCAL, OFICINA_PRINCIPAL, financiacionPM.getDescripcion(),
                                                        "", financiacionPM.getVlrTotalRefinanciar() + financiacionPM.getVlrTotalInteresesPendientes(), 
                                                        financiacionPM.getVlrTotalRefinanciar() + financiacionPM.getVlrTotalInteresesPendientes(), 1.00, financiacionPM.getFechaFactura(), item,
                                                        0, 0, financiacionPM.getFechaFactura(), "0099-01-01 00:00:00" ,
                                                        "0099-01-01 00:00:00", "0099-01-01 00:00:00", usuario.getLogin(),
                                                        fechaHoy, usuario.getLogin(), fechaHoy, BASE_LOCAL, "",
                                                        "", CUENTA_CABECERA_INGRESO_REFINANCIACION, "", "", 0.00, 0.00,
                                                        HANDLE_CODE_NC_REFINANCIACION, "", "0099-01-01 00:00:00", "", "",
                                                        "", "", "", "", logWriter) ; 

                    comandos_sql.add(comandoSQL);                   


                   // Grabando todo a la base de datos.
                   model.consorcioService.ejecutarBatchSQL(comandos_sql, logWriter);
                   
                   next  = "/jsp/consorcio/refinanciarCliente.jsp?msj=";
                   msj   = "";
                   
                   
                   
               }
                
                

            } // Fin de GENERAR_FACTURAS



            
            


            
            
            
     

            
            /* *********************************************************************************************************************************************/
            /* FINAL REFINANCIACION */ 
            /* *********************************************************************************************************************************************/
            
            
       
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            //  Arma una lista de las acciones que no se han prefacturado para el contratista


            if (evento.equalsIgnoreCase("ACCIONES_SIN_PREFACTURAR_CONTRATISTA")) {

                model.consorcioService.setAccionesContratista(usuario.getNitPropietario(),2);
                List listaAccionesContratista = model.consorcioService.getAccionesContratista();
                String anioImpuesto = request.getParameter("anio_impuesto") == null ? "" : request.getParameter("anio_impuesto");
                if(!listaAccionesContratista.isEmpty()) {
                    model.consorcioService.setContratista(usuario.getNitPropietario());
                    com.tsp.opav.model.beans.Contratista contratista = model.consorcioService.getContratista();

                    int numeroAnos = Integer.parseInt(NUMERO_ANOS_IMPUESTOS);

                    model.consorcioService.buscaImpuesto("FINV", contratista, "IVA",numeroAnos ,anioImpuesto);
                    model.consorcioService.buscaImpuesto("FINV", contratista, "IVA","RIVA",numeroAnos);
                    model.consorcioService.buscaImpuesto("FINV", contratista, "CONAIU","RETEICA", "RETEICA CON AIU", numeroAnos );
                    model.consorcioService.buscaImpuesto("FINV", contratista, "SINAIU", "RETEICA","RETEICA SIN AIU", numeroAnos );
                    model.consorcioService.buscaImpuesto("FINV", contratista, "MATERIAL", "CONAIU" , "RETEFUENTE" , "RETEFUENTE MATERIAL CON AIU", numeroAnos);
                    model.consorcioService.buscaImpuesto("FINV", contratista, "MATERIAL", "SINAIU" , "RETEFUENTE" , "RETEFUENTE MATERIAL SIN AIU", numeroAnos);
                    model.consorcioService.buscaImpuesto("FINV", contratista, "OBRA", "CONAIU" , "RETEFUENTE", "RETEFUENTE MANO DE OBRA CON AIU", numeroAnos);
                    model.consorcioService.buscaImpuesto("FINV", contratista, "OBRA", "SINAIU" , "RETEFUENTE", "RETEFUENTE MANO DE OBRA SIN AIU", numeroAnos);
                    model.consorcioService.buscaImpuesto("FINV", contratista, "OTROS", "CONAIU" , "RETEFUENTE", "RETEFUENTE OTROS CON AIU", numeroAnos);
                    model.consorcioService.buscaImpuesto("FINV", contratista, "OTROS", "SINAIU" , "RETEFUENTE", "RETEFUENTE OTROS SIN AIU", numeroAnos);
                    model.consorcioService.buscaImpuesto("FINV", contratista, "AIU", "CONAIU" , "RETEFUENTE" , "RETEFUENTE CON AIU",numeroAnos);
                    model.consorcioService.buscaImpuesto("FINV", contratista, "AIU", "SINAIU" , "RETEFUENTE", "RETEFUENTE SIN AIU", numeroAnos);

                    next  = "/jsp/consorcio/prefacturaContratistaSeleccionada.jsp?msj=";
                }
                else {
                    msj   = "El contratista no tiene registros disponibles...";
                    next  = "/jsp/consorcio/prefacturaContratista.jsp?msj=";
                }
            } // Fin de ACCIONES_SIN_PREFACTURAR_CONTRATISTA




        // EVENTO : PROCESAR_ACCIONES_CONTRATISTA en  prefacturaContratistaSeleccionada.jsp
        // Procesa las acciones que fueron marcadas para prefacturar al contratista
        if(evento.equals("PROCESAR_ACCIONES_CONTRATISTA")) {

           String[] id_accion   = request.getParameterValues("ckestado");
           String usuario_prefactura_contratista = usuario.getLogin();

           String estado = "";                                                  // Contiene el estado de la actualizacion de la base de datos en lote ""= OK , "ERROR"= Se presento error
           String comandoSQL = "";                                              // Almacena un string SQL
           Vector comandos_sql =new Vector();                                   // Almacena una lista de String SQL que actualizaran la BD en modo transaccion

           int secuencia_prefactura = 0;
           String prefactura_contratista = "";


           if( id_accion!= null){

                try {
                    com.tsp.opav.model.beans.Contratista contratista        = model.consorcioService.getContratista();
                    //aqui vamos hacer un fritanga gracias al cmdapbon
                    model.consorcioService.setAccionesContratista(usuario.getNitPropietario(),1);
                    List listaAccionesContratista  = model.consorcioService.getAccionesContratista();


                    // DEFINICION DE PORCENTAJES DE IMPUESTOS

                    double por_iva_contratista  = 0.00;
                    String cod_iva_contratista  =  "";

                    double por_reteiva_contratista  =  0.00;
                    String cod_reteiva_contratista  =  "";

                    double por_reteica_contratista  = 0.00;
                    String cod_reteica_contratista   = "";

                    double por_ret_material_contratista  = 0.00;
                    String cod_ret_material_contratista  = "";

                    double por_ret_mano_obra_contratista = 0.00;
                    String cod_ret_mano_obra_contratista = "";

                    double por_ret_otros_contratista     = 0.00;
                    String cod_ret_otros_contratista     = "";

                    double por_ret_aiu_contratista     = 0.00;
                    String cod_ret_aiu_contratista     = "";


                    // PROCESAR CADA ACCION SELECCIONADA
                    for(int i=0;i<id_accion.length;i++){

                        //int accion = Integer.parseInt(id_accion[i]);
                        String accion = id_accion[i];

                        double val_iva_contratista   = 0;
                        double val_ret_material_contratista  = 0;
                        double val_ret_mano_obra_contratista = 0;
                        double val_ret_otros_contratista     = 0;
                        double val_ret_aiu_contratista       = 0;
                        double val_reteiva_contratista       = 0;
                        double val_reteica_contratista       = 0;

                        Iterator it = listaAccionesContratista.iterator();
                        boolean localizado = false;

                        // Localizar valores a prefacturar de la accion seleccionada en la lista de acciones del contratista
                        while (it.hasNext() && (localizado == false) ) {

                            AccionContratista accionContratista = (AccionContratista)it.next();
                            //int accion_no =  Integer.parseInt(prefactura.getId_accion()) ;
                            String accion_no =  accionContratista.getId_accion() ;
                            if (accion_no.equals(accion)){
                                // Id_Accion de la lista concuerda con la selecciona en pantalla
                                double material          = accionContratista.getMaterial();
                                double mano_obra         = accionContratista.getMano_obra();
                                double otros             = accionContratista.getOtros();
                                double administracion    = accionContratista.getAdministracion();
                                double imprevisto        = accionContratista.getImprevisto();
                                double utilidad          = accionContratista.getUtilidad();


                                // Determinacion de fecha de la factura del cliente donde se facturo la accion

                                Calendar ahoraCal = Calendar.getInstance();
                                int ano = ahoraCal.get(Calendar.YEAR);
                                String anoVigencia = Integer.toString(ano);

                                String fechaFactura = model.consorcioService.getFechaFactura("FINV", "FAC",  accionContratista.getFactura_cliente() ) ;
                                if(!fechaFactura.isEmpty() && (fechaFactura.length()== 10) ) {
                                    anoVigencia = fechaFactura.substring(0, 4);
                                }

                                // Localizacion del porcentaje de iva

                                por_iva_contratista  = model.consorcioService.getPorcentaje("IVA",anoVigencia);
                                cod_iva_contratista  =  model.consorcioService.getCodigoImpuesto("IVA",anoVigencia);


                                // Calculo de los impuestos de retencion

                                if(utilidad != 0.00){

                                    por_reteiva_contratista  =  model.consorcioService.getPorcentaje("RETEIVA",anoVigencia);
                                    cod_reteiva_contratista  =  model.consorcioService.getCodigoImpuesto("RETEIVA",anoVigencia);

                                    por_reteica_contratista  = model.consorcioService.getPorcentaje("RETEICA CON AIU",anoVigencia);
                                    cod_reteica_contratista   = model.consorcioService.getCodigoImpuesto("RETEICA CON AIU",anoVigencia);

                                    por_ret_material_contratista  = model.consorcioService.getPorcentaje("RETEFUENTE MATERIAL CON AIU",anoVigencia);
                                    cod_ret_material_contratista  = model.consorcioService.getCodigoImpuesto("RETEFUENTE MATERIAL CON AIU",anoVigencia);

                                    por_ret_mano_obra_contratista = model.consorcioService.getPorcentaje("RETEFUENTE MANO DE OBRA CON AIU",anoVigencia);
                                    cod_ret_mano_obra_contratista = model.consorcioService.getCodigoImpuesto("RETEFUENTE MANO DE OBRA CON AIU",anoVigencia);

                                    por_ret_otros_contratista     = model.consorcioService.getPorcentaje("RETEFUENTE OTROS CON AIU",anoVigencia);
                                    cod_ret_otros_contratista     = model.consorcioService.getCodigoImpuesto("RETEFUENTE OTROS CON AIU",anoVigencia);

                                    por_ret_aiu_contratista     = model.consorcioService.getPorcentaje("RETEFUENTE CON AIU",anoVigencia);
                                    cod_ret_aiu_contratista     = model.consorcioService.getCodigoImpuesto("RETEFUENTE CON AIU",anoVigencia);
                                }
                                else {
                                    por_reteiva_contratista  =  model.consorcioService.getPorcentaje("RETEIVA",anoVigencia);
                                    cod_reteiva_contratista  =  model.consorcioService.getCodigoImpuesto("RETEIVA",anoVigencia);

                                    por_reteica_contratista  = model.consorcioService.getPorcentaje("RETEICA SIN AIU",anoVigencia);
                                    cod_reteica_contratista   = model.consorcioService.getCodigoImpuesto("RETEICA SIN AIU",anoVigencia);

                                    por_ret_material_contratista  = model.consorcioService.getPorcentaje("RETEFUENTE MATERIAL SIN AIU",anoVigencia);
                                    cod_ret_material_contratista  = model.consorcioService.getCodigoImpuesto("RETEFUENTE MATERIAL SIN AIU",anoVigencia);

                                    por_ret_mano_obra_contratista = model.consorcioService.getPorcentaje("RETEFUENTE MANO DE OBRA SIN AIU",anoVigencia);
                                    cod_ret_mano_obra_contratista = model.consorcioService.getCodigoImpuesto("RETEFUENTE MANO DE OBRA SIN AIU",anoVigencia);

                                    por_ret_otros_contratista     = model.consorcioService.getPorcentaje("RETEFUENTE OTROS SIN AIU",anoVigencia);
                                    cod_ret_otros_contratista     = model.consorcioService.getCodigoImpuesto("RETEFUENTE OTROS SIN AIU",anoVigencia);

                                    por_ret_aiu_contratista     = model.consorcioService.getPorcentaje("RETEFUENTE SIN AIU",anoVigencia);
                                    cod_ret_aiu_contratista     = model.consorcioService.getCodigoImpuesto("RETEFUENTE SIN AIU",anoVigencia);
                                }


                                double total_contratista = material + mano_obra + otros + administracion + imprevisto + utilidad;
                                double bonificacion      = -accionContratista.getBonificacion();
                                double subtotal          =  total_contratista + bonificacion ;

                                double val_base_iva_contratista =  accionContratista.getBase_iva_contratista();


                                double val_base_aiu      = administracion + imprevisto + utilidad;





                                val_iva_contratista             =  accionContratista.getIva_contratista();
                                if(val_iva_contratista == 0) {
                                    por_iva_contratista  =  0;
                                    cod_iva_contratista  =  "";
                                }


                                // Toma en cuenta si el contratista es gran contributente en cuyo caso reteica y reteiva = 0
                                if(contratista.getGran_contribuyente().equalsIgnoreCase("SI")) {  // 2010-06-14
                                    val_reteica_contratista = 0;
                                    por_reteica_contratista  =  0;
                                    cod_reteica_contratista  =  "";
                                    val_reteiva_contratista = 0;
                                    por_reteiva_contratista  =  0;
                                    cod_reteiva_contratista  =  "";
                                }
                                else {
                                    // Calculo del reteiva
                                    val_reteiva_contratista         = -Util.redondear2(val_base_iva_contratista * por_reteiva_contratista/100, 2);
                                    if(val_reteiva_contratista == 0) {
                                        por_reteiva_contratista  =  0;
                                        cod_reteiva_contratista  =  "";
                                    }
                                    // Calculo del reteica
                                    val_reteica_contratista         = -Util.redondear2(subtotal * por_reteica_contratista/100, 2);
                                    if(val_reteica_contratista == 0) {
                                        por_reteica_contratista  =  0;
                                        cod_reteica_contratista  =  "";
                                    }
                                }




                                // Se le descuenta a la mano de obra la bonificacion
                                if(mano_obra > 0){
                                    val_ret_mano_obra_contratista   = -Util.redondear2( (mano_obra + bonificacion) * por_ret_mano_obra_contratista/100, 2);
                                }
                                else {
                                    val_ret_mano_obra_contratista   = -Util.redondear2( mano_obra * por_ret_mano_obra_contratista/100, 2);
                                }
                                if(val_ret_mano_obra_contratista == 0) {
                                    por_ret_mano_obra_contratista  =  0;
                                    cod_ret_mano_obra_contratista  =  "";
                                }

                                // SI a la mano de obra no se le desconto la bonificacion , entonces se le descuenta a otros
                                if ( (mano_obra <= 0) && (otros > 0) ) {
                                    val_ret_otros_contratista       = -Util.redondear2( (otros + bonificacion) * por_ret_otros_contratista/100, 2);
                                }
                                else
                                {
                                    val_ret_otros_contratista       = -Util.redondear2(otros * por_ret_otros_contratista/100, 2);
                                }
                                if(val_ret_otros_contratista == 0) {
                                    por_ret_otros_contratista  =  0;
                                    cod_ret_otros_contratista  =  "";
                                }

                                // Si a la mano de obra o a otros no se le desconto la bonificacion , entonces se le descuenta a materiales
                                if ( (mano_obra <= 0) && (otros <= 0) ) {
                                    val_ret_material_contratista       = -Util.redondear2( (material + bonificacion) * por_ret_material_contratista/100, 2);
                                }
                                else
                                {
                                    val_ret_material_contratista       = -Util.redondear2(material * por_ret_material_contratista/100, 2);
                                }
                                if(val_ret_material_contratista == 0) {
                                    por_ret_material_contratista  =  0;
                                    cod_ret_material_contratista  =  "";
                                }

                                val_ret_aiu_contratista         = -Util.redondear2(val_base_aiu * por_ret_aiu_contratista/100, 2);
                                if(val_ret_aiu_contratista == 0) {
                                    por_ret_aiu_contratista  =  0;
                                    cod_ret_aiu_contratista  =  "";
                                }


                                double subtotal1=subtotal + val_iva_contratista;
                                double por_factoring_contratista = accionContratista.getPorcentaje_factoring();


                                if ((accionContratista.getContratista().equals("CC008") ||
                                     accionContratista.getContratista().equals("CC036") ||
                                     accionContratista.getContratista().equals("CC017")) && accionContratista.getPeriodo()==1  && accionContratista.getValor_agregado().equalsIgnoreCase("NO")){//091105
                                    por_factoring_contratista = 2;
                                }


                                double val_factoring_contratista      = -Util.redondear2(subtotal1 * por_factoring_contratista/100, 2);


                                double por_formula_contratista   = accionContratista.getPorcentaje_formula();
                                double val_formula_contratista   = -Util.redondear2(subtotal1*por_formula_contratista/100, 2);




                                /* Se comentario debido a que en este nuevo consorcio no existe el esquema comision MODELO ANTERIOR
                                // El parametro fue pasado con valor cero 0.00
                                //inicio090511
                                double valorFormulaProvintegral=0;

                                try{

                                // Se va a agregar una columna en la prefactura llamada valor_formula_provintegral en caso que sea modelo anterior

                                if (model.negociosApplusService.getEsquemaComision( accionContratista.getId_orden()).equals("MODELO_ANTERIOR")){
                                    valorFormula=valorFormula/2.00;
                                    valorFormulaProvintegral=valorFormula;
                                }
                                }catch(Exception eee){
                                    System.out.println("accion prefac registra"+eee.toString()+"__"+eee.getMessage());
                                }
                                //fin090511
                                */


                                localizado = true;



                                // CALCULO DE GENERACION DE LA SECUENCIA DE LA PREFACTURA A ASIGNAR

                                contratista.setSecuencia_prefactura( contratista.getSecuencia_prefactura() + 1);
                                secuencia_prefactura = contratista.getSecuencia_prefactura();
                                prefactura_contratista = contratista.getId_contratista() + '-' + Util.llenarConCerosALaIzquierda( secuencia_prefactura, 6 );

                                // Actualiza la secuencia de prefactura

                                comandoSQL = model.consorcioService.setSecuenciaPrefactura(contratista.getId_contratista());
                                comandos_sql.add(comandoSQL);

                                // Actualiza las acciones prefacturadas

                                comandoSQL = model.consorcioService.setAccionContratista (accion_no, prefactura_contratista, usuario_prefactura_contratista,
                                                                             cod_iva_contratista, por_iva_contratista, val_base_iva_contratista, val_iva_contratista,
                                                                             cod_ret_material_contratista, por_ret_material_contratista, val_ret_material_contratista,
                                                                             cod_ret_mano_obra_contratista, por_ret_mano_obra_contratista, val_ret_mano_obra_contratista,
                                                                             cod_ret_otros_contratista, por_ret_otros_contratista, val_ret_otros_contratista,
                                                                             cod_ret_aiu_contratista, por_ret_aiu_contratista, val_ret_aiu_contratista,
                                                                             por_factoring_contratista, val_factoring_contratista,
                                                                             por_formula_contratista, val_formula_contratista,
                                                                             cod_reteica_contratista, por_reteica_contratista, val_reteica_contratista,
                                                                             cod_reteiva_contratista, por_reteiva_contratista, val_reteiva_contratista);
                                comandos_sql.add(comandoSQL);
                            }
                        }

                    }// Final del recorrido de la lista acciones mostradas en pantalla


                    estado = model.consorcioService.ejecutarBatchSQL(comandos_sql);
                    comandos_sql.clear();


                    // Refresca la lista de prefacturas
                    model.consorcioService.setAccionesContratista(usuario.getNitPropietario(),2);
                    listaAccionesContratista = model.consorcioService.getAccionesContratista();
                    if(!listaAccionesContratista.isEmpty() ) {
                        model.consorcioService.setContratista(usuario.getNitPropietario());
                        contratista = model.consorcioService.getContratista();
                        secuencia_prefactura = contratista.getSecuencia_prefactura() + 1;
                        prefactura_contratista = contratista.getId_contratista() + '-' + Util.llenarConCerosALaIzquierda( secuencia_prefactura, 6 );
                        next = "/jsp/consorcio/prefacturaContratistaSeleccionada.jsp";

                    }
                    else {
                        msj   = "El contratista no tiene registros disponibles...";
                        next  = "/jsp/consorcio/prefacturaContratista.jsp?msj=";
                    }
                    next += msj ;
                    this.dispatchRequest(next);

                }
                catch (SQLException e){

                    throw new ServletException(e.getMessage());
                }
           }


        }


            //  EVENTO  :  CREAR_FACTURA_CONTRATISTA de facturaCrearContratista.jsp
            //  Conforma una lista resumida de prefacturas
            if (evento.equalsIgnoreCase("CREAR_FACTURA_CONTRATISTA")) {

                HFacturaContratista hilo = new HFacturaContratista();
                hilo.start(model, usuario, distrito);

                aceptarDisable = "S";

                next  = "/jsp/consorcio/facturaCrearContratista.jsp?aceptarDisable="+aceptarDisable+"?msj=";
                msj   = "Se ha iniciado el proceso de crear facturas de contratistas. Ver log de proceso para ver su finalizacion";

            } // Fin de CREAR_FACTURA_CONTRATISTA





            //  EVENTO  :  SOLICITUD_POR_FACTURAR  de solicitudLiquidacion.jsp
            //  Arma una lista de solicitudes pendientes de facturar a los clientes
            if (evento.equalsIgnoreCase("SOLICITUD_POR_FACTURAR")) {

                model.consorcioService.buscaSolicitudPorFacturar();
                List listaSolicitudPorFacturar = model.consorcioService.getSolicitudPorFacturar();

                if(!listaSolicitudPorFacturar.isEmpty() ) {

                    next  = "/jsp/opav/liquidacion/solicitudListaPorFacturar.jsp?msj=";
                    msj   = "";
                }
                else {
                    msj   = "No existen ofertas a listar";
                    next  = "/jsp/opav/liquidacion/ofertaAcceder.jsp?msj=";
                }
            } // Fin de OFERTAS_POR_FACTURAR



            //  EVENTO  :  DETALLAR_SOLICITUD  de solicitudListaPorFacturar.jsp o de solicitudLiquidacionEspecifica.jsp
            //  Extrae la informacion de la solicitud seleccionada
            //  Extrae la informacion de los diferentes subclientes de esa solicitud
            //  Extrae la informacion de las acciones de esa solicitud
            if (evento.equalsIgnoreCase("DETALLAR_SOLICITUD")) {

                try {


                    String opcion = ( request.getParameter("opcion")!= null) ? request.getParameter("opcion") : "";
                    String id_solicitud = ( request.getParameter("id_solicitud")!= null) ? request.getParameter("id_solicitud") : "";
                    int parcial = Integer.parseInt( (request.getParameter("parcial")!= null) ? request.getParameter("parcial") : "1"  );
                    String fechaInicioPago = ( request.getParameter("fechaInicioPago")!= null) ? request.getParameter("fechaInicioPago") : "0099-01-01";

                    // System.out.println(fechaInicioPago);

                    // Crea un objeto SolicitudPorFacturar correspondiente a la solicitud seleccionada,
                    // la extrae de la lista de ofertas por facturar
                    // El objeto sera utilizado en la pagina solicitudDetallar.jsp
                    model.consorcioService.buscarSolicitudPorFacturar(id_solicitud, parcial);

                    // Crea una lista de objetos SubclientePorSolicitud correspondiente a los subclientes asociados a la solicitud seleccionada,
                    // El objeto sera utilizado en la pagina solicitudDetallar.jsp
                    model.consorcioService.buscaSubclientePorSolicitud(id_solicitud, parcial, fechaInicioPago);


                    // Crea una lista de objetos AccionPorFacturar correspondiente a la solicitud seleccionada,
                    // El objeto sera utilizado en la pagina solicitudDetallar.jsp
                    model.consorcioService.buscaAccionPorSolicitud(id_solicitud, parcial);


                    // Actualiza la lista de SubclientePorFacturar incluyendoles los valores relacionados a la financiacion
                    model.consorcioService.CalculaFinanciacion();

                    msj   = "";
                    next  = "/jsp/opav/liquidacion/solicitudDetallar.jsp?opcion="+opcion+"&msj=";

                }
                catch (Exception e) {
                     Util.imprimirTrace(e);
                    request.setAttribute("javax.servlet.jsp.jspException", e);
                    RequestDispatcher rd = this.application.getRequestDispatcher("/error/ErrorPage.jsp");
                    rd.forward(request, response);
                }
            } // Fin de DETALLAR_SOLICITUD









            //  EVENTO  :  APLICAR_LIQUIDACION  de solicitudDetallar.jsp
            //  Actualiza simbolo variable , observacion , cambia id_estado del negocion para una orden y registra la liquidacion de cada accion
            if (evento.equalsIgnoreCase("APLICAR_LIQUIDACION")) {


               String opcion = ( request.getParameter("opcion")!= null) ? request.getParameter("opcion") : "";
               /*
                  var elemento_simbolo      = document.getElementById("simboloVariable"+k).value;
                   var elemento_observacion  = document.getElementById("observacion"+k).value;
                   var elemento_fechaFactura = document.getElementById("fechaFactura"+k).value;
                   var elemento_fechaInicioPago = document.getElementById("fechaInicioPago"+k).value;


                   lista_simbolo = lista_simbolo + elemento_simbolo + "|";
                   lista_observacion = lista_observacion + elemento_observacion + "|";
                   lista_fechaFactura = lista_fechaFactura + elemento_fechaFactura + "|";

                                   
                   lista_fechaInicioPago = lista_fechaInicioPago + elemento_fechaInicioPago + "|";
                */
               
               //para aplicar liquidacion.
               int numeroItems= Integer.parseInt(request.getParameter("numItens"));
             
                String lista_simboloVariable = "";
                String lista_observacion = "";
                String lista_fechaFactura = "";
                String lista_fechaInicioPago = "";

                for (int i = 1; i <= numeroItems - 1; i++) {

                    String elemento_simbolo = request.getParameter("simboloVariable" + i);// document.getElementById("simboloVariable" + k).value;
                    String elemento_observacion = request.getParameter("observacion" + i);// document.getElementById("observacion" + k).value;
                    String elemento_fechaFactura = request.getParameter("fechaFactura" + i);//document.getElementById("fechaFactura" + k).value;
                    String elemento_fechaInicioPago = request.getParameter("fechaInicioPago" + i);// document.getElementById("fechaInicioPago" + k).value;


                    lista_simboloVariable = lista_simboloVariable + elemento_simbolo + "|";
                    lista_observacion = lista_observacion + elemento_observacion + "|";
                    lista_fechaFactura = lista_fechaFactura + elemento_fechaFactura + "|";

                    lista_fechaInicioPago = lista_fechaInicioPago + elemento_fechaInicioPago + "|";
                }


               System.out.println("Inicio lectura parametro") ;

/*
               String lista_simboloVariable  = request.getParameter("lista_simboloVariable");
               String lista_observacion      = request.getParameter("lista_observacion");
               String lista_fechaFactura     = request.getParameter("lista_fechaFactura");
               String lista_fechaInicioPago     = request.getParameter("lista_fechaInicioPago");*/


               lista_simboloVariable = lista_simboloVariable.substring(0,lista_simboloVariable.length() - 1 );
               lista_observacion     = lista_observacion.substring(0,lista_observacion.length() - 1 );
               lista_fechaFactura    = lista_fechaFactura.substring(0,lista_fechaFactura.length() - 1 );
               lista_fechaInicioPago = lista_fechaInicioPago.substring(0,lista_fechaInicioPago.length() - 1 );

               System.out.println("Parametro lista_simboloVariable : " + lista_simboloVariable);
               System.out.println("Parametro lista_observacion : " + lista_observacion);
               System.out.println("Parametro lista_fechaFactura : " + lista_fechaFactura);
               System.out.println("Parametro lista_fechaInicioPago : " + lista_fechaInicioPago);



               String[] simboloVariable  = lista_simboloVariable.split("\\|");
               String[] observacion      = lista_observacion.split("\\|");
               String[] fechaFactura     = lista_fechaFactura.split("\\|");
               String[] fechaInicioPago  = lista_fechaInicioPago.split("\\|");



               System.out.println("Final lectura parametro") ;

               System.out.println("Impresion de parametros Longitud = "+simboloVariable.length);
               for (int x=0;x<simboloVariable.length;x++) {
                   System.out.println("Simbolo variable[" + x + "] = " + simboloVariable[x]);
                   System.out.println("Observacion = [" + x + "] = " + observacion[x]);
                   System.out.println("fechaFactura = [" + x + "] = " + fechaFactura[x]);
                   System.out.println("fechaInicioPago = [" + x + "] = " + fechaInicioPago[x]);
               }


               String error = "NO";



                Vector comandos_sql =new Vector();
                String comandoSQL = "";

                java.util.Date fechaActual = new Date();
                String last_update = fechaActual.toString();



                // Actualiza las acciones relacionadas a la solicitud
                try {
                    List listaAccionPorSolicitud = model.consorcioService.getAccionPorSolicitud();
                    com.tsp.opav.model.beans.AccionPorSolicitud accion = null;


                    Iterator it = listaAccionPorSolicitud.iterator();

                    while (it.hasNext()) {
                       accion = (com.tsp.opav.model.beans.AccionPorSolicitud)it.next();
                       comandoSQL = model.consorcioService.setAcciones(accion, last_update, usuario.getLogin());

                       // System.out.println(comandoSQL+ "\n");

                       comandos_sql.add(comandoSQL);
                    }
                }
                catch (Exception e) {
                    Util.imprimirTrace(e);
                    error = "SI";
                }


               // Actualiza los valores de los subclientes relacionados a la solicitud
                try {
                    int i = 0;
                    String simbolo_variable = "";
                    String fecha_factura = "";
                    String observacion_subcliente = "";
                    String fecha_inicioPago = "";

                    com.tsp.opav.model.beans.SubclientePorSolicitud subcliente = null;
                    List listaSubclientePorSolicitud = model.consorcioService.getSubclientePorSolicitud();
                    ListIterator lit = listaSubclientePorSolicitud.listIterator();

                    while (lit.hasNext()) {
                       subcliente  = (com.tsp.opav.model.beans.SubclientePorSolicitud) lit.next();

                       simbolo_variable = simboloVariable[i];
                       observacion_subcliente = observacion[i];
                       fecha_factura = fechaFactura[i];
                       fecha_inicioPago = fechaInicioPago[i];


                       System.out.println(simbolo_variable + "-" + observacion_subcliente + "-" + fecha_factura);
                       System.out.println(simboloVariable[i] + "-" + observacion[i] + "-" + fechaFactura[i] + "-" + fechaInicioPago[i] );



                       subcliente.setSimbolo_variable(simbolo_variable);
                       subcliente.setObservacion(observacion_subcliente);
                       subcliente.setFecha_factura(fecha_factura);
                       subcliente.setFecha_inicio_pago(fecha_inicioPago);

                       i++;

                       comandoSQL = model.consorcioService.setSubclientesOfertas(subcliente, simbolo_variable,observacion_subcliente, fecha_factura, usuario.getLogin()) ;

                       System.out.println(comandoSQL+ "\n");

                       comandos_sql.add(comandoSQL);
                    }
                }
                catch (Exception e) {
                    error = "SI";
                }



                if(error.equalsIgnoreCase("NO")){

                    // Grabando todo a la base de datos.
                    String estado = model.consorcioService.ejecutarBatchSQL(comandos_sql);

                    if (estado.equalsIgnoreCase("ERROR")){
                        msj  = "Los datos no fueron registrados. Se genero un rollback sobre la base de datos";
                        next = "/jsp/opav/liquidacion/solicitudDetallar.jsp?opcion="+opcion+"&msj=";
                    }
                    else{

                        model.consorcioService.buscaSolicitudPorFacturar();

                        msj="";
                        next = "/jsp/opav/liquidacion/solicitudListaPorFacturar.jsp?opcion="+opcion+"&msj=";



                    }
                }
                else {
                    if ( opcion.equalsIgnoreCase("LISTADO") ){
                        msj  = "Los datos no fueron registrados. Hubo un error al generar el SQL de actualizacion al subcliente"  ;
                        next = "/jsp/opav/liquidacion/solicitudListaPorFacturar.jsp?opcion="+opcion+"&msj=";

                    }
                    else
                    {
                        msj  = ""  ;
                        next = "/jsp/opav/liquidacion/solicitudLiquidacionEspecifica.jsp?opcion="+opcion+"&msj=";
                    }
                }
                

            } // Fin de APLICAR_LIQUIDACION



            //  EVENTO  :  GENERAR_FACTURAR_ECA de facturaEcaCreacion.jsp
            //  Envia una prefactura a exportar a excell
            if (evento.equalsIgnoreCase("GENERAR_FACTURAR_ECA")) {

                // System.out.println ("Inicio antes del hilo");


                HGenerarFacturaEcaOpav hilo =  new HGenerarFacturaEcaOpav();
                hilo.start(model, usuario);

                // System.out.println ("Final despues del hilo");

                aceptarDisable = "S";
                msj   = "La creacion de las facturas CxC a Eca se ha iniciado...Ver log de proceso para ver su finalizacion";
                next  = "/jsp/opav/facturacion/facturaEcaCreacion.jsp?aceptarDisable="+aceptarDisable+"?msj=";

            } // Fin de GENERAR_FACTURAR_ECA






            next += msj ;
            this.dispatchRequest(next);

        }catch (Exception e) {
             Util.imprimirTrace(e);
        }

    }


    
        public double get_anualidad(double vp , double i , double n ) {

            // Sirve para pasar conseguir el valor de una anualidad especificando un interes en  Mensual Vencida

            // r = anual trimestre anticipado.

            double r = Math.pow((1+(i/100)),n);
            double factor =  (r-1)/(i/100*r);
            double a = vp / factor;

            return a;
        }         
    
        /*
        *  v = valor
        *  i = tasa periodica mensual
        *  n = numero de dias
        */
        public double getInteresMora(double v , double i , double n ) {
            
            // Pasar la tasa periodica mensual a efectiva anual
            
            double ea = Math.pow( (1+i) ,(360/30) ) - 1.00 ;
            double id = Math.pow( (1+ea) ,(n/365) ) - 1 ;
            
            return v*id ;
            

        }             
        
        
        

}










