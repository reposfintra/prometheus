/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.opav.controller;

import com.tsp.opav.model.beans.AccionesEca;
import com.tsp.opav.model.beans.Cotizacion;
import com.tsp.opav.model.beans.OfertaElca;
import com.tsp.operation.model.TransaccionService;
import com.tsp.operation.model.beans.Usuario;
import javax.servlet.http.*;

import com.tsp.operation.model.beans.Convenio;
import com.tsp.operation.model.beans.NegocioTrazabilidad;
import com.tsp.operation.model.beans.Negocios;
import com.tsp.operation.model.beans.SolicitudAval;
import com.tsp.operation.model.beans.SolicitudLaboral;
import com.tsp.operation.model.beans.SolicitudPersona;
import com.tsp.operation.model.services.GestionConveniosService;
import com.tsp.operation.model.services.GestionSolicitudAvalService;
import com.tsp.operation.model.services.NegociosGenService;
import com.tsp.opav.model.services.ClientesVerService;
import com.tsp.opav.model.services.CotizacionService;
import com.tsp.opav.model.services.GestionSolicitudAiresAAEService;
import com.tsp.operation.model.beans.BeanGeneral;
import com.tsp.operation.model.beans.Cliente;
import com.tsp.operation.model.beans.SerieGeneral;
import com.tsp.operation.model.services.NegocioTrazabilidadService;
import com.tsp.operation.model.threads.HSendMail2;
import com.tsp.util.Util;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;

/**
 *
 * @author Iris Vargas
 */
public class GestionSolicitudAiresAAEAction extends Action {

    //Opciones dentro de la clase:
    private final int CARGAR_CLIENTE = 0;
    private final int GUARDAR_SOLICITUD = 1;
    private final int CARGAR_INSTALACIONES = 2;
    private final int OBTENER_PRECIO_VENTA = 3;
    private final int CARGAR_CIUDADES = 4;
    private final int CARGAR_HORAS = 5;
    private final int PUEDE_VISITAR = 6;
    private final int VALOR_FINANCIADO = 7;
    GestionSolicitudAiresAAEService aaserv;
    GestionSolicitudAvalService gsaserv;
    String next;
    String login;
    boolean redirect = false;
    String cadenaWrite = "";
    Usuario usuario = null;

    public GestionSolicitudAiresAAEAction() {
        this.next = "";
    }

    @Override
    public void run() throws javax.servlet.ServletException {

        try {
            int op = (request.getParameter("opcion") != null) ? Integer.parseInt(request.getParameter("opcion")) : -1;
            HttpSession session = request.getSession();
            com.tsp.util.Util.logController(((com.tsp.operation.model.beans.Usuario) session.getAttribute("Usuario")).getLogin(), this.getClass().getName());
            usuario = (Usuario) session.getAttribute("Usuario");
            aaserv = new GestionSolicitudAiresAAEService(usuario.getBd());
            gsaserv = new GestionSolicitudAvalService(usuario.getBd());
            login = usuario.getLogin();

            gsaserv.setDstrct(usuario.getDstrct() != null ? usuario.getDstrct() : "FINV");
            gsaserv.setLoginuser(usuario.getLogin() != null ? usuario.getLogin() : "ADMIN");


            switch (op) {
                case CARGAR_CLIENTE:
                    cargarCliente();
                    break;
                case GUARDAR_SOLICITUD:
                    guardarSolicitud();
                    break;
                case CARGAR_INSTALACIONES:
                    cargarInstalaciones();
                    break;
                case OBTENER_PRECIO_VENTA:
                    getPrecioVenta();
                    break;
                case CARGAR_CIUDADES:
                    cargarCiudades();
                    break;
                case CARGAR_HORAS:
                    cargarHoras();
                    break;
                case PUEDE_VISITAR:
                    puedeRealizarVisita();
                    break;
                case VALOR_FINANCIADO:
                    getValorFinanciacion();
                    break;

            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (redirect == false) {
                    this.escribirResponse(cadenaWrite);
                } else {
                    this.dispatchRequest(next);
                }
            } catch (Exception e) {
                System.out.println("Error al redireccionar o escribir respuesta: " + e.toString());
                e.printStackTrace();
            }
        }





    }

    /**
     * Escribe el resultado de la consulta en el response de Ajax
     * @param dato la cadena a escribir
     * @throws Exception cuando hay un error
     */
    protected void escribirResponse(String dato) throws Exception {
        try {
            response.setContentType("text/plain; charset=utf-8");
            response.setHeader("Cache-Control", "no-cache");
            response.getWriter().println(dato);
        } catch (Exception e) {
            throw new Exception("Error al escribir el response: " + e.toString());
        }
    }

    /**
     * Busca las instalciones de un equipo
     * @throws Exception
     */
    private void cargarInstalaciones() throws Exception {
        redirect = false;
        String equipo = request.getParameter("equipo") != null ? request.getParameter("equipo") : "";
        String j = request.getParameter("i") != null ? request.getParameter("i") : "";
        ArrayList lista = null;
        try {
            lista = aaserv.buscarInstalaciones(equipo);
        } catch (Exception e) {
            System.out.println("error(action): " + e.toString());
            e.printStackTrace();
        }
        cadenaWrite = "<select id='instalacion"+j+"' name='instalacion"+j+"' style='width:90%;' onchange='getPrecioVenta(\"instalacion\","+j+")'>";
        if (lista.size() > 0) {
            String[] dato1 = null;
            for (int i = 0; i < lista.size(); i++) {
                dato1 = ((String) lista.get(i)).split(";_;");
                cadenaWrite += " <option value='" + dato1[0] + "'>" + dato1[1] + "</option>";
            }
        } else {
            cadenaWrite += "<option value=''>...</option>";
        }

        cadenaWrite += "</select>";
    }
   
    /**
     * Busca las instalciones de un equipo
     * @throws Exception
     */
   private void cargarCiudades() throws Exception {
        redirect = false;

        String codpt = request.getParameter("dept") != null ? request.getParameter("dept") : "";
        String idciu = request.getParameter("ciu") != null ? request.getParameter("ciu") : "";
        ArrayList lista = null;
        lista = aaserv.buscarCiudadesAAAE(codpt);
        redirect = false;
        try {
        } catch (Exception e) {
            System.out.println("error(action): " + e.toString());
            e.printStackTrace();
        }
        cadenaWrite = "<select id='" + idciu + "' name='" + idciu + "' style='width:20em;'>";
        if (lista.size() > 0) {
            String[] dato1 = null;
            for (int i = 0; i < lista.size(); i++) {
                dato1 = ((String) lista.get(i)).split(";_;");
                cadenaWrite += " <option value='" + dato1[0] + "'>" + dato1[1] + "</option>";
            }
        } else {
            cadenaWrite += "<option value=''>...</option>";
        }
        cadenaWrite += "</select>";
    }

    /**
     * Busca cliente o formualrio decuerdo al campo de ejecucion
     * @throws Exception
     */
    private void getPrecioVenta() throws Exception {
        redirect = false;
        String material = request.getParameter("material") != null ? request.getParameter("material") : "";
        String tipo_solicitud = request.getParameter("tipo_solicitud") != null ? request.getParameter("tipo_solicitud") : "";
        DateFormat dateFormat = new SimpleDateFormat("yyyyMM");
        Date dat = new Date();
        double precio = 0;
        try {
            precio = aaserv.getPrecioVentaMaterial(material, dateFormat.format(dat), tipo_solicitud);

        } catch (Exception e) {
            System.out.println("error en action: " + e.toString());
            e.printStackTrace();
        }
        cadenaWrite = precio + "";
    }

    /**
     * Busca cliente o formualrio decuerdo al campo de ejecucion
     * @throws Exception
     */
    private void cargarCliente() throws Exception {
        String campo = request.getParameter("campo") != null ? request.getParameter("campo") : "";
        String dato = request.getParameter("dato") != null ? request.getParameter("dato") : "";
        String info = "";
        boolean pasa = true;
        SolicitudPersona persona;
        SolicitudLaboral laboral;
        redirect = false;
        try {
            if (campo.equals("nic")) {

                info = aaserv.infoCliente(dato);

            } else if (campo.equals("id")) {


                persona = gsaserv.buscarPersonaId(dato);
                if (persona != null) {
                    laboral = gsaserv.buscarLaboralId(dato);
                    info += persona.getPrimerApellido() + ";_;";//0
                    info += persona.getSegundoApellido() + ";_;";//1
                    info += persona.getPrimerNombre() + ";_;";//2
                    info += persona.getSegundoNombre() + ";_;";//3
                    info += persona.getNombre() + ";_;";//4
                    info += persona.getTipoId() + ";_;";//5
                    info += persona.getIdentificacion() + ";_;";//6
                    info += persona.getDireccion() + ";_;";//7
                    info += persona.getTelefono() + ";_;";//8
                    info += persona.getFechaNacimiento().substring(0, 10) + ";_;";//9
                    info += persona.getEstadoCivil() + ";_;";//10
                    info += persona.getCiudad() + ";_;";//11
                    info += persona.getDepartamento() + ";_;";//12
                    info += persona.getEstrato() + ";_;";//13
                    info += persona.getTipoVivienda() + ";_;";//14
                    info += persona.getRepresentanteLegal() + ";_;";//15
                    //DATOS DEL CONYUGUE
                    info += Util.customFormat(Double.parseDouble(persona.getSalarioCony())) + ";_;";//16
                    //INFOMACION LABORAL Y ECONOMICA
                    if (laboral != null) {

                        info += Util.customFormat(Double.parseDouble(laboral.getSalario())) + ";_;";//17
                        info += Util.customFormat(Double.parseDouble(laboral.getOtrosIngresos())) + ";_;";//18
                        info += Util.customFormat(Double.parseDouble(laboral.getGastosArriendo())) + ";_;";//19
                    } else {
                        info += " ;_;";//16
                        info += " ;_;";//17
                        info += " ";//18 
                    }
                }
            }

        } catch (Exception e) {
            pasa = false;
            System.out.println("error en action: " + e.toString());
            e.printStackTrace();
        }
        if (pasa == true && (info.equals("") == false)) {
            cadenaWrite = info;
        } else {
            cadenaWrite = "La informacion digitada no se encuentra registrada";
        }
    }

    private void guardarSolicitud() throws Exception {
        String msj = "Solicitud insertada exitosamente";
        try {
            redirect = true;
            GestionConveniosService gcserv = new GestionConveniosService(usuario.getBd());
            NegociosGenService ngserv = new NegociosGenService(usuario.getBd());
            SolicitudAval solicitud = new SolicitudAval();
            SolicitudPersona persona = new SolicitudPersona();
            SolicitudLaboral laboral = new SolicitudLaboral();
            NegocioTrazabilidad negtraza = new NegocioTrazabilidad();
           
            //aqui voy a capturar el nit o cedula del cliente para validar 
            //si pasa politacas de datacredito segun el criterio de riesgos.
            String miId = request.getParameter("id") != null ? request.getParameter("id") : "";

            boolean sw = true;
            int num_solicitud = 0;
            num_solicitud = (request.getParameter("num_form") != null && request.getParameter("num_form").equals("") == false) ? Integer.parseInt(request.getParameter("num_form")) : 0;//comentado
            int filas = (request.getParameter("filas") != null && request.getParameter("filas").equals("") == false) ? Integer.parseInt(request.getParameter("filas")) : 0;
            //primero los datos basicos de la solicitud
            String tipo_p = request.getParameter("tipo_p") != null && !request.getParameter("tipo_p").equals("") ? request.getParameter("tipo_p") : "natural";
            if (tipo_p.equals("juridica")) {
                solicitud.setTipoPersona("J");
            } else if (tipo_p.equals("natural")) {
                solicitud.setTipoPersona("N");
            } else {
                solicitud.setTipoPersona("N");
            }
            String fecha_cons = request.getParameter("fecha_cons") != null && !request.getParameter("fecha_cons").equals("") ? request.getParameter("fecha_cons") : "now()";
            String valor_solicitado = request.getParameter("valor_solicitado") != null && !request.getParameter("valor_solicitado").trim().equals("") ? request.getParameter("valor_solicitado").replaceAll(",", "") : "0";
            String convenio = request.getParameter("convenio") != null ? request.getParameter("convenio") : "";
            String plazo = request.getParameter("plazo") != null ? request.getParameter("plazo") : "1";
            String fecha_vis_plan = request.getParameter("f_vis_plan") != null ? request.getParameter("f_vis_plan") : "0099-01-01 00:00:00";
            double valor_equipos=0;
            double valor_instalaciones=0;
            for (int i = 0; i <= filas; i++) {
                 valor_equipos += request.getParameter("valor_equipo" + i) != null ? Double.parseDouble(request.getParameter("valor_equipo" + i).replaceAll(",", "")) : 0;
                 valor_instalaciones += request.getParameter("valor_instalacion" + i) != null ? Double.parseDouble(request.getParameter("valor_instalacion" + i).replaceAll(",", "")) : 0;
             }


            solicitud.setNumeroSolicitud("" + num_solicitud);
            solicitud.setFechaConsulta(fecha_cons);
            solicitud.setValorSolicitado((valor_equipos+valor_instalaciones)+"");
            solicitud.setAgente("");
            solicitud.setAfiliado("");
            solicitud.setCodigo("");
            solicitud.setNumeroAprobacion("");
            solicitud.setEstadoSol("P");
            solicitud.setIdConvenio(convenio);
            solicitud.setActividadNegocio("");
            solicitud.setProducto("03");
            solicitud.setServicio("");
            solicitud.setCiudadMatricula("");
            solicitud.setValorProducto((valor_equipos+valor_instalaciones)+"");
            solicitud.setAsesor("");
            solicitud.setPlazo(plazo);
            solicitud.setSector(null);
            solicitud.setSubsector(null);
            solicitud.setTipoNegocio("03");
            solicitud.setPlazoPrCuota("30");
            solicitud.setBanco("");
            solicitud.setNumTipoNegocio("");

            persona = null;
            persona = new SolicitudPersona();
            persona.setNumeroSolicitud("" + num_solicitud);
            persona.setTipoPersona("N");
            persona.setTipo("S");
            String cod_cli = gsaserv.codigoCliente(request.getParameter("id") != null ? request.getParameter("id") : "");
            persona.setCodcli(cod_cli.split(";_;")[0]);
            persona.setPrimerApellido(request.getParameter("pr_apellido_nat") != null ? request.getParameter("pr_apellido_nat") : "");
            persona.setSegundoApellido(request.getParameter("seg_apellido_nat") != null ? request.getParameter("seg_apellido_nat") : "");
            persona.setPrimerNombre(request.getParameter("pr_nombre_nat") != null ? request.getParameter("pr_nombre_nat") : "");
            persona.setSegundoNombre(request.getParameter("seg_nombre_nat") != null ? request.getParameter("seg_nombre_nat") : "");
            persona.setNombre(persona.getPrimerApellido() + " " + persona.getSegundoApellido() + " " + persona.getPrimerNombre() + " " + persona.getSegundoNombre());//nombre completo
            persona.setGenero("");
            persona.setTipoId(tipo_p.equals("juridica") ? "NIT" : "CED");
            persona.setIdentificacion(request.getParameter("id") != null && !request.getParameter("id").equals("") ? request.getParameter("id") : "");
            persona.setDireccion(request.getParameter("dir_nat") != null ? request.getParameter("dir_nat") : "");
            persona.setTelefono(request.getParameter("tel_nat") != null ? request.getParameter("tel_nat") : "");
            persona.setFechaExpedicionId("0099-01-01 00:00:00");
            persona.setCiudadExpedicionId("");
            persona.setDptoExpedicionId("");
            persona.setFechaNacimiento(request.getParameter("f_nac_nat") != null ? (request.getParameter("f_nac_nat").equals("") == true ? "0099-01-01 00:00:00" : request.getParameter("f_nac_nat")) : "0099-01-01 00:00:00");
            persona.setCiudadNacimiento("");
            persona.setDptoNacimiento("");
            persona.setEstadoCivil(request.getParameter("est_civil_nat") != null ? request.getParameter("est_civil_nat") : "");
            persona.setNivelEstudio("");
            persona.setProfesion("");
            persona.setPersonasACargo("0");
            persona.setNumHijos("0");
            persona.setTotalGrupoFamiliar("0");
            persona.setCiudad(request.getParameter("ciu_nat") != null ? request.getParameter("ciu_nat") : "");
            persona.setDepartamento(request.getParameter("dep_nat") != null ? request.getParameter("dep_nat") : "");
            persona.setBarrio("");
            persona.setEstrato(request.getParameter("estr_nat") != null && !request.getParameter("estr_nat").equals("") ? request.getParameter("estr_nat") : "0");
            persona.setFechaConstitucion("0099-01-01 00:00:00");
            persona.setTipoVivienda(request.getParameter("tipo_viv_nat") != null ? request.getParameter("tipo_viv_nat") : "");
            persona.setTiempoResidencia("0 A�os 0 Meses");
            persona.setCelular("");
            persona.setEmail("");

            //si tiene conyuge

            persona.setPrimerApellidoCony("");
            persona.setSegundoApellidoCony("");
            persona.setPrimerNombreCony("");
            persona.setSegundoNombreCony("");
            persona.setTipoIdentificacionCony("CED");
            persona.setIdentificacionCony("");
            persona.setEmpresaCony("");
            persona.setDireccionEmpresaCony("");
            persona.setTelefonoCony("");
            persona.setCargoCony("");
            persona.setSalarioCony(request.getParameter("emp_sal_con_nat") != null && !request.getParameter("emp_sal_con_nat").equals("") ? request.getParameter("emp_sal_con_nat").replaceAll(",", "") : "0");
            persona.setCelularCony("");
            persona.setEmailCony("");

            if (tipo_p.equals("juridica")) {
                persona.setRepresentanteLegal(request.getParameter("rep_legal") != null ? request.getParameter("rep_legal") : "");
                persona.setNombre(request.getParameter("razon_social") != null ? request.getParameter("razon_social") : "");
            }


            //datos laborales
            laboral = null;
            laboral = new SolicitudLaboral();
            laboral.setNumeroSolicitud("" + num_solicitud);
            laboral.setTipo("S");
            laboral.setOcupacion("");
            laboral.setActividadEconomica("");
            laboral.setNombreEmpresa("");
            laboral.setNit("");
            laboral.setDireccion("");
            laboral.setDireccionCobro("");
            laboral.setCiudad("");
            laboral.setDepartamento("");
            laboral.setTelefono("");
            laboral.setExtension("");
            laboral.setCargo("");
            laboral.setFechaIngreso("0099-01-01 00:00:00");
            laboral.setTipoContrato("");
            laboral.setSalario(request.getParameter("sal_nat") != null && !request.getParameter("sal_nat").trim().equals("") ? request.getParameter("sal_nat").replaceAll(",", "") : "0");
            laboral.setOtrosIngresos(request.getParameter("otros_nat") != null && !request.getParameter("otros_nat").trim().equals("") ? request.getParameter("otros_nat").replaceAll(",", "") : "0");
            laboral.setConceptoOtrosIng("");
            laboral.setGastosManutencion("0");
            laboral.setGastosCreditos("0");
            laboral.setGastosArriendo(request.getParameter("arr_nat") != null && !request.getParameter("arr_nat").trim().equals("") ? request.getParameter("arr_nat").replaceAll(",", "") : "0");
            laboral.setCelular("");
            laboral.setEmail("");
            laboral.setEps("");
            laboral.setTipoAfiliacion("");


            TransaccionService tService = new TransaccionService(usuario.getBd());
            tService.crearStatement();
            ClientesVerService clserv = new ClientesVerService(usuario.getBd());
            String existe_cliente = clserv.ValidaClienteXnit(persona.getIdentificacion());

            if (existe_cliente.equals("NX")) {
                Cliente cl = new Cliente();
                SerieGeneral s_id_cliente = clserv.getSerie("FINV", "OP", "CLIENTE");
                clserv.setSerie("FINV", "OP", "CLIENTE");
                persona.setCodcli(s_id_cliente.getUltimo_prefijo_numero());
                cl.setCodcli(persona.getCodcli());
                cl.setCiudad(persona.getCiudad());
                cl.setNomContacto("");
                cl.setCelRepresentante("");
                cl.setSector((request.getParameter("zona") != null && !(request.getParameter("zona").equals(""))) ? request.getParameter("zona") : "");
                cl.setDireccion(persona.getDireccion());
                cl.setNit(persona.getIdentificacion());
                cl.setCargoContacto("");
                cl.setNomcli(persona.getNombre());
                cl.setNomRepresentante(persona.getRepresentanteLegal());
                cl.setTelContacto("");
                cl.setTelRepresentante("");
                cl.setTipo(request.getParameter(persona.getTipoId()));
                cl.setIdEjecutivo((request.getParameter("ejecutivo_cta") != null && !(request.getParameter("ejecutivo_cta").equals(""))) ? (request.getParameter("ejecutivo_cta").split(","))[0] : "");
                String[] nic = {request.getParameter("nic") != null ? request.getParameter("nic") : ""};
                cl.setNics(nic);
                cl.setId_padre(cl.getCodcli());
                cl.setOficial(false);
                cl.setEdif("N");

                tService.getSt().addBatch(clserv.insertarCliente(cl, usuario.getLogin()));
                if(!clserv.existeNic( cl.getNics()[0])){
                    ArrayList<String> listSql = clserv.insertarNics(cl.getCodcli(), usuario.getLogin(), cl.getNics());
                    for (String sql : listSql) {
                        tService.getSt().addBatch(sql);
                    }
                }
                tService.getSt().addBatch(clserv.insertSubcliente(cl.getCodcli(), cl.getId_padre(), usuario.getLogin()));

            } else if (existe_cliente.equals("N")) {
                tService.getSt().addBatch(clserv.marcarCliente(persona.getIdentificacion()));
            }
            if(persona.getCodcli().equals("")){
               persona.setCodcli(clserv.codclifinv(persona.getIdentificacion()));
            }

            tService.getSt().addBatch(gsaserv.insertSolicitud(solicitud));
            tService.getSt().addBatch(gsaserv.insertPersona(persona));
            tService.getSt().addBatch(gsaserv.insertLaboral(laboral));

            String mensaje = "";
            Convenio conve = gcserv.buscar_convenio("fintra", convenio);//Obtener la informacion del convenio

            negtraza.setActividad("SOL");
            negtraza.setNumeroSolicitud(num_solicitud + "");
            negtraza.setUsuario(usuario.getLogin());
            negtraza.setCausal("");
            negtraza.setComentarios("");
            negtraza.setDstrct(usuario.getDstrct());
            negtraza.setConcepto("");
            Negocios neg = new Negocios();
            neg.setCod_cli(persona.getIdentificacion());
            neg.setMod_aval("");
            neg.setMod_cust("");
            neg.setMod_rem((String) request.getParameter("rem"));
            neg.setVr_negocio(Double.parseDouble(solicitud.getValorSolicitado()));
            neg.setVr_desem(0);
            neg.setVr_aval(0);
            neg.setVr_cust(0);
            neg.setPor_rem(0);
            neg.setNodocs(Integer.parseInt(solicitud.getPlazo()));
            neg.setCreation_user(usuario.getLogin());
            neg.setDist(usuario.getDstrct());
            neg.setTneg(solicitud.getTipoNegocio());
            neg.setFpago(solicitud.getPlazoPrCuota());
            neg.setCod_tabla("0");
            neg.setEsta("");
            neg.setFecha_neg(solicitud.getFechaConsulta());
            neg.setFecha_liquidacion("0099-01-01 00:00:00");
            neg.setNitp(solicitud.getAfiliado());
            neg.setTotpagado(Double.parseDouble(valor_solicitado));
            neg.setCodigoBanco(solicitud.getBanco());
            neg.setvalor_aval("0");
            neg.setvalor_remesa("0");
            neg.setCnd_aval("");
            neg.setId_convenio(Integer.parseInt(solicitud.getIdConvenio()));
            neg.setId_remesa(0);
            neg.setId_sector(solicitud.getSector());
            neg.setId_subsector(solicitud.getSubsector());
            neg.setEstado("P");
            neg.setTasa("0");
            neg.setPorcentaje_cat(0);
            neg.setValor_capacitacion(0);
            neg.setValor_central(0);
            neg.setValor_seguro(0);
            neg.setFechatran("0099-01-01 00:00:00");
            neg.setTipoConv("Multiservicio");
            neg.setTipoProceso("PSR");

//          se comenta por solicitud del senor luis bernal ojo al futuro hay que dejarlo como estaba... 
//            if (gsaserv.aptoCredito(conve, persona, laboral, solicitud, usuario, Integer.parseInt(solicitud.getPlazo()), Double.parseDouble(solicitud.getValorSolicitado()) * Integer.parseInt(solicitud.getPlazo()), neg.getTipoProceso())) {
            if(true){
//               mensaje = gsaserv.getMensaje();
//                String comentario = gsaserv.getComentario();
//                negtraza.setCausal(gsaserv.getCausal());
//                if (!gsaserv.getEstado_neg().equals("")) {
//                    neg.setEstado(gsaserv.getEstado_neg());
//                }
//                OfertaElca ofeca = new OfertaElca();
//                //validar si el negocio es rechazado por datacredito con unica causal limite maximo de endeudamiento
//                if (neg.getEstado().equals("R")) {
//                    if (gsaserv.validarCausalMLE(persona.getIdentificacion(), gsaserv.getNitEmpresa()))//validar la si la causal es LMD
//                    {
//                        neg.setEstado("P");
//
//                        mensaje += "Negocio: Continua Proceso";
//
//                        ofeca.setEstado_cartera("010");
//                    } else {
//
//                        //validamos si el cliente es factible de credito segun riesgos... 
//                        if (gsaserv.ValidarClienteSaltaData(miId)) {
//
//                            //ponemos el negocios en aceptado, mandamos el mensaje y ponemos estado cartera en aprobado
//                            neg.setEstado("P");
//                            mensaje += "Negocio: Continua Proceso";
//                            ofeca.setEstado_cartera("010");
//
//                        } else {
//
//                            ofeca.setEstado_cartera("000");
//                            mensaje += "Negocio: Rechazado";
//
//                        }
//
//                    }
//                } else {
//                    neg.setEstado("P");
//
//                    mensaje += "Negocio: Continua Proceso";
//
//                    ofeca.setEstado_cartera("010");
//                }
//
//                if (comentario.equals("")) {
//                    comentario = mensaje;
//                }
//               
                /* este bloque es temporal*/
                OfertaElca ofeca = new OfertaElca();
                neg.setEstado("P");
                mensaje += "Negocio: Continua Proceso";
                ofeca.setEstado_cartera("010");
                String comentario="no consulta datacredito: LBERNAL";
                /* este bloque es temporal*/
                
                negtraza.setComentarios(comentario);
                negtraza.setConcepto(gsaserv.getResdeudor());

                ofeca.setId_cliente(persona.getCodcli());
                ofeca.setNic(request.getParameter("nic") != null ? request.getParameter("nic") : "");
                ofeca.setDescripcion(request.getParameter("situacion") != null ? request.getParameter("situacion") : "");
                ofeca.setTipo_solicitud(request.getParameter("tipo_solicitud"));
                ofeca.setOficial("0");
                ofeca.setAviso(solicitud.getNumeroSolicitud());
                String responsable = request.getParameter("responsable") != null ? request.getParameter("responsable") : "";
                ofeca.setResponsable(responsable);
                ofeca.setTipoDtf("43");
                ofeca.setCreation_user(usuario.getLogin());
                String fecha_actual = Util.getFechaActual_String(9);
                ofeca.setFec_val_cartera(fecha_actual);
                SerieGeneral s_id_cliente = clserv.getSerie("FINV", "OP", "OFMS");
                clserv.setSerie("FINV", "OP", "OFMS");
                ofeca.setId_solicitud(s_id_cliente.getUltimo_prefijo_numero());
                tService.getSt().addBatch(clserv.insertarOf(ofeca));
                String accion1 = "";
                String accion2 = "";
                s_id_cliente = clserv.getSerie("FINV", "OP", "OFMS");
                clserv.setSerie("FINV", "OP", "OFMS");

                if (ofeca.getEstado_cartera().equals("010")) {
                    
                    AccionesEca acceca = new AccionesEca();
                    acceca.setDescripcion("Suministro de equipos");
                    acceca.setContratista(request.getParameter("proveedor") != null ? request.getParameter("proveedor") : "");
                    acceca.setId_solicitud(ofeca.getId_solicitud());
                    acceca.setCreation_user(usuario.getLogin());

                    s_id_cliente = clserv.getSerie("FINV", "OP", "ACMS");
                    clserv.setSerie("FINV", "OP", "ACMS");
                    acceca.setId_accion(s_id_cliente.getUltimo_prefijo_numero());
                    accion1 = acceca.getId_accion();
                    String trabajo="";
                     for (int i = 0; i <= filas; i++) {
                         trabajo+=(request.getParameter("nmb_equipo"+i) != null ? request.getParameter("nmb_equipo"+i) : "")+" "+((request.getParameter("cantidad"+i) != null && request.getParameter("cantidad"+i).equals("") == false) ?("-Cantidad: "+ Integer.parseInt(request.getParameter("cantidad"+i))) :("-Cantidad: "+ 1))+"\n";
                     }
                    acceca.setAlcance(trabajo);
                    acceca.setTipo_trabajo("Suministros equipo");
                    tService.getSt().addBatch(clserv.insertarAcc(fecha_vis_plan, acceca));
                    CotizacionService ctserv = new CotizacionService();
                    Cotizacion cot = null;
                    cot = new Cotizacion();
                    cot.setAccion(acceca.getId_accion());
                    cot.setCodigo(ctserv.getConsecutivo(acceca.getId_accion(), fecha_actual));
                    tService.getSt().addBatch(ctserv.insertarCotizacion(cot, usuario.getLogin()));
                    cot.setFecha(fecha_actual);
                    for (int i = 0; i <= filas; i++) {
                        String equipo = request.getParameter("equipo" + i) != null ? request.getParameter("equipo" + i) : "";
                        int cantidad = (request.getParameter("cantidad" + i) != null && request.getParameter("cantidad" + i).equals("") == false) ? Integer.parseInt(request.getParameter("cantidad" + i)) : 1;

                        cot.setCantidad(cantidad);
                        cot.setMaterial(equipo);//codigo material
                        cot.setObservacion("");
                        cot.setCompra("N");
                        cot.setCantidad_compra(0);
                        cot.setValor(aaserv.getPrecioMaterial(equipo));

                        tService.getSt().addBatch(ctserv.insertarCotizacionDet(cot, usuario.getLogin()));
                    }


                    tService.getSt().addBatch(ctserv.actualizarAccion(acceca.getId_accion(), usuario.getLogin()));
                    tService.getSt().addBatch(ctserv.actualizarEstadoAccion(acceca.getId_accion(), usuario.getLogin(), "060"));

                    acceca = new AccionesEca();
                    String instalador=request.getParameter("instalador") != null ?request.getParameter("instalador") : request.getParameter("contratista")!= null ?request.getParameter("contratista"):"" ;
                    acceca.setDescripcion("Instalacion de equipos");
                    acceca.setContratista(instalador);
                    acceca.setId_solicitud(ofeca.getId_solicitud());
                    acceca.setCreation_user(usuario.getLogin());

                    s_id_cliente = clserv.getSerie("FINV", "OP", "ACMS");
                    clserv.setSerie("FINV", "OP", "ACMS");
                    acceca.setId_accion(s_id_cliente.getUltimo_prefijo_numero());
                    accion2 = acceca.getId_accion();
                    acceca.setAlcance("");
                    acceca.setTipo_trabajo("");
                    tService.getSt().addBatch(clserv.insertarAcc(fecha_vis_plan, acceca     ));
                    cot.setAccion(acceca.getId_accion());
                    cot.setCodigo(ctserv.getConsecutivo(acceca.getId_accion(), fecha_actual));
                    cot.setFecha(fecha_actual);
                    tService.getSt().addBatch(ctserv.insertarCotizacion(cot, usuario.getLogin()));
                    for (int i = 0; i <= filas; i++) {
                        String instalacion = request.getParameter("instalacion"+i) != null ? request.getParameter("instalacion"+i) : "";
                        int cantidad = (request.getParameter("cantidad"+i) != null && request.getParameter("cantidad"+i).equals("") == false) ? Integer.parseInt(request.getParameter("cantidad"+i)) : 1;
                        
                        cot.setCantidad(cantidad);                    
                        cot.setMaterial(instalacion);//codigo material
                        cot.setObservacion("");
                        cot.setCompra("N");
                        cot.setCantidad_compra(0);
                        cot.setValor(aaserv.getPrecioMaterial(instalacion));
                        
                        tService.getSt().addBatch(ctserv.insertarCotizacionDet(cot, usuario.getLogin()));

                    }
                    tService.getSt().addBatch(ctserv.actualizarAccion(acceca.getId_accion(), usuario.getLogin()));

                    // se genera la Oferta
                    String consecutivo = model.ElectricaribeOfertaSvc.getConsecutivoOferta();
                    DateFormat dateFormat = new SimpleDateFormat("yyyyMM");
                    Date d = new Date();
                    consecutivo = d.toString().substring(d.toString().length() - 2) + ".OPA" + consecutivo;
                    String tipo_sol = gsaserv.refTablagen("TIPO_OFERT", ofeca.getTipo_solicitud());
                    consecutivo = consecutivo + tipo_sol;
                    ofeca.setConsecutivo_oferta(consecutivo);
                    ofeca.setUser_update(usuario.getLogin());                    
                    tService.getSt().addBatch(model.ElectricaribeOfertaSvc.actualizarOferta(ofeca));
                    tService.getSt().addBatch(model.ElectricaribeOfertaSvc.actPrecio_venta(ofeca.getId_solicitud(), dateFormat.format(d)));
                    BeanGeneral visita = new BeanGeneral();
                    visita.setValor_01(ofeca.getId_solicitud());
                    visita.setValor_02(fecha_vis_plan);
                    visita.setValor_03(request.getParameter("hora") != null ? request.getParameter("hora") : "");
                    visita.setValor_04(instalador);
                    visita.setCreation_user(usuario.getLogin());
                    tService.getSt().addBatch(aaserv.insertarVisita(visita));
                }

                neg.setCod_negocio(ngserv.UpCP(conve.getPrefijo_negocio()));//Generar el codigo del negocio
                tService.getSt().addBatch(ngserv.insertNegocio(neg));
                tService.getSt().addBatch(ngserv.updateFormulario(neg, num_solicitud + ""));
                NegocioTrazabilidadService trazaService = new NegocioTrazabilidadService();
                tService.getSt().addBatch(trazaService.insertNegocioTrazabilidad(negtraza));
                tService.getSt().addBatch(trazaService.updateActividad(neg.getCod_negocio(), negtraza.getActividad()));
                BeanGeneral encuesta = new BeanGeneral();
                encuesta.setValor_01(request.getParameter("pregunta1") != null ? request.getParameter("pregunta1") : "");
                encuesta.setValor_02(request.getParameter("pregunta2") != null ? request.getParameter("pregunta2") : "");
                encuesta.setValor_03(request.getParameter("pregunta3") != null ? request.getParameter("pregunta3") : "");
                encuesta.setValor_04(request.getParameter("pregunta4") != null ? request.getParameter("pregunta4") : "");
                encuesta.setValor_05(request.getParameter("pregunta5") != null ? request.getParameter("pregunta5") : "");
                encuesta.setCreation_user(usuario.getLogin());
                encuesta.setValor_06(ofeca.getId_solicitud());
                tService.getSt().addBatch(aaserv.insertarEncuesta(encuesta));
                msj+="\n"+mensaje;
                try {
                    tService.execute();
                } catch (Exception e) {
                    sw = false;
                    e.printStackTrace();
                    msj = e.getMessage();
                }
                try {
                    if (sw) {
                        /***************************Envio de Correo***************************/
                        HSendMail2 hSendMail2 = new HSendMail2();
                        String[] datos_mensaje = clserv.getDatosMensaje(accion2);
                        String correosMT = gsaserv.nombreTablagen("CORREODEC", "MT");
                        if (ofeca.getEstado_cartera().equals("010")) {
                            String ruta = "";
                            String archivo = "";
                            if (gsaserv.exportarPdf(num_solicitud, usuario.getLogin(), "")) {

                                ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
                                ruta = rb.getString("ruta") + "/exportar/migracion/" + usuario.getLogin() + "/";
                                SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMdd");
                                archivo = "solicitud_aval_" + num_solicitud + "_" + fmt.format(new Date()) + ".pdf";
                            }else{
                                msj+=" pero el formulario adjunto no fue enviado al instalador, debe enviarse manualmente";
                            }


                          hSendMail2.start("opav_asignacion@fintravalores.com",
                                   /* datos_mensaje[3]*/"ivargas@geotech.com.co",/* correosMT*/"", "", "Solicitud " + ofeca.getId_solicitud(),
                                    "Se�ores " + datos_mensaje[0] + ", Zona " + datos_mensaje[1] + ": se ha creado la solicitud " + ofeca.getId_solicitud()
                                    + " para el cliente  " + persona.getNombre()+ " ubicado en la direccion "+persona.getDireccion()+" y telefono No. "+persona.getTelefono()+
                                    ". La visita quedo agendada para el dia "+fecha_vis_plan+" y hora  "+request.getParameter("hora"), usuario.getLogin(), ruta, archivo);
                        }

                        /*************************Fin Envio de Correo*************************/
                        /***************************Envio de Correo***************************/
                        hSendMail2 = new HSendMail2();
                        hSendMail2.start("opav_asignacion@fintravalores.com",
                               /* correosMT*/"ivargas@geotech.com.co",
                                "", "", "Nueva Solicitud " + ofeca.getId_solicitud(),
                                "Se han creado un solicitud nueva identificada con el codigo " + ofeca.getId_solicitud()+ ", asociada al cliente " + datos_mensaje[2], usuario.getLogin(), "", "");
                        /************************Fin Envio de Correo**************************/
                    }
                } catch (Exception e) {
                    sw = false;
                    msj = e.toString();
                    e.printStackTrace();
                    System.out.println("Error:" + e.toString());
                }
            } else {
                msj = gsaserv.getMensaje();
            }
        } catch (Exception e) {
            System.out.println("error en action: " + e.toString());
            msj = e.toString();
            e.printStackTrace();
        }
        next = "/jsp/opav/mensaje_solicitud_equipo.jsp?mensaje=" + msj;
    }

    /**
     * Busca las posibles horas de una visita
     * @throws Exception
     */
    private void cargarHoras() throws Exception {
        redirect = false;
        String fecha = request.getParameter("fecha") != null ? request.getParameter("fecha") : "";
        String instalador = request.getParameter("instalador") != null ? request.getParameter("instalador") : "";
        ArrayList horas = aaserv.cargarHorasDisponibles(fecha, instalador);
        redirect = false;
        try {
            cadenaWrite = "<select id='hora' name='hora' onchange='puedeHacerVisita()'>";
            cadenaWrite += "<option value=''>...</option>";
            String[] dato1 = null;
            for (int i = 0; i < horas.size(); i++) {
                dato1 = ((String) horas.get(i)).split(";_;");               
                    cadenaWrite += " <option value='" + dato1[0] + "'>" + dato1[1] + "</option>";                
            }
            cadenaWrite += "</select>";
        } catch (Exception e) {
            System.out.println("error(action): " + e.toString());
            e.printStackTrace();
        }
    }


    /**
     * Busca las posibles horas de una visita
     * @throws Exception
     */
    private void puedeRealizarVisita() throws Exception {
        try{
        redirect = false;
        String hora = request.getParameter("hora") != null ? request.getParameter("hora") : "";
        String fecha = request.getParameter("fecha") != null ? request.getParameter("fecha").substring(0, 10) : "";
        String contratista = request.getParameter("contratista") != null ? request.getParameter("contratista") : "";
        cadenaWrite=aaserv.puedeRealizarVisita(contratista, fecha, hora)+"";
        } catch (Exception e) {
            System.out.println("error(action): " + e.toString());
            e.printStackTrace();
        }
    }

    /**
     * Busca el valor de finaciacion de un credito a un plazo determinado
     * @throws Exception
     */
    private void getValorFinanciacion() throws Exception {
        redirect = false;

        int plazo = request.getParameter("plazo") != null ? Integer.parseInt(request.getParameter("plazo")) : 1;
        double equipo = request.getParameter("equipo") != null ? Double.parseDouble(request.getParameter("equipo")) : 0;
        double instalacion = request.getParameter("instalacion") != null ? Double.parseDouble(request.getParameter("instalacion")) : 0;

        String precio = "0";
        try {
            precio = Util.customFormat(aaserv.getValorFinanciado(equipo+instalacion, plazo));

        } catch (Exception e) {
            System.out.println("error en action: " + e.toString());
            e.printStackTrace();
        }
        cadenaWrite = precio + "";
    }



}
