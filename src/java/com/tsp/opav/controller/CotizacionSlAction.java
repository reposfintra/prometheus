/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.opav.controller;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.tsp.exceptions.InformationException;
import com.tsp.opav.model.DAOS.CotizacionSlDAO;
import com.tsp.opav.model.DAOS.impl.CotizacionSlImpl;

import com.tsp.operation.model.beans.Usuario;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;

/**
 *
 * @author user
 */
public class CotizacionSlAction extends Action {

    private final int CARGAR_GRID_ACCION = 1;
    private final int CARGAR_DATOS_CABECERA = 2;
    private final int CARGAR_GRID_APU = 3;
    private final int CARGAR_APU_X_GRUPO = 4;
    private final int GUARDAR_COTIZACION = 5;
    private final int CARGAR_COMBO_METODO_CALCULO = 6;
    private final int METODO_CALCULO_X_LINEA = 7;
    private final int CARGAR_TABLA_DETALLADA = 8;
    private final int CARGAR_COSTOS_ADMON_DEFAULT = 9;
    private final int CARGAR_UNIDADES_ADMON = 10;
    private final int CARGAR_ITEMS_CATEGORIA_ADMON = 11;
    private final int GUARDAR_COSTOS_ADMON_CATEGORIA = 12;
    private final int GUARDAR_COSTOS_ADMON_PROYECTO = 13;
    private final int ANULAR_COSTO_ADMON_PROYECTO = 14;
    private final int CARGAR_GRID_ACCION_W = 15;
    private final int CARGAR_DATOS_CABECERA_W = 16;
    private final int CARGAR_GRID_APU_W = 17;
    private final int CARGAR_APU_X_GRUPO_W = 18;
    private final int GUARDAR_COTIZACION_W = 19;
    private final int CARGAR_COMBO_METODO_CALCULO_W = 20;
    private final int METODO_CALCULO_X_LINEA_W = 21;
    private final int CARGAR_TABLA_DETALLADA_W = 22;
    private final int CARGAR_COMBO_W = 23;
    private final int CARGAR_PORCENTAJES = 24;
    private final int INSERTAR_PORCENTAJES_GLOBAL_CONTRATISTA = 25;
    private final int INSERTAR_RENTABILIDADES_CABECERA = 26;
    private final int OBTENER_VALOR_ITEM_BY_DEFAULT = 27;
    private final int INSERTAR_PORCENTAJES_CONTRATISTA = 28;
    private final int CARGAR_TOTALES_TIPO_INSUMO = 29;
    private final int CARGAR_TOTALES_CATEGORIA = 30;

    

    private JsonObject respuesta;
    private CotizacionSlDAO dao;
    Usuario usuario = null;
    private String txtResp;

    @Override
    public void run() throws ServletException, InformationException {

        try {
            HttpSession session = request.getSession();
            usuario = (Usuario) session.getAttribute("Usuario");
            dao = new CotizacionSlImpl(usuario.getBd());
            int opcion = (request.getParameter("opcion") != null)
                    ? Integer.parseInt(request.getParameter("opcion"))
                    : -1;
            switch (opcion) {
                case CARGAR_GRID_ACCION:
                    this.cargar_grid_accion();
                    break;
                case CARGAR_DATOS_CABECERA:
                    this.cargar_datos_cabecera();
                    break;
                case CARGAR_GRID_APU:
                    this.getAPUs();
                    break;
                case CARGAR_APU_X_GRUPO:
                    this.getAPUXGrupo();
                    break;
                case GUARDAR_COTIZACION:
                    this.guardarCotizacion();
                    break;
                case CARGAR_COMBO_METODO_CALCULO:
                    this.getMetodoCalculo();
                    break;
                case METODO_CALCULO_X_LINEA:
                    this.getMetodoCalculoXLinea();
                    break;
                case CARGAR_TABLA_DETALLADA:
                    this.CargarTablaDetallada();
                    break;
                    case CARGAR_GRID_ACCION_W:
                    this.cargar_grid_accion_w();
                    break;
                case CARGAR_DATOS_CABECERA_W:
                    this.cargar_datos_cabecera_w();
                    break;
                case CARGAR_GRID_APU_W:
                    this.getAPUs_w();
                    break;
                case CARGAR_APU_X_GRUPO_W:
                    this.getAPUXGrupo_w();
                    break;
                case GUARDAR_COTIZACION_W:
                    this.guardarCotizacion_w();
                    break;
                case CARGAR_COMBO_METODO_CALCULO_W:
                    this.getMetodoCalculo_w();
                    break;
                case METODO_CALCULO_X_LINEA_W:
                    this.getMetodoCalculoXLinea_w();
                    break;
                case CARGAR_TABLA_DETALLADA_W:
                    this.CargarTablaDetallada_w();
                    break;
                case CARGAR_COMBO_W:
                    this.cargar_Combo_W();
                    break;
                case CARGAR_PORCENTAJES:
                    this.cargar_porcentajes();
                    break;
                case CARGAR_COSTOS_ADMON_DEFAULT:
                    this.cargar_grid_costos_admon();
                    break;
                case CARGAR_UNIDADES_ADMON:
                    this.listarUndsAdmon();
                    break;
                case CARGAR_ITEMS_CATEGORIA_ADMON:
                    this.listarItemsCategoria();
                    break;
                case GUARDAR_COSTOS_ADMON_CATEGORIA:
                    this.guardarCostosAdmonCategoria();
                    break;
                case GUARDAR_COSTOS_ADMON_PROYECTO:
                    this.guardarCostosAdmonAll();
                    break;
                case ANULAR_COSTO_ADMON_PROYECTO:
                    anularCostoAdmonProyecto();
                    break;
                case INSERTAR_PORCENTAJES_GLOBAL_CONTRATISTA:
                    insertar_Porcentajes_Global_Contratista();
                    break;
                case INSERTAR_RENTABILIDADES_CABECERA:
                    insertar_rentabilidades_cabecera();
                    break;
                case OBTENER_VALOR_ITEM_BY_DEFAULT:
                    obtenerValorItemDefault();
                    break;
                case INSERTAR_PORCENTAJES_CONTRATISTA:
                    insertar_Porcentajes_Contratista();
                    break;
                case CARGAR_TOTALES_TIPO_INSUMO:
                    cargar_totales_tipo_insumo();
                    break;
                case CARGAR_TOTALES_CATEGORIA:
                    cargar_totales_categoria();
                    break;

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void getMetodoCalculo() {
        try {
            txtResp = dao.cargarMetodoCalculo();
            this.printlnResponse(txtResp, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getMetodoCalculoXLinea() {
        try {
            String metodo = request.getParameter("metodo");
            String codigo = request.getParameter("codigo");
            txtResp = dao.cargarMetodoCalculoXLinea(metodo, codigo);
            this.printlnResponse(txtResp, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void cargar_grid_accion() throws IOException {
        String apugrupo = request.getParameter("apugrupo");
        String metcalc = request.getParameter("metcalc");
        String id = request.getParameter("id");

         
        respuesta = dao.cargarGridAccionExiste(id);
        
//        if (!dao.existeCotizacion(id)) {
//            respuesta = dao.cargarGridAccion(apugrupo, metcalc, id);
//        } else {
//            respuesta = dao.cargarGridAccionExiste(id);
//        }

        response.setContentType("application/json; charset=utf-8");
        response.setHeader("Cache-Control", "no-cache");
        response.getWriter().println((new Gson()).toJson(respuesta));
    }

    private void cargar_datos_cabecera() {
        try {
            String id = request.getParameter("id_accion");
            if (!dao.existeCotizacion(id)) {
                txtResp = dao.cargardatoscabecera(id);
            } else {
                txtResp = dao.datosExistecabecera(id);
            }
            this.printlnResponse(txtResp, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void printlnResponse(String respuesta, String contentType) throws Exception {
        response.setContentType(contentType + " charset=UTF-8;");
        response.setHeader("Cache-Control", "no-store");
        response.setDateHeader("Expires", 0);
        response.getWriter().println(respuesta);

    }

    private void getAPUs() throws IOException {
        respuesta = dao.cargarAPUs();
        response.setContentType("application/json; charset=utf-8");
        response.setHeader("Cache-Control", "no-cache");
        response.getWriter().println((new Gson()).toJson(respuesta));
    }

    private void getAPUXGrupo() throws IOException {
        String grupo_apu = request.getParameter("grupo_apu");
        String apugrupo = request.getParameter("apugrupo");
        respuesta = dao.cargarAPUXGrupo(grupo_apu, apugrupo);
        response.setContentType("application/json; charset=utf-8");
        response.setHeader("Cache-Control", "no-cache");
        response.getWriter().println((new Gson()).toJson(respuesta));
    }

    private void guardarCotizacion() throws IOException {
        JsonObject info = (JsonObject) (new JsonParser()).parse(request.getParameter("informacion"));
        JsonObject info_detalle = (JsonObject) (new JsonParser()).parse(request.getParameter("informacion_detallada"));
        info.addProperty("usuario", ((Usuario) request.getSession().getAttribute("Usuario")).getLogin());
        info.addProperty("dstrct", ((Usuario) request.getSession().getAttribute("Usuario")).getDstrct());
        info.addProperty("nomapu", (request.getParameter("nomapu")));
        info.addProperty("CodCliente", (request.getParameter("CodCliente")));
        info.addProperty("NombreCliente", (request.getParameter("NombreCliente")));
        info.addProperty("CodCotizacion", (request.getParameter("CodCotizacion")));
        info.addProperty("fecha", (request.getParameter("fecha")));
        info.addProperty("visualizacion", (request.getParameter("visualizacion")));
        info.addProperty("modalidad", (request.getParameter("modalidad")));
        info.addProperty("valcotizacion", (request.getParameter("valcotizacion")));
        info.addProperty("valdesc", (request.getParameter("valdesc")));
        info.addProperty("subtotal", (request.getParameter("subtotal")));
        info.addProperty("perc_iva", (request.getParameter("perc_iva")));
        info.addProperty("valiva", (request.getParameter("valiva")));
        info.addProperty("perc_admon", (request.getParameter("perc_admon")));
        info.addProperty("val_admon", (request.getParameter("val_admon")));
        info.addProperty("perc_imprevisto", (request.getParameter("perc_imprevisto")));
        info.addProperty("val_imprevisto", (request.getParameter("val_imprevisto")));
        info.addProperty("perc_utilidad", (request.getParameter("perc_utilidad")));
        info.addProperty("val_utilidad", (request.getParameter("val_utilidad")));
        info.addProperty("perc_aiu", (request.getParameter("perc_aiu")));
        info.addProperty("val_aiu", (request.getParameter("val_aiu")));
        info.addProperty("val_total", (request.getParameter("val_total")));
        info.addProperty("anticipo", (request.getParameter("anticipo")));
        info.addProperty("perc_anticipo", (request.getParameter("perc_anticipo")));
        info.addProperty("val_anticipo", (request.getParameter("val_anticipo")));
        info.addProperty("retegarantia", (request.getParameter("retegarantia")));
        info.addProperty("perc_rete", (request.getParameter("perc_rete")));
        info.addProperty("idaccion", (request.getParameter("idaccion")));
        info.addProperty("val_material", (request.getParameter("val_material")));
        info.addProperty("val_mano_obra", (request.getParameter("val_mano_obra")));
        info.addProperty("val_equipos", (request.getParameter("val_equipos")));
        info.addProperty("val_herramientas", (request.getParameter("val_herramientas")));
        info.addProperty("val_transporte", (request.getParameter("val_transporte")));
        info.addProperty("val_tramites", (request.getParameter("val_tramites")));
        info.addProperty("existe", (request.getParameter("existe")));
        info.addProperty("id", (request.getParameter("id")));
        respuesta = dao.guardarCotizacion(info, info_detalle);
        response.setContentType("application/json; charset=utf-8");
        response.setHeader("Cache-Control", "no-cache");
        response.getWriter().println((new Gson()).toJson(respuesta));
    }

    private void CargarTablaDetallada() throws IOException {
        String id_accion = request.getParameter("id");


        
        if (!dao.existeCotizacion(id_accion)) {
            respuesta = dao.cargarTablaDetallada(id_accion);
        } else {
            //respuesta = dao.cargarTablaDetallada(id_accion);
            respuesta = dao.cargarTablaDetalladaExiste(id_accion);
        }

        response.setContentType("application/json; charset=utf-8");
        response.setHeader("Cache-Control", "no-cache");
        response.getWriter().println((new Gson()).toJson(respuesta));
    }
    
    private void getMetodoCalculo_w() {
        try {
            txtResp = dao.cargarMetodoCalculo_w();
            this.printlnResponse(txtResp, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getMetodoCalculoXLinea_w() {
        try {
            String metodo = request.getParameter("metodo");
            String codigo = request.getParameter("codigo");
            txtResp = dao.cargarMetodoCalculoXLinea_w(metodo, codigo);
            this.printlnResponse(txtResp, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void cargar_grid_accion_w() throws IOException {
        String apugrupo = request.getParameter("apugrupo");
        String metcalc = request.getParameter("metcalc");
        String id = request.getParameter("id");
        String op = request.getParameter("op");
        String filtro = request.getParameter("filtro");
        String id_rel_actividades_apu = request.getParameter("id_rel_actividades_apu");

        if (!dao.existeCotizacion_w(id)) {
            respuesta = dao.cargarGridAccion_w(apugrupo, metcalc, id);
        } else {
            respuesta = dao.cargarGridAccionExiste_w(id,op,filtro,id_rel_actividades_apu);
        }

        response.setContentType("application/json; charset=utf-8");
        response.setHeader("Cache-Control", "no-cache");
        response.getWriter().println((new Gson()).toJson(respuesta));
    }

    private void cargar_datos_cabecera_w() {
        try {
            String id = request.getParameter("id_accion");
            if (!dao.existeCotizacion_w(id)) {
                txtResp = dao.cargardatoscabecera_w(id);
            } else {
                txtResp = dao.datosExistecabecera_w(id);
            }
            this.printlnResponse(txtResp, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

   

    private void getAPUs_w() throws IOException {
        respuesta = dao.cargarAPUs_w();
        response.setContentType("application/json; charset=utf-8");
        response.setHeader("Cache-Control", "no-cache");
        response.getWriter().println((new Gson()).toJson(respuesta));
    }

    private void getAPUXGrupo_w() throws IOException {
        String grupo_apu = request.getParameter("grupo_apu");
        String apugrupo = request.getParameter("apugrupo");
        respuesta = dao.cargarAPUXGrupo_w(grupo_apu, apugrupo);
        response.setContentType("application/json; charset=utf-8");
        response.setHeader("Cache-Control", "no-cache");
        response.getWriter().println((new Gson()).toJson(respuesta));
    }

    private void guardarCotizacion_w() throws IOException {
        JsonObject info = (JsonObject) (new JsonParser()).parse(request.getParameter("informacion"));
        JsonObject info_detalle = (JsonObject) (new JsonParser()).parse(request.getParameter("informacion_detallada"));
        info.addProperty("usuario", ((Usuario) request.getSession().getAttribute("Usuario")).getLogin());
        info.addProperty("dstrct", ((Usuario) request.getSession().getAttribute("Usuario")).getDstrct());
        info.addProperty("nomapu", (request.getParameter("nomapu")));
        info.addProperty("CodCliente", (request.getParameter("CodCliente")));
        info.addProperty("NombreCliente", (request.getParameter("NombreCliente")));
        info.addProperty("CodCotizacion", (request.getParameter("CodCotizacion")));
        info.addProperty("fecha", (request.getParameter("fecha")));
        info.addProperty("visualizacion", (request.getParameter("visualizacion")));
        info.addProperty("modalidad", (request.getParameter("modalidad")));
        info.addProperty("valcotizacion", (request.getParameter("valcotizacion")));
        info.addProperty("valdesc", (request.getParameter("valdesc")));
        info.addProperty("subtotal", (request.getParameter("subtotal")));
        info.addProperty("perc_iva", (request.getParameter("perc_iva")));
        info.addProperty("valiva", (request.getParameter("valiva")));
        info.addProperty("perc_admon", (request.getParameter("perc_admon")));
        info.addProperty("val_admon", (request.getParameter("val_admon")));
        info.addProperty("perc_imprevisto", (request.getParameter("perc_imprevisto")));
        info.addProperty("val_imprevisto", (request.getParameter("val_imprevisto")));
        info.addProperty("perc_utilidad", (request.getParameter("perc_utilidad")));
        info.addProperty("val_utilidad", (request.getParameter("val_utilidad")));
        info.addProperty("perc_aiu", (request.getParameter("perc_aiu")));
        info.addProperty("val_aiu", (request.getParameter("val_aiu")));
        info.addProperty("val_total", (request.getParameter("val_total")));
        info.addProperty("anticipo", (request.getParameter("anticipo")));
        info.addProperty("perc_anticipo", (request.getParameter("perc_anticipo")));
        info.addProperty("val_anticipo", (request.getParameter("val_anticipo")));
        info.addProperty("retegarantia", (request.getParameter("retegarantia")));
        info.addProperty("perc_rete", (request.getParameter("perc_rete")));
        info.addProperty("idaccion", (request.getParameter("idaccion")));
        info.addProperty("val_material", (request.getParameter("val_material")));
        info.addProperty("val_mano_obra", (request.getParameter("val_mano_obra")));
        info.addProperty("val_equipos", (request.getParameter("val_equipos")));
        info.addProperty("val_herramientas", (request.getParameter("val_herramientas")));
        info.addProperty("val_transporte", (request.getParameter("val_transporte")));
        info.addProperty("val_tramites", (request.getParameter("val_tramites")));
        info.addProperty("existe", (request.getParameter("existe")));
        info.addProperty("id", (request.getParameter("id")));
        respuesta = dao.guardarCotizacion_w(info, info_detalle);
        response.setContentType("application/json; charset=utf-8");
        response.setHeader("Cache-Control", "no-cache");
        response.getWriter().println((new Gson()).toJson(respuesta));
    }

    private void CargarTablaDetallada_w() throws IOException {
            String id_accion = request.getParameter("id");

        if (!dao.existeCotizacion_w(id_accion)) {
            respuesta = dao.cargarTablaDetallada_w(id_accion);
        } else {
            respuesta = dao.cargarTablaDetalladaExiste_w(id_accion);
        }

        response.setContentType("application/json; charset=utf-8");
        response.setHeader("Cache-Control", "no-cache");
        response.getWriter().println((new Gson()).toJson(respuesta));
    }
    
    private void cargar_grid_costos_admon() {
        try {
            String id_solicitud = (request.getParameter("num_solicitud") != null) ? request.getParameter("num_solicitud") : "";
            String id_categoria = (request.getParameter("id_categoria") != null) ? request.getParameter("id_categoria") : "";
            if (!dao.existenCostosAdmonProyecto(id_solicitud)) {
                txtResp = dao.cargarCostosAdmon("cargarCostosAdmonByDefault",id_categoria,id_solicitud);
            } else {
                txtResp = dao.cargarCostosAdmon("cargarCostosAdmonProyectoByCat",id_categoria,id_solicitud);
            }
            this.printlnResponse(txtResp, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private void listarUndsAdmon() {
        try {
            txtResp = dao.listarUndsAdmon();
            this.printlnResponse(txtResp, "application/json;");
        } catch (Exception ex) {           
            ex.printStackTrace();
        }
    }
    
    private void listarItemsCategoria() {
        try {
            String id_categoria = (request.getParameter("id_categoria") != null) ? request.getParameter("id_categoria") : "";
            txtResp = dao.listarItemsCategoria(id_categoria);
            this.printlnResponse(txtResp, "application/json;");
        } catch (Exception ex) {           
            ex.printStackTrace();
        }
    }
    
        
    private void guardarCostosAdmonCategoria() {
        try {
            String num_solicitud = (request.getParameter("num_solicitud") != null) ? request.getParameter("num_solicitud") : "";
            String id_categoria = (request.getParameter("id_categoria") != null) ? request.getParameter("id_categoria") : "";
            JsonObject items = (JsonObject) (new JsonParser()).parse(request.getParameter("listadoitems"));
            txtResp = dao.guardarCostosAdmon(num_solicitud,id_categoria,items,usuario);
            this.printlnResponse(txtResp, "application/json;");
        } catch (Exception ex) {           
            ex.printStackTrace();
        }
    }
    
    private void guardarCostosAdmonAll() {
        try {
            String num_solicitud = (request.getParameter("num_solicitud") != null) ? request.getParameter("num_solicitud") : "";        
            JsonObject items = (JsonObject) (new JsonParser()).parse(request.getParameter("listadoitems"));
            txtResp = dao.guardarCostosAdmon(num_solicitud,items,usuario);
            this.printlnResponse(txtResp, "application/json;");
        } catch (Exception ex) {           
            ex.printStackTrace();
        }
    }
    
       public void anularCostoAdmonProyecto() {
 
        try {
            String id = request.getParameter("id") != null ? request.getParameter("id") : "";           
            txtResp = dao.anularCostoAdmonProyecto(id,usuario);
            this.printlnResponse(txtResp, "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    } 
    

    private void cargar_Combo_W() {
        try {
            String op = request.getParameter("op") == null ? "" : request.getParameter("op");
            String json = dao.cargar_Combo_W(op);
            this.printlnResponseAjax(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void cargar_porcentajes() {
        try {
                String idaccion = request.getParameter("idaccion") == null ? "" : request.getParameter("idaccion");
            this.printlnResponseAjax(dao.cargar_Porcentajes(idaccion), "application/json;");
        } catch (Exception ex) {

            ex.printStackTrace();
        }
    }

    private void insertar_Porcentajes_Global_Contratista() {
        try {
            String id = request.getParameter("id") == null ? "" : request.getParameter("id");
            String porcentaje = request.getParameter("porcentaje") == null ? "" : request.getParameter("porcentaje");
            String idaccion = request.getParameter("idaccion") == null ? "" : request.getParameter("idaccion");
            String json = dao.insertar_Porcentajes_Global_Contratista(id,porcentaje,idaccion);
            this.printlnResponseAjax(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void insertar_rentabilidades_cabecera() {
        try {
            String idaccion = request.getParameter("idaccion") == null ? "" : request.getParameter("idaccion");
            String subtotal = request.getParameter("subtotal") == null ? "" : request.getParameter("subtotal");
            String valor_contratista = request.getParameter("valor_contratista") == null ? "" : request.getParameter("valor_contratista");
            String perc_contratista = request.getParameter("perc_contratista") == null ? "" : request.getParameter("perc_contratista");
            String valor_esquema = request.getParameter("valor_esquema") == null ? "" : request.getParameter("valor_esquema");
            String perc_esquema = request.getParameter("perc_esquema") == null ? "" : request.getParameter("perc_esquema");
            String json = dao.insertar_rentabilidades_cabecera(idaccion,subtotal,valor_contratista,perc_contratista,valor_esquema,perc_esquema);
            this.printlnResponseAjax(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void obtenerValorItemDefault() { 
        try {
            String idCategoria = request.getParameter("id_cat") != null ? request.getParameter("id_cat") : "";   
            String idItem = request.getParameter("id_item") != null ? request.getParameter("id_item") : "";   
            txtResp = dao.obtenerValorItemByDefault(idCategoria, idItem);
            this.printlnResponse(txtResp, "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    } 
    
    private void insertar_Porcentajes_Contratista() {
        try {
            
            String idaccion = request.getParameter("idaccion") == null ? "" : request.getParameter("idaccion");
            JsonObject rows = (JsonObject) (new JsonParser()).parse(request.getParameter("informacion"));
            String json = dao.insertar_Porcentajes_Contratista(rows,idaccion);
            this.printlnResponseAjax(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private void cargar_totales_categoria() {
        try {
            
            String idaccion = request.getParameter("idaccion") == null ? "" : request.getParameter("idaccion");
            String idtipoinsumo = request.getParameter("idtipoinsumo") == null ? "" : request.getParameter("idtipoinsumo");
            String json = dao.cargar_totales_categoria(idaccion,idtipoinsumo);
            this.printlnResponseAjax(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private void cargar_totales_tipo_insumo() {
        try {
            String idaccion = request.getParameter("idaccion") == null ? "" : request.getParameter("idaccion");

            String json = dao.cargar_totales_tipo_insumo(idaccion);
            this.printlnResponseAjax(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    
    


}
