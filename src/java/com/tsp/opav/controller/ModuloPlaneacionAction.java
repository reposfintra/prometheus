 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.opav.controller;

/**
 *
 * @author User
 */
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonArray;
import com.tsp.exceptions.InformationException;
import com.tsp.opav.model.DAOS.ModuloPlaneacionDAO;
import com.tsp.opav.model.DAOS.impl.ModuloPlaneacionImpl;
import com.tsp.operation.model.TransaccionService;
import static com.tsp.operation.model.beans.Amortizacion.fmt;
import com.tsp.operation.model.beans.POIWrite;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.services.NegocioTrazabilidadService;
import com.tsp.util.Utility;
import com.tsp.util.Util;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;
import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.util.HSSFColor;

public class ModuloPlaneacionAction extends Action {

    private final int CARGAR_CONCEPTOS = 0;
    private final int INSERTAR_CONCEPTO = 1;
    private final int ACTUALIZAR_CONCEPTO = 2;
    private final int ANULAR_CONCEPTO = 3;
    private final int CARGAR_RIESGOS = 4;
    private final int INSERTAR_RIESGOS = 5;
    private final int ACTUALIZAR_RIESGOS = 6;
    private final int ANULAR_RIESGOS = 7;
    private final int CARGAR_INDICADORES_GESTION = 8;
    private final int INSERTAR_INDICADORES_GESTION = 9;
    private final int ACTUALIZAR_INDICADORES_GESTION = 10;
    private final int ANULAR_INDICADORES_GESTION = 11;
    private final int CARGAR_UNIDADES_MEDIDA = 12;
    private final int GUARDAR_CARGA_LABORAL = 13;
    private final int CARGAR_CARGA_LABORAL = 14;
    private final int ACTUALIZAR_CARGA_LABORAL = 15;
    private final int ANULAR_CARGA_LABORAL = 16;
    private final int CARGAR_CARGOS = 17;
    private final int CARGAR_CARGAS_LABORALES = 18;
    private final int CARGAR_CARGAS_LABORALES_ASOCIADO = 19;
    private final int ASOCIAR_CARGA_LABORAL = 20;
    private final int DESASOCIAR_CARGA_LABORAL = 21;
    private final int CARGAR_TABLA_TAREAS = 22;
    private final int CARGAR_TABLA_ASIGNACION = 23;
    private final int EXPORTAR_TAREAS = 24;
    private final int EXPORTAR_ASIGNACION = 25;

    private final int CARGAR_HITOS = 26;
    private final int INSERTAR_HITOS = 27;
    private final int ACTUALIZAR_HITOS = 28;
    private final int ANULAR_HITOS = 29;
    private final int CARGAR_ACTIVIDADES = 30;
    private final int INSERTAR_ACTIVIDAD = 31;
    private final int ACTUALIZAR_ACTIVIDAD = 32;
    private final int ANULAR_ACTIVIDAD = 33;
    private final int INGRESAR_PESO_ACTIVIDAD_ASO_CARGO = 34;
    private final int CARGAR_MATRIS_INTERESADOS = 35;
    private final int INSERTAR_MATRIS_INTERESADOS = 36;
    private final int ACTUALIZAR_MATRIS_INTERESADOS = 37;
    private final int ANULAR_ITEM_MATRIS_INTERESADOS = 38;
    private final int CARGAR_PLAN_CONTATOS = 39;
    private final int INSERTAR_PLAN_CONTATOS = 70;
    private final int ACTUALIZAR_PLAN_CONTATOS = 71;
    private final int ANULAR_PLAN_CONTATOS = 72;
    private final int CARGAR_ASPECTOS_PROYECTO = 73;
    private final int INSERTAR_ASPECTOS_PROYECTO = 74;
    private final int ACTUALIZAR_ASPECTOS_PROYECTO = 75;
    private final int LISTAR_CARGAS_LABORALES = 76;
    private final int LISTAR_INFOMACION_ACTIVIDADES = 77;
    private final int LISTARCARGASLABORALES_DETALLE = 78;
    private final int GUARDAR_ASIGNACION_PUNTAJE = 79;
    private final int EDITAR_ASIGNACION_PUNTAJE = 80;
    private final int ANULAR_ASIGNACION_PUNTAJE = 81;
    
    private final int CARGAR_FLUJO_CAJA = 40;
    private final int INSERTAR_FLUJO = 41;
    private final int ACTUALIZAR_FLUJO = 42;
    private final int ANULAR_FLUJO = 43;
    
    private final int CARGAR_INDICADOR = 44;
    private final int INSERTAR_INDICADOR = 45;
    private final int ACTUALIZAR_INDICADOR = 46;
    private final int ANULAR_INDICADOR = 47;
    
    private final int CARGAR_GESTION = 48;
    private final int INSERTAR_GESTION = 49;
    private final int ACTUALIZAR_GESTION = 50;
    private final int ANULAR_GESTION = 51;
    
    private final int CARGAR_PROYECCION = 52;
    
    private final int CARGAR_HIST_EQUIPOS = 53;
    private final int CARGAR_HIST_PERSONAL = 54;
    
    private final int INSERTAR_EQUIPOS = 55;
    private final int INSERTAR_PERSONAL = 56;
    
    private final int CARGAR_MATERIAL = 57;
    private final int INSERTAR_MATERIAL = 58;
    
    private final int CARGAR_CATEGORIA = 59;
    private final int GUARDAR_CATEGORIA = 60;
    
    private final int CARGAR_IMPACTO_X_CATEGORIA = 61;
    private final int GUARDAR_IMPACTO = 62;
    
    private final int CARGAR_RIESGOS_CI = 63;
    private final int INSERTAR_RIESGO_CI = 64;
    private final int ACTUALIZAR_RIESGO_CI = 65;
    private final int ANULAR_RIESGO_CI = 66;
    
    private final int CARGAR_IMPACTO = 67;
    private final int ACTUALIZAR_IMPACTO = 68;
    private final int ANULAR_IMPACTO = 69;
    private final int CARGAR_UNIDADES_MEDIDA_OPAV = 82;
    

    
    
    private JsonObject respuesta;
    private ModuloPlaneacionDAO dao;
    Usuario usuario = null;
    String reponseJson = "{}";
    String typeResponse = "application/json;";
    String rutaInformes;
    HSSFCellStyle header, titulo1, titulo2, titulo3, titulo4, titulo5, letra, numero, dinero, dinero2, numeroCentrado, porcentaje, letraCentrada, numeroNegrita, ftofecha;
    POIWrite xls;
    String nombre;
    private int fila = 0;

    @Override
    public void run() throws ServletException, InformationException {
        try {

            HttpSession session = request.getSession();
            usuario = (Usuario) session.getAttribute("Usuario");
            dao = new ModuloPlaneacionImpl(usuario.getBd());
            int opcion = (request.getParameter("opcion") != null)
                    ? Integer.parseInt(request.getParameter("opcion"))
                    : -1;
            switch (opcion) {
                case CARGAR_CONCEPTOS:
                    this.cargar_conceptos();
                    break;

                case INSERTAR_CONCEPTO:
                    this.insertar_Concepto();
                    break;

                case ACTUALIZAR_CONCEPTO:
                    this.actualizar_Concepto();
                    break;

                case ANULAR_CONCEPTO:
                    this.anular_Concepto();
                    break;

                case CARGAR_RIESGOS:
                    this.cargar_Riesgos();
                    break;

                case INSERTAR_RIESGOS:
                    this.insertar_Riesgos();
                    break;

                case ACTUALIZAR_RIESGOS:
                    this.actualizar_Riesgos();
                    break;

                case ANULAR_RIESGOS:
                    this.anular_Riesgos();
                    break;

                case CARGAR_INDICADORES_GESTION:
                    this.cargar_Indicadores_Gestion();
                    break;

                case INSERTAR_INDICADORES_GESTION:
                    this.insertar_Indicadores_Gestion();
                    break;

                case ACTUALIZAR_INDICADORES_GESTION:
                    this.actualizar_Indicadores_Gestion();
                    break;

                case ANULAR_INDICADORES_GESTION:
                    this.anular_Indicadores_Gestion();
                    break;

                case CARGAR_UNIDADES_MEDIDA:
                    this.cargar_Unidades_Medida();
                    break;

                case CARGAR_UNIDADES_MEDIDA_OPAV:
                    this.cargar_Unidades_Medida_Opav();
                    break;
                    

                case GUARDAR_CARGA_LABORAL:
                    this.guardar_Carga_Laboral();
                    break;

                case CARGAR_CARGA_LABORAL:
                    this.cargar_Carga_Laboral();
                    break;

                case ACTUALIZAR_CARGA_LABORAL:
                    this.actualizar_Carga_Laboral();
                    break;

                case ANULAR_CARGA_LABORAL:
                    this.anular_Carga_Laboral();
                    break;

                case CARGAR_CARGOS:
                    this.cargar_Cargos();
                    break;

                case CARGAR_CARGAS_LABORALES:
                    this.cargar_Cargas_Laborales();
                    break;

                case CARGAR_CARGAS_LABORALES_ASOCIADO:
                    this.cargar_cargas_laborales_Asociado();
                    break;

                case ASOCIAR_CARGA_LABORAL:
                    this.asociar_Carga_Laboral();
                    break;

                case DESASOCIAR_CARGA_LABORAL:
                    this.desasociar_Carga_Laboral();
                    break;

                case CARGAR_TABLA_TAREAS:
                    this.cargar_tareas();
                    break;

                case CARGAR_TABLA_ASIGNACION:
                    this.cargar_asignacion();
                    break;

                case EXPORTAR_TAREAS:
                    this.exportar_tareas();
                    break;

                case EXPORTAR_ASIGNACION:
                    this.exportar_asignacion();
                    break;

                case CARGAR_HITOS:
                    this.cargar_Hitos();
                    break;

                case INSERTAR_HITOS:
                    this.insertar_Hitos();
                    break;

                case ACTUALIZAR_HITOS:
                    this.actualizar_Hitos();
                    break;

                case ANULAR_HITOS:
                    this.anular_Hitos();
                    break;
                    
                    
                case CARGAR_FLUJO_CAJA:
                    this.Cargar_flujo_caja();
                    break;
                    
                case INSERTAR_FLUJO:
                    this.insertar_Flujo();
                    break;
                    
                case ACTUALIZAR_FLUJO:
                    this.actualizar_Flujo();
                    break;
                    
                case ANULAR_FLUJO:
                    this.anular_Flujo();
                    break;
                    
                    
                case CARGAR_INDICADOR:
                    this.cargar_Indicador();
                    break;
                    
                    
                case INSERTAR_INDICADOR:
                    this.insertar_Indicador();
                    break;

                case CARGAR_ACTIVIDADES:
                    this.cargar_Actividades();
                    break;

                case INSERTAR_ACTIVIDAD:
                    this.insertar_Actividad();
                    break;

                case ACTUALIZAR_ACTIVIDAD:
                    this.actualizar_Actividad();
                    break;

                case ANULAR_ACTIVIDAD:
                    this.anular_Actividad();
                    break;

                case INGRESAR_PESO_ACTIVIDAD_ASO_CARGO:
                    this.ingresar_Peso_Actividad_Aso_Cargo();
                    break;

                case CARGAR_MATRIS_INTERESADOS:
                    this.cargar_Matris_Interesados();
                    break;

                case INSERTAR_MATRIS_INTERESADOS:
                    this.insertar_Matris_Interesados();
                    break;

                case ACTUALIZAR_MATRIS_INTERESADOS:
                    this.actualizar_Matris_Interesados();
                    break;

                case ANULAR_ITEM_MATRIS_INTERESADOS:
                    this.anular_Item_Matris_Interesados();
                    break;

                case CARGAR_PLAN_CONTATOS:
                    this.cargar_Plan_Contatos();
                    break;

                case INSERTAR_PLAN_CONTATOS:
                    this.insertar_Plan_Contatos();
                    break;

                case ACTUALIZAR_PLAN_CONTATOS:
                    this.actualizar_Plan_Contatos();
                    break;

                case ANULAR_PLAN_CONTATOS:
                    this.anular_Item_Plan_Contatos();
                    break;

                case CARGAR_ASPECTOS_PROYECTO:
                    this.CARGAR_ASPECTOS_PROYECTO();
                    break;

                case INSERTAR_ASPECTOS_PROYECTO:
                    this.INSERTAR_ASPECTOS_PROYECTO();
                    break;

                case ACTUALIZAR_ASPECTOS_PROYECTO:
                    this.ACTUALIZAR_ASPECTOS_PROYECTO();
                    break;

                case LISTAR_CARGAS_LABORALES:
                    this.listar_Cargas_Laborales();
                    break;

                case LISTAR_INFOMACION_ACTIVIDADES:
                    this.listar_Infomacion_Actividades();
                    break;

                case LISTARCARGASLABORALES_DETALLE:
                    this.listarCargasLaborales_Detalle();
                    break;

                case GUARDAR_ASIGNACION_PUNTAJE:
                    this.guardar_Asignacion_Puntaje();
                    break;

                case EDITAR_ASIGNACION_PUNTAJE:
                    this.editar_Asignacion_Puntaje();
                    break;

                case ANULAR_ASIGNACION_PUNTAJE:
                    this.anular_Asignacion_Puntaje();
                    break;
                case ACTUALIZAR_INDICADOR:
                    this.actualizar_Indicador();
                    break;

                case ANULAR_INDICADOR:
                    this.anular_Indicador();
                    break;
                    
                    
                case CARGAR_GESTION:
                    this.Cargar_gestion();
                    break;
                    
                case INSERTAR_GESTION:
                    this.insertar_gestion();
                    break;
                    
                case ACTUALIZAR_GESTION:
                    this.actualizar_gestion();
                    break;
                    
                case ANULAR_GESTION:
                    this.anular_gestion();
                    break;
                    
                
                case CARGAR_PROYECCION:
                    this.Cargar_proyeccion();
                    break;
                    
                    
                case CARGAR_HIST_EQUIPOS:
                    this.Cargar_Equi();
                    break;
                    
                case CARGAR_HIST_PERSONAL:
                    this.Cargar_Personal();
                    break;
                    
                case INSERTAR_EQUIPOS:
                    this.insertar_equipos();
                    break;
                    
                case INSERTAR_PERSONAL:
                    this.insertar_personal();
                    break;
                    
                case CARGAR_MATERIAL:
                    this.Cargar_Material();
                    break;
                    
                case INSERTAR_MATERIAL:
                    this.insertar_material();
                    break;
                    
                case CARGAR_CATEGORIA:
                    this.cargarComboCategoria();
                    break;
                    
                case GUARDAR_CATEGORIA:
                    this.guardar_tipo_categoria();
                    break;
                    
                    
                case CARGAR_IMPACTO_X_CATEGORIA:
                    this.cargarProcesosMeta("A");
                    break;
                    
                case GUARDAR_IMPACTO:
                    this.guardar_impacto();
                    break;
                    
                //////////////////////////////////////
                case CARGAR_RIESGOS_CI:
                    this.Cargar_riesgo_ci();
                    break;
                    
                case INSERTAR_RIESGO_CI:
                    this.insertar_riesgo_ci();
                    break;
                    
                case ACTUALIZAR_RIESGO_CI:
                    this.actualizar_riesgo_ci();
                    break;
                    
                case ANULAR_RIESGO_CI:
                    this.anular_riesgo_ci();
                    break;   
                    
                case CARGAR_IMPACTO:
                    this.cargarImpacto("A");
                    break;
                    
                case ACTUALIZAR_IMPACTO:
                    this.actualizar_impacto();
                    break;
                    
                case ANULAR_IMPACTO:
                    this.anular_impacto();
                    break;
                //////////////////////////////////
                
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                this.printlnResponseAjax(reponseJson, typeResponse);
            } catch (Exception ex1) {
                Logger.getLogger(MaestroProyectoAction.class.getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }

    private void exportar_tareas() throws Exception {
        try {
            String resp1 = "";
            String url = "";
            String json = request.getParameter("listado") != null ? request.getParameter("listado") : "";
            JsonArray asJsonArray = (JsonArray) new JsonParser().parse(json);
            //Set<Map.Entry<String, JsonElement>> entrySet = object.entrySet();

            this.generarRUTA();
            this.crearLibro("Tareas_", "Tareas");
            String[] cabecera = {"Nombre Tareas", "Nivel Tarea", "Valor WBS"};

            short[] dimensiones = new short[]{5000, 5000, 5000};

            this.generaTitulos(cabecera, dimensiones);

            for (int i = 0; i < asJsonArray.size(); i++) {
                JsonObject objects = (JsonObject) asJsonArray.get(i);
                fila++;
                int col = 0;
                xls.adicionarCelda(fila, col++, objects.get("nombre").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("nivel").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("valor").getAsString(), letra);
            }

            this.cerrarArchivo();

            fmt = new SimpleDateFormat("yyyMMdd HH:mm");
            url = request.getContextPath() + "/exportar/migracion/" + usuario.getLogin().toUpperCase() + "/Tareas_" + fmt.format(new Date()) + ".xls";
            resp1 = Utility.getIcono(request.getContextPath(), 5) + "Se ha generado con exito.<br/><br/>"
                    + Utility.getIcono(request.getContextPath(), 7) + " <a href='" + url + "' >Ver Documento</a></td>";

            //this.printlnResponse(resp1, "text/plain");
            this.reponseJson = resp1;
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
    }

    private void exportar_asignacion() throws Exception {
        try {
            String resp1 = "";
            String url = "";
            String json = request.getParameter("listado") != null ? request.getParameter("listado") : "";
            JsonArray asJsonArray = (JsonArray) new JsonParser().parse(json);
            //Set<Map.Entry<String, JsonElement>> entrySet = object.entrySet();

            this.generarRUTA();
            this.crearLibro("Asignacion_", "Asignacion");
            String[] cabecera = {"Nombre Tarea", "Nombre del Recurso", "Cantidad", "Rendimiento", "Unidad", "Cantidad Apu"};

            short[] dimensiones = new short[]{5000, 5000, 5000, 5000, 5000, 5000};

            this.generaTitulos(cabecera, dimensiones);

            for (int i = 0; i < asJsonArray.size(); i++) {
                JsonObject objects = (JsonObject) asJsonArray.get(i);
                fila++;
                int col = 0;
                xls.adicionarCelda(fila, col++, objects.get("nombre").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("recurso").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("cantidad").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("rendimiento").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("unidad").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("cantidad_apu").getAsString(), letra);
            }

            this.cerrarArchivo();

            fmt = new SimpleDateFormat("yyyMMdd HH:mm");
            url = request.getContextPath() + "/exportar/migracion/" + usuario.getLogin().toUpperCase() + "/Asignacion_" + fmt.format(new Date()) + ".xls";
            resp1 = Utility.getIcono(request.getContextPath(), 5) + "Se ha generado con exito.<br/><br/>"
                    + Utility.getIcono(request.getContextPath(), 7) + " <a href='" + url + "' >Ver Documento</a></td>";

            //this.printlnResponse(resp1, "text/plain");
            this.reponseJson = resp1;
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
    }

    public void printlnResponse(String respuesta, String contentType) throws Exception {
        response.setContentType(contentType + " charset=UTF-8;");
        response.setHeader("Cache-Control", "no-store");
        response.setDateHeader("Expires", 0);
        response.getWriter().println(respuesta);

    }

    private void cerrarArchivo() throws Exception {
        try {
            if (xls != null) {
                xls.cerrarLibro();
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            throw new Exception("Error en crearArchivo ....\n" + ex.getMessage());
        }
    }

    private void generaTitulos(String[] cabecera, short[] dimensiones) throws Exception {
        try {

            fila = 0;

            for (int i = 0; i < cabecera.length; i++) {
                xls.adicionarCelda(fila, i, cabecera[i], titulo2);
                if (i < dimensiones.length) {
                    xls.cambiarAnchoColumna(i, dimensiones[i]);
                }
            }

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception("Error en generarArchivo ...\n" + ex.getMessage());
        }
    }

    private void generarRUTA() throws Exception {
        try {

            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            rutaInformes = rb.getString("ruta") + "/exportar/migracion/" + usuario.getLogin();
            File archivo = new File(rutaInformes);
            if (!archivo.exists()) {
                archivo.mkdirs();
            }

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
    }

    private void crearLibro(String nameFileParcial, String titulo) throws Exception {
        try {
            fmt = new SimpleDateFormat("yyyMMdd HH:mm");
            this.crearArchivo(nameFileParcial + fmt.format(new Date()) + ".xls", titulo);

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception("Error en generarArchivo ...\n" + ex.getMessage());
        }
    }

    private void crearArchivo(String nameFile, String titulo) throws Exception {
        try {
            InitArchivo(nameFile);
            xls.obtenerHoja("hoja1");
            // xls.combinarCeldas(0, 0, 0, 8);
            // xls.adicionarCelda(0,0, titulo, header);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception("Error en crearArchivo ....\n" + ex.getMessage());
        }
    }

    private void InitArchivo(String nameFile) throws Exception {
        try {
            xls = new com.tsp.operation.model.beans.POIWrite();
            nombre = "/exportar/migracion/" + usuario.getLogin() + "/" + nameFile;
            xls.nuevoLibro(rutaInformes + "/" + nameFile);
            header = xls.nuevoEstilo("Tahoma", 10, true, false, "text", HSSFColor.GREEN.index, xls.NONE, HSSFCellStyle.ALIGN_LEFT);
            titulo1 = xls.nuevoEstilo("Tahoma", 8, true, false, "text", xls.NONE, xls.NONE, xls.NONE);
            titulo2 = xls.nuevoEstilo("Tahoma", 8, true, false, "text", HSSFColor.WHITE.index, HSSFColor.GREEN.index, HSSFCellStyle.ALIGN_CENTER, 2);
            letra = xls.nuevoEstilo("Tahoma", 8, false, false, "", xls.NONE, xls.NONE, xls.NONE);
            dinero = xls.nuevoEstilo("Tahoma", 8, false, false, "#,##0.00", xls.NONE, xls.NONE, xls.NONE);
            dinero2 = xls.nuevoEstilo("Tahoma", 8, false, false, "#,##0", xls.NONE, xls.NONE, xls.NONE);
            porcentaje = xls.nuevoEstilo("Tahoma", 8, false, false, "0.00%", xls.NONE, xls.NONE, xls.NONE);
            ftofecha = xls.nuevoEstilo("Tahoma", 8, false, false, "yyyy-mm-dd", xls.NONE, xls.NONE, xls.NONE);

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception("Error en InitArchivo .... \n" + ex.getMessage());
        }

    }

    public void cargar_tareas() {
        try {
            String id_solicitud = request.getParameter("id_solicitud") == null ? "" : request.getParameter("id_solicitud");
            this.reponseJson = dao.cargar_tareas(id_solicitud);

        } catch (Exception ex) {

            ex.printStackTrace();
        }
    }

    public void cargar_asignacion() {
        try {
            String id_solicitud = request.getParameter("id_solicitud") == null ? "" : request.getParameter("id_solicitud");
            this.reponseJson = dao.cargar_asignacion(id_solicitud);

        } catch (Exception ex) {

            ex.printStackTrace();
        }
    }

    private void cargar_conceptos() {
        try {
            this.reponseJson = dao.cargar_conceptos();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void insertar_Concepto() {

        try {
            String nombre = request.getParameter("nombre") != null ? Util.setCodificacionCadena(request.getParameter("nombre"), "ISO-8859-1") : "";
            String descripcion = request.getParameter("descripcion") != null ? Util.setCodificacionCadena(request.getParameter("descripcion"), "ISO-8859-1") : "";
            String factura = request.getParameter("factura") != null ? Util.setCodificacionCadena(request.getParameter("factura"), "ISO-8859-1") : "";
            this.reponseJson = dao.guardar_Concepto(nombre, descripcion, factura, usuario);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void actualizar_Concepto() {

        try {
            String id = request.getParameter("id") != null ? request.getParameter("id") : "";
            String nombre = request.getParameter("nombre") != null ? Util.setCodificacionCadena(request.getParameter("nombre"), "ISO-8859-1") : "";
            String descripcion = request.getParameter("descripcion") != null ? Util.setCodificacionCadena(request.getParameter("descripcion"), "ISO-8859-1") : "";
            String factura = request.getParameter("factura") != null ? Util.setCodificacionCadena(request.getParameter("factura"), "ISO-8859-1") : "";
            this.reponseJson = dao.actualizar_Concepto(id, nombre, descripcion, factura, usuario);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void anular_Concepto() {
        try {
            String id = request.getParameter("id") != null ? request.getParameter("id") : "";
            this.reponseJson = dao.anular_Concepto(id, usuario);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void cargar_Riesgos() {
        try {
            this.reponseJson = dao.cargar_Riesgos();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void insertar_Riesgos() {

        try {
            String nombre = request.getParameter("nombre") != null ? Util.setCodificacionCadena(request.getParameter("nombre"), "ISO-8859-1") : "";
            String descripcion = request.getParameter("descripcion") != null ? Util.setCodificacionCadena(request.getParameter("descripcion"), "ISO-8859-1") : "";
            this.reponseJson = dao.guardar_Riesgos(nombre, descripcion, usuario);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void actualizar_Riesgos() {

        try {
            String id = request.getParameter("id") != null ? request.getParameter("id") : "";
            String nombre = request.getParameter("nombre") != null ? Util.setCodificacionCadena(request.getParameter("nombre"), "ISO-8859-1") : "";
            String descripcion = request.getParameter("descripcion") != null ? Util.setCodificacionCadena(request.getParameter("descripcion"), "ISO-8859-1") : "";
            this.reponseJson = dao.actualizar_Riesgos(id, nombre, descripcion, usuario);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void anular_Riesgos() {
        try {
            String id = request.getParameter("id") != null ? request.getParameter("id") : "";
            this.reponseJson = dao.anular_Riesgos(id, usuario);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void cargar_Indicadores_Gestion() {
        try {
            this.reponseJson = dao.cargar_Indicadores_Gestion();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void insertar_Indicadores_Gestion() {
        try {
            String nombre = request.getParameter("nombre") != null ? Util.setCodificacionCadena(request.getParameter("nombre"), "ISO-8859-1") : "";
            String descripcion = request.getParameter("descripcion") != null ? Util.setCodificacionCadena(request.getParameter("descripcion"), "ISO-8859-1") : "";
            this.reponseJson = dao.insertar_Indicadores_Gestion(nombre, descripcion, usuario);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void actualizar_Indicadores_Gestion() {
        try {
            String id = request.getParameter("id") != null ? request.getParameter("id") : "";
            String nombre = request.getParameter("nombre") != null ? Util.setCodificacionCadena(request.getParameter("nombre"), "ISO-8859-1") : "";
            String descripcion = request.getParameter("descripcion") != null ? Util.setCodificacionCadena(request.getParameter("descripcion"), "ISO-8859-1") : "";
            this.reponseJson = dao.actualizar_Indicadores_Gestion(id, nombre, descripcion, usuario);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void anular_Indicadores_Gestion() {
        try {
            String id = request.getParameter("id") != null ? request.getParameter("id") : "";
            this.reponseJson = dao.anular_Indicadores_Gestion(id, usuario);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void cargar_Unidades_Medida() {
        try {
            this.reponseJson = dao.cargar_Unidades_Medida();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void guardar_Carga_Laboral() {
        try {
            String idActividad = request.getParameter("idActividad") != null ? Util.setCodificacionCadena(request.getParameter("idActividad"), "ISO-8859-1") : "";
            String nivel = request.getParameter("nivel") != null ? Util.setCodificacionCadena(request.getParameter("nivel"), "ISO-8859-1") : "";
            String rang_ini = request.getParameter("rang_ini") != null ? Util.setCodificacionCadena(request.getParameter("rang_ini"), "ISO-8859-1") : "";
            String rang_final = request.getParameter("rang_final") != null ? Util.setCodificacionCadena(request.getParameter("rang_final"), "ISO-8859-1") : "";
            String unidad_medida = request.getParameter("unidad_medida") != null ? Util.setCodificacionCadena(request.getParameter("unidad_medida"), "ISO-8859-1") : "";
            this.reponseJson = dao.guardar_Carga_Laboral(idActividad, nivel, rang_ini, rang_final, unidad_medida, usuario);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void cargar_Carga_Laboral() {
        try {
            String idActividad = request.getParameter("idActividad") != null ? Util.setCodificacionCadena(request.getParameter("idActividad"), "ISO-8859-1") : "";
            this.reponseJson = dao.cargar_Carga_Laboral(idActividad);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void actualizar_Carga_Laboral() {
        try {
            String id = request.getParameter("id") != null ? Util.setCodificacionCadena(request.getParameter("id"), "ISO-8859-1") : "";
            String idActividad = request.getParameter("idActividad") != null ? Util.setCodificacionCadena(request.getParameter("idActividad"), "ISO-8859-1") : "";
            String nivel = request.getParameter("nivel") != null ? Util.setCodificacionCadena(request.getParameter("nivel"), "ISO-8859-1") : "";
            String rang_ini = request.getParameter("rang_ini") != null ? Util.setCodificacionCadena(request.getParameter("rang_ini"), "ISO-8859-1") : "";
            String rang_final = request.getParameter("rang_final") != null ? Util.setCodificacionCadena(request.getParameter("rang_final"), "ISO-8859-1") : "";
            String unidad_medida = request.getParameter("unidad_medida") != null ? Util.setCodificacionCadena(request.getParameter("unidad_medida"), "ISO-8859-1") : "";
            this.reponseJson = dao.actualizar_Carga_Laboral(id, idActividad, nivel, rang_ini, rang_final, unidad_medida, usuario);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void anular_Carga_Laboral() {
        try {
            String id = request.getParameter("id") != null ? request.getParameter("id") : "";
            this.reponseJson = dao.anular_Carga_Laboral(id, usuario);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void cargar_Cargos() {
        try {
            this.reponseJson = dao.cargar_Cargos();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void cargar_Cargas_Laborales() {
        try {
            String id = request.getParameter("id") != null ? request.getParameter("id") : "";
            this.reponseJson = dao.cargar_Cargas_Laborales(id);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void cargar_cargas_laborales_Asociado() {
        try {
            String id = request.getParameter("id") != null ? request.getParameter("id") : "";
            this.reponseJson = dao.cargar_Cargas_Laborales_Asociado(id);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void asociar_Carga_Laboral() {
        String listado[] = request.getParameter("listado").split(",");
        int id = Integer.parseInt(request.getParameter("id"));

        try {
            TransaccionService tService = new TransaccionService(usuario.getBd());
            tService.crearStatement();

            for (int i = 0; i < listado.length; i++) {

                tService.getSt().addBatch(dao.asociar_Carga_Laboral(id, listado[i], usuario));
            }
            tService.execute();
            String resp = "{\"respuesta\":\"OK\"}";
            this.reponseJson = resp;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void desasociar_Carga_Laboral() {
        String listado[] = request.getParameter("listado").split(",");
        int id = Integer.parseInt(request.getParameter("id"));

        try {
            TransaccionService tService = new TransaccionService(usuario.getBd());
            tService.crearStatement();

            for (int i = 0; i < listado.length; i++) {

                tService.getSt().addBatch("delete from opav.sl_rel_cargo_actividad_planeacion where id =" + listado[i]);

            }
            tService.execute();
            String resp = "{\"respuesta\":\"OK\"}";
            this.reponseJson = resp;
            //this.printlnResponse(resp, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void cargar_Hitos() {
        try {
            String idaccion = request.getParameter("idaccion") == null ? "" : request.getParameter("idaccion");
            this.reponseJson = dao.cargar_hitos(idaccion);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void insertar_Hitos() {

        try {
            String nombre = request.getParameter("nombre") != null ? Util.setCodificacionCadena(request.getParameter("nombre"), "ISO-8859-1") : "";
            String fecha = request.getParameter("fecha") != null ? Util.setCodificacionCadena(request.getParameter("fecha"), "ISO-8859-1") : "";
            String idaccion = request.getParameter("idaccion") != null ? Util.setCodificacionCadena(request.getParameter("idaccion"), "ISO-8859-1") : "";
            String empresa = usuario.getDstrct();
            if(!dao.existeHitos(empresa, nombre, idaccion)){
                this.reponseJson = dao.guardar_hitos(nombre, fecha, usuario, idaccion);
            }else{
                this.reponseJson = "{\"error\":\" No se creo el hito, puede que el nombre ya exista\"}";
            }
                
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void actualizar_Hitos() {

        try {
            String id = request.getParameter("id") != null ? request.getParameter("id") : "";
            String nombre = request.getParameter("nombre") != null ? Util.setCodificacionCadena(request.getParameter("nombre"), "ISO-8859-1") : "";
            String fecha = request.getParameter("fecha") != null ? Util.setCodificacionCadena(request.getParameter("fecha"), "ISO-8859-1") : "";
            String idaccion = request.getParameter("idaccion") != null ? Util.setCodificacionCadena(request.getParameter("idaccion"), "ISO-8859-1") : "";
            String empresa = usuario.getDstrct();
            if(!dao.existeHitos(empresa, nombre, idaccion)){
                this.reponseJson = dao.actualizar_hitos(id, nombre, fecha, usuario, idaccion);
            }else{
                this.reponseJson = "{\"error\":\" No se actualizo el hito, puede que el nombre ya exista\"}";
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void anular_Hitos() {
        try {
            String id = request.getParameter("id") != null ? request.getParameter("id") : "";
            this.reponseJson = dao.anular_hitos(id, usuario);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void cargar_Actividades() {
        try {
            this.reponseJson = dao.cargar_Actividades();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void insertar_Actividad() {
        try {
            String nombre = request.getParameter("nombre") != null ? Util.setCodificacionCadena(request.getParameter("nombre"), "ISO-8859-1") : "";
            String descripcion = request.getParameter("descripcion") != null ? Util.setCodificacionCadena(request.getParameter("descripcion"), "ISO-8859-1") : "";
            this.reponseJson = dao.insertar_Actividad(nombre, descripcion, usuario);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void actualizar_Actividad() {
        try {
            String id = request.getParameter("id") != null ? request.getParameter("id") : "";
            String nombre = request.getParameter("nombre") != null ? Util.setCodificacionCadena(request.getParameter("nombre"), "ISO-8859-1") : "";
            String descripcion = request.getParameter("descripcion") != null ? Util.setCodificacionCadena(request.getParameter("descripcion"), "ISO-8859-1") : "";
            this.reponseJson = dao.actualizar_Actividad(id, nombre, descripcion, usuario);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void anular_Actividad() {
        try {
            String id = request.getParameter("id") != null ? request.getParameter("id") : "";
            this.reponseJson = dao.anular_Actividad(id, usuario);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void ingresar_Peso_Actividad_Aso_Cargo() {
        try {

            JsonObject rows = (JsonObject) (new JsonParser()).parse(request.getParameter("informacion"));
            this.reponseJson = (new Gson()).toJson(dao.ingresar_Peso_Actividad_Aso_Cargo(rows));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void cargar_Matris_Interesados() {
        try {
            String id_solicitud = request.getParameter("id_solicitud") != null ? request.getParameter("id_solicitud") : "";
            this.reponseJson = dao.cargar_Matris_Interesados(id_solicitud);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void insertar_Matris_Interesados() {
        try {
            String id_solicitud = request.getParameter("id_solicitud") != null ? Util.setCodificacionCadena(request.getParameter("id_solicitud"), "ISO-8859-1") : "";
            String nombre = request.getParameter("nombre") != null ? Util.setCodificacionCadena(request.getParameter("nombre"), "ISO-8859-1") : "";
            String empresa = request.getParameter("empresa") != null ? Util.setCodificacionCadena(request.getParameter("empresa"), "ISO-8859-1") : "";
            String tipo_cliente = request.getParameter("tipo_cliente") != null ? Util.setCodificacionCadena(request.getParameter("tipo_cliente"), "ISO-8859-1") : "";
            String telefono = request.getParameter("telefono") != null ? Util.setCodificacionCadena(request.getParameter("telefono"), "ISO-8859-1") : "";
            String correo_electronico = request.getParameter("correo_electronico") != null ? Util.setCodificacionCadena(request.getParameter("correo_electronico"), "ISO-8859-1") : "";
            String rol = request.getParameter("rol") != null ? Util.setCodificacionCadena(request.getParameter("rol"), "ISO-8859-1") : "";
            this.reponseJson = dao.insertar_Matris_Interesados(id_solicitud, nombre, empresa, tipo_cliente, telefono, correo_electronico, rol, usuario);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void actualizar_Matris_Interesados() {
        try {
            String id = request.getParameter("id") != null ? Util.setCodificacionCadena(request.getParameter("id"), "ISO-8859-1") : "";
            String nombre = request.getParameter("nombre") != null ? Util.setCodificacionCadena(request.getParameter("nombre"), "ISO-8859-1") : "";
            String empresa = request.getParameter("empresa") != null ? Util.setCodificacionCadena(request.getParameter("empresa"), "ISO-8859-1") : "";
            String tipo_cliente = request.getParameter("tipo_cliente") != null ? Util.setCodificacionCadena(request.getParameter("tipo_cliente"), "ISO-8859-1") : "";
            String telefono = request.getParameter("telefono") != null ? Util.setCodificacionCadena(request.getParameter("telefono"), "ISO-8859-1") : "";
            String correo_electronico = request.getParameter("correo_electronico") != null ? Util.setCodificacionCadena(request.getParameter("correo_electronico"), "ISO-8859-1") : "";
            String rol = request.getParameter("rol") != null ? Util.setCodificacionCadena(request.getParameter("rol"), "ISO-8859-1") : "";
            this.reponseJson = dao.actualizar_Matris_Interesados(id, nombre, empresa, tipo_cliente, telefono, correo_electronico, rol, usuario);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void anular_Item_Matris_Interesados() {
        try {
            String id = request.getParameter("id") != null ? request.getParameter("id") : "";
            this.reponseJson = dao.anular_Item_Matris_Interesados(id, usuario);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void cargar_Plan_Contatos() {
        try {
            String id_solicitud = request.getParameter("id_solicitud") != null ? request.getParameter("id_solicitud") : "";
            this.reponseJson = dao.cargar_Plan_Contatos(id_solicitud);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void insertar_Plan_Contatos() {
        try {
            String id_solicitud = request.getParameter("id_solicitud") != null ? Util.setCodificacionCadena(request.getParameter("id_solicitud"), "ISO-8859-1") : "";
            String tipo_documento = request.getParameter("tipo_documento") != null ? Util.setCodificacionCadena(request.getParameter("tipo_documento"), "ISO-8859-1") : "";
            String medio_transmision = request.getParameter("medio_transmision") != null ? Util.setCodificacionCadena(request.getParameter("medio_transmision"), "ISO-8859-1") : "";
            String responsable = request.getParameter("responsable") != null ? Util.setCodificacionCadena(request.getParameter("responsable"), "ISO-8859-1") : "";
            String tipo_informacion = request.getParameter("tipo_informacion") != null ? Util.setCodificacionCadena(request.getParameter("tipo_informacion"), "ISO-8859-1") : "";
            String medio_almacenamiento = request.getParameter("medio_almacenamiento") != null ? Util.setCodificacionCadena(request.getParameter("medio_almacenamiento"), "ISO-8859-1") : "";
            String periocidad = request.getParameter("periocidad") != null ? Util.setCodificacionCadena(request.getParameter("periocidad"), "ISO-8859-1") : "";
            String destinatario_principal = request.getParameter("destinatario_principal") != null ? Util.setCodificacionCadena(request.getParameter("destinatario_principal"), "ISO-8859-1") : "";
            String destinatario_secundario = request.getParameter("destinatario_secundario") != null ? Util.setCodificacionCadena(request.getParameter("destinatario_secundario"), "ISO-8859-1") : "";
            this.reponseJson = dao.insertar_Plan_Contatos(id_solicitud, tipo_documento, medio_transmision, responsable, tipo_informacion, medio_almacenamiento, periocidad , destinatario_principal, destinatario_secundario, usuario);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void actualizar_Plan_Contatos() {
        try {
            String id = request.getParameter("id") != null ? Util.setCodificacionCadena(request.getParameter("id"), "ISO-8859-1") : "";
            String tipo_documento = request.getParameter("tipo_documento") != null ? Util.setCodificacionCadena(request.getParameter("tipo_documento"), "ISO-8859-1") : "";
            String medio_transmision = request.getParameter("medio_transmision") != null ? Util.setCodificacionCadena(request.getParameter("medio_transmision"), "ISO-8859-1") : "";
            String responsable = request.getParameter("responsable") != null ? Util.setCodificacionCadena(request.getParameter("responsable"), "ISO-8859-1") : "";
            String tipo_informacion = request.getParameter("tipo_informacion") != null ? Util.setCodificacionCadena(request.getParameter("tipo_informacion"), "ISO-8859-1") : "";
            String medio_almacenamiento = request.getParameter("medio_almacenamiento") != null ? Util.setCodificacionCadena(request.getParameter("medio_almacenamiento"), "ISO-8859-1") : "";
            String periocidad = request.getParameter("periocidad") != null ? Util.setCodificacionCadena(request.getParameter("periocidad"), "ISO-8859-1") : "";
            String destinatario_principal = request.getParameter("destinatario_principal") != null ? Util.setCodificacionCadena(request.getParameter("destinatario_principal"), "ISO-8859-1") : "";
            String destinatario_secundario = request.getParameter("destinatario_secundario") != null ? Util.setCodificacionCadena(request.getParameter("destinatario_secundario"), "ISO-8859-1") : "";
            this.reponseJson = dao.actualizar_Plan_Contatos(id, tipo_documento, medio_transmision, responsable, tipo_informacion, medio_almacenamiento, periocidad , destinatario_principal, destinatario_secundario, usuario);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void anular_Item_Plan_Contatos() {
        try {
            String id = request.getParameter("id") != null ? request.getParameter("id") : "";
            this.reponseJson = dao.anular_Item_Plan_Contatos(id, usuario);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void CARGAR_ASPECTOS_PROYECTO() {
        try {
            String id_solicitud = request.getParameter("id_solicitud") != null ? request.getParameter("id_solicitud") : "";
            this.reponseJson = dao.cargar_Aspectos_Proyecto(id_solicitud);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void INSERTAR_ASPECTOS_PROYECTO() {
        try {
            String id_solicitud = request.getParameter("id_solicitud") != null ? Util.setCodificacionCadena(request.getParameter("id_solicitud"), "ISO-8859-1") : "";
            String descripcion_proyecto = request.getParameter("descripcion_proyecto") != null ? Util.setCodificacionCadena(request.getParameter("descripcion_proyecto"), "ISO-8859-1") : "";
            String requisitos_calidad_normas = request.getParameter("requisitos_calidad_normas") != null ? Util.setCodificacionCadena(request.getParameter("requisitos_calidad_normas"), "ISO-8859-1") : "";
            String alcances_contrato = request.getParameter("alcances_contrato") != null ? Util.setCodificacionCadena(request.getParameter("alcances_contrato"), "ISO-8859-1") : "";
            String Penalizaciones = request.getParameter("Penalizaciones") != null ? Util.setCodificacionCadena(request.getParameter("Penalizaciones"), "ISO-8859-1") : "";
            String fecha_inicio_cliente = request.getParameter("fecha_inicio_cliente") != null ? Util.setCodificacionCadena(request.getParameter("fecha_inicio_cliente"), "ISO-8859-1") : "";
            String fecha_fin_cliente = request.getParameter("fecha_fin_cliente") != null ? Util.setCodificacionCadena(request.getParameter("fecha_fin_cliente"), "ISO-8859-1") : "";
            String fecha_inicio_real = request.getParameter("fecha_inicio_real") != null ? Util.setCodificacionCadena(request.getParameter("fecha_inicio_real"), "ISO-8859-1") : "";
            String fecha_fin_real = request.getParameter("fecha_fin_real") != null ? Util.setCodificacionCadena(request.getParameter("fecha_fin_real"), "ISO-8859-1") : "";
            String necesidades_dise�o = request.getParameter("necesidades_diseno") != null ? Util.setCodificacionCadena(request.getParameter("necesidades_diseno"), "ISO-8859-1") : "";
            this.reponseJson = dao.insertar_Aspectos_Proyecto(id_solicitud, descripcion_proyecto, requisitos_calidad_normas, alcances_contrato, Penalizaciones, fecha_inicio_cliente, fecha_fin_cliente , fecha_inicio_real, fecha_fin_real, necesidades_dise�o,usuario);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void ACTUALIZAR_ASPECTOS_PROYECTO() {
        try {
            String id = request.getParameter("id") != null ? Util.setCodificacionCadena(request.getParameter("id"), "ISO-8859-1") : "";
            String descripcion_proyecto = request.getParameter("descripcion_proyecto") != null ? Util.setCodificacionCadena(request.getParameter("descripcion_proyecto"), "ISO-8859-1") : "";
            String requisitos_calidad_normas = request.getParameter("requisitos_calidad_normas") != null ? Util.setCodificacionCadena(request.getParameter("requisitos_calidad_normas"), "ISO-8859-1") : "";
            String alcances_contrato = request.getParameter("alcances_contrato") != null ? Util.setCodificacionCadena(request.getParameter("alcances_contrato"), "ISO-8859-1") : "";
            String Penalizaciones = request.getParameter("Penalizaciones") != null ? Util.setCodificacionCadena(request.getParameter("Penalizaciones"), "ISO-8859-1") : "";
            String fecha_inicio_cliente = request.getParameter("fecha_inicio_cliente") != null ? Util.setCodificacionCadena(request.getParameter("fecha_inicio_cliente"), "ISO-8859-1") : "";
            String fecha_fin_cliente = request.getParameter("fecha_fin_cliente") != null ? Util.setCodificacionCadena(request.getParameter("fecha_fin_cliente"), "ISO-8859-1") : "";
            String fecha_inicio_real = request.getParameter("fecha_inicio_real") != null ? Util.setCodificacionCadena(request.getParameter("fecha_inicio_real"), "ISO-8859-1") : "";
            String fecha_fin_real = request.getParameter("fecha_fin_real") != null ? Util.setCodificacionCadena(request.getParameter("fecha_fin_real"), "ISO-8859-1") : "";
            String necesidades_dise�o = request.getParameter("necesidades_diseno") != null ? Util.setCodificacionCadena(request.getParameter("necesidades_diseno"), "ISO-8859-1") : "";
            this.reponseJson = dao.actualizar_Aspectos_Proyecto(id,  descripcion_proyecto, requisitos_calidad_normas, alcances_contrato, Penalizaciones, fecha_inicio_cliente, fecha_fin_cliente , fecha_inicio_real, fecha_fin_real, necesidades_dise�o,usuario);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void listar_Cargas_Laborales() {
        try {
            String id_solicitud = request.getParameter("id_solicitud") != null ? request.getParameter("id_solicitud") : "";
            this.reponseJson = dao.listar_Cargas_Laborales(id_solicitud);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void listar_Infomacion_Actividades() {
         try {
            this.reponseJson = dao.listar_Infomacion_Actividades();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void listarCargasLaborales_Detalle() {
        try {
            String id_ind_carga_laboral = request.getParameter("id_ind_carga_laboral") != null ? request.getParameter("id_ind_carga_laboral") : "";
            String id_cargo = request.getParameter("id_cargo") != null ? request.getParameter("id_cargo") : "";
            this.reponseJson = dao.listarCargasLaborales_Detalle(id_ind_carga_laboral,id_cargo);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void guardar_Asignacion_Puntaje() {
        try {
            String id_solicitud = request.getParameter("id_solicitud") != null ? request.getParameter("id_solicitud") : "";
            String id_cargo = request.getParameter("cargo") != null ? request.getParameter("cargo") : "";
            JsonObject rows = (JsonObject) (new JsonParser()).parse(request.getParameter("datosGrillas"));
            this.reponseJson = dao.guardar_Asignacion_Puntaje(id_solicitud,id_cargo,rows,usuario);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void editar_Asignacion_Puntaje() {
        try {
            String id = request.getParameter("id") != null ? request.getParameter("id") : "";
            String id_cargo = request.getParameter("cargo") != null ? request.getParameter("cargo") : "";
            JsonObject rows = (JsonObject) (new JsonParser()).parse(request.getParameter("datosGrillas"));
            this.reponseJson = dao.editar_Asignacion_Puntaje(id,rows,usuario);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    private void anular_Asignacion_Puntaje() {
        try {
            String id = request.getParameter("id") != null ? request.getParameter("id") : "";
            this.reponseJson = dao.anular_Asignacion_Puntaje(id,usuario);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    private void Cargar_flujo_caja() {
        try {
            String idaccion = request.getParameter("idaccion") == null ? "" : request.getParameter("idaccion");
            this.reponseJson = dao.cargar_flujo_caja(idaccion);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public void insertar_Flujo() {

        try {
            String periodo = request.getParameter("periodo") != null ? Util.setCodificacionCadena(request.getParameter("periodo"), "ISO-8859-1") : "";
            String valor = request.getParameter("valor") != null ? Util.setCodificacionCadena(request.getParameter("valor"), "ISO-8859-1") : "";
            String concepto = request.getParameter("concepto") != null ? Util.setCodificacionCadena(request.getParameter("concepto"), "ISO-8859-1") : "";
            String descripcion = request.getParameter("descripcion") != null ? Util.setCodificacionCadena(request.getParameter("descripcion"), "ISO-8859-1") : "";
            String idaccion = request.getParameter("idaccion") != null ? Util.setCodificacionCadena(request.getParameter("idaccion"), "ISO-8859-1") : "";
            this.reponseJson = dao.guardar_flujo(periodo, valor, concepto, descripcion, usuario, idaccion);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public void actualizar_Flujo() {

        try {
            String id = request.getParameter("id") != null ? request.getParameter("id") : "";
            String periodo = request.getParameter("periodo") != null ? Util.setCodificacionCadena(request.getParameter("periodo"), "ISO-8859-1") : "";
            String valor = request.getParameter("valor") != null ? Util.setCodificacionCadena(request.getParameter("valor"), "ISO-8859-1") : "";
            String concepto = request.getParameter("concepto") != null ? Util.setCodificacionCadena(request.getParameter("concepto"), "ISO-8859-1") : "";
            String descripcion = request.getParameter("descripcion") != null ? Util.setCodificacionCadena(request.getParameter("descripcion"), "ISO-8859-1") : "";
            String idaccion = request.getParameter("idaccion") != null ? Util.setCodificacionCadena(request.getParameter("idaccion"), "ISO-8859-1") : "";
            this.reponseJson = dao.actualizar_Flujo(id, periodo, valor, concepto, descripcion, usuario, idaccion);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    private void anular_Flujo() {
        try {
            String id = request.getParameter("id") != null ? request.getParameter("id") : "";
            this.reponseJson = dao.anular_flujo(id, usuario);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    private void cargar_Indicador() {
        try {
            this.reponseJson = dao.cargar_indicador();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public void insertar_Indicador() {

        try {
            String nombre = request.getParameter("nombre") != null ? Util.setCodificacionCadena(request.getParameter("nombre"), "ISO-8859-1") : "";
            String descripcion = request.getParameter("descripcion") != null ? Util.setCodificacionCadena(request.getParameter("descripcion"), "ISO-8859-1") : "";
            this.reponseJson = dao.guardar_indicador(nombre, descripcion, usuario);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public void actualizar_Indicador() {

        try {
            String id = request.getParameter("id") != null ? request.getParameter("id") : "";
            String nombre = request.getParameter("nombre") != null ? Util.setCodificacionCadena(request.getParameter("nombre"), "ISO-8859-1") : "";
            String descripcion = request.getParameter("descripcion") != null ? Util.setCodificacionCadena(request.getParameter("descripcion"), "ISO-8859-1") : "";
            this.reponseJson = dao.actualizar_indicador(id, nombre, descripcion, usuario);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void anular_Indicador() {
        try {
            String id = request.getParameter("id") != null ? request.getParameter("id") : "";
            this.reponseJson = dao.anular_indicador(id, usuario);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    
    //////////////////////////////////////////////////////
    private void Cargar_gestion() {
        try {
            String idaccion = request.getParameter("idaccion") == null ? "" : request.getParameter("idaccion");
            this.reponseJson = dao.cargar_gestion(idaccion);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public void insertar_gestion() {

        try {
            String indicador = request.getParameter("indicador") != null ? Util.setCodificacionCadena(request.getParameter("indicador"), "ISO-8859-1") : "";
            String valor_min = request.getParameter("valor_min") != null ? Util.setCodificacionCadena(request.getParameter("valor_min"), "ISO-8859-1") : "";
            String valor_max = request.getParameter("valor_max") != null ? Util.setCodificacionCadena(request.getParameter("valor_max"), "ISO-8859-1") : "";
            String alerta_min = request.getParameter("alerta_min") != null ? Util.setCodificacionCadena(request.getParameter("alerta_min"), "ISO-8859-1") : "";
            String alerta_max = request.getParameter("alerta_max") != null ? Util.setCodificacionCadena(request.getParameter("alerta_max"), "ISO-8859-1") : "";
            String idaccion = request.getParameter("idaccion") != null ? Util.setCodificacionCadena(request.getParameter("idaccion"), "ISO-8859-1") : "";
            this.reponseJson = dao.guardar_gestion(indicador, valor_min, valor_max, alerta_min, alerta_max, usuario, idaccion);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public void actualizar_gestion() {

        try {
            String id = request.getParameter("id") != null ? request.getParameter("id") : "";
            String indicador = request.getParameter("indicador") != null ? Util.setCodificacionCadena(request.getParameter("indicador"), "ISO-8859-1") : "";
            String valor_min = request.getParameter("valor_min") != null ? Util.setCodificacionCadena(request.getParameter("valor_min"), "ISO-8859-1") : "";
            String valor_max = request.getParameter("valor_max") != null ? Util.setCodificacionCadena(request.getParameter("valor_max"), "ISO-8859-1") : "";
            String alerta_min = request.getParameter("alerta_min") != null ? Util.setCodificacionCadena(request.getParameter("alerta_min"), "ISO-8859-1") : "";
            String alerta_max = request.getParameter("alerta_max") != null ? Util.setCodificacionCadena(request.getParameter("alerta_max"), "ISO-8859-1") : "";
            String idaccion = request.getParameter("idaccion") != null ? Util.setCodificacionCadena(request.getParameter("idaccion"), "ISO-8859-1") : "";
            this.reponseJson = dao.actualizar_gestion(id, indicador, valor_min, valor_max, alerta_min, alerta_max, usuario, idaccion);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    private void anular_gestion() {
        try {
            String id = request.getParameter("id") != null ? request.getParameter("id") : "";
            this.reponseJson = dao.anular_gestion(id, usuario);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    ////////////////////////////////////////////////////////////////
    
    private void Cargar_proyeccion() {
        try {
            String idaccion = request.getParameter("idaccion") == null ? "" : request.getParameter("idaccion");
            this.reponseJson = dao.cargar_proyeccion(idaccion);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    
    /////////////////////////////////////////////////////////////////
    
    private void Cargar_Equi() {
        try {
            String idaccion = request.getParameter("idaccion") == null ? "" : request.getParameter("idaccion");
            if(dao.existe_equipo(idaccion)){
                this.reponseJson = dao.cargar_equipos_bd(idaccion); 
            }else{
               this.reponseJson = dao.cargar_equipos(idaccion); 
            }
            
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    private void Cargar_Personal() {
        try {
            String idaccion = request.getParameter("idaccion") == null ? "" : request.getParameter("idaccion");
            if(dao.existe_personal(idaccion)){
                this.reponseJson = dao.cargar_personal_bd(idaccion); 
            }else{
               this.reponseJson = dao.cargar_personal(idaccion); 
            }
            
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    private void insertar_equipos() {
        
        try {
            JsonObject info = (JsonObject) (new JsonParser()).parse(Util.setCodificacionCadena(request.getParameter("informacion"),"ISO-8859-1"));
            info.addProperty("usuario", ((Usuario) request.getSession().getAttribute("Usuario")).getLogin());
            info.addProperty("dstrct", ((Usuario) request.getSession().getAttribute("Usuario")).getDstrct());
            respuesta = dao.ingresar_equipos(info);
            this.reponseJson=(new Gson()).toJson(respuesta);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
                
    }
    
    private void insertar_personal() {
        
        try {
            JsonObject info = (JsonObject) (new JsonParser()).parse(Util.setCodificacionCadena(request.getParameter("informacion"),"ISO-8859-1"));
            info.addProperty("usuario", ((Usuario) request.getSession().getAttribute("Usuario")).getLogin());
            info.addProperty("dstrct", ((Usuario) request.getSession().getAttribute("Usuario")).getDstrct());
            respuesta = dao.ingresar_personal(info);
            this.reponseJson=(new Gson()).toJson(respuesta);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        
    }
    
    //////////////////////////////////////////
    
    private void Cargar_Material() {
        try {
            String idaccion = request.getParameter("idaccion") == null ? "" : request.getParameter("idaccion");
            if(dao.existe_material(idaccion)){
                this.reponseJson = dao.cargar_material_bd(idaccion); 
            }else{
               this.reponseJson = dao.cargar_material(idaccion); 
            }
            
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    private void insertar_material() {
        
        try {
            JsonObject info = (JsonObject) (new JsonParser()).parse(Util.setCodificacionCadena(request.getParameter("informacion"),"ISO-8859-1"));
            info.addProperty("usuario", ((Usuario) request.getSession().getAttribute("Usuario")).getLogin());
            info.addProperty("dstrct", ((Usuario) request.getSession().getAttribute("Usuario")).getDstrct());
            respuesta = dao.ingresar_material(info);
            this.reponseJson=(new Gson()).toJson(respuesta);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
                
    }
    
    private void cargarComboCategoria() throws IOException {
        try {
            JsonObject info = (JsonObject) (new JsonParser()).parse(request.getParameter("informacion"));
            respuesta = dao.cargar_combo_categoria(info);
            this.reponseJson = (new Gson()).toJson(respuesta);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    private void guardar_tipo_categoria() {
        try {
            String nombre = Util.setCodificacionCadena(request.getParameter("nombre"),"ISO-8859-1");
            String puntaje = (request.getParameter("puntaje")!=null) ? request.getParameter("puntaje"):"0";
            String empresa = usuario.getDstrct();

            String resp="{}";
            if (!dao.existeTipoCategoria(empresa, nombre, "")) {

                this.reponseJson = dao.guardarTipoCategoria(empresa, nombre, puntaje, usuario.getLogin());
            } else {

                this.reponseJson = "{\"error\":\" No se creo la categoria, puede que el nombre ya exista\"}";
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private void cargarProcesosMeta(String status) {
        try {
            this.reponseJson = dao.cargarCategoria(usuario.getDstrct(), status, request.getParameter("categoria"));
            /*Gson gson = new Gson();
            String json = "{\"page\":1,\"rows\":" + gson.toJson(lista) + "}";
            this.printlnResponse(json, "application/json;");*/
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private void guardar_impacto() {
        try {
            
            String nombre = Util.setCodificacionCadena(request.getParameter("nombre"),"ISO-8859-1");
            String descripcion = Util.setCodificacionCadena(request.getParameter("descripcion"),"ISO-8859-1");
            String categoria = Util.setCodificacionCadena(request.getParameter("categoria"),"ISO-8859-1");
            String puntaje = (request.getParameter("puntaje")!=null) ? request.getParameter("puntaje"):"0";
            String empresa = usuario.getDstrct();

            String resp="{}";
            if (!dao.existeIndicador(empresa, nombre, categoria)) {

                this.reponseJson = dao.guardarIndicador(empresa, nombre, descripcion, puntaje, categoria, usuario.getLogin());
            } else {

                this.reponseJson = "{\"error\":\" No se creo el indicador, puede que el nombre ya exista\"}";
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private void cargarImpacto(String status) {
        try {
            this.reponseJson = dao.cargarImpacto(usuario.getDstrct(), status, request.getParameter("categoria"));
            /*Gson gson = new Gson();
            String json = "{\"page\":1,\"rows\":" + gson.toJson(lista) + "}";
            this.printlnResponse(json, "application/json;");*/
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    ////////////////////////////////////////
    private void Cargar_riesgo_ci() {
        try {
            String idaccion = request.getParameter("idaccion") == null ? "" : request.getParameter("idaccion");
            this.reponseJson = dao.cargar_riesgo_ci(idaccion);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public void insertar_riesgo_ci() {

        try {
            String categoria = request.getParameter("categoria") != null ? Util.setCodificacionCadena(request.getParameter("categoria"), "ISO-8859-1") : "";
            String impacto = request.getParameter("impacto") != null ? Util.setCodificacionCadena(request.getParameter("impacto"), "ISO-8859-1") : "";
            String probabilidad = request.getParameter("probabilidad") != null ? Util.setCodificacionCadena(request.getParameter("probabilidad"), "ISO-8859-1") : "";
            String descripcion = request.getParameter("descripcion") != null ? Util.setCodificacionCadena(request.getParameter("descripcion"), "ISO-8859-1") : "";
            String puntaje = request.getParameter("puntaje") != null ? Util.setCodificacionCadena(request.getParameter("puntaje"), "ISO-8859-1") : "";
            String idaccion = request.getParameter("idaccion") != null ? Util.setCodificacionCadena(request.getParameter("idaccion"), "ISO-8859-1") : "";
            this.reponseJson = dao.guardar_riesgo_ci(categoria, impacto, probabilidad, descripcion, puntaje, usuario, idaccion);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public void actualizar_riesgo_ci() {

        try {
            String id = request.getParameter("id") != null ? request.getParameter("id") : "";
            String categoria = request.getParameter("categoria") != null ? Util.setCodificacionCadena(request.getParameter("categoria"), "ISO-8859-1") : "";
            String impacto = request.getParameter("impacto") != null ? Util.setCodificacionCadena(request.getParameter("impacto"), "ISO-8859-1") : "";
            String probabilidad = request.getParameter("probabilidad") != null ? Util.setCodificacionCadena(request.getParameter("probabilidad"), "ISO-8859-1") : "";
            String descripcion = request.getParameter("descripcion") != null ? Util.setCodificacionCadena(request.getParameter("descripcion"), "ISO-8859-1") : "";
            String puntaje = request.getParameter("puntaje") != null ? Util.setCodificacionCadena(request.getParameter("puntaje"), "ISO-8859-1") : "";
            String idaccion = request.getParameter("idaccion") != null ? Util.setCodificacionCadena(request.getParameter("idaccion"), "ISO-8859-1") : "";
            this.reponseJson = dao.actualizar_riesgo_ci(id, categoria, impacto, probabilidad, descripcion, puntaje, usuario, idaccion);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    private void anular_riesgo_ci() {
        try {
            String id = request.getParameter("id") != null ? request.getParameter("id") : "";
            this.reponseJson = dao.anular_riesgo_ci(id, usuario);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    ////////////////////////////////////////
    
    public void actualizar_impacto() {

        try {
            String id = request.getParameter("id") != null ? request.getParameter("id") : "";
            String nombre = request.getParameter("nombre") != null ? Util.setCodificacionCadena(request.getParameter("nombre"), "ISO-8859-1") : "";
            String descripcion = request.getParameter("descripcion") != null ? Util.setCodificacionCadena(request.getParameter("descripcion"), "ISO-8859-1") : "";
            String puntaje = request.getParameter("puntaje") != null ? Util.setCodificacionCadena(request.getParameter("puntaje"), "ISO-8859-1") : "";
            this.reponseJson = dao.actualizar_impacto(id, nombre, descripcion, puntaje, usuario);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    private void anular_impacto() {
        try {
            String id = request.getParameter("id") != null ? request.getParameter("id") : "";
            this.reponseJson = dao.anular_impacto(id, usuario);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void cargar_Unidades_Medida_Opav() {
        try {
            this.reponseJson = dao.cargar_Unidades_Medida_Opav();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
