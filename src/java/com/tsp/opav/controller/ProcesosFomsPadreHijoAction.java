/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.opav.controller;

/**
 *
 * @author user
 */
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.tsp.exceptions.InformationException;
import com.tsp.opav.model.DAOS.ProcesosFomsPadreHijoDAO;
import com.tsp.opav.model.DAOS.impl.ProcesosFomsPadreHijoImpl;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.util.Util;
import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;

public class ProcesosFomsPadreHijoAction extends Action {

    private final int CARGAR_INFO_SOLICITUDES = 0;
    private final int CARGAR_FOMS_DISPIBLES = 1;
    private final int CARGAR_FOMS_RELACIONADOS = 2;
    private final int ASOCIAR_FOMS = 3;
    private final int DESASOCIAR_FOMS = 4;


    private ProcesosFomsPadreHijoDAO dao;
    Usuario usuario = null;
    String reponseJson = "{}";

    @Override
    public void run() throws ServletException, InformationException {
        try {

            HttpSession session = request.getSession();
            usuario = (Usuario) session.getAttribute("Usuario");
            dao = new ProcesosFomsPadreHijoImpl(usuario.getBd());
            int opcion = (request.getParameter("opcion") != null)
                    ? Integer.parseInt(request.getParameter("opcion"))
                    : -1;
            switch (opcion) {
                case CARGAR_INFO_SOLICITUDES:
                    this.cargarInfoSolicitudes();
                    break;
                case CARGAR_FOMS_DISPIBLES:
                    this.cargarFomsDispibles();
                    break;
                case CARGAR_FOMS_RELACIONADOS:
                    this.cargarFomsRelacionados();
                    break;
                case ASOCIAR_FOMS:
                    this.asociarFoms();
                    break;
                case DESASOCIAR_FOMS:
                    this.dasociarFoms();
                    break;

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void cargarInfoSolicitudes() {
        try {
            String solicitud = request.getParameter("id_solicitud") == null ? "" : request.getParameter("id_solicitud");
            String foms = request.getParameter("foms") == null ? "" : request.getParameter("foms");
            this.printlnResponseAjax(dao.cargarInfoSolicitudes(solicitud,foms), "application/json;");
        } catch (Exception ex) {

            ex.printStackTrace();
        }
    }
    
    private void cargarFomsDispibles() {
        try {   
                String solicitud = request.getParameter("id_solicitud") == null ? "" : request.getParameter("id_solicitud");
            this.printlnResponseAjax(dao.cargarFomsDispibles(solicitud), "application/json;");
        } catch (Exception ex) {

            ex.printStackTrace();
        }
    }
    
    private void cargarFomsRelacionados() {
        try {
            String solicitud = request.getParameter("id_solicitud") == null ? "" : request.getParameter("id_solicitud");
            this.printlnResponseAjax(dao.cargarFomsRelacionados(solicitud), "application/json;");
        } catch (Exception ex) {

            ex.printStackTrace();
        }
    }

    private void asociarFoms() {
        try {
            String solicitud = request.getParameter("id_solicitud") == null ? "" : request.getParameter("id_solicitud");
            String lista = request.getParameter("lista") == null ? "" : request.getParameter("lista");
            this.printlnResponseAjax(dao.asociarFoms(solicitud ,lista , usuario), "application/json;");
        } catch (Exception ex) {

            ex.printStackTrace();
        }
    }

    private void dasociarFoms() {
        try {
            String solicitud = request.getParameter("id_solicitud") == null ? "" : request.getParameter("id_solicitud");
            String lista = request.getParameter("lista") == null ? "" : request.getParameter("lista");
            this.printlnResponseAjax(dao.desasociarFoms(solicitud ,lista , usuario), "application/json;");
        } catch (Exception ex) {

            ex.printStackTrace();
        }
    }
}
