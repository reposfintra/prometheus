/*opav/controller
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.opav.controller;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.tsp.exceptions.InformationException;
import com.tsp.opav.model.DAOS.ProcesosCatalogoDAO;
import com.tsp.opav.model.DAOS.impl.ProcesosCatalogoImpl;
import com.tsp.opav.controller.Action;
import com.tsp.operation.model.TransaccionService;
import com.tsp.opav.model.beans.Categoria;
import com.tsp.opav.model.beans.Mapeo_insumos;
import com.tsp.operation.controller.FintraSoporteAction;

import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.beans.Proveedor;
import com.tsp.opav.model.beans.ValorPredeterminado;
import com.tsp.opav.model.beans.UnidadMedida;
import com.tsp.opav.model.beans.Especificaciones;
import static com.tsp.operation.model.beans.Amortizacion.fmt;
import com.tsp.operation.model.beans.POIWrite;
import com.tsp.util.Utility;
import com.tsp.util.Util;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.util.HSSFColor;

/**
 *
 * @author fnunez
 */
public class ProcesosCatalogoAction extends Action {

    private final int CARGAR_PROCESOS_META = 1;
    private final int GUARDAR_PROCESOS_META = 2;
    private final int ACTUALIZAR_PROCESO_META = 3;
    private final int CARGAR_PROCESO_INTERNO = 4;
    private final int GUARDAR_PROCESO_INTERNO = 5;
    private final int CARGAR_UNIDADES_NEGOCIO = 6;
    private final int ASOCIAR_UNIDADES_NEGOCIO = 7;
    private final int CARGAR_UND_NEGOCIOS_PROINTERNO = 8;
    private final int ACTUALIZAR_PROINTERNO = 9;
    private final int DESASOCIAR_UND_PROINTERNO = 10;
    private final int LISTAR_USUARIOS = 11;
    private final int LISTAR_PROINTERNO_USUARIO = 12;
    private final int LISTAR_PROINTERNO = 13;
    private final int ASOCIAR_PROINTERNO_USUARIO = 14;
    private final int ANULAR_PROCESO_META = 15;
    private final int ANULAR_PROCESO_INTERNO = 16;
    private final int CARGAR_CBO_PROCESOS_META = 17;
    private final int LISTAR_USUARIOS_PROINTERNO = 18;
    private final int LISTAR_USUARIOS_REL_PROINTERNO = 19;
    private final int ASOCIAR_USUARIO_PROINTERNO = 20;
    private final int DESASOCIAR_USUARIO_PROINTERNO = 21;
    private final int EXISTE_USUARIO_REL_PROCESO = 22;
    private final int CARGAR_METAS_ANULADOS = 23;
    private final int ACTIVAR_PROCESO_META = 24;
    private final int CARGAR_PROCESOS_ANULADOS = 25;
    private final int ACTIVAR_PROCESO_INTERNO = 26;
    private final int ACTUALIZAR_MODERADOR_PROINTERNO = 27;
    private final int BUSCAR_USUARIOS_REL_PROINTERNO = 28;
    private final int CARGAR_REL_PROCESOS_USUARIO = 29;
    private final int CARGAR_REL_TIPO_REQ_USUARIO = 30;
    private final int ASIGNAR_PROCESOS_REQ_USUARIO = 31;
    private final int ASIGNAR_TIPOS_REQ_USUARIO = 32;
    private final int CREAR_ESPECIFICACION = 33;
    private final int CREAR_SUBCATEGORIA = 34;
    private final int CARGAR_CATEGORIAS = 35;
    private final int CARGAR_COLUMNAS_ESPECIFICACIONES = 36;
    private final int CARGAR_PROVEEDORES = 37;
    private final int LISTAR_VALORESPREDETERMINADOS = 38;
    private final int CARGAR_UNIDADMEDIDA = 39;
    private final int CARGAR_COMBO = 40;
    private final int guardarValorPredeterminado = 41;
    private final int GUARDARVALPREDESP = 42;
    /*private final int CARGAR_ITEMSELECCION = 43;*///38
    private final int GUARDAR_DATOS = 46;//41
    private final int CARGAR_DATOS = 47;//42
    private final int LISTAR_VALORESPREDETERMINADOS_FILTRO = 48;
    private final int LISTAR_VALORESPREDETERMINADOS_FILTRO_EDIT = 49;
    private final int LISTAR_VALORESPREDETERMINADOS_EDIT = 50;
    private final int CREAR_ESPECIFICACION_EDIT = 51;
    private final int CARGAR_COMBO_INSUMOS = 52;
    private final int EXPORTAR_EXCEL_INSUMOS = 53;
    private final int ANULAR_INSUMOS = 54;
    private final int CARGAR_NOMBRES_SUBCATEGORIAS = 55;
    private final int CARGAR_NOMBRES_ESPECIFICACIONES = 56;
    private final int CARGAR_MAPEO = 57;
    private final int EXPORTAR_EXCEL_INSUMOS_MAPEO = 58;
    private final int GUARDAR_TIPO_INSUMO = 59;
    private final int CARGAR_COMBO_TIPO_INSUMOS = 60;
    private final int CARGAR_COMBO_CATEGORIA = 61;
    private final int CARGAR_TIPO_INSUMO = 62;
    private final int ACTUALIZAR_TIPO_INSUMO = 63;
    private final int ACTIVAR_INACTIVAR_TIPO_INSUMO = 64;

    private ProcesosCatalogoDAO dao;
    Usuario usuario = null;

    private JsonObject respuesta;
    private boolean ctrlRequest = true;
    String reponseJson = "{}";
    HSSFCellStyle header, titulo1, titulo2, titulo3, titulo4, titulo5, letra, numero, dinero, dinero2, numeroCentrado, porcentaje, letraCentrada, numeroNegrita, ftofecha;
    POIWrite xls;
    private int fila = 0;
    String rutaInformes;
    String nombre;

    @Override
    public void run() throws ServletException, InformationException {
        try {
            HttpSession session = request.getSession();
            usuario = (Usuario) session.getAttribute("Usuario");
            dao = new ProcesosCatalogoImpl(usuario.getBd());
            int opcion = (request.getParameter("opcion") != null)
                    ? Integer.parseInt(request.getParameter("opcion"))
                    : -1;
            switch (opcion) {
                case CARGAR_PROCESOS_META:
                    this.cargarProcesosMeta("A");
                    break;
                case GUARDAR_PROCESOS_META:
                    this.guardarProcesosMeta();
                    break;
                case ACTUALIZAR_PROCESO_META:
                    this.actualizarProcesosMeta();
                    break;
                case CARGAR_PROCESO_INTERNO:
                    this.cargarProcesoInterno("A");
                    break;
                case GUARDAR_PROCESO_INTERNO:
                    this.guardarProcesoInterno();
                    break;
                case CARGAR_UNIDADES_NEGOCIO:
                    this.cargarUnidadesNegocio();
                    break;
                case ASOCIAR_UNIDADES_NEGOCIO:
                    this.asociarUnidadesNegocio();
                    break;
                case CARGAR_UND_NEGOCIOS_PROINTERNO:
                    this.cargarUndNegociosProinterno();
                    break;
                case ACTUALIZAR_PROINTERNO:
                    this.actualizarProinterno();
                    break;
                case DESASOCIAR_UND_PROINTERNO:
                    this.desasociarUndProinterno();
                    break;
                case LISTAR_USUARIOS:
                    this.listarUsuario();
                    break;
                case LISTAR_PROINTERNO_USUARIO:
                    this.listarProinternoUsuario();
                    break;
                case LISTAR_PROINTERNO:
                    this.listarProinterno();
                    break;
                case ASOCIAR_PROINTERNO_USUARIO:
                    this.asociarProinternoUsuario();
                    break;
                case ANULAR_PROCESO_META:
                    this.anularProcesosMeta();
                    break;
                case ANULAR_PROCESO_INTERNO:
                    this.anularProcesoInterno();
                    break;
                case CARGAR_CBO_PROCESOS_META:
                    this.cargarComboProcesosMeta();
                    break;
                case LISTAR_USUARIOS_PROINTERNO:
                    this.listarUsuariosProinterno();
                    break;
                case LISTAR_USUARIOS_REL_PROINTERNO:
                    this.listarUsuariosRelProInterno();
                    break;
                case ASOCIAR_USUARIO_PROINTERNO:
                    this.asociarUsuariosProinterno();
                    break;
                case DESASOCIAR_USUARIO_PROINTERNO:
                    this.desasociarUsuariosProinterno();
                    break;
                case EXISTE_USUARIO_REL_PROCESO:
                    this.existenUsuariosRelProceso();
                    break;
                case CARGAR_METAS_ANULADOS:
                    this.cargarProcesosMeta("");
                    break;
                case ACTIVAR_PROCESO_META:
                    activarProcesosMeta();
                    break;
                case CARGAR_PROCESOS_ANULADOS:
                    this.cargarProcesoInterno("");
                    break;
                case ACTIVAR_PROCESO_INTERNO:
                    activarProcesoInterno();
                    break;
                case ACTUALIZAR_MODERADOR_PROINTERNO:
                    actualizarModerador();
                    break;
                case BUSCAR_USUARIOS_REL_PROINTERNO:
                    this.cargarUsuariosRelProInterno();
                    break;
                case CARGAR_REL_PROCESOS_USUARIO:
                    this.cargarRelProcesosUsuario();
                    break;
                case CARGAR_REL_TIPO_REQ_USUARIO:
                    this.cargarRelTipoRequisicionUsuario();
                    break;
                case ASIGNAR_PROCESOS_REQ_USUARIO:
                    this.asignarProcesosReqUsuario();
                    break;
                case ASIGNAR_TIPOS_REQ_USUARIO:
                    this.asignarTiposReqUsuario();
                    break;
                case CREAR_ESPECIFICACION:
                    this.crearEspecificacion();
                    break;
                case CREAR_SUBCATEGORIA:
                    this.guardarSubcategoriaRel();
                    break;
                case CARGAR_CATEGORIAS:
                    this.cargarCategorias();
                    break;
                case CARGAR_COLUMNAS_ESPECIFICACIONES:
                    this.obtenerEspecificacionesColumnas();
                    break;
                case CARGAR_PROVEEDORES:
                    this.cargarprovedores("A");
                    break;
                case LISTAR_VALORESPREDETERMINADOS:
                    this.listar_valorespredeterminados("A");
                    break;
                case CARGAR_UNIDADMEDIDA:
                    this.cargarunidadmedida("A");
                    break;
                case CARGAR_COMBO:
                    this.cargar_combo();
                    break;
                case guardarValorPredeterminado:
                    this.guardarValorPredeterminado();
                    break;
                case GUARDARVALPREDESP:
                    this.asociarValPred();
                    break;
                case GUARDAR_DATOS:
                    this.guardar_datos();
                    break;
                case CARGAR_DATOS:
                    this.consultar();
                    break;
                case LISTAR_VALORESPREDETERMINADOS_FILTRO:
                    this.listar_valorespredeterminados_filtro("A");
                    break;
                case LISTAR_VALORESPREDETERMINADOS_FILTRO_EDIT:
                    this.listar_valorespredeterminados_filtro_edit("A");
                    break;
                case LISTAR_VALORESPREDETERMINADOS_EDIT:
                    this.listar_valorespredeterminados_edit("A");
                    break;
                case CREAR_ESPECIFICACION_EDIT:
                    this.crearEspecificacionEdit();
                    break;
                case CARGAR_COMBO_INSUMOS:
                    this.cargarComboInsumos();
                    break;
                case CARGAR_NOMBRES_SUBCATEGORIAS:
                    this.cargarNom_subCat();
                    break;
                case CARGAR_NOMBRES_ESPECIFICACIONES:
                    this.cargarNom_Especificaciones();
                    break;
                case EXPORTAR_EXCEL_INSUMOS:
                    this.exportarExcelInsumos();
                    break;
                case ANULAR_INSUMOS:
                    this.anularInsumo();
                    break;
                case CARGAR_MAPEO:
                    this.CargarMapeo();
                    break;
                case EXPORTAR_EXCEL_INSUMOS_MAPEO:
                    this.exportarExcelInsumosMapeo();
                    break;
                case GUARDAR_TIPO_INSUMO:
                    this.guardar_tipo_insumo();
                    break;
                case CARGAR_COMBO_TIPO_INSUMOS:
                    this.cargar_combo_tipo_insumos();
                    break;
                case CARGAR_COMBO_CATEGORIA:
                    this.cargar_combo_categoria();
                    break;
                case CARGAR_TIPO_INSUMO:
                    cargar_tipo_insumo();
                    break;
                case ACTUALIZAR_TIPO_INSUMO:
                    actualizar_tipo_insumo();
                    break;
                case ACTIVAR_INACTIVAR_TIPO_INSUMO:
                    activaInactivaTipoInsumo();
                    break;    
                    
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void consultar() throws IOException {
        ctrlRequest = true;
        //JsonObject info = (JsonObject) (new JsonParser()).parse(request.getParameter("informacion"));
        String idsubcategoria = request.getParameter("subcategoria");
        respuesta = dao.consultar(idsubcategoria);
        response.setContentType("application/json; charset=ISO-8859-1");
        response.setHeader("Cache-Control", "no-cache");
        response.getWriter().println((new Gson()).toJson(respuesta));
    }

    private void guardar_datos() throws IOException {
        
        ctrlRequest = true;
        try {
            JsonObject info = (JsonObject) (new JsonParser()).parse(Util.setCodificacionCadena(request.getParameter("informacion"),"ISO-8859-1"));
            info.addProperty("usuario", ((Usuario) request.getSession().getAttribute("Usuario")).getLogin());
            info.addProperty("dstrct", ((Usuario) request.getSession().getAttribute("Usuario")).getDstrct());
            respuesta = dao.modificar_esp(info);
            this.printlnResponse((new Gson()).toJson(respuesta), "application/json;");
        } catch (Exception ex) {
            Logger.getLogger(ProcesosCatalogoAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

        private void guardarValorPredeterminado() throws IOException {
        ctrlRequest = true;
        JsonObject info = (JsonObject) (new JsonParser()).parse(Util.setCodificacionCadena(request.getParameter("informacion"),"ISO-8859-1"));
        info.addProperty("usuario", ((Usuario) request.getSession().getAttribute("Usuario")).getLogin());
        info.addProperty("dstrct", ((Usuario) request.getSession().getAttribute("Usuario")).getDstrct());
        respuesta = dao.modificar_Val(info);
        response.setContentType("application/json; charset=ISO-8859-1");
        response.setHeader("Cache-Control", "no-cache");
        response.getWriter().println((new Gson()).toJson(respuesta));
    }

    private void cargarprovedores(String status) {
        try {
            ArrayList<Proveedor> lista = dao.cargarproveedores(usuario.getDstrct(), status);
            Gson gson = new Gson();
            String json = "{\"page\":1,\"rows\":" + gson.toJson(lista) + "}"; //{"page":1,"rows":  }
            this.printlnResponse(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void listar_valorespredeterminados(String status) {
        try {
            ArrayList<ValorPredeterminado> lista = dao.listar_valorespredeterminados(usuario.getDstrct(), status);
            Gson gson = new Gson();
            String json = "{\"page\":1,\"rows\":" + gson.toJson(lista) + "}"; //{"page":1,"rows":  }
            this.printlnResponse(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void listar_valorespredeterminados_filtro(String status) {
        try {
            String id = request.getParameter("id");
            String ids = request.getParameter("ids");
            ArrayList<ValorPredeterminado> lista = dao.listar_valorespredeterminados_filtro(usuario.getDstrct(), status, id, ids);
            Gson gson = new Gson();
            String json = "{\"page\":1,\"rows\":" + gson.toJson(lista) + "}"; //{"page":1,"rows":  }
            this.printlnResponse(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void listar_valorespredeterminados_edit(String status) {
        try {
            String id = request.getParameter("id");
            ArrayList<ValorPredeterminado> lista = dao.listar_valorespredeterminados_edit(usuario.getDstrct(), status, id);
            Gson gson = new Gson();
            String json = "{\"page\":1,\"rows\":" + gson.toJson(lista) + "}"; //{"page":1,"rows":  }
            this.printlnResponse(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void listar_valorespredeterminados_filtro_edit(String status) {
        try {
            String id = request.getParameter("id");
            ArrayList<ValorPredeterminado> lista = dao.listar_valorespredeterminados_filtro_edit(usuario.getDstrct(), status, id);
            Gson gson = new Gson();
            String json = "{\"page\":1,\"rows\":" + gson.toJson(lista) + "}"; //{"page":1,"rows":  }
            this.printlnResponse(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void cargarunidadmedida(String status) {
        try {
            ArrayList<UnidadMedida> lista = dao.cargarunidadmedida(usuario.getDstrct(), status);
            Gson gson = new Gson();
            String json = "{\"page\":1,\"rows\":" + gson.toJson(lista) + "}"; //{"page":1,"rows":  }
            this.printlnResponse(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void obtenerEspecificacionesColumnas() throws Exception {
            ProcesosCatalogoDAO dao = new ProcesosCatalogoImpl(usuario.getBd());
        String subcategoria = request.getParameter("subcategoria");
        this.printlnResponse(dao.obtenerEspecificacionesColumnas(subcategoria), "application/json;");
    }

    public void cargarCategorias() {
        try {

            // String multiservicio = request.getParameter("multiservicio") == null ? "" : request.getParameter("multiservicio");
            String json = dao.cargarCategorias();
            this.printlnResponse(json, "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(FintraSoporteAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void cargarProcesosMeta(String status) {
        try {
            ArrayList<Categoria> lista = dao.cargarProcesosMeta(usuario.getDstrct(), status, request.getParameter("insumo"));
            Gson gson = new Gson();
            String json = "{\"page\":1,\"rows\":" + gson.toJson(lista) + "}";
            this.printlnResponse(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * envia respuesta al cliente
     *
     */
    public void printlnResponse(String respuesta, String contentType) throws Exception {
        response.setContentType(contentType + " charset=UTF-8;");
        response.setHeader("Cache-Control", "no-store");
        response.setDateHeader("Expires", 0);
        response.getWriter().println(respuesta);

    }

    private void guardarProcesosMeta() {
        try {
            String nombre = Util.setCodificacionCadena(request.getParameter("nombre"),"ISO-8859-1");
            String descripcion = Util.setCodificacionCadena(request.getParameter("descripcion"),"ISO-8859-1");
            String empresa = usuario.getDstrct();
            String idinsumo = request.getParameter("insumo");
            String resp = "{}";
            if (!dao.existeMetaProceso(empresa, nombre)) {
                resp = dao.guardarMetaProceso(idinsumo, empresa, nombre, descripcion, usuario.getLogin());
            } else {
                resp = "{\"error\":\" No se cre� el meta proceso, puede que el nombre ya exista\"}";
            }
            this.printlnResponse(resp, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*private void guardarValorPredeterminado() {
     try {
     String nombre = request.getParameter("nombre");
     String descripcion = request.getParameter("descripcion");
     String empresa = usuario.getDstrct();
     String resp = "{}";
     if (!dao.existeMetaProceso(empresa, nombre)) {
     resp = dao.guardarValorPredeterminado(empresa, nombre, usuario.getLogin());
     } else {
     resp = "{\"error\":\" No se cre� el meta proceso, puede que el nombre ya exista\"}";
     }
     this.printlnResponse(resp, "application/json;");
     } catch (Exception e) {
     e.printStackTrace();
     }
     }*/
    private void crearEspecificacion() {

        try {

            ctrlRequest = true;
            JsonObject info = (JsonObject) (new JsonParser()).parse(Util.setCodificacionCadena(request.getParameter("informacion"),"ISO-8859-1"));
            info.addProperty("usuario", ((Usuario) request.getSession().getAttribute("Usuario")).getLogin());
            info.addProperty("dstrct", ((Usuario) request.getSession().getAttribute("Usuario")).getDstrct());
            respuesta = dao.modificar_espVal(info);
            response.setContentType("application/json; charset=ISO-8859-1");
            response.setHeader("Cache-Control", "no-cache");
            response.getWriter().println((new Gson()).toJson(respuesta));

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void crearEspecificacionEdit() {
        try {
            ctrlRequest = true;
            JsonObject info = (JsonObject) (new JsonParser()).parse(Util.setCodificacionCadena(request.getParameter("informacion"),"ISO-8859-1"));
            info.addProperty("usuario", ((Usuario) request.getSession().getAttribute("Usuario")).getLogin());
            info.addProperty("dstrct", ((Usuario) request.getSession().getAttribute("Usuario")).getDstrct());
            respuesta = dao.modificar_espVal_edit(info);
            response.setContentType("application/json; charset=ISO-8859-1");
            response.setHeader("Cache-Control", "no-cache");
            response.getWriter().println((new Gson()).toJson(respuesta));

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void actualizarProcesosMeta() {
        try {
            String nombre = Util.setCodificacionCadena(request.getParameter("nombre"),"ISO-8859-1");
            String descripcion = Util.setCodificacionCadena(request.getParameter("descripcion"),"ISO-8859-1");
            String empresa = usuario.getDstrct();
            String idProceso = request.getParameter("idProceso");
            String resp = "{}";
            if (!dao.existeMetaProceso(empresa, nombre, Integer.parseInt(idProceso))) {
                resp = dao.actualizarMetaProceso(empresa, nombre, descripcion, idProceso, usuario.getLogin());
            } else {
                resp = "{\"error\":\" No se actualiz� el meta proceso, puede que este ya exista\"}";
            }
            this.printlnResponse(resp, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void cargarProcesoInterno(String status) {
        try {
            ArrayList<Categoria> lista;
            if (request.getParameter("procesoMeta") != null) {
                String idProMeta = request.getParameter("procesoMeta");
                lista = dao.cargarProcesoInterno(Integer.parseInt(idProMeta), status);
            } else {
                lista = dao.cargarProcesoInterno();
            }

            Gson gson = new Gson();
            String json = "{\"page\":1,\"rows\":" + gson.toJson(lista) + "}";
            this.printlnResponse(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void guardarProcesoInterno() {
        try {
            String nombre = request.getParameter("nombre");
            String descripcion = request.getParameter("descripcion");
            String procesoMeta = request.getParameter("procesoMeta");
            String resp = "{}";
            if (!dao.existeProcesoInterno(procesoMeta, nombre)) {
                resp = dao.guardarProcesoInterno(procesoMeta, nombre, descripcion, usuario.getLogin(), usuario.getDstrct());
                this.printlnResponse(resp, "application/json;");
            } else {
                resp = "{\"error\":\" No se cre� el proceso interno, puede que el nombre ya exista.\"}";
                this.printlnResponse(resp, "application/json;");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void guardarSubcategoriaRel() {
        try {
            String nombre = Util.setCodificacionCadena(request.getParameter("nombre"),"ISO-8859-1");
            String descripcion = Util.setCodificacionCadena(request.getParameter("descripcion"),"ISO-8859-1");
            String procesoMeta = request.getParameter("procesoMeta");
            String resp = "{}";
            int cod = 0;

            TransaccionService tService = new TransaccionService(usuario.getBd());
            tService.crearStatement();
            if (!dao.existeSubcategoria(nombre, usuario.getDstrct())) {
                cod = dao.guardarSubcategoria(nombre, descripcion, usuario.getLogin(), usuario.getDstrct());
            } else {
                cod = dao.obtenerIdSubcategoria(nombre, usuario.getDstrct());
            }

            if (!dao.existeRelCatSubcategoria(cod, procesoMeta)) {
                dao.guardarRelCatSubcategoria(procesoMeta, cod, usuario.getLogin(), usuario.getDstrct());
            } else {
                dao.actualizarRelCatSubcategoria(procesoMeta, cod, usuario.getDstrct());
            }

            resp = "{\"respuesta\":\"OK\"}";
            this.printlnResponse(resp, "application/json;");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void cargarUnidadesNegocio() {

        try {
            String idProceso = request.getParameter("idProceso");
            ArrayList<Especificaciones> lista = dao.cargarUnidadesNegocio(Integer.parseInt(idProceso));
            Gson gson = new Gson();
            String json = "{\"page\":1,\"rows\":" + gson.toJson(lista) + "}";
            this.printlnResponse(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void asociarUnidadesNegocio() {
        String nomProceso = request.getParameter("nomProceso");
        String idProMeta = request.getParameter("procesoMeta");
        String idProcesointerno = request.getParameter("idProinterno");
        String listado[] = request.getParameter("listado").split(",");
        int idProInterno = 0;
        try {
            TransaccionService tService = new TransaccionService(usuario.getBd());
            tService.crearStatement();
            if (idProcesointerno == null) {
                idProInterno = dao.obtenerIdProcesoInterno(nomProceso, idProMeta);
            } else {
                idProInterno = Integer.parseInt(idProcesointerno);
            }
            for (int i = 0; i < listado.length; i++) {

                String campos[] = listado[i].split("-");

                tService.getSt().addBatch(dao.insertarRelUnidadProInterno(idProInterno, campos[0], campos[1], usuario.getDstrct()));
            }
            tService.execute();
            String resp = "{\"respuesta\":\"OK\"}";
            this.printlnResponse(resp, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void asociarValPred() {
        String idProcesointerno = request.getParameter("idProinterno");
        String listado[] = request.getParameter("listado").split(",");
        int idProInterno = 0;
        try {
            TransaccionService tService = new TransaccionService(usuario.getBd());
            tService.crearStatement();
            if (idProcesointerno != null) {
                idProInterno = Integer.parseInt(idProcesointerno);
            }
            for (int i = 0; i < listado.length; i++) {

                tService.getSt().addBatch(dao.insertarRelValPred(idProInterno, listado[i], usuario.getDstrct(), usuario.getLogin()));
            }
            tService.execute();
            String resp = "{\"respuesta\":\"OK\"}";
            this.printlnResponse(resp, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void cargarUndNegociosProinterno() {
        String idProinterno = request.getParameter("idProinterno");
        try {
            ArrayList<Especificaciones> lista = dao.cargarUndNegocioProinterno(idProinterno);
            Gson gson = new Gson();
            String json = "{\"page\":1,\"rows\":" + gson.toJson(lista) + "}";
            this.printlnResponse(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void actualizarProinterno() {
        try {
            int idProinterno = Integer.parseInt(request.getParameter("idProinterno"));
            String nombre = Util.setCodificacionCadena(request.getParameter("nomProinterno"),"ISO-8859-1");
            String descripcion = Util.setCodificacionCadena(request.getParameter("descProinterno"),"ISO-8859-1");
            String idProMeta = request.getParameter("idProMeta");
            int idSubcategoria = Integer.parseInt(request.getParameter("idSubcategoria"));
            String resp = "{}";
            if (!dao.existeProcesoInterno(idProMeta, nombre, idProinterno)) {
                resp = dao.actualizarProcesoInterno(idProinterno, nombre, descripcion, Integer.parseInt(idProMeta), usuario.getLogin(), usuario.getDstrct(), idSubcategoria);
            } else {
                resp = "{\"error\":\" No se actualiz� el proceso interno, puede que ya exista.\"}";
            }
            this.printlnResponse(resp, "application/json;");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void desasociarUndProinterno() {
        try {
            String idSubcategoria = request.getParameter("idSubcategoria");
            String idProInterno = request.getParameter("idProinterno");
            String listado[] = request.getParameter("listado").split(",");
            String resp = "";
            try {

                if (!dao.verificaExisteSub(Integer.parseInt(idProInterno))) {

                    TransaccionService tService = new TransaccionService(usuario.getBd());
                    tService.crearStatement();
                    for (int i = 0; i < listado.length; i++) {
                        tService.getSt().addBatch(dao.eliminarUndProinterno(idSubcategoria, listado[i]));
                    }
                    tService.execute();
                    resp = "{\"respuesta\":\"OK\"}";

                } else {
                    resp = "{\"respuesta\":\"OK1\"}";
                }

                this.printlnResponse(resp, "application/json;");
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void listarUsuario() {
        try {
            ArrayList<Usuario> lista = dao.listarUsuario();
            Gson gson = new Gson();
            String json = "{\"page\":1,\"rows\":" + gson.toJson(lista) + "}";
            this.printlnResponse(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void listarProinternoUsuario() {
        try {
            String codUsuario = request.getParameter("codUsuario");
            ArrayList<Categoria> lista = dao.listarProinternoUsuario(Integer.parseInt(codUsuario));
            Gson gson = new Gson();
            String json = "{\"page\":1,\"rows\":" + gson.toJson(lista) + "}";
            this.printlnResponse(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void listarProinterno() {
        try {
            ArrayList<Categoria> lista = dao.listarProinterno();
            Gson gson = new Gson();
            String json = "{\"page\":1,\"rows\":" + gson.toJson(lista) + "}";
            this.printlnResponse(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void asociarProinternoUsuario() {
        String listado[] = request.getParameter("listado").split(",");
        String codUsuario = request.getParameter("codUsuario");
        String login = request.getParameter("login");
        try {
            TransaccionService tService = new TransaccionService(usuario.getBd());
            tService.crearStatement();

            for (int i = 0; i < listado.length; i++) {
                tService.getSt().addBatch(dao.insertarRelProInternoUser(listado[i], Integer.parseInt(codUsuario), login, usuario.getDstrct()));
            }
            tService.execute();
            this.printlnResponse("OK", "application/text;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void anularProcesosMeta() {
        try {
            int idProceso = Integer.parseInt(request.getParameter("idProceso"));
            String resp = dao.anularMetaProceso(idProceso, usuario.getLogin());
            this.printlnResponse(resp, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void anularProcesoInterno() {
        try {
            int idProinterno = Integer.parseInt(request.getParameter("idProinterno"));
            int idProcesoMeta = Integer.parseInt(request.getParameter("idProcesoMeta"));
            String resp = dao.anularProcesoInterno(idProinterno, idProcesoMeta, usuario.getLogin());
            this.printlnResponse(resp, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void cargarComboProcesosMeta() {
        try {
            String json = dao.cargarComboProcesoMeta(usuario.getDstrct());
            this.printlnResponse(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void listarUsuariosProinterno() {
        try {
            String idProceso = request.getParameter("idProceso");
            ArrayList<Usuario> lista = dao.listarUsuariosProinterno(Integer.parseInt(idProceso));
            Gson gson = new Gson();
            String json = "{\"page\":1,\"rows\":" + gson.toJson(lista) + "}";
            this.printlnResponse(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void listarUsuariosRelProInterno() {
        try {
            String idProceso = request.getParameter("idProceso");
            ArrayList<Usuario> lista = dao.listarUsuariosRelProInterno(Integer.parseInt(idProceso));
            Gson gson = new Gson();
            String json = "{\"page\":1,\"rows\":" + gson.toJson(lista) + "}";
            this.printlnResponse(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void asociarUsuariosProinterno() {
        String listado = request.getParameter("listado");
        String idProInterno = request.getParameter("idProInterno");

        try {
            JsonParser jsonParser = new JsonParser();
            JsonObject jo = (JsonObject) jsonParser.parse(listado);
            JsonArray jsonArr = jo.getAsJsonArray("usuarios");

            TransaccionService tService = new TransaccionService(usuario.getBd());
            tService.crearStatement();
            for (int i = 0; i < jsonArr.size(); i++) {
                String codUsuario = jsonArr.get(i).getAsJsonObject().get("cod_usuario").getAsJsonPrimitive().getAsString();
                String login = jsonArr.get(i).getAsJsonObject().get("id_usuario").getAsJsonPrimitive().getAsString();
                tService.getSt().addBatch(dao.insertarRelProInternoUser(idProInterno, Integer.parseInt(codUsuario), login, usuario.getDstrct()));
            }
            tService.execute();
            String resp = "{\"respuesta\":\"OK\"}";
            this.printlnResponse(resp, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void desasociarUsuariosProinterno() {
        try {
            String idProInterno = request.getParameter("idProinterno");
            String listado[] = request.getParameter("listado").split(",");
            try {
                TransaccionService tService = new TransaccionService(usuario.getBd());
                tService.crearStatement();
                for (int i = 0; i < listado.length; i++) {
                    tService.getSt().addBatch(dao.eliminarUsuarioProinterno(Integer.parseInt(idProInterno), listado[i]));
                }
                tService.execute();
                String resp = "{\"respuesta\":\"OK\"}";
                this.printlnResponse(resp, "application/json;");
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void existenUsuariosRelProceso() {
        try {
            String id = request.getParameter("idProceso");
            String tipoProceso = request.getParameter("tipo");
            String resp = "{}";
            if (dao.existenUsuariosRelProceso(Integer.parseInt(id), tipoProceso)) {
                resp = "{\"respuesta\":\"SI\"}";
                this.printlnResponse(resp, "application/json;");
            } else {
                resp = "{\"respuesta\":\"NO\"}";
                this.printlnResponse(resp, "application/json;");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void activarProcesosMeta() {
        try {
            int idProceso = Integer.parseInt(request.getParameter("idProceso"));
            String resp = dao.activarMetaProceso(idProceso);
            this.printlnResponse(resp, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void activarProcesoInterno() {
        try {
            int idProinterno = Integer.parseInt(request.getParameter("idProinterno"));
            String resp = dao.activarProcesoInterno(idProinterno);
            this.printlnResponse(resp, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void actualizarModerador() {
        try {
            int idProinterno = Integer.parseInt(request.getParameter("idProinterno"));
            int idusuario = Integer.parseInt(request.getParameter("idusuario"));
            String moderador = request.getParameter("moderador");
            String resp = dao.actualizarModerador(idProinterno, idusuario, moderador);
            this.printlnResponse(resp, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void cargarUsuariosRelProInterno() {
        try {
            ArrayList<Usuario> lista = dao.listarUsuariosRelProInterno();
            Gson gson = new Gson();
            String json = "{\"page\":1,\"rows\":" + gson.toJson(lista) + "}";
            this.printlnResponse(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void cargarRelProcesosUsuario() {
        try {
            String usuario_proceso = (request.getParameter("usuario_proc") != null) ? request.getParameter("usuario_proc") : "";
            ArrayList<Categoria> lista = dao.cargarProcesosRelUsuario(usuario_proceso);
            Gson gson = new Gson();
            String json = "{\"page\":1,\"rows\":" + gson.toJson(lista) + "}";
            this.printlnResponse(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void cargarRelTipoRequisicionUsuario() {
        try {
            String usuario_proceso = (request.getParameter("usuario_proc") != null) ? request.getParameter("usuario_proc") : "";
            ArrayList<Categoria> lista = dao.cargarTipoRequisicionRelUsuario(usuario_proceso);
            Gson gson = new Gson();
            String json = "{\"page\":1,\"rows\":" + gson.toJson(lista) + "}";
            this.printlnResponse(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void asignarProcesosReqUsuario() {
        String idUsuario = (request.getParameter("id_usuario") != null) ? request.getParameter("id_usuario") : "";
        String login = (request.getParameter("login") != null) ? request.getParameter("login") : "";
        String listado[] = request.getParameter("listado").split(",");
        try {
            TransaccionService tService = new TransaccionService(usuario.getBd());
            tService.crearStatement();
            tService.getSt().addBatch(dao.eliminarRelProcesoReqUser(Integer.parseInt(idUsuario)));
            for (int i = 0; i < listado.length; i++) {
                tService.getSt().addBatch(dao.insertarRelProcesoReqUser(listado[i], Integer.parseInt(idUsuario), login, usuario));
            }
            tService.execute();
            String resp = "{\"respuesta\":\"OK\"}";
            this.printlnResponse(resp, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void asignarTiposReqUsuario() {
        String idUsuario = (request.getParameter("id_usuario") != null) ? request.getParameter("id_usuario") : "";
        String login = (request.getParameter("login") != null) ? request.getParameter("login") : "";
        String listado[] = request.getParameter("listado").split(",");
        try {
            TransaccionService tService = new TransaccionService(usuario.getBd());
            tService.crearStatement();
            tService.getSt().addBatch(dao.eliminarRelTipoReqUser(Integer.parseInt(idUsuario)));
            for (int i = 0; i < listado.length; i++) {
                tService.getSt().addBatch(dao.insertarRelTipoReqUser(listado[i], Integer.parseInt(idUsuario), login, usuario));
            }
            tService.execute();
            String resp = "{\"respuesta\":\"OK\"}";
            this.printlnResponse(resp, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void cargar_combo() throws IOException {
        ctrlRequest = true;
        JsonObject info = (JsonObject) (new JsonParser()).parse(Util.setCodificacionCadena(request.getParameter("informacion"),"ISO-8859-1"));
        respuesta = dao.cargar_combo(info);
        response.setContentType("application/json; charset=ISO-8859-1");
        response.setHeader("Cache-Control", "no-cache");
        response.getWriter().println((new Gson()).toJson(respuesta));
    }

    /*private void cargarComboInsumos() {
     try {
     String json = dao.cargarComboInsumos(usuario.getDstrct());
     this.printlnResponse(json, "application/json;");
     } catch (Exception e) {
     e.printStackTrace();
     }
     }*/
    private void cargarComboInsumos() throws IOException {
        ctrlRequest = true;
        JsonObject info = (JsonObject) (new JsonParser()).parse(request.getParameter("informacion"));
        respuesta = dao.cargar_combo_insumos(info);
        response.setContentType("application/json; charset=ISO-8859-1");
        response.setHeader("Cache-Control", "no-cache");
        response.getWriter().println((new Gson()).toJson(respuesta));
    }

    private void cargarNom_subCat() {
        try {
            ArrayList<String> lista = dao.cargarNom_subCat(request.getParameter("insumo"));
            Gson gson = new Gson();
            String json = gson.toJson(lista);
            this.printlnResponse(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void cargarNom_Especificaciones() {
        try {
            ArrayList<String> lista = dao.cargarNom_Especificacion(request.getParameter("categoria"));
            Gson gson = new Gson();
            String json = gson.toJson(lista);
            this.printlnResponse(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void exportarExcelInsumos() throws Exception {
        try {
            String resp1 = "";
            String url = "";
            String json = request.getParameter("listado") != null ? request.getParameter("listado") : "";
            String subcategoria = request.getParameter("subcategoria");
            JsonArray asJsonArray = (JsonArray) new JsonParser().parse(json);
            //Set<Map.Entry<String, JsonElement>> entrySet = object.entrySet();
            
            byte[] bytes = subcategoria.getBytes("ISO-8859-1");
            subcategoria = new String(bytes, "UTF-8");

            this.generarRUTA();
            this.crearLibro("Insumos_", "Codigo");
            String[] cabecera = {"Descripcion Insumo", "Codigo Insumo"};

            short[] dimensiones = new short[]{5000, 5000};

            this.generaTitulos(cabecera, dimensiones);

            for (int i = 0; i < asJsonArray.size(); i++) {
                JsonObject objects = (JsonObject) asJsonArray.get(i);
                fila++;
                int col = 0;
                xls.adicionarCelda(fila, col++, objects.get("descripcion").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("codigogenerado").getAsString(), letra);
            }

            this.cerrarArchivo();

            fmt = new SimpleDateFormat("yyyMMdd HH:mm");
            url = request.getContextPath() + "/exportar/migracion/" + usuario.getLogin().toUpperCase() + "/Insumos_" + fmt.format(new Date()) + ".xls";
            resp1 = Utility.getIcono(request.getContextPath(), 5) + "Se ha generado con exito.<br/><br/>"
                    + Utility.getIcono(request.getContextPath(), 7) + " <a href='" + url + "' >Ver Documento</a></td>";

            this.printlnResponse(resp1, "text/plain");
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
    }

    private void generarRUTA() throws Exception {
        try {

            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            rutaInformes = rb.getString("ruta") + "/exportar/migracion/" + usuario.getLogin();
            File archivo = new File(rutaInformes);
            if (!archivo.exists()) {
                archivo.mkdirs();
            }

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
    }

    private void crearLibro(String nameFileParcial, String titulo) throws Exception {
        try {
            fmt = new SimpleDateFormat("yyyMMdd HH:mm");
            this.crearArchivo(nameFileParcial + fmt.format(new Date()) + ".xls", titulo);

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception("Error en generarArchivo ...\n" + ex.getMessage());
        }
    }

    private void crearArchivo(String nameFile, String titulo) throws Exception {
        try {
            InitArchivo(nameFile);
            xls.obtenerHoja("hoja1");
            // xls.combinarCeldas(0, 0, 0, 8);
            // xls.adicionarCelda(0,0, titulo, header);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception("Error en crearArchivo ....\n" + ex.getMessage());
        }
    }

    private void InitArchivo(String nameFile) throws Exception {
        try {
            xls = new com.tsp.operation.model.beans.POIWrite();
            nombre = "/exportar/migracion/" + usuario.getLogin() + "/" + nameFile;
            xls.nuevoLibro(rutaInformes + "/" + nameFile);
            header = xls.nuevoEstilo("Tahoma", 10, true, false, "text", HSSFColor.GREEN.index, xls.NONE, HSSFCellStyle.ALIGN_LEFT);
            titulo1 = xls.nuevoEstilo("Tahoma", 8, true, false, "text", xls.NONE, xls.NONE, xls.NONE);
            titulo2 = xls.nuevoEstilo("Tahoma", 8, true, false, "text", HSSFColor.WHITE.index, HSSFColor.GREEN.index, HSSFCellStyle.ALIGN_CENTER, 2);
            letra = xls.nuevoEstilo("Tahoma", 8, false, false, "", xls.NONE, xls.NONE, xls.NONE);
            dinero = xls.nuevoEstilo("Tahoma", 8, false, false, "#,##0.00", xls.NONE, xls.NONE, xls.NONE);
            dinero2 = xls.nuevoEstilo("Tahoma", 8, false, false, "#,##0", xls.NONE, xls.NONE, xls.NONE);
            porcentaje = xls.nuevoEstilo("Tahoma", 8, false, false, "0.00%", xls.NONE, xls.NONE, xls.NONE);
            ftofecha = xls.nuevoEstilo("Tahoma", 8, false, false, "yyyy-mm-dd", xls.NONE, xls.NONE, xls.NONE);

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception("Error en InitArchivo .... \n" + ex.getMessage());
        }

    }

    private void generaTitulos(String[] cabecera, short[] dimensiones) throws Exception {
        try {

            fila = 0;

            for (int i = 0; i < cabecera.length; i++) {
                xls.adicionarCelda(fila, i, cabecera[i], titulo2);
                if (i < dimensiones.length) {
                    xls.cambiarAnchoColumna(i, dimensiones[i]);
                }
            }

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception("Error en generarArchivo ...\n" + ex.getMessage());
        }
    }

    private void cerrarArchivo() throws Exception {
        try {
            if (xls != null) {
                xls.cerrarLibro();
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            throw new Exception("Error en crearArchivo ....\n" + ex.getMessage());
        }
    }

    private void anularInsumo() {
        try {
            String id = request.getParameter("id");
            String resp = "{}";
            if (dao.anularInsumo(Integer.parseInt(id))) {
                resp = "{\"respuesta\":\"SI\"}";
                this.printlnResponse(resp, "application/json;");
            } else {
                resp = "{\"respuesta\":\"NO\"}";
                this.printlnResponse(resp, "application/json;");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void CargarMapeo() {
        try {
            ArrayList<Mapeo_insumos> lista = dao.cargarMapeo(request.getParameter("subcategoria"));
            Gson gson = new Gson();
            String json = "{\"page\":1,\"rows\":" + gson.toJson(lista) + "}";
            this.printlnResponse(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void exportarExcelInsumosMapeo() throws Exception {
        try {
            String resp1 = "";
            String url = "";
            String json = request.getParameter("listado") != null ? request.getParameter("listado") : "";
            JsonArray asJsonArray = (JsonArray) new JsonParser().parse(json);
            this.generarRUTA();
            this.crearLibro("Insumos", "Codigo");
            //String[] cabecera = {"Descripcion Insumo", "Codigo Insumo"};
            String[] cabecera = {"Id", "Cod. Material", "Descripcion", "Fecha", "Nit", "Nomb. Proveedor", "Cantidad", "Precio Unitario"};

            short[] dimensiones = new short[]{5000, 5000, 5000, 5000, 5000, 5000, 5000};

            this.generaTitulos(cabecera, dimensiones);

            for (int i = 0; i < asJsonArray.size(); i++) {
                JsonObject objects = (JsonObject) asJsonArray.get(i);
                fila++;
                int col = 0;
                xls.adicionarCelda(fila, col++, objects.get("id").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("codigo_material").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("descripcion").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("fecha").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("nit_proveedor").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("payment_name").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("cantidad").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("valor_unitario").getAsString(), letra);
            }

            this.cerrarArchivo();

            fmt = new SimpleDateFormat("yyyMMdd HH:mm");
            url = request.getContextPath() + "/exportar/migracion/" + usuario.getLogin().toUpperCase() + "/Insumos" + fmt.format(new Date()) + ".xls";
            resp1 = Utility.getIcono(request.getContextPath(), 5) + "Se ha generado con exito.<br/><br/>"
                    + Utility.getIcono(request.getContextPath(), 7) + " <a href='" + url + "' >Ver Documento</a></td>";

            this.printlnResponse(resp1, "text/plain");
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
    }
    
    private void guardar_tipo_insumo() {
        try {
            String nombre = Util.setCodificacionCadena(request.getParameter("nombre"),"ISO-8859-1");
            String porc_rentabilidad = (request.getParameter("porc_rentabilidad")!=null) ? request.getParameter("porc_rentabilidad"):"0";
            String iva_con_aiu = (request.getParameter("iva_con_aiu")!=null) ? request.getParameter("iva_con_aiu"):"0";
            String empresa = usuario.getDstrct();

            String resp = "{}";
            if (!dao.existeTipoInsumo(empresa, nombre, "")) {
                resp = dao.guardarTipoInsumo(empresa, nombre, porc_rentabilidad, iva_con_aiu, usuario.getLogin());
            } else {
                resp = "{\"error\":\" No se creo el Tipo Insumo, puede que el nombre ya exista\"}";
            }
            this.printlnResponse(resp, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void cargar_combo_tipo_insumos() {
        try {
            String json = dao.cargar_combo_tipo_insumos();
            this.printlnResponseAjax(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void cargar_combo_categoria() {
        try {
            
            String json = dao.cargar_combo_categoria(Integer.parseInt(request.getParameter("tipo_insumo")));
            this.printlnResponseAjax(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private void cargar_tipo_insumo() {
        try {

            String json = dao.CargarTipoInsumos();
            this.printlnResponseAjax(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private void actualizar_tipo_insumo() {
        try {
            String id = request.getParameter("id") != null ? request.getParameter("id") : "";
            String nombre = Util.setCodificacionCadena(request.getParameter("nombre"),"ISO-8859-1");
            String porc_rentabilidad = (request.getParameter("porc_rentabilidad")!=null) ? request.getParameter("porc_rentabilidad"):"0";
            String iva_con_aiu = (request.getParameter("iva_con_aiu")!=null) ? request.getParameter("iva_con_aiu"):"0";
            String empresa = usuario.getDstrct();

            String resp = "{}";
            if (!dao.existeTipoInsumo(empresa, nombre,id)) {
                resp = dao.actualizarTipoInsumo(id, nombre, porc_rentabilidad, iva_con_aiu, usuario);
            } else {
                resp = "{\"error\":\" No se creo el Tipo Insumo, puede que el nombre ya exista\"}";
            }
            this.printlnResponse(resp, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void activaInactivaTipoInsumo() {

        try {
            String id = request.getParameter("id") != null ? request.getParameter("id") : "";
            String resp  = dao.activaInactivaTipoInsumo(id, usuario);
            this.printlnResponse(resp, "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(MaestroProyectoAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

}
