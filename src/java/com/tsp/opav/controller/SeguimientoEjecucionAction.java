package com.tsp.opav.controller;

import com.tsp.exceptions.InformationException;
import com.tsp.opav.model.beans.Actividades;
import com.tsp.opav.model.beans.OfertaSeguimiento;
import javax.servlet.ServletException;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.opav.model.threads.HSeguimientoEjecucionPDF;
import java.util.ArrayList;
import java.util.Vector;
import javax.servlet.http.HttpSession;
//Para generar el grafico
import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import org.jfree.chart.*;
import org.jfree.chart.plot.*;
import org.jfree.chart.renderer.LineAndShapeRenderer;
import org.jfree.data.category.DefaultCategoryDataset;


/**
 * SeguimientoEjecucionAction.java<br/>
 * Action para el programa de seguimiento a la ejecucion de los trabajos<br/>
 * 8/06/2010
 * @author darrieta-GEOTECH
 * @version 1.0
 */
public class SeguimientoEjecucionAction extends Action{

    private String next="";
    private Usuario usuario;
    private boolean redirect = true;
    private int numFechas;

    @Override
    public void run() throws ServletException, InformationException {
        try {
            HttpSession session = request.getSession();
            usuario = (Usuario) session.getAttribute("Usuario");
            String opcion = request.getParameter("opcion");

            if(opcion.equals("guardar")){
                guardarProgramacion("G");
            }else if(opcion.equals("confirmar")){
                guardarProgramacion("");
            }else if(opcion.equals("ajustes")){
                guardarAjustesProgramacion();
            }else if(opcion.equals("grafico")){
                generarGrafico();
            }else if(opcion.equals("guardarSeguimiento")){
                this.guardarSeguimiento("G");
            }else if(opcion.equals("confirmarSeguimiento")){
                guardarSeguimiento("");
            }else if(opcion.equals("generarPDF")){
                generarPDF();
            }else if(opcion.equals("generarCronograma")){
                generarCronograma();
            }

            if(redirect){
                this.dispatchRequest(next);
            }

        } catch (Exception e) {
            e.printStackTrace();
            throw new ServletException("Error en SeguimientoEjecucionAction: "+e.getMessage());
        }
    }

    private void guardarProgramacion(String reg_status) throws Exception{
        String idAccion = request.getParameter("idAccion");
        String idSolicitud = request.getParameter("idSolicitud");
        //int numActividades = Integer.parseInt(request.getParameter("cont"));
        int maxId = Integer.parseInt(request.getParameter("maximo"));
        ArrayList listaActividades = new ArrayList();

        for (int i = 1; i <= maxId; i++) {
            if(request.getParameter("cmbActividad"+i)!=null){
                Actividades act = new Actividades();
                String idActividad = request.getParameter("cmbActividad"+i).split(";")[0];
                act.setId(Integer.parseInt(idActividad));
                act.setResponsable(request.getParameter("cmbResponsable"+i));
                String peso = request.getParameter("txtPeso"+i).equals("")?"0":request.getParameter("txtPeso"+i);
                act.setPeso(Integer.parseInt(peso));
                String fechai = request.getParameter("fechaInicial").equals("")?"0099-01-01":request.getParameter("fechaInicial");
                act.setFecha(fechai);
                fechai = request.getParameter("txtFechaInicial"+i).equals("")?null:request.getParameter("txtFechaInicial"+i);
                act.setFechaInicial(fechai);
                String fechaf = request.getParameter("txtFechaFinal"+i).equals("")?null:request.getParameter("txtFechaFinal"+i);
                act.setFechaFinal(fechaf);
                act.setId_accion(idAccion);
                act.setReg_status(reg_status);
                act.setCreation_user(usuario.getLogin());
                act.setUser_update(usuario.getLogin());
                listaActividades.add(act);
            }
        }
        Vector comandos = model.seguimientoEjecucionService.guardarProgramacionActividades(listaActividades);
        //JPACOSTA, Dic 2013.
        if (!request.getParameter("fechaInicial").equals("")) {
            comandos.add(model.seguimientoEjecucionService.actualizarFechaInicial(
                    idSolicitud,
                    request.getParameter("fechaInicial")));
        }
        model.applusService.ejecutarSQL(comandos);
        String mensaje;
        if(reg_status.equals("G")){
            mensaje = "Se han guardado los cambios";
        }else{
            mensaje = "Se ha confirmado la programacion";
        }
        redirect = true;
        next="/jsp/opav/programacionActividades.jsp?id_accion="+idAccion+"&id_solicitud="+idSolicitud+"&mensaje="+mensaje;
    }

    private void guardarAjustesProgramacion() throws Exception{

        int numActividades = Integer.parseInt(request.getParameter("numActividades"));
        Vector comandos = new Vector();
        //JPACOSTA, Dic 2013.
        if (!request.getParameter("txtFecha").equals("")) {
            comandos.add(model.seguimientoEjecucionService.actualizarFechaInicial(
                    request.getParameter("id_solicitud"),
                    request.getParameter("txtFecha")));
        }
        for (int i = 0; i < numActividades; i++) {

            String fechaInicial = request.getParameter("txtFechaInicial"+i);
            String fechaFinal = request.getParameter("txtFechaFinal"+i);
            String finicial_original = request.getParameter("hidFechaInicial"+i);
            String ffinal_original = request.getParameter("hidFechaFinal"+i);

            if(!fechaInicial.equals(finicial_original) || !fechaFinal.equals(ffinal_original)){
                Actividades act = new Actividades();
                int peso = Integer.parseInt(request.getParameter("hidPeso"+i));
                String idAccion = request.getParameter("hidIdAccion"+i);
                int idActividad = Integer.parseInt(request.getParameter("hidIdActividad"+i));
                act.setPeso(peso);
                act.setId_accion(idAccion);
                act.setId(idActividad);
                act.setResponsable(request.getParameter("hidResponsable"+i));
                act.setFecha(request.getParameter("txtFecha").equals("")?
                        "0099-01-01 00:00:00" : request.getParameter("txtFecha"));
                act.setFechaInicial(fechaInicial);
                act.setFechaFinal(fechaFinal);
                act.setReg_status("");
                act.setUser_update(usuario.getLogin());
                comandos.add(model.seguimientoEjecucionService.actualizarActividadAccion(act));
            }
        }
        model.applusService.ejecutarSQL(comandos);
        redirect = true;
        next="/jsp/opav/ajustesProgramacion.jsp?id_solicitud="+request.getParameter("id_solicitud")+"&mensaje=Se han guardado los cambios";
    }

    private void guardarSeguimiento(String regStatus) throws Exception{

        int numActividades = Integer.parseInt(request.getParameter("numActividades"));
        String fecha = request.getParameter("txtFecha");
        String idSolicitud = request.getParameter("id_solicitud");
        //String observacion = request.getParameter("areaObservacion").trim();
        //Setear bean para guardar observaciones y totales
        float avanceEsperado = Float.parseFloat(request.getParameter("hidTotalAvanceEsp"));
        float avanceRegistrado = Float.parseFloat(request.getParameter("hidTotalAvance"));
        OfertaSeguimiento ofertaSeg = new OfertaSeguimiento();
        ofertaSeg.setId_solicitud(idSolicitud);
        ofertaSeg.setFecha(fecha);
        ofertaSeg.setObservaciones("");
        ofertaSeg.setAvance_esperado(avanceEsperado);
        ofertaSeg.setAvance_registrado(avanceRegistrado);
        ofertaSeg.setReg_status(regStatus);
        ofertaSeg.setCreation_user(usuario.getLogin());
        ofertaSeg.setUser_update(usuario.getLogin());

        ArrayList listaActividades = new ArrayList();
        for (int i = 0; i < numActividades; i++) {

            Actividades act = new Actividades();
            float avance = Float.parseFloat(request.getParameter("txtAvance"+i));
            avanceEsperado = Float.parseFloat(request.getParameter("hidAvanceEsperado"+i));
            act.setAvance(avance);
            act.setAvance_esperado(avanceEsperado);
            act.setId_accion(request.getParameter("hidIdAccion"+i));
            int idActividad = Integer.parseInt(request.getParameter("hidIdActividad"+i));
            act.setId(idActividad);
            act.setFecha(fecha);
            act.setId_solicitud(idSolicitud);
            act.setReg_status(regStatus);
            act.setCreation_user(usuario.getLogin());
            act.setUser_update(usuario.getLogin());
            act.setObservaciones(request.getParameter("comentario"+i));
            listaActividades.add(act);
        }
        Vector comandos = model.seguimientoEjecucionService.guardarSeguimientoActividades(listaActividades, ofertaSeg);
        String mensaje;
        if(regStatus.equals("G")){
            mensaje = "Se han guardado los cambios";
        }else{
            mensaje = "Se ha confirmado el seguimiento";
        }
        model.applusService.ejecutarSQL(comandos);
        redirect = true;
        next="/jsp/opav/seguimientoEjecucion.jsp?id_solicitud="+idSolicitud+"&mensaje="+mensaje;
    }

    public JFreeChart crearChart() throws Exception{

        String idSolicitud = request.getParameter("id_solicitud");
        String fecha = model.seguimientoEjecucionService.obtenerFechaFinalSolicitud(idSolicitud);
        ArrayList<OfertaSeguimiento> totales = model.seguimientoEjecucionService.consultarTotalesSeguimientos2(idSolicitud);
        numFechas = totales.size();

        if(numFechas == 0) return null;
        
        final String series1 = "Avance esperado";
        final String series2 = "Avance registrado";

        // create the dataset...
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();

        String ultimaFecha = "";
        for (int i = 0; i < totales.size(); i++) {
            dataset.addValue(totales.get(i).getAvance_registrado(), series2, totales.get(i).getFecha());
            dataset.addValue(totales.get(i).getAvance_esperado(), series1, totales.get(i).getFecha());
            if(i == (totales.size()-1)){
                ultimaFecha = totales.get(i).getFecha();
            }
        }

        //Proyectar hasta la fecha de finalizacion del proyecto
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Date ultimaFechaD = df.parse(ultimaFecha);
        Date fechaFinal = df.parse(fecha);
        if( ultimaFechaD.compareTo(fechaFinal) < 0){
            // Calcular el numero de dias
            long diferencia = fechaFinal.getTime() - ultimaFechaD.getTime();
            int dias = (int)Math.floor(diferencia / (1000 * 60 * 60 * 24));// / MILLSECS_PER_DAY;
            dias = dias / 7;
            System.out.println("dias: "+dias);

            for (int i = 0; i < (dias-1); i++) {
                //calcular avance esperado
                Calendar cal = Calendar.getInstance();
                cal.setTime( ultimaFechaD );
                cal.add( Calendar.DATE, 7*(i+1) );
                String fechai = df.format(cal.getTime());
                ArrayList<Actividades> consulta = model.seguimientoEjecucionService.consultarAvanceSolicitud(idSolicitud,fechai);
                float totalAvanceEsp=0;
                for (int j = 0; j < consulta.size(); j++) {
                    Actividades act = consulta.get(j);
                    float avEsperado = (100*act.getDias_actual()) / act.getDias_actividad();
                    avEsperado = (avEsperado<100)?avEsperado:100;
                    totalAvanceEsp += (act.getPeso_obra()*avEsperado)/100;
                }
                dataset.addValue(totalAvanceEsp, series1, fechai);
            }
            numFechas += dias;
            dataset.addValue(100, series1, fecha);
        }


        JFreeChart chart = ChartFactory.createLineChart(
            "Seguimientos a la ejecución",       // chart title
            "Fecha",                   // domain axis label
            "Avance",                  // range axis label
            dataset,                   // data
            PlotOrientation.VERTICAL,
            true,                      // include legend
            true,                      // tooltips
            false                      // urls
        );
        
      /*  CategoryPlot plot = (CategoryPlot) chart.getPlot();
        LineAndShapeRenderer renderer = (LineAndShapeRenderer) plot.getRenderer();
        renderer.setDrawShapes(true); */

        redirect = false;
        return chart;
    }

    private void generarGrafico() throws Exception{
        response.setContentType("image/jpeg");

        OutputStream salida = response.getOutputStream();
        JFreeChart grafica = crearChart();

        int ancho = 100 + (numFechas * 100);
        ancho = (ancho>400)?ancho:400;
        int alto = 500;

        ChartUtilities.writeChartAsJPEG(salida,grafica,ancho,alto);

        salida.close();

    }

    private void generarPDF() throws Exception{
        String idSolicitud = request.getParameter("id_solicitud");

        HSeguimientoEjecucionPDF hilo = new HSeguimientoEjecucionPDF();
        hilo.start("",usuario.getLogin(), model, idSolicitud, crearChart(), (numFechas * 100));

        redirect = true;
        String mensaje="Se ha iniciado la generación del archivo PDF";
        next="/jsp/opav/consultaEjecucion.jsp?id_solicitud="+idSolicitud+"&mensaje="+mensaje;
    }
    
    private void generarCronograma() throws Exception {
        String idSolicitud = request.getParameter("id_solicitud");
        
        HSeguimientoEjecucionPDF hilo = new HSeguimientoEjecucionPDF();
        hilo.start("cronograma", usuario.getLogin(), model, idSolicitud, null, 0);
        
        redirect = false;
        String mensaje="Se ha iniciado la generación del archivo PDF";
    }
}