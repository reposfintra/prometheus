/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.opav.controller;
import java.util.*;
import javax.servlet.http.*;
import com.tsp.opav.model.beans.*;
import com.tsp.opav.model.services.*;
import com.tsp.operation.model.beans.Usuario;
/**
 *
 * @author Rhonalf
 */
public class CotizacionVerAction extends Action {

    Usuario usuario;
    public CotizacionVerAction(){

    }

    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        String next  = "/jsp/opav/cotizacion/ver_cotizacion.jsp";
        CotizacionService csr;
        request.setAttribute("msg", "");
        ArrayList list = new ArrayList();
        try {
            HttpSession session = request.getSession();
            usuario = (Usuario) session.getAttribute("Usuario");
           //jjcastro
            String cons = request.getParameter("consecutivo")!=null? request.getParameter("consecutivo"):"";
            csr = new CotizacionService(usuario.getBd());
            CotizacionService cserv = new CotizacionService(usuario.getBd());
            ArrayList listica = cserv.buscar(cons, "id_accion");
            request.setAttribute("listado", listica);

            		ArrayList ver = (ArrayList)request.getAttribute("listado");
			int tam = ver.size();
			Cotizacion compra = null;
			String cons1 = "";
			if(tam>0) {
				for(int i=0;i<tam;i++){
					compra=(Cotizacion)ver.get(i);
					cons = compra.getCodigo();


                                }
                        }

           list = csr.verDetalles(cons);
           if(list.size()>0) {
               request.setAttribute("lista", list);
               request.setAttribute("msg", "");
           }
           else request.setAttribute("msg", "No hay datos que mostrar");
        }
        catch(Exception e){
            System.out.println("Error en el run del action ver datos: "+e.toString());
            request.setAttribute("msg", "Error en la visualizacion");
        }
        this.dispatchRequest(next);
    }

}