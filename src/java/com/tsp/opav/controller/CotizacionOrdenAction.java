/*
 * CotizacionOrdenAction.java
 * Creado por: Ing. Jose Castro
 * Date: 16 Junio 2010
 *
 */
package com.tsp.opav.controller;
import com.tsp.operation.model.beans.Usuario;
import javax.servlet.http.*;
import java.util.*;
import com.tsp.opav.model.services.*;
import com.tsp.opav.model.beans.*;
/**
 *
 * @author Ing. Jose Castro
 */
public class CotizacionOrdenAction  extends Action{


   //Opciones dentro de la clase:


//Opciones para cargar la plantilla de la cotizacion
    private final int CARGAR_COTIZACIONES   = 0;
    private final int CONSULTAR_COTIZACION =  1;
    private final int AGREGAR_COTIZACION    =  2;

    //Parte Cotizacion
    private final int COTIZACION_NUEVA    =  3;
    private final int LISTAR_CATEGORIA_TIPO = 4;
    private final int LISTAR_MATERIALES = 5;
    private final int CONSULTA_PRECIO = 6;

    private final int CARGAR_ITEMS = 7;
    private final int AGREGAR_ITEM = 8;


    CotizacionService cotizacionService;
    MaterialesService materialesService;


    String next;
    String loginx;
    Usuario usuario;
    
    public CotizacionOrdenAction(){
     cotizacionService =  new CotizacionService();
     materialesService = new MaterialesService();
     this.next = "/jsp/opav/cotizacion/listadocotizacionesaux.jsp";
    }

    public void run() throws javax.servlet.ServletException {

                    try {
                        int op = (request.getParameter("opcion") != null) ? Integer.parseInt(request.getParameter("opcion").toString()) : -1;
                        HttpSession session = request.getSession();
                        com.tsp.util.Util.logController(((com.tsp.operation.model.beans.Usuario) session.getAttribute("Usuario")).getLogin(), this.getClass().getName());
                        usuario = (Usuario) session.getAttribute("Usuario");
                        loginx=usuario.getLogin();



                        switch (op) {
                            case CARGAR_COTIZACIONES:
                                    String contratista = request.getParameter("contratista").toString()!=null?request.getParameter("contratista"):"";
                                    String id_accion_inicial = request.getParameter("id_accion_inicial").toString()!=null?request.getParameter("id_accion_inicial"):"";
                                    CotizacionService clvsrv = new CotizacionService(usuario.getBd());
                                    ArrayList listadoCotizaciones = clvsrv.cotizacionesContratista(contratista);
                                    session.setAttribute("listadoCotizaciones", listadoCotizaciones);
                                    session.setAttribute("id_accion_inicial", id_accion_inicial);

                                    this.next = "/jsp/opav/cotizacion/listadocotizacionesaux.jsp";
                                    this.dispatchRequest(this.next + "?op=0");
                            break;
                            case CONSULTAR_COTIZACION:
                                    this.consultarCotizacion();
                            break;
                            case AGREGAR_COTIZACION: //opcion 2
                                    this.agregarCotizacion();
                            break;
                            case COTIZACION_NUEVA://opcion 3
                                    this.cotizacionNueva();
                            break;
                            case LISTAR_CATEGORIA_TIPO://opcion 4
                                    this.consultaCategoriaTipo();
                            break;
                            case LISTAR_MATERIALES://opcion 5
                                    this.consultaMateriales();
                            break;
                            case CONSULTA_PRECIO://opcion 6
                                    this.consultaPrecio();
                            break;
                            case CARGAR_ITEMS ://opcion 7
                                    this.cargarItems();
                            break;
                            case AGREGAR_ITEM :
                                    this.agregarItem();
                            break;

                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }

    }



/**
 * Dado un id_accion y un index devuelve los detalles de una cotizacion
 * @throws Exception
 */
    private void consultarCotizacion() throws Exception{
        HttpSession session = request.getSession();
        CotizacionService clvsrv = new CotizacionService(usuario.getBd());
        int index = Integer.parseInt(request.getParameter("index").toString());
        String id_accion = request.getParameter("id_accion");

         ArrayList detalles = clvsrv.buscarDets(id_accion, "id_accion");
         session.setAttribute("detalles", detalles);
        response.setContentType("text/plain; charset=utf-8");
        response.setHeader("Cache-Control", "no-cache");

        this.dispatchRequest(next+"?op=1&index="+index+"&id_accion="+id_accion);
    }



/**
 * De Acuerdo a la operacion seleccionada en la pagina de plantilla de cotizacion
 * Agrega, reemplaza o Cancela: deja su estado actual, la informacion de una cotizacion
 * @throws Exception
 */
    private void agregarCotizacion() throws Exception {

        HttpSession session = request.getSession();
        CotizacionService clvsrv = new CotizacionService(usuario.getBd());
        int index = Integer.parseInt(request.getParameter("index").toString());
        String id_accion = request.getParameter("id_accion");
        String criterio = request.getParameter("criterio");
        String id_accion_inicial = (String) session.getAttribute("id_accion_inicial");

        String operacion = "guardar";

//buscarDetsPlantilla
        ArrayList cde = clvsrv.buscarDetsPlantilla(criterio, id_accion_inicial, id_accion);//detalles para agregar de la id_accion original
        session.setAttribute("detalles", cde);
        next = "/jsp/opav/cotizacion/listadoitemcotizacion.jsp?idaccion=" + id_accion_inicial + "&criterio=" + criterio + "&operacion=" + operacion + "&id_accion_plantilla=" + id_accion;
        response.setContentType("text/plain; charset=utf-8");
        response.setHeader("Cache-Control", "no-cache");
        response.getWriter().println(next);

    }



/**
 * Al crear, modificar: Carga la informacion de la cabecera de una cotizacion
 * @throws Exception
 */
    private void cotizacionNueva() throws Exception {
        try {
            HttpSession session = request.getSession();
            CotizacionService clvsrv = new CotizacionService(usuario.getBd());
            String idaccion = request.getParameter("idaccion") != null ? request.getParameter("idaccion") : "";
            String operacion = request.getParameter("operacion") != null ? request.getParameter("operacion") : "";
             String borrador = "false";

             if(clvsrv.buscarBorrador(idaccion)){
             borrador = "true";
             }
             session.setAttribute("borrador", borrador);

            String[] datitos = new String[2];
            datitos = clvsrv.datosAccion(idaccion).split(";");
            session.setAttribute("datos_cabecera", datitos);
            String fecha = "";

            ArrayList cde = clvsrv.buscarDets(idaccion, "id_accion");
            if (cde.size() > 0) {
                Cotizacion cdets = (Cotizacion) cde.get(0);
                fecha = cdets.getFecha().substring(0, 10);
                session.setAttribute("fecha", fecha);
            } else {
                fecha = "";
                session.setAttribute("fecha", fecha);
            }


            boolean existeCotizacion = clvsrv.existeCotizacion(idaccion);
            session.setAttribute("existeCotizacion", existeCotizacion);

            this.next = "/jsp/opav/cotizacion/listadoitemcotizacionaux.jsp";
            this.dispatchRequest(next + "?op=0&idaccion=" + idaccion + "&operacion=" + operacion);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


/**
 * Carga los items de una cotizacion dado el id_accion, en caso de ser nueva devuelve la lista vacia
 * @throws Exception
 */
    private void cargarItems() throws Exception {
        try {
            HttpSession session = request.getSession();
            CotizacionService clvsrv = new CotizacionService(usuario.getBd());
            String idaccion = request.getParameter("idaccion") != null ? request.getParameter("idaccion") : "";
            String operacion = request.getParameter("operacion") != null ? request.getParameter("operacion") : "";
            String criterio = request.getParameter("criterio") != null ? request.getParameter("criterio") : "";
            String id_accion_plantilla = request.getParameter("id_accion_plantilla") != null ? request.getParameter("id_accion_plantilla") : "";

            String borrador = "false";
            String fecha = "";

            ArrayList detallesCotizacion = new ArrayList();
            int tam = 0;
            Cotizacion cot = null;


            if (!operacion.equals("modificar")) {
                //Verifico si tiene borrador

//-----------
                if ((criterio.equals("")) || (criterio.equals("CANCELAR"))) {
                    if (clvsrv.buscarBorrador(idaccion)) {
                        borrador = "true";
                        //Si existe un borrador debo buscarlo y enviarlo
                        detallesCotizacion = clvsrv.buscarDetCotizacion(idaccion, "B");
                    } else {
                        if (detallesCotizacion.size() <= 0) {
                            tam = detallesCotizacion.size() + 1;
                            cot = new Cotizacion();
                            cot.setCodigo("");
                            cot.setIdcategoria(0);
                            cot.setMaterial("-1");
                            cot.setTipo_material("-1");
                            cot.setFecha("");
                            cot.setObservacion("");
                            cot.setAccion(idaccion);
                            cot.setCantidad(0);
                            cot.setId("0");

                            cot.setCompra("0");
                            cot.setCantidad_compra(0);
                            cot.setValor(0);
                            cot.setDescripcion_categoria("");
                            cot.setDescripcion_material("");
                            cot.setPrecioen_material(0);

                            detallesCotizacion.add(cot);

                        }
                    }
                } else {
                    borrador = "true";
                    detallesCotizacion = clvsrv.buscarDetsPlantilla(criterio, idaccion, id_accion_plantilla);
                }
//----------

            } else {
//Si la operacion es modificar

                ArrayList cde = clvsrv.buscarDets(idaccion, "id_accion");
                Cotizacion cdets = (Cotizacion) cde.get(0);
                fecha = cdets.getFecha().substring(0, 10);
                session.setAttribute("fecha", fecha);
                borrador = "false";
                //Si existe un borrador debo buscarlo y enviarlo
                detallesCotizacion = clvsrv.buscarDetCotizacion(idaccion, "P");

//
            }

            boolean existeCotizacion = clvsrv.existeCotizacion(idaccion);

            session.setAttribute("borrador", borrador);
            session.setAttribute("existeCotizacion", existeCotizacion);
            session.setAttribute("detallesCotizacion", detallesCotizacion);
            //

            response.setContentType("text/plain; charset=utf-8");
            response.setHeader("Cache-Control", "no-cache");
            this.next = "/jsp/opav/cotizacion/listadoitemcotizacionaux.jsp";
            this.dispatchRequest(next + "?op=1&idaccion=" + idaccion + "&operacion=" + operacion);


        } catch (Exception e) {
            e.printStackTrace();
        }



    }


/**
 * Consulta las Categorias dada un tipo de material
 * @throws Exception
 */
  private void consultaCategoriaTipo() throws Exception {
        String xml = "";
        //validamos para que consultar los materiales simpre con M.
        String tipo = request.getParameter("tipo") != null ? (request.getParameter("tipo").equals("MO") ? "M" :request.getParameter("tipo")) : "";
        CategoriaService clvsrv = new CategoriaService(usuario.getBd());
        clvsrv.listadoCategoriasTipo(tipo);
        Map categorias = clvsrv.getListadoCategoriasTipo();

        xml = "<?xml version=\"1.0\" encoding=\"iso-8859-1\"?>";
        xml += "<mensaje>";
        xml += this.getXMLCategorias(categorias);
        xml += "</mensaje>";

        response.setContentType("text/xml");
        response.setHeader("Cache-Control", "no-cache");
        response.getWriter().println(xml);
    }


   private String getXMLCategorias(Map map)throws Exception{
        String xml = "";
        Iterator categorias = map.keySet().iterator();
        while (categorias.hasNext()){
            String cat = categorias.next().toString();
            xml += "<categoria codcat=\""+map.get(cat).toString()+"\">"+cat+"</categoria>";
        }
        return xml;
    }


/**
 * Devuelve los materiales dados el tipo de material, el id_categoria, id_subcategoria, id_tiposubcategoria
 * @throws Exception
 */
  /**
 * Devuelve los materiales dados el tipo de material, el id_categoria, id_subcategoria, id_tiposubcategoria
 * @throws Exception
 */
  private void consultaMateriales() throws Exception {
        String xml = "";
        String tipo = request.getParameter("tipo") != null ? (request.getParameter("tipo").equals("MO")?"M":request.getParameter("tipo")) : "";

        int idcat = 0;
        int idsubcat = 0;
        int idtiposubcat = 0;

        //if(!tipo.equals("D")){
        if(tipo.equals("M")){
        idcat = Integer.parseInt(request.getParameter("idcat").toString());
        idsubcat = Integer.parseInt(request.getParameter("idsubcat").toString());
        idtiposubcat = Integer.parseInt(request.getParameter("idtiposubcat").toString());
        }

        CategoriaService clvsrv = new CategoriaService(usuario.getBd());
        clvsrv.listadoMateriales(tipo, idcat, idsubcat, idtiposubcat);
        Map materiales = clvsrv.getlistadoMateriales();

        xml = "<?xml version=\"1.0\" encoding=\"iso-8859-1\"?>";
        xml += "<mensaje>";
        xml += this.getXMLMateriales(materiales);
        xml += "</mensaje>";

        response.setContentType("text/xml");
        response.setHeader("Cache-Control", "no-cache");
        response.getWriter().println(xml);
    }


   private String getXMLMateriales(Map map)throws Exception{
        String xml = "";
        Iterator materiales = map.keySet().iterator();
        while (materiales.hasNext()){
            String cat = materiales.next().toString();
            xml += "<material codcat=\""+map.get(cat).toString()+"\">"+cat+"</material>";
        }
        return xml;
    }


/**
 * Consulta el precio de un material dado su id
 * @throws Exception
 */
       private void consultaPrecio() throws Exception{
        MaterialesService mser = new MaterialesService(usuario.getBd());
        String idmat = (request.getParameter("idmat").toString());
        String tipo = request.getParameter("tipo") != null ? request.getParameter("tipo"): "";
        
           //aqui vamos hacer la mara�a para que el material si es oficial ponerle el 27 y quitarle el 17
           double resultado = 0;
           String result="";
           if (tipo.equals("MO")) {
              
               String precio = mser.buscarPrecioMaterial(idmat);
               double aux = Double.parseDouble(precio);
               resultado = (aux / 1.17) * 1.27;
               resultado=Math.round(resultado);
               result=""+resultado;                      
               
           } else {
               result= mser.buscarPrecioMaterial(idmat);
              // resultado = Double.parseDouble(precio);
           }

        String xml = "";

        xml = "<?xml version=\"1.0\" encoding=\"iso-8859-1\"?>";
        xml += "<mensaje>";
        xml += "<respuesta>"+result+"</respuesta>";
        xml += "</mensaje>";

        response.setContentType("text/xml");
        response.setHeader("Cache-Control", "no-cache");
        response.getWriter().println(xml);
    }





/**
 * Almacena la informacion de una cotizacion, o actualiza
 * @throws Exception
 */
 private void agregarItem() throws Exception{
        try {
            HttpSession session = request.getSession();
            Usuario usuario = (Usuario) session.getAttribute("Usuario");
            String loginx = usuario.getLogin();
            CotizacionService clvsrv = new CotizacionService(usuario.getBd());
            String idaccion = request.getParameter("multiservicios") != null ? request.getParameter("multiservicios") : "";
	    String id_solic=request.getParameter("solic") != null ? request.getParameter("solic") : "";
            String operacion = request.getParameter("operacion") != null ? request.getParameter("operacion") : "";
            String ms = request.getParameter("multiservicios") != null ? request.getParameter("multiservicios") : "";
            String fecha = request.getParameter("fecha") != null ? ((request.getParameter("fecha")).equals("")? "now()": request.getParameter("fecha") ):"now()";
            ArrayList detallesCotizacion = new ArrayList();
            int tam = 0;
            Cotizacion cot = null;
            request.setAttribute("msg", "");
            ArrayList listado = new ArrayList();
            MaterialesService prs = new MaterialesService();
            ArrayList ars = null;
            Material mat = null;
            String borrador = "false";

            //
            int tam_items = (request.getParameter("tam_items") != null) ? Integer.parseInt(request.getParameter("tam_items").toString()) : -1;

            String tipo          = "";
            String categoria  = "";
            String material    = "";
            double cantidad = 0;
            double precio = 0;
            String nota = "";
            String compra = "";
	    String idcotdet="";
            double cantidad_compra = 0;


            for (int i = 0; i < tam_items; i++) {

                tipo = request.getParameter("tipo" + i) != null ? request.getParameter("tipo" + i) : "";
		idcotdet = request.getParameter("idcotdet" + i) != null ? request.getParameter("idcotdet" + i) : "";
                categoria = request.getParameter("categoria" + i) != null ? request.getParameter("categoria" + i) : "";
                material = request.getParameter("material" + i) != null ? request.getParameter("material" + i) : "";

                if (!material.equals("") && !material.equals("-1")) {

                //cantidad = (request.getParameter("cantidad" + i) != null) ? Double.parseDouble(request.getParameter("cantidad" + i)) : 0;
                cantidad = (request.getParameter("cantidad" + i) != null) ? Double.parseDouble(request.getParameter("cantidad" + i).replaceAll(",","")) : 0;//

                precio = (request.getParameter("precio" + i) != null) ? Double.parseDouble(request.getParameter("precio" + i).replaceAll(",", "")) : 0;
                nota = request.getParameter("nota" + i) != null ? request.getParameter("nota" + i) : "";
                compra = request.getParameter("compra" + i) != null ? request.getParameter("compra" + i) : "N";
                //cantidad_compra = (request.getParameter("cantidad_compra" + i) != null) ? Double.parseDouble(request.getParameter("cantidad_compra" + i).toString()) : 0;

                //cantidad_compra = (request.getParameter("cantidad_compra" + i) != null) ? (request.getParameter("cantidad_compra" + i).equals("")==false?Double.parseDouble(request.getParameter("cantidad_compra" + i).toString()):0) : 0;
                cantidad_compra = (request.getParameter("cantidad_compra" + i) != null) ? (request.getParameter("cantidad_compra" + i).equals("")==false?Double.parseDouble(request.getParameter("cantidad_compra" + i).toString().replaceAll(",","")):0) : 0;


                        cot = new Cotizacion();
			cot.setId(idcotdet);
                        cot.setAccion(ms);
                        cot.setCantidad(cantidad);
                        cot.setCodigo("");
                        cot.setFecha(fecha);
                        cot.setMaterial(material);//codigo material
                        cot.setObservacion(nota);
                        cot.setCompra(compra);
                        cot.setCantidad_compra(cantidad_compra);
                        cot.setValor(precio);
                        //esto es para saber si es una cotizacion oficial
                        cot.setOficial(tipo.equals("MO")?"S":"");
                        listado.add(cot);

              }

            }



            if (!operacion.equals("")) {
//Opcion guardar
                if (operacion.equals("guardar")) {
                    if (listado.size() > 0) {
                        //clvsrv.borrarDetalles(ms);
                        clvsrv.guardarBorrador(listado, fecha, loginx);
                    } else {
                        System.out.println("No hay datos para pasar a borrador");
                    }
                }
//Opcion crear
                if (operacion.equals("crear")) {
                    if (listado.size() > 0) {
                        //clvsrv.borrarDetalles(ms);
                        clvsrv.insertDetsPlusOrden(listado, fecha, loginx);
                    } else {
                        System.out.println("No hay datos para insertar");
                    }
                }
//Opcion Modificar
		if (operacion.equals("modificar")) {
                    if (listado.size() > 0) {
                        clvsrv.updateDetsPlusOrden(listado, fecha, loginx,ms);
                    } else {
                        System.out.println("No hay datos para Modificar");
                    }
                }
            }


            /////////////

//Se carga la informacion una vez almacenada
   if ((!operacion.equals("modificar")) && (!operacion.equals("crear"))) {
                if (clvsrv.buscarBorrador(idaccion)) {
                    ArrayList cde = clvsrv.buscarDets(idaccion, "id_accion");
                    if (cde.size() > 0) {
                        Cotizacion cdets = (Cotizacion) cde.get(0);
                        fecha = cdets.getFecha().substring(0, 10);
                        session.setAttribute("fecha", fecha);
                        borrador = "true";
                        //Si existe un borrador debo buscarlo y enviarlo
                        detallesCotizacion = clvsrv.buscarDetCotizacion(idaccion, "B");
                    }
                }
            } else {
                ArrayList cde = clvsrv.buscarDets(idaccion, "id_accion");
                if (cde.size() > 0) {
                    Cotizacion cdets = (Cotizacion) cde.get(0);
                    fecha = cdets.getFecha().substring(0, 10);
                    session.setAttribute("fecha", fecha);
                    borrador = "true";
                    //Si existe un borrador debo buscarlo y enviarlo
                    detallesCotizacion = clvsrv.buscarDetCotizacion(idaccion, "P");
                }
            }

            /////////////


            boolean existeCotizacion = clvsrv.existeCotizacion(idaccion);

            session.setAttribute("existeCotizacion", existeCotizacion);
            session.setAttribute("detallesCotizacion", detallesCotizacion);
//

            this.next = "/jsp/opav/cotizacion/listadoitemcotizacion.jsp?idaccion="+idaccion+"&operacion="+operacion;
	     if(listado.size()>0){
                if(operacion.equals("crear")){
                    this.next = "?estado=Clientes&accion=Ver&opcion=buscarsolicitud&idsolicitud="+id_solic;
                }
            }
            response.getWriter().println(next+"-"+listado.size());
        } catch (Exception e) {
            e.printStackTrace();
        }
 }










}