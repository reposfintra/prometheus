/**
 * Autor  : Ing. Roberto Rocha P..
 * Date  : 10 de Julio de 2007
 * Copyrigth Notice : Fintravalores S.A. S.A
 * Version 1.0
-->
<%--
-@(#)
--Descripcion : Action que maneja los avales de Fenalco
 **/
package com.tsp.opav.controller;


import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.Paragraph;
import com.tsp.opav.model.beans.*;
import com.tsp.opav.model.services.*;
import com.tsp.operation.model.CiudadService;
import com.tsp.operation.model.beans.Cliente;
import com.tsp.operation.model.beans.Usuario;
import javax.servlet.http.*;
import com.tsp.operation.model.threads.*;
import com.tsp.util.Util;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;//RESPONSABLE JCASTRO
import java.util.ResourceBundle;

public class ClientesVerAction extends Action {

    Usuario usuario;
    public ClientesVerAction() {
    }

    public void run() {
        String next = "/jsp/opav/datos_electricaribe.jsp";
        HttpSession session = request.getSession();
        usuario = (Usuario) session.getAttribute("Usuario");
        String op = request.getParameter("opcion");
        String fapro = "";//jjcastro
        ClientesVerService clsrv = new ClientesVerService(usuario.getBd());
        InterventorService iserv = new InterventorService(usuario.getBd());
        boolean redirect = true;
         String respuesta="";
        //------------------------RESPONSABLE JCASTRO
        try {
            ArrayList listadoResponsables = clsrv.listadoResponsables();
            session.setAttribute("listadoResponsables", listadoResponsables);
            ArrayList listadoInterventores = iserv.listadoInterventores();
            session.setAttribute("listadoInterventores", listadoInterventores);
        } catch (Exception e) {
            e.printStackTrace();
        }
        //-------------------------
        if (op.equals("creacliente") || op.equals("modcliente")) {
            Cliente cl=new Cliente();
            String situacion=(request.getParameter("detalle_inconsistencia")!=null)?request.getParameter("detalle_inconsistencia"):"";
            cl.setCodcli(request.getParameter("cliente"));
            cl.setCiudad((request.getParameter("ciudad")!=null && !(request.getParameter("ciudad").equals("")))?request.getParameter("ciudad"):request.getParameter("ciudadd"));
            cl.setNomContacto(request.getParameter("nombre_contacto"));
            cl.setCelRepresentante(request.getParameter("celular_representante"));
            cl.setSector((request.getParameter("zona")!=null && !(request.getParameter("zona").equals("")))?request.getParameter("zona"):request.getParameter("zonaa"));
            cl.setDireccion(request.getParameter("direccion"));
            cl.setNit(request.getParameter("nit"));
            cl.setDigito_verificacion((request.getParameter("digito_verificacion"))!= null ? request.getParameter("digito_verificacion"):"");
            cl.setEmail((request.getParameter("email_contacto"))!= null ? request.getParameter("email_contacto"):"");
            cl.setCargoContacto(request.getParameter("cargo_contacto"));
            cl.setNomcli(request.getParameter("nombre"));
            cl.setNomRepresentante(request.getParameter("nombre_representante"));
            //cl.setSector((request.getParameter("sector")!=null && !(request.getParameter("sector").equals("")))?request.getParameter("sector"):request.getParameter("sectorr"));
            cl.setTelContacto(request.getParameter("telefono_contacto"));
            cl.setCelContacto(request.getParameter("celular_contacto"));
            cl.setTelRepresentante(request.getParameter("telefono_representante"));
            cl.setTipo((request.getParameter("tipo_identificacion")!=null &&  !(request.getParameter("tipo_identificacion").equals("")))?request.getParameter("tipo_identificacion"):request.getParameter("tipo_identificacionn"));
            cl.setIdEjecutivo((request.getParameter("ejecutivo_cta")!=null &&  !(request.getParameter("ejecutivo_cta").equals("")))?(request.getParameter("ejecutivo_cta").split(","))[0]:request.getParameter("ejecutivo_ctaa"));

            boolean sw=true;
            if(request.getParameter("ejecutivo_cta").equals("")){
                sw=false;
            }

            cl.setNics(request.getParameterValues("nicc"));

            /* Este codigo aca es para poner la parte
             * de la validacion para los padres de
             * los clientes
             */
            if(request.getParameter("padre").equals("")){
                cl.setId_padre(cl.getCodcli());
            }
            else
            {
                cl.setId_padre(request.getParameter("padre"));
            }

            /* Esta parte del codigo aca es para ver
             * el cliente es un cliente oficial
             */
            if(request.getParameter("cliespec").equals("true")){
                cl.setOficial(true);
            }
            else{
                cl.setOficial(false);
            }
            cl.setEdif((request.getParameter("edificio")!=null)?"S":"N");


            if(op.equals("creacliente")){
                try{


                    String resp=clsrv.insertarCl(cl,usuario.getLogin());
                    clsrv.buscarCl(cl);
                    next=next+"?mensaje="+resp+"&ciudadd="+cl.getCiudad()+"&nombre_contacto="+cl.getNomContacto()
                            +"&celular_representante="+cl.getCelRepresentante()+"&zonaa="+cl.getCiudad()
                            +"&direccion="+cl.getDireccion()
                            +"&nit="+cl.getNit()+"&cargo_contacto="+cl.getCargoContacto()
                            +"&nombre="+cl.getNomcli()+"&nombre_representante="+cl.getNomRepresentante()
                            +"&sectorr="+cl.getSector()+"&telefono_contacto="+cl.getTelContacto()+"&celular_contacto="+cl.getCelContacto()+"&creation_user="+cl.getCreationUser()
                            +"&telefono_representante="+cl.getTelRepresentante()+"&tipo_identificacionn="+cl.getTipo()
                            +"&ejecutivo_ctaa="+cl.getIdEjecutivo()+"&detalle_inconsistenciaa="+situacion+"&login_ejecutivo="+clsrv.getLoginEjecutivo(cl.getCodcli())
                            +"&padre="+ cl.getId_padre()
                            +"&edificio="+ cl.getEdif()
                            +"&cliespec="+cl.isOficial()
                            +"&digito_verif="+cl.getDigito_verificacion();
                    if(!cl.getCodcli().equals("")){
                        next=next+"&idusuario="+cl.getCodcli();
                    }
                    session.setAttribute("nics", cl.getNics());
                    System.out.println(next);
                }
                catch(Exception e)
                {
                    e.printStackTrace();
                    System.out.println("Error:"+e.toString());
               }
            }

            if (op.equals("modcliente")) {
                OfertaElca ofeca = new OfertaElca();
                ofeca.setId_solicitud(request.getParameter("idsolicitud"));
                AccionesEca acceca = new AccionesEca();
                try {
                    String resp = clsrv.updateCl(cl, usuario.getLogin(), sw);
                    clsrv.buscarCl(cl);
                    if (ofeca.getId_solicitud() != null && !ofeca.getId_solicitud().equals("")) {
                        clsrv.buscarOf(ofeca);
                        acceca.setId_solicitud(ofeca.getId_solicitud());
                        fapro = ofeca.getFec_val_cartera().equals("0099-01-01 00:00:00") ? "Sin Fecha" : ofeca.getFec_val_cartera();//JJCASTRO
                    }
                    next = next + "?mensaje=" + resp + "&ciudadd=" + cl.getCiudad() + "&nombre_contacto=" + cl.getNomContacto() + "&celular_representante=" + cl.getCelRepresentante() + "&zonaa=" + cl.getSector() + "&direccion=" + cl.getDireccion() + "&nit=" + cl.getNit() + "&cargo_contacto=" + cl.getCargoContacto() + "&nombre=" + cl.getNomcli() + "&nombre_representante=" + cl.getNomRepresentante() + "&sectorr=" + cl.getSector() + "&telefono_contacto=" + 
                            cl.getTelContacto() + "&celular_contacto=" + cl.getCelContacto() + "&email_contacto=" + cl.getEmail()+ "&telefono_representante=" + cl.getTelRepresentante() + "&tipo_identificacionn=" +
                            cl.getTipo() + "&creation_user=" + cl.getCreationUser() + "&ejecutivo_ctaa=" + cl.getIdEjecutivo() + "&detalle_inconsistenciaa=" + situacion +
                            "&login_ejecutivo=" + clsrv.getLoginEjecutivo(cl.getCodcli()) + "&padre="
                            + ((cl.getId_padre().equals(cl.getCodcli())) ? "" : cl.getId_padre())
                            +"&edificio="+ cl.getEdif()
                            + "&cliespec=" + cl.isOficial()+"&digito_verif=" + cl.getDigito_verificacion();
                    if (!cl.getCodcli().equals("")) {
                        next = next + "&idusuario=" + cl.getCodcli();
                    }
                    if (ofeca.getId_solicitud() != null && !ofeca.getId_solicitud().equals("")) {
                        next = next + "&idsolicitud=" + ofeca.getId_solicitud() + "&historico=" + ofeca.getDescripcion() + "&fechainicio=" + ofeca.getCreation_date().substring(0, 11) + "&nicsof=" + ofeca.getNic() + "&sol_creation_user=" + ofeca.getCreation_user() + "&tipo_solicitudd=" + ofeca.getTipo_solicitud() + "&estado_cartera=" + ofeca.getEstado_cartera() //JJCASTRO
                                + "&fec_val_cartera2=" + fapro + "&responsable=" + ofeca.getResponsable()  ;//responsable opav JJCASTRO//JJCASTRO
                    }
                    session.setAttribute("nics", cl.getNics());
                    session.setAttribute("acciones", clsrv.buscarAcc(acceca, usuario.getNitPropietario()));
                    System.out.println(next);
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("Error:" + e.toString());
                }
            }

        } else {
            if (op.equals("buscarcliente")) {
                try {//linea movida en 090929

                    String nicc = request.getParameter("nicc");//090929
                    String idclie = request.getParameter("idclie");//090929
                    if (idclie == null || idclie.equals("")) {//090929
                        idclie = model.NegociosApplusService.getIdClie(nicc);//090929
                    }//090929

                    Cliente cl = new Cliente();
                    cl.setCodcli(request.getParameter("clientee"));//090929
                    cl.setCodcli(idclie);//090929

                    next = next + "?mensaje=" + clsrv.buscarCl(cl) + "&ciudadd=" + cl.getCiudad() + "&nombre_contacto=" + cl.getNomContacto() +
                            "&celular_representante=" + cl.getCelRepresentante() + "&zonaa=" + cl.getSector() + "&direccion=" + cl.getDireccion() +
                            "&nit=" + cl.getNit() + "&cargo_contacto=" + cl.getCargoContacto() + "&nombre=" + cl.getNomcli() + "&nombre_representante=" +
                            cl.getNomRepresentante() + "&sectorr=" + cl.getSector() + "&telefono_contacto=" + cl.getTelContacto()
                            + "&celular_contacto=" + cl.getCelContacto() + "&email_contacto=" + cl.getEmail() + "&telefono_representante=" + cl.getTelRepresentante() + "&tipo_identificacionn=" + 
                            cl.getTipo() + "&creation_user=" + cl.getCreationUser() + "&ejecutivo_ctaa=" + cl.getIdEjecutivo() + "&login_ejecutivo=" +
                            clsrv.getLoginEjecutivo(cl.getCodcli()) +
                            "&padre=" + cl.getId_padre() 
                             +"&edificio="+ cl.getEdif()
                            + "&cliespec=" + cl.isOficial()
                             +"&digito_verif="+cl.getDigito_verificacion();
                    if (!cl.getCodcli().equals("")) {
                        next = next + "&idusuario=" + cl.getCodcli();
                    }
                    session.setAttribute("nics", cl.getNics());
                    System.out.println(next);
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("Error:" + e.toString());
                }
            }

        }
        if (op.equals("creasolicitud")) {
            OfertaElca ofeca = new OfertaElca();
             Cliente cl = new Cliente();
            ofeca.setId_cliente(request.getParameter("cliente"));
            ofeca.setNic(request.getParameter("nicsoff"));
            ofeca.setDescripcion(request.getParameter("detalle_inconsistencia"));
            ofeca.setTipo_solicitud(request.getParameter("tipo_solicitud"));
            ofeca.setOficial("0");
             ofeca.setAviso(request.getParameter("inpAviso")!=null?request.getParameter("inpAviso"):"");//HAROLD 1
            String opd = request.getParameter("opd")!=null?request.getParameter("opd"):"";//<!-- 2010-06-12 rhonalf -->

            String fecha_actual = "";//jjcastro Emergencia
            String estado_cartera = "";//jjcastro Emergencia
            //RESPONSABLE OPAV JJCASTRO
            String responsable = request.getParameter("responsable")!=null?request.getParameter("responsable"):"";
            String interventor = request.getParameter("interventor")!=null?request.getParameter("interventor"):"";
            ofeca.setResponsable(responsable);
             ofeca.setInterventor(interventor);
            ofeca.setTipoDtf("1");
            //RESPONSABLE OPAV JJCASTRO
            /*if (request.getParameter("tipo_solicitud").equals("Emergencia")){
             //JJCASTRO EMERGENCIA
                estado_cartera = "010";
                ofeca.setEstado_cartera(estado_cartera);//JJCASTRO
                fecha_actual = request.getParameter("fec_val_cartera");
                if (estado_cartera.equals("010") || estado_cartera.equals("000")) {
                    if (fecha_actual.equals("Sin Fecha")) {
                        fecha_actual = Util.getFechaActual_String(9);//jjcastro
                    }
                }
                fecha_actual = fecha_actual.equals("Sin Fecha") ? "0099-01-01 00:00:00" : fecha_actual;//jjcastro
                ofeca.setFec_val_cartera(fecha_actual);//JJCASTRO
            //JJCASTRO EMERGENCIA
                opd = "";
            }
            else{*/
                ofeca.setEstado_cartera("Estudio");//JJCASTRO EMERGENCIA
                ofeca.setFec_val_cartera("0099-01-01 00:00:00");//JJCASTRO
                fecha_actual = "Sin Fecha";//JJCASTRO EMERGENCIA
                //ofeca.setAviso(opd);
                if (request.getParameter("tipo_solicitud").equals("Emergencia"))
                {//20101111
                        ofeca.setEstado_cartera("010");
                        opd = "";//ojo 20101111
                }
                else
                {

                       ofeca.setAviso(opd);//ojo 20101111


                 }

                 ofeca.setTipoDtf(clsrv.getDistribucion(request.getParameter("tipo_solicitud")));

            cl.setCodcli(ofeca.getId_cliente());
            String resp = "";
            try {
                clsrv.buscarCl(cl);
                resp = clsrv.insertarOf(ofeca, usuario.getLogin());
                clsrv.buscarOf(ofeca);
                fapro =  ofeca.getFec_val_cartera().equals("0099-01-01 00:00:00")?"Sin Fecha":ofeca.getFec_val_cartera();//JJCASTRO
                next = next + "?mensaje=" + resp + "&ciudadd=" + cl.getCiudad() + "&nombre_contacto=" + cl.getNomContacto() + "&celular_representante=" + cl.getCelRepresentante() + "&zonaa=" + cl.getSector() + "&direccion=" + cl.getDireccion() +
                        "&nit=" + cl.getNit() + "&cargo_contacto=" + cl.getCargoContacto() + "&nombre=" + cl.getNomcli() + "&nombre_representante=" + cl.getNomRepresentante() + "&sectorr=" + cl.getSector() + "&telefono_contacto="
                        + cl.getTelContacto() + "&celular_contacto=" + cl.getCelContacto() + "&email_contacto=" + cl.getEmail()+ "&telefono_representante=" + cl.getTelRepresentante() +
                        "&tipo_identificacionn=" + cl.getTipo() + "&creation_user=" + cl.getCreationUser() + "&ejecutivo_ctaa=" + cl.getIdEjecutivo() + "&historico=" +
                        ofeca.getDescripcion() + "&login_ejecutivo=" + clsrv.getLoginEjecutivo(cl.getCodcli()) + "&fechainicio=" + ofeca.getCreation_date().substring(0, 11) +
                        "&nicsof=" + ofeca.getNic() + "&sol_creation_user=" + ofeca.getCreation_user() + "&tipo_solicitudd=" + ofeca.getTipo_solicitud() + "&tasa_trans=" +
                        ofeca.getOficial() + "&estado_cartera=" + ofeca.getEstado_cartera() //JJCASTRO
                        + "&fec_val_cartera2=" + fapro     + "&responsable=" + ofeca.getResponsable() + "&interventor=" + ofeca.getInterventor()+
                            "&padre=" + cl.getId_padre() 
                             +"&edificio="+ cl.getEdif()
                            + "&cliespec=" + cl.isOficial()
                            +"&digito_verif="+cl.getDigito_verificacion();  //responsable opav JJCASTRO


                if (!cl.getCodcli().equals("")) {
                    next = next + "&idusuario=" + cl.getCodcli();
                }
                if (!ofeca.getId_solicitud().equals("")) {
                    next = next + "&idsolicitud=" + ofeca.getId_solicitud();
                }
                session.setAttribute("nics", cl.getNics());

                /***************************Envio de Correo***************************/
                HSendMail2 hSendMail2 = new HSendMail2();
                hSendMail2.start("jpinedo@fintravalores.com",
                        clsrv.getMails("3", request.getParameter("idsolicitud"), ""),
                        "", "", "Nueva Solicitud " + ofeca.getId_solicitud(),
                        "Se ha creado una solicitud nueva identificada con el codigo " + ofeca.getId_solicitud() + ", asociada al cliente " + cl.getNomcli(), usuario.getLogin(),"","");
                /************************Fin Envio de Correo**************************/
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("Error:" + e.toString());
            }

        } else {
            AccionesEca acceca = new AccionesEca();
            if (op.equals("modsolicitud"))
            {
                OfertaElca ofeca = new OfertaElca();
                Cliente cl = new Cliente();
                String estado_cartera = !request.getParameter("est_mar").equals("") ? request.getParameter("est_mar") : request.getParameter("estado_cartera");
                String tipo_solicitud = (request.getParameter("tipo_solicitud").equals(""))?request.getParameter("solicitud_asignada"):request.getParameter("tipo_solicitud");
                ofeca.setEstado_cartera(estado_cartera);//JJCASTRO

                String fecha_actual = request.getParameter("fec_val_cartera");
                if (estado_cartera.equals("010") || estado_cartera.equals("000")) {

                    if (fecha_actual.equals("Sin Fecha")) {
                        fecha_actual = Util.getFechaActual_String(9);//jjcastro
                    }

                }
                fecha_actual = fecha_actual.equals("Sin Fecha") ? "0099-01-01 00:00:00" : fecha_actual;//jjcastro
                ofeca.setFec_val_cartera(fecha_actual);//JJCASTRO
                ofeca.setId_solicitud(request.getParameter("idsolicitud"));
                ofeca.setDescripcion(" (" + usuario.getLogin() + "): " + request.getParameter("detalle_inconsistencia"));
                ofeca.setTipo_solicitud(tipo_solicitud);
                ofeca.setOficial("0");

                ofeca.setAviso(request.getParameter("inpAviso")!=null?request.getParameter("inpAviso"):"");//HAROLD 1
//-------------------- RESPONSABLE OPAV
                if (!request.getParameter("tipo_solicitud").equals("Emergencia")) {
                     ofeca.setAviso(request.getParameter("opd")!=null?request.getParameter("opd"):"");//HAROLD1
                }
                
                ofeca.setTipoDtf(clsrv.getDistribucion(tipo_solicitud));
//-------------------- RESPONSABLE OPAV








                //RESPONSABLE OPAV JJCASTRO


            String responsable = request.getParameter("responsable")!=null?request.getParameter("responsable"):"";
            ofeca.setResponsable(responsable);

            String interventor = request.getParameter("interventor")!=null?request.getParameter("interventor"):"";
            ofeca.setInterventor(interventor);

            //RESPONSABLE OPAV JJCASTRO


                 //ofeca.setAviso(request.getParameter("inpAviso")!=null?request.getParameter("inpAviso"):"");//HAROLD 1
                cl.setCodcli(request.getParameter("cliente"));
                acceca.setId_solicitud(ofeca.getId_solicitud());
                String resp = "";
                try {
                    clsrv.buscarCl(cl);
                    resp = clsrv.updateOf(ofeca, usuario.getLogin());
                    clsrv.buscarOf(ofeca);
                    fapro = ofeca.getFec_val_cartera().equals("0099-01-01 00:00:00") ? "Sin Fecha" : ofeca.getFec_val_cartera();//JJCASTRO

                    next = next + "?mensaje=" + resp + "&ciudadd=" + cl.getCiudad() + "&nombre_contacto=" + cl.getNomContacto() + "&celular_representante=" +
                    cl.getCelRepresentante() + "&zonaa=" + cl.getSector() + "&direccion=" + cl.getDireccion() + "&nit=" + cl.getNit() +
                            "&cargo_contacto=" + cl.getCargoContacto() + "&nombre=" + cl.getNomcli() + "&nombre_representante=" +
                            cl.getNomRepresentante() + "&sectorr=" + cl.getSector() + "&telefono_contacto=" + cl.getTelContacto() + "&celular_contacto=" +
                            cl.getCelContacto() + "&email_contacto=" + cl.getEmail()+ "&login_ejecutivo=" + clsrv.getLoginEjecutivo(cl.getCodcli()) + "&telefono_representante=" +
                            cl.getTelRepresentante() + "&tipo_identificacionn=" + cl.getTipo() + "&creation_user=" + cl.getCreationUser() +
                            "&ejecutivo_ctaa=" + cl.getIdEjecutivo() + "&historico=" + ofeca.getDescripcion() + "&sol_creation_user=" +
                            ofeca.getCreation_user() + "&fechainicio=" + ofeca.getCreation_date().substring(0, 11) + "&nicsof=" + ofeca.getNic() +
                            "&tipo_solicitudd=" + ofeca.getTipo_solicitud() + "&tasa_trans=" + ofeca.getOficial() + "&estado_cartera=" +
                            ofeca.getEstado_cartera() //JJCASTRO
                            + "&fec_val_cartera2=" + fapro + "&responsable=" + ofeca.getResponsable()+  "&historico=" + ofeca.getDescripcion() + "&sol_creation_user=" +
                            ofeca.getCreation_user() + "&fechainicio=" + ofeca.getCreation_date().substring(0, 11) + "&nicsof=" + ofeca.getNic() +
                            "&tipo_solicitudd=" + ofeca.getTipo_solicitud() + "&tasa_trans=" + ofeca.getOficial() + "&estado_cartera=" +
                            ofeca.getEstado_cartera() //JJCASTRO
                            + "&fec_val_cartera2=" + fapro + "&responsable=" + ofeca.getResponsable() + "&interventor=" + ofeca.getInterventor()
                            + "&padre=" + cl.getId_padre()
                             +"&edificio="+ cl.getEdif()
                            + "&cliespec=" + cl.isOficial()
                            +"&digito_verif="+cl.getDigito_verificacion();

                    if (!cl.getCodcli().equals("")) {
                        next = next + "&idusuario=" + cl.getCodcli();
                    }
                    if (!ofeca.getId_solicitud().equals("")) {
                        next = next + "&idsolicitud=" + ofeca.getId_solicitud();
                    }
                    session.setAttribute("nics", cl.getNics());
                    // session.setAttribute("id_cliente", cl.getCodcli());
                    session.setAttribute("acciones", clsrv.buscarAcc(acceca, usuario.getNitPropietario()));

                    /***************************Envio de Correo***************************/
                    HSendMail2 hSendMail2 = new HSendMail2();
                    hSendMail2.start("jpinedo@fintravalores.com",
                            clsrv.getMails("3", request.getParameter("idsolicitud"), ""),
                            "", "", "Nueva Solicitud " + ofeca.getId_solicitud(),
                            "Se ha modificado la solicitud identificada con el codigo " + ofeca.getId_solicitud() + ", asociada al cliente " + 
                            cl.getNomcli(), usuario.getLogin(),"","");
                    /*************************Fin Envio de Correo*************************/
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("Error:" + e.toString());
                }
            } else {
                if (op.equals("buscarsolicitud")) {
                    OfertaElca ofeca = new OfertaElca();
                    Cliente cl = new Cliente();
                    ofeca.setId_solicitud(request.getParameter("idsolicitud"));
                    String resp = "";
                    try {
                        resp = clsrv.buscarOf(ofeca);
                        acceca.setId_solicitud(ofeca.getId_solicitud());
                        cl.setCodcli(ofeca.getId_cliente());
                        clsrv.buscarCl(cl);
                        fapro = ofeca.getFec_val_cartera().equals("0099-01-01 00:00:00") ? "Sin Fecha" : ofeca.getFec_val_cartera();//JJCASTRO

                        next = next + "?mensaje=" + resp + "&ciudadd=" + cl.getCiudad() + "&nombre_contacto=" + cl.getNomContacto() + "&celular_representante=" + cl.getCelRepresentante() + "&zonaa=" + cl.getSector() + "&direccion=" + cl.getDireccion() +
                        "&nit=" + cl.getNit() + "&cargo_contacto=" + cl.getCargoContacto() + "&nombre=" + cl.getNomcli() + "&nombre_representante=" + cl.getNomRepresentante() + "&sectorr=" + cl.getSector() + "&telefono_contacto="
                        + cl.getTelContacto() + "&celular_contacto="+ cl.getCelContacto() + "&email_contacto=" + cl.getEmail() + "&telefono_representante=" + cl.getTelRepresentante() +
                        "&tipo_identificacionn=" + cl.getTipo() + "&creation_user=" + cl.getCreationUser() + "&ejecutivo_ctaa=" + cl.getIdEjecutivo() + "&historico=" +
                        ofeca.getDescripcion() + "&login_ejecutivo=" + clsrv.getLoginEjecutivo(cl.getCodcli()) + "&fechainicio=" + ofeca.getCreation_date().substring(0, 11) +
                        "&nicsof=" + ofeca.getNic() + "&sol_creation_user=" + ofeca.getCreation_user() + "&tipo_solicitudd=" + ofeca.getTipo_solicitud() + "&tasa_trans=" +
                        ofeca.getOficial() + "&estado_cartera=" + ofeca.getEstado_cartera() //JJCASTRO
                        + "&fec_val_cartera2=" + fapro     + "&responsable=" + ofeca.getResponsable() +"&historico=" + ofeca.getDescripcion() + "&sol_creation_user=" +
                            ofeca.getCreation_user() + "&fechainicio=" + ofeca.getCreation_date().substring(0, 11) + "&nicsof=" + ofeca.getNic() +
                            "&tipo_solicitudd=" + ofeca.getTipo_solicitud() + "&tasa_trans=" + ofeca.getOficial() 
                            + "&estado_cartera="
                            +"&edificio="+ cl.getEdif()
                            + "&estado_cartera="+ ofeca.getEstado_cartera() //JJCASTRO
                            + "&fec_val_cartera2=" + fapro + "&responsable=" + ofeca.getResponsable() + "&interventor=" + ofeca.getInterventor()
                            +"&padre=" + cl.getId_padre() + "&cliespec=" + cl.isOficial()
                            +"&digito_verif="+cl.getDigito_verificacion();//responsable opav JJCASTRO




                        if (!cl.getCodcli().equals("")) {
                            next = next + "&idusuario=" + cl.getCodcli();
                        }
                        if (!ofeca.getId_solicitud().equals("")) {
                            next = next + "&idsolicitud=" + ofeca.getId_solicitud();
                        }
                        //session.setAttribute("id_cliente", cl.getCodcli());
                        session.setAttribute("nics", cl.getNics());
                        session.setAttribute("acciones", clsrv.buscarAcc(acceca, usuario.getNitPropietario()));

                    } catch (Exception e) {
                        e.printStackTrace();
                        System.out.println("Error:" + e.toString());
                    }
                }
            }
        }
        if (op.equals("creaaccion")) {
            OfertaElca ofeca = new OfertaElca();
            Cliente cl = new Cliente();
            AccionesEca acceca = new AccionesEca();
            acceca.setDescripcion(request.getParameter("descripcion"));
            acceca.setContratista(request.getParameter("contratista"));
            ofeca.setId_solicitud(request.getParameter("idsolicitud"));
            acceca.setId_solicitud(ofeca.getId_solicitud());
            String resp = "";
            try {
                resp = clsrv.insertarAcc(acceca, usuario.getLogin());
                clsrv.buscarOf(ofeca);
                cl.setCodcli(ofeca.getId_cliente());
                clsrv.buscarCl(cl);


                next = next + "?mensaje=" + resp + "&ciudadd=" + cl.getCiudad() + "&nombre_contacto=" + cl.getNomContacto() +
                        "&celular_representante=" + cl.getCelRepresentante() + "&zonaa=" + cl.getSector() + "&direccion=" +
                        cl.getDireccion() + "&nit=" + cl.getNit() + "&cargo_contacto=" + cl.getCargoContacto() + "&nombre=" + cl.getNomcli() +
                        "&nombre_representante=" + cl.getNomRepresentante() + "&sectorr=" + cl.getSector() + "&telefono_contacto=" + cl.getTelContacto() +
                        "&celular_contacto=" + cl.getCelContacto() + "&login_ejecutivo=" + clsrv.getLoginEjecutivo(cl.getCodcli()) +
                        "&telefono_representante=" + cl.getTelRepresentante() + "&tipo_identificacionn=" + cl.getTipo() + "&creation_user=" +
                        cl.getCreationUser() + "&ejecutivo_ctaa=" + cl.getIdEjecutivo() + "&historico=" + ofeca.getDescripcion() +
                        "&sol_creation_user=" + ofeca.getCreation_user() + "&tipo_solicitudd=" + ofeca.getTipo_solicitud() + "&fechainicio=" +
                        ofeca.getCreation_date().substring(0, 11) + "&nicsof=" + ofeca.getNic() + "&estado_cartera=" + ofeca.getEstado_cartera() //JJCASTRO
                        + "&fec_val_cartera2=" + ofeca.getFec_val_cartera() + "&responsable=" + ofeca.getResponsable()+"&padre=" + cl.getId_padre()
                        +"&edificio="+ cl.getEdif()+"&digito_verif=" + cl.getDigito_verificacion();

                if(!cl.getCodcli().equals(""))//Error montaje 7
                {
                    next = next + "&idusuario=" + cl.getCodcli();
               }
                if (!ofeca.getId_solicitud().equals("")) {
                    next = next + "&idsolicitud=" + ofeca.getId_solicitud();
                }
                session.setAttribute("nics", cl.getNics());
                session.setAttribute("acciones", clsrv.buscarAcc(acceca, usuario.getNitPropietario()));

                /***************************Envio de Correo***************************/
                HSendMail2 hSendMail2 = new HSendMail2();

                String[] datos_mensaje = clsrv.getDatosMensaje(acceca.getId_accion());//091203

                //armar la ruta
                ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
                String ruta = rb.getString("ruta") + "/exportar/migracion/" + usuario.getLogin()+ "/";
                String nombreArch = pdfVisitaTecnica(ruta, ofeca, acceca, cl);
                ruta = (nombreArch == "") ? "" : ruta;
                hSendMail2.start("opav_asignacion@fintravalores.com",
                        //clsrv.getMails("3",  request.getParameter("idsolicitud"),acceca.getContratista())
                        //linea que reemplaza a la anterior en 091201:
                        datos_mensaje[3],"", "", "Solicitud " + ofeca.getId_solicitud(),
                        "Se�ores " + datos_mensaje[0] + ", Zona " + datos_mensaje[1] + ": se ha creado la solicitud No. " + ofeca.getId_solicitud() + " para el cliente  " + datos_mensaje[2] 
                        + ", desde este momento tiene tres (3) d�as h�biles para realizar la cotizaci�n, "
                        + "en caso contrario de No enviarla durante este plazo se direccionar� la solicitud a otro Contratista. Si no se encuentra adjunto el formato de visita tecnica, "
                        + "favor consultar en http://www.fintra.co/ y gestionar lo mas pronto posible.",
                        usuario.getLogin(),ruta,nombreArch);

                /*hSendMail2.start("imorales@fintravalores.com",
                clsrv.getMails("3",  request.getParameter("idsolicitud"),acceca.getContratista()),
                "", "", "Nueva Solicitud "+ofeca.getId_solicitud(),
                "Se ha creado una nueva accion identificada con el codigo "+acceca.getId_accion()+", asociada al contratista "+acceca.getContratista(),usuario.getLogin());*/
                /*************************Fin Envio de Correo*************************/
            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("Error:" + e.toString());
            }
        } else {
            if (op.equals("eliminaaccion")) {
                OfertaElca ofeca = new OfertaElca();
                Cliente cl = new Cliente();
                AccionesEca acceca = new AccionesEca();
                acceca.setDescripcion(request.getParameter("descripcion"));
                acceca.setContratista(request.getParameter("contratista"));
                acceca.setId_accion(request.getParameter("idaccion"));
                ofeca.setId_solicitud(request.getParameter("idsolicitud"));
                acceca.setId_solicitud(ofeca.getId_solicitud());
                String resp = "";
                try {
                    resp = clsrv.delAcc(acceca, usuario.getLogin());
                    clsrv.buscarOf(ofeca);
                    cl.setCodcli(ofeca.getId_cliente());
//                    clsrv.buscarCl(cle);


                    next = next + "?mensaje=" + resp + "&ciudadd=" + cl.getCiudad() + "&nombre_contacto=" + cl.getNomContacto() +
                            "&celular_representante=" + cl.getCelRepresentante() + "&zonaa=" + cl.getSector() + "&direccion=" +
                            cl.getDireccion() + "&nit=" + cl.getNit() + "&cargo_contacto=" + cl.getCargoContacto() + "&nombre=" + cl.getNomcli() +
                            "&nombre_representante=" + cl.getNomRepresentante() + "&login_ejecutivo=" + clsrv.getLoginEjecutivo(cl.getCodcli())
                            + "&sectorr=" + cl.getSector() + "&telefono_contacto=" + cl.getTelContacto() + "&celular_contacto=" + cl.getCelContacto() +
                            "&sol_creation_user=" + ofeca.getCreation_user() + "&telefono_representante=" + cl.getTelRepresentante() +
                            "&tipo_identificacionn=" + cl.getTipo() + "&creation_user=" + cl.getCreationUser() + "&ejecutivo_ctaa=" +
                            cl.getIdEjecutivo() + "&historico=" + ofeca.getDescripcion() + "&fechainicio=" + ofeca.getCreation_date().substring(0, 11) +
                            "&nicsof=" + ofeca.getNic() + "&tipo_solicitudd=" + ofeca.getTipo_solicitud() + "&estado_cartera=" + ofeca.getEstado_cartera() //JJCASTRO
                            + "&fec_val_cartera2=" + ofeca.getFec_val_cartera() + "&responsable=" + ofeca.getResponsable()
                            +"&edificio="+ cl.getEdif();

                    if (!cl.getCodcli().equals("")) {
                        next = next + "&idusuario=" + cl.getCodcli();
                    }
                    if (!ofeca.getId_solicitud().equals("")) {
                        next = next + "&idsolicitud=" + ofeca.getId_solicitud();
                    }
                    session.setAttribute("nics", cl.getNics());
                    session.setAttribute("acciones", clsrv.buscarAcc(acceca, usuario.getNitPropietario()));
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("Error:" + e.toString());
                }
            }
        }

        if (op.equals("refrescarx")) {
            OfertaElca ofeca = new OfertaElca();
            Cliente cl = new Cliente();
            AccionesEca acceca = new AccionesEca();
            acceca.setDescripcion(request.getParameter("descripcion"));
            acceca.setContratista(request.getParameter("contratista"));
            ofeca.setId_solicitud(request.getParameter("idsolicitud"));
            acceca.setId_solicitud(ofeca.getId_solicitud());
            String resp = "";
            try {
                //resp=clsrv.insertarAcc(acceca,usuario.getLogin());
                resp = "pagina refrescada.";
                clsrv.buscarOf(ofeca);
                cl.setCodcli(ofeca.getId_cliente());
//                clsrv.buscarCl(cle);
                fapro = ofeca.getFec_val_cartera().equals("0099-01-01 00:00:00") ? "Sin Fecha" : ofeca.getFec_val_cartera();//JJCASTRO


                /*next=next+"?mensaje="+resp+"&ciudadd="+cle.getCiudad()+"&nombre_contacto="+cle.getNombre_contacto()
                +"&celular_representante="+cl.getCelRepresentante()+"&zonaa="+cle.getDepartamento()
                +"&direccion="+cle.getDireccion()
                +"&nit="+cle.getNit()+"&cargo_contacto="+cle.getCargo_contacto()
                +"&nombre="+cle.getNombre()+"&nombre_representante="+cle.getNombre_representante()
                +"&sectorr="+cle.getSector()+"&telefono_contacto="+cle.getTel1()+"&celular_contacto="+cle.getTel2()+"&login_ejecutivo="+clsrv.getLoginEjecutivo(cle.getId_cliente())
                +"&telefono_representante="+cle.getTel_representante()+"&tipo_identificacionn="+cle.getTipo()+"&creation_user="+cle.getCreation_user()
                +"&ejecutivo_ctaa="+cle.getId_ejecutivo()+"&historico="+ofeca.getDescripcion()+"&sol_creation_user="+ofeca.getCreation_user()+"&tipo_solicitudd="+ofeca.getTipo_solicitud()
                +"&fechainicio="+ofeca.getCreation_date().substring(0,11)+"&nicsof="+ofeca.getNic();*/

                next = next + "?mensaje=" + resp + "&ciudadd=" + cl.getCiudad() + "&nombre_contacto=" + cl.getNomContacto() +
                            "&celular_representante=" + cl.getCelRepresentante() + "&zonaa=" + cl.getSector() + "&direccion=" +
                            cl.getDireccion() + "&nit=" + cl.getNit() + "&cargo_contacto=" + cl.getCargoContacto() + "&nombre=" + cl.getNomcli() +
                            "&nombre_representante=" + cl.getNomRepresentante() + "&login_ejecutivo=" + clsrv.getLoginEjecutivo(cl.getCodcli())
                            + "&sectorr=" + cl.getSector() + "&telefono_contacto=" + cl.getTelContacto() + "&celular_contacto=" + cl.getCelContacto() +
                            "&sol_creation_user=" + ofeca.getCreation_user() + "&telefono_representante=" + cl.getTelRepresentante() +
                            "&tipo_identificacionn=" + cl.getTipo() + "&creation_user=" + cl.getCreationUser() + "&ejecutivo_ctaa=" +
                            cl.getIdEjecutivo() + "&historico=" + ofeca.getDescripcion() + "&fechainicio=" + ofeca.getCreation_date().substring(0, 11) +
                            "&nicsof=" + ofeca.getNic() + "&tipo_solicitudd=" + ofeca.getTipo_solicitud() + "&estado_cartera=" + ofeca.getEstado_cartera() //JJCASTRO
                            + "&fec_val_cartera2=" + ofeca.getFec_val_cartera() + "&responsable=" + ofeca.getResponsable()
                            +"&edificio="+ cl.getEdif();

                if (!cl.getCodcli().equals("")) {
                    next = next + "&idusuario=" + cl.getCodcli();
                }
                if (!ofeca.getId_solicitud().equals("")) {
                    next = next + "&idsolicitud=" + ofeca.getId_solicitud();
                }
                session.setAttribute("nics", cl.getNics());
                session.setAttribute("acciones", clsrv.buscarAcc(acceca, usuario.getNitPropietario()));

            } catch (Exception e) {
                System.out.println("error refrescando" + e.toString() + "__" + e.getMessage());
                e.printStackTrace();
                System.out.println("Error:" + e.toString());
            }
        }
        if(op.equals("equival")){
            next = "/jsp/opav/eq_pag.jsp";
            String route="";
            try {
                route = clsrv.insercion();
            }
            catch (Exception e) {
                System.out.println("error: "+e.toString());
                e.printStackTrace();
            }
            next = next + "?cadenatabla="+route;
        }
        if(op.equals("sequiv")){
            try {
                next = "/jsp/opav/eq_pag.jsp";
                String stringfec="";
                java.text.SimpleDateFormat fmt = new java.text.SimpleDateFormat("yyyyMMdd");
                stringfec = request.getParameter("stringfec")!=null? request.getParameter("stringfec") :fmt.format( new java.util.Date() );
                java.util.ArrayList lista = null;
                int filas = request.getParameter("numfilas")!=null? Integer.parseInt(request.getParameter("numfilas")):0;
                lista = new java.util.ArrayList();
                int progreso = 0;
                if(filas>0){
                    for(int i=0;i<filas;i++){
                        if((request.getParameter("x"+i)!=null) && (!( (request.getParameter("x"+i)).equals("") ) )
                                && (request.getParameter("codfin"+i)!=null) && (!( (request.getParameter("codfin"+i)).equals("") ) ) ){
                            lista.add(request.getParameter("x"+i) + ";_;" + request.getParameter("codfin"+i));
                            progreso++;
                        }
                    }
                    if(progreso>0){
                        clsrv.directoequiv(lista, stringfec);
                    }
                    else{
                        System.out.println("No se seleccionaron filas para procesar");
                    }
                }
                else{
                    System.out.println("No existen filas para procesar");
                }
                next += "?cadenatabla=" + stringfec;
            }
            catch (Exception e) {
                System.out.println("Error al insertar en equivalencia en fintra: "+e.toString());
                e.printStackTrace();
            }
        }
        if(op.equals("sendit")){
            try {
                next = "/jsp/opav/eq_pag.jsp";
                String stringfec="";
                java.util.ArrayList lista = null;
                int filas = request.getParameter("numfilas")!=null? Integer.parseInt(request.getParameter("numfilas")):0;
                lista = new java.util.ArrayList();
                int progreso = 0;
                java.text.SimpleDateFormat fmt = new java.text.SimpleDateFormat("yyyyMMdd");
                stringfec = request.getParameter("stringfec")!=null? request.getParameter("stringfec") :fmt.format( new java.util.Date() );
                if(filas>0){
                    for(int i=0;i<filas;i++){
                        if((request.getParameter("x"+i)!=null) && (!( (request.getParameter("x"+i)).equals("") ) ) ){
                            lista.add(request.getParameter("x"+i));
                            progreso++;
                        }
                    }
                    if(progreso>0){
                        clsrv.insFin(lista, stringfec);
                    }
                    else{
                        System.out.println("No se seleccionaron filas para procesar");
                    }
                }
                else{
                    System.out.println("No existen filas para procesar");
                }
                next += "?cadenatabla=" + stringfec;
            }
            catch (Exception e) {
                System.out.println("Error al insertar en fintra: "+e.toString());
                e.printStackTrace();
            }
        }

        try {
            if(op.equals("missing")){
                java.util.ArrayList lista = null;
                String cadresp="<table style='border-collapse:collapse;width:500px;' border='1'>" +
                        "<tr class='subtitulo1'><td>Nombre</td><td>Nit</td></tr>";
                String cadtemp[] = null;
                try {
                    lista = clsrv.buscarId();
                    if(lista.size()>0){
                        for (int i = 0; i < lista.size(); i++) {
                            cadtemp = ((String)(lista.get(i))).split(";_;");
                            cadresp = cadresp + "<tr class='fila'><td>"+cadtemp[0]+"</td><td>"+cadtemp[1]+"</td></tr>";
                        }
                    }
                    else{
                        cadresp = cadresp + "<tr class='fila'><td colspan='2'>No se encontraron registros</td></tr>";
                    }
                }
                catch (Exception e) {
                    System.out.println("error: "+e.toString());
                    e.printStackTrace();
                }
                cadresp = cadresp + "</table>";
                response.setContentType("text/plain; charset=utf-8");
                response.setHeader("Cache-Control", "no-cache");
                response.getWriter().println(cadresp);

            }
            else if(op.equals("viewer")){
                java.util.ArrayList lista = null;
                String cadresp="<table style='border-collapse:collapse;width:500px;' border='1'>" +
                        "<tr class='fila'><td colspan='2' style='text-align:center;'>Coincidencias encontradas en fintra</td></tr>" +
                        "<tr class='subtitulo1'><td>Nombre</td><td>Nit</td></tr>";
                String fec = "";
                String cadtemp[] = null;
                String nitquery = "";
                try {
                    nitquery = request.getParameter("nit")!=null? request.getParameter("nit") :"";
                    fec = request.getParameter("fec")!=null? request.getParameter("fec") :"";
                    lista = clsrv.buscarReps(nitquery,fec);
                    if(lista.size()>0){
                        for (int i = 0; i < lista.size(); i++) {
                            cadtemp = ((String)(lista.get(i))).split(";_;");
                            cadresp = cadresp + "<tr class='fila'><td>"+cadtemp[0]+"</td><td>"+cadtemp[1]+"</td></tr>";
                        }
                    }
                    else{
                        cadresp = cadresp + "<tr class='fila'><td colspan='2' style='text-align:center;'>No se encontraron registros</td></tr>";
                    }
                }
                catch (Exception e) {
                    System.out.println("error: "+e.toString());
                    e.printStackTrace();
                }
                cadresp = cadresp + "</table>";
                response.setContentType("text/plain; charset=utf-8");
                response.setHeader("Cache-Control", "no-cache");
                response.getWriter().println(cadresp);
            }

            else
            {
            
            }



           /* if(op.equals("valida_cliente"))
            {
              redirect=false;
              this.RespuestaValidaCliente(clsrv.ValidaClienteXnit(request.getParameter("nit")));
            }*/


            int opcion = 0;
            if (!op.equals(""))
            {
                //vamos a validar la excepcion
                try {
                    opcion = Integer.parseInt(op);
                } catch (NumberFormatException e) {
                    //entra aqui cuando ocurre un error
                    opcion = 0;
                }

            }
            switch (opcion)
            {
                case 10://valida_cliente
                    redirect=false;
                    this.RespuestaXML(clsrv.ValidaClienteXnit(request.getParameter("nit")));

                break;

                case 11://valida_cliente
                redirect=false;
               this.RespuestaXMLCliente(clsrv.MarcarCliente(request.getParameter("nit")));

                break;

            }







        }
        catch (Exception eee)
        {
            eee.printStackTrace();
            System.out.println("Error:" + eee.toString());
        }


        try
      {
        if(redirect==true)
        {
            //this.RespuestaValidaCliente();
            this.dispatchRequest(next);
        }

      }
      catch (Exception e)
      {
          System.out.println("Error al redireccionar o escribir respuesta: "+e.toString());
          e.printStackTrace();
      }
    
    
    
    }










           private void RespuestaXML(String dato) throws Exception {
       String xml = "";
       String  cod_interventor = request.getParameter("cod_inter")!=null?request.getParameter("cod_inter"):"";
       String  opcion = request.getParameter("op")!=null?request.getParameter("op"):"";
       InterventorService iserv = new InterventorService();


        xml = "<?xml version=\"1.0\" encoding=\"iso-8859-1\"?>";
        xml += "<mensaje>";      
        xml += dato;
        xml += "</mensaje>";

        response.setContentType("text/xml");
        response.setHeader("Cache-Control", "no-cache");
        response.getWriter().println(xml);
    }





       private void RespuestaXMLCliente(Cliente cl) throws Exception {
       String xml = "";
       String  cod_interventor = request.getParameter("cod_inter")!=null?request.getParameter("cod_inter"):"";
       String  opcion = request.getParameter("op")!=null?request.getParameter("op"):"";
       InterventorService iserv = new InterventorService();


        xml = "<?xml version=\"1.0\" encoding=\"iso-8859-1\"?>";
        xml += "<datos>";
        xml += "<codigo>";
        xml += cl.getCodcli();
        xml += "</codigo>";
        xml += "<nit>";
        xml += cl.getNit();
        xml += "</nit>";
        xml += "<nombre>";
        xml += cl.getNomcli();
        xml += "</nombre>";
        xml += "</datos>";



        response.setContentType("text/xml");
        response.setHeader("Cache-Control", "no-cache");
        response.getWriter().println(xml);
    }







       protected void escribirResponse(String dato) throws Exception{
        try {
            response.setContentType("text/plain; charset=utf-8");
            response.setHeader("Cache-Control", "no-cache");
            response.getWriter().println(dato);
        }
        catch (Exception e) {
            throw new Exception("Error al escribir el response: "+e.toString());
        }
    }

    private String pdfVisitaTecnica(String ruta, OfertaElca oec, AccionesEca aec, Cliente cl) {
        try {
            String nombre = "Formato_Visita_Tecnica.pdf";
            File dir = new File(ruta);
            dir.mkdir();
            
            Document document = new Document(PageSize.A4.rotate());
            PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(ruta+nombre));
            
            document.open();
            document.addTitle("GCF-RE-14 Formato de Visita T�cnica");
            document.addAuthor("Consorcio multiservicios");
            
            PdfPTable tabla = new PdfPTable(3);
            tabla.setWidthPercentage(100);
            tabla.setWidths(new int[]{10, 50, 10});
            tabla.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
            tabla.setKeepTogether(true);
            
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            Image logo = Image.getInstance(rb.getString("ruta")+"/images/selectrik.jpg");
            logo.scalePercent(55f);
            PdfPCell celda = new PdfPCell(logo);
            celda.setHorizontalAlignment(Element.ALIGN_CENTER);
            tabla.addCell(celda);
            
            celda = new PdfPCell(new Phrase("FORMATO DE VISITA T�CNICA", new Font(Font.FontFamily.TIMES_ROMAN, 14)));
            celda.setHorizontalAlignment(Element.ALIGN_CENTER);
            tabla.addCell(celda);
            
            celda = new PdfPCell(new Phrase("C�digo: GCF-RE-14\nVersi�n: 01\nVigente desde: 2013-10-10", new Font(Font.FontFamily.TIMES_ROMAN, 8)));
            celda.setHorizontalAlignment(Element.ALIGN_CENTER);
            tabla.addCell(celda);
            
            document.add(tabla);
            CiudadService cs = new CiudadService(usuario.getBd());
            
            Paragraph parrafo = new Paragraph(new Phrase("Fecha solicitud: " 
                                + oec.getCreation_date().substring(0, oec.getCreation_date().indexOf(" "))+", "
                                + cs.obtenerCiudad(cl.getCiudad()).getNomCiu()+"\n\n", new Font(Font.FontFamily.TIMES_ROMAN, 8)));
            parrafo.setAlignment(Element.ALIGN_RIGHT);
            document.add(parrafo);
            
            tabla = new PdfPTable(3);
            tabla.setWidthPercentage(100);
            
            celda = new PdfPCell(new Phrase("DATOS B�SICOS DEL CLIENTE Y DE LA VISITA", new Font(Font.FontFamily.TIMES_ROMAN, 9)));
            celda.setBackgroundColor(BaseColor.GRAY);
            celda.setColspan(3);
            tabla.addCell(celda);
            
            tabla.addCell(new Phrase("Cliente: "+cl.getNomcli(), new Font(Font.FontFamily.TIMES_ROMAN, 8)));
            tabla.addCell(new Phrase("Direcci�n: "+cl.getDireccion(), new Font(Font.FontFamily.TIMES_ROMAN, 8)));
            tabla.addCell(new Phrase("Id solicitud: "+aec.getId_solicitud(), new Font(Font.FontFamily.TIMES_ROMAN, 8)));
            
            tabla.addCell(new Phrase("Contacto: "+cl.getNomContacto(), new Font(Font.FontFamily.TIMES_ROMAN, 8)));
            tabla.addCell(new Phrase("Telefono contacto: "+cl.getTelContacto(), new Font(Font.FontFamily.TIMES_ROMAN, 8)));
            tabla.addCell(new Phrase("P.B.: ", new Font(Font.FontFamily.TIMES_ROMAN, 8)));
            
            PdfPTable tablaInt = new PdfPTable(3);
            
            tablaInt.addCell(new Phrase("NIC: "+oec.getNic(), new Font(Font.FontFamily.TIMES_ROMAN, 8)));
            tablaInt.addCell(new Phrase("NR: "+(cl.getTipo().equals("NR")?"X":""), new Font(Font.FontFamily.TIMES_ROMAN, 8)));
            tablaInt.addCell(new Phrase("R: "+(cl.getTipo().equals("R")?"X":""), new Font(Font.FontFamily.TIMES_ROMAN, 8)));
            tabla.addCell(tablaInt);
            tabla.addCell(new Phrase("Persona que atiende la visita: "+cl.getNomContacto(), new Font(Font.FontFamily.TIMES_ROMAN, 8)));
            tabla.addCell(new Phrase("P.A.: ", new Font(Font.FontFamily.TIMES_ROMAN, 8)));
            
            tablaInt = new PdfPTable(2);
            tablaInt.addCell(new Phrase("Fecha Visita: ", new Font(Font.FontFamily.TIMES_ROMAN, 8)));
            tablaInt.addCell(new Phrase("Hora Visita: ", new Font(Font.FontFamily.TIMES_ROMAN, 8)));
            tabla.addCell(tablaInt);
            tabla.addCell(new Phrase("Nombre ejecutivo de cuenta: "+cl.getIdEjecutivo(), new Font(Font.FontFamily.TIMES_ROMAN, 8)));
            tabla.addCell(new Phrase("No. Aviso: ", new Font(Font.FontFamily.TIMES_ROMAN, 8)));
            
            celda = new PdfPCell(new Phrase("DA�O / SOLICITUD REPORTADA", new Font(Font.FontFamily.TIMES_ROMAN, 9)));
            celda.setBackgroundColor(BaseColor.GRAY);
            tabla.addCell(celda);
            
            celda = new PdfPCell(new Phrase("DIAGRAMA UNIFILIAR", new Font(Font.FontFamily.TIMES_ROMAN, 9)));
            celda.setBackgroundColor(BaseColor.GRAY);
            celda.setColspan(2);
            tabla.addCell(celda);
            
            tabla.addCell(new Phrase(aec.getDescripcion(), new Font(Font.FontFamily.TIMES_ROMAN, 9)));
            
            celda = new PdfPCell(new Phrase(""));
            celda.setColspan(2);
            celda.setRowspan(2);
            tabla.addCell(celda);
            
            tablaInt = new PdfPTable(12);
            celda = new PdfPCell(new Phrase("Cajas Primarias:", new Font(Font.FontFamily.TIMES_ROMAN, 6)));
            celda.setColspan(2);
            tablaInt.addCell(celda);
            tablaInt.addCell(new Phrase("Cant.", new Font(Font.FontFamily.TIMES_ROMAN, 6)));
            tablaInt.addCell(new Phrase("", new Font(Font.FontFamily.TIMES_ROMAN, 6)));
            tablaInt.addCell(new Phrase("Est.", new Font(Font.FontFamily.TIMES_ROMAN, 6)));
            tablaInt.addCell(new Phrase("", new Font(Font.FontFamily.TIMES_ROMAN, 6)));
            celda = new PdfPCell(new Phrase("Tipo Apoyo:", new Font(Font.FontFamily.TIMES_ROMAN, 6)));
            celda.setColspan(2);
            tablaInt.addCell(celda);
            tablaInt.addCell(new Phrase("", new Font(Font.FontFamily.TIMES_ROMAN, 6)));
            celda = new PdfPCell(new Phrase("Estado:", new Font(Font.FontFamily.TIMES_ROMAN, 6)));
            celda.setColspan(2);
            tablaInt.addCell(celda);
            tablaInt.addCell(new Phrase("", new Font(Font.FontFamily.TIMES_ROMAN, 6)));
            
            celda = new PdfPCell(new Phrase("Desc. s tensi�n:", new Font(Font.FontFamily.TIMES_ROMAN, 6)));
            celda.setColspan(2);
            tablaInt.addCell(celda);
            tablaInt.addCell(new Phrase("Cant.", new Font(Font.FontFamily.TIMES_ROMAN, 6)));
            tablaInt.addCell(new Phrase("", new Font(Font.FontFamily.TIMES_ROMAN, 6)));
            tablaInt.addCell(new Phrase("Est.", new Font(Font.FontFamily.TIMES_ROMAN, 6)));
            tablaInt.addCell(new Phrase("", new Font(Font.FontFamily.TIMES_ROMAN, 6)));
            celda = new PdfPCell(new Phrase("Polo TIerra:", new Font(Font.FontFamily.TIMES_ROMAN, 6)));
            celda.setColspan(2);
            tablaInt.addCell(celda);
            tablaInt.addCell(new Phrase("", new Font(Font.FontFamily.TIMES_ROMAN, 6)));
            tablaInt.addCell(new Phrase("Est", new Font(Font.FontFamily.TIMES_ROMAN, 6)));
            celda = new PdfPCell(new Phrase("", new Font(Font.FontFamily.TIMES_ROMAN, 6)));
            celda.setColspan(2);
            tablaInt.addCell(celda);
            
            celda = new PdfPCell(new Phrase("Term. Premolde:", new Font(Font.FontFamily.TIMES_ROMAN, 6)));
            celda.setColspan(2);
            tablaInt.addCell(celda);
            tablaInt.addCell(new Phrase("Cant.", new Font(Font.FontFamily.TIMES_ROMAN, 6)));
            tablaInt.addCell(new Phrase("", new Font(Font.FontFamily.TIMES_ROMAN, 6)));
            tablaInt.addCell(new Phrase("Est.", new Font(Font.FontFamily.TIMES_ROMAN, 6)));
            tablaInt.addCell(new Phrase("", new Font(Font.FontFamily.TIMES_ROMAN, 6)));
            celda = new PdfPCell(new Phrase("Acometida:", new Font(Font.FontFamily.TIMES_ROMAN, 6)));
            celda.setColspan(2);
            tablaInt.addCell(celda);
            tablaInt.addCell(new Phrase("AERE.", new Font(Font.FontFamily.TIMES_ROMAN, 6)));
            tablaInt.addCell(new Phrase("", new Font(Font.FontFamily.TIMES_ROMAN, 6)));
            tablaInt.addCell(new Phrase("SUBT.", new Font(Font.FontFamily.TIMES_ROMAN, 6)));
            tablaInt.addCell(new Phrase("", new Font(Font.FontFamily.TIMES_ROMAN, 6)));
                        
            tablaInt.addCell(new Phrase("Bajante:", new Font(Font.FontFamily.TIMES_ROMAN, 6)));
            tablaInt.addCell(new Phrase("Est.", new Font(Font.FontFamily.TIMES_ROMAN, 6)));
            tablaInt.addCell(new Phrase("", new Font(Font.FontFamily.TIMES_ROMAN, 6)));
            celda = new PdfPCell(new Phrase("Conductor.", new Font(Font.FontFamily.TIMES_ROMAN, 6)));
            celda.setColspan(2);
            tablaInt.addCell(celda);
            tablaInt.addCell(new Phrase("Tipo", new Font(Font.FontFamily.TIMES_ROMAN, 6)));
            tablaInt.addCell(new Phrase("", new Font(Font.FontFamily.TIMES_ROMAN, 6)));
            tablaInt.addCell(new Phrase("Cal.", new Font(Font.FontFamily.TIMES_ROMAN, 6)));
            tablaInt.addCell(new Phrase("", new Font(Font.FontFamily.TIMES_ROMAN, 6)));
            celda = new PdfPCell(new Phrase("Aislam.", new Font(Font.FontFamily.TIMES_ROMAN, 6)));
            celda.setColspan(2);
            tablaInt.addCell(celda);
            tablaInt.addCell(new Phrase("", new Font(Font.FontFamily.TIMES_ROMAN, 6)));
            
            celda = new PdfPCell(new Phrase("Tipo de S/E:", new Font(Font.FontFamily.TIMES_ROMAN, 6)));
            celda.setColspan(2);
            tablaInt.addCell(celda);
            celda = new PdfPCell(new Phrase("", new Font(Font.FontFamily.TIMES_ROMAN, 6)));
            celda.setColspan(2);
            tablaInt.addCell(celda);
            celda = new PdfPCell(new Phrase("No Trafos:", new Font(Font.FontFamily.TIMES_ROMAN, 6)));
            celda.setColspan(2);
            tablaInt.addCell(celda);
            tablaInt.addCell(new Phrase("", new Font(Font.FontFamily.TIMES_ROMAN, 6)));
            celda = new PdfPCell(new Phrase("Capacidad Total (kVA):", new Font(Font.FontFamily.TIMES_ROMAN, 6)));
            celda.setColspan(5);
            tablaInt.addCell(celda);
            
            celda = new PdfPCell(new Phrase("No Fases:", new Font(Font.FontFamily.TIMES_ROMAN, 6)));
            celda.setColspan(2);
            tablaInt.addCell(celda);
            tablaInt.addCell(new Phrase("", new Font(Font.FontFamily.TIMES_ROMAN, 6)));
            tablaInt.addCell(new Phrase("VP.", new Font(Font.FontFamily.TIMES_ROMAN, 6)));
            celda = new PdfPCell(new Phrase("", new Font(Font.FontFamily.TIMES_ROMAN, 6)));
            celda.setColspan(2);
            tablaInt.addCell(celda);
            tablaInt.addCell(new Phrase("VS.", new Font(Font.FontFamily.TIMES_ROMAN, 6)));
            celda = new PdfPCell(new Phrase("", new Font(Font.FontFamily.TIMES_ROMAN, 6)));
            celda.setColspan(2);
            tablaInt.addCell(celda);
            celda = new PdfPCell(new Phrase("Est. Neutro:", new Font(Font.FontFamily.TIMES_ROMAN, 6)));
            celda.setColspan(2);
            tablaInt.addCell(celda);
            tablaInt.addCell(new Phrase("", new Font(Font.FontFamily.TIMES_ROMAN, 6)));
            
            celda = new PdfPCell(new Phrase("Estado transformador:", new Font(Font.FontFamily.TIMES_ROMAN, 6)));
            celda.setColspan(3);
            tablaInt.addCell(celda);
            tablaInt.addCell(new Phrase("", new Font(Font.FontFamily.TIMES_ROMAN, 6)));
            tablaInt.addCell(new Phrase("Emp.", new Font(Font.FontFamily.TIMES_ROMAN, 6)));
            tablaInt.addCell(new Phrase("", new Font(Font.FontFamily.TIMES_ROMAN, 6)));
            tablaInt.addCell(new Phrase("Pint.", new Font(Font.FontFamily.TIMES_ROMAN, 6)));
            celda = new PdfPCell(new Phrase("", new Font(Font.FontFamily.TIMES_ROMAN, 6)));
            celda.setColspan(2);
            tablaInt.addCell(celda);
            tablaInt.addCell(new Phrase("Oxid", new Font(Font.FontFamily.TIMES_ROMAN, 6)));
            celda = new PdfPCell(new Phrase("", new Font(Font.FontFamily.TIMES_ROMAN, 6)));
            celda.setColspan(2);
            tablaInt.addCell(celda);
            
            celda = new PdfPCell(new Phrase("Malla a tierra:", new Font(Font.FontFamily.TIMES_ROMAN, 6)));
            celda.setColspan(2);
            tablaInt.addCell(celda);
            tablaInt.addCell(new Phrase("Est.", new Font(Font.FontFamily.TIMES_ROMAN, 6)));
            tablaInt.addCell(new Phrase("", new Font(Font.FontFamily.TIMES_ROMAN, 6)));
            celda = new PdfPCell(new Phrase("Term. Premolde. Interior:", new Font(Font.FontFamily.TIMES_ROMAN, 6)));
            celda.setColspan(4);
            tablaInt.addCell(celda);
            tablaInt.addCell(new Phrase("Cant.", new Font(Font.FontFamily.TIMES_ROMAN, 6)));
            tablaInt.addCell(new Phrase("", new Font(Font.FontFamily.TIMES_ROMAN, 6)));
            tablaInt.addCell(new Phrase("Est.", new Font(Font.FontFamily.TIMES_ROMAN, 6)));
            tablaInt.addCell(new Phrase("", new Font(Font.FontFamily.TIMES_ROMAN, 6)));
            
            celda = new PdfPCell(new Phrase("Celdas y tableros:", new Font(Font.FontFamily.TIMES_ROMAN, 6)));
            celda.setRowspan(2);
            celda.setColspan(2);
            tablaInt.addCell(celda);
            celda = new PdfPCell(new Phrase("Seleccionador:", new Font(Font.FontFamily.TIMES_ROMAN, 6)));
            celda.setColspan(3);
            tablaInt.addCell(celda);
            celda = new PdfPCell(new Phrase("", new Font(Font.FontFamily.TIMES_ROMAN, 6)));
            celda.setColspan(2);
            tablaInt.addCell(celda);
            celda = new PdfPCell(new Phrase("Transformador:", new Font(Font.FontFamily.TIMES_ROMAN, 6)));
            celda.setColspan(3);
            tablaInt.addCell(celda);
            celda = new PdfPCell(new Phrase("", new Font(Font.FontFamily.TIMES_ROMAN, 6)));
            celda.setColspan(2);
            tablaInt.addCell(celda);
            
            celda = new PdfPCell(new Phrase("Baja tensi�n:", new Font(Font.FontFamily.TIMES_ROMAN, 6)));
            celda.setColspan(3);
            tablaInt.addCell(celda);
            tablaInt.addCell(new Phrase("Cant.", new Font(Font.FontFamily.TIMES_ROMAN, 6)));
            tablaInt.addCell(new Phrase("", new Font(Font.FontFamily.TIMES_ROMAN, 6)));
            celda = new PdfPCell(new Phrase("Breaker ppal.", new Font(Font.FontFamily.TIMES_ROMAN, 6)));
            celda.setColspan(3);
            tablaInt.addCell(celda);
            celda = new PdfPCell(new Phrase("", new Font(Font.FontFamily.TIMES_ROMAN, 6)));
            celda.setColspan(2);
            tablaInt.addCell(celda);
            
            celda = new PdfPCell(new Phrase("Celda medida:", new Font(Font.FontFamily.TIMES_ROMAN, 6)));
            celda.setColspan(4);
            tablaInt.addCell(celda);
            celda = new PdfPCell(new Phrase("Equipo medida:", new Font(Font.FontFamily.TIMES_ROMAN, 6)));
            celda.setColspan(3);
            tablaInt.addCell(celda);
            celda = new PdfPCell(new Phrase("", new Font(Font.FontFamily.TIMES_ROMAN, 6)));
            celda.setColspan(2);
            tablaInt.addCell(celda);
            tablaInt.addCell(new Phrase("tipo:", new Font(Font.FontFamily.TIMES_ROMAN, 6)));
            celda = new PdfPCell(new Phrase("", new Font(Font.FontFamily.TIMES_ROMAN, 6)));
            celda.setColspan(2);
            tablaInt.addCell(celda);
            
            celda = new PdfPCell(new Phrase("Banco de cond:", new Font(Font.FontFamily.TIMES_ROMAN, 6)));
            celda.setColspan(2);
            tablaInt.addCell(celda);
            celda = new PdfPCell(new Phrase("     kvar", new Font(Font.FontFamily.TIMES_ROMAN, 6)));
            celda.setHorizontalAlignment(Element.ALIGN_RIGHT);
            celda.setColspan(2);
            tablaInt.addCell(celda);
            celda = new PdfPCell(new Phrase("Planta emerg:", new Font(Font.FontFamily.TIMES_ROMAN, 6)));
            celda.setColspan(3);
            tablaInt.addCell(celda);
            celda = new PdfPCell(new Phrase("", new Font(Font.FontFamily.TIMES_ROMAN, 6)));
            celda.setColspan(2);
            tablaInt.addCell(celda);
            tablaInt.addCell(new Phrase("KVA:", new Font(Font.FontFamily.TIMES_ROMAN, 6)));
            celda = new PdfPCell(new Phrase("", new Font(Font.FontFamily.TIMES_ROMAN, 6)));
            celda.setColspan(2);
            tablaInt.addCell(celda);
            
            celda = new PdfPCell(tablaInt);
            tabla.addCell(celda);
            
            celda = new PdfPCell(new Phrase("OBSERVACIONES GENERALES", new Font(Font.FontFamily.TIMES_ROMAN, 9)));
            celda.setBackgroundColor(BaseColor.GRAY);
            tabla.addCell(celda);
            
            celda = new PdfPCell(new Phrase("INSPECCI�N RESULTANTE", new Font(Font.FontFamily.TIMES_ROMAN, 9)));
            celda.setBackgroundColor(BaseColor.GRAY);
            celda.setColspan(2);
            tabla.addCell(celda);
            
            celda = new PdfPCell(new Phrase("\n\n\n\n\n\n\n"));
            tabla.addCell(celda);
            
            celda = new PdfPCell(new Phrase());
            celda.setColspan(2);
            tabla.addCell(celda);

            celda = new PdfPCell(new Phrase("Firma de quien atendi� la visita:\nN�mero de C�dula:", new Font(Font.FontFamily.TIMES_ROMAN, 8)));
            tabla.addCell(celda);
            
            tablaInt = new PdfPTable(2);
            String nomLis = "";
            String[] aux;
            HttpSession session = request.getSession();
            ArrayList lista = (ArrayList) session.getAttribute("listadoResponsables");
            for (Object d : lista ){
                aux = (String[])d;
                if(aux[0].equalsIgnoreCase(oec.getResponsable())) {
                    nomLis = aux[1]; break;
                }
            }
            tablaInt.addCell(new Phrase("Responsable: "+nomLis, new Font(Font.FontFamily.TIMES_ROMAN, 8)));
            ClientesVerService clsrv = new ClientesVerService(usuario.getBd());
            lista = clsrv.getContratistas();
            nomLis = "";
            for(Object d : lista) {
                aux = (String[])d;
                if(aux[1].equalsIgnoreCase(aec.getContratista()) ) {
                    nomLis = aux[0]; break;
                }
            }
            tablaInt.addCell(new Phrase("Contratista asignado:  "+nomLis+"\nFecha envio: ", new Font(Font.FontFamily.TIMES_ROMAN, 8)));
            celda = new PdfPCell(tablaInt);
            celda.setColspan(2);
            tabla.addCell(celda);
            
            document.add(tabla);
            document.close();
            
            return nombre;
        } catch (Exception ex) {
            ex.printStackTrace();
            return "";
        }
    }
}
