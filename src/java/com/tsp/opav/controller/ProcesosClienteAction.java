/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.opav.controller;

/**
 *
 * @author user
 */
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.tsp.exceptions.InformationException;
import com.tsp.opav.model.DAOS.ProcesosClienteDAO;
import com.tsp.opav.model.DAOS.impl.ProcesosClienteImpl;
import com.tsp.opav.model.beans.AccionesEca;
import com.tsp.opav.model.beans.CotizacionSl;
import com.tsp.operation.model.beans.BeanGeneral;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.beans.Cliente;
import com.tsp.opav.model.beans.OfertaElca;
import com.tsp.operation.model.TransaccionService;
import com.tsp.operation.model.beans.BeansMultiservicio;
import com.tsp.util.ExcelApiUtil;
import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import com.tsp.util.Util;
import static java.lang.Short.parseShort;
import java.net.URLDecoder;
import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.xhtmlrenderer.css.style.derived.StringValue;

public class ProcesosClienteAction extends Action {

    private final int cargarTiposClientes = 0;
    private final int cargarDepartamentos = 1;
    private final int cargarCiudades = 2;
    private final int CrearCliente = 3;
    private final int cargarClientesPadre = 4;
    private final int cargarNics = 5;
    private final int cargarClienteId = 6;
    private final int cargarPadreId = 7;
    private final int cargarDepCiuid = 8;
    private final int obteneridclientenit = 9;
    private final int modificarCliente = 10;
    private final int CargarNics = 11;
    private final int cargarListadoLineaDeNegocio = 12;
    private final int cargarListadoTipoSolicitud = 13;
    private final int cargarListadoResponsable = 14;
    private final int cargarListadoInterventor = 15;
    private final int crearSolicitud = 16;
    private final int cargarSolicitud = 17;
    private final int cargarLinieaNegocioTP = 18;
    private final int modificarSolicitud = 19;
    private final int cargarCartera = 20;
    private final int cargarContratistas = 21;
    private final int asociarContratistaSolicitud = 22;
    private final int cargarAccionesAsociadas = 23;
    private final int cargarTipoTrabajo = 24;
    private final int cargarAlcanceAccion = 25;
    private final int cargarListaApu = 26;
    private final int cargarApuAsociados = 27;
    private final int validaExisteNic = 28;
    private final int cargarClientesAll = 29;
    private final int getClienteById = 30;
    private final int asociarApuAccion = 31;
    private final int desasociarApuAccion = 32;
    private final int asignarContratista = 33;
    private final int asignarAlcances = 34;
    private final int actualizarCartera = 35;
    private final int cargarSolicitudesIdcli = 36;
    private final int CARGAR_LINEA_NEGOCIO = 37;
    private final int CARGAR_ESTADO_CARTERA = 38;
    private final int CARGAR_INFO_SOLICITUDES = 39;
    private final int CARGAR_ETAPAS = 40;
    private final int CARGAR_ESTADOS_ETAPAS = 41;
    private final int CAMBIAR_ESTADO_ETAPA = 42;
    private final int cargarResponsable = 43;
    private final int CARGAR_REPORTE_FATURACION = 44;
    private final int cargarAccionPrincipal = 46;
    private final int cargarCotizacion = 47;
    private final int CARGAR_FACTURAS_PARCIALES = 48;
    private final int guardar_coste_Proyecto = 49;
    private final int CARGAR_FACTURAS_VENTA = 50;
    private final int CARGAR_COTIZACION_FAC = 51;
    private final int GUARDAR_FACTURACION = 52;
    private final int ENVIAR_FACT_CLIENTE = 53;
    private final int ELIMINAR_FACT_CLIENTE = 54;
    private final int ANULAR_FACTACTURA_PARCIAL = 55;
    private final int CARGAR_ARCHIVO = 56;
    private final int LISTAR_ARCHIVOS_CARGADOS = 57;
    private final int ELIMINAR_ARCHIVO_CARGADO = 58;
    private final int ABRIR_ARCHIVO = 59;
    private final int CARGAR_INSUMOS_SOLICITUD = 60;
    private final int CARGAR_ANTICIPOS = 62;
    private final int GUARDAR_ANTICIPOS = 63;
    private final int GENERAR_PDF_CUENTAS_COBRO = 64;
    private final int BUSCAR_ACCION_CAMBIO_COTIZACION = 65;
    private final int GUARDAR_CAMBIOS_COTIZACION_TEMP = 66;
    private final int BUSCAR_COTIZACION_TEM = 67;
    private final int CARGAR_HISTORICO_COTIZACION = 68;
    private final int CREAR_HISTORICO_COTIZACION = 69;
    private final int CARGAR_AREAS_HISTORICO_COTIZACION = 70;
    private final int CARGAR_DISCIPLINAS_HISTORICO_COTIZACION = 71;
    private final int CARGAR_CAPITULOS_HISTORICO_COTIZACION = 72;
    private final int CARGAR_ACTIVIDADES_HISTORICO_COTIZACION = 73;
    private final int OBTENER_ID_REL_ACTIVIDADES_APU = 74;
    private final int SET_RENTABILIDAD_GLOBAL = 75;
    private final int INSERTAR_CABECERA_TABLA_TEMPORAL = 76;
    private final int USUARIO_ADMIN = 77;
    private final int AUTOCOMPLETAR = 78;
    private final int UPDATE_PRESUPUESTO_TERMINADO = 79;
    private final int GENERAR_OT = 80;
    private final int CARGAR_TRAZABILIDAD_OFERTA = 81;
    private final int CARGAR_INFORMACION_MODALIDAD_PROYECTO = 82;
    private final int ACTUALIZAR_INFORMACION_MODALIDAD_PROYECTO = 83;
    private final int CARGAR_TABLAS_MODULO_IMPRESION_OFERTA = 84;
    private final int GENERAR_PDF_COTIZACION = 85;
    private final int EXPORTAR_EXCEL = 86;
    private final int CALCULAR_IVA_COMPENSAR = 87;
    private final int CARGAR_REPORTE_ANTICIPO_MS = 88;
    private final int EXPORTAR_EXCEL_REPORTE_ANTICIPO_MS = 89;
    private final int GENERAR_PDF_PRESUPUESTO_DETALLE = 90;
    private final int GUARDAR_CONDIONES_COMERIALES = 91;
    private final int CARGAR_CONDICIONES_COMERCIALES = 92;
    private final int CARGAR_ACCIONES_PREPARADAS = 93;
    private final int CARGAR_IVA_COMPENSAR = 94;
    private final int CARGAR_CIA = 95;
    private final int CARGAR_REPORTE_ANTICIPOS = 96;
    private final int EXPORTAREXCELREPORTEANTICIPO = 97;
    private final int GENERAR_CENTRO_COSTO = 98;
    private final int CARGARCASOSANTICIPO = 99;
    private final int GENERAR_CENTRO_COSTO_LOTES = 100;
    private final int GUARDAR_NOTAS_OFERTA = 101;
    private final int CARGAR_COMBO_GENERICO = 102;
    private final int CARGAR_NOTAS_OFERTA = 103;
    private final int ELIMINAR_NOTAS_OFERTA = 104;
    private final int EDITAR_NOTAS_OFERTA = 105;
    private final int CLONACIONPROYECTOS = 106;



    private ProcesosClienteDAO dao;
    Usuario usuario = null;
    String reponseJson = "{}";

    @Override
    public void run() throws ServletException, InformationException {
        try {

            HttpSession session = request.getSession();
            usuario = (Usuario) session.getAttribute("Usuario");
            dao = new ProcesosClienteImpl(usuario.getBd());
            int opcion = (request.getParameter("opcion") != null)
                    ? Integer.parseInt(request.getParameter("opcion"))
                    : -1;
            switch (opcion) {
                case cargarTiposClientes:
                    this.cargarTiposClientes();
                    break;
                case cargarDepartamentos:
                    this.cargarDepartamentos();
                    break;
                case cargarCiudades:
                    this.cargarCiudades();
                    break;
                case CrearCliente:
                    this.crearCliente();
                    break;
                case cargarClientesPadre:
                    this.cargarClientesPadre();
                    break;
                case cargarNics:
                    this.cargarNics();
                    break;
                case cargarClienteId:
                    this.cargarClienteId();
                    break;
                case cargarPadreId:
                    this.cargarPadreId();
                    break;
                case cargarDepCiuid:
                    this.cargarDepCiuid();
                    break;
                case obteneridclientenit:
                    this.obteneridclientenit();
                    break;
                case modificarCliente:
                    this.modificarCliente();
                    break;
                case CargarNics:
                    this.CargarNics();
                    break;
                case cargarListadoLineaDeNegocio:
                    this.cargarListadoLineaDeNegocio();
                    break;
                case cargarListadoTipoSolicitud:
                    this.cargarListadoTipoSolicitud();
                    break;
                case cargarListadoResponsable:
                    this.cargarListadoResponsable();
                    break;
                case cargarListadoInterventor:
                    this.cargarListadoInterventor();
                    break;
                case crearSolicitud:
                    this.crearSolicitud();
                    break;
                case cargarSolicitud:
                    this.cargarSolicitud();
                    break;
                case cargarLinieaNegocioTP:
                    this.cargarLinieaNegocioTP();
                    break;

                case modificarSolicitud:
                    this.modificarSolicitud();
                    break;
                case cargarCartera:
                    this.cargarCartera();
                    break;
                case cargarContratistas:
                    this.cargarContratistas();
                    break;
                case asociarContratistaSolicitud:
                    this.asociarContratistaSolicitud();
                    break;
                case cargarAccionesAsociadas:
                    this.cargarAccionesAsociadas();
                    break;
                case cargarTipoTrabajo:
                    this.cargarTipoTrabajo();
                    break;
                case cargarAlcanceAccion:
                    this.cargarAlcanceAccion();
                    break;
                case cargarListaApu:
                    this.cargarListaApu();
                    break;
                case cargarApuAsociados:
                    this.cargarApuAsociados();
                    break;
                case validaExisteNic:
                    this.existeNic();
                    break;
                case cargarClientesAll:
                    this.cargarClientesAll();
                    break;
                case getClienteById:
                    this.getInfoClienteById();
                    break;
                case asociarApuAccion:
                    this.asociarApuAccion();
                    break;
                case desasociarApuAccion:
                    this.desasociarApuAccion();
                    break;
                case asignarContratista:
                    this.asignarContratista();
                    break;
                case asignarAlcances:
                    this.asignarAlcances();
                    break;
                case actualizarCartera:
                    this.actualizarCartera();
                    break;
                case cargarSolicitudesIdcli:
                    this.cargarSolicitudesIdcli();
                    break;
                case CARGAR_LINEA_NEGOCIO:
                    cargarLineaNegocio();
                    break;
                case CARGAR_ESTADO_CARTERA:
                    cargarEstadoCartera();
                    break;
                case CARGAR_INFO_SOLICITUDES:
                    cargarInfoSolicitudes();
                    break;
                case CARGAR_ETAPAS:
                    cargarEtapas();
                    break;
                case CARGAR_ESTADOS_ETAPAS:
                    cargarEstadoEtapas();
                    break;
                case CAMBIAR_ESTADO_ETAPA:
                    cambiarEstadoEtapaOferta();
                    break;
                case cargarResponsable:
                    cargarResponsable();
                    break;
                case CARGAR_REPORTE_FATURACION:
                    this.cargarReporteFacturacion();
                    break;
                case cargarAccionPrincipal:
                    this.cargarAccionPrincipal();
                    break;
                case cargarCotizacion:
                    this.cargarCotizacion();
                    break;
                case CARGAR_FACTURAS_PARCIALES:
                    this.listarFacturasParciales();
                    break;
                case guardar_coste_Proyecto:
                    this.guardar_coste_Proyecto();
                    break;
                case CARGAR_FACTURAS_VENTA:
                    cargarFacturasVenta();
                    break;
                case CARGAR_COTIZACION_FAC:
                    cargarCotizacionFac();
                    break;
                case GUARDAR_FACTURACION:
                    guardarFacturacion();
                    break;
                case ENVIAR_FACT_CLIENTE:
                    procesoEnviarFacturacliente();
                    break;
                case ELIMINAR_FACT_CLIENTE:
                    eliminarFacturacliente();
                    break;
                case ANULAR_FACTACTURA_PARCIAL:
                    anular_factura_parcial();
                    break;
                case CARGAR_ARCHIVO:
                    cargar_archivo();
                    break;
                case LISTAR_ARCHIVOS_CARGADOS:
                    listar_archivos_cargados();
                    break;
                case ELIMINAR_ARCHIVO_CARGADO:
                    eliminar_archivo_cargado();
                    break;
                case ABRIR_ARCHIVO:
                    almacenarArchivoEnCarpetaUsuario();
                    break;
                case CARGAR_INSUMOS_SOLICITUD:
                    cargar_Insumos_Solicitud();
                    break;
                case CARGAR_HISTORICO_COTIZACION:
                    cargar_Historico_Cotizacion();
                    break;
                case CREAR_HISTORICO_COTIZACION:
                    crear_Historico_Cotizacion();
                    break;
                case CARGAR_AREAS_HISTORICO_COTIZACION:
                    cargar_Areas_Historico_cotizacion();
                    break;
                case CARGAR_DISCIPLINAS_HISTORICO_COTIZACION:
                    cargar_Disciplinas_Historico_Cotizacion();
                    break;
                case CARGAR_ACTIVIDADES_HISTORICO_COTIZACION:
                    cargar_Actividades_Historico_Cotizacion();
                    break;
                case CARGAR_ANTICIPOS:
                    cargarAnticipos();
                    break;
                case GUARDAR_ANTICIPOS:
                    guardarAnticipo();
                    break;
                case GENERAR_PDF_CUENTAS_COBRO:
                    exportarPdfCuentaCobro();
                    break;
                case CARGAR_CAPITULOS_HISTORICO_COTIZACION:
                    cargar_Capitulos_Historico_cotizacion();
                    break;
                case OBTENER_ID_REL_ACTIVIDADES_APU:
                    obtener_id_rel_actividades_apu();
                    break;
                case SET_RENTABILIDAD_GLOBAL:
                    set_Rentabilidad_Global();
                    break;
                case BUSCAR_ACCION_CAMBIO_COTIZACION:
                    buscarAccion();
                    break;
                case GUARDAR_CAMBIOS_COTIZACION_TEMP:
                    guardarCambioCotizacion();
                    break;
                case BUSCAR_COTIZACION_TEM:
                    buscarCotizacionTem();
                    break;
                case INSERTAR_CABECERA_TABLA_TEMPORAL:
                    insertar_cabecera_tabla_temporal();
                    break;
                case USUARIO_ADMIN:
                    usuario_admin();
                    break;
                case AUTOCOMPLETAR:
                    autocompletar();
                    break;
                case UPDATE_PRESUPUESTO_TERMINADO:
                    update_Presupuesto_Terminado();
                    break;
                case GENERAR_OT:
                    generar_OT();
                    break;
                case CARGAR_TRAZABILIDAD_OFERTA:
                    cargarTrazabilidadOferta();
                    break;
                case CARGAR_INFORMACION_MODALIDAD_PROYECTO:
                    cargarInformacionModalidadProyecto();
                    break;
                case ACTUALIZAR_INFORMACION_MODALIDAD_PROYECTO:
                    guardarInformacionModalidadProyecto();
                    break;
                case CARGAR_TABLAS_MODULO_IMPRESION_OFERTA:
                    cargar_tablas_modulo_impresion_oferta();
                    break;
                case GENERAR_PDF_COTIZACION:
                    generar_Pdf_Cotizacion();
                    break;
                case EXPORTAR_EXCEL:
                    exportarExcel();
                    break;
                case CALCULAR_IVA_COMPENSAR:
                    calcular_iva_compensar();
                    break;
                case CARGAR_REPORTE_ANTICIPO_MS:
                    cargarReporteAnticiposMs();
                    break;
                case EXPORTAR_EXCEL_REPORTE_ANTICIPO_MS:
                    exportarExcelReporteAnticiposMs();
                    break;
                case GENERAR_PDF_PRESUPUESTO_DETALLE:
                    generar_Pdf_Presupuesto_Detalle();
                    break;
                case GUARDAR_CONDIONES_COMERIALES:
                    guardarCondionesComerciales();
                    break;
                case CARGAR_CONDICIONES_COMERCIALES:
                    cargarCondionesComerciales();
                    break;
                case CARGAR_ACCIONES_PREPARADAS:
                    cargarAccionesPreparadas();
                    break;
                case CARGAR_IVA_COMPENSAR:
                    cargar_Iva_Compensar();
                    break;
                case CARGAR_CIA:
                    cargar_CIA();
                    break;
                case CARGAR_REPORTE_ANTICIPOS:
                    cargarReporteAnticipos();
                    break;
                case EXPORTAREXCELREPORTEANTICIPO:
                    exportarexcelreporteanticipo();
                    break;
                case GENERAR_CENTRO_COSTO:
                    generar_centro_costo();
                    break;
                case CARGARCASOSANTICIPO:
                    cargarCasosAnticipo();
                    break;
                case GENERAR_CENTRO_COSTO_LOTES:
                    generar_centro_costo_lotes();
                    break;
                case GUARDAR_NOTAS_OFERTA:
                    guardar_notas_oferta();
                    break;
                case CARGAR_COMBO_GENERICO:
                    this.cargarComboGenerico();
                    break ;
                    
                case CARGAR_NOTAS_OFERTA:
                    this.obtenerNotasOferta();
                break;
                
                case ELIMINAR_NOTAS_OFERTA:
                    this.eliminar_notas_oferta();
                    break;
                    
                case EDITAR_NOTAS_OFERTA:
                    this.editar_notas_oferta();
                    break;
                    
                case CLONACIONPROYECTOS:
                    this.clonacionProyectos();
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void cargarTiposClientes() {
        try {
            String json = dao.cargarTiposClientes();
            this.printlnResponseAjax(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void cargarDepartamentos() {
        try {
            String json = dao.cargarDepartamentos();
            this.printlnResponseAjax(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void cargarCiudades() {
        try {
            String json = dao.cargarCiudades(Util.setCodificacionCadena(request.getParameter("Departamento"), "ISO-8859-1"));
            this.printlnResponseAjax(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void crearCliente() {
        try {
            Cliente cliente = new Cliente();

            cliente.setNomcli(Util.setCodificacionCadena(request.getParameter("NombreCliente"), "ISO-8859-1"));
            cliente.setNit(Util.setCodificacionCadena(request.getParameter("NitCliente"), "ISO-8859-1"));
            cliente.setNic(request.getParameter("Nic"));
            cliente.setCiudad(Util.setCodificacionCadena(request.getParameter("Ciudad"), "ISO-8859-1"));
            cliente.setDireccion(Util.setCodificacionCadena(request.getParameter("DireccionCliente"), "ISO-8859-1"));
            cliente.setNomContacto(Util.setCodificacionCadena(request.getParameter("NombreContacto"), "ISO-8859-1"));
            cliente.setTelContacto(request.getParameter("TelefonoContacto"));
            cliente.setCelContacto(request.getParameter("CelularContacto"));
            cliente.setEmail(Util.setCodificacionCadena(request.getParameter("EmailContacto"), "ISO-8859-1"));
            cliente.setCargoContacto(Util.setCodificacionCadena(request.getParameter("CargoContacto"), "ISO-8859-1"));
            cliente.setNomRepresentante(Util.setCodificacionCadena(request.getParameter("NombreRepresentateLegal"), "ISO-8859-1"));
            cliente.setTelRepresentante(request.getParameter("TelRepresentateLegal"));
            cliente.setCelRepresentante(request.getParameter("CelRepresentateLegal"));
            cliente.setEmailrepresentantelega(Util.setCodificacionCadena(request.getParameter("EmailRepresentateLegal"), "ISO-8859-1"));
            cliente.setId_padre(request.getParameter("idClientePadre"));
            cliente.setClasificacion(Util.setCodificacionCadena(request.getParameter("Clasificacion"), "ISO-8859-1"));
            cliente.setDigito_verificacion(request.getParameter("DigitoVerificacion"));
            cliente.setTipo(request.getParameter("tipoCliente"));
            
            

            JsonObject info = (JsonObject) (new JsonParser()).parse(request.getParameter("nics"));

            String json = dao.crearCliente(cliente, usuario, info);
            this.printlnResponseAjax(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void cargarClientesPadre() {
        try {
            String q = request.getParameter("q") != null ? request.getParameter("q") : "";
            String json = dao.cargarClientesPadre(q);
            this.printlnResponseAjax(json, "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    private void cargarNics() {
        try {
            ArrayList<BeanGeneral> lista = dao.cargarNics2();
            Gson gson = new Gson();
            String json = "{\"page\":1,\"rows\":" + gson.toJson(lista) + "}";
            this.printlnResponseAjax(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void cargarClienteId() {
        try {
            String json = dao.cargarClienteId(request.getParameter("idcliente"));
            this.printlnResponseAjax(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void cargarPadreId() {
        try {
            String json = dao.cargarPadreId(request.getParameter("idClientePadre"));
            this.printlnResponseAjax(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void cargarDepCiuid() {
        try {
            String json = dao.cargarDepCiuid(request.getParameter("idCiudad"));
            this.printlnResponseAjax(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void obteneridclientenit() {
        try {
            String json = dao.obteneridclientenit(Util.setCodificacionCadena(request.getParameter("busqueda"), "ISO-8859-1"));
            this.printlnResponseAjax(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void modificarCliente() {
        try {
            Cliente cliente = new Cliente();

            cliente.setNomcli(Util.setCodificacionCadena(request.getParameter("NombreCliente"), "ISO-8859-1"));
            cliente.setNit(request.getParameter("NitCliente"));
            cliente.setNic(request.getParameter("Nic"));
            cliente.setCiudad(Util.setCodificacionCadena(request.getParameter("Ciudad"), "ISO-8859-1"));
            cliente.setDireccion(Util.setCodificacionCadena(request.getParameter("DireccionCliente"), "ISO-8859-1"));
            cliente.setNomContacto(Util.setCodificacionCadena(request.getParameter("NombreContacto"), "ISO-8859-1"));
            cliente.setTelContacto(request.getParameter("TelefonoContacto"));
            cliente.setCelContacto(request.getParameter("CelularContacto"));
            cliente.setEmail(Util.setCodificacionCadena(request.getParameter("EmailContacto"), "ISO-8859-1"));
            cliente.setCargoContacto(Util.setCodificacionCadena(request.getParameter("CargoContacto"), "ISO-8859-1"));
            cliente.setNomRepresentante(Util.setCodificacionCadena(request.getParameter("NombreRepresentateLegal"), "ISO-8859-1"));
            cliente.setTelRepresentante(request.getParameter("TelRepresentateLegal"));
            cliente.setCelRepresentante(request.getParameter("CelRepresentateLegal"));
            cliente.setEmailrepresentantelega(Util.setCodificacionCadena(request.getParameter("EmailRepresentanteLegal"), "ISO-8859-1"));
            cliente.setId_padre(request.getParameter("idClientePadre"));
            //cliente.setPadre(request.getParameter("padre"));
            cliente.setCodcli(request.getParameter("idcliente"));
            cliente.setDigito_verificacion(request.getParameter("DigitoVerificacion"));

            JsonObject info = (JsonObject) (new JsonParser()).parse(request.getParameter("nics"));
            String json = dao.modificarCliente(cliente, usuario, info);
            this.printlnResponseAjax(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void CargarNics() {
        try {
            String lista = dao.CargarNics(request.getParameter("idcliente"));
            String json = "{\"page\":1,\"rows\":" + lista + "}";
            this.printlnResponseAjax(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void cargarListadoLineaDeNegocio() {
        try {
            String json = dao.cargarListadoLineaDeNegocio();
            this.printlnResponseAjax(json, "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void cargarListadoTipoSolicitud() {
        try {
            String q = request.getParameter("q") != null ? request.getParameter("q") : "";
            String codproyecto = request.getParameter("idLineaDeNegocio") != null ? request.getParameter("idLineaDeNegocio") : "";
            String json = dao.cargarListadoTipoSolicitud(q, codproyecto);
            this.printlnResponseAjax(json, "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void cargarListadoResponsable() {
        try {
            String json = dao.cargarListadoResponsable();
            this.printlnResponseAjax(json, "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void cargarListadoInterventor() {
        try {
            String json = dao.cargarListadoInterventor();
            this.printlnResponseAjax(json, "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void crearSolicitud() {

        try {
            OfertaElca ofeca = new OfertaElca();
            ofeca.setId_cliente(request.getParameter("idcliente"));
            ofeca.setNic(request.getParameter("nic"));
            ofeca.setDescripcion(URLDecoder.decode(request.getParameter("descripcion"), "UTF-8"));
//            ofeca.setTipo_solicitud(request.getParameter("tiposolicitud"));
            ofeca.setOficial("0");
            ofeca.setAviso(request.getParameter("inpAviso") != null ? request.getParameter("inpAviso") : "");
            String opd = request.getParameter("opd") != null ? request.getParameter("opd") : "";

            String fecha_actual = "";
            String estado_cartera = "";
            String fecha_limite = request.getParameter("fecha_limite") != null ? request.getParameter("fecha_limite") : "";
            ofeca.setFecha_limite(fecha_limite);
            
            String responsable = request.getParameter("responsable") != null ? URLDecoder.decode(request.getParameter("responsable"), "UTF-8") : "";
            String interventor = request.getParameter("interventor") != null ? URLDecoder.decode(request.getParameter("interventor"), "UTF-8") : "";
            ofeca.setResponsable(responsable);
            ofeca.setInterventor(interventor);

            ofeca.setEstado_cartera(estado_cartera);
            ofeca.setFec_val_cartera("0099-01-09 00:00:00");
            fecha_actual = "Sin Fecha";

            ofeca.setEstado_cartera("Estudio");  
            ofeca.setNomproyecto(URLDecoder.decode(request.getParameter("nomproyecto"), "UTF-8"));
            
            ofeca.setTipo_negocio(request.getParameter("tipoNegocio") != null ? URLDecoder.decode(request.getParameter("tipoNegocio"), "UTF-8") : "" );
            ofeca.setTipo_trabajo(request.getParameter("tipoTrabajo") != null ? URLDecoder.decode(request.getParameter("tipoTrabajo"), "UTF-8") : "");

            String json = dao.crearSolicitud(ofeca, usuario, request.getParameter("enviar"));
            this.printlnResponseAjax(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void cargarSolicitud() {
        try {
            String json = dao.cargarSolicitud(Util.setCodificacionCadena(request.getParameter("busqueda"), "ISO-8859-1"));
            this.printlnResponseAjax(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void cargarLinieaNegocioTP() {
        try {
            String json = dao.cargarLinieaNegocioTS(Util.setCodificacionCadena(request.getParameter("tiposolicitud"), "ISO-8859-1"));
            this.printlnResponseAjax(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void modificarSolicitud() {
        try {
            OfertaElca ofeca = new OfertaElca();
            ofeca.setId_solicitud(request.getParameter("idSolicitud"));
            ofeca.setId_cliente(request.getParameter("idcliente"));
            ofeca.setNic(request.getParameter("nic"));
            ofeca.setDescripcion(Util.setCodificacionCadena(request.getParameter("descripcion"), "ISO-8859-1"));
            ofeca.setLineanegocio(Util.setCodificacionCadena(request.getParameter("lineanegocio"), "ISO-8859-1"));
            //ofeca.setTipo_solicitud(request.getParameter("tiposolicitud"));
            ofeca.setAviso(request.getParameter("inpAviso") != null ? request.getParameter("inpAviso") : "");
            String opd = request.getParameter("opd") != null ? request.getParameter("opd") : "";

            String fecha_actual = "";
            String estado_cartera = "";

            String responsable = request.getParameter("responsable") != null ? Util.setCodificacionCadena(request.getParameter("responsable"), "ISO-8859-1") : "";
            String interventor = request.getParameter("interventor") != null ? Util.setCodificacionCadena(request.getParameter("interventor"), "ISO-8859-1") : "";
            ofeca.setResponsable(responsable);
            ofeca.setInterventor(interventor);

            ofeca.setEstado_cartera(request.getParameter("estadoCartera"));

            ofeca.setFec_val_cartera("0099-01-09 00:00:00");
            fecha_actual = "Sin Fecha";

//            if (request.getParameter("tiposolicitud").equals("Emergencia")) {
//                ofeca.setEstado_cartera("010");
//                opd = "";
//            } else {
//                ofeca.setEstado_cartera(request.getParameter("estadoCartera"));
//
//            }
            ofeca.setAviso(opd);

            String json = dao.modificarSolicitud(ofeca, usuario);
            this.printlnResponseAjax(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void cargarCartera() {
        try {
            String json = dao.cargarCartera();
            this.printlnResponseAjax(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void cargarContratistas() {
        try {
            String lista = dao.cargarContratistas(request.getParameter("ts"));
            String json = "{\"page\":1,\"rows\":" + lista + "}";
            this.printlnResponseAjax(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void cargarListaApu() {
        try {
            String lista = dao.cargarListaApu(request.getParameter("idaccion"));
            String json = "{\"page\":1,\"rows\":" + lista + "}";
            this.printlnResponseAjax(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void asociarContratistaSolicitud() {
        try {
            AccionesEca acceca = new AccionesEca();
            acceca.setId_solicitud(request.getParameter("idSolicitud"));
            acceca.setContratista(request.getParameter("idcontratista"));
            acceca.setDescripcion(Util.setCodificacionCadena(request.getParameter("descripcion"), "ISO-8859-1"));
            acceca.setTipo_trabajo(request.getParameter("idtipotrabajo"));

            String json = dao.asociarContratistaSolicitud(acceca, usuario);
            this.printlnResponseAjax(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void cargarAccionesAsociadas() {
        try {
            String lista = dao.cargarAccionesAsociadas(request.getParameter("idsolicitud"));
            String json = "{\"page\":1,\"rows\":" + lista + "}";
            this.printlnResponseAjax(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void cargarTipoTrabajo() {
        try {
            String json = dao.cargarTipoTrabajo(request.getParameter("tiposolicitud"));
            this.printlnResponseAjax(json, "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void cargarAlcanceAccion() {
        try {
            String json = dao.cargarAlcanceAccion(request.getParameter("idaccion"), request.getParameter("idsolicitud"));
            this.printlnResponseAjax(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void cargarApuAsociados() {
        try {
            String lista = dao.cargarApuAsociados(request.getParameter("idaccion"));
            String json = "{\"page\":1,\"rows\":" + lista + "}";
            this.printlnResponseAjax(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void asociarApuAccion() {
        try {
            String json = dao.asociarApuAccion(request.getParameter("idaccion"), request.getParameter("listado"), usuario);
            this.printlnResponseAjax(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void desasociarApuAccion() {
        try {
            String json = dao.desasociarApuAccion(request.getParameter("idaccion"), request.getParameter("listado"), usuario);
            this.printlnResponseAjax(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void existeNic() {
        try {
            String json = "{}";
            String nic = request.getParameter("valor_nic") != null ? request.getParameter("valor_nic") : "";
            if (dao.VerificaNic(nic)) {
                json = "{\"respuesta\":\"SI\"}";
                this.printlnResponseAjax(json, "application/json;");
            } else {
                json = "{\"respuesta\":\"NO\"}";
                this.printlnResponseAjax(json, "application/json;");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void cargarClientesAll() {
        try {
            String q = request.getParameter("q") != null ? request.getParameter("q") : "";
            String json = dao.cargarClientesAll(q);
            this.printlnResponseAjax(json, "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    private void getInfoClienteById() {
        try {
            String json = dao.getInfoClienteById(request.getParameter("idcliente"));
            this.printlnResponseAjax(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void asignarContratista() {
        try {
            String idcontratista = request.getParameter("idcontratista");
            String idaccion = request.getParameter("idaccion");
            String nomcontratista = request.getParameter("nomcontratista");
            String json = dao.asignarContratista(idaccion, idcontratista, usuario, nomcontratista);
            this.printlnResponseAjax(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void asignarAlcances() {
        try {
            OfertaElca ofeca = new OfertaElca();
            ofeca.setId_solicitud(request.getParameter("idSolicitud"));
            ofeca.setId_cliente(request.getParameter("idcliente"));
            ofeca.setNic(request.getParameter("nic"));
            ofeca.setDescripcion(Util.setCodificacionCadena(request.getParameter("descripcion"), "ISO-8859-1"));
            ofeca.setTipo_solicitud(request.getParameter("tiposolicitud"));
            ofeca.setAviso(request.getParameter("inpAviso") != null ? request.getParameter("inpAviso") : "");
            String opd = request.getParameter("opd") != null ? request.getParameter("opd") : "";

            String fecha_actual = "";
            String estado_cartera = "";

            String responsable = request.getParameter("responsable") != null ? Util.setCodificacionCadena(request.getParameter("responsable"), "ISO-8859-1") : "";
            String interventor = request.getParameter("interventor") != null ? Util.setCodificacionCadena(request.getParameter("interventor"), "ISO-8859-1") : "";
            ofeca.setResponsable(responsable);
            ofeca.setInterventor(interventor);

            ofeca.setEstado_cartera(request.getParameter("estadoCartera"));

            ofeca.setFec_val_cartera("0099-01-09 00:00:00");
            fecha_actual = "Sin Fecha";

            if (request.getParameter("tiposolicitud").equals("Emergencia")) {
                ofeca.setEstado_cartera("010");
                opd = "";
            } else {
                ofeca.setEstado_cartera(request.getParameter("estadoCartera"));

            }
            ofeca.setAviso(opd);

            String json = dao.modificarSolicitud(ofeca, usuario);
            this.printlnResponseAjax(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void actualizarCartera() {
        try {
            OfertaElca ofeca = new OfertaElca();
            ofeca.setId_solicitud(request.getParameter("idSolicitud"));
            ofeca.setId_cliente(request.getParameter("idcliente"));
            ofeca.setNic(request.getParameter("nic"));
            ofeca.setDescripcion(Util.setCodificacionCadena(request.getParameter("descripcion"), "ISO-8859-1"));
//            ofeca.setTipo_solicitud(request.getParameter("tiposolicitud"));
            ofeca.setAviso(request.getParameter("inpAviso") != null ? request.getParameter("inpAviso") : "");
            String opd = request.getParameter("opd") != null ? request.getParameter("opd") : "";

            //String responsable = request.getParameter("responsable") != null ? request.getParameter("responsable") : "";
            String interventor = request.getParameter("interventor") != null ? Util.setCodificacionCadena(request.getParameter("interventor"), "ISO-8859-1") : "";
            //ofeca.setResponsable(responsable);
            ofeca.setInterventor(interventor);

            ofeca.setEstado_cartera(request.getParameter("estadoCartera"));

//            if (request.getParameter("tiposolicitud").equals("Emergencia")) {
//                ofeca.setEstado_cartera("010");
//                opd = "";
//            } else {
//                ofeca.setEstado_cartera(request.getParameter("estadoCartera"));
//
//            }
            ofeca.setAviso(opd);

            String json = dao.actualizarCartera(ofeca, usuario);
            this.printlnResponseAjax(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void cargarSolicitudesIdcli() {
        try {
            String lista = dao.cargarSolicitudesIdcli(request.getParameter("idcli"));
            String json = "{\"page\":1,\"rows\":" + lista + "}";
            this.printlnResponseAjax(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void cargarLineaNegocio() {
        try {
            this.printlnResponseAjax(dao.cargarLineaNegocio(), "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public void cargarCasosAnticipo() {
        try {
            this.printlnResponseAjax(dao.cargarCasosAnticipo(), "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void cargarEstadoCartera() {
        try {
            this.printlnResponseAjax(dao.cargarEstadoCartera(), "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void cargarInfoSolicitudes() {
        try {
            String lineaNegocio = request.getParameter("lineaNegocio") == null ? "" : request.getParameter("lineaNegocio");
            String responsable = request.getParameter("responsable") == null ? "" : Util.setCodificacionCadena(request.getParameter("responsable"), "ISO-8859-1");
            String solicitud = request.getParameter("solicitud") == null ? "" : request.getParameter("solicitud");
            String estadoCartera = request.getParameter("estadoCartera") == null ? "" : request.getParameter("estadoCartera");
            String fechaInicio = request.getParameter("fechaInicio") == null ? "" : request.getParameter("fechaInicio");
            String fechafin = request.getParameter("fechafin") == null ? "" : request.getParameter("fechafin");
            String trazabilidad = request.getParameter("trazabilidad") == null ? "" : request.getParameter("trazabilidad");
            String etapaActual = request.getParameter("etapaActual") == null ? "" : request.getParameter("etapaActual");
            String tipo_proyecto = request.getParameter("tipo_proyecto") == null ? "" : request.getParameter("tipo_proyecto");
            String foms = request.getParameter("foms") == null ? "" : request.getParameter("foms");
            String id_cliente = request.getParameter("id_cliente") == null ? "" : request.getParameter("id_cliente");
            String nom_proyecto = request.getParameter("nom_proyecto") == null ? "" : request.getParameter("nom_proyecto");
            
            String obj=dao.cargarInfoSolicitudes(lineaNegocio, responsable, solicitud, estadoCartera, fechaInicio, fechafin, trazabilidad, etapaActual, tipo_proyecto, foms, id_cliente, nom_proyecto);
            
            this.printlnResponseAjax(obj, "application/json;");
        } catch (Exception ex) {

            ex.printStackTrace();
        }
    }

    public void cargarEtapas() {
        try {
            String etapa = request.getParameter("etapa") == null ? "" : Util.setCodificacionCadena(request.getParameter("etapa"), "ISO-8859-1");
            this.printlnResponseAjax(dao.cargarEtapas(etapa), "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void cargarEstadoEtapas() {
        try {
            String etapa = request.getParameter("etapa") == null ? "" : Util.setCodificacionCadena(request.getParameter("etapa"), "ISO-8859-1");
            String id_estado = request.getParameter("id_estado") == null ? "" : request.getParameter("id_estado");
            this.printlnResponseAjax(dao.cargarEstadoEtapas(etapa, id_estado), "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void cambiarEstadoEtapaOferta() {
        String json = "{\"respuesta\":\"Guardado\"}";
        try {
            String etapa = (request.getParameter("etapa") != null) ? request.getParameter("etapa") : "";
            String estado_actual = (request.getParameter("estado_actual") != null) ? request.getParameter("estado_actual") : "";
            String estado = (request.getParameter("estados") != null) ? request.getParameter("estados") : "";
            String causal = (request.getParameter("causal") != null) ? Util.setCodificacionCadena(request.getParameter("causal"), "ISO-8859-1") : "";
            String observacion = (request.getParameter("observacion") != null) ? Util.setCodificacionCadena(request.getParameter("observacion"), "ISO-8859-1") : "";
            String idsolicitud = (request.getParameter("idsolicitud") != null) ? request.getParameter("idsolicitud") : "";
            TransaccionService tService = new TransaccionService(this.usuario.getBd());
            tService.crearStatement();
            tService.getSt().addBatch(dao.trazabilidadOfertafin(idsolicitud, usuario, estado_actual, estado));
            tService.getSt().addBatch(dao.trazabilidadOferta(idsolicitud, usuario, etapa, estado, causal, observacion));
            tService.getSt().addBatch(dao.cambioEstadoEtapaOferta(idsolicitud, usuario, estado_actual, estado));
            tService.execute();
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    public void cargarReporteFacturacion() {
        try {
            String num_solicitud = (request.getParameter("id_solicitud") != null) ? request.getParameter("id_solicitud") : "";
            this.printlnResponseAjax(dao.cargarReporteFacturacion(num_solicitud), "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void cargarResponsable() {
        try {
            this.printlnResponseAjax(new Gson().toJson(usuario), "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void cargarAccionPrincipal() {
        try {
            String lista = dao.cargarAccionPrincipal(request.getParameter("idsolicitud"));
            String json = "{\"page\":1,\"rows\":" + lista + "}";
            this.printlnResponseAjax(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void cargarCotizacion() {
        try {
            String json = dao.cargarCotizacion(request.getParameter("idaccion"));
            this.printlnResponseAjax(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void listarFacturasParciales() {
        try {
            String num_solicitud = (request.getParameter("num_solicitud") != null) ? request.getParameter("num_solicitud") : "";
            String json = dao.cargarFacturasParciales(num_solicitud);
            this.printlnResponseAjax(json, "application/json;");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void guardar_coste_Proyecto() {
        try {
            CotizacionSl cotizacion = new CotizacionSl();
            cotizacion.setId_accion(request.getParameter("id_accion"));
            cotizacion.setVigencia_cotizacion(request.getParameter("fecha"));
            cotizacion.setForma_visualizacion(request.getParameter("visualizacion"));
            cotizacion.setModalidad_comercial(request.getParameter("modalidad"));
            cotizacion.setValor_cotizacion(Double.parseDouble(request.getParameter("valcotizacion")));
            cotizacion.setValor_descuento(Double.parseDouble(request.getParameter("valdesc")));
            cotizacion.setSubtotal(Double.parseDouble(request.getParameter("subtotal")));
            cotizacion.setPerc_iva(Double.parseDouble(request.getParameter("perc_iva")));
            cotizacion.setValor_iva(Double.parseDouble(request.getParameter("valiva")));
            cotizacion.setPerc_administracion(Double.parseDouble(request.getParameter("perc_admon")));
            cotizacion.setAdministracion(Double.parseDouble(request.getParameter("val_admon")));
            cotizacion.setPerc_imprevisto(Double.parseDouble(request.getParameter("perc_imprevisto")));
            cotizacion.setImprevisto(Double.parseDouble(request.getParameter("val_imprevisto")));
            cotizacion.setPerc_utilidad(Double.parseDouble(request.getParameter("perc_utilidad")));
            cotizacion.setUtilidad(Double.parseDouble(request.getParameter("val_utilidad")));
            cotizacion.setPerc_aiu(Double.parseDouble(request.getParameter("perc_aiu")));
            cotizacion.setValor_aiu(Double.parseDouble(request.getParameter("val_aiu")));
            cotizacion.setTotal(Double.parseDouble(request.getParameter("val_total")));
            cotizacion.setAnticipo(request.getParameter("anticipo"));
            cotizacion.setPerc_anticipo(Double.parseDouble(request.getParameter("perc_anticipo")));
            cotizacion.setValor_anticipo(Double.parseDouble(request.getParameter("val_anticipo")));
            cotizacion.setRetegarantia(request.getParameter("retegarantia"));
            cotizacion.setPerc_retegarantia(Double.parseDouble(request.getParameter("perc_rete")));
            cotizacion.setPerc_descuento(Double.parseDouble(request.getParameter("perc_descuento")));
            String idsolicitud = request.getParameter("idsolicitud");
            JsonObject facturas = (JsonObject) (new JsonParser()).parse(request.getParameter("listadofacturas"));

            String json = dao.guardar_coste_Proyecto(cotizacion, facturas, idsolicitud, usuario);
            this.printlnResponseAjax(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void cargarFacturasVenta() {
        try {
            String num_solicitud = (request.getParameter("id_solicitud") != null) ? request.getParameter("id_solicitud") : "";
            this.printlnResponseAjax(dao.cargarFacturasVenta(num_solicitud), "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void cargarCotizacionFac() {
        try {
            String num_solicitud = (request.getParameter("id_solicitud") != null) ? request.getParameter("id_solicitud") : "";
            this.printlnResponseAjax(dao.cargarCotizacionFac(num_solicitud), "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void guardarFacturacion() {
        String json = "";
        try {
            JsonObject informacion = (JsonObject) (new JsonParser()).parse(request.getParameter("informacion"));
            // json = this.dao.guardarFacturacion(informacion, usuario);
            json = this.dao.prepararFacturacion(informacion, usuario);
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"ERROR:)\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    public void procesoEnviarFacturacliente() {
        String json = "{\"respuesta\":\"Guardado\"}";
        try {
            String id = request.getParameter("id");
            String num_factura = request.getParameter("num_factura");
            String valor = request.getParameter("valor");
            String amortizacion = request.getParameter("amortizacion");
            String retegarantia = request.getParameter("retegarantia");
            TransaccionService tService = new TransaccionService(this.usuario.getBd());
            tService.crearStatement();
            tService.getSt().addBatch(this.dao.cambiarEstadoEnviado(usuario, id));
            tService.getSt().addBatch(this.dao.guardarControlAmortizacion(usuario, num_factura, valor, amortizacion, retegarantia, id));
            tService.getSt().addBatch(this.dao.guardarHistoricoControlAmortizacion(usuario, num_factura, valor, amortizacion, retegarantia, id));
            tService.execute();
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    public void eliminarFacturacliente() {
        String json = "{\"respuesta\":\"Eliminado\"}";
        try {
            String id = request.getParameter("id");
            String num_factura = request.getParameter("num_factura");
            TransaccionService tService = new TransaccionService(this.usuario.getBd());
            tService.crearStatement();
            tService.getSt().addBatch(this.dao.eliminarFacturaCliente(id));
            tService.getSt().addBatch(this.dao.eliminarControlAmortizacion(num_factura));

            tService.execute();
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    private void anular_factura_parcial() {
        try {
            String json = dao.anular_factura_parcial(request.getParameter("id_factura_parcial"));
            this.printlnResponseAjax(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void cargar_archivo() {
        String num_solicitud = "";
        try {
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");//se consiguen los datos de db.properties

            String directorioArchivos = rb.getString("rutaImagenes") + "opav/archivos/";//se establece la ruta de la imagen

            //this.createDir(directorioArchivos );
            if (ServletFileUpload.isMultipartContent(request)) {

                ServletFileUpload servletFileUpload = new ServletFileUpload(new DiskFileItemFactory());
                List fileItemsList = servletFileUpload.parseRequest(request);

                //// Itero para obtener todos los FileItem
                Iterator it = fileItemsList.iterator();
                String nombre_archivo;
                String fieldtem = "", valortem = "", idxx = "";

                while (it.hasNext()) {
                    FileItem fileItem = (FileItem) it.next();

                    if ((fileItem.isFormField())) {

                        fieldtem = fileItem.getFieldName();
                        valortem = fileItem.getString();

                        if (fieldtem.equals("idsolicitud_carga_archivo")) {
                            num_solicitud = valortem;
                        }

                    }

                    if (!(fileItem.isFormField())) {

                        //// Nombre del archivo en el cliente. Algunos navegadores (por ej. IE 6)
                        //// incluyen el path completo, lo que puede implicar separar path
                        //// de nombre.
                        if (fileItem.getName() != "" && !fileItem.getName().isEmpty() && fileItem.getSize() > 0) {

                            String nombreArchivo = fileItem.getName();

                            String[] nombrearreglo = nombreArchivo.split("\\\\");
                            String nombreArchivoremix = nombrearreglo[(nombrearreglo.length - 1)];
                            String rutaArchivo = directorioArchivos + num_solicitud;
                            nombre_archivo = nombreArchivoremix;

                            this.createDir(rutaArchivo);

                            File archivo = new File(rutaArchivo + "/" + nombre_archivo);
                            fileItem.write(archivo);
                        }

                    }

                }
            }
            reponseJson = "{\"respuesta\":\"OK\"}";
            this.printlnResponseAjax(reponseJson, "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(MinutasContratacionAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }

    }

    public void createDir(String dir) throws Exception {
        try {
            File f = new File(dir);
            if (!f.exists()) {
                f.mkdir();
            }
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    private void listar_archivos_cargados() {
        List lista = null;
        Gson gson = new Gson();
        try {
            String id_solicitud = (request.getParameter("id_solicitud") != null) ? request.getParameter("id_solicitud") : "";
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String rutaOrigen = rb.getString("rutaImagenes") + "opav/archivos/";
            this.reponseJson = gson.toJson(dao.searchNombresArchivos(rutaOrigen, id_solicitud));
            this.printlnResponseAjax(reponseJson, "application/json;");
        } catch (Exception e) {
            System.out.println("errorrr::" + e.toString() + "__" + e.getMessage());
        }

    }

    private void eliminar_archivo_cargado() {
        try {
            String documento = (request.getParameter("num_solicitud") != null) ? Util.setCodificacionCadena(request.getParameter("num_solicitud"), "ISO-8859-1") : "";
            String nomarchivo = (request.getParameter("nomarchivo") != null) ? Util.setCodificacionCadena(request.getParameter("nomarchivo"), "ISO-8859-1") : "";
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String rutaOrigen = rb.getString("rutaImagenes") + "opav/archivos/";
            if (dao.eliminarArchivo(("" + rutaOrigen), documento, nomarchivo)) {
                this.reponseJson = "{\"respuesta\":\"OK\"}";
            } else {
                this.reponseJson = "{\"respuesta\":\"ERROR\"}";
            }
            this.printlnResponseAjax(reponseJson, "application/json;");
        } catch (Exception e) {
            System.out.println("errorrr::" + e.toString() + "__" + e.getMessage());
        }
    }

    private void almacenarArchivoEnCarpetaUsuario() throws Exception {
        try {
            String documento = (request.getParameter("num_solicitud") != null) ? Util.setCodificacionCadena(request.getParameter("num_solicitud"), "ISO-8859-1") : "";
            String nomarchivo = (request.getParameter("nomarchivo") != null) ? Util.setCodificacionCadena(request.getParameter("nomarchivo"), "ISO-8859-1") : "";
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String rutaOrigen = rb.getString("rutaImagenes") + "opav/archivos/";
            String rutaDestino = rb.getString("ruta") + "/images/multiservicios/" + usuario.getLogin() + "/";
            if (dao.almacenarArchivoEnCarpetaUsuario(documento, ("" + rutaOrigen + documento), ("" + rutaDestino), nomarchivo)) {
                this.reponseJson = "{\"respuesta\":\"SI\",\"login\":\"" + usuario.getLogin() + "\"}";
            } else {
                this.reponseJson = "{\"respuesta\":\"NO\"}";
            }
            this.printlnResponseAjax(reponseJson, "application/json;");
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    private void cargar_Insumos_Solicitud() {
        try {
            String idsolicitud = request.getParameter("idsolicitud") == null ? "" : request.getParameter("idsolicitud");
            this.printlnResponseAjax(dao.cargar_Insumos_Solicitud(idsolicitud), "application/json;");
        } catch (Exception ex) {

            ex.printStackTrace();
        }
    }

    private void cargar_Historico_Cotizacion() {
        try {
            String idsolicitud = request.getParameter("idsolicitud") == null ? "" : request.getParameter("idsolicitud");
            this.printlnResponseAjax(dao.cargar_Historico_Cotizacion(idsolicitud), "application/json;");
        } catch (Exception ex) {

            ex.printStackTrace();
        }
    }

    private void crear_Historico_Cotizacion() {
        try {
            String idsolicitud = request.getParameter("idsolicitud") == null ? "" : request.getParameter("idsolicitud");
            this.printlnResponseAjax(dao.crear_Historico_Cotizacion(idsolicitud, usuario), "application/json;");
        } catch (Exception ex) {

            ex.printStackTrace();
        }
    }

    private void cargar_Areas_Historico_cotizacion() {
        try {
            String idsolicitud = request.getParameter("idsolicitud") == null ? "" : request.getParameter("idsolicitud");
            this.printlnResponseAjax(dao.cargar_Areas_Historico_cotizacion(idsolicitud), "application/json;");
        } catch (Exception ex) {

            ex.printStackTrace();
        }
    }

    private void cargar_Disciplinas_Historico_Cotizacion() {
        try {
            String idarea = request.getParameter("idarea") == null ? "0" : request.getParameter("idarea");
            this.printlnResponseAjax(dao.cargar_Disciplinas_Historico_Cotizacion(idarea), "application/json;");
        } catch (Exception ex) {

            ex.printStackTrace();
        }
    }

    private void cargar_Capitulos_Historico_cotizacion() {
        try {
            String iddisciplina = request.getParameter("iddisciplina") == null ? "" : request.getParameter("iddisciplina");
            this.printlnResponseAjax(dao.cargar_Capitulos_Historico_cotizacion(iddisciplina), "application/json;");
        } catch (Exception ex) {

            ex.printStackTrace();
        }
    }

    private void cargar_Actividades_Historico_Cotizacion() {
        try {
            String idcapitulo = request.getParameter("idcapitulo") == null ? "" : request.getParameter("idcapitulo");
            this.printlnResponseAjax(dao.cargar_Actividades_Historico_Cotizacion(idcapitulo), "application/json;");
        } catch (Exception ex) {

            ex.printStackTrace();
        }
    }

    public void cargarAnticipos() {
        try {
            String num_solicitud = (request.getParameter("id_solicitud") != null) ? request.getParameter("id_solicitud") : "";
            this.printlnResponseAjax("{\"page\":1,\"rows\":" + dao.cargarAnticipos(num_solicitud) + "}", "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void cargarReporteAnticipos() {
        try {
            BeansMultiservicio informacion = new BeansMultiservicio();
            String num_solicitud = (request.getParameter("id_solicitud") != null) ? request.getParameter("id_solicitud") : "";
            String multiservicio = (request.getParameter("foms") != null) ? request.getParameter("foms") : "";
            String estado = (request.getParameter("estado_") != null) ? request.getParameter("estado_") : "";
            String anticipo = (request.getParameter("anticipo") != null) ? request.getParameter("anticipo") : "";
            informacion.setParametro1(num_solicitud);
            informacion.setParametro2(multiservicio);
            informacion.setParametro3(estado);
            informacion.setParametro4(anticipo);

            this.printlnResponseAjax(dao.cargarReporteAnticipos(informacion), "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void exportarexcelreporteanticipo() throws Exception {
        String json = request.getParameter("listado") != null ? request.getParameter("listado") : "";
        String nameColum = ((String) request.getParameter("namecolum") != null ? request.getParameter("namecolum") : "");
        JsonArray asJsonArray = (JsonArray) new JsonParser().parse(json);
        String[] cabecera = null;
        short[] dimensiones = null;
        String nombreArchivo = "Reporte_Anticipo", titulo = "Reporte Anticipo";
        cabecera = nameColum.split(",");
        dimensiones = new short[]{
            5000, 5000, 5000, 7000, 7000, 7000, 7000, 7000, 7000, 7000,
            7000, 7000, 7000, 7000, 7000, 7000, 7000
        };

        ExcelApiUtil apiUtil = new ExcelApiUtil(usuario);
        reponseJson = apiUtil.crearArchivoExcel(nombreArchivo, titulo, asJsonArray, dimensiones, cabecera, request);
        this.printlnResponseAjax(reponseJson, "text/plain");
    }

    private void guardarAnticipo() {
        String json = "";
        try {
            JsonObject informacion = (JsonObject) (new JsonParser()).parse(request.getParameter("informacion"));
            json = this.dao.guardarAnticipo(informacion, usuario);
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"ERROR:)\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    public void exportarPdfCuentaCobro() {
        String json = "";
        try {
            JsonObject informacion = (JsonObject) (new JsonParser()).parse(request.getParameter("informacion"));
            json = "{\"respuesta\":\"" + dao.exportarPdfCuentaCobro(usuario, informacion) + "\"}";
            this.printlnResponseAjax(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        }
    }

    private void obtener_id_rel_actividades_apu() {
        try {
            String op = request.getParameter("op") == null ? "" : request.getParameter("op");
            String filtro = request.getParameter("filtro") == null ? "" : request.getParameter("filtro");
            String id_solicitud = request.getParameter("id_solicitud") == null ? "" : request.getParameter("id_solicitud");
            String json = dao.obtener_id_rel_actividades_apu(id_solicitud, op, filtro);
            this.printlnResponseAjax(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void set_Rentabilidad_Global() {
        try {
            String perc_contratista = request.getParameter("perc_contratista") == null ? "" : request.getParameter("perc_contratista");
            String perc_esquema = request.getParameter("perc_esquema") == null ? "" : request.getParameter("perc_esquema");
            String filtro = request.getParameter("filtro") == null ? "" : request.getParameter("filtro");
            String tipo = request.getParameter("tipo") == null ? "" : request.getParameter("tipo");
            String distribucion_rentabilidad_esquema = request.getParameter("distribucion_rentabilidad_esquema") == null ? "" : request.getParameter("distribucion_rentabilidad_esquema");
            String json = dao.set_Rentabilidad_Global(filtro, perc_contratista, perc_esquema, tipo, distribucion_rentabilidad_esquema);
            this.printlnResponseAjax(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void buscarAccion() {
        try {
            String num_solicitud = (request.getParameter("id_solicitud") != null) ? request.getParameter("id_solicitud") : "";
            this.printlnResponseAjax(dao.buscarAccion(num_solicitud), "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void guardarCambioCotizacion() {
        String json = "";
        try {
            JsonObject informacion = (JsonObject) (new JsonParser()).parse(request.getParameter("informacion"));
            json = this.dao.guardarCambioCotizacion(informacion, usuario);
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"ERROR:)\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    public void buscarCotizacionTem() {
        try {
            String num_solicitud = (request.getParameter("id_solicitud") != null) ? request.getParameter("id_solicitud") : "";
            this.printlnResponseAjax(dao.buscarCotizacionTem(num_solicitud), "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void insertar_cabecera_tabla_temporal() {
        try {
            String id_solicitud = (request.getParameter("id_solicitud") != null) ? request.getParameter("id_solicitud") : "";
            this.printlnResponseAjax(dao.insertar_cabecera_tabla_temporal(id_solicitud, usuario), "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void usuario_admin() {
        try {
            String Respuesta;
            if (usuario.getTipo().equals("TSPUSER")) {
                Respuesta = "\"respuesta\":\"OK\"";
            } else {
                Respuesta = "\"respuesta\":\".l.\"";
            }
            this.printlnResponseAjax(Respuesta, "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void autocompletar() {
        try {
            String q = request.getParameter("q") != null ? request.getParameter("q") : "";
            String opp = request.getParameter("opp") != null ? request.getParameter("opp") : "";
            String json = dao.autocompletar(q, opp);
            this.printlnResponseAjax(json, "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void update_Presupuesto_Terminado() {
        try {
            String id_solicitud = (request.getParameter("id_solicitud") != null) ? request.getParameter("id_solicitud") : "";
            String estado = (request.getParameter("estadoo") != null) ? request.getParameter("estadoo") : "";
            this.printlnResponseAjax(dao.update_Presupuesto_Terminado(id_solicitud, estado, usuario), "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void generar_OT() {
        try {
            String id_solicitud = (request.getParameter("id_solicitud") != null) ? request.getParameter("id_solicitud") : "";
            this.printlnResponseAjax(dao.generar_OT(id_solicitud, usuario), "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void cargarTrazabilidadOferta() {
        try {
            String idsolicitud = request.getParameter("idsolicitud") == null ? "" : request.getParameter("idsolicitud");
            this.printlnResponseAjax(dao.cargarTrazabilidadOferta(idsolicitud), "application/json;");
        } catch (Exception ex) {

            ex.printStackTrace();
        }
    }

    public void cargarInformacionModalidadProyecto() {
        try {
            String solicitud = request.getParameter("id_solicitud") == null ? "" : request.getParameter("id_solicitud");
            String nom_proyecto = request.getParameter("nombre_proyecto") == null ? "" : request.getParameter("nombre_proyecto");
            this.printlnResponseAjax(dao.cargarInformacionModalidadProyecto(solicitud, nom_proyecto), "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void calcular_iva_compensar() {
        try {
            String solicitud = request.getParameter("id_solicitud") == null ? "" : request.getParameter("id_solicitud");
            String subtotal = request.getParameter("subtotal") == null ? "0" : request.getParameter("subtotal");
            this.printlnResponseAjax(dao.calcular_iva_compensar(solicitud, subtotal), "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void guardarInformacionModalidadProyecto() {
        String json = "";
        try {
            String solicitud = request.getParameter("id_solicitud") == null ? "" : request.getParameter("id_solicitud");
            String porc_administracion = request.getParameter("porc_administracion") == null ? "" : request.getParameter("porc_administracion");
            String administracion = request.getParameter("administracion") == null ? "" : request.getParameter("administracion");
            String porc_imprevisto = request.getParameter("porc_imprevisto") == null ? "" : request.getParameter("porc_imprevisto");
            String imprevisto = request.getParameter("imprevisto") == null ? "" : request.getParameter("imprevisto");
            String porc_utilidad = request.getParameter("porc_utilidad") == null ? "" : request.getParameter("porc_utilidad");
            String utilidad = request.getParameter("utilidad") == null ? "" : request.getParameter("utilidad");
            String iva = request.getParameter("iva") == null ? "" : request.getParameter("iva");
            String perc_iva = request.getParameter("perc_iva") == null ? "" : request.getParameter("perc_iva");
            String total = request.getParameter("total") == null ? "" : request.getParameter("total");
            String modalidad_comercial = request.getParameter("modalidad_comercial") == null ? "" : request.getParameter("modalidad_comercial");
            String porc_aiu = request.getParameter("porc_aiu") == null ? "" : request.getParameter("porc_aiu");
            String aiu = request.getParameter("aiu") == null ? "0" : request.getParameter("aiu");
            //String iva_compensar = request.getParameter("iva_compensar") == null ? "0" : request.getParameter("iva_compensar");
            String iva_compensado = request.getParameter("iva_compensado") == null ? "0" : request.getParameter("iva_compensado");
            String porc_iva_compensado = request.getParameter("porc_iva_compensado") == null ? "0" : request.getParameter("porc_iva_compensado");
            this.printlnResponseAjax(dao.guardarInformacionModalidadProyecto(solicitud, porc_administracion, administracion, porc_imprevisto, imprevisto, porc_utilidad, utilidad, iva, usuario, perc_iva, total, modalidad_comercial, porc_aiu, aiu, iva_compensado, porc_iva_compensado), "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"ERROR:)\"}";
        }
    }

    private void cargar_tablas_modulo_impresion_oferta() {
        try {
            String idsolicitud = request.getParameter("idsolicitud") == null ? "" : request.getParameter("idsolicitud");
            String opc = request.getParameter("opc") == null ? "" : request.getParameter("opc");
            this.printlnResponseAjax(dao.cargar_tablas_modulo_impresion_oferta(idsolicitud, opc), "application/json;");
        } catch (Exception ex) {

            ex.printStackTrace();
        }
    }
    
    
    
    public void obtenerNotasOferta()
    throws Exception {
        
      try{  
        
     String id_solicitud = (request.getParameter("id_solicitud") != null) ? request.getParameter("id_solicitud") : "";
//         JsonArray jsonArrNotas= new JsonArray(); 
//         jsonArrNotas =dao.getNotasOferta(id_solicitud);
         

         String json = dao.obtenerNotasOferta(id_solicitud);
         
         
            this.printlnResponseAjax(json, "application/json;");
            
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    
    }
    
    
    

    public void generar_Pdf_Cotizacion() throws Exception {
        String json = "";
        try {
            String id_solicitud = (request.getParameter("id_solicitud") != null) ? request.getParameter("id_solicitud") : "";
            String detallado = (request.getParameter("detallado") != null) ? request.getParameter("detallado") : "";
            String apu = (request.getParameter("apu") != null) ? request.getParameter("apu") : "";
            boolean precios  = (request.getParameter("precios") != null) ? Boolean.parseBoolean(request.getParameter("precios")) : false;

            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");//se consiguen los datos de db.properties
            String directorioOrigen = rb.getString("ruta") + "/images/multiservicios/" + usuario.getLogin() + "/";//se establece la ruta de la imagen 
            String directorioDestino = rb.getString("rutaImagenes") + "gestionadministrativa/minutas/";
            this.createDir(directorioOrigen);
            JsonArray jsonArrAreas = new JsonArray();
            JsonArray jsonArrCap = new JsonArray();
            JsonObject jobjCot = dao.getInfoPdfCotizacion("SQL_OBTENER__INFO_COTIZACION", id_solicitud).get(0).getAsJsonObject();

            
            switch (detallado) {
            case "SI":
                jsonArrAreas = dao.getInfoPdfCotizacion("SQL_OBTENER_TOTALES_AREAS_COTIZACION_SIN_ADMINISTRACION", id_solicitud);
                jsonArrCap = dao.getInfoPdfCotizacion("SLQ_OBTENER_TOTALES_CAPITULOS_COTIZACION_SIN_ADMINISTRACION", id_solicitud);
                break;
            case "NO":
                jsonArrAreas = dao.getInfoPdfCotizacion("OBTENERTOTALESAREASCOTIZACION", id_solicitud);
                jsonArrCap = dao.getInfoPdfCotizacion("OBTENERTOTALESCAPITULOSCOTIZACION", id_solicitud);
                break;
            }

            
            
            
            /**
             * 
             * se buscan las notas de la oferta
             * 
             */
            
            JsonArray jsonArrNotas= new JsonArray(); 
            jsonArrNotas = dao.getNotasOferta(id_solicitud);

            
             JsonArray ubicacionPdfs = new JsonArray(); 
             
             if(apu.equals("NO"))
                json = dao.generar_Pdf_Cotizacion(id_solicitud, directorioOrigen, jobjCot, jsonArrAreas, jsonArrCap,jsonArrNotas, usuario, detallado , precios);
             else
                json = dao.generar_Pdf_Cotizacion_APU(id_solicitud, directorioOrigen, jobjCot, jsonArrAreas, jsonArrCap,jsonArrNotas, usuario, detallado,precios);


             JsonObject jsonO = new JsonObject();
             jsonO.addProperty("Ruta", rb.getString("rutaImagenes") +"portafolio1.pdf");
             ubicacionPdfs.add(jsonO);

             JsonParser jsonParser = new JsonParser();
             jsonO = (JsonObject) jsonParser.parse(json);
             jsonO.addProperty("Ruta",rb.getString("ruta") + jsonO.get("Ruta").getAsString());
             ubicacionPdfs.add(jsonO);
             
            
//             
//             JsonObject respuesta = new JsonObject();
//                String valor = "0";
//                for (int i = 0; i < notas.size(); i++) {
//                  respuesta = notas.get(i).getAsJsonObject();
//            
//                }
             
             
//             JsonObject jsonObj = new JsonObject();
//             jsonObj.addProperty("Ruta", rb.getString("rutaImagenes") +"portafolio2.pdf");
//             ubicacionPdfs.add(jsonObj);

                          
//             jsonO.addProperty("Ruta", "D:\\Desktop\\portafolio2.pdf");

             
             json = dao.merge_Pdf(ubicacionPdfs , usuario,jsonArrNotas);
            
            
/*            json = dao.generar_Pdf_Cotizacion(id_solicitud, directorioOrigen, jobjCot, jsonArrAreas, jsonArrCap, usuario, detallado);*/
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            this.printlnResponseAjax(json, "application/json;");
        }
    }

    private void exportarExcel() throws Exception {
        String json = request.getParameter("listado") != null ? request.getParameter("listado") : "";
        JsonArray asJsonArray = (JsonArray) new JsonParser().parse(json);
        String[] cabecera = null;
        short[] dimensiones = null;
        String nombreArchivo = "Reporte_APU", titulo = "REPORTE APUS";

        cabecera = new String[]{"Nombre Area", "Nombre Capitulo", "Nombre APU", "Nombre Unidad", "Cantidad", "Valor Unitario", "Total"};

        dimensiones = new short[]{
            10000, 15000, 15000, 5000, 3000, 5000, 5000
        };

        ExcelApiUtil apiUtil = new ExcelApiUtil(usuario);
        reponseJson = apiUtil.crearArchivoExcel(nombreArchivo, titulo, asJsonArray, dimensiones, cabecera, request);
        this.printlnResponseAjax(reponseJson, "text/plain");
    }

    public void cargarReporteAnticiposMs() {
        try {
            this.printlnResponseAjax(dao.cargarReporteAnticiposMs(), "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void exportarExcelReporteAnticiposMs() throws Exception {
        String json = request.getParameter("listado") != null ? request.getParameter("listado") : "";
        JsonArray asJsonArray = (JsonArray) new JsonParser().parse(json);
        String[] cabecera = null;
        short[] dimensiones = null;
        String nombreArchivo = "Reporte_Anticipos", titulo = "Reporte Anticipos";

        cabecera = new String[]{"Id", "Id solicitud", "Multiservicio", "Nombre Proyecto", "Anticipo", "Valor Anticipo", "#CXC", "Valor CXC", "Valor Abono CXC", "Valor Saldo CXC", "Fecha CXC", "#CXP", "Valor CXP", "Fecha CXP"};

        dimensiones = new short[]{
            3000, 7000, 9000, 7000, 7000, 7000, 7000, 7000, 7000, 7000,
            7000, 7000, 7000, 7000};

        ExcelApiUtil apiUtil = new ExcelApiUtil(usuario);
        reponseJson = apiUtil.crearArchivoExcel(nombreArchivo, titulo, asJsonArray, dimensiones, cabecera, request);
        this.printlnResponseAjax(reponseJson, "text/plain");
    }

    public void generar_Pdf_Presupuesto_Detalle() throws Exception {
        String json = "";
        try {
            String id_solicitud = (request.getParameter("id_solicitud") != null) ? request.getParameter("id_solicitud") : "";
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");//se consiguen los datos de db.properties
            String directorioOrigen = rb.getString("ruta") + "/images/multiservicios/" + usuario.getLogin() + "/";//se establece la ruta de la imagen 
            String directorioDestino = rb.getString("rutaImagenes") + "gestionadministrativa/minutas/";
            this.createDir(directorioOrigen);

            JsonObject jobjCot = dao.getInfoPdfCotizacion("SQL_OBTENER__INFO_COTIZACION", id_solicitud).get(0).getAsJsonObject();
            JsonArray jsonArrAreas = dao.getInfoPdfCotizacion("obtenerTotalesAreasPresupuesto", id_solicitud);
            JsonArray jsonArrCap = dao.getInfoPdfCotizacion("obtenerTotalesCapitulosPresupuesto", id_solicitud);
            json = dao.generar_Pdf_Presuesto(id_solicitud, directorioOrigen, jobjCot, jsonArrAreas, jsonArrCap, usuario);
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            this.printlnResponseAjax(json, "application/json;");
        }
    }

    private void guardarCondionesComerciales() {
        String json = "";
        try {
            JsonObject informacion = (JsonObject) (new JsonParser()).parse(request.getParameter("informacion"));
            json = this.dao.guardarCondionesComerciales(informacion, usuario);
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"ERROR:)\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    private void cargarCondionesComerciales() {
        String json = "";
        try {
            String id_solicitud = request.getParameter("id_solicitud") != null ? request.getParameter("id_solicitud") : "";
            json = this.dao.cargarCondionesComerciales(id_solicitud);
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"ERROR:)\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    public void cargarAccionesPreparadas() {
        try {
            String num_solicitud = (request.getParameter("id_solicitud") != null) ? request.getParameter("id_solicitud") : "";
            this.printlnResponseAjax(dao.cargarAccionesPreparadas(num_solicitud), "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void cargar_Iva_Compensar() {
        try {
            String id_solicitud = (request.getParameter("id_solicitud") != null) ? request.getParameter("id_solicitud") : "";
            this.printlnResponseAjax(dao.cargar_Iva_Compensar(id_solicitud), "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void cargar_CIA() {
        try {
            String id_solicitud = (request.getParameter("id_solicitud") != null) ? request.getParameter("id_solicitud") : "";
            this.printlnResponseAjax(dao.cargar_CIA(id_solicitud), "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void clonacionProyectos() {
        try {
            String id_solicitud_origen = (request.getParameter("clonacion_id_solicitud_origen") != null) ? request.getParameter("clonacion_id_solicitud_origen") : "";
            String id_solicitud_destino = (request.getParameter("clonacion_id_solicitud_destino") != null) ? request.getParameter("clonacion_id_solicitud_destino") : "";
            this.printlnResponseAjax(dao.clonacionProyectos(id_solicitud_origen, id_solicitud_destino, usuario), "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void generar_centro_costo() {
        try {
            String id_solicitud = (request.getParameter("id_solicitud") != null) ? request.getParameter("id_solicitud") : "";
            Integer id = Integer.parseInt((request.getParameter("id") != null) ? request.getParameter("id") : "");
            this.printlnResponseAjax(dao.generar_Centros_Costos(id_solicitud, usuario, id ), "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void generar_centro_costo_lotes() {
        try {
             // 1 es especiales
             //2 es masivos
            //3 tablerista
            Integer modalidad = Integer.parseInt((request.getParameter("modalidad") != null) ? request.getParameter("modalidad") : "");
            this.printlnResponseAjax(dao.generar_centro_costo_lotes(usuario, modalidad), "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void guardar_notas_oferta() {
        String json = "";
        try {
            
            String info = URLDecoder.decode(request.getParameter("informacion"), "UTF-8"); 
            
            JsonObject informacion = (JsonObject) (new JsonParser()).parse(info);
             
            System.out.println(informacion);
                       
            json = this.dao.guardar_notas_oferta(informacion, usuario);
            
            
        } catch (Exception e) {
            
            e.printStackTrace();
            json = "{\"respuesta\":\"ERROR:)\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }
    
    
    
    
    private void editar_notas_oferta() {
        String json = "";
        try {
            
            String info = URLDecoder.decode(request.getParameter("informacion"), "UTF-8"); 
            
            JsonObject informacion = (JsonObject) (new JsonParser()).parse(info);
             
            System.out.println(informacion);
            
            
            String idNota = informacion.get("idNota").getAsString();
            String nota = informacion.get("nota").getAsString();


            json = this.dao.actualizarNotaOferta(idNota, nota);
            
        } catch (Exception e) {
            
            e.printStackTrace();
            json = "{\"respuesta\":\"ERROR:)\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }
    
    
    
    public void eliminar_notas_oferta() {
        String json = "{\"respuesta\":\"Eliminado\"}";
        try {
            String id = request.getParameter("idNota");
            TransaccionService tService = new TransaccionService(this.usuario.getBd());
            tService.crearStatement();
            tService.getSt().addBatch(this.dao.eliminarNotaOferta(id));

            tService.execute();
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }
    
    
      
    
    
    
    private void cargarComboGenerico() {
        String json = "";
        try {
            
            String op = request.getParameter("op") != null ? request.getParameter("op") : "";
            String param = request.getParameter("param") != null ? request.getParameter("param") : "";
            json = dao.cargarComboGenerico(op, param);
        } catch (Exception e) {
            
            e.printStackTrace();
            json = "{\"respuesta\":\"ERROR:)\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }
}
