/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.opav.controller;

import com.tsp.exceptions.InformationException;
import com.tsp.opav.model.DAOS.AceptacionesConsolidadoDAO;
import com.tsp.opav.model.DAOS.impl.AceptacionesConsolidadoImpl;
import javax.servlet.ServletException;

/**
 *
 * @author jpacosta
 */
public class AceptacionesConsolidadoAction extends Action {
    private final int BUSCAR_CONSOLIDADOS = 0;
    private final int DETALLAR_CONSOLIDADOS = 1;
    private AceptacionesConsolidadoDAO dao;
    private String txtResp;
    private String tipo;
    
    @Override
    public void run() throws ServletException, InformationException {
        try {
            this.tipo = "application/json";
            this.txtResp = "";
            dao = new AceptacionesConsolidadoImpl();
            int opcion = (request.getParameter("opcion") != null) 
                    ? Integer.parseInt(request.getParameter("opcion"))
                    : -1;
            switch (opcion) {
                case BUSCAR_CONSOLIDADOS:
                    this.buscar(); break; 
                case DETALLAR_CONSOLIDADOS:
                    this.detallarConsolidado(); break;
            }
        } catch (Exception e) {
            this.txtResp = "{mensaje:"+e.getMessage()+"}";
            e.printStackTrace();
        } finally {
            try {
                response.setContentType(this.tipo + "; charset=utf-8");
                response.setHeader("Cache-Control", "no-cache");
                response.getWriter().println(txtResp);
            } catch(Exception es) {
                
            }
        }
    }
    
    private void buscar() throws Exception{
        String periodo = request.getParameter("periodo");
        String tipos = request.getParameter("tipos_sol");
        String estados = request.getParameter("estados");
        String interventores = request.getParameter("interventores");
        String responsables = request.getParameter("responsables");
        
        this.txtResp = dao.buscar(periodo, tipos,estados,interventores,responsables);
    }
    
    private void detallarConsolidado() throws Exception {
        String periodo = request.getParameter("periodo");
        String sector = request.getParameter("sector");
        
        this.txtResp = dao.detallarConsolidado(periodo, sector);
    }
}
