/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * CotizacionExcelAction.java : Se encarga de exportar a formato xls una cotizacion
 * Copyright 2010 Rhonalf Martinez Villa <rhonaldomaster@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.tsp.opav.controller;
import java.util.*;
import com.tsp.opav.model.beans.*;
import com.tsp.opav.model.services.*;
import com.tsp.operation.model.beans.Usuario;
import javax.servlet.http.*;
import com.tsp.util.*;
import java.io.File;
import java.text.SimpleDateFormat;
/**
 *
 * @author rhonalf
 */
public class CotizacionExcelAction extends Action{

    Usuario user;
    public CotizacionExcelAction(){

    }

    @Override
    public void run(){
        String mensaje = "<span class='fila'>Archivo generado de forma correcta<br>Revise su directorio de archivos</span>";
        try {
            HttpSession misesion = request.getSession();
            user = (Usuario)misesion.getAttribute("Usuario");
            String login = user.getLogin();
            String idaccion = request.getParameter("idaccion")!=null ? request.getParameter("idaccion") : "";
            String consec = "";
            CotizacionService csr = null;
            String ruta = "";
            csr = new CotizacionService(user.getBd());
            consec = csr.getConsecutivo(idaccion);
            csr = null;
            ruta = this.directorioArchivo(login, consec);
            this.conversion(consec,ruta,idaccion);
        }
        catch (Exception e) {
            mensaje = "<span class='fila'>Ocurrio un error al generar el archivo</span>";
            System.out.println("Error en CotizacionExcelAction: "+e.toString());
            e.printStackTrace();
        }
        finally {
            try { this.escribirResponse(mensaje); }catch (Exception e) { System.out.println("Error al dar los datos al jsp en CotizacionExcelAction: "+e.toString()); }
        }
    }

    /**
     * Le entrega los datos a la peticion ajax
     * @param resp lo que se quiere entregar
     * @throws Exception cuando ocurre un error
     */
    private void escribirResponse(String resp) throws Exception{
        try {
            response.setContentType("text/plain; charset=utf-8");
            response.setHeader("Cache-Control", "no-cache");
            response.getWriter().println(resp);
        }
        catch (Exception e) {
            System.out.println("Error al escribir la respuesta: "+e.toString());
            throw new Exception("Error al escribir la respuesta: "+e.toString());
        }
    }

    /**
     * Crea el objeto que se convertira en documento y hace llamados a los metodos que le dan los valores al archivo
     * @param consecutivo Consecutivo de la cotizacion que se exporta
     * @param ruta Donde quedara guardado el archivo
     * @param accion id_accion a la que pertenece la cotizacion
     * @throws Exception Cuando hay error
     */
    private void conversion(String consecutivo,String ruta,String accion) throws Exception{
        ExcelApplication excel = null;
        String descripcion = consecutivo;
        CotizacionService csr = null;
        try {
            excel = this.instanciar(descripcion);
            excel.setDataCell(0, 0, "Cotizacion num.");
            excel.setCellStyle(0, 0, excel.getStyle("estilo2"));
            excel.setDataCell(0, 1, descripcion);
            excel.setCellStyle(0, 1, excel.getStyle("estilo3"));
            csr = new CotizacionService(user.getBd());
            String[] datos = (csr.datosAccion(accion)).split(";");
            excel.setDataCell(1, 0, "Solicitud num.");
            excel.setCellStyle(1, 0, excel.getStyle("estilo2"));
            excel.setDataCell(1, 1, datos[0]);
            excel.setCellStyle(1, 1, excel.getStyle("estilo3"));
            excel.setDataCell(2, 0, "Nombre cliente");
            excel.setCellStyle(2, 0, excel.getStyle("estilo2"));
            excel.setDataCell(2, 1, datos[1]);
            excel.setCellStyle(2, 1, excel.getStyle("estilo3"));
            String[] cabecera = {"Codigo","Descripcion","Medida","Cantidad","Valor unitario","Valor total","Observacion"};
            for (int i = 0; i < cabecera.length; i++) {
                excel.setDataCell(4, i, cabecera[i]);
                excel.setCellStyle(4, i, excel.getStyle("estilo2"));
            }
            this.llenado(excel,consecutivo);
            excel.saveToFile(ruta);
        }
        catch (Exception e) {
            throw new Exception("Error al convertir a excel: "+e.toString());
        }
    }

    /**
     * Genera la ruta en que se guardara el archivo
     * @param user usuario que genera el archivo
     * @param cons consecutivo de la cotizacion
     * @return String con la ruta en la que queda el archivo
     * @throws Exception cuando ocurre algun error
     */
    private String directorioArchivo(String user,String cons) throws Exception{
        String ruta = "";
        try {
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            ruta = rb.getString("ruta") + "/exportar/migracion/" + user;
            SimpleDateFormat fmt = new SimpleDateFormat("yyyMMdd_hhmmss");
            File archivo = new File( ruta );
            if (!archivo.exists()) archivo.mkdirs();
            ruta = ruta + "/" + cons +"_" + fmt.format( new Date() ) +".xls";
        }
        catch (Exception e) {
            throw new Exception("Error al generar el directorio: "+e.toString());
        }
        return ruta;
    }

    /**
     * Crea un objeto tipo ExcelApplication
     * @param descripcion Nombre de la hoja principal del libro
     * @return Objeto ExcelApplication creado
     * @throws Exception Cuando hay error
     */
    private ExcelApplication instanciar(String descripcion) throws Exception{
        ExcelApplication excel = new ExcelApplication();
        try{
            excel.createSheet(descripcion);
            excel.createFont("Titulo", "Arial", (short)1, true, (short)12);
            excel.createFont("Subtitulo", "Verdana", (short)0, true, (short)10);
            excel.createFont("Contenido", "Verdana", (short)0, false, (short)10);

            excel.createColor((short)11, (byte)255, (byte)255, (byte)255);//Blanco
            excel.createColor((short)9, (byte)26, (byte)126, (byte)0);//Verde dark
            excel.createColor((short)10, (byte)37, (byte)69, (byte)255);//Azul oscuro

            excel.createStyle("estilo1", excel.getFont("Titulo"), (short)10, true, "@");
            excel.createStyle("estilo2", excel.getFont("Subtitulo"), (short)9, true, "@");
            excel.createStyle("estilo3", excel.getFont("Contenido"), (short)11, true, "@");

        }
        catch(Exception e){
            throw new Exception("Error al instanciar el objeto: "+e.toString());
        }
        return excel;
    }

    /**
     * Se encarga de llenar los datos del documento
     * @param excel Objeto que se convertira en el archivo
     * @param consecutivo Consecutivo de la cotizacion
     * @throws Exception Cuando hay un error
     */
    private void llenado(ExcelApplication excel,String consecutivo) throws Exception{
        CotizacionService csr = null;
        MaterialesService mserv = null;
        ArrayList listado = null;
        ArrayList rs = null;
        Cotizacion cot = null;
        Material mat = null;
        String nombre="";
        String medida="";
        double acumulador = 0;
        int maxfila = 2;
        try {
            csr = new CotizacionService(user.getBd());
            mserv = new MaterialesService(user.getBd());
            listado = csr.buscarDets(consecutivo, "cod_cotizacion");
            for (int i = 0; i < listado.size(); i++) {
                maxfila = listado.size() + 6;
                cot = (Cotizacion)listado.get(i);
                //-------------------------------------------------------------
                excel.setCellStyle(i+5, 0, excel.getStyle("estilo3"));
                excel.setDataCell(i+5, 0, cot.getMaterial());
                //-------------------------------------------------------------
                rs = (ArrayList)mserv.buscarPor(1,cot.getMaterial());
                if (rs.size()>0) {
                     mat = (Material)rs.get(0);
                }
                else {
                    rs = mserv.buscarPorAnul(1, cot.getMaterial());
                    if (rs.size()>0) {
                        mat = (Material)rs.get(0);
                    }
                }
                if(mat!=null){
                    nombre = mat.getDescripcion();
                    medida = mat.getMedida();
                }
                //-------------------------------------------------------------
                excel.setCellStyle(i+5, 1, excel.getStyle("estilo3"));
                excel.setDataCell(i+5, 1, nombre);
                //-------------------------------------------------------------
                excel.setCellStyle(i+5, 2, excel.getStyle("estilo3"));
                excel.setDataCell(i+5, 2, medida);
                //-------------------------------------------------------------
                excel.setCellStyle(i+5, 3, excel.getStyle("estilo3"));
                excel.setDataCell(i+5, 3, cot.getCantidad());
                //-------------------------------------------------------------
                excel.setCellStyle(i+5, 4, excel.getStyle("estilo3"));
                excel.setDataCell(i+5, 4, cot.getValor());
                //-------------------------------------------------------------
                excel.setCellStyle(i+5, 5, excel.getStyle("estilo3"));
                excel.setDataCell(i+5, 5, (cot.getValor() * cot.getCantidad()));
                //-------------------------------------------------------------
                excel.setCellStyle(i+5, 6, excel.getStyle("estilo3"));
                excel.setDataCell(i+5, 6, cot.getObservacion());
                acumulador = acumulador + (cot.getValor() * cot.getCantidad());
            }

            excel.setCellStyle(maxfila, 4, excel.getStyle("estilo2"));
            excel.setDataCell(maxfila, 4, "Total cotizacion");

            excel.setCellStyle(maxfila, 5, excel.getStyle("estilo3"));
            excel.setDataCell(maxfila, 5, acumulador);
        }
        catch (Exception e) {
            throw new Exception("Error al llenar el archivo: "+e.toString());
        }
    }

}