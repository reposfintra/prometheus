/*
 * EncriptionException.java
 *
 * Created on 30 de marzo de 2004, 10:35 AM
 */

package com.tsp.exceptions;

/*
 * Excepcion ocurrida durante el proceso de encripción.
 */
public class EncriptionException extends Exception
{
  public EncriptionException()
  {
  }

  public EncriptionException(String msg)
  {
    super(msg);
  }
}