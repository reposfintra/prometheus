/*
 * Class.java
 *
 * Created on 8 de abril de 2005, 02:44 PM
 */

package com.tsp.exceptions;

/**
 * Excepci�n lanzada con prop�sitos informativos, para determinar que ocurri�
 * un error durante una validaci�n. Se utilizar� para diferenciar los errores
 * informativos de los de sistema, los cuales indican que hay un problema en
 * el programa.
 * @author  Ivan Herazo
 */
public class InformationException extends Exception
{
  /** Crea una nueva instancia de esta clase */
  public InformationException() {
    super();
  }
  
  /**
   * Crea una nueva instancia de esta clase.
   * @param infoMsg Mensaje informativo.
   */
  public InformationException(String infoMsg) {
    super(infoMsg);
  }
}
