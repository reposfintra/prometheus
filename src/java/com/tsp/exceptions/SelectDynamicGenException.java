/*
 * SelectDynamicGenException.java
 *
 * Created on 3 de abril de 2004, 09:41 AM
 */

package com.tsp.exceptions;

/**
 * Excepci�n ocurrida durante el proceso de generaci�n din�mica de
 * un cuadro combinado para selecci�n �nica a partir de una tabla.
 * @author  iherazo
 */
public class SelectDynamicGenException extends Exception
{
  public SelectDynamicGenException()
  {
  }
  
  public SelectDynamicGenException(String msg)
  {
    super(msg);
  }
}
