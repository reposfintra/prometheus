/*
 * ChartGenerationException.java
 *
 * Created on 23 de abril de 2004, 04:23 PM
 */

package com.tsp.exceptions;

/**
 * Excepci�n ocurrida durante el proceso de generaci�n din�mica de una gr�fica.
 * @author  iherazo
 */
public class ChartGenerationException extends Exception
{
  public ChartGenerationException()
  {
  }
  
  public ChartGenerationException(String msg)
  {
    super(msg);
  }  
}
