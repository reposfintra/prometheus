/*
 * SessionExpiredException.java
 *
 * Created on 30 de marzo de 2004, 10:36 AM
 */

package com.tsp.exceptions;

/**
 * Excepcion que ocurre cuando se ejecuta una accion en la aplicacion
 * despu�s que la sesi�n expira.
 * (Se utilizar� como alternativa en caso de no poder manejar esta falla
 *  de otra manera)
 * @author  iherazo
 */
public class SessionExpiredException extends Exception
{
  public SessionExpiredException()
  {
  }
  
  public SessionExpiredException(String msg)
  {
    super(msg);
  }
}
