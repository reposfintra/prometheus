/*
 * EmailSendingException.java
 *
 * Created on 4 de enero de 2007, 11:51 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.tsp.exceptions;

import java.io.IOException;

/**
 * Excepcion lanzada por el programa {@link com.tsp.util.sendmail.SendMail} cuando 
 * ocurre un error al enviar un email.
 * @author Ing. Ivan Herazo
 */
public class EmailSendingException extends IOException
{
  /** Crea e inicializa una instancia de esta clase */
  public EmailSendingException() {
    super();
  }
  
  /**
   * Crea e inicializa una instancia de esta clase
   * @param msg Descripcion del error
   */
  public EmailSendingException(String msg) {
    super(msg);
  }
}
