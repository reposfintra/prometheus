/********************************************************************
 *  Nombre Clase.................   GrabarComprobanteService.java
 *  Descripci�n..................   Service para la grabacion de comprobante
 *  Autor........................   Ing. Ivan Dario Gomez
 *  Fecha........................   7 de junio de 2006, 05:20 PM
 *  Versi�n......................   1.0
 *  Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

/*
 * GrabarComprobanteService.java
 *
 * Created on 7 de junio de 2006, 05:20 PM
 */

package com.tsp.finanzas.contab.model.services;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.finanzas.contab.model.beans.*;
import com.tsp.finanzas.contab.model.DAO.*;
import com.tsp.operation.model.DAOS.*;
import com.tsp.operation.model.*;


public class GrabarComprobanteService {
    
    private GrabarComprobanteDAO dao;
    private OpDAO                opDAO;
    private List  lista  = new LinkedList();
    private List  listaComp = null;
    private List  ListaTercero = null;
    private Hashtable cuenta;
    private List Periodos;
    private ComprobanteFacturas Comprobante ;
    
    /** Creates a new instance of GrabarComprobanteService */
    public GrabarComprobanteService() {
        dao = new GrabarComprobanteDAO();
        opDAO = new OpDAO();
    }
    public GrabarComprobanteService(String dataBaseName) {
        dao = new GrabarComprobanteDAO(dataBaseName);
        opDAO = new OpDAO(dataBaseName);
    }
    
    
    /**
     * Metodo buscarTipoDoc ,Metodo para buscar los tipos de documentos
     * @autor : Ing. Ivan Dario Gomez
     * @param : String dstrct
     * @see   : buscarTipoDoc - GrabarComprobanteDAO
     * @version : 1.0
     */
    public void buscarTipoDoc(String dstrct) throws SQLException{
        try{
            this.lista = dao.buscarTipoDoc(dstrct);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    
     /**
     * Metodo buscarDatosTipoDoc ,Metodo para buscar los datos de un  tipo de documento
     * @autor : Ing. Ivan Dario Gomez
     * @param : String dstrct
     * @see   : buscarDatosTipoDoc - GrabarComprobanteDAO
     * @version : 1.0
     */
    public Hashtable buscarDatosTipoDoc(String dstrct,String codigo) throws SQLException{
        try{
            return dao.buscarDatosTipoDoc(dstrct,codigo);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    /**
     * Metodo buscarCuenta , para buscar los datos de la cuenta
     * @autor   : Ing. Ivan Dario Gomez Vanegas
     * @param   : String dstrct String cuenta
     * @see     : buscarCuenta - GrabarComprobanteDAO
     * @throws  : SQLException
     * @version : 1.0
     */
    public void buscarCuenta(String dstrct, String cuenta)throws SQLException{
        try{
            this.cuenta =  dao.buscarCuenta(dstrct,cuenta);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    
    
    /**
     * Metodo AgregarMayorizacion , metododo para ingresar en mayorizacion
     * @autor   : Ing. Ivan Dario Gomez Vanegas
     * @param   : List listaContable
     * @see     : AgregarMayorizacion - GrabarComprobanteDAO
     * @throws  : Exception
     * @version : 1.0
     */
    public void AgregarMayorizacion(List listaContable,String usuario)throws Exception{
        ArrayList<String> listSql = new ArrayList<String>();
        try{
            
            for(int i=0;i<listaContable.size();i++){
                ComprobanteFacturas  comprobante = (ComprobanteFacturas) listaContable.get(i);
                List item = comprobante.getItems();
                for(int j=0;j<item.size();j++){
                    ComprobanteFacturas  comprodet = (ComprobanteFacturas) item.get(j);
                    String periodo = comprodet.getPeriodo().substring(0,4);
                    double saldo = 0;
                    ////System.out.println("TOTAL DEBITO --->"+comprodet.getTotal_debito());
                    if(comprodet.getTotal_debito()== 0.0){
                        saldo = comprodet.getTotal_credito() * -1;
                        
                    }else{
                        saldo = comprodet.getTotal_debito() ;
                    }
                    ////System.out.println("SALDO--->"+saldo);
                    if(dao.ExisteMayorizacion(comprodet.getDstrct(),periodo, comprodet.getCuenta())){
                        //Tengo que hacer un UPDATE
                        
                        listSql.add(dao.UpdateMayor(comprodet, saldo));
                        
                    }else{
                        listSql.add(dao.InsertMayor(comprodet, saldo, usuario));
                    }
                    ////System.out.println("AUXILIAR ---->"+ comprodet.getAuxiliar());
                    if(!comprodet.getAuxiliar().equals("")){
                        if(dao.ExisteMayorSubledger(comprodet.getDstrct(),periodo, comprodet.getCuenta(),comprodet.getAuxiliar())){
                            //Tengo que hacer un UPDATE
                            listSql.add(dao.UpdateMayorSubledger(comprodet, saldo));
                        }else{
                            listSql.add(dao.InsertMayorSubledger(comprodet, saldo, usuario));
                        }
                        
                    }
                    
                    
                }
            }
            
            TransaccionService tService = new TransaccionService(this.dao.getDatabaseName()); 
            tService.crearStatement();
            for (String sql : listSql) {
                System.out.println(sql);
                tService.getSt().addBatch(sql);
            }
            tService.execute();
            
            
            
        }
        catch(Exception e){
            throw new Exception(e.getMessage());
        }
    }
    
    /**
     * Metodo buscarPeriodos , para buscar los periodos abiertos
     * @autor   : Ing. Ivan Dario Gomez Vanegas
     * @param   : String dstrct
     * @see     : buscarPeriodos - GrabarComprobanteDAO
     * @throws  : SQLException
     * @version : 1.0
     */
    public void buscarPeriodos(String dstrct)throws SQLException{
        try{
            this.Periodos =  dao.buscarPeriodos(dstrct);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
     /**
     * Metodo ComprobanteSearch , para buscar un comprobante
     * @autor   : Ing. Ivan Dario Gomez Vanegas
     * @param   : String dstrct,String tipodoc, String numdoc,int grupo
     * @see     : ComprobanteSearch - GrabarComprobanteDAO
     * @throws  : SQLException
     * @version : 1.0
     */
    public ComprobanteFacturas ComprobanteSearch(String dstrct,String tipodoc, String numdoc,int grupo)throws Exception{
        try{
            return  dao.ComprobanteSearch(dstrct, tipodoc, numdoc, grupo);
        }
        catch(Exception e){
            throw new SQLException(e.getMessage());
        }
    }
    
    /**
     * Getter for property lista.
     * @return Value of property lista.
     */
    public java.util.List getLista() {
        return lista;
    }
    
    /**
     * Getter for property cuenta.
     * @return Value of property cuenta.
     */
    public java.util.Hashtable getCuenta() {
        return cuenta;
    }
    
    /**
     * Getter for property existeCuenta.
     * @return Value of property existeCuenta.
     */
    public boolean isExisteCuenta() {
        return dao.isExisteCuenta();
    }
    
    /**
     * Getter for property Comprobante.
     * @return Value of property Comprobante.
     */
    public com.tsp.finanzas.contab.model.beans.ComprobanteFacturas getComprobante() {
        return Comprobante;
    }
    
    /**
     * Setter for property Comprobante.
     * @param Comprobante New value of property Comprobante.
     */
    public void setComprobante(com.tsp.finanzas.contab.model.beans.ComprobanteFacturas Comprobante) {
        this.Comprobante = Comprobante;
    }
    
    /**
     * Getter for property Periodos.
     * @return Value of property Periodos.
     */
    public List getPeriodos() {
        return Periodos;
    }
     /**
     * Getter for property Periodos.
     * @return Value of property Periodos.
     */
    public String getMoneda(String cia)throws Exception {
        String moneda = "";
        try{
            moneda =  opDAO.getDatosCia(cia);
          
       }catch(Exception e){
           e.printStackTrace();
       }
       return moneda;
       
    }
    
    
    /**
     * Metodo buscarCuenta , para buscar los datos de la cuenta
     * @autor   : Ing. Ivan Dario Gomez Vanegas
     * @param   : String dstrct String cuenta
     * @see     : buscarCuenta - GrabarComprobanteDAO
     * @throws  : SQLException
     * @version : 1.0
     */
    public void DeleteComprobante(String dstrct, String cuenta)throws SQLException{
        try{
             dao.buscarCuenta(dstrct,cuenta);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    
    /**
     * Metodo buscarCuenta , para buscar los datos de la cuenta
     * @autor   : Ing. Ivan Dario Gomez Vanegas
     * @param   : String dstrct String cuenta
     * @see     : buscarCuenta - GrabarComprobanteDAO
     * @throws  : SQLException
     * @version : 1.0
     */
    public void BuscarComprobantes(String dstrct,String tipodoc, String numdoc)throws Exception{
        try{
            this.listaComp =  dao.BuscarComprobantes(dstrct, tipodoc, numdoc);
        }
        catch(Exception e){
            throw new Exception(e.getMessage());
        }
    }
    
    /**
     * Getter for property listaComp.
     * @return Value of property listaComp.
     */
    public java.util.List getListaComp() {
        return listaComp;
    }    
    
    
     /**
     * M�todo que actualiza en Comprobante
     * @autor....... ivan gomez
     * @param....... ComprobanteFacturas comprobante
     * @see......... UpdateComprobante - GrabarComprobanteDAO
     * @throws...... Exception
     * @version.....1.0.
     **/
    public  void UpdateComprobante(ComprobanteFacturas comprobante)throws Exception{
        try{
           ArrayList<String> sql  =  dao.UpdateComprobante(comprobante);
           TransaccionService  scv = new TransaccionService(this.dao.getDatabaseName());
             scv.crearStatement();
            for (int i = 0; i < sql.size(); i++) {
                scv.getSt().addBatch(sql.get(i));
            }
             scv.execute();
        }
        catch(Exception e){
            throw new Exception(e.getMessage());
        } 
    }
    
    /**
     * M�todo que actualiza la serie
     * @autor.......ivan gomez
     * @param....... dstrct y el tipo de documento
     * @throws......Exception
     * @version.....1.0.
     **/
    public  void UpdateSerie(String dstrct, String codigo)throws Exception{
        try{
            dao.UpdateSerie(dstrct, codigo);
        }
        catch(Exception e){
            throw new Exception(e.getMessage());
        }  
    }
    
    /**
     * Metodo busquedaTercero, busca la lista de terceros
     * @param: valor
     * @autor : Ing. Ivan Dario Gomez
     * @version : 1.0
     */
    public void busquedaTercero(String valor) throws Exception {
         try{
            setListaTercero( dao.busquedaTercero(valor));
        }
        catch(Exception e){
            throw new Exception(e.getMessage());
        }
    }
    
    
    
    /**
     * Metodo existeDocumento, verifica un documento en la tabla de comprobantes
     * @param: dstrct
     * @param: tipdoc
     * @param: numdoc
     * @autor : Ing. Mario Fontalvo
     * @version : 1.0
     */
    public boolean existeDocumento(String dstrct, String tipdoc, String numdoc) throws Exception {
         try{
            return dao.existeDocumento(dstrct, tipdoc, numdoc);
        }
        catch(Exception e){
            throw new Exception(e.getMessage());
        }
    }    
    
    /**
     * Getter for property ListaTercero.
     * @return Value of property ListaTercero.
     */
    public java.util.List getListaTercero() {
        return ListaTercero;
    }
    
    /**
     * Setter for property ListaTercero.
     * @param ListaTercero New value of property ListaTercero.
     */
    public void setListaTercero(java.util.List ListaTercero) {
        this.ListaTercero = ListaTercero;
    }
    
}
