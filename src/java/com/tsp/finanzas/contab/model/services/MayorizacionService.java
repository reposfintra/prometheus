/*
 * MayorizacionService.java
 *
 * Created on 9 de junio de 2006, 07:41 PM
 */

/*********************************************************************************
 * Nombre clase :      MayorizacionService.java                                  *
 * Descripcion :       Service de MayorizacionService.java                      *
 * Autor :             Osvaldo P�rez Ferrer                                                   *
 * Fecha :             09 de JUNIO de 2006, 7:41 PM                              *
 * Version :           1.0                                                       *
 * Copyright :         Fintravalores S.A.                                   *
 *********************************************************************************/

package com.tsp.finanzas.contab.model.services;


import com.tsp.finanzas.contab.model.beans.*;
import com.tsp.finanzas.contab.model.DAO.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.Usuario;
import com.aspose.cells.*;
import com.tsp.operation.model.beans.AsposeUtil;
import java.text.*;
/**
 *
 * @author  Osvaldo
 */
public class MayorizacionService {
    
    MayorizacionDAO mayor;
    Usuario usuario;
    /** Creates a new instance of MayorizacionService */
    public MayorizacionService() {
        mayor = new MayorizacionDAO();
    }
    public MayorizacionService(String dataBaseName) {
        mayor = new MayorizacionDAO(dataBaseName);
    }
    
     /**
     *Metodo que modifica la tabla mayor actualizando los valores debito o credito
     *y actualiza el saldo actual
     *@autor: Osvaldo P�rez
     *@param: comprobante contiene la informaci�n o filtros para obtener el detalle de un comprobante                     
     *@throws: En caso de que un error de base de datos ocurra.
     *@return Una cadena con el sql obtenido
     */
    public String updateMayorizacionCuenta( Comprobantes comprobante ) throws SQLException{
        return mayor.updateMayorizacionCuenta(comprobante);
    }
    
    
     /**
     *Metodo que permite actualizar la tabla MAYORSUBLEDGER con los cr�ditos o d�bitos, y permite
     *actualizar al igual el saldo actual.
     *@autor: Osvaldo P�rez Ferrer
     *@param comprobante contiene la informaci�n o filtros para obtener el detalle de un 
     *                   compobante
     *@throws: En caso de que un error de base de datos ocurra.
     *@return Una cadena con el sql obtenido
     */
    public String updateMayorizacionSubledgerCuenta( Comprobantes comprobante ) throws SQLException{
       return mayor.updateMayorizacionSubledgerCuenta(comprobante );
    }
     /****
     *Metodo que permite obtener las sumatorias de las cuentas ICG de la tabla mayor
     *@autor: David Pi�a
     *@param anio El a�o del periodo
     *@param mes El mes del periodo
     *@throws: En caso de que un error de base de datos ocurra.     
     ****/
    public void getTotalesSumatoriasICG( String anio, String mes ) throws SQLException{
        mayor.getTotalesSumatoriasICG( anio, mes );
    }
    /****
     *Metodo que permite obtener las sumatorias de los elementos de gasto de las cuentas I, C o G
     *@autor: David Pi�a
     *@param anio El a�o del periodo
     *@param mes El mes del periodo
     *@param tipo El tipo de cuenta I, C o G
     *@throws: En caso de que un error de base de datos ocurra.     
     ****/
    public void getTotalesSumatoriasElementoGasto( String anio, String mes, String tipo ) throws SQLException{
        mayor.getTotalesSumatoriasElementoGasto( anio, mes, tipo );
    }
    
    /**
     * Getter for property mayor.
     * @return Value of property mayor.
     */
    public com.tsp.finanzas.contab.model.beans.Mayorizacion getMayor() {
        return mayor.getMayor();
    }
    
    /**
     * Setter for property mayor.
     * @param mayor New value of property mayor.
     */
    public void setMayor(com.tsp.finanzas.contab.model.beans.Mayorizacion mayorizacion) {
        mayor.setMayor( mayorizacion );
    }
    
    /**
     * Getter for property vector.
     * @return Value of property vector.
     */
    public java.util.Vector getVector() {
        return mayor.getVector();
    }
    
    /**
     * Setter for property vector.
     * @param vector New value of property vector.
     */
    public void setVector(java.util.Vector vector) {
        mayor.setVector( vector );
    }
    

    
    /************************************************************************
     * metodo existeEnMayor: verifica si el registro existe en mayor o mayor_subledger
     * @param: Comprobantes c, objeto con los datos de la cuenta
     * @param: String tabla, tabla de la que se desea consultar 'mayor' o 'mayor_subledger'
     * @return: true or false
     * @throws: en caso de un error de BD
     ************************************************************************/
    public boolean existeEnMayor( Comprobantes c, String tabla ) throws Exception{
        return mayor.existeEnMayor(c, tabla);
    }
    
    /************************************************************************
     * metodo insertMayorizacionCuenta, genera SQL para insertar un registro en con.mayor
     * @author: Ing. Osvaldo P�rez Ferrer
     * @param: Comprobantes c, objeto con datos a insertar
     ************************************************************************/
    public String insertMayorizacionCuenta( Comprobantes c ) throws SQLException{
        return mayor.insertMayorizacionCuenta(c, this.usuario);
    }
    
    /************************************************************************
     * metodo insertMayorizacionSubledgerCuenta, genera SQL para insertar un 
     *        registro en con.mayor_subledger
     * @author: Ing. Osvaldo P�rez Ferrer
     * @param: Comprobantes c, objeto con datos a insertar
     ************************************************************************/
    public String insertMayorizacionSubledgerCuenta( Comprobantes c ) throws SQLException{
        return mayor.insertMayorizacionSubledgerCuenta(c, this.usuario);
    }
    
    /**
     * Getter for property usuario.
     * @return Value of property usuario.
     */
    public com.tsp.operation.model.beans.Usuario getUsuario() {
        return usuario;
    }
    
    /**
     * Setter for property usuario.
     * @param usuario New value of property usuario.
     */
    public void setUsuario(com.tsp.operation.model.beans.Usuario usuario) {
        this.usuario = usuario;
    }
      /**
      * metodo para iniciarl el proceso de mayorizacion
      * @param user usuario en session
      * @param mes mes de la consulta
      * @param ano año de la consulta
      * @author MGarizao - GEOTECH
      * @date 17/02/2010
      * @version 1.0
      * @return mensaje de confirmacion del proceso
      * @throws SQLException
      */
    public String obtenerProcesoMayorizacion(Usuario user,String mes,String ano) throws SQLException{

        String msn = "";
        Vector comando = new Vector();
        comando = mayor.buscarComprobantePeriodo(comando, user, ano+mes);
        comando = mayor.buscarComprobanteMayorizar(comando);
        comando = mayor.actualizarComprobanteSinCuenta(comando);
        comando = mayor.actualizarComprobanteConCuenta(comando);
        comando = mayor.actualizarComprobanteDiferenciaTotales(comando);
        comando = mayor.ingresarMayor(comando, user, ano);
        comando = mayor.actualizarMayor(comando, user, ano, mes);
        comando = mayor.actualizarFechaComprobante(comando, user);
        msn = mayor.ejecutarComando(comando);
        return msn;
    }


    /**
     * metodo para buscar la informacioni del proceso de mayorizacion que se va a exportar a excel
     * @param mes mes de la consulta
     * @param ano año de la consulta
     * @author MGarizao - GEOTECH
     * @date 17/02/2010
     * @version 1.0
     * @return Vector con los diferentes arreglos de los reportes
     * @throws SQLException
     */
    public Vector obtenerProcesoMayorizadoExcel(String mes,String ano) throws SQLException{

      Vector excel = mayor.buscarProcesoMayorizado(mes, ano);
      return excel;

    }

    /**
     * metodo para generar el reporte en excel
     * @param excel vector con la informacion de los tres reportes
     * @param mes mes de captura
     * @param ano año de caprtura
     * @param ruta ruta de salida del archivo
     * @throws Exception
     */
    public void generarReporteMayorizacion(Vector excel,String mes,String ano,String ruta) throws Exception{


        SimpleDateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy h:mm a");
        AsposeUtil as = new AsposeUtil();
        Workbook libro = as.crearLibro("logresultado"+ano+mes);
        String nombreLibro = as.crearNombreLibro("Logresultado"+ano+mes,4, false);
        Style st = as.crearEstilo(libro, nombreLibro, 10, false, false, 0, "", Color.BLACK, false, Color.BLACK, (short) 0);
        libro.removeSheet(0);

        Worksheet sheetResultado = as.crearHoja(libro, "Resultado");
        as.setCelda(sheetResultado, 1, 0, "Titulo",st);
        as.setNegrilla(sheetResultado, true, 1, 0);
        as.setCelda(sheetResultado, 1, 1, "Resultado del proceso de Mayorizacion",st);
        as.setCelda(sheetResultado, 2, 0, "Periodo", st);
        as.setNegrilla(sheetResultado, true, 2, 0);
        as.setCelda(sheetResultado, 2, 1, ano+mes, st);
        as.setCelda(sheetResultado, 3, 0, "Fecha", st);
        as.setNegrilla(sheetResultado, true,3, 0);
        as.setCelda(sheetResultado, 3, 1, formatoFecha.format(new java.util.Date()), st);
        as.setCelda(sheetResultado, 5, 0, "Fuente", st);
        as.setNegrilla(sheetResultado, true, 5, 0);
        as.setCelda(sheetResultado, 5, 1, "valor_debito", st);
        as.setNegrilla(sheetResultado, true,5,1);
        as.setCelda(sheetResultado, 5, 2, "valor_credito", st);
        as.setNegrilla(sheetResultado, true, 5, 2);
        as.setCelda(sheetResultado, 5, 3, "diferencia", st);
        as.setNegrilla(sheetResultado, true,5, 3);

        int i = 0;
        Vector reporte = (Vector) excel.elementAt(0);
        int fila = 6;

        int j = 1;

        while (i < reporte.size()){

            Mayorizacion mayor = (Mayorizacion) reporte.elementAt(i);
            as.setCelda(sheetResultado, fila, 0, mayor.getFuente(), st);
            as.setCelda(sheetResultado, fila, 1,mayor.getValor_debito(), st);
            as.setCelda(sheetResultado, fila, 2,mayor.getValor_credito(), st);
            as.setCelda(sheetResultado, fila, 3, mayor.getDiferencia(), st);
            fila = fila + 1;
            i = i + 1;

        }

         sheetResultado.autoFitRows();
         sheetResultado.autoFitColumns();

         Worksheet sheetComprobante = as.crearHoja(libro, "Comprobantes");
         as.setCelda(sheetComprobante, 0, 0, "tipodoc", st);
         as.setNegrilla(sheetComprobante, true, 0, 0);
         as.setCelda(sheetComprobante, 0, 1, "numdoc", st);
         as.setNegrilla(sheetComprobante, true, 0, 1);
         as.setCelda(sheetComprobante, 0, 2, "grupo_transaccion", st);
         as.setNegrilla(sheetComprobante, true, 0,2);
         as.setCelda(sheetComprobante, 0, 3,"total_debito", st);
         as.setNegrilla(sheetComprobante, true, 0, 3);
         as.setCelda(sheetComprobante,0, 4, "total_credito", st);
         as.setNegrilla(sheetComprobante, true, 0, 4);
         as.setCelda(sheetComprobante, 0, 5, "TOTAL DEBITO DIFERENTE AL CREDITO", st);
         as.setNegrilla(sheetComprobante, true, 0, 5);
         as.setCelda(sheetComprobante, 0, 6, "DIFERENCIA ENTRE EL DETALLE Y LA CABECERA", st);
         as.setNegrilla(sheetComprobante, true,0, 6);
         as.setCelda(sheetComprobante, 0, 7, "CUENTA INVALIDA", st);
         as.setNegrilla(sheetComprobante, true, 0, 7);

         i = 0;
         fila = 1;
         reporte = (Vector) excel.elementAt(1);

         while(i < reporte.size()){

              if (i > 0 && (i % 10000) == 0){
                     sheetComprobante = as.crearHoja(libro, "Comprobantes "+j);
                     as.setCelda(sheetComprobante, 0, 0, "tipodoc", st);
                     as.setNegrilla(sheetComprobante, true, 0, 0);
                     as.setCelda(sheetComprobante, 0, 1, "numdoc", st);
                     as.setNegrilla(sheetComprobante, true, 0, 1);
                     as.setCelda(sheetComprobante, 0, 2, "grupo_transaccion", st);
                     as.setNegrilla(sheetComprobante, true, 0,2);
                     as.setCelda(sheetComprobante, 0, 3,"total_debito", st);
                     as.setNegrilla(sheetComprobante, true, 0, 3);
                     as.setCelda(sheetComprobante,0, 4, "total_credito", st);
                     as.setNegrilla(sheetComprobante, true, 0, 4);
                     as.setCelda(sheetComprobante, 0, 5, "TOTAL DEBITO DIFERENTE AL CREDITO", st);
                     as.setNegrilla(sheetComprobante, true, 0, 5);
                     as.setCelda(sheetComprobante, 0, 6, "DIFERENCIA ENTRE EL DETALLE Y LA CABECERA", st);
                     as.setNegrilla(sheetComprobante, true,0, 6);
                     as.setCelda(sheetComprobante, 0, 7, "CUENTA INVALIDA", st);
                     as.setNegrilla(sheetComprobante, true, 0, 7);
                     j++;
                     fila=1;
                }


             Mayorizacion mayor = (Mayorizacion) reporte.elementAt(i);
             as.setCelda(sheetComprobante, fila, 0, mayor.getTipodoc(), st);
             as.setCelda(sheetComprobante, fila, 1, mayor.getNumdoc(), st);
             as.setCelda(sheetComprobante, fila, 2, mayor.getGrupotransaccion(), st);
             as.setCelda(sheetComprobante, fila, 3, mayor.getValor_debito(), st);
             as.setCelda(sheetComprobante, fila, 4, mayor.getValor_credito(), st);
             as.setCelda(sheetComprobante, fila, 5, mayor.getInd_deb_cre_cab(), st);
             as.setCelda(sheetComprobante, fila, 6, mayor.getInd_deb_cre_cab_det(),st);
             as.setCelda(sheetComprobante, fila, 7, mayor.getInd_cuenta_invalida(), st);
             fila = fila + 1;
             i = i + 1;


         }

         sheetComprobante.autoFitRows();
         sheetComprobante.autoFitColumns();

         Worksheet sheetDetalle = as.crearHoja(libro, "Detalles_comprobantes");
         as.setCelda(sheetDetalle, 0, 0, "tipodoc", st);
         as.setNegrilla(sheetDetalle, true, 0, 0);
         as.setCelda(sheetDetalle, 0, 1, "numdoc", st);
         as.setNegrilla(sheetDetalle, true, 0, 1);
         as.setCelda(sheetDetalle,0, 2, "grupo_transaccion", st);
         as.setNegrilla(sheetDetalle, true, 0, 2);
         as.setCelda(sheetDetalle, 0, 3, "transaccion", st);
         as.setNegrilla(sheetDetalle, true, 0, 3);
         as.setCelda(sheetDetalle, 0, 4, "cuenta", st);
         as.setNegrilla(sheetDetalle, true, 0, 4);
         as.setCelda(sheetDetalle, 0, 5, "auxiliar", st);
         as.setNegrilla(sheetDetalle, true, 0, 5);
         as.setCelda(sheetDetalle, 0, 6, "valor_debito", st);
         as.setNegrilla(sheetDetalle, true, 0, 6);
         as.setCelda(sheetDetalle, 0, 7, "valor_credito", st);
         as.setNegrilla(sheetDetalle, true, 0, 7);
         as.setCelda(sheetDetalle, 0, 8, "detalle", st);
         as.setNegrilla(sheetDetalle, true, 0, 8);

         i = 0;
         fila = 1;
         reporte = (Vector) excel.elementAt(2);

         j = 1;

         while(i < reporte.size()){

             if (i > 0 && (i % 10000) == 0){
                 sheetDetalle = as.crearHoja(libro, "Detalles_comprobantes "+j);
                 as.setCelda(sheetDetalle, 0, 0, "tipodoc", st);
                 as.setNegrilla(sheetDetalle, true, 0, 0);
                 as.setCelda(sheetDetalle, 0, 1, "numdoc", st);
                 as.setNegrilla(sheetDetalle, true, 0, 1);
                 as.setCelda(sheetDetalle,0, 2, "grupo_transaccion", st);
                 as.setNegrilla(sheetDetalle, true, 0, 2);
                 as.setCelda(sheetDetalle, 0, 3, "transaccion", st);
                 as.setNegrilla(sheetDetalle, true, 0, 3);
                 as.setCelda(sheetDetalle, 0, 4, "cuenta", st);
                 as.setNegrilla(sheetDetalle, true, 0, 4);
                 as.setCelda(sheetDetalle, 0, 5, "auxiliar", st);
                 as.setNegrilla(sheetDetalle, true, 0, 5);
                 as.setCelda(sheetDetalle, 0, 6, "valor_debito", st);
                 as.setNegrilla(sheetDetalle, true, 0, 6);
                 as.setCelda(sheetDetalle, 0, 7, "valor_credito", st);
                 as.setNegrilla(sheetDetalle, true, 0, 7);
                 as.setCelda(sheetDetalle, 0, 8, "detalle", st);
                 as.setNegrilla(sheetDetalle, true, 0, 8);
                 fila=1;

             }

             Mayorizacion mayor = (Mayorizacion) reporte.elementAt(i);
             as.setCelda(sheetDetalle, fila, 0,mayor.getTipodoc(), st);
             as.setCelda(sheetDetalle, fila, 1, mayor.getNumdoc(), null);
             as.setCelda(sheetDetalle, fila, 2, mayor.getGrupotransaccion(), st);
             as.setCelda(sheetDetalle, fila, 3, mayor.getTransaccion(), st);
             as.setCelda(sheetDetalle, fila, 4, mayor.getCuenta(), st);
             as.setCelda(sheetDetalle, fila, 5, mayor.getAuxiliar(), st);
             as.setCelda(sheetDetalle, fila, 6, mayor.getValor_debito(), st);
             as.setCelda(sheetDetalle, fila, 7,mayor.getValor_credito(), st);
             as.setCelda(sheetDetalle, fila, 8, mayor.getInd_cuenta_invalida(), st);
             fila = fila + 1;
             i = i + 1;


         }

         sheetDetalle.autoFitRows();
         sheetDetalle.autoFitColumns();
         as.cerrarLibro(libro, ruta, nombreLibro, 4);

    }

    /**
     * metodo para iniciar el proceso de desmayorizacion
     * @param user usuario en session
     * @param mes mes de captura
     * @param ano año de captura
     * @author MGarizao - GEOTECH
     * @date 17/02/2010
     * @version 1.0
     * @return mensaje de confirmacion
     * @throws SQLException
     */
    public String obtenerProcesoDesMayorizacion(Usuario user,String mes,String ano) throws SQLException{


        String msn = "";
        Vector comando = new Vector();
        comando = mayor.actualizarDesComprobante(comando, user, ano, mes);
        comando = mayor.actualizarDesMayor(comando, user, mes, ano);
        comando = mayor.actualizarDesSubledger(comando, user, mes, ano);
        msn = mayor.ejecutarComando(comando);
        return msn;

    }
    
}
