/************************************************************************
* Nombre ....................ConsultasSaldosContablesService.java       *
* Descripci�n................Clase que maneja los servicios al model    *
*                            relacionados con los saldos contables.     *
* Autor......................LREALES                                    *
* Fecha Creaci�n.............12 de Junio de 2006, 10:42 AM              *
* Versi�n....................1.0                                        *
* Coyright...................Transportes Sanchez Polo S.A.              *
************************************************************************/

package com.tsp.finanzas.contab.model.services;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.finanzas.contab.model.beans.*;
import com.tsp.finanzas.contab.model.DAO.ConsultasSaldosContablesDAO;

public class ConsultasSaldosContablesService {
    
    private ConsultasSaldosContablesDAO saldos_dao;    
    
    /** Creates a new instance of ConsultasSaldosContablesService */
    public ConsultasSaldosContablesService () {
        saldos_dao = new ConsultasSaldosContablesDAO();
    }
    public ConsultasSaldosContablesService (String dataBaseName) {
        saldos_dao = new ConsultasSaldosContablesDAO(dataBaseName);
    }
    
    /**
     * Metodo: getSaldos, permite retornar un objeto de registros de saldos contables.
     * @autor : LREALES
     * @param : -
     * @version : 1.0
     */    
    public SaldosContables getSaldos()throws SQLException{
        
        return saldos_dao.getSaldos();
        
    }
    
    /**
     * Metodo: setSaldos, permite obtener un objeto de registros de saldos contables.
     * @autor : LREALES
     * @param : objeto
     * @version : 1.0
     */
    public void setSaldos ( SaldosContables saldos )throws SQLException {
        
        saldos_dao.setSaldos( saldos );
        
    }
    
    /**
     * Metodo: getVec_saldos, permite retornar un vector de registros de saldos contables.
     * @autor : LREALES
     * @param : -
     * @version : 1.0
     */    
    public Vector getVec_saldos()throws SQLException{
        
        return saldos_dao.getVec_saldos();
        
    }
    
    /**
     * Metodo: setVec_saldos, permite obtener un vector de registros de saldos contables.
     * @autor : LREALES
     * @param : vector
     * @version : 1.0
     */    
    public void setVec_saldos( Vector SaldosContables )throws SQLException{
        
        saldos_dao.setVec_saldos( SaldosContables );
        
    }
        
    /**
    * Metodo listaIncluyeSubledger, lista la informacion de las cuentas
    * Incluyendo Subledgers!!!!!!!!!
    * @see subledgerCompleto - ConsultasSaldosContablesDAO
    * @param : el distrito, la fecha inicial, la fecha final, la cuenta inicial y la cuenta final.
    * @version : 1.0
    */    
    public void listaIncluyeSubledger( String distrito, String anio_ini, String anio_fin, String cuenta_ini, String cuenta_fin )throws SQLException{
        
        saldos_dao.listaIncluyeSubledger( distrito, anio_ini, anio_fin, cuenta_ini, cuenta_fin );
        
    }
        
    /**
    * Metodo listaSaldosContables, lista la informacion de las cuentas
    * @see listaSaldosContables - ConsultasSaldosContablesDAO
    * @param : el distrito, la fecha inicial, la fecha final, la cuenta inicial y la cuenta final.
    * @version : 1.0
    */    
    public void listaSaldosContables( String distrito, String anio_ini, String anio_fin, String cuenta_ini, String cuenta_fin )throws SQLException{
        
        saldos_dao.listaSaldosContables( distrito, anio_ini, anio_fin, cuenta_ini, cuenta_fin );
        
    }
    
}