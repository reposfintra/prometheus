/*
 * CmcService.java
 *
 * Created on 12 de junio de 2006, 03:59 PM
 */

/*********************************************************************************
 * Nombre clase :      CmcService.java                                           *
 * Descripcion :       Service de CmcDAO.java                                    *
 * Autor :             David Pi�a Lopez                                          *
 * Fecha :             12 de junio de 2006, 03:59 PM                             *
 * Version :           1.0                                                       *
 * Copyright :         Fintravalores S.A.                                   *
 *********************************************************************************/

package com.tsp.finanzas.contab.model.services;

import com.tsp.finanzas.contab.model.beans.Cmc;
import com.tsp.finanzas.contab.model.DAO.*;
import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.ApplusService;
/**
 *
 * @author  David
 */
public class CmcService {
    
    CmcDAO dao;
    /** Creates a new instance of CmcService */
    public CmcService() {
        dao = new CmcDAO();
    }
    public CmcService(String dataBaseName) {
        dao = new CmcDAO(dataBaseName);
    }
    
    /**
     *M�todo que permite insertar un cmc_doc
     *@autor: David Pi�a     
     *@throws: En caso de que un error de base de datos ocurra.     
     */
    public void insertar() throws SQLException{
        dao.insertar();
    }
    /**
     *M�todo que busca un cmc_doc a trav�s de varios filtros
     *@autor: David Pi�a     
     *@throws: En caso de que un error de base de datos ocurra.     
     */
    public void buscar() throws SQLException{
        dao.buscar();
    }
    /**
     *M�todo que actualiza un cmc_doc
     *@autor: David Pi�a          
     *@param tipoDocumento El tipo de documento
     *@param cmc_old el Viejo Cmc
     *@throws: En caso de que un error de base de datos ocurra.     
     */
    public void actualizar( String tipoDocumento, String cmc_old ) throws SQLException{
        dao.actualizar( tipoDocumento, cmc_old );
    }
    /**
     *M�todo que obtiene un cmc_doc
     *@autor: David Pi�a     
     *@throws: En caso de que un error de base de datos ocurra.     
     */
    public void obtenerCmcDoc() throws SQLException{
        dao.obtenerCmcDoc();
    }
    /**
     *M�todo que obtiene todos los cmc activos
     *@autor: David Pi�a     
     *@throws: En caso de que un error de base de datos ocurra.     
     */
    public void obtenerCmc() throws SQLException{
        dao.obtenerCmc();
    }
    
    /**
     * Getter for property cmc.
     * @return Value of property cmc.
     */
    public com.tsp.finanzas.contab.model.beans.Cmc getCmc() {
        return dao.getCmc();
    }
    
    /**
     * Setter for property cmc.
     * @param cmc New value of property cmc.
     */
    public void setCmc(com.tsp.finanzas.contab.model.beans.Cmc cmc) {
        dao.setCmc( cmc );
    }
    
    /**
     * Getter for property treemap.
     * @return Value of property treemap.
     */
    public java.util.TreeMap getTreemap() {
        return dao.getTreemap();
    }
    
    /**
     * Setter for property treemap.
     * @param treemap New value of property treemap.
     */
    public void setTreemap(java.util.TreeMap treemap) {
        dao.setTreemap( treemap );
    }
    
    /**
     * Getter for property vector.
     * @return Value of property vector.
     */
    public java.util.Vector getVector() {
        return dao.getVector();
    }
    
    /**
     * Setter for property vector.
     * @param vector New value of property vector.
     */
    public void setVector(java.util.Vector vector) {
        dao.setVector( vector );
    }

    public List<Cmc> ListarCmc(String cod) throws SQLException
{
          return  dao.ListarCmc(cod);
}
    
}
