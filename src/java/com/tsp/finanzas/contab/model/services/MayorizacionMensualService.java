/********************************************************************************
 * Nombre clase . . . . . .   ReporteRtfeService.java                           *
 * Descripci�n  .. . . . . .  Service que manda a hacer las consultas           *
 * Autor . . . . . . . . . .  Ing. Pablo Emilio Bassil Orozco                   *
 * Fecha . . . . . . . . . .  Created on 10 de Febrero de 2009                  *
 * Version . . . . . . . . .  1.0                                               *
 * Copyright ...TSP - TRANSPORTES SANCHEZ POLO S.A.                             *
 ********************************************************************************/

package com.tsp.finanzas.contab.model.services;

import com.tsp.finanzas.contab.model.DAO.*;
import java.sql.SQLException;
import java.util.Vector;

/**
 * Este service llama al DAO para hacer la consulta
 * y retorna un <code>Vector</code> con los resultados,
 * los cuales son creados a partir del objeto
 * <code>FacturaReporteRfte</code>
 *
 * @author Pablo Emilio Bassil Orozco
 */
public class MayorizacionMensualService{

    private MayorizacionMensualDAO dao;

    public MayorizacionMensualService(){
        dao = new MayorizacionMensualDAO();
    }
    public MayorizacionMensualService(String dataBaseName){
        dao = new MayorizacionMensualDAO(dataBaseName);
    }

    public void setParams(int a�o, int mes, String user, String base, String dstrct){ 
        dao.setDatos(a�o, mes, user, base, dstrct);
    }

    public boolean desmayorizar(){
        try {
            dao.actualizarComprobante();
            dao.actualizarMayor();
            dao.actualizarMayorSubledger();
            return true;
        }
        catch(SQLException e) {
            System.out.print("\n\n" + e.toString());
            return false;
        }
    }

    public Vector mayorizar() throws SQLException{
        dao.correrQueries();
        return dao.verificacion();
    }
}