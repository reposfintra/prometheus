/************************************************************************
* Nombre ....................PlanDeCuentasService.java                  *
* Descripci�n................Clase que maneja los servicios al model    *
*                            relacionados con el plan de cuentas.       *
* Autor......................LREALES                                    *
* Fecha Creaci�n.............6 de Junio de 2006, 07:11 AM               *
* Versi�n....................1.0                                        *
* Coyright...................Transportes Sanchez Polo S.A.              *
************************************************************************/

package com.tsp.finanzas.contab.model.services;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.finanzas.contab.model.beans.*;
import com.tsp.finanzas.contab.model.DAO.PlanDeCuentasDAO;

public class PlanDeCuentasService {

    private PlanDeCuentasDAO cuentas_dao;
    private Vector elementos;
    private Hashtable hashtable;

    /** Creates a new instance of PlanDeCuentasService */
    public PlanDeCuentasService () {
        cuentas_dao = new PlanDeCuentasDAO();
    }
    public PlanDeCuentasService (String dataBaseName) {
        cuentas_dao = new PlanDeCuentasDAO(dataBaseName);
    }

    /**
     * Metodo: getCuentas, permite retornar un objeto de registros de cuentas.
     * @autor : LREALES
     * @param : -
     * @version : 1.0
     */
    public PlanDeCuentas getCuentas()throws SQLException{

        return cuentas_dao.getCuentas();

    }

    /**
     * Metodo: setCuentas, permite obtener un objeto de registros de cuentas.
     * @autor : LREALES
     * @param : objeto
     * @version : 1.0
     */
    public void setCuentas ( PlanDeCuentas cuentas )throws SQLException {

        cuentas_dao.setCuentas( cuentas );

    }

    /**
     * Metodo: getVec_cuentas, permite retornar un vector de registros de cuentas.
     * @autor : LREALES
     * @param : -
     * @version : 1.0
     */
    public Vector getVec_cuentas()throws SQLException{

        return cuentas_dao.getVec_cuentas();

    }

    /**
     * Metodo: setVec_cuentas, permite obtener un vector de registros de cuentas.
     * @autor : LREALES
     * @param : vector
     * @version : 1.0
     */
    public void setVec_cuentas( Vector PlanDeCuentas )throws SQLException{

        cuentas_dao.setVec_cuentas( PlanDeCuentas );

    }

    /**
    * Metodo insertCuenta, ingresa las cuentas ( PlanDeCuentas ).
    * @autor : LREALES
    * @see insertCuenta - PlanDeCuentasDAO
    * @param : objeto
    * @version : 1.0
    */
    public void insertCuenta( PlanDeCuentas cuentas ) throws SQLException{

        cuentas_dao.setCuentas( cuentas );
        cuentas_dao.insertCuenta();

    }

    /**
    * Metodo searchCuenta, permite buscar una cuenta dado unos parametros
    * @autor : LREALES
    * @see searchCuenta - PlanDeCuentasDAO
    * @param : el distrito y el numero de la cuenta
    * @version : 1.0
    */
    public void searchCuenta( String distrito, String cuenta )throws SQLException{

        cuentas_dao.searchCuenta( distrito, cuenta );

    }

    /**
    * Metodo existCuenta, permite verificar si existe una cuenta dada
    * @autor : LREALES
    * @see existCuenta - PlanDeCuentasDAO
    * @param : el distrito y el numero de la cuenta
    * @version : 1.0
    */
    public boolean existCuenta( String distrito, String cuenta ) throws SQLException{

        return cuentas_dao.existCuenta( distrito, cuenta );

    }

    /**
    * Metodo listCuenta, lista la informacion de las cuentas
    * @see listCuenta - PlanDeCuentasDAO
    * @param : -
    * @version : 1.0
    */
    public void listCuenta()throws SQLException{

        cuentas_dao.listCuenta();

    }

    /**
    * Metodo updateCuenta, permite actualizar una cuenta dados unos parametros
    * @autor : LREALES
    * @see updateCuenta - PlanDeCuentasDAO
    * @param : objeto
    * @version : 1.0
    */
    public void updateCuenta( PlanDeCuentas cuentas )throws SQLException{

        cuentas_dao.setCuentas( cuentas );
        cuentas_dao.updateCuenta();

    }

    /**
    * Metodo anularCuenta, permite anular una cuenta de la bd
    * @autor : LREALES
    * @see anularCuenta - PlanDeCuentasDAO
    * @param : objeto
    * @version : 1.0
    */
    public void anularCuenta( PlanDeCuentas cuentas )throws SQLException{

        cuentas_dao.setCuentas( cuentas );
        cuentas_dao.anularCuenta();

    }

    /**
     * Metodo: searchDetalleCuenta, permite listar las cuentas por detalles dados unos parametros.
     * @autor : LREALES
     * @see searchDetalleCuenta - PlanDeCuentasDAO
     * @param : el distrito, y el numero de la cuenta.
     * @version : 1.0
     */
    public void searchDetalleCuenta ( String distrito, String cuenta ) throws SQLException{

        cuentas_dao.searchDetalleCuenta( distrito, cuenta );

    }

    /**
    * Metodo existAgencia, permite verificar si existe una agencia dada
    * @autor : LREALES
    * @see existAgencia - PlanDeCuentasDAO
    * @param : el codigo
    * @version : 1.0
    */
    public boolean existAgencia( String codigo ) throws SQLException{

        return cuentas_dao.existAgencia( codigo );

    }

    /**
    * Metodo existUnidadDeNegocio, permite verificar si existe una unidad de negocio dada
    * @autor : LREALES
    * @see existUnidadDeNegocio - PlanDeCuentasDAO
    * @param : el codigo
    * @version : 1.0
    */
    public boolean existUnidadDeNegocio( String codigo ) throws SQLException{

        return cuentas_dao.existUnidadDeNegocio( codigo );

    }

    /**
    * Metodo existSeccion, permite verificar si existe una seccion dada
    * @autor : LREALES
    * @see existSeccion - PlanDeCuentasDAO
    * @param : el codigo
    * @version : 1.0
    */
    public boolean existSeccion( String codigo ) throws SQLException{

        return cuentas_dao.existSeccion( codigo );

    }

    /**
    * Metodo existCliente, permite verificar si existe un cliente dado
    * @autor : LREALES
    * @see existCliente - PlanDeCuentasDAO
    * @param : el codigo
    * @version : 1.0
    */
    public boolean existCliente( String codigo ) throws SQLException{

        return cuentas_dao.existCliente( codigo );

    }

    /**
    * Metodo existElementoDelGasto, permite verificar si existe un elemento del gasto dado
    * @autor : LREALES
    * @see existElementoDelGasto - PlanDeCuentasDAO
    * @param : el codigo
    * @version : 1.0
    */
    public boolean existElementoDelGasto( String codigo ) throws SQLException{

        return cuentas_dao.existElementoDelGasto( codigo );

    }
        /**
     *Metodo que obtiene las cuentas filtrada por el nombre corto
     *@autor: David Pi�a
     *@param vlr Valor del nombre corto
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void obtenerCuentasNombreCorto( String vlr ) throws SQLException{
        cuentas_dao.obtenerCuentasNombreCorto( vlr );
    }

    /**
     * Getter for property elementos.
     * @return Value of property elementos.
     */
    public java.util.Vector getElementos() {
        return elementos;
    }

    /**
     * Setter for property elementos.
     * @param elementos New value of property elementos.
     */
    public void setElementos(java.util.Vector elementos) {
        this.elementos = elementos;
    }

    /**
     * Getter for property hashtable.
     * @return Value of property hashtable.
     */
    public java.util.Hashtable getHashtable() {
        return hashtable;
    }

    /**
     * Setter for property hashtable.
     * @param hashtable New value of property hashtable.
     */
    public void setHashtable(java.util.Hashtable hashtable) {
        this.hashtable = hashtable;
    }


    /**
    * Metodo insertCuenta, ingresa las cuentas ( PlanDeCuentas ).
    * @autor : AMATURANA
    * @see insertCuenta - PlanDeCuentasDAO
    * @param : objeto
    * @version : 1.0
    */
    public String insertCuentaQuery( PlanDeCuentas cuentas ) throws SQLException{
        cuentas_dao.setCuentas( cuentas );
        return cuentas_dao.insertCuentaQuery();

    }



    /**
    * Metodo reg_status_Cuenta, permite verificar si existe una cuenta dada esta anulada o no
    * @autor : LREALES
    * @see reg_status_Cuenta - PlanDeCuentasDAO
    * @param : el distrito y el numero de la cuenta
    * @version : 1.0
    */
    public String reg_status_Cuenta( String distrito, String cuenta ) throws SQLException{

        return cuentas_dao.reg_status_Cuenta( distrito, cuenta );

    }




     /**
     * Metodo:          consultaCuentaModulo
     * Descripcion :    M�todo que permite buscar una cuenta, retornando un objeto tipo planDeCluenta
     *                  para saber si esta cuenta esta correcta para este modulo, es necesario verificar que
     *                  getDetalle, getsubledger y getPerteneceAmodulo sean 'S'
     * @autor :         Diogenes Bastidas
     * @param:          distrito y numero de la cuenta, modulo
     * @return:         consultaCuentaModulo
     * @throws:         SQLException
     * @version :       1.0
     */
    public PlanDeCuentas consultaCuentaModulo ( String distrito, String cuenta, int modulo  )throws SQLException{
        PlanDeCuentas cuent = new PlanDeCuentas();
        String [] modulos = new String [10];
        cuentas_dao.searchCuenta( distrito, cuenta );
        cuent = cuentas_dao.getCuentas();
        if (cuent!=null) {
        modulos[0] = cuent.getModulo1();
        modulos[1] = cuent.getModulo2();
        modulos[2] = cuent.getModulo3();
        modulos[3] = cuent.getModulo4();
        modulos[4] = cuent.getModulo5();
        modulos[5] = cuent.getModulo6();
        modulos[6] = cuent.getModulo7();
        modulos[7] = cuent.getModulo8();
        modulos[8] = cuent.getModulo9();
        modulos[9] = cuent.getModulo10();
        cuent.setPerteneceAmodulo(modulos[modulo-1]);
        }
        return cuent;
    }
      /**
    * @autor : LFRIERI
    * @param : el codigo
    * @version : 1.0
    */
    public String NombreAgencia( String codigo ) throws SQLException{
        return cuentas_dao.NombreAgencia( codigo );
    }

    /**
    * @autor : LFRIERI
    * @param : el codigo
    * @version : 1.0
    */
    public String NombreUnidad( String codigo ) throws SQLException{
        return cuentas_dao.NombreUnidadDeNegocio(codigo);
    }


    /**
    * @autor : LFRIERI
    * @param : el codigo
    * @version : 1.0
    */
    public String NombreCliente( String codigo ) throws SQLException{
        return cuentas_dao.NombreCliente(codigo);
    }

    /**
    * @autor : LFRIERI
    * @param : el codigo
    * @version : 1.0
    */
    public String NombreElemento( String codigo ) throws SQLException {
        return cuentas_dao.NombreElementoDelGasto(codigo);
    }

         /**
     * Getter for property lista.
     * @return Value of property lista.
     */
    public java.util.LinkedList getLista() {
        return cuentas_dao.getLista();
    }

    /**
     * Setter for property lista.
     * @param lista New value of property lista.
     */
    public void setLista(java.util.LinkedList lista) {
        cuentas_dao.setLista(lista);
    }

     /**
     * Metodo:          consultaCuenta
     * Descripcion :    M�todo que permite buscar una cuenta dado unos parametros.
     * @autor :         JDERLAROSA
     * @param:          distrito y numero de la cuenta.
     * @return:         -
     * @throws:         SQLException
     * @version :       1.0
     */
    public void consultaCuenta ( String distrito, String cuenta,String Tip,String Age,String Uni,String Cli,String Ele )throws SQLException{

        cuentas_dao.consultaCuenta( distrito, cuenta,Tip,Age,Uni,Cli,Ele);

    }
     public void consultaCuentaSinComodin ( String distrito, String cuenta, String cuenta2)throws SQLException{

        cuentas_dao.consultaCuentaSinComodin( distrito, cuenta, cuenta2);

    }

     public String NombreSeccion( String codigo ) throws SQLException {//20100824
        return cuentas_dao.NombreSeccion(codigo);//20100824
    }//20100824

}