/********************************************************************
 *  Nombre Clase.................   CuentasService.java
 *  Descripci�n..................   Service de la tabla cuentas
 *  Autor........................   Ing. Leonardo Parody Ponce
 *  Fecha........................   26.12.2005
 *  Versi�n......................   1.0
 *  Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/
package com.tsp.finanzas.contab.model.services;
import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.finanzas.contab.model.beans.*;
import com.tsp.finanzas.contab.model.DAO.*;


public class CuentasService {
    
    private CuentasDAO DAO;
    
    private TreeMap cuentas = new TreeMap();
    
    /** Creates a new instance of CuentasService */
    
    public CuentasService() {
        DAO = new CuentasDAO();
    }
    public CuentasService(String dataBaseName) {
        DAO = new CuentasDAO(dataBaseName);
    }
    
    /**
     * Metodo IngresarCuentas , Metodo que ingresa una cuenta
     * @autor : Ing. Leonardo Parody
     * @param : Cuentas cuenta
     * @version : 1.0
     */
    public void IngresarCuentas(Cuentas cuenta) throws SQLException{
        try{
            DAO.InsertarCuenta(cuenta);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    /**
     * Metodo ModificarCuentas, Metodo que modifica una cuenta
     * @autor : Ing. Leonardo Parody
     * @param : Cuentas cuenta
     * @version : 1.0
     */
    public void ModificarCuentas(Cuentas cuenta) throws SQLException{
        try{
            DAO.ModificarCuenta(cuenta);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    /**
     * Metodo AnularCuentas, Metodo que anula una cuenta
     * @autor : Ing. Leonardo Parody
     * @param : String cuenta
     * @version : 1.0
     */
    public void AnularCuentas(String cuenta) throws SQLException{
        try{
            DAO.AnularCuenta(cuenta);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    /**
     * Metodo ConsultarCuentas, Metodo que consulta una cuenta
     * @autor : Ing. Leonardo Parody
     * @param : String cuenta
     * @version : 1.0
     */
    public void ConsultarCuentas(String cuenta) throws SQLException{
        try{
            DAO.ConsultarCuenta(cuenta);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    /**
     * Metodo getCuenta, Metodo que retorna una cuenta
     * @autor : Ing. Leonardo Parody
     * @version : 1.0
     */
    public Cuentas getCuenta(){
        return DAO.getCuenta();
    }
    /**
     * Metodo ExisteCuentas, Metodo que consulta una cuenta
     * @autor : Ing. Leonardo Parody
     * @param : String cuenta
     * @version : 1.0
     */
    public boolean ExisteCuentas(String cuenta) throws SQLException{
        try{
            return DAO.ExisteCuenta(cuenta);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    /**
     * Metodo ListarCuentas, Metodo que consulta una cuenta
     * @autor : Ing. Leonardo Parody
     * @version : 1.0
     */
    public List ListarCuentas(String iniciocuenta) throws SQLException{
        try{
            return DAO.ListarCuentas(iniciocuenta);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    /**
     * Metodo searchDatosCuentas, Busca los datos de la tabla cuentas
     * @autor : Leonardo Parody
     * @version : 1.0
     */
    public void searchDatosCuentas() throws SQLException {
        DAO.buscarListaCodigo();
        cuentas = DAO.getCuentaTree();
    }
    /**
     * Getter for property cuentas.
     * @return Value of property cuentas.
     */
    public java.util.TreeMap getCuentas() throws SQLException {
        return cuentas;
    } 
    
}
