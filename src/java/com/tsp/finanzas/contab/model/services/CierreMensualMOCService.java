/************************************************************************
* Nombre ....................CierreMensualMOCService.java               *
* Descripci�n................Clase que maneja los servicios al model    *
*                            relacionados con el cierre mensual de      *
*                            los modulos operativos y contables.        *
* Autor......................LREALES                                    *
* Fecha Creaci�n.............28 de Junio de 2006, 05:45 PM              *
* Versi�n....................1.0                                        *
* Coyright...................Transportes Sanchez Polo S.A.              *
************************************************************************/

package com.tsp.finanzas.contab.model.services;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.finanzas.contab.model.beans.*;
import com.tsp.finanzas.contab.model.DAO.CierreMensualMOCDAO;

public class CierreMensualMOCService {
    
    private CierreMensualMOCDAO cierre_dao;
    
    /** Creates a new instance of CierreMensualMOCService */
    public CierreMensualMOCService () {
        cierre_dao = new CierreMensualMOCDAO();
    }
    public CierreMensualMOCService (String dataBaseName) {
        cierre_dao = new CierreMensualMOCDAO(dataBaseName);
    }
    
    /**
     * Metodo: getPeriodo, permite retornar un objeto de registros de periodos.
     * @autor : LREALES
     * @param : -
     * @version : 1.0
     */    
    public PeriodoContable getPeriodo()throws SQLException{
        
        return cierre_dao.getPeriodo();
        
    }
    
    /**
     * Metodo: setPeriodo, permite obtener un objeto de registros de periodos.
     * @autor : LREALES
     * @param : objeto
     * @version : 1.0
     */
    public void setPeriodo ( PeriodoContable periodo )throws SQLException {
        
        cierre_dao.setPeriodo( periodo );
        
    }
    
    /**
     * Metodo: getVec_periodo, permite retornar un vector de registros de periodos.
     * @autor : LREALES
     * @param : -
     * @version : 1.0
     */    
    public Vector getVec_periodo()throws SQLException{
        
        return cierre_dao.getVec_periodo();
        
    }
    
    /**
     * Metodo: setVec_periodo, permite obtener un vector de registros de periodos.
     * @autor : LREALES
     * @param : vector
     * @version : 1.0
     */    
    public void setVec_periodo( Vector PeriodoContable )throws SQLException{
        
        cierre_dao.setVec_periodo( PeriodoContable );
        
    }
     
   /**
    * Metodo existePeriodo, permite verificar si existe un periodo dado.
    * @autor : LREALES
    * @see existePeriodo - CierreMensualMOCDAO
    * @param : distrito, a�o del periodo y mes del periodo.
    * @version : 1.0
    */       
    public boolean existePeriodo( String distrito, String anio, String mes ) throws SQLException{
        
        return cierre_dao.existePeriodo( distrito, anio, mes );
       
    } 
    
    /**
    * Metodo buscarPeriodo, permite buscar un periodo dado.
    * @autor : LREALES
    * @see buscarPeriodo - CierreMensualMOCDAO
    * @param : distrito, a�o del periodo y mes del periodo.
    * @version : 1.0
    */      
    public void buscarPeriodo( String distrito, String anio, String mes )throws SQLException{
               
        cierre_dao.buscarPeriodo( distrito, anio, mes );
        
    }
    
    /**
    * Metodo cerrarPeriodo, permite cerrar un periodo en la bd.
    * @autor : LREALES
    * @see cerrarPeriodo - CierreMensualMOCDAO
    * @param : objeto
    * @version : 1.0
    */      
    public void cerrarPeriodo( PeriodoContable periodo )throws SQLException{
        
        cierre_dao.setPeriodo( periodo );
        cierre_dao.cerrarPeriodo();
        
    }
    
}