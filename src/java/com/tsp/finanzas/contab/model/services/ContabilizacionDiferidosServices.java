/***********************************
 * Nombre Clase ............. Contabilizacion Negocios Services.java
 * Descripci�n  .. . . . . .  Permitimos realizar los servicios para la contabilizacion de negocios
 * Autor  . . . . . . . . . . ING ROBERTO ROCHA P
* Fecha . . . . . . . . . . . 06/09/2006
 * versi�n . . . . . . . . .  1.0
 * Copyright ...............  FINTRAVALORES S.A.
 ************************************/



package com.tsp.finanzas.contab.model.services;


import java.io.*;
import java.util.*;
import com.tsp.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.util.LogWriter;
import com.tsp.finanzas.contab.model.beans.*;
import com.tsp.finanzas.contab.model.DAO.ContabilizacionDiferidosDAO;
import com.tsp.finanzas.contab.model.DAO.ContabilizacionFacturasCLDAO;
import com.tsp.finanzas.contab.model.DAO.ComprobantesDAO;
import com.tsp.finanzas.contab.model.DAO.MovAuxiliarDAO;
import com.tsp.operation.model.DAOS.GestionConveniosDAO;
import com.tsp.operation.model.DAOS.Tipo_impuestoDAO;
import com.tsp.operation.model.TransaccionService;
import com.tsp.operation.model.services.DirectorioService;
import com.tsp.operation.model.services.TasaService;
import com.tsp.operation.model.beans.Tasa;
import com.tsp.operation.model.beans.*;

public class ContabilizacionDiferidosServices {


    private  ContabilizacionDiferidosDAO  dao;
    private  ComprobantesDAO             comproDAO;
    private  MovAuxiliarDAO              movDAO;
    private  List                        listaContable;
    private  boolean                     process;
    private String     IVA               = "IVA";
    private String     TIPO_FACTURA      = "010";
    private String     TIPO_FACTURA_AMD  = "FAA";
    private String     DIF_CAMBIO_OP     = "99";
    private LogWriter logWriter;
    public  List      listaErrores;
    public  String    ESTADO             =  "";
    public  int       CANT_ERROES        =  0;
    public  List                        listitems;
    private     int     items;//los items del comprobante;
    private     double      totdeb;
    private     double      totcred;
    private     double      interes;
    private     double      ingresos;
    private final String TIPO_OC         = "001";


    private final String FACTURA         = "010";
    private final String NC              = "035";
    private final String ND              = "036";




    public ContabilizacionDiferidosServices() {
        dao            = new  ContabilizacionDiferidosDAO();
        comproDAO      = new  ComprobantesDAO();
        movDAO         = new  MovAuxiliarDAO();
        listaContable  = new LinkedList();
        process        = false;
        listitems       =   new LinkedList();
        listaErrores   = new LinkedList();
        ESTADO         =  "";
        CANT_ERROES    =  0;
    }
    public ContabilizacionDiferidosServices(String dataBaseName) {
        dao            = new  ContabilizacionDiferidosDAO(dataBaseName);
        comproDAO      = new  ComprobantesDAO(dataBaseName);
        movDAO         = new  MovAuxiliarDAO(dataBaseName);
        listaContable  = new LinkedList();
        process        = false;
        listitems       =   new LinkedList();
        listaErrores   = new LinkedList();
        ESTADO         =  "";
        CANT_ERROES    =  0;
    }




    public void reset(){
        listaContable  = new LinkedList();
    }





    /**
     * M�todo que carga los negocios que no esten contabilizados
     * @autor.......rarp
     * @throws......Exception
     * @version.....1.0.
     **/
    public void contabilizar(String distrito, String ayer, Usuario usuario)throws Exception{
        try{
            String periodo = ayer.substring(0,7).replaceAll("-","");
            String hoy     =  Utility.getHoy("-");
            listaErrores   = new LinkedList();
            ESTADO         =  "";
            CANT_ERROES    =  0;
            listaContable  =  dao.getDiferidosNoContabilizados(hoy );
            // COMPROBANTES : NEGOCIOS
            for(int i=0;i<listaContable.size();i++)
            {
                Comprobantes  comprobante = (Comprobantes)listaContable.get(i);
                GestionConveniosDAO convenioDao = new GestionConveniosDAO(this.dao.getDatabaseName());
                Convenio convenio = convenioDao.buscar_convenio(usuario.getBd(), String.valueOf(comprobante.getId_convenio()));
                totdeb=0;
                totcred=0;
                items=0;
                int sec    = comproDAO.getGrupoTransaccion();
                double iva=0;
                
                listitems=new LinkedList();
                comprobante.setFechadoc     ( ayer     );//fecha del comprobante
                comprobante.setTipo_operacion("005");
                comprobante.setGrupo_transaccion(sec);
                comprobante.setUsuario      (usuario.getLogin());
                comprobante.setAprobador    (usuario.getLogin());
                comprobante.setPeriodo  (periodo);
                comprobante.setSucursal("OP");
                comprobante.setTipo("C");
                totdeb=comprobante.getTotal_debito();
                totcred=comprobante.getTotal_credito();
                //items
                itemscopy(comprobante);//1era cta
                               
                //validamos el iva  para el cat
                if(comprobante.getAplica_iva().equals("S")){
                     Tipo_impuestoDAO timpuestoDao = new Tipo_impuestoDAO(usuario.getBd());
                     Tipo_impuesto impuesto = timpuestoDao.buscarImpuestoxCodigoxFecha(convenio.getDstrct(), convenio.getImpuesto(),Util.getFechahoy());
                     iva=(comprobante.getTotal_debito()*impuesto.getPorcentaje1())/(100+impuesto.getPorcentaje1());
                     itemscopy2(comprobante,impuesto.getCod_cuenta_contable(),iva); //En auxiliar se guarda la cuenta que se obtiene del convenio     
                }  
                
                itemscopy2(comprobante,comprobante.getAuxiliar(),totdeb-iva); //En auxiliar se guarda la cuenta que se obtiene del convenio
                
                //fin items
                comprobante.setTotal_items(items);
                comprobante.setTotal_debito(totdeb);
                comprobante.setTotal_credito(totdeb);
                comprobante.setItems(listitems);
                List ins = new LinkedList();

                ins.add( comprobante );
                insertar( ins, usuario.getLogin() );


            } // For facturas

            ESTADO  =  "Comprobantes generados : " + ( listaContable.size() - CANT_ERROES  ) + ", Comprobantes con Errores : "+ CANT_ERROES;
            if( CANT_ERROES >0  ){
                 LogErrores( listaErrores , usuario );
            }

        }catch(Exception e){
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }

    }


    ///metodo que copia los datos de un comprobante a sus items

    public void itemscopy(Comprobantes neg)throws Exception{
        Comprobantes item=new Comprobantes();
        try{
         item.setTipo("D");
         item.setDstrct(neg.getDstrct());
         item.setTipodoc(neg.getTipodoc());
         item.setNumdoc(neg.getNumdoc());
         item.setGrupo_transaccion(neg.getGrupo_transaccion());
         item.setPeriodo(neg.getPeriodo());
         item.setTercero(neg.getTercero());
         item.setDocumento_interno(neg.getNumdoc());
         item.setDocrelacionado(neg.getNumdoc());
         item.setTipo_operacion(neg.getTipo_operacion());
         item.setAbc(neg.getAbc());
         item.setUsuario(neg.getUsuario());
         item.setBase(neg.getBase());
         item.setCuenta(neg.getCuenta());
         item.setDetalle(neg.getRef_1());
         item.setTotal_debito(neg.getTotal_debito());
         item.setTotal_credito(0.0);
         item.setAuxiliar("");
  	 item.setTdoc_rel(neg.getTdoc_rel());
  	 item.setNumdoc_rel(neg.getNumdoc_rel());
        }catch(Exception e){
            throw new Exception( e.getMessage() );
        }
        items++;//contar los items
        listitems.add(item);//falta agregar a lista de items
    }


    /// items rd
    public void itemscopy2(Comprobantes neg,String parm,double parm2)throws Exception{
        Comprobantes item=new Comprobantes();
        try{
         item.setTipo("D");
         item.setDstrct(neg.getDstrct());
         item.setTipodoc(neg.getTipodoc());
         item.setNumdoc(neg.getNumdoc());
         item.setGrupo_transaccion(neg.getGrupo_transaccion());
         item.setPeriodo(neg.getPeriodo());
         item.setTercero(neg.getTercero());
         item.setDocumento_interno(neg.getNumdoc());
         item.setDocrelacionado(neg.getNumdoc());
         item.setTipo_operacion(neg.getTipo_operacion());
         item.setAbc(neg.getAbc());
         item.setUsuario(neg.getUsuario());
         item.setBase(neg.getBase());
         item.setCuenta(parm);
         item.setDetalle(dao.getAccountDES(parm));
         item.setTotal_credito(parm2);
         item.setTotal_debito(0.0);
         item.setAuxiliar("");
 	 item.setTdoc_rel(neg.getTdoc_rel());
 	 item.setNumdoc_rel(neg.getNumdoc_rel());
         totcred=parm2;
        }catch(Exception e){
            throw new Exception( e.getMessage() );
        }
        items++;//contar los items
        listitems.add(item);//falta agregar a lista de items
    }


    /**
     * M�todo que inserta el comprobante y comprodet
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public  void insertar(List lista, String user)throws Exception{
        try{

            TransaccionService  scv = new TransaccionService(this.dao.getDatabaseName());
            System.out.println("Insert");
            for(int i=0;i<lista.size();i++){
                Comprobantes  comprobante = (Comprobantes) lista.get(i);
                String eva =  evaluarComprobante( comprobante );
                System.out.println("EVA "+ eva);
                if( eva.equals("") ){

                    List items = comprobante.getItems();
                    List aux   = new LinkedList();
                    int  sw    = 0;


                 // Validamos los items:
                    if( items!= null )
                        for(int j=0;j<items.size();j++){
                            Comprobantes  comprodet = (Comprobantes)items.get(j);
                            String evaDet = evaluarComprobante(comprodet);
                            if( evaDet.equals("") )
                                aux.add( comprodet );
                            else{
                                comprobante.setComentario( "Uno de sus item presenta irregularidad" );
                                listaErrores.add( comprobante );     // Guardamos la factura
                                CANT_ERROES ++;

                                comprodet.setComentario( evaDet );   // Guardamos el item
                                listaErrores.add( comprodet );

                                sw = 1;
                                break;
                            }
                        }

                    if ( sw == 0  ){

                        items = aux;  // Items q cumplen para ser insertados

                        if( items!= null  && items.size()>0){

                            // Tomamos la secuencia:
                            int sec    = comproDAO.getGrupoTransaccion();


                            // Realizamos los insert, update:
                            comprobante.setGrupo_transaccion( sec );
                            ArrayList<String> sqList=new ArrayList<>();
                            sqList.add(dao.InsertComprobante       (comprobante , user ));
                            sqList.addAll(dao.InsertComprobanteDetalle(items, sec, user));

                                    //actualizar tabla de negocios

                            // Ejecutamos el SQL
                            if(!sqList.isEmpty()){
                                try{
                                    scv.crearStatement();
                                    for (String sql : sqList) {
                                     scv.getSt().addBatch(sql);   
                                    }
                                    scv.execute();
                                    dao.updatecont(comprobante.getUsuario(),comprobante.getGrupo_transaccion(),comprobante.getPeriodo(),comprobante.getNumdoc());
                                }catch(Exception e){
                                    //dao.deleteComprobante( comprobante );
                                    comprobante.setComentario("Error al momento de insertar : " + e.getMessage() );
                                    CANT_ERROES++;
                                    listaErrores.add( comprobante );
                                }
                            }

                        }else{
                            comprobante.setComentario("No presenta items validos");
                            CANT_ERROES++;
                            listaErrores.add(  comprobante  );
                        }


                   }


                }else{
                    comprobante.setComentario( eva );
                    listaErrores.add(  comprobante  );
                    CANT_ERROES ++;
                }

            }

        }catch(Exception e){
            e.printStackTrace();
            throw new Exception( " INSERTAR COMPROBANTE FACTURAS : "+ e.getMessage() );
        }
    }








    /**
     * M�todo que evalua datos v�lidos del comprobante y comprodet
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public String evaluarComprobante(Comprobantes  comprobante)throws Exception{
       String msj =  "";
        try{

            // Campos comunes:
            if ( comprobante.getDstrct().       trim().equals(""))    msj += " No presenta distrito";
            if ( comprobante.getTipodoc().      trim().equals(""))    msj += " No presenta tipo de documento";
            if ( comprobante.getNumdoc().       trim().equals(""))    msj += " No presenta n�mero de documento";
            if ( comprobante.getPeriodo().      trim().equals(""))    msj += " No presenta periodo de contabilizaci�n";
            if ( comprobante.getDetalle().      trim().equals(""))    msj += " No presenta una descripci�n o detalle";
            if ( comprobante.getBase().         trim().equals(""))    msj += " No presenta base";


            String tipo = comprobante.getTipo();

            if ( tipo.equals(  "C" )  ){

                if ( comprobante.getTotal_debito() != comprobante.getTotal_credito() )  msj += " Est� descuadrado en valores debitos y cr�ditos";
                if ( comprobante.getSucursal().     trim().equals(""))                  msj += " No presenta sucursal";
                if ( comprobante.getFechadoc().     trim().equals(""))                  msj += " No presenta fecha de documento";
                if ( comprobante.getTercero().      trim().equals(""))    msj += " No presenta tercero";
                if ( comprobante.getMoneda().       trim().equals(""))        msj += " No presenta moneda";
                if ( comprobante.getAprobador().    trim().equals(""))        msj += " No presenta aprobador";

            }
            else{

                if ( comprobante.getCuenta().       trim().equals(""))    msj += " No presenta cuenta";
                else{
                    String cta  = comprobante.getCuenta();
                    Hashtable  cuenta =  movDAO.getCuenta(comprobante.getDstrct(), cta  );

                    if ( cuenta != null){

                        String  requiereAux      = (String) cuenta.get("auxiliar");
                        String  requiereTercero  = (String) cuenta.get("tercero");

                        // Tercero.
                        if ( requiereTercero .equals("S")   &&  comprobante.getTercero().  trim().equals(""))
                            msj += " La cuenta "+ cta +" requiere tercero  y el item no presenta tercero";

                        // Auxiliar.
                        if ( requiereAux.equals("S") ){

                             if(  comprobante.getAuxiliar(). trim().equals("")  )
                                   msj += " La cuenta "+ cta +" requiere auxiliar y el item no presenta auxiliar";

                             if( !dao.existeCuentaSubledger( comprobante.getDstrct() , cta , comprobante.getAuxiliar()  )   )
                                   msj += "La cuenta " + cta + " no tiene el subledger "+  comprobante.getAuxiliar()  +" asociado";
                        }




                    }else
                        msj += " La cuenta "+ cta +" no existe o no est� activa";

                }

            }



        }catch(Exception e){
            e.printStackTrace();
            throw new Exception( e.getMessage() );
        }
        return msj;
    }


    /**
     * M�todo que genera el Log de Errores
     * @autor.......fvillacob
     * @throws......Exception
     * @version..1
     **/
    public void LogErrores(List lista, Usuario user)throws Exception{
        try{

            // Ruta del archivo:
            String       fecha = Util.getFechaActual_String(6).replaceAll("/|:| ","");

            DirectorioService  svc   =  new DirectorioService();
            svc.create( user.getLogin() );
            String             url   =  svc.getUrl() + user.getLogin() + "/"+user.getBd()+"_CONTADIFERIDOS" +  fecha +".txt";


            PrintWriter pw = new PrintWriter( new BufferedWriter( new FileWriter(url) )  );
            LogWriter logM = new LogWriter("Contabilizaci�n de Diferidos",LogWriter.INFO, pw);

            try{
                logM.log("Inicio de la Contabilizaci�n....",logM.INFO);

                for(int j=0;j<lista.size();j++){
                    Comprobantes  comprodet = (Comprobantes)lista.get(j);
                    String msj  =  comprodet.getNumdoc()   +" "+ comprodet.getDetalle()  +" CAUSA : "+ comprodet.getComentario();
                    logM.log(msj,logM.INFO);
                }

                logM.log("Finalizaci�n...",logM.INFO);

            }catch (Exception e){
                logM.log("\n\nError en Contabilizaci�n Diferidos: "+e.getMessage(),logM.ERROR);
            }
            finally{
                pw.close();
            }

        }catch(Exception e){
            e.printStackTrace();
            throw new Exception( e.getMessage() );
        }
    }


    // SET AND GET : -------------------------------------------------------------------------


    /**
     * Getter for property listaContable.
     * @return Value of property listaContable.
     */
    public java.util.List getListaContable() {
        return listaContable;
    }

    /**
     * Setter for property listaContable.
     * @param listaContable New value of property listaContable.
     */
    public void setListaContable(java.util.List listaContable) {
        this.listaContable = listaContable;
    }





    /**
     * Getter for property process.
     * @return Value of property process.
     */
    public boolean isProcess() {
        return process;
    }

    /**
     * Setter for property process.
     * @param process New value of property process.
     */
    public void setProcess(boolean process) {
        this.process = process;
    }





    /**
     * M�todo que descontabilizar
     * @autor.......jescandon
     * @throws......Exception
     * @version.....1.0.
     **/
    public String Descontabilizar(Vector VComprobantes, String user)throws Exception{
        String msjError    = "";

        List listaErrores  = new LinkedList();
        List listaGrabada  = new LinkedList();


        try{

            String sql         = "";
            Iterator it        = VComprobantes.iterator();
            Vector   VComprobantesDet;

            while( it.hasNext() ){
                Comprobantes aux = (Comprobantes)it.next();
                VComprobantesDet = dao.VectorComprobantes(aux);

                Iterator it2 = VComprobantesDet.iterator();

                while( it2.hasNext()){
                    Comprobantes auxdet = (Comprobantes)it2.next();
                    sql  += dao.DescontabilizarMayor(auxdet);
                    sql  += dao.UpdateFechaAplicacion(auxdet);
                    if( auxdet.getAuxiliar().length() > 0 ){
                        sql += dao.DescontabilizarMayoSubledger(auxdet);
                    }

                }//fin ComprobantesDet


                // Ejecutamos el SQL
                if(!sql.equals("")){



                    try{
                        TransaccionService  scv = new TransaccionService(this.dao.getDatabaseName());
                        scv.crearStatement();
                        scv.getSt().addBatch(sql);

                        scv.execute();

                        listaGrabada.add(aux);


                    }catch(Exception e){
                        aux.setComentario( e.getMessage() );
                        listaErrores.add(aux);
                        msjError += " No se pudo descontabilizar el comprobante -> " + aux.getNumdoc() + " ";
                    }

                }

                sql = "";

            }//fin Comprobantes



            if( listaErrores.size()>0  ){
                LogErroresDescontabilizar( listaErrores, user  );
                msjError += " Comprobantes grabados "+  listaGrabada.size() +". Hay comprobantes que presentar�n errores, no cumple con alguna condici�n. Puede consultar el log de errores";
            }


        }catch(Exception e){
            throw new Exception( " Descontabilizar : "+ e.getMessage() );
        }

        return msjError;

    }



    public void LogErroresDescontabilizar(List lista, String user)throws Exception{
        try{
            ResourceBundle rb  = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String       ruta  = rb.getString("ruta");
            String       fecha = Util.getFechaActual_String(6).replaceAll("/|:| ","");
            String       url   = ruta + "/exportar/migracion/" + user + "/" + "Descontabilizacion" + user +  fecha +".log";

            PrintWriter pw = new PrintWriter( new BufferedWriter( new FileWriter(url) )  );
            LogWriter logM = new LogWriter("Descontabilizacion",LogWriter.INFO, pw);

            try{
                logM.log("Inicio de la Descontabilizacion....",logM.INFO);

                for(int j=0;j<lista.size();j++){
                    Comprobantes  comprodet = (Comprobantes)lista.get(j);
                    String msj  =  comprodet.getNumdoc()   + " " + " CAUSA : "+ comprodet.getComentario();
                    logM.log(msj,logM.INFO);
                }

                logM.log("Finalizaci�n...",logM.INFO);

            }catch (Exception e){
                logM.log("\n\nError en Contabilizaci�n Facturas: "+e.getMessage(),logM.ERROR);
            }
            finally{
                pw.close();
            }

        }catch(Exception e){
            throw new Exception( e.getMessage() );
        }
    }

     /**
     * M�todo de reversion de comprobantes
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public  String Reversar(Vector lista, String user)throws Exception{
        String msjError    = "";
        try{

            String sql         = "";
            List listaGrabada  = new LinkedList();//Lista de grabacion
            List listaErrores  = new LinkedList();//Lista de errores;

            for(int i=0;i<lista.size();i++){

                Comprobantes  comprobante         = (Comprobantes) lista.get(i);
                ComprobanteFacturas  aux          = this.dao.getComprobantes(comprobante);

                this.dao.InsertComprobante(aux, user );
                int sec    = comproDAO.getGrupoTransaccion();
                aux.setGrupo_transaccion( sec );


                Comprobantes comp = new Comprobantes();
                comp.setTipodoc(aux.getTipodoc());
                comp.setNumdoc(aux.getNumdoc());
                comp.setGrupo_transaccion(comprobante.getGrupo_transaccion());

                List items = this.dao.ListaComprobantes(comp);

                sql       += dao.InsertComprobanteDetalle(items, sec, user );

                aux.setComentario(sql);
                listaGrabada.add( aux );
            }

            // Ejecutamos el SQL
            if(!sql.equals("")){

                try{
                    TransaccionService  scv = new TransaccionService(this.dao.getDatabaseName());
                    scv.crearStatement();
                    scv.getSt().addBatch(sql);
                    scv.execute();

                }catch(Exception e){
                    listaGrabada  = this.dao.deleteComprobanteSQL( listaGrabada );
                    listaErrores.addAll( listaGrabada );
                    listaGrabada = new LinkedList();
                    msjError += "No se pudieron grabar algunos comprobantes hay un error en el batch->";
                    e.printStackTrace();
                }

            }

            if( listaErrores.size()>0  ){
                LogErroresReversar( listaErrores, user  );
                msjError += " Comprobantes grabados "+  listaGrabada.size() +". Hay comprobantes que presentar�n errores, no cumple con alguna condici�n o el query presenta algun error. Puede consultar el log de errores";
            }


        }catch(Exception e){
            throw new Exception( " Reversar : "+ e.getMessage() );
        }

        return msjError;

    }


    public void LogErroresReversar(List lista, String user)throws Exception{
        try{
            ResourceBundle rb  = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String       ruta  = rb.getString("ruta");
            String       fecha = Util.getFechaActual_String(6).replaceAll("/|:| ","");
            String       url   = ruta + "/exportar/migracion/" + user + "/" + "Descontabilizacion" + user +  fecha +".log";

            PrintWriter pw = new PrintWriter( new BufferedWriter( new FileWriter(url) )  );
            LogWriter logM = new LogWriter("Reversion",LogWriter.INFO, pw);

            try{
                logM.log("Inicio de la Reversion....",logM.INFO);

                for(int j=0;j<lista.size();j++){
                    ComprobanteFacturas  comprodet = (ComprobanteFacturas)lista.get(j);
                    String msj  =  comprodet.getNumdoc()   + " " + "Error en el Query : " + comprodet.getComentario();
                    logM.log(msj,logM.INFO);
                }

                logM.log("Finalizaci�n...",logM.INFO);

            }catch (Exception e){
                logM.log("\n\nError en Reversion Facturas: "+e.getMessage(),logM.ERROR);
            }
            finally{
                pw.close();
            }

        }catch(Exception e){
            throw new Exception( e.getMessage() );
        }
    }

    /**
     * Getter for property ESTADO.
     * @return Value of property ESTADO.
     */
    public java.lang.String getESTADO() {
        return ESTADO;
    }

    /**
     * Setter for property ESTADO.
     * @param ESTADO New value of property ESTADO.
     */
    public void setESTADO(java.lang.String ESTADO) {
        this.ESTADO = ESTADO;
    }

}
