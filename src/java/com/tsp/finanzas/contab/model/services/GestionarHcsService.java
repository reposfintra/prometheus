/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.finanzas.contab.model.services;


import com.tsp.finanzas.contab.model.DAO.GestionarHcsDAO;
import com.tsp.finanzas.contab.model.beans.Hc;
import com.tsp.finanzas.contab.model.beans.*;
import com.tsp.finanzas.contab.model.DAO.CmcDAO;


import java.io.*;
import java.sql.*;
import java.util.*;

import java.util.LinkedList;
import java.util.List;


/**
 *
 * @author imorales
 */
public class GestionarHcsService
{
    private GestionarHcsDAO hcDao;
     private CmcDAO cmcDao;
    /** Creates a new instance of IdentidadService */
    public GestionarHcsService() {
        hcDao = new GestionarHcsDAO();
       cmcDao = new CmcDAO();

    }
    public GestionarHcsService(String dataBaseName) {
        hcDao = new GestionarHcsDAO(dataBaseName);
       cmcDao = new CmcDAO(dataBaseName);

    }
    /**
     * Metodo obtIdentidad, retorna el objeto de Identidades
     * @param:
     * @autor : Ing. Diogenes Bastidas Morales
     * @see: obtIdentidad - IdentidadDAO()
     * @version : 1.0
     */
    public List<Hc> ListarHc() throws SQLException {

          return  hcDao.ListarHc();

    }




    public Hc Buscar(String codigo) throws SQLException {

          return  hcDao.BuscarHc(codigo);

    }





    /********************************* Inserar un hc**************************************************/
     public Vector InsertarHc(List lista_cmc, Hc hc)throws SQLException{
        Vector queries = new Vector();
       /* queries.add(dao.eliminarOfertaEjecucion(((Actividades)listaActividades.get(0))));
        queries.add(dao.eliminarOfertaSeguimiento(ofertaSeg));*/
        //Guardar observacion y totales
        queries.add(hcDao.InsertarHc(hc));
        //guardar seguimiento para cada actividad
        for (int i = 0; i < lista_cmc.size(); i++) {

            Cmc cmc = (Cmc)lista_cmc.get(i);
            cmc.setCmc(hc.getTableCode());
            queries.add(cmcDao.Insertar(cmc) );
        }
        return queries;
    }





         /********************************* Actualizar un hc**************************************************/
     public Vector ActualizarHc(List lista_cmc, Hc hc)throws SQLException{
        Vector queries = new Vector();

        queries.add(hcDao.ActualizarHc(hc));
        queries.add(cmcDao.Eliminar(hc.getTableCode()));
        for (int i = 0; i < lista_cmc.size(); i++)
        {

            Cmc cmc = (Cmc)lista_cmc.get(i);
            cmc.setCmc(hc.getTableCode());
            cmc.setUsuario(hc.getUserUpdate());
            cmc.setDstrct("FINV");
            queries.add(cmcDao.Insertar(cmc) );
         }




        return queries;
    }
     
    public boolean ValidarHc(String codigo) throws SQLException {
                  return  hcDao.ValidarHc(codigo);
    }

}