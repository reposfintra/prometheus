/*
 * ContabilizacionNotasAjusteServices.java
 *
 * Created on 24 de junio de 2008, 11:11
 */

package com.tsp.finanzas.contab.model.services;

import com.tsp.finanzas.contab.model.DAO.ContabilizacionNotasAjusteDAO;
import com.tsp.finanzas.contab.model.beans.Comprobantes;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.beans.Ingreso;
import com.tsp.operation.model.beans.Ingreso_detalle;
import com.tsp.operation.model.beans.factura;
import java.util.*;

/**
 *
 * @author  navi
 */
public class ContabilizacionNotasAjusteServices {
    
    ContabilizacionNotasAjusteDAO dao;   
    boolean processRun = false;
        
    /** Creates a new instance of ContabilizacionNotasAjusteServices */
    public ContabilizacionNotasAjusteServices() {
        dao = new  ContabilizacionNotasAjusteDAO();
    }
    public ContabilizacionNotasAjusteServices(String dataBaseName) {
        dao = new  ContabilizacionNotasAjusteDAO(dataBaseName);
    }
    
    public Vector obtenerDetalleIngreso(Ingreso cab) throws Exception {
        return dao.obtenerDetalleIngreso(cab);
    }
    
   
    /**
     * Metodo para obtener la equivalencia contable del tipo del documento
     * @autor mfontalvo
     * @param tipo_documento
     * @return equivalencia contable del tipo de documento
     * @throws Exception.
     */
    public TreeMap obtenerEquivalenciaTipoDoc() throws Exception{
        return dao.obtenerEquivalenciaTipoDoc();
    }
    
    
    /**
     * Metodo para obtener ingresos pendientes por contabilizar
     * @autor mfontalvo
     * @throws Exception.
     * @return Vector, lista de ingresos.
     */    
    public Vector obtenerIngresoSinContabilizar() throws Exception{
        return dao.obtenerIngresoSinContabilizar();
    }
    
    
    /**
     * Metodo para obtener ingresos ya contabilizados , anulados y sin descontabilizar
     * @autor mfontalvo
     * @throws Exception.
     * @return Vector, lista de ingresos.
     */
    public Vector obtenerIngresoSinDescontabilizar() throws Exception {
        return dao.obtenerIngresoSinDescontabilizar();
    } 
    
    
    /**
     * Metodo para guardar el comprobante contable con sus detalles
     * @autor mfontalvo
     * @param c, combrobante a guardar
     * @param in, ingreso a procesar
     * @param in, ingreso q se esta procesando
     * @throws Exception.
     */
    public void saveComprobante (Comprobantes c, Ingreso in) throws Exception {
        dao.saveComprobante(c, in);
    }
    
    
    /**
     * Metodo para buscar un comprobante con sus detalles
     * @autor mfontalvo
     * @param tipo_documento, tipo del documentos del comprobante
     * @param documento, documento del comprobante
     * @transaccion, grupo de la transaccion del comprobante
     * @throws Exception.
     * @return Comprobante contable.
     */
    public Comprobantes getComprobante(String tipo_documento, String documento, int transaccion) throws Exception {
        return dao.getComprobante(tipo_documento, documento, transaccion);
    }
    
    /**
     * Metodo para obtener valores relacionados a los items q se van a contabilizar
     * @autor mfontalvo
     * @param cab, ingreso a consultar.
     * @throws Exception.
     * @return factura, bean q contiene los datos del la factura relacionada a un item de un ingreso
     */
    public factura obtenerDatosFactura(Ingreso_detalle item) throws Exception {
        return dao.obtenerDatosFactura(item);
    }
  
    /**
     * Getter for property processRun.
     * @return Value of property processRun.
     */
    public boolean isProcessRun() {
        return processRun;
    }
    
    /**
     * Setter for property processRun.
     * @param processRun New value of property processRun.
     */
    public void setProcessRun(boolean processRun) {
        this.processRun = processRun;
    }
    
     public String banco(String a)throws Exception
    {
        return dao.BancoDAO(a);
    }
    
    public String nombcliente(String a)throws Exception
    {
        return dao.ClienteDAO(a);
    }
    
    public String nombcuenta(String a)throws Exception
    {
        return dao.CuentaDAO(a);
    }
    
}
