/********************************************************************
 *      Nombre Clase.................   Tipo_DoctoService.java
 *      Descripci�n..................   Servicios del archivo con.tipo_docto
 *      Autor........................   Ing. Andr�s Maturana De La Cruz
 *      Fecha........................   8 de junio de 2006
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes S�nchez Polo S.A.
 *******************************************************************/

package com.tsp.finanzas.contab.model.services;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.finanzas.contab.model.DAO.*;
import com.tsp.finanzas.contab.model.beans.*;
import com.tsp.finanzas.contab.model.beans.Tipo_Docto;

/**
 *
 * @author  Ing. Andr�s Maturana De La Cruz
 */
public class Tipo_DoctoService {
    Tipo_DoctoDAO dao;
    
    /** Crea una nueva instancia de  Tipo_DoctoService */
    public Tipo_DoctoService() {
        dao = new Tipo_DoctoDAO();
    }
    public Tipo_DoctoService(String dataBaseName) {
        dao = new Tipo_DoctoDAO(dataBaseName);
    }
    
    /**
     * Getter for property docto.
     * @return Value of property docto.
     */
    public com.tsp.finanzas.contab.model.beans.Tipo_Docto getDocto() {
        return dao.getDocto();
    }
    
    /**
     * Setter for property docto.
     * @param docto New value of property docto.
     */
    public void setDocto(com.tsp.finanzas.contab.model.beans.Tipo_Docto docto) {
        dao.setDocto(docto);
    }
    
    /**
     * Getter for property treemap.
     * @return Value of property treemap.
     */
    public java.util.TreeMap getTreemap() {
        return dao.getTreemap();
    }
    
    /**
     * Setter for property treemap.
     * @param treemap New value of property treemap.
     */
    public void setTreemap(java.util.TreeMap treemap) {
        dao.setTreemap(treemap);
    }
    
    /**
     * Getter for property vector.
     * @return Value of property vector.
     */
    public java.util.Vector getVector() {
        return dao.getVector();
    }
    
    /**
     * Setter for property vector.
     * @param vector New value of property vector.
     */
    public void setVector(java.util.Vector vector) {
        dao.setVector(vector);
    }
    
    /**
     * Inserta un nuevo registro en el archivo.
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void insertar() throws SQLException{
        dao.insertar();
    }
    
    /**
     * Obtiene un listado de resgitros del archivo tipo de documento.
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void codigosInternos() throws SQLException{
        dao.codigosInternos();
    }
    
    /**
     * Obtiene un registro del archivo.
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param dstrct C�digo del Distrito
     * @param code C�digo del tipo de comprobante
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void obtener(String dstrct, String code) throws SQLException{
        dao.obtener(dstrct, code);
    }
    
    /**
     * Realiza la busqueda en el archivo.
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param dstrct C�digo del Distrito
     * @param code C�digo del tipo de comprobante
     * @param codeint C�digo interno
     * @param descrip Descripci�n del tipo de comprobante
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void search(String dstrct, String code, String codeint, String descrip) throws SQLException{
        dao.search(dstrct, code, codeint, descrip);
    }
    
    /**
     * Actualiza un registro en el archivo.
     * @autor Ing. Andr�s Maturana De La Cruz
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void actualizar() throws SQLException{
        dao.actualizar();
    }
    
    /**
     * Verifica si un codigo interno ya fue asignado en los comprobantes no diarios.
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param dstrct C�digo del Distrito
     * @param codeint C�digo del interno del tipo de comprobante
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public boolean isCodigoInternoUsado(String dstrct, String codint) throws SQLException{
        return dao.isCodigoInternoUsado(dstrct, codint);
    }
      /**
     *Metodo que permite obtener todos los tipo_docto
     *@autor: David Pi�a     
     *@throws: En caso de que un error de base de datos ocurra.     
     */
    public void ObtenerTipo_docto() throws SQLException{
        dao.ObtenerTipo_docto();
    }
        /**
     * Metodo que obtiene la consulta para actualizar Tipo_docto
     * @autor David Pi�a L�pez
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     * @return El sql de actualizar tipo_docto
     */
    public String getActualizar() throws SQLException{
        return dao.getActualizar();
    }

    public List<Tipo_Docto> ListarTipoDoc()  throws SQLException{
        return  dao.ListarTipoDoc();
    }

    public Tipo_Docto VerTipo_Docto(String dstrct, String code) throws SQLException{
       return dao.VerTipo_Docto(dstrct, code);
    }
    
}
