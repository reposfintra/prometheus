/***************************************
 * Nombre Clase ............. ListadoBusquedaServices.java
 * Descripci�n  .. . . . . .  Permitimos buscar listados
 * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
 * Fecha . . . . . . . . . .  09/06/2006
 * versi�n . . . . . . . . .  1.0
 * Copyright ...............  Transportes Sanchez Polo S.A.
 *******************************************/


package com.tsp.finanzas.contab.model.services;




import java.io.*;
import java.util.*;
import com.tsp.finanzas.contab.model.DAO.ListadoBusquedaDAO;
import com.tsp.util.*;




public class ListadoBusquedaServices {
    
    private ListadoBusquedaDAO dao;
    private List               memoria;
   
    
    
    
    public ListadoBusquedaServices() {
        dao      = new ListadoBusquedaDAO();
        memoria  = new LinkedList();
    }
    public ListadoBusquedaServices(String dataBaseName) {
        dao      = new ListadoBusquedaDAO(dataBaseName);
        memoria  = new LinkedList();
    }
    
    
    
    
    
    /**
       * M�todo que busca el listado
       * @autor.......fvillacob
       * @throws......Exception
       * @version.....1.0.
       **/
    public Vector getLista(String sql, String filtro)throws Exception{
        Vector lista = new Vector();
        try{
            
            Vector aux = this.getListaConsulta(sql, filtro);
            if(aux==null){
               lista               = dao.getLista( sql, filtro);
               Hashtable  consulta = construir(sql,filtro,lista);
               this.memoria.add( consulta );
               ////System.out.println("Grabado");
            }
            else{
               lista = aux;
               ////System.out.println("Asign�");  
            }
            
        }catch(Exception e){
            throw new Exception( e.getMessage() );
        }
        
        return lista;
    }
    
    
    
    
    
    
    private Hashtable construir(String sql, String filtro, List lista){
          Hashtable  ht = new Hashtable();
            ht.put("sql",    sql);
            ht.put("filtro", filtro);
            ht.put("lista",  lista);          
          return ht;
    }
    
    
    
    
    
    
    
    private Vector getListaConsulta(String sql, String filtro)throws Exception{
        Vector lista = null;
        try{
            ////System.out.println("_____________________________________");
            for(int i=0;i<this.memoria.size();i++){
                 Hashtable  ht   =  ( Hashtable ) this.memoria.get(i);                 
                 String     sq   =  (String)  ht.get("sql");
                 String     fil  =  (String)  ht.get("filtro");
                 Vector     lis  =  (Vector)  ht.get("lista");                 
                
                 ////System.out.println("CONSULTAS GRABADAS ->" + sq + "->"+ fil);
                 
                 if(  sq.equals(sql) && fil.equals(filtro)   ){
                     lista = lis;
                     break;
                 }
            }
                   
        }catch(Exception e){
            throw new Exception( e.getMessage() );
        }
        
        return lista;
    }
    
    
    
    
    
    
    
    
}
