/***
 * Nombre clase :                 SubledgerService.java
 * Descripcion :                  Clase que maneja los Service
 *                                los cuales contienen los servicios que interactuan
 *                                el DAO
 * Autor :                        Ing. Diogenes Antonio Bastidas Morales
 * Fecha :                        6 de junio de 2006, 06:27 PM
 * Version :                      1.0
 * Copyright :                    Fintravalores S.A.
 ****/


package com.tsp.finanzas.contab.model.services;
import com.tsp.finanzas.contab.model.beans.*;
import com.tsp.operation.model.beans.*;
import com.tsp.finanzas.contab.model.DAO.*;

import java.io.*;
import java.sql.*;
import java.util.*;


public class SubledgerService {
    private SubledgerDAO subledgerdao;
    private Vector codigos;
    /** Creates a new instance of SubledgerService */
    public SubledgerService() {
        subledgerdao = new SubledgerDAO();
    }
    public SubledgerService(String dataBaseName) {
        subledgerdao = new SubledgerDAO(dataBaseName);
    }
    
    
    /**
     * Metodo insertarIngreso, ingresa un registro en la tabla ingreso
     * @param:
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    
    public void insertarSubledger(Subledger subledger) throws Exception {
        try{
            subledgerdao.setSubledger(subledger);
            subledgerdao.insertarSubledger();
        }catch(Exception e){
            throw new Exception(e.getMessage());
        }
    }
    
    /**
     * Getter for property subledger.
     * @return Value of property subledger.
     */
    public Subledger getSubledger() {
        return subledgerdao.getSubledger();
    }
    
    
    /**
     * Metodo modificarIngreso, mopdifica un registro en la tabla ingreso
     * @param:
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public void modificarSubledger(Subledger subledger) throws Exception {
        try{
            subledgerdao.setSubledger(subledger);
            subledgerdao.modificarSubledger();
        }catch(Exception e){
            throw new Exception(e.getMessage());
        }
    }
    
    /**
     * Metodo que obtiene los tipo subledger de una cuenta
     * @param: numero de cuenta
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public void buscarCuentasTipoSubledger(String cuenta) throws Exception{
        try{
            subledgerdao.buscarCuentasTipoSubledger(cuenta);
        }catch(Exception e){
            throw new Exception(e.getMessage());
        }
    }
    
    /**
     * Getter for property cuentasTipoSubledger.
     * @return Value of property cuentasTipoSubledger.
     */
    public TreeMap getCuentasTipoSubledger() {
        return subledgerdao.getCuentasTipoSubledger();
    }
    
    /**
     * Setter for property cuentasTipoSubledger.
     * @param cuentasTipoSubledger New value of property cuentasTipoSubledger.
     */
    public void setCuentasTipoSubledger(java.util.TreeMap cuentasTipoSubledger) {
        subledgerdao.setCuentasTipoSubledger(cuentasTipoSubledger);
    }
    
    /**
     * Metodo modificarIngreso, mopdifica un registro en la tabla ingreso
     * @param:
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public boolean existeSubledgerAnulado(String dstrct, String cuenta, String tipo, String id_subledger) throws Exception {
        return subledgerdao.existeSubledgerAnulado(dstrct, cuenta, tipo,id_subledger);
    }
    
    /**
     * Getter for property listasubledger.
     * @return Value of property listasubledger.
     */
    public java.util.LinkedList getListasubledger() {
        return subledgerdao.getListasubledger();
    }
    
    /**
     * Metodo busquedaSubledger, busca la lista de subledger
     * @param: distrito, cuenta, subledger
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public void busquedaSubledger(String dstrct, String cuenta, String tipo, String id_subledger) throws Exception {
        subledgerdao.busquedaSubledger(dstrct, cuenta, tipo, id_subledger);
    }
    
    /**
     * Metodo buscarSubledger, busca un objeto tipo subledger
     * @param: distrito, cuenta, tipo, id_subledger
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public void buscarSubledger(String dstrct, String cuenta, String tipo, String id_subledger) throws Exception {
        subledgerdao.buscarSubledger(dstrct, cuenta, tipo, id_subledger);
    }
    
    /**
     * Getter for property tsubledger.
     * @return Value of property tsubledger.
     */
    public com.tsp.operation.model.beans.TablaGen getTsubledger() {
        return subledgerdao.getTsubledger();
    }
    
    /**
     * Metodo anularSubledger, Anula un registro en la tabla subledger
     * @param:
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    
    public void anularSubledger(Subledger subledger) throws Exception {
        subledgerdao.setSubledger(subledger);
        subledgerdao.anularSubledger();
    }
    /**
     * Metodo existeTipoSubledger, retorna true si existe la relacion
     * @param:cuenta, tipo
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public boolean existeTipoSubledger(String cuenta, String tipo)throws Exception {
        Vector vec = subledgerdao.buscarCodigosCuenta(cuenta);
        ////System.out.println("Tama�o Vecto"+ vec.size());
        for(int i=0; i<vec.size();i++){
            String obj = (String) vec.get(i);
            if(obj.equals(tipo)){
                return true;
            }
        }
        return false;
    }
    
    /**
     * Metodo agregarCuentasTSubledger, agrega la relacion cuenta tiposubledger en la tablagen
     * sino existe ingresa la informacion al registro, y existe agregar el codigo en la campo dato
     * @param:objeto tipo tablagen
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public void agregarCuentasTSubledger(TablaGen tsubledger)throws Exception {
        boolean sw = false;
        String valor = "";
        try{
            subledgerdao.setTsubledger(tsubledger);
            try{
                subledgerdao.agregarCuentasTSubledger();
            }catch(Exception e){
                sw = true;
            }
            if(sw){
                valor = subledgerdao.buscarTiposCuenta(tsubledger.getTable_code())+tsubledger.getDato();
                tsubledger.setDato(valor);
                subledgerdao.setTsubledger(tsubledger);
                subledgerdao.modificarCuentasTSubledger();
            }
        }catch(Exception e){
            throw new Exception(e.getMessage());
        }
    }
    
    /**
     * Metodo modificarCuentasTSubledger, modifica la relacion cuenta con tipo subledger
     * buscar los tipo subledger de una cuenta, lo elimina y actualiza el nuevo codigo
     * @param:objeto tipo tablagen
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public void modificarCuentasTSubledger(TablaGen tsubledger, String tipo) throws Exception {
        String dato = "";
        try{
            //busco los codigos y elimino el que quiero modificar
            Vector vec = subledgerdao.buscarCodigosCuenta(tsubledger.getTable_code());
            for(int i=0; i<vec.size();i++){
                String obj = (String) vec.get(i);
                if(obj.equals(tipo)){
                    vec.remove(i);
                }
            }
            //concateno los codigos para actualizar el registro
            for(int i=0; i<vec.size();i++){
		dato += (String) vec.get(i);
            }
           // ////System.out.println("DATO  "+tsubledger.getDato());
            dato+=tsubledger.getDato();
            tsubledger.setDato(dato);
            subledgerdao.setTsubledger(tsubledger);
            subledgerdao.modificarCuentasTSubledger();
        }catch(Exception e){
            throw new Exception(e.getMessage());
        }
    }
    
    /**
     * Metodo que obtiene un vector con los codigos de los tipos de subledger de una cuenta
     * @param: numero de cuenta
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public Vector buscarCodigosCuenta(String cuenta) throws Exception{
        try{
            this.codigos = subledgerdao.buscarCodigosCuenta(cuenta);
            return codigos;
        }catch(Exception e){
            throw new Exception(e.getMessage());
        }
    }
    
    /**
     * Metodo que obtiene los tipo subledger de una cuenta
     * @param: numero de cuenta
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public void busquedaCuentasTipoSubledger(String dstrct, String cuenta) throws Exception{
        try{
            subledgerdao.busquedaCuentasTipoSubledger(dstrct,cuenta);
        }catch(Exception e){
            throw new Exception(e.getMessage());
        }
    }
    
    /**
     * Getter for property cuentastsubledger.
     * @return Value of property cuentastsubledger.
     */
    public java.util.LinkedList getCuentastsubledger() {
        return subledgerdao.getCuentastsubledger();
    }
    
    
    /**
     * Metodo eliminarCuentasTSubledger, elimina el tipo subledger
     * @param:objeto tipo tablagen
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public void eliminarCuentasTSubledger(TablaGen tsubledger ) throws Exception {
        String dato = "";
        try{
            //busco los codigos y elimino el que quiero modificar
            Vector vec = subledgerdao.buscarCodigosCuenta(tsubledger.getTable_code());
            for(int i=0; i<vec.size();i++){
                String obj = (String) vec.get(i);
                if(obj.equals(tsubledger.getDato())){
                    vec.remove(i);
                }
            }
            //concateno los codigos para actualizar el registro
            for(int i=0; i<vec.size();i++){
		dato += (String) vec.get(i);
            }
            
            tsubledger.setDato(dato);
            subledgerdao.setTsubledger(tsubledger);
            subledgerdao.modificarCuentasTSubledger();
        }catch(Exception e){
            throw new Exception(e.getMessage());
        }
    }
    
    /**
     * Getter for property codigos.
     * @return Value of property codigos.
     */
    public java.util.Vector getCodigos() {
        return codigos;
    }
    
    /**
     * Setter for property codigos.
     * @param codigos New value of property codigos.
     */
    public void setCodigos(java.util.Vector codigos) {
        this.codigos = codigos;
    }
    
     /**
     * Setter for property cuentastsubledger.
     * @param cuentastsubledger New value of property cuentastsubledger.
     */
    public void setCuentastsubledger(LinkedList cuentastsubledger) {
        subledgerdao.setCuentastsubledger(cuentastsubledger);
    }
    
    /**
     * Metodo buscarIdSubledger, busca la lista id subledger con sus nombres,
     * realizando la busqueda por id o por nombre
     * @param: distrito, cuenta, tipo, 
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public void buscarIdSubledger(String dstrct, String cuenta, String tipo, String valor) throws Exception {
        subledgerdao.buscarIdSubledger(dstrct, cuenta, tipo, valor); 
        
    }
    
     /**
     * Metodo existeSubledger, retorna true si existe el subledger
     * @param: distrito, cuenta, subledger
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public boolean existeSubledger(String dstrct, String cuenta, String tipo, String id_subledger) throws Exception {
        return subledgerdao.existeSubledger(dstrct, cuenta, tipo, id_subledger);
    }
    
    
     /**
     * Metodo que obtiene la cuentas de un tipo de subledger
     * @param: buscarCuentas_Tipo
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public Vector buscarCuentas_Tipo(String tipo) throws Exception{
        return subledgerdao.buscarCuentas_Tipo(tipo);
    }
    
    public static void main(String a [])throws Exception{
       SubledgerService prog = new SubledgerService();
       Vector vec = prog.buscarCuentas_Tipo("PS");
       for(int i=0; i<vec.size();i++){
           TablaGen objtipo = (TablaGen) vec.get(i);
           //System.out.println(objtipo.getTable_code());
           
       }
    }
}