/********************************************************************
 *      Nombre Clase.................   ReporteGralService.java
 *      Descripci�n..................   Servicios del Data Access Object
 *      Autor........................   Ing. Andr�s Maturana De La Cruz
 *      Fecha........................   21 de junio de 2006, 02:43 PM
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes S�nchez Polo S.A.
 *******************************************************************/

package com.tsp.finanzas.contab.model.services;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.finanzas.contab.model.DAO.*;
import com.tsp.finanzas.contab.model.beans.*;


//dom4j
import org.dom4j.Element;
import org.dom4j.DocumentHelper;
import org.dom4j.Document;

//Logger
import org.apache.log4j.*;

/**
 *
 * @author  Ing. Andr�s Maturana De La Cruz
 */
public class ReporteGralService {
    ReporteGralDAO dao;
    Document documento;
    
    Logger log = Logger.getLogger (ReporteGralService.class);
    
    /** Crea una nueva instancia de  ReporteGralService */
    public ReporteGralService() {
        dao =  new ReporteGralDAO();
    }
    public ReporteGralService(String dataBaseName) {
        dao =  new ReporteGralDAO(dataBaseName);
    }
    
    
    /**
     * Genera un documento XMl a partir de las las facturas en un per�odo dado.
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param dstrct C�digo del Distrito
     * @param fecha_ini Fecha inicial del per�odo
     * @param Fecha_fin Fecha final del reporte
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    /**
     * Getter for property vector.
     * @return Value of property vector.
     */
    public java.util.Vector getVector() {
        return dao.getVector();
    }
    
    /**
     * Setter for property vector.
     * @param vector New value of property vector.
     */
    public void setVector(java.util.Vector vector) {
        dao.setVector(vector);
    }
    
    /**
     * Valida una cadena de caracteres
     * @return 
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param obj Cadena a verificar
     * @version 1.0
     */
    protected String obtenerTexto(Object obj) {
        if ( obj == null ) {
            return "";
        }
        else {
            return obj.toString();
        }
    }
    
    /**
     * Getter for property documento.
     * @return Value of property documento.
     */
    public org.dom4j.Document getDocumento() {
        return documento;
    }
    
    /**
     * Setter for property documento.
     * @param documento New value of property documento.
     */
    public void setDocumento(org.dom4j.Document documento) {
        this.documento = documento;
    }   
    
    /**
     * Obtiene las facturas en un per�odo dado.
     * @param fecha_fin
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param dstrct C�digo del Distrito
     * @param fecha_ini Fecha inicial del per�odo
     * @param login Login del usuario que cre� la factura
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void reporteFacturasClientes(String dstrct, String fecha_ini, String fecha_fin, String login) throws SQLException{
        dao.reporteFacturasClientes(dstrct, fecha_ini, fecha_fin, login);
    }
     /**
     * Obtiene las facturas en un per�odo dado.
     * @param fecha_fin
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param dstrct C�digo del Distrito
     * @param fecha_ini Fecha inicial del per�odo
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void reporteVentasDiario(String dstrct, String fecha_ini, String fecha_fin) throws SQLException{
        dao.reporteVentasDiario(dstrct, fecha_ini + " 00:00:00", fecha_fin + " 23:59:59");
    }
    
    /**
     * Genera un documento XMl a partir de las las facturas en un per�odo dado.
     * @autor Ing. Andr�s Maturana De La Cruz
     * @version 1.0
     */
    public void reporteVentasDiarioXml(){
        
        System.gc();
        
        Element raiz = DocumentHelper.createElement("raiz");
        Vector vec = this.getVector();
        
        if( vec!=null && vec.size()>0 ){
            RepGral obj = (RepGral) vec.elementAt(0);
            //Hashtable ht = (Hashtable) vec.elementAt(0);
            String fec = obj.getFecha();
            //String fec = ht.get("fecha").toString();
            
            String anterior = obj.getFactura();
            //String anterior = ht.get("factura").toString();
            long prox = Long.parseLong(anterior.substring(1, anterior.length())) + 1;
            log.info("PROXIMA INI(" + anterior + "): " + prox);
            
            Element fecha = raiz.addElement("fecha");
            int sw = 1;
            double total = 0;
            for( int i=0; i<vec.size(); i++){
                
                RepGral obj0 = (RepGral) vec.elementAt(i);
                //Hashtable ht0 = (Hashtable) vec.elementAt(i);
                /*if( fec.compareTo(ht0.get("fecha").toString())!=0 ) {
                    log.info("FECHA " + fec + " : " + com.tsp.util.Util.customFormat(total));
                    fecha.addElement("total").addText(this.obtenerTexto(String.valueOf(com.tsp.util.Util.customFormat(total))));
                    total = 0;
                    fec = ht0.get("fecha").toString();
                    fecha = raiz.addElement("fecha");
                    sw = 1;
                }*/
                
                Element factura;// = fecha.addElement("factura");
                
                /* FACTURA INTERMEDIA FALTANTE */
                String actual = obj0.getFactura();
                //String actual = ht0.get("factura").toString();
                log.info("> FACTURA: " + actual);
                
                if ( vec.size()>(i+1) ) {
                    RepGral obj1 = (RepGral) vec.elementAt(i+1);
                    //Hashtable ht1 = (Hashtable) vec.elementAt(i+1);
                    String factnext = obj1.getFactura();
                    //String factnext = ht1.get("factura").toString();
                    String factnext_aux = factnext.substring(1, factnext.length());
                    factnext = String.valueOf(Long.parseLong(factnext.substring(1, factnext.length())));
                    log.info(">> FACTURA NEXT: " + factnext);
                    String ceros = "";
                    for( int k=0; k<(factnext_aux.length() - factnext.length()); k++){
                        ceros += "0";
                    }
                    
                    String actual0 = String.valueOf(Long.parseLong(actual.substring(1, actual.length())));
                    log.info("FACTURA ACTUAL: " + actual0);
                    log.info("FACTURA PROX: " + prox);
                    System.gc();
                    while( factnext.compareTo(String.valueOf(prox))!=0 ){
                        factura = fecha.addElement("factura");
                        String faltante = actual.substring(0, 1) + ceros//actual.length() - String.valueOf(prox).length())
                        + String.valueOf(prox);
                        log.info("**FALTO FACTURA: " + faltante );
                        factura.addElement("documento").addText(this.obtenerTexto("*" + faltante));
                        factura.addElement("nit").addText("");
                        factura.addElement("codcli").addText("");
                        factura.addElement("valor").addText("");
                        
                        
                        /*if( i == 1 ){
                            anterior = ht0.get("factura").toString();
                            prox = Long.parseLong(anterior.substring(1, anterior.length())) + 1;
                        } else {
                            prox++;
                        }*/
                        
                        prox++;
                        
                        log.info(">>> FACTURA NEXT: " + factnext + " >>> PROX: " + prox);
                        System.gc();
                    }
                    
                    //anterior = ht0.get("factura").toString();
                    //prox = Long.parseLong(anterior.substring(1, anterior.length())) + 1;
                    /*if( i == 1 ){
                        anterior = ht0.get("factura").toString();
                        prox = Long.parseLong(anterior.substring(1, anterior.length())) + 1;
                    } else {
                        prox++;
                    }*/
                    prox++;
                }
                /************************************************************************************************/
                
                if( fec.compareTo(obj0.getFecha())!=0 ) {
                //if( fec.compareTo(ht0.get("fecha").toString())!=0 ) {
                    log.info("FECHA " + fec + " : " + com.tsp.util.Util.customFormat(total));
                    fecha.addElement("total").addText(this.obtenerTexto(String.valueOf(com.tsp.util.Util.customFormat(total))));
                    total = 0;
                    fec = obj0.getFecha();
                    //fec = ht0.get("fecha").toString();
                    fecha = raiz.addElement("fecha");
                    sw = 1;
                }
                
                factura = fecha.addElement("factura");
                
                if( fec.compareTo(obj0.getFecha())!=0 ) {
                //if( fec.compareTo(ht0.get("fecha").toString())!=0 ) {
                    log.info("FECHA " + fec + " : " + com.tsp.util.Util.customFormat(total));
                    fecha.addElement("total").addText(this.obtenerTexto(String.valueOf(com.tsp.util.Util.customFormat(total))));
                    total = 0;
                    fec = obj0.getFecha();
                    //fec = ht0.get("fecha").toString();
                    fecha = raiz.addElement("fecha");
                    sw = 1;
                }
                
                factura.addElement("documento").addText(this.obtenerTexto(obj0.getFactura()));
                //factura.addElement("documento").addText(this.obtenerTexto(ht0.get("factura").toString()));
                
                if( sw == 1 )
                    factura.addElement("fecha").addText(obj0.getFecha());
                    //factura.addElement("fecha").addText(this.obtenerTexto(ht0.get("fecha").toString()));
                else
                    factura.addElement("fecha").addText("");
                
                sw = 0;
                
                factura.addElement("documento").addText(this.obtenerTexto(obj0.getFactura()));
                factura.addElement("documento").addText(this.obtenerTexto(obj0.getNit()));
                factura.addElement("codcli").addText(this.obtenerTexto(obj0.getCodcli()));
                /*factura.addElement("documento").addText(this.obtenerTexto(ht0.get("factura").toString()));
                factura.addElement("nit").addText(this.obtenerTexto(ht0.get("nit").toString()));
                factura.addElement("codcli").addText(this.obtenerTexto(ht0.get("codcli").toString()));*/
                
                String fechaA = obj0.getFecha_anula();
                //String fechaA = ht0.get("fec_anulacion").toString();
                log.info("FACTURA: " + obj0.getFactura() + " - FECHA DE ANULACION: " + fechaA);
                //log.info("FACTURA: " + ht0.get("factura").toString() + " - FECHA DE ANULACION: " + fechaA);
                if( fechaA.compareTo("0099-01-01 00:00:00")==0 ){
                    double valor = Double.parseDouble(obj0.getValor());
                    //double valor = Double.parseDouble(ht0.get("valor").toString());
                    total += valor;
                    factura.addElement("valor").addText(this.obtenerTexto(String.valueOf(com.tsp.util.Util.customFormat(valor))));
                } else {
                    factura.addElement("valor").addText(".00");
                }
            }
            
            log.info("FECHA " + fec + " : " + com.tsp.util.Util.customFormat(total));
            fecha.addElement("total").addText(this.obtenerTexto(String.valueOf(com.tsp.util.Util.customFormat(total))));
        }
        //log.info(documento.getXMLEncoding());
        documento = DocumentHelper.createDocument(raiz);
        System.gc();
    }
    
}
