/************************************************************************
* Nombre ....................ReporteFacturasProveedorService.java       *
* Descripci�n................Clase que maneja los servicios al model    *
*                            relacionados con el reporte de facturas    *
*                            de proveedor.                              *
* Autor......................LREALES                                    *
* Fecha Creaci�n.............27 de Junio de 2006, 07:50 AM              *
* Versi�n....................1.0                                        *
* Coyright...................Transportes Sanchez Polo S.A.              *
************************************************************************/

package com.tsp.finanzas.contab.model.services;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.finanzas.contab.model.beans.*;
import com.tsp.finanzas.contab.model.DAO.ReporteFacturasProveedorDAO;

public class ReporteFacturasProveedorService {
    
    private ReporteFacturasProveedorDAO reporte_dao;    
    
    /** Creates a new instance of ReporteFacturasProveedorService */
    public ReporteFacturasProveedorService () {
        reporte_dao = new ReporteFacturasProveedorDAO();
    }
    public ReporteFacturasProveedorService (String dataBaseName) {
        reporte_dao = new ReporteFacturasProveedorDAO(dataBaseName);
    }
    
    /**
     * Metodo: getReporte, permite retornar un objeto de registros del reporte.
     * @autor : LREALES
     * @param : -
     * @version : 1.0
     */    
    public ReporteFacturasProveedor getReporte()throws SQLException{
        
        return reporte_dao.getReporte();
        
    }
    
    /**
     * Metodo: setReporte, permite obtener un objeto de registros del reporte.
     * @autor : LREALES
     * @param : objeto
     * @version : 1.0
     */
    public void setReporte ( ReporteFacturasProveedor reporte )throws SQLException {
        
        reporte_dao.setReporte( reporte );
        
    }
    
    /**
     * Metodo: getVec_reporte, permite retornar un vector de registros del reporte.
     * @autor : LREALES
     * @param : -
     * @version : 1.0
     */    
    public Vector getVec_reporte()throws SQLException{
        
        return reporte_dao.getVec_reporte();
        
    }
    
    /**
     * Metodo: setVec_reporte, permite obtener un vector de registros del reporte.
     * @autor : LREALES
     * @param : vector
     * @version : 1.0
     */    
    public void setVec_reporte( Vector ReporteFacturasProveedor )throws SQLException{
        
        reporte_dao.setVec_reporte( ReporteFacturasProveedor );
        
    }
            
    /**
    * Metodo listaFacturasProveedor, lista la informacion del reporte
    * @see listaFacturasProveedor - ReporteFacturasProveedorDAO
    * @param : la fecha inicial, la fecha final y el usuario.
    * @version : 1.0
    */    
    public void listaFacturasProveedor( String fecha_inicial, String fecha_final, String usuario ) throws SQLException {
        
        reporte_dao.listaFacturasProveedor( fecha_inicial, fecha_final, usuario );
        
    }
    
}