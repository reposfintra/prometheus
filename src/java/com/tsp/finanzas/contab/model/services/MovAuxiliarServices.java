/***************************************
 * Nombre Clase ............. MovAuxiliarServices.java
 * Descripci�n  .. . . . . .  Permitimos realizar los servicios para la consulta de los movimientos auxiliares
 * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
 * Fecha . . . . . . . . . .  07/06/2006
 * versi�n . . . . . . . . .  1.0
 * Copyright ...............  Transportes Sanchez Polo S.A.
 *******************************************/



package com.tsp.finanzas.contab.model.services;




import java.io.*;
import java.util.*;
import java.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.finanzas.contab.model.beans.*;
import com.tsp.finanzas.contab.model.DAO.MovAuxiliarDAO;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.util.*;




public class MovAuxiliarServices {
    
    
    private  MovAuxiliarDAO   DAO;    
    
    
    private List              listTipoAuxiliar;
    
    
    private List              listCuentas;
    private String            periodo;
    private Hashtable         cuentaPrevia;
    
    private String            anoConsulta;
    private String            mesInicioConsulta;
    
    private String            nombreAuxiliar;         
    
    
 // Parametros busqueda:
    private  String distrito;
    private  String cuentaIni;
    private  String cuentaFin;
    private  String fechaIni;   
    private  String fechaFin;
    private  String auxiliar;
    private  String tipoAuxiliar;
    
    //mfontalvo 23/04/2007
    private Vector datos;
    
    
    public MovAuxiliarServices() {
        DAO               = new  MovAuxiliarDAO();
        listTipoAuxiliar  = new LinkedList();
        periodo           = "";
    }
    public MovAuxiliarServices(String dataBaseName) {
        DAO               = new  MovAuxiliarDAO(dataBaseName);
        listTipoAuxiliar  = new LinkedList();
        periodo           = "";
    }
    
    
    
    
    private String reset(String val){
        if(val==null)
           val = "";
        return val;
    }
    
    
    
        
    
    public void loadRequest(HttpServletRequest request){
         reset();
         distrito          = reset( request.getParameter("txtDistrito") );
         cuentaIni         = reset( request.getParameter("cuentaIni")   );
         cuentaFin         = reset( request.getParameter("cuentaFin")   ); 
         fechaIni          = reset( request.getParameter("txtAnoIni") ) +  reset( request.getParameter("txtMesIni") );
         
         anoConsulta       = reset( request.getParameter("txtAnoIni") );
         mesInicioConsulta = reset( request.getParameter("txtMesIni") );
         
         fechaFin          = reset( request.getParameter("txtAnoFin") ) +  reset( request.getParameter("txtMesFin") ); 
         auxiliar          = reset( request.getParameter("txtIdentificacion") );
         tipoAuxiliar      = reset( request.getParameter("txtTipo") );
         if(auxiliar.equals("")){
            tipoAuxiliar = "";
         }         
    }
    
    
    
    
    
    public void reset(){
         listCuentas       = new LinkedList();
         cuentaPrevia      = new Hashtable();
         distrito          = "";
         cuentaIni         = "";
         cuentaFin         = ""; 
         fechaIni          = "";
         fechaFin          = ""; 
         tipoAuxiliar      = "";
         auxiliar          = "";
         
         anoConsulta       = "";
         mesInicioConsulta = "";
         nombreAuxiliar    = "";
    }
    
    
    
     /**
     * M�todos que busca los tipos de auxiliar
     * @autor.......fvillacob 
     * @version.....1.0.     
     **/ 
    public void searchTipoAuxiliar()throws Exception{
        try{
            this.listTipoAuxiliar =  DAO.getTiposAuxiliar();
        }catch(Exception e){
            throw new Exception ("searchTipoAuxiliar:"+e.getMessage());
        }
    }
    
    
    
    
    /**
     * M�todos que busca los tipos de auxiliar
     * @autor.......fvillacob 
     * @version.....1.0.     
     **/ 
    public void searchRangoCuentas()throws Exception{
        listCuentas     = new LinkedList();
        List cuentasAll = new LinkedList();
        try{
             cuentasAll =  DAO.getCuentasRango(distrito, cuentaIni, cuentaFin);
             
             excluirRepetidos(cuentasAll);
             if(!this.auxiliar.equals("")){
                 obtenerSoloCuentasDelAuxiliar();
                 nombreAuxiliar = DAO.nombreAuxiliar(this.tipoAuxiliar, this.auxiliar);   
             }
             
             if( listCuentas.size()>0 ){                 
                  this.cuentaPrevia   = (Hashtable)listCuentas.get(0);
                  this.cuentaPrevia   = completar( this.cuentaPrevia);
             }
             
             
        }catch(Exception e){
            throw new Exception ("searchRangoCuentas: " + e.getMessage());
        }
    }
    
    
    
    
    
    /**
     * M�todos que elimina cuentas repetidas
     * @autor.......fvillacob 
     * @version.....1.0.     
     **/ 
    private void excluirRepetidos(List lista)throws Exception{
        try{
            if( lista!=null)
                for(int i=0;i<lista.size();i++){
                    Hashtable  cuenta   = (Hashtable)lista.get(i);
                    String     noCuenta = (String) cuenta.get("cuenta");
                    int sw = 0;
                    
                    for(int k=0;k<listCuentas.size();k++){
                        Hashtable  cuentaDetalle     = (Hashtable)listCuentas.get(k);
                        String     noCuentaDetalle   = (String) cuentaDetalle.get("cuenta");
                        if( noCuentaDetalle.equals( noCuenta )  ){
                            sw = 1;
                            break;
                        }
                    }
                    
                    if (sw==0)
                        listCuentas.add(cuenta);
                }
            
            
        }catch(Exception e){
            throw new Exception ("excluirRepetidos: "+e.getMessage());
        }
    }
    
    
    
    
    /**
     * M�todos que elimina cuentas que no sean del auxiliar
     * @autor.......fvillacob 
     * @version.....1.0.     
     **/ 
    private void obtenerSoloCuentasDelAuxiliar()throws Exception{
        
        List aux = new LinkedList();
        
        try{
            
            if( listCuentas!=null)
                for(int i=0;i<listCuentas.size();i++){
                    Hashtable  cuenta    = (Hashtable)listCuentas.get(i);
                    String     distrito   = (String)cuenta.get("distrito");
                    String     noCuenta   = (String)cuenta.get("cuenta");  
                    if(  DAO.isDelAuxiliar(distrito,noCuenta,this.tipoAuxiliar,this.auxiliar)  )
                        aux.add( cuenta );
                }
                    
            listCuentas = aux;
            
            
        }catch(Exception e){
            throw new Exception ("obtenerSoloCuentasDelAuxiliar(): "+e.getMessage());
        }
    }
    
    
    
    
    
     /**
     * M�todos que obtiene la cuenta seleccionada
     * @autor.......fvillacob 
     * @version.....1.0.     
     **/ 
    public void obtenerCuenta(String cuenta)throws Exception{
        try{
           if( listCuentas!=null)
                for(int i=0;i<listCuentas.size();i++){
                     Hashtable  detalle           = (Hashtable)listCuentas.get(i);
                     String     noCuentaDetalle   = (String) detalle.get("cuenta");
                     if( noCuentaDetalle.equals( cuenta )  ){
                         detalle = completar(detalle);
                         this.cuentaPrevia = detalle;
                         break;                         
                     }
                }
            
        }catch(Exception e){
            throw new Exception ("obtenerCuenta " + e.getMessage());
        }
    }
    
    
    
    
    
    
    /**
     * M�todos que complementa la lista, solo aquellas que no esten completas
     * @autor.......fvillacob 
     * @version.....1.0.     
     **/ 
    public void formatear() throws Exception{
         try{
           if( listCuentas!=null)
                for(int i=0;i<listCuentas.size();i++){
                     Hashtable  detalle           = (Hashtable)listCuentas.get(i);
                     detalle = completar(detalle);
                }
            
        }catch(Exception e){
            throw new Exception ("formatear() " + e.getMessage());
        }
    }
    
    
    
    
    
    
    
    /**
     * M�todos que obtiene estructura del arbol, movimientos
     * @autor.......fvillacob 
     * @version.....1.0.     
     **/ 
    public  Hashtable  completar(Hashtable cuenta)throws Exception{
        try{
            
            String   formato   = (String)cuenta.get("formato");
            if( formato == null ){            
            
                        String   distrito   = (String)cuenta.get("distrito");
                        String   noCuenta   = (String)cuenta.get("cuenta");
                        String   padre      = (String)cuenta.get("padre");


                     // Estructura:           
                        List   arbol  =  this.obtenerArbol(distrito, padre);
                        cuenta.put("arbol", arbol ); 


                     // Saldo anterior:  
                        String saldoCuenta = (String)cuenta.get("saldoAnterior");
                        double saldo       = 0;          
                        if(  this.auxiliar.equals("")  )     saldo = DAO.saldoA�oAnterior_Mayorizacion          (distrito, noCuenta, this.anoConsulta )                  +   DAO.saldoMes_Mayorizacion          (distrito, noCuenta, this.anoConsulta, this.mesInicioConsulta ) ;
                        else                                 saldo = DAO.saldoA�oAnterior_Mayorizacion_Subledger(distrito, noCuenta, this.auxiliar, this.anoConsulta )   +   DAO.saldoMes_Mayorizacion_Subledger(distrito, noCuenta, this.auxiliar,  this.anoConsulta, this.mesInicioConsulta ) ;
                        cuenta.put("saldoAnterior", String.valueOf(saldo) );


                     // Movimientos  y Totales          
                         List   movimientos  =  this.buscarMovimientos(distrito, noCuenta);

                           double  totalDebito  = 0;
                           double  totalCredito = 0;
                           double  totalSaldo   = 0;

                           for(int i=0;i<movimientos.size();i++){
                                 Hashtable  mov  =  (Hashtable)movimientos.get(i);                     

                                 double saldoMov   =   Double.parseDouble( (String) mov.get("vlrSaldo")   );
                                 double vlrDebito  =   Double.parseDouble( (String) mov.get("vlrDebito")  );
                                 double vlrCredito =   Double.parseDouble( (String) mov.get("vlrCredito") );

                                 saldo        += saldoMov;

                                 totalSaldo   += saldo;
                                 totalDebito  += vlrDebito;
                                 totalCredito += vlrCredito;

                                 mov.put("saldoAcumulado", String.valueOf( saldo ) );
                           }

                        cuenta.put("movimientos", movimientos );

                        cuenta.put("totalDebito",  String.valueOf( totalDebito  ) ); 
                        cuenta.put("totalCredito", String.valueOf( totalCredito ) ); 
                        cuenta.put("totalSaldo",   String.valueOf( totalSaldo   ) ); 
                        
                        
                        cuenta.put("formato", "ok");   // Activamos el sw del formato
             
            }
            
            
        }catch(Exception e){
            throw new Exception ("completar: " + e.getMessage());
        }
        return cuenta;
    }
    
    
    
    
    
    
    
    
    
    
    /**
     * M�todos que forma la estructura del arbol de cada cuenta
     * @autor.......fvillacob 
     * @version.....1.0.     
     **/ 
    private List obtenerArbol(String distrito, String cuenta)throws Exception{
        List  arbol = new LinkedList();
        try{
             Hashtable  padre = DAO.getCuenta(distrito, cuenta );
             if(padre!=null)
                 do{
                    arbol.add(padre); 

                    String   dis   = (String)padre.get("distrito");
                    String   cue   = (String)padre.get("padre");

                    padre = DAO.getCuenta(dis, cue);

                 }while(padre!=null);
              
             arbol = this.invertirArbol(arbol);
            
        }catch(Exception e){
            throw new Exception ("obtenerArbol:" + e.getMessage());
        }
        return arbol;
    }    
    
    
   /**
     * M�todos que invierte la estructura del arbol
     * @autor.......fvillacob 
     * @version.....1.0.     
     **/  
    private List invertirArbol(List lista)throws Exception{
        List arbol = new LinkedList();
        try{
            for(int i=(lista.size()-1);i>=0;i--){
                 Hashtable  padre = (Hashtable)lista.get(i);
                 arbol.add( padre );
            }
            
        }catch(Exception e){
            throw new Exception ("invertirArbol:" + e.getMessage());
        }
        return arbol;
    }
    
    
    
    
    
    /**
     * M�todos que busca los movimientos de la cuenta
     * @autor.......fvillacob 
     * @version.....1.0.     
     **/ 
    public List buscarMovimientos(String distrito, String cuenta)throws Exception{
        List movimientos = new LinkedList();
        try{
            movimientos = DAO.getMovimiento(distrito, cuenta, this.fechaIni, this.fechaFin, this.tipoAuxiliar, this.auxiliar );            
        }catch(Exception e){
            throw new Exception ("buscarMovimientos " + e.getMessage());
        }
        return movimientos;
    }
    
    
    
    
    
    
    
    
    /**
     * M�todos que genera el treeview de cuentas
     * @autor.......fvillacob 
     * @version.....1.0.     
     **/ 
    public String treeViewCuentas(String distrito, String elemento)throws Exception{
        String code = "";
        String sw   = "";
        try{
            
            List opciones = this.DAO.getCuentasTreeView(distrito);
            for(int i=0;i<opciones.size();i++){
                Hashtable opcion = (Hashtable)opciones.get(i);
                String folder    = (String)opcion.get("folder"); 
                String level     = (String)opcion.get("nivel");
                String item      = (String)opcion.get("item");
                
                if( folder.equals("S") && level.equals("1") ){
                    
                    if( !sw.equals("")  && !item.equals(sw)){
                        String var = sw.replaceAll(" ","");
                        code += this.codeCierre( var );
                    }  
                    
                }
                
                code  +=  (folder.equals("S") )?this.codeFolder(opcion): this.codeNodo(opcion, elemento);
                 sw    =  (folder.equals("S") && level.equals("1") )? item: sw; 
            }
            
        }catch(Exception e){
            throw new Exception( e.getMessage() );
        }
        return code;
    }
    
    
    
    
   
    
    public String codeFolder(Hashtable cuenta) throws Exception{
        String js = "";
        try{
            
            String padre      = (String) cuenta.get("padre");
            String var        = (String) cuenta.get("item");
            String des        = (String) cuenta.get("nombreitem");
            String level      = (String) cuenta.get("nivel");            
            
            String nameVarPadre    =  (level.equals("1"))? padre :"CTA_" + padre;
            String nameVar         =  "CTA_" + var;
            
            js=
            "\n\t  " + nameVarPadre  +".addItem('"+ des +"','','text','','');  "+
            "\n\t  var " + nameVar   +" = null;                                "+
            "\n\t  "     + nameVar   +" = new MTMenu();                        "+
            "\n\t  "     + nameVarPadre +".makeLastSubmenu("+ nameVar +",false);      ";
        }
        catch(Exception ex){
            throw new Exception(ex.getMessage());
        }
        return js;
    }
    
    
    
    
    
    
    public String codeNodo(Hashtable cuenta, String elemento) throws Exception{
        String js = "";
        try{
            String padre      = (String) cuenta.get("padre");
            String des        = (String) cuenta.get("nombreitem");
            String var        = (String) cuenta.get("item");
            
            String nameVar    =  "CTA_" + padre;
            
            js =  "\n\t  " + nameVar +".addItem('" + des +"','"+ var +"','text','',''); \n";            
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
        return js;
    }
    
    
   
    public String codeCierre(String var){
        String js   = " menu.makeLastSubmenu("+ "CTA_"+ var +",false); \n ";
        return js;
    }
    
    
    
    
    
   
    
    
    
    
    
    //____________________________________________________________________________
    
    
    
    /**
     * Getter for property listTipoAuxiliar.
     * @return Value of property listTipoAuxiliar.
     */
    public java.util.List getListTipoAuxiliar() {
        return listTipoAuxiliar;
    }    
    
    /**
     * Setter for property listTipoAuxiliar.
     * @param listTipoAuxiliar New value of property listTipoAuxiliar.
     */
    public void setListTipoAuxiliar(java.util.List listTipoAuxiliar) {
        this.listTipoAuxiliar = listTipoAuxiliar;
    }    
    
    
    
    
    
    
    /**
     * Getter for property listCuentas.
     * @return Value of property listCuentas.
     */
    public java.util.List getListCuentas() {
        return listCuentas;
    }    
    
    /**
     * Setter for property listCuentas.
     * @param listCuentas New value of property listCuentas.
     */
    public void setListCuentas(java.util.List listCuentas) {
        this.listCuentas = listCuentas;
    }    
    
    
    
    
    
    /**
     * Getter for property periodo.
     * @return Value of property periodo.
     */
    public java.lang.String getPeriodo() {
        return periodo;
    }    
    
    /**
     * Setter for property periodo.
     * @param periodo New value of property periodo.
     */
    public void setPeriodo(java.lang.String periodo) {
        this.periodo = periodo;
    }   
    
    
    
    
    /**
     * Getter for property fechaIni.
     * @return Value of property fechaIni.
     */
    public java.lang.String getFechaIni() {
        return fechaIni;
    }    
    
    /**
     * Setter for property fechaIni.
     * @param fechaIni New value of property fechaIni.
     */
    public void setFechaIni(java.lang.String fechaIni) {
        this.fechaIni = fechaIni;
    }    
    
    
    
    
    
    /**
     * Getter for property fechaFin.
     * @return Value of property fechaFin.
     */
    public java.lang.String getFechaFin() {
        return fechaFin;
    }    
    
    
    /**
     * Setter for property fechaFin.
     * @param fechaFin New value of property fechaFin.
     */
    public void setFechaFin(java.lang.String fechaFin) {
        this.fechaFin = fechaFin;
    }    
    
    
    
    
    
    /**
     * Getter for property cuentaIni.
     * @return Value of property cuentaIni.
     */
    public java.lang.String getCuentaIni() {
        return cuentaIni;
    }    
    
    /**
     * Setter for property cuentaIni.
     * @param cuentaIni New value of property cuentaIni.
     */
    public void setCuentaIni(java.lang.String cuentaIni) {
        this.cuentaIni = cuentaIni;
    }    
    
    
    
    
    
    /**
     * Getter for property cuentaFin.
     * @return Value of property cuentaFin.
     */
    public java.lang.String getCuentaFin() {
        return cuentaFin;
    }    
    
    /**
     * Setter for property cuentaFin.
     * @param cuentaFin New value of property cuentaFin.
     */
    public void setCuentaFin(java.lang.String cuentaFin) {
        this.cuentaFin = cuentaFin;
    }
    
    
    
    
    
    /**
     * Getter for property auxiliar.
     * @return Value of property auxiliar.
     */
    public java.lang.String getAuxiliar() {
        return auxiliar;
    }
    
    /**
     * Setter for property auxiliar.
     * @param auxiliar New value of property auxiliar.
     */
    public void setAuxiliar(java.lang.String auxiliar) {
        this.auxiliar = auxiliar;
    }
    
    
    
    
    
    
    /**
     * Getter for property tipoAuxiliar.
     * @return Value of property tipoAuxiliar.
     */
    public java.lang.String getTipoAuxiliar() {
        return tipoAuxiliar;
    }
    
    /**
     * Setter for property tipoAuxiliar.
     * @param tipoAuxiliar New value of property tipoAuxiliar.
     */
    public void setTipoAuxiliar(java.lang.String tipoAuxiliar) {
        this.tipoAuxiliar = tipoAuxiliar;
    }
    
    
    
    
    /**
     * Getter for property distrito.
     * @return Value of property distrito.
     */
    public java.lang.String getDistrito() {
        return distrito;
    }    
    
    /**
     * Setter for property distrito.
     * @param distrito New value of property distrito.
     */
    public void setDistrito(java.lang.String distrito) {
        this.distrito = distrito;
    }    
    
    
    
    
    
    
    /**
     * Getter for property cuentaPrevia.
     * @return Value of property cuentaPrevia.
     */
    public java.util.Hashtable getCuentaPrevia() {
        return cuentaPrevia;
    }    
    
    /**
     * Setter for property cuentaPrevia.
     * @param cuentaPrevia New value of property cuentaPrevia.
     */
    public void setCuentaPrevia(java.util.Hashtable cuentaPrevia) {
        this.cuentaPrevia = cuentaPrevia;
    }    
    
    
    
    
    /**
     * Getter for property nombreAuxiliar.
     * @return Value of property nombreAuxiliar.
     */
    public java.lang.String getNombreAuxiliar() {
        return nombreAuxiliar;
    }
    
    /**
     * Setter for property nombreAuxiliar.
     * @param nombreAuxiliar New value of property nombreAuxiliar.
     */
    public void setNombreAuxiliar(java.lang.String nombreAuxiliar) {
        this.nombreAuxiliar = nombreAuxiliar;
    }
    
    
   
     
    /**
     * M�todo que obtiene los los datos de los movimientos auxiliares
     * segun los parametros establecidos
     * @autor: Mario Fontalvo
     * @param cuenta_ri, rango inicial de la cuenta
     * @param cuenta_rf, rango final de la cuenta
     * @param fecha_ri, rango inicial de la fecha
     * @param fecha_ri, rango final de la fecha
     * @throws Exception.
     */
    public void obtenerCuentas(Usuario usuario, String dstrct, String cuenta_ri, String cuenta_rf, String fecha_ri, String fecha_rf, String cuenta, String periodo) throws Exception{
        datos = DAO.obtenerCuentas(usuario,dstrct, cuenta_ri, cuenta_rf, fecha_ri, fecha_rf, cuenta, periodo);
    }
        

    /**
     * Getter for property datos.
     * @return Value of property datos.
     */
    public Vector getDatos() {
        return datos;
    }    
    
    /**
     * Setter for property datos.
     * @param datos New value of property datos.
     */
    public void setDatos(Vector datos) {
        this.datos = datos;
    }
    
}
