/********************************************************************
 *  Nombre Clase.................   TablaMovconService.java
 *  Descripci�n..................   Service del DAO de la tabla movcon
 *  Autor........................   Ing. Leonardo Parody
 *  Fecha........................   09.02.2006
 *  Versi�n......................   1.0
 *  Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/
package com.tsp.finanzas.contab.model.services;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.finanzas.contab.model.beans.*;
import com.tsp.finanzas.contab.model.DAO.*;

/**
 *
 * @author  EQUIPO12
 */
public class TablaMovconService {
    TablaMovconDAO dao;
    /** Creates a new instance of TablaMovconService */
    public TablaMovconService() {
        dao = new TablaMovconDAO();
    }
    
     /**
         * Metodo InsertMSF900movcon , Metodo que llama a InsertMSF900movcon en el DAO
         * @autor : Ing. Leonardo Parody
         * @param : String processdate
         * @param : String dstrct
         * @param : String base
         * @param : String user
         * @param : String dstrct_code
         * @return : void
         * @version : 1.0
         */
        
        public void InsertMSF900movcon(List processdate, String dstrct, String base, String user) throws SQLException{
            ////System.out.println("Estoy en el service");    
            dao.InsertMSF900movcon(processdate, dstrct, base, user);
            ////System.out.println("ya me vine del dao");
        }
        
        /**
         * Metodo InsertMSF900movcon , Metodo que llama a InsertMSF900movcon en el DAO
         * @autor : Ing. Leonardo Parody
         * @param : String processdate
         * @param : String dstrct
         * @param : String base
         * @param : String user
         * @param : String dstrct_code
         * @return : void
         * @version : 1.0
         */
        
        public void InsertMSF900movconPeriodo(String periodo, String dstrct, String base, String user) throws SQLException{
            ////System.out.println("Estoy en el service");    
            dao.InsertMSF900movconPeriodo(periodo, dstrct, base, user);
            ////System.out.println("ya me vine del dao");
        }
        
        /**
         * Metodo InsertPtoGastosAdmin , Metodo que llama a InsertMovconPresGastosAdmin en el DAO
         * @autor : Ing. Leonardo Parody
         * @param : String dstrct
         * @param : String base
         * @param : String user
         * @param : String dstrct_code
         * @return : void
         * @version : 1.0
         */
        
        public void InsertPtoGastosAdmin(String dstrct, String base, String user,String a�omes, String check) throws SQLException{
            ////System.out.println("Estoy en el service");    
            dao.InsertMovconPresGastosAdmin(dstrct, base, user, check, a�omes); 
            ////System.out.println("ya me vine del dao");
        }
        
        /**
         * Metodo InsertPtoGastosAdmin , Metodo que llama a InsertMovconPresGastosAdmin en el DAO
         * @autor : Ing. Leonardo Parody
         * @param : String dstrct
         * @param : String base
         * @param : String user
         * @param : String dstrct_code
         * @return : void
         * @version : 1.0
         */
        
        public void InicializaPresGastosAdmin(String dstrct, String base, String user, String a�omes) throws SQLException{
            ////System.out.println("Estoy en el service");    
            dao.InicializaPresGastosAdmin(dstrct, user, a�omes, base);
            ////System.out.println("ya me vine del dao");
        }
        
        /**
         * Metodo getFechadehoy , Metodo que llama a getFechadehoy en el DAO
         * @autor : Ing. Leonardo Parody
         * @return : Calendar
         * @version : 1.0
         */
        
        public Calendar getFechadehoy() throws SQLException{
            return dao.getFechadeHoy();
        }
        
        /**
         * Metodo getFechadehoy , Metodo que llama a getFechadehoy en el DAO
         * @autor : Ing. Leonardo Parody
         * @version : 1.0
         */
        
        public void setFechadehoy(Calendar Fechadehoy) throws SQLException{
            dao.setFechadeHoy(Fechadehoy);
        }
        /**
         * Metodo getFechadeinicioProceso , Metodo que llama a getFechainicioproceso en el DAO
         * @autor : Ing. Leonardo Parody
         * @return : Calendar
         * @version : 1.0
         */
        
        public Calendar getFechadeinicioProceso() throws SQLException{
            return dao.getFechainicioproceso();
        }
        
        /**
         * Metodo InicializarMovcon , Metodo que llama a getFechainicioproceso en el DAO
         * @autor : Ing. Leonardo Parody
         * @param : String periodo
         * @return : Calendar
         * @version : 1.0
         */
        
        public void InicializarMovcon( String periodo ) throws SQLException{
            dao.InicializarMovcon( periodo ) ;
        }
        
        
        
         /**
         * Metodo InsertValoresejecutadosPorPeriodos , Metodo que llama a getFechainicioproceso en el DAO
         * @autor : Ing. Leonardo Parody
         * @param : String dstrct
         * @param : String user
         * @param : String a�omes
         * @param : String base
         * @return : Calendar
         * @version : 1.0
         */
        
        public void InsertValoresejecutadosPorPeriodos( String dstrct, String user, String a�omes, String base ) throws SQLException{
            dao.InsertValoresejecutadosPorPeriodos(dstrct, a�omes, base, user);
        }
    
}
