/*********************************************************************************
 * Nombre clase :      ComprobantesService.java                                  *
 * Descripcion :       Service del ComprobantesService.java                      *
 * Autor :             KREALES                                                   *
 * Fecha :             05 de JUNIO de 2006, 1:45 PM                              *
 * Version :           1.0                                                       *
 * Copyright :         Transporte Sanchez Polo                                   *
 *********************************************************************************/

package com.tsp.finanzas.contab.model.services;

import com.tsp.finanzas.contab.model.beans.*;
import com.tsp.finanzas.contab.model.DAO.*;
import com.tsp.operation.model.beans.Usuario;
import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.*;

public class ComprobantesService {
    
    private ComprobantesDAO dao;
    private String fecha_ini;
    private String fecha_fin;
    
    
    private int    grupTrans;
    
    
    
    /** Creates a new instance of ImprimirOrdenDeCargaService */
    public ComprobantesService() {
        dao = new ComprobantesDAO();   
    }
    public ComprobantesService(String dataBaseName) {
        dao = new ComprobantesDAO(dataBaseName);   
    }
    /**
     * Getter for property grupTrans.
     * @return Value of property grupTrans.
     */
    public int getGrupTrans() {
        return grupTrans;
    }
    
    /**
     * Setter for property grupTrans.
     * @param grupTrans New value of property grupTrans.
     */
    public void setGrupTrans(int grupTrans) {
        this.grupTrans = grupTrans;
    }
    
    
    /*Metodo Descontabilizacion*/
    /*Metodo Descontabilizacion*/
    public String descontabilizar( String distrito, String tipo_doc, String documento, int transaccion_num, Usuario usuario  ){
        String query = "";
        try{
            
            String fecha_actual = Util.getFechaActual_String(6);
            this.grupTrans = 0;
            
            
            com.tsp.finanzas.contab.model.Model modFin = new com.tsp.finanzas.contab.model.Model(usuario.getBd());
            
            String codigo_documento = modFin.comprobanteService.equivalenciaTipo_documento(tipo_doc);
            
            modFin.comprobanteService.getComprobante( codigo_documento, documento, transaccion_num );
            
            com.tsp.finanzas.contab.model.beans.Comprobantes comp = new com.tsp.finanzas.contab.model.beans.Comprobantes();
            comp                                                  = modFin.comprobanteService.getComprobante();
            
            double credito = 0;
            double debito  = 0;
            
            /*Creacion del Nuevo del Objeto Comprobante*/
            
            if( comp != null ){
                int grupo_transaccion   = comp.getGrupo_transaccion();
                credito                 = comp.getTotal_credito();
                debito                  = comp.getTotal_debito();
                comp.setTotal_debito( credito);
                comp.setTotal_credito( debito );
                comp.setDstrct( distrito );
                comp.setSucursal( "OP" );
                comp.setFechadoc( Util.getFechaActual_String(4) );
                //comp.setDetalle( "DESCONTABILIZACION  "+ comp.getDetalle() );
                comp.setDetalle( "ANULACION  "+ comp.getDetalle() ); //AMATURANA 05.04.2007
                comp.setPeriodo( fecha_actual.substring(0,4) + fecha_actual.substring(5,7) );
                comp.setGrupo_transaccion( modFin.comprobanteService.getGrupoTransaccion() );
                
                
                this.grupTrans = comp.getGrupo_transaccion();
                comp.setUsuario( usuario.getLogin() );
                comp.setBase( usuario.getBase() );
                modFin.comprobanteService.setComprobante( comp );
                
                query += modFin.comprobanteService.getInsert() + ";";
                
                comp.setGrupo_transaccion( grupo_transaccion );
                
                modFin.comprobanteService.getDetallesComprobante(comp);
                for( int k=0; k < modFin.comprobanteService.getVector().size(); k++ ){
                    com.tsp.finanzas.contab.model.beans.Comprobantes compVec = ( com.tsp.finanzas.contab.model.beans.Comprobantes )modFin.comprobanteService.getVector().get( k );
                    compVec.setPeriodo( fecha_actual.substring(0,4) + fecha_actual.substring(5,7) );
                    credito = 0;
                    debito  = 0;
                    credito = compVec.getTotal_credito();
                    debito  = compVec.getTotal_debito();
                    compVec.setTotal_debito( credito );
                    compVec.setTotal_credito( debito );
                    compVec.setGrupo_transaccion( grupTrans );
                    compVec.setDetalle( "DESCONTABILIZACION "+ compVec.getDetalle() );
                    compVec.setDocumento_interno( tipo_doc );
                    compVec.setUsuario( usuario.getLogin() );
                    compVec.setBase( usuario.getBase() );
                    modFin.comprobanteService.setComprobante( compVec );
                    query += modFin.comprobanteService.getInsertItem()+ ";";
                }
                
            }
            
            
        }catch(Exception e){
            e.printStackTrace();
        }
        return query;
    }
    
    
    public String equivalenciaTipo_documento(  String tipo_documento  ){
        String codigo = "";
        try{
            codigo = dao.equivalenciaTipo_documento( tipo_documento );
        }catch(Exception e){
            e.printStackTrace();
        }
        return codigo;
    }
    
    
    public String descontabilizacionOP( String factura ){
        
        String query = "";
        try{
            query = dao.descontabilizarOP(factura);
        }catch(Exception e){
            e.printStackTrace();
        }
        return query;
    }
    
    public String descontabilizarFactura( String usuario_anulo, int transaccion_anulacion, String distrito, String tipo_documento , String documento ){
        
        String query = "";
        try{
            query = dao.descontabilizarFactura(usuario_anulo, transaccion_anulacion, distrito, tipo_documento, documento);
        }catch(Exception e){
            e.printStackTrace();
        }
        return query;
    }
    
    
    /**
     *Metodo que inserta orden de carga
     *@autor: Karen Reales
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public String insert() throws SQLException {
        return dao.insert();
    }
    
    /**
     *Metodo que modifica la planilla y coloca la fecha de contabilizacion
     *el periodo y el numero de la transaccion
     *@autor: Karen Reales
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public String  marcarPlanilla() throws SQLException {
        return dao.marcarPlanilla();
    }
    
    /**
     * Getter for property comprobante.
     * @return Value of property comprobante.
     */
    public com.tsp.finanzas.contab.model.beans.Comprobantes getComprobante() {
        return dao.getComprobante();
    }
    
    /**
     * Setter for property comprobante.
     * @param comprobante New value of property comprobante.
     */
    public void setComprobante(com.tsp.finanzas.contab.model.beans.Comprobantes comprobante) {
        dao.setComprobante(comprobante);
    }
    /**
     *Metodo que modifica la planilla y coloca la fecha de contabilizacion
     *el periodo y el numero de la transaccion
     *@autor: Karen Reales
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void buscarPlanillas(String tipodoc) throws SQLException {
        dao.buscarPlanillas(tipodoc);
    }
    
    /**
     * Getter for property vector.
     * @return Value of property vector.
     */
    public java.util.Vector getVector() {
        return dao.getVector();
    }
    /**
     * Setter for property vector.
     * @param vector New value of property vector.
     */
    public void setVector(java.util.Vector vector) {
        dao.setVector(vector);
    }
    
    /**
     *Metodo que modifica la planilla y coloca la fecha de contabilizacion
     *el periodo y el numero de la transaccion
     *@autor: Karen Reales
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void buscarPlarem(String numpla) throws SQLException {
        dao.buscarPlarem(numpla);
    }
    /**
     *Metodo que inserta orden de carga
     *@autor: Karen Reales
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public String insertItem() throws SQLException {
        return dao.insertItem();
    }
    
    /**
     *Metodo que modifica la planilla y coloca la fecha de contabilizacion
     *el periodo y el numero de la transaccion
     *@autor: Karen Reales
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public int getGrupoTransaccion() throws SQLException {
        return dao.getGrupoTransaccion();
    }
    /**
     *Metodo que modifica la planilla y coloca la fecha de contabilizacion
     *el periodo y el numero de la transaccion
     *@autor: Karen Reales
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public String getTipoDocumento(String documento) throws SQLException {
        return dao.getTipoDocumento(documento);
    }
    /**
     *Metodo que permite obtener la todos los comprobantes con
     *no tengan fecha de aplicaci�n es decir la fecha por defecto
     *del campo
     *@param fechaInicial la fecha de inicio del comprobante
     *@param fechaFinal la fecha final del comprobante
     *@autor: David Pi�a
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void getComprobantesSinAplicacion( String fechaInicial, String fechaFinal ) throws SQLException {
        dao.getComprobantesSinAplicacion( fechaInicial, fechaFinal  );
    }
    /**
     *Metodo que permite realizar la sumatoria debito y
     *@autor: David Pi�a
     *@param comprobante El comprobante que contiene la llave para realizar la sumatoria
     *                   tambien tiene el campo al que se desea aplicar la sumatoria
     *@param campo nombre del campo que se desea aplicar la sumatoria
     *@throws: En caso de que un error de base de datos ocurra.
     *@return la sumatoria del campo dado
     */
    public double getSumatoriaComprobantes( Comprobantes comprobante, String campo ) throws SQLException {
        return dao.getSumatoriaComprobantes( comprobante, campo );
    }
    /**
     *Metodo que permite obtener los detalles de un comprobante sobre la tabla
     *COMPRODET
     *@autor: David Pi�a
     *@param comprobante contiene la informaci�n o filtros para obtener el detalle de un
     *                   compobante
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void getDetallesComprobante( Comprobantes comprobante ) throws SQLException {
        dao.getDetallesComprobante( comprobante );
    }
    /**
     *Metodo que permite actualizar la fecha de aplicacion de un comprobante
     *@autor: David Pi�a
     *@param comprobante contiene la informaci�n o filtros para obtener el detalle de un
     *                   compobante
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void updateAplicacionComprobante( Comprobantes comprobante ) throws SQLException {
        dao.updateAplicacionComprobante( comprobante );
    }
    /*Karen Reales*/
    
    /**
     *Metodo que inserta orden de carga
     *@autor: Karen Reales
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void listarComprobantes() throws SQLException {
        dao.listarComprobantes();
    }
    
    /**
     *Metodo que busca una lista de detalles de un comprobante dado un tipo de documento
     *de contabilizacion.
     *@autor: Karen Reales
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void listarComprobantesDet() throws SQLException {
        dao.listarComprobantesDet();
    }
    /**
     *Metodo que busca una lista de movpla dado un tipo de documento
     *@param: Tipo de documento para buscar cmc
     *@autor: Karen Reales
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void buscarMovpla(String tipoDoc) throws SQLException {
        dao.buscarMovpla(tipoDoc);
    }
    /**
     *Metodo que modifica el movpla y coloca la fecha de contabilizacion
     *el periodo y el numero de la transaccion
     *@autor: Karen Reales
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public String  marcarMovPlanilla() throws SQLException {
        return dao.marcarMovPlanilla();
    }
    /**
     *Metodo busca una lista de remesas sin contabilizar dado un tipo de documento
     *@param: Tipo de documento para buscar cmc
     *@autor: Karen Reales
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void buscarRemesas(String tipoDoc) throws SQLException {
        dao.buscarRemesas(tipoDoc);
    }
    /**
     *Metodo que modifica la remesa y coloca la fecha de contabilizacion
     *el periodo y el numero de la transaccion
     *@autor: Karen Reales
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public String marcarRemesa() throws SQLException {
        return dao.marcarRemesa();
    }
    /**
     *Metodo que busca una lista de egresos sin contabilizar
     *@autor: Karen Reales
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void buscarEgresos() throws SQLException {
        dao.buscarEgresos();
    }
    /**
     *Metodo que busca una lista de egresos detallados dado un egreso
     *@autor: Karen Reales
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void buscarEgresosDet(Comprobantes compro) throws SQLException {
        dao.buscarEgresosDet(compro); 
    }
    /**
     *Metodo que busca una lista de remesas anuladas sin contabilizar
     *@autor: Karen Reales
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void buscarListaRemesasAnuladas() throws SQLException {
        dao.buscarListaRemesasAnuladas();
    }
    /**
     *Metodo que modifica el egreso y coloca la fecha de contabilizacion
     *el periodo y el numero de la transaccion
     *@autor: Karen Reales
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public ArrayList<String> marcarEgreso() throws SQLException, Exception {
        return dao.marcarEgreso();
    }
    /**
     *Metodo que busca una lista de egresos anulados sin contabilizar
     *@autor: Karen Reales
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void buscarListaEgresosAnulados() throws SQLException {
        dao.buscarListaEgresosAnulados();
    }
    /**
     *Metodo que busca una lista de facturas sin contabilizar su ajuste por cambio
     *@param: Tipo de documento para buscar un cmc
     *@autor: Karen Reales
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void buscarAjustesXCambio(String tipoDoc) throws SQLException {
        dao.buscarAjustesXCambio(tipoDoc);
    }
    /**
     *Metodo que busca una lista de facturas sin contabilizar su ajuste por valor
     *@param: Tipo de documento para buscar un cmc
     *@autor: Karen Reales
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void buscarAjustesXValor(String tipoDoc) throws SQLException {
        dao.buscarAjustesXValor(tipoDoc);
    }
    /**
     *Metodo que modifica el egreso anulado y coloca la fecha de contabilizacion
     *el periodo y el numero de la transaccion
     *@autor: Karen Reales
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public String marcarEgresoAnulado() throws SQLException {
        return dao.marcarEgresoAnulado();
    }
    /**
     *Metodo que modifica el documento (Planilla o Remesa) anulado y coloca la fecha de contabilizacion
     *el periodo y el numero de la transaccion
     *@autor: Karen Reales
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public String marcarDocumentoAnulado() throws SQLException {
        return dao.marcarDocumentoAnulado();
    }
    /**
     *Metodo que modifica un plarem y coloca la fecha de contabilizacion
     *el periodo y el numero de la transaccion a aquellas planillas redistribuidas
     *@autor: Karen Reales
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public String  marcarRedistribucion() throws SQLException {
        return dao.marcarRedistribucion();
    }
    /**
     *Metodo que modifica un cxpdoc y coloca la fecha de contabilizacion
     *el periodo y el numero de la transaccion
     *@autor: Karen Reales
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public String marcarDifXCambio() throws SQLException {
        return dao.marcarDifXCambio();
    }
    /**
     *Metodo que modifica un cxpdoc y coloca la fecha de contabilizacion
     *el periodo y el numero de la transaccion
     *@autor: Karen Reales
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public String marcarDifXValor() throws SQLException {
        return dao.marcarDifXValor();
    }
    /**
     *Metodo que busca una lista de planillas anuladas sin contabilizar
     *@autor: Karen Reales
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void buscarListaPlaAnuladas() throws SQLException {
        dao.buscarListaPlaAnuladas();
    }
    /**
     *Metodo que busca una lista de plarem sin contabilizar
     *@autor: Karen Reales
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void buscarListaPlarem() throws SQLException {
        dao.buscarListaPlarem();
    }
    /**
     *Metodo que busca una una planilla dada y los anticipos de la planilla
     *@param: Numero de la planilla
     *@autor: Karen Reales
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void buscarPlanilla(String numpla) throws SQLException {
        dao.buscarPlanilla(numpla);
    }
    /**
     *Metodo que modifica un plarem y coloca la fecha de contabilizacion
     *el periodo y el numero de la transaccion
     *@autor: Karen Reales
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public String marcarPlarem() throws SQLException {
        return dao.marcarPlarem();
    }
    
    /**
     * M�todo que busca la moneda local
     * @autor.......Karen Reales
     * @throws......Exception
     * @version.....1.0.
     **/
    public  String getMoneda(String distrito) throws Exception{
        return dao.getMoneda(distrito);
    }
    /**
     *Metodo que obtiene los tipos de documento que se encuentren en comprodet
     *@autor: Osvaldo P�rez Ferrer
     *@throws: En caso de que un error de base de datos ocurra.
     *@return: vector
     */
    
    public Vector obtenerTiposDoc( String db) throws SQLException{
        return dao.obtenerTiposDoc(db);
    }
    /**
     *Metodo que inserta los comprodet en la tabla temporal con
     *distrito, periodo, tipodoc para las posibles asociaciones
     *de las cuentas CGI a una cuenta normal
     *@autor: Osvaldo P�rez Ferrer
     *@param dstrct distrito
     *@param periodo periodo
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void  pasarATemporal( String dstrct, String periodo, String db) throws SQLException{
        dao.pasarATemporal(dstrct, periodo, db );
    }
    
    /**
     *Metodo que obtiene los comprobantes con tipodoc dado
     *con sus totales debito y credito
     *@autor: Osvaldo P�rez Ferrer
     *@throws: En caso de que un error de base de datos ocurra.
     *@param: tipodoc tipo de documento
     *@return: vector
     */
    public Vector obtenerCreditoDebitoTipodoc(String tipodoc, String db) throws SQLException{
        return dao.obtenerCreditoDebitoTipodoc(tipodoc, db);
    }
    
    /**
     *Metodo que crea una tabla temporal para estandarizar los comprodet
     *y generar el Libro Diario
     *@autor: Osvaldo P�rez Ferrer
     *@throws: En caso de que un error de base de datos ocurra.
     *@param: nombre para la tabla
     */
    public void crearTablaTemporal(String nombre) throws SQLException{
        dao.crearTablaTemporal(nombre);
    }
    
    /**
     *Metodo que borra la tabla dada del esquema tem
     *@autor: Osvaldo P�rez Ferrer
     *@throws: En caso de que un error de base de datos ocurra.
     *@param: nombre de la tabla
     */
    public void dropTablaTemporal(String nombre) throws SQLException{
        dao.dropTablaTemporal(nombre);
    }
    /**
     *Metodo que verifica si existe el periodo dado en comprodet
     *@autor: Osvaldo P�rez Ferrer
     *@throws: En caso de que un error de base de datos ocurra.
     *@param: periodo
     *@return true si existe el periodo, false en caso contrario
     */
    public boolean existePeriodo(String periodo) throws SQLException{
        return dao.existePeriodo(periodo);
    }
    /**
     *Metodo que devuelve la consulta sql para insertar un comprodet
     *@autor: David Pi�a Lopez
     *@throws: En caso de que un error de base de datos ocurra.
     *@return Un String con el sql
     */
    public String getInsertItem() throws SQLException {
        return dao.getInsertItem();
    }
    /**
     *Metodo que devuelve la consulta sql para insertar un comprobante
     *@autor: David Pi�a Lopez
     *@throws: En caso de que un error de base de datos ocurra.
     *@return Un String con el sql
     */
    public String getInsert() throws SQLException {
        return dao.getInsert();
    }
    /**
     *Metodo que permite actualizar el numero de items de un comprobante retornando la cadena
     *con el sql
     *@autor: David Pi�a
     *@param comprobante contiene la informaci�n o filtros para obtener el detalle de un
     *                   compobante
     *@throws: En caso de que un error de base de datos ocurra.
     *@return La cadena con el sql para actualizar el comprobante
     */
    public String getUpdateNumItems( Comprobantes comprobante ) throws SQLException {
        return dao.getUpdateNumItems( comprobante );
    }
    
    /**
     *Funci�n que permite obtener Comprobantes dados unos criterios
     *@autor: David Pi�a
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void getComprobantes() throws SQLException {
        dao.getComprobantes();
    }
    /**
     *Metodo que obtiene un camprobante no contabilizado en base a su llave primaria
     *@param tipodoc Tipo de documento
     *@param numdoc Numero de docuento
     *@param grupo_transaccion Grupo de transaccion
     *@autor: David Pi�a
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void getComprobante( String tipodoc, String numdoc, int grupo_transaccion ) throws SQLException {
        dao.getComprobante( tipodoc, numdoc, grupo_transaccion );
    }
    
    /**
     * Metodo para extraer los comprobantes de diarios
     * @autor mfontalvo
     * @param tipodoc Tipo de documento
     * @param numdoc numero del documento
     * @param numtransaccion grupo transaccion
     * @param periodo Periodo contable
     * @throws Exception.
     * @return Vector vector de comprobantes.
     */
    public Vector obtenerComprobantes(String tipodoc, String numdoc, String numtransaccion, String periodo) throws Exception{
        return dao.getComprobantes(tipodoc, numdoc, numtransaccion, periodo);
    }
    
    /**
     * Metodo para extraer los comprobantes de diarios
     * @autor mfontalvo
     * @param Vector vector de comprobantes a setear
     * @throws Exception.
     * @return Vector vector de comprobantes.
     */
    public Vector obtenerComprobantes(Vector datos) throws Exception{
        return dao.getComprobantes(datos);
    }
    
    /**
     *Metodo que permite obtener las cuentas tipo I y C con elemento de gasto 8005 y 9005
     *respectivamente teniendo en cuenta el periodo y rango de fechas dado
     *@autor: David Pi�a
     *@param periodo periodo del detalle del comprobante
     *@param fechaInicio La fecha de inicio
     *@param fechaFin La fecha de Final
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void getCuentasIC( String periodo, String fechaInicio, String fechaFin ) throws SQLException {
        dao.getCuentasIC( periodo, fechaInicio, fechaFin );
    }
    
    /**
     *Metodo que modifica tablagen y coloca el ultimo numero de transaccion guardado
     *@autor: Karen Reales
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public String actualizarTablaGen() throws SQLException {
        return dao.actualizarTablaGen();
    }
    
    /**
     *Metodo que busca una planilla seleccionada para descontabilizar
     *@autor: Karen Reales
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void buscarPlanillaDescontabilizar(String documento) throws SQLException {
        dao.buscarPlanillaDescontabilizar(documento);
    }
    /**
     *Metodo que desmarca y borra los datos contabilizados y la op generada de una planilla
     *@autor: Karen Reales
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void descontabilizarPlanilla() throws SQLException {
        dao.descontabilizarPlanilla();
    }
    
    /**
     *Metodo que permite buscar los datos de egreso
     *@param document numero del egreso
     *@param transaction numero de la transaccion
     *@autor: mfontalvo
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public Hashtable searchEgreso( String document, String transaction ) throws SQLException {
        return dao.searchEgreso(document, transaction);
    }
    
    
    
    
    /**
     *Metodo que busca una lista de egresos sin contabilizar
     *@autor Ing. Andr�s Maturana De La Cruz
     *@param dstrct Distrito.
     *@param fechai Fecha inicial del per�odo.
     *@param fechaf Fecha final del per�odo.
     *@throws En caso de que un error de base de datos ocurra.
     */
    public void consultaComprobantes(String dstrct, String fechai, String fechaf) throws SQLException {
        dao.consultaComprobantes(dstrct, fechai, fechaf);
    }
    
    /**
     *Metodo que busca una lista de egresos sin contabilizar
     *@autor Ing. Andr�s Maturana De La Cruz
     *@param dstrct Distrito.
     *@param fechai Fecha inicial del per�odo.
     *@param fechaf Fecha final del per�odo.
     *@throws En caso de que un error de base de datos ocurra.
     */
    public void consultaComprobantesDetalle(String dstrct, String fechai, String fechaf) throws SQLException {
        dao.consultaComprobantesDetalle(dstrct, fechai, fechaf);
    }
    
    /**
     * Getter for property reporte.
     * @return Value of property reporte.
     */
    public java.util.Vector getReporte() {
        return dao.getReporte();
    }
    
    /**
     * Setter for property reporte.
     * @param reporte New value of property reporte.
     */
    public void setReporte(java.util.Vector reporte) {
        dao.setReporte(reporte);
    }
    
    /**
     * Getter for property fecha_ini.
     * @return Value of property fecha_ini.
     */
    public java.lang.String getFecha_ini() {
        return fecha_ini;
    }
    
    /**
     * Setter for property fecha_ini.
     * @param fecha_ini New value of property fecha_ini.
     */
    public void setFecha_ini(java.lang.String fecha_ini) {
        this.fecha_ini = fecha_ini;
    }
    
    /**
     * Getter for property fecha_fin.
     * @return Value of property fecha_fin.
     */
    public java.lang.String getFecha_fin() {
        return fecha_fin;
    }
    
    /**
     * Setter for property fecha_fin.
     * @param fecha_fin New value of property fecha_fin.
     */
    public void setFecha_fin(java.lang.String fecha_fin) {
        this.fecha_fin = fecha_fin;
    }
    
    public String descontabilizarFactura( String usuario_anulo, int transaccion_anulacion, String distrito, String proveedor, String tipo_documento , String documento ){
        
        String query = "";
        try{
            query = dao.descontabilizarFactura(usuario_anulo, transaccion_anulacion, distrito, proveedor, tipo_documento, documento);
        }catch(Exception e){
            e.printStackTrace();
        }
        return query;
    }
    
    /*Metodo Descontabilizacion*/
    public ArrayList<String> descontabilizar( String distrito, String tipo_doc, String documento, String clase_doc, int transaccion_num, Usuario usuario  ){
        ArrayList<String> query =new ArrayList<>();
        try{
            
            String fecha_actual = Util.getFechaActual_String(6);
            this.grupTrans = 0;
            
            
            com.tsp.finanzas.contab.model.Model modFin = new com.tsp.finanzas.contab.model.Model(usuario.getBd());
            
            //String codigo_documento = modFin.comprobanteService.equivalenciaTipo_documento(tipo_doc);
            String codigo_documento = "FAA";
            if( clase_doc.equals("0") ) codigo_documento = "FAC";
            else if ( clase_doc.equals("2") ) codigo_documento = "FNC";
            else if ( clase_doc.equals("3") ) codigo_documento = "FND";
            
            modFin.comprobanteService.getComprobante( codigo_documento, documento, transaccion_num );
            
            com.tsp.finanzas.contab.model.beans.Comprobantes comp = new com.tsp.finanzas.contab.model.beans.Comprobantes();
            comp                                                  = modFin.comprobanteService.getComprobante();
            
            double credito = 0;
            double debito  = 0;
            
            /*Creacion del Nuevo del Objeto Comprobante*/
            
            if( comp != null ){
                int grupo_transaccion   = comp.getGrupo_transaccion();
                credito                 = comp.getTotal_credito();
                debito                  = comp.getTotal_debito();
                comp.setTotal_debito( credito);
                comp.setTotal_credito( debito );
                comp.setDstrct( distrito );
                comp.setSucursal( "OP" );
                comp.setFechadoc( Util.getFechaActual_String(4) );
                //comp.setDetalle( "DESCONTABILIZACION  "+ comp.getDetalle() );
                comp.setDetalle( "ANULACION  "+ comp.getDetalle() ); //AMATURANA 05.04.2007
                comp.setPeriodo( fecha_actual.substring(0,4) + fecha_actual.substring(5,7) );
                comp.setGrupo_transaccion( modFin.comprobanteService.getGrupoTransaccion() );
                
                
                this.grupTrans = comp.getGrupo_transaccion();
                comp.setUsuario( usuario.getLogin() );
                comp.setBase( usuario.getBase() );
                modFin.comprobanteService.setComprobante( comp );
                
                query.add(modFin.comprobanteService.getInsert());
                
                comp.setGrupo_transaccion( grupo_transaccion );
                
                modFin.comprobanteService.getDetallesComprobante(comp);
                for( int k=0; k < modFin.comprobanteService.getVector().size(); k++ ){
                    com.tsp.finanzas.contab.model.beans.Comprobantes compVec = ( com.tsp.finanzas.contab.model.beans.Comprobantes )modFin.comprobanteService.getVector().get( k );
                    compVec.setPeriodo( fecha_actual.substring(0,4) + fecha_actual.substring(5,7) );
                    credito = 0;
                    debito  = 0;
                    credito = compVec.getTotal_credito();
                    debito  = compVec.getTotal_debito();
                    compVec.setTotal_debito( credito );
                    compVec.setTotal_credito( debito );
                    compVec.setGrupo_transaccion( grupTrans );
                    compVec.setDetalle( "DESCONTABILIZACION "+ compVec.getDetalle() );
                    compVec.setDocumento_interno( tipo_doc );
                    compVec.setUsuario( usuario.getLogin() );
                    compVec.setBase( usuario.getBase() );
                    modFin.comprobanteService.setComprobante( compVec );
                    query.add(modFin.comprobanteService.getInsertItem());
                }
                
            }
            
            
        }catch(Exception e){
            e.printStackTrace();
        }
        return query;
    }
    
    public String descontabilizacionOP( String factura, String numpla ){
        
        String query = "";
        try{
            query = dao.descontabilizarOP(factura, numpla);
        }catch(Exception e){
            e.printStackTrace();
        }
        return query;
    }
    
    
    /**
     *Metodo busca una lista de mov.remesas sin contabilizar dado un tipo de documento
     *@param: Tipo de documento para buscar cmc
     *@autor: Karen Reales
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void buscarMovRemesas(String tipoDoc) throws SQLException {
        dao.buscarMovRemesas(tipoDoc);
    }
    
    public String marcarMovRemesa() throws SQLException {
        return dao.marcarMovRemesa();
    }
    
     /**
     *Metodo que busca una lista de egresos sin contabilizar
     *@autor Ing. jose de la rosa
     *@param dstrct Distrito.
     *@param fechai Fecha inicial del per�odo.
     *@param fechaf Fecha final del per�odo.
     *@param tipodoc Tipo documento.
     *@param documento Documento.
     *@throws En caso de que un error de base de datos ocurra.
     */
    public void consultaReporteComprobantes(String dstrct, String fechai, String fechaf, String tipodoc, String documento ) throws SQLException {
        dao.consultaReporteComprobantes( dstrct, fechai, fechaf, tipodoc, documento );
    }
    
        /**
     *Metodo que permite actualizar el periodo de un comprobante
     *@autor: Ing. jose de la rosa
     *@param periodo, tipo documento, documento, grupo transaccion
     *@throws: En caso de que un error de base de datos ocurra.
     */    
    public void updatePeriodoComprobante( String periodo, String tipo, String documento, int transaccion ) throws SQLException {
        dao.updatePeriodoComprobante( periodo, tipo, documento, transaccion );
    }
     /**
     *Metodo que permite obtener la todos los comprobantes con
     *no tengan fecha de aplicaci�n es decir la fecha por defecto
     *del campo
     *@param fechaInicial la fecha de inicio del comprobante
     *@param fechaFinal la fecha final del comprobante
     *@autor: Mario Fontalvo
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void getComprobanteParaMayorizar( String tipodoc, String numdoc, int grupo_transaccion ) throws SQLException {
        dao.getComprobanteParaMayorizar(tipodoc, numdoc, grupo_transaccion);
    }
    
    /**
     *Metodo que permite obtener la cuenta de aprovechamineto dependiendo si es comision o cuantro x mil
     *y/o si es un negocio Fenalco o un prestamo.
     *@param tipo tipo de tabla
     *@param codEgreso codigo del egreso
     *@autor: tmolina
     *@return cuenta
     *@throws: SQLException En caso de que un error de base de datos ocurra.
     */
    public String getCuentaAprovechamiento( String tipo, String codEgreso ) throws SQLException {
       return dao.getCuentaAprovechamiento(tipo, codEgreso);
    }
    
}