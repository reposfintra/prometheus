/***********************************
 * Nombre Clase ............. ContabilizacionFacturasServices.java
 * Descripci�n  .. . . . . .  Permitimos realizar los servicios para la contabilizacion de facturas
 * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
 * Fecha . . . . . . . . . .  13/06/2006
 * versi�n . . . . . . . . .  1.0
 * Copyright ...............  Transportes Sanchez Polo S.A.
 ************************************/



package com.tsp.finanzas.contab.model.services;



import java.io.*;
import java.util.*;
import com.tsp.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.util.LogWriter;
import com.tsp.finanzas.contab.model.beans.*;
import com.tsp.finanzas.contab.model.DAO.ContabilizacionFacturasDAO;
import com.tsp.finanzas.contab.model.DAO.ContabilizacionFacturasCLDAO;
import com.tsp.finanzas.contab.model.DAO.ComprobantesDAO;
import com.tsp.finanzas.contab.model.DAO.MovAuxiliarDAO;
import com.tsp.operation.model.TransaccionService;
import com.tsp.operation.model.services.DirectorioService;
import com.tsp.operation.model.services.TasaService;
import com.tsp.operation.model.beans.Tasa;
import com.tsp.operation.model.beans.Usuario;


public class ContabilizacionFacturasServices {


    private  ContabilizacionFacturasDAO  dao;
    private  ContabilizacionFacturasCLDAO  daoCL;
    private  ComprobantesDAO             comproDAO;
    private  MovAuxiliarDAO              movDAO;
    private  List                        listaContable;
    private  boolean                     process;
    private String     IVA               = "IVA";
    private String     TIPO_FACTURA      = "010";
    private String     TIPO_FACTURA_AMD  = "FAA";
    private String     DIF_CAMBIO_OP     = "99";
    private LogWriter logWriter;
    public  List      listaErrores;
    public  String    ESTADO             =  "";
    public  int       CANT_ERROES        =  0;
    private Usuario usuario;

    
    private final String TIPO_OC         = "001";

    
    private final String FACTURA         = "010";
    private final String NC              = "035";
    private final String ND              = "036";
    



    public ContabilizacionFacturasServices() {
        dao            = new  ContabilizacionFacturasDAO();
        daoCL          = new  ContabilizacionFacturasCLDAO();
        comproDAO      = new  ComprobantesDAO();
        movDAO         = new  MovAuxiliarDAO();
        listaContable  = new LinkedList();
        process        = false;

        listaErrores   = new LinkedList();
        ESTADO         =  "";
        CANT_ERROES    =  0;

    }

    public ContabilizacionFacturasServices(String dataBaseName) {
        dao            = new  ContabilizacionFacturasDAO(dataBaseName);
        daoCL          = new  ContabilizacionFacturasCLDAO(dataBaseName);
        comproDAO      = new  ComprobantesDAO(dataBaseName);
        movDAO         = new  MovAuxiliarDAO(dataBaseName);
        listaContable  = new LinkedList();
        process        = false;

        listaErrores   = new LinkedList();
        ESTADO         =  "";
        CANT_ERROES    =  0;

    }

    public void reset(){
        listaContable  = new LinkedList();
    }





    /**
     * M�todo que carga las factura que no esten contabilizadas
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public void contabilizar(String distrito, String ayer, Usuario usuario)throws Exception{
        try{
            
            String periodo =  ayer.substring(0,7).replaceAll("-","");
            String hoy     =  Utility.getHoy("-");

            this.usuario = usuario;
            listaErrores   = new LinkedList();
            ESTADO         =  "";
            CANT_ERROES    =  0;
            listaContable  =  dao.getFacturasNoContabilizadas(distrito, ayer );

            // COMPROBANTES : FACTURAS
            for(int i=0;i<listaContable.size();i++){
                ComprobanteFacturas  comprobante = (ComprobanteFacturas) listaContable.get(i);
                comprobante.setFechadoc   ( ayer     );
                comprobante.setPeriodo    ( periodo  );
                comprobante.setTipo_operacion("IGEN");
                comprobante.setOrigen("A");
                String  TIPO_DOC    = comprobante.getTipodoc_factura();  // FAC o ND
                double  vlrDebito   = 0;
                double  vlrCredito  = 0;
                int sw=1;
                List listaDetalle   = new LinkedList();
                // ITEMS de la factura:
                //pregunto si el hc es diferente de fenalco, en caso afirmativo agrego un item al comprobante con la cuenta del hc
                //se debe agregar al query para que traiga la naturaleza de la cuenta, eso es en el caso este el otro queda igual
                //if( ((comprobante.getTipodoc_factura()).equals("FAC")  || comprobante.getTipodoc_factura().equals("FAG")) && (!(comprobante.getHc()).equals("01")))
                if( ((comprobante.getTipodoc_factura()).equals("FAC")  || comprobante.getTipodoc_factura().equals("FAG")) && (!(comprobante.getHc()).equals("01")) && (!(comprobante.getHc()).equals("04")) && (!(comprobante.getHc()).equals("AA"))  && (!(comprobante.getHc()).equals("DF")) )
                {
                    ComprobanteFacturas  comprodetn= new ComprobanteFacturas();
                    comprodetn=copy(comprobante,comprodetn);
                    comprodetn.setTotal_debito(comprobante.getTotal_debito());
                    comprodetn.setAuxiliar("");
                    //MOD PRONTO PAGO 27-abril-2009
                    if( (comprobante.getTotal_debito()<0) && (comprobante.getHc().equals("EX")) ) {
                        ////.out.println("aqui");
                        comprodetn.setTotal_debito(0);
                        comprodetn.setTotal_credito(comprobante.getTotal_debito() * -1);
                        vlrDebito  +=  comprodetn.getTotal_credito();
                        ////.out.println("valor debito_"+vlrDebito);
                    }else{
                        vlrDebito  +=  comprodetn.getTotal_debito();
                    }
                    //END MOD PRONTO PAGO 27-abril-2009
                    //vlrDebito  +=  comprodetn.getTotal_debito();
                    listaDetalle.add(comprodetn);
                    sw=0;
                }
                List items = dao.getItems( comprobante );
                for(int j=0;j<items.size();j++){
                    ComprobanteFacturas  comprodet = (ComprobanteFacturas)items.get(j);
                    //falta preguntarrrrr para que sirva para el otro
                    //hago un si donde pregunto por el hc y coloco los items corespondientes, se debe tener en cuenta la naturaleza de la cuenta en caso de ser debito o credito para asignarle los valores
                    if (sw == 0) {
                        double vlrCreditoOriginal = comprodet.getTotal_credito();//090404

                        comprodet.setPeriodo(comprobante.getPeriodo());
                        comprodet.setBase("COL");
                        comprodet.setTotal_credito(comprodet.getTotal_debito());
                        vlrCredito += comprodet.getTotal_debito();

                        comprodet.setTotal_debito(vlrCreditoOriginal);//0   //090404
                        //vlrDebito +=  comprodet.getTotal_debito();//090404
                        //comprodet.setTotal_debito(0);//090404

                        //MOD PRONTO PAGO 27-abril-2009
                        if ((comprodet.getCmc().equals("EX")) && (comprodet.getVlrME() < 0)) {
                            comprodet.setTotal_debito(-comprodet.getVlrME());
                            vlrCredito += comprodet.getTotal_debito();
                        } else {
                            vlrDebito += comprodet.getTotal_debito();//090404 //27 abril
                        }
                        //END MOD PRONTO PAGO 27-abril-2009

                        listaDetalle.add(comprodet);
                    } else {
                        comprodet = copy(comprobante, comprodet);
                        if (comprodet.getTotal_debito() != 0 || comprodet.getTotal_credito() != 0) {
                            vlrDebito += comprodet.getTotal_debito();
                            vlrCredito += comprodet.getTotal_credito();
                        }
                        listaDetalle.add(comprodet);
                    }
                }// For Items

              // ----------------------------------------------------------------------------------------------------
              // SALDO
              // ----------------------------------------------------------------------------------------------------
                //vlrCredito=Util.redondear2(vlrCredito, 4);//090404
                vlrCredito=Util.redondear2(vlrCredito, 2);//090406
                vlrDebito=Util.redondear2(vlrDebito, 2);//090406
                double  saldoComprobante         =  vlrDebito - vlrCredito;

                if(  saldoComprobante != 0   ){

                    double  vlrSaldoDebito           =  ( saldoComprobante > 0)?  0  :  saldoComprobante * -1 ;
                    double  vlrSaldoCredito          =  ( saldoComprobante < 0)?  0  :  saldoComprobante      ;
                    ComprobanteFacturas detalleSaldo = this.detalleSaldo(comprobante);
                    //if (  (comprobante.getTipodoc()).equals("FAC")  )
                    if (  (comprobante.getTipodoc()).equals("FAC")  ||  (comprobante.getTipodoc()).equals("NDC"))
                    {
                        detalleSaldo = this.detalleSaldo(comprobante);
                    }
                    /*
                    if (  (comprobante.getTipodoc()).equals("ND")  )
                    {

                        String[] bco= new String[2];
                        String temp="";
                        temp=(daoCL.getBco(comprobante.getNumdoc()));
                        if (!(temp!=null && !(temp.equals("")) )){//si no se encontro una cuenta debe ser que la nd esta asociada a una nota de ajuste
                            //por lo que se obtendra la cuenta de la nota de ajuste
                            temp=(daoCL.getBcoNotaAjuste(comprobante.getNumdoc()));//
                            ////.out.println("temp"+temp);

                        }
                        bco=temp.split("#");
                        detalleSaldo = this.detalleSaldo(comprobante);
                        detalleSaldo.setTercero("");
                        detalleSaldo.setTdoc_rel("" );
                        detalleSaldo.setNumdoc_rel("");
                        detalleSaldo.setAuxiliar("");
                        detalleSaldo.setCuenta(bco[1]);//cuanta Banco
                        detalleSaldo.setDetalle(bco[0]);//nombre banco

                    }
                    */
                    detalleSaldo.setTotal_credito( vlrSaldoCredito );
                    detalleSaldo.setTotal_debito ( vlrSaldoDebito  );
                    listaDetalle.add( detalleSaldo );
                    vlrCredito += vlrSaldoCredito;
                    vlrDebito  += vlrSaldoDebito;
                }



                // ACTUALIZAMOS DATOS COMPROBANTES
                comprobante.setTotal_credito( vlrCredito   );
                comprobante.setTotal_debito ( vlrDebito    );
                comprobante.setItems        ( listaDetalle );
                comprobante.setTotal_items  ( listaDetalle.size()   );



                // INSERTAMOS EL COMPROBANTE
                List ins = new LinkedList();
                     ins.add( comprobante );

                insertar( ins, usuario.getLogin() );


            } // For facturas



            ESTADO  =  "Comprobantes generados : " + ( listaContable.size() - CANT_ERROES  ) + ", Comprobantes con Errores : "+ CANT_ERROES;
            if( CANT_ERROES >0  ){
                 LogErrores( listaErrores , usuario );
            }



        }catch(Exception e){
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }

    }





    /**
     * M�todo que copia valores
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public ComprobanteFacturas copy(ComprobanteFacturas items, ComprobanteFacturas imp)throws Exception{
        try{

            imp.setDstrct           (  items.getDstrct()            );
            imp.setTipodoc          (  items.getTipodoc()           );
            imp.setNumdoc           (  items.getNumdoc()            );
            imp.setCuenta           (  items.getCuenta()            );
            imp.setTercero          (  items.getTercero()           );
            imp.setTipodoc_factura  (  items.getTipodoc_factura()   );
            imp.setDocumento_interno(  items.getDocumento_interno() );
            imp.setPeriodo          (  items.getPeriodo()           );
            imp.setFechadoc         (  items.getFechadoc()          );
            imp.setBase             (  items.getBase()              );
            imp.setTipo             (  "D"              );
            imp.setClaseFactura     (  items.getClaseFactura()      );
            imp.setAbc              (  items.getAbc()               );
            imp.setDetalle          (  items.getRef_3()             );
            //imp.setTdoc_rel(items.getTdoc_rel());
            //imp.setNumdoc_rel(items.getNumdoc_rel());
            imp.setTipo_referencia_1(items.getTipo_referencia_1());
            imp.setReferencia_1(items.getReferencia_1());
            imp.setTipo_referencia_2(items.getTipo_referencia_2());
            imp.setReferencia_2(items.getReferencia_2());
            imp.setTipo_referencia_3(items.getTipo_referencia_3());
            imp.setReferencia_3(items.getReferencia_3());
        }catch(Exception e){
            throw new Exception( e.getMessage() );
        }
        return imp;
    }




    /**
     * M�todo que copia valores del comprobante para generar el detalle del saldo
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public ComprobanteFacturas detalleSaldo(ComprobanteFacturas comprobante)throws Exception{
        ComprobanteFacturas  detSaldo = new  ComprobanteFacturas();
        try{

            detSaldo.setDstrct           (  comprobante.getDstrct()             );
            detSaldo.setTipodoc          (  comprobante.getTipodoc()            );
            detSaldo.setNumdoc           (  comprobante.getNumdoc()             );
            detSaldo.setPeriodo          (  comprobante.getPeriodo()            );
            detSaldo.setCuenta           (  (comprobante.getCuenta().equals("13109902"))?"13109901":"13109601");

            if (comprobante.getNumdoc().substring(0,2).equals("PC") || comprobante.getNumdoc().substring(0,2).equals("PL")){
                detSaldo.setCuenta           (  "16252001"             );
            }
            if(comprobante.getCuenta().equals("16252002")){
                detSaldo.setCuenta("16252001");
            }
            /*
            if ( comprobante.getNumdoc().substring(0,2).equals("MC") || comprobante.getNumdoc().substring(0,2).equals("ND") ){
                detSaldo.setCuenta ( comprobante.getCuentaContable() );
            }*/
            
            if ( comprobante.getNumdoc().substring(0,2).equals("MC") || comprobante.getNumdoc().substring(0,2).equals("ND") || 
                    comprobante.getNumdoc().substring(0,2).equals("AP") || comprobante.getNumdoc().substring(0,2).equals("AC")
                    || comprobante.getNumdoc().substring(0,2).equals("CK") ){
                detSaldo.setCuenta ( comprobante.getCuentaContable() );
            }            
            
            /*
             if(comprobante.getCuenta().equals("13109602")){
                detSaldo.setCuenta("13109601");
            }
             */

            detSaldo.setAuxiliar         (  "RD-"+ comprobante.getTercero()     );
            detSaldo.setDetalle          (  comprobante.getRef_3()               );
            detSaldo.setTercero          (  comprobante.getTercero()            );
            detSaldo.setBase             (  comprobante.getBase()               );
            detSaldo.setDocumento_interno(  comprobante.getDocumento_interno()  );
            detSaldo.setTipo             (  "D"                          );
            detSaldo.setClaseFactura     (  comprobante.getClaseFactura()       );
            detSaldo.setTipodoc_factura  (  comprobante.getTipodoc_factura()    );
            detSaldo.setTdoc_rel         (  comprobante.getTipodoc()         );
            detSaldo.setNumdoc_rel       (  comprobante.getNumdoc()        );
            detSaldo.setAbc              ("N/A");
            detSaldo.setTipo_referencia_1("");
            detSaldo.setReferencia_1("");
            detSaldo.setTipo_referencia_2("");
            detSaldo.setReferencia_2("");
            detSaldo.setTipo_referencia_3("");
            detSaldo.setReferencia_3("");

        }catch(Exception e){
            throw new Exception( e.getMessage() );
        }
        return detSaldo;
    }




    
    
    
    /**
     * M�todo que inserta el comprobante y comprodet
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     * @modificado darrieta 04/01/2011
     **/
    public  void insertar(List lista, String user)throws Exception{
        try{
            
            TransaccionService  scv = new TransaccionService(this.dao.getDatabaseName());

            for(int i=0;i<lista.size();i++){
                ComprobanteFacturas  comprobante = (ComprobanteFacturas) lista.get(i);
                
                ArrayList<String> listaSql = this.generarSQLComprobante(comprobante, user);
                
                 // Ejecutamos el SQL
                if (!listaSql.equals("") && listaSql!= null) {
                    try {
                        scv.crearStatement();
                       for (String sql : listaSql) {
                            scv.getSt().addBatch(sql);
                        }
                        scv.execute();

                    } catch (Exception e) {
                        dao.deleteComprobante(comprobante);
                        comprobante.setComentario("Error al momento de insertar : " + e.getMessage());
                        CANT_ERROES++;
                        listaErrores.add(comprobante);
                    }
                }
            }
            
        }catch(Exception e){
            e.printStackTrace();
            throw new Exception( " INSERTAR COMPROBANTE FACTURAS : "+ e.getMessage() );            
        }
    }




  
    public  void insertarEG(List lista, String user)throws Exception{
        try{

            TransaccionService  scv = new TransaccionService(this.dao.getDatabaseName());
            ////.out.println("Tama�o 1 "+lista.size());
            for(int i=0;i<lista.size();i++){
                Comprobantes  comprobante = (Comprobantes) lista.get(i);

                //inicio de 090917
                comprobante.setTotal_credito(Util.redondear2(comprobante.getTotal_credito(),2));
                comprobante.setTotal_debito(Util.redondear2(comprobante.getTotal_debito(),2));
                //fin de 090917

                String eva =  evaluarComprobante( comprobante );
                ////.out.println("evaluacion de comprobante "+eva+" ");
                System.out.println("eva::"+eva);
                //.log("evaluacion de comprobante:"+eva,LogWriter.INFO);
                if( eva.equals("") ){
                    List items = comprobante.getItems();
                    List aux   = new LinkedList();
                    int  sw    = 0;
                    ////.out.println("Tama�o "+items.size());
                 // Validamos los items:
                    if( items!= null ){
                        for(int j=0;j<items.size();j++){
                            Comprobantes  comprodet = (Comprobantes)items.get(j);
                            String evaDet = evaluarComprobante(comprodet);
                            //.out.println("evaDet::"+evaDet);
                            if( evaDet.equals("") )
                                aux.add( comprodet );
                            else{
                                System.out.println("Uno de sus item presenta irregularidad_"+comprobante.getNumdoc()+"evaDet"+evaDet);
                                comprobante.setComentario( "Uno de sus item presenta irregularidad" );
                                listaErrores.add( comprobante );     // Guardamos la factura
                                CANT_ERROES ++;

                                comprodet.setComentario( evaDet );   // Guardamos el item
                                listaErrores.add( comprodet );

                                sw = 1;
                                break;
                            }
                        }
                    }else{
                        System.out.println("items es nuill");
                    }
                    //.out.println("sww"+sw);
                    if ( sw == 0  ){

                        items = aux;  // Items q cumplen para ser insertados
                        //.out.println("items.size()"+items.size());
                        if( items!= null  && items.size()>0){

                            // Tomamos la secuencia:
                            int sec    = comproDAO.getGrupoTransaccion();


                            // Realizamos los insert, update:
                            comprobante.setGrupo_transaccion( sec );
                            ArrayList<String> ListSql=new ArrayList<>();
                                   ListSql.add(dao.InsertComprobante(comprobante, user ));
                                   ListSql.addAll(dao.InsertComprobanteDetalle2(items, sec, user ));
                                   ////.out.println("Salio de Insertar 45 "+sql);
                            if (!comprobante.getOrigen().equals("COMPROBANTE")   ){

                                if ( comprobante.getOrigen().equals("P")   ){

                                    comproDAO.setComprobante(comprobante);
                                    ListSql.addAll(comproDAO.marcarEgreso());                // Egresos
                                }

                            }


                            // Ejecutamos el SQL
          
                           if(!ListSql.isEmpty()){
                                try{
                                    scv.crearStatement();
                                    for(String sql : ListSql) {
                                          scv.getSt().addBatch(sql);
                                        }
                                 
                                    scv.execute();

                                }catch(Exception e){
                                    //dao.deleteComprobante( comprobante );
                                    comprobante.setComentario("Error al momento de insertar : " + e.getMessage() );
                                    CANT_ERROES++;
                                    listaErrores.add( comprobante );
                                }
                            }
                           ListSql=null;

                        }else{
                            System.out.println("Uno de sus item presenta irregularidad_"+comprobante.getNumdoc());
                            comprobante.setComentario("No presenta items validos");
                            CANT_ERROES++;
                            listaErrores.add(  comprobante  );
                        }


                   }


                }else{
                    System.out.println("ocurrio error en insertarEG"+comprobante.getNumdoc());
                    comprobante.setComentario( eva );
                    listaErrores.add(  comprobante  );
                    CANT_ERROES ++;
                }

            }
          ////.out.println("Salio de Insertar");
        }catch(Exception e){
            System.out.println("Error de Insertar");
            e.printStackTrace();
            throw new Exception( " INSERTAR COMPROBANTE FACTURAS : "+ e.getMessage() );

        }
    }








    /**
     * M�todo que evalua datos v�lidos del comprobante y comprodet
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public String evaluarComprobante(Comprobantes  comprobante)throws Exception{
        String msj =  "";
        try{
            ////.out.println("Evalua");
            // Campos comunes:
            if ( comprobante.getDstrct().       trim().equals(""))    msj += " No presenta distrito";
            if ( comprobante.getTipodoc().      trim().equals(""))    msj += " No presenta tipo de documento";
            if ( comprobante.getNumdoc().       trim().equals(""))    msj += " No presenta n�mero de documento";
            if ( comprobante.getPeriodo().      trim().equals(""))    msj += " No presenta periodo de contabilizaci�n";
            if ( comprobante.getDetalle().      trim().equals(""))    msj += " No presenta una descripci�n o detalle";
            if ( comprobante.getBase().         trim().equals(""))    msj += " No presenta base";


            String tipo = comprobante.getTipo();

            if ( tipo.equals(  dao.CABECERA )  ){
                System.out.println("comprobante.getTotal_debito()"+comprobante.getTotal_debito()+"comprobante.getTotal_credito() "+comprobante.getTotal_credito() );
                if ( comprobante.getTotal_debito() != comprobante.getTotal_credito() )  msj += " Est� descuadrado en valores debitos y cr�ditos";
                if ( comprobante.getSucursal().     trim().equals(""))                  msj += " No presenta sucursal";
                if ( comprobante.getFechadoc().     trim().equals(""))                  msj += " No presenta fecha de documento";


                if ( comprobante.getMoneda().       trim().equals(""))        msj += " No presenta moneda";
                if ( comprobante.getAprobador().    trim().equals(""))        msj += " No presenta aprobador";

            }
            else{

                if ( comprobante.getCuenta().       trim().equals(""))    msj += " No presenta cuenta";
                else{
                    String cta  = comprobante.getCuenta();
                    Hashtable  cuenta =  movDAO.getCuenta(comprobante.getDstrct(), cta  );

                    if ( cuenta != null){

                        String  requiereAux      = (String) cuenta.get("auxiliar");
                        String  requiereTercero  = (String) cuenta.get("tercero");

                        // Tercero.
                        if ( requiereTercero .equals("S")   &&  comprobante.getTercero().  trim().equals(""))
                            msj += " La cuenta "+ cta +" requiere tercero  y el item no presenta tercero";

                        // Auxiliar.
                        if ( requiereAux.equals("S") ){

                             if(  comprobante.getAuxiliar(). trim().equals("")  )
                                   msj += " La cuenta "+ cta +" requiere auxiliar y el item no presenta auxiliar";

                             if( !dao.existeCuentaSubledger( comprobante.getDstrct() , cta , comprobante.getAuxiliar()  )   )
                                   msj += "La cuenta " + cta + " no tiene el subledger "+  comprobante.getAuxiliar()  +" asociado";
                        }




                    }else
                        msj += " La cuenta "+ cta +" no existe o no est� activa";

                }

            }


           ////.out.println("Salio de Evaluar "+msj);
        }catch(Exception e){
            System.out.println("error en evaluarComprobante"+e.toString());
            e.printStackTrace();
            throw new Exception( e.getMessage() );
        }
        return msj;
    }


    public String evaluarComprobante(ComprobanteFacturas  comprobante)throws Exception{
        String msj =  comprobante.getComentario();
        ////.out.println("EValuar");
        try{

            // Campos comunes:
            if ( comprobante.getDstrct().       trim().equals(""))    msj += " No presenta distrito";
            if ( comprobante.getTipodoc().      trim().equals(""))    msj += " No presenta tipo de documento";
            if ( comprobante.getNumdoc().       trim().equals(""))    msj += " No presenta n�mero de documento";
            if ( comprobante.getPeriodo().      trim().equals(""))    msj += " No presenta periodo de contabilizaci�n";
            if ( comprobante.getDetalle().      trim().equals(""))    msj += " No presenta una descripci�n o detalle";
            if ( comprobante.getBase().         trim().equals(""))    msj += " No presenta base";


            String tipo = comprobante.getTipo();

            if ( tipo.equals(  dao.CABECERA )  ){

                if ( comprobante.getTotal_debito() != comprobante.getTotal_credito() )  msj += " Est� descuadrado en valores debitos y cr�ditos";
                if ( comprobante.getSucursal().     trim().equals(""))                  msj += " No presenta sucursal";
                if ( comprobante.getFechadoc().     trim().equals(""))                  msj += " No presenta fecha de documento";

                if ( comprobante.getDocumento_interno().equals( TIPO_FACTURA )  )
                    if ( comprobante.getTercero().      trim().equals(""))    msj += " No presenta tercero";

                if ( comprobante.getMoneda().       trim().equals(""))        msj += " No presenta moneda";
                if ( comprobante.getAprobador().    trim().equals(""))        msj += " No presenta aprobador";

            }
            else{

                if ( comprobante.getCuenta().       trim().equals(""))    msj += " No presenta cuenta";
                else{
                    String cta  = comprobante.getCuenta();
                    Hashtable  cuenta =  movDAO.getCuenta(comprobante.getDstrct(), cta  );

                    if ( cuenta != null){

                        String  requiereAux      = (String) cuenta.get("auxiliar");
                        String  requiereTercero  = (String) cuenta.get("tercero");

                        // Tercero.
                        if ( requiereTercero .equals("S")   &&  comprobante.getTercero().  trim().equals(""))
                            msj += " La cuenta "+ cta +" requiere tercero  y el item no presenta tercero";

                        // Auxiliar.
                        if ( requiereAux.equals("S") ){

                             if(  comprobante.getAuxiliar(). trim().equals("")  )
                                   msj += " La cuenta "+ cta +" requiere auxiliar y el item no presenta auxiliar";

                             if( !dao.existeCuentaSubledger( comprobante.getDstrct() , cta , comprobante.getAuxiliar()  )   )
                                   msj += "La cuenta " + cta + " no tiene el subledger "+  comprobante.getAuxiliar()  +" asociado";
                        }




                    }else
                        msj += " La cuenta "+ cta +" no existe o no est� activa";

                }

            }

            ////.out.println("Men "+msj);

        }catch(Exception e){
            System.out.println("error en contabilizacion:"+e.toString()+"__"+e.getMessage()+"__"+comprobante.getNumdoc()+"_");
            e.printStackTrace();
            throw new Exception( e.getMessage() );
        }
        return msj;
    }

    /**
     * M�todo que genera el Log de Errores
     * @autor.......fvillacob
     * @throws......Exception
     * @version..1
     **/
    public void LogErrores(List lista, Usuario user)throws Exception{
        try{

            // Ruta del archivo:
            String       fecha = Util.getFechaActual_String(6).replaceAll("/|:| ","");

            DirectorioService  svc   =  new DirectorioService();
            svc.create( user.getLogin() );
            String             url   =  svc.getUrl() + user.getLogin() + "/"+user.getBd()+"_CONTAFACT" +  fecha +".txt";


            PrintWriter pw = new PrintWriter( new BufferedWriter( new FileWriter(url) )  );
            LogWriter logM = new LogWriter("Contabilizaci�n de Facturas",LogWriter.INFO, pw);

            try{
                logM.log("Inicio de la Contabilizaci�n....",logM.INFO);

                for(int j=0;j<lista.size();j++){
                    ComprobanteFacturas  comprodet = (ComprobanteFacturas)lista.get(j);
                    String msj  =  comprodet.getNumdoc()   +" "+ comprodet.getDetalle()  +" CAUSA : "+ comprodet.getComentario();
                    logM.log(msj,logM.INFO);
                }

                logM.log("Finalizaci�n...",logM.INFO);

            }catch (Exception e){
                logM.log("\n\nError en Contabilizaci�n Facturas: "+e.getMessage(),logM.ERROR);
            }
            finally{
                pw.close();
            }

        }catch(Exception e){
            e.printStackTrace();
            throw new Exception( e.getMessage() );
        }
    }


    // SET AND GET : -------------------------------------------------------------------------


    /**
     * Getter for property listaContable.
     * @return Value of property listaContable.
     */
    public java.util.List getListaContable() {
        return listaContable;
    }

    /**
     * Setter for property listaContable.
     * @param listaContable New value of property listaContable.
     */
    public void setListaContable(java.util.List listaContable) {
        this.listaContable = listaContable;
    }





    /**
     * Getter for property process.
     * @return Value of property process.
     */
    public boolean isProcess() {
        return process;
    }

    /**
     * Setter for property process.
     * @param process New value of property process.
     */
    public void setProcess(boolean process) {
        this.process = process;
    }





    /**
     * M�todo que descontabilizar
     * @autor.......jescandon
     * @throws......Exception
     * @version.....1.0.
     **/
    public String Descontabilizar(Vector VComprobantes, String user)throws Exception{
        String msjError    = "";

        List listaErrores  = new LinkedList();
        List listaGrabada  = new LinkedList();


        try{

            ArrayList<String> sql         = new ArrayList<String>();
            Iterator it        = VComprobantes.iterator();
            Vector   VComprobantesDet;

            while( it.hasNext() ){
                Comprobantes aux = (Comprobantes)it.next();
                VComprobantesDet = dao.VectorComprobantes(aux);

                Iterator it2 = VComprobantesDet.iterator();

                while( it2.hasNext()){
                    Comprobantes auxdet = (Comprobantes)it2.next();
                    sql.add( dao.DescontabilizarMayor(auxdet) );
                    sql.add( dao.UpdateFechaAplicacion(auxdet) );
                    if( auxdet.getAuxiliar().length() > 0 ){
                        sql.add( dao.DescontabilizarMayoSubledger(auxdet) );
                    }

                }//fin ComprobantesDet


                // Ejecutamos el SQL
                if(!sql.equals("")){



                    try{
                        TransaccionService  scv = new TransaccionService(this.dao.getDatabaseName());
                        scv.crearStatement();
                        for (int j = 0; j < sql.size(); j++) {
                           scv.getSt().addBatch(sql.get(j));
                        }
                        scv.execute();

                        listaGrabada.add(aux);


                    }catch(Exception e){
                        aux.setComentario( e.getMessage() );
                        listaErrores.add(aux);
                        msjError += " No se pudo descontabilizar el comprobante -> " + aux.getNumdoc() + " ";
                    }

                }

            }//fin Comprobantes



            if( listaErrores.size()>0  ){
                LogErroresDescontabilizar( listaErrores, user  );
                msjError += " Comprobantes grabados "+  listaGrabada.size() +". Hay comprobantes que presentar�n errores, no cumple con alguna condici�n. Puede consultar el log de errores";
            }


        }catch(Exception e){
            throw new Exception( " Descontabilizar : "+ e.getMessage() );
        }

        return msjError;

    }



    public void LogErroresDescontabilizar(List lista, String user)throws Exception{
        try{
            ResourceBundle rb  = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String       ruta  = rb.getString("ruta");
            String       fecha = Util.getFechaActual_String(6).replaceAll("/|:| ","");
            String       url   = ruta + "/exportar/migracion/" + user + "/" + "Descontabilizacion" + user +  fecha +".log";

            PrintWriter pw = new PrintWriter( new BufferedWriter( new FileWriter(url) )  );
            LogWriter logM = new LogWriter("Descontabilizacion",LogWriter.INFO, pw);

            try{
                logM.log("Inicio de la Descontabilizacion....",logM.INFO);

                for(int j=0;j<lista.size();j++){
                    Comprobantes  comprodet = (Comprobantes)lista.get(j);
                    String msj  =  comprodet.getNumdoc()   + " " + " CAUSA : "+ comprodet.getComentario();
                    logM.log(msj,logM.INFO);
                }

                logM.log("Finalizaci�n...",logM.INFO);

            }catch (Exception e){
                logM.log("\n\nError en Contabilizaci�n Facturas: "+e.getMessage(),logM.ERROR);
            }
            finally{
                pw.close();
            }

        }catch(Exception e){
            throw new Exception( e.getMessage() );
        }
    }

     /**
     * M�todo de reversion de comprobantes
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public  String Reversar(Vector lista, String user)throws Exception{
        String msjError    = "";
        try{

            ArrayList<String> sql  =new ArrayList<>() ;
            List listaGrabada  = new LinkedList();//Lista de grabacion
            List listaErrores  = new LinkedList();//Lista de errores;

            for(int i=0;i<lista.size();i++){

                Comprobantes  comprobante         = (Comprobantes) lista.get(i);
                ComprobanteFacturas  aux          = this.dao.getComprobantes(comprobante);

                int sec    = comproDAO.getGrupoTransaccion();
                aux.setGrupo_transaccion( sec );

                sql.add(this.dao.InsertComprobante(aux, user ));



                Comprobantes comp = new Comprobantes();
                comp.setTipodoc(aux.getTipodoc());
                comp.setNumdoc(aux.getNumdoc());
                comp.setGrupo_transaccion(comprobante.getGrupo_transaccion());

                List items = this.dao.ListaComprobantes(comp);
                ArrayList<String> InsertComprobanteDetalle = dao.InsertComprobanteDetalle(items, sec, user );
                for (String InsertComprobanteDetalle1 : InsertComprobanteDetalle) {
                    sql.add(InsertComprobanteDetalle1);
                }
                
                //esto es para concatenar los SQL 
                String getConcatenaQuerys = "";
                for (int j = 0; j < sql.size(); j++) {
                    getConcatenaQuerys += sql.get(j);
                }
                aux.setComentario(getConcatenaQuerys);
                listaGrabada.add( aux );
            }

            // Ejecutamos el SQL
            if(!sql.isEmpty()){

                try{
                    TransaccionService  scv = new TransaccionService(this.dao.getDatabaseName());
                    scv.crearStatement();
                    for (int j = 0; j < sql.size(); j++) {
                         scv.getSt().addBatch(sql.get(j));
                    }
                    scv.execute();

                }catch(Exception e){
                    listaGrabada  = this.dao.deleteComprobanteSQL( listaGrabada );
                    listaErrores.addAll( listaGrabada );
                    listaGrabada = new LinkedList();
                    msjError += "No se pudieron grabar algunos comprobantes hay un error en el batch->";
                    e.printStackTrace();
                }

            }

            if( listaErrores.size()>0  ){
                LogErroresReversar( listaErrores, user  );
                msjError += " Comprobantes grabados "+  listaGrabada.size() +". Hay comprobantes que presentar�n errores, no cumple con alguna condici�n o el query presenta algun error. Puede consultar el log de errores";
            }


        }catch(Exception e){
            throw new Exception( " Reversar : "+ e.getMessage() );
        }

        return msjError;

    }



    public void LogErroresReversar(List lista, String user)throws Exception{
        try{
            ResourceBundle rb  = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String       ruta  = rb.getString("ruta");
            String       fecha = Util.getFechaActual_String(6).replaceAll("/|:| ","");
            String       url   = ruta + "/exportar/migracion/" + user + "/" + "Descontabilizacion" + user +  fecha +".log";

            PrintWriter pw = new PrintWriter( new BufferedWriter( new FileWriter(url) )  );
            LogWriter logM = new LogWriter("Reversion",LogWriter.INFO, pw);

            try{
                logM.log("Inicio de la Reversion....",logM.INFO);

                for(int j=0;j<lista.size();j++){
                    ComprobanteFacturas  comprodet = (ComprobanteFacturas)lista.get(j);
                    String msj  =  comprodet.getNumdoc()   + " " + "Error en el Query : " + comprodet.getComentario();
                    logM.log(msj,logM.INFO);
                }

                logM.log("Finalizaci�n...",logM.INFO);

            }catch (Exception e){
                logM.log("\n\nError en Reversion Facturas: "+e.getMessage(),logM.ERROR);
            }
            finally{
                pw.close();
            }

        }catch(Exception e){
            throw new Exception( e.getMessage() );
        }
    }

    /**
     * Getter for property ESTADO.
     * @return Value of property ESTADO.
     */
    public java.lang.String getESTADO() {
        return ESTADO;
    }

    /**
     * Setter for property ESTADO.
     * @param ESTADO New value of property ESTADO.
     */
    public void setESTADO(java.lang.String ESTADO) {
        this.ESTADO = ESTADO;
    }

/**
     * genera el query para insertar el comprobante y comprodet
     * @param comprobante
     * @param user
     * @return SQL generado
     * @throws Exception 
     */
    public ArrayList<String> generarSQLComprobante(ComprobanteFacturas comprobante, String user) throws Exception {
        ArrayList<String> listSql = new ArrayList<String>();
        try {
                String eva = evaluarComprobante(comprobante);
                if (eva.equals("")) {
                    List items = comprobante.getItems();
                    List aux = new LinkedList();
                    int sw = 0;
                    // Validamos los items:
                    if (items != null) {
                        for (int j = 0; j < items.size(); j++) {
                            ComprobanteFacturas comprodet = (ComprobanteFacturas) items.get(j);
                            String evaDet = evaluarComprobante(comprodet);
                            if (evaDet.equals("")) {
                                aux.add(comprodet);
                            } else {
                                comprobante.setComentario("Uno de sus item presenta irregularidad");
                                listaErrores.add(comprobante);     // Guardamos la factura
                                CANT_ERROES++;

                                comprodet.setComentario(evaDet);   // Guardamos el item
                                listaErrores.add(comprodet);

                                sw = 1;
                                break;
                            }
                        }
                    }

                    if (sw == 0) {
                        items = aux;  // Items q cumplen para ser insertados

                        if (items != null && items.size() > 0) {
                            // Tomamos la secuencia:
                            int sec = comproDAO.getGrupoTransaccion();

                            // Realizamos los insert, update:
                            comprobante.setGrupo_transaccion(sec);

                            listSql.add(dao.InsertComprobante(comprobante, user));
                            listSql.addAll(dao.InsertComprobanteDetalle(items, sec, user));
                            if (!comprobante.getOrigen().equals("COMPROBANTE")) {

                                if (comprobante.getOrigen().equals("P")) {
                                    comproDAO.setComprobante(comprobante);
                                    listSql.addAll(comproDAO.marcarEgreso());                // Egresos
                                }
                                if (comprobante.getOrigen().equals("C")) {
                                    ContabilizacionFacturasCLDAO daoCL = new ContabilizacionFacturasCLDAO(this.dao.getDatabaseName());
                                    listSql.add(daoCL.updateFacturaClientes(comprobante, user));     // CxP       (Proveedor)
                                }
                                if (comprobante.getOrigen().equals("A")) {
                                    ContabilizacionFacturasCLDAO daoCL = new ContabilizacionFacturasCLDAO(this.dao.getDatabaseName());
                                    listSql.add(dao.updateFactura(comprobante, user));    // CXC       (Proveedor)
                                }
                            }

                            if (comprobante.isOP() || comprobante.isAdicion()) {
                                listSql.add(dao.updateMovplaTipoQ(comprobante, user));
                            }
                            

                        } else {
                            comprobante.setComentario("No presenta items validos");
                            CANT_ERROES++;
                            listaErrores.add(comprobante);
                        }
                    }
                } else {
                    comprobante.setComentario(eva);
                    listaErrores.add(comprobante);
                    CANT_ERROES++;
                }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(" INSERTAR COMPROBANTE FACTURAS : " + e.getMessage());
        }
        return listSql;
    }



}
