/**
 * *********************************
 * Nombre Clase ............. Contabilizacion Negocios Services.java Descripci�n
 * .. . . . . . Permitimos realizar los servicios para la contabilizacion de
 * negocios Autor . . . . . . . . . . ING ROBERTO ROCHA P Fecha . . . . . . . .
 * . . . 06/09/2006 versi�n . . . . . . . . . 1.0 Copyright ...............
 * FINTRAVALORES S.A.
 ***********************************
 */
package com.tsp.finanzas.contab.model.services;

import java.io.*;
import java.util.*;
import com.tsp.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.util.LogWriter;
import com.tsp.finanzas.contab.model.beans.*;
import com.tsp.finanzas.contab.model.DAO.ContabilizacionNegociosDAO;
import com.tsp.finanzas.contab.model.DAO.ContabilizacionFacturasCLDAO;
import com.tsp.finanzas.contab.model.DAO.ComprobantesDAO;
import com.tsp.finanzas.contab.model.DAO.MovAuxiliarDAO;
import com.tsp.operation.model.TransaccionService;
import com.tsp.operation.model.services.DirectorioService;
import com.tsp.operation.model.services.TasaService;
import com.tsp.operation.model.beans.Tasa;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.GestionConveniosDAO;
import com.tsp.operation.model.DAOS.Tipo_impuestoDAO;
import com.tsp.operation.model.DAOS.GestionSolicitudAvalDAO;
import com.tsp.operation.model.services.NegociosGenService;
import com.tsp.operation.model.DAOS.GestionSolicitudAvalDAO;

public class ContabilizacionNegociosServices {

    private ContabilizacionNegociosDAO dao;
    private ComprobantesDAO comproDAO;
    private MovAuxiliarDAO movDAO;
    private List listaContable;
    private boolean process;
    private String IVA = "IVA";
    private String TIPO_FACTURA = "010";
    private String TIPO_FACTURA_AMD = "FAA";
    private String DIF_CAMBIO_OP = "99";
    private LogWriter logWriter;
    public List listaErrores;
    public String ESTADO = "";
    public int CANT_ERROES = 0;
    public List listitems;
    private int items;//los items del comprobante;
    private double totdeb;
    private double totcred;
    private double interes;
    private double ingresos;
    private double custodia;
    private double remesa;
    private double comisionesContra;
    private double comisionTercero;
    private final String TIPO_OC = "001";
    private final String FACTURA = "010";
    private final String NC = "035";
    private final String ND = "036";
    private double aval;
    private double cuota_admin;
    private double seguro_deudor;
    private  NegociosGenService negserv;  

    public ContabilizacionNegociosServices() {
        dao = new ContabilizacionNegociosDAO();
        comproDAO = new ComprobantesDAO();
        movDAO = new MovAuxiliarDAO();
        listaContable = new LinkedList();
        process = false;
        listitems = new LinkedList();
        listaErrores = new LinkedList();
        ESTADO = "";
        CANT_ERROES = 0;
        negserv = new  NegociosGenService();
    }
    public ContabilizacionNegociosServices(String dataBaseName) {
        dao = new ContabilizacionNegociosDAO(dataBaseName);
        comproDAO = new ComprobantesDAO(dataBaseName);
        movDAO = new MovAuxiliarDAO(dataBaseName);
        listaContable = new LinkedList();
        process = false;
        listitems = new LinkedList();
        listaErrores = new LinkedList();
        ESTADO = "";
        CANT_ERROES = 0;
        negserv = new  NegociosGenService(dataBaseName);
    }

    public void reset() {
        listaContable = new LinkedList();
    }

    /**
     * M?todo que carga los negocios que no esten contabilizados
     * @autor.......rarp
     * @throws......Exception
     * @version.....1.0.
     * @modificado darrieta 11/10/2010 - ivargas 04/11/2011
     **/
   
    public void contabilizar(String distrito, String ayer, String usuario, Usuario uSesion) throws Exception {
        try {
            String periodo = ayer.substring(0, 7).replaceAll("-", "");
            String hoy = Utility.getHoy("-");
            listaErrores = new LinkedList();
            ESTADO = "";
            CANT_ERROES = 0;
            listaContable = dao.getNegociosNoContabilizados(hoy);
            // COMPROBANTES : NEGOCIOS
            for (int i = 0; i < listaContable.size(); i++) {
                Comprobantes comprobante = (Comprobantes) listaContable.get(i);
                items = 0;
                ingresos = 0;
                comisionesContra = 0;
                cuota_admin=0;
                seguro_deudor=0;
                listitems = new LinkedList();

                GestionConveniosDAO convenioDao = new GestionConveniosDAO(this.dao.getDatabaseName());
                Convenio convenio = convenioDao.buscar_convenio(uSesion.getBd(), String.valueOf(comprobante.getId_convenio()));
                if (!convenio.getTipo().equals("Multiservicio")) {
                ConvenioCxc tituloValor = convenio.buscarTituloValor(comprobante.getTipoNegocio());

                comprobante.setFechadoc(ayer);//fecha del comprobante
                comprobante.setPeriodo(periodo);
                comprobante.setTipo_operacion("001");
                comprobante.setUsuario(usuario);
                comprobante.setAprobador(usuario);
                comprobante.setTipo("C");

                comprobante.setCuenta(tituloValor.getCuenta_cxc()); //Cuenta del titulo valor


                if (convenio.getTipo().equals("Microcredito")) {
                    
                    //validamos que sea un negocio de renovacion.
                    boolean renovacion = false;//dao.getRenovacion(comprobante.getNumdoc());
                    //seteamos cuentas 
                    String cuentaInt="";
                    String cuentaCat="";
                    String cuentaCA="";
                    String cuentaSegDeudor="";
                    //Configuracion de cuentas
                    for (CargosFijosConvenios cfc : convenio.getCargos_fijos_convenios()) {
                        if(cfc.getDescripcion().equals("INTERES")){ cuentaInt = cfc.getCuenta();  }
                        if(cfc.getDescripcion().equals("CAT")){ cuentaCat = cfc.getCuenta();}
                        if(cfc.getDescripcion().equals("CUOTA-ADMINISTRCION")){ cuentaCA = cfc.getCuenta();}
                        if(cfc.getDescripcion().equals("SEGURO-DEUDOR")){ cuentaSegDeudor = cfc.getCuenta();}
                     }

                    if (!renovacion) {//si es false se contabiliza como estaba antes. 
                        
                        //1-valor debito =Valor Negocio + Intereses Corriente + Comision Cat + Cuota Administracion
                        totdeb = comprobante.getValor() + comprobante.getValor_int_coriente() + comprobante.getValor_cat() + comprobante.getCuota_administracion() + comprobante.getSeguro_deudor() + comprobante.getVlr_aval();
                        comprobante.setTotal_debito(totdeb);
                        itemscopy(comprobante); //valor
                        
                        //2-Credito Valor Neg
                          itemscopy3(comprobante, convenio.getCuenta_cxp(), comprobante.getValor(), comprobante.getTercero());
                        
                        //3-Credito Intereses Corrientes
                          itemscopy3(comprobante, cuentaInt, comprobante.getValor_int_coriente(), comprobante.getTercero());
                        
                        //4-Credito Valor cat
                          itemscopy3(comprobante, cuentaCat, comprobante.getValor_cat(), comprobante.getTercero());
                        
                        //5-Credito Valor cuota Admin
                          itemscopy3(comprobante, cuentaCA, comprobante.getCuota_administracion(), comprobante.getTercero()); 
                          
                        //7-Seguro deudor
                          itemscopy3(comprobante, cuentaSegDeudor, comprobante.getSeguro_deudor(), comprobante.getTercero()); 
                          
                        //8-Agrega valor del aval 
                          itemscopy3(comprobante, convenio.getCuenta_cxp(), comprobante.getVlr_aval(), comprobante.getTercero()); 
                          
                        //6-ajustamos debito y credito                        
                          comprobante.setTotal_credito(totdeb);
                          comprobante.setTotal_items(items);

                    } else {
                        
                        //1-Buscamos el total del capital que incluye los intereses capitalizados
                        double vlrTotalCapitalizado = dao.getVlrTotalCapitalizado(comprobante.getNumdoc());
                        System.out.println("Valor Capitalizado: " + vlrTotalCapitalizado);
                         
                        //2-calculamos el total debito del negocio
                        totdeb =vlrTotalCapitalizado + comprobante.getValor_int_coriente() + comprobante.getValor_cat() + comprobante.getCuota_administracion() + comprobante.getSeguro_deudor() +  comprobante.getVlr_aval();
                        comprobante.setTotal_debito(totdeb);
                        itemscopy(comprobante);
                        
                        //3-modificamos el valor credito por el del liquidador.
                        comprobante.setTotal_credito(totdeb);
        
                        //4-calculamos valor intereses primera cuota
                        double interesPrimeraCuota = vlrTotalCapitalizado - comprobante.getValor();
                       
                        /**
                         * creamos el segundo item para el comprobante, 
                         * la cuenta toca pegarla en el codigo mientras se modifica el convenio
                         * este item se crea solo cuando el valor de los intereses es mayor a cero.*/
                        
                        if (interesPrimeraCuota > 0) {
                            itemscopy3(comprobante, "I010130014169", interesPrimeraCuota, comprobante.getTercero());
                        }
                        //5-creamos el tercer item del comprobante con el valor negocio.
                        itemscopy3(comprobante, convenio.getCuenta_cxp(), comprobante.getValor(), comprobante.getTercero());
                        
                        //6-Credito Intereses Corrientes
                          itemscopy3(comprobante, cuentaInt, comprobante.getValor_int_coriente(), comprobante.getTercero());
                        
                        //7-Credito Valor cat
                          itemscopy3(comprobante, cuentaCat, comprobante.getValor_cat(), comprobante.getTercero());
                        
                        //8-Credito Valor cuota Admin
                          itemscopy3(comprobante, cuentaCA, comprobante.getCuota_administracion(), comprobante.getTercero());
                          
                        //9-Seguro deudor
                          itemscopy3(comprobante, cuentaSegDeudor, comprobante.getSeguro_deudor(), comprobante.getTercero()); 
                          
                        //10-Agrega valor del aval 
                          itemscopy3(comprobante, convenio.getCuenta_cxp(), comprobante.getVlr_aval(), comprobante.getTercero()); 

                        //cantidad de items
                        comprobante.setTotal_items(items);

                    }


                } else if (convenio.getTipo().equals("Consumo")){
                    if (convenio.isRedescuento() && !convenio.isAval_anombre()) {

                        ConveniosRemesas convRemesa = convenio.buscarRemesa(comprobante.getId_remesa());
                        ArrayList<ConvenioComision> comisiones = dao.buscar_comisiones(comprobante.getNumdoc());

                        Tipo_impuestoDAO timpuestoDao = new Tipo_impuestoDAO(this.dao.getDatabaseName());
                        Tipo_impuesto impuesto = timpuestoDao.buscarImpuestoxCodigoxFecha(distrito, convenio.getImpuesto(), comprobante.getFecha_creacion());

                        double porcImpuesto = impuesto.getPorcentaje1() / 100;
                        totdeb = comprobante.getTotal_debito();
                        totcred = comprobante.getTotal_credito();
                        aval = comprobante.getVlr_aval();
                        custodia = comprobante.getCustodia() * comprobante.getRef_5();
                        remesa = comprobante.getRem();
                        interes = dao.getSumaInteres(comprobante.getNumdoc());

                        itemscopy(comprobante);
                        for (int j = 0; j < comisiones.size(); j++) { //ingresa las contrapartidas de las comisiones en el debito
                            ConvenioComision convComision = comisiones.get(j);
                            if (convComision.isIndicador_contra()) {
                                itemscopy4(comprobante, convComision.getCuenta_contra(), convComision.getValor_comision());
                                comisionesContra += convComision.getValor_comision();
                            }
                        }
                        if (custodia > 0) {
                            itemscopy2(comprobante, convenio.getCuenta_custodia(), custodia);
                        }
                        if (remesa > 0) {
                            itemscopy2(comprobante, convRemesa.getCuenta_remesa(), remesa);
                        }
                        itemscopy2(comprobante, convenio.getCuenta_interes(), interes);
                        itemscopy3(comprobante, convenio.getCuenta_cxp(), totcred);

                        // Ingresa las comisiones
                        for (int j = 0; j < comisiones.size(); j++) { //ingresa las comisiones
                            ConvenioComision convComision = comisiones.get(j);
                            double vlrComision = convComision.getValor_comision() / (1 + porcImpuesto);
                            double impComision = convComision.getValor_comision() - vlrComision;
                            if (convComision.isComision_tercero() && convenio.isFactura_tercero()) {
                                comisionTercero = convComision.getValor_comision();
                                itemscopy3(comprobante, convComision.getCuenta_comision(), vlrComision);
                                itemscopy3(comprobante, convComision.getCuenta_comision(), impComision);
                            }
                            if (convComision.isIndicador_contra()) {
                                itemscopy2(comprobante, convComision.getCuenta_comision(), vlrComision);
                                itemscopy2(comprobante, convComision.getCuenta_comision(), impComision);
                            }
                        }

                        if (convenio.isFactura_tercero()) {  //ingresa el aval si el convenio factura a traves de tercero

                            double vlrAval = aval / (1 + porcImpuesto);
                            double impAval = aval - vlrAval;

                            itemscopy2(comprobante, convenio.getCuenta_aval(), vlrAval);
                            itemscopy2(comprobante, convenio.getCuenta_aval(), impAval);
                        }

                        if (!convenio.isFactura_tercero()) {
                            aval = 0;
                        }


                        comprobante.setTotal_items(items);
                        comprobante.setTotal_debito(totdeb + comisionesContra);
                        comprobante.setTotal_credito(totcred + interes + custodia + remesa + comisionesContra + aval + comisionTercero);

                        double diferenciaxdecimales = comprobante.getTotal_debito() - comprobante.getTotal_credito();
                        if (diferenciaxdecimales != 0) {
                            itemscopy5(comprobante, convenio.getCuenta_ajuste(), diferenciaxdecimales);
                            if (diferenciaxdecimales < 0) {
                                comprobante.setTotal_debito(comprobante.getTotal_debito() + (diferenciaxdecimales * (-1)));
                            } else {
                                comprobante.setTotal_credito(comprobante.getTotal_credito() + diferenciaxdecimales);
                            }
                        }
                    } else {

                        if (convenio.getTipo().equals("Consumo") && convenio.isRedescuento() && convenio.isAval_anombre() && (comprobante.isRef_6() || negserv.isNegocioAval(comprobante.getNumdoc()))) {// es financiado o es un negocio de aval

                            ConveniosRemesas convRemesa = convenio.buscarRemesa(comprobante.getId_remesa());
                            ArrayList<ConvenioComision> comisiones = dao.buscar_comisiones(comprobante.getNumdoc());

                            Tipo_impuestoDAO timpuestoDao = new Tipo_impuestoDAO(this.dao.getDatabaseName());
                            Tipo_impuesto impuesto = timpuestoDao.buscarImpuestoxCodigoxFecha(distrito, convenio.getImpuesto(), comprobante.getFecha_creacion());

                            double porcImpuesto = impuesto.getPorcentaje1() / 100;
                            //se resta cuota adminisracion -comprobante.getCuota_administracion()
                            totdeb = comprobante.getTotal_debito();
                            totcred = comprobante.getTotal_credito();
                            aval = comprobante.getVlr_aval();
                            custodia = comprobante.getCustodia() * comprobante.getRef_5();
                            remesa = comprobante.getRem();
                            interes = dao.getSumaInteres(comprobante.getNumdoc());
                            cuota_admin =comprobante.getCuota_administracion();
                            seguro_deudor =comprobante.getSeguro_deudor();
                            
                            
                            itemscopy(comprobante);
                            for (int j = 0; j < comisiones.size(); j++) { //ingresa las contrapartidas de las comisiones en el debito
                                ConvenioComision convComision = comisiones.get(j);
                                if (convComision.isIndicador_contra()) {
                                    itemscopy4(comprobante, convComision.getCuenta_contra(), convComision.getValor_comision());
                                    comisionesContra += convComision.getValor_comision();
                                }
                            }
                            if (custodia > 0) {
                                itemscopy2(comprobante, convenio.getCuenta_custodia(), custodia);
                            }
                            if (remesa > 0) {
                                itemscopy2(comprobante, convRemesa.getCuenta_remesa(), remesa);
                            }
                      
                            if (cuota_admin > 0) {
                                itemscopy2(comprobante, convenio.getCuenta_cuota_administracion(), cuota_admin);
                            }
                            
                            if (seguro_deudor > 0) {
                                itemscopy2(comprobante, convenio.getCuenta_seguro(), seguro_deudor);
                            }

                            if (!convenio.isMediador_aval())//generar items intereses
                            {
                                itemscopy2(comprobante, convenio.getCuenta_interes(), interes);
                            }

                            if (negserv.isNegocioAval(comprobante.getNumdoc()))// si es un negocio de aval
                            {
                                itemscopy3(comprobante, convenio.getCuenta_cxp_avalista(), totcred);
                            } else {

                                itemscopy3(comprobante, convenio.getCuenta_cxp(), totcred);
                            }
                            // Ingresa las comisiones
                            for (int j = 0; j < comisiones.size(); j++) { //ingresa las comisiones
                                ConvenioComision convComision = comisiones.get(j);
                                double vlrComision = convComision.getValor_comision() / (1 + porcImpuesto);
                                double impComision = convComision.getValor_comision() - vlrComision;
                                if (convComision.isComision_tercero() && convenio.isFactura_tercero()) {
                                    comisionTercero = convComision.getValor_comision();
                                    itemscopy3(comprobante, convComision.getCuenta_comision(), vlrComision);
                                    itemscopy3(comprobante, convComision.getCuenta_comision(), impComision);
                                }
                                if (convComision.isIndicador_contra()) {
                                    itemscopy2(comprobante, convComision.getCuenta_comision(), vlrComision);
                                    itemscopy2(comprobante, convComision.getCuenta_comision(), impComision);
                                }
                            }
                            /*if (!convenio.isFactura_tercero()) {
                                aval = 0;
                            }*/
                            comprobante.setTotal_items(items);
                            comprobante.setTotal_debito(totdeb + comisionesContra);
                         //  comprobante.setTotal_credito(totcred + interes + custodia + remesa + comisionesContra + aval + comisionTercero);
                            comprobante.setTotal_credito(totcred + interes + custodia + remesa + comisionesContra + aval + comisionTercero+cuota_admin+seguro_deudor);

                            double diferenciaxdecimales = comprobante.getTotal_debito() - comprobante.getTotal_credito();
                            if (diferenciaxdecimales != 0) {
                                itemscopy5(comprobante, convenio.getCuenta_ajuste(), diferenciaxdecimales);
                                if (diferenciaxdecimales < 0) {
                                    comprobante.setTotal_debito(comprobante.getTotal_debito() + (diferenciaxdecimales * (-1)));
                                } else {
                                    comprobante.setTotal_credito(comprobante.getTotal_credito() + diferenciaxdecimales);
                                }
                            }
                        } else {

                            if (convenio.getTipo().equals("Consumo") && convenio.isRedescuento() && convenio.isAval_anombre() && (!comprobante.isRef_6() && !negserv.isNegocioAval(comprobante.getNumdoc() ) && convenio.isMediador_aval())) {// no es financiado  y no es un negocio de aval

                                ConveniosRemesas convRemesa = convenio.buscarRemesa(comprobante.getId_remesa());
                                ArrayList<ConvenioComision> comisiones = dao.buscar_comisiones(comprobante.getNumdoc());

                                Tipo_impuestoDAO timpuestoDao = new Tipo_impuestoDAO(this.dao.getDatabaseName());
                                Tipo_impuesto impuesto = timpuestoDao.buscarImpuestoxCodigoxFecha(distrito, convenio.getImpuesto(), comprobante.getFecha_creacion());

                                double porcImpuesto = impuesto.getPorcentaje1() / 100;
                               
                                totcred = comprobante.getTotal_credito();
                                totdeb=totcred;
                                comprobante.setTotal_debito(totcred);

                                 aval = comprobante.getVlr_aval();
                                
                                custodia = comprobante.getCustodia() * comprobante.getRef_5();
                                remesa = comprobante.getRem();
                                interes = dao.getSumaInteres(comprobante.getNumdoc());

                                itemscopy(comprobante);
                                for (int j = 0; j < comisiones.size(); j++) { //ingresa las contrapartidas de las comisiones en el debito
                                    ConvenioComision convComision = comisiones.get(j);
                                    if (convComision.isIndicador_contra()) {
                                        itemscopy4(comprobante, convComision.getCuenta_contra(), convComision.getValor_comision());
                                        comisionesContra += convComision.getValor_comision();
                                    }
                                }
                                if (custodia > 0) {
                                    itemscopy2(comprobante, convenio.getCuenta_custodia(), custodia);
                                }
                                if (remesa > 0) {
                                    itemscopy2(comprobante, convRemesa.getCuenta_remesa(), remesa);
                                }



                                    itemscopy3(comprobante, convenio.getCuenta_cxp(), totcred);//cxp negocio
                                    comprobante.setTotal_debito(aval);// cxc aval negocio
                                    itemscopy(comprobante);
                                    comprobante.setTercero(convenio.getNit_anombre());
                                    itemscopy3(comprobante, convenio.getCuenta_cxp_avalista(), aval);// cxp aval

                                



                                // Ingresa las comisiones
                                for (int j = 0; j < comisiones.size(); j++) { //ingresa las comisiones
                                    ConvenioComision convComision = comisiones.get(j);
                                    double vlrComision = convComision.getValor_comision() / (1 + porcImpuesto);
                                    double impComision = convComision.getValor_comision() - vlrComision;
                                    if (convComision.isComision_tercero() && convenio.isFactura_tercero()) {
                                        comisionTercero = convComision.getValor_comision();
                                        itemscopy3(comprobante, convComision.getCuenta_comision(), vlrComision);
                                        itemscopy3(comprobante, convComision.getCuenta_comision(), impComision);
                                    }
                                    if (convComision.isIndicador_contra()) {
                                        itemscopy2(comprobante, convComision.getCuenta_comision(), vlrComision);
                                        itemscopy2(comprobante, convComision.getCuenta_comision(), impComision);
                                    }
                                }
       
                                comprobante.setTotal_items(items);
                                comprobante.setTotal_debito(totdeb + custodia + remesa + comisionesContra + aval + comisionTercero);
                                comprobante.setTotal_credito(totcred + interes + custodia + remesa + comisionesContra + aval + comisionTercero);

                                double diferenciaxdecimales = comprobante.getTotal_debito() - comprobante.getTotal_credito();
                                if (diferenciaxdecimales != 0) {
                                    itemscopy5(comprobante, convenio.getCuenta_ajuste(), diferenciaxdecimales);
                                    if (diferenciaxdecimales < 0) {
                                        comprobante.setTotal_debito(comprobante.getTotal_debito() + (diferenciaxdecimales * (-1)));
                                    } else {
                                        comprobante.setTotal_credito(comprobante.getTotal_credito() + diferenciaxdecimales);
                                    }
                                }
                            } else {

                                if (convenio.getTipo().equals("Consumo") && convenio.isRedescuento() && convenio.isAval_anombre() && (!comprobante.isRef_6() && !negserv.isNegocioAval(comprobante.getNumdoc())) && !convenio.isMediador_aval() ) {// no es financiado  y no es un negocio de aval

                                ConveniosRemesas convRemesa = convenio.buscarRemesa(comprobante.getId_remesa());
                                ArrayList<ConvenioComision> comisiones = dao.buscar_comisiones(comprobante.getNumdoc());

                                Tipo_impuestoDAO timpuestoDao = new Tipo_impuestoDAO(this.dao.getDatabaseName());
                                Tipo_impuesto impuesto = timpuestoDao.buscarImpuestoxCodigoxFecha(distrito, convenio.getImpuesto(), comprobante.getFecha_creacion());

                                double porcImpuesto = impuesto.getPorcentaje1() / 100;

                                totcred = comprobante.getTotal_credito();
                                totdeb=totcred;
                                comprobante.setTotal_debito(totcred);

                                aval = comprobante.getVlr_aval();

                                custodia = comprobante.getCustodia() * comprobante.getRef_5();
                                remesa = comprobante.getRem();
                                interes = dao.getSumaInteres(comprobante.getNumdoc());                              
                                cuota_admin =comprobante.getCuota_administracion();
                                seguro_deudor=comprobante.getSeguro_deudor();
                                
                                comprobante.setTotal_debito(totcred+interes+cuota_admin+seguro_deudor);
                  
                                itemscopy(comprobante);
                                for (int j = 0; j < comisiones.size(); j++) { //ingresa las contrapartidas de las comisiones en el debito
                                    ConvenioComision convComision = comisiones.get(j);
                                    if (convComision.isIndicador_contra()) {
                                        itemscopy4(comprobante, convComision.getCuenta_contra(), convComision.getValor_comision());
                                        comisionesContra += convComision.getValor_comision();
                                    }
                                }
                                if (custodia > 0) {
                                    itemscopy2(comprobante, convenio.getCuenta_custodia(), custodia);
                                }
                                if (remesa > 0) {
                                    itemscopy2(comprobante, convRemesa.getCuenta_remesa(), remesa);
                                }

                                if (cuota_admin > 0) {
                                    itemscopy2(comprobante, convenio.getCuenta_cuota_administracion(), cuota_admin);
                                }
                                
                                if (seguro_deudor > 0) {
                                    itemscopy2(comprobante, convenio.getCuenta_seguro(), seguro_deudor);
                                }


                                itemscopy3(comprobante, convenio.getCuenta_cxp(), totcred);//cxp negocio
                                //aqui cramos una variable temporal para guardar el nit del afiliado 
                                String codCli = comprobante.getTercero();
                                comprobante.setTercero(convenio.getNit_anombre());
                                
                                if (!convenio.getId_convenio().equals("35")) {
                                    comprobante.setTotal_debito(aval);// cxc aval negocio
                                    itemscopy(comprobante);
                                    // itemscopy3(comprobante, convenio.getCuenta_cxp_avalista(), aval);// cxp aval
                                    //creamos item para la cxp de aval a nombre de fenalco.
                                    itemCxpAval(comprobante, convenio.getCuenta_cxp_avalista(), aval);
                                }
                                
                                

                                
                                // Ingresa las comisiones
                                for (int j = 0; j < comisiones.size(); j++) { //ingresa las comisiones
                                    ConvenioComision convComision = comisiones.get(j);
                                    double vlrComision = convComision.getValor_comision() / (1 + porcImpuesto);
                                    double impComision = convComision.getValor_comision() - vlrComision;
                                    if (convComision.isComision_tercero() && convenio.isFactura_tercero()) {
                                        comisionTercero = convComision.getValor_comision();
                                        itemscopy3(comprobante, convComision.getCuenta_comision(), vlrComision);
                                        itemscopy3(comprobante, convComision.getCuenta_comision(), impComision);
                                    }
                                    if (convComision.isIndicador_contra()) {
                                        itemscopy2(comprobante, convComision.getCuenta_comision(), vlrComision);
                                        itemscopy2(comprobante, convComision.getCuenta_comision(), impComision);
                                    }
                                }

                                //cambiamos el tercero nuevamente como venia originalmente 
                                comprobante.setTercero(codCli);
                                itemscopy2(comprobante, convenio.getCuenta_interes(), interes);
                                comprobante.setTotal_items(items);
                                comprobante.setTotal_debito(totdeb + custodia + interes+remesa + comisionesContra + aval + comisionTercero+cuota_admin+seguro_deudor);
                               // comprobante.setTotal_credito(totcred +  interes+custodia + remesa + comisionesContra + aval + comisionTercero);
                                comprobante.setTotal_credito(totcred +  interes+custodia + remesa + comisionesContra + aval + comisionTercero+cuota_admin+seguro_deudor);

                                double diferenciaxdecimales = comprobante.getTotal_debito() - comprobante.getTotal_credito();
                                if (diferenciaxdecimales != 0) {
                                    itemscopy5(comprobante, convenio.getCuenta_ajuste(), diferenciaxdecimales);
                                    if (diferenciaxdecimales < 0) {
                                        comprobante.setTotal_debito(comprobante.getTotal_debito() + (diferenciaxdecimales * (-1)));
                                    } else {
                                        comprobante.setTotal_credito(comprobante.getTotal_credito() + diferenciaxdecimales);
                                    }
                                }
                            }
                             else
                                     {
                                    GestionSolicitudAvalDAO solDao = new GestionSolicitudAvalDAO(this.dao.getDatabaseName());
                                    String form = solDao.buscarNumSolicitud(comprobante.getNumdoc());
                                    ArrayList<SolicitudDocumentos> lisdoc = solDao.buscarDocsSolicitud(Integer.parseInt(form));
                                    double comision_aval = 0;
                                    for (int j = 0; j < lisdoc.size(); j++) {
                                        comision_aval += lisdoc.get(j).getComisionAval();
                                    }
                                    comprobante.setTotal_debito(comprobante.getValor());
                                    //DEBITO
                                    comprobante.setCuenta(tituloValor.getCuenta_prov_cxc());
                                    itemscopy(comprobante);
                                    comprobante.setTercero(comprobante.getRef_1());
                                    itemscopy4(comprobante, convenio.getCctrl_db_cxc_aval(), comision_aval);
                                    //CREDITO
                                    itemscopy3(comprobante, tituloValor.getCuenta_prov_cxp() != null ? tituloValor.getCuenta_prov_cxp() : "", comprobante.getTotal_debito(), comprobante.getTercero());
                                    Tipo_impuestoDAO timpuestoDao = new Tipo_impuestoDAO(this.dao.getDatabaseName());
                                    Tipo_impuesto impuesto = timpuestoDao.buscarImpuestoxCodigoxFecha(comprobante.getDstrct(), convenio.getImpuesto(), comprobante.getFechadoc());

                                    double iva = (comision_aval * impuesto.getPorcentaje1()) / (100 + impuesto.getPorcentaje1());
                                    double ingreso = comision_aval - iva;
                                    itemscopy3(comprobante, convenio.getCctrl_cr_cxc_aval(), ingreso, comprobante.getTercero());
                                    itemscopy3(comprobante, convenio.getCctrl_iva_cxc_aval(), iva, comprobante.getTercero());

                                    totcred = comprobante.getValor() + comision_aval;
                                    comprobante.setTotal_credito(totcred);
                                    comprobante.setTotal_debito(totcred);


                                    comprobante.setTotal_items(items);

                                }
                            }
                        }


                        }
                    

                }
                comprobante.setTotal_items(items);
                comprobante.setItems(listitems);

                List ins = new LinkedList();
                ins.add(comprobante);
                insertar(ins, usuario);
            }
            }

            ESTADO = "Comprobantes generados : " + (listaContable.size() - CANT_ERROES) + ", Comprobantes con Errores : " + CANT_ERROES;
            if (CANT_ERROES > 0) {
                LogErrores(listaErrores,uSesion);
            }
           

        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }

    }
    /*
     * public double intcau(double interes,java.util.Calendar Fecha_Negocio,int
     * nodocs) { double vr=0; int mesn=0,anion=0,dian=0; java.util.Calendar
     * Fecha_Fin=java.util.Calendar.getInstance(); java.util.Calendar
     * Fecha_FinNeg=java.util.Calendar.getInstance(); mesn =
     * Fecha_Negocio.get(Calendar.MONTH); anion =
     * Fecha_Negocio.get(Calendar.YEAR); dian =
     * Fecha_Negocio.get(Calendar.DATE); Fecha_Negocio.set(anion, mesn-1, dian);
     * Fecha_FinNeg.set(anion, mesn-1, dian); int diam =
     * Util.diasMesB(anion,mesn); Fecha_Fin.set(anion, mesn-1, diam); int dias =
     * 1+(Util.diasTranscurridos(Fecha_Negocio, Fecha_Fin));
     * Util.AddMes(Fecha_FinNeg,nodocs); int
     * diasn=1+(Util.diasTranscurridos(Fecha_Negocio, Fecha_FinNeg)); return
     * java.lang.Math.round((interes*dias)/diasn);
    }
     */
    ///metodo que copia los datos de un comprobante a sus items
    public void itemscopy(Comprobantes neg) throws Exception {
        Comprobantes item = new Comprobantes();
        try {
            item.setTipo("D");
            item.setDstrct(neg.getDstrct());
            item.setTipodoc(neg.getTipodoc());
            item.setNumdoc(neg.getNumdoc());
            item.setGrupo_transaccion(neg.getGrupo_transaccion());
            item.setPeriodo(neg.getPeriodo());
            item.setTercero(neg.getTercero());
            item.setDocumento_interno(neg.getNumdoc());
            item.setDocrelacionado(neg.getNumdoc());
            item.setTipo_operacion(neg.getTipo_operacion());
            item.setAbc(neg.getAbc());
            item.setUsuario(neg.getUsuario());
            item.setBase(neg.getBase());
            item.setCuenta(neg.getCuenta());
            item.setDetalle(neg.getRef_2());
            item.setTotal_credito(0.0);
            item.setTotal_debito(neg.getTotal_debito());
            item.setAuxiliar(neg.getAuxiliar());
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
        items++;//contar los items
        listitems.add(item);//falta agregar a lista de items
    }

    /// items rd
    public void itemscopy2(Comprobantes neg, String parm, double parm2) throws Exception {
        Comprobantes item = new Comprobantes();
        try {
            item.setTipo("D");
            item.setDstrct(neg.getDstrct());
            item.setTipodoc(neg.getTipodoc());
            item.setNumdoc(neg.getNumdoc());
            item.setGrupo_transaccion(neg.getGrupo_transaccion());
            item.setPeriodo(neg.getPeriodo());
            item.setTercero(neg.getTercero());
            item.setDocumento_interno(neg.getNumdoc());
            item.setDocrelacionado(neg.getNumdoc());
            item.setTipo_operacion(neg.getTipo_operacion());
            item.setAbc(neg.getAbc());
            item.setUsuario(neg.getUsuario());
            item.setBase(neg.getBase());
            item.setCuenta(parm);
            item.setDetalle(dao.getAccountDES(parm));
            item.setTotal_credito(parm2);
            item.setTotal_debito(0.0);
            item.setAuxiliar("");
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
        items++;//contar los items
        listitems.add(item);//falta agregar a lista de items
    }

    public void itemscopy5(Comprobantes neg, String parm, double parm2) throws Exception {
        Comprobantes item = new Comprobantes();
        try {
            item.setTipo("D");//
            item.setDstrct(neg.getDstrct()); //
            item.setTipodoc(neg.getTipodoc());//
            item.setNumdoc(neg.getNumdoc());//
            item.setGrupo_transaccion(neg.getGrupo_transaccion());//
            item.setPeriodo(neg.getPeriodo());//
            item.setTercero(neg.getTercero());//
            item.setDocumento_interno(neg.getNumdoc());//
            item.setDocrelacionado(neg.getNumdoc());//
            item.setTipo_operacion(neg.getTipo_operacion());//
            item.setAbc(neg.getAbc());//
            item.setUsuario(neg.getUsuario());//
            item.setBase(neg.getBase());//
            item.setCuenta(parm);//
            item.setDetalle(dao.getAccountDES(parm));//
            if (parm2 < 0) {
                item.setTotal_debito(parm2 * (-1));//
                item.setTotal_credito(0.0);//
            } else {
                item.setTotal_debito(0.0); //
                item.setTotal_credito(parm2);//
            }
            item.setAuxiliar("");
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
        items++;//contar los items
        listitems.add(item);//falta agregar a lista de items
    }

    public void itemscopy3(Comprobantes neg, String parm, double parm2) throws Exception {
        Comprobantes item = new Comprobantes();
        try {
            item.setTipo("D");
            item.setDstrct(neg.getDstrct());
            item.setTipodoc(neg.getTipodoc());
            item.setNumdoc(neg.getNumdoc());
            item.setGrupo_transaccion(neg.getGrupo_transaccion());
            item.setPeriodo(neg.getPeriodo());
            item.setTercero(neg.getRef_1());
            item.setDocumento_interno(neg.getNumdoc());
            item.setDocrelacionado(neg.getNumdoc());
            item.setTipo_operacion(neg.getTipo_operacion());
            item.setAbc(neg.getAbc());
            item.setUsuario(neg.getUsuario());
            item.setBase(neg.getBase());
            item.setCuenta(parm);
            item.setDetalle(dao.getAccountDES(parm));
            item.setTotal_credito(parm2);
            item.setTotal_debito(0.0);
            item.setAuxiliar("AR-" + neg.getRef_1());
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
        items++;//contar los items
        listitems.add(item);//falta agregar a lista de items
    }
    
    
    /*
     *Metodo para agregar el item de la cxp de aval con tercero para fenalco atalntico
     *autor: egonzalez 
     *
     */
      public void itemCxpAval(Comprobantes neg, String parm, double parm2) throws Exception {
        Comprobantes item = new Comprobantes();
        try {
            item.setTipo("D");
            item.setDstrct(neg.getDstrct());
            item.setTipodoc(neg.getTipodoc());
            item.setNumdoc(neg.getNumdoc());
            item.setGrupo_transaccion(neg.getGrupo_transaccion());
            item.setPeriodo(neg.getPeriodo());
            item.setTercero(neg.getTercero());
            item.setDocumento_interno(neg.getNumdoc());
            item.setDocrelacionado(neg.getNumdoc());
            item.setTipo_operacion(neg.getTipo_operacion());
            item.setAbc(neg.getAbc());
            item.setUsuario(neg.getUsuario());
            item.setBase(neg.getBase());
            item.setCuenta(parm);
            item.setDetalle(dao.getAccountDES(parm));
            item.setTotal_credito(parm2);
            item.setTotal_debito(0.0);
            item.setAuxiliar("AR-" + neg.getRef_1());
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
        items++;//contar los items
        listitems.add(item);//falta agregar a lista de items
    }

    public void itemscopy4(Comprobantes neg, String parm, double parm2) throws Exception {
        Comprobantes item = new Comprobantes();
        try {
            item.setTipo("D");
            item.setDstrct(neg.getDstrct());
            item.setTipodoc(neg.getTipodoc());
            item.setNumdoc(neg.getNumdoc());
            item.setGrupo_transaccion(neg.getGrupo_transaccion());
            item.setPeriodo(neg.getPeriodo());
            item.setTercero(neg.getTercero());
            item.setDocumento_interno(neg.getNumdoc());
            item.setDocrelacionado(neg.getNumdoc());
            item.setTipo_operacion(neg.getTipo_operacion());
            item.setAbc(neg.getAbc());
            item.setUsuario(neg.getUsuario());
            item.setBase(neg.getBase());
            item.setCuenta(parm);
            item.setDetalle(dao.getAccountDES(parm));
            item.setTotal_credito(0.0);
            item.setTotal_debito(parm2);
            item.setAuxiliar("");
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
        items++;//contar los items
        listitems.add(item);//falta agregar a lista de items
    }

    /**
     * M�todo que inserta el comprobante y comprodet @autor.......fvillacob
     *
     * @throws......Exception
     * @version.....1.0.
     *
     */
    public void insertar(List lista, String user) throws Exception {
        try {

            TransaccionService scv = new TransaccionService(this.dao.getDatabaseName());
            //variables para verificar valores de comprodet y comprobante.
            double valorCreditoC = 0, valorDebitoC = 0;
            double valorCreditoD = 0, valorDebitoD = 0;
            
            //System.out.println("Insert");
            for (int i = 0; i < lista.size(); i++) {
                Comprobantes comprobante = (Comprobantes) lista.get(i);
                
                //obetenemos los valores creditos y debitos del la cabezera del comprobante.
                valorCreditoC=this.roundNumber(comprobante.getTotal_credito(),2);
                valorDebitoC=this.roundNumber(comprobante.getTotal_debito(),2);
                
                String eva = evaluarComprobante(comprobante);
                //System.out.println("EVA "+ eva);
                if (eva.equals("")) {

                    List items = comprobante.getItems();
                    List aux = new LinkedList();
                    int sw = 0;


                    // Validamos los items:
                    if (items != null) {

                        for (int j = 0; j < items.size(); j++) {
                            Comprobantes comprodet = (Comprobantes) items.get(j);
                            String evaDet = evaluarComprobante(comprodet);
                           
                            //sumamos valores creditos y debitos del comprodet
                            //para despues compararlos con la cabezera
                            valorCreditoD = this.roundNumber(valorCreditoD + comprodet.getTotal_credito(),2);
                            //valorCreditoD = valorCreditoD + comprodet.getTotal_credito();
                            valorDebitoD = this.roundNumber(valorDebitoD + comprodet.getTotal_debito(),2);
                            //valorDebitoD = valorDebitoD + comprodet.getTotal_debito();
                            
                            if (evaDet.equals("")) {
                                aux.add(comprodet);
                            } else {
                                comprobante.setComentario("Uno de sus item presenta irregularidad");
                                listaErrores.add(comprobante);     // Guardamos la factura
                                CANT_ERROES++;

                                comprodet.setComentario(evaDet);   // Guardamos el item
                                listaErrores.add(comprodet);

                                sw = 1;
                                break;
                            }
                        }
                    }
                    //aqui vamos a validar los intems con la cabezera.....!!!!!!
                    if (sw == 0 && (valorCreditoC == valorCreditoD && valorDebitoC == valorDebitoD)) {

                        items = aux;  // Items q cumplen para ser insertados

                        if (items != null && items.size() > 0) {

                            // Tomamos la secuencia:
                            int sec = comproDAO.getGrupoTransaccion();


                            // Realizamos los insert, update:
                            comprobante.setGrupo_transaccion(sec);
                            ArrayList<String> sql = new ArrayList<String>();
                            sql.add(dao.InsertComprobante(comprobante, user));
                            sql.addAll(dao.InsertComprobanteDetalle(items, sec, user));

                            //actualizar tabla de negocios

                            // Ejecutamos el SQL
                            if (!sql.equals("")) {
                                try {
                                    scv.crearStatement();
                                    for (String sql1 : sql) {
                                        scv.getSt().addBatch(sql1);
                                    }
                                    
                                    scv.execute();
                                    dao.updatecont(comprobante.getUsuario(), comprobante.getGrupo_transaccion(), comprobante.getPeriodo(), comprobante.getNumdoc());
                                } catch (Exception e) {
                                    //dao.deleteComprobante( comprobante );
                                    comprobante.setComentario("Error al momento de insertar : " + e.getMessage());
                                    CANT_ERROES++;
                                    listaErrores.add(comprobante);
                                }
                            }

                        } else {
                            comprobante.setComentario("No presenta items validos");
                            CANT_ERROES++;
                            listaErrores.add(comprobante);
                        }


                    }else{
                      
                        //log de errores cuando no cuadra el detalle con la cabezera...
                        String mjs = sw == 1 ? "Error en comprobante detalle" : "Valores debitos y creditos diferentes";
                        comprobante.setComentario(mjs);
                        CANT_ERROES++;
                        listaErrores.add(comprobante);     // agregamos mensaje al log.
                        
                    
                    }


                } else {
                    comprobante.setComentario(eva);
                    listaErrores.add(comprobante);
                    CANT_ERROES++;
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(" INSERTAR COMPROBANTE NEGOCIOS : " + e.getMessage());
        }
    }

    /**
     * M�todo que evalua datos v�lidos del comprobante y comprodet
     * @autor.......fvillacob
     *
     * @throws......Exception
     * @version.....1.0.
     *
     */
    public String evaluarComprobante(Comprobantes comprobante) throws Exception {
        String msj = "";
        try {
            //System.out.println("Evalua");
            // Campos comunes:
            if (comprobante.getDstrct().trim().equals("")) {
                msj += " No presenta distrito";
            }
            if (comprobante.getTipodoc().trim().equals("")) {
                msj += " No presenta tipo de documento";
            }
            if (comprobante.getNumdoc().trim().equals("")) {
                msj += " No presenta n�mero de documento";
            }
            if (comprobante.getPeriodo().trim().equals("")) {
                msj += " No presenta periodo de contabilizaci�n";
            }
            if (comprobante.getDetalle().trim().equals("")) {
                msj += " No presenta una descripci�n o detalle";
            }
            if (comprobante.getBase().trim().equals("")) {
                msj += " No presenta base";
            }


            String tipo = comprobante.getTipo();

            if (tipo.equals("C")) {

                if (comprobante.getTotal_debito() != comprobante.getTotal_credito()) {
                    msj += " Est� descuadrado en valores debitos y cr�ditos";
                }
                if (comprobante.getSucursal().trim().equals("")) {
                    msj += " No presenta sucursal";
                }
                if (comprobante.getFechadoc().trim().equals("")) {
                    msj += " No presenta fecha de documento";
                }
                if (comprobante.getTercero().trim().equals("")) {
                    msj += " No presenta tercero";
                }
                if (comprobante.getMoneda().trim().equals("")) {
                    msj += " No presenta moneda";
                }
                if (comprobante.getAprobador().trim().equals("")) {
                    msj += " No presenta aprobador";
                }

            } else {

                if (comprobante.getCuenta().trim().equals("")) {
                    msj += " No presenta cuenta";
                } else {
                    String cta = comprobante.getCuenta();
                    Hashtable cuenta = movDAO.getCuenta(comprobante.getDstrct(), cta);

                    if (cuenta != null) {

                        String requiereAux = (String) cuenta.get("auxiliar");
                        String requiereTercero = (String) cuenta.get("tercero");

                        // Tercero.
                        if (requiereTercero.equals("S") && comprobante.getTercero().trim().equals("")) {
                            msj += " La cuenta " + cta + " requiere tercero  y el item no presenta tercero";
                        }

                        // Auxiliar.
                        if (requiereAux.equals("S")) {

                            if (comprobante.getAuxiliar().trim().equals("")) {
                                msj += " La cuenta " + cta + " requiere auxiliar y el item no presenta auxiliar";
                            }

                            if (!dao.existeCuentaSubledger(comprobante.getDstrct(), cta, comprobante.getAuxiliar())) {
                                msj += "La cuenta " + cta + " no tiene el subledger " + comprobante.getAuxiliar() + " asociado";
                            }
                        }




                    } else {
                        msj += " La cuenta " + cta + " no existe o no est� activa";
                    }

                }

            }

            System.out.println("Msg" + msj);

        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
        return msj;
    }

    /**
     * M�todo que genera el Log de Errores @autor.......fvillacob
     *
     * @throws......Exception
     * @version..1
     *
     */
    public void LogErrores(List lista, Usuario user) throws Exception {
        try {

            // Ruta del archivo:
            String fecha = Util.getFechaActual_String(6).replaceAll("/|:| ", "");

            DirectorioService svc = new DirectorioService();
            svc.create(user.getLogin());
            String url = svc.getUrl() + user.getLogin() + "/"+user.getBd()+"_CONTANEGOCIOS" + fecha + ".txt";


            PrintWriter pw = new PrintWriter(new BufferedWriter(new FileWriter(url)));
            LogWriter logM = new LogWriter("Contabilizaci�n de Negocios", LogWriter.INFO, pw);

            try {
                logM.log("Inicio de la Contabilizaci�n....", logM.INFO);

                for (int j = 0; j < lista.size(); j++) {
                    Comprobantes comprodet = (Comprobantes) lista.get(j);
                    String msj = comprodet.getNumdoc() + " " + comprodet.getDetalle() + " CAUSA : " + comprodet.getComentario();
                    logM.log(msj, logM.INFO);
                }

                logM.log("Finalizaci�n...", logM.INFO);

            } catch (Exception e) {
                logM.log("\n\nError en Contabilizaci�n Facturas: " + e.getMessage(), logM.ERROR);
            } finally {
                pw.close();
            }

        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }

    // SET AND GET : -------------------------------------------------------------------------
    /**
     * Getter for property listaContable.
     *
     * @return Value of property listaContable.
     */
    public java.util.List getListaContable() {
        return listaContable;
    }

    /**
     * Setter for property listaContable.
     *
     * @param listaContable New value of property listaContable.
     */
    public void setListaContable(java.util.List listaContable) {
        this.listaContable = listaContable;
    }

    /**
     * Getter for property process.
     *
     * @return Value of property process.
     */
    public boolean isProcess() {
        return process;
    }

    /**
     * Setter for property process.
     *
     * @param process New value of property process.
     */
    public void setProcess(boolean process) {
        this.process = process;
    }

    /**
     * M�todo que descontabilizar @autor.......jescandon
     *
     * @throws......Exception
     * @version.....1.0.
     *
     */
    public String Descontabilizar(Vector VComprobantes, String user) throws Exception {
        String msjError = "";

        List listaErrores = new LinkedList();
        List listaGrabada = new LinkedList();


        try {

            String sql = "";
            Iterator it = VComprobantes.iterator();
            Vector VComprobantesDet;

            while (it.hasNext()) {
                Comprobantes aux = (Comprobantes) it.next();
                VComprobantesDet = dao.VectorComprobantes(aux);

                Iterator it2 = VComprobantesDet.iterator();

                while (it2.hasNext()) {
                    Comprobantes auxdet = (Comprobantes) it2.next();
                    sql += dao.DescontabilizarMayor(auxdet);
                    sql += dao.UpdateFechaAplicacion(auxdet);
                    if (auxdet.getAuxiliar().length() > 0) {
                        sql += dao.DescontabilizarMayoSubledger(auxdet);
                    }

                }//fin ComprobantesDet


                // Ejecutamos el SQL
                if (!sql.equals("")) {



                    try {
                        TransaccionService scv = new TransaccionService(this.dao.getDatabaseName());
                        scv.crearStatement();
                        scv.getSt().addBatch(sql);

                        scv.execute();

                        listaGrabada.add(aux);


                    } catch (Exception e) {
                        aux.setComentario(e.getMessage());
                        listaErrores.add(aux);
                        msjError += " No se pudo descontabilizar el comprobante -> " + aux.getNumdoc() + " ";
                    }

                }

                sql = "";

            }//fin Comprobantes



            if (listaErrores.size() > 0) {
                LogErroresDescontabilizar(listaErrores, user);
                msjError += " Comprobantes grabados " + listaGrabada.size() + ". Hay comprobantes que presentar�n errores, no cumple con alguna condici�n. Puede consultar el log de errores";
            }


        } catch (Exception e) {
            throw new Exception(" Descontabilizar : " + e.getMessage());
        }

        return msjError;

    }

    public void LogErroresDescontabilizar(List lista, String user) throws Exception {
        try {
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String ruta = rb.getString("ruta");
            String fecha = Util.getFechaActual_String(6).replaceAll("/|:| ", "");
            String url = ruta + "/exportar/migracion/" + user + "/" + "Descontabilizacion" + user + fecha + ".log";

            PrintWriter pw = new PrintWriter(new BufferedWriter(new FileWriter(url)));
            LogWriter logM = new LogWriter("Descontabilizacion", LogWriter.INFO, pw);

            try {
                logM.log("Inicio de la Descontabilizacion....", logM.INFO);

                for (int j = 0; j < lista.size(); j++) {
                    Comprobantes comprodet = (Comprobantes) lista.get(j);
                    String msj = comprodet.getNumdoc() + " " + " CAUSA : " + comprodet.getComentario();
                    logM.log(msj, logM.INFO);
                }

                logM.log("Finalizaci�n...", logM.INFO);

            } catch (Exception e) {
                logM.log("\n\nError en Contabilizaci�n Facturas: " + e.getMessage(), logM.ERROR);
            } finally {
                pw.close();
            }

        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    /**
     * M�todo de reversion de comprobantes @autor.......fvillacob
     *
     * @throws......Exception
     * @version.....1.0.
     *
     */
    public String Reversar(Vector lista, String user) throws Exception {
        String msjError = "";
        try {

            String sql = "";
            List listaGrabada = new LinkedList();//Lista de grabacion
            List listaErrores = new LinkedList();//Lista de errores;

            for (int i = 0; i < lista.size(); i++) {

                Comprobantes comprobante = (Comprobantes) lista.get(i);
                ComprobanteFacturas aux = this.dao.getComprobantes(comprobante);

                this.dao.InsertComprobante(aux, user);
                int sec = comproDAO.getGrupoTransaccion();
                aux.setGrupo_transaccion(sec);


                Comprobantes comp = new Comprobantes();
                comp.setTipodoc(aux.getTipodoc());
                comp.setNumdoc(aux.getNumdoc());
                comp.setGrupo_transaccion(comprobante.getGrupo_transaccion());

                List items = this.dao.ListaComprobantes(comp);

                sql += dao.InsertComprobanteDetalle(items, sec, user);

                aux.setComentario(sql);
                listaGrabada.add(aux);
            }

            // Ejecutamos el SQL
            if (!sql.equals("")) {

                try {
                    TransaccionService scv = new TransaccionService(this.dao.getDatabaseName());
                    scv.crearStatement();
                    scv.getSt().addBatch(sql);
                    scv.execute();

                } catch (Exception e) {
                    listaGrabada = this.dao.deleteComprobanteSQL(listaGrabada);
                    listaErrores.addAll(listaGrabada);
                    listaGrabada = new LinkedList();
                    msjError += "No se pudieron grabar algunos comprobantes hay un error en el batch->";
                    e.printStackTrace();
                }

            }

            if (listaErrores.size() > 0) {
                LogErroresReversar(listaErrores, user);
                msjError += " Comprobantes grabados " + listaGrabada.size() + ". Hay comprobantes que presentar�n errores, no cumple con alguna condici�n o el query presenta algun error. Puede consultar el log de errores";
            }


        } catch (Exception e) {
            throw new Exception(" Reversar : " + e.getMessage());
        }

        return msjError;

    }

    public void LogErroresReversar(List lista, String user) throws Exception {
        try {
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String ruta = rb.getString("ruta");
            String fecha = Util.getFechaActual_String(6).replaceAll("/|:| ", "");
            String url = ruta + "/exportar/migracion/" + user + "/" + "Descontabilizacion" + user + fecha + ".log";

            PrintWriter pw = new PrintWriter(new BufferedWriter(new FileWriter(url)));
            LogWriter logM = new LogWriter("Reversion", LogWriter.INFO, pw);

            try {
                logM.log("Inicio de la Reversion....", logM.INFO);

                for (int j = 0; j < lista.size(); j++) {
                    ComprobanteFacturas comprodet = (ComprobanteFacturas) lista.get(j);
                    String msj = comprodet.getNumdoc() + " " + "Error en el Query : " + comprodet.getComentario();
                    logM.log(msj, logM.INFO);
                }

                logM.log("Finalizaci�n...", logM.INFO);

            } catch (Exception e) {
                logM.log("\n\nError en Reversion Facturas: " + e.getMessage(), logM.ERROR);
            } finally {
                pw.close();
            }

        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    /**
     * Getter for property ESTADO.
     *
     * @return Value of property ESTADO.
     */
    public java.lang.String getESTADO() {
        return ESTADO;
    }

    /**
     * Setter for property ESTADO.
     *
     * @param ESTADO New value of property ESTADO.
     */
    public void setESTADO(java.lang.String ESTADO) {
        this.ESTADO = ESTADO;
    }

    public void itemscopy3(Comprobantes neg, String parm, double parm2, String tercero) throws Exception {
        Comprobantes item = new Comprobantes();
        try {
            item.setTipo("D");
            item.setDstrct(neg.getDstrct());
            item.setTipodoc(neg.getTipodoc());
            item.setNumdoc(neg.getNumdoc());
            item.setGrupo_transaccion(neg.getGrupo_transaccion());
            item.setPeriodo(neg.getPeriodo());
            item.setTercero(tercero);
            item.setDocumento_interno(neg.getNumdoc());
            item.setDocrelacionado(neg.getNumdoc());
            item.setTipo_operacion(neg.getTipo_operacion());
            item.setAbc(neg.getAbc());
            item.setUsuario(neg.getUsuario());
            item.setBase(neg.getBase());
            item.setCuenta(parm);
            item.setDetalle(dao.getAccountDES(parm));
            item.setTotal_credito(parm2);
            item.setTotal_debito(0.0);
            item.setAuxiliar("");
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
        items++;//contar los items
        listitems.add(item);//falta agregar a lista de items
    }
    
    public double roundNumber(double value, int places) {
    if (places < 0) throw new IllegalArgumentException();

    long factor = (long) Math.pow(10, places);
    value = value * factor;
    long tmp = Math.round(value);
    return (double) tmp / factor;
}

       
    
}
