/*
 * ImportacionComprobantesService.java
 *
 * Created on 24 de marzo de 2007, 08:49 AM
 */

package com.tsp.finanzas.contab.model.services;

import com.tsp.finanzas.contab.model.DAO.ImportacionComprobantesDAO;
import com.tsp.finanzas.contab.model.DAO.ContabilizacionIngresosDAO;
import com.tsp.finanzas.contab.model.beans.Comprobantes;
import java.util.*;


public class ImportacionComprobantesServices {
    
    ContabilizacionIngresosDAO daoConta;
    ImportacionComprobantesDAO dao;
    
    
    
    public ImportacionComprobantesServices() {
        dao = new ImportacionComprobantesDAO();
        daoConta = new ContabilizacionIngresosDAO();
    }
    public ImportacionComprobantesServices(String dataBaseName) {
        dao = new ImportacionComprobantesDAO(dataBaseName);
        daoConta = new ContabilizacionIngresosDAO(dataBaseName);
    }
    
    
    /**
     * Metodo para obtener los comprobantes pendientes a migrar.
     */
    public Vector obtenerComprobantesPendientes (String usuario) throws Exception {
        return dao.obtenerComprobantesPendientes(usuario);
    }
    
 
    /**
     * busca el valor de las monedas de las compa�ias 
     */
    public TreeMap obtenerDistritos() throws Exception {   
        return dao.obtenerDistritos();
    }
    
        
    /**
     * busca el valor de las monedas de las compa�ias 
     */
    public TreeMap obtenerDocumentosContables() throws Exception {
        return dao.obtenerDocumentosContables();
    }
    
    
    /**
     * verifica si un documento ya existe en la tabla de comprobantes 
     */
    public boolean existeDocumento(String dstrct, String tipodoc, String numdoc) throws Exception {
        return dao.existeDocumento(dstrct, tipodoc, numdoc);
    }
    
  
    /**
     * verifica si un nit ya existe en la tabla de nits
     */
    public boolean existeNit(String nit) throws Exception {
        return dao.existeNit(nit);
    }
    
    
    /**
     * verifica si un tipo de operacion ya existe
     */
    public boolean existeTipoOperacion(String tipo) throws Exception {
        return dao.existeTipoOperacion(tipo);
    }
    
    
    /**
     * captura el estado de un periodo A: abierto, C: cerrado
     */
    public String   obtenerEstadoPeriodoContable(String dstrct, String ano, String mes) throws Exception {
        return dao.obtenerEstadoPeriodoContable(dstrct, ano, mes);
    }
    
    

    /** 
     * validacion de la cuenta y del auxiliar asignado + el tercero
     */
    public String searchCuenta(String dstrct, String cuenta, String tipoauxiliar, String auxiliar, String tercero) throws Exception {
        return dao.searchCuenta(dstrct, cuenta, tipoauxiliar, auxiliar, tercero);
    }

    
    
    
    
    /** verifica si una planilla ya existe en la tabla de nits*/
    public boolean existePlanilla(String dstrct, String numpla) throws Exception {
        return dao.existePlanilla(dstrct, numpla);
    }
    /** verifica si una remesa ya existe en la tabla de nits*/
    public boolean existeRemesa(String dstrct, String numrem) throws Exception {
        return dao.existeRemesa(dstrct, numrem);
    }
    /** verifica si una remesa ya existe en la tabla de nits*/
    public boolean existeFacturaProveedor(String dstrct, String proveedor, String tipo_documento, String documento) throws Exception {
        return dao.existeFacturaProveedor(dstrct, proveedor, tipo_documento, documento);
    }
    /** verifica si una remesa ya existe en la tabla de nits*/
    public boolean existeFacturaCliente(String dstrct, String tipo_documento, String documento) throws Exception {
        return dao.existeFacturaCliente(dstrct, tipo_documento, documento);
    }
    /** verifica si una remesa ya existe en la tabla de nits*/
    public boolean existeIngreso(String dstrct, String tipo_documento, String num_ingreso) throws Exception {
        return dao.existeIngreso(dstrct, tipo_documento, num_ingreso);
    }      
    /** verifica si una remesa ya existe en la tabla de nits*/
    public boolean existeEgreso(String dstrct, String tipo_documento, String documento) throws Exception {
        return dao.existeEgreso(dstrct, tipo_documento, documento);
    }
    
    
    
    
    
    
    /**
     * Metodo para guardar el comprobante contable con sus detalles
     * @autor mfontalvo
     * @param c, combrobante a guardar
     * @param in, ingreso q se esta procesando.
     * @throws Exception.
     */
    public void saveComprobante (Comprobantes c) throws Exception {
        daoConta.saveComprobante(c);
    }
    
    /**
     * Metodo para validar el codigo abc
     * @autor mfontalvo
     * @param abc, codigo a validar     
     * @throws Exception.
     */
    public boolean searchABC(String abc) throws Exception {
        return dao.searchABC(abc); 
    }     
        
}
