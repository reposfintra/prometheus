/**************************************************************************
 * Nombre: ......................PeriodoService.java                      *
 * Descripci�n: .................Service de periodo_contable                 *
 * Autor:........................Osvaldo P�rez Ferrer                     *
 * Fecha:........................17 de Junio de 2006, 03:38 PM            *
 * Versi�n: Java ................1.0                                      *
 * Copyright: Fintravalores S.A. S.A.                                *
 **************************************************************************/

/*
 * PeriodoContableService.java
 *
 * Created on 17 de junio de 2006, 03:37 PM
 */

package com.tsp.finanzas.contab.model.services;

import com.tsp.finanzas.contab.model.beans.*;
import com.tsp.finanzas.contab.model.DAO.*;
import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.*;
/**
 *
 * @author  Osvaldo
 */
public class PeriodoContableService {
    
    
    private PeriodoContableDAO p;
    /** Creates a new instance of PeriodoContableService */
    public PeriodoContableService() {     
        p = new PeriodoContableDAO();
    }
    public PeriodoContableService(String dataBaseName) {     
        p = new PeriodoContableDAO(dataBaseName);
    }
    
    
     /**
     *Metodo que inserta periodo contable
     *@autor: Osvaldo P�rez Ferrer
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void insertPeriodoContable(PeriodoContable pc) throws SQLException{
        p.insertarPeriodoContable(pc);
    }
    
      /**
     *Metodo que obtiene todos los periodos contables
     *@autor: Osvaldo P�rez Ferrer
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public Vector getAllPeriodos() throws SQLException{
        return p.getAllPeriodos();
    }
    
    /**
     * M�todo que permite obtener un periodo
     *@param dstrct Distrito
     *@param anio A�o del periodo
     *@param mes Mes del periodo contable     
     */
    public void obtenerPeriodo( String dstrct, String anio, String mes ) throws SQLException{
        p.obtenerPeriodo( dstrct, anio, mes );
    }
    
    /**
     *M�todo que permite obtener un periodo est� anulado o no
     *@param dstrct Distrito
     *@param anio A�o del periodo
     *@param mes Mes del periodo contable     
     */
     public PeriodoContable obtenerTodosPeriodo( String dstrct, String anio, String mes ) throws SQLException{
         return p.obtenerTodosPeriodo(dstrct, anio, mes);
     }
     
       /**
     *M�todo que actualiza en la base de datos el objeto dado
     *@param PeriodoContable p     
     */ 
    public void updatePeriodoContable(PeriodoContable periodo) throws SQLException{
        p.updatePeriodoContable(periodo);
    }
    
     /**
     *M�todo que anula en la base de datos el periodo contable
     *@param dist distrito
     *@param anio a�o 
     *@param mes mes
     */
    public void deletePeriodoContable(String dist, String anio, String mes) throws SQLException{
        p.deletePeriodoContable(dist, anio, mes);
    }

    /**
     * Getter for property p.
     * @return Value of property p.
     */
    public PeriodoContable getP() {
        return p.getP();
    }
    
    /**
     * Setter for property p.
     * @param p New value of property p.
     */
    public void setP(PeriodoContable per) {
        p.setP(per);
    }
    
      /**
     *Metodo que obtiene todos los periodos contables que pertenezcan al a�o dado
     *@autor: Osvaldo P�rez Ferrer
     * @param : String anio: a�o del cual se desean consultar los periodos
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public Vector getAllPeriodos( String anio) throws SQLException{
        return p.getAllPeriodos( anio );
    }
}
