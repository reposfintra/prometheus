/***************************************
 * Nombre Clase ............. ContabilizacionFacturasCLServices.java
 * Descripci�n  .. . . . . .  Permitimos realizar los servicios para la contabilizacion de facturas de clientes
 * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
 * Fecha . . . . . . . . . .  10/07/2006
 * versi�n . . . . . . . . .  1.0
 * Copyright ...............  Transportes Sanchez Polo S.A.
 *******************************************/




package com.tsp.finanzas.contab.model.services;





import java.io.*;
import java.util.*;
import com.tsp.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.finanzas.contab.model.beans.*;
import com.tsp.finanzas.contab.model.DAO.ContabilizacionFacturasCLDAO;
import com.tsp.finanzas.contab.model.DAO.ContabilizacionFacturasDAO;
import com.tsp.operation.model.services.TasaService;
import com.tsp.operation.model.beans.Tasa;
import com.tsp.finanzas.contab.model.DAO.ComprobantesDAO;
import com.tsp.operation.model.beans.Usuario;





public class ContabilizacionFacturasCLServices {


    private  boolean                       process;
    private  ContabilizacionFacturasCLDAO  dao;
    private  List                          listaContable;
    private  ContabilizacionFacturasDAO    dao2;

    private  String    CTA_DIF_CAMBIO_OT      =  "G01C009107027";
    private  String    DESC_DIF_CAMBIO        =  "DIFERENCIA EN CAMBIO REMESA ";
    private  String    DESC_DIF_FLETE         =  "DIFERENCIA EN FLETE REMESA ";
    private  String    CONCEPTO_DIF           =  "99";
    private  String    TIPO_OT                =  "002";



    private final    String  PESOS            =  "PES";
    private final    String  TIPO_FLETE       =  "FLETE";
    private final    String  TIPO_CAMBIO      =  "CAMBIO";

    private  ComprobantesDAO             comproDAO;




    public ContabilizacionFacturasCLServices() {
        dao           = new ContabilizacionFacturasCLDAO();
        process       = false;
        listaContable = new LinkedList();
        comproDAO      = new  ComprobantesDAO();
        dao2            = new ContabilizacionFacturasDAO();
    }

    public ContabilizacionFacturasCLServices(String dataBaseName) {
        dao           = new ContabilizacionFacturasCLDAO(dataBaseName);
        process       = false;
        listaContable = new LinkedList();
        comproDAO      = new  ComprobantesDAO(dataBaseName);
        dao2            = new ContabilizacionFacturasDAO(dataBaseName);
    }

    public void setDataBaseName(String dataBaseName) {
        dao.setDatabaseName(dataBaseName);
        dao2.setDatabaseName(dataBaseName);
        comproDAO.setDatabaseName(dataBaseName);
    }


    public void reset(){
        listaContable  = new LinkedList();
    }






     /**
       * M�todo que carga las factura clientes que no esten contabilizadas
       * @autor.......fvillacob
       * @throws......Exception
       * @version.....1.0.
       **/
  public String contabilizar(String distrito, String ayer, Usuario usuario, String id_agencia)throws Exception{//fdiaz 2011-03-14 - GEOTECH - Contabilizacion IVA en CxP
        String msj = "";
        try{
           String hoy     =  Utility.getHoy("-");
           String periodo = ayer.substring(0, 7).replaceAll("-", "");
           listaContable  =  dao.getFacturasClientesNoContabilizadas(distrito, ayer);//ARREGLAR LOS PARAMETROS PARA PRUEBA
           ContabilizacionFacturasServices  svc    = new ContabilizacionFacturasServices(dao.getDatabaseName());
           String  anoFactura  =  hoy.substring(0,4);
           String  TIPO_DOC;
           String   ctasaldo="";
           String   auxiliar="";
           // COMPROBANTES : FACTURAS

           for(int i=0;i<listaContable.size();i++){

                    ComprobanteFacturas  comprobante = (ComprobanteFacturas) listaContable.get(i);
                    int sec    = comproDAO.getGrupoTransaccion();
                    comprobante.setFechadoc   ( hoy );
                    comprobante.setOrigen("C");
                    comprobante.setSucursal("OP");
                    comprobante.setTipo_operacion("002");
                    comprobante.setBase("COL");
                    comprobante.setUsuario(usuario.getLogin());
                    comprobante.setAprobador(usuario.getLogin());
                    comprobante.setGrupo_transaccion(sec);
                    comprobante.setPeriodo(periodo);
                    TIPO_DOC = comprobante.getTipodoc();
                    double  vlrDebito  = 0;
                    double  vlrCredito = 0;
                    List listaDetalle  = new LinkedList(); //ALMACENA LOS ITEMS CREADOS
                // ITEMS de la factura:
                    List items = dao.getItems( comprobante );
                    for(int j=0;j<items.size();j++){
                        ComprobanteFacturas  comprodet = (ComprobanteFacturas)items.get(j);
                        comprodet.setPeriodo     ( comprobante.getPeriodo()      );
                        comprodet.setFechadoc    ( comprobante.getFechadoc()     );
                        comprodet.setTipodoc     ( comprobante.getTipodoc()      );
                        comprodet.setBase        ( comprobante.getBase()         );
                        comprodet.setTipo_referencia_1(comprobante.getTipo_referencia_1());
                        comprodet.setReferencia_1(comprobante.getReferencia_1());
                        comprodet.setTipo        ("D");
                        comprodet.setDocumento_interno("FAP");
                        comprobante.setNumdoc_rel(comprodet.getNumdoc_rel());//fdiaz 20/05/2010
                        comprobante.setTdoc_rel(comprodet.getTdoc_rel());//fdiaz 20/05/2010
                        comprobante.setTipo_referencia_1(comprodet.getTipo_referencia_1());//fdiaz 24/05/2010
                        comprobante.setReferencia_1(comprodet.getReferencia_1());//fdiaz 24/05/2010
                        comprobante.setTipo_referencia_2(comprodet.getTipo_referencia_2());//fdiaz 24/05/2010
                        comprobante.setReferencia_2(comprodet.getReferencia_2());//fdiaz 24/05/2010
                        comprobante.setTipo_referencia_3(comprodet.getTipo_referencia_3());//fdiaz 24/05/2010
                        comprobante.setReferencia_3(comprodet.getReferencia_3());//fdiaz 24/05/2010
                        if(comprodet.getTipo_valor().equals("003") || comprodet.getTipo_valor().equals("002") || comprodet.getTipo_valor().equals("007"))//Mod Tmolina 2008-09-15
                        {
                            ComprobanteFacturas con=itemscopy(comprobante);
                            con.setTotal_debito(comprodet.getValor());
                            con.setAuxiliar(comprodet.getAuxiliar());
                            con.setCuenta(comprodet.getCuenta());
                            ctasaldo=comprobante.getCuenta();
                            auxiliar="AR-"+comprobante.getTercero();
                            vlrDebito+=comprodet.getValor();
                            listaDetalle.add(con);
                        }
                        else
                        {
                            ComprobanteFacturas con=itemscopy(comprobante);
                            if( TIPO_DOC.equals( "NC" ) ){
                                    con.setTotal_credito(comprodet.getValor());

                                     if (comprodet.getValor()<0){
                                       con.setTotal_credito(0);
                                       con.setTotal_debito(-comprodet.getValor());
                                       vlrDebito+=-comprodet.getValor();
                                     }
                                    else
                                     {
                                        vlrCredito+=comprodet.getValor();
                                    }
                                    con.setCuenta(comprodet.getCuenta());
                                    con.setDetalle("NOTA CREDITO");
                                    ctasaldo=comprobante.getCuenta();
                                    
                                }
                                else
                                {
                                    //inicio de parte importante 090525

                                    con.setCuenta(comprodet.getCuenta());
                                    ctasaldo=comprobante.getCuenta();
                                    //System.out.println("vlrDebitomiti:"+vlrDebito);
                                    //System.out.println("comprodet.getValor()2:"+comprodet.getValor());
                                    //System.out.println("=vlrDebito+comprodet.getValor()"+(vlrDebito+comprodet.getValor()));

                                    if (comprodet.getValor()<0){
                                        con.setTotal_debito(0);
                                        con.setTotal_credito(-comprodet.getValor());
                                        vlrCredito=vlrCredito-comprodet.getValor();
                                        vlrCredito=redondear(vlrCredito,2);
                                    }else{
                                        con.setTotal_debito(comprodet.getValor());
                                        con.setTotal_credito(0);
                                        vlrDebito+=comprodet.getValor();//vlrDebito=vlrDebito+comprodet.getValor();
                                        vlrDebito=redondear(vlrDebito,2);

                                    }

                                    //fin de parte importante 090525

                                    //con.setTotal_debito(comprodet.getValor());
                                    //con.setCuenta(comprodet.getCuenta());
                                    //ctasaldo=comprobante.getCuenta();

                                    //vlrDebito+=comprodet.getValor();
                                    //vlrDebito=redondear(vlrDebito,2);
                                    if( TIPO_DOC.equals( "ND" ) ){
                                        con.setDetalle("NOTA DEBITO");
                                    }

                                }
                            listaDetalle.add(con);

                            List listImpuestos = dao2.getImpuestos( comprodet, "OP" , anoFactura );
                            int  resultSQL     = dao2.CANTIDAD_IMP_ITEM;
                            int  totalIMP      = dao2.getCantidadItemsImpuestos(comprodet);

                        // Control,cantidad de Imp
                           if( totalIMP ==  resultSQL  ) {
                               //ystem.out.println("Si");
                                 for(int k=0;k<listImpuestos.size();k++){
                                        ComprobanteFacturas  imp = (ComprobanteFacturas)listImpuestos.get(k);
                                        imp = copy( comprodet, imp );

                                        String tipoImp        = imp.getTipoImpuesto();

                                     // Cuenta del impuesto (Para el iva colocar el mismo del item):
                                       String cta = dao2.getCuentaImpuesto(imp.getDstrct(), imp.getCodigoImpuesto(), id_agencia, anoFactura);//fdiaz 2011-03-14 - GEOTECH - Contabilizacion IVA en CxP
                                        String requiereAUX =  dao2.requiereAUX(distrito, cta );
                                        if( !requiereAUX.equals("S")  )
                                             imp.setAuxiliar("");

                                        imp.setCuenta( cta  );

                                        double  vlrImp        = imp.getVlr();
                                        double  vlrDebitoImp  = 0;
                                        double  vlrCreditoImp = 0;


                                     // Calculamos ubicacion valor:

                                        if( !TIPO_DOC.equals( "NC" ) ){
                                             vlrDebitoImp  = ( vlrImp>0 )? vlrImp        : 0;
                                             vlrCreditoImp = ( vlrImp<0 )? (vlrImp * -1) : 0;
                                        }
                                        else{ // Invertimos Valores para las NC:
                                             vlrDebitoImp  = ( vlrImp>0 )?  0   :  vlrImp * -1 ;
                                             vlrCreditoImp = ( vlrImp<0 )?  0   :  vlrImp      ;
                                        }

                                        imp.setTotal_debito ( vlrDebitoImp  );
                                        imp.setTotal_credito( vlrCreditoImp );

                                     // Grabamos detalle
                                        listaDetalle.add( imp );

                                     // Sumatoria valores
                                        vlrDebito  +=  vlrDebitoImp;
                                        vlrCredito +=  vlrCreditoImp;
                                        }


                                       }else{
                                           comprodet.setComentario(" Para el item " + comprodet.getNoItem()  +  " no se procesaron todos sus impuestos");
                                            //ystem.out.println("No");
                                       }
                                }

                    }//for Items

                // Pendiente saldo:

                   double  saldoComprobante         =  Util.roundByDecimal( vlrDebito - vlrCredito, 2 );

                   if(  saldoComprobante !=  0    ){

                           double  vlrSaldoDebito           =  ( saldoComprobante > 0)?  0  :  saldoComprobante * -1 ;
                           double  vlrSaldoCredito          =  ( saldoComprobante < 0)?  0  :  saldoComprobante      ;
                           Comprobantes detalleSaldo = itemscopy(comprobante);
                           detalleSaldo.setTotal_credito( (vlrSaldoCredito) );
                           detalleSaldo.setTotal_debito ( vlrSaldoDebito  );
                           detalleSaldo.setCuenta(ctasaldo);
                           detalleSaldo.setAuxiliar(auxiliar);
                           detalleSaldo.setDetalle(comprobante.getDetalle());
                           ///FIN CONDICIONES


                           vlrDebito  +=  vlrSaldoDebito;
                           vlrDebito=redondear(vlrDebito,2);
                           vlrCredito +=  vlrSaldoCredito;
                           vlrCredito =redondear(vlrCredito ,2);
                           listaDetalle.add( detalleSaldo );

                    }
                  // ystem.out.println("Items "+listaDetalle.size());
                 // ACTUALIZAMOS DATOS COMPROBANTES
                    comprobante.setTotal_credito( vlrCredito   );
                    comprobante.setTotal_debito ( vlrDebito    );
                    comprobante.setItems        ( listaDetalle );
                    comprobante.setTotal_items  ( listaDetalle.size()   );


                // INSERTAMOS
                   List ins = new LinkedList();
                   ins.add( comprobante );
                   svc.insertar( ins, usuario.getLogin() );  // INSERTAMOS EL COMPROBANTE
            }

            svc.ESTADO  =  "Comprobantes generados : " + ( listaContable.size() - svc.CANT_ERROES  ) + ", Comprobantes con Errores : "+ svc.CANT_ERROES;
            if( svc.CANT_ERROES >0  ){
                 svc.LogErrores( svc.listaErrores , usuario );
            }

            msj = svc.getESTADO();



        }catch(Exception e){
            e.printStackTrace();
            msj = e.getMessage();
            throw new Exception(e.getMessage());
        }

        return msj;

    }





    /**
       * M�todo que carga el item de diferencia
       * @autor.......fvillacob
       * @throws......Exception
       * @version.....1.0.
       **/
    public ComprobanteFacturas itemscopy(ComprobanteFacturas neg)throws Exception{
        ComprobanteFacturas item=new ComprobanteFacturas();
        try{
         item.setTipo("D");
         item.setDstrct(neg.getDstrct());
         item.setTipodoc(neg.getTipodoc());
         item.setNumdoc(neg.getNumdoc());
         item.setGrupo_transaccion(neg.getGrupo_transaccion());
         item.setPeriodo(neg.getPeriodo());
         item.setTercero(neg.getTercero());
         item.setDocumento_interno(neg.getNumdoc());
         //item.setNumdoc_rel(neg.getNumdoc());
         item.setTipo_operacion(neg.getTipo_operacion());
         item.setAbc(neg.getAbc());
         item.setUsuario(neg.getUsuario());
         item.setBase(neg.getBase());
         item.setCuenta(neg.getCuenta());
         item.setDetalle(neg.getRef_3());
         item.setAuxiliar(neg.getAuxiliar());
         item.setAbc("N/A");
         //item.setTdoc_rel(neg.getTipodoc());
         item.setNumdoc_rel(neg.getNumdoc_rel());//fdiaz 20/05/2010
	 item.setTdoc_rel(neg.getTdoc_rel());//fdiaz 20/05/2010
	 item.setTipo_referencia_1(neg.getTipo_referencia_1());//fdiaz 24/05/2010
	 item.setReferencia_1(neg.getReferencia_1());//fdiaz 24/05/2010
	 item.setTipo_referencia_2(neg.getTipo_referencia_2());//fdiaz 24/05/2010
	 item.setReferencia_2(neg.getReferencia_2());//fdiaz 24/05/2010
	 item.setTipo_referencia_3(neg.getTipo_referencia_3());//fdiaz 24/05/2010
	 item.setReferencia_3(neg.getReferencia_3());//fdiaz 24/05/2010
        }catch(Exception e){
            throw new Exception( e.getMessage() );
        }
        return(item);//falta agregar a lista de items
    }


    public ComprobanteFacturas copy(ComprobanteFacturas items, ComprobanteFacturas imp)throws Exception{
        try{

            imp.setDstrct           (  items.getDstrct()            );
            imp.setTipodoc          (  items.getTipodoc()           );
            imp.setNumdoc           (  items.getNumdoc()            );
            imp.setCuenta           (  items.getCuenta()            );
            imp.setAuxiliar         (  items.getAuxiliar()          );
            imp.setTercero          (  items.getTercero()           );
            imp.setTipodoc_factura  (  items.getTipodoc_factura()   );
            imp.setDocumento_interno(  items.getDocumento_interno() );
            imp.setPeriodo          (  items.getPeriodo()           );
            imp.setFechadoc         (  items.getFechadoc()          );
            imp.setBase             (  items.getBase()              );
            imp.setTipo             (  items.getTipo()              );
            imp.setClaseFactura     (  items.getClaseFactura()      );
            imp.setAbc              (  items.getAbc()               );
            imp.setTdoc_rel         (  items.getTdoc_rel()          );
            //imp.setNumdoc_rel       (  items.getNumdoc_rel()        );
            imp.setTipo_referencia_1("");//fdiaz 24/05/2010
            imp.setReferencia_1("");//fdiaz 24/05/2010
            imp.setTipo_referencia_2("");//fdiaz 24/05/2010
            imp.setReferencia_2("");//fdiaz 24/05/2010
            imp.setTipo_referencia_3("");//fdiaz 24/05/2010
            imp.setReferencia_3("");//fdiaz 24/05/2010

        }catch(Exception e){
            throw new Exception( e.getMessage() );
        }
        return imp;
    }



     /**
       * M�todo que carga el item de diferencia
       * @autor.......fvillacob
       * @throws......Exception
       * @version.....1.0.
       **/
    public  ComprobanteFacturas  getDiferenciaTransporte(ComprobanteFacturas comprobante, ComprobanteFacturas comprodet, String tipo, double valor )throws Exception{
        ComprobanteFacturas  diferencia   =  new  ComprobanteFacturas();
        try{
                 valor = Util.roundByDecimal(valor,2);

                 double  vlrDebito       =    ( ( valor > 0 )? 0  : valor * -1 ) ;
                 double  vlrCredito      =    ( valor < 0 )? 0    : valor        ;
                 String  cta             =    ( tipo.equals( TIPO_FLETE ) )?  dao.getAccoundCodeI( comprodet.getOt() ) :  CTA_DIF_CAMBIO_OT   ;
                 String  desc            =    ( tipo.equals( TIPO_FLETE ) )?  DESC_DIF_FLETE                           :  DESC_DIF_CAMBIO     ;

                 diferencia.setDstrct           (   comprodet.getDstrct()              );
                 diferencia.setTipodoc          (   comprodet.getTipodoc()             );
                 diferencia.setTercero          (   comprodet.getTercero()             );
                 diferencia.setNumdoc           (   comprodet.getNumdoc()              );
                 diferencia.setNoItem           (   comprodet.getNoItem()+"-1"         );
                 diferencia.setDetalle          (   desc + comprodet.getOt()           );
                 diferencia.setPeriodo          (   comprodet.getPeriodo()             );
                 diferencia.setFechadoc         (   comprodet.getFechadoc()            );
                 diferencia.setBase             (   comprodet.getBase()                );
                 diferencia.setCuenta           (   cta                                );
                 diferencia.setAuxiliar         (   comprodet.getAuxiliar()            );
                 diferencia.setTipodoc_factura  (   comprodet.getTipodoc_factura()     );
                 diferencia.setDocumento_interno(   comprodet.getDocumento_interno()   );
                 diferencia.setTipo             (   comprodet.getTipo()                );
                 diferencia.setConcepto         (   CONCEPTO_DIF                       );
                 diferencia.setClaseFactura     (   comprodet.getClaseFactura()        );
                 diferencia.setTdoc_rel         (   TIPO_OT                            );
                 diferencia.setNumdoc_rel       (   comprodet.getOt()                  );
                 diferencia.setVlr              (   valor                              );
                 diferencia.setTotal_credito    (   vlrCredito                         );
                 diferencia.setTotal_debito     (   vlrDebito                          );

             //    ystem.out.println( diferencia.getCuenta() + " "+ diferencia.getDetalle() +" CR:"+ diferencia.getTotal_credito() +" DB:"+ diferencia.getTotal_debito()  );

        }catch(Exception e){
           throw new Exception ( e.getMessage()  );
        }
        return diferencia;
    }








    //---------------------------------------------------------------------------------------

    /**
     * Getter for property process.
     * @return Value of property process.
     */
    public boolean isProcess() {
        return process;
    }

    /**
     * Setter for property process.
     * @param process New value of property process.
     */
    public void setProcess(boolean process) {
        this.process = process;
    }



    /**
     * Getter for property listaContable.
     * @return Value of property listaContable.
     */
    public java.util.List getListaContable() {
        return listaContable;
    }

    /**
     * Setter for property listaContable.
     * @param listaContable New value of property listaContable.
     */
    public void setListaContable(java.util.List listaContable) {
        this.listaContable = listaContable;
    }


    public double redondear( double numero, int decimales ) {
	return Math.round(numero*Math.pow(10,decimales))/Math.pow(10,decimales);
    }


}
