package com.tsp.finanzas.contab.model.beans;

public class ResultadoMayorizacion {
    private double mayor;
    private double movimiento;
    private double cabecera;
    private String archivo;

    public ResultadoMayorizacion(String archivo, double mayor, double movimiento, double cabecera) {
        this.archivo = archivo;
        this.mayor = mayor;
        this.movimiento = movimiento;
        this.cabecera = cabecera;
    }

    public String getArchivo() {
        return archivo;
    }

    public double getCabecera() {
        return cabecera;
    }    

    public double getMayor() {
        return mayor;
    }
    
    public double getMovimiento() {
        return movimiento;
    }
}
