/********************************************************************
 *      Nombre Clase.................   RepGral.java
 *      Descripci�n..................
 *      Autor........................   Ing. Andr�s Maturana De La Cruz
 *      Fecha........................   14 de septiembre de 2006, 09:29 AM
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes S�nchez Polo S.A.
 *******************************************************************/
package com.tsp.finanzas.contab.model.beans;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.util.*;


/**
 *
 * @author  Ing. Andr�s Maturana De La Cruz
 */
public class RepGral implements Serializable {
    
    private String factura;
    private String fecha;
    private String nit;
    private String codcli;
    private String valor;
    private String fecha_anula;
    
    /** Crea una nueva instancia de  RepGral */
    public RepGral() {
    }
    
    /**
     * Obtiene el valor de cada uno de los campos del <code>ResultSet</code>
     * @autor Ing. Andr�s Maturana
     * @param rs <code>ResultSet</code> de donde se obtienen los campos
     * @throws SQLException
     * @version 1.0
     */
    public void Load(ResultSet rs) throws SQLException{
        this.setFactura(rs.getString("documento")); 
        this.setFecha(rs.getString("fecha_creacion"));
        this.setNit(rs.getString("nit"));
        this.setCodcli(rs.getString("codcli"));
        this.setValor(rs.getString("valor_factura"));
        this.setFecha_anula(rs.getString("fecha_anulacion"));
    }
    
    /**
     * Getter for property factura.
     * @return Value of property factura.
     */
    public java.lang.String getFactura() {
        return factura;
    }
    
    /**
     * Setter for property factura.
     * @param factura New value of property factura.
     */
    public void setFactura(java.lang.String factura) {
        this.factura = factura;
    }
    
    /**
     * Getter for property fecha.
     * @return Value of property fecha.
     */
    public java.lang.String getFecha() {
        return fecha;
    }
    
    /**
     * Setter for property fecha.
     * @param fecha New value of property fecha.
     */
    public void setFecha(java.lang.String fecha) {
        this.fecha = fecha;
    }
    
    /**
     * Getter for property nit.
     * @return Value of property nit.
     */
    public java.lang.String getNit() {
        return nit;
    }
    
    /**
     * Setter for property nit.
     * @param nit New value of property nit.
     */
    public void setNit(java.lang.String nit) {
        this.nit = nit;
    }
    
    /**
     * Getter for property codcli.
     * @return Value of property codcli.
     */
    public java.lang.String getCodcli() {
        return codcli;
    }
    
    /**
     * Setter for property codcli.
     * @param codcli New value of property codcli.
     */
    public void setCodcli(java.lang.String codcli) {
        this.codcli = codcli;
    }
    
    /**
     * Getter for property valor.
     * @return Value of property valor.
     */
    public java.lang.String getValor() {
        return valor;
    }
    
    /**
     * Setter for property valor.
     * @param valor New value of property valor.
     */
    public void setValor(java.lang.String valor) {
        this.valor = valor;
    }
    
    /**
     * Getter for property fecha_anula.
     * @return Value of property fecha_anula.
     */
    public java.lang.String getFecha_anula() {
        return fecha_anula;
    }
    
    /**
     * Setter for property fecha_anula.
     * @param fecha_anula New value of property fecha_anula.
     */
    public void setFecha_anula(java.lang.String fecha_anula) {
        this.fecha_anula = fecha_anula;
    }
    
}
