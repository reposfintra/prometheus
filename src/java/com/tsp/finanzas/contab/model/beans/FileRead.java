/*
 * FileRead.java
 *
 * Created on 6 de julio de 2006, 08:32 AM
 */
/**
 * Nombre        FileRead.java
 * Descripci�n   Utilidades para leer archivos
 * Autor         David Pi�a Lopez
 * Fecha         6 de julio de 2006, 08:32 AM
 * Version       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 **/
package com.tsp.finanzas.contab.model.beans;

import java.io.*;
import java.util.*;
import com.tsp.util.*;
/**
 *
 * @author  David
 */
public class FileRead  extends BufferedReader{
    
    private Vector lista;
    private double total_debito;
    private double total_credito;
    
    /** Creates a new instance of FileRead */
    public FileRead( String filename )  throws FileNotFoundException{
        super( 
           new BufferedReader( 
           new FileReader    (new File(filename) )));
        lista = new Vector();
    }
    
    public Vector cargarArchivo() throws Exception{
        String buffer = "";
        total_debito = 0;
        total_credito = 0;
        do{
            buffer = readLine();            
            if( buffer != null && buffer.length() > 202 ) {
                Comprobantes detalle = new Comprobantes();
                detalle.setDstrct( buffer.substring( 12, 15 ).trim() );                
                detalle.setCuenta( buffer.substring( 19, 32 ).trim() );                
                detalle.setPeriodo( "20" + buffer.substring( 55, 59 ).trim() );                
                double valor = Double.parseDouble( buffer.substring( 59, 80 ).trim() );
                
                if( valor > 0 ){
                    detalle.setTotal_debito( valor );
                    detalle.setTotal_credito( 0 );
                    total_debito += valor;
                }else{
                    detalle.setTotal_credito( valor * (-1) );
                    detalle.setTotal_debito( 0 );
                    total_credito += (valor * (-1));
                }                
                detalle.setFechadoc( Util.fechaPostgres( buffer.substring( 114, 122 ).trim() ) );
                detalle.setNumdoc( buffer.substring( 150, 160 ).trim() );                
                detalle.setDetalle( buffer.substring( 160, 184 ).trim() );
                lista.add( detalle );
            }
        }while( buffer != null );
        this.close();
        return lista;        
    }    
    
    /**
     * Getter for property lista.
     * @return Value of property lista.
     */
    public java.util.Vector getLista() {
        return lista;
    }
    
    /**
     * Setter for property lista.
     * @param lista New value of property lista.
     */
    public void setLista(java.util.Vector lista) {
        this.lista = lista;
    }
    
    /**
     * Getter for property total_credito.
     * @return Value of property total_credito.
     */
    public double getTotal_credito() {
        return total_credito;
    }
    
    /**
     * Setter for property total_credito.
     * @param total_credito New value of property total_credito.
     */
    public void setTotal_credito(double total_credito) {
        this.total_credito = total_credito;
    }
    
    /**
     * Getter for property total_debito.
     * @return Value of property total_debito.
     */
    public double getTotal_debito() {
        return total_debito;
    }
    
    /**
     * Setter for property total_debito.
     * @param total_debito New value of property total_debito.
     */
    public void setTotal_debito(double total_debito) {
        this.total_debito = total_debito;
    }
    
}
