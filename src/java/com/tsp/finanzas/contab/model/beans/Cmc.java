/********************************************************************************
 * Nombre clase :                   Cmc.java                                    *
 * Descripcion :                    Estructura del CMC                          *
 * Autor :                          David Pi�a L�pez                            *
 * Fecha :                          12 de junio de 2006                         *
 * Version :                        1.0                                         *
 * Copyright :                      Fintravalores S.A.                     *
 ********************************************************************************/
package com.tsp.finanzas.contab.model.beans;

import java.sql.*;

/**
 *
 * @author  David
 */
public class Cmc {

    private String reg_status;
    private String dstrct;
    private String tipodoc;
    private String cmc;
    private String cuenta;
    private String dbcr;
    private String usuario;
    private String last_update;
    private String user_update;
    private String creation_date;
    private String creation_user;
    private String sigla_comprobante ;
    private String base;
    private String tipo_cuenta;
    private String descripcionCmc;
    private String descripcionTipodoc;

    /** Creates a new instance of Cmc */
    public Cmc() {
    }

    public void Load(ResultSet rs) throws SQLException {
        this.setReg_status(rs.getString("reg_status"));
        this.setDstrct(rs.getString("dstrct"));
        this.setTipodoc(rs.getString("tipodoc"));
        this.setCmc(rs.getString("cmc"));
        this.setCuenta(rs.getString("cuenta"));
        this.setDbcr(rs.getString("dbcr"));
        this.setTipo_cuenta(rs.getString("tipo_cuenta"));
    }

    public Cmc(String dstrct,String tipodoc,String cmc,String cuenta,String dbcr,String tipo_cuenta,
                        String last_update,String creation_date,String base,String sigla_comprobante,String user){
     this.dstrct= dstrct;
     this.tipodoc=tipodoc ;
     this.cmc= cmc;
     this.cuenta= cuenta;
     this.dbcr= dbcr;
     this.tipo_cuenta=tipo_cuenta ;
     this.last_update=last_update ;
     this.creation_date= creation_date;
     this.base=base ;
     this.sigla_comprobante=sigla_comprobante ;
     this.usuario=usuario ;

    }

    /**
     * Getter for property reg_status.
     * @return Value of property reg_status.
     */
    public java.lang.String getReg_status() {
        return reg_status;
    }

    /**
     * Setter for property reg_status.
     * @param reg_status New value of property reg_status.
     */
    public void setReg_status(java.lang.String reg_status) {
        this.reg_status = reg_status;
    }

    /**
     * Getter for property dstrct.
     * @return Value of property dstrct.
     */
    public java.lang.String getDstrct() {
        return dstrct;
    }

    /**
     * Setter for property dstrct.
     * @param dstrct New value of property dstrct.
     */
    public void setDstrct(java.lang.String dstrct) {
        this.dstrct = dstrct;
    }

    /**
     * Getter for property tipodoc.
     * @return Value of property tipodoc.
     */
    public java.lang.String getTipodoc() {
        return tipodoc;
    }

    /**
     * Setter for property tipodoc.
     * @param tipodoc New value of property tipodoc.
     */
    public void setTipodoc(java.lang.String tipodoc) {
        this.tipodoc = tipodoc;
    }

    /**
     * Getter for property cmc.
     * @return Value of property cmc.
     */
    public java.lang.String getCmc() {
        return cmc;
    }

    /**
     * Setter for property cmc.
     * @param cmc New value of property cmc.
     */
    public void setCmc(java.lang.String cmc) {
        this.cmc = cmc;
    }

    /**
     * Getter for property cuenta.
     * @return Value of property cuenta.
     */
    public java.lang.String getCuenta() {
        return cuenta;
    }

    /**
     * Setter for property cuenta.
     * @param cuenta New value of property cuenta.
     */
    public void setCuenta(java.lang.String cuenta) {
        this.cuenta = cuenta;
    }

    /**
     * Getter for property dbcr.
     * @return Value of property dbcr.
     */
    public java.lang.String getDbcr() {
        return dbcr;
    }

    /**
     * Setter for property dbcr.
     * @param dbcr New value of property dbcr.
     */
    public void setDbcr(java.lang.String dbcr) {
        this.dbcr = dbcr;
    }

    /**
     * Getter for property tipo_cuenta.
     * @return Value of property tipo_cuenta.
     */
    public java.lang.String getTipo_cuenta() {
        return tipo_cuenta;
    }

    /**
     * Setter for property tipo_cuenta.
     * @param tipo_cuenta New value of property tipo_cuenta.
     */
    public void setTipo_cuenta(java.lang.String tipo_cuenta) {
        this.tipo_cuenta = tipo_cuenta;
    }

    /**
     * Getter for property descripcionCmc.
     * @return Value of property descripcionCmc.
     */
    public java.lang.String getDescripcionCmc() {
        return descripcionCmc;
    }

    /**
     * Setter for property descripcionCmc.
     * @param descripcionCmc New value of property descripcionCmc.
     */
    public void setDescripcionCmc(java.lang.String descripcionCmc) {
        this.descripcionCmc = descripcionCmc;
    }

    /**
     * Getter for property usuario.
     * @return Value of property usuario.
     */
    public java.lang.String getUsuario() {
        return usuario;
    }

    /**
     * Setter for property usuario.
     * @param usuario New value of property usuario.
     */
    public void setUsuario(java.lang.String usuario) {
        this.usuario = usuario;
    }

    /**
     * Getter for property base.
     * @return Value of property base.
     */
    public java.lang.String getBase() {
        return base;
    }

    /**
     * Setter for property base.
     * @param base New value of property base.
     */
    public void setBase(java.lang.String base) {
        this.base = base;
    }

    /**
     * Getter for property descripcionTipodoc.
     * @return Value of property descripcionTipodoc.
     */
    public java.lang.String getDescripcionTipodoc() {
        return descripcionTipodoc;
    }

    /**
     * Setter for property descripcionTipodoc.
     * @param descripcionTipodoc New value of property descripcionTipodoc.
     */
    public void setDescripcionTipodoc(java.lang.String descripcionTipodoc) {
        this.descripcionTipodoc = descripcionTipodoc;
    }
    public String getSigla_comprobante(){
        return sigla_comprobante;
    }

    public void setSigla_comprobante(String sigla_comprobante){
        this.sigla_comprobante = sigla_comprobante;
    }
}
