
package com.tsp.finanzas.contab.model.beans;
import java.util.Vector;
import java.util.*;

public class Comprobantes {
    private String dstrct;
    private String tipodoc;
    private String numdoc;
    private int grupo_transaccion;
    private int transaccion;
    private String sucursal;
    private String periodo;
    private String fechadoc;
    private String detalle;
    private String tercero;
    private double total_debito;
    private double total_credito;
    private int total_items;
    private String moneda;
    private String fecha_aplicacion;
    private String usuario_aplicacion;
    private String aprobador;
    private String usuario;
    private String base;
    private String cuenta;
    private String cuentacontable;
    private String auxiliar;
    private String documento_interno;
    private int porcent;
    private String account_code_c;
    private String cmc;
    private boolean tieneProv;
    private String numrem;
    private int porcent_contabilizado;
    private String tipo_cuenta;
    private String tipo_valor;//indica D si es debito, C si es credito
    private String fecha_creacion;//indica D si es debito, C si es credito
    private String banco;
    private String sucursal_banco;
    private String tipodoc_rel = "";//mfontalvo 14/04/07
    private String numdoc_rel = "";//mfontalvo 14/04/07
    private boolean isDifCambio;
    private Vector difCambio;
    private String tipo_operacion;
    private String tipo;
    private String origen;
    private String abc;
    private String tdoc_rel;
    private String docrelacionado;
    private String modrem;

    private Vector lista;
    private double valor;
    private double valor_for;
    private double valor_unit;
    private double cantidad;
    private double custodia;
    private double rem;
    private String sj;
    private String placa;
    private List items;

    private String comentario;
    private boolean errorDifCambio;

    //mfontalvo 23/04/2007
    private String nombre_tercero;
    private String ABC = "";

    private double sum_credito = 0;
    private double sum_debito = 0;

    private String ref_1 = "";

    private String ref_2 = "";

    private String ref_3;

    private double ref_4;

    private int ref_5;
    
    private boolean ref_6;

    private double tdesc;

    private double tpr;

    private String moneda_foranea = "";

    private double vlr_item_for;

    private double vlr_aval;

    private int id_convenio;
    private int id_remesa;
    private String tipoNegocio;
    
    private double cuota_administracion;
    private String aplica_iva;
    private double valor_cat;
    private double valor_int_coriente;
    private double seguro_deudor;
    

    public double getVlr_aval() {
        return vlr_aval;
    }

    public void setVlr_aval(double vlr_aval) {
        this.vlr_aval = vlr_aval;
    }


    /** Creates a new instance of Cliente */
    public Comprobantes() {
    }

    /**
     * Getter for property aprobador.
     * @return Value of property aprobador.
     */
    public java.lang.String getAprobador() {
        return aprobador;
    }

    /**
     * Setter for property aprobador.
     * @param aprobador New value of property aprobador.
     */
    public void setAprobador(java.lang.String aprobador) {
        this.aprobador = aprobador;
    }

    /**
     * Getter for property auxiliar.
     * @return Value of property auxiliar.
     */
    public java.lang.String getAuxiliar() {
        return auxiliar;
    }

    /**
     * Setter for property auxiliar.
     * @param auxiliar New value of property auxiliar.
     */
    public void setAuxiliar(java.lang.String auxiliar) {
        this.auxiliar = auxiliar;
    }

    /**
     * Getter for property base.
     * @return Value of property base.
     */
    public java.lang.String getBase() {
        return base;
    }

    /**
     * Setter for property base.
     * @param base New value of property base.
     */
    public void setBase(java.lang.String base) {
        this.base = base;
    }

    /**
     * Getter for property cuenta.
     * @return Value of property cuenta.
     */
    public java.lang.String getCuenta() {
        return cuenta;
    }

    /**
     * Setter for property cuenta.
     * @param cuenta New value of property cuenta.
     */
    public void setCuenta(java.lang.String cuenta) {
        this.cuenta = cuenta;
    }

    /**
    * AGREGAR CUENTA NUEVA
    */

    public java.lang.String getCuentaContable() {
        return cuentacontable;
    }

    public void setCuentaContable(java.lang.String cuentacontable) {
        this.cuentacontable = cuentacontable;
    }
    
    /**
     * Getter for property detalle.
     * @return Value of property detalle.
     */
    public java.lang.String getDetalle() {
        return detalle;
    }

    /**
     * Setter for property detalle.
     * @param detalle New value of property detalle.
     */
    public void setDetalle(java.lang.String detalle) {
        this.detalle = detalle;
    }

    /**
     * Getter for property documento_interno.
     * @return Value of property documento_interno.
     */
    public java.lang.String getDocumento_interno() {
        return documento_interno;
    }

    /**
     * Setter for property documento_interno.
     * @param documento_interno New value of property documento_interno.
     */
    public void setDocumento_interno(java.lang.String documento_interno) {
        this.documento_interno = documento_interno;
    }

    /**
     * Getter for property dstrct.
     * @return Value of property dstrct.
     */
    public java.lang.String getDstrct() {
        return dstrct;
    }

    /**
     * Setter for property dstrct.
     * @param dstrct New value of property dstrct.
     */
    public void setDstrct(java.lang.String dstrct) {
        this.dstrct = dstrct;
    }

    /**
     * Getter for property fecha_aplicacion.
     * @return Value of property fecha_aplicacion.
     */
    public java.lang.String getFecha_aplicacion() {
        return fecha_aplicacion;
    }

    /**
     * Setter for property fecha_aplicacion.
     * @param fecha_aplicacion New value of property fecha_aplicacion.
     */
    public void setFecha_aplicacion(java.lang.String fecha_aplicacion) {
        this.fecha_aplicacion = fecha_aplicacion;
    }

    /**
     * Getter for property fechadoc.
     * @return Value of property fechadoc.
     */
    public java.lang.String getFechadoc() {
        return fechadoc;
    }

    /**
     * Setter for property fechadoc.
     * @param fechadoc New value of property fechadoc.
     */
    public void setFechadoc(java.lang.String fechadoc) {
        this.fechadoc = fechadoc;
    }

    /**
     * Getter for property grupo_transaccion.
     * @return Value of property grupo_transaccion.
     */
    public int getGrupo_transaccion() {
        return grupo_transaccion;
    }

    /**
     * Setter for property grupo_transaccion.
     * @param grupo_transaccion New value of property grupo_transaccion.
     */
    public void setGrupo_transaccion(int grupo_transaccion) {
        this.grupo_transaccion = grupo_transaccion;
    }

    /**
     * Getter for property moneda.
     * @return Value of property moneda.
     */
    public java.lang.String getMoneda() {
        return moneda;
    }

    /**
     * Setter for property moneda.
     * @param moneda New value of property moneda.
     */
    public void setMoneda(java.lang.String moneda) {
        this.moneda = moneda;
    }

    /**
     * Getter for property numdoc.
     * @return Value of property numdoc.
     */
    public java.lang.String getNumdoc() {
        return numdoc;
    }

    /**
     * Setter for property numdoc.
     * @param numdoc New value of property numdoc.
     */
    public void setNumdoc(java.lang.String numdoc) {
        this.numdoc = numdoc;
    }

    /**
     * Getter for property periodo.
     * @return Value of property periodo.
     */
    public java.lang.String getPeriodo() {
        return periodo;
    }

    /**
     * Setter for property periodo.
     * @param periodo New value of property periodo.
     */
    public void setPeriodo(java.lang.String periodo) {
        this.periodo = periodo;
    }

    /**
     * Getter for property sucursal.
     * @return Value of property sucursal.
     */
    public java.lang.String getSucursal() {
        return sucursal;
    }

    /**
     * Setter for property sucursal.
     * @param sucursal New value of property sucursal.
     */
    public void setSucursal(java.lang.String sucursal) {
        this.sucursal = sucursal;
    }

    /**
     * Getter for property tercero.
     * @return Value of property tercero.
     */
    public java.lang.String getTercero() {
        return tercero;
    }

    /**
     * Setter for property tercero.
     * @param tercero New value of property tercero.
     */
    public void setTercero(java.lang.String tercero) {
        this.tercero = tercero;
    }

    /**
     * Getter for property tipodoc.
     * @return Value of property tipodoc.
     */
    public java.lang.String getTipodoc() {
        return tipodoc;
    }

    /**
     * Setter for property tipodoc.
     * @param tipodoc New value of property tipodoc.
     */
    public void setTipodoc(java.lang.String tipodoc) {
        this.tipodoc = tipodoc;
    }

    /**
     * Getter for property total_credito.
     * @return Value of property total_credito.
     */
    public double getTotal_credito() {
        return total_credito;
    }

    /**
     * Setter for property total_credito.
     * @param total_credito New value of property total_credito.
     */
    public void setTotal_credito(double total_credito) {
        this.total_credito = total_credito;
    }

    /**
     * Getter for property total_debito.
     * @return Value of property total_debito.
     */
    public double getTotal_debito() {
        return total_debito;
    }

    /**
     * Setter for property total_debito.
     * @param total_debito New value of property total_debito.
     */
    public void setTotal_debito(double total_debito) {
        this.total_debito = total_debito;
    }

    /**
     * Getter for property total_items.
     * @return Value of property total_items.
     */
    public int getTotal_items() {
        return total_items;
    }

    /**
     * Setter for property total_items.
     * @param total_items New value of property total_items.
     */
    public void setTotal_items(int total_items) {
        this.total_items = total_items;
    }

    /**
     * Getter for property transaccion.
     * @return Value of property transaccion.
     */
    public int getTransaccion() {
        return transaccion;
    }

    /**
     * Setter for property transaccion.
     * @param transaccion New value of property transaccion.
     */
    public void setTransaccion(int transaccion) {
        this.transaccion = transaccion;
    }

    /**
     * Getter for property usuario.
     * @return Value of property usuario.
     */
    public java.lang.String getUsuario() {
        return usuario;
    }

    /**
     * Setter for property usuario.
     * @param usuario New value of property usuario.
     */
    public void setUsuario(java.lang.String usuario) {
        this.usuario = usuario;
    }

    /**
     * Getter for property usuario_aplicacion.
     * @return Value of property usuario_aplicacion.
     */
    public java.lang.String getUsuario_aplicacion() {
        return usuario_aplicacion;
    }

    /**
     * Setter for property usuario_aplicacion.
     * @param usuario_aplicacion New value of property usuario_aplicacion.
     */
    public void setUsuario_aplicacion(java.lang.String usuario_aplicacion) {
        this.usuario_aplicacion = usuario_aplicacion;
    }

    /**
     * Getter for property cmc.
     * @return Value of property cmc.
     */
    public java.lang.String getCmc() {
        return cmc;
    }

    /**
     * Setter for property cmc.
     * @param cmc New value of property cmc.
     */
    public void setCmc(java.lang.String cmc) {
        this.cmc = cmc;
    }

    /**
     * Getter for property account_code_c.
     * @return Value of property account_code_c.
     */
    public java.lang.String getAccount_code_c() {
        return account_code_c;
    }

    /**
     * Setter for property account_code_c.
     * @param account_code_c New value of property account_code_c.
     */
    public void setAccount_code_c(java.lang.String account_code_c) {
        this.account_code_c = account_code_c;
    }

    /**
     * Getter for property porcent.
     * @return Value of property porcent.
     */
    public int getPorcent() {
        return porcent;
    }

    /**
     * Setter for property porcent.
     * @param porcent New value of property porcent.
     */
    public void setPorcent(int porcent) {
        this.porcent = porcent;
    }

    /**
     * Getter for property tieneProv.
     * @return Value of property tieneProv.
     */
    public boolean isTieneProv() {
        return tieneProv;
    }

    /**
     * Setter for property tieneProv.
     * @param tieneProv New value of property tieneProv.
     */
    public void setTieneProv(boolean tieneProv) {
        this.tieneProv = tieneProv;
    }

    /**
     * Getter for property numrem.
     * @return Value of property numrem.
     */
    public java.lang.String getNumrem() {
        return numrem;
    }

    /**
     * Setter for property numrem.
     * @param numrem New value of property numrem.
     */
    public void setNumrem(java.lang.String numrem) {
        this.numrem = numrem;
    }

    /**
     * Getter for property porcent_contabilizado.
     * @return Value of property porcent_contabilizado.
     */
    public int getPorcent_contabilizado() {
        return porcent_contabilizado;
    }

    /**
     * Setter for property porcent_contabilizado.
     * @param porcent_contabilizado New value of property porcent_contabilizado.
     */
    public void setPorcent_contabilizado(int porcent_contabilizado) {
        this.porcent_contabilizado = porcent_contabilizado;
    }

    /**
     * Getter for property tipo_cuenta.
     * @return Value of property tipo_cuenta.
     */
    public java.lang.String getTipo_cuenta() {
        return tipo_cuenta;
    }

    /**
     * Setter for property tipo_cuenta.
     * @param tipo_cuenta New value of property tipo_cuenta.
     */
    public void setTipo_cuenta(java.lang.String tipo_cuenta) {
        this.tipo_cuenta = tipo_cuenta;
    }

    /**
     * Getter for property tipo_valor.
     * @return Value of property tipo_valor.
     */
    public java.lang.String getTipo_valor() {
        return tipo_valor;
    }

    /**
     * Setter for property tipo_valor.
     * @param tipo_valor New value of property tipo_valor.
     */
    public void setTipo_valor(java.lang.String tipo_valor) {
        this.tipo_valor = tipo_valor;
    }

    /**
     * Getter for property fecha_creacion.
     * @return Value of property fecha_creacion.
     */
    public java.lang.String getFecha_creacion() {
        return fecha_creacion;
    }

    /**
     * Setter for property fecha_creacion.
     * @param fecha_creacion New value of property fecha_creacion.
     */
    public void setFecha_creacion(java.lang.String fecha_creacion) {
        this.fecha_creacion = fecha_creacion;
    }

    /**
     * Getter for property banco.
     * @return Value of property banco.
     */
    public java.lang.String getBanco() {
        return banco;
    }

    /**
     * Setter for property banco.
     * @param banco New value of property banco.
     */
    public void setBanco(java.lang.String banco) {
        this.banco = banco;
    }

    /**
     * Getter for property sucursal_banco.
     * @return Value of property sucursal_banco.
     */
    public java.lang.String getSucursal_banco() {
        return sucursal_banco;
    }

    /**
     * Setter for property sucursal_banco.
     * @param sucursal_banco New value of property sucursal_banco.
     */
    public void setSucursal_banco(java.lang.String sucursal_banco) {
        this.sucursal_banco = sucursal_banco;
    }

    /**
     * Getter for property tipodoc_rel.
     * @return Value of property tipodoc_rel.
     */
    public java.lang.String getTipodoc_rel() {
        return tipodoc_rel;
    }

    /**
     * Setter for property tipodoc_rel.
     * @param tipodoc_rel New value of property tipodoc_rel.
     */
    public void setTipodoc_rel(java.lang.String tipodoc_rel) {
        this.tipodoc_rel = tipodoc_rel;
    }

    /**
     * Getter for property numdoc_rel.
     * @return Value of property numdoc_rel.
     */
    public java.lang.String getNumdoc_rel() {
        return numdoc_rel;
    }

    /**
     * Setter for property numdoc_rel.
     * @param numdoc_rel New value of property numdoc_rel.
     */
    public void setNumdoc_rel(java.lang.String numdoc_rel) {
        this.numdoc_rel = numdoc_rel;
    }

    /**
     * Getter for property difCambio.
     * @return Value of property difCambio.
     */
    public boolean isIsDifCambio() {
        return isDifCambio;
    }

    /**
     * Setter for property difCambio.
     * @param difCambio New value of property difCambio.
     */
    public void setIsDifCambio(boolean difCambio) {
        this.isDifCambio = difCambio;
    }

    /**
     * Getter for property difCambio.
     * @return Value of property difCambio.
     */
    public Vector getDifCambio() {
        return difCambio;
    }

    /**
     * Setter for property difCambio.
     * @param difCambio New value of property difCambio.
     */
    public void setDifCambio(Vector difCambio) {
        this.difCambio = difCambio;
    }

    /**
     * Getter for property tipo_operacion.
     * @return Value of property tipo_operacion.
     */
    public java.lang.String getTipo_operacion() {
        return tipo_operacion;
    }

    /**
     * Setter for property tipo_operacion.
     * @param tipo_operacion New value of property tipo_operacion.
     */
    public void setTipo_operacion(java.lang.String tipo_operacion) {
        this.tipo_operacion = tipo_operacion;
    }

    /**
     * Getter for property lista.
     * @return Value of property lista.
     */
    public java.util.Vector getLista() {
        return lista;
    }

    /**
     * Setter for property lista.
     * @param lista New value of property lista.
     */
    public void setLista(java.util.Vector lista) {
        this.lista = lista;
    }

    /**
     * Getter for property valor.
     * @return Value of property valor.
     */
    public double getValor() {
        return valor;
    }

    /**
     * Setter for property valor.
     * @param valor New value of property valor.
     */
    public void setValor(double valor) {
        this.valor = valor;
    }

    /**
     * Getter for property valor_for.
     * @return Value of property valor_for.
     */
    public double getValor_for() {
        return valor_for;
    }

    /**
     * Setter for property valor_for.
     * @param valor_for New value of property valor_for.
     */
    public void setValor_for(double valor_for) {
        this.valor_for = valor_for;
    }

    /**
     * Getter for property valor_unit.
     * @return Value of property valor_unit.
     */
    public double getValor_unit() {
        return valor_unit;
    }

    /**
     * Setter for property valor_unit.
     * @param valor_unit New value of property valor_unit.
     */
    public void setValor_unit(double valor_unit) {
        this.valor_unit = valor_unit;
    }

    /**
     * Getter for property cantidad.
     * @return Value of property cantidad.
     */
    public double getCantidad() {
        return cantidad;
    }

    /**
     * Setter for property cantidad.
     * @param cantidad New value of property cantidad.
     */
    public void setCantidad(double cantidad) {
        this.cantidad = cantidad;
    }

    /**
     * Getter for property sj.
     * @return Value of property sj.
     */
    public java.lang.String getSj() {
        return sj;
    }

    /**
     * Setter for property sj.
     * @param sj New value of property sj.
     */
    public void setSj(java.lang.String sj) {
        this.sj = sj;
    }

    /**
     * Getter for property placa.
     * @return Value of property placa.
     */
    public java.lang.String getPlaca() {
        return placa;
    }

    /**
     * Setter for property placa.
     * @param placa New value of property placa.
     */
    public void setPlaca(java.lang.String placa) {
        this.placa = placa;
    }

    /**
     * Getter for property comentario.
     * @return Value of property comentario.
     */
    public java.lang.String getComentario() {
        return comentario;
    }

    /**
     * Setter for property comentario.
     * @param comentario New value of property comentario.
     */
    public void setComentario(java.lang.String comentario) {
        this.comentario = comentario;
    }

    /**
     * Getter for property errorDifCambio.
     * @return Value of property errorDifCambio.
     */
    public boolean isErrorDifCambio() {
        return errorDifCambio;
    }

    /**
     * Setter for property errorDifCambio.
     * @param errorDifCambio New value of property errorDifCambio.
     */
    public void setErrorDifCambio(boolean errorDifCambio) {
        this.errorDifCambio = errorDifCambio;
    }

    /**
     * Getter for property nombre_tercero.
     * @return Value of property nombre_tercero.
     */
    public java.lang.String getNombre_tercero() {
        return nombre_tercero;
    }

    /**
     * Setter for property nombre_tercero.
     * @param nombre_tercero New value of property nombre_tercero.
     */
    public void setNombre_tercero(java.lang.String nombre_tercero) {
        this.nombre_tercero = nombre_tercero;
    }

    /**
     * Getter for property ABC.
     * @return Value of property ABC.
     */
    public java.lang.String getABC() {
        return ABC;
    }

    /**
     * Setter for property ABC.
     * @param ABC New value of property ABC.
     */
    public void setABC(java.lang.String ABC) {
        this.ABC = ABC;
    }

    /**
     * Getter for property sum_credito.
     * @return Value of property sum_credito.
     */
    public double getSum_credito() {
        return sum_credito;
    }

    /**
     * Setter for property sum_credito.
     * @param sum_credito New value of property sum_credito.
     */
    public void setSum_credito(double sum_credito) {
        this.sum_credito = sum_credito;
    }

    /**
     * Getter for property sum_debito.
     * @return Value of property sum_debito.
     */
    public double getSum_debito() {
        return sum_debito;
    }

    /**
     * Setter for property sum_debito.
     * @param sum_debito New value of property sum_debito.
     */
    public void setSum_debito(double sum_debito) {
        this.sum_debito = sum_debito;
    }

    /**
     * Getter for property ref_1.
     * @return Value of property ref_1.
     */
    public java.lang.String getRef_1() {
        return ref_1;
    }

    /**
     * Setter for property ref_1.
     * @param ref_1 New value of property ref_1.
     */
    public void setRef_1(java.lang.String ref_1) {
        this.ref_1 = ref_1;
    }

    /**
     * Getter for property ref_2.
     * @return Value of property ref_2.
     */
    public java.lang.String getRef_2() {
        return ref_2;
    }

    /**
     * Setter for property ref_2.
     * @param ref_2 New value of property ref_2.
     */
    public void setRef_2(java.lang.String ref_2) {
        this.ref_2 = ref_2;
    }

    /**
     * Getter for property items.
     * @return Value of property items.
     */
    public java.util.List getItems() {
        return items;
    }

    /**
     * Setter for property items.
     * @param items New value of property items.
     */
    public void setItems(java.util.List items) {
        this.items = items;
    }

    /**
     * Getter for property tipo.
     * @return Value of property tipo.
     */
    public java.lang.String getTipo() {
        return tipo;
    }

    /**
     * Setter for property tipo.
     * @param tipo New value of property tipo.
     */
    public void setTipo(java.lang.String tipo) {
        this.tipo = tipo;
    }

    /**
     * Getter for property origen.
     * @return Value of property origen.
     */
    public java.lang.String getOrigen() {
        return origen;
    }

    /**
     * Setter for property origen.
     * @param origen New value of property origen.
     */
    public void setOrigen(java.lang.String origen) {
        this.origen = origen;
    }

    /**
     * Getter for property abc.
     * @return Value of property abc.
     */
    public java.lang.String getAbc() {
        return abc;
    }

    /**
     * Setter for property abc.
     * @param abc New value of property abc.
     */
    public void setAbc(java.lang.String abc) {
        this.abc = abc;
    }

    /**
     * Getter for property tdoc_rel.
     * @return Value of property tdoc_rel.
     */
    public java.lang.String getTdoc_rel() {
        return tdoc_rel;
    }

    /**
     * Setter for property tdoc_rel.
     * @param tdoc_rel New value of property tdoc_rel.
     */
    public void setTdoc_rel(java.lang.String tdoc_rel) {
        this.tdoc_rel = tdoc_rel;
    }

    /**
     * Getter for property docrelacionado.
     * @return Value of property docrelacionado.
     */
    public java.lang.String getDocrelacionado() {
        return docrelacionado;
    }

    /**
     * Setter for property docrelacionado.
     * @param docrelacionado New value of property docrelacionado.
     */
    public void setDocrelacionado(java.lang.String docrelacionado) {
        this.docrelacionado = docrelacionado;
    }

    /**
     * Getter for property ref_3.
     * @return Value of property ref_3.
     */
    public java.lang.String getRef_3() {
        return ref_3;
    }

    /**
     * Setter for property ref_3.
     * @param ref_3 New value of property ref_3.
     */
    public void setRef_3(java.lang.String ref_3) {
        this.ref_3 = ref_3;
    }

    /**
     * Getter for property ref_4.
     * @return Value of property ref_4.
     */
    public double getRef_4() {
        return ref_4;
    }

    /**
     * Setter for property ref_4.
     * @param ref_4 New value of property ref_4.
     */
    public void setRef_4(double ref_4) {
        this.ref_4 = ref_4;
    }

    /**
     * Getter for property ref_5.
     * @return Value of property ref_5.
     */
    public int getRef_5() {
        return ref_5;
    }

    /**
     * Setter for property ref_5.
     * @param ref_5 New value of property ref_5.
     */
    public void setRef_5(int ref_5) {
        this.ref_5 = ref_5;
    }

    /**
     * Getter for property custodia.
     * @return Value of property custodia.
     */
    public double getCustodia() {
        return custodia;
    }

    /**
     * Setter for property custodia.
     * @param custodia New value of property custodia.
     */
    public void setCustodia(double custodia) {
        this.custodia = custodia;
    }

    /**
     * Getter for property rem.
     * @return Value of property rem.
     */
    public double getRem() {
        return rem;
    }

    /**
     * Setter for property rem.
     * @param rem New value of property rem.
     */
    public void setRem(double rem) {
        this.rem = rem;
    }

    /**
     * Getter for property modrem.
     * @return Value of property modrem.
     */
    public java.lang.String getModrem() {
        return modrem;
    }

    /**
     * Setter for property modrem.
     * @param modrem New value of property modrem.
     */
    public void setModrem(java.lang.String modrem) {
        this.modrem = modrem;
    }

    /**
     * Getter for property tdesc.
     * @return Value of property tdesc.
     */
    public double getTdesc() {
        return tdesc;
    }

    /**
     * Setter for property tdesc.
     * @param tdesc New value of property tdesc.
     */
    public void setTdesc(double tdesc) {
        this.tdesc = tdesc;
    }

    /**
     * Getter for property tpr.
     * @return Value of property tpr.
     */
    public double getTpr() {
        return tpr;
    }

    /**
     * Setter for property tpr.
     * @param tpr New value of property tpr.
     */
    public void setTpr(double tpr) {
        this.tpr = tpr;
    }

    /**
     * Getter for property moneda_foranea.
     * @return Value of property moneda_foranea.
     */
    public java.lang.String getMoneda_foranea() {
        return moneda_foranea;
    }

    /**
     * Setter for property moneda_foranea.
     * @param moneda_foranea New value of property moneda_foranea.
     */
    public void setMoneda_foranea(java.lang.String moneda_foranea) {
        this.moneda_foranea = moneda_foranea;
    }

    /**
     * Getter for property vlr_item_for.
     * @return Value of property vlr_item_for.
     */
    public double getVlr_item_for() {
        return vlr_item_for;
    }

    /**
     * Setter for property vlr_item_for.
     * @param vlr_item_for New value of property vlr_item_for.
     */
    public void setVlr_item_for(double vlr_item_for) {
        this.vlr_item_for = vlr_item_for;
    }

      /**
     * Get the value of tipoNegocio
     *
     * @return the value of tipoNegocio
     */
    public String getTipoNegocio() {
        return tipoNegocio;
    }

    /**
     * Set the value of tipoNegocio
     *
     * @param tipoNegocio new value of tipoNegocio
     */
    public void setTipoNegocio(String tipoNegocio) {
        this.tipoNegocio = tipoNegocio;
    }


    /**
     * Get the value of id_remesa
     *
     * @return the value of id_remesa
     */
    public int getId_remesa() {
        return id_remesa;
    }

    /**
     * Set the value of id_remesa
     *
     * @param id_remesa new value of id_remesa
     */
    public void setId_remesa(int id_remesa) {
        this.id_remesa = id_remesa;
    }

    /**
     * Get the value of id_convenio
     *
     * @return the value of id_convenio
     */
    public int getId_convenio() {
        return id_convenio;
    }

    /**
     * Set the value of id_convenio
     *
     * @param id_convenio new value of id_convenio
     */
    public void setId_convenio(int id_convenio) {
        this.id_convenio = id_convenio;
    }


     public boolean isRef_6() {
        return ref_6;
    }

    public void setRef_6(boolean ref_6) {
        this.ref_6 = ref_6;
    }

    /**
     * @return the cuota_administracion
     */
    public double getCuota_administracion() {
        return cuota_administracion;
    }

    /**
     * @param cuota_administracion the cuota_administracion to set
     */
    public void setCuota_administracion(double cuota_administracion) {
        this.cuota_administracion = cuota_administracion;
    }

    /**
     * @return the aplica_iva
     */
    public String getAplica_iva() {
        return aplica_iva;
    }

    /**
     * @param aplica_iva the aplica_iva to set
     */
    public void setAplica_iva(String aplica_iva) {
        this.aplica_iva = aplica_iva;
    }

    /**
     * @return the valor_cat
     */
    public double getValor_cat() {
        return valor_cat;
    }

    /**
     * @param valor_cat the valor_cat to set
     */
    public void setValor_cat(double valor_cat) {
        this.valor_cat = valor_cat;
    }

    /**
     * @return the valor_int_coriente
     */
    public double getValor_int_coriente() {
        return valor_int_coriente;
    }

    /**
     * @param valor_int_coriente the valor_int_coriente to set
     */
    public void setValor_int_coriente(double valor_int_coriente) {
        this.valor_int_coriente = valor_int_coriente;
    }

    public double getSeguro_deudor() {
        return seguro_deudor;
    }

    public void setSeguro_deudor(double seguro_deudor) {
        this.seguro_deudor = seguro_deudor;
    }
    
    
}
