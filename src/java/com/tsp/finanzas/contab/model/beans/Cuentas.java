/********************************************************************
 *  Nombre Clase.................   Cuentas.java
 *  Descripci�n..................   Bean de la tabla Cuentas
 *  Autor........................   Ing. Leonardo Parody Ponce
 *  Fecha........................   26.12.2005
 *  Versi�n......................   1.0
 *  Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/
package com.tsp.finanzas.contab.model.beans;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.util.*;

/** Creates a new instance of cuentas */
public class Cuentas {
        private String cuenta;
        private String nombre_corto;
        private String nombre_largo;
        private String observacion;
        private String fin_de_periodo;
        private String auxiliar;
        private String activa;
        private String modulo1;
        private String modulo2;
        private String modulo3;
        private String modulo4;
        private String modulo5;
        private String modulo6;
        private String modulo7;
        private String modulo8;
        private String modulo9;
        private String modulo10;
        private String last_update;
        private String user_update;
        private String creation_date;
        private String creation_user;
        private String reg_status;
        private String dstrct;
        private String base;
        
        /**
         * Obtiene el valor de cada uno de los campos del <code>ResultSet</code>
         * @autor Ing. Leonardo Parody
         * @param rs <code>ResultSet</code> de donde se obtienen los campos
         * @throws SQLException
         * @version 1.0
         */
        
        
        public static Cuentas Load(ResultSet rs) throws SQLException{
                
                Cuentas cuenta = new Cuentas();
                cuenta.setObservacion(rs.getString("nombre_observacion"));
                cuenta.setCuenta(rs.getString("cuenta"));
                cuenta.setNombre_corto(rs.getString("nombre_corto"));
                cuenta.setNombre_largo(rs.getString("nombre_largo"));
                cuenta.setFin_de_periodo(rs.getString("fin_periodo"));
                cuenta.setAuxiliar(rs.getString("auxiliar"));
                cuenta.setActiva(rs.getString("activa"));
                cuenta.setModulo1(rs.getString("modulo1"));
                cuenta.setModulo2(rs.getString("modulo2"));
                cuenta.setModulo3(rs.getString("modulo3"));
                cuenta.setModulo4(rs.getString("modulo4"));
                cuenta.setModulo5(rs.getString("modulo5"));
                cuenta.setModulo6(rs.getString("modulo6"));
                cuenta.setModulo7(rs.getString("modulo7"));
                cuenta.setModulo8(rs.getString("modulo8"));
                cuenta.setModulo9(rs.getString("modulo9"));
                cuenta.setModulo10(rs.getString("modulo10"));
                cuenta.setLast_update(rs.getString("last_update"));
                cuenta.setReg_status(rs.getString("reg_status"));
                cuenta.setBase(rs.getString("base"));
                cuenta.setDstrct(rs.getString("dstrct"));
                cuenta.setCreation_date(rs.getString("creation_date"));
                cuenta.setCreation_user(rs.getString("creation_user"));
                cuenta.setUser_update(rs.getString("user_update"));
                return cuenta;
                
        }
        
        /**
         * Getter for property agencia.
         * @return Value of property cuenta.
         */
        public java.lang.String getCuenta() {
                return cuenta;
        }
        
        /**
         * Setter for property agencia.
         * @param agencia New value of property cuenta.
         */
        public void setCuenta(java.lang.String Cuenta) {
                this.cuenta = Cuenta;
        }
        
        /**
         * Getter for property base.
         * @return Value of property base.
         */
        public java.lang.String getBase() {
                return base;
        }
        
        /**
         * Setter for property base.
         * @param base New value of property base.
         */
        public void setBase(java.lang.String base) {
                this.base = base;
        }
        
        /**
         * Getter for property creation_date.
         * @return Value of property creation_date.
         */
        public java.lang.String getCreation_date() {
                return creation_date;
        }
        
        /**
         * Setter for property creation_date.
         * @param creation_date New value of property creation_date.
         */
        public void setCreation_date(java.lang.String creation_date) {
                this.creation_date = creation_date;
        }
        
        /**
         * Getter for property creation_user.
         * @return Value of property creation_user.
         */
        public java.lang.String getCreation_user() {
                return creation_user;
        }
        
        /**
         * Setter for property creation_user.
         * @param creation_user New value of property creation_user.
         */
        public void setCreation_user(java.lang.String creation_user) {
                this.creation_user = creation_user;
        }
        
        /**
         * Getter for property dstrct.
         * @return Value of property dstrct.
         */
        public java.lang.String getDstrct() {
                return dstrct;
        }
        
        /**
         * Setter for property dstrct.
         * @param dstrct New value of property dstrct.
         */
        public void setDstrct(java.lang.String dstrct) {
                this.dstrct = dstrct;
        }
        
        
        
        /**
         * Getter for property nombre_corto.
         * @return Value of property nombre_corto.
         */
        public java.lang.String getNombre_corto() {
                return nombre_corto;
        }
        
        /**
         * Setter for property nombre_corto.
         * @param fec_utilizacion New value of property nombre_corto.
         */
        public void setNombre_corto(java.lang.String Nombre_corto) {
                this.nombre_corto = Nombre_corto;
        }
        /**
         * Getter for property nombre_largo.
         * @return Value of property nombre_largo.
         */
        public java.lang.String getNombre_largo() {
                return nombre_largo;
        }
        
        /**
         * Setter for property nombre_largo.
         * @param fec_utilizacion New value of property nombre_largo.
         */
        public void setNombre_largo(java.lang.String Nombre_largo) {
                this.nombre_largo = Nombre_largo;
        }
        
        /**
         * Getter for property last_update.
         * @return Value of property last_update.
         */
        public java.lang.String getLast_update() {
                return last_update;
        }
        
        /**
         * Setter for property last_update.
         * @param last_update New value of property last_update.
         */
        public void setLast_update(java.lang.String last_update) {
                this.last_update = last_update;
        }
        
        /**
         * Getter for property observacion.
         * @return Value of property observacion.
         */
        public java.lang.String getObservacion() {
                return observacion;
        }
        
        /**
         * Setter for property observacion.
         * @param numpla New value of property observacion.
         */
        public void setObservacion(java.lang.String Observacion) {
                this.observacion = Observacion;
        }
        
        /**
         * Getter for property fin_de_periodo.
         * @return Value of property fin_de_periodo.
         */
        public java.lang.String getFin_de_periodo() {
                return fin_de_periodo;
        }
        
        /**
         * Setter for property fin_de_periodo.
         * @param precinto New value of property fin_de_periodo.
         */
        public void setFin_de_periodo(java.lang.String Fin_de_periodo) {
                this.fin_de_periodo = Fin_de_periodo;
        }
        
        /**
         * Getter for property auxiliar.
         * @return Value of property auxiliar.
         */
        public java.lang.String getAuxiliar() {
                return auxiliar;
        }
        
        /**
         * Setter for property auxiliar.
         * @param precinto New value of property auxiliar.
         */
        public void setAuxiliar(java.lang.String Auxiliar) {
                this.auxiliar = Auxiliar;
        }
        /**
         * Getter for property activa.
         * @return Value of property activa.
         */
        public java.lang.String getActiva() {
                return activa;
        }
        
        /**
         * Setter for property activa.
         * @param precinto New value of property activa.
         */
        public void setActiva(java.lang.String Activa) {
                this.activa = Activa;
        }
        /**
         * Getter for property modulo1.
         * @return Value of property modulo1.
         */
        public java.lang.String getModulo1() {
                return modulo1;
        }
        
        /**
         * Setter for property modulo1.
         * @param precinto New value of property modulo1.
         */
        public void setModulo1(java.lang.String Modulo1) {
                this.modulo1 = Modulo1;
        }
        
        /**
         * Getter for property modulo2.
         * @return Value of property modulo2.
         */
        public java.lang.String getModulo2() {
                return modulo2;
        }
        
        /**
         * Setter for property modulo2.
         * @param precinto New value of property modulo2.
         */
        public void setModulo2(java.lang.String Modulo2) {
                this.modulo2 = Modulo2;
        }
        /**
         * Getter for property modulo3.
         * @return Value of property modulo3.
         */
        public java.lang.String getModulo3() {
                return modulo3;
        }
        
        /**
         * Setter for property modulo3.
         * @param precinto New value of property modulo3.
         */
        public void setModulo3(java.lang.String Modulo3) {
                this.modulo3 = Modulo3;
        }
        /**
         * Getter for property modulo4.
         * @return Value of property modulo4.
         */
        public java.lang.String getModulo4() {
                return modulo4;
        }
        
        /**
         * Setter for property modulo4.
         * @param precinto New value of property modulo4.
         */
        public void setModulo4(java.lang.String Modulo4) {
                this.modulo4 = Modulo4;
        }
        /**
         * Getter for property modulo5.
         * @return Value of property modulo5.
         */
        public java.lang.String getModulo5() {
                return modulo5;
        }
        
        /**
         * Setter for property modulo5.
         * @param precinto New value of property modulo5.
         */
        public void setModulo5(java.lang.String Modulo5) {
                this.modulo5 = Modulo5;
        }
        /**
         * Getter for property modulo6.
         * @return Value of property modulo6.
         */
        public java.lang.String getModulo6() {
                return modulo6;
        }
        
        /**
         * Setter for property modulo6.
         * @param precinto New value of property modulo6.
         */
        public void setModulo6(java.lang.String Modulo6) {
                this.modulo6 = Modulo6;
        }
        /**
         * Getter for property modulo7.
         * @return Value of property modulo7.
         */
        public java.lang.String getModulo7() {
                return modulo7;
        }
        
        /**
         * Setter for property modulo7.
         * @param precinto New value of property modulo7.
         */
        public void setModulo7(java.lang.String Modulo7) {
                this.modulo7 = Modulo7;
        }
        /**
         * Getter for property modulo8.
         * @return Value of property modulo8.
         */
        public java.lang.String getModulo8() {
                return modulo8;
        }
        
        /**
         * Setter for property modulo8.
         * @param precinto New value of property modulo8.
         */
        public void setModulo8(java.lang.String Modulo8) {
                this.modulo8 = Modulo8;
        }
        /**
         * Getter for property modulo9.
         * @return Value of property modulo9.
         */
        public java.lang.String getModulo9() {
                return modulo9;
        }
        
        /**
         * Setter for property modulo9.
         * @param precinto New value of property modulo9.
         */
        public void setModulo9(java.lang.String Modulo9) {
                this.modulo9 = Modulo9;
        }
        /**
         * Getter for property modulo10.
         * @return Value of property modulo10.
         */
        public java.lang.String getModulo10() {
                return modulo10;
        }
        
        /**
         * Setter for property modulo10.
         * @param precinto New value of property modulo10.
         */
        public void setModulo10(java.lang.String Modulo10) {
                this.modulo10 = Modulo10;
        }
        /**
         * Getter for property reg_status.
         * @return Value of property reg_status.
         */
        public java.lang.String getReg_status() {
                return reg_status;
        }
        
        /**
         * Setter for property reg_status.
         * @param reg_status New value of property reg_status.
         */
        public void setReg_status(java.lang.String reg_status) {
                this.reg_status = reg_status;
        }
        
        /**
         * Getter for property user_update.
         * @return Value of property user_update.
         */
        public java.lang.String getUser_update() {
                return user_update;
        }
        
        /**
         * Setter for property user_update.
         * @param user_update New value of property user_update.
         */
        public void setUser_update(java.lang.String user_update) {
                this.user_update = user_update;
        }
        
        
        
        
}
