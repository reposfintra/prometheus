/******************************************************************************** 
 * Nombre clase :                   ReporteFacturasProveedor.java               *
 * Descripcion :                    Estructura del Reporte Facturas Proveedor   *
 * Autor :                          LREALES                                     *
 * Fecha :                          24 de junio de 2006, 08:38 AM               *
 * Version :                        1.0                                         *
 * Copyright :                      Fintravalores S.A.                     *
 ********************************************************************************/

package com.tsp.finanzas.contab.model.beans;

import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.sql.*;

public class ReporteFacturasProveedor {
    
    // 1. Default
    private String reg_status = "";
    private String dstrct = "";
    private String last_update = "";
    private String user_update = "";
    private String creation_date = "";
    private String creation_user = "";
    private String base = "";
    
    // 2. Datos Basicos    
    private String fecha_documento = "";
    private String proveedor = "";
    private String nom_proveedor = "";
    private String gran_contribuyente = "";
    private String autoret_rfte = "";
    private String autoret_iva = "";
    private String autoret_ica = "";
    private String clasificacion = "";
    private String documento = "";
    private String item = "";
    private String codigo_cuenta = "";
    private String vlr = "";
    private String cod_impuesto = "";
    private String porcent_impuesto = "";
    private String vlr_total_impuesto = "";
    
    public static ReporteFacturasProveedor load ( ResultSet rs ) throws SQLException {
    
        ReporteFacturasProveedor reporte = new ReporteFacturasProveedor();
        
        reporte.setFecha_documento( rs.getString("fecha_documento") );
        reporte.setProveedor( rs.getString("proveedor") );
        reporte.setNom_proveedor( rs.getString("nom_proveedor") );
        reporte.setGran_contribuyente( rs.getString("gran_contribuyente") );
        reporte.setAutoret_rfte( rs.getString("autoret_rfte") );
        reporte.setAutoret_iva( rs.getString("autoret_iva") );
        reporte.setAutoret_ica( rs.getString("autoret_ica") );
        reporte.setClasificacion( rs.getString("clasificacion") );
        reporte.setDocumento( rs.getString("documento") );
        reporte.setItem( rs.getString("item") );
        reporte.setCodigo_cuenta( rs.getString("codigo_cuenta") );
        reporte.setVlr( rs.getString("vlr") );
        reporte.setCod_impuesto( rs.getString("cod_impuesto") );
        reporte.setPorcent_impuesto( rs.getString("porcent_impuesto") );
        reporte.setVlr_total_impuesto( rs.getString("vlr_total_impuesto") );
        
        //reporte.setReg_status( rs.getString("reg_status") );
        //reporte.setDstrct( rs.getString("dstrct") );
        //reporte.setLast_update( rs.getString("last_update") );
        //reporte.setUser_update( rs.getString("user_update") );
        //reporte.setCreation_date( rs.getString("creation_date") );
        reporte.setCreation_user( rs.getString("creation_user") );
        //reporte.setBase( rs.getString("base") );        
                
        return reporte;
    
    }
      
    /**
     * 1. Default:
     * Almacena datos de default del Reporte Facturas Proveedor
     * @autor........  LREALES
     * @date.........  24 de junio de 2006
     * @version .....  1.0      
     */ 
    
    /**
     * Getter for property reg_status.
     * @return Value of property reg_status.
     */
    public java.lang.String getReg_status() {
        return reg_status;
    }
    
    /**
     * Setter for property reg_status.
     * @param reg_status New value of property reg_status.
     */
    public void setReg_status(java.lang.String reg_status) {
        this.reg_status = reg_status;
    }
    
    /**
     * Getter for property dstrct.
     * @return Value of property dstrct.
     */
    public java.lang.String getDstrct() {
        return dstrct;
    }
    
    /**
     * Setter for property dstrct.
     * @param dstrct New value of property dstrct.
     */
    public void setDstrct(java.lang.String dstrct) {
        this.dstrct = dstrct;
    }
    
    /**
     * Getter for property last_update.
     * @return Value of property last_update.
     */
    public java.lang.String getLast_update() {
        return last_update;
    }
    
    /**
     * Setter for property last_update.
     * @param last_update New value of property last_update.
     */
    public void setLast_update(java.lang.String last_update) {
        this.last_update = last_update;
    }
    
    /**
     * Getter for property user_update.
     * @return Value of property user_update.
     */
    public java.lang.String getUser_update() {
        return user_update;
    }
    
    /**
     * Setter for property user_update.
     * @param user_update New value of property user_update.
     */
    public void setUser_update(java.lang.String user_update) {
        this.user_update = user_update;
    }
    
    /**
     * Getter for property creation_date.
     * @return Value of property creation_date.
     */
    public java.lang.String getCreation_date() {
        return creation_date;
    }
    
    /**
     * Setter for property creation_date.
     * @param creation_date New value of property creation_date.
     */
    public void setCreation_date(java.lang.String creation_date) {
        this.creation_date = creation_date;
    }
    
    /**
     * Getter for property creation_user.
     * @return Value of property creation_user.
     */
    public java.lang.String getCreation_user() {
        return creation_user;
    }
    
    /**
     * Setter for property creation_user.
     * @param creation_user New value of property creation_user.
     */
    public void setCreation_user(java.lang.String creation_user) {
        this.creation_user = creation_user;
    }
    
    /**
     * Getter for property base.
     * @return Value of property base.
     */
    public java.lang.String getBase() {
        return base;
    }
    
    /**
     * Setter for property base.
     * @param base New value of property base.
     */
    public void setBase(java.lang.String base) {
        this.base = base;
    }
    
    /**
     * 2. Datos Basicos:
     * Almacena datos basicos del Reporte Facturas Proveedor
     * @autor........  LREALES
     * @date.........  24 de junio de 2006
     * @version .....  1.0      
     */ 
    
    /**
     * Getter for property fecha_documento.
     * @return Value of property fecha_documento.
     */
    public java.lang.String getFecha_documento() {
        return fecha_documento;
    }
    
    /**
     * Setter for property fecha_documento.
     * @param fecha_documento New value of property fecha_documento.
     */
    public void setFecha_documento(java.lang.String fecha_documento) {
        this.fecha_documento = fecha_documento;
    }
    
    /**
     * Getter for property proveedor.
     * @return Value of property proveedor.
     */
    public java.lang.String getProveedor() {
        return proveedor;
    }
    
    /**
     * Setter for property proveedor.
     * @param proveedor New value of property proveedor.
     */
    public void setProveedor(java.lang.String proveedor) {
        this.proveedor = proveedor;
    }
    
    /**
     * Getter for property nom_proveedor.
     * @return Value of property nom_proveedor.
     */
    public java.lang.String getNom_proveedor() {
        return nom_proveedor;
    }
    
    /**
     * Setter for property nom_proveedor.
     * @param nom_proveedor New value of property nom_proveedor.
     */
    public void setNom_proveedor(java.lang.String nom_proveedor) {
        this.nom_proveedor = nom_proveedor;
    }
    
    /**
     * Getter for property gran_contribuyente.
     * @return Value of property gran_contribuyente.
     */
    public java.lang.String getGran_contribuyente() {
        return gran_contribuyente;
    }
    
    /**
     * Setter for property gran_contribuyente.
     * @param gran_contribuyente New value of property gran_contribuyente.
     */
    public void setGran_contribuyente(java.lang.String gran_contribuyente) {
        this.gran_contribuyente = gran_contribuyente;
    }
    
    /**
     * Getter for property autoret_rfte.
     * @return Value of property autoret_rfte.
     */
    public java.lang.String getAutoret_rfte() {
        return autoret_rfte;
    }
    
    /**
     * Setter for property autoret_rfte.
     * @param autoret_rfte New value of property autoret_rfte.
     */
    public void setAutoret_rfte(java.lang.String autoret_rfte) {
        this.autoret_rfte = autoret_rfte;
    }
    
    /**
     * Getter for property autoret_iva.
     * @return Value of property autoret_iva.
     */
    public java.lang.String getAutoret_iva() {
        return autoret_iva;
    }
    
    /**
     * Setter for property autoret_iva.
     * @param autoret_iva New value of property autoret_iva.
     */
    public void setAutoret_iva(java.lang.String autoret_iva) {
        this.autoret_iva = autoret_iva;
    }
    
    /**
     * Getter for property autoret_ica.
     * @return Value of property autoret_ica.
     */
    public java.lang.String getAutoret_ica() {
        return autoret_ica;
    }
    
    /**
     * Setter for property autoret_ica.
     * @param autoret_ica New value of property autoret_ica.
     */
    public void setAutoret_ica(java.lang.String autoret_ica) {
        this.autoret_ica = autoret_ica;
    }
    
    /**
     * Getter for property clasificacion.
     * @return Value of property clasificacion.
     */
    public java.lang.String getClasificacion() {
        return clasificacion;
    }
    
    /**
     * Setter for property clasificacion.
     * @param clasificacion New value of property clasificacion.
     */
    public void setClasificacion(java.lang.String clasificacion) {
        this.clasificacion = clasificacion;
    }
    
    /**
     * Getter for property documento.
     * @return Value of property documento.
     */
    public java.lang.String getDocumento() {
        return documento;
    }
    
    /**
     * Setter for property documento.
     * @param documento New value of property documento.
     */
    public void setDocumento(java.lang.String documento) {
        this.documento = documento;
    }
    
    /**
     * Getter for property item.
     * @return Value of property item.
     */
    public java.lang.String getItem() {
        return item;
    }
    
    /**
     * Setter for property item.
     * @param item New value of property item.
     */
    public void setItem(java.lang.String item) {
        this.item = item;
    }
    
    /**
     * Getter for property codigo_cuenta.
     * @return Value of property codigo_cuenta.
     */
    public java.lang.String getCodigo_cuenta() {
        return codigo_cuenta;
    }
    
    /**
     * Setter for property codigo_cuenta.
     * @param codigo_cuenta New value of property codigo_cuenta.
     */
    public void setCodigo_cuenta(java.lang.String codigo_cuenta) {
        this.codigo_cuenta = codigo_cuenta;
    }
    
    /**
     * Getter for property vlr.
     * @return Value of property vlr.
     */
    public java.lang.String getVlr() {
        return vlr;
    }
    
    /**
     * Setter for property vlr.
     * @param vlr New value of property vlr.
     */
    public void setVlr(java.lang.String vlr) {
        this.vlr = vlr;
    }
    
    /**
     * Getter for property cod_impuesto.
     * @return Value of property cod_impuesto.
     */
    public java.lang.String getCod_impuesto() {
        return cod_impuesto;
    }
    
    /**
     * Setter for property cod_impuesto.
     * @param cod_impuesto New value of property cod_impuesto.
     */
    public void setCod_impuesto(java.lang.String cod_impuesto) {
        this.cod_impuesto = cod_impuesto;
    }
    
    /**
     * Getter for property porcent_impuesto.
     * @return Value of property porcent_impuesto.
     */
    public java.lang.String getPorcent_impuesto() {
        return porcent_impuesto;
    }
    
    /**
     * Setter for property porcent_impuesto.
     * @param porcent_impuesto New value of property porcent_impuesto.
     */
    public void setPorcent_impuesto(java.lang.String porcent_impuesto) {
        this.porcent_impuesto = porcent_impuesto;
    }
    
    /**
     * Getter for property vlr_total_impuesto.
     * @return Value of property vlr_total_impuesto.
     */
    public java.lang.String getVlr_total_impuesto() {
        return vlr_total_impuesto;
    }
    
    /**
     * Setter for property vlr_total_impuesto.
     * @param vlr_total_impuesto New value of property vlr_total_impuesto.
     */
    public void setVlr_total_impuesto(java.lang.String vlr_total_impuesto) {
        this.vlr_total_impuesto = vlr_total_impuesto;
    }
    
} 