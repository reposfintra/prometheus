/******************************************************************************** 
 * Nombre clase :                   PlanDeCuentas.java                          *
 * Descripcion :                    Estructura del Plan De Cuentas              *
 * Autor :                          LREALES                                     *
 * Fecha :                          5 de junio de 2006, 04:59 PM                *
 * Version :                        1.0                                         *
 * Copyright :                      Fintravalores S.A.                     *
 ********************************************************************************/

package com.tsp.finanzas.contab.model.beans;

import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.sql.*;

public class PlanDeCuentas {
    
    // 1. Datos Basicos    
    private String reg_status = "";
    private String dstrct = "";
    private String cuenta = "";
    private String nombre_largo = "";
    private String nombre_corto = "";
    private String nombre_observacion = "";
    private String fin_periodo = "";
    private String auxiliar = "";
    private String activa = "";
    private String base = "";
    private String modulo1 = "";
    private String modulo2 = "";
    private String modulo3 = "";
    private String modulo4 = "";
    private String modulo5 = "";
    private String modulo6 = "";
    private String modulo7 = "";
    private String modulo8 = "";
    private String modulo9 = "";
    private String modulo10 = "";
    private String cta_dependiente = "";
    private String nivel = "";
    private String cta_cierre = "";
    private String subledger = "";
    private String tercero = "";
    private String last_update = "";
    private String user_update = "";
    private String creation_date = "";
    private String creation_user = "";
    
    private String detalle = "";
    
    private String perteneceAmodulo = "";
    
    
    
    public static PlanDeCuentas load ( ResultSet rs ) throws SQLException {
    
        PlanDeCuentas cuentas = new PlanDeCuentas();
        
        cuentas.setReg_status( rs.getString("reg_status") );
        cuentas.setDstrct( rs.getString("dstrct") );
        cuentas.setCuenta( rs.getString("cuenta") );
        cuentas.setNombre_largo( rs.getString("nombre_largo") );
        cuentas.setNombre_corto( rs.getString("nombre_corto") );
        cuentas.setNombre_observacion( rs.getString("nombre_observacion") );
        cuentas.setFin_periodo( rs.getString("fin_periodo") );
        cuentas.setAuxiliar( rs.getString("auxiliar") );
        cuentas.setActiva( rs.getString("activa") );
        cuentas.setBase( rs.getString("base") );        
        cuentas.setModulo1( rs.getString("modulo1") );
        cuentas.setModulo2( rs.getString("modulo2") );
        cuentas.setModulo3( rs.getString("modulo3") );
        cuentas.setModulo4( rs.getString("modulo4") );
        cuentas.setModulo5( rs.getString("modulo5") );
        cuentas.setModulo6( rs.getString("modulo6") );
        cuentas.setModulo7( rs.getString("modulo7") );
        cuentas.setModulo8( rs.getString("modulo8") );
        cuentas.setModulo9( rs.getString("modulo9") );
        cuentas.setModulo10( rs.getString("modulo10") );
        cuentas.setCta_dependiente( rs.getString("cta_dependiente") );
        cuentas.setNivel( rs.getString("nivel") );
        cuentas.setCta_cierre( rs.getString("cta_cierre") );
        cuentas.setSubledger( rs.getString("subledger") );
        cuentas.setTercero( rs.getString("tercero") );
        cuentas.setLast_update( rs.getString("last_update") );
        cuentas.setUser_update( rs.getString("user_update") );
        cuentas.setCreation_date( rs.getString("creation_date") );
        cuentas.setCreation_user( rs.getString("creation_user") ); 
        
        cuentas.setDetalle( rs.getString("detalle") );
        
        return cuentas;
    
    }
       
    /**
     * 1. Datos Basicos:
     * Almacena datos basicos del plan de cuentas
     * @autor........  LREALES
     * @date.........  5 de junio de 2006
     * @version .....  1.0      
     */ 
    
    /**
     * Getter for property reg_status.
     * @return Value of property reg_status.
     */
    public java.lang.String getReg_status() {
        return reg_status;
    }
    
    /**
     * Setter for property reg_status.
     * @param reg_status New value of property reg_status.
     */
    public void setReg_status(java.lang.String reg_status) {
        this.reg_status = reg_status;
    }
    
    /**
     * Getter for property dstrct.
     * @return Value of property dstrct.
     */
    public java.lang.String getDstrct() {
        return dstrct;
    }
    
    /**
     * Setter for property dstrct.
     * @param dstrct New value of property dstrct.
     */
    public void setDstrct(java.lang.String dstrct) {
        this.dstrct = dstrct;
    }
    
    /**
     * Getter for property cuenta.
     * @return Value of property cuenta.
     */
    public java.lang.String getCuenta() {
        return cuenta;
    }
    
    /**
     * Setter for property cuenta.
     * @param cuenta New value of property cuenta.
     */
    public void setCuenta(java.lang.String cuenta) {
        this.cuenta = cuenta;
    }
    
    /**
     * Getter for property nombre_largo.
     * @return Value of property nombre_largo.
     */
    public java.lang.String getNombre_largo() {
        return nombre_largo;
    }
    
    /**
     * Setter for property nombre_largo.
     * @param nombre_largo New value of property nombre_largo.
     */
    public void setNombre_largo(java.lang.String nombre_largo) {
        this.nombre_largo = nombre_largo;
    }
    
    /**
     * Getter for property nombre_corto.
     * @return Value of property nombre_corto.
     */
    public java.lang.String getNombre_corto() {
        return nombre_corto;
    }
    
    /**
     * Setter for property nombre_corto.
     * @param nombre_corto New value of property nombre_corto.
     */
    public void setNombre_corto(java.lang.String nombre_corto) {
        this.nombre_corto = nombre_corto;
    }
    
    /**
     * Getter for property nombre_observacion.
     * @return Value of property nombre_observacion.
     */
    public java.lang.String getNombre_observacion() {
        return nombre_observacion;
    }
    
    /**
     * Setter for property nombre_observacion.
     * @param nombre_observacion New value of property nombre_observacion.
     */
    public void setNombre_observacion(java.lang.String nombre_observacion) {
        this.nombre_observacion = nombre_observacion;
    }
    
    /**
     * Getter for property fin_periodo.
     * @return Value of property fin_periodo.
     */
    public java.lang.String getFin_periodo() {
        return fin_periodo;
    }
    
    /**
     * Setter for property fin_periodo.
     * @param fin_periodo New value of property fin_periodo.
     */
    public void setFin_periodo(java.lang.String fin_periodo) {
        this.fin_periodo = fin_periodo;
    }
    
    /**
     * Getter for property auxiliar.
     * @return Value of property auxiliar.
     */
    public java.lang.String getAuxiliar() {
        return auxiliar;
    }
    
    /**
     * Setter for property auxiliar.
     * @param auxiliar New value of property auxiliar.
     */
    public void setAuxiliar(java.lang.String auxiliar) {
        this.auxiliar = auxiliar;
    }
    
    /**
     * Getter for property activa.
     * @return Value of property activa.
     */
    public java.lang.String getActiva() {
        return activa;
    }
    
    /**
     * Setter for property activa.
     * @param activa New value of property activa.
     */
    public void setActiva(java.lang.String activa) {
        this.activa = activa;
    }
    
    /**
     * Getter for property base.
     * @return Value of property base.
     */
    public java.lang.String getBase() {
        return base;
    }
    
    /**
     * Setter for property base.
     * @param base New value of property base.
     */
    public void setBase(java.lang.String base) {
        this.base = base;
    }
    
    /**
     * Getter for property modulo1.
     * @return Value of property modulo1.
     */
    public java.lang.String getModulo1() {
        return modulo1;
    }
    
    /**
     * Setter for property modulo1.
     * @param modulo1 New value of property modulo1.
     */
    public void setModulo1(java.lang.String modulo1) {
        this.modulo1 = modulo1;
    }
    
    /**
     * Getter for property modulo2.
     * @return Value of property modulo2.
     */
    public java.lang.String getModulo2() {
        return modulo2;
    }
    
    /**
     * Setter for property modulo2.
     * @param modulo2 New value of property modulo2.
     */
    public void setModulo2(java.lang.String modulo2) {
        this.modulo2 = modulo2;
    }
    
    /**
     * Getter for property modulo3.
     * @return Value of property modulo3.
     */
    public java.lang.String getModulo3() {
        return modulo3;
    }
    
    /**
     * Setter for property modulo3.
     * @param modulo3 New value of property modulo3.
     */
    public void setModulo3(java.lang.String modulo3) {
        this.modulo3 = modulo3;
    }
    
    /**
     * Getter for property modulo4.
     * @return Value of property modulo4.
     */
    public java.lang.String getModulo4() {
        return modulo4;
    }
    
    /**
     * Setter for property modulo4.
     * @param modulo4 New value of property modulo4.
     */
    public void setModulo4(java.lang.String modulo4) {
        this.modulo4 = modulo4;
    }
    
    /**
     * Getter for property modulo5.
     * @return Value of property modulo5.
     */
    public java.lang.String getModulo5() {
        return modulo5;
    }
    
    /**
     * Setter for property modulo5.
     * @param modulo5 New value of property modulo5.
     */
    public void setModulo5(java.lang.String modulo5) {
        this.modulo5 = modulo5;
    }
    
    /**
     * Getter for property modulo6.
     * @return Value of property modulo6.
     */
    public java.lang.String getModulo6() {
        return modulo6;
    }
    
    /**
     * Setter for property modulo6.
     * @param modulo6 New value of property modulo6.
     */
    public void setModulo6(java.lang.String modulo6) {
        this.modulo6 = modulo6;
    }
    
    /**
     * Getter for property modulo7.
     * @return Value of property modulo7.
     */
    public java.lang.String getModulo7() {
        return modulo7;
    }
    
    /**
     * Setter for property modulo7.
     * @param modulo7 New value of property modulo7.
     */
    public void setModulo7(java.lang.String modulo7) {
        this.modulo7 = modulo7;
    }
    
    /**
     * Getter for property modulo8.
     * @return Value of property modulo8.
     */
    public java.lang.String getModulo8() {
        return modulo8;
    }
    
    /**
     * Setter for property modulo8.
     * @param modulo8 New value of property modulo8.
     */
    public void setModulo8(java.lang.String modulo8) {
        this.modulo8 = modulo8;
    }
    
    /**
     * Getter for property modulo9.
     * @return Value of property modulo9.
     */
    public java.lang.String getModulo9() {
        return modulo9;
    }
    
    /**
     * Setter for property modulo9.
     * @param modulo9 New value of property modulo9.
     */
    public void setModulo9(java.lang.String modulo9) {
        this.modulo9 = modulo9;
    }
    
    /**
     * Getter for property modulo10.
     * @return Value of property modulo10.
     */
    public java.lang.String getModulo10() {
        return modulo10;
    }
    
    /**
     * Setter for property modulo10.
     * @param modulo10 New value of property modulo10.
     */
    public void setModulo10(java.lang.String modulo10) {
        this.modulo10 = modulo10;
    }
    
    /**
     * Getter for property cta_dependiente.
     * @return Value of property cta_dependiente.
     */
    public java.lang.String getCta_dependiente() {
        return cta_dependiente;
    }
    
    /**
     * Setter for property cta_dependiente.
     * @param cta_dependiente New value of property cta_dependiente.
     */
    public void setCta_dependiente(java.lang.String cta_dependiente) {
        this.cta_dependiente = cta_dependiente;
    }
    
    /**
     * Getter for property nivel.
     * @return Value of property nivel.
     */
    public java.lang.String getNivel() {
        return nivel;
    }
    
    /**
     * Setter for property nivel.
     * @param nivel New value of property nivel.
     */
    public void setNivel(java.lang.String nivel) {
        this.nivel = nivel;
    }
    
    /**
     * Getter for property cta_cierre.
     * @return Value of property cta_cierre.
     */
    public java.lang.String getCta_cierre() {
        return cta_cierre;
    }
    
    /**
     * Setter for property cta_cierre.
     * @param cta_cierre New value of property cta_cierre.
     */
    public void setCta_cierre(java.lang.String cta_cierre) {
        this.cta_cierre = cta_cierre;
    }
    
    /**
     * Getter for property subledger.
     * @return Value of property subledger.
     */
    public java.lang.String getSubledger() {
        return subledger;
    }
    
    /**
     * Setter for property subledger.
     * @param subledger New value of property subledger.
     */
    public void setSubledger(java.lang.String subledger) {
        this.subledger = subledger;
    }
    
    /**
     * Getter for property tercero.
     * @return Value of property tercero.
     */
    public java.lang.String getTercero() {
        return tercero;
    }
    
    /**
     * Setter for property tercero.
     * @param tercero New value of property tercero.
     */
    public void setTercero(java.lang.String tercero) {
        this.tercero = tercero;
    }
    
    /**
     * Getter for property last_update.
     * @return Value of property last_update.
     */
    public java.lang.String getLast_update() {
        return last_update;
    }
    
    /**
     * Setter for property last_update.
     * @param last_update New value of property last_update.
     */
    public void setLast_update(java.lang.String last_update) {
        this.last_update = last_update;
    }
    
    /**
     * Getter for property user_update.
     * @return Value of property user_update.
     */
    public java.lang.String getUser_update() {
        return user_update;
    }
    
    /**
     * Setter for property user_update.
     * @param user_update New value of property user_update.
     */
    public void setUser_update(java.lang.String user_update) {
        this.user_update = user_update;
    }
    
    /**
     * Getter for property creation_date.
     * @return Value of property creation_date.
     */
    public java.lang.String getCreation_date() {
        return creation_date;
    }
    
    /**
     * Setter for property creation_date.
     * @param creation_date New value of property creation_date.
     */
    public void setCreation_date(java.lang.String creation_date) {
        this.creation_date = creation_date;
    }
    
    /**
     * Getter for property creation_user.
     * @return Value of property creation_user.
     */ 
    public java.lang.String getCreation_user() {
        return creation_user;
    }
    
    /**
     * Setter for property creation_user.
     * @param creation_user New value of property creation_user.
     */
    public void setCreation_user(java.lang.String creation_user) {
        this.creation_user = creation_user;
    }
        
    /**
     * Getter for property detalle.
     * @return Value of property detalle.
     */
    public java.lang.String getDetalle() {
        return detalle;
    }
    
    /**
     * Setter for property detalle.
     * @param detalle New value of property detalle.
     */
    public void setDetalle(java.lang.String detalle) {
        this.detalle = detalle;
    }
    
    /**
     * Getter for property perteneceAmodulo.
     * @return Value of property perteneceAmodulo.
     */
    public java.lang.String getPerteneceAmodulo() {
        return perteneceAmodulo;
    }
    
    /**
     * Setter for property perteneceAmodulo.
     * @param perteneceAmodulo New value of property perteneceAmodulo.
     */
    public void setPerteneceAmodulo(java.lang.String perteneceAmodulo) {
        this.perteneceAmodulo = perteneceAmodulo;
    }
    
} 