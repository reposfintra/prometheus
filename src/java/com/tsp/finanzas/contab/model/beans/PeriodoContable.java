/**************************************************************************
 * Nombre: ......................PeriodoContable.java                     *
 * Descripci�n: .................Bean de periodo_contable                 *
 * Autor:........................Osvaldo P�rez Ferrer                     *
 * Fecha:........................16 de Junio de 2006, 06:13 PM            *
 * Versi�n: Java ................1.0                                      *
 * Copyright: Fintravalores S.A. S.A.                                *
 **************************************************************************/


/*
 * PeriodoContable.java
 *
 * Created on 16 de junio de 2006, 06:12 PM
 */

package com.tsp.finanzas.contab.model.beans;

/**
 *
 * @author  Osvaldo
 */
public class PeriodoContable {
    
    private String distrito;
    private String anio;
    private String mes;
    private String ac;
    private String iva;
    private String fec_pre_iva;
    private String retencion;
    private String fec_pre_retencion;
    private String comercio;
    private String fec_pre_comercio;
    private String renta; 
    private String fec_pre_renta;
    private String creation_user;
    private String base;
    private String reg_status;
    private String user_update;
     //lreales
    private String last_update;
    private String creation_date;
    
    
    /** Creates a new instance of PeriodoContable */
    public PeriodoContable() {
    }
    
    /**
     * Getter for property anio.
     * @return Value of property anio.
     */
    public java.lang.String getAnio() {
        return anio;
    }
    
    /**
     * Setter for property anio.
     * @param anio New value of property anio.
     */
    public void setAnio(java.lang.String anio) {
        this.anio = anio;
    }
    
    /**
     * Getter for property mes.
     * @return Value of property mes.
     */
    public java.lang.String getMes() {
        return mes;
    }
    
    /**
     * Setter for property mes.
     * @param mes New value of property mes.
     */
    public void setMes(java.lang.String mes) {
        this.mes = mes;
    }
    
    /**
     * Getter for property ac.
     * @return Value of property ac.
     */
    public java.lang.String getAc() {
        return ac;
    }
    
    /**
     * Setter for property ac.
     * @param ac New value of property ac.
     */
    public void setAc(java.lang.String ac) {
        this.ac = ac;
    }
    
    /**
     * Getter for property iva.
     * @return Value of property iva.
     */
    public java.lang.String getIva() {
        return iva;
    }
    
    /**
     * Setter for property iva.
     * @param iva New value of property iva.
     */
    public void setIva(java.lang.String iva) {
        this.iva = iva;
    }
    
    /**
     * Getter for property fec_pre_iva.
     * @return Value of property fec_pre_iva.
     */
    public java.lang.String getFec_pre_iva() {
        return fec_pre_iva;
    }
    
    /**
     * Setter for property fec_pre_iva.
     * @param fec_pre_iva New value of property fec_pre_iva.
     */
    public void setFec_pre_iva(java.lang.String fec_pre_iva) {
        this.fec_pre_iva = fec_pre_iva;
    }
    
    /**
     * Getter for property retencion.
     * @return Value of property retencion.
     */
    public java.lang.String getRetencion() {
        return retencion;
    }
    
    /**
     * Setter for property retencion.
     * @param retencion New value of property retencion.
     */
    public void setRetencion(java.lang.String retencion) {
        this.retencion = retencion;
    }
    
    /**
     * Getter for property fec_pre_retencion.
     * @return Value of property fec_pre_retencion.
     */
    public java.lang.String getFec_pre_retencion() {
        return fec_pre_retencion;
    }
    
    /**
     * Setter for property fec_pre_retencion.
     * @param fec_pre_retencion New value of property fec_pre_retencion.
     */
    public void setFec_pre_retencion(java.lang.String fec_pre_retencion) {
        this.fec_pre_retencion = fec_pre_retencion;
    }
    
    /**
     * Getter for property comercio.
     * @return Value of property comercio.
     */
    public java.lang.String getComercio() {
        return comercio;
    }
    
    /**
     * Setter for property comercio.
     * @param comercio New value of property comercio.
     */
    public void setComercio(java.lang.String comercio) {
        this.comercio = comercio;
    }
    
    /**
     * Getter for property fec_pre_comercio.
     * @return Value of property fec_pre_comercio.
     */
    public java.lang.String getFec_pre_comercio() {
        return fec_pre_comercio;
    }
    
    /**
     * Setter for property fec_pre_comercio.
     * @param fec_pre_comercio New value of property fec_pre_comercio.
     */
    public void setFec_pre_comercio(java.lang.String fec_pre_comercio) {
        this.fec_pre_comercio = fec_pre_comercio;
    }
    
    /**
     * Getter for property renta.
     * @return Value of property renta.
     */
    public java.lang.String getRenta() {
        return renta;
    }
    
    /**
     * Setter for property renta.
     * @param renta New value of property renta.
     */
    public void setRenta(java.lang.String renta) {
        this.renta = renta;
    }
    
    /**
     * Getter for property fec_pre_renta.
     * @return Value of property fec_pre_renta.
     */
    public java.lang.String getFec_pre_renta() {
        return fec_pre_renta;
    }
    
    /**
     * Setter for property fec_pre_renta.
     * @param fec_pre_renta New value of property fec_pre_renta.
     */
    public void setFec_pre_renta(java.lang.String fec_pre_renta) {
        this.fec_pre_renta = fec_pre_renta;
    }
    
    /**
     * Getter for property distrito.
     * @return Value of property distrito.
     */
    public java.lang.String getDistrito() {
        return distrito;
    }
    
    /**
     * Setter for property distrito.
     * @param distrito New value of property distrito.
     */
    public void setDistrito(java.lang.String distrito) {
        this.distrito = distrito;
    }
    
    /**
     * Getter for property base.
     * @return Value of property base.
     */
    public java.lang.String getBase() {
        return base;
    }
    
    /**
     * Setter for property base.
     * @param base New value of property base.
     */
    public void setBase(java.lang.String base) {
        this.base = base;
    }
    
    /**
     * Getter for property creation_user.
     * @return Value of property creation_user.
     */
    public java.lang.String getCreation_user() {
        return creation_user;
    }
    
    /**
     * Setter for property creation_user.
     * @param creation_user New value of property creation_user.
     */
    public void setCreation_user(java.lang.String creation_user) {
        this.creation_user = creation_user;
    }
    
    /**
     * Getter for property reg_status.
     * @return Value of property reg_status.
     */
    public java.lang.String getReg_status() {
        return reg_status;
    }
    
    /**
     * Setter for property reg_status.
     * @param reg_status New value of property reg_status.
     */
    public void setReg_status(java.lang.String reg_status) {
        this.reg_status = reg_status;
    }
    
    /**
     * Getter for property user_update.
     * @return Value of property user_update.
     */
    public java.lang.String getUser_update() {
        return user_update;
    }
    
    /**
     * Setter for property user_update.
     * @param user_update New value of property user_update.
     */
    public void setUser_update(java.lang.String user_update) {
        this.user_update = user_update;
    }
    
    /**
     * Getter for property last_update.
     * @return Value of property last_update.
     */
    public java.lang.String getLast_update() {
        return last_update;
    }
    
    /**
     * Setter for property last_update.
     * @param last_update New value of property last_update.
     */
    public void setLast_update(java.lang.String last_update) {
        this.last_update = last_update;
    }
    
    /**
     * Getter for property creation_date.
     * @return Value of property creation_date.
     */
    public java.lang.String getCreation_date() {
        return creation_date;
    }
    
    /**
     * Setter for property creation_date.
     * @param creation_date New value of property creation_date.
     */
    public void setCreation_date(java.lang.String creation_date) {
        this.creation_date = creation_date;
    }
    
}

