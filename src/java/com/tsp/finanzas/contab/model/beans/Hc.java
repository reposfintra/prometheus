/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.finanzas.contab.model.beans;

import java.io.Serializable;
import java.util.Date;


/**
 *
 * @author imorales
 */

public class Hc implements Serializable {
    private static final long serialVersionUID = 1L;
    
    private String tableType;
    private String tableCode;
    private String regStatus;
    private String referencia;
    private String descripcion;
    private Date lastUpdate;
    private String userUpdate;
    private Date creationDate;
    private String creationUser;
    private String dato;

    public Hc() {
    }



    public Hc(String tableType, String tableCode, String regStatus, String referencia, String descripcion, Date lastUpdate, String userUpdate, Date creationDate, String creationUser, String dato) {
        this.tableType = tableType;
         this.tableCode = tableCode;
        this.regStatus = regStatus;
        this.referencia = referencia;
        this.descripcion = descripcion;
        this.lastUpdate = lastUpdate;
        this.userUpdate = userUpdate;
        this.creationDate = creationDate;
        this.creationUser = creationUser;
        this.dato = dato;
    }


        public Hc(String tableCode, String descripcion, String user) {
        this.tableCode = tableCode;
        this.referencia = tableCode;
        this.descripcion = descripcion;
        this.userUpdate = user;
        this.creationUser = user;
    }





    public String getRegStatus() {
        return regStatus;
    }

    public void setRegStatus(String regStatus) {
        this.regStatus = regStatus;
    }

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public String getUserUpdate() {
        return userUpdate;
    }

    public void setUserUpdate(String userUpdate) {
        this.userUpdate = userUpdate;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public String getCreationUser() {
        return creationUser;
    }

    public void setCreationUser(String creationUser) {
        this.creationUser = creationUser;
    }

    public String getDato() {
        return dato;
    }

    public void setDato(String dato) {
        this.dato = dato;
    }

    public String getTableType() {
        return tableType;
    }

    public void setTableType(String tableType) {
        this.tableType = tableType;
    }

    public String getTableCode() {
        return tableCode;
    }

    public void setTableCode(String tableCode) {
        this.tableCode = tableCode;
    }

  

}
