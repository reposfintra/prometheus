/****
 * Nombre:              Subledger.java                           
 * Descripci�n:         Beans de acuerdo especial.               
 * Autor:               Ing. Diogenes Antonio Bastidas Morales 
 * Fecha:               6 de junio de 2006, 02:57 PM
 * Versi�n:             Java 1.0
 * Copyright: Fintravalores S.A. S.A.                                
 ******/

package com.tsp.finanzas.contab.model.beans;
import java.io.*;
import java.sql.*;
import java.util.*;
import java.io.Serializable;


public class Subledger implements Serializable {
    private String dstrct;
    private String reg_status;
    private String cuenta;
    private String tipo_subledger;
    private String id_subledger;
    private String nombre;
    private String creation_user;
    private String creation_date;
    private String user_update;
    private String last_update;
    private String base;
    private String des_tipo;
    
    /** Creates a new instance of Subledger */
    public Subledger() {
    }
    
    /**
     * Getter for property base.
     * @return Value of property base.
     */
    public java.lang.String getBase() {
        return base;
    }
    
    /**
     * Setter for property base.
     * @param base New value of property base.
     */
    public void setBase(java.lang.String base) {
        this.base = base;
    }
    
    
    /**
     * Getter for property creation_date.
     * @return Value of property creation_date.
     */
    public java.lang.String getCreation_date() {
        return creation_date;
    }
    
    /**
     * Setter for property creation_date.
     * @param creation_date New value of property creation_date.
     */
    public void setCreation_date(java.lang.String creation_date) {
        this.creation_date = creation_date;
    }
    
    /**
     * Getter for property creation_user.
     * @return Value of property creation_user.
     */
    public java.lang.String getCreation_user() {
        return creation_user;
    }
    
    /**
     * Setter for property creation_user.
     * @param creation_user New value of property creation_user.
     */
    public void setCreation_user(java.lang.String creation_user) {
        this.creation_user = creation_user;
    }
    
    /**
     * Getter for property cuenta.
     * @return Value of property cuenta.
     */
    public java.lang.String getCuenta() {
        return cuenta;
    }
    
    /**
     * Setter for property cuenta.
     * @param cuenta New value of property cuenta.
     */
    public void setCuenta(java.lang.String cuenta) {
        this.cuenta = cuenta;
    }
    
    /**
     * Getter for property last_update.
     * @return Value of property last_update.
     */
    public java.lang.String getLast_update() {
        return last_update;
    }
    
    /**
     * Setter for property last_update.
     * @param last_update New value of property last_update.
     */
    public void setLast_update(java.lang.String last_update) {
        this.last_update = last_update;
    }
    
    
    
    /**
     * Getter for property nombre.
     * @return Value of property nombre.
     */
    public java.lang.String getNombre() {
        return nombre;
    }
    
    /**
     * Setter for property nombre.
     * @param nombre New value of property nombre.
     */
    public void setNombre(java.lang.String nombre) {
        this.nombre = nombre;
    }
    
    /**
     * Getter for property user_update.
     * @return Value of property user_update.
     */
    public java.lang.String getUser_update() {
        return user_update;
    }
    
    /**
     * Setter for property user_update.
     * @param user_update New value of property user_update.
     */
    public void setUser_update(java.lang.String user_update) {
        this.user_update = user_update;
    }
    
    /**
     * Getter for property reg_status.
     * @return Value of property reg_status.
     */
    public java.lang.String getReg_status() {
        return reg_status;
    }
    
    /**
     * Setter for property reg_status.
     * @param reg_status New value of property reg_status.
     */
    public void setReg_status(java.lang.String reg_status) {
        this.reg_status = reg_status;
    }
    
    /**
     * Getter for property dstrct.
     * @return Value of property dstrct.
     */
    public java.lang.String getDstrct() {
        return dstrct;
    }
    
    /**
     * Setter for property dstrct.
     * @param dstrct New value of property dstrct.
     */
    public void setDstrct(java.lang.String dstrct) {
        this.dstrct = dstrct;
    }
    
    /**
     * Getter for property tipo_subledger.
     * @return Value of property tipo_subledger.
     */
    public java.lang.String getTipo_subledger() {
        return tipo_subledger;
    }
    
    /**
     * Setter for property tipo_subledger.
     * @param tipo_subledger New value of property tipo_subledger.
     */
    public void setTipo_subledger(java.lang.String tipo_subledger) {
        this.tipo_subledger = tipo_subledger;
    }
    
    /**
     * Getter for property id_subledger.
     * @return Value of property id_subledger.
     */
    public java.lang.String getId_subledger() {
        return id_subledger;
    }
    
    /**
     * Setter for property id_subledger.
     * @param id_subledger New value of property id_subledger.
     */
    public void setId_subledger(java.lang.String id_subledger) {
        this.id_subledger = id_subledger;
    }
    
    /**
     * Getter for property des_tipo.
     * @return Value of property des_tipo.
     */
    public java.lang.String getDes_tipo() {
        return des_tipo;
    }
    
    /**
     * Setter for property des_tipo.
     * @param des_tipo New value of property des_tipo.
     */
    public void setDes_tipo(java.lang.String des_tipo) {
        this.des_tipo = des_tipo;
    }
    
}
