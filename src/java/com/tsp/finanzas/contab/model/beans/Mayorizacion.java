/*
 * Mayorizacion.java
 *
 * Created on 8 de junio de 2006, 05:22 PM
 */
/********************************************************************
 *  Nombre Clase.................   Mayorizacion.java
 *  Descripci�n..................   Bean de la tabla Mayor
 *  Autor........................   Osvaldo P�rez Ferrer
 *  Fecha........................   8/6/2006
 *  Versi�n......................   1.0
 *  Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/
package com.tsp.finanzas.contab.model.beans;

/**
 *
 * @author  Osvaldo
 */
public class Mayorizacion {

    private String distrito;
    private String cuenta;
    private String anio;
    private double saldo_ant;
    private int mes;
    private double movdeb;
    private double movcre;
    private double saldo_act;
    private String usuario_creacion;
    private String usuario_modificacion;
    private String base;
    /** Creates a new instance of Mayor */
    private int transaccion;

    /**
     * Get the value of transaccion
     *
     * @return the value of transaccion
     */
    public int getTransaccion() {
        return transaccion;
    }

    /**
     * Set the value of transaccion
     *
     * @param transaccion new value of transaccion
     */
    public void setTransaccion(int transaccion) {
        this.transaccion = transaccion;
    }
    private String auxiliar;

    /**
     * Get the value of auxiliar
     *
     * @return the value of auxiliar
     */
    public String getAuxiliar() {
        return auxiliar;
    }

    /**
     * Set the value of auxiliar
     *
     * @param auxiliar new value of auxiliar
     */
    public void setAuxiliar(String auxiliar) {
        this.auxiliar = auxiliar;
    }
    private String ind_cuenta_invalida;

    /**
     * Get the value of ind_cuenta_invalida
     *
     * @return the value of ind_cuenta_invalida
     */
    public String getInd_cuenta_invalida() {
        return ind_cuenta_invalida;
    }

    /**
     * Set the value of ind_cuenta_invalida
     *
     * @param ind_cuenta_invalida new value of ind_cuenta_invalida
     */
    public void setInd_cuenta_invalida(String ind_cuenta_invalida) {
        this.ind_cuenta_invalida = ind_cuenta_invalida;
    }
    private String ind_deb_cre_cab_det;

    /**
     * Get the value of ind_deb_cre_cab_det
     *
     * @return the value of ind_deb_cre_cab_det
     */
    public String getInd_deb_cre_cab_det() {
        return ind_deb_cre_cab_det;
    }

    /**
     * Set the value of ind_deb_cre_cab_det
     *
     * @param ind_deb_cre_cab_det new value of ind_deb_cre_cab_det
     */
    public void setInd_deb_cre_cab_det(String ind_deb_cre_cab_det) {
        this.ind_deb_cre_cab_det = ind_deb_cre_cab_det;
    }
    private String ind_deb_cre_cab;

    /**
     * Get the value of ind_deb_cre_cab
     *
     * @return the value of ind_deb_cre_cab
     */
    public String getInd_deb_cre_cab() {
        return ind_deb_cre_cab;
    }

    /**
     * Set the value of ind_deb_cre_cab
     *
     * @param ind_deb_cre_cab new value of ind_deb_cre_cab
     */
    public void setInd_deb_cre_cab(String ind_deb_cre_cab) {
        this.ind_deb_cre_cab = ind_deb_cre_cab;
    }
    private int grupotransaccion;

    /**
     * Get the value of grupotransaccion
     *
     * @return the value of grupotransaccion
     */
    public int getGrupotransaccion() {
        return grupotransaccion;
    }

    /**
     * Set the value of grupotransaccion
     *
     * @param grupotransaccion new value of grupotransaccion
     */
    public void setGrupotransaccion(int grupotransaccion) {
        this.grupotransaccion = grupotransaccion;
    }
    private String numdoc;

    /**
     * Get the value of numdoc
     *
     * @return the value of numdoc
     */
    public String getNumdoc() {
        return numdoc;
    }

    /**
     * Set the value of numdoc
     *
     * @param numdoc new value of numdoc
     */
    public void setNumdoc(String numdoc) {
        this.numdoc = numdoc;
    }
    private String tipodoc;

    /**
     * Get the value of tipodoc
     *
     * @return the value of tipodoc
     */
    public String getTipodoc() {
        return tipodoc;
    }

    /**
     * Set the value of tipodoc
     *
     * @param tipodoc new value of tipodoc
     */
    public void setTipodoc(String tipodoc) {
        this.tipodoc = tipodoc;
    }
    private float diferencia;

    /**
     * Get the value of diferencia
     *
     * @return the value of diferencia
     */
    public float getDiferencia() {
        return diferencia;
    }

    /**
     * Set the value of diferencia
     *
     * @param diferencia new value of diferencia
     */
    public void setDiferencia(float diferencia) {
        this.diferencia = diferencia;
    }
    private float valor_debito;

    /**
     * Get the value of valor_debito
     *
     * @return the value of valor_debito
     */
    public float getValor_debito() {
        return valor_debito;
    }

    /**
     * Set the value of valor_debito
     *
     * @param valor_debito new value of valor_debito
     */
    public void setValor_debito(float valor_debito) {
        this.valor_debito = valor_debito;
    }
    private float valor_credito;

    /**
     * Get the value of valor_credito
     *
     * @return the value of valor_credito
     */
    public float getValor_credito() {
        return valor_credito;
    }

    /**
     * Set the value of valor_credito
     *
     * @param valor_credito new value of valor_credito
     */
    public void setValor_credito(float valor_credito) {
        this.valor_credito = valor_credito;
    }
    private String fuente;

    /**
     * Get the value of fuente
     *
     * @return the value of fuente
     */
    public String getFuente() {
        return fuente;
    }

    /**
     * Set the value of fuente
     *
     * @param fuente new value of fuente
     */
    public void setFuente(String fuente) {
        this.fuente = fuente;
    }

    public Mayorizacion() {
    }

    /**
     * Getter for property distrito.
     * @return Value of property distrito.
     */
    public java.lang.String getDistrito() {
        return distrito;
    }

    /**
     * Setter for property distrito.
     * @param distrito New value of property distrito.
     */
    public void setDistrito(java.lang.String distrito) {
        this.distrito = distrito;
    }

    /**
     * Getter for property cuenta.
     * @return Value of property cuenta.
     */
    public java.lang.String getCuenta() {
        return cuenta;
    }

    /**
     * Setter for property cuenta.
     * @param cuenta New value of property cuenta.
     */
    public void setCuenta(java.lang.String cuenta) {
        this.cuenta = cuenta;
    }

    /**
     * Getter for property saldo_ant.
     * @return Value of property saldo_ant.
     */
    public double getSaldo_ant() {
        return saldo_ant;
    }

    /**
     * Setter for property saldo_ant.
     * @param saldo_ant New value of property saldo_ant.
     */
    public void setSaldo_ant(double saldo_ant) {
        this.saldo_ant = saldo_ant;
    }

    /**
     * Getter for property anio.
     * @return Value of property anio.
     */
    public java.lang.String getAnio() {
        return anio;
    }

    /**
     * Setter for property anio.
     * @param anio New value of property anio.
     */
    public void setAnio(java.lang.String anio) {
        this.anio = anio;
    }

    /**
     * Getter for property mes.
     * @return Value of property mes.
     */
    public int getMes() {
        return mes;
    }

    /**
     * Setter for property mes.
     * @param mes New value of property mes.
     */
    public void setMes(int mes) {
        this.mes = mes;
    }

    /**
     * Getter for property movdeb.
     * @return Value of property movdeb.
     */
    public double getMovdeb() {
        return movdeb;
    }

    /**
     * Setter for property movdeb.
     * @param movdeb New value of property movdeb.
     */
    public void setMovdeb(double movdeb) {
        this.movdeb = movdeb;
    }

    /**
     * Getter for property movcre.
     * @return Value of property movcre.
     */
    public double getMovcre() {
        return movcre;
    }

    /**
     * Setter for property movcre.
     * @param movcre New value of property movcre.
     */
    public void setMovcre(double movcre) {
        this.movcre = movcre;
    }

    /**
     * Getter for property saldo_act.
     * @return Value of property saldo_act.
     */
    public double getSaldo_act() {
        return saldo_act;
    }

    /**
     * Setter for property saldo_act.
     * @param saldo_act New value of property saldo_act.
     */
    public void setSaldo_act(double saldo_act) {
        this.saldo_act = saldo_act;
    }

    /**
     * Getter for property usuario_creacion.
     * @return Value of property usuario_creacion.
     */
    public java.lang.String getUsuario_creacion() {
        return usuario_creacion;
    }

    /**
     * Setter for property usuario_creacion.
     * @param usuario_creacion New value of property usuario_creacion.
     */
    public void setUsuario_creacion(java.lang.String usuario_creacion) {
        this.usuario_creacion = usuario_creacion;
    }

    /**
     * Getter for property usuario_modificacion.
     * @return Value of property usuario_modificacion.
     */
    public java.lang.String getUsuario_modificacion() {
        return usuario_modificacion;
    }

    /**
     * Setter for property usuario_modificacion.
     * @param usuario_modificacion New value of property usuario_modificacion.
     */
    public void setUsuario_modificacion(java.lang.String usuario_modificacion) {
        this.usuario_modificacion = usuario_modificacion;
    }

    /**
     * Getter for property base.
     * @return Value of property base.
     */
    public java.lang.String getBase() {
        return base;
    }

    /**
     * Setter for property base.
     * @param base New value of property base.
     */
    public void setBase(java.lang.String base) {
        this.base = base;
    }
}

