/***************************************
 * Nombre Clase ............. ComprobanteFacturas.java
 * Descripci�n  .. . . . . .  Permitimos encapsular datos de la factura para realizar la contabilizacion de la misma
 * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
 * Fecha . . . . . . . . . .  13/06/2006
 * versi�n . . . . . . . . .  1.0
 * Copyright ...............  Transportes Sanchez Polo S.A.
 *******************************************/
package com.tsp.finanzas.contab.model.beans;


import java.util.*;


public class ComprobanteFacturas extends  Comprobantes{
    
    
    private String tipo_referencia_1 = "";
    private String referencia_1 = "";
    private String tipo_referencia_2 = "";
    private String referencia_2 = "";
    private String tipo_referencia_3 = "";
    private String referencia_3 = "";

    private String tipodoc_factura;
    private String hc;
    private double vlr;
    private double vlrME;
    private String noItem;
    private String codigoImpuesto;
    private String tipoImpuesto;
    private List items;
    
    
   
   
    private String comentario = "";
    private String tipo;
    private String oc;
    
    private boolean existeCuenta = true;
    private Vector codSubledger;
    private String fechaCreacion;
    private String serie;
    
    private String origen;
    
    
    private String  claseFactura = "";
    private String  tipoCmc      = "";
    
    private String  origenOC     = "";
    private String  fechaFactura = "";
    
    
    private String  concepto     = "";
    private double  vlrOC        = 0;
    private double  vlrOCME      = 0;
    
    private String  tipo_contabilizacion = "";
    
    private String tipo_docrelacionado;
    private String docrelacionado;
           
    
    private String  ot         = "";
    private double  vlrot;
    private double  vlrot_me;
    private String  monedaOT   = "";
    private String  fechaOT    = "";
    
    
    private String  monedaFac  = "";
    private String  monedaOC   = "";
    private String  fechaOC    = "";
    
    
    
    private double cantidadDespacho  = 0;
    private double cantidadCumplido  = 0;
    private double valorUnitario     = 0;
    
    
    private double  tasaOC           =  0;
    private double  tasaOT           =  0;
    
    
    private String  abc              = "";
    private String tdoc_rel          = "";//mfontalvo 14/04/07
    private String doc_rel           = "";//mfontalvo 14/04/07
    
    
    private String  otTransporte     = "";
    private String  otReembolsable   = "";

    private String  num_remesas   = "";
    private String  num_documentos   = "";
    private String documento_rel2 = "";

    private String multiservicio = "";

    public String getNum_documentos() {
        return num_documentos;
    }

    public void setNum_documentos(String num_documentos) {
        this.num_documentos = num_documentos;
    }

    public String getNum_remesas() {
        return num_remesas;
    }

    public void setNum_remesas(String num_remesas) {
        this.num_remesas = num_remesas;
    }
    
    
    public ComprobanteFacturas() {
        items  = new LinkedList();
        oc     = "";
        origen = "P";

    }
    
    
    
    
    
    
    /**
     * Getter for property tipodoc_factura.
     * @return Value of property tipodoc_factura.
     */
    public java.lang.String getTipodoc_factura() {
        return tipodoc_factura;
    }    
    
    /**
     * Setter for property tipodoc_factura.
     * @param tipodoc_factura New value of property tipodoc_factura.
     */
    public void setTipodoc_factura(java.lang.String tipodoc_factura) {
        this.tipodoc_factura = tipodoc_factura;
    }
    
    
    
    
    /**
     * Getter for property items.
     * @return Value of property items.
     */
    public java.util.List getItems() {
        return items;
    }
    
    /**
     * Setter for property items.
     * @param items New value of property items.
     */
    public void setItems(java.util.List items) {
        this.items = items;
    }
    
    
    
    
    /**
     * Getter for property vlr.
     * @return Value of property vlr.
     */
    public double getVlr() {
        return vlr;
    }
    
    /**
     * Setter for property vlr.
     * @param vlr New value of property vlr.
     */
    public void setVlr(double vlr) {
        this.vlr = vlr;
    }
    
    
    
    /**
     * Getter for property hc.
     * @return Value of property hc.
     */
    public java.lang.String getHc() {
        return hc;
    }
    
    /**
     * Setter for property hc.
     * @param hc New value of property hc.
     */
    public void setHc(java.lang.String hc) {
        this.hc = hc;
    }
    
    
    
    /**
     * Getter for property noItem.
     * @return Value of property noItem.
     */
    public java.lang.String getNoItem() {
        return noItem;
    }
    
    /**
     * Setter for property noItem.
     * @param noItem New value of property noItem.
     */
    public void setNoItem(java.lang.String noItem) {
        this.noItem = noItem;
    }
    
    
    
    
    
    /**
     * Getter for property tipoImpuesto.
     * @return Value of property tipoImpuesto.
     */
    public java.lang.String getTipoImpuesto() {
        return tipoImpuesto;
    }
    
    /**
     * Setter for property tipoImpuesto.
     * @param tipoImpuesto New value of property tipoImpuesto.
     */
    public void setTipoImpuesto(java.lang.String tipoImpuesto) {
        this.tipoImpuesto = tipoImpuesto;
    }
    
    
    
    
    
    /**
     * Getter for property comentario.
     * @return Value of property comentario.
     */
    public java.lang.String getComentario() {
        return comentario;
    }
    
    /**
     * Setter for property comentario.
     * @param comentario New value of property comentario.
     */
    public void setComentario(java.lang.String comentario) {
        this.comentario = comentario;
    }
    
    
    
    
    
    
    /**
     * Getter for property tipo.
     * @return Value of property tipo.
     */
    public java.lang.String getTipo() {
        return tipo;
    }
    
    /**
     * Setter for property tipo.
     * @param tipo New value of property tipo.
     */
    public void setTipo(java.lang.String tipo) {
        this.tipo = tipo;
    }
    
    /**
     * Getter for property existeCuenta.
     * @return Value of property existeCuenta.
     */
    public boolean isExisteCuenta() {
        return existeCuenta;
    }
    
    /**
     * Setter for property existeCuenta.
     * @param existeCuenta New value of property existeCuenta.
     */
    public void setExisteCuenta(boolean existeCuenta) {
        this.existeCuenta = existeCuenta;
    }
    
    /**
     * Getter for property codSubledger.
     * @return Value of property codSubledger.
     */
    public java.util.Vector getCodSubledger() {
        return codSubledger;
    }    
   
    /**
     * Setter for property codSubledger.
     * @param codSubledger New value of property codSubledger.
     */
    public void setCodSubledger(java.util.Vector codSubledger) {
        this.codSubledger = codSubledger;
    }    
    
    /**
     * Getter for property fechaCreacion.
     * @return Value of property fechaCreacion.
     */
    public java.lang.String getFechaCreacion() {
        return fechaCreacion;
    }
    
    /**
     * Setter for property fechaCreacion.
     * @param fechaCreacion New value of property fechaCreacion.
     */
    public void setFechaCreacion(java.lang.String fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }
    
    /**
     * Getter for property oc.
     * @return Value of property oc.
     */
    public java.lang.String getOc() {
        return oc;
    }
    
    /**
     * Setter for property oc.
     * @param oc New value of property oc.
     */
    public void setOc(java.lang.String oc) {
        this.oc = oc;
    }
    
    /**
     * Getter for property codigoImpuesto.
     * @return Value of property codigoImpuesto.
     */
    public java.lang.String getCodigoImpuesto() {
        return codigoImpuesto;
    }
    
    /**
     * Setter for property codigoImpuesto.
     * @param codigoImpuesto New value of property codigoImpuesto.
     */
    public void setCodigoImpuesto(java.lang.String codigoImpuesto) {
        this.codigoImpuesto = codigoImpuesto;
    }
    
    /**
     * Getter for property serie.
     * @return Value of property serie.
     */
    public java.lang.String getSerie() {
        return serie;
    }
    
    /**
     * Setter for property serie.
     * @param serie New value of property serie.
     */
    public void setSerie(java.lang.String serie) {
        this.serie = serie;
    }
    
    /**
     * Getter for property origen.
     * @return Value of property origen.
     */
    public java.lang.String getOrigen() {
        return origen;
    }
    
    /**
     * Setter for property origen.
     * @param origen New value of property origen.
     */
    public void setOrigen(java.lang.String origen) {
        this.origen = origen;
    }
    
    /**
     * Getter for property claseFactura.
     * @return Value of property claseFactura.
     */
    public java.lang.String getClaseFactura() {
        return claseFactura;
    }
    
    /**
     * Setter for property claseFactura.
     * @param claseFactura New value of property claseFactura.
     */
    public void setClaseFactura(java.lang.String claseFactura) {
        this.claseFactura = claseFactura;
    }
    
    
    
    
    /**
     * Getter for property OP.
     * @return Value of property OP.
     */
    public boolean isOP() {
        return   (  claseFactura.equals("0")  ||  claseFactura.equals("1") )?true:false;
    }    
    
    
    
    /**
     * Getter for property OP.
     * @return Value of property OP.
     */
    public boolean isAdicion() {
        boolean estado = false;
        if( !isOP() ){
             try{
                 if(  this.getNumdoc().substring(0,2).equals("RA")  ||   this.getNumdoc().substring(0,3).equals("AOP")  )
                      estado = true;                 
             }catch(Exception e){}
        }
        
        return  estado;
    }  
    
    
    
    
    /**
     * Getter for property origenOC.
     * @return Value of property origenOC.
     */
    public java.lang.String getOrigenOC() {
        return origenOC;
    }    
    
    /**
     * Setter for property origenOC.
     * @param origenOC New value of property origenOC.
     */
    public void setOrigenOC(java.lang.String origenOC) {
        this.origenOC = origenOC;
    }
    
    /**
     * Getter for property fechaFactura.
     * @return Value of property fechaFactura.
     */
    public java.lang.String getFechaFactura() {
        return fechaFactura;
    }
    
    /**
     * Setter for property fechaFactura.
     * @param fechaFactura New value of property fechaFactura.
     */
    public void setFechaFactura(java.lang.String fechaFactura) {
        this.fechaFactura = fechaFactura;
    }
    
    /**
     * Getter for property tipoCmc.
     * @return Value of property tipoCmc.
     */
    public java.lang.String getTipoCmc() {
        return tipoCmc;
    }
    
    /**
     * Setter for property tipoCmc.
     * @param tipoCmc New value of property tipoCmc.
     */
    public void setTipoCmc(java.lang.String tipoCmc) {
        this.tipoCmc = tipoCmc;
    }
    
    /**
     * Getter for property concepto.
     * @return Value of property concepto.
     */
    public java.lang.String getConcepto() {
        return concepto;
    }
    
    /**
     * Setter for property concepto.
     * @param concepto New value of property concepto.
     */
    public void setConcepto(java.lang.String concepto) {
        this.concepto = concepto;
    }
    
    /**
     * Getter for property vlrOC.
     * @return Value of property vlrOC.
     */
    public double getVlrOC() {
        return vlrOC;
    }
    
    /**
     * Setter for property vlrOC.
     * @param vlrOC New value of property vlrOC.
     */
    public void setVlrOC(double vlrOC) {
        this.vlrOC = vlrOC;
    }
    
    /**
     * Getter for property tipo_contabilizacion.
     * @return Value of property tipo_contabilizacion.
     */
    public java.lang.String getTipo_contabilizacion() {
        return tipo_contabilizacion;
    }
    
    /**
     * Setter for property tipo_contabilizacion.
     * @param tipo_contabilizacion New value of property tipo_contabilizacion.
     */
    public void setTipo_contabilizacion(java.lang.String tipo_contabilizacion) {
        this.tipo_contabilizacion = tipo_contabilizacion;
    }
    
    /**
     * Getter for property tipo_docrelacionado.
     * @return Value of property tipo_docrelacionado.
     */
    public java.lang.String getTipo_docrelacionado() {
        return tipo_docrelacionado;
    }
    
    /**
     * Setter for property tipo_docrelacionado.
     * @param tipo_docrelacionado New value of property tipo_docrelacionado.
     */
    public void setTipo_docrelacionado(java.lang.String tipo_docrelacionado) {
        this.tipo_docrelacionado = tipo_docrelacionado;
    }
    
    /**
     * Getter for property docrelacionado.
     * @return Value of property docrelacionado.
     */
    public java.lang.String getDocrelacionado() {
        return docrelacionado;
    }
    
    /**
     * Setter for property docrelacionado.
     * @param docrelacionado New value of property docrelacionado.
     */
    public void setDocrelacionado(java.lang.String docrelacionado) {
        this.docrelacionado = docrelacionado;
    }
    
    /**
     * Getter for property ot.
     * @return Value of property ot.
     */
    public java.lang.String getOt() {
        return ot;
    }
    
    /**
     * Setter for property ot.
     * @param ot New value of property ot.
     */
    public void setOt(java.lang.String ot) {
        this.ot = ot;
    }
    
    /**
     * Getter for property vlrot.
     * @return Value of property vlrot.
     */
    public double getVlrot() {
        return vlrot;
    }
    
    /**
     * Setter for property vlrot.
     * @param vlrot New value of property vlrot.
     */
    public void setVlrot(double vlrot) {
        this.vlrot = vlrot;
    }
    
    
    
    /**
     * Getter for property monedaFac.
     * @return Value of property monedaFac.
     */
    public java.lang.String getMonedaFac() {
        return monedaFac;
    }
    
    /**
     * Setter for property monedaFac.
     * @param monedaFac New value of property monedaFac.
     */
    public void setMonedaFac(java.lang.String monedaFac) {
        this.monedaFac = monedaFac;
    }
    
    /**
     * Getter for property fechaOC.
     * @return Value of property fechaOC.
     */
    public java.lang.String getFechaOC() {
        return fechaOC;
    }
    
    /**
     * Setter for property fechaOC.
     * @param fechaOC New value of property fechaOC.
     */
    public void setFechaOC(java.lang.String fechaOC) {
        this.fechaOC = fechaOC;
    }
    
    /**
     * Getter for property monedaOC.
     * @return Value of property monedaOC.
     */
    public java.lang.String getMonedaOC() {
        return monedaOC;
    }
    
    /**
     * Setter for property monedaOC.
     * @param monedaOC New value of property monedaOC.
     */
    public void setMonedaOC(java.lang.String monedaOC) {
        this.monedaOC = monedaOC;
    }
    
    /**
     * Getter for property cantidadDespacho.
     * @return Value of property cantidadDespacho.
     */
    public double getCantidadDespacho() {
        return cantidadDespacho;
    }
    
    /**
     * Setter for property cantidadDespacho.
     * @param cantidadDespacho New value of property cantidadDespacho.
     */
    public void setCantidadDespacho(double cantidadDespacho) {
        this.cantidadDespacho = cantidadDespacho;
    }
    
    /**
     * Getter for property cantidadCumplido.
     * @return Value of property cantidadCumplido.
     */
    public double getCantidadCumplido() {
        return cantidadCumplido;
    }
    
    /**
     * Setter for property cantidadCumplido.
     * @param cantidadCumplido New value of property cantidadCumplido.
     */
    public void setCantidadCumplido(double cantidadCumplido) {
        this.cantidadCumplido = cantidadCumplido;
    }
    
    /**
     * Getter for property valorUnitario.
     * @return Value of property valorUnitario.
     */
    public double getValorUnitario() {
        return valorUnitario;
    }
    
    /**
     * Setter for property valorUnitario.
     * @param valorUnitario New value of property valorUnitario.
     */
    public void setValorUnitario(double valorUnitario) {
        this.valorUnitario = valorUnitario;
    }
    
    /**
     * Getter for property vlrOCME.
     * @return Value of property vlrOCME.
     */
    public double getVlrOCME() {
        return vlrOCME;
    }
    
    /**
     * Setter for property vlrOCME.
     * @param vlrOCME New value of property vlrOCME.
     */
    public void setVlrOCME(double vlrOCME) {
        this.vlrOCME = vlrOCME;
    }
    
    /**
     * Getter for property vlrME.
     * @return Value of property vlrME.
     */
    public double getVlrME() {
        return vlrME;
    }
    
    /**
     * Setter for property vlrME.
     * @param vlrME New value of property vlrME.
     */
    public void setVlrME(double vlrME) {
        this.vlrME = vlrME;
    }
    
    /**
     * Getter for property tasaOC.
     * @return Value of property tasaOC.
     */
    public double getTasaOC() {
        return tasaOC;
    }
    
    /**
     * Setter for property tasaOC.
     * @param tasaOC New value of property tasaOC.
     */
    public void setTasaOC(double tasaOC) {
        this.tasaOC = tasaOC;
    }
    
    /**
     * Getter for property abc.
     * @return Value of property abc.
     */
    public java.lang.String getAbc() {
        return abc;
    }
    
    /**
     * Setter for property abc.
     * @param abc New value of property abc.
     */
    public void setAbc(java.lang.String abc) {
        this.abc = abc;
    }
    
    /**
     * Getter for property tdoc_rel.
     * @return Value of property tdoc_rel.
     */
    public java.lang.String getTdoc_rel() {
        return tdoc_rel;
    }
    
    /**
     * Setter for property tdoc_rel.
     * @param tdoc_rel New value of property tdoc_rel.
     */
    public void setTdoc_rel(java.lang.String tdoc_rel) {
        this.tdoc_rel = tdoc_rel;
    }
    
    /**
     * Getter for property doc_rel.
     * @return Value of property doc_rel.
     */
    public java.lang.String getDoc_rel() {
        return doc_rel;
    }
    
    /**
     * Setter for property doc_rel.
     * @param doc_rel New value of property doc_rel.
     */
    public void setDoc_rel(java.lang.String doc_rel) {
        this.doc_rel = doc_rel;
    }
    
    /**
     * Getter for property vlrot_me.
     * @return Value of property vlrot_me.
     */
    public double getVlrot_me() {
        return vlrot_me;
    }
    
    /**
     * Setter for property vlrot_me.
     * @param vlrot_me New value of property vlrot_me.
     */
    public void setVlrot_me(double vlrot_me) {
        this.vlrot_me = vlrot_me;
    }
    
    /**
     * Getter for property monedaOT.
     * @return Value of property monedaOT.
     */
    public java.lang.String getMonedaOT() {
        return monedaOT;
    }
    
    /**
     * Setter for property monedaOT.
     * @param monedaOT New value of property monedaOT.
     */
    public void setMonedaOT(java.lang.String monedaOT) {
        this.monedaOT = monedaOT;
    }
    
    /**
     * Getter for property fechaOT.
     * @return Value of property fechaOT.
     */
    public java.lang.String getFechaOT() {
        return fechaOT;
    }
    
    /**
     * Setter for property fechaOT.
     * @param fechaOT New value of property fechaOT.
     */
    public void setFechaOT(java.lang.String fechaOT) {
        this.fechaOT = fechaOT;
    }
    
    /**
     * Getter for property tasaOT.
     * @return Value of property tasaOT.
     */
    public double getTasaOT() {
        return tasaOT;
    }
    
    /**
     * Setter for property tasaOT.
     * @param tasaOT New value of property tasaOT.
     */
    public void setTasaOT(double tasaOT) {
        this.tasaOT = tasaOT;
    }
    
    /**
     * Getter for property otTransporte.
     * @return Value of property otTransporte.
     */
    public java.lang.String getOtTransporte() {
        return otTransporte;
    }
    
    /**
     * Setter for property otTransporte.
     * @param otTransporte New value of property otTransporte.
     */
    public void setOtTransporte(java.lang.String otTransporte) {
        this.otTransporte = otTransporte;
    }
    
    
    
    public boolean isTransporte(){
        return ( this.otTransporte.equals("S") )?true:false;
    }
    
    
    public boolean isReembolsable(){
        return ( this.otReembolsable.equals("S") )?true:false;
    }
    
    
    /**
     * Getter for property otReembolsable.
     * @return Value of property otReembolsable.
     */
    public java.lang.String getOtReembolsable() {
        return otReembolsable;
    }    
    
    /**
     * Setter for property otReembolsable.
     * @param otReembolsable New value of property otReembolsable.
     */
    public void setOtReembolsable(java.lang.String otReembolsable) {
        this.otReembolsable = otReembolsable;
    }    
    
        public String getReferencia_1() {
        return referencia_1;
    }

    public void setReferencia_1(String referencia_1) {
        this.referencia_1 = referencia_1;
    }

    public String getReferencia_2() {
        return referencia_2;
    }

    public void setReferencia_2(String referencia_2) {
        this.referencia_2 = referencia_2;
    }

    public String getReferencia_3() {
        return referencia_3;
    }

    public void setReferencia_3(String referencia_3) {
        this.referencia_3 = referencia_3;
    }

    public String getTipo_referencia_1() {
        return tipo_referencia_1;
    }

    public void setTipo_referencia_1(String tipo_referencia_1) {
        this.tipo_referencia_1 = tipo_referencia_1;
    }

    public String getTipo_referencia_2() {
        return tipo_referencia_2;
    }

    public void setTipo_referencia_2(String tipo_referencia_2) {
        this.tipo_referencia_2 = tipo_referencia_2;
    }

    public String getTipo_referencia_3() {
        return tipo_referencia_3;
    }

    public void setTipo_referencia_3(String tipo_referencia_3) {
        this.tipo_referencia_3 = tipo_referencia_3;
    }

    /**
     * @return the documento_rel2
     */
    public String getDocumento_rel2() {
        return documento_rel2;
    }

    /**
     * @param documento_rel2 the documento_rel2 to set
     */
    public void setDocumento_rel2(String documento_rel2) {
        this.documento_rel2 = documento_rel2;
    }

    public String getMultiservicio() {
        return multiservicio;
    }

    public void setMultiservicio(String multiservicio) {
        this.multiservicio = multiservicio;
    }

}
