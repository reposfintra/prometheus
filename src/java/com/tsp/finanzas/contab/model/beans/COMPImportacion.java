/*
 * COMPImportacion.java
 *
 * Created on 24 de marzo de 2007, 08:20 AM
 */

package com.tsp.finanzas.contab.model.beans;

import java.sql.*;
import com.tsp.util.Util;

public class COMPImportacion {
    
    
    String dstrct;
    String tipo_documento;
    String tipo_documento_interno;
    String documento;
    String descripcion;
    String tercero;
    String periodo;
    String fecha_documento;
    double total_debito;
    double total_credito;
    double total_foraneo;
    String moneda;
    String sucursal;
    String tipo_operacion;
    String creation_user;
    String creation_date;
    String base;
    
    String item;
    String cuenta;
    String tipo_auxiliar;
    String auxiliar;
    String descripcion_item;
    double debito;
    double credito;
    String tercero_item;
    String tdoc_rel;
    String doc_rel;
    String abc;
    double valor_foraneo;
    
    
    
    /** Creates a new instance of COMPImportacion */
    public COMPImportacion() {
    }
    
    public static COMPImportacion load(ResultSet rs) throws Exception {
        COMPImportacion dt = new COMPImportacion();
        dt.setDstrct          ( Util.coalesce( rs.getString("dstrct")          , "" ) );
        dt.setTipo_documento  ( Util.coalesce( rs.getString("tipo_documento")  , "" ) );
        dt.setDocumento       ( Util.coalesce( rs.getString("documento")       , "" ) );
        dt.setDescripcion     ( Util.coalesce( rs.getString("descripcion")     , "" ) );
        dt.setTercero         ( Util.coalesce( rs.getString("tercero")         , "" ) );
        dt.setPeriodo         ( Util.coalesce( rs.getString("periodo")         , "" ) );
        dt.setFecha_documento ( Util.coalesce( rs.getString("fecha_documento") , "" ) );
        dt.setTotal_debito    ( rs.getDouble("total_debito")  );
        dt.setTotal_credito   ( rs.getDouble("total_credito") );
        dt.setTotal_foraneo   ( rs.getDouble("total_foraneo") );
        dt.setMoneda          ( Util.coalesce( rs.getString("moneda")          , "" ) );
        dt.setSucursal        ( Util.coalesce( rs.getString("sucursal")        , "" ) );
        dt.setTipo_operacion  ( Util.coalesce( rs.getString("tipo_operacion")  , "" ) );
        dt.setCreation_user   ( Util.coalesce( rs.getString("creation_user")   , "" ) );
        dt.setCreation_date   ( Util.coalesce( rs.getString("creation_date")   , "" ) );
        dt.setBase            ( Util.coalesce( rs.getString("base")            , "" ) );
        dt.setItem            ( Util.coalesce( rs.getString("item")            , "" ) );
        dt.setCuenta          ( Util.coalesce( rs.getString("cuenta")          , "" ) );
        dt.setTipo_auxiliar   ( Util.coalesce( rs.getString("tipo_auxiliar")   , "" ) );
        dt.setAuxiliar        ( Util.coalesce( rs.getString("auxiliar")        , "" ) );
        dt.setDescripcion_item( Util.coalesce( rs.getString("descripcion_item"), "" ) );
        dt.setDebito          ( rs.getDouble("debito")  );
        dt.setCredito         ( rs.getDouble("credito") );        
        dt.setValor_foraneo   ( rs.getDouble("valor_foraneo") );        
        dt.setTercero_item    ( Util.coalesce( rs.getString("tercero_item")    , "" ) );
        dt.setTdoc_rel        ( Util.coalesce( rs.getString("tdoc_rel")        , "" ) );
        dt.setDoc_rel         ( Util.coalesce( rs.getString("doc_rel")         , "" ) );
        dt.setAbc             ( Util.coalesce( rs.getString("abc")         , "" ) );
        return dt;
        
    }
    
    /**
     * Getter for property auxiliar.
     * @return Value of property auxiliar.
     */
    public java.lang.String getAuxiliar() {
        return auxiliar;
    }    
        
    /**
     * Setter for property auxiliar.
     * @param auxiliar New value of property auxiliar.
     */
    public void setAuxiliar(java.lang.String auxiliar) {
        this.auxiliar = auxiliar;
    }
    
    /**
     * Getter for property base.
     * @return Value of property base.
     */
    public java.lang.String getBase() {
        return base;
    }
    
    /**
     * Setter for property base.
     * @param base New value of property base.
     */
    public void setBase(java.lang.String base) {
        this.base = base;
    }
    
    /**
     * Getter for property creation_date.
     * @return Value of property creation_date.
     */
    public java.lang.String getCreation_date() {
        return creation_date;
    }
    
    /**
     * Setter for property creation_date.
     * @param creation_date New value of property creation_date.
     */
    public void setCreation_date(java.lang.String creation_date) {
        this.creation_date = creation_date;
    }
    
    /**
     * Getter for property creation_user.
     * @return Value of property creation_user.
     */
    public java.lang.String getCreation_user() {
        return creation_user;
    }
    
    /**
     * Setter for property creation_user.
     * @param creation_user New value of property creation_user.
     */
    public void setCreation_user(java.lang.String creation_user) {
        this.creation_user = creation_user;
    }
    
    /**
     * Getter for property credito.
     * @return Value of property credito.
     */
    public double getCredito() {
        return credito;
    }
    
    /**
     * Setter for property credito.
     * @param credito New value of property credito.
     */
    public void setCredito(double credito) {
        this.credito = credito;
    }
    
    /**
     * Getter for property cuenta.
     * @return Value of property cuenta.
     */
    public java.lang.String getCuenta() {
        return cuenta;
    }
    
    /**
     * Setter for property cuenta.
     * @param cuenta New value of property cuenta.
     */
    public void setCuenta(java.lang.String cuenta) {
        this.cuenta = cuenta;
    }
    
    /**
     * Getter for property debito.
     * @return Value of property debito.
     */
    public double getDebito() {
        return debito;
    }
    
    /**
     * Setter for property debito.
     * @param debito New value of property debito.
     */
    public void setDebito(double debito) {
        this.debito = debito;
    }
    
    /**
     * Getter for property descripcion.
     * @return Value of property descripcion.
     */
    public java.lang.String getDescripcion() {
        return descripcion;
    }
    
    /**
     * Setter for property descripcion.
     * @param descripcion New value of property descripcion.
     */
    public void setDescripcion(java.lang.String descripcion) {
        this.descripcion = descripcion;
    }
    
    /**
     * Getter for property descripcion_item.
     * @return Value of property descripcion_item.
     */
    public java.lang.String getDescripcion_item() {
        return descripcion_item;
    }
    
    /**
     * Setter for property descripcion_item.
     * @param descripcion_item New value of property descripcion_item.
     */
    public void setDescripcion_item(java.lang.String descripcion_item) {
        this.descripcion_item = descripcion_item;
    }
    
    /**
     * Getter for property doc_rel.
     * @return Value of property doc_rel.
     */
    public java.lang.String getDoc_rel() {
        return doc_rel;
    }
    
    /**
     * Setter for property doc_rel.
     * @param doc_rel New value of property doc_rel.
     */
    public void setDoc_rel(java.lang.String doc_rel) {
        this.doc_rel = doc_rel;
    }
    
    /**
     * Getter for property documento.
     * @return Value of property documento.
     */
    public java.lang.String getDocumento() {
        return documento;
    }
    
    /**
     * Setter for property documento.
     * @param documento New value of property documento.
     */
    public void setDocumento(java.lang.String documento) {
        this.documento = documento;
    }
    
    /**
     * Getter for property dstrct.
     * @return Value of property dstrct.
     */
    public java.lang.String getDstrct() {
        return dstrct;
    }
    
    /**
     * Setter for property dstrct.
     * @param dstrct New value of property dstrct.
     */
    public void setDstrct(java.lang.String dstrct) {
        this.dstrct = dstrct;
    }
    
    /**
     * Getter for property fecha_documento.
     * @return Value of property fecha_documento.
     */
    public java.lang.String getFecha_documento() {
        return fecha_documento;
    }
    
    /**
     * Setter for property fecha_documento.
     * @param fecha_documento New value of property fecha_documento.
     */
    public void setFecha_documento(java.lang.String fecha_documento) {
        this.fecha_documento = fecha_documento;
    }
    
    /**
     * Getter for property item.
     * @return Value of property item.
     */
    public java.lang.String getItem() {
        return item;
    }
    
    /**
     * Setter for property item.
     * @param item New value of property item.
     */
    public void setItem(java.lang.String item) {
        this.item = item;
    }
    
    /**
     * Getter for property moneda.
     * @return Value of property moneda.
     */
    public java.lang.String getMoneda() {
        return moneda;
    }
    
    /**
     * Setter for property moneda.
     * @param moneda New value of property moneda.
     */
    public void setMoneda(java.lang.String moneda) {
        this.moneda = moneda;
    }
    
    /**
     * Getter for property periodo.
     * @return Value of property periodo.
     */
    public java.lang.String getPeriodo() {
        return periodo;
    }
    
    /**
     * Setter for property periodo.
     * @param periodo New value of property periodo.
     */
    public void setPeriodo(java.lang.String periodo) {
        this.periodo = periodo;
    }
    
    /**
     * Getter for property sucursal.
     * @return Value of property sucursal.
     */
    public java.lang.String getSucursal() {
        return sucursal;
    }
    
    /**
     * Setter for property sucursal.
     * @param sucursal New value of property sucursal.
     */
    public void setSucursal(java.lang.String sucursal) {
        this.sucursal = sucursal;
    }
    
    /**
     * Getter for property tdoc_rel.
     * @return Value of property tdoc_rel.
     */
    public java.lang.String getTdoc_rel() {
        return tdoc_rel;
    }
    
    /**
     * Setter for property tdoc_rel.
     * @param tdoc_rel New value of property tdoc_rel.
     */
    public void setTdoc_rel(java.lang.String tdoc_rel) {
        this.tdoc_rel = tdoc_rel;
    }
    
    /**
     * Getter for property tercero.
     * @return Value of property tercero.
     */
    public java.lang.String getTercero() {
        return tercero;
    }
    
    /**
     * Setter for property tercero.
     * @param tercero New value of property tercero.
     */
    public void setTercero(java.lang.String tercero) {
        this.tercero = tercero;
    }
    
    /**
     * Getter for property tercero_item.
     * @return Value of property tercero_item.
     */
    public java.lang.String getTercero_item() {
        return tercero_item;
    }
    
    /**
     * Setter for property tercero_item.
     * @param tercero_item New value of property tercero_item.
     */
    public void setTercero_item(java.lang.String tercero_item) {
        this.tercero_item = tercero_item;
    }
    
    /**
     * Getter for property tipo_auxiliar.
     * @return Value of property tipo_auxiliar.
     */
    public java.lang.String getTipo_auxiliar() {
        return tipo_auxiliar;
    }
    
    /**
     * Setter for property tipo_auxiliar.
     * @param tipo_auxiliar New value of property tipo_auxiliar.
     */
    public void setTipo_auxiliar(java.lang.String tipo_auxiliar) {
        this.tipo_auxiliar = tipo_auxiliar;
    }
    
    /**
     * Getter for property tipo_documento.
     * @return Value of property tipo_documento.
     */
    public java.lang.String getTipo_documento() {
        return tipo_documento;
    }
    
    /**
     * Setter for property tipo_documento.
     * @param tipo_documento New value of property tipo_documento.
     */
    public void setTipo_documento(java.lang.String tipo_documento) {
        this.tipo_documento = tipo_documento;
    }
    
    /**
     * Getter for property tipo_documento_interno.
     * @return Value of property tipo_documento_interno.
     */
    public java.lang.String getTipo_documento_interno() {
        return tipo_documento_interno;
    }
    
    /**
     * Setter for property tipo_documento_interno.
     * @param tipo_documento_interno New value of property tipo_documento_interno.
     */
    public void setTipo_documento_interno(java.lang.String tipo_documento_interno) {
        this.tipo_documento_interno = tipo_documento_interno;
    }
    
    /**
     * Getter for property tipo_operacion.
     * @return Value of property tipo_operacion.
     */
    public java.lang.String getTipo_operacion() {
        return tipo_operacion;
    }
    
    /**
     * Setter for property tipo_operacion.
     * @param tipo_operacion New value of property tipo_operacion.
     */
    public void setTipo_operacion(java.lang.String tipo_operacion) {
        this.tipo_operacion = tipo_operacion;
    }
    
    /**
     * Getter for property total_credito.
     * @return Value of property total_credito.
     */
    public double getTotal_credito() {
        return total_credito;
    }
    
    /**
     * Setter for property total_credito.
     * @param total_credito New value of property total_credito.
     */
    public void setTotal_credito(double total_credito) {
        this.total_credito = total_credito;
    }
    
    /**
     * Getter for property total_debito.
     * @return Value of property total_debito.
     */
    public double getTotal_debito() {
        return total_debito;
    }
    
    /**
     * Setter for property total_debito.
     * @param total_debito New value of property total_debito.
     */
    public void setTotal_debito(double total_debito) {
        this.total_debito = total_debito;
    }
    
    
    public String getKey() {
        return (tipo_documento + "-" + documento);
    }
    
    /**
     * Getter for property abc.
     * @return Value of property abc.
     */
    public java.lang.String getAbc() {
        return abc;
    }
    
    /**
     * Setter for property abc.
     * @param abc New value of property abc.
     */
    public void setAbc(java.lang.String abc) {
        this.abc = abc;
    }
    
    /**
     * Getter for property total_foraneo.
     * @return Value of property total_foraneo.
     */
    public double getTotal_foraneo() {
        return total_foraneo;
    }
    
    /**
     * Setter for property total_foraneo.
     * @param total_foraneo New value of property total_foraneo.
     */
    public void setTotal_foraneo(double total_foraneo) {
        this.total_foraneo = total_foraneo;
    }    
    
    /**
     * Getter for property valor_foraneo.
     * @return Value of property valor_foraneo.
     */
    public double getValor_foraneo() {
        return valor_foraneo;
    }
    
    /**
     * Setter for property valor_foraneo.
     * @param valor_foraneo New value of property valor_foraneo.
     */
    public void setValor_foraneo(double valor_foraneo) {
        this.valor_foraneo = valor_foraneo;
    }
    
}