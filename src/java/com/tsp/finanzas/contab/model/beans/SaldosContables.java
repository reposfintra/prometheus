/******************************************************************************** 
 * Nombre clase :                   SaldosContables.java                        *
 * Descripcion :                    Estructura de los Saldos Contables          *
 * Autor :                          LREALES                                     *
 * Fecha :                          12 de junio de 2006, 08:42 AM               *
 * Version :                        1.0                                         *
 * Copyright :                      Fintravalores S.A.                     *
 ********************************************************************************/

package com.tsp.finanzas.contab.model.beans;

import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.sql.*;

public class SaldosContables {
    
    // 1. Datos Basicos    
    private String nombre_corto = "";
    
    private String nombre = "";
    private String tipo_subledger = "";
    private String id_subledger = "";
        
    private String subledger = "";
       
    private String cuenta = "";
    private String anio = "";
    private double saldoant = 0;
    private double movdeb01 = 0;
    private double movcre01 = 0;
    private double movdeb02 = 0;
    private double movcre02 = 0;
    private double movdeb03 = 0;
    private double movcre03 = 0;
    private double movdeb04 = 0;
    private double movcre04 = 0;
    private double movdeb05 = 0;
    private double movcre05 = 0;
    private double movdeb06 = 0;
    private double movcre06 = 0;
    private double movdeb07 = 0;
    private double movcre07 = 0;
    private double movdeb08 = 0;
    private double movcre08 = 0;
    private double movdeb09 = 0;
    private double movcre09 = 0;
    private double movdeb10 = 0;
    private double movcre10 = 0;
    private double movdeb11 = 0;
    private double movcre11 = 0;
    private double movdeb12 = 0;
    private double movcre12 = 0;
    private double saldoact = 0; 
    
    private String reg_status = "";
    private String dstrct = ""; 
    private String last_update = "";
    private String user_update = "";
    private String creation_date = "";
    private String creation_user = "";
    private String base = "";
    
    private double findemes01 = 0;
    private double findemes02 = 0;
    private double findemes03 = 0;
    private double findemes04 = 0;
    private double findemes05 = 0;
    private double findemes06 = 0;
    private double findemes07 = 0;
    private double findemes08 = 0;
    private double findemes09 = 0;
    private double findemes10 = 0;
    private double findemes11 = 0;
    private double findemes12 = 0;
    
    private String cuenta_ini = "";
    private String cuenta_fin = "";
    private String fecha_ini = "";
    private String fecha_fin = "";
    
    public static SaldosContables load ( ResultSet rs ) throws SQLException {
    
        SaldosContables saldos = new SaldosContables();
        
        saldos.setNombre_corto( rs.getString("nombre_corto") );
        /*
        saldos.setNombre( rs.getString("nombre") );
        saldos.setTipo_subledger( rs.getString("tipo_subledger") );
        saldos.setId_subledger( rs.getString("id_subledger") );
        
        saldos.setSubledger( rs.getString("subledger") );
        */
        saldos.setCuenta( rs.getString("cuenta") );
        saldos.setAnio( rs.getString("anio") );
        saldos.setSaldoant( rs.getDouble("saldoant") );
        saldos.setMovdeb01( rs.getDouble("movdeb01") );
        saldos.setMovcre01( rs.getDouble("movcre01") );
        saldos.setMovdeb02( rs.getDouble("movdeb02") );
        saldos.setMovcre02( rs.getDouble("movcre02") );
        saldos.setMovdeb03( rs.getDouble("movdeb03") );
        saldos.setMovcre03( rs.getDouble("movcre03") );
        saldos.setMovdeb04( rs.getDouble("movdeb04") );
        saldos.setMovcre04( rs.getDouble("movcre04") );
        saldos.setMovdeb05( rs.getDouble("movdeb05") );
        saldos.setMovcre05( rs.getDouble("movcre05") );
        saldos.setMovdeb06( rs.getDouble("movdeb06") );
        saldos.setMovcre06( rs.getDouble("movcre06") );
        saldos.setMovdeb07( rs.getDouble("movdeb07") );
        saldos.setMovcre07( rs.getDouble("movcre07") );
        saldos.setMovdeb08( rs.getDouble("movdeb08") );
        saldos.setMovcre08( rs.getDouble("movcre08") );
        saldos.setMovdeb09( rs.getDouble("movdeb09") );
        saldos.setMovcre09( rs.getDouble("movcre09") );
        saldos.setMovdeb10( rs.getDouble("movdeb10") );
        saldos.setMovcre10( rs.getDouble("movcre10") );
        saldos.setMovdeb11( rs.getDouble("movdeb11") );
        saldos.setMovcre11( rs.getDouble("movcre11") );
        saldos.setMovdeb12( rs.getDouble("movdeb12") );
        saldos.setMovcre12( rs.getDouble("movcre12") );
        saldos.setSaldoact( rs.getDouble("saldoact") );
        /*
        saldos.setReg_status( rs.getString("reg_status") );
        saldos.setDstrct( rs.getString("dstrct") );
        saldos.setLast_update( rs.getString("last_update") );
        saldos.setUser_update( rs.getString("user_update") );
        saldos.setCreation_date( rs.getString("creation_date") );
        saldos.setCreation_user( rs.getString("creation_user") );
        saldos.setBase( rs.getString("base") );
        
        saldos.setFindemes01( rs.getDouble("findemes01") );
        saldos.setFindemes02( rs.getDouble("findemes02") );
        saldos.setFindemes03( rs.getDouble("findemes03") );
        saldos.setFindemes04( rs.getDouble("findemes04") );
        saldos.setFindemes05( rs.getDouble("findemes05") );
        saldos.setFindemes06( rs.getDouble("findemes06") );
        saldos.setFindemes07( rs.getDouble("findemes07") );
        saldos.setFindemes08( rs.getDouble("findemes08") );
        saldos.setFindemes09( rs.getDouble("findemes09") );
        saldos.setFindemes10( rs.getDouble("findemes10") );
        saldos.setFindemes11( rs.getDouble("findemes11") );
        saldos.setFindemes12( rs.getDouble("findemes12") );
        
        saldos.setCuenta_ini( rs.getString("cuenta_ini") );
        saldos.setCuenta_fin( rs.getString("cuenta_fin") );
        saldos.setFecha_ini( rs.getString("fecha_ini") );
        saldos.setFecha_fin( rs.getString("fecha_fin") );
        */
        return saldos;
    
    }
       
    /**
     * 1. Datos Basicos:
     * Almacena datos basicos de los saldos contables
     * @autor........  LREALES
     * @date.........  12 de junio de 2006
     * @version .....  1.0      
     */ 
    
    /**
     * Getter for property nombre_corto.
     * @return Value of property nombre_corto.
     */
    public java.lang.String getNombre_corto() {
        return nombre_corto;
    }
    
    /**
     * Setter for property nombre_corto.
     * @param nombre_corto New value of property nombre_corto.
     */
    public void setNombre_corto(java.lang.String nombre_corto) {
        this.nombre_corto = nombre_corto;
    }
    
    /**
     * Getter for property nombre.
     * @return Value of property nombre.
     */
    public java.lang.String getNombre() {
        return nombre;
    }
    
    /**
     * Setter for property nombre.
     * @param nombre New value of property nombre.
     */
    public void setNombre(java.lang.String nombre) {
        this.nombre = nombre;
    }
    
    /**
     * Getter for property tipo_subledger.
     * @return Value of property tipo_subledger.
     */
    public java.lang.String getTipo_subledger() {
        return tipo_subledger;
    }
    
    /**
     * Setter for property tipo_subledger.
     * @param tipo_subledger New value of property tipo_subledger.
     */
    public void setTipo_subledger(java.lang.String tipo_subledger) {
        this.tipo_subledger = tipo_subledger;
    }
    
    /**
     * Getter for property id_subledger.
     * @return Value of property id_subledger.
     */
    public java.lang.String getId_subledger() {
        return id_subledger;
    }
    
    /**
     * Setter for property id_subledger.
     * @param id_subledger New value of property id_subledger.
     */
    public void setId_subledger(java.lang.String id_subledger) {
        this.id_subledger = id_subledger;
    }
    
    /**
     * Getter for property subledger.
     * @return Value of property subledger.
     */
    public java.lang.String getSubledger() {
        return subledger;
    }
    
    /**
     * Setter for property subledger.
     * @param subledger New value of property subledger.
     */
    public void setSubledger(java.lang.String subledger) {
        this.subledger = subledger;
    }
    
    /**
     * Getter for property cuenta.
     * @return Value of property cuenta.
     */
    public java.lang.String getCuenta() {
        return cuenta;
    }
    
    /**
     * Setter for property cuenta.
     * @param cuenta New value of property cuenta.
     */
    public void setCuenta(java.lang.String cuenta) {
        this.cuenta = cuenta;
    }
    
    /**
     * Getter for property anio.
     * @return Value of property anio.
     */
    public java.lang.String getAnio() {
        return anio;
    }
    
    /**
     * Setter for property anio.
     * @param anio New value of property anio.
     */
    public void setAnio(java.lang.String anio) {
        this.anio = anio;
    }
    
    /**
     * Getter for property saldoant.
     * @return Value of property saldoant.
     */
    public double getSaldoant() {
        return saldoant;
    }
    
    /**
     * Setter for property saldoant.
     * @param saldoant New value of property saldoant.
     */
    public void setSaldoant(double saldoant) {
        this.saldoant = saldoant;
    }
    
    /**
     * Getter for property movdeb01.
     * @return Value of property movdeb01.
     */
    public double getMovdeb01() {
        return movdeb01;
    }
    
    /**
     * Setter for property movdeb01.
     * @param movdeb01 New value of property movdeb01.
     */
    public void setMovdeb01(double movdeb01) {
        this.movdeb01 = movdeb01;
    }
    
    /**
     * Getter for property movcre01.
     * @return Value of property movcre01.
     */
    public double getMovcre01() {
        return movcre01;
    }
    
    /**
     * Setter for property movcre01.
     * @param movcre01 New value of property movcre01.
     */
    public void setMovcre01(double movcre01) {
        this.movcre01 = movcre01;
    }
    
    /**
     * Getter for property movdeb02.
     * @return Value of property movdeb02.
     */
    public double getMovdeb02() {
        return movdeb02;
    }
    
    /**
     * Setter for property movdeb02.
     * @param movdeb02 New value of property movdeb02.
     */
    public void setMovdeb02(double movdeb02) {
        this.movdeb02 = movdeb02;
    }
    
    /**
     * Getter for property movcre02.
     * @return Value of property movcre02.
     */
    public double getMovcre02() {
        return movcre02;
    }
    
    /**
     * Setter for property movcre02.
     * @param movcre02 New value of property movcre02.
     */
    public void setMovcre02(double movcre02) {
        this.movcre02 = movcre02;
    }
    
    /**
     * Getter for property movdeb03.
     * @return Value of property movdeb03.
     */
    public double getMovdeb03() {
        return movdeb03;
    }
    
    /**
     * Setter for property movdeb03.
     * @param movdeb03 New value of property movdeb03.
     */
    public void setMovdeb03(double movdeb03) {
        this.movdeb03 = movdeb03;
    }
    
    /**
     * Getter for property movcre03.
     * @return Value of property movcre03.
     */
    public double getMovcre03() {
        return movcre03;
    }
    
    /**
     * Setter for property movcre03.
     * @param movcre03 New value of property movcre03.
     */
    public void setMovcre03(double movcre03) {
        this.movcre03 = movcre03;
    }
    
    /**
     * Getter for property movdeb04.
     * @return Value of property movdeb04.
     */
    public double getMovdeb04() {
        return movdeb04;
    }
    
    /**
     * Setter for property movdeb04.
     * @param movdeb04 New value of property movdeb04.
     */
    public void setMovdeb04(double movdeb04) {
        this.movdeb04 = movdeb04;
    }
    
    /**
     * Getter for property movcre04.
     * @return Value of property movcre04.
     */
    public double getMovcre04() {
        return movcre04;
    }
    
    /**
     * Setter for property movcre04.
     * @param movcre04 New value of property movcre04.
     */
    public void setMovcre04(double movcre04) {
        this.movcre04 = movcre04;
    }
    
    /**
     * Getter for property movdeb05.
     * @return Value of property movdeb05.
     */
    public double getMovdeb05() {
        return movdeb05;
    }
    
    /**
     * Setter for property movdeb05.
     * @param movdeb05 New value of property movdeb05.
     */
    public void setMovdeb05(double movdeb05) {
        this.movdeb05 = movdeb05;
    }
    
    /**
     * Getter for property movcre05.
     * @return Value of property movcre05.
     */
    public double getMovcre05() {
        return movcre05;
    }
    
    /**
     * Setter for property movcre05.
     * @param movcre05 New value of property movcre05.
     */
    public void setMovcre05(double movcre05) {
        this.movcre05 = movcre05;
    }
    
    /**
     * Getter for property movdeb06.
     * @return Value of property movdeb06.
     */
    public double getMovdeb06() {
        return movdeb06;
    }
    
    /**
     * Setter for property movdeb06.
     * @param movdeb06 New value of property movdeb06.
     */
    public void setMovdeb06(double movdeb06) {
        this.movdeb06 = movdeb06;
    }
    
    /**
     * Getter for property movcre06.
     * @return Value of property movcre06.
     */
    public double getMovcre06() {
        return movcre06;
    }
    
    /**
     * Setter for property movcre06.
     * @param movcre06 New value of property movcre06.
     */
    public void setMovcre06(double movcre06) {
        this.movcre06 = movcre06;
    }
    
    /**
     * Getter for property movdeb07.
     * @return Value of property movdeb07.
     */
    public double getMovdeb07() {
        return movdeb07;
    }
    
    /**
     * Setter for property movdeb07.
     * @param movdeb07 New value of property movdeb07.
     */
    public void setMovdeb07(double movdeb07) {
        this.movdeb07 = movdeb07;
    }
    
    /**
     * Getter for property movcre07.
     * @return Value of property movcre07.
     */
    public double getMovcre07() {
        return movcre07;
    }
    
    /**
     * Setter for property movcre07.
     * @param movcre07 New value of property movcre07.
     */
    public void setMovcre07(double movcre07) {
        this.movcre07 = movcre07;
    }
    
    /**
     * Getter for property movdeb08.
     * @return Value of property movdeb08.
     */
    public double getMovdeb08() {
        return movdeb08;
    }
    
    /**
     * Setter for property movdeb08.
     * @param movdeb08 New value of property movdeb08.
     */
    public void setMovdeb08(double movdeb08) {
        this.movdeb08 = movdeb08;
    }
    
    /**
     * Getter for property movcre08.
     * @return Value of property movcre08.
     */
    public double getMovcre08() {
        return movcre08;
    }
    
    /**
     * Setter for property movcre08.
     * @param movcre08 New value of property movcre08.
     */
    public void setMovcre08(double movcre08) {
        this.movcre08 = movcre08;
    }
    
    /**
     * Getter for property movdeb09.
     * @return Value of property movdeb09.
     */
    public double getMovdeb09() {
        return movdeb09;
    }
    
    /**
     * Setter for property movdeb09.
     * @param movdeb09 New value of property movdeb09.
     */
    public void setMovdeb09(double movdeb09) {
        this.movdeb09 = movdeb09;
    }
    
    /**
     * Getter for property movcre09.
     * @return Value of property movcre09.
     */
    public double getMovcre09() {
        return movcre09;
    }
    
    /**
     * Setter for property movcre09.
     * @param movcre09 New value of property movcre09.
     */
    public void setMovcre09(double movcre09) {
        this.movcre09 = movcre09;
    }
    
    /**
     * Getter for property movdeb10.
     * @return Value of property movdeb10.
     */
    public double getMovdeb10() {
        return movdeb10;
    }
    
    /**
     * Setter for property movdeb10.
     * @param movdeb10 New value of property movdeb10.
     */
    public void setMovdeb10(double movdeb10) {
        this.movdeb10 = movdeb10;
    }
    
    /**
     * Getter for property movcre10.
     * @return Value of property movcre10.
     */
    public double getMovcre10() {
        return movcre10;
    }
    
    /**
     * Setter for property movcre10.
     * @param movcre10 New value of property movcre10.
     */
    public void setMovcre10(double movcre10) {
        this.movcre10 = movcre10;
    }
    
    /**
     * Getter for property movdeb11.
     * @return Value of property movdeb11.
     */
    public double getMovdeb11() {
        return movdeb11;
    }
    
    /**
     * Setter for property movdeb11.
     * @param movdeb11 New value of property movdeb11.
     */
    public void setMovdeb11(double movdeb11) {
        this.movdeb11 = movdeb11;
    }
    
    /**
     * Getter for property movcre11.
     * @return Value of property movcre11.
     */
    public double getMovcre11() {
        return movcre11;
    }
    
    /**
     * Setter for property movcre11.
     * @param movcre11 New value of property movcre11.
     */
    public void setMovcre11(double movcre11) {
        this.movcre11 = movcre11;
    }
    
    /**
     * Getter for property movdeb12.
     * @return Value of property movdeb12.
     */
    public double getMovdeb12() {
        return movdeb12;
    }
    
    /**
     * Setter for property movdeb12.
     * @param movdeb12 New value of property movdeb12.
     */
    public void setMovdeb12(double movdeb12) {
        this.movdeb12 = movdeb12;
    }
    
    /**
     * Getter for property movcre12.
     * @return Value of property movcre12.
     */
    public double getMovcre12() {
        return movcre12;
    }
    
    /**
     * Setter for property movcre12.
     * @param movcre12 New value of property movcre12.
     */
    public void setMovcre12(double movcre12) {
        this.movcre12 = movcre12;
    }
    
    /**
     * Getter for property saldoact.
     * @return Value of property saldoact.
     */
    public double getSaldoact() {
        return saldoact;
    }
    
    /**
     * Setter for property saldoact.
     * @param saldoact New value of property saldoact.
     */
    public void setSaldoact(double saldoact) {
        this.saldoact = saldoact;
    }
    
    /**
     * Getter for property reg_status.
     * @return Value of property reg_status.
     */
    public java.lang.String getReg_status() {
        return reg_status;
    }
    
    /**
     * Setter for property reg_status.
     * @param reg_status New value of property reg_status.
     */
    public void setReg_status(java.lang.String reg_status) {
        this.reg_status = reg_status;
    }
    
    /**
     * Getter for property dstrct.
     * @return Value of property dstrct.
     */
    public java.lang.String getDstrct() {
        return dstrct;
    }
    
    /**
     * Setter for property dstrct.
     * @param dstrct New value of property dstrct.
     */
    public void setDstrct(java.lang.String dstrct) {
        this.dstrct = dstrct;
    }
    
    /**
     * Getter for property last_update.
     * @return Value of property last_update.
     */
    public java.lang.String getLast_update() {
        return last_update;
    }
    
    /**
     * Setter for property last_update.
     * @param last_update New value of property last_update.
     */
    public void setLast_update(java.lang.String last_update) {
        this.last_update = last_update;
    }
    
    /**
     * Getter for property user_update.
     * @return Value of property user_update.
     */
    public java.lang.String getUser_update() {
        return user_update;
    }
    
    /**
     * Setter for property user_update.
     * @param user_update New value of property user_update.
     */
    public void setUser_update(java.lang.String user_update) {
        this.user_update = user_update;
    }
    
    /**
     * Getter for property creation_date.
     * @return Value of property creation_date.
     */
    public java.lang.String getCreation_date() {
        return creation_date;
    }
    
    /**
     * Setter for property creation_date.
     * @param creation_date New value of property creation_date.
     */
    public void setCreation_date(java.lang.String creation_date) {
        this.creation_date = creation_date;
    }
    
    /**
     * Getter for property creation_user.
     * @return Value of property creation_user.
     */
    public java.lang.String getCreation_user() {
        return creation_user;
    }
    
    /**
     * Setter for property creation_user.
     * @param creation_user New value of property creation_user.
     */
    public void setCreation_user(java.lang.String creation_user) {
        this.creation_user = creation_user;
    }
    
    /**
     * Getter for property base.
     * @return Value of property base.
     */
    public java.lang.String getBase() {
        return base;
    }
    
    /**
     * Setter for property base.
     * @param base New value of property base.
     */
    public void setBase(java.lang.String base) {
        this.base = base;
    }
        
    /**
     * Getter for property findemes01.
     * @return Value of property findemes01.
     */
    public double getFindemes01() {
        return findemes01;
    }
    
    /**
     * Setter for property findemes01.
     * @param findemes01 New value of property findemes01.
     */
    public void setFindemes01(double findemes01) {
        this.findemes01 = findemes01;
    }
    
    /**
     * Getter for property findemes02.
     * @return Value of property findemes02.
     */
    public double getFindemes02() {
        return findemes02;
    }
    
    /**
     * Setter for property findemes02.
     * @param findemes02 New value of property findemes02.
     */
    public void setFindemes02(double findemes02) {
        this.findemes02 = findemes02;
    }
    
    /**
     * Getter for property findemes03.
     * @return Value of property findemes03.
     */
    public double getFindemes03() {
        return findemes03;
    }
    
    /**
     * Setter for property findemes03.
     * @param findemes03 New value of property findemes03.
     */
    public void setFindemes03(double findemes03) {
        this.findemes03 = findemes03;
    }
    
    /**
     * Getter for property findemes04.
     * @return Value of property findemes04.
     */
    public double getFindemes04() {
        return findemes04;
    }
    
    /**
     * Setter for property findemes04.
     * @param findemes04 New value of property findemes04.
     */
    public void setFindemes04(double findemes04) {
        this.findemes04 = findemes04;
    }
    
    /**
     * Getter for property findemes05.
     * @return Value of property findemes05.
     */
    public double getFindemes05() {
        return findemes05;
    }
    
    /**
     * Setter for property findemes05.
     * @param findemes05 New value of property findemes05.
     */
    public void setFindemes05(double findemes05) {
        this.findemes05 = findemes05;
    }
    
    /**
     * Getter for property findemes06.
     * @return Value of property findemes06.
     */
    public double getFindemes06() {
        return findemes06;
    }
    
    /**
     * Setter for property findemes06.
     * @param findemes06 New value of property findemes06.
     */
    public void setFindemes06(double findemes06) {
        this.findemes06 = findemes06;
    }
    
    /**
     * Getter for property findemes07.
     * @return Value of property findemes07.
     */
    public double getFindemes07() {
        return findemes07;
    }
    
    /**
     * Setter for property findemes07.
     * @param findemes07 New value of property findemes07.
     */
    public void setFindemes07(double findemes07) {
        this.findemes07 = findemes07;
    }
    
    /**
     * Getter for property findemes08.
     * @return Value of property findemes08.
     */
    public double getFindemes08() {
        return findemes08;
    }
    
    /**
     * Setter for property findemes08.
     * @param findemes08 New value of property findemes08.
     */
    public void setFindemes08(double findemes08) {
        this.findemes08 = findemes08;
    }
    
    /**
     * Getter for property findemes09.
     * @return Value of property findemes09.
     */
    public double getFindemes09() {
        return findemes09;
    }
    
    /**
     * Setter for property findemes09.
     * @param findemes09 New value of property findemes09.
     */
    public void setFindemes09(double findemes09) {
        this.findemes09 = findemes09;
    }
    
    /**
     * Getter for property findemes10.
     * @return Value of property findemes10.
     */
    public double getFindemes10() {
        return findemes10;
    }
    
    /**
     * Setter for property findemes10.
     * @param findemes10 New value of property findemes10.
     */
    public void setFindemes10(double findemes10) {
        this.findemes10 = findemes10;
    }
    
    /**
     * Getter for property findemes11.
     * @return Value of property findemes11.
     */
    public double getFindemes11() {
        return findemes11;
    }
    
    /**
     * Setter for property findemes11.
     * @param findemes11 New value of property findemes11.
     */
    public void setFindemes11(double findemes11) {
        this.findemes11 = findemes11;
    }
    
    /**
     * Getter for property findemes12.
     * @return Value of property findemes12.
     */
    public double getFindemes12() {
        return findemes12;
    }
    
    /**
     * Setter for property findemes12.
     * @param findemes12 New value of property findemes12.
     */
    public void setFindemes12(double findemes12) {
        this.findemes12 = findemes12;
    }
    
    /**
     * Getter for property cuenta_ini.
     * @return Value of property cuenta_ini.
     */
    public java.lang.String getCuenta_ini() {
        return cuenta_ini;
    }
    
    /**
     * Setter for property cuenta_ini.
     * @param cuenta_ini New value of property cuenta_ini.
     */
    public void setCuenta_ini(java.lang.String cuenta_ini) {
        this.cuenta_ini = cuenta_ini;
    }
    
    /**
     * Getter for property cuenta_fin.
     * @return Value of property cuenta_fin.
     */
    public java.lang.String getCuenta_fin() {
        return cuenta_fin;
    }
    
    /**
     * Setter for property cuenta_fin.
     * @param cuenta_fin New value of property cuenta_fin.
     */
    public void setCuenta_fin(java.lang.String cuenta_fin) {
        this.cuenta_fin = cuenta_fin;
    }
    
    /**
     * Getter for property fecha_ini.
     * @return Value of property fecha_ini.
     */
    public java.lang.String getFecha_ini() {
        return fecha_ini;
    }
    
    /**
     * Setter for property fecha_ini.
     * @param fecha_ini New value of property fecha_ini.
     */
    public void setFecha_ini(java.lang.String fecha_ini) {
        this.fecha_ini = fecha_ini;
    }
    
    /**
     * Getter for property fecha_fin.
     * @return Value of property fecha_fin.
     */
    public java.lang.String getFecha_fin() {
        return fecha_fin;
    }
    
    /**
     * Setter for property fecha_fin.
     * @param fecha_fin New value of property fecha_fin.
     */
    public void setFecha_fin(java.lang.String fecha_fin) {
        this.fecha_fin = fecha_fin;
    }
    
} 