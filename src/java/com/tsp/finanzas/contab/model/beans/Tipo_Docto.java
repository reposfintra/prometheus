/********************************************************************
 *      Nombre Clase.................   Tipo_Docto.java
 *      Descripci�n..................   Define el objeto Tipo Docto
 *      Autor........................   Ing. Andr�s Maturana De La Cruz
 *      Fecha........................   8 de junio de 2006
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes S�nchez Polo S.A.
 *******************************************************************/

package com.tsp.finanzas.contab.model.beans;

import java.sql.*;
import java.util.*;

/**
 *
 * @author  Ing. Andr�s Maturana De La Cruz
 */
public class Tipo_Docto {
    private String reg_status;
    private String codigo;
    private String codigo_interno;
    private String descripcion;
    private String tercero;
    private String maneja_serie;
    private String serie_ini;
    private String serie_fin;
    private String serie_act;
    private String prefijo ;
    private String prefijo_anio;
    private String prefijo_mes;
    private String base;
    private String usuario;
    private String distrito;
    private String long_serie;
    private String document_name;
    private String esDiario;
    
    /** Creates a new instance of Tipo_Docto */
    public Tipo_Docto() {
    }
    
    public Tipo_Docto(String codeint) {
        this.setCodigo("");
        this.setCodigo_interno(codeint);
        this.setDescripcion("");
        this.setManeja_serie("N");
        this.setPrefijo("");
        this.setPrefijo_anio("");
        this.setPrefijo_mes("");
        this.setSerie_fin("");
        this.setSerie_ini("");
        this.setTercero("M");
        this.setLong_serie("");
        this.setEsDiario("N");   
    }
    
    public void Load(ResultSet rs) throws SQLException{
        this.setBase(rs.getString("base"));
        this.setCodigo(rs.getString("codigo"));
        this.setCodigo_interno(rs.getString("codigo_interno"));
        this.setDescripcion(rs.getString("descripcion"));
        this.setDistrito(rs.getString("dstrct"));
        this.setManeja_serie(rs.getString("maneja_serie"));
        this.setPrefijo(rs.getString("prefijo"));
        this.setPrefijo_anio(rs.getString("prefijo_anio"));
        this.setPrefijo_mes(rs.getString("prefijo_mes"));
        this.setReg_status(rs.getString("reg_status"));
        this.setSerie_act(rs.getString("serie_act"));
        this.setSerie_fin(rs.getString("serie_fin"));
        this.setSerie_ini(rs.getString("serie_ini"));
        this.setTercero(rs.getString("tercero"));
        this.setLong_serie(rs.getString("long_serie"));
        this.setEsDiario(rs.getString("comprodiario"));
    }
    
    /**
     * Getter for property base.
     * @return Value of property base.
     */
    public java.lang.String getBase() {
        return base;
    }
    
    /**
     * Setter for property base.
     * @param base New value of property base.
     */
    public void setBase(java.lang.String base) {
        this.base = base;
    }
    
    /**
     * Getter for property codigo.
     * @return Value of property codigo.
     */
    public java.lang.String getCodigo() {
        return codigo;
    }
    
    /**
     * Setter for property codigo.
     * @param codigo New value of property codigo.
     */
    public void setCodigo(java.lang.String codigo) {
        this.codigo = codigo;
    }
    
    /**
     * Getter for property codigo_interno.
     * @return Value of property codigo_interno.
     */
    public java.lang.String getCodigo_interno() {
        return codigo_interno;
    }
    
    /**
     * Setter for property codigo_interno.
     * @param codigo_interno New value of property codigo_interno.
     */
    public void setCodigo_interno(java.lang.String codigo_interno) {
        this.codigo_interno = codigo_interno;
    }
    
    /**
     * Getter for property descripcion.
     * @return Value of property descripcion.
     */
    public java.lang.String getDescripcion() {
        return descripcion;
    }
    
    /**
     * Setter for property descripcion.
     * @param descripcion New value of property descripcion.
     */
    public void setDescripcion(java.lang.String descripcion) {
        this.descripcion = descripcion;
    }
    
    /**
     * Getter for property distrito.
     * @return Value of property distrito.
     */
    public java.lang.String getDistrito() {
        return distrito;
    }
    
    /**
     * Setter for property distrito.
     * @param distrito New value of property distrito.
     */
    public void setDistrito(java.lang.String distrito) {
        this.distrito = distrito;
    }
    
    /**
     * Getter for property maneja_serie.
     * @return Value of property maneja_serie.
     */
    public java.lang.String getManeja_serie() {
        return maneja_serie;
    }
    
    /**
     * Setter for property maneja_serie.
     * @param maneja_serie New value of property maneja_serie.
     */
    public void setManeja_serie(java.lang.String maneja_serie) {
        this.maneja_serie = maneja_serie;
    }
    
    /**
     * Getter for property prefijo.
     * @return Value of property prefijo.
     */
    public java.lang.String getPrefijo() {
        return prefijo;
    }
    
    /**
     * Setter for property prefijo.
     * @param prefijo New value of property prefijo.
     */
    public void setPrefijo(java.lang.String prefijo) {
        this.prefijo = prefijo;
    }
    
    /**
     * Getter for property prefijo_anio.
     * @return Value of property prefijo_anio.
     */
    public java.lang.String getPrefijo_anio() {
        return prefijo_anio;
    }
    
    /**
     * Setter for property prefijo_anio.
     * @param prefijo_anio New value of property prefijo_anio.
     */
    public void setPrefijo_anio(java.lang.String prefijo_anio) {
        this.prefijo_anio = prefijo_anio;
    }
    
    /**
     * Getter for property prefijo_mes.
     * @return Value of property prefijo_mes.
     */
    public java.lang.String getPrefijo_mes() {
        return prefijo_mes;
    }
    
    /**
     * Setter for property prefijo_mes.
     * @param prefijo_mes New value of property prefijo_mes.
     */
    public void setPrefijo_mes(java.lang.String prefijo_mes) {
        this.prefijo_mes = prefijo_mes;
    }
    
    /**
     * Getter for property reg_status.
     * @return Value of property reg_status.
     */
    public java.lang.String getReg_status() {
        return reg_status;
    }
    
    /**
     * Setter for property reg_status.
     * @param reg_status New value of property reg_status.
     */
    public void setReg_status(java.lang.String reg_status) {
        this.reg_status = reg_status;
    }
    
    /**
     * Getter for property serie_act.
     * @return Value of property serie_act.
     */
    public java.lang.String getSerie_act() {
        return serie_act;
    }
    
    /**
     * Setter for property serie_act.
     * @param serie_act New value of property serie_act.
     */
    public void setSerie_act(java.lang.String serie_act) {
        this.serie_act = serie_act;
    }
    
    /**
     * Getter for property serie_fin.
     * @return Value of property serie_fin.
     */
    public java.lang.String getSerie_fin() {
        return serie_fin;
    }
    
    /**
     * Setter for property serie_fin.
     * @param serie_fin New value of property serie_fin.
     */
    public void setSerie_fin(java.lang.String serie_fin) {
        this.serie_fin = serie_fin;
    }
    
    /**
     * Getter for property serie_ini.
     * @return Value of property serie_ini.
     */
    public java.lang.String getSerie_ini() {
        return serie_ini;
    }
    
    /**
     * Getter for property serie_ini.
     * @return Value of property serie_ini.
     */
    public java.lang.String getSerie_iniN() {
        String n = "";
        
        for( int i=0; i<( Integer.parseInt(this.long_serie) - this.serie_ini.length()); i++)
            n += "0";
        
        return (n + serie_ini);
    }
    
    /**
     * Setter for property serie_ini.
     * @param serie_ini New value of property serie_ini.
     */
    public void setSerie_ini(java.lang.String serie_ini) {
        this.serie_ini = serie_ini;
    }
    
    /**
     * Getter for property tercero.
     * @return Value of property tercero.
     */
    public java.lang.String getTercero() {
        return tercero;
    }
    
    /**
     * Setter for property tercero.
     * @param tercero New value of property tercero.
     */
    public void setTercero(java.lang.String tercero) {
        this.tercero = tercero;
    }
    
    /**
     * Getter for property tercero.
     * @return Value of property tercero.
     */
    public java.lang.String getTerceroN() {
        if( this.getTercero().compareTo("M")==0 )
            return "MANDATORIO";
        else if( this.getTercero().compareTo("N")==0 )
            return "NO APLICA";
        else
            return "OPCIONAL";
    }
    
    /**
     * Getter for property usuario.
     * @return Value of property usuario.
     */
    public java.lang.String getUsuario() {
        return usuario;
    }
    
    /**
     * Setter for property usuario.
     * @param usuario New value of property usuario.
     */
    public void setUsuario(java.lang.String usuario) {
        this.usuario = usuario;
    }
    
    /**
     * Getter for property long_serie.
     * @return Value of property long_serie.
     */
    public java.lang.String getLong_serie() {
        return long_serie;
    }
    
    /**
     * Setter for property long_serie.
     * @param long_serie New value of property long_serie.
     */
    public void setLong_serie(java.lang.String long_serie) {
        this.long_serie = long_serie;
    }
    
    /**
     * Getter for property document_name.
     * @return Value of property document_name.
     */
    public java.lang.String getDocument_name() {
        return document_name;
    }
    
    /**
     * Setter for property document_name.
     * @param document_name New value of property document_name.
     */
    public void setDocument_name(java.lang.String document_name) {
        this.document_name = document_name;
    }
    
    /**
     * Getter for property esDiario.
     * @return Value of property esDiario.
     */
    public java.lang.String getEsDiario() {
        return esDiario;
    }    
    
    /**
     * Setter for property esDiario.
     * @param esDiario New value of property esDiario.
     */
    public void setEsDiario(java.lang.String esDiario) {
        this.esDiario = esDiario;
    }    
    
}
