package com.tsp.finanzas.contab.model;
import com.tsp.finanzas.contab.model.beans.*;
import com.tsp.finanzas.contab.model.services.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.ApplusService;
import com.tsp.operation.model.services.SerieGeneralService;

public class Model {

    /*****jpinedo*****/
    public transient SerieGeneralService serieGeneralService;
    public transient GestionarHcsService Hcsevice;
    public transient ApplusService applusService;

    //leonardo parody
    public transient MayorizacionMensualService MayorizacionMensual;
    
    public transient CuentasService cuentaService;
    public transient CuentaValidaService cuentavalidaservice;
   // public transient TablaMovconService tablamovconservice;
    
    //david p
    public transient ComprobantesService comprobanteService;
    public transient MayorizacionService mayorizacionService;
    public transient CmcService  cmcService;
    public transient Tipo_DoctoService tipo_doctoSvc;
    
    //diogenes
    public transient SubledgerService subledgerService;
    
    //fernell
    public transient ListadoBusquedaServices ListadoBusquedaSvc;
    public transient MovAuxiliarServices MovAuxiliarSvc;
    public transient ContabilizacionFacturasServices ContabilizacionFacSvc;
    public transient ContabilizacionFacturasCLServices  ContabilizacionFacturasCLSvc; 
    
    //lreales
    public transient PlanDeCuentasService planDeCuentasService;
    public transient ConsultasSaldosContablesService consultasSaldosContablesService;
    public transient ReporteFacturasProveedorService reporteFacturasProveedorService;
    public transient CierreMensualMOCService cierreMensualMOCService;
    
    //operez
    public transient PeriodoContableService periodoContableService;
    
    //amaturana
    public transient ReporteGralService reportes;
    
    //igomez
    public transient GrabarComprobanteService GrabarComprobanteSVC;
    public transient ContabilizacionIngresosServices ContabilizacionIngresosSvc;
   
    // declaracion
    public transient ImportacionComprobantesServices ICSvc;

     //RROCHA
    public transient ContabilizacionNegociosServices ContabilizacionNegSvc;
    
    public transient ContabilizacionDiferidosServices ContabilizacionDifSvc;

    public transient ContabilizacionNotasAjusteServices ContabilizacionNotasAjusteSvc;    
    
    /** Creates a new instance of Model */
    public Model() {
        try{ /*
            Hcsevice = new GestionarHcsService();
            applusService = new ApplusService();
            
            //leonardo parody
            cuentaService = new CuentasService();
            cuentavalidaservice = new CuentaValidaService();
       //     tablamovconservice = new TablaMovconService();
            
            //david p
            comprobanteService  =  new ComprobantesService();//David P.
            mayorizacionService =  new MayorizacionService();
            cmcService          =  new CmcService();
            tipo_doctoSvc       =  new Tipo_DoctoService();
            
            //diogenes
            subledgerService    =  new SubledgerService();
            
            //fernell
            ListadoBusquedaSvc  =  new ListadoBusquedaServices();
            MovAuxiliarSvc      =  new MovAuxiliarServices();
            ContabilizacionFacSvc = new ContabilizacionFacturasServices();
            ContabilizacionFacturasCLSvc  =  new  ContabilizacionFacturasCLServices();
            
            //lreales
            planDeCuentasService = new PlanDeCuentasService();
            consultasSaldosContablesService = new ConsultasSaldosContablesService();
            reporteFacturasProveedorService = new ReporteFacturasProveedorService();
            cierreMensualMOCService = new CierreMensualMOCService();
            
            //operez
            periodoContableService = new PeriodoContableService();
            
            //amaturana
            reportes = new ReporteGralService();
            
            //igomez
            GrabarComprobanteSVC = new GrabarComprobanteService();
            
            ContabilizacionIngresosSvc = new ContabilizacionIngresosServices();
            
            // instancia
            ICSvc = new ImportacionComprobantesServices();
            
            //rarp
            ContabilizacionNegSvc= new ContabilizacionNegociosServices();
            
            ContabilizacionDifSvc=new ContabilizacionDiferidosServices();
            
            ContabilizacionNotasAjusteSvc=new ContabilizacionNotasAjusteServices() ;
            
            MayorizacionMensual = new MayorizacionMensualService();
            
            serieGeneralService = new SerieGeneralService();
            */
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
    public Model(String dataBaseName) {
        try{
            comprobanteService  =  new ComprobantesService(dataBaseName);
            ContabilizacionFacSvc = new ContabilizacionFacturasServices(dataBaseName);
            ContabilizacionFacturasCLSvc  =  new  ContabilizacionFacturasCLServices(dataBaseName);
            planDeCuentasService = new PlanDeCuentasService(dataBaseName);
            subledgerService    =  new SubledgerService(dataBaseName);
            serieGeneralService = new SerieGeneralService(dataBaseName);
            GrabarComprobanteSVC = new GrabarComprobanteService(dataBaseName);
            ICSvc = new ImportacionComprobantesServices(dataBaseName);
            Hcsevice = new GestionarHcsService(dataBaseName);
            applusService = new ApplusService(dataBaseName);
            mayorizacionService =  new MayorizacionService(dataBaseName);
            
            cuentaService = new CuentasService(dataBaseName);
            cuentavalidaservice = new CuentaValidaService(dataBaseName);
            cmcService          =  new CmcService(dataBaseName);
            tipo_doctoSvc       =  new Tipo_DoctoService(dataBaseName);
            ListadoBusquedaSvc  =  new ListadoBusquedaServices(dataBaseName);
            MovAuxiliarSvc      =  new MovAuxiliarServices(dataBaseName);
            consultasSaldosContablesService = new ConsultasSaldosContablesService(dataBaseName);
            reporteFacturasProveedorService = new ReporteFacturasProveedorService(dataBaseName);
            cierreMensualMOCService = new CierreMensualMOCService(dataBaseName);
            periodoContableService = new PeriodoContableService(dataBaseName);
            reportes = new ReporteGralService(dataBaseName);
            ContabilizacionIngresosSvc = new ContabilizacionIngresosServices(dataBaseName);
            ContabilizacionNegSvc= new ContabilizacionNegociosServices(dataBaseName);
            ContabilizacionDifSvc=new ContabilizacionDiferidosServices(dataBaseName);
            ContabilizacionNotasAjusteSvc=new ContabilizacionNotasAjusteServices(dataBaseName);
            MayorizacionMensual = new MayorizacionMensualService(dataBaseName);
        }catch(Exception e){
            e.printStackTrace();
        } 
    }
}
