/******************************************************************************
 * Nombre clase :                   HiloReporteFacturasProveedor.java         *
 * Descripcion :                    Clase que maneja los eventos relacionados *
 *                                  con el programa que busca el reporte de   *
 *                                  facturas de proveedor en la BD.           *
 * Autor :                          LREALES                                   *
 * Fecha :                          27 de Junio de 2006, 09:33 AM             *
 * Version :                        1.0                                       *
 * Copyright :                      Fintravalores S.A.                   *
 *****************************************************************************/

package com.tsp.finanzas.contab.model.threads;

import java.util.*;
import java.io.*;
import java.lang.*;
import java.sql.*;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.beans.POIWrite;
import javax.servlet.*;
import com.tsp.exceptions.*;
import com.tsp.operation.controller.*;
import java.text.*;
import javax.servlet.http.*;
import com.tsp.util.*;
import com.tsp.operation.model.*;
import com.tsp.util.Util;
import com.tsp.finanzas.contab.controller.*;
import com.tsp.finanzas.contab.model.beans.*;
import com.tsp.finanzas.contab.model.services.*;
import java.sql.SQLException;

public class HiloReporteFacturasProveedor extends Thread{
        
    private Vector datos;
    private String path;
    private String usuario;
    private String fi;
    private String ff;
    private String m = "";//
    private String procesoName;
    private String des;
    private Model model = new Model();
    
    public HiloReporteFacturasProveedor() {
    }
    
    public void start( Vector datos, String usu, String fecha_inicial, String fecha_final, String usuario_factura ) {
      
        this.datos = datos;
        this.procesoName = "Reporte Facturas Proveedor";
        this.des = "Reporte de Facturas de Proveedor";
        this.usuario = usu;  
        this.fi = fecha_inicial;
        this.ff = fecha_final;
        
        super.start();
        
    }
    
    public synchronized void run(){
        
        try{
            
            //Inicia el proceso
            model.LogProcesosSvc.InsertProceso( procesoName, this.hashCode(), des, usuario );
            
            Util u = new Util();
            
            ResourceBundle rb = ResourceBundle.getBundle( "com/tsp/util/connectionpool/db" );
            path = rb.getString( "ruta" );
                        
            File file = new File( path + "/exportar/migracion/" + usuario );
            file.mkdirs();
               
            String fecha = Util.getFechaActual_String(6);            
            fecha=fecha.replaceAll( "/", "-" );
            fecha=fecha.replaceAll( ":", "_" );
            
            String nombreArch= "ReporteFacturasProveedor[" + fecha + "].xls";
            String       Hoja  = "ReporteFacturasProveedor";
            String       Ruta  = path + "/exportar/migracion/" + usuario + "/" +nombreArch; 
            
            HSSFWorkbook wb    = new HSSFWorkbook();
            HSSFSheet    sheet = wb.createSheet( Hoja );
            HSSFRow      row   = null;
            HSSFRow      row2  = null;
            HSSFCell     cell  = null;
            
            for ( int col = 0; col < 40 ; col++ ){
                
                sheet.setColumnWidth( (short) col, (short) ( ( 50 * 8 ) / ( (double) 1 / 15 ) ) ); 
                
            }
            //pa colocar la columna mas angosta
            //sheet.setColumnWidth( (short)0, (short) ( ( 50 * 8 ) / ( (double) 1 / 10 ) ) );
            
            /****  ENCABEZADO Y DEFINICION DE ESTILOS ************************************************/
            
            /** ENCABEZADO GENERAL *******************************/
            HSSFFont  fuente1 = wb.createFont();
            fuente1.setFontName("verdana");
            fuente1.setFontHeightInPoints((short)(16)) ;
            fuente1.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            fuente1.setColor((short)(0x1));
            
            HSSFCellStyle estilo1 = wb.createCellStyle();
            estilo1.setFont(fuente1);
            estilo1.setFillForegroundColor(HSSFColor.ORANGE.index);
            estilo1.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            
            
            /** SUBTITULO *******************************/
            HSSFFont  fuenteX = wb.createFont();
            fuenteX.setFontName("verdana");
            fuenteX.setFontHeightInPoints((short)(11)) ;
            fuenteX.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            fuenteX.setColor((short)(0x1));
            
            HSSFCellStyle estiloX = wb.createCellStyle();
            estiloX.setFont(fuenteX);
            estiloX.setFillForegroundColor(HSSFColor.ORANGE.index);
            estiloX.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            
            /** TEXTO EN EL ENCABEAZADO *************************/
            HSSFFont  fuente2 = wb.createFont();
            fuente2.setFontName("verdana");
            fuente2.setFontHeightInPoints((short)(11)) ;
            fuente2.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            fuente2.setColor((short)(0x0));
            
            HSSFCellStyle estilo2 = wb.createCellStyle();
            estilo2.setFont(fuente2);
            estilo2.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo2.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            
            /** ENCABEZADO DE LAS COLUMNAS***********************/
            HSSFFont  fuente3 = wb.createFont();
            fuente3.setFontName("verdana");
            fuente3.setFontHeightInPoints((short)(9)) ;
            fuente3.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            fuente3.setColor((short)(0x1));
            
            HSSFCellStyle estilo3 = wb.createCellStyle();
            estilo3.setFont(fuente3);
            estilo3.setFillForegroundColor(HSSFColor.SEA_GREEN.index);
            estilo3.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo3.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo3.setBottomBorderColor(HSSFColor.BLACK.index);
            estilo3.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo3.setLeftBorderColor  (HSSFColor.BLACK.index);
            estilo3.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo3.setRightBorderColor(HSSFColor.BLACK.index);
            estilo3.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo3.setTopBorderColor   (HSSFColor.BLACK.index);
            estilo3.setAlignment(HSSFCellStyle.ALIGN_CENTER);            
                        
            /** TEXTO NORMAL ************************************/
            HSSFFont  fuente4 = wb.createFont();
            fuente4.setFontName("verdana");
            fuente4.setFontHeightInPoints((short)(9)) ;
            fuente4.setBoldweight(HSSFFont.BOLDWEIGHT_NORMAL);
            fuente4.setColor((short)(0x0));
            
            HSSFCellStyle estilo4 = wb.createCellStyle();
            estilo4.setFont(fuente4);
            estilo4.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo4.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo4.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo4.setBottomBorderColor(HSSFColor.BLACK.index);
            estilo4.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo4.setLeftBorderColor  (HSSFColor.BLACK.index);
            estilo4.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo4.setRightBorderColor(HSSFColor.BLACK.index);
            estilo4.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo4.setTopBorderColor   (HSSFColor.BLACK.index);
            
            /** NUMEROS ************************************/
            HSSFCellStyle estilo5 = wb.createCellStyle();
            estilo5.setFont(fuente4);
            estilo5.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo5.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo5.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo5.setBottomBorderColor(HSSFColor.BLACK.index);
            estilo5.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo5.setLeftBorderColor  (HSSFColor.BLACK.index);
            estilo5.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo5.setRightBorderColor(HSSFColor.BLACK.index);
            estilo5.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo5.setTopBorderColor   (HSSFColor.BLACK.index);
            estilo5.setAlignment(HSSFCellStyle.ALIGN_RIGHT);
            /****************************************************/
            
            HSSFFont fuente6 = wb.createFont();
            fuente6.setColor((short)0x0);  
            fuente6.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            
            HSSFCellStyle estilo6 = wb.createCellStyle();
            estilo6.setFont(fuente6);
            estilo6.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo6.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo6.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo6.setBottomBorderColor(HSSFColor.WHITE.index);
            estilo6.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo6.setLeftBorderColor  (HSSFColor.WHITE.index);
            estilo6.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo6.setRightBorderColor(HSSFColor.WHITE.index);
            estilo6.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo6.setTopBorderColor   (HSSFColor.WHITE.index);
            
            /****************************************************/  
            /* MONEDA */
            HSSFCellStyle estilo7 = wb.createCellStyle();
            estilo7.setFont(fuente4);
            estilo7.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo7.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo7.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo7.setBottomBorderColor(HSSFColor.BLACK.index);
            estilo7.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo7.setLeftBorderColor  (HSSFColor.BLACK.index);
            estilo7.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo7.setRightBorderColor(HSSFColor.BLACK.index);
            estilo7.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo7.setTopBorderColor   (HSSFColor.BLACK.index);
            estilo7.setAlignment(HSSFCellStyle.ALIGN_RIGHT);
            estilo7.setDataFormat(wb.createDataFormat().getFormat("$#,##0.00"));
            
            /* FLOAT 2 DECIMAS */
            /*HSSFCellStyle estilo8 = wb.createCellStyle();
            estilo8.setFont(fuente4);
            estilo8.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo8.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo8.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo8.setBottomBorderColor(HSSFColor.BLACK.index);
            estilo8.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo8.setLeftBorderColor  (HSSFColor.BLACK.index);
            estilo8.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo8.setRightBorderColor(HSSFColor.BLACK.index);
            estilo8.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo8.setTopBorderColor   (HSSFColor.BLACK.index);
            estilo8.setDataFormat(wb.createDataFormat().getFormat("#,##"));*/
            
            /* TEXTO NORMAL CENTRADO */
            HSSFCellStyle estilo9 = wb.createCellStyle();
            estilo9.setFont(fuente4);
            estilo9.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo9.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo9.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo9.setBottomBorderColor(HSSFColor.BLACK.index);
            estilo9.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo9.setLeftBorderColor  (HSSFColor.BLACK.index);
            estilo9.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo9.setRightBorderColor(HSSFColor.BLACK.index);
            estilo9.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo9.setTopBorderColor   (HSSFColor.BLACK.index);
            estilo9.setAlignment(HSSFCellStyle.ALIGN_CENTER);
            
            row  = sheet.createRow((short)(0));
            row  = sheet.createRow((short)(1));
            row  = sheet.createRow((short)(2));
            row  = sheet.createRow((short)(3));
            row  = sheet.createRow((short)(4));
            row  = sheet.createRow((short)(5));
            row  = sheet.createRow((short)(6));
            row  = sheet.createRow((short)(7));
            row  = sheet.createRow((short)(8));
            row  = sheet.createRow((short)(9));
            
            for ( int j = 0; j < 16; j++ ) {
                
                row  = sheet.getRow((short)(0));
                cell = row.createCell((short)(j));
                cell.setCellStyle(estilo1);
                row  = sheet.getRow((short)(1));
                cell = row.createCell((short)(j)); 
                cell.setCellStyle(estiloX);
                
            }
            
            row  = sheet.getRow((short)(0));
            cell = row.getCell((short)(0));
            cell.setCellValue("TRANSPORTES SANCHEZ POLO");
                        
            row  = sheet.getRow((short)(1));            
            cell = row.getCell((short)(0));            
            cell.setCellValue("Reporte de Facturas de Proveedor");
            
            for( int i = 2; i < 5; i++ ){
                 
                for ( int j = 0; j < 16; j++ ) {
                    
                    row  = sheet.getRow((short)(i));
                    cell = row.createCell((short)(j)); 
                    cell.setCellStyle(estilo6);
                    
                }      
                
            }
            
            //FECHA
            row = sheet.getRow((short)(3));  
            cell = row.createCell((short)(0));            
            cell = row.getCell((short)(0));  
            cell.setCellStyle(estilo6);
            cell.setCellValue("FECHA DEL REPORTE: ");
            cell = row.createCell((short)(1));            
            cell = row.getCell((short)(1));            
            cell.setCellStyle(estilo6);
            cell.setCellValue(Util.getFechaActual_String(7));
                      
            /*************************************************************************************/
            /***** RECORRER LOS DATOS ******/
            int Fila = 5;
            
            for ( int i = 0; i < datos.size(); i++ ){
                
                ReporteFacturasProveedor r = ( ReporteFacturasProveedor ) datos.elementAt( i );
                
                Fila++;
                row  = sheet.createRow( (short)(Fila) );
                
                cell = row.createCell( (short)(0) );
                cell.setCellStyle( estilo9 );
                cell.setCellValue( r.getFecha_documento() );
                
                cell = row.createCell( (short)(1) );
                cell.setCellStyle( estilo4 );
                cell.setCellValue( r.getProveedor() );
                
                cell = row.createCell( (short)(2) );
                cell.setCellStyle( estilo4 );
                cell.setCellValue( r.getNom_proveedor() );
                
                cell = row.createCell( (short)(3) );
                cell.setCellStyle( estilo9 );
                cell.setCellValue( r.getGran_contribuyente().equals("S")?"Si":"No" );
                
                cell = row.createCell( (short)(4) );
                cell.setCellStyle( estilo9 );
                cell.setCellValue( r.getAutoret_rfte().equals("S")?"Si":"No" );
                
                cell = row.createCell( (short)(5) );
                cell.setCellStyle( estilo9 );
                cell.setCellValue( r.getAutoret_iva().equals("S")?"Si":"No" );
                
                cell = row.createCell( (short)(6) );
                cell.setCellStyle( estilo9 );
                cell.setCellValue( r.getAutoret_ica().equals("S")?"Si":"No" );
                
                cell = row.createCell( (short)(7) );
                cell.setCellStyle( estilo9 );
                cell.setCellValue( r.getClasificacion().equals("NIT")?"Si":"No" );
                
                cell = row.createCell( (short)(8) );
                cell.setCellStyle( estilo4 );
                cell.setCellValue( r.getDocumento() );
                
                cell = row.createCell( (short)(9) );
                cell.setCellStyle( estilo9 );
                cell.setCellValue( r.getItem() );
                
                cell = row.createCell( (short)(10) );
                cell.setCellStyle( estilo4 );
                cell.setCellValue( r.getCodigo_cuenta() );
                
                double valor = Double.parseDouble( r.getVlr().equals("")?"0":r.getVlr() );                
                cell = row.createCell( (short)(11) );
                cell.setCellStyle( estilo7 );
                cell.setCellValue( valor );
                
                cell = row.createCell( (short)(12) );
                cell.setCellStyle( estilo9 );
                cell.setCellValue( r.getCod_impuesto() );
                
                cell = row.createCell( (short)(13) );
                cell.setCellStyle( estilo5 );
                cell.setCellValue( r.getPorcent_impuesto().equals("")?"0.00":r.getPorcent_impuesto() );
                
                double valor_tt_imp = Double.parseDouble( r.getVlr_total_impuesto().equals("")?"0":r.getVlr_total_impuesto() );   
                cell = row.createCell( (short)(14) );
                cell.setCellStyle( estilo7 );
                cell.setCellValue( valor_tt_imp );
                
                cell = row.createCell( (short)(15) );
                cell.setCellStyle( estilo4 );
                cell.setCellValue( r.getCreation_user() );
                
            }//end for datos
            
            row = sheet.createRow((short)(5));
            
            row = sheet.getRow( (short)(5) );            
            cell = row.createCell((short)(0));
            cell.setCellStyle(estilo3);
            cell.setCellValue("FECHA FACTURA");
            
            cell = row.createCell((short)(1));
            cell.setCellStyle(estilo3);            
            cell.setCellValue("NIT PROVEEDOR");
            
            cell = row.createCell((short)(2));
            cell.setCellStyle(estilo3);
            cell.setCellValue("NOMBRE PROVEEDOR");
            
            cell = row.createCell((short)(3));
            cell.setCellStyle(estilo3);            
            cell.setCellValue("GRAN CONTRIBUYENTE");
            
            cell = row.createCell((short)(4));
            cell.setCellStyle(estilo3);
            cell.setCellValue("APLICA RETE FUENTE");
            
            cell = row.createCell((short)(5));
            cell.setCellStyle(estilo3);            
            cell.setCellValue("APLICA RETE IVA");
            
            cell = row.createCell((short)(6));
            cell.setCellStyle(estilo3);
            cell.setCellValue("APLICA RETE ICA");
            
            cell = row.createCell((short)(7));
            cell.setCellStyle(estilo3);
            cell.setCellValue("PERSONA JURIDICA");
            
            cell = row.createCell((short)(8));
            cell.setCellStyle(estilo3);
            cell.setCellValue("NUMERO FACTURA");
            
            cell = row.createCell((short)(9));
            cell.setCellStyle(estilo3);
            cell.setCellValue("NUMERO ITEM");
            
            cell = row.createCell((short)(10));
            cell.setCellStyle(estilo3);
            cell.setCellValue("CODIGO CONTABLE");
            
            cell = row.createCell((short)(11));
            cell.setCellStyle(estilo3);
            cell.setCellValue("VALOR");
            
            cell = row.createCell((short)(12));
            cell.setCellStyle(estilo3);
            cell.setCellValue("CODIGO IMPUESTO");
            
            cell = row.createCell((short)(13));
            cell.setCellStyle(estilo3);
            cell.setCellValue("PORCENTAJE IMPUESTO");
            
            cell = row.createCell((short)(14));
            cell.setCellStyle(estilo3);
            cell.setCellValue("VALOR IMPUESTO");
            
            cell = row.createCell((short)(15));
            cell.setCellStyle(estilo3);
            cell.setCellValue("USUARIO CREO");
            
            /*************************************************************************************/
            /**** GUARDAR DATOS EN EL ARCHIVO  ***/
            FileOutputStream fo = new FileOutputStream( Ruta );
            wb.write( fo );
            fo.close();
        
            //Finalizo el proceso exitosamente
            model.LogProcesosSvc.finallyProceso( procesoName, this.hashCode(), usuario, "PROCESO EXITOSO" );
            
        } catch( Exception e ){    
            
            e.printStackTrace();
            
            //Capturo errores finalizando proceso
            try{
                
                model.LogProcesosSvc.finallyProceso( procesoName, this.hashCode(), usuario, "ERROR : " + e.getMessage() );
            
            } catch( Exception f ){
                
                f.printStackTrace();
                
                try{
                    
                    model.LogProcesosSvc.finallyProceso( procesoName,this.hashCode(), usuario, "ERROR : " + f.getMessage() );
                
                } catch( Exception p ){ p.printStackTrace(); }
                
            }

        }
        
        finally{
            
            super.destroy();
            
        }
        
    }

    //funcion para formatear el texto de los decimales
    public String customFormat(double value) {
        DecimalFormat df = (DecimalFormat) NumberFormat.getNumberInstance(new Locale("en", "US"));
        df.applyPattern("$#,###.##");
        df.setMaximumFractionDigits(2);
        df.setMinimumFractionDigits(2);
        return df.format(value);
    }
    
}