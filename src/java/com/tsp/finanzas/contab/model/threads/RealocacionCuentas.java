/*
 * RealocacionCuentas.java
 *
 * Created on 23 de junio de 2006, 03:48 PM
 */
/*
 * Nombre        RealocacionCuentas.java
 * Descripci�n   Proceso de realocacion de cuentas
 * Autor         David Pi�a L�pez
 * Fecha         23 de junio de 2006, 01:30 PM
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */
package com.tsp.finanzas.contab.model.threads;

import com.tsp.util.*;

import com.tsp.operation.model.beans.*;
import com.tsp.finanzas.contab.model.beans.*;
import com.tsp.finanzas.contab.model.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.beans.*;
import java.io.*;
import java.sql.*;
import java.util.*;
import java.lang.*;
import javax.servlet.jsp.*;
import javax.servlet.http.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import javax.servlet.http.*;
import com.tsp.util.LogWriter;


import java.text.*;

/**
 *
 * @author  David
 */
public class RealocacionCuentas extends Thread{ 
    private Usuario usuario;
    private com.tsp.finanzas.contab.model.Model model;
    private com.tsp.operation.model.Model modelOperation;
    private String anio;
    private String mes;
    private Vector errores;
    private Vector comproDiario;
    private double totalDebito;
    private double totalCredito;
    private PrintWriter pw;
    private LogWriter logWriter;
    
    /** Creates a new instance of RealocacionCuentas */
    public RealocacionCuentas() {
        errores = new Vector();
        comproDiario = new Vector();
        totalDebito = 0;
        totalCredito = 0;
    }
    public void start( Usuario user, String anio, String mes ) throws Exception{
        this.usuario = user;
        this.anio = anio;
        this.mes = mes;
        model = new com.tsp.finanzas.contab.model.Model(user.getBd());
        modelOperation = new com.tsp.operation.model.Model(user.getBd());
        
        java.util.Date ahora = new java.util.Date();
        java.text.SimpleDateFormat fechaHora = new java.text.SimpleDateFormat("yyyy-MM-dd hh.mm.ss");
        String fechaTimestamp = fechaHora.format(ahora);
        
        ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
        String ruta = rb.getString("ruta");
        //Creaci�n del archivo log del usuario
        File file = new File( ruta + "/exportar/migracion/" + usuario.getLogin() );
        file.mkdirs();
        
        pw = new PrintWriter( System.err, true );        
        String logFile = ruta +"/exportar/migracion/" + usuario.getLogin() + "/RealocacionCuentas-" + fechaTimestamp + ".log";        
        pw = new PrintWriter( new FileWriter( logFile, true ), true );
        logWriter = new LogWriter( "RealocacionCuentas", LogWriter.INFO, pw );
        logWriter.setPrintWriter( pw );
        
        this.start();
    }
    public void run(){
        try{
            logWriter.log("**********************************************************",LogWriter.INFO);
            logWriter.log("*  PROCESO DE REALOCACI�N DE CUENTAS                      ",LogWriter.INFO);
            logWriter.log("*  Usuario  :  "+usuario.getLogin()+"                     ",LogWriter.INFO);
            logWriter.log("*  Fecha    :  "+Util.getFechaActual_String(6)+"          ",LogWriter.INFO);
            logWriter.log("**********************************************************",LogWriter.INFO);
            
            modelOperation.LogProcesosSvc.InsertProceso("Realocacion de Cuentas", this.hashCode(), "Proceso de Realocacion de Cuentas", usuario.getLogin() );
            logWriter.log("INICIO DEL PROCESO ",LogWriter.INFO);
            
            model.mayorizacionService.getTotalesSumatoriasICG( anio, mes );
            Vector cuentasICG = model.mayorizacionService.getVector();
            Mayorizacion mayorDiario;
            for(int i = 0; i < cuentasICG.size(); i++ ){
                Mayorizacion mayorICG = (Mayorizacion)cuentasICG.get( i );
                
                logWriter.log("Ingreso Cuenta ::: " + mayorICG.getCuenta() + "989989989999",LogWriter.INFO);
                
                totalDebito = mayorICG.getMovcre();
                totalCredito = mayorICG.getMovdeb();
                
                mayorDiario = new Mayorizacion();
                mayorDiario.setCuenta( mayorICG.getCuenta() + "989989989999" );
                mayorDiario.setMovdeb( totalDebito );
                mayorDiario.setMovcre( totalCredito );
                
                errores = new Vector();
                comproDiario = new Vector();
                comproDiario.add( mayorDiario );                
                                
                model.mayorizacionService.getTotalesSumatoriasElementoGasto( anio, mes, mayorICG.getCuenta().trim() );
                Vector cuentasElemento = model.mayorizacionService.getVector();                
                for(int j = 0; j < cuentasElemento.size(); j++ ){
                    Mayorizacion mayorElemento = (Mayorizacion)cuentasElemento.get( j );
                    
                    if( mayorElemento.getCuenta().equalsIgnoreCase("vacio") ){
                        ////////System.out.println("No existe cuenta para el elemento de gasto "+ mayorElemento.getDistrito());
                        logWriter.log( "No existe cuenta para el elemento de gasto "+ mayorElemento.getDistrito(), LogWriter.ERROR );
                        errores.add( "No existe cuenta para el elemento de gasto "+ mayorElemento.getDistrito() );
                    }
                    
                    totalDebito += mayorElemento.getMovdeb();
                    totalCredito += mayorElemento.getMovcre();
                    
                    mayorDiario = new Mayorizacion();
                    mayorDiario.setCuenta( mayorElemento.getCuenta() );
                    mayorDiario.setMovdeb( mayorElemento.getMovdeb() );
                    mayorDiario.setMovcre( mayorElemento.getMovcre() );
                    comproDiario.add( mayorDiario );
                    
                }
                
                if( totalDebito != totalCredito ){
                   // //////System.out.println("Los totales Debito y Credito del comprobante diario son diferentes en la cuenta " + mayorICG.getCuenta() + "989989989999" + " ::: Total Debito = " + customFormat(totalDebito) + " Total Credito " + customFormat(totalCredito) );
                   // errores.add( "Los totales Debito y Credito del comprobante diario son diferentes en la cuenta " + mayorICG.getCuenta() + "989989989999" + " ::: Total Debito = " + customFormat(totalDebito) + " Total Credito " + customFormat(totalCredito) );
                }
                if( errores.size() == 0 ){
                    ingresarComprobante();
                }                
            }
            logWriter.log("FIN DEL PROCESO ",LogWriter.INFO);
            modelOperation.LogProcesosSvc.finallyProceso("Realocacion de Cuentas", this.hashCode(), usuario.getLogin(), "PROCESO EXITOSO");
            
        }catch( Exception e ){
            try{                
                modelOperation.LogProcesosSvc.finallyProceso("Realocacion de Cuentas", this.hashCode(),usuario.getLogin(),"ERROR :" + e.getMessage());
            }catch ( SQLException ex) {
                //////System.out.println("Error guardando el proceso");
            }            
            e.printStackTrace();
        }        
    }
    private void ingresarComprobante() throws Exception{
        String sql = "";
        
        Comprobantes comprobante = new Comprobantes();
        comprobante.setDstrct( (usuario.getCia()!=null)? usuario.getCia() : "" );
        comprobante.setTipodoc( "CRA" );
        comprobante.setNumdoc( obtenerNumDoc() );
        comprobante.setSucursal( (usuario.getId_agencia()!=null)? usuario.getId_agencia() : "" );
        comprobante.setPeriodo( anio + mes );
        comprobante.setFechadoc( fechaAplicar() );
        comprobante.setDetalle( descripcionDetalle() );        
        comprobante.setTercero( "" );
        comprobante.setTotal_credito( totalCredito );
        comprobante.setTotal_debito( totalDebito );
        comprobante.setTotal_items( 0 );
        comprobante.setMoneda( obtenerMonedaDistrito() );
        comprobante.setAprobador( "ADMIN" );
        comprobante.setUsuario( (usuario.getLogin()!=null)? usuario.getLogin() : "" );
        comprobante.setBase( (usuario.getBase()!=null)? usuario.getBase() : "" );
        comprobante.setTipodoc_rel( " " );
        comprobante.setNumdoc_rel( " " );
        
        model.comprobanteService.setComprobante( comprobante );
        sql = model.comprobanteService.getInsert();
        
        model.tipo_doctoSvc.obtener( (usuario.getCia()!=null)? usuario.getCia() : "", "CRA"  );
        Tipo_Docto td = model.tipo_doctoSvc.getDocto();
        if( td != null ){
            td.setUsuario( (usuario.getLogin()!=null)? usuario.getLogin() : "" );
            td.setSerie_act( "" + (Integer.parseInt( td.getSerie_act() ) + 1) );
            model.tipo_doctoSvc.setDocto( td );
            sql += ";" + model.tipo_doctoSvc.getActualizar();
            
        }
        TransaccionService  svc = new  TransaccionService(usuario.getBd());
        svc.crearStatement();
        svc.getSt().addBatch( sql );
        svc.execute();
        ////////System.out.println( sql );
        
        comprobante.setGrupo_transaccion( model.comprobanteService.getGrupoTransaccion() );
        comprobante.setDocumento_interno( "CRA" );
        sql = "";
        int totalItems = 0;        
        for(int k = 0; k < comproDiario.size(); k++ ){
            Mayorizacion mayorDiario = (Mayorizacion)comproDiario.get( k );
            
            comprobante.setCuenta( mayorDiario.getCuenta() );
            
            if( mayorDiario.getMovcre() != 0 && mayorDiario.getMovdeb() != 0 ){
                
                comprobante.setTotal_credito( mayorDiario.getMovcre() );
                comprobante.setTotal_debito( 0 );                
                model.comprobanteService.setComprobante( comprobante );            
                sql += model.comprobanteService.getInsertItem() + ";";
                
                comprobante.setTotal_credito( 0 );
                comprobante.setTotal_debito( mayorDiario.getMovdeb() );
                model.comprobanteService.setComprobante( comprobante );            
                sql += model.comprobanteService.getInsertItem() + ";";
                
                totalItems += 2;                
            }else{
                comprobante.setTotal_credito( mayorDiario.getMovcre() );
                comprobante.setTotal_debito( mayorDiario.getMovdeb() );
                model.comprobanteService.setComprobante( comprobante );
                sql += model.comprobanteService.getInsertItem() + ";";
                totalItems++;
            }
            
        }        
        comprobante.setTotal_items( totalItems );        
        sql += model.comprobanteService.getUpdateNumItems( comprobante );
        
        svc = new  TransaccionService(usuario.getBd());
        svc.crearStatement();
        svc.getSt().addBatch( sql );
        svc.execute();
        
        ////////System.out.println( sql );
    }
    
    //funcion para formatear el texto de los decimales
    public String customFormat(double value) {
        DecimalFormat df = (DecimalFormat) NumberFormat.getNumberInstance(new Locale("en", "US"));
        df.applyPattern("#,###.##########");
        df.setMaximumFractionDigits(10);
        df.setMinimumFractionDigits(10);
        return df.format(value);
    }
    
    //M�todo que obtiene la constante de la descripcion del campo detalle de comprodet
    private String descripcionDetalle(){
        String periodo = anio + mes;        
        return "COMPRO. REALOCACION PERIODO "+periodo+" DE "+Util.getFechaActual_String(6);
    }
    //M�todo que obtiene la fecha del documento ::: fechadoc
    private String fechaAplicar(){
        String periodo = anio + "-" + mes;
        String periodoA = Util.getFechaActual_String(1) + "-" + Util.getFechaActual_String(3);
        return ( !periodo.equals( periodoA ) ? periodo+"-"+Util.DiasMes( Integer.parseInt(mes) ) : Util.getFechaActual_String(4) );
    }
    
    //Metodo que permite obtener el numero de documento de un periodo
    private String obtenerMonedaDistrito() throws Exception{
        String result = "";
        modelOperation.ciaService.buscarCia( usuario.getCia() );
        Compania cia = modelOperation.ciaService.getCompania();
        if( cia != null ){
            result = cia.getMoneda();
        }        
        return result;        
    }
    
    //Metodo que permite obtener el numero de documento de un periodo
    private String obtenerNumDoc() throws Exception{
        String result = "";
        model.tipo_doctoSvc.obtener( (usuario.getCia()!=null)? usuario.getCia() : "" , "CRA" );
        Tipo_Docto td = model.tipo_doctoSvc.getDocto();
        if( td != null ){
            result = td.getPrefijo();
            if( !td.getPrefijo_anio().trim().equalsIgnoreCase("") ){
                result += anio.substring( anio.length() - td.getPrefijo_anio().length(), anio.length());
            }        
            if( !td.getPrefijo_mes().trim().equalsIgnoreCase("") ){
                result += mes.substring( mes.length() - td.getPrefijo_mes().length(), mes.length());
            }
            if( !td.getLong_serie().equals("0") ){
                result += Util.llenarConCerosALaIzquierda( Integer.parseInt( td.getSerie_act() ) + 1, Integer.parseInt( td.getLong_serie() ) );
            }
        }
        return result;        
    }
    public static void main(String a [])throws Exception{
        try{
            RealocacionCuentas rc = new RealocacionCuentas();
            Usuario user = new Usuario();
            user.setCia( "FINV" );
            user.setLogin( "GPINA" );
            user.setId_agencia( "BQ" );
            user.setBase( "COL" );
            rc.start( user, "2006", "06" );            
            
            
        }catch(SQLException e){
            //////System.out.println("Error "+e.getMessage());
        }
    }
    
}
