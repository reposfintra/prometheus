/*
 * HContabilizacionNotasAjuste.java
 *
 * Created on 24 de junio de 2008, 11:07
 */

package com.tsp.finanzas.contab.model.threads;

import com.tsp.finanzas.contab.model.services.ContabilizacionNotasAjusteServices;
import com.tsp.finanzas.contab.model.beans.Comprobantes;
import com.tsp.operation.model.LogProcesosService;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.beans.Ingreso;
import com.tsp.operation.model.beans.Ingreso_detalle;
import com.tsp.operation.model.beans.factura;
import org.apache.log4j.Logger;

import com.tsp.util.*;
import java.util.*;
import java.lang.*;
import java.text.*;
import java.io.*;

/**
 *
 * @author  navi
 */
public class HContabilizacionNotasAjuste  extends Thread{//modificado en 0908261209 para montar en productivo pero no se ha montado
        
    TreeMap        equivalencias = null;
    String         periodo   = "";
    String         DATE      = "";
    String         TIMESTAMP = "";
    Usuario        usuario;
    String         processName = "ContabilizacionNotaAjuste";
    final String   CUENTA_DIF_CAMBIO = "G01C00910727";
    String ccuenta = "";
    String cuenta = "";
    ContabilizacionNotasAjusteServices svc;
    LogProcesosService logp;
    Logger  logger = Logger.getLogger(HContabilizacionNotasAjuste.class);
    
    FileWriter       fw  = null;
    PrintWriter      pw  = null;
    SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss");
    SimpleDateFormat fmt2= new SimpleDateFormat("yyyyMMdd_kkmmss");
    Date             fechaActual = new Date();    
    
    /** Creates a new instance of HContabilizacionNotasAjuste */
    public HContabilizacionNotasAjuste() {
        super("Contabilizacion de Notas de Ajuste");
    }
    
    public void start (Usuario usuario, ContabilizacionNotasAjusteServices svc) throws Exception{
        
        if (usuario==null || usuario.getLogin().equals(""))
            throw new Exception("Debe definir el usuario de contabilizacion para poder continuar");
        
        
        this.usuario = usuario;    
        this.svc = svc;
        init();
        super.start();
    }    
  
    public void init() throws Exception{
        try {
            if (svc==null)
                throw new Exception("No se ha inicializado el Service de Contabilizacion de Notas de Ajuste");
                
                
            //svc = new ContabilizacionIngresosServices(); 
            logp          = new LogProcesosService(usuario.getBd());
            equivalencias = svc.obtenerEquivalenciaTipoDoc();
            Date date     = new Date();
            DATE          = UtilFinanzas.customFormatDate(date, "yyyy-MM-dd");
            TIMESTAMP     = UtilFinanzas.customFormatDate(date, "yyyy-MM-dd HH:mm:ss");
            
            // para contabilizar con datos inferiores al dia anterior
            date.setDate( date.getDate() -1 );
            periodo       = UtilFinanzas.customFormatDate(date, "yyyyMM");

                               
        } catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
    }
    
    

    
    
    public synchronized void run() {
        try{
            // inicializando log de contabilizacion
            initLOG();
            writeLOG("INICIO CONTABILIZACION DE NOTAS DE AJUSTE.");
            logp.InsertProceso(this.processName, this.hashCode(), "Contabilizacion Notas de Ajuste", usuario.getLogin());
            
            svc.setProcessRun(true);
            
            try{
                
                // contabiliazaciones
                this.IngresosSinContabilizar();
                
                // descontabilizaciones
                //this.IngresosSinDescontabilizar();
                
                
                
            } catch (Exception e){
                e.printStackTrace();
                writeLOG (e.getMessage());
            }
            
            writeLOG("PROCESO FINALIZADO.");
            closeLOG();
            logp.finallyProceso(this.processName, this.hashCode(), usuario.getLogin(), "Proceso terminado, verifique el log de contabilizacion");
            
            
        }catch (Exception ex){
            
            try{
                logp.finallyProceso(this.processName, this.hashCode(), usuario.getLogin(), ex.getMessage());
            } catch (Exception e){}
            
        } finally {
            svc.setProcessRun(false);
        }
        
    }
    
    
    public void IngresosSinContabilizar() throws Exception {
        Vector ingresos = svc.obtenerIngresoSinContabilizar();
        if (ingresos!=null && !ingresos.isEmpty()){
            writeLOG("NOTAS DE AJUSTE PENDIENTE POR CONTABILIZAR : " + ingresos.size() );
            int contabilizados = 0;
            for (int i = 0;  i < ingresos.size(); i++ ){
                Ingreso in = (Ingreso) ingresos.get(i);
                Vector detalles = svc.obtenerDetalleIngreso(in);                
                    if (  contabilizar(in, detalles))
                        contabilizados++;
                }            
            writeLOG("RESUMEN");
            writeLOG("\tContabilizados    : " + contabilizados);
            writeLOG("\tNo contabilizados : " + (ingresos.size() - contabilizados));
        }else {
            writeLOG("Notas de Ajuste PENDIENTES POR CONTABILIZAR : " + 0 );
        }
        writeLOG("");         
        writeLOG("");
        writeLOG(""); 
    }
    
    
    
    /*public void IngresosSinDescontabilizar() throws Exception {
        Vector ingresos = svc.obtenerIngresoSinDescontabilizar();
        if (ingresos!=null && !ingresos.isEmpty()){
            writeLOG("INGRESOS PENDIENTES POR DESCONTABILIZAR : " + ingresos.size() );
            int descontabilizados = 0;
            for (int i = 0;  i < ingresos.size(); i++ ){
                Ingreso in = (Ingreso) ingresos.get(i);
                
                if (in.getTipo_documento().equalsIgnoreCase("ING") || in.getTipo_documento().equalsIgnoreCase("ICR"))
                    if(descontabilizar(in))
                        descontabilizados++;
            }
            writeLOG("RESUMEN");
            writeLOG("\tDescontabilizados    : " + descontabilizados);
            writeLOG("\tNo descontabilizados : " + (ingresos.size() - descontabilizados));
        }else {
            writeLOG("INGRESOS PENDIENTE POR DESCONTABILIZAR : " + 0 );
        }
        writeLOG("");
        writeLOG(""); 
        writeLOG("");         
    }  */  
  
  

    /**
     * Contabilizacion de un ingreso
     * @param mfontalvo
     */
    private boolean contabilizar(Ingreso in, Vector detalles) throws Exception {        
        try{            
            if (in == null)
                throw new Exception("Nota de ajuste nula");
            
            String equivalencia = (String) equivalencias.get( in.getTipo_documento() );
            if (equivalencia == null)
                throw new Exception("No tiene equivalencia contable el tipo de documento");
            
            String des="";
            String auxi="";
            String ter="";
            int sw_negativo=0;       
            //inicio de nucleo1 de contabilizacion de ica
            if(in.getTipo_documento().equalsIgnoreCase("ICA"))
                if(in.getVlr_ingreso() < 0)
                {
                    sw_negativo=1;
                    in.setVlr_ingreso(in.getVlr_ingreso()*-1);
                }
                cuenta=in.getCuenta();
                if(in.getCuenta().equals("G010010015335"))
                {
                    //cuenta=in.getCuenta();
                    des="AJUSTE VALOR REAL";
                    auxi="";
                    ter=in.getNitcli();
                    //ccuenta=in.getCuenta_cliente();
                }
                else
                {
                     if(in.getCuenta().equals("I010010014205") || in.getCuenta().equals("I010010014169") /*|| in.getCuenta().equals("I010060034169")*/)
                     {   
                        //cuenta=in.getCuenta_cliente();
                        des=in.getNomCliente();
                        auxi="RD-"+in.getNitcli();
                        ter=in.getNitcli();
                        //ccuenta=in.getCuenta();
                     }
                     else
                     {
                        if (sw_negativo==0)
                        {//es positivo y no es de fenalco
                            //cuenta=in.getCuenta_cliente();
                            des=nombcuenta(in.getCuenta());
                            auxi="";
                            ter=in.getNitcli();
                            //ccuenta=in.getCuenta_cliente();
                        }
                        else
                        {
                           //cuenta=in.getCuenta_cliente(); //hc
                           des=nombcuenta(in.getCuenta_cliente());
                           auxi="";
                           ter=in.getNitcli();
                           //ccuenta=in.getCuenta();
                        }
                     }
                }
                
                if (cuenta.equals("13109601")){
                    auxi="RD-"+in.getNitcli();
                }
                
            
            //fin de nucleo1 de contabilizacion de ica
            
            // los ingresos miscelaneos requieren items para contabilizarse
            /*if ( in.getTipo_ingreso().equalsIgnoreCase("M")){
                if (detalles == null || detalles.isEmpty() )
                    throw new Exception("Se requieren los items en los ingresos miscelaneos para poder contabilizarlos");
            }*/
            
            
            Comprobantes com = new Comprobantes();
            com.setDstrct        ("FINV");
            com.setTipodoc       ( equivalencia );
            com.setNumdoc        ( in.getNum_ingreso() );
            com.setSucursal      ( "" );
            com.setPeriodo       ( periodo );
            com.setFechadoc      ( DATE );
            com.setDetalle       ( in.getDescripcion_ingreso() );
            com.setTercero       ( in.getNitcli() );
            com.setTotal_debito  ( 0 );
            com.setTotal_credito ( 0 );
            com.setTotal_items   ( 0 );
            com.setMoneda        ( "PES" );
            com.setUsuario       ( usuario.getLogin() );
            com.setFecha_creacion( TIMESTAMP          );
            com.setBase          ( usuario.getBase()  );
            com.setTipo_operacion( in.getConcepto()   );
            com.setDocumento_interno( in.getTipo_documento() );
            
            Vector detalle = new Vector();
            Comprobantes it1 = new Comprobantes();
            
            
            // item del banco o la nota credito
            Comprobantes detalle_principal = new Comprobantes();
            detalle_principal.setDstrct           ( com.getDstrct()   );
            detalle_principal.setTipodoc          ( equivalencia );
            detalle_principal.setNumdoc           ( in.getNum_ingreso()  );
            detalle_principal.setPeriodo          ( com.getPeriodo()  );
            detalle_principal.setCuenta           ( cuenta  );
            detalle_principal.setAuxiliar         ( auxi  );
            detalle_principal.setDetalle          ( des  );
            if (sw_negativo==0){
                detalle_principal.setTotal_debito     ( Double.parseDouble(String.valueOf(in.getVlr_ingreso()) )); 
                detalle_principal.setTotal_credito    ( 0 );   
            }else{
                detalle_principal.setTotal_debito     ( 0 );
                detalle_principal.setTotal_credito    ( Double.parseDouble(String.valueOf(in.getVlr_ingreso()) )); 
            }
            
            
            detalle_principal.setTercero          ( ter  );
            detalle_principal.setDocumento_interno( com.getDocumento_interno() );
            detalle_principal.setUsuario          (usuario.getLogin() );
            detalle_principal.setFecha_creacion   ( TIMESTAMP );
            detalle_principal.setBase             ( usuario.getBase() );
            detalle_principal.setTipodoc_rel      ( "" );
            detalle_principal.setNumdoc_rel       ( "" );
        /*Comprobantes banco =         generarItem(
                    new String[] 
                    { com.getDstrct(), 
                      equivalencia, 
                      in.getNum_ingreso(), 
                      com.getPeriodo(), 
                      cuenta, 
                      auxi, 
                      des,//NOMBRE BANCO U OTRO EN CASO DE SER NC
                      String.valueOf(in.getVlr_ingreso()), 
                      "0" , 
                      ter, 
                      com.getDocumento_interno(), 
                      usuario.getLogin(), 
                      TIMESTAMP,                   
                      usuario.getBase(), 
                      "", 
                      ""
                    }
                );   */  
        
            detalle.add( detalle_principal );//banco
            //System.out.println("1debitocom"+com.getTotal_debito()+"1creditocom"+com.getTotal_credito());
            com.setTotal_debito( com.getTotal_debito() + detalle_principal.getTotal_debito());  
            com.setTotal_credito( com.getTotal_credito() + detalle_principal.getTotal_credito());
            com.setLista( detalle );
            
            
            // ITEMS DEL INGRESO
            double taplicado = 0;
            
            for (int i = 0; i < detalles.size(); i++){
                Ingreso_detalle item = (Ingreso_detalle) detalles.get(i);
                int sw_item_negativo=0;
                if (item.getValor_ingreso()<0){
                   item.setValor_ingreso(item.getValor_ingreso()*(-1)); 
                   sw_item_negativo=1;
                }
                
                if ( !item.isAnulado() ){
                    //System.out.println("item"+com.getNumdoc()+"i"+i);
                    double []total = generarItemsComprobante_X_ItemIngreso( com, item,com.getLista(),sw_negativo ,sw_item_negativo);//090826
                    com.setTotal_debito ( com.getTotal_debito()  + total[0] );
                    com.setTotal_credito( com.getTotal_credito() + total[1] );
                    taplicado +=  total[2];
                }
                //System.out.println("debitocom"+com.getTotal_debito()+"creditocom"+com.getTotal_credito());
            }
            
            
            // solo para los ingresos de clientes
            double saldo  =0;
            /*if (in.getVlr_ingreso_me()>=0){
                saldo  = in.getVlr_ingreso_me() - taplicado;
            }else{
                saldo  = (in.getVlr_ingreso_me()*(-1)) - taplicado;
            }*///090826
            
            double vsaldo = Util.roundByDecimal( saldo * in.getVlr_tasa() , 0);
            //inicio de probablemente basura
            if (in.getTipo_ingreso().equalsIgnoreCase("C") && saldo!=0){
                
                 if ( in.getCuenta_cliente().equals("")) 
                     throw new Exception("Se requiere codigo de cuenta del cliente para asignar saldo.");
                
                Comprobantes csaldo = 
                generarItem(
                    new String[] 
                    { com.getDstrct(), 
                      equivalencia, 
                      in.getNum_ingreso(), 
                      com.getPeriodo(), 
                      in.getCuenta_cliente(), 
                      "", 
                      "SALDO INGRESO",
                      "0", 
                      String.valueOf( vsaldo ), 
                      in.getNitcli(), 
                      com.getDocumento_interno(), 
                      usuario.getLogin(), 
                      TIMESTAMP,                   
                      usuario.getBase(), 
                      "", 
                      ""
                    }
                );            
                detalle.add(csaldo);                
                com.setTotal_credito( com.getTotal_credito() + vsaldo );                
            }           
            else if (in.getTipo_ingreso().equalsIgnoreCase("M")  && saldo!=0){
                throw new Exception("El ingreso miscelaneo no esta aplicado totalmente.");
            }
            //fin de probablemente basura
            
            //System.out.println( in.getNum_ingreso() );
            //System.out.println(com.getTotal_debito() + "     " + com.getTotal_credito());
            
            
            double dif_final = com.getTotal_debito() - com.getTotal_credito();
            //System.out.println("DIF " + dif_final);
             
            if ( dif_final != 0) {
                Comprobantes diferencia = 
                    generarItem(
                        new String[] 
                        { com.getDstrct(), 
                          equivalencia, 
                          in.getNum_ingreso(), 
                          com.getPeriodo(), 
                          this.CUENTA_DIF_CAMBIO, 
                          "", 
                          "DIF.EN.CAMBIOY",
                          String.valueOf( (dif_final < 0 ? -dif_final : 0 )), 
                          String.valueOf( (dif_final > 0 ?  dif_final : 0 )), 
                          in.getNitcli(), 
                          com.getDocumento_interno(), 
                          usuario.getLogin(), 
                          TIMESTAMP,                   
                          usuario.getBase(), 
                          "", 
                          ""
                        }                    
                    );
                detalle.add(diferencia);   
                com.setTotal_debito ( com.getTotal_debito()  + diferencia.getTotal_debito()  );
                com.setTotal_credito( com.getTotal_credito() + diferencia.getTotal_credito() );
            }
            
            
            if (com.getTotal_debito() != com.getTotal_credito())
                throw new Exception("El total debito es diferente al total credito");
            
            
            
            
            com.setTotal_items( detalle.size() );
            salvarComprobante(com, in);
            writeLOG("["+ in.getTipo_documento() +"] " + in.getNum_ingreso() + ":\t CONTABILIZADO ");
            return true;
            
            
        } catch (Exception ex){
            if (in!=null)
                writeLOG("["+ in.getTipo_documento() +"] " + in.getNum_ingreso() + ":\t " + ex.getMessage());
            else    
                writeLOG(ex.getMessage());
            return false;
        }
    }    
    
    
    
    
  /**
     * Contabilizacion de un ingreso
     * @param mfontalvo
     */
    /*private boolean contabilizarRI_RC(Ingreso in, Vector detalles) throws Exception {
        
        try{
            
            if (in == null)
                throw new Exception("Ingreso no valido");

            
            String equivalencia = (String) equivalencias.get( in.getTipo_documento() );
            if (equivalencia == null)
                throw new Exception("No tiene equivalencia contable el tipo de documento");
            
            
            
            if ( in.getCuenta_cliente().equals("")) 
                throw new Exception("Se requiere codigo de cuenta del cliente para asignar debito.");            
            
            
            // los ingresos miscelaneos requieren items para contabilizarse
            if ( in.getTipo_ingreso().equalsIgnoreCase("M"))
                throw new Exception("Los ingresos miscelaneos no aplican al tipo de ingresos 'RI'.");
            
            
            Comprobantes com = new Comprobantes();
            com.setDstrct        ("FINV");
            com.setTipodoc       ( equivalencia );
            com.setNumdoc        ( in.getNum_ingreso() );
            com.setSucursal      ( "" );
            com.setPeriodo       ( periodo );
            com.setFechadoc      ( DATE );
            com.setDetalle       ( in.getDescripcion_ingreso() );
            com.setTercero       ( in.getNitcli() );
            com.setTotal_debito  ( 0 );
            com.setTotal_credito ( 0 );
            com.setTotal_items   ( 0 );
            com.setMoneda        ( "PES" );
            com.setUsuario       ( usuario.getLogin() );
            com.setFecha_creacion( TIMESTAMP          );
            com.setBase          ( usuario.getBase()  );
            com.setTipo_operacion( in.getConcepto()   );
            com.setDocumento_interno( in.getTipo_documento() );
            
            Vector detalle = new Vector();
            Comprobantes it1 = new Comprobantes();
            
            
            // item del banco o la nota credito
            Comprobantes banco = 
            generarItem(
                new String[] 
                { com.getDstrct(), 
                  equivalencia, 
                  in.getNum_ingreso(), 
                  com.getPeriodo(), 
                   in.getCuenta_cliente(), 
                  "", 
                  "INGRESO",
                  String.valueOf(in.getVlr_ingreso()), 
                  "0" , 
                  in.getNitcli(), 
                  com.getDocumento_interno(), 
                  usuario.getLogin(), 
                  TIMESTAMP,                   
                  usuario.getBase(), 
                  "", 
                  ""
                }
            );            
            detalle.add( banco );
            com.setTotal_debito( com.getTotal_debito() + in.getVlr_ingreso() );  
            com.setLista( detalle );
            
            
            // ITEMS DEL INGRESO
            double taplicado = 0;
            for (int i = 0; i < detalles.size(); i++){
                Ingreso_detalle item = (Ingreso_detalle) detalles.get(i);
                
                if ( !item.isAnulado() ){
                    double []total = generarItemsComprobante_X_ItemIngreso( com, item,com.getLista() );
                    com.setTotal_debito ( com.getTotal_debito()  + total[0] );
                    com.setTotal_credito( com.getTotal_credito() + total[1] );
                    taplicado +=  total[2];
                }
            }
            
            
            // solo para los ingresos de clientes
            double saldo  = in.getVlr_ingreso_me() - taplicado;
            double vsaldo = Util.roundByDecimal( saldo * in.getVlr_tasa() , 0);
            if (in.getTipo_ingreso().equalsIgnoreCase("C") && saldo!=0){
                
                 if ( in.getCuenta_cliente().equals("")) 
                     throw new Exception("Se requiere codigo de cuenta del cliente para asignar saldo.");
                
                Comprobantes csaldo = 
                generarItem(
                    new String[] 
                    { com.getDstrct(), 
                      equivalencia, 
                      in.getNum_ingreso(), 
                      com.getPeriodo(), 
                      in.getCuenta_cliente(), 
                      "", 
                      "SALDO INGRESO",
                      "0", 
                      String.valueOf( vsaldo ), 
                      in.getNitcli(), 
                      com.getDocumento_interno(), 
                      usuario.getLogin(), 
                      TIMESTAMP,                   
                      usuario.getBase(), 
                      "", 
                      ""
                    }
                );            
                detalle.add(csaldo);                
                com.setTotal_credito( com.getTotal_credito() + vsaldo );                
            }
            else if (in.getTipo_ingreso().equalsIgnoreCase("M") && saldo!=0){
                throw new Exception("El ingreso miscelaneo no esta aplicado totalmente.");
            }
            
            //System.out.println( in.getNum_ingreso() );
            //System.out.println(com.getTotal_debito() + "     " + com.getTotal_credito());
            
            
            double dif_final = com.getTotal_debito() - com.getTotal_credito();
            //System.out.println("DIF " + dif_final);
             
            if ( dif_final != 0) {
                Comprobantes diferencia = 
                    generarItem(
                        new String[] 
                        { com.getDstrct(), 
                          equivalencia, 
                          in.getNum_ingreso(), 
                          com.getPeriodo(), 
                          this.CUENTA_DIF_CAMBIO, 
                          "", 
                          "DIF.EN.CAMBIO",
                          String.valueOf( (dif_final < 0 ? -dif_final : 0 )), 
                          String.valueOf( (dif_final > 0 ?  dif_final : 0 )), 
                          in.getNitcli(), 
                          com.getDocumento_interno(), 
                          usuario.getLogin(), 
                          TIMESTAMP,                   
                          usuario.getBase(), 
                          "", 
                          ""
                        }                    
                    );
                    
                detalle.add(diferencia);                
                com.setTotal_debito ( com.getTotal_debito()  + diferencia.getTotal_debito()  );
                com.setTotal_credito( com.getTotal_credito() + diferencia.getTotal_credito() );
            }
            
            
            if (com.getTotal_debito() != com.getTotal_credito())
                throw new Exception("El total debito es diferente al total credito");
            
            
            
            
            com.setTotal_items( detalle.size() );
            salvarComprobante(com, in);
            writeLOG("["+ in.getTipo_documento() +"] " + in.getNum_ingreso() + ":\t CONTABILIZADO ");
            return true;
            
            
        } catch (Exception ex){
            if (in!=null)
                writeLOG("["+ in.getTipo_documento() +"] " + in.getNum_ingreso() + ":\t " + ex.getMessage());
            else    
                writeLOG(ex.getMessage());
            return false;
        }
    }*/    
    
        
    
    /**
     * Metodo para descontabilizar un ingreso
     * @param ingreso, ingreso a descontabilizar
     * @throws Exception.
     * @return boolean, estado del proceso de descontabilizacion.
     */   
    /*
    private boolean descontabilizar(Ingreso in) throws Exception {
        try{
            if (in==null)
                throw new Exception("Ingreso no valido.");
            
            if (!in.getReg_status().equalsIgnoreCase("A"))
                throw new Exception("El ingreso no esta anulado para descontabilizarlos");
            
            if (in.getFecha_contabilizacion().equals("0099-01-01 00:00:00"))
                throw new Exception("El ingreso no esta contabilizado para descontabilizarlo");
            
            Comprobantes com = svc.getComprobante( in.getTipo_documento(), in.getNum_ingreso(), in.getTransaccion() );
            if (com == null)
                throw new Exception("No se encontro el comprobante ["+ in.getTransaccion() +"], asociado al ingreso");
            
            Vector detalles = com.getLista();
            if (detalles == null || detalles.isEmpty())
                throw new Exception("El comprobante ["+ in.getTransaccion() +"] no posee items");
            
            
            com.setDetalle       ( "DESCONTABILIZACION INGRESO" );
            com.setUsuario       ( usuario.getLogin() );
            com.setFecha_creacion( TIMESTAMP          );
            com.setBase          ( usuario.getBase()  );
            
            for (int i = 0; i<detalles.size(); i++){
                Comprobantes c = (Comprobantes) detalles.get(i);
                double debito  = c.getTotal_debito();
                c.setTotal_debito ( c.getTotal_credito() );
                c.setTotal_credito( debito               );
                
                c.setUsuario       ( com.getUsuario()        );
                c.setFecha_creacion( com.getFecha_creacion() );
                c.setBase          ( com.getBase()           );
            }
            
            salvarComprobante(com, in);
            writeLOG("["+ in.getTipo_documento() +"] " + in.getNum_ingreso() + ":\t DESCONTABILIZADO ");
            return true;
            
        } catch (Exception ex){
            if (in!=null)
                writeLOG("["+ in.getTipo_documento() +"] " + in.getNum_ingreso() + ":\t " + ex.getMessage());
            else    
                writeLOG(ex.getMessage());
            return false;
        }
    } */   
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    public double[] generarItemsComprobante_X_ItemIngreso (Comprobantes cab, Ingreso_detalle item, Vector detalleComprobante, int sw_negativo ,int sw_item_negativox) throws Exception{//090826
        
        double tdebito   = 0;
        double tcredito  = 0;
        double taplicado = 0;
        try {            
            if (item == null)
                throw new Exception ("Item de nota d ajuste no valido");
            
            // deudores varios
            if (item.getCuenta().equals(""))
                throw new Exception ("El item ["+ item.getItem() +"] no presenta codigo contable.");
             
            double vdeudor     = item.getValor_ingreso();
            double vdiferencia = item.getValor_diferencia();
            
            if (item.getTipo_doc().equalsIgnoreCase("FAC") ) {
                if (!item.getDocumento().trim().equals("")){
                    factura fact = svc.obtenerDatosFactura(item);
                    
                    if (fact==null)
                        throw new Exception("La factura que asocio al item del ingreso no esta regitrada en la base de datos");
                    
                        vdeudor = vdeudor - Math.abs(vdiferencia);
                        
                } else
                    throw new Exception("El item indica que tiene una factura relacionada(tipo_doc), pero no define el numero de la misma(documento)");
            }
            
                
             /*if(!(cab.getTipodoc().equalsIgnoreCase("ICA"))){    
                Comprobantes deudor = 
                generarItem(
                    new String[] 
                    { cab.getDstrct(), 
                      cab.getTipodoc(), 
                      cab.getNumdoc(), 
                      cab.getPeriodo(), 
                      ((cab.getTipodoc()).equals("ING"))?item.getCuenta():"13109602" , 
                      item.getAuxiliar(), 
                      ((item.getDocumento()).equals(""))?nombcuenta(item.getCuenta()):"PAGO " + item.getDocumento()+" "+ nombcliente(item.getNit_cliente()),
                      String.valueOf( vdeudor < 0 ? vdeudor : 0 ), 
                      String.valueOf( vdeudor > 0 ? vdeudor : 0 ), 
                      item.getNit_cliente(), 
                      cab.getDocumento_interno(), 
                      usuario.getLogin(), 
                      TIMESTAMP,
                      usuario.getBase(), 
                      item.getTipo_doc(), 
                      item.getDocumento()
                    }
                );
                tdebito   += deudor.getTotal_debito();
                tcredito  += deudor.getTotal_credito();
                taplicado += item.getValor_ingreso_me();
                detalleComprobante.add(deudor); 
            }
            else
            {*/
            Comprobantes comproica=new Comprobantes();
            comproica.setDstrct           ( cab.getDstrct()   );
            comproica.setTipodoc          ( cab.getTipodoc()  );
            comproica.setNumdoc           ( cab.getNumdoc()  );
            comproica.setPeriodo          ( cab.getPeriodo() );
            comproica.setCuenta           ( item.getCuenta()  );
            comproica.setAuxiliar         ( item.getAuxiliar()  );
            //comproica.setDetalle          ( (( cuenta.equals("G010010015335")))?"Ajuste " + item.getDocumento()+" "+ nombcliente(item.getNit_cliente()):nombcuenta(ccuenta));
            comproica.setDetalle          ( (( cuenta.equals("G010010015335")))?"Ajuste " + item.getDocumento()+" "+ nombcliente(item.getNit_cliente()):nombcuenta(comproica.getCuenta()));//antes ccuenta en vez de getCuenta
            //if (sw_negativo==0){//090826
            if (sw_item_negativox==0){//090826
                comproica.setTotal_debito     ( 0 );
                comproica.setTotal_credito   ( Double.parseDouble(String.valueOf(( vdeudor < 0 ? vdeudor*-1 : vdeudor ))) );    
            }else{
                comproica.setTotal_debito     ( Double.parseDouble(String.valueOf(( vdeudor < 0 ? vdeudor*-1 : vdeudor ))) );
                comproica.setTotal_credito    ( 0 );
            }
            comproica.setTercero          ( item.getNit_cliente()  );
            comproica.setDocumento_interno( cab.getDocumento_interno()  );
            comproica.setUsuario          ( usuario.getLogin());
            comproica.setFecha_creacion   ( TIMESTAMP );
            comproica.setBase             ( usuario.getBase() );
            comproica.setTipodoc_rel      ( item.getTipo_doc() );
            comproica.setNumdoc_rel       ( item.getDocumento() );
        
                /*Comprobantes comproica = 
                generarItem(
                    new String[] 
                    { cab.getDstrct(), 
                      cab.getTipodoc(), 
                      cab.getNumdoc(), 
                      cab.getPeriodo(), 
                      (( cuenta.equals("G010010015335")))?"13109602":ccuenta,  
                      (( cuenta.equals("G010010015335")))?item.getAuxiliar():"", 
                      (( cuenta.equals("G010010015335")))?"Ajuste " + item.getDocumento()+" "+ nombcliente(item.getNit_cliente()):nombcuenta(ccuenta),
                      String.valueOf(  0 ), 
                      String.valueOf( vdeudor < 0 ? vdeudor*-1 : vdeudor ), 
                      item.getNit_cliente(), 
                      cab.getDocumento_interno(), 
                      usuario.getLogin(), 
                      TIMESTAMP,
                      usuario.getBase(), 
                      item.getTipo_doc(), 
                      item.getDocumento()
                    }
                );*/
            tdebito   += comproica.getTotal_debito();
            tcredito  += comproica.getTotal_credito();
            if (item.getValor_ingreso_me()>=0){
                taplicado += item.getValor_ingreso_me();
            }else{
                taplicado += (item.getValor_ingreso_me()*(-1));
            }
            
            detalleComprobante.add(comproica);
                
            //}
                      
            //inicio de probablemente basura
            if ( vdiferencia != 0) {
                Comprobantes diferencia = 
                    generarItem(
                        new String[] 
                        { cab.getDstrct(), 
                          cab.getTipodoc(), 
                          cab.getNumdoc(), 
                          cab.getPeriodo(), 
                          this.CUENTA_DIF_CAMBIO, 
                          "" , 
                          "DIF.EN.CAMBIOX " + item.getDocumento(),
                          String.valueOf( 0 ), 
                          String.valueOf( Math.abs(vdiferencia) ), 
                          item.getNit_cliente(), 
                          cab.getDocumento_interno(),  
                          usuario.getLogin(), 
                          TIMESTAMP, 
                          usuario.getBase(), 
                          item.getTipo_doc(), 
                          item.getDocumento()
                        }
                    );
                tdebito   += diferencia.getTotal_debito();
                tcredito  += diferencia.getTotal_credito();
                detalleComprobante.add(diferencia);                
            }
            //fin de probablemente basura
            //inicio de probablemente basura
            
            // item de retencion en la fuente
            if (!item.getCodigo_retefuente().trim().equals("")){
                
                if (item.getAccount_code_rfte().trim().equals(""))
                    throw new Exception("El codigo de retefuente asociado a la documento ["+ item.getDocumento() +"] no presenta codigo de cuenta");
                
                Comprobantes rfte = 
                generarItem(
                    new String[] 
                    { cab.getDstrct(), 
                      cab.getTipodoc(), 
                      cab.getNumdoc(), 
                      cab.getPeriodo(), 
                      item.getAccount_code_rfte(), 
                      "",
                      "RFTE " + item.getDocumento(),
                      String.valueOf(item.getValor_retefuente()) , 
                      "0", 
                      item.getNit_cliente(), 
                      cab.getDocumento_interno(),  
                      usuario.getLogin(), 
                      TIMESTAMP, 
                      usuario.getBase(), 
                      item.getTipo_doc(), 
                      item.getDocumento()
                    }
                );     
                tdebito   += item.getValor_retefuente();
                taplicado -= item.getValor_retefuente_me();
                detalleComprobante.add(rfte);
            }
            
            // item de retencion de industia y comercio
            if (!item.getCodigo_reteica().trim().equals("")){
                
                if (item.getAccount_code_rica().trim().equals(""))
                    throw new Exception("El codigo de reteica asociado a la documento ["+ item.getDocumento() +"] no presenta codigo de cuenta");
                
                
                Comprobantes rica = 
                generarItem(
                    new String[] 
                    { cab.getDstrct(), 
                      cab.getTipodoc(), 
                      cab.getNumdoc(), 
                      cab.getPeriodo(), 
                      item.getAccount_code_rica(), 
                      "",
                      "RICA " + item.getDocumento(),
                      String.valueOf(item.getValor_reteica()) , 
                      "0", 
                      item.getNit_cliente(), 
                      cab.getDocumento_interno(), 
                      usuario.getLogin(), 
                      TIMESTAMP, 
                      usuario.getBase(), 
                      item.getTipo_doc(), 
                      item.getDocumento()
                    }
                );  
                
                
                
                tdebito   += item.getValor_reteica();
                taplicado -= item.getValor_reteica_me();
                detalleComprobante.add(rica);
            }     
            //fin de probablemente basura

            
            return new double [] { tdebito, tcredito , taplicado };
        }catch (Exception ex){
            //ex.printStackTrace();
            throw new Exception (ex.getMessage());
        }
    }
    
    
    
    
    
    

    
    /***
     * FUNCIONES DEL LOG 
     */
    public void initLOG () throws Exception{
        try{
            String ruta = UtilFinanzas.obtenerRuta("ruta", "/exportar/migracion/" + usuario.getLogin());
            fw = new FileWriter (ruta + "/"+usuario.getBd()+"_CNA_"+ fmt2.format(fechaActual) +".txt");
            pw = new PrintWriter(new BufferedWriter(fw));            
        } catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
    }
    
    public void closeLOG() throws Exception {
        try{
            pw.close();
            fw.close();
        } catch (Exception ex){
            throw new Exception(ex.getMessage());
        }
    }
    
    
  public void writeLOG(String msg)throws Exception {
        try{
            if (pw!=null ){
                String []msgs = msg.split("\\n");
                for (int i = 0; i< msgs.length;i++)
                    pw.println("[" + getCurrentTime() + "] " +  msgs[i] );
                pw.flush();
            }
        } catch (Exception e){
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }
    public void writeLOG2(String msg)throws Exception {
        try{
            if (pw!=null ){
                String []msgs = msg.split("\\n");
                for (int i = 0; i< msgs.length ; i++)
                    pw.println( msgs[i] );
                pw.flush();
            }
        } catch (Exception e){
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }    
    public String getCurrentTime(){
        fechaActual.setTime( System.currentTimeMillis());
        return fmt.format(fechaActual );
    }
   

   
    public Comprobantes generarItem(String []args) throws Exception {
        Comprobantes item = new Comprobantes();
        item.setDstrct           ( args[0]   );
        item.setTipodoc          ( args[1]  );
        item.setNumdoc           ( args[2]  );
        item.setPeriodo          ( args[3]  );
        item.setCuenta           ( args[4]  );
        item.setAuxiliar         ( args[5]  );
        item.setDetalle          ( args[6]  );
        item.setTotal_debito     ( Double.parseDouble(args[7] ) );
        item.setTotal_credito    ( Double.parseDouble(args[8] ) );        
        item.setTercero          ( args[9]  );
        item.setDocumento_interno( args[10]  );
        item.setUsuario          ( args[11] );
        item.setFecha_creacion   ( args[12] );
        item.setBase             ( args[13] );
        item.setTipodoc_rel      ( args[14] );
        item.setNumdoc_rel       ( args[15] );
        return item;
    }
        
    
    public void salvarComprobante(Comprobantes com, Ingreso in) throws Exception{
        try {
            svc.saveComprobante(com, in);
        }catch (Exception ex){
            throw new Exception(ex.getMessage());
        }
    }
    
    
       public String bank(String cta)throws Exception{
        try {
            return (svc.banco(cta));
        }catch (Exception ex){
            throw new Exception(ex.getMessage());
        }
    } 
    
    public String nombcliente(String cta)throws Exception{
        try {
            return (svc.nombcliente(cta)); 
        }catch (Exception ex){
            throw new Exception(ex.getMessage());
        }
    } 
    
    public String nombcuenta(String cta)throws Exception{
        try {
            return (svc.nombcuenta(cta));
        }catch (Exception ex){
            throw new Exception(ex.getMessage());
        }
    } 
    
    
    
    /* *
     * @param args the command line arguments
    
    public static void main(String[] args) throws Exception{
        
        try{
            HContabilizacionNotasAjuste h = new HContabilizacionNotasAjuste();
            h.start("ADMIN", new com.tsp.finanzas.contab.model.services.ContabilizacionNotasAjusteServices()  );
        } catch (Exception ex){
            ex.printStackTrace();
        }
    }
     */
}

