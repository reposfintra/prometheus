    /***************************************
    * Nombre Clase ............. hnegocios.java
    * Descripción  .. . . . . .  contabiliza negocios
    * Autor  . . . . . . . . . . ING ROBERTO ROCHA P
    * Fecha . . . . . . . . . .  10/09/2007
    * versión . . . . . . . . .  1.0
    * Copyright ...FINTRAVALORES SA.
    *******************************************/


package com.tsp.finanzas.contab.model.threads;

import java.io.*;
import java.util.*;
import com.tsp.finanzas.contab.model.*;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.LogProcesosService;
import com.tsp.util.*;


public class HContabilizacionNegocio extends Thread{

    private Model     model;
    private String    procesoName;
    private Usuario   user;
    private String    fechaCorte;


    public HContabilizacionNegocio() {
    }



    public void start(Model modelo, Usuario usuario, String fechaCorte) throws Exception{
         try{
                this.model       = modelo;
                this.user        = usuario;
                this.procesoName = "Contabilización Negocio";
                this.fechaCorte  = fechaCorte;
                super.start();
        }catch(Exception e){
           throw new Exception( e.getMessage() );
       }
    }



     public synchronized void run(){
       LogProcesosService log = new LogProcesosService(user.getBd());
       try{
           String comentario="EXITOSO";
           log.InsertProceso(this.procesoName, this.hashCode(), "Fecha de corte "+ fechaCorte  ,this.user.getLogin() );
           model.ContabilizacionNegSvc.contabilizar( "FINV", fechaCorte , "ADMIN", this.user);
           comentario    =  model.ContabilizacionNegSvc.getESTADO();
           model.ContabilizacionNegSvc.setProcess( false );
           log.finallyProceso(this.procesoName, this.hashCode(), this.user.getLogin(),comentario);
       }catch(Exception e){
           try{
               model.ContabilizacionNegSvc.setProcess( false );
               log.finallyProceso(this.procesoName,this.hashCode(), this.user.getLogin(),"ERROR Hilo: " + e.getMessage()); }
           catch(Exception f){}
       }
    }


}
