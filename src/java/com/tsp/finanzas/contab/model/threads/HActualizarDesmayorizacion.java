/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.finanzas.contab.model.threads;

import com.tsp.finanzas.contab.model.services.MayorizacionService;
import com.tsp.operation.model.LogProcesosService;
import com.tsp.operation.model.beans.Usuario;
import java.io.*;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;

/**
 * HActualizarDesmayorizacion.java<br />
 * Hilo para iniciar el proceso de desmayorizacion<br />
 * 17/02/2010
 * @author  Mgarizao - GEOTECH
 * @version 1.0
 */
public class HActualizarDesmayorizacion extends Thread{

    Usuario usuario;
    MayorizacionService mayorizacionService;
    LogProcesosService  logSvc;
    private ResourceBundle rb;
    private String ruta;
    String proceso;
    String ano;
    String mes;
    File file;
    FileWriter       fw  = null;
    PrintWriter      pw  = null;
    SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss");
    SimpleDateFormat fmt2= new SimpleDateFormat("yyyyMMdd_kkmmss");
    Date             fechaActual = new Date();


     public void start(LogProcesosService logSvc,MayorizacionService mayor,Usuario User,String ano,String mes){
       this.mayorizacionService = mayor;
       this.usuario = User;
       this.logSvc = logSvc;
       this.ano = ano;
       this.mes = mes;
       super.start();
    }

     public synchronized  void run(){


            try{


            rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            this.ruta = rb.getString("ruta") + "/exportar/migracion/" + usuario.getLogin();
            this.file = new File(ruta);
            this.file.mkdirs();
            this.proceso = "Actualizar Desmayorizacion";
            fw = new FileWriter(ruta + "/Logresultado" +ano + mes + ".txt");
            pw = new PrintWriter(new BufferedWriter(fw));


            try {


                writeLog("*************************************************");
                writeLog("* Inicio del Proceso de Desmayorizacion *");
                writeLog("*************************************************");
                Procesar();
                writeLog("*************************************************");
                writeLog("*               Proceso Finalizado              *");
                writeLog("*************************************************");
            } catch (Exception ex) {
                ex.printStackTrace();
                writeLog("*************************************************");
                writeLog("*    Finalizando Proceso pero con anomalias     *");
                writeLog("*************************************************");
                writeLog(ex.getMessage());
            }


              pw.close();
            }catch (Exception ex){

                ex.printStackTrace();


            }

    }



     /**
      * metodo para procesar la desmayorizacion
      * @version MGarizao - GEOTECH
      * @date 17/02/2010
      * @version 1.0
      * @throws Exception
      */
     public void Procesar() throws Exception{
        try {
            logSvc.InsertProceso(this.proceso, this.hashCode(), "Proceso de Desmayorizacion", usuario.getLogin());
            String msn = mayorizacionService.obtenerProcesoDesMayorizacion(usuario, mes, ano);

             writeLog2("");
             writeLog2("");
             writeLog2("");
             writeLog2("RESUMEN : ");
             writeLog2("Proceso de DESMAYORIZACION             : " + msn   );
             writeLog2("");
             logSvc.finallyProceso(this.proceso, this.hashCode(), usuario.getLogin(), msn);

              

        }catch (SQLException ex) {

            ex.printStackTrace();
            writeLog2   ( ex.getMessage() );
            logSvc.finallyProceso(this.proceso, this.hashCode(), this.usuario.getLogin(), "Finalizado con error ..");
            throw new Exception(ex.getMessage());
        }

     }



     /**
      * metodo para escribir en el archivo .txt
      * @param msg mensaje del proceso
      * @author MGarizao - GEOTECH
      * @date 17/02/2010
      * @version 1.0
      * @throws Exception
      */
      public void writeLog(String msg)throws Exception {
        try{
            if (pw!=null ){
                String []msgs = msg.split("\\n");
                for (int i = 0; i< msgs.length;i++)
                    pw.println("[" + getCurrentTime() + "] " +  msgs[i] );
                pw.flush();
            }
        } catch (Exception e){
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }


     /**
      * metodo para escribir los parrafos durante el proceso  en el archivo .txt
      * @param msg mensaje del proceso
      * @author MGarizao - GEOTECH
      * @date 17/02/2010
      * @version 1.0
      * @throws Exception
      */
     public void writeLog2(String msg)throws Exception {
        try{
            if (pw!=null ){

                pw.println( msg);
                pw.flush();
            }
        } catch (Exception e){
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }

     /**
      * metodo para obtener la fecha y hora actual
      * @author MGarizao - GEOTECH
      * @date 17/02/2010
      * @version 1.0
      * @return
      */
     public String getCurrentTime(){
        fechaActual.setTime( System.currentTimeMillis());
        return fmt.format(fechaActual );
    }

}
