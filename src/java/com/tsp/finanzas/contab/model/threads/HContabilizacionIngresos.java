/*
 * HContabilizacionIngresos.java
 *
 * Created on 26 de febrero de 2007, 10:19 AM
 */

package com.tsp.finanzas.contab.model.threads;


import com.tsp.finanzas.contab.model.services.ContabilizacionIngresosServices;
import com.tsp.finanzas.contab.model.beans.Comprobantes;
import com.tsp.operation.model.LogProcesosService;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.beans.Ingreso;
import com.tsp.operation.model.beans.Ingreso_detalle;
import com.tsp.operation.model.beans.factura;
import org.apache.log4j.Logger;

import com.tsp.util.*;
import java.util.*;
import java.lang.*;
import java.text.*;
import java.io.*;

/**
 *
 * @author  equipo
 */
public class HContabilizacionIngresos extends Thread{

    TreeMap        equivalencias = null;
    String         periodo   = "";
    String         DATE      = "";
    String         TIMESTAMP = "";
    Usuario        usuario;
    String         processName = "ContabilizacionIngreso";
    final String   CUENTA_DIF_CAMBIO = "G01C00910727";
    String ccuenta = "";
    String cuenta = "";
    ContabilizacionIngresosServices svc;
    LogProcesosService logp;
    Logger  logger = Logger.getLogger(HContabilizacionIngresos.class);

    FileWriter       fw  = null;
    PrintWriter      pw  = null;
    SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss");
    SimpleDateFormat fmt2= new SimpleDateFormat("yyyyMMdd_kkmmss");
    Date             fechaActual = new Date();



    /** Creates a new instance of HContabilizacionIngresos */
    public HContabilizacionIngresos() {
        super("Contabilizacion de Ingresos");

    }

    /*public void start (Usuario usuario) throws Exception{
        this.usuario = usuario;
        init();
        super.start();
    }*/

    public void start (Usuario usuario, ContabilizacionIngresosServices svc) throws Exception{

        if (usuario==null || usuario.getLogin().equals(""))
            throw new Exception("Debe definir el usuario de contabilizacion para poder continuar");


        this.usuario = usuario;
        this.svc = svc;
        init();
        super.start();
    }

    public void init() throws Exception{
        try {
            if (svc==null)
                throw new Exception("No se ha inicializado el Service de Contabilizacion de Ingresos");


            //svc = new ContabilizacionIngresosServices();
            logp          = new LogProcesosService(usuario.getBd());
            equivalencias = svc.obtenerEquivalenciaTipoDoc();
            Date date     = new Date();
            DATE          = UtilFinanzas.customFormatDate(date, "yyyy-MM-dd");
            TIMESTAMP     = UtilFinanzas.customFormatDate(date, "yyyy-MM-dd HH:mm:ss");

            // para contabilizar con datos inferiores al dia anterior
            date.setDate( date.getDate() -1 );
            periodo       = UtilFinanzas.customFormatDate(date, "yyyyMM");


        } catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
    }





    public synchronized void run() {
        try{
            // inicializando log de contabilizacion
            initLOG();
            writeLOG("INICIO CONTABILIZACION DE INGRESOS.");
            logp.InsertProceso(this.processName, this.hashCode(), "Contabilizacion Ingresos", usuario.getLogin());

            svc.setProcessRun(true);

            try{

                // contabiliazaciones
                this.IngresosSinContabilizar();

                // descontabilizaciones
                this.IngresosSinDescontabilizar();



            } catch (Exception e){
                e.printStackTrace();
                writeLOG (e.getMessage());
            }

            writeLOG("PROCESO FINALIZADO.");
            closeLOG();
            logp.finallyProceso(this.processName, this.hashCode(), usuario.getLogin(), "Proceso terminado, verifique el log de contabilizacion");


        }catch (Exception ex){

            try{
                logp.finallyProceso(this.processName, this.hashCode(), usuario.getLogin(), ex.getMessage());
            } catch (Exception e){}

        } finally {
            svc.setProcessRun(false);
        }

    }


    public void IngresosSinContabilizar() throws Exception {
        Vector ingresos = svc.obtenerIngresoSinContabilizar();
        if (ingresos!=null && !ingresos.isEmpty()){
            writeLOG("INGRESOS PENDIENTE POR CONTABILIZAR : " + ingresos.size() );
            int contabilizados = 0;
            for (int i = 0;  i < ingresos.size(); i++ ){
                Ingreso in = (Ingreso) ingresos.get(i);
                Vector detalles = svc.obtenerDetalleIngreso(in);
                if (in.getTipo_documento().equalsIgnoreCase("ING") || in.getTipo_documento().equalsIgnoreCase("ICR")|| in.getTipo_documento().equalsIgnoreCase("ICA")){
                    if ( (in.getNum_ingreso().startsWith("RI") || in.getNum_ingreso().startsWith("RC") ) && contabilizarRI_RC(in, detalles))
                        contabilizados++;
                    else if ( !in.getNum_ingreso().startsWith("RI") && contabilizar(in, detalles))
                        contabilizados++;
                }
            }
            writeLOG("RESUMEN");
            writeLOG("\tContabilizados    : " + contabilizados);
            writeLOG("\tNo contabilizados : " + (ingresos.size() - contabilizados));
        }else {
            writeLOG("INGRESOS PENDIENTE POR CONTABILIZAR : " + 0 );
        }
        writeLOG("");
        writeLOG("");
        writeLOG("");
    }



    public void IngresosSinDescontabilizar() throws Exception {
        Vector ingresos = svc.obtenerIngresoSinDescontabilizar();
        if (ingresos!=null && !ingresos.isEmpty()){
            writeLOG("INGRESOS PENDIENTES POR DESCONTABILIZAR : " + ingresos.size() );
            int descontabilizados = 0;
            for (int i = 0;  i < ingresos.size(); i++ ){
                Ingreso in = (Ingreso) ingresos.get(i);

                if (in.getTipo_documento().equalsIgnoreCase("ING") || in.getTipo_documento().equalsIgnoreCase("ICR"))
                    if(descontabilizar(in))
                        descontabilizados++;
            }
            writeLOG("RESUMEN");
            writeLOG("\tDescontabilizados    : " + descontabilizados);
            writeLOG("\tNo descontabilizados : " + (ingresos.size() - descontabilizados));
        }else {
            writeLOG("INGRESOS PENDIENTE POR DESCONTABILIZAR : " + 0 );
        }
        writeLOG("");
        writeLOG("");
        writeLOG("");
    }



    /**
     * Contabilizacion de un ingreso
     * @param mfontalvo
     */
    private boolean contabilizar(Ingreso in, Vector detalles) throws Exception {

        try{

            if (in == null)
                throw new Exception("Ingreso no valido");


            String equivalencia = (String) equivalencias.get( in.getTipo_documento() );
            if (equivalencia == null)
                throw new Exception("No tiene equivalencia contable el tipo de documento");

            String des="";
            String auxi="";
            String ter="";
            int sw=0;
            if (in.getTipo_documento().equalsIgnoreCase("ING")){
                if (in.getCuenta_banco().trim().equals(""))
                    throw new Exception("Se requiere la cuenta del banco");
                cuenta = in.getCuenta_banco();
                des= bank(cuenta);
            } else if (in.getTipo_documento().equalsIgnoreCase("ICR")){
                if (in.getCuenta().trim().equals(""))
                    throw new Exception("Se requiere la cuenta de la nota credito");
                auxi="RD-8901009858";
                ter="8901009858";
                if(in.getCuenta_cliente().equals("16252001")){
                    cuenta = "16252001";
                    des="FENALCO TEMP FIDUCIA";
                }else{
                    cuenta = "13109601";
                    des="FENALCO";
                }
            }else if(in.getTipo_documento().equalsIgnoreCase("ICA"))
            {
                if(in.getVlr_ingreso() < 0)
                {
                    sw=1;
                    in.setVlr_ingreso(in.getVlr_ingreso()*-1);
                }
                if(in.getCuenta().equals("G010010015335"))
                {
                    cuenta=in.getCuenta();
                    des="AJUSTE VALOR REAL";
                    auxi="";
                    ter=in.getNitcli();
                    ccuenta=in.getCuenta_cliente();
                }
                else
                {
                     if(in.getCuenta().equals("I010010014205") || in.getCuenta().equals("I010010014169"))
                     {
                        cuenta=in.getCuenta_cliente();
                        des=in.getNomCliente();
                        auxi="RD-"+in.getNitcli();
                        ter=in.getNitcli();
                        ccuenta=in.getCuenta();
                     }
                     else
                     {
                        if (sw==0)
                        {//es positivo y no es de fenalco
                            cuenta=in.getCuenta();
                            des=nombcuenta(in.getCuenta());
                            auxi="";
                            ter=in.getNitcli();
                            ccuenta=in.getCuenta_cliente();
                        }
                        else
                        {
                           cuenta=in.getCuenta_cliente(); //hc
                           des=nombcuenta(in.getCuenta_cliente());
                           auxi="";
                           ter=in.getNitcli();
                           ccuenta=in.getCuenta();
                        }
                     }
                }

            }

            // los ingresos miscelaneos requieren items para contabilizarse
            if ( in.getTipo_ingreso().equalsIgnoreCase("M")){
                if (detalles == null || detalles.isEmpty() )
                    throw new Exception("Se requieren los items en los ingresos miscelaneos para poder contabilizarlos");
            }


            Comprobantes com = new Comprobantes();
            com.setDstrct        ("FINV");
            com.setTipodoc       ( equivalencia );
            com.setNumdoc        ( in.getNum_ingreso() );
            com.setSucursal      ( "" );
            com.setPeriodo       ( periodo );
            com.setFechadoc      ( DATE );
            com.setDetalle       ( in.getDescripcion_ingreso() );
            com.setTercero       ( in.getNitcli() );
            com.setTotal_debito  ( 0 );
            com.setTotal_credito ( 0 );
            com.setTotal_items   ( 0 );
            com.setMoneda        ( "PES" );
            com.setUsuario       ( usuario.getLogin() );
            com.setFecha_creacion( TIMESTAMP          );
            com.setBase          ( usuario.getBase()  );
            com.setTipo_operacion( in.getConcepto()   );
            com.setDocumento_interno( in.getTipo_documento() );

            Vector detalle = new Vector();
            Comprobantes it1 = new Comprobantes();


            // item del banco o la nota credito
            Comprobantes banco =
                generarItem(
                    new String[]
                    { com.getDstrct(),
                      equivalencia,
                      in.getNum_ingreso(),
                      com.getPeriodo(),
                      cuenta,
                      auxi,
                      des,//NOMBRE BANCO U OTRO EN CASO DE SER NC
                      String.valueOf(in.getVlr_ingreso()),
                      "0" ,
                      ter,
                      com.getDocumento_interno(),
                      usuario.getLogin(),
                      TIMESTAMP,
                      usuario.getBase(),
                      "",
                      ""
                    }
                );
            detalle.add( banco );
            com.setTotal_debito( com.getTotal_debito() + in.getVlr_ingreso() );
            com.setLista( detalle );


            // ITEMS DEL INGRESO
            double taplicado = 0;
            for (int i = 0; i < detalles.size(); i++){
                Ingreso_detalle item = (Ingreso_detalle) detalles.get(i);

                if ( !item.isAnulado() ){
                    double []total = generarItemsComprobante_X_ItemIngreso( com, item,com.getLista() );
                    com.setTotal_debito ( com.getTotal_debito()  + total[0] );
                    com.setTotal_credito( com.getTotal_credito() + total[1] );
                    taplicado +=  total[2];
                }
            }


            // solo para los ingresos de clientes
            double saldo  = in.getVlr_ingreso_me() - taplicado;
            double vsaldo = Util.roundByDecimal( saldo * in.getVlr_tasa() , 0);
            if (in.getTipo_ingreso().equalsIgnoreCase("C") && saldo!=0){

                 if ( in.getCuenta_cliente().equals(""))
                     throw new Exception("Se requiere codigo de cuenta del cliente para asignar saldo.");

                Comprobantes csaldo =
                generarItem(
                    new String[]
                    { com.getDstrct(),
                      equivalencia,
                      in.getNum_ingreso(),
                      com.getPeriodo(),
                      in.getCuenta_cliente(),
                      "",
                      "SALDO INGRESO",
                      "0",
                      String.valueOf( vsaldo ),
                      in.getNitcli(),
                      com.getDocumento_interno(),
                      usuario.getLogin(),
                      TIMESTAMP,
                      usuario.getBase(),
                      "",
                      ""
                    }
                );
                detalle.add(csaldo);
                com.setTotal_credito( com.getTotal_credito() + vsaldo );
            }
            else if (in.getTipo_ingreso().equalsIgnoreCase("M") && saldo!=0){
                throw new Exception("El ingreso miscelaneo no esta aplicado totalmente.");
            }

            //System.out.println( in.getNum_ingreso() );
            //System.out.println(com.getTotal_debito() + "     " + com.getTotal_credito());


            double dif_final = com.getTotal_debito() - com.getTotal_credito();
            //System.out.println("DIF " + dif_final);

            if ( dif_final != 0) {
                Comprobantes diferencia =
                    generarItem(
                        new String[]
                        { com.getDstrct(),
                          equivalencia,
                          in.getNum_ingreso(),
                          com.getPeriodo(),
                          this.CUENTA_DIF_CAMBIO,
                          "",
                          "DIF.EN.CAMBIO",
                          String.valueOf( (dif_final < 0 ? -dif_final : 0 )),
                          String.valueOf( (dif_final > 0 ?  dif_final : 0 )),
                          in.getNitcli(),
                          com.getDocumento_interno(),
                          usuario.getLogin(),
                          TIMESTAMP,
                          usuario.getBase(),
                          "",
                          ""
                        }
                    );
                detalle.add(diferencia);
                com.setTotal_debito ( com.getTotal_debito()  + diferencia.getTotal_debito()  );
                com.setTotal_credito( com.getTotal_credito() + diferencia.getTotal_credito() );
            }


            if (com.getTotal_debito() != com.getTotal_credito())
                throw new Exception("El total debito es diferente al total credito");




            com.setTotal_items( detalle.size() );
            salvarComprobante(com, in);
            writeLOG("["+ in.getTipo_documento() +"] " + in.getNum_ingreso() + ":\t CONTABILIZADO ");
            return true;


        } catch (Exception ex){
            if (in!=null)
                writeLOG("["+ in.getTipo_documento() +"] " + in.getNum_ingreso() + ":\t " + ex.getMessage());
            else
                writeLOG(ex.getMessage());
            return false;
        }
    }




  /**
     * Contabilizacion de un ingreso
     * @param mfontalvo
     */
    private boolean contabilizarRI_RC(Ingreso in, Vector detalles) throws Exception {

        try{

            if (in == null)
                throw new Exception("Ingreso no valido");


            String equivalencia = (String) equivalencias.get( in.getTipo_documento() );
            if (equivalencia == null)
                throw new Exception("No tiene equivalencia contable el tipo de documento");



            if ( in.getCuenta_cliente().equals(""))
                throw new Exception("Se requiere codigo de cuenta del cliente para asignar debito.");


            // los ingresos miscelaneos requieren items para contabilizarse
            if ( in.getTipo_ingreso().equalsIgnoreCase("M"))
                throw new Exception("Los ingresos miscelaneos no aplican al tipo de ingresos 'RI'.");


            Comprobantes com = new Comprobantes();
            com.setDstrct        ("FINV");
            com.setTipodoc       ( equivalencia );
            com.setNumdoc        ( in.getNum_ingreso() );
            com.setSucursal      ( "" );
            com.setPeriodo       ( periodo );
            com.setFechadoc      ( DATE );
            com.setDetalle       ( in.getDescripcion_ingreso() );
            com.setTercero       ( in.getNitcli() );
            com.setTotal_debito  ( 0 );
            com.setTotal_credito ( 0 );
            com.setTotal_items   ( 0 );
            com.setMoneda        ( "PES" );
            com.setUsuario       ( usuario.getLogin() );
            com.setFecha_creacion( TIMESTAMP          );
            com.setBase          ( usuario.getBase()  );
            com.setTipo_operacion( in.getConcepto()   );
            com.setDocumento_interno( in.getTipo_documento() );

            Vector detalle = new Vector();
            Comprobantes it1 = new Comprobantes();


            // item del banco o la nota credito
            Comprobantes banco =
            generarItem(
                new String[]
                { com.getDstrct(),
                  equivalencia,
                  in.getNum_ingreso(),
                  com.getPeriodo(),
                   in.getCuenta_cliente(),
                  "",
                  "INGRESO",
                  String.valueOf(in.getVlr_ingreso()),
                  "0" ,
                  in.getNitcli(),
                  com.getDocumento_interno(),
                  usuario.getLogin(),
                  TIMESTAMP,
                  usuario.getBase(),
                  "",
                  ""
                }
            );
            detalle.add( banco );
            com.setTotal_debito( com.getTotal_debito() + in.getVlr_ingreso() );
            com.setLista( detalle );


            // ITEMS DEL INGRESO
            double taplicado = 0;
            for (int i = 0; i < detalles.size(); i++){
                Ingreso_detalle item = (Ingreso_detalle) detalles.get(i);

                if ( !item.isAnulado() ){
                    double []total = generarItemsComprobante_X_ItemIngreso( com, item,com.getLista() );
                    com.setTotal_debito ( com.getTotal_debito()  + total[0] );
                    com.setTotal_credito( com.getTotal_credito() + total[1] );
                    taplicado +=  total[2];
                }
            }


            // solo para los ingresos de clientes
            double saldo  = in.getVlr_ingreso_me() - taplicado;
            double vsaldo = Util.roundByDecimal( saldo * in.getVlr_tasa() , 0);
            if (in.getTipo_ingreso().equalsIgnoreCase("C") && saldo!=0){

                 if ( in.getCuenta_cliente().equals(""))
                     throw new Exception("Se requiere codigo de cuenta del cliente para asignar saldo.");

                Comprobantes csaldo =
                generarItem(
                    new String[]
                    { com.getDstrct(),
                      equivalencia,
                      in.getNum_ingreso(),
                      com.getPeriodo(),
                      in.getCuenta_cliente(),
                      "",
                      "SALDO INGRESO",
                      "0",
                      String.valueOf( vsaldo ),
                      in.getNitcli(),
                      com.getDocumento_interno(),
                      usuario.getLogin(),
                      TIMESTAMP,
                      usuario.getBase(),
                      "",
                      ""
                    }
                );
                detalle.add(csaldo);
                com.setTotal_credito( com.getTotal_credito() + vsaldo );
            }
            else if (in.getTipo_ingreso().equalsIgnoreCase("M") && saldo!=0){
                throw new Exception("El ingreso miscelaneo no esta aplicado totalmente.");
            }

            //System.out.println( in.getNum_ingreso() );
            //System.out.println(com.getTotal_debito() + "     " + com.getTotal_credito());


            double dif_final = com.getTotal_debito() - com.getTotal_credito();
            //System.out.println("DIF " + dif_final);

            if ( dif_final != 0) {
                Comprobantes diferencia =
                    generarItem(
                        new String[]
                        { com.getDstrct(),
                          equivalencia,
                          in.getNum_ingreso(),
                          com.getPeriodo(),
                          this.CUENTA_DIF_CAMBIO,
                          "",
                          "DIF.EN.CAMBIO",
                          String.valueOf( (dif_final < 0 ? -dif_final : 0 )),
                          String.valueOf( (dif_final > 0 ?  dif_final : 0 )),
                          in.getNitcli(),
                          com.getDocumento_interno(),
                          usuario.getLogin(),
                          TIMESTAMP,
                          usuario.getBase(),
                          "",
                          ""
                        }
                    );

                detalle.add(diferencia);
                com.setTotal_debito ( com.getTotal_debito()  + diferencia.getTotal_debito()  );
                com.setTotal_credito( com.getTotal_credito() + diferencia.getTotal_credito() );
            }


            if (com.getTotal_debito() != com.getTotal_credito())
                throw new Exception("El total debito es diferente al total credito");




            com.setTotal_items( detalle.size() );
            salvarComprobante(com, in);
            writeLOG("["+ in.getTipo_documento() +"] " + in.getNum_ingreso() + ":\t CONTABILIZADO ");
            return true;


        } catch (Exception ex){
            if (in!=null)
                writeLOG("["+ in.getTipo_documento() +"] " + in.getNum_ingreso() + ":\t " + ex.getMessage());
            else
                writeLOG(ex.getMessage());
            return false;
        }
    }



    /**
     * Metodo para descontabilizar un ingreso
     * @param ingreso, ingreso a descontabilizar
     * @throws Exception.
     * @return boolean, estado del proceso de descontabilizacion.
     */
    private boolean descontabilizar(Ingreso in) throws Exception {
        try{
            if (in==null)
                throw new Exception("Ingreso no valido.");

            if (!in.getReg_status().equalsIgnoreCase("A"))
                throw new Exception("El ingreso no esta anulado para descontabilizarlos");

            if (in.getFecha_contabilizacion().equals("0099-01-01 00:00:00"))
                throw new Exception("El ingreso no esta contabilizado para descontabilizarlo");

            Comprobantes com = svc.getComprobante( in.getTipo_documento(), in.getNum_ingreso(), in.getTransaccion() );
            if (com == null)
                throw new Exception("No se encontro el comprobante ["+ in.getTransaccion() +"], asociado al ingreso");

            Vector detalles = com.getLista();
            if (detalles == null || detalles.isEmpty())
                throw new Exception("El comprobante ["+ in.getTransaccion() +"] no posee items");


            com.setDetalle       ( "DESCONTABILIZACION INGRESO" );
            com.setUsuario       ( usuario.getLogin() );
            com.setFecha_creacion( TIMESTAMP          );
            com.setBase          ( usuario.getBase()  );

            for (int i = 0; i<detalles.size(); i++){
                Comprobantes c = (Comprobantes) detalles.get(i);
                double debito  = c.getTotal_debito();
                c.setTotal_debito ( c.getTotal_credito() );
                c.setTotal_credito( debito               );

                c.setUsuario       ( com.getUsuario()        );
                c.setFecha_creacion( com.getFecha_creacion() );
                c.setBase          ( com.getBase()           );
            }

            salvarComprobante(com, in);
            writeLOG("["+ in.getTipo_documento() +"] " + in.getNum_ingreso() + ":\t DESCONTABILIZADO ");
            return true;

        } catch (Exception ex){
            if (in!=null)
                writeLOG("["+ in.getTipo_documento() +"] " + in.getNum_ingreso() + ":\t " + ex.getMessage());
            else
                writeLOG(ex.getMessage());
            return false;
        }
    }
























    public double[] generarItemsComprobante_X_ItemIngreso (Comprobantes cab, Ingreso_detalle item, Vector detalleComprobante ) throws Exception{

        double tdebito   = 0;
        double tcredito  = 0;
        double taplicado = 0;
        try {
            /** prametros generacion de item
                    item.setDstrct           ( args[0]   );
                    item.setTipodoc          ( args[1]  );
                    item.setNumdoc           ( args[2]  );
                    item.setPeriodo          ( args[3]  );
                    item.setCuenta           ( args[4]  );
                    item.setAuxiliar         ( args[5]  );
                    item.setDetalle          ( args[6]  );
                    item.setTotal_debito     ( Double.parseDouble(args[7] ) );
                    item.setTotal_credito    ( Double.parseDouble(args[8] ) );
                    item.setTercero          ( args[9]  );
                    item.setDocumento_interno( args[10]  );
                    item.setUsuario          ( args[11] );
                    item.setFecha_creacion   ( args[12] );
                    item.setBase             ( args[13] );
                    item.setTipodoc_rel      ( args[14] );
                    item.setNumdoc_rel       ( args[15] );
             */


            if (item == null)
                throw new Exception ("Item de ingreso no valido");

            // deudores varios
            if (item.getCuenta().equals(""))
                throw new Exception ("El item ["+ item.getItem() +"] no presenta codigo contable.");



            double vdeudor     = item.getValor_ingreso();
            double vdiferencia = item.getValor_diferencia();

            if (item.getTipo_doc().equalsIgnoreCase("FAC") ) {
                if (!item.getDocumento().trim().equals("")){
                    factura fact = svc.obtenerDatosFactura(item);

                    if (fact==null)
                        throw new Exception("La factura que asocio al item del ingreso no esta regitrada en la base de datos");

                        vdeudor = vdeudor - Math.abs(vdiferencia);

                } //else
                   // throw new Exception("El item indica que tiene una factura relacionada(tipo_doc), pero no define el numero de la misma(documento)");
            }


             if(!(cab.getTipodoc().equalsIgnoreCase("ICA"))){
                 String cuent ="13109602";
                 if(item.getCuenta().equals("16252002")){
                    cuent = "16252002";
                 }

                 if(item.getCuenta().equals("13109902")){
                    cuent = "13109902";
                 }
                 
                Comprobantes deudor =
                generarItem(
                    new String[]
                    { cab.getDstrct(),
                      cab.getTipodoc(),
                      cab.getNumdoc(),
                      cab.getPeriodo(),
                      ((cab.getTipodoc()).equals("ING"))?item.getCuenta():cuent ,   //Mod Tmolina 09-08-2008
                      item.getAuxiliar(),
                      ((item.getDocumento()).equals(""))?nombcuenta(item.getCuenta()):"PAGO " + item.getDocumento()+" "+ nombcliente(item.getNit_cliente()),
                      String.valueOf( vdeudor < 0 ? vdeudor*-1 : 0 ),
                      String.valueOf( vdeudor > 0 ? vdeudor : 0 ),
                      item.getNit_cliente(),
                      cab.getDocumento_interno(),
                      usuario.getLogin(),
                      TIMESTAMP,
                      usuario.getBase(),
                      item.getTipo_doc(),
                      item.getDocumento()
                    }
                );
                tdebito   += deudor.getTotal_debito();
                tcredito  += deudor.getTotal_credito();
                taplicado += item.getValor_ingreso_me();
                detalleComprobante.add(deudor);
            }
            else
            {
                Comprobantes comproica =
                generarItem(
                    new String[]
                    { cab.getDstrct(),
                      cab.getTipodoc(),
                      cab.getNumdoc(),
                      cab.getPeriodo(),
                      (( cuenta.equals("G010010015335")))?"13109602":ccuenta,
                      (( cuenta.equals("G010010015335")))?item.getAuxiliar():"",
                      (( cuenta.equals("G010010015335")))?"Ajuste " + item.getDocumento()+" "+ nombcliente(item.getNit_cliente()):nombcuenta(ccuenta),
                      String.valueOf(  0 ),
                      String.valueOf( vdeudor < 0 ? vdeudor*-1 : vdeudor ),
                      item.getNit_cliente(),
                      cab.getDocumento_interno(),
                      usuario.getLogin(),
                      TIMESTAMP,
                      usuario.getBase(),
                      item.getTipo_doc(),
                      item.getDocumento()
                    }
                );
                tdebito   += comproica.getTotal_debito();
                tcredito  += comproica.getTotal_credito();
                taplicado += item.getValor_ingreso_me();
                detalleComprobante.add(comproica);

            }


            if ( vdiferencia != 0) {
                Comprobantes diferencia =
                    generarItem(
                        new String[]
                        { cab.getDstrct(),
                          cab.getTipodoc(),
                          cab.getNumdoc(),
                          cab.getPeriodo(),
                          this.CUENTA_DIF_CAMBIO,
                          "" ,
                          "DIF.EN.CAMBIO " + item.getDocumento(),
                          String.valueOf( 0 ),
                          String.valueOf( Math.abs(vdiferencia) ),
                          item.getNit_cliente(),
                          cab.getDocumento_interno(),
                          usuario.getLogin(),
                          TIMESTAMP,
                          usuario.getBase(),
                          item.getTipo_doc(),
                          item.getDocumento()
                        }
                    );
                tdebito   += diferencia.getTotal_debito();
                tcredito  += diferencia.getTotal_credito();
                detalleComprobante.add(diferencia);
            }


            // item de retencion en la fuente
            if (!item.getCodigo_retefuente().trim().equals("")){

                if (item.getAccount_code_rfte().trim().equals(""))
                    throw new Exception("El codigo de retefuente asociado a la documento ["+ item.getDocumento() +"] no presenta codigo de cuenta");

                Comprobantes rfte =
                generarItem(
                    new String[]
                    { cab.getDstrct(),
                      cab.getTipodoc(),
                      cab.getNumdoc(),
                      cab.getPeriodo(),
                      item.getAccount_code_rfte(),
                      "",
                      "RFTE " + item.getDocumento(),
                      String.valueOf(item.getValor_retefuente()) ,
                      "0",
                      item.getNit_cliente(),
                      cab.getDocumento_interno(),
                      usuario.getLogin(),
                      TIMESTAMP,
                      usuario.getBase(),
                      item.getTipo_doc(),
                      item.getDocumento()
                    }
                );
                tdebito   += item.getValor_retefuente();
                taplicado -= item.getValor_retefuente_me();
                detalleComprobante.add(rfte);
            }

            // item de retencion de industia y comercio
            if (!item.getCodigo_reteica().trim().equals("")){

                if (item.getAccount_code_rica().trim().equals(""))
                    throw new Exception("El codigo de reteica asociado a la documento ["+ item.getDocumento() +"] no presenta codigo de cuenta");


                Comprobantes rica =
                generarItem(
                    new String[]
                    { cab.getDstrct(),
                      cab.getTipodoc(),
                      cab.getNumdoc(),
                      cab.getPeriodo(),
                      item.getAccount_code_rica(),
                      "",
                      "RICA " + item.getDocumento(),
                      String.valueOf(item.getValor_reteica()) ,
                      "0",
                      item.getNit_cliente(),
                      cab.getDocumento_interno(),
                      usuario.getLogin(),
                      TIMESTAMP,
                      usuario.getBase(),
                      item.getTipo_doc(),
                      item.getDocumento()
                    }
                );
                tdebito   += item.getValor_reteica();
                taplicado -= item.getValor_reteica_me();
                detalleComprobante.add(rica);
            }



            return new double [] { tdebito, tcredito , taplicado };
        }catch (Exception ex){
            //ex.printStackTrace();
            throw new Exception (ex.getMessage());
        }
    }








    /***
     * FUNCIONES DEL LOG
     */
    public void initLOG () throws Exception{
        try{
            String ruta = UtilFinanzas.obtenerRuta("ruta", "/exportar/migracion/" + usuario.getLogin());
            fw = new FileWriter (ruta + "/"+usuario.getBd()+"_LCI_"+ fmt2.format(fechaActual) +".txt");
            pw = new PrintWriter(new BufferedWriter(fw));
        } catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
    }

    public void closeLOG() throws Exception {
        try{
            pw.close();
            fw.close();
        } catch (Exception ex){
            throw new Exception(ex.getMessage());
        }
    }


  public void writeLOG(String msg)throws Exception {
        try{
            if (pw!=null ){
                String []msgs = msg.split("\\n");
                for (int i = 0; i< msgs.length;i++)
                    pw.println("[" + getCurrentTime() + "] " +  msgs[i] );
                pw.flush();
            }
        } catch (Exception e){
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }
    public void writeLOG2(String msg)throws Exception {
        try{
            if (pw!=null ){
                String []msgs = msg.split("\\n");
                for (int i = 0; i< msgs.length ; i++)
                    pw.println( msgs[i] );
                pw.flush();
            }
        } catch (Exception e){
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }
    public String getCurrentTime(){
        fechaActual.setTime( System.currentTimeMillis());
        return fmt.format(fechaActual );
    }



    public Comprobantes generarItem(String []args) throws Exception {
        Comprobantes item = new Comprobantes();
        item.setDstrct           ( args[0]   );
        item.setTipodoc          ( args[1]  );
        item.setNumdoc           ( args[2]  );
        item.setPeriodo          ( args[3]  );
        item.setCuenta           ( args[4]  );
        item.setAuxiliar         ( args[5]  );
        item.setDetalle          ( args[6]  );
        item.setTotal_debito     ( Double.parseDouble(args[7] ) );
        item.setTotal_credito    ( Double.parseDouble(args[8] ) );
        item.setTercero          ( args[9]  );
        item.setDocumento_interno( args[10]  );
        item.setUsuario          ( args[11] );
        item.setFecha_creacion   ( args[12] );
        item.setBase             ( args[13] );
        item.setTipodoc_rel      ( args[14] );
        item.setNumdoc_rel       ( args[15] );
        return item;
    }


    public void salvarComprobante(Comprobantes com, Ingreso in) throws Exception{
        try {
            svc.saveComprobante(com, in);
        }catch (Exception ex){
            throw new Exception(ex.getMessage());
        }
    }


       public String bank(String cta)throws Exception{
        try {
            return (svc.banco(cta));
        }catch (Exception ex){
            throw new Exception(ex.getMessage());
        }
    }

    public String nombcliente(String cta)throws Exception{
        try {
            return (svc.nombcliente(cta));
        }catch (Exception ex){
            throw new Exception(ex.getMessage());
        }
    }

    public String nombcuenta(String cta)throws Exception{
        try {
            return (svc.nombcuenta(cta));
        }catch (Exception ex){
            throw new Exception(ex.getMessage());
        }
    }



    /*
     * @param args the command line arguments
     
    public static void main(String[] args) throws Exception{

        try{
            HContabilizacionIngresos h = new HContabilizacionIngresos();
            h.start("ADMIN", new com.tsp.finanzas.contab.model.services.ContabilizacionIngresosServices()  );
        } catch (Exception ex){
            ex.printStackTrace();
        }
    }
    */
}
