/*
 * Nombre        HContabilizacion.java
 * Descripci�n   Contabiliza los documentos como planilla.
 * Autor         kreales
 * Fecha         05 de junio
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */

package com.tsp.finanzas.contab.model.threads;

import com.tsp.util.*;
import com.tsp.finanzas.contab.model.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.beans.*;
import com.tsp.finanzas.contab.model.beans.*;
import java.io.*;
import java.sql.*;
import java.util.*;
import java.lang.*;
import javax.servlet.jsp.*;
import javax.servlet.http.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import javax.servlet.http.*;
import com.tsp.util.LogWriter;

public class HContabilizacionPlanilla extends Thread{
    
    private   String usuario;
    private PrintWriter pw;
    private LogWriter logWriter;
    java.text.SimpleDateFormat anomes;
    java.text.SimpleDateFormat fecha;
    java.text.SimpleDateFormat fechaHora;
    String periodo;
    String fechadoc;
    String fechaInicio;
    java.util.Date pdate;
    java.util.Date ahora;
    String tipodoc;
    String moneda;
    public HContabilizacionPlanilla() {
    }
    
    /**
     * Establece las variables iniciales y activa el inicio del hilo
     * @param user El usuario en sesi�n
     * @throws Exception Si algun error ocurre en el acceso a la BD
     */
    public void start( String user, String tipo, String moneda ) throws Exception{
        /*
         *Busco la fecha actual y le resto un dia.
         */
        Calendar hoy = Calendar.getInstance();
        hoy.add(hoy.DATE, -1);
        
        pdate = hoy.getTime();
        ahora = new java.util.Date();
        
        anomes = new java.text.SimpleDateFormat("yyyyMM");
        fecha = new java.text.SimpleDateFormat("yyyy-MM-dd");
        fechaHora = new java.text.SimpleDateFormat("yyyy-MM-dd hh:mm");
        
        periodo = anomes.format(pdate);
        fechadoc = fecha.format(pdate);
        fechaInicio= fechaHora.format(ahora);
        
        
        ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
        String ruta = rb.getString("ruta")+ "/exportar/migracion/"+user;
        
        File file = new File(ruta);
        file.mkdirs();
        
        
        pw = new PrintWriter(System.err, true);
        
        String logFile = ruta + "/HContabilizacionPlanilla"+fechadoc+".txt";
        
        pw = new PrintWriter(new FileWriter(logFile, true), true);
        logWriter = new LogWriter("HContabilizacionPlanilla", LogWriter.INFO, pw);
        usuario=user;
        logWriter.setPrintWriter(pw);
        
        tipodoc=tipo;
        this.moneda = moneda;
        super.start();
    }
    
    
    /**
     * Metodo llamado por Java para inciciar la ejecuci�n del hilo
     */
    public void run(){
        com.tsp.finanzas.contab.model.Model model = new com.tsp.finanzas.contab.model.Model();
        com.tsp.operation.model.Model modelOperation = new com.tsp.operation.model.Model();
        int nosubidas=0;
        int subidas=0;
        int canttotal=0;
        
        try{
            
            
            
            logWriter.log("**********************************************************",LogWriter.INFO);
            logWriter.log("*  NUEVO PROCESO DE CONTABILIZACION DE PLANILLA          *",LogWriter.INFO);
            logWriter.log("*  Periodo  :  "+periodo+"                               *",LogWriter.INFO);
            logWriter.log("*  FechaDoc :  "+fechadoc+"                              *",LogWriter.INFO);
            logWriter.log("**********************************************************",LogWriter.INFO);
            
            com.tsp.operation.model.Model modelo =  new com.tsp.operation.model.Model();
            modelOperation.LogProcesosSvc.InsertProceso("Generacion Comprobantes", this.hashCode(), "Generacion Comprobantes Planilla", usuario);
            try{
                model.comprobanteService.buscarPlanillas(tipodoc);
            }catch(SQLException e){
                try{
                    modelOperation.LogProcesosSvc.finallyProceso("Generacion Comprobantes", this.hashCode(),usuario,"ERROR :" + e.getMessage());
                }catch ( SQLException ex) {
                    System.out.println("Error guardando el proceso");
                }
                e.printStackTrace();
            }
            
            Vector plas = model.comprobanteService.getVector();
            
            
            logWriter.log("Se encontraron "+plas.size()+" registros para contabilizar",LogWriter.INFO);
            canttotal = plas.size();
            for(int i =0; i<plas.size();i++){
                Vector consultas = new Vector();
                Vector errorCuenta = new Vector();
                //   Vector consultas2 = new Vector();
                
                Comprobantes c = (Comprobantes) plas.elementAt(i);
                boolean estaCuenta=true;
                if(c.isTieneProv() && c.getTotal_credito()>0 && !c.getMoneda().equals("") && !c.getCmc().equals("") ){
                    
                    double valorPla = c.getTotal_credito();
                    c.setPeriodo(periodo);
                    c.setFechadoc(fechadoc);
                    c.setAprobador(usuario);
                    c.setDstrct("FINV");
                    c.setTipodoc(tipodoc);
                    c.setMoneda(moneda);
                    
                    model.comprobanteService.buscarPlarem(c.getNumdoc());
                    Vector det = model.comprobanteService.getVector();
                   // System.out.println("Total items " +det.size());
                    
                    c.setTotal_items(det.size()+1);
               //     System.out.println("Planilla "+c.getNumdoc());
                    if(this.validarItems(det) && det.size()>0 && this.existeCuenta(det)){
                        
                        subidas++;
                        
                        int gtransaccion = model.comprobanteService.getGrupoTransaccion();
                        c.setGrupo_transaccion(gtransaccion);
                        c.setTipo_operacion("CPL");
                        model.comprobanteService.setComprobante(c);
                        
                        consultas.add(model.comprobanteService.insert());
                        consultas.add(model.comprobanteService.actualizarTablaGen());
                        //modelo.despachoService.insertar(consultas2);
                        
                        
                        
                        c.setDocumento_interno("001");
                        c.setGrupo_transaccion(gtransaccion);
                        
                        //BUSCO PLAREM PARA EL DETALLE
                        int valorTotal=0;
                        for(int j =0; j<det.size()-1;j++){
                            
                            
                            Comprobantes detalle = (Comprobantes) det.elementAt(j);
                            
                            
                            double valor = com.tsp.util.Util.redondear((detalle.getPorcent()*valorPla)/100,0);
                            int valorTruncado = (int) valor;
                            
                            valorTotal=valorTotal+valorTruncado;
                            
                            
                            c.setTotal_debito(valorTruncado);
                            c.setTotal_credito(0);
                            c.setCuenta(detalle.getAccount_code_c());
                            c.setPorcent(detalle.getPorcent());
                            c.setNumrem(detalle.getNumrem());
                            c.setMoneda(moneda);
                            
                            model.comprobanteService.setComprobante(c);
                            
                            consultas.add(model.comprobanteService.insertItem());
                            consultas.add(model.comprobanteService.marcarPlarem());
                        }
                        
                        
                        
                        Comprobantes detalle = (Comprobantes) det.lastElement();
                        
                        double sobrante = valorPla-valorTotal;
                        c.setTotal_debito(sobrante);
                        c.setTotal_credito(0);
                        c.setCuenta(detalle.getAccount_code_c());
                        c.setPorcent(detalle.getPorcent());
                        c.setNumrem(detalle.getNumrem());
                        c.setMoneda(moneda);
                        
                        model.comprobanteService.setComprobante(c);
                        consultas.add(model.comprobanteService.insertItem());
                        consultas.add(model.comprobanteService.marcarPlarem());
                        
                        
                        c.setTotal_debito(0);
                        c.setTotal_credito(valorPla);
                        c.setCuenta(c.getCmc());
                        c.setMoneda(moneda);
                        model.comprobanteService.setComprobante(c);
                        consultas.add(model.comprobanteService.insertItem());
                        consultas.add(model.comprobanteService.marcarPlanilla());
                        
                        
                        
                    }
                    else{
                        nosubidas++;
                //        System.out.println("*************************No se subio "+c.getNumdoc());
                        if(!tieneCuenta(det)){
                            logWriter.log("    Placa   =   "+c.getPlaca()+"  propietario  =  "+c.getTercero()+"  Planilla  =  "+c.getNumdoc()+"  "+"No se encontro Account_code_c",LogWriter.ERROR);
                        }
                        if(!masPorcent(det)){
                            logWriter.log("    Placa   =   "+c.getPlaca()+"  propietario  =  "+c.getTercero()+"  Planilla  =  "+c.getNumdoc()+"  "+"La suma de porcentajes es Distinta a 100",LogWriter.ERROR);
                        }
                        if(!existeCuenta(det)){
                            
                            for(int j =0; j<det.size();j++){
                                Comprobantes detalle = (Comprobantes) det.elementAt(j);
                                if(!model.planDeCuentasService.existCuenta("FINV",detalle.getAccount_code_c())){
                                 //   System.out.println("No existe la cuenta "+detalle.getAccount_code_c());
                                    logWriter.log("    Placa   =   "+c.getPlaca()+"  propietario  =  "+c.getTercero()+"  Planilla  =  "+c.getNumdoc()+"  "+"La cuenta "+detalle.getAccount_code_c()+" no existe en el plan de cuentas",LogWriter.ERROR);
                                }
                                
                            }
                            
                        }
                    }
                }
                else{
                    if(!c.isTieneProv())
                        logWriter.log("    Placa   =   "+c.getPlaca()+"  propietario  =  "+c.getTercero()+"  Planilla  =  "+c.getNumdoc()+"  "+"No se encontro proveedor",LogWriter.ERROR);
                    if (c.getTotal_credito()<=0)
                        logWriter.log("    Placa   =   "+c.getPlaca()+"  propietario  =  "+c.getTercero()+"  Planilla  =  "+c.getNumdoc()+"  "+"La planilla tiene valor en 0 ",LogWriter.ERROR);
                    if(c.getMoneda().equals(""))
                        logWriter.log("    Placa   =   "+c.getPlaca()+"  propietario  =  "+c.getTercero()+"  Planilla  =  "+c.getNumdoc()+"  "+"La planilla no tiene definida una moneda",LogWriter.ERROR);
                    
                    if(c.getCmc().equals(""))
                        logWriter.log("    Placa   =   "+c.getPlaca()+"  propietario  =  "+c.getTercero()+"  Planilla  =  "+c.getNumdoc()+"  "+"No se encontro numero de cuenta de la  planilla",LogWriter.ERROR);
                    
                    
                    nosubidas++;
                }
                
                modelo.despachoService.insertar(consultas);
                
                
            }
            
            
            
            java.util.Date fin = new java.util.Date();
            String fechaFin= fechaHora.format(fin);
            
            //ENVIAMOS EMAIL DE LA CONTABILIZACION
            String mailInt="";
            String mailExt="";
            boolean swInt=false;
            boolean swExt=false;
            LinkedList lista = modelOperation.tablaGenService.obtenerInfoTablaGen("EMAIL", "contabpla");
            if(lista!=null){
                Iterator it = lista.iterator();
                while(it.hasNext()){
                    TablaGen t = (TablaGen)it.next();
                    System.out.println("-"+t.getDescripcion());
                    
                    if(t.getReferencia().equals("I")){
                        //verifico si exsite una arroba en la descripcion
                        if( t.getDescripcion().indexOf("@") != -1){
                            mailInt += t.getDescripcion()+";";
                        }
                        //se obtiene el login del usuario y se busca el email
                        else{
                            modelOperation.usuarioService.searchUsuario( t.getDescripcion().toUpperCase() );
                            Usuario u = modelOperation.usuarioService.getUsuario();
                            if(!u.getEmail().equals(""))
                                mailInt += u.getEmail()+";";
                        }
                        swInt = true;
                    }
                    //si el email del cliente es externo
                    else{
                        //verifico si exsite una arroba en la descripcion
                        if( t.getDescripcion().indexOf("@") != -1){
                            mailExt += t.getDescripcion()+";";
                        }
                        else{
                            //se obtiene el login del usuario y se busca el email
                            modelOperation.usuarioService.searchUsuario( t.getDescripcion().toUpperCase() );
                            Usuario u = modelOperation.usuarioService.getUsuario();
                            if(!u.getEmail().equals(""))
                                mailExt += u.getEmail()+";";
                        }
                        swExt = true;
                    }
                }
                
               // System.out.println("interno "+mailInt);
               // System.out.println("externo "+mailExt);
                
                com.tsp.operation.model.beans.SendMail email = new com.tsp.operation.model.beans.SendMail();
                email.setEmailfrom("procesos@sanchezpolo.com");
                email.setSendername( "PROCESO CONTABILIDAD" );
                email.setRecstatus("A");
                email.setEmailcode("");
                email.setEmailcopyto("");
                email.setEmailsubject("CONTABILIZACION PLANILLA");
                email.setEmailbody("SE GENERO LA CONTABILIZACION DE LA PLANILLA EN UN PROCESO AUTOMATICO \n\n" +
                "Planillas Contabilizadas:    "+subidas+
                "\nPlanillas NO contabilizadas: " +nosubidas+
                "\n\nFECHA INICIO:    "+ fechaInicio
                +"\nFECHA FINAL      "+ fechaFin+
                "\n\nProceso finalizado con exito" +
                "\n\n\nNOTA: Para mas informacion a cerca de las planillas no contabilizadas consulte" +
                " el log de contabilizacion.");
                
                if(swInt==true){
                    mailInt = mailInt.substring(0, mailInt.length() - 1 );
                    email.setEmailto(mailInt);
                    email.setTipo("I");
                    modelOperation.sendMailService.sendMail(email);
                }
                if(swExt==true){
                    mailExt = mailExt.substring(0, mailExt.length() - 1 );
                    email.setEmailto(mailExt);
                    email.setTipo("E");
                    modelOperation.sendMailService.sendMail(email);
                }
                
            }
            
            modelOperation.LogProcesosSvc.finallyProceso("Generacion Comprobantes", this.hashCode(), usuario, "PROCESO EXITOSO");
            
        }catch(Exception e){
            
            try{
                modelOperation.LogProcesosSvc.finallyProceso("Generacion Comprobantes", this.hashCode(),usuario,"ERROR :" + e.getMessage());
            }catch ( SQLException ex) {
                System.out.println("Error guardando el proceso");
            }
            logWriter.log("ERROR: "+e.toString(),LogWriter.ERROR);
            e.printStackTrace();
        }
        finally{
            canttotal = canttotal-( subidas+nosubidas);
            logWriter.log("FIN DEL PROCESO ",LogWriter.INFO);
            logWriter.log("Planillas Contabilizadas:    "+subidas,LogWriter.INFO);
            logWriter.log("Planillas NO contabilizadas: " +nosubidas,LogWriter.INFO);
            logWriter.log("Planillas NO contabilizadas por ERROR DEL PROGRAMA: " +canttotal,LogWriter.INFO);
            
        }
    }
    public boolean validarItems(Vector det){
        boolean sw=true;
        int total = 0;
        for(int j =0; j<det.size();j++){
            Comprobantes detalle = (Comprobantes) det.elementAt(j);
            if(detalle.getAccount_code_c().equals("")){
                sw=false;
            }
            total=total+detalle.getPorcent();
        }
        if(total !=100){
            sw=false;
        }
        if(total ==0){
            sw=true;
        }
        return sw;
    }
    
    public boolean tieneCuenta(Vector det){
        boolean sw=true;
        
        for(int j =0; j<det.size();j++){
            Comprobantes detalle = (Comprobantes) det.elementAt(j);
            if(detalle.getAccount_code_c().equals("")){
                sw=false;
            }
            
        }
        return sw;
    }
    
    public boolean existeCuenta(Vector det){
        
        boolean sw=true;
        try{
    //        System.out.println("Buscamos la cuenta...");
            com.tsp.finanzas.contab.model.Model model = new com.tsp.finanzas.contab.model.Model();
            for(int j =0; j<det.size();j++){
                Comprobantes detalle = (Comprobantes) det.elementAt(j);
                if(!model.planDeCuentasService.existCuenta("FINV",detalle.getAccount_code_c())){
                    sw=false;
                   // System.out.println("NO existe la cuenta..."+detalle.getAccount_code_c());
                }
                
                
            }
        }catch (SQLException e){
            System.out.println("Error "+e.getMessage());
        }
        return sw;
    }
    
    public boolean masPorcent(Vector det){
        boolean sw=true;
        int total = 0;
        for(int j =0; j<det.size();j++){
            Comprobantes detalle = (Comprobantes) det.elementAt(j);
            total=total+detalle.getPorcent();
        }
        if(total !=100){
            sw=false;
        }
        if(total ==0){
            sw= true;
        }
        return sw;
    }
    
    public static void main(String a [])throws Exception{
        try{
            com.tsp.finanzas.contab.model.services.ComprobantesService c = new com.tsp.finanzas.contab.model.services.ComprobantesService();
            String tipoDoc= c.getTipoDocumento("001");
            HContabilizacionPlanilla hilo = new HContabilizacionPlanilla();
            hilo.start("ADMIN",tipoDoc,"PES");
        }catch(SQLException e){
            System.out.println("Error "+e.getMessage());
        }
    }
    
    
}
