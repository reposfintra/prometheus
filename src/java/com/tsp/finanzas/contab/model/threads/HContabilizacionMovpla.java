/*
 * Nombre        HContabilizacion.java
 * Descripci�n   Contabiliza los documentos como planilla.
 * Autor         kreales
 * Fecha         05 de junio
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */

package com.tsp.finanzas.contab.model.threads;

import com.tsp.util.*;
import com.tsp.finanzas.contab.model.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.beans.*;
import com.tsp.finanzas.contab.model.beans.*;
import java.io.*;
import java.sql.*;
import java.util.*;
import java.lang.*;
import javax.servlet.jsp.*;
import javax.servlet.http.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import javax.servlet.http.*;
import com.tsp.util.LogWriter;

public class HContabilizacionMovpla extends Thread{
    
    private   String usuario;
    private PrintWriter pw;
    private LogWriter logWriter;
    java.text.SimpleDateFormat anomes;
    java.text.SimpleDateFormat fecha;
    java.text.SimpleDateFormat fechaHora;
    String periodo;
    String fechadoc;
    String fechaInicio;
    java.util.Date pdate;
    java.util.Date ahora;
    String tipodoc;
    String moneda;
    public HContabilizacionMovpla() {
    }
    
    /**
     * Establece las variables iniciales y activa el inicio del hilo
     * @param user El usuario en sesi�n
     * @throws Exception Si algun error ocurre en el acceso a la BD
     */
    public void start( String user, String tipo, String moneda ) throws Exception{
        /*
         *Busco la fecha actual y le resto un dia.
         */
        Calendar hoy = Calendar.getInstance();
        hoy.add(hoy.DATE, -1);
        
        pdate = hoy.getTime();
        ahora = new java.util.Date();
        
        anomes = new java.text.SimpleDateFormat("yyyyMM");
        fecha = new java.text.SimpleDateFormat("yyyy-MM-dd");
        fechaHora = new java.text.SimpleDateFormat("yyyy-MM-dd hh:mm");
        
        periodo = anomes.format(pdate);
        fechadoc = fecha.format(pdate);
        fechaInicio= fechaHora.format(ahora);
        
        
        ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
        String ruta = rb.getString("ruta")+ "/exportar/migracion/"+user;
        
        File file = new File(ruta);
        file.mkdirs();
        
        pw = new PrintWriter(System.err, true);
        
        String logFile = ruta + "/HContabilizacionMovpla"+fechadoc+".log";
        
        pw = new PrintWriter(new FileWriter(logFile, true), true);
        logWriter = new LogWriter("HContabilizacionMovpla", LogWriter.INFO, pw);
        usuario=user;
        logWriter.setPrintWriter(pw);
        
        tipodoc=tipo;
        this.moneda=moneda;
        super.start();
    }
    
    
    /**
     * Metodo llamado por Java para inciciar la ejecuci�n del hilo
     */
    public void run(){
        com.tsp.finanzas.contab.model.Model model = new com.tsp.finanzas.contab.model.Model();
        com.tsp.operation.model.Model modelOperation = new com.tsp.operation.model.Model();
        
        try{
            
            Vector consultas = new Vector();
            
            logWriter.log("**********************************************************",LogWriter.INFO);
            logWriter.log("*  NUEVO PROCESO DE CONTABILIZACION DE MOVPLANILLA       *",LogWriter.INFO);
            logWriter.log("*  Periodo  :  "+periodo+"                               *",LogWriter.INFO);
            logWriter.log("*  FechaDoc :  "+fechadoc+"                              *",LogWriter.INFO);
            logWriter.log("**********************************************************",LogWriter.INFO);
            
            
            modelOperation.LogProcesosSvc.InsertProceso("Generacion Comprobantes", this.hashCode(), "Generacion Comprobantes Movpla", usuario);
            
            model.comprobanteService.buscarMovpla(tipodoc);
            Vector plas = model.comprobanteService.getVector();
            
            logWriter.log("Se encontraron "+plas.size()+" registros para contabilizar",LogWriter.INFO);
            
            int nosubidas=0;
            int subidas=0;
            String cuenta ="";
            for(int i =0; i<plas.size();i++){
                
                Comprobantes c = (Comprobantes) plas.elementAt(i);
                cuenta = c.getCuenta();
                c.setMoneda(moneda);
                if(c.isTieneProv() && !c.getMoneda().equals("") && !c.getCmc().equals("") && !c.getTipo_cuenta().equals("") && !c.getCuenta().equals("")){
                    
                    double valorPla = c.getTotal_credito();
                    c.setPeriodo(periodo);
                    c.setFechadoc(fechadoc);
                    c.setAprobador(usuario);
                    c.setDstrct("FINV");
                    
                    if (c.getTipo_valor().equals("C")){
                        c.setTipodoc("AEP");
                        c.setDetalle("ANULA "+c.getDetalle());
                    }
                    else if (c.getTipo_valor().equals("D")){
                        c.setTipodoc("EPL");
                        c.setDetalle(c.getDetalle());
                    }
                    
                    
                    if(c.getTipo_cuenta().equals("E")){
                        
                        //System.out.println("tipo de cuenta ELEMENTO DE GASTO");
                        
                        /*
                         * Se buscan los plarem asociados a esa planilla para distrubuir el costo del
                         * movpla encontrado
                         */
                        model.comprobanteService.buscarPlarem(c.getNumdoc());
                        Vector det = model.comprobanteService.getVector();
                        //System.out.println("Total items " +det.size());
                        
                        c.setTotal_items(det.size()+1);
                        
                        if(this.validarItems(det, cuenta) && det.size()>0){
                            
                            subidas++;
                            
                            model.comprobanteService.setComprobante(c);
                            model.comprobanteService.insert();
                            
                            int gtransaccion = model.comprobanteService.getGrupoTransaccion();
                            
                            c.setDocumento_interno("001");
                            c.setGrupo_transaccion(gtransaccion);
                            
                            //BUSCO PLAREM PARA EL DETALLE
                            int valorTotal=0;
                            for(int j =0; j<det.size()-1;j++){
                                
                                Comprobantes detalle = (Comprobantes) det.elementAt(j);
                                
                                double valor = com.tsp.util.Util.redondear((valorPla * detalle.getPorcent())/100,0);
                                int valorTruncado = (int) valor;
                                
                                String cuentaDet = detalle.getAccount_code_c().substring(0,detalle.getAccount_code_c().length()-4)+cuenta;
                                c.setCuenta(cuentaDet);
                                
                                valorTotal=valorTotal+valorTruncado;
                                
                                //System.out.println("Planilla "+c.getNumdoc());
                                //System.out.println("Valor a contabilizar "+valorTruncado);
                                
                                if(c.getTipo_valor().equals("D")){
                                    
                                    //PRIMERO GRABO EL VALOR DEL DEBITO
                                    c.setTotal_debito(valorTruncado);
                                    c.setTotal_credito(0);
                                    
                                    model.comprobanteService.setComprobante(c);
                                    consultas.add(model.comprobanteService.insertItem());
                                    
                                    
                                }
                                else if (c.getTipo_valor().equals("C")){
                                    
                                    //PRIMERO GRABO EL VALOR DEL CREDITO
                                    c.setTotal_debito(0);
                                    c.setTotal_credito(valorTruncado);
                                    
                                    model.comprobanteService.setComprobante(c);
                                    consultas.add(model.comprobanteService.insertItem());
                                    
                                }
                                
                                
                            }
                            
                            Comprobantes detalle = (Comprobantes) det.lastElement();
                            double sobrante = valorPla-valorTotal;
                            String cuentaDet = detalle.getAccount_code_c().substring(0,detalle.getAccount_code_c().length()-4)+cuenta;
                            c.setCuenta(cuentaDet);
                            
                            if(c.getTipo_valor().equals("D")){
                                
                                //PRIMERO GRABO EL VALOR DEL DEBITO
                                c.setTotal_debito(sobrante);
                                c.setTotal_credito(0);
                                
                                model.comprobanteService.setComprobante(c);
                                consultas.add(model.comprobanteService.insertItem());
                                
                                
                                //SEGUNDO GRABO EL VALOR DEL CREDITO
                                c.setTotal_debito(0);
                                c.setTotal_credito(valorPla);
                                c.setCuenta(c.getCmc());
                                model.comprobanteService.setComprobante(c);
                                consultas.add(model.comprobanteService.insertItem());
                            }
                            else if (c.getTipo_valor().equals("C")){
                                //PRIMERO GRABO EL VALOR DEL CREDITO
                                c.setTotal_debito(0);
                                c.setTotal_credito(sobrante);
                                model.comprobanteService.setComprobante(c);
                                consultas.add(model.comprobanteService.insertItem());
                                
                                //PRIMERO GRABO EL VALOR DEL DEBITO
                                c.setTotal_credito(0);
                                c.setTotal_debito(valorPla);
                                c.setCuenta(c.getCmc());
                                model.comprobanteService.setComprobante(c);
                                consultas.add(model.comprobanteService.insertItem());
                                
                            }
                            consultas.add(model.comprobanteService.marcarMovPlanilla());
                        }
                        else{
                            nosubidas++;
                            logWriter.log(c.getNumdoc()+"  "+"No se encontro Account_code_c o la cuenta no es valida ",LogWriter.ERROR);
                        }
                        
                        //OJO AQUI hay q generar mas items...
                    }
                    else{
                        
                        c.setTotal_items(2);
                        model.comprobanteService.setComprobante(c);
                        model.comprobanteService.insert();
                        
                        int gtransaccion = model.comprobanteService.getGrupoTransaccion();
                        c.setDocumento_interno("001");
                        c.setGrupo_transaccion(gtransaccion);
                        double valor = com.tsp.util.Util.redondear(c.getTotal_credito(),0);
                        int valorTruncado = (int) valor;
                        //System.out.println("Planilla "+c.getNumdoc());
                        //System.out.println("Valor a contabilizar "+valorTruncado);
                        if(c.getTipo_valor().equals("D")){
                            //PRIMERO GRABO EL VALOR DEL DEBITO
                            c.setTotal_debito(valorTruncado);
                            c.setTotal_credito(0);
                            
                            model.comprobanteService.setComprobante(c);
                            consultas.add(model.comprobanteService.insertItem());
                            
                            
                            //SEGUNDO GRABO EL VALOR DEL CREDITO
                            c.setTotal_debito(0);
                            c.setTotal_credito(valorTruncado);
                            c.setCuenta(c.getCmc());
                            model.comprobanteService.setComprobante(c);
                            consultas.add(model.comprobanteService.insertItem());
                        }
                        else if (c.getTipo_valor().equals("C")){
                            //PRIMERO GRABO EL VALOR DEL CREDITO
                            c.setTotal_debito(0);
                            c.setTotal_credito(valorTruncado);
                            model.comprobanteService.setComprobante(c);
                            consultas.add(model.comprobanteService.insertItem());
                            
                            //PRIMERO GRABO EL VALOR DEL DEBITO
                            c.setTotal_debito(valorTruncado);
                            c.setTotal_credito(0);
                            c.setCuenta(c.getCmc());
                            model.comprobanteService.setComprobante(c);
                            consultas.add(model.comprobanteService.insertItem());
                        }
                        consultas.add(model.comprobanteService.marcarMovPlanilla());
                        
                    }
                    
                    
                    
                }
                else{
                    if(c.isTieneProv())
                        logWriter.log(c.getNumdoc()+"  "+"No se encontro proveedor",LogWriter.ERROR);
                    if (c.getTotal_credito()<=0)
                        logWriter.log(c.getNumdoc()+"  "+"La planilla tiene valor en 0 ",LogWriter.ERROR);
                    if(c.getMoneda().equals(""))
                        logWriter.log(c.getNumdoc()+"  "+"La planilla no tiene definida una moneda",LogWriter.ERROR);
                    if(c.getCmc().equals(""))
                        logWriter.log(c.getNumdoc()+"  "+"El proveedor no tiene numero de cuenta",LogWriter.ERROR);
                    if(c.getTipo_cuenta().equals(""))
                        logWriter.log(c.getNumdoc()+"  "+"No se encontro tipo de cuenta del concepto ",LogWriter.ERROR);
                    if(c.getCuenta().equals(""))
                        logWriter.log(c.getNumdoc()+"  "+"No se encontro cuenta del concepto ",LogWriter.ERROR);
                    nosubidas++;
                }
            }
            
            com.tsp.operation.model.Model modelo =  new com.tsp.operation.model.Model();
            modelo.despachoService.insertar(consultas);
            
            java.util.Date fin = new java.util.Date();
            String fechaFin= fechaHora.format(fin);
            
            //ENVIAMOS EMAIL DE LA CONTABILIZACION
            String mailInt="";
            String mailExt="";
            boolean swInt=false;
            boolean swExt=false;
            LinkedList lista = modelOperation.tablaGenService.obtenerInfoTablaGen("EMAIL", "contabpla");
            if(lista!=null){
                Iterator it = lista.iterator();
                while(it.hasNext()){
                    TablaGen t = (TablaGen)it.next();
                    //System.out.println("-"+t.getDescripcion());
                    
                    if(t.getReferencia().equals("I")){
                        //verifico si exsite una arroba en la descripcion
                        if( t.getDescripcion().indexOf("@") != -1){
                            mailInt += t.getDescripcion()+";";
                        }
                        //se obtiene el login del usuario y se busca el email
                        else{
                            modelOperation.usuarioService.searchUsuario( t.getDescripcion().toUpperCase() );
                            Usuario u = modelOperation.usuarioService.getUsuario();
                            if(!u.getEmail().equals(""))
                                mailInt += u.getEmail()+";";
                        }
                        swInt = true;
                    }
                    //si el email del cliente es externo
                    else{
                        //verifico si exsite una arroba en la descripcion
                        if( t.getDescripcion().indexOf("@") != -1){
                            mailExt += t.getDescripcion()+";";
                        }
                        else{
                            //se obtiene el login del usuario y se busca el email
                            modelOperation.usuarioService.searchUsuario( t.getDescripcion().toUpperCase() );
                            Usuario u = modelOperation.usuarioService.getUsuario();
                            if(!u.getEmail().equals(""))
                                mailExt += u.getEmail()+";";
                        }
                        swExt = true;
                    }
                }
                subidas = plas.size()-nosubidas;
                //System.out.println("interno "+mailInt);
                //System.out.println("externo "+mailExt);
                
                com.tsp.operation.model.beans.SendMail email = new com.tsp.operation.model.beans.SendMail();
                email.setEmailfrom("procesos@sanchezpolo.com");
                email.setSendername( "PROCESO CONTABILIDAD" );
                email.setRecstatus("A");
                email.setEmailcode("");
                email.setEmailcopyto("");
                email.setEmailsubject("CONTABILIZACION PLANILLA");
                email.setEmailbody("SE GENERO LA CONTABILIZACION DE LA PLANILLA EN UN PROCESO AUTOMATICO \n\n" +
                "Planillas Contabilizadas:    "+subidas+
                "\nPlanillas NO contabilizadas: " +nosubidas+
                "\n\nFECHA INICIO:    "+ fechaInicio
                +"\nFECHA FINAL      "+ fechaFin+
                "\n\nProceso finalizado con exito" +
                "\n\n\nNOTA: Para mas informacion a cerca de las planillas no contabilizadas consulte" +
                " el log de contabilizacion.");
                if(swInt==true){
                    mailInt = mailInt.substring(0, mailInt.length() - 1 );
                    email.setEmailto(mailInt);
                    email.setTipo("I");
                    modelOperation.sendMailService.sendMail(email);
                }
                if(swExt==true){
                    mailExt = mailExt.substring(0, mailExt.length() - 1 );
                    email.setEmailto(mailExt);
                    email.setTipo("E");
                    modelOperation.sendMailService.sendMail(email);
                }
                
            }
            logWriter.log("FIN DEL PROCESO ",LogWriter.INFO);
            logWriter.log("Planillas Contabilizadas:    "+subidas,LogWriter.INFO);
            logWriter.log("Planillas NO contabilizadas: " +nosubidas,LogWriter.INFO);
            
            modelOperation.LogProcesosSvc.finallyProceso("Generacion Comprobantes", this.hashCode(), usuario, "PROCESO EXITOSO");
            
        }catch(Exception e){
            
            try{
                modelOperation.LogProcesosSvc.finallyProceso("Generacion Comprobantes", this.hashCode(),usuario,"ERROR :" + e.getMessage());
            }catch ( SQLException ex) {
                //System.out.println("Error guardando el proceso");
            }
            e.printStackTrace();
        }
    }
    
    public static void main(String a [])throws Exception{
        try{
            com.tsp.finanzas.contab.model.services.ComprobantesService c = new com.tsp.finanzas.contab.model.services.ComprobantesService();
            String tipoDoc= c.getTipoDocumento("001");
            HContabilizacionMovpla hilo = new HContabilizacionMovpla();
            hilo.start("ADMIN",tipoDoc,"PES");
        }catch(SQLException e){
            //System.out.println("Error "+e.getMessage());
        }
    }
    public boolean validarItems(Vector det, String cuenta){
        boolean sw=true;
        try{
            com.tsp.finanzas.contab.model.Model model = new com.tsp.finanzas.contab.model.Model();
            for(int j =0; j<det.size();j++){
                String cuentaDet ="";
                Comprobantes detalle = (Comprobantes) det.elementAt(j);
                if(detalle.getAccount_code_c().equals("")){
                    sw=false;
                }
                else{
                    if(detalle.getAccount_code_c().length()>4){
                        cuentaDet = detalle.getAccount_code_c().substring(0,detalle.getAccount_code_c().length()-4)+cuenta;
                    }
                    
                    
                }
                //System.out.println("CUenta a buscar "+cuentaDet);
                model.planDeCuentasService.searchCuenta("FINV",cuentaDet);
                
                
                if(model.planDeCuentasService.getVec_cuentas()!=null){
                    if(model.planDeCuentasService.getVec_cuentas().size()<=0){
                        sw=false;
                    }
                }
                
                
                
            }
        }catch(SQLException e){
            //System.out.println("Error "+e.getMessage());
        }
        return sw;
    }
    
}
