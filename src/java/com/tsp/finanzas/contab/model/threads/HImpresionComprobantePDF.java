/**
 * Nombre        HImpresionComprobantePDF.java
 * Descripci�n
 * Autor         Mario Fontalvo Solano
 * Fecha         10 de julio de 2006, 08:23 AM
 * Version       1.0
 * Coyright      Transportes S�nchez Polo S.A.
 **/

package com.tsp.finanzas.contab.model.threads;


import com.tsp.operation.model.beans.Usuario;
import com.tsp.finanzas.contab.model.Model;
import com.tsp.pdf.ComprobantePDF;
import com.tsp.operation.model.LogProcesosService;

import java.io.*;
import java.util.*;
import java.text.*;

public class HImpresionComprobantePDF {
    
    Usuario usuario;
    Model   model;
    Vector  datos = null;
    boolean autocompletar = false;
    String tipodoc = "";
    String numdoc = "";
    String numtransaccion = "";
    String periodo = "";
    SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMdd_hhmmss");
    
    String procesoName = "Impresion Comprobantes PDF";
    
    /** Crea una nueva instancia de  HImpresionComprobantePDF */
    public HImpresionComprobantePDF() {
    }
    
    public void start(String tipodoc, String numdoc, String numtransaccion, String periodo, Model model, Usuario usuario){
        this.tipodoc  = tipodoc;
        this.numdoc   = numdoc;
        this.numtransaccion = numtransaccion;
        this.periodo  = periodo;
        autocompletar = false;
        this.model    = model;
        this.usuario  = usuario;
        run();
    }
    public void start(Vector datos, Model model, Usuario usuario){
        autocompletar = true;
        this.model    = model;
        this.usuario  = usuario;
        this.datos    = datos;
        run();
    }
    
    public synchronized void run() {
        LogProcesosService log = new LogProcesosService(usuario.getBd());
        try{
            log.InsertProceso( this.procesoName, this.hashCode(),"Impresion de comprobantes contables", usuario.getLogin() );
            if (!autocompletar)
                datos = model.comprobanteService.obtenerComprobantes(tipodoc, numdoc, numtransaccion, periodo);
            else
                datos = model.comprobanteService.obtenerComprobantes(datos);
            
            // creacion del directorio del usuario
            ResourceBundle rb   = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String         path = rb.getString("ruta");
            String         ruta = path + "/exportar/migracion/" + usuario.getLogin();
            File archivo = new File( ruta );
            if (!archivo.exists()) archivo.mkdirs();
            
            
            
            if (datos!=null && !datos.isEmpty()){

                // generacion del pdf
                File template = new File(path + "/Templates/Comprobante.xsl");
                File output   = new File(ruta + "/Comprobantes" + "_" + fmt.format( new Date() )+ ".pdf" ) ;
                ComprobantePDF pdf = new ComprobantePDF(template, output, model);
                pdf.prepararPDF(datos);
                pdf.generarPDF();
                log.finallyProceso(this.procesoName,this.hashCode(),  this.usuario.getLogin() , "EXITOSO.");

            }
            else{
                log.finallyProceso(this.procesoName,this.hashCode(),  this.usuario.getLogin() , "No hay datos para generar en archivo ComprobantesPDF.");
            }




        }catch (Exception e){
            e.printStackTrace();
            //////System.out.println("No se pudo generar el pdf , ha ocurrido el siguiente error: " + e.getMessage());
            try{
                log.finallyProceso(this.procesoName,this.hashCode(),  this.usuario.getLogin() ,"No se pudo generar el pdf , ha ocurrido el siguiente error: " + e.getMessage());
            }catch(Exception ex){}

        }
    }
}
