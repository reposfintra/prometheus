/*
 * Nombre        HGeneradorReportesMayor.java
 * Autor         Osvaldo P�rez Ferrer
 * Fecha         19 de agosto de 2006, 04:34 PM
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */

package com.tsp.finanzas.contab.model.threads;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.aspose.cells.*;

import com.tsp.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.finanzas.contab.model.DAO.MayorizacionDAO;

public class HGeneradorReportesMayor extends Thread{
        
    String ruta;    
    
    private String NETO    = "MV";
    private String SALDO   = "SA";
    private String DEBITO  = "MD";
    private String CREDITO = "MC";
    private String[] hojas;        
    
    MayorizacionDAO mayor;
            
    private String procesoName;
    private String des;
    
    int mes_actual;
    int ano_actual;
    Model model;
    
    private String usuario;
    private Vector archivos;
    
    private LogWriter   logTrans;
    private PrintWriter pw;
    
    /**
     * Crea una nueva instancia de  HGeneradorReportesMayor
     */
    public HGeneradorReportesMayor( String anio, String mes, Model model, Usuario usuario, Vector archivos, String ruta ){
        
        this.procesoName    = "Generador de Reportes";
        this.des            = "Generacion de reportes contables de mayorizacion";
        int a               = Integer.parseInt( anio );
        int m               = Integer.parseInt( mes );
        this.model          = model;
        this.usuario        = usuario.getLogin();
        this.archivos       = archivos;
        this.ruta           = ruta;
        this.setActual( a, m );
        this.mayor = new MayorizacionDAO(usuario.getBd());
    }
    
    
    public void start(){
        super.start();     
    }
    
    
    public synchronized void run(){
        
        
        String comentario="EXITOSO";
        
        try{
            model.LogProcesosSvc.InsertProceso( this.procesoName, this.hashCode(),"Generador de Reportes", this.usuario);
            initLog();
            
            if( archivos != null && archivos.size()>0 ){
                
                logTrans.log( archivos.size() + " Archivos a procesar " , logTrans.INFO);
                logTrans.log( "" , logTrans.INFO);
                //Se recorre el vector con los nombres de los archivos
                for( int i=0; i<archivos.size(); i++ ){
                    logTrans.log("Procesando "+ String.valueOf( archivos.get(i)).replaceAll("SIN_PROCESAR-", "") , logTrans.INFO);
                    //Se procesa cada libro(Archivo)
                    this.procesarLibro( (String)archivos.get(i) );
                    logTrans.log("Terminado "+ String.valueOf( archivos.get(i)).replaceAll("SIN_PROCESAR-", "") , logTrans.INFO);
                }
                
            }
            
            closeLog(); // cerrando archivo de log
            model.LogProcesosSvc.finallyProceso(this.procesoName,this.hashCode(),  this.usuario ,comentario);
            
        }catch (Exception ex){
            try{
                model.LogProcesosSvc.finallyProceso(this.procesoName,this.hashCode(),  this.usuario ,"ERROR Hilo: " + ex.getMessage());
            }catch(Exception e){}
        }
        
    }
    
    /****************************************************************
     * Metodo procesarLibro: carga el libro con el nombre de archivo dado
     *        para procesarlo.
     * @author: Ing. Osvaldo P�rez Ferrer
     * @param:  nombre_archivo, nombre del excel
     ****************************************************************/
    public void procesarLibro( String nombre_archivo ){
        try{
            
            //Vector que contiene las celdas en las que se debe escribir valor
            Vector celdasActualizar = new Vector();
            
            //Se carga el libro
            Workbook workbook = new Workbook();
            workbook.open( ruta+"/"+nombre_archivo );
            
            //Se obtienen las hojas
            Worksheets worksheets = workbook.getWorksheets();
            hojas = new String[worksheets.size()];
            
            //Se recorren todas las hojas del libro
            for( int h=0; h<worksheets.size(); h++ ){
                
                Worksheet sheet = worksheets.getSheet(h);
                //Se almacenan los nombres de las hojas
                hojas[h] =  sheet.getName();
                
                Cells cells = sheet.getCells();
                Cell  cell;
                
                //Se obtienen los comentarios de la hoja
                Comments comments = sheet.getComments();
                Comment com;
                
                //Vector para nombre_hoja, nombre_celda, y valor a colocar en la celda
                String[] data = new String[3];
                
                double total = 0;
                
                String note = "";
                
                //Se leen los comentarios de la hoja
                for( int i=0; i<comments.size(); i++ ){
                    
                    com = comments.get(i);
                    
                    //Texto del comentario
                    note = com.getNote();
                    note = note.trim();
                    
                    //Celda que contiene el comentario
                    cell = cells.getCell( com.getRow(), com.getColumn() );
                    
                    try{
                        //Se calcula el valor a colocar en la celda, a partir del comentario
                        if( note.equals("F") ){
                            data[0]= ""+sheet.getName();
                            data[1]= ""+cell.getName();
                            data[2]= Util.NombreMes(this.mes_actual)+"/"+this.ano_actual;
                        }else{
                            total = this.procesarComment( note );
                            
                            //Se guarda nombre_hoja, nombre_celda, y valor a colocar en la celda
                            data[0]= ""+sheet.getName();
                            data[1]= ""+cell.getName();
                            data[2]= ""+total;
                        }
                        
                        
                    }catch (Exception e){
                        //Si ocurre excepcion por formato inv�lido de comentario, se coloca 'ERR' en la celda
                        data[0]= ""+sheet.getName();
                        data[1]= ""+cell.getName();
                        data[2]= "ERR";
                    }
                    
                    //Se agrega la celda que se debe actualizar
                    celdasActualizar.add(data);
                    data = new String[3];
                }
            }
            //Se escriben los resultados del proceso de lectura de comentarios
            this.escribir( celdasActualizar, hojas, workbook, nombre_archivo );
            
        }catch (Exception ex){
            //System.out.println("error en HGeneradorReportesMayor.run --> "+ex.getMessage());
        }
    }
    
    /****************************************************************
     * Metodo escribir: crea
     *        para procesarlo.
     * @author: Ing. Osvaldo P�rez Ferrer
     * @param:  nombre_archivo, nombre del excel
     ****************************************************************/
    public void escribir( Vector v, String[] hojas, Workbook workbook, String archivo ){
        try{
            
            String[] pos = new String[3];
            String hoja;
            
            //Se recorren las hojas del libro
            for( int h=0; h<hojas.length; h++ ){
                
                hoja = hojas[h];
                Worksheet sheet = workbook.getSheet(hoja);
                
                //Se recorre el vector de celdas que se deben actualizar
                for( int i=0; i<v.size(); i++ ){
                    
                    pos = (String[])v.get(i);
                    //Si el dato del vector, corresponde a la hoja actual, se escribe el dato en la celda
                    if( pos[0].equals( hoja ) ){
                        try{
                            Style st = workbook.createStyle();
                            st.setNumber(3); //Formato #,##0
                            sheet.getCell( pos[1] ).setStyle(st);                            
                            sheet.getCell( pos[1] ).setValue( Double.parseDouble( pos[2] ) );
                        }catch (Exception e){
                            //Si lo que se va a escribir en la celda es 'ERR', no se aplica formato
                            sheet.getCell( pos[1] ).setValue( pos[2] );
                        }
                        
                    }
                    
                }
                
            }
            //Se guarda el libro ya procesado
            workbook.save( ruta+"/"+archivo.replaceAll("SIN_PROCESAR-", "") );
            
            //Se borra el libro original
            File f = new File(ruta+"/"+archivo);
            f.delete();
            
            workbook = new Workbook();
            
        }catch (Exception ex){
            //System.out.println( " ERROR HGeneradorReportesMayor.escribir "+ex.getMessage() );
        }
    }
    
    /***********************************************************************************
     *metodo procesarComment: metodo que divide el comentario de la celda en la cuenta
     *                         o expresion like de cuentas, y el valor que se
     *                         desea calcular
     * @param : comment comentario de la celda
     ***********************************************************************************/
    public double procesarComment( String comment ) throws Exception{
        
        String[] lineas = null;
        Vector condiciones = new Vector();
        double total = 0;
        
        boolean cero;
        
        try{
            
            if( comment != null && comment!="" ){
                
                //Se separa el comentario por salto de linea
                lineas = comment.split("\n");
                String query  = "";
                int index     = 0;
                String linea    = "";
                String cuenta = "";
                String expr   = "";
                String mes    = "";
                
                for( int i=0; i<lineas.length ;i++ ){
                    //Verificamos si contiene :
                    linea = lineas[i];
                    index = linea.indexOf( ":" );
                    
                    expr = ( index != -1 )? linea.substring( index+1, linea.length() ) : NETO+"-0";
                    //Vector con los tipos de opciones
                    if ( !condiciones.contains( expr ) ){
                        condiciones.add( expr );
                    }
                }
                
                for( int i=0; i<condiciones.size(); i++ ){
                    
                    for( int j=0; j<lineas.length ;j++ ){
                        //Verificamos si contiene :
                        linea = lineas[j];
                        index = linea.indexOf( ":" );
                        
                        if( index != -1 ){
                            //se separa la cuenta de la condicion
                            cuenta = linea.substring( 0, index );
                            expr   = linea.substring( index+1, linea.length() );
                        }
                        else{
                            cuenta = lineas[j];
                            expr   = NETO+"-0";
                        }
                        
                        if( ((String)condiciones.get(i) ).equals(expr) ){                            
                            if( expr.indexOf(":<0") == -1 && expr.indexOf(":>0") == -1 ){
                                if( (cuenta.indexOf( "_" ) != -1) || (cuenta.indexOf( "%" ) != -1) ){
                                    query += " cuenta LIKE '"+cuenta+"' OR";
                                }else{
                                    query += " cuenta  = '"+cuenta+"' OR";
                                }
                                mes = expr.substring( (expr.indexOf("-"))+1,expr.length() );
                            }else if (expr.indexOf(":<0") > 0){
                                double total2 = 0;
                                String query2 = "";
                                if( (cuenta.indexOf( "_" ) != -1) || (cuenta.indexOf( "%" ) != -1) ){
                                    query2 += " cuenta LIKE '"+cuenta+"' OR";
                                }else{
                                    query2 += " cuenta  = '"+cuenta+"' OR";
                                }
                                
                                expr = expr.replaceAll(":<0","");
                                mes = expr.substring( (expr.indexOf("-"))+1,expr.length() );                                
                                query2 = query2.substring( 0, query2.length()-3 );                                
                                total2 = this.generarQuery(query2, expr, mes) ;
                                query2 = "";
                                total += total2 < 0? 0 : total2;
                            }else if (expr.indexOf(":>0") > 0){
                                double total2 = 0;
                                String query2 = "";
                                if( (cuenta.indexOf( "_" ) != -1) || (cuenta.indexOf( "%" ) != -1) ){
                                    query2 += " cuenta LIKE '"+cuenta+"' OR";
                                }else{
                                    query2 += " cuenta  = '"+cuenta+"' OR";
                                }
                                
                                expr = expr.replaceAll(":>0","");
                                mes = expr.substring( (expr.indexOf("-"))+1,expr.length() );                                
                                query2 = query2.substring( 0, query2.length()-3 );                                
                                total2 = this.generarQuery(query2, expr, mes) ;
                                query2 = "";
                                total += total2 > 0? 0 : total2;
                            }
                        }
                    }
                    if( query.length() > 0 ){
                    query = query.substring( 0, query.length()-3 );
                    total += this.generarQuery(query, (String)condiciones.get(i), mes) ;
                    query = "";                    
                    }
                }
            }
            
        }catch (Exception ex){
            //System.out.println("ERROR HGeneradorReportesMayor.procesarComment"+ex.getMessage());
            throw new Exception("");
        }
        return total;
    }
    
    
    public double generarQuery( String query, String opc, String m ) throws Exception{
        
        String opcion = "";
        double total  = 0;
        
        Calendar anterior = Calendar.getInstance();
        anterior.set( this.ano_actual, this.mes_actual-1, 10);
        
        try{
            
            int mes = Integer.parseInt( m );
            
            
            if( mes != 0 ){
                anterior.set( this.ano_actual, ( (mes_actual-1) - mes) , 15 );
                query += " AND anio= '"+anterior.get( 1 )+"'";
            }else{
                query += " AND anio= '"+anterior.get(1) +"'";
            }
            
            mes = anterior.get( anterior.MONTH ) + 1;
            
            opcion = opc.substring( 0, opc.indexOf("-") );
            
            if( opcion.equals( NETO ) ){
                
                total = mayor.obtenerDebitoCreditoCuentas( ""+mes, query, "movdeb" ) -
                mayor.obtenerDebitoCreditoCuentas( ""+mes, query, "movcre" );
                
            }else if( opcion.equals( DEBITO ) ){
                
                total = mayor.obtenerDebitoCreditoCuentas( ""+mes, query, "movdeb" );
                
            }else if( opcion.equals( CREDITO ) ){
                
                total = mayor.obtenerDebitoCreditoCuentas( ""+mes, query, "movcre" );
                
            }else if( opcion.equals( SALDO ) ){
                
                total = mayor.calcularSaldoAnteriorXCuentas( ""+mes, query, ""+anterior.get(1) );
                
            }
        }catch (Exception ex){
            //System.out.println(" error en HGeneradorReportesMayor.generarQuery --> "+ex.getMessage() );
            throw new Exception(" error en comentario ");
        }
        return total;
    }
    
    
    public void setActual( int anio, int mes ){
        Calendar actual   = Calendar.getInstance();
        actual.set( anio, (mes-1) , 10 );
        this.mes_actual = actual.get(2) + 1;
        this.ano_actual = actual.get(1);
    }
    
    public void initLog()throws Exception{
        java.text.SimpleDateFormat fmt = new java.text.SimpleDateFormat("yyyyMMdd_hhmmss");
        pw     = new PrintWriter(new BufferedWriter(new FileWriter(ruta + "/logGeneradorReportes_"+ fmt.format(new java.util.Date()) +".txt")));
        logTrans = new LogWriter("Generador_de_Reportes", LogWriter.INFO , pw );
        logTrans.log( "Proceso Inicializado", logTrans.INFO );
    }
    
    public void closeLog() throws Exception{
        logTrans.log( "Proceso Finalizado", logTrans.INFO );
        pw.close();
    }
    

    
    
}
