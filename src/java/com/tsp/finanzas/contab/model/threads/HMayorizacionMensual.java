/********************************************************************************
 * Nombre clase . . . . . .   HReporteRtfe.java                                 *
 * Descripci�n  .. . . . . .  Hilo que crea el reporte                          *
 * Autor . . . . . . . . . .  Ing. Pablo Emilio Bassil Orozco                   *
 * Fecha . . . . . . . . . .  Created on 10 de Febrero de 2009                  *
 * Version . . . . . . . . .  1.0                                               *
 * Copyright ...TSP - TRANSPORTES SANCHEZ POLO S.A.                             *
 ********************************************************************************/

package com.tsp.finanzas.contab.model.threads;

import com.tsp.finanzas.contab.model.Model;
import com.tsp.finanzas.contab.model.beans.ResultadoMayorizacion;
import com.tsp.util.*;
import com.tsp.operation.model.beans.*;
import java.io.*;
import java.util.*;

import com.tsp.operation.model.LogProcesosService;//20100820

public class HMayorizacionMensual extends Thread{
    
    private     Model   model;
    private     String  path;
    
    private     String  a�o;
    private     String  mes;
    
    private     Usuario usuario;
    private     int     op;

    private     ResultadoMayorizacion rm;
    private     Vector  vrm;
    
    public HMayorizacionMensual() {
    }

    public String sacarMes(int m){
        String mesSelect = "ninguno";
        if (m==1) {
            mesSelect = "Enero";
        }
        if (m==2) {
            mesSelect = "Febrero";
        }
        if (m==3) {
            mesSelect = "Marzo";
        }
        if (m==4) {
            mesSelect = "Abril";
        }
        if (m==5) {
            mesSelect = "Mayo";
        }
        if (m==6) {
            mesSelect = "Junio";
        }
        if (m==7) {
            mesSelect = "Julio";
        }
        if (m==8) {
            mesSelect = "Agosto";
        }
        if (m==9) {
            mesSelect = "Septiembre";
        }
        if (m==10) {
            mesSelect = "Octubre";
        }
        if (m==11) {
            mesSelect = "Noviembre";
        }
        if (m==12) {
            mesSelect = "Diciembre";
        }
        if (m==13) {
            mesSelect = "Fiscal";
        } 
        return mesSelect;
    }
    
    public void init(String a, String m, Usuario u, Model mod){
        usuario = u;
        a�o = a;
        mes = m;
        this.model = mod;
        super.start();
    }

    public void setOp(int OP){
        op=OP;
    }    

    public void run(){
        LogProcesosService log = new LogProcesosService(usuario.getBd());//20100820
        if (op == 1) {
            try {
                String hoy  =  Utility.getHoy("-");//20100820
                log.InsertProceso("DESMAYORIZACION_X", this.hashCode(), hoy ,this.usuario.getLogin() ); //20100820
                model = new Model(usuario.getBd());
                ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
                path = rb.getString("ruta") +
                        "/exportar/migracion/" +
                        usuario.getLogin().toUpperCase() +
                        "/"+usuario.getBd()+
                        "_DesmayorizacionMensual - " +
                        //Util.getFechaActual_String(4);//20100820
                        Util.getFechaActual_String(10).replaceAll(":", "").replaceAll(" ", "");//20100820
                model.MayorizacionMensual.setParams( Integer.parseInt(a�o),
                                                    Integer.parseInt(mes),
                                                    usuario.getLogin(),
                                                    usuario.getBase(),
                                                    usuario.getDstrct() );

                PrintWriter escribir = new PrintWriter(new BufferedWriter(new FileWriter(path+".txt")));
                //FileOutputStream fos;//20100823
                //DataOutputStream dos;//20100823

                //File file = new File(path+".txt");//20100823
                //fos = new FileOutputStream(file);//20100823
                //dos = new DataOutputStream(fos);//20100823

                if ( model.MayorizacionMensual.desmayorizar() ) {
                    escribir.println("La Desmayorizacion del mes de " + sacarMes( Integer.parseInt(mes) ) + ", ha finalizado");//20100823
                }
                else
                {   escribir.println("Desmayorizacion Fallida");//20100823
                }
                escribir.close();
                //dos.close();//20100823
                //fos.close();//20100823
                log.finallyProceso("DESMAYORIZACION_X", this.hashCode(), this.usuario.getLogin(),"ok."); //20100820

            }
            catch (Exception e){
                System.out.println("error desmayorizando :"+e.toString());//20100820
                try{log.finallyProceso("DESMAYORIZACION_X", this.hashCode(), this.usuario.getLogin(),"ERROR Hilo: " + e.toString());//20100820
                } catch(Exception f){System.out.println("error en catch:"+f.toString());} //20100820
                e.printStackTrace();//20100820
            }
        }
        else{
            try {

                String hoy  =  Utility.getHoy("-");//20100820
                log.InsertProceso("MAYORIZACION_X", this.hashCode(), hoy ,this.usuario.getLogin() ); //20100820

                model = new Model(usuario.getBd());
                ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
                path = rb.getString("ruta") +
                        "/exportar/migracion/" +
                        usuario.getLogin().toUpperCase() +
                        "/"+usuario.getBd()+
                        "_MayorizacionMensual - " +
                        Util.getFechaActual_String(10).replaceAll(":", "").replaceAll(" ", "");//20100820
                model.MayorizacionMensual.setParams( Integer.parseInt(a�o),
                                                    Integer.parseInt(mes),
                                                    usuario.getLogin(),
                                                    usuario.getBase(),
                                                    usuario.getDstrct() );
                
                PrintWriter escribir = new PrintWriter(new BufferedWriter(new FileWriter(path+".txt")));
                escribir.println("PROCESO DE MAYORIZACION");
                escribir.println("\t\tMAYOR\t\tMOVIMIENTO\tCABECERA");

                vrm = model.MayorizacionMensual.mayorizar();

                for (int i = 0; i < vrm.size(); i++) {
                    rm = (ResultadoMayorizacion) vrm.get(i);
                    //lo siguiente lo hago asi por el espaciado de las columndas en el .TXT
                    if(i == 0 || i == 2){
                        escribir.println( rm.getArchivo() +"\t"+ String.valueOf(rm.getMayor()) +"\t\t"+ String.valueOf(rm.getMovimiento()) +"\t\t"+ String.valueOf(rm.getCabecera()) );
                    }
                    else
                    {
                        escribir.println( rm.getArchivo() +"\t\t"+ String.valueOf(rm.getMayor()) +"\t\t"+ String.valueOf(rm.getMovimiento()) +"\t\t"+ String.valueOf(rm.getCabecera()) );
                    }
                }
                escribir.close();

                log.finallyProceso("MAYORIZACION_X", this.hashCode(), this.usuario.getLogin(),"ok."); //20100820
                
            }
            catch (Exception e){
                System.out.println("error mayorizando :"+e.toString());//20100820
                try{log.finallyProceso("MAYORIZACION_X", this.hashCode(), this.usuario.getLogin(),"ERROR Hilo: " + e.toString());//20100820
                } catch(Exception f){System.out.println("error en catch:"+f.toString());} //20100820
                e.printStackTrace();//20100820
            }
        }
    }   
}
