/*
 * HImportacionComprobantes.java
 *
 * Created on 23 de marzo de 2007, 02:21 PM
 */

package com.tsp.finanzas.contab.model.threads;


import com.tsp.finanzas.contab.model.services.ImportacionComprobantesServices;
import com.tsp.finanzas.contab.model.beans.COMPImportacion;
import com.tsp.finanzas.contab.model.beans.Comprobantes;
import com.tsp.finanzas.contab.model.beans.Tipo_Docto;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.beans.CIA;
import com.tsp.operation.model.threads.Evento;
import com.tsp.util.UtilFinanzas;
import com.tsp.util.Util;
import org.apache.log4j.Logger;


import java.io.*;
import java.util.*;
import java.util.regex.*;
import java.text.*;
/**
 *
 * @author  equipo
 */
public class HImportacionComprobantes extends Evento {
    
    
    FileWriter       fw  = null;
    PrintWriter      pw  = null;
    SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss");
    SimpleDateFormat fmt2= new SimpleDateFormat("yyyyMMdd_kkmmss");
    Date             fechaActual = new Date();
    
    
    ImportacionComprobantesServices ICSvc; 
    Logger  logger = Logger.getLogger(HImportacionComprobantes.class);
        
    
    
    TreeMap cias       = null;
    TreeMap documentos = null;
    
    
    CIA         cia = null; 
    Tipo_Docto  doc = null;      
    
    /** Creates a new instance of HImportacionComprobantes */
    public HImportacionComprobantes() {
    }
    
    /** Crea una nueva instancia de  HImportacionCXP */
    public HImportacionComprobantes(Usuario usuario) {
        super(usuario);
    }
    
 
    /** Procedimiento general del hilo */
    public synchronized void run(){
        try{
            
            
            String ruta = UtilFinanzas.obtenerRuta("ruta", "/exportar/migracion/" + getUsuario().getLogin());
            fw = new FileWriter (ruta + "/"+getUsuario().getBd()+"_LMC_"+ fmt2.format(fechaActual) +".txt");
            pw = new PrintWriter(new BufferedWriter(fw));
            
            
            try{
                ICSvc = new ImportacionComprobantesServices(getUsuario().getBd());
                writeLog("*****************************************************");
                writeLog("* Inicio del Proceso de Importacion de Comprobantes *");
                writeLog("*****************************************************");
                Procesar();
                writeLog("*****************************************************");
                writeLog("*                 Proceso Finalizado                *");
                writeLog("*****************************************************");
            }catch (Exception ex){
                ex.printStackTrace();
                writeLog("*****************************************************");
                writeLog("*     Finalizando Proceso pero con anomalias        *");
                writeLog("*****************************************************");
                writeLog(ex.getMessage());
            }
            
            pw.close();
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }
        
  
    
    
    /**
     * procedimiento general de importacion lectura y agrupacion de de datos
     * @autor mfontalvo
     * @throws Exception.
     * @see ProcesarFactura(List , TreeMap , TreeMap )
     */
    
    public void Procesar()throws Exception{
        
        try {
            int fprocesadas   = 0;
            int fnoprocesadas = 0;
            Vector lista = ICSvc.obtenerComprobantesPendientes(getUsuario().getLogin() );
            if (lista != null && !lista.isEmpty()){
                
                COMPImportacion anterior = null;
                int ranInicial = 0;
                
                /** inicializacion de parametros globales */
                cias       = ICSvc.obtenerDistritos();
                documentos = ICSvc.obtenerDocumentosContables();
                
                
                
                /** recorriendo los comprobantes pendientes por subir */
                for (int ranFinal = 0; ranFinal < lista.size(); ranFinal++){
                    
                    /** obteniendo objeto de  la lista */
                    COMPImportacion item = (COMPImportacion) lista.get(ranFinal);
                    
                    /** procesa una comprobante al detectar una nueva */
                    if (anterior!=null && !anterior.getKey().equals(item.getKey())){
                        
                        /** intenta procesar un comprobante con sus items */
                        try{
                            //System.out.println("C:" + ranInicial + " - " + (ranFinal));
                            if (ProcesarComprobante(lista.subList(ranInicial, ranFinal)))
                                fprocesadas++;
                            else
                                fnoprocesadas++;
                            
                        }catch (Exception ex){
                            fnoprocesadas++;
                            writeLog2 ( ex.getMessage() );
                        }
                        writeLog2("");
                        ranInicial = ranFinal;
                    }
                    anterior = item;
                }
                
                /** intenta procesar el ultimo comprobante con sus items */
                try{
                    //System.out.println("F:" + ranInicial + " - " + lista.size());
                    if (lista.subList(ranInicial, lista.size())!=null && !lista.subList(ranInicial, lista.size()).isEmpty()){
                        if (ProcesarComprobante(lista.subList(ranInicial, lista.size())))
                            fprocesadas++;
                        else
                            fnoprocesadas++;
                    }
                }catch (Exception ex){
                    fnoprocesadas++;
                    writeLog2 ( ex.getMessage() );
                }
                writeLog2("");
                writeLog2("");
                writeLog2("");
                writeLog2("RESUMEN : ");
                writeLog2("Comprobantes PROCESADOS    : " + fprocesadas   );
                writeLog2("Comprobantes NO PROCESADOS : " + fnoprocesadas );
                writeLog2("");
            }
            
        }
        catch(Exception e) {
            e.printStackTrace();
            logger.error( e.getMessage() );
            writeLog2   ( e.getMessage() );
            throw new Exception(e.getMessage());
        }
    }
    
    
    
    
    
    
    

    public boolean ProcesarComprobante(List comprobante)throws Exception{
        String msgError = "";
        boolean estado = false;
        if (comprobante!=null && !comprobante.isEmpty()){
            
            COMPImportacion pItem =  (COMPImportacion) comprobante.get(0);
            writeLog2("____________________________________________");
            writeLog2("Distrito   : " + pItem.getDstrct()         );
            writeLog2("Tipo Doc   : " + pItem.getTipo_documento() );
            writeLog2("Documento  : " + pItem.getDocumento()      );            
            writeLog2("Nro. Items : " + comprobante.size() + "\n" );
            
            try {
                Vector       items    = new Vector ();
                Comprobantes cab      = verificacionCabecera(pItem);
                double total_debito  = 0;
                double total_credito = 0;
                double total_foraneo = 0;
                
                /** recorriendo los items de comprobantes */
                boolean sw = true;
                for (int  i = 0 ; i < comprobante.size() ; i++) {
                    COMPImportacion item = (COMPImportacion) comprobante.get(i);
                    // cambio del numero del item a un numero valido
                    item.setItem( this.getItem(item.getItem()));
                    writeLog2("Verificando Item : " + item.getItem());
                    try{
                        Comprobantes it  = verificacionItem(cab, item);
                        total_debito    += it.getTotal_debito();
                        total_credito   += it.getTotal_credito();
                        total_foraneo   += it.getValor_for();
                        
                        items.add(it);
                    } catch (Exception ex){
                        writeLog2(ex.getMessage() + "\n");
                        sw = false;
                    }
                    
                    total_debito  = Util.roundByDecimal( total_debito , 2);
                    total_credito = Util.roundByDecimal( total_credito, 2);
                    total_foraneo = Util.roundByDecimal( total_foraneo, 2);
                }
                
                if (sw){
                    // validacion de totales
                    if (total_debito != cab.getTotal_debito() )
                        throw new Exception("El Total debito ["+ cab.getTotal_debito() +"] de la cabecera, no corresponde con la sumatoria de los debitos de sus items ["+ total_debito +"], favor verificar");

                    // validacion de totales
                    if (total_credito != cab.getTotal_credito() )
                        throw new Exception("El Total credito ["+ cab.getTotal_credito() +"] de la cabecera, no corresponde con la sumatoria de los creditos de sus items ["+ total_credito +"], favor verificar");
                    
                    // validacion de totales foraneos
                    if (total_foraneo !=  cab.getValor_for() )
                        throw new Exception("El Total Foraneo ["+ cab.getValor_for() +"] de la cabecera, no corresponde con la sumatoria de los foraneos de sus items ["+ total_foraneo +"], favor verificar");
                    
                    // validacion igualdad en creditos y debitos
                    if (cab.getTotal_debito() != cab.getTotal_credito() )
                        throw new Exception("El Total credito ["+ cab.getTotal_credito() +"] debe ser igual al total debito ["+ cab.getTotal_debito() +"], favor verificar");
                    
                    // grabacion del comprobante
                    try{
                        cab.setLista(items);
                        cab.setTotal_items( items.size() );
                        ICSvc.saveComprobante(cab);
                        writeLog2("Comprobante Procesado");
                        estado = true;
                    }catch (Exception ex){
                        ex.printStackTrace();
                        writeLog2(ex.getMessage()); 
                    }
                    
                } 
            }
            catch(Exception e) {
                writeLog2(  e.getMessage() );
                e.printStackTrace();
            } finally{
                return estado;
            }
        } else {
            return false;
        }
        
    }
        
    
    
  
    private Comprobantes verificacionCabecera(COMPImportacion pItem) throws Exception {
        try{
            
            String msgError = "";
            
            Comprobantes cab = new Comprobantes();

            // compa�ia
            cia = (CIA) cias.get( pItem.getDstrct() );
            if (cia==null){
                msgError += "\nDistrito '"+ pItem.getDstrct() +"' no esta registrado en la tabla de Cias";
            }
            
            if (cia!=null && !cia.getDistrito().equals(getUsuario().getDstrct())){
                msgError += "\nEl distrito del usuario que ejecuta el proceso es diferente al del comprobante, deben ser iguales";
            }            
            
            doc = (Tipo_Docto) documentos.get( pItem.getTipo_documento() );
            
            // tipo de documento
            if ( doc == null ){
                msgError += "\nTipo de Documento  '" + pItem.getTipo_documento() + "', no es valido.";
                throw new Exception(msgError);
            }
            
            //  documento
            if (pItem.getDocumento().equals("")){
                msgError += "\nDefina el numero del Documento";
            } else  if (ICSvc.existeDocumento(pItem.getDstrct(),pItem.getTipo_documento(),pItem.getDocumento()) ){
                msgError += "\nEl documento ya esta registrado en la base de datos";
            }
                        
            if (doc.getTercero().equalsIgnoreCase("M") && pItem.getTercero().trim().equals("")){
                msgError += "\nEl tipo de documento definido requiere tercero";
            }            
            
            if (pItem.getTercero().trim().equals("") && !ICSvc.existeNit(pItem.getTercero())){
                msgError += "\nEl nit del tercero no existe o esta anulado.";
            }
            
            if ( !pItem.getMoneda().matches("PES|DOL|BOL") ){
                msgError += "\nMoneda no Valida.";
            }
            
            // validacion del periodo
            try{
                
                int ano = Integer.parseInt(pItem.getPeriodo().substring(0,4));
                int mes = Integer.parseInt(pItem.getPeriodo().substring(4,6));
                
                //System.out.println(ano + "  "  + mes);
                
                if (mes<1 || mes>12)
                    throw new Exception("");
                
                String periodoActual = UtilFinanzas.customFormatDate(new Date(),"yyyyMM");
                if ( Integer.parseInt(pItem.getPeriodo()) > Integer.parseInt(periodoActual)  )
                    msgError += "\nEl periodo no puede ser mayor al periodo actual.";
                
                String estado = ICSvc.obtenerEstadoPeriodoContable(pItem.getDstrct(), String.valueOf(ano), UtilFinanzas.mesFormat(mes));
                if ( estado.equals("") )
                    msgError += "\nEl periodo contable no existe en el sistema";
                else if ( estado.equals("C") )
                    msgError += "\nEl periodo contable ya se encuentra cerrado.";
                
                
            } catch (Exception ex){
                msgError += "\nFormato de periodo no valido, se requiere [AAAAMM]";
            }
            
            
            
            
            try{
                if (!numeroValido( pItem.getDocumento(), pItem.getPeriodo() ) )
                    msgError += "\nNumero del comprobante no cumple con el formato de la serie.";
            } catch (Exception ex){
                msgError += "\nNumero del comprobante no cumple con el formato de la serie.";
            }
            
            
            
            // validacion de la fecha del documento
            try{
                String fecha = pItem.getFecha_documento().replaceAll("/|-", "");
                Date fc = UtilFinanzas.StringToDate(fecha, "yyyyMMdd");
                Date fa = new Date();
                if ( fc.after(fa) )
                    throw new Exception("");
            } catch (Exception ex){
                msgError += "\nFecha del comprobante no valido, esta debe ser menor o igual al dia de hoy.";
            }
            
            
            
            if (!ICSvc.existeTipoOperacion(pItem.getTipo_operacion()  )){
                msgError += "\nEl tipo de operacion no existe o esta anulado.";
            }
            
            
            if (!getUsuario().getLogin().equals( pItem.getCreation_user() )){
                msgError += "\nEl usuario creacion debe ser el mismo que ejecute el proceso";
            }
                     

            
            if (!msgError.equals(""))
                throw new Exception (msgError.replaceFirst("\n",""));
            cab.setDstrct            ( pItem.getDstrct() );
            cab.setTipodoc           ( pItem.getTipo_documento() );
            cab.setNumdoc            ( pItem.getDocumento() );
            cab.setSucursal          ( pItem.getSucursal() );
            cab.setPeriodo           ( pItem.getPeriodo() );
            cab.setFechadoc          ( pItem.getFecha_documento() );
            cab.setDetalle           ( pItem.getDescripcion() );
            cab.setTercero           ( pItem.getTercero() );
            cab.setTotal_debito      ( pItem.getTotal_debito() );
            cab.setTotal_credito     ( pItem.getTotal_credito() );
            cab.setMoneda            ( cia.getMoneda() );
            cab.setUsuario           ( pItem.getCreation_user() );
            cab.setFecha_creacion    ( "now()" );
            cab.setBase              ( pItem.getBase() );
            cab.setTipo_operacion    ( pItem.getTipo_operacion() );
            cab.setValor_for         ( pItem.getTotal_foraneo() );
            cab.setMoneda_foranea    ( pItem.getMoneda() );
            cab.setMoneda            ( "PES" );
           
            return cab;
        } catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
    }    
    
    
    
    
    
    
    
    
    
    
    
 
    private Comprobantes verificacionItem(Comprobantes cab, COMPImportacion item) throws Exception{
        try{
            
            String msgError = "";
            
            Comprobantes it = new Comprobantes();
            // validar otras cabeceras contra la primera
            if (!cab.getDstrct().equals( item.getDstrct())){
                msgError += "\nDistrito '"+ item.getDstrct() +"', no corresponde con el distrito del item base.";
            }
            
            if (!cab.getTipodoc().equals( item.getTipo_documento() )){
                msgError += "\nTipo de Documento '"+ item.getTipo_documento() +"', no corresponde con el tipo de documento del item base";
            }
            if (!cab.getNumdoc().equals( item.getDocumento()  )){
                msgError += "\nDocumento '"+ item.getDocumento() +"' , no corresponde con el documento del item base" ;
            }
            if (!cab.getPeriodo().equals( item.getPeriodo()  )){
                msgError += "\nEl periodo '" + item.getPeriodo()  +"', no corresponde con el periodo del item base";
            }
            if (!cab.getFechadoc().equals( item.getFecha_documento() )){
                msgError += "\nFecha Documento : '" + item.getFecha_documento()  +"', no corresponde con la fecha del documento del item base";
            }
            if (!cab.getDetalle().equals( item.getDescripcion() )){
                msgError += "\nDescripcion del Documento '" + item.getDescripcion()  +"', no corresponde con la descripcion del documento del item base";
            }
            if (!cab.getTercero().equals( item.getTercero() )){
                msgError += "\nEl tercero '" + item.getTercero()  +"', no corresponde conel tercero del item base";
            }            
            if (cab.getTotal_debito() != item.getTotal_debito() ){
                msgError += "\nTotal debito '" + item.getTotal_debito()  +"', no corresponde con el total debito del item base";
            }
            if (cab.getTotal_credito() != item.getTotal_credito() ){
                msgError += "\nTotal credito '" + item.getTotal_credito()  +"', no corresponde con el total credito del item base";
            }   
            if (cab.getValor_for() != item.getTotal_foraneo() ){
                msgError += "\nTotal foraneo '" + item.getTotal_credito()  +"', no corresponde con el total foraneo del item base";
            } 
            
            if (!cab.getSucursal().equals( item.getSucursal())){
                msgError += "\nLa sucursal '" + item.getSucursal()  +"', no corresponde con la sucursal del item base";
            }                        
            if (!cab.getUsuario().equals( item.getCreation_user())){
                msgError += "\nCreation User del Item : '" + item.getCreation_user()  +"', no corresponde con el usuario de creacion del item base";
            }  
            if (!cab.getTipo_operacion().equals( item.getTipo_operacion())){
                msgError += "\nEl tipo de Operacion '" + item.getTipo_operacion()  +"', no corresponde con el tipo de operacion del item base";
            }
            if (!cab.getBase().equals( item.getBase())){
                msgError += "\nLa base '" + item.getBase()  +"', no corresponde con la base del item base";
            }  
            if (!cab.getMoneda_foranea().equals( item.getMoneda())){
                msgError += "\nLa moneda '" + item.getMoneda()  +"', no corresponde con la moneda del item base";
            }
            
            
            // validaciones de la cuenta
            if ( item.getCuenta().trim().equals("") )
                msgError += "\nDebe indicar el codigo de cuenta";
            else {
                String msg =ICSvc.searchCuenta(item.getDstrct(), item.getCuenta(), item.getTipo_auxiliar(), item.getAuxiliar() , item.getTercero_item() );
                if ( !msg.equals("OK" ) )  msgError += "\n" + msg;
            }
            
            if ( item.getDebito() != 0 && item.getCredito() != 0  )
                msgError += "\nSolo se puede asignar un tipo de valor (DEBITO|CREDITO) por item.";
            else if ( item.getDebito() < 0 || item.getCredito() < 0  )
                msgError += "\nNo se permiten valores negativos para los debitos y los creditos.";
            else if ( item.getDebito() == 0 && item.getCredito() == 0  )
                msgError += "\nNo se permiten valores de CERO para los debitos y los creditos. que gracia es esa.";

            /*
            if ( !item.getCuenta().trim().equals("") && "CGI".indexOf(item.getCuenta().substring(0,1))!=-1 ){
                if (item.getAbc().trim().equals("")){
                    msgError += "\nSe requiere codigo ABC";
                } else if ( !ICSvc.searchABC(item.getAbc().trim()) ){ 
                    msgError += "\nEl codigo ABC '"+ item.getAbc().trim() +"' no esta registrado.";
                }
            }else{
                if ( !item.getAbc().trim().equals("") && !ICSvc.searchABC(item.getAbc().trim()) ) 
                    msgError += "\nEl codigo ABC '"+ item.getAbc().trim() +"' no esta registrado.";
            }
            */
            
            
            
            
            if (  ! (item.getTdoc_rel().trim().equals("") && item.getDoc_rel().trim().equals("")) ){
                if ( !item.getTdoc_rel().trim().equals("") && !item.getDoc_rel().trim().equals("") ){
                    //validar segun el tipo
                    
                    
                    if ( item.getTdoc_rel().equalsIgnoreCase("001") ){
                        if ( !ICSvc.existePlanilla( item.getDstrct(), item.getDoc_rel()))
                            msgError += "\nEl documento relacionado (PLANILLA) no se encontro en el sistema";
                    } else if ( item.getTdoc_rel().equalsIgnoreCase("002") ){
                        if ( !ICSvc.existeRemesa( item.getDstrct(), item.getDoc_rel()))
                            msgError += "\nEl documento relacionado (REMESA) no se encontro en el sistema";
                    } else if ( item.getTdoc_rel().equalsIgnoreCase("010") ){
                        if ( !ICSvc.existeFacturaProveedor( item.getDstrct(), item.getTercero_item() , item.getTdoc_rel(), item.getDoc_rel()))
                            msgError += "\nEl documento relacionado (FACTURA PROVEEDOR) no se encontro en el sistema";
                    } else if ( item.getTdoc_rel().equalsIgnoreCase("FAC") ){
                        if ( !ICSvc.existeFacturaCliente(item.getDstrct(), item.getTdoc_rel(), item.getDoc_rel()))
                            msgError += "\nEl documento relacionado (FACTURA CLIENTE) no se encontro en el sistema";
                    } else if ( item.getTdoc_rel().equalsIgnoreCase("ING") ){
                        if ( !ICSvc.existeIngreso( item.getDstrct(), item.getTdoc_rel(), item.getDoc_rel()))
                            msgError += "\nEl documento relacionado (INGRESO CLIENTE) no se encontro en el sistema";
                    } else if ( item.getTdoc_rel().equalsIgnoreCase("EGR") ){
                        if ( !ICSvc.existeEgreso( item.getDstrct(), item.getTdoc_rel(), item.getDoc_rel()))
                            msgError += "\nEl documento relacionado (EGRESO) no se encontro en el sistema";
                    } else {
                        if(item.getTdoc_rel().equalsIgnoreCase("PROV")){
                            msgError +="";
                        }else{
                            msgError += "\nDocumento relacionado no valido.";    
                        }
                    }                                       
                } else
                    msgError += "\nIndique en conjunto el tipo de documento relacionado y el documento relacionado";
            }
                
           
            
            if (!msgError.equals("")){
                throw new Exception (msgError.replaceFirst("\n",""));
            }


            it.setDstrct           ( cab.getDstrct()            );
            it.setTipodoc          ( cab.getTipodoc()           );
            it.setNumdoc           ( cab.getNumdoc()            );
            it.setPeriodo          ( cab.getPeriodo()           );
            it.setDetalle          ( item.getDescripcion_item() );
            it.setCuenta           ( item.getCuenta()           );
            it.setAuxiliar         ( (!item.getAuxiliar().equals("")?item.getTipo_auxiliar() + "-" + item.getAuxiliar():"") );
            it.setTotal_debito     ( item.getDebito()           );
            it.setTotal_credito    ( item.getCredito()          );            
            it.setValor_for        ( item.getValor_foraneo()    );            
            it.setTercero          ( item.getTercero_item()     );
            it.setDocumento_interno( doc.getCodigo_interno()    );
            it.setFecha_creacion   ( cab.getFecha_creacion()    );
            it.setUsuario          ( cab.getUsuario()           );            
            it.setBase             ( cab.getBase()              );
            
            it.setTipodoc_rel      ( item.getTdoc_rel()         );
            it.setNumdoc_rel       ( item.getDoc_rel()          );            
            return it;
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception (ex.getMessage());
        }
    }
    
    
    
    
    
    public boolean numeroValido ( String numero , String periodo ) throws Exception {
        try {
            
            if (doc.getManeja_serie().equalsIgnoreCase("N"))
                return true;
            else {
                String sintaxis = "^";
                ////////////////////////////////////////////////////////////////
                sintaxis += doc.getPrefijo();
                sintaxis += obtenerFormatoAnno(periodo.substring(0,4) , doc.getPrefijo_anio()) ;
                sintaxis += (doc.getPrefijo_mes().equals("MM") ? periodo.substring(4,6) :"" );
                sintaxis += "\\d{" + doc.getLong_serie() + "}";
                ////////////////////////////////////////////////////////////////
                sintaxis += "$";
                //System.out.println( sintaxis );
                Pattern p = Pattern.compile( sintaxis );
                Matcher m = p.matcher(numero);                
                return m.matches();
            }
            
        } catch (Exception ex){
            ex.printStackTrace();
            throw new Exception (ex.getMessage());
        }
    }
    
    
    private String obtenerFormatoAnno(String anno, String p_anno){
        return  ( p_anno.length() == 1 ? anno.substring(3,4) :
                  p_anno.length() == 2 ? anno.substring(2,4) :
                  p_anno.length() == 3 ? anno.substring(1,4) : anno ) ;
    }
    
    /**
     * Metodo para obtener el numero valido del item
     * @autor mfontalvo
     * @param item, cadena a cambiar
     * @throws Exception.
     * @return Numero valido del item
     **/
    public String getItem(String item) throws Exception{
        try{
            String is = "";
            int    in = Integer.parseInt(item);
            if (in < 100)
                is = com.tsp.util.UtilFinanzas.rellenar( String.valueOf(in) , "0", 3);
            else 
                is = String.valueOf(in);
                
            return is;            
        } catch (Exception ex) {
            throw new Exception("Item no valido");
        }
    }
    
    
    
    
    ////////////////////////////////////////////////////////////////////////////
    // METODOS PARA ESCRIBIR EN EL LOG
    ////////////////////////////////////////////////////////////////////////////
    
    

    public void writeLog(String msg)throws Exception {
        try{
            if (pw!=null ){
                String []msgs = msg.split("\\n");
                for (int i = 0; i< msgs.length;i++)
                    pw.println("[" + getCurrentTime() + "] " +  msgs[i] );
                pw.flush();
            }
        } catch (Exception e){
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }
    public void writeLog2(String msg)throws Exception {
        try{
            if (pw!=null ){
                String []msgs = msg.split("\\n");
                for (int i = 0; i< msgs.length ; i++)
                    pw.println( msgs[i] );
                pw.flush();
            }
        } catch (Exception e){
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }    
    public String getCurrentTime(){
        fechaActual.setTime( System.currentTimeMillis());
        return fmt.format(fechaActual );
    }

    
    public static void main (String args[]) throws Exception {
        
        Usuario u = new Usuario();
        u.setDstrct("FINV");
        u.setLogin("RROCHA");
        
        HImportacionComprobantes h = new HImportacionComprobantes( u );
        h.start();

        
    }
}