/*
 * Nombre        HContabilizacion.java
 * Descripci�n   Contabiliza los documentos como planilla.
 * Autor         kreales
 * Fecha         05 de junio
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */

package com.tsp.finanzas.contab.model.threads;

import com.tsp.util.*;
import com.tsp.finanzas.contab.model.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.beans.*;
import com.tsp.finanzas.contab.model.beans.*;
import java.io.*;
import java.sql.*;
import java.util.*;
import java.lang.*;
import javax.servlet.jsp.*;
import javax.servlet.http.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import javax.servlet.http.*;
import com.tsp.util.LogWriter;

public class HContabilizacionAnulacionEgreso extends Thread{
    
    private   String usuario;
    private PrintWriter pw;
    private LogWriter logWriter;
    java.text.SimpleDateFormat anomes;
    java.text.SimpleDateFormat fecha;
    java.text.SimpleDateFormat fechaHora;
    String periodo;
    String fechadoc;
    String fechaInicio;
    java.util.Date pdate;
    java.util.Date ahora;
    String tipodoc;
    String moneda;
    public HContabilizacionAnulacionEgreso() {
    }
    
    /**
     * Establece las variables iniciales y activa el inicio del hilo
     * @param user El usuario en sesi�n
     * @throws Exception Si algun error ocurre en el acceso a la BD
     */
    public void start( String user, String tipo, String moneda ) throws Exception{
        /*
         *Busco la fecha actual y le resto un dia.
         */
        Calendar hoy = Calendar.getInstance();
        hoy.add(hoy.DATE, -1);
        
        pdate = hoy.getTime();
        ahora = new java.util.Date();
        
        anomes = new java.text.SimpleDateFormat("yyyyMM");
        fecha = new java.text.SimpleDateFormat("yyyy-MM-dd");
        fechaHora = new java.text.SimpleDateFormat("yyyy-MM-dd hh:mm");
        
        periodo = anomes.format(pdate);
        fechadoc = fecha.format(pdate);
        fechaInicio= fechaHora.format(ahora);
        
        
        ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
        String ruta = rb.getString("ruta")+ "/exportar/migracion/"+user;
        
        File file = new File(ruta);
        file.mkdirs();
        
        pw = new PrintWriter(System.err, true);
        
        String logFile = ruta + "/HContabilizacionAnulacionEgreso"+fechadoc+".txt";
        
        pw = new PrintWriter(new FileWriter(logFile, true), true);
        logWriter = new LogWriter("HContabilizacionAnulacionEgreso", LogWriter.INFO, pw);
        usuario=user;
        logWriter.setPrintWriter(pw);
        
        tipodoc=tipo;
        this.moneda=moneda;
        super.start();
    }
    
    
    /**
     * Metodo llamado por Java para inciciar la ejecuci�n del hilo
     */
    public void run(){
        com.tsp.finanzas.contab.model.Model model = new com.tsp.finanzas.contab.model.Model();
        com.tsp.operation.model.Model modelOperation = new com.tsp.operation.model.Model();
        int nosubidas=0;
        int subidas=0;
        int canttotal=0;
        int cantPlas=0;
        try{
            
            
            logWriter.log("**********************************************************",LogWriter.INFO);
            logWriter.log("*  NUEVO PROCESO DE CONTABILIZACION DE ANULACIONEGR      *",LogWriter.INFO);
            logWriter.log("*  Periodo  :  "+periodo+"                               *",LogWriter.INFO);
            logWriter.log("*  FechaDoc :  "+fechadoc+"                              *",LogWriter.INFO);
            logWriter.log("**********************************************************",LogWriter.INFO);
            
            //com.tsp.operation.model.Model modelo =  new com.tsp.operation.model.Model();
            modelOperation.LogProcesosSvc.InsertProceso("Generacion Comprobantes", this.hashCode(), "Generacion Comprobantes Anulacion Egresos", usuario);
            
            //BUSCO LOS PLAREM QUE HAN SIDO CAMBIADOS.
            model.comprobanteService.buscarListaEgresosAnulados();
            Vector plas = model.comprobanteService.getVector();
            cantPlas=plas.size();
            logWriter.log("Se encontraron "+plas.size()+" registros para contabilizar",LogWriter.INFO);
            
            String tipos = "='EGR'" ;
            for(int i =0; i<plas.size();i++){
                
                Comprobantes c = (Comprobantes) plas.elementAt(i);
                c.setTipodoc(tipos);
                
                String branch_code = c.getBanco();
                String agencia = c.getSucursal_banco();
                model.comprobanteService.setComprobante(c);
                
                model.comprobanteService.listarComprobantes();
                Vector comprobantes = model.comprobanteService.getVector();
                if(comprobantes.size()>0){
                    canttotal++;
                    subidas++;
                }
                
                for(int j =0; j<comprobantes.size();j++){
                    Vector consultas = new Vector();
                    
                    
                    Comprobantes cpla = (Comprobantes) comprobantes.elementAt(j);
                    //System.out.println("Encontre los comprobantes de "+cpla.getNumdoc());
                    //System.out.println("Comprobante CPL "+cpla.getGrupo_transaccion()+" Comprobante C."+c.getGrupo_transaccion());
                    
                    if(cpla.getGrupo_transaccion()==c.getGrupo_transaccion()){
                        
                        double totaldebito=cpla.getTotal_debito();
                        double totalCredito = cpla.getTotal_credito();
                        model.comprobanteService.listarComprobantesDet();
                        
                        cpla.setTotal_credito(totaldebito);
                        cpla.setTotal_debito(totalCredito);
                        cpla.setDetalle("ANULA "+cpla.getDetalle());
                        cpla.setTipodoc(tipodoc);
                        cpla.setPeriodo(periodo);
                        cpla.setFechadoc(fechadoc);
                        cpla.setAprobador(usuario);
                        cpla.setMoneda(moneda);
                        cpla.setTipo_operacion("AEGR");
                        int gtransaccion = model.comprobanteService.getGrupoTransaccion();
                        cpla.setGrupo_transaccion(gtransaccion);
                        cpla.setTipo_operacion("AEGR");
                        cpla.setBanco(branch_code);
                        cpla.setSucursal_banco(agencia);
                        
                      /*  int gtransaccion = model.comprobanteService.getGrupoTransaccion();
                        c.setGrupo_transaccion(gtransaccion);
                       */
                        model.comprobanteService.setComprobante(cpla);
                        consultas.add(model.comprobanteService.insert());
                        consultas.add(model.comprobanteService.actualizarTablaGen());
                        
                        
                        // modelo.despachoService.insertar(consultas);
                        
                        cpla.setTipodoc(tipos);
                        model.comprobanteService.setComprobante(cpla);
                        
                        
                        
                        
                        Vector detalles = model.comprobanteService.getVector();
                        for(int k =0; k<detalles.size();k++){
                            
                            Comprobantes cdet = (Comprobantes) detalles.elementAt(k);
                            
                            
                            //System.out.println("Encontre los detalles de "+cdet.getNumdoc());
                            
                            double valordebito=cdet.getTotal_debito();
                            double valorCredito = cdet.getTotal_credito();
                            cdet.setTipodoc(tipodoc);
                            cdet.setTotal_debito(valorCredito);
                            cdet.setTotal_credito(valordebito);
                            cdet.setPeriodo(periodo);
                            cdet.setFechadoc(fechadoc);
                            cdet.setAprobador(usuario);
                            cdet.setDetalle("ANULA "+cdet.getDetalle());
                            cdet.setMoneda(moneda);
                            cdet.setTipo_operacion("AEGR");
                            cdet.setGrupo_transaccion(gtransaccion);
                            //System.out.println("Se guarda con numero de transaccion "+gtransaccion);
                            //System.out.println("Se guarda con tipo de documento "+cdet.getTipodoc());
                            //System.out.println("Se guarda con numero de egreso "+cdet.getNumdoc());
                            cdet.setBanco(branch_code);
                            cdet.setSucursal_banco(agencia);
                            model.comprobanteService.setComprobante(cdet);
                            
                            consultas.add(model.comprobanteService.insertItem());
                            
                        }
                        consultas.add(model.comprobanteService.marcarEgresoAnulado());
                    }
                    
                    for(int l = 0; l< consultas.size(); l++){
                        //System.out.println("Insertar :"+(String)consultas.elementAt(l));
                    }
                    modelOperation.despachoService.insertar(consultas);
                    
                }
                
            }
            
            
            
            
            java.util.Date fin = new java.util.Date();
            String fechaFin= fechaHora.format(fin);
            
            //ENVIAMOS EMAIL DE LA CONTABILIZACION
            String mailInt="";
            String mailExt="";
            boolean swInt=false;
            boolean swExt=false;
            LinkedList lista = modelOperation.tablaGenService.obtenerInfoTablaGen("EMAIL", "contabpla");
            if(lista!=null){
                Iterator it = lista.iterator();
                while(it.hasNext()){
                    TablaGen t = (TablaGen)it.next();
                    //System.out.println("-"+t.getDescripcion());
                    
                    if(t.getReferencia().equals("I")){
                        //verifico si exsite una arroba en la descripcion
                        if( t.getDescripcion().indexOf("@") != -1){
                            mailInt += t.getDescripcion()+";";
                        }
                        //se obtiene el login del usuario y se busca el email
                        else{
                            modelOperation.usuarioService.searchUsuario( t.getDescripcion().toUpperCase() );
                            Usuario u = modelOperation.usuarioService.getUsuario();
                            if(!u.getEmail().equals(""))
                                mailInt += u.getEmail()+";";
                        }
                        swInt = true;
                    }
                    //si el email del cliente es externo
                    else{
                        //verifico si exsite una arroba en la descripcion
                        if( t.getDescripcion().indexOf("@") != -1){
                            mailExt += t.getDescripcion()+";";
                        }
                        else{
                            //se obtiene el login del usuario y se busca el email
                            modelOperation.usuarioService.searchUsuario( t.getDescripcion().toUpperCase() );
                            Usuario u = modelOperation.usuarioService.getUsuario();
                            if(!u.getEmail().equals(""))
                                mailExt += u.getEmail()+";";
                        }
                        swExt = true;
                    }
                }
                
                //System.out.println("interno "+mailInt);
                //System.out.println("externo "+mailExt);
                
                com.tsp.operation.model.beans.SendMail email = new com.tsp.operation.model.beans.SendMail();
                email.setEmailfrom("procesos@sanchezpolo.com");
                email.setSendername( "PROCESO CONTABILIDAD" );
                email.setRecstatus("A");
                email.setEmailcode("");
                email.setEmailcopyto("");
                email.setEmailsubject("CONTABILIZACION ANULACION DE EGRESO");
                email.setEmailbody("SE GENERO LA CONTABILIZACION DE LA ANULACION DE UN EGRESO EN UN PROCESO AUTOMATICO \n\n" +
                "Anulaciones Contabilizadas:    "+subidas+
                "\nAnulaciones NO contabilizadas: " +nosubidas+
                "\n\nFECHA INICIO:    "+ fechaInicio
                +"\nFECHA FINAL      "+ fechaFin+
                "\n\nProceso finalizado con exito" +
                "\n\n\nNOTA: Para mas informacion a cerca de las anulaciones no contabilizadas consulte" +
                " el log de contabilizacion.");
                if(swInt==true){
                    mailInt = mailInt.substring(0, mailInt.length() - 1 );
                    email.setEmailto(mailInt);
                    email.setTipo("I");
                    modelOperation.sendMailService.sendMail(email);
                }
                if(swExt==true){
                    mailExt = mailExt.substring(0, mailExt.length() - 1 );
                    email.setEmailto(mailExt);
                    email.setTipo("E");
                    modelOperation.sendMailService.sendMail(email);
                }
                
            }
            
            modelOperation.LogProcesosSvc.finallyProceso("Generacion Comprobantes", this.hashCode(), usuario, "PROCESO EXITOSO");
            
        }catch(Exception e){
            
            try{
                modelOperation.LogProcesosSvc.finallyProceso("Generacion Comprobantes", this.hashCode(),usuario,"ERROR :" + e.getMessage());
            }catch ( SQLException ex) {
                //System.out.println("Error guardando el proceso");
            }
            logWriter.log("ERROR: "+e.toString(),LogWriter.ERROR);
            e.printStackTrace();
        }
        finally{
            canttotal = canttotal-( subidas+nosubidas);
            logWriter.log("FIN DEL PROCESO ",LogWriter.INFO);
            logWriter.log("Anulaciones Totales encontradas:    "+cantPlas,LogWriter.INFO);
            logWriter.log("Anulaciones Contabilizadas:    "+subidas,LogWriter.INFO);
            logWriter.log("Anulaciones NO contabilizadas: " +nosubidas,LogWriter.INFO);
            logWriter.log("Anulaciones NO contabilizadas por ERROR DEL PROGRAMA: " +canttotal,LogWriter.INFO);
        }
    }
    
    
    public static void main(String a [])throws Exception{
        try{
            com.tsp.finanzas.contab.model.services.ComprobantesService c = new com.tsp.finanzas.contab.model.services.ComprobantesService();
            String tipoDoc= c.getTipoDocumento("003");
            
            HContabilizacionAnulacionEgreso hilo = new HContabilizacionAnulacionEgreso();
            hilo.start("ADMIN",tipoDoc,"PES");
        }catch(SQLException e){
            //System.out.println("Error "+e.getMessage());
        }
    }
}
