/*
 * HReporteMoviminetoAuxiliar.java
 *
 * Created on 20 de abril de 2007, 11:57 AM
 */

package com.tsp.finanzas.contab.model.threads;

import com.tsp.operation.model.beans.POIWrite;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.finanzas.contab.model.beans.*;
import com.tsp.finanzas.contab.model.services.MovAuxiliarServices;
import com.tsp.operation.model.LogProcesosService;


import java.io.*;
import java.util.*;
import java.text.*;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.util.HSSFColor;

public class HReporteMovimientoAuxiliar extends Thread{

    String distrito;
    String cuenta_ri;
    String cuenta_rf;
    String cuenta;
    String fecha_ri;
    String fecha_rf;
    String periodo;

    MovAuxiliarServices movSvc;
    LogProcesosService  logSvc;
    Usuario usuario;
    SimpleDateFormat fmt;
    String processName = "Movimiento Auxiliar";



    // variables del archivo de excel
    String   ruta;
    String   nombre;
    POIWrite xls;
    HSSFCellStyle header  , titulo1, titulo2, titulo3 , titulo4, titulo5, letra, numero, dinero, numeroCentrado, porcentaje, letraCentrada, numeroNegrita;
    HSSFColor     cAzul   , cVerde, cAmarillo, cGris ;
    int fila = 0;



    /** Creates a new instance of HReporteMoviminetoAuxiliar */
    public HReporteMovimientoAuxiliar() {
    }

    public void start(MovAuxiliarServices movSvc, LogProcesosService logSvc, String distrito, String cuenta_ri, String cuenta_rf, String fecha_ri, String fecha_rf, String cuenta, String periodo, Usuario usuario){
        this.distrito   = distrito;
        this.cuenta_ri  = cuenta_ri;
        this.cuenta_rf  = cuenta_rf;
        this.fecha_ri   = fecha_ri;
        this.fecha_rf   = fecha_rf;
        this.cuenta     = cuenta;
        this.periodo    = periodo;
        this.usuario    = usuario;
        this.movSvc     = movSvc;
        this.logSvc     = logSvc;
        super.start();
    }


    public synchronized void run(){
        try{
            logSvc.InsertProceso(this.processName, this.hashCode(), "Generacion del Reporte Movimiento Auxiliar.", usuario.getLogin());
            this.generarRUTA();
            this.generarArchivo();
            logSvc.finallyProceso(this.processName, this.hashCode(), usuario.getLogin(), "PROCESO EXITOSO");
        }catch (Exception ex){
            ex.printStackTrace();
            try{
                logSvc.finallyProceso(this.processName, this.hashCode(), usuario.getLogin(), "Finalizado con error ...\n" + ex.getMessage());
            }catch (Exception e){
                System.out.println("Error HReporteOcsConAnticipoSinReporte ...\n" + e.getMessage());
            }
        }
    }

 public void generarRUTA() throws Exception{
        try{

            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            ruta = rb.getString("ruta") + "/exportar/migracion/" + usuario.getLogin();
            File archivo = new File( ruta );
            if (!archivo.exists()) archivo.mkdirs();

        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }

    }


    /**
     * Metodo para Incializar las variables del archivo de excel
     * @autor mfontalvo
     * @param nameFile, nombre del archivo
     * throws Exception.
     */
    private void InitArchivo(String nameFile) throws Exception{
        try{
            xls          = new com.tsp.operation.model.beans.POIWrite();
            nombre       = "/exportar/migracion/" + usuario.getLogin() + "/" + nameFile;
            xls.nuevoLibro( ruta + "/" + nameFile );

            // estilos
            header         = xls.nuevoEstilo("Tahoma", 14, true  , false, "text"  , HSSFColor.ORANGE.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
            titulo1        = xls.nuevoEstilo("Tahoma", 8 , true  , false, "text"  , xls.NONE  , xls.NONE , xls.NONE);
            titulo2        = xls.nuevoEstilo("Tahoma", 8 , true  , false, "text"  , HSSFColor.WHITE.index, HSSFColor.DARK_BLUE.index , HSSFCellStyle.ALIGN_CENTER, 2);
            letra          = xls.nuevoEstilo("Tahoma", 8 , false , false, ""      , xls.NONE , xls.NONE , xls.NONE);
            letraCentrada  = xls.nuevoEstilo("Tahoma", 8 , false , false, ""      , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_CENTER);
            numero         = xls.nuevoEstilo("Tahoma", 8 , false , false, "#"     , xls.NONE , xls.NONE , xls.NONE);
            numeroCentrado = xls.nuevoEstilo("Tahoma", 8 , false , false, "#"     , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_CENTER);
            dinero         = xls.nuevoEstilo("Tahoma", 8 , false , false, "#,##0.00" , xls.NONE , xls.NONE , xls.NONE);
            numeroNegrita  = xls.nuevoEstilo("Tahoma", 8 , true  , false, "#"     , xls.NONE , xls.NONE , xls.NONE);
            porcentaje     = xls.nuevoEstilo("Tahoma", 8 , false , false, "0.00%" , xls.NONE , xls.NONE , xls.NONE);

        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception("Error en InitArchivo .... \n" + ex.getMessage());
        }

    }



    /**
     * Metodo para crear el  archivo de excel
     * @autor mfontalvo
     * @param nameFile, Nombre del archivo
     * @param titulo, titulo del archivo de excel
     * @throws Exception.
     */
    /*private void crearArchivo(String nameFile, String titulo) throws Exception{
        try{
            fmt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            InitArchivo(nameFile);
            xls.obtenerHoja("Base");
            xls.combinarCeldas(0, 0, 0, 8);
            xls.adicionarCelda(0,0, titulo, header);
            xls.adicionarCelda(1,0, "FECHA REPORTE" , titulo1);
            xls.adicionarCelda(1,1, fmt.format( new Date())  , titulo1 );


            if (cuenta.equals("")){
                xls.adicionarCelda(2,0, "RANGO CUENTAS", titulo1);
                xls.adicionarCelda(2,1, cuenta_ri + " - " + cuenta_rf  , titulo1);
            } else{
                xls.adicionarCelda(2,0, "CUENTA", titulo1);
                xls.adicionarCelda(2,1, cuenta , titulo1);
            }



            if (periodo.equals("")){
                xls.adicionarCelda(3,0, "RANGO FECHAS", titulo1);
                xls.adicionarCelda(3,1, fecha_ri + " - " + fecha_rf  , titulo1);
            } else {
                xls.adicionarCelda(3,0, "PERIODO", titulo1);
                xls.adicionarCelda(3,1, periodo  , titulo1);
            }
            xls.adicionarCelda(4,0, "USUARIO", titulo1);
            xls.adicionarCelda(4,1, usuario.getNombre() , titulo1);


        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception( "Error en crearArchivo ....\n"  + ex.getMessage() );
        }
    }*/

    /**
     * Metodo para crear el  archivo de excel
     * @autor mfontalvo
     * @param nameFile, Nombre del archivo
     * @param titulo, titulo del archivo de excel
     * @throws Exception.
     */
    private void crearArchivo(String titulo, String hoja) throws Exception{
        try{
            fmt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            //InitArchivo(nameFile);
            xls.obtenerHoja("hoja1");
            xls.combinarCeldas(0, 0, 0, 8);
            xls.adicionarCelda(0,0, titulo, header);
            xls.adicionarCelda(1,0, "FECHA REPORTE" , titulo1);
            xls.adicionarCelda(1,1, fmt.format( new Date())  , titulo1 );


            if (cuenta.equals("")){
                xls.adicionarCelda(2,0, "RANGO CUENTAS", titulo1);
                xls.adicionarCelda(2,1, cuenta_ri + " - " + cuenta_rf  , titulo1);
            } else{
                xls.adicionarCelda(2,0, "CUENTA", titulo1);
                xls.adicionarCelda(2,1, cuenta , titulo1);
            }



            if (periodo.equals("")){
                xls.adicionarCelda(3,0, "RANGO FECHAS", titulo1);
                xls.adicionarCelda(3,1, fecha_ri + " - " + fecha_rf  , titulo1);
            } else {
                xls.adicionarCelda(3,0, "PERIODO", titulo1);
                xls.adicionarCelda(3,1, periodo  , titulo1);
            }
            xls.adicionarCelda(4,0, "USUARIO", titulo1);
            xls.adicionarCelda(4,1, usuario.getNombre() , titulo1);


            //

            xls.obtenerHoja("hoja2");
            xls.combinarCeldas(0, 0, 0, 8);
            xls.adicionarCelda(0,0, titulo, header);
            xls.adicionarCelda(1,0, "FECHA REPORTE" , titulo1);
            xls.adicionarCelda(1,1, fmt.format( new Date())  , titulo1 );


            if (cuenta.equals("")){
                xls.adicionarCelda(2,0, "RANGO CUENTAS", titulo1);
                xls.adicionarCelda(2,1, cuenta_ri + " - " + cuenta_rf  , titulo1);
            } else{
                xls.adicionarCelda(2,0, "CUENTA", titulo1);
                xls.adicionarCelda(2,1, cuenta , titulo1);
            }



            if (periodo.equals("")){
                xls.adicionarCelda(3,0, "RANGO FECHAS", titulo1);
                xls.adicionarCelda(3,1, fecha_ri + " - " + fecha_rf  , titulo1);
            } else {
                xls.adicionarCelda(3,0, "PERIODO", titulo1);
                xls.adicionarCelda(3,1, periodo  , titulo1);
            }
            xls.adicionarCelda(4,0, "USUARIO", titulo1);
            xls.adicionarCelda(4,1, usuario.getNombre() , titulo1);
            //



        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception( "Error en crearArchivo ....\n"  + ex.getMessage() );
        }
    }



    /**
     * Exportacion del las amortizaciones transferidas
     * @autor mfontalvo
     * @throws Exception.
     */
   /* private void generarArchivo() throws Exception {
        try{
            fmt= new SimpleDateFormat("yyyMMdd_hhmmss");
            this.crearArchivo("ReporteMOVAUX_" + fmt.format( new Date() ) +".xls", "REPORTE MOVIMIENTO AUXILIAR");
            fila = 6;

            movSvc.obtenerCuentas(distrito, cuenta_ri, cuenta_rf, fecha_ri, fecha_rf, cuenta, periodo);
            Vector vector = movSvc.getDatos();
            if (vector!=null && !vector.isEmpty()){



                    // encabezado
                String [] cabecera = {
                    "CUENTA", "AUXILIAR", "PERIODO", "FECHA", "TIPO COMPROBANTE", "NUMERO COMPROBANTE", "DESCRIPCION", "ABC",  "DEBITO", "CREDITO",
                    "VALOR FORANEO","MONEDA FORANEA",
                    "NIT TERCERO", "TERCERO", "TIPO DOC REL", "DOC REL"

                };
                short  [] dimensiones = new short [] {
                    7000, 7000, 4000, 4000, 8000, 6000, 10000, 4000, 4500, 4500,
                    4500, 4500,
                    4000, 8000, 5000, 5000
                };
                for ( int i = 0; i<cabecera.length; i++){
                    xls.adicionarCelda(fila,  i, cabecera[i], titulo2);
                    if ( i < dimensiones.length )
                        xls.cambiarAnchoColumna(i, dimensiones[i] );
                }
                fila++;


                for ( int i = 0; i< vector.size(); i++, fila++){
                    int col = 0;
                    ComprobanteFacturas c = (ComprobanteFacturas) vector.get(i);

                    // datos del comprobante
                    xls.adicionarCelda(fila  , col++ , c.getCuenta()            , letra          );
                    xls.adicionarCelda(fila  , col++ , c.getAuxiliar()          , letra          );
                    xls.adicionarCelda(fila  , col++ , c.getPeriodo()           , letraCentrada  );
                    xls.adicionarCelda(fila  , col++ , c.getFechadoc()          , letraCentrada  );
                    xls.adicionarCelda(fila  , col++ , c.getTipodoc()           , letra          );
                    xls.adicionarCelda(fila  , col++ , c.getNumdoc()            , letra          );
                    xls.adicionarCelda(fila  , col++ , c.getDetalle()           , letra          );
                    xls.adicionarCelda(fila  , col++ , c.getAbc()               , letraCentrada  );
                    xls.adicionarCelda(fila  , col++ , c.getTotal_debito()      , dinero         );
                    xls.adicionarCelda(fila  , col++ , c.getTotal_credito()     , dinero         );
                    xls.adicionarCelda(fila  , col++ , c.getVlr_item_for()      , dinero         );
                    xls.adicionarCelda(fila  , col++ , c.getMoneda_foranea()    , dinero         );
                    xls.adicionarCelda(fila  , col++ , c.getTercero()           , letraCentrada  );
                    xls.adicionarCelda(fila  , col++ , c.getNombre_tercero()    , letra          );
                    xls.adicionarCelda(fila  , col++ , c.getTipodoc_rel()       , letraCentrada  );
                    xls.adicionarCelda(fila  , col++ , c.getNumdoc_rel()        , letra          );
                }



            } else {
                xls.adicionarCelda(fila  ,0, "NO HAY DATOS PARA MOSTRAR", titulo5);
                xls.combinarCeldas(fila , 0, fila, 0+5);
                fila++;
            }


            this.cerrarArchivo();
            xls = null;
            vector = null;
            movSvc.setDatos(null);

        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception("Error en generarArchivo ...\n" + ex.getMessage() );
        }
    }*/

    /**
     * Exportacion del las amortizaciones transferidas
     * @autor mfontalvo
     * @throws Exception.
     */
    private void generarArchivo() throws Exception {
        try{
            fmt= new SimpleDateFormat("yyyMMdd_hhmmss");

            this.creacionArchivo(this.usuario.getBd()+"_ReporteMOVAUX_" + fmt.format( new Date() ) +".xls");
            this.crearArchivo("REPORTE MOVIMIENTO AUXILIAR", "hoja1");
            xls.obtenerHoja("hoja1");
            fila = 6;

            movSvc.obtenerCuentas(usuario,distrito, cuenta_ri, cuenta_rf, fecha_ri, fecha_rf, cuenta, periodo);
            Vector vector = movSvc.getDatos();
            if (vector!=null && !vector.isEmpty()){

                xls.adicionarCelda(fila  ,0, "ESCRIBIO DATOS", titulo5);



                    // encabezado
                /*
                String [] cabecera = {
                    "CUENTA", "AUXILIAR", "PERIODO", "FECHA", "TIPO COMPROBANTE", "NUMERO COMPROBANTE", "DESCRIPCION", "ABC",  "DEBITO", "CREDITO",
                    "VALOR FORANEO","MONEDA FORANEA",
                    "NIT TERCERO", "TERCERO", "TIPO DOC REL", "DOC REL"

                };
                 */
                String [] cabecera = {
                    "CUENTA", "AUXILIAR", "PERIODO", "FECHA", "TIPO COMPROBANTE", "NUMERO COMPROBANTE", "DESCRIPCION", "ABC",  "DEBITO", "CREDITO",
                    "VALOR FORANEO","MONEDA FORANEA",
                    "NIT TERCERO", "TERCERO", "TIPO DOC REL", "DOC REL", "TIPO REF 1", "REF 1", "TIPO REF 2", "REF 2", "TIPO REF 3", "REF 3","FACTURA AVAL","MULTISERVICIO"

                };
                /*
                short  [] dimensiones = new short [] {
                    7000, 7000, 4000, 4000, 8000, 6000, 10000, 4000, 4500, 4500,
                    4500, 4500,
                    4000, 8000, 5000, 5000
                };
                 */
                short  [] dimensiones = new short [] {
                    7000, 7000, 4000, 4000, 8000, 6000, 10000, 4000, 4500, 4500,
                    4500, 4500,
                    4000, 8000, 5000, 8000, 5000, 8000, 5000, 8000, 5000, 8000, 7000,7000
                };

                for ( int i = 0; i<cabecera.length; i++){
                    xls.adicionarCelda(fila,  i, cabecera[i], titulo2);
                    if ( i < dimensiones.length )
                        xls.cambiarAnchoColumna(i, dimensiones[i] );
                }
                fila++;


                for ( int i = 0; i< vector.size(); i++, fila++){
                    int col = 0;
                    ComprobanteFacturas c = (ComprobanteFacturas) vector.get(i);

                    // datos del comprobante
                    xls.adicionarCelda(fila  , col++ , c.getCuenta()            , letra          );
                    xls.adicionarCelda(fila  , col++ , c.getAuxiliar()          , letra          );
                    xls.adicionarCelda(fila  , col++ , c.getPeriodo()           , letraCentrada  );
                    xls.adicionarCelda(fila  , col++ , c.getFechadoc()          , letraCentrada  );
                    xls.adicionarCelda(fila  , col++ , c.getTipodoc()           , letra          );
                    xls.adicionarCelda(fila  , col++ , c.getNumdoc()            , letra          );
                    xls.adicionarCelda(fila  , col++ , c.getDetalle()           , letra          );
                    xls.adicionarCelda(fila  , col++ , c.getAbc()               , letraCentrada  );
                    xls.adicionarCelda(fila  , col++ , c.getTotal_debito()      , dinero         );
                    xls.adicionarCelda(fila  , col++ , c.getTotal_credito()     , dinero         );
                    xls.adicionarCelda(fila  , col++ , c.getVlr_item_for()      , dinero         );
                    xls.adicionarCelda(fila  , col++ , c.getMoneda_foranea()    , dinero         );
                    xls.adicionarCelda(fila  , col++ , c.getTercero()           , letraCentrada  );
                    xls.adicionarCelda(fila  , col++ , c.getNombre_tercero()    , letra          );
                    xls.adicionarCelda(fila  , col++ , c.getTipodoc_rel()       , letraCentrada  );
                    xls.adicionarCelda(fila  , col++ , c.getNumdoc_rel()        , letra          );

                    xls.adicionarCelda(fila  , col++ , c.getTipo_referencia_1() , letra          );
                    xls.adicionarCelda(fila  , col++ , c.getReferencia_1()      , letra          );
                    xls.adicionarCelda(fila  , col++ , c.getTipo_referencia_2() , letra          );
                    xls.adicionarCelda(fila  , col++ , c.getReferencia_2()      , letra          );
                    xls.adicionarCelda(fila  , col++ , c.getTipo_referencia_3() , letra          );
                    xls.adicionarCelda(fila  , col++ , c.getReferencia_3()      , letra          );
                    //modifiquemos el excel......!!!!!
                    xls.adicionarCelda(fila  , col++ , c.getDocumento_rel2()    , letra          );
                    xls.adicionarCelda(fila  , col++ , c.getMultiservicio(), letra          );

                }



            } else {
                xls.adicionarCelda(fila  ,0, "NO HAY DATOS PARA MOSTRAR", titulo5);
                xls.combinarCeldas(fila , 0, fila, 0+5);
                fila++;
            }

            this.cerrarArchivo();
            xls = null;
            vector = null;
            movSvc.setDatos(null);

        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception("Error en generarArchivo ...\n" + ex.getMessage() );
        }
    }


    /**
     * Metodo para cerrar el  archivo de excel
     * @autor mfontalvo
     * @throws Exception.
     */
    private void cerrarArchivo() throws Exception {
        try{
            if (xls!=null)
                xls.cerrarLibro();
        }catch (Exception ex){
            throw new Exception( "Error en crearArchivo ....\n"  + ex.getMessage() );
        }
    }

     /**
     *Metodo Para Crear el Archivo
     *
     *
    */
    private void creacionArchivo(String nameFile){
        try{
        fmt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        InitArchivo(nameFile);
        }catch (Exception e){
        }


    }


}