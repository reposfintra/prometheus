/*
 * Nombre        HContabilizacion.java
 * Descripci�n   Contabiliza los documentos como planilla.
 * Autor         kreales
 * Fecha         05 de junio
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */

package com.tsp.finanzas.contab.model.threads;

import com.tsp.util.*;
import com.tsp.finanzas.contab.model.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.beans.*;
import com.tsp.finanzas.contab.model.beans.*;
import java.io.*;
import java.sql.*;
import java.util.*;
import java.lang.*;
import javax.servlet.jsp.*;
import javax.servlet.http.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import javax.servlet.http.*;
import com.tsp.util.LogWriter;

public class HContabilizacionAnulacionPlanilla extends Thread{
    
    private   String usuario;
    private PrintWriter pw;
    private LogWriter logWriter;
    java.text.SimpleDateFormat anomes;
    java.text.SimpleDateFormat fecha;
    java.text.SimpleDateFormat fechaHora;
    String periodo;
    String fechadoc;
    String fechaInicio;
    java.util.Date pdate;
    java.util.Date ahora;
    String tipodoc;
    String moneda;
    
    public HContabilizacionAnulacionPlanilla() {
    }
    
    /**
     * Establece las variables iniciales y activa el inicio del hilo
     * @param user El usuario en sesi�n
     * @throws Exception Si algun error ocurre en el acceso a la BD
     */
    public void start( String user, String tipo, String moneda ) throws Exception{
        /*
         *Busco la fecha actual y le resto un dia.
         */
        Calendar hoy = Calendar.getInstance();
        hoy.add(hoy.DATE, -1);
        
        pdate = hoy.getTime();
        ahora = new java.util.Date();
        
        anomes = new java.text.SimpleDateFormat("yyyyMM");
        fecha = new java.text.SimpleDateFormat("yyyy-MM-dd");
        fechaHora = new java.text.SimpleDateFormat("yyyy-MM-dd hh:mm");
        
        periodo = anomes.format(pdate);
        fechadoc = fecha.format(pdate);
        fechaInicio= fechaHora.format(ahora);
        
        
        ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
        String ruta = rb.getString("ruta")+ "/exportar/migracion/"+user;
        
        File file = new File(ruta);
        file.mkdirs();
        
        
        pw = new PrintWriter(System.err, true);
        
        String logFile = ruta + "/HContabilizacionAnulacionPla"+fechadoc+".txt";
        
        pw = new PrintWriter(new FileWriter(logFile, true), true);
        logWriter = new LogWriter("HContabilizacionAnulacionPla", LogWriter.INFO, pw);
        usuario=user;
        logWriter.setPrintWriter(pw);
        
        tipodoc=tipo;
        this.moneda = moneda;
        
        super.start();
    }
    
    
    /**
     * Metodo llamado por Java para inciciar la ejecuci�n del hilo
     */
    public void run(){
        com.tsp.finanzas.contab.model.Model model = new com.tsp.finanzas.contab.model.Model();
        com.tsp.operation.model.Model modelOperation = new com.tsp.operation.model.Model();
        int nosubidas=0;
        int subidas=0;
        int canttotal=0;
        int cantPlas=0;
        try{
            
            logWriter.log("**********************************************************",LogWriter.INFO);
            logWriter.log("*  NUEVO PROCESO DE CONTABILIZACION DE ANULACIONPLA      *",LogWriter.INFO);
            logWriter.log("*  Periodo  :  "+periodo+"                               *",LogWriter.INFO);
            logWriter.log("*  FechaDoc :  "+fechadoc+"                              *",LogWriter.INFO);
            logWriter.log("**********************************************************",LogWriter.INFO);
            
            
            modelOperation.LogProcesosSvc.InsertProceso("Generacion Comprobantes", this.hashCode(), "Generacion Comprobantes Planillas Anuladas", usuario);
            
            //BUSCO LOS PLAREM QUE HAN SIDO CAMBIADOS.
            model.comprobanteService.buscarListaPlaAnuladas();
            Vector plas = model.comprobanteService.getVector();
            
            
            
            String tipos = "in ('PLA','RPL')" ;
            cantPlas=plas.size();
            com.tsp.operation.model.Model modelo =  new com.tsp.operation.model.Model();
            
            for(int i =0; i<plas.size();i++){
                
                
                Comprobantes c = (Comprobantes) plas.elementAt(i);
                c.setTipodoc(tipos);
                c.setDocumento_interno("001");
                c.setMoneda(moneda);
                c.setTipo_operacion("APL");
                model.comprobanteService.setComprobante(c);
                
                model.comprobanteService.listarComprobantes();
                Vector comprobantes = model.comprobanteService.getVector();
                if(comprobantes.size()>0){
                    canttotal++;
                    subidas++;
                }
                for(int j =0; j<comprobantes.size();j++){
                    Vector consultas = new Vector();
                   // Vector consultas2 = new Vector();
                    
                    Comprobantes cpla = (Comprobantes) comprobantes.elementAt(j);
                    //  //System.out.println("Encontre los comprobantes de "+cpla.getNumdoc());
                    int grupo_anterior = cpla.getGrupo_transaccion();
                    double totaldebito=cpla.getTotal_debito();
                    double totalCredito = cpla.getTotal_credito();
                    
                    cpla.setTotal_credito(totaldebito);
                    cpla.setTotal_debito(totalCredito);
                    cpla.setDetalle("ANULA "+cpla.getDetalle());
                    cpla.setTipodoc(tipodoc);
                    cpla.setPeriodo(periodo);
                    cpla.setFechadoc(fechadoc);
                    cpla.setAprobador(usuario);
                    cpla.setMoneda(moneda);
                    cpla.setTipo_operacion("APL");
                    
                    int gtransaccion = model.comprobanteService.getGrupoTransaccion()+1;
                    cpla.setGrupo_transaccion(gtransaccion);
                    model.comprobanteService.setComprobante(cpla);
                    consultas.add(model.comprobanteService.insert());
                    consultas.add(model.comprobanteService.actualizarTablaGen());
                    
                    
                    cpla.setTipodoc(tipos);
                    cpla.setGrupo_transaccion(grupo_anterior);
                    model.comprobanteService.setComprobante(cpla);
                    model.comprobanteService.listarComprobantesDet();
                    
                    
                    //gtransaccion = model.comprobanteService.getGrupoTransaccion();
                    cpla.setGrupo_transaccion(gtransaccion);
                    
                    Vector detalles = model.comprobanteService.getVector();
                    for(int k =0; k<detalles.size();k++){
                        
                        Comprobantes cdet = (Comprobantes) detalles.elementAt(k);
                        
                        
                        //   //System.out.println("Encontre los detalles de "+cdet.getNumdoc());
                        
                        double valordebito=cdet.getTotal_debito();
                        double valorCredito = cdet.getTotal_credito();
                        cdet.setTipodoc("PLA");
                        cdet.setTotal_debito(valorCredito);
                        cdet.setTotal_credito(valordebito);
                        cdet.setPeriodo(periodo);
                        cdet.setFechadoc(fechadoc);
                        cdet.setAprobador(usuario);
                        cdet.setDetalle("ANULA "+cdet.getDetalle());
                        cdet.setGrupo_transaccion(gtransaccion);
                        cdet.setMoneda(moneda);
                        cdet.setTipo_operacion("APL");
                        //  //System.out.println("Se guarda con numero de transaccion "+gtransaccion);
                        // //System.out.println("Se guarda con tipo de documento "+cdet.getTipodoc());
                        ////System.out.println("Se guarda con numero de planilla "+cdet.getNumdoc());
                        model.comprobanteService.setComprobante(cdet);
                        consultas.add(model.comprobanteService.insertItem());
                        
                    }
                    cpla.setDocumento_interno("001");
                    model.comprobanteService.setComprobante(cpla);
                    consultas.add(model.comprobanteService.marcarDocumentoAnulado());
                    
                    modelo.despachoService.insertar(consultas);
                    
                    
                }
                
            }
            logWriter.log("Se encontraron "+canttotal+" registros para contabilizar",LogWriter.INFO);
            
            
            
            
            java.util.Date fin = new java.util.Date();
            String fechaFin= fechaHora.format(fin);
            
            //ENVIAMOS EMAIL DE LA CONTABILIZACION
            String mailInt="";
            String mailExt="";
            boolean swInt=false;
            boolean swExt=false;
            LinkedList lista = modelOperation.tablaGenService.obtenerInfoTablaGen("EMAIL", "contabpla");
            if(lista!=null){
                Iterator it = lista.iterator();
                while(it.hasNext()){
                    TablaGen t = (TablaGen)it.next();
                    //System.out.println("-"+t.getDescripcion());
                    
                    if(t.getReferencia().equals("I")){
                        //verifico si exsite una arroba en la descripcion
                        if( t.getDescripcion().indexOf("@") != -1){
                            mailInt += t.getDescripcion()+";";
                        }
                        //se obtiene el login del usuario y se busca el email
                        else{
                            modelOperation.usuarioService.searchUsuario( t.getDescripcion().toUpperCase() );
                            Usuario u = modelOperation.usuarioService.getUsuario();
                            if(!u.getEmail().equals(""))
                                mailInt += u.getEmail()+";";
                        }
                        swInt = true;
                    }
                    //si el email del cliente es externo
                    else{
                        //verifico si exsite una arroba en la descripcion
                        if( t.getDescripcion().indexOf("@") != -1){
                            mailExt += t.getDescripcion()+";";
                        }
                        else{
                            //se obtiene el login del usuario y se busca el email
                            modelOperation.usuarioService.searchUsuario( t.getDescripcion().toUpperCase() );
                            Usuario u = modelOperation.usuarioService.getUsuario();
                            if(!u.getEmail().equals(""))
                                mailExt += u.getEmail()+";";
                        }
                        swExt = true;
                    }
                }
                
                //System.out.println("interno "+mailInt);
                //System.out.println("externo "+mailExt);
                
                com.tsp.operation.model.beans.SendMail email = new com.tsp.operation.model.beans.SendMail();
                email.setEmailfrom("procesos@sanchezpolo.com");
                email.setSendername( "PROCESO CONTABILIDAD" );
                email.setRecstatus("A");
                email.setEmailcode("");
                email.setEmailcopyto("");
                email.setEmailsubject("CONTABILIZACION ANULACION PLANILLA");
                email.setEmailbody("SE GENERO LA CONTABILIZACION DE LA ANULACION DE UNA PLANILLA EN UN PROCESO AUTOMATICO \n\n" +
                "Redistribuciones Contabilizadas:    "+subidas+
                "\nRedistribuciones NO contabilizadas: " +nosubidas+
                "\n\nFECHA INICIO:    "+ fechaInicio
                +"\nFECHA FINAL      "+ fechaFin+
                "\n\nProceso finalizado con exito" +
                "\n\n\nNOTA: Para mas informacion a cerca de las redistribuciones no contabilizadas consulte" +
                " el log de contabilizacion.");
                if(swInt==true){
                    mailInt = mailInt.substring(0, mailInt.length() - 1 );
                    email.setEmailto(mailInt);
                    email.setTipo("I");
                    modelOperation.sendMailService.sendMail(email);
                }
                if(swExt==true){
                    mailExt = mailExt.substring(0, mailExt.length() - 1 );
                    email.setEmailto(mailExt);
                    email.setTipo("E");
                    modelOperation.sendMailService.sendMail(email);
                }
                
            }
            
            modelOperation.LogProcesosSvc.finallyProceso("Generacion Comprobantes", this.hashCode(), usuario, "PROCESO EXITOSO");
            
        }catch(Exception e){
            
            try{
                modelOperation.LogProcesosSvc.finallyProceso("Generacion Comprobantes", this.hashCode(),usuario,"ERROR :" + e.getMessage());
            }catch ( SQLException ex) {
                //System.out.println("Error guardando el proceso");
            }
            logWriter.log("ERROR: "+e.toString(),LogWriter.ERROR);
            e.printStackTrace();
        }
        finally{
            canttotal = canttotal-( subidas+nosubidas);
            logWriter.log("FIN DEL PROCESO ",LogWriter.INFO);
            logWriter.log("Anulaciones Totales encontradas:    "+cantPlas,LogWriter.INFO);
            logWriter.log("Anulaciones Contabilizadas:    "+subidas,LogWriter.INFO);
            logWriter.log("Anulaciones NO contabilizadas: " +nosubidas,LogWriter.INFO);
            logWriter.log("Anulaciones NO contabilizadas por ERROR DEL PROGRAMA: " +canttotal,LogWriter.INFO);
        }
    }
    
    
    public boolean validarItems(Vector det){
        boolean sw=true;
        for(int j =0; j<det.size();j++){
            Comprobantes detalle = (Comprobantes) det.elementAt(j);
            if(detalle.getAccount_code_c().equals("")){
                sw=false;
            }
            
        }
        return sw;
    }
    public Vector buscarValoresDebito(Vector det, double vlrpla){
        
        //System.out.println("Verificar "+det.size()+" registros");
        Vector vec =new Vector();
        
        for(int j =0; j<det.size();j++){
            
            Comprobantes detalle = (Comprobantes) det.elementAt(j);
            
            if(detalle.getPorcent()!= detalle.getPorcent_contabilizado()){
                
                ////System.out.println("Cuenta "+detalle.getAccount_code_c());
                
                double valorNuevo = (vlrpla * detalle.getPorcent())/100;
                double valorViejo = (vlrpla * detalle.getPorcent_contabilizado())/100;
                
                double diferencia = valorNuevo - valorViejo;
                
                if(diferencia>0){
                    detalle.setTotal_debito(diferencia);
                    detalle.setTotal_credito(0);
                    vec.add(detalle);
                }
                
            }
            
        }
        return vec;
    }
    
    
    public Vector buscarValoresCredito(Vector det, double vlrpla){
        
        //System.out.println("Verificar "+det.size()+" registros para CREDITO");
        Vector vec =new Vector();
        
        for(int j =0; j<det.size();j++){
            
            Comprobantes detalle = (Comprobantes) det.elementAt(j);
            
            if(detalle.getPorcent()!= detalle.getPorcent_contabilizado()){
                
                ////System.out.println("Cuenta "+detalle.getAccount_code_c());
                
                double valorNuevo = (vlrpla * detalle.getPorcent())/100;
                double valorViejo = (vlrpla * detalle.getPorcent_contabilizado())/100;
                
                double diferencia = valorNuevo - valorViejo;
                
                if(diferencia<0){
                    //System.out.println("SE AGREGO EL VALOR "+diferencia);
                    //System.out.println("Con la cuenta "+detalle.getAccount_code_c());
                    detalle.setTotal_credito(diferencia*-1);
                    detalle.setTotal_debito(0);
                    vec.add(detalle);
                }
                
                
            }
            
        }
        return vec;
    }
    public double[] buscarTotales(Vector detDebito,Vector detCredito){
        
        double[] valores = {0,0};
        double valorCredito=0;
        double valorDebito=0;
        
        for(int j =0; j<detDebito.size();j++){
            
            Comprobantes detalle = (Comprobantes) detDebito.elementAt(j);
            valorDebito=valorDebito+detalle.getTotal_debito();
            
        }
        
        for(int j =0; j<detCredito.size();j++){
            
            Comprobantes detalle = (Comprobantes) detCredito.elementAt(j);
            valorCredito=valorCredito+detalle.getTotal_credito();
            
        }
        
        valores[0]=valorCredito;
        valores[1]=valorDebito;
        return valores;
    }
    public static void main(String a [])throws Exception{
        try{
            com.tsp.finanzas.contab.model.services.ComprobantesService c = new com.tsp.finanzas.contab.model.services.ComprobantesService();
            String tipoDoc= c.getTipoDocumento("001");
            HContabilizacionAnulacionPlanilla hilo = new HContabilizacionAnulacionPlanilla();
            hilo.start("ADMIN",tipoDoc,"PES");
        }catch(SQLException e){
            //System.out.println("Error "+e.getMessage());
        }
    }
}
