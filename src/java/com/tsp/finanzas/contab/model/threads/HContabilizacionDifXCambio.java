/*
 * Nombre        HContabilizacion.java
 * Descripci�n   Contabiliza los documentos como planilla.
 * Autor         kreales
 * Fecha         05 de junio
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */

package com.tsp.finanzas.contab.model.threads;

import com.tsp.util.*;
import com.tsp.finanzas.contab.model.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.beans.*;
import com.tsp.finanzas.contab.model.beans.*;
import java.io.*;
import java.sql.*;
import java.util.*;
import java.lang.*;
import javax.servlet.jsp.*;
import javax.servlet.http.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import javax.servlet.http.*;
import com.tsp.util.LogWriter;

public class HContabilizacionDifXCambio extends Thread{
    
    private   String usuario;
    private PrintWriter pw;
    private LogWriter logWriter;
    java.text.SimpleDateFormat anomes;
    java.text.SimpleDateFormat fecha;
    java.text.SimpleDateFormat fechaHora;
    String periodo;
    String fechadoc;
    String fechaInicio;
    java.util.Date pdate;
    java.util.Date ahora;
    String tipodoc;
    String moneda;
    public HContabilizacionDifXCambio() {
    }
    
    /**
     * Establece las variables iniciales y activa el inicio del hilo
     * @param user El usuario en sesi�n
     * @throws Exception Si algun error ocurre en el acceso a la BD
     */
    public void start( String user, String tipo, String moneda ) throws Exception{
        /*
         *Busco la fecha actual y le resto un dia.
         */
        Calendar hoy = Calendar.getInstance();
        hoy.add(hoy.DATE, -1);
        
        pdate = hoy.getTime();
        ahora = new java.util.Date();
        
        anomes = new java.text.SimpleDateFormat("yyyyMM");
        fecha = new java.text.SimpleDateFormat("yyyy-MM-dd");
        fechaHora = new java.text.SimpleDateFormat("yyyy-MM-dd hh:mm");
        
        periodo = anomes.format(pdate);
        fechadoc = fecha.format(pdate);
        fechaInicio= fechaHora.format(ahora);
        
        
        ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
        String ruta = rb.getString("ruta")+ "/exportar/migracion/"+user;
        
        File file = new File(ruta);
        file.mkdirs();
        
        pw = new PrintWriter(System.err, true);
        
        String logFile = ruta + "/HContabilizacionDifXCambio"+fechadoc+".log";
        
        pw = new PrintWriter(new FileWriter(logFile, true), true);
        logWriter = new LogWriter("HContabilizacionDifXCambio", LogWriter.INFO, pw);
        usuario=user;
        logWriter.setPrintWriter(pw);
        
        tipodoc=tipo;
        this.moneda = moneda;
        super.start();
    }
    
    
    /**
     * Metodo llamado por Java para inciciar la ejecuci�n del hilo
     */
    public void run(){
        com.tsp.finanzas.contab.model.Model model = new com.tsp.finanzas.contab.model.Model();
        com.tsp.operation.model.Model modelOperation = new com.tsp.operation.model.Model();
        
        try{
            
            Vector consultas = new Vector();
            
            logWriter.log("**********************************************************",LogWriter.INFO);
            logWriter.log("*  NUEVO PROCESO DE CONTABILIZACION DE DIF X CAMBIO      *",LogWriter.INFO);
            logWriter.log("*  Periodo  :  "+periodo+"                               *",LogWriter.INFO);
            logWriter.log("*  FechaDoc :  "+fechadoc+"                              *",LogWriter.INFO);
            logWriter.log("**********************************************************",LogWriter.INFO);
            
            
            modelOperation.LogProcesosSvc.InsertProceso("Generacion Comprobantes", this.hashCode(), "Generacion Comprobantes Diferecia X Cambio o Valor", usuario);
            /*
             * Primero realizamos ajuste por cambio
             */
            model.comprobanteService.buscarAjustesXCambio(tipodoc);
            Vector plas = model.comprobanteService.getVector();
            
            logWriter.log("Se encontraron "+plas.size()+" registros para contabilizar ajuste por cambio",LogWriter.INFO);
            
            int nosubidas=0;
            int subidas=0;
            String cuenta ="";
            Vector marcar = new Vector();
            for(int i =0; i<plas.size();i++){
                
                Comprobantes c = (Comprobantes) plas.elementAt(i);
                cuenta = c.getCuenta();
                
                if(c.isTieneProv() && !c.getMoneda().equals("") && !c.getCmc().equals("")  && !c.getCuenta().equals("")){
                    
                    double valorDif = c.getTotal_credito();
                    
                    c.setPeriodo(periodo);
                    c.setFechadoc(fechadoc);
                    c.setAprobador(usuario);
                    c.setDstrct("FINV");
                    c.setTipodoc("AJC");
                    c.setTotal_items(2);
                    c.setUsuario(usuario);
                    c.setDocumento_interno("010");
                    c.setMoneda(moneda);
                    int gtransaccion = model.comprobanteService.getGrupoTransaccion();
                    c.setGrupo_transaccion(gtransaccion);
                    
                    model.comprobanteService.setComprobante(c);
                    model.comprobanteService.insert();
                    
                    
                    double valor =c.getTotal_credito();
                    
                    
                    //System.out.println("OP "+c.getNumdoc());
                    //System.out.println("Valor a contabilizar "+valor);
                    
                    if(c.getTipo_valor().equals("D")){
                        //PRIMERO GRABO EL VALOR DEL DEBITO
                        c.setTotal_debito(valor);
                        c.setTotal_credito(0);
                        c.setCuenta(cuenta);
                        model.comprobanteService.setComprobante(c);
                        consultas.add(model.comprobanteService.insertItem());
                        
                        
                        //SEGUNDO GRABO EL VALOR DEL CREDITO
                        c.setTotal_debito(0);
                        c.setTotal_credito(valor);
                        c.setCuenta(c.getCmc());
                        model.comprobanteService.setComprobante(c);
                        consultas.add(model.comprobanteService.insertItem());
                    }
                    else if (c.getTipo_valor().equals("C")){
                        //PRIMERO GRABO EL VALOR DEL CREDITO
                        c.setTotal_debito(0);
                        c.setTotal_credito(valor);
                        c.setCuenta(cuenta);
                        model.comprobanteService.setComprobante(c);
                        consultas.add(model.comprobanteService.insertItem());
                        
                        //SEGUNDO GRABO EL VALOR DEL DEBITO
                        c.setTotal_debito(valor);
                        c.setTotal_credito(0);
                        c.setCuenta(c.getCmc());
                        model.comprobanteService.setComprobante(c);
                        consultas.add(model.comprobanteService.insertItem());
                    }
                    marcar.add(c);
                    //model.comprobanteService.marcarDifXCambio();
                    
                }
                else{
                    if(c.isTieneProv())
                        logWriter.log(c.getNumdoc()+"  "+"No se encontro proveedor",LogWriter.ERROR);
                    if (c.getTotal_credito()<=0)
                        logWriter.log(c.getNumdoc()+"  "+"La diferencia tiene valor en 0 ",LogWriter.ERROR);
                    if(c.getMoneda().equals(""))
                        logWriter.log(c.getNumdoc()+"  "+"La Factura no tiene definida una moneda",LogWriter.ERROR);
                    if(c.getCmc().equals(""))
                        logWriter.log(c.getNumdoc()+"  "+"El proveedor no tiene numero de cuenta",LogWriter.ERROR);
                    if(c.getCuenta().equals(""))
                        logWriter.log(c.getNumdoc()+"  "+"No se encontro cuenta del concepto ",LogWriter.ERROR);
                    nosubidas++;
                }
            }
            
             /*
              * Despues realizamos ajuste por valor
              */
            model.comprobanteService.buscarAjustesXValor(tipodoc);
            plas = model.comprobanteService.getVector();
            
            logWriter.log("Se encontraron "+plas.size()+" registros para contabilizar ajuste por valor",LogWriter.INFO);
            
            cuenta ="";
            
            for(int i =0; i<plas.size();i++){
                
                Comprobantes c = (Comprobantes) plas.elementAt(i);
                cuenta = c.getCuenta();
                boolean tieneTasa = true;
                double valorDif = c.getTotal_credito();
                c.setUsuario(usuario);
                c.setDocumento_interno("010");
                
                if(!c.getMoneda().equals(moneda)){
                    modelOperation.tasaService.buscarValorTasa(moneda,c.getMoneda(),moneda,c.getFechadoc());
                    Tasa tasa = modelOperation.tasaService.obtenerTasa();
                    if(tasa!=null){
                        tieneTasa = true;
                        double vtasa = tasa.getValor_tasa();
                        double vConvertido = vtasa*valorDif;
                        //System.out.println("Valor tasa "+vtasa);
                        //System.out.println("Valor al cambio "+vConvertido);
                        
                        c.setTotal_credito(vConvertido);
                        c.setTotal_debito(vConvertido);
                        c.setMoneda("PES");
                    }
                    else{
                        tieneTasa = false;
                        //System.out.println("No se encontro tasa");
                        c.setTotal_credito(0);
                        c.setTotal_debito(0);
                    }
                }
                
                if(c.isTieneProv() && c.getTotal_credito()!=0 && !c.getMoneda().equals("") && !c.getCmc().equals("")  && !c.getCuenta().equals("")&& tieneTasa){
                    
                    c.setMoneda(moneda);
                    c.setPeriodo(periodo);
                    c.setFechadoc(fechadoc);
                    c.setAprobador(usuario);
                    c.setDstrct("FINV");
                    c.setTipodoc("AJV");
                    c.setTotal_items(2);
                    int gtransaccion = model.comprobanteService.getGrupoTransaccion();
                    c.setGrupo_transaccion(gtransaccion);
                    
                    model.comprobanteService.setComprobante(c);
                    model.comprobanteService.insert();
                    
                    
                    
                    c.setDocumento_interno("010");
                    
                    
                    double valor =c.getTotal_credito();
                    
                    
                    //System.out.println("OP "+c.getNumdoc());
                    //System.out.println("Valor a contabilizar "+valor);
                    
                    if(c.getTipo_valor().equals("D")){
                        //PRIMERO GRABO EL VALOR DEL DEBITO
                        c.setTotal_debito(valor);
                        c.setTotal_credito(0);
                        c.setCuenta(cuenta);
                        model.comprobanteService.setComprobante(c);
                        consultas.add(model.comprobanteService.insertItem());
                        
                        
                        //SEGUNDO GRABO EL VALOR DEL CREDITO
                        c.setTotal_debito(0);
                        c.setTotal_credito(valor);
                        c.setCuenta(c.getCmc());
                        model.comprobanteService.setComprobante(c);
                        consultas.add(model.comprobanteService.insertItem());
                    }
                    else if (c.getTipo_valor().equals("C")){
                        //PRIMERO GRABO EL VALOR DEL CREDITO
                        c.setTotal_debito(0);
                        c.setTotal_credito(valor);
                        c.setCuenta(cuenta);
                        model.comprobanteService.setComprobante(c);
                        consultas.add(model.comprobanteService.insertItem());
                        
                        //SEGUNDO GRABO EL VALOR DEL DEBITO
                        c.setTotal_debito(valor);
                        c.setTotal_credito(0);
                        c.setCuenta(c.getCmc());
                        model.comprobanteService.setComprobante(c);
                        consultas.add(model.comprobanteService.insertItem());
                    }
                    // model.comprobanteService.marcarDifXCambio();
                    marcar.add(c);
                    
                }
                else{
                    if(c.isTieneProv())
                        logWriter.log(c.getNumdoc()+"  "+"No se encontro proveedor",LogWriter.ERROR);
                    if (c.getTotal_credito()<=0)
                        logWriter.log(c.getNumdoc()+"  "+"La diferencia tiene valor en 0 ",LogWriter.ERROR);
                    if(c.getMoneda().equals(""))
                        logWriter.log(c.getNumdoc()+"  "+"La Factura no tiene definida una moneda",LogWriter.ERROR);
                    if(c.getCmc().equals(""))
                        logWriter.log(c.getNumdoc()+"  "+"El proveedor no tiene numero de cuenta",LogWriter.ERROR);
                    if(c.getCuenta().equals(""))
                        logWriter.log(c.getNumdoc()+"  "+"No se encontro cuenta del concepto ",LogWriter.ERROR);
                    if(!tieneTasa)
                        logWriter.log(c.getNumdoc()+"  "+"No se encontro valor de la tasa para la moneda  "+c.getMoneda(),LogWriter.ERROR);
                    nosubidas++;
                }
            }
            
            for(int i =0; i<marcar.size();i++){
                Comprobantes c = (Comprobantes) marcar.elementAt(i);
                model.comprobanteService.setComprobante(c);
                if(c.getTipodoc().equals("AJC")){
                    
                    consultas.add(model.comprobanteService.marcarDifXCambio());
                }
                else if(c.getTipodoc().equals("AJV")){
                    consultas.add(model.comprobanteService.marcarDifXValor());
                }
                
            }
            
            com.tsp.operation.model.Model modelo =  new com.tsp.operation.model.Model();
            modelo.despachoService.insertar(consultas);
            
            
            java.util.Date fin = new java.util.Date();
            String fechaFin= fechaHora.format(fin);
            
            //ENVIAMOS EMAIL DE LA CONTABILIZACION
            String mailInt="";
            String mailExt="";
            boolean swInt=false;
            boolean swExt=false;
            LinkedList lista = modelOperation.tablaGenService.obtenerInfoTablaGen("EMAIL", "contabpla");
            if(lista!=null){
                Iterator it = lista.iterator();
                while(it.hasNext()){
                    TablaGen t = (TablaGen)it.next();
                    //System.out.println("-"+t.getDescripcion());
                    
                    if(t.getReferencia().equals("I")){
                        //verifico si exsite una arroba en la descripcion
                        if( t.getDescripcion().indexOf("@") != -1){
                            mailInt += t.getDescripcion()+";";
                        }
                        //se obtiene el login del usuario y se busca el email
                        else{
                            modelOperation.usuarioService.searchUsuario( t.getDescripcion().toUpperCase() );
                            Usuario u = modelOperation.usuarioService.getUsuario();
                            if(!u.getEmail().equals(""))
                                mailInt += u.getEmail()+";";
                        }
                        swInt = true;
                    }
                    //si el email del cliente es externo
                    else{
                        //verifico si exsite una arroba en la descripcion
                        if( t.getDescripcion().indexOf("@") != -1){
                            mailExt += t.getDescripcion()+";";
                        }
                        else{
                            //se obtiene el login del usuario y se busca el email
                            modelOperation.usuarioService.searchUsuario( t.getDescripcion().toUpperCase() );
                            Usuario u = modelOperation.usuarioService.getUsuario();
                            if(!u.getEmail().equals(""))
                                mailExt += u.getEmail()+";";
                        }
                        swExt = true;
                    }
                }
                
                //System.out.println("interno "+mailInt);
                //System.out.println("externo "+mailExt);
                
                com.tsp.operation.model.beans.SendMail email = new com.tsp.operation.model.beans.SendMail();
                email.setEmailfrom("procesos@sanchezpolo.com");
                email.setSendername( "PROCESO CONTABILIDAD" );
                email.setRecstatus("A");
                email.setEmailcode("");
                email.setEmailcopyto("");
                email.setEmailsubject("CONTABILIZACION AJUSTE DE DIFERENCIA POR CAMBIO Y DIFERENCIA POR VALOR");
                email.setEmailbody("SE GENERO LA CONTABILIZACION DE LOS AJUSTES DE DIFERENCIA POR CAMBIO Y " +
                "DIFERENCIA POR VALOR EN UN PROCESO AUTOMATICO \n\n" +
                "Facturas Contabilizadas:    "+subidas+
                "\nFacturas NO contabilizadas: " +nosubidas+
                "\n\nFECHA INICIO:    "+ fechaInicio
                +"\nFECHA FINAL      "+ fechaFin+
                "\n\nProceso finalizado con exito" +
                "\n\n\nNOTA: Para mas informacion a cerca de las Facturas no contabilizadas consulte" +
                " el log de contabilizacion.");
                if(swInt==true){
                    mailInt = mailInt.substring(0, mailInt.length() - 1 );
                    email.setEmailto(mailInt);
                    email.setTipo("I");
                    modelOperation.sendMailService.sendMail(email);
                }
                if(swExt==true){
                    mailExt = mailExt.substring(0, mailExt.length() - 1 );
                    email.setEmailto(mailExt);
                    email.setTipo("E");
                    modelOperation.sendMailService.sendMail(email);
                }
                
            }
            logWriter.log("FIN DEL PROCESO ",LogWriter.INFO);
            logWriter.log("Facturas Contabilizadas:    "+subidas,LogWriter.INFO);
            logWriter.log("Facturas NO contabilizadas: " +nosubidas,LogWriter.INFO);
            
            modelOperation.LogProcesosSvc.finallyProceso("Generacion Comprobantes", this.hashCode(), usuario, "PROCESO EXITOSO");
            
        }catch(Exception e){
            
            try{
                modelOperation.LogProcesosSvc.finallyProceso("Generacion Comprobantes", this.hashCode(),usuario,"ERROR :" + e.getMessage());
            }catch ( SQLException ex) {
                //System.out.println("Error guardando el proceso");
            }
            e.printStackTrace();
        }
    }
    
    public static void main(String a [])throws Exception{
        try{
            com.tsp.finanzas.contab.model.services.ComprobantesService c = new com.tsp.finanzas.contab.model.services.ComprobantesService();
            String tipoDoc= c.getTipoDocumento("001");
            HContabilizacionDifXCambio hilo = new HContabilizacionDifXCambio();
            hilo.start("ADMIN",tipoDoc,"PES");
        }catch(SQLException e){
            //System.out.println("Error "+e.getMessage());
        }
    }
    
    
}
