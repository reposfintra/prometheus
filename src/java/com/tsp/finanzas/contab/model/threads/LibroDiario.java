/*************************************************************
 * Nombre: LibroDiarioPDF.java
 * Descripci�n: Hilo para crear el reporte de Libro Diario y Resumen
 * Autor: Osvaldo P�rez
 * Fecha: 28 de Junio de 2006
 * Versi�n: Java 1.0
 * Copyright: Fintravalores S.A. S.A.
 **************************************************************/

/*
 * LibroDiarioPDF.java
 *
 * Created on 28 de junio de 2006, 09:28 AM
 */

package com.tsp.finanzas.contab.model.threads;

/**
 *
 * @author  Osvaldo
 */

import com.tsp.pdf.*;
import java.util.*;
import java.io.*;
import java.lang.*;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import com.tsp.operation.model.beans.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.util.*;
import com.tsp.operation.model.*;

public class LibroDiario extends Thread{
    
    
    private String ruta;
    private String usuario;
    private String procesoName;
    private String des;
    private String periodo;
    private String distrito;
    private Date d;
    private Model model = new Model();
    
    /** Creates a new instance of LibroDiarioPDF */
    public LibroDiario() {
    }
    
    public void start( String u, String periodo, String distrito) {
        
        this.procesoName = "Libro Diario";
        this.des = "Reporte de Libro Diario";
        this.usuario = u;
        this.distrito = distrito;
        this.periodo = periodo;
        this.d = new Date();
        super.start();
    }
    
    
    public synchronized void run(){
        LibroDiarioPDF lib;
        try{
            String fecha = "";
            Util u = new Util();
            String comentario="EXITOSO";
            model.LogProcesosSvc.InsertProceso( this.procesoName, this.hashCode()," Libro Diario", this.usuario );
            
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            ruta = rb.getString("ruta");
            
            File file = new File(ruta + "/exportar/migracion/" + usuario + "/");
            file.mkdirs();
            
            fecha = Util.getFechaActual_String(6);
            fecha = fecha.replaceAll("/", "-");
            fecha = fecha.replaceAll(":", "_");
            
            File pdf = new File(ruta + "/exportar/migracion/" + usuario + "/LibroDiario "+fecha+".pdf");
            File xslt = new File(ruta + "/Templates/libroDiario.xsl");
            
            
            lib = new LibroDiarioPDF(xslt, pdf);
            
            fecha = Utility.getDate(6);
            fecha = fecha.replaceAll("/| |:", "");
            String tabla = "libro_diario_"+fecha;
            
            lib.generarLibroDiario(distrito, periodo, tabla);
            
            model.LogProcesosSvc.finallyProceso(this.procesoName,this.hashCode(),  this.usuario ,comentario);
        }catch(Exception e){
            //////System.out.println(e.getMessage());
            try{
                //////System.out.println("Salida Error...");
                model.LogProcesosSvc.finallyProceso(this.procesoName,this.hashCode(),  this.usuario ,"ERROR Hilo: " + e.getMessage());
            }catch(Exception ex){}
            
        }finally{
            super.destroy();
        }
    }
}
