  /***************************************
    * Nombre Clase ............. HContabilizacionCL.java
    * Descripción  .. . . . . .  Permite contabilizar las facturas de clientes
    * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
    * Fecha . . . . . . . . . .  10/07/2006
    * versión . . . . . . . . .  1.0
    * Copyright ...Transportes Sanchez Polo S.A.
    *******************************************/





package com.tsp.finanzas.contab.model.threads;


import com.tsp.finanzas.contab.model.*;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.LogProcesosService;
import com.tsp.util.*;



public class HContabilizacionCL  extends Thread{
    
   
    private Model     model;
    private String    procesoName;
    private Usuario   user;
   
    
    
    public HContabilizacionCL() {
    }
    
    
    
     public void start(Model modelo, Usuario usuario) throws Exception{
         try{
                this.model       = modelo;
                this.user        = usuario;
                this.procesoName = "Contabilización Facturas CxP (CL)";
                super.start();
        }catch(Exception e){
           throw new Exception( e.getMessage() );
       }
    }
     
     
     
     
    
    
     public synchronized void run(){
       LogProcesosService log = new LogProcesosService(this.user.getBd());
       try{
           
           String hoy  =  Utility.getHoy("-");
           String ayer =  Utility.convertirFecha( hoy, -1);
           
           String comentario="EXITOSO";
           log.InsertProceso(this.procesoName, this.hashCode(), "Corte "+ ayer ,this.user.getLogin() ); 
           
           
           
           comentario = model.ContabilizacionFacturasCLSvc.contabilizar( user.getDstrct(), ayer, user, user.getId_agencia());//fdiaz 2011-03-14 - GEOTECH - Contabilizacion IVA en CxP
           
          //List  listaContable =  model.ContabilizacionFacturasCLSvc.getListaContable();
          //model.ContabilizacionFacSvc.insertar(listaContable, user.getLogin()   );           
          //comentario    +=  model.ContabilizacionFacSvc.getESTADO(); 
           
           
           
           model.ContabilizacionFacturasCLSvc.setProcess( false );           
           log.finallyProceso(this.procesoName, this.hashCode(), this.user.getLogin(),comentario);
             
       }catch(Exception e){
           try{       
               model.ContabilizacionFacSvc.setProcess( false );
               log.finallyProceso(this.procesoName,this.hashCode(), this.user.getLogin(),"ERROR Hilo: " + e.getMessage()); }
           catch(Exception f){}   
       }
    }
    
     
     
    
}
