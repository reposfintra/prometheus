/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.finanzas.contab.model.threads;

import com.tsp.finanzas.contab.model.services.MayorizacionService;
import com.tsp.operation.model.LogProcesosService;
import com.tsp.operation.model.beans.Usuario;
import java.io.*;
import java.util.*;
import java.sql.SQLException;
import java.text.SimpleDateFormat;




/**
 * HActualizarMayorizacion.java<br />
 * Hilo para iniciarl la actualizacion de los mayores contables<br />
 * 17/02/2010
 * @author  Mgarizao - GEOTECH
 * @version 1.0
 */
public class HActualizarMayorizacion extends Thread{

    Usuario usuario;
    MayorizacionService mayorizacionService;
    LogProcesosService  logSvc;
    private ResourceBundle rb;
    private String ruta;
    String proceso;
    String ano;
    String mes;
    File file;
    FileWriter       fw  = null;
    PrintWriter      pw  = null;
    SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss");
    SimpleDateFormat fmt2= new SimpleDateFormat("yyyyMMdd_kkmmss");
    Date             fechaActual = new Date();


     public void start(LogProcesosService logSvc,MayorizacionService mayor,Usuario User,String ano,String mes){
       this.mayorizacionService = mayor;
       this.usuario = User;
       this.logSvc = logSvc;
       this.ano = ano;
       this.mes = mes;
       super.start();
    }

     public synchronized  void run(){


            try{


            rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            this.ruta = rb.getString("ruta") + "/exportar/migracion/" + usuario.getLogin();
            this.file = new File(ruta);
            this.file.mkdirs();
            this.proceso = "Actualizar Mayorizacion";
            fw = new FileWriter(ruta + "/Logresultado" +ano + mes + "_"+usuario.getEmpresa()+".txt");
            pw = new PrintWriter(new BufferedWriter(fw));


            try {


                writeLog("*************************************************");
                writeLog("* Inicio del Proceso de Mayorizacion *");
                writeLog("*************************************************");
                Procesar();
                writeLog("*************************************************");
                writeLog("*               Proceso Finalizado              *");
                writeLog("*************************************************");
            } catch (Exception ex) {
                ex.printStackTrace();
                writeLog("*************************************************");
                writeLog("*    Finalizando Proceso pero con anomalias     *");
                writeLog("*************************************************");
                writeLog(ex.getMessage());
            }


              pw.close();
            }catch (Exception ex){
                
                ex.printStackTrace();
               

            }

    }



     /**
      * metodo para procesar la mayorizacion
      * @author MGarizao - GEOTECH
      * @date 17/02/2010
      * @version 1.0
      * @throws Exception
      */
     public void Procesar() throws Exception{
        try {
            logSvc.InsertProceso(this.proceso, this.hashCode(), "Proceso de Mayorizacion", usuario.getLogin());
            String msn = mayorizacionService.obtenerProcesoMayorizacion(usuario, mes, ano);
            
             writeLog2("");
             writeLog2("");
             writeLog2("");
             writeLog2("RESUMEN : ");
             writeLog2("Proceso de MAYORIZACION             : " + msn   );
             writeLog2("");
             logSvc.finallyProceso(this.proceso, this.hashCode(), usuario.getLogin(), msn);

              if(msn.equals("Transaccion exitosa")){

                 writeLog2("");
                 writeLog2("");
                 writeLog2("");
                 writeLog2("INICIANDO LA EXPORTACION A EXCEL : ");
                 writeLog2("");

                  try{
                     Vector excel = mayorizacionService.obtenerProcesoMayorizadoExcel(mes, ano);
                     mayorizacionService.generarReporteMayorizacion(excel, mes, ano, ruta);
                     writeLog2("Estado             :  exportado con exito" );
                     writeLog2("");
                  }catch(Exception e){

                     writeLog2("Estado             :  exportacion no exitosa "+ e.getMessage() );
                     writeLog2("");
                  }
              }


        }catch (SQLException ex) {
            
            ex.printStackTrace();
            writeLog2   ( ex.getMessage() );
            logSvc.finallyProceso(this.proceso, this.hashCode(), this.usuario.getLogin(), "Finalizado con error ...");
            throw new Exception(ex.getMessage());
        }

     }


     /**
      * metdodo para escribir en el log .txt
      * @param msg mensaje del proceso
      * @author MGarizao - GEOTECH
      * @date 17/02/2010
      * @version 1.0
      * @throws Exception
      */
      public void writeLog(String msg)throws Exception {
        try{
            if (pw!=null ){
                String []msgs = msg.split("\\n");
                for (int i = 0; i< msgs.length;i++)
                    pw.println("[" + getCurrentTime() + "] " +  msgs[i] );
                pw.flush();
            }
        } catch (Exception e){
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }

      /**
       * metodo para escribir los parrafos internedios en el archivo .txt
       * @param msg mensaje del proceso
       * @authoR MGarizao - GEOTECH
       * @date 17/02/2010
       * @version 1.0
       * @throws Exception
       */
     public void writeLog2(String msg)throws Exception {
        try{
            if (pw!=null ){

                pw.println( msg);
                pw.flush();
            }
        } catch (Exception e){
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }

     /**
      * metodo para obtener la fecha y hora actual
      * @author MGarizao - GEOTECH
      * @date 17/02/2010
      * @version 1.0
      * @return
      */
     public String getCurrentTime(){
        fechaActual.setTime( System.currentTimeMillis());
        return fmt.format(fechaActual );
    }

}
