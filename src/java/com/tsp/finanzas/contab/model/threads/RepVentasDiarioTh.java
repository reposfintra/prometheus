/********************************************************************
 *      Nombre Clase.................   ReporteFactCliTh.java
 *      Descripci�n..................   Hilo para la escritura del xls
 *      Autor........................   Tito Andr�s Maturana
 *      Fecha........................   03.02.2006
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.finanzas.contab.model.threads;

import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import java.io.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.operation.model.*;
import com.tsp.finanzas.contab.model.*;
import com.tsp.finanzas.contab.model.beans.*;
import java.util.*;
import java.text.*;
//import com.tsp.operation.model.threads.POIWrite;

//Logger
import org.apache.log4j.*;



public class RepVentasDiarioTh extends Thread{
    
    Logger logger = Logger.getLogger (this.getClass());
    
    private Vector reporte;
    private Usuario user;    
    private String fechai;
    private String fechaf;
    
    private com.tsp.operation.model.Model model;
    private com.tsp.finanzas.contab.model.Model modelop;
    
    //Log Procesos
    private String procesoName;
    private String des;
    
    public void start(com.tsp.operation.model.Model op, com.tsp.finanzas.contab.model.Model modelo, Usuario user, String fechai, String fechaf){
        this.reporte = reporte;
        this.user = user;
        this.fechaf = fechaf;
        this.fechai = fechai;
        
        this.modelop   = modelo;
        this.model = op;
        this.procesoName = "Reporte Ventas Diario";
        this.des = "Reporte Ventas Diario:  " + fechai + " - " + fechaf;
                
        super.start();
    }
    
    public synchronized void run(){
        try{
            //INSERTO EN EL LOG DE PROCESO
            model.LogProcesosSvc.InsertProceso(this.procesoName, this.hashCode(), this.des, user.getLogin());
            
                
            modelop.reportes.reporteVentasDiario(user.getDstrct(), fechai, fechaf);
            this.reporte = modelop.reportes.getVector();
            
            //logger.info("RESULTADOS ENCONTRADOS: " + this.reporte.size());
        
            //Fecha del sistema
            Calendar FechaHoy = Calendar.getInstance();
            Date d = FechaHoy.getTime();
            SimpleDateFormat s = new SimpleDateFormat("yyyyMMdd_kkmm");
            SimpleDateFormat s1 = new SimpleDateFormat("yyyy-MM-dd-hh:mm");
            SimpleDateFormat fec = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss");
            SimpleDateFormat time = new SimpleDateFormat("hh:mm");
            String FechaFormated = s.format(d);
            String FechaFormated1 = s1.format(d);
            String hora = time.format(d);
            String Fecha = fec.format(d);
            
            
            //obtener cabecera de ruta
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String path = rb.getString("ruta");
            //armas la ruta
            String Ruta1  = path + "/exportar/migracion/" + user.getLogin() + "/";
            //crear la ruta
            File file = new File(Ruta1);
            file.mkdirs();
            
            POIWrite xls = new POIWrite(Ruta1 +"ReporteVentasDiario_" + FechaFormated + ".xls");
            String nom_archivo = "ReporteVentasDiario_" + FechaFormated + ".xls";            
            
            //Definici�n de Estilos
            HSSFCellStyle fecha  = xls.nuevoEstilo("Arial", 10, false , false, "yyyy/mm/dd hh:mm"  , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
            HSSFCellStyle texto  = xls.nuevoEstilo("Arial", 10, false , false, "text"        , xls.NONE , xls.NONE , xls.NONE);
            HSSFCellStyle texto_ttl  = xls.nuevoEstilo("Arial", 10, false , false, "text"        , xls.NONE , xls.NONE , xls.NONE);
            HSSFCellStyle numero = xls.nuevoEstilo("Arial", 10, false , false, ""            , xls.NONE , xls.NONE , xls.NONE);
            
            HSSFCellStyle header1      = xls.nuevoEstilo("Arial", 11, true , false, "text"        , HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle header2      = xls.nuevoEstilo("Arial", 18, true , false, "text"        , HSSFColor.WHITE.index , HSSFColor.ORANGE.index , HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle header3      = xls.nuevoEstilo("Arial", 12, true , false, "text"        , HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
            HSSFCellStyle texto2      = xls.nuevoEstilo("Arial", 12, false , false, "text"        , HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
            HSSFCellStyle titulo      = xls.nuevoEstilo("Arial", 12, true  , false, "text"        , HSSFColor.BLACK.index , HSSFColor.PINK.index , HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle fechatitle  = xls.nuevoEstilo("Arial", 12, true  , false, "yyyy/mm/dd"  , HSSFColor.RED.index , xls.NONE, HSSFCellStyle.ALIGN_CENTER );
            HSSFCellStyle entero      = xls.nuevoEstilo("Arial", 12, false , false, "text"        , HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle flotante      = xls.nuevoEstilo("Arial", 12, false , false, "text"        , HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_RIGHT);
            HSSFCellStyle moneda  = xls.nuevoEstilo("Arial", 10, false , false, "#,##0.00", xls.NONE , xls.NONE , xls.NONE);
            //\"$ \"
            HSSFCellStyle moneda_ttl  = xls.nuevoEstilo("Arial", 10, false , false, "#,##0.00", xls.NONE , xls.NONE , xls.NONE);
            
            texto_ttl.setBorderTop(texto_ttl.BORDER_THIN);
            moneda_ttl.setBorderTop(texto_ttl.BORDER_THIN);
            
            com.tsp.finanzas.contab.model.beans.RepGral nuevo = new com.tsp.finanzas.contab.model.beans.RepGral();
                
            int hoja = 1;
            int fila = 4;
            int col  = 0;
            int nceldas = 5;
            
            xls.obtenerHoja("HOJA " + hoja);
            xls.cambiarMagnificacion(3,4);// cabecera
            xls.cambiarAnchoColumna(0, 4000);
            xls.cambiarAnchoColumna(1, 4000);
            xls.cambiarAnchoColumna(2, 4000);
            xls.cambiarAnchoColumna(3, 3000);
            xls.cambiarAnchoColumna(4, 5000);
            xls.cambiarAnchoColumna(5, 3000);

            xls.combinarCeldas(0, 0, 0, nceldas);
            xls.combinarCeldas(1, 0, 1, nceldas);
            xls.combinarCeldas(2, 0, 1, nceldas);
            xls.adicionarCelda(0, 0, "REPORTE DE VENTAS DIARIO", header1);
            xls.adicionarCelda(1, 0, "Nit : 890.103.161-1", header1);
            xls.adicionarCelda(2, 0, "INFORME SEMANAL DE VENTAS", header1);
            
            xls.adicionarCelda(fila, col++, "FACTURA", header1);
            xls.adicionarCelda(fila, col++, "FECHA DE", header1);
            xls.adicionarCelda(fila, col++, "CODIGO DE", header1);
            xls.adicionarCelda(fila, col++, "NIT", header1);
            xls.adicionarCelda(fila, col++, "VALOR", header1);
            xls.adicionarCelda(fila, col++, "IVA", header1);
            fila++;
            col = 0;
            xls.adicionarCelda(fila, col++, "", header1);
            xls.adicionarCelda(fila, col++, "FACTURA", header1);
            xls.adicionarCelda(fila, col++, "CLIENTE", header1);
            xls.adicionarCelda(fila, col++, "", header1);
            xls.adicionarCelda(fila, col++, "", header1);
            xls.adicionarCelda(fila, col++, "", header1);
            fila++;
            col = 0;
            xls.adicionarCelda(fila, col++, "", header1);
            xls.adicionarCelda(fila, col++, "", header1);
            xls.adicionarCelda(fila, col++, "", header1);
            xls.adicionarCelda(fila, col++, "", header1);
            xls.adicionarCelda(fila, col++, "", header1);
            xls.adicionarCelda(fila, col++, "", header1);
            
            if( this.reporte!=null && this.reporte.size()>0 ){
                
                com.tsp.finanzas.contab.model.beans.RepGral obj = (com.tsp.finanzas.contab.model.beans.RepGral) this.reporte.elementAt(0);
                String fra_fec = obj.getFecha();
                
                String anterior = obj.getFactura();
                long prox = Long.parseLong(anterior.substring(1, anterior.length())) + 1;
                //logger.info("PROXIMA INI(" + anterior + "): " + prox);
                
                int sw = 1;
                double total = 0;
                
                for( int i=0; i<this.reporte.size(); i++){
                    com.tsp.finanzas.contab.model.beans.RepGral obj0 = (com.tsp.finanzas.contab.model.beans.RepGral) this.reporte.elementAt(i);
                   
                    String actual = obj0.getFactura();
                    //logger.info("> FACTURA: " + actual);
                    
                    if( fra_fec.compareTo(obj0.getFecha())!=0 ) {                        
                        //logger.info("FECHA " + fec + " : " + com.tsp.util.Util.customFormat(total));
                        /* IMPRIMIMOS EL TOTAL */                    
                        col=0;
                        fila++;
                        xls.adicionarCelda(fila, col++, "", texto_ttl);
                        xls.adicionarCelda(fila, col++, "", texto_ttl);
                        xls.adicionarCelda(fila, col++, "", texto_ttl);
                        xls.adicionarCelda(fila, col++, "", texto_ttl);
                        xls.adicionarCelda(fila, col++, total, moneda_ttl);
                        xls.adicionarCelda(fila, col++, 0, moneda_ttl);
                        
                        total = 0;
                        fra_fec = obj0.getFecha();
                        sw = 1;
                        
                        /* GENERAMOS UNA NUEVA HOJA */
                        fila = 4;
                        col  = 0;
                        hoja++;
                        xls.obtenerHoja("HOJA " + hoja);
                        xls.cambiarMagnificacion(3,4);// cabecera
                        xls.cambiarAnchoColumna(0, 4000);
                        xls.cambiarAnchoColumna(1, 4000);
                        xls.cambiarAnchoColumna(2, 4000);
                        xls.cambiarAnchoColumna(3, 3000);
                        xls.cambiarAnchoColumna(4, 5000);
                        xls.cambiarAnchoColumna(5, 3000);
                        
                        xls.combinarCeldas(0, 0, 0, nceldas);
                        xls.combinarCeldas(1, 0, 1, nceldas);
                        xls.combinarCeldas(2, 0, 1, nceldas);
                        xls.adicionarCelda(0, 0, "REPORTE DE VENTAS DIARIO", header1);
                        xls.adicionarCelda(1, 0, "Nit : 890.103.161-1", header1);
                        xls.adicionarCelda(2, 0, "INFORME SEMANAL DE VENTAS", header1);
                        
                        xls.adicionarCelda(fila, col++, "FACTURA", header1);
                        xls.adicionarCelda(fila, col++, "FECHA DE", header1);
                        xls.adicionarCelda(fila, col++, "CODIGO DE", header1);
                        xls.adicionarCelda(fila, col++, "NIT", header1);
                        xls.adicionarCelda(fila, col++, "VALOR", header1);
                        xls.adicionarCelda(fila, col++, "IVA", header1);
                        fila++;
                        col = 0;
                        xls.adicionarCelda(fila, col++, "", header1);
                        xls.adicionarCelda(fila, col++, "FACTURA", header1);
                        xls.adicionarCelda(fila, col++, "CLIENTE", header1);
                        xls.adicionarCelda(fila, col++, "", header1);
                        xls.adicionarCelda(fila, col++, "", header1);
                        xls.adicionarCelda(fila, col++, "", header1);
                        fila++;
                        col = 0;
                        xls.adicionarCelda(fila, col++, "", header1);
                        xls.adicionarCelda(fila, col++, "", header1);
                        xls.adicionarCelda(fila, col++, "", header1);
                        xls.adicionarCelda(fila, col++, "", header1);
                        xls.adicionarCelda(fila, col++, "", header1);
                        xls.adicionarCelda(fila, col++, "", header1);
                        /**/
                    }
                    
                    
                    /************************************************************************************************/
                    
                    
                    
                    nuevo = new com.tsp.finanzas.contab.model.beans.RepGral();
                    
                    nuevo.setFactura(obj0.getFactura());
                    nuevo.setNit(obj0.getNit());
                    nuevo.setCodcli(obj0.getCodcli());
                    
                    if( sw == 1 )
                        nuevo.setFecha(obj0.getFecha());
                    else
                        nuevo.setFecha("");
                    
                    sw = 0;
                    
                    String fechaA = obj0.getFecha_anula();
                    //logger.info("FACTURA: " + obj0.getFactura() + " - FECHA DE ANULACION: " + fechaA);
                    
                    col=0;
                    fila++;
                    //logger.info(" VALIDACION (total = " + total + "): " +( total==0 ) + " -- " + (total==0 ? nuevo.getFecha() : ""));
                    xls.adicionarCelda(fila, col++, nuevo.getFactura(), texto);
                    xls.adicionarCelda(fila, col++, total==0 ? nuevo.getFecha() : "", texto);
                    xls.adicionarCelda(fila, col++, nuevo.getCodcli(), texto);
                    xls.adicionarCelda(fila, col++, nuevo.getNit(), texto);
                    
                    if( fechaA.compareTo("0099-01-01 00:00:00")==0 ){
                        double valor = Double.parseDouble(obj0.getValor());
                        total += valor;
                        nuevo.setValor(String.valueOf(valor));
                    } else {
                        nuevo.setValor(String.valueOf(0));
                    }
                    
                    xls.adicionarCelda(fila, col++, Double.valueOf(nuevo.getValor()).doubleValue(), moneda);
                    xls.adicionarCelda(fila, col++, "", texto);
                    
                    /******************************************************************************************************/
                    
                     /* FACTURA INTERMEDIA FALTANTE */
                    
                    if ( this.reporte.size()>(i+1) ) {
                        com.tsp.finanzas.contab.model.beans.RepGral obj1 = (com.tsp.finanzas.contab.model.beans.RepGral) this.reporte.elementAt(i+1);
                        String factnext = obj1.getFactura();
                        String fact_prox = obj1.getFactura();
                        String factnext_aux = factnext.substring(1, factnext.length());
                        factnext = String.valueOf(Long.parseLong(factnext.substring(1, factnext.length())));
                        //logger.info(">> FACTURA NEXT: " + factnext);
                        String ceros = "";
                        for( int k=0; k<(factnext_aux.length() - factnext.length()); k++){
                            ceros += "0";
                        }
                        
                        String actual0 = String.valueOf(Long.parseLong(actual.substring(1, actual.length())));
                        logger.info("FACTURA ACTUAL: " + actual0);
                        logger.info("FACTURA PROX: " + prox);
                        logger.info("FACTURA SIGUIENTE: " + factnext);
                        logger.info("pre prox: " + fact_prox.charAt(0) + " - " + actual.charAt(0) + " ? iguales: " + (fact_prox.charAt(0) == actual.charAt(0)) + " " +
                            "? diferente al proximo: " + !factnext.equals(String.valueOf(prox)) );
                        if( fact_prox.charAt(0) == actual.charAt(0) && !factnext.equals(String.valueOf(prox)) && Long.parseLong(factnext) > prox  ){
                            
                            logger.info("... generar fras faltantes desde " + prox + " hasta " + (Long.parseLong(factnext) - 1));
                            while( !factnext.equals(String.valueOf(prox)) ){
                                nuevo = new com.tsp.finanzas.contab.model.beans.RepGral();
                                String faltante = actual.substring(0, 1) + ceros//actual.length() - String.valueOf(prox).length())
                                    + String.valueOf(prox);
                                //logger.info("**FALTO FACTURA: " + faltante );
                                nuevo.setFactura("*" + faltante);// o "*" + faltante
                                nuevo.setNit("");
                                nuevo.setCodcli("");
                                nuevo.setValor("");
                                
                                /* IMPRIMIMOS LA FACTURA*/
                                col = 0;
                                fila++;
                                xls.adicionarCelda(fila, col++, nuevo.getFactura(), texto);
                                xls.adicionarCelda(fila, col++, "", texto);
                                xls.adicionarCelda(fila, col++, nuevo.getCodcli(), texto);
                                xls.adicionarCelda(fila, col++, nuevo.getNit(), texto);
                                xls.adicionarCelda(fila, col++, nuevo.getValor(), texto);
                                xls.adicionarCelda(fila, col++, "", texto);
                                
                                prox++;
                                
                                //logger.info("**FALTO FACTURA: " + faltante + ">>> FACTURA NEXT: " + factnext + " >>> PROX: " + prox);                                
                            }
                        }
                        
                        prox = Long.parseLong(factnext) + 1;
                        
                        System.gc();
                        logger.info(">>> FACTURA NEXT: " + factnext + " >>> PROX: " + prox);
                    }
                    
                }
                
                //logger.info("FECHA " + fec + " : " + com.tsp.util.Util.customFormat(total));
                /* ESCRIBIMOS EL TOTAL DE LA FECHA */      
                col=0;
                fila++;
                xls.adicionarCelda(fila, col++, "", texto_ttl);
                xls.adicionarCelda(fila, col++, "", texto_ttl);
                xls.adicionarCelda(fila, col++, "", texto_ttl);
                xls.adicionarCelda(fila, col++, "", texto_ttl);
                xls.adicionarCelda(fila, col++, total, moneda_ttl);
                xls.adicionarCelda(fila, col++, 0, moneda_ttl);
            }
            
            xls.cerrarLibro();
            //logger.info("PROCESO EXITOSO");
            model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(), user.getLogin(), "PROCESO EXITOSO");
            
        }catch (Exception ex){
            ex.printStackTrace();
            //System.out.println("ERROR AL GENERAR EL ARCHIVO XLS : " + ex.getMessage());
            try{
                model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(), user.getLogin(), "ERROR :" + ex.getMessage());
            }
            catch(Exception f){
                try{
                    model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(), user.getLogin(), "ERROR :");
                }catch(Exception p){    }
            }
        }
        
    }    
   
    
}