/*
 * Nombre        HContabilizacion.java
 * Descripci�n   Contabiliza los documentos como EGRESOS.
 * Autor         kreales
 * Fecha         05 de junio
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */

package com.tsp.finanzas.contab.model.threads;

import com.tsp.util.*;
import com.tsp.finanzas.contab.model.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.beans.*;
import com.tsp.finanzas.contab.model.beans.*;
import java.io.*;
import java.sql.*;
import java.util.*;
import java.lang.*;
import javax.servlet.jsp.*;
import javax.servlet.http.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import javax.servlet.http.*;
import com.tsp.util.LogWriter;
import com.tsp.finanzas.contab.model.services.*;

public class HContabilizacionEgresos extends Thread{

    private   Usuario usuario;
    private PrintWriter pw;
    private LogWriter logWriter;
    java.text.SimpleDateFormat anomes;
    java.text.SimpleDateFormat fecha;
    java.text.SimpleDateFormat fechaHora;
    String periodo;
    String fechadoc;
    String fechaInicio;
    java.util.Date pdate;
    java.util.Date ahora;
    String tipodoc;
    String moneda;

    public HContabilizacionEgresos() {
    }

    /**
     * Establece las variables iniciales y activa el inicio del hilo
     * @param user El usuario en sesi�n
     * @throws Exception Si algun error ocurre en el acceso a la BD
     */
    public void start( Usuario user, String tipo, String moneda ) throws Exception{
        /*
         *Busco la fecha actual y le resto un dia.
         */
        Calendar hoy = Calendar.getInstance();
        hoy.add(hoy.DATE, -1);

        pdate = hoy.getTime();
        ahora = new java.util.Date();

        anomes = new java.text.SimpleDateFormat("yyyyMM");
        fecha = new java.text.SimpleDateFormat("yyyy-MM-dd");
        fechaHora = new java.text.SimpleDateFormat("yyyy-MM-dd hh:mm");

        periodo = anomes.format(pdate);
        fechadoc = fecha.format(pdate);
        fechaInicio= fechaHora.format(ahora);


        ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
        String ruta = rb.getString("ruta")+ "/exportar/migracion/"+user.getLogin();

        File file = new File(ruta);
        file.mkdirs();

        pw = new PrintWriter(System.err, true);

        String logFile = ruta + "/"+user.getBd()+"_HContabilizacionEgresos"+fechadoc+".txt";

        pw = new PrintWriter(new FileWriter(logFile, true), true);
        logWriter = new LogWriter("HContabilizacionEgresos", LogWriter.INFO, pw);
        usuario=user;
        logWriter.setPrintWriter(pw);

        tipodoc=tipo;
        this.moneda=moneda;
        super.start();
    }


    /**
     * Metodo llamado por Java para inciciar la ejecuci�n del hilo
     */
    public void run(){
        com.tsp.finanzas.contab.model.Model model = new com.tsp.finanzas.contab.model.Model(usuario.getBd());
        com.tsp.operation.model.Model modelOperation = new com.tsp.operation.model.Model(usuario.getBd());
        ContabilizacionFacturasServices  svc    = new ContabilizacionFacturasServices(usuario.getBd());
        int canttotal=0;
        try{



            logWriter.log("**********************************************************",LogWriter.INFO);
            logWriter.log("*  NUEVO PROCESO DE CONTABILIZACION DE EGRESOS           *",LogWriter.INFO);
            logWriter.log("*  Periodo  :  "+periodo+"                               *",LogWriter.INFO);
            logWriter.log("*  FechaDoc :  "+fechadoc+"                              *",LogWriter.INFO);
            logWriter.log("*  Empresa  :  "+usuario.getBd()+"                       *",LogWriter.INFO);
            logWriter.log("**********************************************************",LogWriter.INFO);


            modelOperation.LogProcesosSvc.InsertProceso("Generacion Comprobantes", this.hashCode(), "Generacion Comprobantes Egresos de anticipos", usuario.getLogin());

            model.comprobanteService.buscarEgresos();
            Vector plas = model.comprobanteService.getVector();

            logWriter.log("Se encontraron "+plas.size()+" registros para contabilizar",LogWriter.INFO);
            canttotal=plas.size();

            //com.tsp.operation.model.Model modelo =  new com.tsp.operation.model.Model();

             for(int i=0;i<plas.size();i++){

                    Comprobantes  comprobante = (Comprobantes) plas.get(i);
                    logWriter.log("se esta procesando el egreso "+comprobante.getNumdoc(),LogWriter.INFO);
                    //int sec    = model.comprobanteService.getGrupoTransaccion();;
                    comprobante.setFechadoc   ( fechadoc );
                    String monedaLocal   =  comprobante.getMoneda();                  // MonedaLocal
                    String monedaFac     =  "PES";              // Moneda de la Factura
                    String fechaFactura  =  fechadoc;        // Fecha de la Factura
                    comprobante.setTipo("C");
                    comprobante.setOrigen("P");
                    comprobante.setSucursal("OP");
                    comprobante.setTipo_operacion("003");
                    comprobante.setBase("COL");
                    comprobante.setUsuario(usuario.getLogin());
                    comprobante.setAprobador(usuario.getLogin());
                    comprobante.setGrupo_transaccion(0);
                    comprobante.setPeriodo(periodo);
                    double  vlrDebito  = 0;
                    double  vlrCredito = 0;
                    // ITEMS del egreso :
                    List listaDetalle  = new LinkedList(); //ALMACENA LOS ITEMS CREADOS
                    model.comprobanteService.buscarEgresosDet(comprobante);
                    Vector det = model.comprobanteService.getVector();
                    System.out.println("det.size"+det.size());
                    if (det.size()<=0){
                        logWriter.log("el egreso "+comprobante.getNumdoc()+ " no tiene detalles.",LogWriter.INFO);
                    }
                    for(int j=0;j<det.size();j++){//hacer condiciones
                            Comprobantes  comprodet = (Comprobantes)det.get(j);
                            comprodet.setDstrct(comprobante.getDstrct());
                            comprodet.setTipo("D");
                            comprodet.setGrupo_transaccion(comprobante.getGrupo_transaccion());
                            comprodet.setPeriodo        (comprobante.getPeriodo());
                            comprodet.setAbc            ("N/A");
                            comprodet.setTercero        (comprobante.getTercero());
                            comprodet.setUsuario        (usuario.getBd());
                            //comprodet.setTdoc_rel(comprobante.getTipo_operacion());
                            comprodet.setDocrelacionado (comprobante.getNumdoc());
                            comprodet.setDocumento_interno(comprobante.getNumdoc());
                            //comprodet.setNumdoc_rel(comprobante.getNumdoc());
                            comprodet.setBase           (comprobante.getBase());
                            comprodet.setTipodoc("EGR");
                            vlrCredito+=comprodet.getTotal_credito();
                            vlrDebito+=comprodet.getTotal_debito();

                            comprobante.setTdoc_rel(comprodet.getNumdoc_rel());
                            comprobante.setNumdoc_rel(comprodet.getNumdoc_rel());

                            listaDetalle.add( comprodet  );
                            //banco
                    }
                   Comprobantes comprobanco= copy(comprobante);
                   comprobanco.setTotal_debito(0);
                   comprobanco.setTotal_credito(comprobante.getTotal_credito());
                   vlrCredito+=comprobante.getTotal_credito();
                   comprobanco.setDocrelacionado (comprobante.getNumdoc());
                   comprobanco.setDocumento_interno(comprobante.getNumdoc());
                   comprobanco.setNumdoc_rel(comprobante.getNumdoc());
                   comprobanco.setTercero("");
                   listaDetalle.add( comprobanco  );
                // Pendiente saldo:

                   double  saldoComprobante         =  Util.roundByDecimal( vlrDebito - vlrCredito, 2 );
                   String cuentaA = "";  //TMolina 2008-09-15
                  /* if(  saldoComprobante !=  0    ){

                           double  vlrSaldoDebito           =  ( saldoComprobante > 0)?  0  :  saldoComprobante * -1 ;
                           double  vlrSaldoCredito          =  ( saldoComprobante < 0)?  0  :  saldoComprobante      ;

                           Comprobantes detalleSaldo = copy(comprobante);
                           detalleSaldo.setTotal_credito( (vlrSaldoCredito) );
                           detalleSaldo.setTotal_debito ( vlrSaldoDebito  );
                           detalleSaldo.setCuenta("I010010014208");
                           detalleSaldo.setDetalle("APROVECHAMIENTOS CLIENTES");
                           detalleSaldo.setDocrelacionado (comprobante.getNumdoc());
                           detalleSaldo.setDocumento_interno(comprobante.getNumdoc());
                           detalleSaldo.setNumdoc_rel(comprobante.getNumdoc());
                           vlrDebito  +=  vlrSaldoDebito;
                           vlrCredito +=  vlrSaldoCredito;
                           listaDetalle.add( detalleSaldo );
                           //System.out.println("Diferencia");

                    }*/
                   System.out.println("saldoComprobante"+saldoComprobante);
                   if(  saldoComprobante !=  0    ){//TMolina 2008-09-15

                           double  vlrSaldoDebito           =  ( saldoComprobante > 0)?  0  :  saldoComprobante * -1 ;
                           double  vlrSaldoCredito          =  ( saldoComprobante < 0)?  0  :  saldoComprobante      ;

                           double comisionPrestamo   = Double.parseDouble(comprobante.getComentario());
                           double cuatroXMilPrestamo = comprobante.getValor_for();
                           System.out.println("comisionPrestamo"+comisionPrestamo);
                           if(comisionPrestamo > 0){
                               Comprobantes detalleSaldo = copy(comprobante);
                               detalleSaldo.setTotal_credito( comisionPrestamo );//(vlrSaldoCredito) );
                               detalleSaldo.setTotal_debito ( vlrSaldoDebito  );
                               cuentaA = model.comprobanteService.getCuentaAprovechamiento("TCEGRESOCO",comprobante.getNumdoc());
                               detalleSaldo.setCuenta(cuentaA);
                               detalleSaldo.setDetalle("APROVECHAMIENTOS CLIENTES");
                               detalleSaldo.setDocrelacionado (comprobante.getNumdoc());
                               detalleSaldo.setDocumento_interno(comprobante.getNumdoc());
                               detalleSaldo.setNumdoc_rel(comprobante.getNumdoc());
                               vlrDebito  +=  vlrSaldoDebito;
                               vlrCredito +=  comisionPrestamo;//vlrSaldoCredito;
                               listaDetalle.add( detalleSaldo );
                               vlrSaldoCredito = vlrSaldoCredito - comisionPrestamo;
                           }
                           System.out.println("vlrSaldoCredito"+vlrSaldoCredito );
                           if(vlrSaldoCredito > 0){//Cuatro x mil > 0
                               Comprobantes detalleSaldo2 = copy(comprobante);
                               detalleSaldo2.setTotal_credito( (vlrSaldoCredito) );
                               detalleSaldo2.setTotal_debito ( vlrSaldoDebito  );
                               cuentaA = model.comprobanteService.getCuentaAprovechamiento("TCEGRESOCU",comprobante.getNumdoc());
                               detalleSaldo2.setCuenta(cuentaA);
                               detalleSaldo2.setDetalle("APROVECHAMIENTOS CLIENTES");
                               detalleSaldo2.setDocrelacionado (comprobante.getNumdoc());
                               detalleSaldo2.setDocumento_interno(comprobante.getNumdoc());
                               detalleSaldo2.setNumdoc_rel(comprobante.getNumdoc());
                               vlrDebito  +=  vlrSaldoDebito;
                               vlrCredito +=  vlrSaldoCredito;
                               listaDetalle.add( detalleSaldo2 );
                           }

                    }
                  // System.out.println("Items "+listaDetalle.size());
                 // ACTUALIZAMOS DATOS COMPROBANTES
                    comprobante.setTotal_credito( vlrCredito   );
                    comprobante.setTotal_debito ( vlrDebito    );
                    comprobante.setItems        ( listaDetalle );
                    comprobante.setTotal_items  ( listaDetalle.size()   );


                // INSERTAMOS
                   List ins = new LinkedList();
                   ins.add( comprobante );
                   System.out.println("egresito"+comprobante.getNumdoc());
                   logWriter.log("se va a insertar contablemente el egreso "+comprobante.getNumdoc(),LogWriter.INFO);
                   svc.insertarEG( ins, usuario.getLogin() );  // INSERTAMOS EL COMPROBANTE
                   logWriter.log("evaluacion: "+comprobante.getComentario(),LogWriter.INFO);
                   //System.out.println("after insertareg");
            }


            modelOperation.LogProcesosSvc.finallyProceso("Generacion Comprobantes", this.hashCode(), usuario.getLogin(), "PROCESO EXITOSO");
            //System.out.println("Termino Bienn");
        }catch(Exception e){
            System.out.println("error en contab de egresos"+e.toString());
            e.printStackTrace();
            try{
                modelOperation.LogProcesosSvc.finallyProceso("Generacion Comprobantes", this.hashCode(),usuario.getLogin(),"ERROR :" + e.getMessage());
            }catch ( SQLException ex) {
                System.out.println("Error guardando el proceso");
            }
            logWriter.log("ERROR: "+e.toString(),LogWriter.ERROR);
            System.out.println("Error "+e.toString());
            e.printStackTrace();
        }
        finally{
            //canttotal = canttotal-( subidas+nosubidas);
            logWriter.log("FIN DEL PROCESO ",LogWriter.INFO);
            //logWriter.log("Egresos Contabilizadas:    "+subidas,LogWriter.INFO);
            //logWriter.log("Egresos NO contabilizadas: " +nosubidas,LogWriter.INFO);
            //logWriter.log("Egresos NO contabilizadas por ERROR DEL PROGRAMA: " +canttotal,LogWriter.INFO);
        }
    }

    public boolean validarItems(Vector det){
        boolean sw=true;
        for(int j =0; j<det.size();j++){
            Comprobantes detalle = (Comprobantes) det.elementAt(j);
            if(detalle.getCmc().equals("")){
                sw=false;
            }

        }
        return sw;
    }

    public boolean validarAjc(Vector det){
        boolean sw=true;
        for(int j =0; j<det.size();j++){
            Comprobantes detalle = (Comprobantes) det.elementAt(j);
            sw= detalle.isErrorDifCambio();
            if(sw==true){
                break;
            }
        }
        return sw;
    }
/*
    public static void main(String a [])throws Exception{
        try{
            com.tsp.finanzas.contab.model.services.ComprobantesService c = new com.tsp.finanzas.contab.model.services.ComprobantesService();
            String tipoDoc= c.getTipoDocumento("003");
            HContabilizacionEgresos hilo = new HContabilizacionEgresos();
            hilo.start("ADMIN",tipoDoc,"PES");
        }catch(SQLException e){
            System.out.println("Error "+e.getMessage());
        }
    }
*/
     public  Comprobantes  copy(Comprobantes comprodet)throws Exception{
        Comprobantes  clon   =  new  ComprobanteFacturas();
        try{

                 clon.setTipo             ("D");
                 clon.setDstrct           (   comprodet.getDstrct()              );
                 clon.setTipodoc          (   comprodet.getTipodoc()             );
                 clon.setTercero          (   comprodet.getTercero()             );
                 clon.setNumdoc           (   comprodet.getNumdoc()              );
                 clon.setPeriodo          (   comprodet.getPeriodo()             );
                 clon.setFechadoc         (   comprodet.getFechadoc()            );
                 clon.setAuxiliar         (   ""  );
                 clon.setBase             (   comprodet.getBase()                );
                 clon.setCuenta           (   comprodet.getCuenta()              );
                 clon.setDocumento_interno(   comprodet.getDocumento_interno()   );
                 //clon.setTdoc_rel         (    comprodet.getTipo_operacion()          );
                 //clon.setNumdoc_rel       (    comprodet.getNumdoc()        );
                 clon.setGrupo_transaccion(   comprodet.getGrupo_transaccion()   );
                 clon.setDocrelacionado   (   comprodet.getNumdoc()             );
                 clon.setAbc("N/A");
                 clon.setDetalle          (   comprodet.getRef_1()+"-"+comprodet.getRef_2());

        }catch(Exception e){
           throw new Exception ( e.getMessage() );
        }
        return clon;
    }


}
/*
 Entregado a karen 15 Febrero 2007
 */

