/*
 * Nombre        Reversar.java
 * Descripci�n   Actualiza la tabla mayorizacion teniendo en cuenta la fecha de aplicacion de los comprobantes
 * Autor         Juan M. Escandon
 * Fecha         09 de junio
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */
package com.tsp.finanzas.contab.model.threads;

import com.tsp.operation.model.beans.*;
import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.LogWriter;

public class Reversar extends Thread{
    
    private     Vector Vcomprobantes;
    private     PrintWriter pw;
    private     LogWriter logWriter;
    private     Usuario usuario;
    
    
    public Reversar() {
    }
    
    /**
     * Establece las variables iniciales y activa el inicio del hilo
     * @param user El usuario en sesi�n
     * @param fechaInicial indica el periodo inicial
     * @param fechaFinal indica el periodo final
     * @throws Exception Si algun error ocurre en el acceso a la BD
     */
    public void start( Vector aux, Usuario usuario ) throws Exception{
        
        this.Vcomprobantes = aux;
        this.usuario = usuario;
        
        super.start();
    }
    
    
    /**
     * Metodo llamado por Java para inciciar la ejecuci�n del hilo
     */
    public void run(){
        com.tsp.finanzas.contab.model.Model model = new com.tsp.finanzas.contab.model.Model(usuario.getBd());
        com.tsp.operation.model.Model modelOperation = new com.tsp.operation.model.Model(usuario.getBd());
        String batch = "";
        
        String comentario = "PROCESO EXITOSO";
        
        try{
            
            
            
            modelOperation.LogProcesosSvc.InsertProceso("Reversion", this.hashCode(), "Reversion", usuario.getLogin());
            
            String msj = model.ContabilizacionFacSvc.Reversar(Vcomprobantes, usuario.getLogin());
            
            if( !msj.equals(""))
                comentario = "ERROR: " + msj;
            else
                comentario += " TOTAL:"+ Vcomprobantes.size();
            
            modelOperation.LogProcesosSvc.finallyProceso("Reversion", this.hashCode(), usuario.getLogin(), comentario );
            
        }catch(Exception e){
            try{
                modelOperation.LogProcesosSvc.finallyProceso("Reversion", this.hashCode(),usuario.getLogin(),"ERROR :" + e.getMessage());
            }catch ( SQLException ex) {
                //System.out.println("Error guardando el proceso");
            }
            e.printStackTrace();
        }
    }
    /*
    public static void main(String a [])throws Exception{
        try{
            ActualizacionMayorizacion hilo = new ActualizacionMayorizacion();
            hilo.start("ADMIN", "", "" );
        }catch(SQLException e){
            //System.out.println("Error "+e.getMessage());
        }
    }
    */
}
