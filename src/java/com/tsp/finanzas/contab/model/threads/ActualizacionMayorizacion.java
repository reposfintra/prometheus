/*
 * Nombre        ActualizacionMayorizacion.java
 * Descripción   Actualiza la tabla mayorizacion teniendo en cuenta la fecha de aplicacion de los comprobantes
 * Autor         David Piña López
 * Fecha         09 de junio
 * Versión       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */
package com.tsp.finanzas.contab.model.threads;

import com.tsp.util.*;
import com.tsp.finanzas.contab.model.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.beans.*;
import com.tsp.finanzas.contab.model.beans.*;
import java.io.*;
import java.sql.*;
import java.util.*;
import java.lang.*;
import javax.servlet.jsp.*;
import javax.servlet.http.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import javax.servlet.http.*;
import com.tsp.util.LogWriter;

public class ActualizacionMayorizacion extends Thread{
    
    private   Usuario usuario;
    private   String fechaInicial;
    private   String fechaFinal;
    private PrintWriter pw;
    private LogWriter logWriter;
    java.text.SimpleDateFormat fechaHora;
    String fechaTimestamp;
    java.text.SimpleDateFormat fechaHoraReal;
    String fechaActual;
    java.util.Date ahora;
    com.tsp.finanzas.contab.model.Model model;
    com.tsp.operation.model.Model modelOperation;
    
    public ActualizacionMayorizacion() {
    }
    
    /**
     * Establece las variables iniciales y activa el inicio del hilo
     * @param user El usuario en sesión
     * @param fechaInicial indica el periodo inicial
     * @param fechaFinal indica el periodo final
     * @throws Exception Si algun error ocurre en el acceso a la BD
     */
    public void start( Usuario user, String fechaInicial, String fechaFinal, com.tsp.finanzas.contab.model.Model model ) throws Exception{
        
        this.usuario = user;
        this.fechaInicial = fechaInicial;
        this.fechaFinal = fechaFinal;
        this.model = model;
        
        inicializacion();
        model.comprobanteService.getComprobantesSinAplicacion( fechaInicial, fechaFinal );
        
        super.start();
    }
    
    /**
     * Establece las variables iniciales y activa el inicio del hilo
     * @param user El usuario en sesión
     * @param fechaInicial indica el periodo inicial
     * @param fechaFinal indica el periodo final
     * @throws Exception Si algun error ocurre en el acceso a la BD
     */
    /*
    public void start( String user, Vector listaComprobantes ) throws Exception{
     
        this.usuario = user;
        this.fechaInicial = "";
        this.fechaFinal = "";
     
        inicializacion();
        Vector listaTemp = new Vector();
        for( int i = 0; i < listaComprobantes.size(); i++ ){
            Comprobantes c = (Comprobantes)listaComprobantes.get( i );
            model.comprobanteService.getComprobante( c.getTipodoc(), c.getNumdoc(), c.getGrupo_transaccion() );
            Comprobantes temp = model.comprobanteService.getComprobante();
            if( temp != null ){
                listaTemp.add( temp );
            }
        }
        model.comprobanteService.setVector( listaTemp );
     
        super.start();
    }
     */
    private void inicializacion() throws Exception{
        //model = new com.tsp.finanzas.contab.model.Model();
        modelOperation = new com.tsp.operation.model.Model(usuario.getBd());
        /*
         *Se busca la fecha actual
         */
        ahora = new java.util.Date();
        fechaHora = new java.text.SimpleDateFormat("yyyy-MM-dd hh.mm.ss");
        fechaHoraReal = new java.text.SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        fechaTimestamp = fechaHora.format(ahora);
        fechaActual = fechaHoraReal.format(ahora);
        
        ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
        String ruta = rb.getString("ruta");
        
        //Creación del archivo log del usuario
        File file = new File( ruta + "/exportar/migracion/" + usuario.getLogin() );
        file.mkdirs();
        pw = new PrintWriter( System.err, true );
        String logFile = ruta +"/exportar/migracion/" + usuario.getLogin() + "/"+usuario.getBd()+"_ActualizacionMayorizacion" + fechaTimestamp + ".log";
        pw = new PrintWriter( new FileWriter( logFile, true ), true );
        logWriter = new LogWriter( "ActualizacionMayorizacion", LogWriter.INFO, pw );
        logWriter.setPrintWriter( pw );
    }
    
    
    /**
     * Metodo llamado por Java para inciciar la ejecución del hilo
     */
    public void run(){
        
        String batch = "";
        try{
            logWriter.log("**********************************************************",LogWriter.INFO);
            logWriter.log("*  PROCESO DE ACTUALIZACIÓN DE LA TABLA MAYORIZACIÓN     *",LogWriter.INFO);
            logWriter.log("*  Usuario  :  "+usuario.getLogin()+"                    *",LogWriter.INFO);
            logWriter.log("*  FechaDoc :  "+fechaActual+"                           *",LogWriter.INFO);
            logWriter.log("*  Empresa  :  "+usuario.getBd()+"                       *",LogWriter.INFO);
            logWriter.log("**********************************************************",LogWriter.INFO);
            
            
            modelOperation.LogProcesosSvc.InsertProceso("Actualización Mayorización", this.hashCode(), "Actualización mayorización en base a los comprobantes sin fecha de aplicación", usuario.getLogin());
            
            Vector comprobantes = model.comprobanteService.getVector();
            logWriter.log( "Se encontraron " + comprobantes.size() + " registros para actualizar en mayorización", LogWriter.INFO );
            
            int actualizados = 0;
            for( int i =0; i < comprobantes.size(); i++ ){
                Comprobantes c = (Comprobantes) comprobantes.get( i );
                double sumCredito = model.comprobanteService.getSumatoriaComprobantes( c, "valor_credito" );
                double sumDebito = model.comprobanteService.getSumatoriaComprobantes( c, "valor_debito" );
                actualizados++;
                if(sumCredito == sumDebito){
                    if( sumCredito == c.getTotal_credito() && sumDebito == c.getTotal_debito() ){
                        model.comprobanteService.getDetallesComprobante( c );
                        Vector detalles = model.comprobanteService.getVector();
                        //logWriter.log( "Se encontraron " + detalles.size() + " detalles de comprobantes", LogWriter.INFO );
                        
                        for( int j = 0; j < detalles.size(); j++ ){
                            Comprobantes detalle = (Comprobantes) detalles.get( j );
                            detalle.setUsuario( usuario.getLogin() );
                            if( model.mayorizacionService.existeEnMayor( detalle, "mayor" ) ){
                                batch += model.mayorizacionService.updateMayorizacionCuenta( detalle ) + ";";
                            }else{
                                batch += model.mayorizacionService.insertMayorizacionCuenta(detalle);
                            }
                            if( !detalle.getAuxiliar().equals("''") && !detalle.getAuxiliar().equals("") ){
                                if( model.mayorizacionService.existeEnMayor( detalle, "mayor_subledger" ) ){
                                    batch += model.mayorizacionService.updateMayorizacionSubledgerCuenta(detalle) + ";";
                                }else{
                                    batch += model.mayorizacionService.insertMayorizacionSubledgerCuenta(detalle);
                                }
                            }
                            TransaccionService  svc = new  TransaccionService(usuario.getBd());
                            svc.crearStatement();
                            svc.getSt().addBatch( batch );
                            svc.execute();
                            batch = "";
                        }
                        c.setUsuario( usuario.getLogin() );
                        model.comprobanteService.updateAplicacionComprobante( c );
                        
                    }else{
                        logWriter.log( "No se procesó el registro con tipodoc = "+c.getTipodoc()+ ", numdoc = "+
                        c.getNumdoc()+", grupo_transaccion = "+c.getGrupo_transaccion()+", porque el"+
                        "total_debito y el total_credito del comprobante no concuerda con su detalle ",
                        LogWriter.INFO );
                        actualizados--;
                    }
                }
                else{
                    logWriter.log( "No se procesó el registro con tipodoc = "+c.getTipodoc()+ ", numdoc = "+
                    c.getNumdoc()+", grupo_transaccion = "+c.getGrupo_transaccion()+
                    " por diferencia entre total_debito y total_credito en comprodet", LogWriter.INFO );
                    actualizados--;
                }
            }
            logWriter.log("FIN DEL PROCESO ",LogWriter.INFO);
            logWriter.log("Registros Actualizados:    " + actualizados, LogWriter.INFO);
            logWriter.log("Registros No Actualizados: " + (comprobantes.size() - actualizados), LogWriter.INFO);
            
            modelOperation.LogProcesosSvc.finallyProceso("Actualización Mayorización", this.hashCode(), usuario.getLogin(), "PROCESO EXITOSO");
            
        }catch(Exception e){
            try{
                modelOperation.LogProcesosSvc.finallyProceso("Actualización Mayorización", this.hashCode(),usuario.getLogin(),"ERROR :" + e.getMessage());
            }catch ( SQLException ex) {
                //////System.out.println("Error guardando el proceso");
            }
            e.printStackTrace();
        }
    }
    /*
    public static void main(String a [])throws Exception{
        try{
            ActualizacionMayorizacion hilo = new ActualizacionMayorizacion();
            Usuario u = new Usuario();
            u.setLogin("ADMIN");
            u.setBase("COL");
            com.tsp.finanzas.contab.model.Model m = new com.tsp.finanzas.contab.model.Model(u.getBd());
            m.mayorizacionService.setUsuario(u);
            
            hilo.start(u, "", "", m );
        }catch(SQLException e){
            //////System.out.println("Error "+e.getMessage());
        }
    }*/
}
