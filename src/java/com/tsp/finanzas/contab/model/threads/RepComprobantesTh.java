/********************************************************************
 *      Nombre Clase.................   RepComprobantesTh.java
 *      Descripci�n..................   Hilo para la escritura del xls
 *      Autor........................   Tito Andr�s Maturana
 *      Fecha........................   03.02.2006
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.finanzas.contab.model.threads;

import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import java.io.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.operation.model.*;
import com.tsp.finanzas.contab.model.*;
import com.tsp.finanzas.contab.model.beans.*;
import java.util.*;
import java.text.*;
//import com.tsp.operation.model.threads.POIWrite;

//Logger
import org.apache.log4j.*;



public class RepComprobantesTh extends Thread{
    
    Logger logger = Logger.getLogger (ReporteFactCliTh.class);
    
    private Vector reporte;
    private Usuario user;    
    private String fechai;
    private String fechaf;
    private String dstrct;
    
    private com.tsp.operation.model.Model model;
    private com.tsp.finanzas.contab.model.Model modelop;
    
    //Log Procesos
    private String procesoName;
    private String des;
    
    public void start(com.tsp.operation.model.Model op, com.tsp.finanzas.contab.model.Model modelo, Usuario user, String fechai, String fechaf, String dstrct){
        this.reporte = reporte;
        this.user = user;
        this.fechaf = fechaf;
        this.fechai = fechai;
        this.dstrct = dstrct;
        
        this.modelop   = modelo;
        this.model = op;
        this.procesoName = "Consulta de Comprobantes";
        this.des = "Consulta de Comprobantes:  " + fechai + " hasta " + fechaf;
                
        super.start();
    }
    
    public synchronized void run(){
        try{
            //INSERTO EN EL LOG DE PROCESO
            model.LogProcesosSvc.InsertProceso(this.procesoName, this.hashCode(), this.des, user.getLogin());
            
                
            modelop.comprobanteService.consultaComprobantesDetalle(dstrct, fechai, fechaf);
            this.reporte = modelop.comprobanteService.getReporte();
            
            logger.info("RESULTADOS ENCONTRADOS: " + this.reporte.size());
        
            //Fecha del sistema
            Calendar FechaHoy = Calendar.getInstance();
            Date d = FechaHoy.getTime();
            SimpleDateFormat s = new SimpleDateFormat("yyyyMMdd_kkmm");
            SimpleDateFormat s1 = new SimpleDateFormat("yyyy-MM-dd-hh:mm");
            SimpleDateFormat fec = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss");
            SimpleDateFormat time = new SimpleDateFormat("hh:mm");
            String FechaFormated = s.format(d);
            String FechaFormated1 = s1.format(d);
            String hora = time.format(d);
            String Fecha = fec.format(d);
            
            
            //obtener cabecera de ruta
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String path = rb.getString("ruta");
            //armas la ruta
            String Ruta1  = path + "/exportar/migracion/" + user.getLogin() + "/";
            //crear la ruta
            File file = new File(Ruta1);
            file.mkdirs();
            
            POIWrite xls = new POIWrite(Ruta1 +"ConsultaComprobantes_" + FechaFormated + ".xls");
            String nom_archivo = "ConsultaComprobantes_" + FechaFormated + ".xls";            
            
            //Definici�n de Estilos
            HSSFCellStyle fecha  = xls.nuevoEstilo("Arial", 9, false , false, "yyyy-mm-dd hh:mm"  , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
            HSSFCellStyle fecha2  = xls.nuevoEstilo("Arial", 9, false , false, "yyyy-mm-dd"  , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
            HSSFCellStyle texto  = xls.nuevoEstilo("Arial", 9, false , false, "text"        , xls.NONE , xls.NONE , xls.NONE);
            HSSFCellStyle numero = xls.nuevoEstilo("Arial", 9, false , false, ""            , xls.NONE , xls.NONE , xls.NONE);
            
            HSSFCellStyle header1      = xls.nuevoEstilo("Arial", 12, true , false, "text"        , HSSFColor.WHITE.index , HSSFColor.GREEN.index , HSSFCellStyle.ALIGN_LEFT);
            HSSFCellStyle header1a     = xls.nuevoEstilo("Arial", 12, true , false, "text"        , HSSFColor.BLACK.index , HSSFColor.GREY_25_PERCENT.index , HSSFCellStyle.ALIGN_LEFT);
            HSSFCellStyle header2      = xls.nuevoEstilo("Arial", 18, true , false, "text"        , HSSFColor.WHITE.index , HSSFColor.ORANGE.index , HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle header3      = xls.nuevoEstilo("Arial", 12, true , false, "text"        , HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
            HSSFCellStyle texto2      = xls.nuevoEstilo("Arial", 12, false , false, "text"        , HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
            HSSFCellStyle titulo      = xls.nuevoEstilo("Arial", 12, true  , false, "text"        , HSSFColor.BLACK.index , HSSFColor.PINK.index , HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle fechatitle  = xls.nuevoEstilo("Arial", 12, true  , false, "yyyy/mm/dd"  , HSSFColor.RED.index , xls.NONE, HSSFCellStyle.ALIGN_CENTER );
            HSSFCellStyle entero      = xls.nuevoEstilo("Arial", 12, false , false, "text"        , HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle flotante      = xls.nuevoEstilo("Arial", 12, false , false, "text"        , HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_RIGHT);
            HSSFCellStyle moneda  = xls.nuevoEstilo("Arial", 9, false , false, "#,##0.00", xls.NONE , xls.NONE , xls.NONE);
            //\"$ \"
            
            int hoja = 1;
            boolean nu_hoja = false;
            long c = 0;
            int start = 0;
            
            do{
                xls.obtenerHoja("Hoja " + hoja);
                xls.cambiarMagnificacion(3,4);
                
                // cabecera
                int nceldas = 23;
                
                xls.combinarCeldas(0, 0, 0, nceldas);
                xls.combinarCeldas(3, 0, 3, nceldas);
                xls.combinarCeldas(4, 0, 4, nceldas);
                xls.adicionarCelda(0, 0, "CONSULTA DE COMPROBANTES", header2);
                xls.adicionarCelda(3, 0, "Fecha desde: " + fechai + " Hasta: " + fechaf, header3);
                xls.adicionarCelda(4, 0, "Fecha del proceso: " + Fecha, header3);
                
                
                // subtitulos
                
                
                int fila = 6;
                int col  = 0;
                
                /* INFO DE LA CABECERA */
                xls.adicionarCelda(fila, col++, "Tipo Doc.", header1);
                xls.adicionarCelda(fila, col++, "N�mero Doc.", header1);
                xls.adicionarCelda(fila, col++, "Transacci�n", header1);
                xls.adicionarCelda(fila, col++, "Fecha Doc.", header1);
                xls.adicionarCelda(fila, col++, "Concepto", header1);
                xls.adicionarCelda(fila, col++, "Tercero", header1);
                xls.adicionarCelda(fila, col++, "Moneda", header1);
                xls.adicionarCelda(fila, col++, "Ttl. D�bito", header1);
                xls.adicionarCelda(fila, col++, "Ttl. Cr�dito", header1);
                xls.adicionarCelda(fila, col++, "Fecha Aplicaci�n", header1);
                xls.adicionarCelda(fila, col++, "Usuario Aplicaci�n", header1);
                xls.adicionarCelda(fila, col++, "Aprobador", header1);
                xls.adicionarCelda(fila, col++, "Sucursal", header1);
                xls.adicionarCelda(fila, col++, "Tipo de Operaci�n", header1);
                
                /* DETALLE DEL ITEM*/
                
                
                xls.adicionarCelda(fila, col++, "Cuenta", header1a);
                xls.adicionarCelda(fila, col++, "Auxiliar", header1a);
                xls.adicionarCelda(fila, col++, "Concepto", header1a);
                xls.adicionarCelda(fila, col++, "Vlr. D�bito", header1a);
                xls.adicionarCelda(fila, col++, "Vlr. Cr�dito", header1a);
                xls.adicionarCelda(fila, col++, "Tercero", header1a);
                xls.adicionarCelda(fila, col++, "Doc. Interno", header1a);                
                xls.adicionarCelda(fila, col++, "Doc. Base", header1a);
                xls.adicionarCelda(fila, col++, "Tipo Doc. Relacionado", header1a);
                xls.adicionarCelda(fila, col++, "Doc. Relacionado", header1a);
                
                fila++;
                
                for( int i=start; i<reporte.size(); i++){
                    col = 0;
                    
                    c++;
                    start = i;
                    
                    logger.info("Contador: " + c + ". nu hoja? " + nu_hoja);
                    
                    if( c == 50000 ){
                        c = 0;
                        hoja++;
                        nu_hoja = true;
                        logger.info("Voy a generar una nueva Hoja");
                        break;
                    } else {
                        nu_hoja = false;
                    }
                    
                    ComprobanteFacturas doc = (ComprobanteFacturas) this.reporte.elementAt(i);
                    
                    
                    xls.adicionarCelda(fila, col++, doc.getTipodoc(), texto);
                    xls.adicionarCelda(fila, col++, doc.getNumdoc(), texto);
                    xls.adicionarCelda(fila, col++, doc.getGrupo_transaccion(), texto);
                    xls.adicionarCelda(fila, col++, doc.getFechadoc(), fecha);
                    xls.adicionarCelda(fila, col++, doc.getDetalle(), texto);
                    xls.adicionarCelda(fila, col++, doc.getTercero(), texto);
                    xls.adicionarCelda(fila, col++, doc.getMoneda(), texto);
                    xls.adicionarCelda(fila, col++, Double.valueOf(doc.getTotal_debito()).doubleValue(), moneda);
                    xls.adicionarCelda(fila, col++, Double.valueOf(doc.getTotal_credito()).doubleValue(), moneda);
                    xls.adicionarCelda(fila, col++, (doc.getFecha_aplicacion()!=null &&  !doc.getFecha_aplicacion().equals("0099-01-01 00:00:00") )? doc.getFecha_aplicacion().substring(0, 19) : "", texto);
                    xls.adicionarCelda(fila, col++, doc.getUsuario_aplicacion(), texto);
                    xls.adicionarCelda(fila, col++, doc.getAprobador(), texto);
                    xls.adicionarCelda(fila, col++, doc.getSucursal(), texto);
                    xls.adicionarCelda(fila, col++, doc.getTipo_operacion(), texto);
                    
                    
                    /* ITEMS DEL DOCUMENTO */
                    
                    List items = doc.getItems();
                    
                    for( int j = 0;  j < items.size(); j++ ){
                        
                        ComprobanteFacturas comprodet = new ComprobanteFacturas();
                        comprodet = (ComprobanteFacturas) items.get(j);
                        
                        xls.adicionarCelda(fila, col++, comprodet.getCuenta(), texto);
                        xls.adicionarCelda(fila, col++, comprodet.getAuxiliar(), texto);
                        xls.adicionarCelda(fila, col++, comprodet.getDetalle(), texto);
                        xls.adicionarCelda(fila, col++, Double.valueOf(comprodet.getTotal_debito()).doubleValue(), moneda);                        
                        xls.adicionarCelda(fila, col++, Double.valueOf(comprodet.getTotal_credito()).doubleValue(), moneda);
                        xls.adicionarCelda(fila, col++, comprodet.getTercero(), texto);
                        xls.adicionarCelda(fila, col++, comprodet.getDocumento_interno(), texto);
                        xls.adicionarCelda(fila, col++, comprodet.getBase(), texto);
                        xls.adicionarCelda(fila, col++, comprodet.getTipo_docrelacionado(), texto);
                        xls.adicionarCelda(fila, col++, comprodet.getDocrelacionado(), texto);
                    }
                    
                    fila++;
                }
                
            } while ( nu_hoja );
            
            xls.cerrarLibro();
            model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(), user.getLogin(), "PROCESO EXITOSO");
            
        }catch (Exception ex){
            ex.printStackTrace();
            //////System.out.println("ERROR AL GENERAR EL ARCHIVO XLS : " + ex.getMessage());
            try{
                model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(), user.getLogin(), "ERROR :" + ex.getMessage());
            }
            catch(Exception f){
                try{
                    model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(), user.getLogin(), "ERROR :");
                }catch(Exception p){    }
            }
        }
        
    }    
   
    
}