/*
 * Nombre        HContabilizacion.java
 * Descripci�n   Contabiliza los documentos como Remesa.
 * Autor         kreales
 * Fecha         05 de junio
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */

package com.tsp.finanzas.contab.model.threads;

import com.tsp.util.*;
import com.tsp.finanzas.contab.model.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.beans.*;
import com.tsp.finanzas.contab.model.beans.*;
import java.io.*;
import java.sql.*;
import java.util.*;
import java.lang.*;
import javax.servlet.jsp.*;
import javax.servlet.http.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import javax.servlet.http.*;
import com.tsp.util.LogWriter;

public class HContabilizacionMovRemesa extends Thread{
    
    private String usuario;
    private PrintWriter pw;
    private LogWriter logWriter;
    java.text.SimpleDateFormat anomes;
    java.text.SimpleDateFormat fecha;
    java.text.SimpleDateFormat fechaHora;
    String periodo;
    String fechadoc;
    String fechaInicio;
    java.util.Date pdate;
    java.util.Date ahora;
    String tipodoc;
    String moneda;
    public HContabilizacionMovRemesa() {
    }
    
    /**
     * Establece las variables iniciales y activa el inicio del hilo
     * @param user El usuario en sesi�n
     * @throws Exception Si algun error ocurre en el acceso a la BD
     */
    public void start( String user, String tipo, String moneda ) throws Exception{
        /*
         *Busco la fecha actual y le resto un dia.
         */
        Calendar hoy = Calendar.getInstance();
        hoy.add(hoy.DATE, -1);
        
        pdate = hoy.getTime();
        ahora = new java.util.Date();
        
        anomes = new java.text.SimpleDateFormat("yyyyMM");
        fecha = new java.text.SimpleDateFormat("yyyy-MM-dd");
        fechaHora = new java.text.SimpleDateFormat("yyyy-MM-dd hh:mm");
        
        periodo = anomes.format(pdate);
        fechadoc = fecha.format(pdate);
        fechaInicio= fechaHora.format(ahora);
        
        
        ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
        String ruta = rb.getString("ruta")+ "/exportar/migracion/"+user;
        
        File file = new File(ruta);
        file.mkdirs();
        
        
        pw = new PrintWriter(System.err, true);
        
        String logFile = ruta + "/HContabilizacionMovRemesa"+fechadoc+".txt";
        
        pw = new PrintWriter(new FileWriter(logFile, true), true);
        logWriter = new LogWriter("HContabilizacionMovRemesa", LogWriter.INFO, pw);
        usuario=user;
        logWriter.setPrintWriter(pw);
        
        tipodoc=tipo;
        this.moneda=moneda;
        super.start();
    }
    
    
    /**
     * Metodo llamado por Java para inciciar la ejecuci�n del hilo
     */
    public void run(){
        com.tsp.finanzas.contab.model.Model model = new com.tsp.finanzas.contab.model.Model();
        com.tsp.operation.model.Model modelOperation = new com.tsp.operation.model.Model();
        int nosubidas=0;
        int subidas=0;
        int canttotal=0;
        try{
            
            
            
            logWriter.log("**********************************************************",LogWriter.INFO);
            logWriter.log("*  NUEVO PROCESO DE CONTABILIZACION DE MOV -REMESAS      *",LogWriter.INFO);
            logWriter.log("*  Periodo  :  "+periodo+"                               *",LogWriter.INFO);
            logWriter.log("*  FechaDoc :  "+fechadoc+"                              *",LogWriter.INFO);
            logWriter.log("**********************************************************",LogWriter.INFO);
            
            
            modelOperation.LogProcesosSvc.InsertProceso("Generacion Comprobantes", this.hashCode(), "Generacion Comprobantes MovRemesa", usuario);
            
            model.comprobanteService.buscarMovRemesas(tipodoc);
            Vector plas = model.comprobanteService.getVector();
            
            logWriter.log("Se encontraron "+plas.size()+" registros para contabilizar",LogWriter.INFO);
            com.tsp.operation.model.Model modelo =  new com.tsp.operation.model.Model();
            canttotal=plas.size();
            
            for(int i =0; i<plas.size();i++){
                Vector consultas = new Vector();
                //Vector consultas2 = new Vector();
                
                Comprobantes c = (Comprobantes) plas.elementAt(i);
                c.setMoneda(moneda);
                
                if(c.isTieneProv() && c.getTotal_credito()!=0 && !c.getMoneda().equals("") && !c.getCmc().equals("") && !c.getAccount_code_c().equals("")){
                    subidas++;
                    
                    double valorRem = c.getTotal_credito();
                    String cuentaDeb = c.getCmc();
                    String cuentaCred =c.getAccount_code_c();
                    
                    //System.out.println("Cuenta debito " +cuentaDeb);
                    //System.out.println("Cuenta credito " +cuentaCred);
                    
                    c.setPeriodo(periodo);
                    c.setFechadoc(fechadoc);
                    c.setAprobador(usuario);
                    c.setDstrct("FINV");
                    c.setTipodoc(tipodoc);
                    c.setTotal_items(2);
                    
                    int gtransaccion = model.comprobanteService.getGrupoTransaccion();
                    c.setGrupo_transaccion(gtransaccion);
                    c.setTipo_operacion("REAJT");
                    model.comprobanteService.setComprobante(c);
                    consultas.add(model.comprobanteService.insert());
                    consultas.add(model.comprobanteService.actualizarTablaGen());
                    
                    c.setDocumento_interno("002");
                    c.setGrupo_transaccion(gtransaccion);
                    
                    if(valorRem >0){
                        
                        c.setTotal_debito(valorRem);
                        c.setTotal_credito(0);
                        c.setCuenta(cuentaDeb);
                        model.comprobanteService.setComprobante(c);
                        consultas.add(model.comprobanteService.insertItem());
                        
                        
                        
                        c.setTotal_debito(0);
                        c.setTotal_credito(valorRem);
                        c.setCuenta(cuentaCred);
                        model.comprobanteService.setComprobante(c);
                        consultas.add(model.comprobanteService.insertItem());
                    }
                    else{
                        valorRem =valorRem*-1;
                        c.setTotal_debito(0);
                        c.setTotal_credito(valorRem);
                        c.setCuenta(cuentaDeb);
                        model.comprobanteService.setComprobante(c);
                        consultas.add(model.comprobanteService.insertItem());
                        
                        
                        
                        c.setTotal_debito(valorRem);
                        c.setTotal_credito(0);
                        c.setCuenta(cuentaCred);
                        model.comprobanteService.setComprobante(c);
                        consultas.add(model.comprobanteService.insertItem());
                        
                    }
                    
                    consultas.add(model.comprobanteService.marcarMovRemesa());
                    
                    
                }
                else{
                    if(!c.isTieneProv())
                        logWriter.log(c.getNumdoc()+"  "+"No se encontro cliente",LogWriter.ERROR);
                    if (c.getTotal_credito()==0)
                        logWriter.log(c.getNumdoc()+"  "+"El MovRemesa tiene valor en 0 ",LogWriter.ERROR);
                    if(c.getMoneda().equals(""))
                        logWriter.log(c.getNumdoc()+"  "+"El MovRemesa no tiene definida una moneda",LogWriter.ERROR);
                    
                    if(c.getCmc().equals(""))
                        logWriter.log(c.getNumdoc()+"  "+"No se encontro numero de cuenta para el movremesa",LogWriter.ERROR);
                    
                    if(c.getAccount_code_c().equals(""))
                        logWriter.log(c.getNumdoc()+"  "+"El standard no tiene numero de cuenta de ingreso",LogWriter.ERROR);
                    
                    nosubidas++;
                }
                modelo.despachoService.insertar(consultas);
            }
            
            
            
            java.util.Date fin = new java.util.Date();
            String fechaFin= fechaHora.format(fin);
            
            //ENVIAMOS EMAIL DE LA CONTABILIZACION
            String mailInt="";
            String mailExt="";
            boolean swInt=false;
            boolean swExt=false;
            LinkedList lista = modelOperation.tablaGenService.obtenerInfoTablaGen("EMAIL", "contabpla");
            if(lista!=null){
                Iterator it = lista.iterator();
                while(it.hasNext()){
                    TablaGen t = (TablaGen)it.next();
                    //System.out.println("-"+t.getDescripcion());
                    
                    if(t.getReferencia().equals("I")){
                        //verifico si exsite una arroba en la descripcion
                        if( t.getDescripcion().indexOf("@") != -1){
                            mailInt += t.getDescripcion()+";";
                        }
                        //se obtiene el login del usuario y se busca el email
                        else{
                            modelOperation.usuarioService.searchUsuario( t.getDescripcion().toUpperCase() );
                            Usuario u = modelOperation.usuarioService.getUsuario();
                            if(!u.getEmail().equals(""))
                                mailInt += u.getEmail()+";";
                        }
                        swInt = true;
                    }
                    //si el email del cliente es externo
                    else{
                        //verifico si exsite una arroba en la descripcion
                        if( t.getDescripcion().indexOf("@") != -1){
                            mailExt += t.getDescripcion()+";";
                        }
                        else{
                            //se obtiene el login del usuario y se busca el email
                            modelOperation.usuarioService.searchUsuario( t.getDescripcion().toUpperCase() );
                            Usuario u = modelOperation.usuarioService.getUsuario();
                            if(!u.getEmail().equals(""))
                                mailExt += u.getEmail()+";";
                        }
                        swExt = true;
                    }
                }
                
                //System.out.println("interno "+mailInt);
                //System.out.println("externo "+mailExt);
                
                com.tsp.operation.model.beans.SendMail email = new com.tsp.operation.model.beans.SendMail();
                email.setEmailfrom("procesos@sanchezpolo.com");
                email.setSendername( "PROCESO CONTABILIDAD" );
                email.setRecstatus("A");
                email.setEmailcode("");
                email.setEmailcopyto("");
                email.setEmailsubject("CONTABILIZACION REMESA");
                email.setEmailbody("SE GENERO LA CONTABILIZACION DE LA REMESA EN UN PROCESO AUTOMATICO \n\n" +
                "Mov Remesas Contabilizadas:    "+subidas+
                "\nMov Remesas NO contabilizadas: " +nosubidas+
                "\n\nFECHA INICIO:    "+ fechaInicio
                +"\nFECHA FINAL      "+ fechaFin+
                "\n\nProceso finalizado con exito" +
                "\n\n\nNOTA: Para mas informacion a cerca de las mov remesas no contabilizadas consulte" +
                " el log de contabilizacion.");
                if(swInt==true){
                    mailInt = mailInt.substring(0, mailInt.length() - 1 );
                    email.setEmailto(mailInt);
                    email.setTipo("I");
                    modelOperation.sendMailService.sendMail(email);
                }
                if(swExt==true){
                    mailExt = mailExt.substring(0, mailExt.length() - 1 );
                    email.setEmailto(mailExt);
                    email.setTipo("E");
                    modelOperation.sendMailService.sendMail(email);
                }
                
            }
            
            
            
            modelOperation.LogProcesosSvc.finallyProceso("Generacion Comprobantes", this.hashCode(), usuario, "PROCESO EXITOSO");
            
        }catch(Exception e){
            
            try{
                modelOperation.LogProcesosSvc.finallyProceso("Generacion Comprobantes", this.hashCode(),usuario,"ERROR :" + e.getMessage());
            }catch ( SQLException ex) {
                //System.out.println("Error guardando el proceso");
            }
            logWriter.log("ERROR: "+e.toString(),LogWriter.ERROR);
            e.printStackTrace();
        }
        finally{
            canttotal = canttotal-( subidas+nosubidas);
            logWriter.log("FIN DEL PROCESO ",LogWriter.INFO);
            logWriter.log("Mov Remesas Contabilizadas:    "+subidas,LogWriter.INFO);
            logWriter.log("Mov Remesas NO contabilizadas: " +nosubidas,LogWriter.INFO);
            logWriter.log("Mov Remesas NO contabilizadas por ERROR DEL PROGRAMA: " +canttotal,LogWriter.INFO);
        }
    }
    
    
    public static void main(String a [])throws Exception{
        try{
            com.tsp.finanzas.contab.model.services.ComprobantesService c = new com.tsp.finanzas.contab.model.services.ComprobantesService();
            String tipoDoc= c.getTipoDocumento("002");
            HContabilizacionMovRemesa hilo = new HContabilizacionMovRemesa();
            hilo.start("ADMIN",tipoDoc,"PES");
        }catch(SQLException e){
            //System.out.println("Error "+e.getMessage());
        }
    }
    
    
}
