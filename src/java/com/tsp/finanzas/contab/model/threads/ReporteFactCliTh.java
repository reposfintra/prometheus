/********************************************************************
 *      Nombre Clase.................   ReporteFactCliTh.java
 *      Descripci�n..................   Hilo para la escritura del xls
 *      Autor........................   Tito Andr�s Maturana
 *      Fecha........................   03.02.2006
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.finanzas.contab.model.threads;

import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import java.io.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.operation.model.*;
import com.tsp.finanzas.contab.model.*;
import java.util.*;
import java.text.*;
//import com.tsp.operation.model.threads.POIWrite;

//Logger
import org.apache.log4j.*;



public class ReporteFactCliTh extends Thread{
    
    Logger logger = Logger.getLogger (ReporteFactCliTh.class);
    
    private Vector reporte;
    private Usuario user;    
    private String fechai;
    private String fechaf;
    private String login;
    
    private com.tsp.operation.model.Model model;
    private com.tsp.finanzas.contab.model.Model modelop;
    
    //Log Procesos
    private String procesoName;
    private String des;
    
    public void start(com.tsp.operation.model.Model op, com.tsp.finanzas.contab.model.Model modelo, Usuario user, String fechai, String fechaf, String login){
        this.reporte = reporte;
        this.user = user;
        this.fechaf = fechaf;
        this.fechai = fechai;
        this.login = login;
        
        this.modelop   = modelo;
        this.model = op;
        this.procesoName = "Reporte Facturas Clientes";
        this.des = "Reporte Facturas Clientes:  " + fechai + " - " + fechaf;
                
        super.start();
    }
    
    public synchronized void run(){
        try{
            //INSERTO EN EL LOG DE PROCESO
            model.LogProcesosSvc.InsertProceso(this.procesoName, this.hashCode(), this.des, user.getLogin());
            
                
            modelop.reportes.reporteFacturasClientes(user.getDstrct(), fechai, fechaf, login);
            this.reporte = modelop.reportes.getVector();
            
            logger.info("RESULTADOS ENCONTRADOS: " + this.reporte.size());
        
            //Fecha del sistema
            Calendar FechaHoy = Calendar.getInstance();
            Date d = FechaHoy.getTime();
            SimpleDateFormat s = new SimpleDateFormat("yyyyMMdd_kkmm");
            SimpleDateFormat s1 = new SimpleDateFormat("yyyy-MM-dd-hh:mm");
            SimpleDateFormat fec = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss");
            SimpleDateFormat time = new SimpleDateFormat("hh:mm");
            String FechaFormated = s.format(d);
            String FechaFormated1 = s1.format(d);
            String hora = time.format(d);
            String Fecha = fec.format(d);
            
            
            //obtener cabecera de ruta
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String path = rb.getString("ruta");
            //armas la ruta
            String Ruta1  = path + "/exportar/migracion/" + user.getLogin() + "/";
            //crear la ruta
            File file = new File(Ruta1);
            file.mkdirs();
            
            POIWrite xls = new POIWrite(Ruta1 +"ReporteFacturasCliente_" + FechaFormated + ".xls");
            String nom_archivo = "ReporteFacturasCliente_" + FechaFormated + ".xls";            
            
            //Definici�n de Estilos
            HSSFCellStyle fecha  = xls.nuevoEstilo("Arial", 9, false , false, "yyyy/mm/dd hh:mm"  , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
            HSSFCellStyle texto  = xls.nuevoEstilo("Arial", 9, false , false, "text"        , xls.NONE , xls.NONE , xls.NONE);
            HSSFCellStyle numero = xls.nuevoEstilo("Arial", 9, false , false, ""            , xls.NONE , xls.NONE , xls.NONE);
            
            HSSFCellStyle header1      = xls.nuevoEstilo("Arial", 12, true , false, "text"        , HSSFColor.WHITE.index , HSSFColor.GREEN.index , HSSFCellStyle.ALIGN_LEFT);
            HSSFCellStyle header2      = xls.nuevoEstilo("Arial", 18, true , false, "text"        , HSSFColor.WHITE.index , HSSFColor.ORANGE.index , HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle header3      = xls.nuevoEstilo("Arial", 12, true , false, "text"        , HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
            HSSFCellStyle texto2      = xls.nuevoEstilo("Arial", 12, false , false, "text"        , HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
            HSSFCellStyle titulo      = xls.nuevoEstilo("Arial", 12, true  , false, "text"        , HSSFColor.BLACK.index , HSSFColor.PINK.index , HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle fechatitle  = xls.nuevoEstilo("Arial", 12, true  , false, "yyyy/mm/dd"  , HSSFColor.RED.index , xls.NONE, HSSFCellStyle.ALIGN_CENTER );
            HSSFCellStyle entero      = xls.nuevoEstilo("Arial", 12, false , false, "text"        , HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle flotante      = xls.nuevoEstilo("Arial", 12, false , false, "text"        , HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_RIGHT);
            HSSFCellStyle moneda  = xls.nuevoEstilo("Arial", 9, false , false, "#,##0.00", xls.NONE , xls.NONE , xls.NONE);
            //\"$ \"
                                                
            xls.obtenerHoja("Reporte de Extrafletes");
            xls.cambiarMagnificacion(3,4);
            
            // cabecera
            int nceldas = 14;
            
            xls.combinarCeldas(0, 0, 0, nceldas);
            xls.combinarCeldas(3, 0, 3, nceldas);
            xls.combinarCeldas(4, 0, 4, nceldas);
            xls.adicionarCelda(0, 0, "REPORTE DE FACTURAS DE CLIENTES", header2);
            xls.adicionarCelda(3, 0, "Fecha desde: " + fechai + " Hasta: " + fechaf, header3);
            xls.adicionarCelda(4, 0, "Fecha del proceso: " + Fecha, header3);
            
                            
            // subtitulos
            
            
            int fila = 6;
            int col  = 0;
            
            xls.adicionarCelda(fila, col++, "FACTURA", header1);
            xls.adicionarCelda(fila, col++, "FECHA", header1);
            xls.adicionarCelda(fila, col++, "CREADA POR", header1);
            xls.adicionarCelda(fila, col++, "NIT CLIENTE", header1);
            xls.adicionarCelda(fila, col++, "NOMBRE CLIENTE", header1);
            xls.adicionarCelda(fila, col++, "GRUPO TRANSACCION", header1);
            xls.adicionarCelda(fila, col++, "TRANSACCION", header1);
            xls.adicionarCelda(fila, col++, "PERIODO", header1);
            xls.adicionarCelda(fila, col++, "CUENTA", header1);
            xls.adicionarCelda(fila, col++, "AUXILIAR", header1);
            xls.adicionarCelda(fila, col++, "DETALLE", header1);
            xls.adicionarCelda(fila, col++, "DEBITO", header1);
            xls.adicionarCelda(fila, col++, "CREDITO", header1);
            xls.adicionarCelda(fila, col++, "TERCERO", header1);
            xls.adicionarCelda(fila, col++, "DOC. INTERNO", header1);
            
            fila++;
            
            for( int i=0; i<reporte.size(); i++){
                col = 0;
                
                Hashtable ht = (Hashtable) reporte.elementAt(i);
                
                xls.adicionarCelda(fila, col++, ht.get("numdoc").toString(), texto);
                                
                java.util.Date date = com.tsp.util.Util.ConvertiraDate0(ht.get("fecha").toString()+":00");
                //xls.adicionarCelda(fila, col++, ht.get("fecha_creacion").toString(), texto);
                xls.adicionarCelda(fila, col++, date, fecha); 
                xls.adicionarCelda(fila, col++, ht.get("login").toString(), texto);
                xls.adicionarCelda(fila, col++, ht.get("nit").toString(), texto);
                xls.adicionarCelda(fila, col++, ht.get("nomcli").toString(), texto);
                xls.adicionarCelda(fila, col++, ht.get("gtrans").toString(), texto);
                xls.adicionarCelda(fila, col++, ht.get("trans").toString(), texto);
                xls.adicionarCelda(fila, col++, ht.get("periodo").toString(), texto);
                xls.adicionarCelda(fila, col++, ht.get("cuenta").toString(), texto);
                xls.adicionarCelda(fila, col++, ht.get("auxiliar").toString(), texto);
                xls.adicionarCelda(fila, col++, ht.get("detalle").toString(), texto);
                xls.adicionarCelda(fila, col++, Double.valueOf(ht.get("vrdeb").toString()).doubleValue(), moneda);
                xls.adicionarCelda(fila, col++, Double.valueOf(ht.get("vrcred").toString()).doubleValue(), moneda);
                xls.adicionarCelda(fila, col++, ht.get("tercero").toString(), texto);
                xls.adicionarCelda(fila, col++, ht.get("docint").toString(), texto);
                
                fila++;
            }         
            
            xls.cerrarLibro();
            model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(), user.getLogin(), "PROCESO EXITOSO");
            
        }catch (Exception ex){
            ex.printStackTrace();
            //////System.out.println("ERROR AL GENERAR EL ARCHIVO XLS : " + ex.getMessage());
            try{
                model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(), user.getLogin(), "ERROR :" + ex.getMessage());
            }
            catch(Exception f){
                try{
                    model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(), user.getLogin(), "ERROR :");
                }catch(Exception p){    }
            }
        }
        
    }    
   
    
}