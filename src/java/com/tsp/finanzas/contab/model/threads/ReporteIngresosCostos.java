/******************************************************************************
 * Nombre clase :                   ReporteIngresosCostos.java                *
 * Descripcion :                    Genera reporte en excel de las planillas  *
 *                                  Para las cuentas I y C                    *  
 * Autor :                          David Pi�a Lopez                          *
 * Fecha :                          29 de junio de 2006                       *
 * Version :                        1.0                                       *
 * Copyright :                      Fintravalores S.A.                   *
 *****************************************************************************/

package com.tsp.finanzas.contab.model.threads;

import java.util.*;
import java.io.*;
import java.lang.*;
import java.sql.*;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.beans.POIWrite;
import javax.servlet.*;
import com.tsp.exceptions.*;
import com.tsp.operation.controller.*;
import java.text.*;
import javax.servlet.http.*;
import com.tsp.util.*;
import com.tsp.operation.model.*;
import com.tsp.util.Util;
import com.tsp.finanzas.contab.controller.*;
import com.tsp.finanzas.contab.model.beans.*;
import com.tsp.finanzas.contab.model.services.*;
import java.sql.SQLException;
/**
 *
 * @author  David
 */
public class ReporteIngresosCostos extends Thread {    
    private String usuario;
    private String periodo;
    private String fechaInicial;
    private String fechaFinal;
    private String path;
    private String procesoName;
    private String des;
    private com.tsp.finanzas.contab.model.Model model;
    private com.tsp.operation.model.Model modelOperation;
    private String num = "";
    private String veh = "";
    private String nit = "";
    private String nom = "";
    private String ori = "";
    private String nomori = "";
    private String agc = "";
    private String nomagc = "";
    private String agasoc = "";
    private String nomagasoc = "";
    private String fecdes = "";
    
    /** Creates a new instance of ReporteIngresosCostos */
    public ReporteIngresosCostos() {
    }
    
    public void start( String periodo, String fechaInicial, String fechaFinal, Usuario user ) {
        this.procesoName = "Reporte de Ingresos y Costos";
        this.des = "Reporte de Ingresos y Costos";        
        this.fechaInicial = fechaInicial;
        this.fechaFinal = fechaFinal;
        this.usuario = user.getLogin();  
        this.periodo = periodo;
        model = new com.tsp.finanzas.contab.model.Model(user.getBd());
        modelOperation = new com.tsp.operation.model.Model(user.getBd());
        super.start();
        
    }
    
    public synchronized void run(){
        try{
            modelOperation.LogProcesosSvc.InsertProceso( procesoName, this.hashCode(), des, usuario );
            
            ResourceBundle rb = ResourceBundle.getBundle( "com/tsp/util/connectionpool/db" );
            path = rb.getString( "ruta" );
                        
            File file = new File( path + "/exportar/migracion/" + usuario );
            file.mkdirs();
               
            String fecha = Util.getFechaActual_String(6);            
            fecha=fecha.replaceAll( "/", "-" );
            fecha=fecha.replaceAll( ":", "_" );
            
            String nombreArch= "Reporte Ingresos Y Costos[" + fecha + "].xls";
            String       Hoja  = "ReporteIngresosCostos";
            String       Ruta  = path + "/exportar/migracion/" + usuario + "/" +nombreArch; 
            
            HSSFWorkbook wb    = new HSSFWorkbook();
            HSSFSheet    sheet = wb.createSheet( Hoja );
            HSSFRow      row   = null;
            HSSFRow      row2  = null;
            HSSFCell     cell  = null;
            
            for ( int col = 0; col < 40 ; col++ ){                
                sheet.setColumnWidth( (short) col, (short) ( ( 50 * 8 ) / ( (double) 1 / 15 ) ) );                 
            }
            //pa colocar la columna mas ancha
            sheet.setColumnWidth( (short)6, (short) ( ( 50 * 8 ) / ( (double) 1 / 25 ) ) );
            sheet.setColumnWidth( (short)10, (short) ( ( 50 * 8 ) / ( (double) 1 / 20 ) ) );
            sheet.setColumnWidth( (short)12, (short) ( ( 50 * 8 ) / ( (double) 1 / 25 ) ) );
            sheet.setColumnWidth( (short)14, (short) ( ( 50 * 8 ) / ( (double) 1 / 20 ) ) );
            
            /****  ENCABEZADO Y DEFINICION DE ESTILOS ************************************************/
            
            /** ENCABEZADO GENERAL *******************************/
            HSSFFont  fuente1 = wb.createFont();
            fuente1.setFontName("verdana");
            fuente1.setFontHeightInPoints((short)(16)) ;
            fuente1.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            fuente1.setColor((short)(0x1));
            
            HSSFCellStyle estilo1 = wb.createCellStyle();
            estilo1.setFont(fuente1);
            estilo1.setFillForegroundColor(HSSFColor.ORANGE.index);
            estilo1.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            
            
            /** SUBTITULO *******************************/
            HSSFFont  fuenteX = wb.createFont();
            fuenteX.setFontName("verdana");
            fuenteX.setFontHeightInPoints((short)(11)) ;
            fuenteX.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            fuenteX.setColor((short)(0x1));
            
            HSSFCellStyle estiloX = wb.createCellStyle();
            estiloX.setFont(fuenteX);
            estiloX.setFillForegroundColor(HSSFColor.ORANGE.index);
            estiloX.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            
            /** TEXTO EN EL ENCABEAZADO *************************/
            HSSFFont  fuente2 = wb.createFont();
            fuente2.setFontName("verdana");
            fuente2.setFontHeightInPoints((short)(11)) ;
            fuente2.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            fuente2.setColor((short)(0x0));
            
            HSSFCellStyle estilo2 = wb.createCellStyle();
            estilo2.setFont(fuente2);
            estilo2.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo2.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            
            /** ENCABEZADO DE LAS COLUMNAS***********************/
            HSSFFont  fuente3 = wb.createFont();
            fuente3.setFontName("verdana");
            fuente3.setFontHeightInPoints((short)(9)) ;
            fuente3.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            fuente3.setColor((short)(0x1));
            
            HSSFCellStyle estilo3 = wb.createCellStyle();
            estilo3.setFont(fuente3);
            estilo3.setFillForegroundColor(HSSFColor.SEA_GREEN.index);
            estilo3.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo3.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo3.setBottomBorderColor(HSSFColor.BLACK.index);
            estilo3.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo3.setLeftBorderColor  (HSSFColor.BLACK.index);
            estilo3.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo3.setRightBorderColor(HSSFColor.BLACK.index);
            estilo3.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo3.setTopBorderColor   (HSSFColor.BLACK.index);
            estilo3.setAlignment(HSSFCellStyle.ALIGN_CENTER);            
                        
            /** TEXTO NORMAL ************************************/
            HSSFFont  fuente4 = wb.createFont();
            fuente4.setFontName("verdana");
            fuente4.setFontHeightInPoints((short)(9)) ;
            fuente4.setBoldweight(HSSFFont.BOLDWEIGHT_NORMAL);
            fuente4.setColor((short)(0x0));
            
            HSSFCellStyle estilo4 = wb.createCellStyle();
            estilo4.setFont(fuente4);
            estilo4.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo4.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo4.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo4.setBottomBorderColor(HSSFColor.BLACK.index);
            estilo4.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo4.setLeftBorderColor  (HSSFColor.BLACK.index);
            estilo4.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo4.setRightBorderColor(HSSFColor.BLACK.index);
            estilo4.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo4.setTopBorderColor   (HSSFColor.BLACK.index);
            
            /** NUMEROS ************************************/
            HSSFCellStyle estilo5 = wb.createCellStyle();
            estilo5.setFont(fuente4);
            estilo5.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo5.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo5.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo5.setBottomBorderColor(HSSFColor.BLACK.index);
            estilo5.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo5.setLeftBorderColor  (HSSFColor.BLACK.index);
            estilo5.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo5.setRightBorderColor(HSSFColor.BLACK.index);
            estilo5.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo5.setTopBorderColor   (HSSFColor.BLACK.index);
            estilo5.setAlignment(HSSFCellStyle.ALIGN_RIGHT);
            /****************************************************/
            
            HSSFFont fuente6 = wb.createFont();
            fuente6.setColor((short)0x0);  
            fuente6.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            
            HSSFCellStyle estilo6 = wb.createCellStyle();
            estilo6.setFont(fuente6);
            estilo6.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo6.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo6.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo6.setBottomBorderColor(HSSFColor.WHITE.index);
            estilo6.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo6.setLeftBorderColor  (HSSFColor.WHITE.index);
            estilo6.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo6.setRightBorderColor(HSSFColor.WHITE.index);
            estilo6.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo6.setTopBorderColor   (HSSFColor.WHITE.index);
            
            /****************************************************/  
            /* MONEDA */
            HSSFCellStyle estilo7 = wb.createCellStyle();
            estilo7.setFont(fuente4);
            estilo7.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo7.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo7.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo7.setBottomBorderColor(HSSFColor.BLACK.index);
            estilo7.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo7.setLeftBorderColor  (HSSFColor.BLACK.index);
            estilo7.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo7.setRightBorderColor(HSSFColor.BLACK.index);
            estilo7.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo7.setTopBorderColor   (HSSFColor.BLACK.index);
            estilo7.setAlignment(HSSFCellStyle.ALIGN_RIGHT);
            estilo7.setDataFormat(wb.createDataFormat().getFormat("$#,##0.00"));  
            
            /* TEXTO NORMAL CENTRADO */
            HSSFCellStyle estilo9 = wb.createCellStyle();
            estilo9.setFont(fuente4);
            estilo9.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo9.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo9.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo9.setBottomBorderColor(HSSFColor.BLACK.index);
            estilo9.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo9.setLeftBorderColor  (HSSFColor.BLACK.index);
            estilo9.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo9.setRightBorderColor(HSSFColor.BLACK.index);
            estilo9.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo9.setTopBorderColor   (HSSFColor.BLACK.index);
            estilo9.setAlignment(HSSFCellStyle.ALIGN_CENTER);
            
            row  = sheet.createRow((short)(0));
            row  = sheet.createRow((short)(1));
            row  = sheet.createRow((short)(2));
            row  = sheet.createRow((short)(3));
            row  = sheet.createRow((short)(4));
            row  = sheet.createRow((short)(5));       
            
            for ( int j = 0; j < 19; j++ ) {                
                row  = sheet.getRow((short)(0));
                cell = row.createCell((short)(j));
                cell.setCellStyle(estilo1);
                row  = sheet.getRow((short)(1));
                cell = row.createCell((short)(j)); 
                cell.setCellStyle(estiloX);                
            }            
            row  = sheet.getRow((short)(0));
            cell = row.getCell((short)(0));
            cell.setCellValue("TRANSPORTES SANCHEZ POLO");
                        
            row  = sheet.getRow((short)(1));            
            cell = row.getCell((short)(0));            
            cell.setCellValue("Reporte de Ingresos y Costos");
            
            for( int i = 2; i < 5; i++ ){                 
                for ( int j = 0; j < 19; j++ ) {                    
                    row  = sheet.getRow((short)(i));
                    cell = row.createCell((short)(j)); 
                    cell.setCellStyle(estilo6);                    
                }      
                
            }
            
            //FECHA
            row = sheet.getRow((short)(3));  
            cell = row.createCell((short)(0));            
            cell = row.getCell((short)(0));  
            cell.setCellStyle(estilo6);
            cell.setCellValue("FECHA DEL REPORTE: ");
            cell = row.createCell((short)(1));            
            cell = row.getCell((short)(1));            
            cell.setCellStyle(estilo6);
            cell.setCellValue(Util.getFechaActual_String(7));
            
            row = sheet.createRow((short)(5));            
            row = sheet.getRow( (short)(5) );            
            
            //SE CREAN LOS ENCABEZADOS
            cell = row.createCell((short)(0));
            cell.setCellStyle(estilo3);            
            cell.setCellValue("NO.REMESA/PLANILLA");
            
            cell = row.createCell((short)(1));
            cell.setCellStyle(estilo3);
            cell.setCellValue("TIPO DOCUMENTO");
            
            cell = row.createCell((short)(2));
            cell.setCellStyle(estilo3);            
            cell.setCellValue("No.DOCUMENTO");
            
            cell = row.createCell((short)(3));
            cell.setCellStyle(estilo3);
            cell.setCellValue("GRUPO TRANSACCI�N");
            
            cell = row.createCell((short)(4));
            cell.setCellStyle(estilo3);
            cell.setCellValue("TRANSACCI�N");
            
            cell = row.createCell((short)(5));
            cell.setCellStyle(estilo3);            
            cell.setCellValue("CUENTA");
            
            cell = row.createCell((short)(6));
            cell.setCellStyle(estilo3);
            cell.setCellValue("DETALLE");
            
            cell = row.createCell((short)(7));
            cell.setCellStyle(estilo3);
            cell.setCellValue("ORIGEN");
            
            cell = row.createCell((short)(8));
            cell.setCellStyle(estilo3);
            cell.setCellValue("DESCRIPCI�N ORIGEN");
            
            cell = row.createCell((short)(9));
            cell.setCellStyle(estilo3);
            cell.setCellValue("AGENCIA DESPACHO");
            
            cell = row.createCell((short)(10));
            cell.setCellStyle(estilo3);
            cell.setCellValue("NOMBRE AGENCIA DESPACHO");
            
            cell = row.createCell((short)(11));
            cell.setCellStyle(estilo3);
            cell.setCellValue("AGENCIA RELACIONADA");
            
            cell = row.createCell((short)(12));
            cell.setCellStyle(estilo3);
            cell.setCellValue("NOMBRE AGENCIA RELACIONADA");
            
            cell = row.createCell((short)(13));
            cell.setCellStyle(estilo3);
            cell.setCellValue("NIT PROP./CLIENTE");
            
            cell = row.createCell((short)(14));
            cell.setCellStyle(estilo3);
            cell.setCellValue("NOMBRE PROP./CLIENTE");
            
            cell = row.createCell((short)(15));
            cell.setCellStyle(estilo3);
            cell.setCellValue("PLACA");
            
            cell = row.createCell((short)(16));
            cell.setCellStyle(estilo3);
            cell.setCellValue("VALOR");
            
            cell = row.createCell((short)(17));
            cell.setCellStyle(estilo3);
            cell.setCellValue("FECHA DESPACHO");
            
            cell = row.createCell((short)(18));
            cell.setCellStyle(estilo3);
            cell.setCellValue("TIPO REGISTRO");
            
            int Fila = 5;
            
            model.comprobanteService.getCuentasIC( periodo, fechaInicial, fechaFinal );
            Vector detalles = model.comprobanteService.getVector();
            for( int i = 0; i < detalles.size(); i++ ){
                Comprobantes detalle = (Comprobantes)detalles.get( i );
                String tipo = detalle.getAuxiliar();
                String tiporegistro = "";
                Planilla pla = null;
                Remesa rem = null;
                inicializarVariables();
                if( tipo.equalsIgnoreCase("001") ){                    
                    modelOperation.planillaService.setPlanilla( null );
                    modelOperation.planillaService.getPlanillaIC( detalle.getDocumento_interno() );
                    pla = modelOperation.planillaService.getPlanilla();
                    if( pla != null ){
                        num = pla.getNumpla();
                        veh = pla.getPlaveh();
                        nit = pla.getNitpro();
                        nom = pla.getNomprop();
                        ori = pla.getOripla();
                        nomori = pla.getNomori();
                        agc = pla.getAgcpla();
                        nomagc = pla.getNomagc();
                        agasoc = pla.getDestinoRelacionado();
                        nomagasoc = pla.getNomdesrel();
                        fecdes = pla.getFeccum();
                    }
                    tiporegistro = "P";
                }else{
                    modelOperation.remesaService.setRemesa( null );
                    modelOperation.remesaService.getRemesaIC( detalle.getDocumento_interno() );
                    rem = modelOperation.remesaService.getRemesa();
                    if( rem != null ){
                        num = rem.getNumrem();
                        veh = "";
                        nit = rem.getCodcli();
                        nom = rem.getNombre_cli();
                        ori = rem.getOrirem();
                        nomori = rem.getNombre_ciu();
                        agc = rem.getAgcrem();
                        nomagc = rem.getCiuRemitente();
                        agasoc = rem.getDestinoRelacionado();
                        nomagasoc = rem.getNombreDestinoRelacionado();
                        fecdes = rem.getFeccum();
                    }
                    tiporegistro = "R";
                }                                
                
                if( rem != null || pla != null ){                    
                
                    Fila++;
                    row  = sheet.createRow( (short)(Fila) );

                    cell = row.createCell( (short)(0) );
                    cell.setCellStyle( estilo9 );
                    cell.setCellValue( num );

                    cell = row.createCell( (short)(1) );
                    cell.setCellStyle( estilo9 );
                    cell.setCellValue( detalle.getTipodoc() );

                    cell = row.createCell( (short)(2) );
                    cell.setCellStyle( estilo9 );
                    cell.setCellValue( detalle.getNumdoc() );

                    cell = row.createCell( (short)(3) );
                    cell.setCellStyle( estilo9 );
                    cell.setCellValue( detalle.getGrupo_transaccion() );

                    cell = row.createCell( (short)(4) );
                    cell.setCellStyle( estilo9 );
                    cell.setCellValue( detalle.getTransaccion() );

                    cell = row.createCell( (short)(5) );
                    cell.setCellStyle( estilo4 );
                    cell.setCellValue( detalle.getCuenta() );

                    cell = row.createCell( (short)(6) );
                    cell.setCellStyle( estilo4 );
                    cell.setCellValue( detalle.getDetalle() );

                    cell = row.createCell( (short)(7) );
                    cell.setCellStyle( estilo9 );
                    cell.setCellValue( ori );

                    cell = row.createCell( (short)(8) );
                    cell.setCellStyle( estilo4 );
                    cell.setCellValue( nomori );

                    cell = row.createCell( (short)(9) );
                    cell.setCellStyle( estilo9 );
                    cell.setCellValue( agc );

                    cell = row.createCell( (short)(10) );
                    cell.setCellStyle( estilo4 );
                    cell.setCellValue( nomagc );

                    cell = row.createCell( (short)(11) );
                    cell.setCellStyle( estilo9 );
                    cell.setCellValue( agasoc );

                    cell = row.createCell( (short)(12) );
                    cell.setCellStyle( estilo4 );
                    cell.setCellValue( nomagasoc );

                    cell = row.createCell( (short)(13) );
                    cell.setCellStyle( estilo4 );
                    cell.setCellValue( nit );

                    cell = row.createCell( (short)(14) );
                    cell.setCellStyle( estilo4 );
                    cell.setCellValue( nom );

                    cell = row.createCell( (short)(15) );
                    cell.setCellStyle( estilo4 );
                    cell.setCellValue( veh );

                    cell = row.createCell( (short)(16) );
                    cell.setCellStyle( estilo7 );
                    cell.setCellValue( (detalle.getTotal_debito() != 0)? detalle.getTotal_debito() : detalle.getTotal_credito());

                    cell = row.createCell( (short)(17) );
                    cell.setCellStyle( estilo9 );
                    cell.setCellValue( fecdes );

                    cell = row.createCell( (short)(18) );
                    cell.setCellStyle( estilo9 );
                    cell.setCellValue( tiporegistro );
                }
                
            }
            
            /*************************************************************************************/
            /**** GUARDAR DATOS EN EL ARCHIVO  ***/
            FileOutputStream fo = new FileOutputStream( Ruta );
            wb.write( fo );
            fo.close();
            
            modelOperation.LogProcesosSvc.finallyProceso( procesoName, this.hashCode(), usuario, "PROCESO EXITOSO" );
        }catch( Exception e ){                
            e.printStackTrace();            
            //Capturo errores finalizando proceso
            try{                
                modelOperation.LogProcesosSvc.finallyProceso( procesoName, this.hashCode(), usuario, "ERROR : " + e.getMessage() );            
            } catch( Exception f ){                
                f.printStackTrace();                
                try{                    
                    modelOperation.LogProcesosSvc.finallyProceso( procesoName,this.hashCode(), usuario, "ERROR : " + f.getMessage() );                
                } catch( Exception p ){ p.printStackTrace(); }                
            }
        }
    }
    private void inicializarVariables(){
        num = "";
        veh = "";
        nit = "";
        nom = "";
        ori = "";
        nomori = "";
        agc = "";
        nomagc = "";
        agasoc = "";
        nomagasoc = "";
        fecdes = "";
    }
    /*
    public static void main( String arg[] ){
        ReporteIngresosCostos ric = new ReporteIngresosCostos();
        ric.start( "200606", "2006/01/01", "2006/12/31", "GPINA" );
    }
    */
}
