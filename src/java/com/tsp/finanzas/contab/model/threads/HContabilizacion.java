    /***************************************
    * Nombre Clase ............. HExportarMovAuxiliar.java
    * Descripción  .. . . . . .  Permite exportar a Excel la lista de cuentas consultadas
    * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
    * Fecha . . . . . . . . . .  10/06/2006
    * versión . . . . . . . . .  1.0
    * Copyright ...Transportes Sanchez Polo S.A.
    *******************************************/


package com.tsp.finanzas.contab.model.threads;





import java.io.*;
import java.util.*;
import com.tsp.finanzas.contab.model.*;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.LogProcesosService;
import com.tsp.util.*;



public class HContabilizacion extends Thread{
    
    private Model     model;
    private String    procesoName;
    private Usuario   user;
   
    
    
    public HContabilizacion() {
    }
    
    
    
    
    public void start(Model modelo, Usuario usuario) throws Exception{
         try{
                this.model       = modelo;
                this.user        = usuario;
                this.procesoName = "Contabilización";
                super.start();
        }catch(Exception e){
           throw new Exception( e.getMessage() );
       }
    }
    
    
    
    
    
    
     public synchronized void run(){
       LogProcesosService log = new LogProcesosService(user.getBd());
       try{
           
           String hoy  =  Utility.getHoy("-");
           String ayer =  Utility.convertirFecha( hoy, -1);
           
           String comentario="EXITOSO";
           log.InsertProceso(this.procesoName, this.hashCode(), hoy ,this.user.getLogin() ); 
           
           model.ContabilizacionFacSvc.contabilizar( user.getDstrct(), ayer, this.user);
                 comentario    =  model.ContabilizacionFacSvc.getESTADO(); 
           
           
           model.ContabilizacionFacSvc.setProcess( false );           
           log.finallyProceso(this.procesoName, this.hashCode(), this.user.getLogin(),comentario);
             
       }catch(Exception e){
           try{       
               model.ContabilizacionFacSvc.setProcess( false );
               log.finallyProceso(this.procesoName,this.hashCode(), this.user.getLogin(),"ERROR Hilo: " + e.getMessage()); }
           catch(Exception f){}   
       }
    }
    
    
}
