   /***************************************
    * Nombre Clase ............. HExportarMovAuxiliar.java
    * Descripci�n  .. . . . . .  Permite exportar a Excel la lista de cuentas consultadas
    * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
    * Fecha . . . . . . . . . .  10/06/2006
    * versi�n . . . . . . . . .  1.0
    * Copyright ...Transportes Sanchez Polo S.A.
    *******************************************/


package com.tsp.finanzas.contab.model.threads;



import java.io.*;
import java.util.*;
import com.tsp.finanzas.contab.model.*;
import com.tsp.operation.model.LogProcesosService;
import com.tsp.operation.model.services.DirectorioService;
import com.tsp.util.*;


import com.tsp.operation.model.beans.POIWrite;
import com.tsp.operation.model.beans.Usuario;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;



public class HExportarMovAuxiliar  extends Thread{
    
    
    private Model              model;
    private String             procesoName;
    private String             user;
    
    private String             url;
    
    private POIWrite           Excel; 
    private String             hoja;
    private int                fila;
    private int                columna;  

    private HSSFCellStyle      texto;
    private HSSFCellStyle      negrilla;
    private HSSFCellStyle      numero; 
    private HSSFCellStyle      titulo;
    private HSSFCellStyle      tituloTotales;
    
    private Usuario usuario;
    
    public HExportarMovAuxiliar() {}
    
    
       

    
    
    public void start(Model modelo, String usuario, Usuario u) throws Exception{
         try{
            this.model       = modelo;
            this.user        = usuario;
            this.procesoName = "Movimiento Auxiliar";
            this.usuario=u;
            super.start();
        }catch(Exception e){
           throw new Exception( e.getMessage() );
       }
    }
    
    
    
    
     /**
     * M�todo que Define estilo del archivo xls
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public void createStyle()throws Exception{
        try{
          this.texto    = Excel.nuevoEstilo("Book Antiqua", 9, false , false, "text"        , Excel.NONE , Excel.NONE , Excel.NONE);
          this.numero   = Excel.nuevoEstilo("Book Antiqua", 9, false , false, ""            , Excel.NONE , Excel.NONE , HSSFCellStyle.ALIGN_RIGHT);
          this.titulo   = Excel.nuevoEstilo("Book Antiqua", 9, true  , false, "text"        , HSSFColor.WHITE.index , HSSFColor.BLACK.index  , HSSFCellStyle.ALIGN_CENTER);              
          this.negrilla = Excel.nuevoEstilo("Book Antiqua", 9, true  , false, "text"        , Excel.NONE , Excel.NONE , Excel.NONE);
          
          tituloTotales = Excel.nuevoEstilo("Book Antiqua", 9, true  , false, ""            , HSSFColor.WHITE.index , HSSFColor.BLACK.index  , HSSFCellStyle.ALIGN_RIGHT);              
          
       }catch(Exception e){
           throw new Exception( e.getMessage() );
       }
    }
    
    
    
    
    /**
     * M�todo que configura parametros de Excel
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
     public void configXLS() throws Exception{
       try{          
            DirectorioService dd = new DirectorioService();
            dd.create(this.user );
            url = dd.getUrl() + this.user + "/MovAuxiliar"+ Util.getFechaActual_String(6).replaceAll("/|:| ","") + ".xls";
           
            this.Excel  = new POIWrite(url);
            createStyle();
            
       }catch(Exception e){
           throw new Exception( e.getMessage() );
       }
    }
    
    
     
     
      /**
     * M�todo que crea la hoja
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public void createHoja(String nombreHoja)throws Exception{
        try{
            
            hoja = nombreHoja;
            this.Excel.obtenerHoja(hoja);             
            setearFila();
            setearColumna();
            
        }catch(Exception e){
           throw new Exception( e.getMessage() );
       }
    }
    
    
    
     /**
     * M�todo que establece la cabecera
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public void cabecera(String cuenta, String nombre, String saldo, String fechaIni, String fechaFin, String auxiliar, String tipoAux)throws Exception{
        try{           
           
            this.Excel.obtenerHoja(hoja);
            
            Excel.adicionarCelda( this.fila, this.columna, "TRANSPORTES SANCHEZ POLO S.A."    ,this.negrilla);   
            Excel.combinarCeldas( this.fila, this.columna, this.fila, this.columna + 1);
            incFila();
            setearColumna(); 
            Excel.adicionarCelda( this.fila, this.columna, "CONSULTA MOVIMIENTOS AUXILIAR"    ,this.negrilla); 
            Excel.combinarCeldas( this.fila, this.columna, this.fila, this.columna + 1);
            incFila();
            setearColumna(); 
            
            Excel.adicionarCelda( this.fila, this.columna, "PERIODO CONSULTA"          ,this.negrilla); incColumna();
            Excel.adicionarCelda( this.fila, this.columna, fechaIni +" - "+  fechaFin  ,this.texto); incColumna();            
            incFila();
            setearColumna();           
            Excel.adicionarCelda( this.fila, this.columna, "CUENTA"             ,this.negrilla); incColumna();
            Excel.adicionarCelda( this.fila, this.columna, cuenta               ,this.texto); incColumna();            
            incFila();
            setearColumna();            
            Excel.adicionarCelda( this.fila, this.columna, "NOMBRE CUENTA"             ,this.negrilla); incColumna();
            Excel.adicionarCelda( this.fila, this.columna, nombre               ,this.texto); incColumna();            
            incFila();
            setearColumna();           
            Excel.adicionarCelda( this.fila, this.columna, "AUXILIAR"              ,this.negrilla); incColumna();
            Excel.adicionarCelda( this.fila, this.columna, auxiliar +" "+ tipoAux  ,this.texto); incColumna();
            incFila();
            setearColumna();            
            Excel.adicionarCelda( this.fila, this.columna, "SALDO ANTERIOR"     ,this.negrilla); incColumna();
            Excel.adicionarCelda( this.fila, this.columna, Util.customFormat(Double.parseDouble(saldo) ) ,this.numero); 
            
            incFila();
            setearColumna();            
            
        }catch(Exception e){
           throw new Exception( e.getMessage() );
       }
    }
    
    
     
      /**
     * M�todo que establece titulos xls
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public void titulos()throws Exception{
        try{
            
            Excel.adicionarCelda( this.fila, this.columna, "DISTRITO",          this.titulo); incColumna();
            Excel.adicionarCelda( this.fila, this.columna, "TIPO DOCUMENTO",    this.titulo); incColumna();
            Excel.adicionarCelda( this.fila, this.columna, "DOCUMENTO",         this.titulo); incColumna();
            
            Excel.adicionarCelda( this.fila, this.columna, "TERCERO",           this.titulo); incColumna();
            Excel.adicionarCelda( this.fila, this.columna, "PERIODO",           this.titulo); incColumna();
            
            Excel.adicionarCelda( this.fila, this.columna, "FECHA",             this.titulo); incColumna();
            Excel.adicionarCelda( this.fila, this.columna, "DESCRIPCION",       this.titulo); incColumna();
            Excel.adicionarCelda( this.fila, this.columna, "TIPO AUXILIAR",     this.titulo); incColumna();
            Excel.adicionarCelda( this.fila, this.columna, "AUXILIAR",          this.titulo); incColumna();
            Excel.adicionarCelda( this.fila, this.columna, "NOMBRE",            this.titulo); incColumna();
            Excel.adicionarCelda( this.fila, this.columna, "MOV DEBITO",        this.titulo); incColumna();
            Excel.adicionarCelda( this.fila, this.columna, "MOV CREDITO",       this.titulo); incColumna();
            Excel.adicionarCelda( this.fila, this.columna, "SALDO ACUMULADO",   this.titulo); 
            
            incFila();
            setearColumna();            
            
        }catch(Exception e){
           throw new Exception( e.getMessage() );
       }
    }
    
    
    
     /**
     * M�todo que establece valores de movimientos de la cuenta
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public void addMovimiento(Hashtable cuenta)throws Exception{
        try{           
           
            this.Excel.obtenerHoja(hoja);
            
            Excel.adicionarCelda( this.fila, this.columna, (String)cuenta.get("distrito")         ,this.texto); incColumna();
            Excel.adicionarCelda( this.fila, this.columna, (String)cuenta.get("tipo_doc")         ,this.texto); incColumna();
            Excel.adicionarCelda( this.fila, this.columna, (String)cuenta.get("documento")        ,this.texto); incColumna();
            
            Excel.adicionarCelda( this.fila, this.columna, (String)cuenta.get("tercero")          ,this.texto); incColumna();
            Excel.adicionarCelda( this.fila, this.columna, (String)cuenta.get("periodo")          ,this.texto); incColumna();
            
            Excel.adicionarCelda( this.fila, this.columna, (String)cuenta.get("fecha")            ,this.texto); incColumna();
            Excel.adicionarCelda( this.fila, this.columna, (String)cuenta.get("descripcion")      ,this.texto); incColumna();
            Excel.adicionarCelda( this.fila, this.columna, (String)cuenta.get("tipo_aux")         ,this.texto); incColumna();
            Excel.adicionarCelda( this.fila, this.columna, (String)cuenta.get("auxiliar")         ,this.texto); incColumna();
            Excel.adicionarCelda( this.fila, this.columna, (String)cuenta.get("nombre")           ,this.texto); incColumna();
            Excel.adicionarCelda( this.fila, this.columna, Util.customFormat(Double.parseDouble((String)cuenta.get("vlrDebito") ) )      ,this.numero); incColumna();
            Excel.adicionarCelda( this.fila, this.columna, Util.customFormat(Double.parseDouble((String)cuenta.get("vlrCredito")) )      ,this.numero); incColumna();
            Excel.adicionarCelda( this.fila, this.columna, Util.customFormat(Double.parseDouble((String)cuenta.get("saldoAcumulado")) )  ,this.numero); 
            
            incFila();
            setearColumna();            
            
        }catch(Exception e){
           throw new Exception( e.getMessage() );
       }
    }
    
    
    
    
    /**
     * M�todo que establece los totales
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public void totales(String debito, String credito, String saldo)throws Exception{
        try{           
           
            this.Excel.obtenerHoja(hoja);            
            Excel.adicionarCelda( this.fila, 9,  "TOTALES" ,this.titulo); 
            Excel.adicionarCelda( this.fila, 10,  Util.customFormat(Double.parseDouble(debito)  ) ,this.tituloTotales); 
            Excel.adicionarCelda( this.fila, 11,  Util.customFormat(Double.parseDouble(credito) ) ,this.tituloTotales); 
            Excel.adicionarCelda( this.fila, 12, Util.customFormat(Double.parseDouble(saldo)   ) ,this.tituloTotales); 
        }catch(Exception e){
           throw new Exception( e.getMessage() );
       }
    }
    
    
    
    
    public void incFila()      {  this.fila++;      }
    public void incColumna()   {  this.columna++;   }    
    public void setearFila()   {  this.fila=0;      }
    public void setearColumna(){  this.columna=0;   }
    
    
    
    
    
     /**
     * M�todo que escribe  en posicion definida
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public void write(int fil, int col, String valor)throws Exception{
       try{
            this.Excel.obtenerHoja(hoja);            
            Excel.adicionarCelda( fil, col, valor  ,this.texto);    
       }catch(Exception e){
           throw new Exception( e.getMessage() );
       }
    }
    
    
    
    
    
     
    
    
    public synchronized void run(){
       LogProcesosService log = new LogProcesosService(usuario.getBd());
       try{
           
           List lista      =  model.MovAuxiliarSvc.getListCuentas();           
           String fechaIni = model.MovAuxiliarSvc.getFechaIni();
           String fechaFin = model.MovAuxiliarSvc.getFechaFin();
           String auxiliar = model.MovAuxiliarSvc.getAuxiliar();
           String tipoAux  = model.MovAuxiliarSvc.getTipoAuxiliar();
           
           String comentario="EXITOSO";
           log.InsertProceso(this.procesoName, this.hashCode(),  model.MovAuxiliarSvc.getCuentaIni()  +"-"+   model.MovAuxiliarSvc.getCuentaFin()  ,this.user); 
           
           
           configXLS();
           
           for(int i=0;i<lista.size();i++){
               Hashtable  cuenta     = (Hashtable)lista.get(i);
               
               String     noCuenta   = (String)cuenta.get("cuenta");
               String     nameCuenta = (String)cuenta.get("nombre_corto");
               String     saldoAnt   = (String)cuenta.get("saldoAnterior");
               
               createHoja(noCuenta);  
               
               cabecera(noCuenta, nameCuenta, saldoAnt, fechaIni, fechaFin, auxiliar, tipoAux);               
               
             // Movimientos:
                List movimientos   = (List) cuenta.get("movimientos");
               
                incFila();
                setearColumna();                
                titulos();                 
                for(int j=0;j<movimientos.size();j++){
                    Hashtable  mov   = (Hashtable)movimientos.get(j);
                    addMovimiento(mov);                    
                } 
                totales((String)cuenta.get("totalDebito"), (String)cuenta.get("totalCredito"), (String)cuenta.get("totalSaldo"));

                
           }
           
           
           Excel.cerrarLibro();
           
           
           log.finallyProceso(this.procesoName,this.hashCode(),this.user,comentario);
             
       }catch(Exception e){
           try{                  
               log.finallyProceso(this.procesoName,this.hashCode(),this.user,"ERROR Hilo: " + e.getMessage()); }
           catch(Exception f){}   
       }
    }
    
    
}
