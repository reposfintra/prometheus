/*
 * Nombre        HContabilizacion.java
 * Descripci�n   Contabiliza los documentos como planilla.
 * Autor         kreales
 * Fecha         05 de junio
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */

package com.tsp.finanzas.contab.model.threads;

import com.tsp.util.*;
import com.tsp.finanzas.contab.model.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.beans.*;
import com.tsp.finanzas.contab.model.beans.*;
import java.io.*;
import java.sql.*;
import java.util.*;
import java.lang.*;
import javax.servlet.jsp.*;
import javax.servlet.http.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import javax.servlet.http.*;
import com.tsp.util.LogWriter;

public class HContabilizacionRedistribucion extends Thread{
    
    private   String usuario;
    private PrintWriter pw;
    private LogWriter logWriter;
    java.text.SimpleDateFormat anomes;
    java.text.SimpleDateFormat fecha;
    java.text.SimpleDateFormat fechaHora;
    String periodo;
    String fechadoc;
    String fechaInicio;
    java.util.Date pdate;
    java.util.Date ahora;
    String tipodoc;
    String moneda;
    public HContabilizacionRedistribucion() {
    }
    
    /**
     * Establece las variables iniciales y activa el inicio del hilo
     * @param user El usuario en sesi�n
     * @throws Exception Si algun error ocurre en el acceso a la BD
     */
    public void start( String user, String tipo, String moneda ) throws Exception{
        /*
         *Busco la fecha actual y le resto un dia.
         */
        Calendar hoy = Calendar.getInstance();
        hoy.add(hoy.DATE, -1);
        
        pdate = hoy.getTime();
        ahora = new java.util.Date();
        
        anomes = new java.text.SimpleDateFormat("yyyyMM");
        fecha = new java.text.SimpleDateFormat("yyyy-MM-dd");
        fechaHora = new java.text.SimpleDateFormat("yyyy-MM-dd hh:mm");
        
        periodo = anomes.format(pdate);
        fechadoc = fecha.format(pdate);
        fechaInicio= fechaHora.format(ahora);
        
        
        ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
        String ruta = rb.getString("ruta")+ "/exportar/migracion/"+user;
        
        File file = new File(ruta);
        file.mkdirs();
        
        
        pw = new PrintWriter(System.err, true);
        
        String logFile = ruta + "/HContabilizacionRedistribucion"+fechadoc+".txt";
        
        pw = new PrintWriter(new FileWriter(logFile, true), true);
        logWriter = new LogWriter("HContabilizacionRedistribucion", LogWriter.INFO, pw);
        usuario=user;
        logWriter.setPrintWriter(pw);
        
        tipodoc=tipo;
        this.moneda=moneda;
        super.start();
    }
    
    
    /**
     * Metodo llamado por Java para inciciar la ejecuci�n del hilo
     */
    public void run(){
        com.tsp.finanzas.contab.model.Model model = new com.tsp.finanzas.contab.model.Model();
        com.tsp.operation.model.Model modelOperation = new com.tsp.operation.model.Model();
        
        try{
            
            Vector consultas= new Vector();
            
            logWriter.log("**********************************************************",LogWriter.INFO);
            logWriter.log("*  NUEVO PROCESO DE CONTABILIZACION DE REDISTRIBUCION    *",LogWriter.INFO);
            logWriter.log("*  Periodo  :  "+periodo+"                               *",LogWriter.INFO);
            logWriter.log("*  FechaDoc :  "+fechadoc+"                              *",LogWriter.INFO);
            logWriter.log("**********************************************************",LogWriter.INFO);
            
            
            modelOperation.LogProcesosSvc.InsertProceso("Generacion Comprobantes", this.hashCode(), "Generacion Comprobantes Redistribucion", usuario);
            
            //BUSCO LOS PLAREM QUE HAN SIDO CAMBIADOS.
            model.comprobanteService.buscarListaPlarem();
            Vector plas = model.comprobanteService.getVector();
            
            logWriter.log("Se encontraron "+plas.size()+" registros para contabilizar",LogWriter.INFO);
            
            int nosubidas=0;
            int subidas=0;
            
            for(int i =0; i<plas.size();i++){
                Comprobantes c = (Comprobantes) plas.elementAt(i);
                
                model.comprobanteService.buscarPlanilla(c.getNumdoc());
                Vector redist = model.comprobanteService.getVector();
                
                String cuenta ="";
                
                for(int k = 0; k <redist.size();k++){
                    
                    c =  (Comprobantes) redist.elementAt(k);
                    cuenta = c.getCuenta();
                    c.setMoneda(moneda);
                    
                    
                    if(!c.getMoneda().equals("") ){
                        
                        c.setPeriodo(periodo);
                        c.setFechadoc(fechadoc);
                        c.setAprobador(usuario);
                        c.setDstrct("FINV");
                        c.setTipodoc(tipodoc);
                        c.setDocumento_interno("001");
                        c.setTipo_operacion("RPL");
                        c.setDetalle("CONTABILIZACION REDISTRIBUCION ");
                        model.comprobanteService.buscarPlarem(c.getNumdoc());
                        Vector det = model.comprobanteService.getVector();
                        
                        c.setTotal_items(det.size()+1);
                        double valorPla = c.getTotal_credito();
                        int porcent=0;
                        if(this.validarItems(det,cuenta,c.getTipo_valor()) && det.size()>0 && validarPorcent(det)){
                            
                            subidas++;
                            
                            //    System.out.println("Voy a buscar los valores");
                            Vector comprodetDebito =this.buscarValoresDebito(det, valorPla);
                            Vector comprodetCredito =this.buscarValoresCredito(det, valorPla);
                            
                            
                            double[] valores= buscarTotales(comprodetDebito,comprodetCredito);
                            
                            c.setTotal_credito(com.tsp.util.Util.redondear(valores[0],0));
                            c.setTotal_debito(com.tsp.util.Util.redondear(valores[1],0));
                            String desc = c.getTipo_valor().equals("P")?"PLANILLA ":"MOVPLA ";
                            
                            int gtransaccion = model.comprobanteService.getGrupoTransaccion();
                            c.setGrupo_transaccion(gtransaccion);
                            
                            model.comprobanteService.setComprobante(c);
                            consultas.add(model.comprobanteService.insert());
                            
                            
                            
                            int valorTotalDebito=0;
                            int valorTotalCredito=0;
                            String numrem ="";
                            if(comprodetDebito.size()>0){
                                
                                //PRIMERO SE GUARDAN LAS CANTIDADES DE DEBITO
                                for(int j =0; j<comprodetDebito.size()-1;j++){
                                    
                                    Comprobantes detalle = (Comprobantes) comprodetDebito.elementAt(j);
                                    numrem = detalle.getNumrem();
                                    porcent = detalle.getPorcent();
                                    
                                    
                                    //    System.out.println("NUMREM "+detalle.getNumrem());
                                    //     System.out.println("PORCENT  "+detalle.getPorcent());
                                    c.setDetalle("CONT REDIST REMESA "+numrem);
                                    
                                    double totalDebito = com.tsp.util.Util.redondear(detalle.getTotal_debito(),0);
                                    int valorTruncadoDebito = (int) totalDebito;
                                    
                                    valorTotalDebito=valorTotalDebito+valorTruncadoDebito;
                                    
                                    c.setTotal_debito(valorTruncadoDebito);
                                    c.setTotal_credito(0);
                                    
                                    if(c.getTipo_valor().equals("P")){
                                        c.setCuenta(detalle.getAccount_code_c());
                                    }
                                    else{
                                        String cuentaDet = detalle.getAccount_code_c().substring(0,detalle.getAccount_code_c().length()-4)+cuenta;
                                        c.setCuenta(cuentaDet);
                                    }
                                    c.setNumrem(numrem);
                                    c.setPorcent(porcent);
                                    model.comprobanteService.setComprobante(c);
                                    consultas.add(model.comprobanteService.insertItem());
                                    consultas.add(model.comprobanteService.marcarRedistribucion());
                                }
                                
                                Comprobantes detalle = (Comprobantes) comprodetDebito.lastElement();
                                numrem = detalle.getNumrem();
                                
                                porcent = detalle.getPorcent();
                                
                                // System.out.println("NUMREM "+detalle.getNumrem());
                                // System.out.println("PORCENT  "+detalle.getPorcent());
                                c.setDetalle("CONT REDIST REMESA "+numrem);
                                
                                double sobranteDebito =0;
                                
                                if(valorTotalDebito>detalle.getTotal_debito()){
                                    sobranteDebito =valores[1]- valorTotalDebito;
                                }
                                else{
                                    sobranteDebito =detalle.getTotal_debito();
                                }
                                
                                c.setTotal_debito(com.tsp.util.Util.redondear(sobranteDebito,0));
                                c.setTotal_credito(0);
                                if(c.getTipo_valor().equals("P")){
                                    c.setCuenta(detalle.getAccount_code_c());
                                }
                                else{
                                    String cuentaDet = detalle.getAccount_code_c().substring(0,detalle.getAccount_code_c().length()-4)+cuenta;
                                    c.setCuenta(cuentaDet);
                                    
                                }
                                
                                c.setNumrem(numrem);
                                //c.setDetalle("CONTABILIZACION REDISTRIBUCION REMESA "+numrem);
                                c.setPorcent(porcent);
                                model.comprobanteService.setComprobante(c);
                                consultas.add(model.comprobanteService.insertItem());
                                consultas.add(model.comprobanteService.marcarRedistribucion());
                                
                            }
                            if(comprodetCredito.size()>0){
                                //   System.out.println("LEEMOS EL VECTOR DE CREDITOS...");
                                //PRIMERO SE GUARDAN LAS CANTIDADES DE CREDITO
                                for(int j =0; j<comprodetCredito.size()-1;j++){
                                    //    System.out.println("ENTRE EN EL VECTOR DE CREDITOS...");
                                    
                                    Comprobantes detalle = (Comprobantes) comprodetCredito.elementAt(j);
                                    numrem = detalle.getNumrem();
                                    porcent = detalle.getPorcent();
                                    double totalCredito = com.tsp.util.Util.redondear(detalle.getTotal_credito(),0);
                                    
                                    //  System.out.println("NUMREM "+detalle.getNumrem());
                                    //  System.out.println("PORCENT  "+detalle.getPorcent());
                                    
                                    c.setDetalle("CONT REDIST REMESA "+numrem);
                                    
                                    int valorTruncadoCredito = (int) totalCredito;
                                    //  System.out.println("VALOR A GUARDAR ..."+valorTruncadoCredito);
                                    
                                    valorTotalCredito=valorTruncadoCredito+valorTotalCredito;
                                    //  System.out.println("ACUMULADO ..."+valorTotalCredito);
                                    
                                    c.setTotal_debito(0);
                                    c.setTotal_credito(valorTruncadoCredito);
                                    
                                    if(c.getTipo_valor().equals("P")){
                                        c.setCuenta(detalle.getAccount_code_c());
                                    }
                                    else{
                                        String cuentaDet = detalle.getAccount_code_c().substring(0,detalle.getAccount_code_c().length()-4)+cuenta;
                                        c.setCuenta(cuentaDet);
                                        
                                    }
                                    c.setNumrem(numrem);
                                    c.setPorcent(porcent);
                                    model.comprobanteService.setComprobante(c);
                                    consultas.add(model.comprobanteService.insertItem());
                                    consultas.add(model.comprobanteService.marcarRedistribucion());
                                    
                                }
                                Comprobantes detalle = (Comprobantes) comprodetCredito.lastElement();
                                numrem = detalle.getNumrem();
                                
                                // System.out.println("NUMREM "+detalle.getNumrem());
                                // System.out.println("PORCENT  "+detalle.getPorcent());
                                c.setDetalle("CONT REDIST REMESA "+numrem);
                                
                                porcent = detalle.getPorcent();
                                double sobranteCredito =0;
                                if(valorTotalCredito>detalle.getTotal_credito()){
                                    sobranteCredito =valores[0]-valorTotalCredito;
                                }
                                else{
                                    sobranteCredito =detalle.getTotal_credito();
                                }
                                c.setTotal_debito(0);
                                c.setTotal_credito(com.tsp.util.Util.redondear(sobranteCredito,0));
                                if(c.getTipo_valor().equals("P")){
                                    c.setCuenta(detalle.getAccount_code_c());
                                }
                                else{
                                    String cuentaDet = detalle.getAccount_code_c().substring(0,detalle.getAccount_code_c().length()-4)+cuenta;
                                    c.setCuenta(cuentaDet);
                                    
                                }
                                c.setNumrem(numrem);
                                c.setPorcent(porcent);
                                model.comprobanteService.setComprobante(c);
                                consultas.add(model.comprobanteService.insertItem());
                                consultas.add(model.comprobanteService.marcarRedistribucion());
                                
                            }
                            
                        }
                        
                        else{
                            nosubidas++;
                            if(!this.validarItems(det,cuenta,c.getTipo_valor()))
                                logWriter.log(c.getNumdoc()+"  "+"No se encontro Account_code_c ",LogWriter.ERROR);
                            //  System.out.println(c.getNumdoc()+"  "+"No se encontro Account_code_c ");
                            if(!validarPorcent(det)){
                                if(!isPorcentCero(det)){
                                    logWriter.log(c.getNumdoc()+"  "+"La suma de porcentajes es distinta de 100",LogWriter.ERROR);
                                    //   System.out.println(c.getNumdoc()+"  "+"La suma de porcentajes es distinta de 100 ");
                                }
                            }
                        }
                    }
                    else{
                        if(c.isTieneProv())
                            logWriter.log(c.getNumdoc()+"  "+"No se encontro proveedor",LogWriter.ERROR);
                        if (c.getTotal_credito()<=0)
                            logWriter.log(c.getNumdoc()+"  "+"La planilla tiene valor en 0 ",LogWriter.ERROR);
                        if(c.getMoneda().equals(""))
                            logWriter.log(c.getNumdoc()+"  "+"La planilla no tiene definida una moneda",LogWriter.ERROR);
                        
                        if(c.getCmc().equals(""))
                            logWriter.log(c.getNumdoc()+"  "+"El proveedor no tiene numero de cuenta",LogWriter.ERROR);
                        
                        nosubidas++;
                    }
                }
            }
            
            com.tsp.operation.model.Model modelo =  new com.tsp.operation.model.Model();
            modelo.despachoService.insertar(consultas);
            
            java.util.Date fin = new java.util.Date();
            String fechaFin= fechaHora.format(fin);
            
            //ENVIAMOS EMAIL DE LA CONTABILIZACION
            String mailInt="";
            String mailExt="";
            boolean swInt=false;
            boolean swExt=false;
            LinkedList lista = modelOperation.tablaGenService.obtenerInfoTablaGen("EMAIL", "contabpla");
            if(lista!=null){
                Iterator it = lista.iterator();
                while(it.hasNext()){
                    TablaGen t = (TablaGen)it.next();
                    System.out.println("-"+t.getDescripcion());
                    
                    if(t.getReferencia().equals("I")){
                        //verifico si exsite una arroba en la descripcion
                        if( t.getDescripcion().indexOf("@") != -1){
                            mailInt += t.getDescripcion()+";";
                        }
                        //se obtiene el login del usuario y se busca el email
                        else{
                            modelOperation.usuarioService.searchUsuario( t.getDescripcion().toUpperCase() );
                            Usuario u = modelOperation.usuarioService.getUsuario();
                            if(!u.getEmail().equals(""))
                                mailInt += u.getEmail()+";";
                        }
                        swInt = true;
                    }
                    //si el email del cliente es externo
                    else{
                        //verifico si exsite una arroba en la descripcion
                        if( t.getDescripcion().indexOf("@") != -1){
                            mailExt += t.getDescripcion()+";";
                        }
                        else{
                            //se obtiene el login del usuario y se busca el email
                            modelOperation.usuarioService.searchUsuario( t.getDescripcion().toUpperCase() );
                            Usuario u = modelOperation.usuarioService.getUsuario();
                            if(!u.getEmail().equals(""))
                                mailExt += u.getEmail()+";";
                        }
                        swExt = true;
                    }
                }
                
                System.out.println("interno "+mailInt);
                System.out.println("externo "+mailExt);
                
                com.tsp.operation.model.beans.SendMail email = new com.tsp.operation.model.beans.SendMail();
                email.setEmailfrom("procesos@sanchezpolo.com");
                email.setSendername( "PROCESO CONTABILIDAD" );
                email.setRecstatus("A");
                email.setEmailcode("");
                email.setEmailcopyto("");
                email.setEmailsubject("CONTABILIZACION REDISTRIBUCION PLANILLA");
                email.setEmailbody("SE GENERO LA CONTABILIZACION DE LA REDISTIBUCION DE UNA PLANILLA EN UN PROCESO AUTOMATICO \n\n" +
                "Redistribuciones Contabilizadas:    "+subidas+
                "\nRedistribuciones NO contabilizadas: " +nosubidas+
                "\n\nFECHA INICIO:    "+ fechaInicio
                +"\nFECHA FINAL      "+ fechaFin+
                "\n\nProceso finalizado con exito" +
                "\n\n\nNOTA: Para mas informacion a cerca de las redistribuciones no contabilizadas consulte" +
                " el log de contabilizacion.");
                if(swInt==true){
                    mailInt = mailInt.substring(0, mailInt.length() - 1 );
                    email.setEmailto(mailInt);
                    email.setTipo("I");
                    modelOperation.sendMailService.sendMail(email);
                }
                if(swExt==true){
                    mailExt = mailExt.substring(0, mailExt.length() - 1 );
                    email.setEmailto(mailExt);
                    email.setTipo("E");
                    modelOperation.sendMailService.sendMail(email);
                }
                
            }
            logWriter.log("FIN DEL PROCESO ",LogWriter.INFO);
            logWriter.log("Redistribuciones Contabilizadas:    "+subidas,LogWriter.INFO);
            logWriter.log("Redistribuciones NO contabilizadas: " +nosubidas,LogWriter.INFO);
            
            modelOperation.LogProcesosSvc.finallyProceso("Generacion Comprobantes", this.hashCode(), usuario, "PROCESO EXITOSO");
            
        }catch(Exception e){
            
            try{
                modelOperation.LogProcesosSvc.finallyProceso("Generacion Comprobantes", this.hashCode(),usuario,"ERROR :" + e.getMessage());
            }catch ( SQLException ex) {
                System.out.println("Error guardando el proceso");
            }
            e.printStackTrace();
        }
    }
    
    public static void main(String a [])throws Exception{
        try{
            HContabilizacionRedistribucion hilo = new HContabilizacionRedistribucion();
            hilo.start("ADMIN","RPL","PES");
        }catch(SQLException e){
            System.out.println("Error "+e.getMessage());
        }
    }
    public boolean validarItems(Vector det, String cuenta, String tipo){
        boolean sw=true;
        try{
            com.tsp.finanzas.contab.model.Model model = new com.tsp.finanzas.contab.model.Model();
            
            for(int j =0; j<det.size();j++){
                
                Comprobantes detalle = (Comprobantes) det.elementAt(j);
                String cuentaDet="";
                
                if(detalle.getAccount_code_c().equals("")){
                    sw=false;
                }
                else{
                    
                    cuentaDet = detalle.getAccount_code_c();
                }
                
                model.planDeCuentasService.searchCuenta("FINV",cuentaDet);
                if(model.planDeCuentasService.getVec_cuentas()!=null){
                    if(model.planDeCuentasService.getVec_cuentas().size()<=0){
                        sw=false;
                    }
                    
                }
                
                
                
                
            }
            
        }catch(SQLException e){
            System.out.println("Error "+e.getMessage());
        }
        return sw;
    }
    
    public boolean validarPorcent(Vector det){
        boolean sw=true;
        
        com.tsp.finanzas.contab.model.Model model = new com.tsp.finanzas.contab.model.Model();
        int porcent = 0;
        for(int j =0; j<det.size();j++){
            
            Comprobantes detalle = (Comprobantes) det.elementAt(j);
            porcent = porcent + detalle.getPorcent();
            
        }
        if(porcent!=100){
            sw= false;
        }
        
        return sw;
    }
    public boolean isPorcentCero(Vector det){
        boolean sw=true;
        
        com.tsp.finanzas.contab.model.Model model = new com.tsp.finanzas.contab.model.Model();
        int porcent = 0;
        for(int j =0; j<det.size();j++){
            
            Comprobantes detalle = (Comprobantes) det.elementAt(j);
            porcent = porcent + detalle.getPorcent();
            
        }
        if(porcent==0){
            sw= true;
        }
        
        return sw;
    }
    public Vector buscarValoresDebito(Vector det, double vlrpla){
        
        // System.out.println("Verificar "+det.size()+" registros");
        Vector vec =new Vector();
        
        for(int j =0; j<det.size();j++){
            
            Comprobantes detalle = (Comprobantes) det.elementAt(j);
            
            if(detalle.getPorcent()!= detalle.getPorcent_contabilizado()){
                
                //System.out.println("Cuenta "+detalle.getAccount_code_c());
                
                double valorNuevo = (vlrpla * detalle.getPorcent())/100;
                double valorViejo = (vlrpla * detalle.getPorcent_contabilizado())/100;
                
                double diferencia = valorNuevo - valorViejo;
                
                if(diferencia>0){
                    detalle.setTotal_debito(diferencia);
                    detalle.setTotal_credito(0);
                    vec.add(detalle);
                }
                
            }
            
        }
        return vec;
    }
    
    
    public Vector buscarValoresCredito(Vector det, double vlrpla){
        
        Vector vec =new Vector();
        
        for(int j =0; j<det.size();j++){
            
            Comprobantes detalle = (Comprobantes) det.elementAt(j);
            
            if(detalle.getPorcent()!= detalle.getPorcent_contabilizado()){
                
                //System.out.println("Cuenta "+detalle.getAccount_code_c());
                
                double valorNuevo = (vlrpla * detalle.getPorcent())/100;
                double valorViejo = (vlrpla * detalle.getPorcent_contabilizado())/100;
                
                double diferencia = valorNuevo - valorViejo;
                
                if(diferencia<0){
                    detalle.setTotal_credito(diferencia*-1);
                    detalle.setTotal_debito(0);
                    vec.add(detalle);
                }
                
                
            }
            
        }
        return vec;
    }
    public double[] buscarTotales(Vector detDebito,Vector detCredito){
        
        double[] valores = {0,0};
        double valorCredito=0;
        double valorDebito=0;
        
        for(int j =0; j<detDebito.size();j++){
            
            Comprobantes detalle = (Comprobantes) detDebito.elementAt(j);
            valorDebito=valorDebito+detalle.getTotal_debito();
            
        }
        
        for(int j =0; j<detCredito.size();j++){
            
            Comprobantes detalle = (Comprobantes) detCredito.elementAt(j);
            valorCredito=valorCredito+detalle.getTotal_credito();
            
        }
        
        valores[0]=valorCredito;
        valores[1]=valorDebito;
        return valores;
    }
    
}
