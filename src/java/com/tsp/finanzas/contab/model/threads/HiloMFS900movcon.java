/********************************************************************
 *  Nombre Clase.................   HiloMFS900movcon.java
 *  Descripci�n..................   Hilo que toma informacion de la tabla MFS900
 *                                  para luego guardarla en la tabla movcon
 *  Autor........................   Ing. Leonardo Parody
 *  Fecha........................   09.02.2006
 *  Versi�n......................   1.0
 *  Copyright....................   Transportes Sanchez Polo S.A.
 *  Modificado...................   egonzalez2014
 *******************************************************************/

package com.tsp.finanzas.contab.model.threads;

import java.io.*;
import com.tsp.operation.model.*;
import java.util.*;
import com.tsp.util.*;
import com.tsp.util.connectionpool.PoolManager;
import javax.servlet.*;

/**
 *
 * @author  EQUIPO12
 */
public class HiloMFS900movcon extends Thread{
     //private String id;
     private String des;
     private String procesoName;
     public com.tsp.finanzas.contab.model.Model modelcontab;
     private String dstrct;
     private String base;
     private String user;
     private String processdate;
     private List processDates;
     private String patch;
     private String inicializa;
     private String check;
     private String a�omes;
     
     public void start(String User, String Dstrct, String Base, List ProcessDates, String Patch, Object modelcon, String Inicializa, String Check, String A�omes){
        //this.id = user;
        this.procesoName = "traslado de registros de movimientos contables de la tabla MSF900 a la tabla movcon";
        this.des = "Traslada los movimientos contables de la tabla MSF900 a la tabla movcon";
        this.base = Base;
        this.dstrct = Dstrct;
        this.user = User;
        this.processDates = ProcessDates;
        this.patch = Patch;
        this.modelcontab = (com.tsp.finanzas.contab.model.Model)modelcon;
        this.inicializa = Inicializa;
        this.check = Check;
        this.a�omes = A�omes;
        super.start();
    }
    
     public synchronized void run(){
        
//        try{
//            
//            Model model = new Model();          
//            //model.LogProcesosSvc.InsertProceso(this.procesoName, this.hashCode(), des, this.id);
//            model.LogProcesosSvc.InsertProceso(this.procesoName, this.hashCode(), des, this.user);
//            if ( ! this.check.equalsIgnoreCase( "checked" ) ) {
//                
//                modelcontab.tablamovconservice.InsertMSF900movcon( this.processDates, this.dstrct, this.base, this.user ) ; 
//                modelcontab.tablamovconservice.InsertPtoGastosAdmin( this.dstrct, this.base, this.user, this.a�omes, this.check ) ; 
//                
//                try{
//                    //lineas para guardar la ultima actualizacion de la base de datos
//                    Properties movcon = new Properties();
//                    InputStream is = PoolManager.getInstance().getClass().getResourceAsStream("com/tsp/util/connectionpool/tablamovcon.properties");
//                    Calendar fechaproceso = modelcontab.tablamovconservice.getFechadeinicioProceso();
//                    if ((this.check.equalsIgnoreCase("checked")&&(this.processDates.size()>0))||((this.check.equalsIgnoreCase("unchecked"))&&(this.processDates.size()>0))) {
//                        movcon.setProperty("fechaultimoproceso", Util.crearStringFecha(fechaproceso));
//                        File file = new File(this.patch,"tablamovcon.properties");
//                        FileOutputStream movstream = new FileOutputStream(file);
//                        movcon.store(movstream,"fechaultimoproceso");
//                        movstream.close();
//                    }
//                    //lineas para setear la fecha de hoy para nopermitir que repitan el movimiento del dia de hoy.
//                    String Fecha = Util.getFechaActual_String(6);
//                    int year=Integer.parseInt(Fecha.substring(0,4));
//                    int month=Integer.parseInt(Fecha.substring(5,7));
//                    int date= Integer.parseInt(Fecha.substring(8,10));
//                    Calendar fech = Calendar.getInstance();
//                    fech.set(year,month,date,0,0,0);
//                    try{
//                        modelcontab.tablamovconservice.setFechadehoy(fech);
//                    }catch (Exception e){
//                        e.printStackTrace();
//                        throw new ServletException(e.getMessage());
//                    }
//                }
//                catch (Exception e){
//                    e.printStackTrace();
//                    throw new ServletException(e.getMessage());
//                    
//                }
//                
//            }else if ( this.check.equalsIgnoreCase( "checked" ) ) {
//                
//                String mesactual = "" + Util.MesActual() ;
//                String a�oactual = "" + Util.AnoActual() ;
//                if ( mesactual.length() == 1 ) {
//                    mesactual = "0" + mesactual ;
//                }
//                String periodoactual = a�oactual + mesactual ;
//                
//                
//                if ( this.inicializa.equalsIgnoreCase( "pto" ) && ( ! a�omes.equalsIgnoreCase( periodoactual ) ) ) {
//                    ////////System.out.println("inicializo presgastosadmin");
//                    ////////System.out.println("inserto valores ejecutados por periodos");
//                    modelcontab.tablamovconservice.InicializaPresGastosAdmin( this.dstrct, this.base, this.user, this.a�omes) ;
//                    modelcontab.tablamovconservice.InsertValoresejecutadosPorPeriodos( this.dstrct, this.user, this.a�omes, this.base ) ;
//                    
//                } else if ( this.inicializa.equalsIgnoreCase( "pto" ) && ( a�omes.equalsIgnoreCase( periodoactual ) ) ) {
//                    
//                    if ( this.processDates.size() > 0  ) {
//                        ////////System.out.println("inserto de msf900 a movcon");
//                        ////////System.out.println("inicializo presgastosadmin");
//                       // //////System.out.println("insertovalores ejecutados por periodo");
//                        modelcontab.tablamovconservice.InsertMSF900movcon(this.processDates, this.dstrct, this.base, this.user ) ;
//                        modelcontab.tablamovconservice.InicializaPresGastosAdmin( this.dstrct, this.base, this.user, this.a�omes ) ;
//                        modelcontab.tablamovconservice.InsertValoresejecutadosPorPeriodos( this.dstrct, this.user, this.a�omes, base ) ;
//                        
//                        try{
//                            //lineas para guardar la ultima actualizacion de la base de datos
//                            Properties movcon = new Properties();
//                            InputStream is = PoolManager.getInstance().getClass().getResourceAsStream("com/tsp/util/connectionpool/tablamovcon.properties");
//                            Calendar fechaproceso = modelcontab.tablamovconservice.getFechadeinicioProceso();
//                            if ((this.check.equalsIgnoreCase("checked")&&(this.processDates.size()>0))||((this.check.equalsIgnoreCase("unchecked"))&&(this.processDates.size()>0))) {
//                                movcon.setProperty("fechaultimoproceso", Util.crearStringFecha(fechaproceso));
//                                File file = new File(this.patch,"tablamovcon.properties");
//                                FileOutputStream movstream = new FileOutputStream(file);
//                                movcon.store(movstream,"fechaultimoproceso");
//                                movstream.close();
//                            }
//                            //lineas para setear la fecha de hoy para nopermitir que repitan el movimiento del dia de hoy.
//                            String Fecha = Util.getFechaActual_String(6);
//                            int year=Integer.parseInt(Fecha.substring(0,4));
//                            int month=Integer.parseInt(Fecha.substring(5,7))-1;
//                            int date= Integer.parseInt(Fecha.substring(8,10));
//                            Calendar fech = Calendar.getInstance();
//                            fech.set(year,month,date,0,0,0);
//                            try{
//                                modelcontab.tablamovconservice.setFechadehoy(fech);
//                            }catch (Exception e){
//                                e.printStackTrace();
//                                throw new ServletException(e.getMessage());
//                            }
//                        }
//                        catch (Exception e){
//                            e.printStackTrace();
//                            throw new ServletException(e.getMessage());
//                            
//                        }
//                        
//                    } else {
//                        ////////System.out.println("inicializo presgastos de admin");
//                        ////////System.out.println("inserto valores ejecutados por periodo");
//                        modelcontab.tablamovconservice.InicializaPresGastosAdmin( this.dstrct, this.base, this.user, this.a�omes ) ;
//                        modelcontab.tablamovconservice.InsertValoresejecutadosPorPeriodos( this.dstrct, this.user, this.a�omes, base ) ;
//                        
//                    }
//                    
//                } else if ( ( this.inicializa.equalsIgnoreCase( "movcon" ) ) && ( ! a�omes.equalsIgnoreCase( periodoactual ) ) ) {
//                    
//                     LinkedList processdatesactualizar = new LinkedList() ;
//                    String Fecha = Util.getFechaActual_String(6);
//                    
//                    int year=Integer.parseInt(this.a�omes.substring(0,4));
//                    int month=Integer.parseInt(this.a�omes.substring(4,6));
//                    
//                    String nextperiodo = String.valueOf( UtilFinanzas.getNextPeriodoNormal( this.a�omes, 1 ) );
//                    
//                    int year1=Integer.parseInt(nextperiodo.substring(0,4));
//                    int month1=Integer.parseInt(nextperiodo.substring(4,6));
//                    //////System.out.println("periodo = "+this.a�omes+"  periodo sig = "+nextperiodo);
//                    
//                    Date FechaInicial = new Date(year+"/"+month+"/01");
//                    Date FechaFinal = new Date(year1+"/"+month1+"/01");
//                    
//                    //Diferencia de dias entre el ultimo proceso y el nuevo proceso
//                    long difdias = (long) ( FechaFinal.getTime() - FechaInicial.getTime() ) / 1000 / 60 /60 / 24;
//                    
//                    //base inicial de process_date
//                    Date FechaFija = new Date("1999/05/01");
//                    long processdatebase = 7060;
//                    long processdatenumber = 0;
//                    //Diferencia de dias entre la fecha base y la fecha inicial
//                    long dias =  (long) (FechaInicial.getTime() - FechaFija.getTime()) / 1000 / 60 /60 / 24;
//                    
//                    for (int i=0; i<difdias; i++) {
//                        processdatenumber = processdatebase + dias + i;
//                        String Processdate=""+processdatenumber;
//                        processdatesactualizar.add(Processdate);
//                        //////System.out.println("Process dates agregados = "+Processdate+ "   tama�o processdates = "+processdatesactualizar.size());
//                    }
//                    
//                    ////////System.out.println("inicializo movcon");
//                    ////////System.out.println("inicializo pres gastos admin");
//                    ////////System.out.println("inserto msf900 en movcon");
//                    ////////System.out.println("inserto valores ejecutados por periodos");
//                    modelcontab.tablamovconservice.InicializarMovcon( this.a�omes ) ;
//                    modelcontab.tablamovconservice.InicializaPresGastosAdmin( this.dstrct, this.base, this.user, this.a�omes ) ;
//                    modelcontab.tablamovconservice.InsertMSF900movcon( processdatesactualizar, this.dstrct, this.base, this.user ) ;
//                    modelcontab.tablamovconservice.InsertValoresejecutadosPorPeriodos( this.dstrct, this.user, this.a�omes, this.base ) ;
//                    
//                } else if ( ( this.inicializa.equalsIgnoreCase( "movcon" ) ) && ( a�omes.equalsIgnoreCase( periodoactual ) ) ) {
//                    
//                    
//                    LinkedList processdatesactualizar = new LinkedList() ;
//                    String Fecha = Util.getFechaActual_String(6);
//                    
//                    int year=Integer.parseInt(Fecha.substring(0,4));
//                    int month=Integer.parseInt(Fecha.substring(5,7));
//                    int date= Integer.parseInt(Fecha.substring(8,10));
//                    
//                    Date FechaInicial = new Date(year+"/"+month+"/01");
//                    Date FechaFinal = new Date(year+"/"+month+"/"+date);
//                    
//                    //Diferencia de dias entre el ultimo proceso y el nuevo proceso
//                    long difdias = (long) ( FechaFinal.getTime() - FechaInicial.getTime() ) / 1000 / 60 /60 / 24;
//                    
//                    //base inicial de process_date
//                    Date FechaFija = new Date("1999/05/01");
//                    long processdatebase = 7060;
//                    long processdatenumber = 0;
//                    //Diferencia de dias entre la fecha base y la fecha inicial
//                    long dias =  (long) (FechaInicial.getTime() - FechaFija.getTime()) / 1000 / 60 /60 / 24;
//                    
//                    for (int i=0; i<difdias; i++) {
//                        processdatenumber = processdatebase + dias + i;
//                        String Processdate=""+processdatenumber;
//                        processdatesactualizar.add(Processdate);
//                        ////////System.out.println("Process dates agregados = "+Processdate+ "   tama�o processdates = "+processdatesactualizar.size());
//                    }
//                
//                    
//                    
//                    ////////System.out.println("inicializomovcon");
//                    ////////System.out.println("inicializo presgastosadmin");
//                    ////////System.out.println("inserto msf900 a movcon");
//                    ////////System.out.println("insert valores ejecutados");
//                    modelcontab.tablamovconservice.InicializarMovcon( this.a�omes ) ;
//                    modelcontab.tablamovconservice.InicializaPresGastosAdmin( this.dstrct, this.base, this.user, this.a�omes ) ;
//                    modelcontab.tablamovconservice.InsertMSF900movcon( processdatesactualizar, this.dstrct, this.base, this.user ) ;
//                    modelcontab.tablamovconservice.InsertValoresejecutadosPorPeriodos( this.dstrct, this.user, this.a�omes, this.base ) ;
//                    ////////System.out.println("ya termine");
//
//                    
//                    if ( ( this.processDates.size() > 0 ) ) {
//                        
//                        try{
//                            //lineas para guardar la ultima actualizacion de la base de datos
//                            Properties movcon = new Properties();
//                            InputStream is = PoolManager.getInstance().getClass().getResourceAsStream("com/tsp/util/connectionpool/tablamovcon.properties");
//                            Calendar fechaproceso = modelcontab.tablamovconservice.getFechadeinicioProceso();
//                            if ((this.check.equalsIgnoreCase("checked")&&(this.processDates.size()>0))||((this.check.equalsIgnoreCase("unchecked"))&&(this.processDates.size()>0))) {
//                                movcon.setProperty("fechaultimoproceso", Util.crearStringFecha(fechaproceso));
//                                File file = new File(this.patch,"tablamovcon.properties");
//                                FileOutputStream movstream = new FileOutputStream(file);
//                                movcon.store(movstream,"fechaultimoproceso");
//                                movstream.close();
//                            }
//                            //lineas para setear la fecha de hoy para nopermitir que repitan el movimiento del dia de hoy.
//                            String Fecha1 = Util.getFechaActual_String(6);
//                            int year1=Integer.parseInt(Fecha1.substring(0,4));
//                            int month1=Integer.parseInt(Fecha1.substring(5,7))-1;
//                            int date1= Integer.parseInt(Fecha1.substring(8,10));
//                            Calendar fech = Calendar.getInstance();
//                            fech.set(year1,month1,date1,0,0,0);
//                            try{
//                                modelcontab.tablamovconservice.setFechadehoy(fech);
//                            }catch (Exception e){
//                                e.printStackTrace();
//                                throw new ServletException(e.getMessage());
//                            }
//                        }
//                        catch (Exception e){
//                            e.printStackTrace();
//                            throw new ServletException(e.getMessage());
//                            
//                        }
//                        
//                    }
//                    
//                }  
//                    
//            }
//            //model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(),this.id, "PROCESO FINALIZADO CON EXITO.");
//            model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(),this.user, "PROCESO FINALIZADO CON EXITO.");
//        }catch (Exception ex){
//                //////System.out.println("Error : " + ex.getMessage());
//                try{
//                        Model model = new Model();
//                        //model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(),this.id,"ERROR :" + ex.getMessage());
//                        model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(),this.user,"ERROR :" + ex.getMessage());
//                }
//                catch(Exception f){
//                        try{
//                                Model model = new Model();
//                                //model.LogProcesosSvc.finallyProceso(this.procesoName,this.hashCode(),this.id,"ERROR :");
//                                model.LogProcesosSvc.finallyProceso(this.procesoName,this.hashCode(),this.user,"ERROR :");
//                        }catch(Exception p){    }
//                }
//        }
//        
   }
    
}
