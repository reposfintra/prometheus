/*************************************************************
 * Nombre: LibroDiarioPDF.java
 * Descripci�n: Hilo para crear el reporte de Libro Diario y Resumen
 * Autor: Osvaldo P�rez
 * Fecha: 28 de Junio de 2006
 * Versi�n: Java 1.0
 * Copyright: Fintravalores S.A. S.A.
 **************************************************************/

/*
 * LibroDiarioPDF.java
 *
 * Created on 28 de junio de 2006, 09:28 AM
 */

package com.tsp.finanzas.contab.model.threads;

/**
 *
 * @author  Osvaldo
 */

import com.tsp.pdf.*;
import java.util.*;
import java.io.*;
import java.lang.*;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import com.tsp.operation.model.beans.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.util.*;
import com.tsp.operation.model.*;

public class LibroMayor extends Thread{
    
    
    private String ruta;
    private Usuario usuario;
    private String procesoName;
    private String des;
    private String periodo;
    private String distrito;
    private Date d;
    private Model model;
    
    /** Creates a new instance of LibroDiarioPDF */
    public LibroMayor() {
    }
    
    public void start( Usuario u, String periodo, String distrito) {
        
        this.procesoName = "Libro Mayor";
        this.des = "Reporte de Libro Mayor";
        this.usuario = u;
        this.distrito = distrito;
        this.periodo = periodo;
        this.d = new Date();
        model = new Model(u.getBd());
        super.start();
    }
    
    
    public synchronized void run(){
        LibroMayorPDF lib;
        try{
            String fecha = "";
            Util u = new Util();
            String comentario="EXITOSO";
            model.LogProcesosSvc.InsertProceso( this.procesoName, this.hashCode(),"Libro Mayor", this.usuario.getLogin() );
            
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            ruta = rb.getString("ruta");            
         
            File file = new File(ruta + "/exportar/migracion/" + usuario.getLogin() + "/");
            file.mkdirs();
            
            fecha = Util.getFechaActual_String(6);
            fecha = fecha.replaceAll("/", "-");
            fecha = fecha.replaceAll(":", "_");
            
            File pdf = new File(ruta + "/exportar/migracion/" + usuario.getLogin() + "/"+usuario.getBd()+"_LibroMayor "+fecha+".pdf");
            File xslt = new File(ruta + "/Templates/libroMayor.xsl");
            
            
            lib = new LibroMayorPDF(xslt, pdf, usuario);
            
            fecha = Utility.getDate(6);
            fecha = fecha.replaceAll("/| |:", "");
            String tabla = "libro_mayor_"+fecha;
            
            lib.generarLibroMayor(distrito, periodo, tabla);
            //lib.generarLibroMayor("FINV", "200606", "prueba");
            //////System.out.println("Fin Libro M");
            model.LogProcesosSvc.finallyProceso(this.procesoName,this.hashCode(),  this.usuario.getLogin() ,comentario);
        }catch(Exception e){
            e.printStackTrace();
            //////System.out.println(e.getMessage());
            try{
                //////System.out.println("Salida Error...");
                model.LogProcesosSvc.finallyProceso(this.procesoName,this.hashCode(),  this.usuario.getLogin() ,"ERROR Hilo: " + e.getMessage());
            }catch(Exception ex){
                ex.printStackTrace();
            }            
        }
    }
    
  /*  public static void main(String[] arg) throws Exception{
        LibroMayor lib = new LibroMayor();
        lib.run();
    }*/
}
