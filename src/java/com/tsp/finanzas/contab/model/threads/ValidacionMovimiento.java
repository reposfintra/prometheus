/*
 * Nombre        ValidacionMovimiento.java
 * Descripci�n   Verifica la integridad de las tablas de contabilidad (con)
 *               comprobante, y comprodet
 * Autor         David Pi�a L�pez
 * Fecha         16 de junio de 2006, 10:14 AM
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */

package com.tsp.finanzas.contab.model.threads;

import com.tsp.util.*;
import com.tsp.finanzas.contab.model.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.beans.*;
import com.tsp.finanzas.contab.model.beans.*;
import java.io.*;
import java.sql.*;
import java.util.*;
import java.lang.*;
import javax.servlet.jsp.*;
import javax.servlet.http.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import javax.servlet.http.*;
import com.tsp.util.LogWriter;
import com.tsp.pdf.*;

public class ValidacionMovimiento extends Thread {
     
    private   String usuario;
    private  static Usuario user;
    private   String fechaInicial;
    private   String fechaFinal;    
    java.text.SimpleDateFormat fechaHora;    
    String fechaTimestamp;
    java.text.SimpleDateFormat fechaHoraReal;
    String fechaActual;
    java.util.Date ahora;    
    ValidacionComprobantesPDF formatoPDF;
    com.tsp.finanzas.contab.model.Model model;
    com.tsp.operation.model.Model modelOperation;
    
    /**
     * Variable tipo boolean 'validar_movimiento'
     * Descripcion :    Variable que permite verificar si existen o no errores
     *                  para llevarlos al Main CierreMensualMOC.
     * @autor :         LREALES
     */
    private boolean validar_movimiento = false;
    
    /**
     * Variable tipo boolean 'validar_finalizo'
     * Descripcion :    Variable que permite verificar si finalizo o no el proceso
     *                  para llevar la informacion al Main CierreMensualMOC.
     * @autor :         LREALES
     */
    private boolean validar_finalizo = false;
    
    /** Creates a new instance of ValidacionMovimiento */
    public ValidacionMovimiento() {
    }
    
    /**
     * Establece las variables iniciales y activa el inicio del hilo
     * @param user El usuario en sesi�n
     * @param fechaInicial indica el periodo inicial
     * @param fechaFinal indica el periodo final
     * @throws Exception Si algun error ocurre en el acceso a la BD
     */
    public void start( Usuario user, Vector listaComprobantes ) throws Exception{        
        
        this.usuario = user.getLogin();
        this.fechaInicial = "";
        this.fechaFinal = "";
        
        inicializacion(user);
        Vector listaTemp = new Vector();
        for( int i = 0; i < listaComprobantes.size(); i++ ){
            Comprobantes c = (Comprobantes)listaComprobantes.get( i );
            model.comprobanteService.getComprobante( c.getTipodoc(), c.getNumdoc(), c.getGrupo_transaccion() );
            Comprobantes temp = model.comprobanteService.getComprobante();
            if( temp != null ){
                listaTemp.add( temp );
            }
        }
        model.comprobanteService.setVector( listaTemp );
        
        super.start();
    }
    
    public void start( Usuario user, String fechaInicial, String fechaFinal ) throws Exception{
        this.usuario = user.getLogin();
        this.user = user;
        this.fechaInicial = fechaInicial;
        this.fechaFinal = fechaFinal;
        
        inicializacion(user);
        model.comprobanteService.getComprobantesSinAplicacion( fechaInicial, fechaFinal );
        
        super.start();
    }
    private void inicializacion(Usuario user) throws Exception{
        model = new com.tsp.finanzas.contab.model.Model(user.getBd());
        modelOperation = new com.tsp.operation.model.Model(user.getBd());
        /*
         *Se busca la fecha actual
         */        
        ahora = new java.util.Date();
        fechaHora = new java.text.SimpleDateFormat("yyyy-MM-dd hh.mm.ss");
        fechaHoraReal = new java.text.SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        fechaTimestamp = fechaHora.format(ahora);
        fechaActual = fechaHoraReal.format(ahora);
        
        ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
        String ruta = rb.getString("ruta");
        
        //Creaci�n del archivo log del usuario para el pdf
        File file = new File( ruta + "/exportar/migracion/" + usuario );
        file.mkdirs();
                
        File xslt = new File(ruta + "/Templates/pdflog.xsl");
        File pdf = new File(ruta +"/exportar/migracion/" + usuario + "/"+user.getBd()+"_logValidacion" + fechaTimestamp + ".pdf");
        
        formatoPDF = new ValidacionComprobantesPDF( xslt, pdf );
        formatoPDF.crearCabecera();
    }
    /**
     * Metodo llamado por Java para inciciar la ejecuci�n del hilo
     */
    public void run(){        
        
        String errores = "";
        try{
            modelOperation.LogProcesosSvc.InsertProceso("Validaci�n de movimiento contable", this.hashCode(), "Validaci�n de comprobantes de movimiento contable", usuario);
            formatoPDF.agregarElementos( "INICIO PROCESO DE VALIDACI�N DE MOVIMIENTO CONTABLE. .::: USUARIO: " + usuario + " .::: FECHA Y HORA: " + Util.getFechaActual_String(6) );            
            Vector comprobantes = model.comprobanteService.getVector();            
            for( int i =0; i < comprobantes.size(); i++ ){
                Comprobantes c = (Comprobantes) comprobantes.get( i );
                double sumCredito = model.comprobanteService.getSumatoriaComprobantes( c, "valor_credito" );
                double sumDebito = model.comprobanteService.getSumatoriaComprobantes( c, "valor_debito" );
                if( c.getTotal_credito() != c.getTotal_debito() ){
                    validar_movimiento = true; // LREALES - Validar errores - CierreMensualMOC
                    errores = "";
                    errores = "Comprobante: dstrct= "+c.getDstrct()+" tipodoc= "+c.getTipodoc()+" numdoc= "+c.getNumdoc()+" grupo_transaccion= "+c.getGrupo_transaccion();
                    errores += " Causa del error: Los Totales D�bito y Cr�dito del comprobante son diferentes. Total D�bito = "+c.getTotal_debito()+" Total Cr�dito = "+c.getTotal_credito();
                    formatoPDF.agregarElementos( errores );                    
                }
                if( sumDebito != c.getTotal_debito() ){
                    validar_movimiento = true; // LREALES - Validar errores - CierreMensualMOC
                    errores = "";
                    errores = "Comprobante: dstrct= "+c.getDstrct()+" tipodoc= "+c.getTipodoc()+" numdoc= "+c.getNumdoc()+" grupo_transaccion= "+c.getGrupo_transaccion();
                    errores += " Causa del error: Los valores D�bito en el detalle no corresponden al total D�bito en el comprobante. Sumatoria D�bito Detalle = "+sumDebito+" Total D�bito Comprobante = "+c.getTotal_debito();
                    formatoPDF.agregarElementos( errores );                    
                }
                if( sumCredito != c.getTotal_credito()){
                    validar_movimiento = true; // LREALES - Validar errores - CierreMensualMOC
                    errores = "";
                    errores = "Comprobante: dstrct= "+c.getDstrct()+" tipodoc= "+c.getTipodoc()+" numdoc= "+c.getNumdoc()+" grupo_transaccion= "+c.getGrupo_transaccion();
                    errores += " Causa del error: Los valores Cr�dito en el detalle no corresponden al total Cr�dito en el comprobante. Sumatoria Cr�dito Detalle = "+sumCredito+" Total Cr�dito Comprobante = "+c.getTotal_credito();
                    formatoPDF.agregarElementos( errores );                    
                }
                if( sumDebito != sumCredito ){
                    validar_movimiento = true; // LREALES - Validar errores - CierreMensualMOC
                    errores = "";
                    errores = "ComproDet: dstrct= "+c.getDstrct()+" tipodoc= "+c.getTipodoc()+" numdoc= "+c.getNumdoc()+" grupo_transaccion= "+c.getGrupo_transaccion();
                    errores += " Causa del error: Los valores D�bito y Cr�dito en la sumatoria del detalle son diferentes. Valor Sumatoria D�bito = "+sumDebito+" Valor Sumatoria Cr�dito = "+sumCredito;
                    formatoPDF.agregarElementos( errores );                    
                }
                model.comprobanteService.getDetallesComprobante( c );
                Vector detalles = model.comprobanteService.getVector();                
                if( detalles.size() != c.getTotal_items() ){   
                    validar_movimiento = true; // LREALES - Validar errores - CierreMensualMOC
                    errores = "";
                    errores = "Comprobante: dstrct= "+c.getDstrct()+" tipodoc= "+c.getTipodoc()+" numdoc= "+c.getNumdoc()+" grupo_transaccion= "+c.getGrupo_transaccion();
                    errores += " Causa del error: El n�mero de registros en el detalle no corresponde a total_items en el comprobante. N�mero registros Detalle = "+detalles.size()+" total_items Comprobante = "+c.getTotal_items();
                    formatoPDF.agregarElementos( errores );                    
                }                
                for( int j = 0; j < detalles.size(); j++ ){
                    Comprobantes detalle = (Comprobantes) detalles.get( j );                    
                    if( c.getGrupo_transaccion() != detalle.getGrupo_transaccion() ){
                        validar_movimiento = true; // LREALES - Validar errores - CierreMensualMOC
                        errores = "";
                        errores = "ComproDet: dstrct= "+detalle.getDstrct()+" tipodoc= "+detalle.getTipodoc()+" numdoc= "+detalle.getNumdoc()+" grupo_transaccion= "+detalle.getGrupo_transaccion()+ " transaccion= "+detalle.getTransaccion();
                        errores += " Causa del error: El valor del grupo transacci�n en el detalle es diferente en el comprobante. Grupo Transacci�n Detalle = "+detalle.getGrupo_transaccion()+" Grupo Transacci�n Detalle = "+c.getGrupo_transaccion();
                        formatoPDF.agregarElementos( errores );                        
                    }
                    if( !c.getPeriodo().equalsIgnoreCase( detalle.getPeriodo() ) ){
                        validar_movimiento = true; // LREALES - Validar errores - CierreMensualMOC
                        errores = "";
                        errores = "ComproDet: dstrct= "+detalle.getDstrct()+" tipodoc= "+detalle.getTipodoc()+" numdoc= "+detalle.getNumdoc()+" grupo_transaccion= "+detalle.getGrupo_transaccion()+ " transaccion= "+detalle.getTransaccion();
                        errores += " Causa del error: El periodo en el detalle es diferente en el comprobante. Periodo Detalle = "+detalle.getPeriodo()+" Periodo Comprobante = "+c.getPeriodo();
                        formatoPDF.agregarElementos( errores );                        
                    }
                    boolean existeCuenta = model.planDeCuentasService.existCuenta( detalle.getDstrct(), detalle.getCuenta() );
                    if(!existeCuenta){
                        validar_movimiento = true; // LREALES - Validar errores - CierreMensualMOC
                        errores = "";
                        errores = "ComproDet: dstrct= "+detalle.getDstrct()+" tipodoc= "+detalle.getTipodoc()+" numdoc= "+detalle.getNumdoc()+" grupo_transaccion= "+detalle.getGrupo_transaccion()+ " transaccion= "+detalle.getTransaccion();
                        errores += " Causa del error: La cuenta del detalle no existe. Cuenta = "+detalle.getCuenta();
                        formatoPDF.agregarElementos( errores );                        
                    }                    
                    if( !detalle.getAuxiliar().equals("''") && !detalle.getAuxiliar().equals("") ){
                        String result[] = detalle.getAuxiliar().split("-");
                        if( result.length != 2 ){
                            validar_movimiento = true; // LREALES - Validar errores - CierreMensualMOC
                            errores = "";
                            errores = "ComproDet: dstrct= "+detalle.getDstrct()+" tipodoc= "+detalle.getTipodoc()+" numdoc= "+detalle.getNumdoc()+" grupo_transaccion= "+detalle.getGrupo_transaccion()+ " transaccion= "+detalle.getTransaccion();
                            errores += " Causa del error: El campo auxiliar en Detalle es inv�lido. Auxiliar = "+detalle.getAuxiliar();
                            formatoPDF.agregarElementos( errores );                            
                        }else{                            
                            model.subledgerService.buscarSubledger( detalle.getDstrct(), detalle.getCuenta(), result[0], result[1] );
                            Subledger existeSubledger = model.subledgerService.getSubledger();
                            if( existeSubledger == null ){
                                validar_movimiento = true; // LREALES - Validar errores - CierreMensualMOC
                                errores = "";
                                errores = "ComproDet: dstrct= "+detalle.getDstrct()+" tipodoc= "+detalle.getTipodoc()+" numdoc= "+detalle.getNumdoc()+" grupo_transaccion= "+detalle.getGrupo_transaccion()+ " transaccion= "+detalle.getTransaccion();
                                errores += " Causa del error: El campo auxiliar no existe como subledger. Subledger = "+detalle.getAuxiliar();
                                formatoPDF.agregarElementos( errores );                                
                            }
                        }
                            
                    }
                    boolean existeNit = modelOperation.nitService.searchNit( detalle.getTercero() ); 
                    if(!existeNit){
                        validar_movimiento = true; // LREALES - Validar errores - CierreMensualMOC
                        errores = "";
                        errores = "ComproDet: dstrct= "+detalle.getDstrct()+" tipodoc= "+detalle.getTipodoc()+" numdoc= "+detalle.getNumdoc()+" grupo_transaccion= "+detalle.getGrupo_transaccion()+ " transaccion= "+detalle.getTransaccion();
                        errores += " Causa del error: El nit almacenado en el campo tercero del detalle no existe. Nit = "+detalle.getTercero();
                        formatoPDF.agregarElementos( errores );                        
                    }
                    if( !detalle.getPeriodo().equals( "''" ) && detalle.getPeriodo().length() == 6 && !detalle.getPeriodo().equals( "" )){
                        String anio = detalle.getPeriodo().substring( 0, 4 );
                        String mes = detalle.getPeriodo().substring( 4, 6 );
                        model.periodoContableService.obtenerPeriodo( detalle.getDstrct(), anio, mes );
                        PeriodoContable pc = model.periodoContableService.getP();
                        if( pc == null ){
                            validar_movimiento = true; // LREALES - Validar errores - CierreMensualMOC
                            errores = "";
                            errores = "ComproDet: dstrct= "+detalle.getDstrct()+" tipodoc= "+detalle.getTipodoc()+" numdoc= "+detalle.getNumdoc()+" grupo_transaccion= "+detalle.getGrupo_transaccion()+ " transaccion= "+detalle.getTransaccion();
                            errores += " Causa del error: Periodo Contable no existe. Periodo = "+detalle.getPeriodo();
                            formatoPDF.agregarElementos( errores );                            
                        }else{
                            if( pc.getAc().equalsIgnoreCase( "C" ) ){
                                validar_movimiento = true; // LREALES - Validar errores - CierreMensualMOC
                                errores = "";
                                errores = "ComproDet: dstrct= "+detalle.getDstrct()+" tipodoc= "+detalle.getTipodoc()+" numdoc= "+detalle.getNumdoc()+" grupo_transaccion= "+detalle.getGrupo_transaccion()+ " transaccion= "+detalle.getTransaccion();
                                errores += " Causa del error: Periodo Contable se encuentra cerrado. Periodo = "+detalle.getPeriodo();
                                formatoPDF.agregarElementos( errores );                                
                            }
                        }
                    }   
                }                
            }
            if( comprobantes.size() == 0 ){
                formatoPDF.agregarElementos( "NO SE GENER� NINGUN ERROR EN LA VALIDACI�N");
            }
            formatoPDF.agregarElementos( "FIN PROCESO DE VALIDACI�N DE MOVIMIENTO CONTABLE EXITOSO. FECHA Y HORA:" + Util.getFechaActual_String(6) );
            modelOperation.LogProcesosSvc.finallyProceso("Validaci�n de movimiento contable", this.hashCode(), usuario, "PROCESO EXITOSO");
            formatoPDF.generarPDF();
            
        } catch( Exception e ){            
            try{
                formatoPDF.agregarElementos( "OCURRIO UN ERROR EN LA GENERACI�N DEL PDF. FECHA Y HORA:" + Util.getFechaActual_String(6) );
                modelOperation.LogProcesosSvc.finallyProceso("Validaci�n de movimiento contable", this.hashCode(),usuario,"ERROR :" + e.getMessage());                
            }catch ( SQLException ex) {
                //////System.out.println("Error guardando el proceso");
            }            
            e.printStackTrace();
            try {
                formatoPDF.generarPDF();
            }catch(Exception ex){ }
        }
        
        finally{
            
            validar_finalizo = true;
            
        }
        
    }
    
    /**
     * m�todo principal para la ejecucion del hilo de validaci�n de comprobantes 
     * de movimiento contable
     * @param args the command line arguments
     */
    public static void main(String[] args)throws Exception{
        try{            
            ValidacionMovimiento hilo = new ValidacionMovimiento(); 
            hilo.start(ValidacionMovimiento.user, "", "" );
        }catch(SQLException e){
            //////System.out.println("Error "+e.getMessage());
        }
    }
    
    /**
     * Getter for property validar_movimiento.
     * @return Value of property validar_movimiento.
     */
    public boolean isValidar_movimiento() {
        
        return validar_movimiento;
        
    }
    
    /**
     * Setter for property validar_movimiento.
     * @param validar_movimiento New value of property validar_movimiento.
     */
    public void setValidar_movimiento( boolean validar_movimiento ) {
        
        this.validar_movimiento = validar_movimiento;
        
    }
    
    /**
     * Getter for property validar_finalizo.
     * @return Value of property validar_finalizo.
     */
    public boolean isValidar_finalizo() {
        
        return validar_finalizo;
        
    }
    
    /**
     * Setter for property validar_finalizo.
     * @param validar_finalizo New value of property validar_finalizo.
     */
    public void setValidar_finalizo( boolean validar_finalizo ) {
        
        this.validar_finalizo = validar_finalizo;
        
    }
    
}