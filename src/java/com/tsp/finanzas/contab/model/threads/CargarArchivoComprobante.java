/*
 * CargarArchivoComprobante.java
 *
 * Created on 6 de julio de 2006, 10:51 AM
 */

/*
 * Nombre        CargarArchivoComprobante.java
 * Descripci�n   Proceso generacion de comprobante a partir de un archivo plano
 * Autor         David Pi�a L�pez
 * Fecha         6 de julio de 2006, 10:51 AM
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */
package com.tsp.finanzas.contab.model.threads;

import com.tsp.util.*;

import com.tsp.operation.model.beans.*;
import com.tsp.finanzas.contab.model.beans.*;
import com.tsp.finanzas.contab.model.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.beans.*;
import java.io.*;
import java.sql.*;
import java.util.*;
import java.lang.*;
import javax.servlet.jsp.*;
import javax.servlet.http.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import javax.servlet.http.*;
import com.tsp.util.LogWriter;

/**
 *
 * @author  David
 */
public class CargarArchivoComprobante extends Thread{
    private Usuario usuario;
    private com.tsp.finanzas.contab.model.Model model;
    private com.tsp.operation.model.Model modelOperation;
    private String anio;
    private String mes;    
    private Vector detalle;
    private double totalDebito;
    private double totalCredito;
    private PrintWriter pw;
    private LogWriter logWriter;
    private File archivo;
    
    /** Creates a new instance of CargarArchivoComprobante */
    public CargarArchivoComprobante() {
    }
    public void start( Usuario user, File file ) throws Exception{
        archivo = file;
        usuario = user;
        this.start();
        model = new com.tsp.finanzas.contab.model.Model(user.getBd());
        modelOperation = new com.tsp.operation.model.Model(user.getBd());        
    }
    public void run(){
        try {        
            modelOperation.LogProcesosSvc.InsertProceso("Comprobante N�mina", this.hashCode(), "Proceso Carga Archivo Comprobante N�mina", usuario.getLogin() );
            FileRead fr = new FileRead( archivo.getAbsolutePath() );
            detalle = fr.cargarArchivo();
            totalDebito = fr.getTotal_debito();
            totalCredito = fr.getTotal_credito();
            if( detalle.size() > 0 ){
                this.ingresarComprobante();
            }
            archivo.delete();
            modelOperation.LogProcesosSvc.finallyProceso("Comprobante N�mina", this.hashCode(), usuario.getLogin(), "PROCESO EXITOSO");
        }catch(Exception ex ){
            try{                
                modelOperation.LogProcesosSvc.finallyProceso("Comprobante N�mina", this.hashCode(),usuario.getLogin(),"ERROR :" + ex.getMessage()); 
            }catch ( SQLException e) {
                //////System.out.println("Error guardando el proceso");
            }            
            ex.printStackTrace();
        }
        
        
    }
    private void ingresarComprobante() throws Exception{
        String sql = "";
        
        Comprobantes comprodet = (Comprobantes)detalle.get( 0 );
        
        Comprobantes comprobante = new Comprobantes();
        comprobante.setDstrct( comprodet.getDstrct() );
        comprobante.setTipodoc( "COD" );
        comprobante.setNumdoc( comprodet.getNumdoc() );
        comprobante.setSucursal( (usuario.getId_agencia()!=null)? usuario.getId_agencia() : "" );
        comprobante.setPeriodo( comprodet.getPeriodo() );
        comprobante.setFechadoc( comprodet.getFechadoc() );
        comprobante.setDetalle( "COMPROBANTE NOMINA" );
        comprobante.setTercero( "" );
        comprobante.setTotal_credito( totalCredito );
        comprobante.setTotal_debito( totalDebito );
        comprobante.setTotal_items( detalle.size() );
        comprobante.setMoneda( obtenerMonedaDistrito() );
        comprobante.setAprobador( "ADMIN" );
        comprobante.setUsuario( (usuario.getLogin()!=null)? usuario.getLogin() : "" );
        comprobante.setBase( (usuario.getBase()!=null)? usuario.getBase() : "" );
        comprobante.setTipodoc_rel( " " );
        comprobante.setNumdoc_rel( " " );
        model.comprobanteService.setComprobante( comprobante );
        sql = model.comprobanteService.getInsert();        
        
        TransaccionService  svc = new  TransaccionService(this.usuario.getBd());
        svc.crearStatement();
        svc.getSt().addBatch( sql );
        svc.execute();
        ////////System.out.println( sql );
        
        comprobante.setGrupo_transaccion( model.comprobanteService.getGrupoTransaccion() );
        comprobante.setDocumento_interno( "COD" );
        sql = "";
        for(int k = 0; k < detalle.size(); k++ ){
            comprodet = (Comprobantes)detalle.get( k );    
            
            comprobante.setCuenta( comprodet.getCuenta() );
            comprobante.setDetalle( comprodet.getDetalle() );
            comprobante.setTotal_credito( comprodet.getTotal_credito() );
            comprobante.setTotal_debito( comprodet.getTotal_debito() );
            model.comprobanteService.setComprobante( comprobante );
            sql += model.comprobanteService.getInsertItem() + ";";                       
            
        }       
        
        svc = new  TransaccionService(this.usuario.getBd());
        svc.crearStatement();
        svc.getSt().addBatch( sql );
        svc.execute();
        
        ////////System.out.println( sql );
    }
    
    //Metodo que permite obtener el numero de documento de un periodo
    private String obtenerMonedaDistrito() throws Exception{
        String result = "";
        modelOperation.ciaService.buscarCia( usuario.getCia() );
        Compania cia = modelOperation.ciaService.getCompania();
        if( cia != null ){
            result = cia.getMoneda();
        }        
        return result;        
    }
    
}
