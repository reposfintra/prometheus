/********************************************************************************
 * Nombre clase . . . . . .   ReporteRtfeDAO.java                               *
 * Descripci�n  .. . . . . .  Aca es donde se hacen las consultas               *
 * Autor . . . . . . . . . .  Ing. Pablo Emilio Bassil Orozco                   *
 * Fecha . . . . . . . . . .  Created on 10 de Febrero de 2009                  *
 * Version . . . . . . . . .  1.0                                               *
 * Copyright ...TSP - TRANSPORTES SANCHEZ POLO S.A.                             *
 ********************************************************************************/

package com.tsp.finanzas.contab.model.DAO;

import com.tsp.finanzas.contab.model.beans.ResultadoMayorizacion;
import java.sql.*;
import com.tsp.operation.model.DAOS.MainDAO;
import java.util.Vector;

public class MayorizacionMensualDAO extends MainDAO{

    private int a�o = 0;
    private int mes = 0;
    private String usuario = "";
    private String base = "";
    private String dstrct = "";

    public MayorizacionMensualDAO(){
        super("MayorizacionMensualDAO.xml");
    }
    public MayorizacionMensualDAO(String dataBaseName){
        super("MayorizacionMensualDAO.xml", dataBaseName);
    }

    public void setDatos(int a, int m, String u, String b, String d){
        this.a�o        = a;
        this.mes        = m;
        this.usuario    = u;
        this.base       = b;
        this.dstrct     = d;
    }

    public String devolverNumeroMes(int num){
        if( String.valueOf(num).length()>1 ){
            return String.valueOf(num);
        }
        else
        {
            return ("0"+String.valueOf(num));
        }
    }

    //Esto es para el proceso de desmayorizacion
    public void actualizarComprobante() throws SQLException{
        PreparedStatement st    = null;
        Connection        con   = null;
        String            query = "SQL_ACTUALIZA_COMPROBANTE";

        try{
            con        = conectarJNDI( query );
            String sql = obtenerSQL  ( query );
            st         = con.prepareStatement( sql );
            String str = (String.valueOf(a�o) + devolverNumeroMes(mes) );
            System.out.println("str"+str+"query"+sql);
            st.setString( 1, str );
            st.executeUpdate();
        }
        catch (SQLException ex) {
            System.out.println("ex en actuacomp"+ex.toString());
            throw new SQLException("ERROR: " + ex.getMessage() + " - " + ex.getErrorCode());
        }
finally{
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
    }

    public void actualizarMayor() throws SQLException{
        Statement st = null;
        ResultSet rs = null;
        Connection con = null;
        String s = this.obtenerSQL("SQL_ACTUALIZA_MAYOR");
        String operacion = "";
        
        try{
            con = this.conectarJNDI("SQL_ACTUALIZA_MAYOR");

            s = s.replace( "xx", devolverNumeroMes(mes) );
            s = s.replace( "yy", devolverNumeroMes(mes) );

            for(int i = 1; i<mes; i++){
                operacion = operacion +" + movdeb"+devolverNumeroMes(i)+" - movcre"+devolverNumeroMes(i);
            }

            s = s.replace( "zz", operacion );
            s = s.replace( "?", String.valueOf(a�o) );
            
            st = con.createStatement();
            st.executeUpdate(s);
        }
        catch (SQLException ex) {
            System.out.println("errrorcitioxx en dao:"+ex.toString());
            throw new SQLException("ERROR: " + ex.getMessage() + " - " + ex.getErrorCode());
        }
finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
    }

    public void actualizarMayorSubledger() throws SQLException{
        Statement st = null;
        ResultSet rs = null;
        Connection con = null;
        String s = this.obtenerSQL("SQL_ACTUALIZA_MAYOR_SUBLEDGER");
        String operacion = "";

        try{
            con = this.conectarJNDI("SQL_ACTUALIZA_MAYOR_SUBLEDGER");

            s = s.replace( "xx", devolverNumeroMes(mes) );
            s = s.replace( "yy", devolverNumeroMes(mes) );

            for(int i = 1; i<mes; i++){
                operacion = operacion +" + movdeb"+devolverNumeroMes(i)+" - movcre"+devolverNumeroMes(i);
            }

            s = s.replace( "zz", operacion );
            s = s.replace( "?", String.valueOf(a�o) );
            
            st = con.createStatement();
            st.executeUpdate(s);
        }
        catch (SQLException ex) {
            System.out.println("errrorcitioww en dao:"+ex.toString());
            throw new SQLException("ERROR: " + ex.getMessage() + " - " + ex.getErrorCode());
        }finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
    }

    //Esto es para el proceso de mayorizacion
    public void correrQueries() throws SQLException{
        Statement st = null;
        ResultSet rs = null;
        Connection con = null;
        String s;

        try{
            s = this.obtenerSQL("SQL_CREAR_COMPROBANTE_POR_MAYORIZAR");
            System.out.println("s1"+s);
            con = this.conectarJNDI("SQL_CREAR_COMPROBANTE_POR_MAYORIZAR");
            st = con.createStatement();
            st.executeUpdate(s);
            //this.cerrarCon(st, con);//20100122

            s = this.obtenerSQL("SQL_CREAR_COMPROBANTE_POR_MAYORIZAR2");
            //con = this.conectarJNDI("SQL_CREAR_COMPROBANTE_POR_MAYORIZAR2");//20100122
            s = s.replace( "#dstrct#", dstrct );
            s = s.replace( "#periodo#", ( String.valueOf(a�o) + devolverNumeroMes(mes) ) );
            System.out.println("s2"+s);
            //st = con.createStatement();
            //st = con.prepareStatement( s );//20100122
            st.executeUpdate(s);
            //this.cerrarCon(st, con);//20100122

            s = this.obtenerSQL("SQL_CREAR_COMPROBANTE_POR_MAYORIZAR3");
            System.out.println("s3"+s);
            //con = this.conectarJNDI("SQL_CREAR_COMPROBANTE_POR_MAYORIZAR3");//20100122
            //st = con.createStatement();//20100122
            //st = con.prepareStatement( s );//20100122
            st.executeUpdate(s);
            //this.cerrarCon(st, con);//20100122



            s = this.obtenerSQL("SQL_CREAR_DETALLE_COMPROBANTE_POR_MAYORIZAR");
            //con = this.conectarJNDI("SQL_CREAR_DETALLE_COMPROBANTE_POR_MAYORIZAR");//20100122
            System.out.println("s4"+s);
            //st = con.createStatement();//20100122
            //st = con.prepareStatement( s );//20100122
            st.executeUpdate(s);
            //this.cerrarCon(st, con);//20100122



            s = this.obtenerSQL("SQL_ACTUALIZAR_DETALLE_COMPROBANTE_POR_MAYORIZAR");
            System.out.println("s5"+s);
            //con = this.conectarJNDI("SQL_ACTUALIZAR_DETALLE_COMPROBANTE_POR_MAYORIZAR");//20100122
            //st = con.createStatement();//20100122
            //st = con.prepareStatement( s );//20100122
            st.executeUpdate(s);
            //this.cerrarCon(st, con);//20100122



            s = this.obtenerSQL("SQL_ACTUALIZAR_COMPROBANTE_POR_MAYORIZAR");
            System.out.println("s6"+s);
            //con = this.conectarJNDI("SQL_ACTUALIZAR_COMPROBANTE_POR_MAYORIZAR");//20100122
            //st = con.createStatement();//20100122
            //st = con.prepareStatement( s );//20100122
            st.executeUpdate(s);
            //this.cerrarCon(st, con);//20100122



            s = this.obtenerSQL("SQL_INSERT_MAYOR");            
            //con = this.conectarJNDI("SQL_INSERT_MAYOR");//20100122
            s = s.replace( "#dstrct#", dstrct );
            s = s.replace( "#anio#", String.valueOf(a�o) );
            s = s.replace( "#user#", String.valueOf(usuario) );
            s = s.replace( "#base#", String.valueOf(base) );
            System.out.println("s7"+s);
            //st = con.createStatement();
            //st = con.prepareStatement( s );//20100122
            st.executeUpdate(s);
            //this.cerrarCon(st, con);//20100122



            s = this.obtenerSQL("SQL_ACTUALIZAR_MAYOR");
            
            //con = this.conectarJNDI("SQL_ACTUALIZAR_MAYOR");//20100122
            s = s.replace( "#mes#", devolverNumeroMes(mes) );
            s = s.replace( "#dstrct#", dstrct );
            s = s.replace( "#anio#", String.valueOf(a�o) );
            System.out.println("s8"+s);
            //st = con.createStatement();
            //st = con.prepareStatement( s );//20100122
            st.executeUpdate(s);
            //this.cerrarCon(st, con);//20100122

            s = this.obtenerSQL("SQL_ACTUALIZAR_COMPROBANTE");
            
            //con = this.conectarJNDI("SQL_ACTUALIZAR_COMPROBANTE");//20100122
            s = s.replace( "#user#", usuario );
            System.out.println("s9"+s);
            //st = con.createStatement();//20100122
            //st = con.prepareStatement( s );//20100122
            st.executeUpdate(s);
            //this.cerrarCon(st, con);//20100122//20100823
        }
        catch (SQLException ex) {
            System.out.println("errrorcitissso en dao:"+ex.toString());
            ex.printStackTrace();//20100122
            throw new SQLException("ERROR: " + ex.getMessage() + " - " + ex.getErrorCode());
        }
        catch(Exception ee){//20100122
            System.out.println("errorre en dao:"+ee.toString());//20100122
            ee.printStackTrace();//20100122
        }//20100122
finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
    }

    public Vector verificacion() throws SQLException{
        Statement st = null;
        ResultSet rs = null;
        Connection con = null;
        String s;
        ResultadoMayorizacion rm;
        Vector<ResultadoMayorizacion> vrm = new Vector();

        try{
            s = this.obtenerSQL("SQL_VERIFICACION_FINAL");
            con = this.conectarJNDI("SQL_VERIFICACION_FINAL");
            s = s.replace( "#anio#", String.valueOf(a�o) );
            s = s.replace( "#mes#", devolverNumeroMes(mes) );
            s = s.replace( "#dstrct#", dstrct );
            s = s.replace( "#periodo#", ( String.valueOf(a�o) + devolverNumeroMes(mes) ) );
            st = con.createStatement();
            rs = st.executeQuery(s);

            while(rs.next()){
                rm = new ResultadoMayorizacion( rs.getString("archivo"), rs.getDouble("total_debito"), rs.getDouble("total_credito"), rs.getDouble("neto") );
                vrm.add(rm);
                rm = null;//Liberar Espacio JJCastro
            }

            return vrm;
        }
        catch (SQLException ex) {
            System.out.println("errrorciticco en dao:"+ex.toString());
            throw new SQLException("ERROR: " + ex.getMessage() + " - " + ex.getErrorCode());
        }
finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
    }

    public boolean existeTabla(String nombreTabla) throws SQLException{
        boolean val = false;

        Statement st = null;
        ResultSet rs = null;
        Connection con = null;        
        String s = this.obtenerSQL("SQL_CONSULTAR_TABLA");


        try{
            con = this.conectarJNDI("SQL_CONSULTAR_TABLA");
            s = s.replace("?", nombreTabla);
            st = con.createStatement();
            rs = st.executeQuery(s);
            val = true;
        }
        catch(Exception ex){
            System.out.println("errrorcitioesese en dao:"+ex.toString());
            val = false;
        }
finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }

        return val;
    }

    public void borrarTabla(String nombreTabla) throws SQLException{

        Statement st = null;
        ResultSet rs = null;
        Connection con = null;        
        String s = this.obtenerSQL("SQL_BORRAR_TABLA");

        try{
            con = this.conectarJNDI("SQL_BORRAR_TABLA");
            s = s.replace("?", nombreTabla);
            st = con.createStatement();
            st.executeUpdate(s);

            /*
            st = con.prepareStatement(this.obtenerSQL("SQL_BORRAR_TABLA"));
            st.setString(1, nombreTabla);
            rs = st.executeQuery();     */
        }
        catch(Exception ex){            
            System.out.println("errrorcillo en dao:"+ex.toString());
        }
        
finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
    }

    public void cerrarCon(PreparedStatement st, Connection con) throws SQLException {
        if (st != null) {
            try {
                st = null;
            } catch (Exception e) {
                throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
            }
        }
        if (con != null) {
            try {
                this.desconectar(con);
            } catch (SQLException e) {
                throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
            }
        }
    }

    public void cerrarCon(Statement st, Connection con) throws SQLException {
        if (st != null) {
            try {
                st = null;
            } catch (Exception e) {
                throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
            }
        }
        if (con != null) {
            try {
                this.desconectar(con);
            } catch (SQLException e) {
                throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
            }
        }
    }
}