/*********************************************************************************
 * Nombre clase :      ComprobantesDAO.java                                      *
 * Descripcion :       DAO del programa de contabilidad                          *
 *                     Clase que maneja los DAO (Data Access Object) los cuales  *
 *                     contienen los metodos que interactuan con la B.D.         *
 * Autor :             KREALES                                                   *
 * Fecha :             5 de junio de 2006, 11:02 AM                              *
 * Version :           1.0                                                       *
 * Copyright :         Fintravalores S.A.                                   *
 *********************************************************************************/

package com.tsp.finanzas.contab.model.DAO;

import java.util.*;
import java.sql.*;
import com.tsp.finanzas.contab.model.beans.*;
import org.apache.log4j.Logger;
import com.tsp.util.*;
import java.util.Date;
import java.text.*;
import com.tsp.util.connectionpool.PoolManager;
import com.tsp.operation.model.DAOS.MainDAO;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.beans.StringStatement;


public class ComprobantesDAO extends MainDAO {

    Comprobantes comprobante;
    Vector vector;
    Vector reporte;
    /** Creates a new instance of ImprimirOrdenDeCargaDAO */
    public ComprobantesDAO() {
        super( "ComprobantesDAO.xml" );
    }
    public ComprobantesDAO(String dataBaseName) {
        super( "ComprobantesDAO.xml", dataBaseName);
    }

    /**
     *Metodo que permite obtener la todos los comprobantes con
     *no tengan fecha de aplicaci�n es decir la fecha por defecto
     *del campo
     *@param fechaInicial la fecha de inicio del comprobante
     *@param fechaFinal la fecha final del comprobante
     *@autor: David Pi�a
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void getComprobantesSinAplicacion( String fechaInicial, String fechaFinal ) throws SQLException {

        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs  =null;
        String clausula = "";
        String query = "SQL_COMPROBANTES_SIN_APLICACION";
        try {
            if( !fechaInicial.equals( "" ) && !fechaFinal.equals( "" ) ){
                clausula = "AND periodo >= " + fechaInicial + " AND periodo <= "+ fechaFinal;
            }

            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));
            String sql = st.toString();
            sql = sql.replaceAll( "#CONDICION#", clausula );
            rs = st.executeQuery( sql );
            vector = new Vector();
            while (rs.next()){
                Comprobantes comprobante = new Comprobantes();
                comprobante.setDstrct( (rs.getString("dstrct")!=null)?rs.getString("dstrct"):"" );
                comprobante.setTipodoc( (rs.getString("tipodoc")!=null)?rs.getString("tipodoc"):"" );
                comprobante.setNumdoc( (rs.getString("numdoc")!=null)?rs.getString("numdoc"):"" );
                comprobante.setGrupo_transaccion( rs.getInt("grupo_transaccion") );
                comprobante.setPeriodo( (rs.getString("periodo")!=null)?rs.getString("periodo"):"" );
                comprobante.setTercero( (rs.getString("tercero")!=null)?rs.getString("tercero"):"" );
                comprobante.setTotal_items( rs.getInt("total_items") );
                comprobante.setTotal_debito( rs.getDouble("total_debito") );
                comprobante.setTotal_credito( rs.getDouble("total_credito") );
                comprobante.setFecha_aplicacion( (rs.getString("fecha_aplicacion")!=null)?rs.getString("fecha_aplicacion"):"" );
                vector.add( comprobante );
                comprobante = null;//Liberar Espacio JJCastro
            }
        } catch( SQLException e ){
            throw new SQLException( "Error en getComprobantesSinAplicacion [ComprobantesDAO]......." + e.getMessage() + " - " + e.getErrorCode() );
        }
finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
    }
    /**
     *Metodo que permite realizar la sumatoria debito y
     *@autor: David Pi�a
     *@param comprobante El comprobante que contiene la llave para realizar la sumatoria
     *                   tambien tiene el campo al que se desea aplicar la sumatoria
     *@param campo nombre del campo que se desea aplicar la sumatoria
     *@throws: En caso de que un error de base de datos ocurra.
     *@return la sumatoria del campo dado
     */
    public double getSumatoriaComprobantes( Comprobantes comprobante, String campo ) throws SQLException {

        Connection con = null;
        PreparedStatement st = null;
        String sql = "";
        ResultSet rs = null;
        double result = 0;
        String query = "SQL_SUMATORIA_DETALLE";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query).replaceAll("#CAMPOSUMATORIA#", campo));
            st.setString(1, comprobante.getTipodoc());
            st.setString(2, comprobante.getNumdoc());
            st.setInt(3, comprobante.getGrupo_transaccion());
            //sql = st.toString();
            //sql = sql.replaceAll("#CAMPOSUMATORIA#", campo);
            rs = st.executeQuery();
            result = (rs.next()) ? rs.getDouble("total") : 0;

        } catch (SQLException e) {
            throw new SQLException("Error en getSumatoriaComprobantes [ComprobantesDAO]......." + e.getMessage() + " - " + e.getErrorCode());
        }
        finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return result;
    }



    /**
     *Metodo que inserta un comprobante
     *@autor: Karen Reales
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public String insert() throws SQLException {

        Connection con = null;
        PreparedStatement st = null;
        String sql = "";
        String query = "SQL_INSERT";

        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));

            st.setString(1, comprobante.getDstrct());
            st.setString(2, comprobante.getTipodoc());
            st.setString(3, comprobante.getNumdoc());
            st.setString(4, comprobante.getSucursal());
            st.setString(5, comprobante.getPeriodo());
            st.setString(6, comprobante.getFechadoc());
            st.setString(7, comprobante.getDetalle());
            st.setString(8, comprobante.getTercero());
            st.setDouble(9, comprobante.getTotal_credito());
            st.setDouble(10, comprobante.getTotal_debito());
            st.setInt(11, comprobante.getTotal_items());
            st.setString(12, comprobante.getMoneda());
            st.setString(13, comprobante.getAprobador());
            st.setString(14, comprobante.getAprobador());
            st.setString(15, comprobante.getBase());
            st.setInt(16, comprobante.getGrupo_transaccion());
            st.setString(17, comprobante.getTipo_operacion());
            //st.executeUpdate();
            sql = st.toString();

        } catch (SQLException e) {

            throw new SQLException("Error en Insert [ComprobantesDAO]......." + e.getMessage() + " - " + e.getErrorCode());

        }

finally {
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return sql;

    }



    /**
     * Getter for property comprobante.
     * @return Value of property comprobante.
     */
    public com.tsp.finanzas.contab.model.beans.Comprobantes getComprobante() {
        return comprobante;
    }

    /**
     * Setter for property comprobante.
     * @param comprobante New value of property comprobante.
     */
    public void setComprobante(com.tsp.finanzas.contab.model.beans.Comprobantes comprobante) {
        this.comprobante = comprobante;
    }


    /**
     * Getter for property vector.
     * @return Value of property vector.
     */
    public java.util.Vector getVector() {
        return vector;
    }

    /**
     * Setter for property vector.
     * @param vector New value of property vector.
     */
    public void setVector(java.util.Vector vector) {
        this.vector = vector;
    }

    /**
     *Metodo que busca una lista de plarem sin contabilizar dada una planilla
     *@param: Numero de la planilla
     *@autor: Karen Reales
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void buscarPlarem(String numpla) throws SQLException {

        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs  =null;
        String query = "SQL_BUSCAR_PLAREM";
        try {

            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, numpla);
            rs=st.executeQuery();
            //////System.out.println("Query busca plarem "+st.toString());
            vector = new Vector();
            while (rs.next()){
                Comprobantes comprobante = new Comprobantes();
                comprobante.setAccount_code_c(rs.getString("account_code_c"));
                comprobante.setPorcent(rs.getInt("porcent"));
                comprobante.setNumrem(rs.getString("numrem"));
                comprobante.setPorcent_contabilizado(rs.getInt("porcent_contabilizacion"));
                vector.add(comprobante);
                comprobante = null;//Liberar Espacio JJCastro

            }
        } catch( SQLException e ){

            throw new SQLException( "Error en buscarPlarem [ComprobantesDAO]......." + e.getMessage() + " - " + e.getErrorCode() );

        }

finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }

    }

    /**
     *Metodo que busca el ultimo numero de transaccion generado
     *@return: Numero de transaccion
     *@autor: Karen Reales
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public int getGrupoTransaccion() throws SQLException {

        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        int grupo = 0;
        String query = "SQL_OBTENER_GRUPOT";
        try {

            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            rs = st.executeQuery();
            if (rs.next()) {
                grupo = rs.getInt("last_value");

            }
        } catch (SQLException e) {

            throw new SQLException("Error en marcarPlanilla [ComprobantesDAO]......." + e.getMessage() + " - " + e.getErrorCode());

        }
finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return grupo;
    }

    /**
     *Metodo busca un tipo de documento dado un documento interno
     *@param: Tipo de documento para buscar
     *@autor: Karen Reales
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public String getTipoDocumento(String documento) throws SQLException {

        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs  =null;
        String tipo="";
        String query = "SQL_OBTENER_DOCUMENTO";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, documento);
            rs=st.executeQuery();
            if (rs.next()){
                tipo = rs.getString("codigo");

            }
        } catch( SQLException e ){

            throw new SQLException( "Error en getTipoDocumento [ComprobantesDAO]......." + e.getMessage() + " - " + e.getErrorCode() );

        }

finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return tipo;
    }
    /**
     *Metodo que busca una lista de plarem sin contabilizar
     *@autor: Karen Reales
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void buscarListaPlarem() throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String tipo = "";
        String query = "SQL_LISTA_PLAREM";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            rs = st.executeQuery();
            vector = new Vector();
            while (rs.next()) {
                Comprobantes comprobante = new Comprobantes();
                comprobante.setNumdoc(rs.getString("numpla"));
                comprobante.setTipodoc_rel("001");
                comprobante.setNumdoc_rel(rs.getString("numpla"));

                vector.add(comprobante);
                comprobante = null; //Liberar Espacio JJCastro

            }
        } catch (SQLException e) {

            throw new SQLException("Error en buscarListaPlarem [ComprobantesDAO]......." + e.getMessage() + " - " + e.getErrorCode());

        }

 finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }

    }
    /**
     *Metodo que modifica un plarem y coloca la fecha de contabilizacion
     *el periodo y el numero de la transaccion
     *@autor: Karen Reales
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public String marcarPlarem() throws SQLException {

        Connection con = null;
        PreparedStatement st = null;
        String sql = "";
        String query = "SQL_UPDATE_PLAREM";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2

            st.setString(1, comprobante.getPeriodo());
            st.setInt(2, comprobante.getTransaccion());
            st.setInt(3, comprobante.getPorcent());
            st.setString(4, comprobante.getNumdoc());
            st.setString(5, comprobante.getNumrem());

            sql = st.toString();

        } catch( SQLException e ){

            throw new SQLException( "Error en marcarPlarem [ComprobantesDAO]......." + e.getMessage() + " - " + e.getErrorCode() );

        }

finally {
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return sql;
    }

    /**
     *Metodo que busca una lista de planillas anuladas sin contabilizar
     *@autor: Karen Reales
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void buscarListaPlaAnuladas() throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs  =null;
        String query = "SQL_LISTA_PLANILLAS_ANULADAS";

        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            rs=st.executeQuery();
            vector = new Vector();
            while (rs.next()){
                Comprobantes comprobante = new Comprobantes();
                comprobante.setNumdoc(rs.getString("documento"));
                comprobante.setTipodoc_rel("001");
                comprobante.setNumdoc_rel(rs.getString("documento"));
                vector.add(comprobante);
                comprobante = null; //Liberar Espacio JJCastro

            }
        } catch( SQLException e ){

            throw new SQLException( "Error en buscarListaPlaAnuladas [ComprobantesDAO]......." + e.getMessage() + " - " + e.getErrorCode() );

        }

finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }


    }
    /**
     *Metodo que busca una lista de comprobantes dado un tipo de documento
     *de contabilizacion.
     *@autor: Karen Reales
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void listarComprobantes() throws SQLException {

        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_BUSCAR_COMPROBANTE";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query).replaceAll("#TIPODOC#", comprobante.getTipodoc()));//JJCastro fase2
            st.setString(1, comprobante.getNumdoc());
            rs = st.executeQuery();
            vector = new Vector();
            while (rs.next()) {
                Comprobantes c = new Comprobantes();
                c.setDstrct(rs.getString("dstrct"));
                c.setTipodoc(rs.getString("tipodoc"));
                c.setNumdoc(rs.getString("numdoc"));
                c.setSucursal(rs.getString("sucursal"));
                c.setPeriodo(rs.getString("periodo"));
                c.setFechadoc(rs.getString("fechadoc"));
                c.setDetalle(rs.getString("detalle"));
                c.setTercero(rs.getString("tercero"));
                c.setTotal_credito(rs.getDouble("total_credito"));
                c.setTotal_debito(rs.getDouble("total_debito"));
                c.setTotal_items(rs.getInt("total_items"));
                c.setMoneda(rs.getString("moneda"));
                c.setAprobador(rs.getString("aprobador"));
                c.setBase(rs.getString("base"));
                c.setGrupo_transaccion(rs.getInt("grupo_transaccion"));
                vector.add(c);
                c = null; //Liberar Espacio JJCastro
            }



        } catch (SQLException e) {
            throw new SQLException("Error en listarComprobantes [ComprobantesDAO]......." + e.getMessage() + " - " + e.getErrorCode());
        }

finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }

    }

    /**
     *Metodo que busca una lista de detallers = st.executeQuery(s);s de un comprobante dado un tipo de documento
     *de contabilizacion.
     *@autor: Karen Reales
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void listarComprobantesDet() throws SQLException {

        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query = "SQL_BUSCAR_COMPRODET";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query).replaceAll("#TIPODOC#", comprobante.getTipodoc()));//JJCastro fase2
            st.setString(1, comprobante.getNumdoc());
            st.setInt(2, comprobante.getGrupo_transaccion());
            //////System.out.println("Query lista detalles "+st.toString());
            rs = st.executeQuery();

            vector = new Vector();
            while (rs.next()) {
                Comprobantes c = new Comprobantes();
                c.setDstrct(rs.getString("dstrct"));
                c.setTipodoc(rs.getString("tipodoc"));
                c.setNumdoc(rs.getString("numdoc"));
                c.setGrupo_transaccion(rs.getInt("Grupo_transaccion"));
                c.setPeriodo(rs.getString("periodo"));
                c.setCuenta(rs.getString("cuenta"));
                c.setDetalle(rs.getString("detalle"));
                c.setTotal_debito(rs.getDouble("valor_debito"));
                c.setTotal_credito(rs.getDouble("valor_credito"));
                c.setTercero(rs.getString("tercero"));
                c.setDocumento_interno(rs.getString("Documento_interno"));
                c.setBase(rs.getString("base"));
                c.setTipodoc_rel(rs.getString("tipodoc_rel"));
                c.setNumdoc_rel(rs.getString("documento_rel"));
                vector.add(c);
                c = null; //Liberar Espacio JJCastro
            }
        } catch (SQLException e) {
            throw new SQLException("Error en listarComprobantes [ComprobantesDAO]......." + e.getMessage() + " - " + e.getErrorCode());
        }
finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }

    }


    /**
     *Metodo que busca una lista de remesas anuladas sin contabilizar
     *@autor: Karen Reales
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void buscarListaRemesasAnuladas() throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_LISTA_REMESAS_ANULADAS";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            rs = st.executeQuery();
            vector = new Vector();
            while (rs.next()) {
                Comprobantes comprobante = new Comprobantes();
                comprobante.setNumdoc(rs.getString("documento"));
                comprobante.setTipodoc_rel("002");
                comprobante.setNumdoc_rel(rs.getString("documento"));

                vector.add(comprobante);
                comprobante = null; //Liberar Espacio JJCastro

            }
        } catch (SQLException e) {
            throw new SQLException("Error en buscarListaRemesasAnuladas [ComprobantesDAO]......." + e.getMessage() + " - " + e.getErrorCode());
        }
finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }


    }
    /**
     *Metodo que busca una lista de egresos anulados sin contabilizar
     *@autor: Karen Reales
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void buscarListaEgresosAnulados() throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_LISTA_EGRESOS_ANULADAS";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            rs = st.executeQuery();
            vector = new Vector();
            while (rs.next()) {
                Comprobantes comprobante = new Comprobantes();
                comprobante.setNumdoc(rs.getString("document_no"));
                comprobante.setGrupo_transaccion(rs.getInt("transaccion"));
                comprobante.setTipodoc_rel("004");
                comprobante.setNumdoc_rel(rs.getString("document_no"));
                comprobante.setBanco(rs.getString("branch_code"));
                comprobante.setSucursal_banco(rs.getString("bank_account_no"));
                vector.add(comprobante);
                comprobante = null; //Liberar Espacio JJCastro
            }
        } catch (SQLException e) {
            throw new SQLException("Error en buscarListaRemesasAnuladas [ComprobantesDAO]......." + e.getMessage() + " - " + e.getErrorCode());
        }
finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }


    }



    /**
     * M�todo que busca la moneda local
     * @autor.......Karen Reales
     * @throws......Exception
     * @version.....1.0.
     **/
    public  String getMoneda(String distrito) throws Exception{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_MONEDA_LOCAL";
        String moneda = "";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, distrito);
            rs = st.executeQuery();
            if (rs.next()) {
                moneda = rs.getString("moneda");
            }

        } catch (Exception e) {
            throw new Exception("getMoneda " + e.getMessage());
        }
finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return moneda;
    }
    /**
     *Metodo que obtiene los tipos de documento que se encuentren en comprodet
     *@autor: Osvaldo P�rez Ferrer
     *@throws: En caso de que un error de base de datos ocurra.
     *@return: vector
     */
    public Vector obtenerTiposDoc(String bd) throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_OBTENER_TIPOSDOC";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query).replaceAll("#TABLE#", bd));//JJCastro fase2
            rs = st.executeQuery();
            vector = new Vector();
            while (rs.next()) {
                vector.add(rs.getString("tipodoc"));
            }
        } catch (SQLException ex) {
            throw new SQLException("Error en obneterTiposDoc]......." + ex.getMessage() + " - " + ex.getErrorCode());
        }
finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return vector;
    }

    /**
     *Metodo que inserta los comprodet en la tabla temporal con
     *distrito, periodo, tipodoc para las posibles asociaciones
     *de las cuentas CGI a una cuenta normal
     *@autor: Osvaldo P�rez Ferrer
     *@param dstrct distrito
     *@param periodo periodo
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void  pasarATemporal( String dstrct, String periodo, String db) throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        String s = "";
        String query = "SQL_CREDEB_CUENTA_NOT_CGI";
        //Insertando los comprobantes NO CGI
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query).replaceAll("#TABLE#", db));//JJCastro fase2
            st.setString(1, dstrct);
            st.setString(2, periodo);
            st.executeUpdate();
        } catch (SQLException ex) {
            throw new SQLException("Error en SQL_CREDEB_CUENTA_NOT_CGI......." + ex.getMessage() + " - " + ex.getErrorCode());
        }
finally{
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }

        //Insertando los comprobantes CGI con elemento
        try{
            //Connection con2 = null;
            String query2 = "SQL_CREDEB_CUENTA_CGI_ELEM";
            con = this.conectarJNDI(query2);
            st = con.prepareStatement(this.obtenerSQL(query2).replaceAll("#TABLE#", db ));//JJCastro fase2
            st.setString( 1 , dstrct );
            st.setString( 2 , periodo );
            st.executeUpdate();
        }catch(SQLException ex){
            throw new SQLException( "Error en SQL_CREDEB_CUENTA_CGI_ELEM......." + ex.getMessage() + " - " + ex.getErrorCode() );
        }
finally{
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }

        //Insertando los comprobantes CGI sin elemento
        try {
            //Connection con3 = null;
            String query3 = "SQL_CREDEB_CUENTA_CGI_NOT_ELEM";
            con = this.conectarJNDI(query3);
            st = con.prepareStatement(this.obtenerSQL(query3).replaceAll("#TABLE#", db));//JJCastro fase2
            st.setString(1, dstrct);
            st.setString(2, periodo);
            st.executeUpdate();
        } catch (SQLException ex) {
            throw new SQLException("Error en SQL_CREDEB_CUENTA_CGI_NOT_ELEM......." + ex.getMessage() + " - " + ex.getErrorCode());
        }
finally {
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }

    }
    /**
     *Metodo que obtiene los comprobantes con tipodoc dado
     *con sus totales debito y credito
     *@autor: Osvaldo P�rez Ferrer
     *@throws: En caso de que un error de base de datos ocurra.
     *@param: tipodoc tipo de documento
     *@return: vector
     */
    public Vector obtenerCreditoDebitoTipodoc(String tipodoc, String db) throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        Comprobantes c;
        vector = new Vector();
        String s = "";
        String query = "SQL_DEBCRE_TIPODOC";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query).replaceAll("#TABLE#", db));//JJCastro fase2
            st.setString(1, tipodoc);
            rs = st.executeQuery();
            while (rs.next()) {
                c = new Comprobantes();
                c.setCuenta(rs.getString("cuenta"));
                c.setTotal_debito(rs.getDouble("debito"));
                c.setTotal_credito(rs.getDouble("credito"));
                c.setTipodoc(rs.getString("tipodoc"));
                //descripcion cuenta
                c.setDetalle((rs.getString("nombre_largo") == null) ? "" : rs.getString("nombre_largo"));
                vector.add(c);
                c = null; //Liberar Espacio JJCastro
            }
        }catch(SQLException ex){
            throw new SQLException( "Error en obtenerCreditoDebitoTipodoc......." + ex.getMessage() + " - " + ex.getErrorCode() );
        }
finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return vector;
    }

    /**
     *Metodo que crea una tabla temporal para estandarizar los comprodet
     *y generar el Libro Diario
     *@autor: Osvaldo P�rez Ferrer
     *@throws: En caso de que un error de base de datos ocurra.
     *@param: nombre para la tabla
     */
    public void crearTablaTemporal(String nombre) throws SQLException{
        Connection con = null;
        String query = "SQL_CREAR_TABLA_TEMPORAL";
        PreparedStatement st = null;
        try{
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query).replaceAll("#TABLE#", nombre));//JJCastro fase2
            st.executeUpdate();
        }catch(SQLException ex){
            throw new SQLException( "Error en crearTablaTemporal......." + ex.getMessage() + " - " + ex.getErrorCode() );
        }
finally {
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
    }

    /**
     *Metodo que borra la tabla dada del esquema tem
     *@autor: Osvaldo P�rez Ferrer
     *@throws: En caso de que un error de base de datos ocurra.
     *@param: nombre de la tabla
     */
    public void dropTablaTemporal(String nombre) throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        String query = "SQL_DROP_TABLA_TEMPORAL";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query).replaceAll("#TABLE#", nombre));//JJCastro fase2
            st.execute();

        } catch (SQLException ex) {
            throw new SQLException("Error en dropTablaTemporal......." + ex.getMessage() + " - " + ex.getErrorCode());
        }
finally{
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
    }
    /**
     *Metodo que verifica si existe el periodo dado en comprodet
     *@autor: Osvaldo P�rez Ferrer
     *@throws: En caso de que un error de base de datos ocurra.
     *@param: periodo
     *@return true si existe el periodo, false en caso contrario
     */
    public boolean existePeriodo(String periodo) throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        int tam = 0;
        String query = "SQL_EXISTE_PERIODO";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, periodo);
            rs = st.executeQuery();
            if (rs.next()) {
                tam = rs.getInt("count");
            }
        } catch (SQLException ex) {
            throw new SQLException("Error en exsistePeriodo......." + ex.getMessage() + " - " + ex.getErrorCode());
        }
finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return (tam > 0)?true:false;
    }

    /**
     *Metodo que obtiene total debito y credito para cada cuenta
     *@autor: Osvaldo P�rez Ferrer
     *@throws: En caso de que un error de base de datos ocurra.
     *@param: table tabla temporal de la que se va a leer
     *@return: vector
     */
    public Vector resumenLibroDiario(String table) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;

        Comprobantes c;
        vector = new Vector();
        String s = "";
        String query = "SQL_RESUMEN_LIBRO_DIARIO";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query).replaceAll("#TABLE#", table));//JJCastro fase2
            rs = st.executeQuery();
            while (rs.next()) {
                c = new Comprobantes();
                c.setCuenta(rs.getString("cuenta"));
                c.setTotal_debito(rs.getDouble("debito"));
                c.setTotal_credito(rs.getDouble("credito"));
                //descripcion cuenta
                c.setDetalle((rs.getString("nombre_largo") == null) ? "" : rs.getString("nombre_largo"));
                vector.add(c);
                c = null; //Liberar Espacio JJCastro
            }
        }catch(SQLException ex){
            throw new SQLException( "Error en resumenLibroDiario......." + ex.getMessage() + " - " + ex.getErrorCode() );
        }
finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return vector;
    }
    /**
     *Metodo que devuelve la consulta sql para insertar un comprobante
     *@autor: David Pi�a Lopez
     *@throws: En caso de que un error de base de datos ocurra.
     *@return Un String con el sql
     */
    public String getInsert() throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        String result = "";
        String query = "SQL_INSERT";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, comprobante.getDstrct());
            st.setString(2, comprobante.getTipodoc());
            st.setString(3, comprobante.getNumdoc());
            st.setString(4, comprobante.getSucursal());
            st.setString(5, comprobante.getPeriodo());
            st.setString(6, comprobante.getFechadoc());
            st.setString(7, comprobante.getDetalle());
            st.setString(8, comprobante.getTercero());
            st.setDouble(9, comprobante.getTotal_debito());
            st.setDouble(10, comprobante.getTotal_credito());
            st.setInt(11, comprobante.getTotal_items());
            st.setString(12, comprobante.getMoneda());
            st.setString(13, comprobante.getAprobador());
            st.setString(14, comprobante.getUsuario());
            st.setString(15, comprobante.getBase());
            st.setInt(16, comprobante.getGrupo_transaccion());//jose 2007-01-23
            st.setString(17, comprobante.getTipo_operacion());//jose 2007-01-23
            result = st.toString();

        }catch( SQLException e ){
            throw new SQLException( "Error en getInsert() [ComprobantesDAO]......." + e.getMessage() + " - " + e.getErrorCode() );
        }finally {
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return result;
    }

    /**
     *Metodo que devuelve la consulta sql para insertar un comprodet
     *@autor: David Pi�a Lopez
     *@throws: En caso de que un error de base de datos ocurra.
     *@return Un String con el sql
     */
    public String getInsertItem() throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        String result = "";
        String query = "SQL_INSERT_ITEMS";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, comprobante.getDstrct());
            st.setString(2, comprobante.getTipodoc());
            st.setString(3, comprobante.getNumdoc());
            st.setInt(4, comprobante.getGrupo_transaccion());
            st.setString(5, comprobante.getPeriodo());
            st.setString(6, comprobante.getCuenta());
            st.setString(7, comprobante.getDetalle());
            st.setDouble(8, comprobante.getTotal_debito());
            st.setDouble(9, comprobante.getTotal_credito());
            st.setString(10, comprobante.getTercero());
            st.setString(11, comprobante.getDocumento_interno());
            st.setString(12, comprobante.getUsuario());
            st.setString(13, comprobante.getBase());
            st.setString(14, comprobante.getTipodoc_rel());
            st.setString(15, comprobante.getNumdoc_rel());
            result = st.toString();
        } catch( SQLException e ){
            throw new SQLException( "Error en getInsertItem() [ComprobantesDAO]......." + e.getMessage() + " - " + e.getErrorCode() );
        }finally {
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return result;
    }
    /**
     *Metodo que permite actualizar el numero de items de un comprobante retornando la cadena
     *con el sql
     *@autor: David Pi�a
     *@param comprobante contiene la informaci�n o filtros para obtener el detalle de un
     *                   compobante
     *@throws: En caso de que un error de base de datos ocurra.
     *@return La cadena con el sql para actualizar el comprobante
     */
    public String getUpdateNumItems( Comprobantes comprobante ) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String result = "";
        String query = "SQL_UPDATE_NUM_ITEMS";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setInt(1, comprobante.getTotal_items());
            st.setString(2, comprobante.getUsuario());
            st.setString(3, comprobante.getTipodoc());
            st.setString(4, comprobante.getNumdoc());
            st.setInt(5, comprobante.getGrupo_transaccion());
            result = st.toString();
        } catch (SQLException e) {
            throw new SQLException("Error en updateNumItems [ComprobantesDAO]......." + e.getMessage() + " - " + e.getErrorCode());
        }
finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return result;
    }

    /**
     *Funci�n que permite obtener Comprobantes dados unos criterios
     *@autor: David Pi�a
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void getComprobantes() throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs  =null;
        String sql = "";
        String criterios = "";
        String orden = "";
        String porfechas = "";
        String query = "SQL_OBTENER_COMPROBANTES";
        try {
            if( comprobante.getAuxiliar() != null && !comprobante.getAuxiliar().equalsIgnoreCase("") ){
                if( comprobante.getAuxiliar().equalsIgnoreCase("N") ){
                    criterios = " AND fecha_aplicacion = '0099-01-01 00:00:00' ";
                }else if( comprobante.getAuxiliar().equalsIgnoreCase("D") ){
                    criterios = " AND total_debito != total_credito ";
                }else if( comprobante.getAuxiliar().equalsIgnoreCase("C") ){
                    criterios = " AND fecha_aplicacion != '0099-01-01 00:00:00' ";
                }
            }
            if( comprobante.getTercero() != null && !comprobante.getTercero().equalsIgnoreCase("") ){
                orden = " ORDER BY " + comprobante.getTercero();
            }
            if( comprobante.getFecha_aplicacion() != null && !comprobante.getFecha_aplicacion().equalsIgnoreCase("") &&
            comprobante.getFecha_creacion() != null && !comprobante.getFecha_creacion().equalsIgnoreCase("") ){
                porfechas = " AND creation_date BETWEEN '" + comprobante.getFecha_aplicacion() + " 00:00:00' AND '" + comprobante.getFecha_creacion() + " 23:59:59' ";
            }else{
                porfechas = " AND periodo LIKE '%" + comprobante.getPeriodo() + "%' ";
            }
            con = this.conectarJNDI(query);
            /*
            st = con.prepareStatement(this.obtenerSQL(query).replaceAll("#CRITERIOS#", criterios + porfechas + orden));//JJCastro fase2
            st.setString( 1, "%"+comprobante.getTipodoc()+"%" );
            st.setString( 2, "%"+comprobante.getNumdoc()+"%" );
             */
            String sqlx=this.obtenerSQL(query);
            if (comprobante.getTipodoc()!=null && !(comprobante.getTipodoc().equals(""))){
                    sqlx=sqlx.replaceAll("#TIPDOC#"," tipodoc='"+comprobante.getTipodoc()+"' AND ");
            }else{
                    sqlx=sqlx.replaceAll("#TIPDOC#"," ");
            }
            if (comprobante.getNumdoc()!=null && !(comprobante.getNumdoc().equals(""))){
                    sqlx=sqlx.replaceAll("#DOCUMENT#"," numdoc = '"+comprobante.getNumdoc()+"' AND ");
            }else{
                    sqlx=sqlx.replaceAll("#DOCUMENT#"," ");
            }
            st = con.prepareStatement(sqlx.replaceAll("#CRITERIOS#", criterios + porfechas + orden));//JJCastro fase2

            ////System.out.println("sql : " + sql);
            rs = st.executeQuery();
            vector = new Vector();
            while ( rs.next() ){
                Comprobantes comprobante = new Comprobantes();
                comprobante.setDstrct( rs.getString("dstrct") );
                comprobante.setPeriodo( (rs.getString("periodo")!=null)?rs.getString("periodo"):"" );
                comprobante.setTipodoc( (rs.getString("tipodoc")!=null)?rs.getString("tipodoc"):"" );
                comprobante.setNumdoc( (rs.getString("numdoc")!=null)?rs.getString("numdoc"):"" );
                comprobante.setGrupo_transaccion( rs.getInt("grupo_transaccion") );
                comprobante.setFechadoc( (rs.getString("creation_date")!=null)?rs.getString("creation_date"):"" );
                comprobante.setFecha_aplicacion( (rs.getString("fecha_aplicacion")!=null)?rs.getString("fecha_aplicacion"):"" );
                comprobante.setUsuario( (rs.getString("creation_user")!=null)?rs.getString("creation_user"):"" );
                comprobante.setTotal_debito( rs.getDouble("total_debito") );
                comprobante.setTotal_credito( rs.getDouble("total_credito") );
                comprobante.setTercero( rs.getString("tercero") );
                vector.add( comprobante );
                comprobante = null; //Liberar Espacio JJCastro
            }
        } catch( SQLException e ){
            throw new SQLException( "Error en getComprobantes [ComprobantesDAO]......." + e.getMessage() + " - " + e.getErrorCode() );
        }
finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
    }

    /**
     *Metodo que permite obtener los detalles de un comprobante sobre la tabla
     *COMPRODET
     *@autor: David Pi�a
     *@param comprobante contiene la informaci�n o filtros para obtener el detalle de un
     *                   compobante
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void getDetallesComprobante( Comprobantes comprobante ) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs  =null;
        String query = "SQL_OBTENER_DETALLES";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString( 1, comprobante.getTipodoc() );
            st.setString( 2, comprobante.getNumdoc() );
            st.setInt( 3, comprobante.getGrupo_transaccion() );
            rs=st.executeQuery();
            vector = new Vector();
            while (rs.next()){
                Comprobantes detalle = new Comprobantes();
                detalle.setTipodoc           ( rs.getString("tipodoc"));
                detalle.setNumdoc            ( rs.getString("numdoc"));
                detalle.setGrupo_transaccion( rs.getInt("grupo_transaccion") );
                detalle.setTransaccion       ( rs.getInt("transaccion") );
                detalle.setPeriodo           ( rs.getString("periodo"));
                detalle.setTercero           ( rs.getString("tercero"));
                detalle.setDstrct            ( rs.getString("dstrct") );
                detalle.setCuenta            ( rs.getString("cuenta") );
                detalle.setAuxiliar          ( rs.getString("auxiliar") );
                detalle.setDetalle           ( rs.getString("detalle") );
                detalle.setTipodoc_rel       ( rs.getString("tipodoc_rel") );
                detalle.setNumdoc_rel        ( rs.getString("documento_rel") );
                detalle.setTotal_debito      ( rs.getDouble("valor_debito") );
                detalle.setTotal_credito     ( rs.getDouble("valor_credito") );
                vector.add( detalle );
                detalle = null; //Liberar Espacio JJCastro
            }
        } catch( SQLException e ){
            throw new SQLException( "Error en getDetallesComprobante [ComprobantesDAO]......." + e.getMessage() + " - " + e.getErrorCode() );
        }
finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
    }





    /**
     * Metodo para extraer los comprobantes de diarios
     * @autor mfontalvo
     * @param tipodoc Tipo de documento
     * @param numdoc numero del documento
     * @param numtransaccion grupo transaccion
     * @param periodo Periodo contable
     * @throws Exception.
     * @return Vector vector de comprobantes.
     */

    public Vector getComprobantes(String tipodoc, String numdoc, String numtransaccion, String periodo) throws Exception{
        Connection con = null;
        Vector dt = new Vector();
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_OBTENER_COMPROBANTES2";

        try {
            // query
            StringBuffer wh = new StringBuffer();
            if (!tipodoc.equals("")) {
                wh.append((wh.toString().equals("") ? " WHERE " : " AND ") + " tipodoc = '" + tipodoc + "' ");
            }
            if (!numdoc.equals("")) {
                wh.append((wh.toString().equals("") ? " WHERE " : " AND ") + " numdoc  = '" + numdoc + "' ");
            }
            if (!numtransaccion.equals("")) {
                wh.append((wh.toString().equals("") ? " WHERE " : " AND ") + " grupo_transaccion = '" + numtransaccion + "' ");
            }
            if (!periodo.equals("")) {
                wh.append((wh.toString().equals("") ? " WHERE " : " AND ") + " periodo = '" + periodo + "' ");
            }
            con = this.conectarJNDI(query);
            query = this.obtenerSQL(query) + wh.toString();

            st = con.prepareStatement(query);//JJCastro fase2
            rs = st.executeQuery();
            while (rs.next()) {
                Comprobantes com = new Comprobantes();
                com.setDstrct(rs.getString("dstrct"));
                com.setTipodoc(rs.getString("tipodoc"));
                com.setNumdoc(rs.getString("numdoc"));
                com.setGrupo_transaccion(rs.getInt("grupo_transaccion"));
                com.setPeriodo(rs.getString("periodo"));
                com.setFechadoc(rs.getString("fechadoc"));
                com.setDetalle(rs.getString("detalle"));
                com.setTercero(rs.getString("tercero"));
                com.setFecha_aplicacion(rs.getString("fecha_aplicacion"));
                com.setUsuario(rs.getString("creation_user"));
                com.setFecha_creacion(rs.getString("creation_date"));
                dt.add(com);
                com = null; //Liberar Espacio JJCastro
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return dt;
    }



    /**
     * Metodo para extraer los comprobantes de diarios
     * @autor mfontalvo
     * @param Vector vector de comprobantes a setear
     * @throws Exception.
     * @return Vector vector de comprobantes.
     */

    public Vector getComprobantes(Vector dt) throws Exception{
        if (dt==null || dt.isEmpty())
            return dt;

        Connection con = null;
        PreparedStatement st  = null;
        ResultSet         rs  = null;
        String query = "SQL_OBTENER_COMPROBANTES2";
        try{
            // query
            StringBuffer wh = new StringBuffer(" WHERE ");
            wh.append("     tipodoc = ? ");
            wh.append(" and numdoc  = ? ");
            wh.append(" and grupo_transaccion = ? ");
            query = this.obtenerSQL(query) + wh.toString();
            for (int i = 0; i < dt.size();) {
                Comprobantes com = (Comprobantes) dt.get(i);
                st.clearParameters();
                st.setString(1, com.getTipodoc());
                st.setString(2, com.getNumdoc());
                st.setInt(3, com.getGrupo_transaccion());
                rs = st.executeQuery();
                if (rs.next()) {
                    com.setDstrct(rs.getString("dstrct"));
                    com.setPeriodo(rs.getString("periodo"));
                    com.setFechadoc(rs.getString("fechadoc"));
                    com.setDetalle(rs.getString("detalle"));
                    com.setTercero(rs.getString("tercero"));
                    com.setFecha_aplicacion(rs.getString("fecha_aplicacion"));
                    com.setUsuario(rs.getString("creation_user"));
                    com.setFecha_creacion(rs.getString("creation_date"));
                    i++;
                } else {
                    dt.remove(i);
                }
            }

        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return dt;
    }

    /**
     *Metodo que permite obtener las cuentas tipo I y C con elemento de gasto 8005 y 9005
     *respectivamente teniendo en cuenta el periodo y rango de fechas dado
     *@autor: David Pi�a
     *@param periodo periodo del detalle del comprobante
     *@param fechaInicio La fecha de inicio
     *@param fechaFin La fecha de Final
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void getCuentasIC( String periodo, String fechaInicio, String fechaFin ) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_OBTENER_CUENTAS_IC";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, periodo);
            st.setString(2, fechaInicio + " 00:00:00");
            st.setString(3, fechaFin + " 23:59:59");
            rs = st.executeQuery();
            vector = new Vector();
            while (rs.next()) {
                Comprobantes comprobante = new Comprobantes();
                comprobante.setTipodoc((rs.getString("tipodoc") != null) ? rs.getString("tipodoc") : "");
                comprobante.setNumdoc((rs.getString("numdoc") != null) ? rs.getString("numdoc") : "");
                comprobante.setGrupo_transaccion(rs.getInt("grupo_transaccion"));
                comprobante.setTransaccion(rs.getInt("transaccion"));
                comprobante.setCuenta((rs.getString("cuenta") != null) ? rs.getString("cuenta") : "");
                comprobante.setDetalle((rs.getString("detalle") != null) ? rs.getString("detalle") : "");
                comprobante.setAuxiliar((rs.getString("tipodoc_rel") != null) ? rs.getString("tipodoc_rel") : "");
                comprobante.setDocumento_interno((rs.getString("documento_rel") != null) ? rs.getString("documento_rel") : "");
                comprobante.setTotal_debito(rs.getDouble("valor_debito"));
                comprobante.setTotal_credito(rs.getDouble("valor_credito"));
                vector.add(comprobante);
                comprobante = null; //Liberar Espacio JJCastro
            }
        } catch (SQLException e) {
            throw new SQLException("Error en getCuentasIC [ComprobantesDAO]......." + e.getMessage() + " - " + e.getErrorCode());
        }
finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
    }

    /*METODOS KAREN 04-10-2006*/
    /**
     *Metodo que inserta los items de un comprobante
     *@autor: Karen Reales
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public String insertItem() throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        String sql = "";
        String query = "SQL_INSERT_ITEMS";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, comprobante.getDstrct());
            st.setString(2, comprobante.getTipodoc());
            st.setString(3, comprobante.getNumdoc());
            st.setInt(4, comprobante.getGrupo_transaccion());
            st.setString(5, comprobante.getPeriodo());
            st.setString(6, comprobante.getCuenta());
            st.setString(7, comprobante.getDetalle());
            st.setDouble(8, comprobante.getTotal_debito());
            st.setDouble(9, comprobante.getTotal_credito());
            st.setString(10, comprobante.getTercero());
            st.setString(11, comprobante.getDocumento_interno());
            st.setString(12, comprobante.getAprobador());
            st.setString(13, comprobante.getBase());
            st.setString(14, comprobante.getTipodoc_rel());
            st.setString(15, comprobante.getNumdoc_rel());
            ////System.out.println("Insert "+st.toString());
            sql = st.toString();
            //st.executeUpdate();

        } catch (SQLException e) {
            throw new SQLException("Error en Insert [ComprobantesDAO]......." + e.getMessage() + " - " + e.getErrorCode());
        }
finally {
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return sql;
    }

    /**
     *Metodo que modifica la planilla y coloca la fecha de contabilizacion
     *el periodo y el numero de la transaccion
     *@autor: Karen Reales
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public String marcarPlanilla() throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        String sql ="";
        String query = "SQL_UPDATE_PLANILLA";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, comprobante.getPeriodo());
            st.setInt(2, comprobante.getGrupo_transaccion());
            st.setString(3, comprobante.getNumdoc());
            sql = st.toString();
        }catch( SQLException e ){
            throw new SQLException( "Error en marcarPlanilla [ComprobantesDAO]......." + e.getMessage() + " - " + e.getErrorCode() );
        }
finally {
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return sql;
    }

    /**
     *Metodo que modifica un cxpdoc y coloca la fecha de contabilizacion
     *el periodo y el numero de la transaccion
     *@autor: Karen Reales
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public String marcarDifXCambio() throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        String sql = "";
        String query = "SQL_UPDATE_AJC";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, comprobante.getPeriodo());
            st.setInt(2, comprobante.getGrupo_transaccion());
            st.setString(3, comprobante.getUsuario());
            st.setString(4, comprobante.getDocumento_interno());
            st.setString(5, comprobante.getNumdoc_rel());
            //st.executeUpdate();
            sql = st.toString();
        } catch (SQLException e) {
            throw new SQLException("Error en marcarDifXCambio [ComprobantesDAO]......." + e.getMessage() + " - " + e.getErrorCode());
        }
finally {
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return sql;
    }
    /**
     *Metodo que modifica un cxpdoc y coloca la fecha de contabilizacion
     *el periodo y el numero de la transaccion
     *@autor: Karen Reales
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public String marcarDifXValor() throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        String sql = "";
        String query = "SQL_UPDATE_AJV";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, comprobante.getPeriodo());
            st.setInt(2, comprobante.getGrupo_transaccion());
            st.setString(3, comprobante.getUsuario());
            st.setString(4, comprobante.getDocumento_interno());
            st.setString(5, comprobante.getNumdoc_rel());
            sql = st.toString();
        } catch( SQLException e ){
            throw new SQLException( "Error en marcarDifXValor [ComprobantesDAO]......." + e.getMessage() + " - " + e.getErrorCode() );
        }
finally{
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return sql ;
    }
    /**
     *Metodo que modifica el movpla y coloca la fecha de contabilizacion
     *el periodo y el numero de la transaccion
     *@autor: Karen Reales
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public String marcarMovPlanilla() throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        String sql = "";
        String query = "SQL_UPDATE_MOVPLA" ;
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, comprobante.getPeriodo());
            st.setInt(2, comprobante.getGrupo_transaccion());
            st.setString(3, comprobante.getNumdoc());
            st.setString(4,comprobante.getFecha_creacion());
            sql = st.toString();
        } catch( SQLException e ){
            throw new SQLException( "Error en marcarMovPlanilla [ComprobantesDAO]......." + e.getMessage() + " - " + e.getErrorCode() );
        }
finally {
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return sql;

    }
    /**
     *Metodo que modifica la remesa y coloca la fecha de contabilizacion
     *el periodo y el numero de la transaccion
     *@autor: Karen Reales
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public String marcarRemesa() throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        String sql="";
        String query = "SQL_UPDATE_REMESA";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, comprobante.getPeriodo());
            st.setInt(2, comprobante.getGrupo_transaccion());
            st.setString(3, comprobante.getNumdoc());
            sql = st.toString();
        } catch( SQLException e ){
            throw new SQLException( "Error en marcarRemesa [ComprobantesDAO]......." + e.getMessage() + " - " + e.getErrorCode() );
        }
finally {
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return sql ;
    }

    /**
     *Metodo que modifica el egreso anulado y coloca la fecha de contabilizacion
     *el periodo y el numero de la transaccion
     *@autor: Karen Reales
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public String marcarEgresoAnulado() throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        String sql ="";
        String query = "SQL_UPDATE_ANULA_EGRESO";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, comprobante.getPeriodo());
            st.setInt(2, comprobante.getGrupo_transaccion());
            st.setString(3, comprobante.getBanco());
            st.setString(4, comprobante.getSucursal_banco());
            st.setString(5, comprobante.getNumdoc());
            sql = st.toString();
        } catch( SQLException e ){
            throw new SQLException( "Error en marcarEgresoAnulado [ComprobantesDAO]......." + e.getMessage() + " - " + e.getErrorCode() );
        }
finally{
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return sql;
    }

    /**
     *Metodo que modifica el documento (Planilla o Remesa) anulado y coloca la fecha de contabilizacion
     *el periodo y el numero de la transaccion
     *@autor: Karen Reales
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public String marcarDocumentoAnulado() throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        String sql = "";
        String query = "SQL_UPDATE_ANULA_DOC";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, comprobante.getPeriodo());
            st.setInt(2, comprobante.getGrupo_transaccion());
            st.setString(3, comprobante.getDocumento_interno());
            st.setString(4, comprobante.getNumdoc());

            sql = st.toString();

        } catch (SQLException e) {

            throw new SQLException("Error en marcarDocumentoAnulado [ComprobantesDAO]......." + e.getMessage() + " - " + e.getErrorCode());

        }
finally{
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return sql;

    }
    /**
     *Metodo que modifica un plarem y coloca la fecha de contabilizacion
     *el periodo y el numero de la transaccion a aquellas planillas redistribuidas
     *@autor: Karen Reales
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public String marcarRedistribucion() throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        String sql="";
        String query = "SQL_UPDATE_REDISTRIBUCION";
        String query2 = "SQL_UPDATE_PORCENT";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, comprobante.getPeriodo());
            st.setInt(2, comprobante.getTransaccion());
            st.setInt(3, comprobante.getPorcent());
            st.setString(4, comprobante.getNumdoc());
            st.setString(5, comprobante.getNumrem());
            sql = st.toString();

            st = con.prepareStatement(this.obtenerSQL(query2));
            st.setInt(1, comprobante.getPorcent());
            st.setString(2, comprobante.getNumdoc());
            st.setString(3, comprobante.getNumrem());
            sql = sql + ";"+st.toString();
        } catch( SQLException e ){
            throw new SQLException( "Error en marcarPlarem [ComprobantesDAO]......." + e.getMessage() + " - " + e.getErrorCode() );
        }
 finally{
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return sql;
    }

    /**
     *Metodo que modifica tablagen y coloca el ultimo numero de transaccion guardado
     *@autor: Karen Reales
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public String actualizarTablaGen() throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        String sql="";
        String query = "SQL_ULTIMO_DOCTO";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, String.valueOf(comprobante.getGrupo_transaccion()));
            st.setString(2, comprobante.getTipodoc());
            sql = st.toString();
        }catch( SQLException e ){
            throw new SQLException( "Error en actualizar Tablagen [ComprobantesDAO]......." + e.getMessage() + " - " + e.getErrorCode() );
        }
        finally {
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return sql;
    }

    /*Contabilizacion Karen*/



    /**
     *Metodo que busca una lista de movpla dado un tipo de documento
     *@param: Tipo de documento para buscar cmc
     *@autor: Karen Reales
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void buscarMovpla(String tipoDoc) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_BUSCAR_MOVPLA";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, tipoDoc);
            rs = st.executeQuery();
            vector = new Vector();
            while (rs.next()) {
                Comprobantes comprobante = new Comprobantes();
                comprobante.setNumdoc(rs.getString("numpla"));
                comprobante.setSucursal("OP");
                comprobante.setDetalle("MOVIMIENTO PLANILLA " + rs.getString("numpla"));
                comprobante.setTercero(rs.getString("nitpro"));
                comprobante.setMoneda(rs.getString("currency"));
                comprobante.setBase(rs.getString("base"));
                comprobante.setCmc(rs.getString("cuenta") != null ? rs.getString("cuenta") : "");
                comprobante.setCuenta(rs.getString("account") != null ? rs.getString("account") : "");
                comprobante.setTieneProv(rs.getBoolean("tiene_provee"));
                comprobante.setTipo_cuenta(rs.getString("tipocuenta") != null ? rs.getString("tipocuenta") : "");
                comprobante.setTipo_valor(rs.getDouble("vlr") < 0 ? "C" : "D");
                comprobante.setFecha_creacion(rs.getString("creation_date"));
                double valor = 0;
                if (rs.getString("ind_vlr").equals("P")) {

                    if (rs.getDouble("vlrpla2") <= 0) {
                        valor = (rs.getDouble("vlrpla") * rs.getDouble("vlr")) / 100;
                    } else {
                        valor = (rs.getDouble("vlrpla2") * rs.getDouble("vlr")) / 100;
                    }

                } else {
                    valor = rs.getDouble("vlr");
                }
                comprobante.setTotal_debito(valor < 0 ? valor * -1 : valor);
                comprobante.setTotal_credito(valor < 0 ? valor * -1 : valor);
                comprobante.setTipodoc_rel("001");
                comprobante.setNumdoc_rel(rs.getString("numpla"));
                vector.add(comprobante);
                comprobante = null; //Liberar Espacio JJCastro

            }
        } catch (SQLException e) {

            throw new SQLException("Error en buscarMovpla [ComprobantesDAO]......." + e.getMessage() + " - " + e.getErrorCode());

        }
finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
    }



    /**
     *Metodo que busca una lista de facturas sin contabilizar su ajuste por cambio
     *@param: Tipo de documento para buscar un cmc
     *@autor: Karen Reales
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void buscarAjustesXCambio(String tipoDoc) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs  =null;
        String query = "SQL_BUSCAR_AJUSTEXCAMBIO";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1,tipoDoc);
            rs=st.executeQuery();
            //////System.out.println("Query "+st.toString());
            vector = new Vector();
            while (rs.next()){
                double valor =rs.getDouble("diferencia");
                if(valor!=0){
                    Comprobantes comprobante = new Comprobantes();
                    comprobante.setNumdoc(rs.getString("numpla"));
                    comprobante.setSucursal("OP");
                    comprobante.setDetalle("AJUSTE X CAMBIO FACT "+rs.getString("documento")+" PLA "+rs.getString("numpla"));
                    comprobante.setTercero(rs.getString("nitpro"));
                    comprobante.setTotal_debito(valor<0?valor*-1:valor);
                    comprobante.setTotal_credito(valor<0?valor*-1:valor);
                    comprobante.setMoneda(rs.getString("moneda"));
                    comprobante.setBase(rs.getString("base"));
                    comprobante.setCmc(rs.getString("cuenta")!=null?rs.getString("cuenta"):"");
                    comprobante.setTieneProv(rs.getBoolean("tiene_provee"));
                    comprobante.setTipo_valor(rs.getDouble("diferencia")<0?"C":"D");
                    comprobante.setCuenta(rs.getString("cuentaAjc"));
                    comprobante.setTipodoc_rel("010");
                    comprobante.setNumdoc_rel(rs.getString("documento"));
                    vector.add(comprobante);
                    comprobante = null; //Liberar Espacio JJCastro
                }

            }
        } catch( SQLException e ){
            throw new SQLException( "Error en buscarAjustesXValor [ComprobantesDAO]......." + e.getMessage() + " - " + e.getErrorCode() );
        }
finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }

    }

    /**
     *Metodo que busca una lista de facturas sin contabilizar su ajuste por valor
     *@param: Tipo de documento para buscar un cmc
     *@autor: Karen Reales
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void buscarAjustesXValor(String tipoDoc) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs  =null;
        String query = "SQL_BUSCAR_AJUSTEXVALOR";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1,tipoDoc);
            rs=st.executeQuery();
            //////System.out.println("Query "+st.toString());
            vector = new Vector();
            while (rs.next()){
                double valor =rs.getDouble("unit_cost")*rs.getDouble("diferencia");
                Comprobantes comprobante = new Comprobantes();
                comprobante.setNumdoc(rs.getString("numpla"));
                comprobante.setSucursal("OP");
                comprobante.setDetalle("AJUSTE X VALOR FACT "+rs.getString("documento")+" PLA "+rs.getString("numpla"));
                comprobante.setTercero(rs.getString("nitpro"));
                comprobante.setTotal_debito(valor<0?valor*-1:valor);
                comprobante.setTotal_credito(valor<0?valor*-1:valor);
                comprobante.setMoneda(rs.getString("currency"));
                comprobante.setBase(rs.getString("base"));
                comprobante.setCmc(rs.getString("cuenta")!=null?rs.getString("cuenta"):"");
                comprobante.setTieneProv(rs.getBoolean("tiene_provee"));
                comprobante.setTipo_valor(rs.getDouble("diferencia")<0?"C":"D");
                comprobante.setCuenta(rs.getString("cuentaAjc"));
                comprobante.setFechadoc(rs.getString("creacion"));
                comprobante.setTipodoc_rel("010");
                comprobante.setNumdoc_rel(rs.getString("documento"));
                vector.add(comprobante);
                comprobante = null;

            }
        } catch( SQLException e ){

            throw new SQLException( "Error en buscarAjustesXValor [ComprobantesDAO]......." + e.getMessage() + " - " + e.getErrorCode() );

        }
finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }

    }

    /**
     *Metodo que busca una lista de planillas sin contabilizar
     *@param: Tipo de documento para buscar el cmc
     *@autor: Karen Reales
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void buscarPlanillas(String tipoDoc) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_BUSCAR_PLANILLAS";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, "PLA");
            rs = st.executeQuery();
            ////////System.out.println("Query "+st.toString());
            vector = new Vector();
            while (rs.next()) {

                Comprobantes comprobante = new Comprobantes();
                comprobante.setNumdoc(rs.getString("numpla"));
                comprobante.setSucursal("OP");
                comprobante.setDetalle("CAUSACION PLANILLA " + rs.getString("numpla"));
                comprobante.setTercero(rs.getString("nitpro"));
                comprobante.setTotal_debito(rs.getDouble("vlrpla2"));
                comprobante.setTotal_credito(rs.getDouble("vlrpla2"));
                comprobante.setMoneda(rs.getString("currency"));
                comprobante.setBase(rs.getString("base"));
                comprobante.setCmc(rs.getString("cuenta") != null ? rs.getString("cuenta") : "");
                comprobante.setTipodoc_rel("001");
                comprobante.setNumdoc_rel(rs.getString("numpla"));
                comprobante.setTieneProv(rs.getBoolean("tiene_provee"));
                comprobante.setPlaca(rs.getString("plaveh"));
                vector.add(comprobante);
                comprobante = null; //Liberar Espacio JJCastro

            }
        } catch (SQLException e) {
            throw new SQLException("Error en marcarPlanilla [ComprobantesDAO]......." + e.getMessage() + " - " + e.getErrorCode());
        }
finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }

    }


    /**
     *Metodo busca una lista de remesas sin contabilizar dado un tipo de documento
     *@param: Tipo de documento para buscar cmc
     *@autor: Karen Reales
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void buscarRemesas(String tipoDoc) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_BUSCAR_REMESAS";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, tipoDoc);
            rs = st.executeQuery();
            ////////System.out.println("Query "+st.toString());
            vector = new Vector();
            while (rs.next()) {
                Comprobantes comprobante = new Comprobantes();
                comprobante.setNumdoc(rs.getString("numrem"));
                comprobante.setSucursal("OP");
                comprobante.setDetalle("CAUSACION REMESA " + rs.getString("numrem"));
                comprobante.setTercero(rs.getString("cliente"));
                comprobante.setTotal_debito(rs.getDouble("vlrrem2"));
                comprobante.setTotal_credito(rs.getDouble("vlrrem2"));
                comprobante.setMoneda(rs.getString("currency") != null ? rs.getString("currency") : "");
                comprobante.setBase(rs.getString("base"));
                comprobante.setCmc(rs.getString("cuenta") != null ? rs.getString("cuenta") : "");
                comprobante.setAccount_code_c(rs.getString("account_code_i"));
                comprobante.setTipodoc_rel("002");
                comprobante.setNumdoc_rel(rs.getString("numrem"));
                comprobante.setTieneProv(rs.getBoolean("tiene_cliente"));
                vector.add(comprobante);
                comprobante = null; //Liberar Espacio JJCastro

            }
        } catch (SQLException e) {
            throw new SQLException("Error en buscarRemesas [ComprobantesDAO]......." + e.getMessage() + " - " + e.getErrorCode());
        }
finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }

    }

    /**
     *Metodo que busca una planilla seleccionada para descontabilizar
     *@autor: Karen Reales
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void buscarPlanillaDescontabilizar(String documento) throws SQLException {
        Connection con = null;
        PreparedStatement st = null, st2 = null;
        ResultSet rs = null, rs2 = null;
        String query = "SQL_PLANILLA_DESCONTABILIZAR";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, documento);
            rs = st.executeQuery();
            if (rs.next()) {
                comprobante = new Comprobantes();
                comprobante.setTipodoc("PLA");
                comprobante.setNumdoc(rs.getString("numpla"));
                comprobante.setValor(rs.getDouble("vlrpla2"));
                comprobante.setValor_for(rs.getDouble("vlrpla"));
                comprobante.setGrupo_transaccion(rs.getInt("GRUPO_TRANSACCION"));
                comprobante.setValor_unit(rs.getDouble("UNIT_COST"));
                comprobante.setCantidad(rs.getDouble("pesoreal"));
                comprobante.setMoneda(rs.getString("currency"));
                comprobante.setNumdoc_rel(rs.getString("factura"));
                comprobante.setTercero(rs.getString("nitpro"));
                comprobante.setDstrct(rs.getString("cia"));

                st2 = con.prepareStatement(this.obtenerSQL("SQL_COSTOS_STANDARD"));
                st2.setString(1, rs.getString("STD_JOB_NO"));
                rs2 = st2.executeQuery();
                Vector v = new Vector();
                while (rs2.next()) {
                    Comprobantes c = new Comprobantes();
                    c.setSj(rs2.getString("sj"));
                    c.setValor_unit(rs2.getDouble("costo"));
                    c.setMoneda(rs2.getString("currency"));
                    v.add(c);
                }
                comprobante.setLista(v);
            }


        } catch (SQLException e) {
            throw new SQLException("Error buscando datos de planilla a descontabilizar [ComprobantesDAO]......." + e.getMessage() + " - " + e.getErrorCode());
        }
finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }

    }

    /**
     *Metodo que desmarca y borra los datos contabilizados y la op generada de una planilla
     *@autor: Karen Reales
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void descontabilizarPlanilla() throws SQLException {

        PreparedStatement st = null, st2=null;
        ResultSet  rs = null,rs2=null;
        Connection con =null;
        String query = "SQL_ACTUALIZAR_VALORPLA";
        try {
                con = this.conectarJNDI(query);
                con.setAutoCommit(false);

                st = con.prepareStatement( this.obtenerSQL("SQL_ACTUALIZAR_VALORPLA" ));//JJCastro fase2
                st.setDouble(1,comprobante.getValor_for());
                st.setDouble(2,comprobante.getValor());
                st.setDouble(3,comprobante.getCantidad());
                st.setDouble(4,comprobante.getValor_unit());
                st.setString(5, comprobante.getMoneda());
                st.setString(6, comprobante.getNumdoc());

                st.setDouble(7,comprobante.getCantidad());
                st.setString(8, comprobante.getDstrct());
                st.setString(9, comprobante.getNumdoc());

                ////System.out.println("QUERY 1"+st);
                st.executeUpdate();


                st = con.prepareStatement( this.obtenerSQL("SQL_BORRAR_CONTAB" ));
                st.setString(1,comprobante.getTipodoc());
                st.setString(2,comprobante.getNumdoc());
                st.setInt(3,comprobante.getGrupo_transaccion());
                ////System.out.println("QUERY 2"+st);
                st.executeUpdate();

                st = con.prepareStatement( this.obtenerSQL("SQL_DESMARCAR_PLANILLA" ));
                st.setString(1,comprobante.getNumdoc());
                st.setString(2,comprobante.getNumdoc());
                ////System.out.println("QUERY 3"+st);
                st.executeUpdate();

                if(!comprobante.getNumdoc_rel().equals("")){

                    st = con.prepareStatement( this.obtenerSQL("SQL_BORRAR_OP" ));
                    st.setString(1,comprobante.getDstrct());
                    st.setString(2,comprobante.getTercero());
                    st.setString(3,comprobante.getNumdoc_rel());

                    st.setString(4,comprobante.getDstrct());
                    st.setString(5,comprobante.getTercero());
                    st.setString(6,comprobante.getNumdoc_rel());

                    st.setString(7,comprobante.getDstrct());
                    st.setString(8,comprobante.getTercero());
                    st.setString(9,comprobante.getNumdoc_rel());

                    st.setString(10,comprobante.getDstrct());
                    st.setString(11,comprobante.getTercero());
                    st.setString(12,comprobante.getNumdoc_rel());
                    ////System.out.println("QUERY 3"+st);
                    st.executeUpdate();
                }


                con.commit();


        } catch( SQLException e ){

            try {

                con.rollback();
            }
            catch (SQLException ignored) {
                throw new SQLException("NO SE PUDO HACER ROLLBACK " + ignored.getMessage() + " " + ignored.getErrorCode()+" <br> La siguiente exception es : ----"+ignored.getNextException());
            }

            throw new SQLException( "Error buscando datos de planilla a descontabilizar [ComprobantesDAO]......." + e.getMessage() + " - " + e.getErrorCode() );

        }
finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }

    }



    /**
     *Metodo que permite buscar los datos de egreso
     *@param document numero del egreso
     *@param transaction numero de la transaccion
     *@autor: mfontalvo
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public Hashtable searchEgreso( String document, String transaction ) throws SQLException {
        PreparedStatement st = null;
        ResultSet rs = null;
        Hashtable egreso = null;
        Connection con = null;
        String query = "SQL_SEARCH_EGRESO";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, document);
            st.setString(2, transaction);
            rs = st.executeQuery();
            if (rs.next()) {
                egreso = new Hashtable();
                egreso.put("reg_status", rs.getString("reg_status"));
                egreso.put("dstrct", rs.getString("dstrct"));
                egreso.put("branch_code", rs.getString("branch_code"));
                egreso.put("bank_account_no", rs.getString("bank_account_no"));
                egreso.put("document_no", rs.getString("document_no"));
                egreso.put("nit", rs.getString("nit"));
            }
        } catch (SQLException e) {
            throw new SQLException("Error en searchEgreso [ComprobantesDAO]......." + e.getMessage() + " - " + e.getErrorCode());
        }
finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return egreso;
    }



    /**
     *Metodo que busca una lista de egresos sin contabilizar
     *@autor Ing. Andr�s Maturana De La Cruz
     *@param dstrct Distrito.
     *@param fechai Fecha inicial del per�odo.
     *@param fechaf Fecha final del per�odo.
     *@throws En caso de que un error de base de datos ocurra.
     */
    public void consultaComprobantes(String dstrct, String fechai, String fechaf) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_CONSULTA_COMPRO";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            Logger logger = Logger.getLogger(this.getClass());
            st.setString(1, dstrct);
            st.setString(2, fechai);
            st.setString(3, fechaf);
            logger.info("CONSULTA COMPROBANTES: " + st);

            rs = st.executeQuery();

            vector = new Vector();
            while (rs.next()) {
                Comprobantes comprobante = new Comprobantes();
                comprobante.setTipodoc_rel(rs.getString("doc_name"));//Descripcion del tipo doc
                comprobante.setTipodoc(rs.getString("tipodoc"));
                comprobante.setNumdoc(rs.getString("numdoc"));
                comprobante.setGrupo_transaccion(rs.getInt("grupo_transaccion"));
                comprobante.setPeriodo(rs.getString("periodo"));
                comprobante.setFechadoc(rs.getString("fechadoc"));
                comprobante.setDetalle(rs.getString("detalle"));
                comprobante.setTercero(rs.getString("tercero"));
                comprobante.setTotal_debito(rs.getDouble("total_debito"));
                comprobante.setTotal_credito(rs.getDouble("total_credito"));
                comprobante.setMoneda(rs.getString("moneda"));
                comprobante.setFecha_aplicacion(rs.getString("fecha_aplicacion"));
                comprobante.setAprobador(rs.getString("aprobador_name"));//**
                comprobante.setUsuario_aplicacion(rs.getString("usuario_aplicacion_name"));//**
                comprobante.setSucursal(rs.getString("sucursal"));
                comprobante.setTipo_operacion(rs.getString("tipo_operacion"));
                vector.add(comprobante);
                comprobante = null; //Liberar Espacio JJCastro
            }
        } catch (SQLException e) {
            throw new SQLException("Error en consultaComprobantes [ComprobantesDAO]......." + e.getMessage() + " - " + e.getErrorCode());
        }
finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }

    }


    /**
     *Metodo que busca una lista de egresos sin contabilizar
     *@autor Ing. Andr�s Maturana De La Cruz
     *@param dstrct Distrito.
     *@param fechai Fecha inicial del per�odo.
     *@param fechaf Fecha final del per�odo.
     *@throws En caso de que un error de base de datos ocurra.
     */
    public void consultaComprobantesDetalle(String dstrct, String fechai, String fechaf) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs  =null;
        String query = "SQL_CONSULTA_COMPRO_DET";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            Logger logger = Logger.getLogger(this.getClass());
            st.setString(1, dstrct);
            st.setString(2, fechai);
            st.setString(3, fechaf);
            logger.info("CONSULTA COMPROBANTES DETALLE: " + st);
            rs = st.executeQuery();
            reporte = new Vector();
            while (rs.next()){
                ComprobanteFacturas comprobantes = new ComprobanteFacturas();
                comprobantes.setDstrct  (rs.getString("dstrct")  );
                comprobantes.setTipodoc(rs.getString("tipodoc") );
                //comprobantes.setTipodoc_rel(rs.getString("doc_name"));//Descripcion del tipo doc
                comprobantes.setNumdoc  (rs.getString("numdoc")  );
                comprobantes.setSucursal(rs.getString("sucursal"));
                comprobantes.setPeriodo(rs.getString("periodo")  );
                comprobantes.setFechadoc(rs.getString("fechadoc"));
                comprobantes.setDetalle(rs.getString("detalle")  );
                comprobantes.setTercero(rs.getString("tercero")  );
                comprobantes.setTipo("C");
                comprobantes.setTotal_debito(rs.getDouble("total_debito") );
                comprobantes.setTotal_credito(rs.getDouble("total_credito")  );
                comprobantes.setMoneda(rs.getString("moneda"));
                comprobantes.setAprobador(rs.getString("aprobador"));
                comprobantes.setBase(rs.getString("base"));
                comprobantes.setGrupo_transaccion(rs.getInt("grupo_transaccion"));
                comprobantes.setFechaCreacion(rs.getString("creation_date"));
                comprobantes.setFecha_aplicacion(rs.getString("fecha_aplicacion"));
                comprobantes.setUsuario_aplicacion(rs.getString("usuario_aplicacion"));
                comprobantes.setSucursal(rs.getString("sucursal"));
                comprobantes.setTipo_operacion(rs.getString("tipo_operacion"));

                /* Info del Item */
                List  items   = new LinkedList();
                ComprobanteFacturas comprodet = new ComprobanteFacturas();
                comprodet.setDstrct(dstrct);
                comprodet.setTipodoc(comprobantes.getTipodoc());
                comprodet.setNumdoc(comprobantes.getNumdoc());
                comprodet.setGrupo_transaccion(comprobantes.getGrupo_transaccion());
                comprodet.setPeriodo(rs.getString("periodo")  );
                comprodet.setCuenta(rs.getString("cuenta"));
                comprodet.setAuxiliar(rs.getString("auxiliar"));
                comprodet.setDetalle(rs.getString("detalle"));
                comprodet.setTotal_debito(rs.getDouble("valor_debito"));
                comprodet.setTotal_credito(rs.getDouble("valor_credito"));
                comprodet.setTercero(rs.getString("tercero"));
                comprodet.setBase(rs.getString("base"));
                comprodet.setDocumento_interno(rs.getString("documento_interno"));
                comprodet.setExisteCuenta(true);
                comprodet.setTipo("D");
                comprodet.setTipo_docrelacionado(rs.getString("tipodoc_rel"));
                comprodet.setDocrelacionado(rs.getString("documento_rel"));

                items.add( comprodet );
                /**************************************************************/

                comprobantes.setItems(items);
                comprobantes.setTotal_items(1);

                reporte.add(comprobantes);
                comprobantes = null; //Liberar Espacio JJCastro
            }
        } catch( SQLException e ){

            throw new SQLException( "Error en consultaComprobantesDetalle [ComprobantesDAO]......." + e.getMessage() + " - " + e.getErrorCode() );

        }
finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }

    }

    /**
     * Getter for property reporte.
     * @return Value of property reporte.
     */
    public java.util.Vector getReporte() {
        return reporte;
    }

    /**
     * Setter for property reporte.
     * @param reporte New value of property reporte.
     */
    public void setReporte(java.util.Vector reporte) {
        this.reporte = reporte;
    }
    /*
     *@author jescandon
     **/
    public String descontabilizar( String distrito, String tipo_doc, String documento, int transaccion_num, Usuario usuario  ){
        String query = "";
        try{

            String fecha_actual = Util.getFechaActual_String(6);
            int grupTrans = 0;

            com.tsp.finanzas.contab.model.Model modFin = new com.tsp.finanzas.contab.model.Model(usuario.getBd());

            modFin.comprobanteService.getComprobante( tipo_doc, documento, transaccion_num );

            com.tsp.finanzas.contab.model.beans.Comprobantes comp = modFin.comprobanteService.getComprobante();

            double credito = 0;
            double debito  = 0;

            /*Creacion del Nuevo del Objeto Comprobante*/
            int grupo_transaccion   = comp.getGrupo_transaccion();
            credito                 = comp.getTotal_credito();
            debito                  = comp.getTotal_debito();
            comp.setTotal_debito( credito);
            comp.setTotal_credito( debito );
            comp.setDstrct( distrito );
            comp.setSucursal( "OP" );
            comp.setFechadoc( Util.getFechaActual_String(4) );
            comp.setDetalle( "DESCONTABILIZACION  "+ comp.getDetalle() );
            comp.setPeriodo( fecha_actual.substring(0,4) + fecha_actual.substring(5,7) );
            comp.setGrupo_transaccion( modFin.comprobanteService.getGrupoTransaccion() );


            grupTrans = comp.getGrupo_transaccion();
            comp.setUsuario( usuario.getLogin() );
            comp.setBase( usuario.getBase() );
            modFin.comprobanteService.setComprobante( comp );

            query += modFin.comprobanteService.getInsert() + ";";

            comp.setGrupo_transaccion( grupo_transaccion );

            modFin.comprobanteService.getDetallesComprobante(comp);
            for( int k=0; k < modFin.comprobanteService.getVector().size(); k++ ){
                com.tsp.finanzas.contab.model.beans.Comprobantes compVec = ( com.tsp.finanzas.contab.model.beans.Comprobantes )modFin.comprobanteService.getVector().get( k );
                compVec.setPeriodo( fecha_actual.substring(0,4) + fecha_actual.substring(5,7) );
                credito = 0;
                debito  = 0;
                credito = compVec.getTotal_credito();
                debito  = compVec.getTotal_debito();
                compVec.setTotal_debito( credito );
                compVec.setTotal_credito( debito );
                compVec.setGrupo_transaccion( grupTrans );
                compVec.setDetalle( "DESCONTABILIZACION "+ compVec.getDetalle() );
                compVec.setDocumento_interno( tipo_doc );
                compVec.setUsuario( usuario.getLogin() );
                compVec.setBase( usuario.getBase() );
                modFin.comprobanteService.setComprobante( compVec );
                query += modFin.comprobanteService.getInsertItem()+ ";";
            }

        }catch(Exception e){
            e.printStackTrace();
        }
        return query;
    }

    /**
     *Metodo que obtiene un camprobante no contabilizado en base a su llave primaria
     *@param tipodoc Tipo de documento
     *@param numdoc Numero de docuento
     *@param grupo_transaccion Grupo de transaccion
     *@autor: David Pi�a
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void getComprobante( String tipodoc, String numdoc, int grupo_transaccion ) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs  =null;
        String query = "SQL_GET_COMPROBANTE";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString( 1, tipodoc );
            st.setString( 2, numdoc );
            st.setInt( 3, grupo_transaccion );
            rs = st.executeQuery();
            comprobante = null;
            while (rs.next()){
                comprobante = new Comprobantes();
                comprobante.setDstrct( (rs.getString("dstrct")!=null)?rs.getString("dstrct"):"" );
                comprobante.setTipodoc( (rs.getString("tipodoc")!=null)?rs.getString("tipodoc"):"" );
                comprobante.setNumdoc( (rs.getString("numdoc")!=null)?rs.getString("numdoc"):"" );
                comprobante.setGrupo_transaccion( rs.getInt("grupo_transaccion") );
                comprobante.setPeriodo( (rs.getString("periodo")!=null)?rs.getString("periodo"):"" );
                comprobante.setTercero( (rs.getString("tercero")!=null)?rs.getString("tercero"):"" );
                comprobante.setTotal_items( rs.getInt("total_items") );
                comprobante.setTotal_debito( rs.getDouble("total_debito") );
                comprobante.setTotal_credito( rs.getDouble("total_credito") );
                comprobante.setFecha_aplicacion( (rs.getString("fecha_aplicacion")!=null)?rs.getString("fecha_aplicacion"):"" );

                comprobante.setMoneda( (rs.getString("moneda")!=null)?rs.getString("moneda"):"" ); //jose 2007-01-23
                comprobante.setAprobador( (rs.getString("aprobador")!=null)?rs.getString("aprobador"):"" ); //jose 2007-01-23
                comprobante.setTipo_operacion( (rs.getString("tipo_operacion")!=null)?rs.getString("tipo_operacion"):"" ); //jose 2007-01-23

                comprobante.setDetalle( (rs.getString("detalle")!=null)?rs.getString("detalle"):"" ); //JuanM 2007-01-24
            }
        } catch( SQLException e ){
            throw new SQLException( "Error en getComprobante [ComprobantesDAO]......." + e.getMessage() + " - " + e.getErrorCode() );
        }
finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
    }


    public String equivalenciaTipo_documento( String tipo_documento ) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs  =   null;
        String query  = "SQL_EQUIVALENCIA_DOCUMENTO";
        String codigo = "";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, tipo_documento);
            rs = st.executeQuery();
            while (rs.next()){
                codigo = ( rs.getString("codigo") != null )?rs.getString("codigo"):"" ;
            }

        } catch( SQLException e ){
            throw new SQLException( "Error en equivalenciaTipo_documento [ComprobantesDAO]......." + e.getMessage() + " - " + e.getErrorCode() );
        }
finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return codigo;
    }




    public String descontabilizarOP( String factura ) throws SQLException {
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_UPDATE_OP";
        String consulta = "";
        Connection con = null;
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, factura);
            st.setString(2, factura);

            consulta = st.toString();

        } catch (SQLException e) {
            throw new SQLException("Error en descontabilizarOP [ComprobantesDAO]......." + e.getMessage() + " - " + e.getErrorCode());
        }
finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return consulta;
    }


    public String descontabilizarFactura( String usuario_anulo, int transaccion_anulacion, String distrito, String tipo_documento , String documento ) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs  =null;
        String query = "SQL_UPDATE_FACTURA";
        String consulta = "";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, usuario_anulo);
            st.setInt(2, transaccion_anulacion);
            st.setString(3, distrito);
            st.setString(4, tipo_documento);
            st.setString(5, documento);
            consulta = st.toString();

        } catch( SQLException e ){
            throw new SQLException( "Error en descontabilizarFactura [ComprobantesDAO]......." + e.getMessage() + " - " + e.getErrorCode() );
        }
finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return consulta;
    }




    /**
     *Metodo que modifica el egreso y coloca la fecha de contabilizacion
     *el periodo y el numero de la transaccion
     * @return 
     * @throws java.lang.Exception
     *@autor: Karen Reales
     */
    public ArrayList<String> marcarEgreso() throws Exception {
        Connection con = null;
        StringStatement st = null;
        StringStatement st2 = null;
        ArrayList<String> sql = new ArrayList<String>();
        String query = "SQL_UPDATE_EGRESO";
        try {
            con = this.conectarJNDI(query);
            st = new StringStatement(this.obtenerSQL(query), true);
            st.setString(1, comprobante.getPeriodo());
            st.setInt(2, comprobante.getGrupo_transaccion());
            st.setString(3, comprobante.getBanco());
            st.setString(4, comprobante.getSucursal_banco());
            st.setString(5, comprobante.getNumdoc());
            sql.add(st.getSql());

            st2 = new StringStatement(this.obtenerSQL("SQL_UPDATE_EGRESODET"),true);
            st2.setInt(1, comprobante.getGrupo_transaccion());
            st2.setString(2, comprobante.getBanco());
            st2.setString(3, comprobante.getSucursal_banco());
            st2.setString(4, comprobante.getNumdoc());
            sql.add(st2.getSql());

        } catch (SQLException e) {
            throw new SQLException("Error en marcarEgresoDet [ComprobantesDAO]......." + e.getMessage() + " - " + e.getErrorCode());
        } finally {
            if (st != null) {
                try {
                    st = null;
                } catch (Exception e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return sql;
    }









    public String descontabilizarFactura( String usuario_anulo, int transaccion_anulacion, String distrito, String proveedor, String tipo_documento, String documento ) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_UPDATE_FACTURA";
        String consulta = "";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, usuario_anulo);
            st.setInt(2, transaccion_anulacion);
            st.setString(3, distrito);
            st.setString(4, proveedor);
            st.setString(5, tipo_documento);
            st.setString(6, documento);
            consulta = st.toString();

        } catch (SQLException e) {
            throw new SQLException("Error en descontabilizarFactura [ComprobantesDAO]......." + e.getMessage() + " - " + e.getErrorCode());
        }
finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return consulta;
    }

   public void buscarEgresosDet(Comprobantes compro) throws SQLException {
        Connection         con     = null;
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        PreparedStatement  st2     = null;
        ResultSet          rs2     = null;

        String             query1  = "SQL_BUSCAR_EGRESOS_DETALLADOS";

        try {

            con = this.conectarJNDI(query1);
            if(con!=null){
                //String banco    = comprobante.getBanco();
               // String sucursal = comprobante.getSucursal_banco();

                st = con.prepareStatement(this.obtenerSQL(query1));//JJCastro fase2
                st.setString(1, compro.getBanco());
                st.setString(2, compro.getSucursal_banco());
                st.setString(3, compro.getNumdoc());
                System.out.println("Query 10 "+st.toString());
                rs = st.executeQuery();
                vector = new Vector();
                while (rs.next()){

                    Comprobantes comprobante = new Comprobantes();
                    comprobante.setNumdoc(rs.getString("document_no"));
                    System.out.println("en while"+comprobante.getNumdoc());
                    comprobante.setSucursal("OP");
                    comprobante.setTercero(rs.getString("nit"));
                    comprobante.setTotal_debito(rs.getDouble("vlr")==0?rs.getDouble("vlr"):rs.getDouble("vlr"));
                    comprobante.setMoneda(rs.getString("currency"));
                    comprobante.setBase(rs.getString("base"));
                    comprobante.setCuenta(rs.getString("cuentaprov")!=null?rs.getString("cuentaprov"):"");
                    comprobante.setTieneProv(rs.getBoolean("tiene_provee"));
                    comprobante.setTipodoc_rel(rs.getString("tipo_documento")!=null?rs.getString("tipo_documento"):"");
                    comprobante.setNumdoc_rel(rs.getString("documento")!=null?rs.getString("documento"):"");
                    comprobante.setDetalle(rs.getString("nomb"));
                    comprobante.setAuxiliar(rs.getString("auxiliar")); //27- abril -2009
                    comprobante.setTipo("D");
                    String dbcr = rs.getString("dbcr"); //27- abril -2009
                    comprobante.setTdoc_rel(rs.getString("tipodocRel"));//fdiaz 25/05/2010
                    comprobante.setNumdoc_rel(rs.getString("documento_rel"));//fdiaz 25/05/2010
                    if(rs.getString("cuentaprov").equalsIgnoreCase("22050102")||rs.getString("cuentaprov").equalsIgnoreCase("23050502")){//TMolina 2008-09-15
                            double diferencia = rs.getDouble("vlr_fact");
                            comprobante.setTotal_credito(0.0);
                            comprobante.setTotal_debito(rs.getDouble("vlr"));
                            comprobante.setAuxiliar("AR-"+rs.getString("nit"));
                            comprobante.setComentario("Egreso");

                    }
                    //Mod pronto pago  27- abril -2009
                    if(!dbcr.equals("")){
                        //System.out.println(dbcr);
                        double valor = comprobante.getTotal_debito();
                        //dbcr = C (valor debe import en credito) Y dbcr = 'D' cuando valor debe import en debito
                        if(dbcr.equals("C")){
                            comprobante.setTotal_debito(0.0);
                            comprobante.setTotal_credito(valor*-1);
                        }else{
                            comprobante.setTotal_debito(valor);
                            comprobante.setTotal_credito(0.0);
                        }
                    }
                    System.out.println("comprobante"+comprobante);
                    //End mod pronto pago 27- abril -2009
                    vector.add(comprobante);
                    comprobante = null; // Liberar Espacio
                }

            }

        } catch( SQLException e ){
            System.out.println("ocurrio error en buscarEgresosDet_"+e.toString()+"__"+e.getMessage());
            e.printStackTrace();
            throw new SQLException( "Error en buscarEgresosDet [ComprobantesDAO]......." + e.getMessage()  );
        }
finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }

    }

    public void buscarEgresosDet() throws SQLException {

        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs  =null, rs2=null;
        String query = "SQL_BUSCAR_EGRESOS_DETALLADOS";
        try {
            con = this.conectarJNDI(query);

            if(con!=null){

                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                //st.setString(1, comprobante.getTipodoc());
                st.setString(1, comprobante.getBanco());
                st.setString(2, comprobante.getSucursal_banco());
                st.setString(3, comprobante.getNumdoc());
                String banco=comprobante.getBanco();
                String sucursal =comprobante.getSucursal_banco();
                rs=st.executeQuery();
                // System.out.println("Query "+st.toString());
                vector = new Vector();

                while (rs.next()){
                    Comprobantes comprobante = new Comprobantes();
                    comprobante.setNumdoc(rs.getString("document_no"));
                    comprobante.setSucursal("OP");
                    comprobante.setDetalle("EGRESO "+rs.getString("document_no")+" "+rs.getString("concept_desc")+" "+rs.getString("documento"));
                    comprobante.setTercero(rs.getString("nit"));
                    comprobante.setTotal_debito(rs.getDouble("vlr")==0?rs.getDouble("vlr"):rs.getDouble("vlr"));
                    comprobante.setMoneda(rs.getString("currency"));
                    comprobante.setBase(rs.getString("base"));
                    comprobante.setCmc(rs.getString("cuentaprov")!=null?rs.getString("cuentaprov"):"");
                    comprobante.setTieneProv(rs.getBoolean("tiene_provee"));
                    comprobante.setTipodoc_rel(rs.getString("tipo_documento")!=null?rs.getString("tipo_documento"):"");
                    comprobante.setNumdoc_rel(rs.getString("documento")!=null?rs.getString("documento"):"");
                    comprobante.setIsDifCambio(false);
                    comprobante.setCuenta("G01C009107027");
                    comprobante.setErrorDifCambio(false);

                    if(rs.getString("tipo_pago").equalsIgnoreCase("C") && rs.getDouble("vlr_neto_me")==0){
                        if(rs.getDouble("vlr_fact")!=0){

                            comprobante.setIsDifCambio(true);
                            comprobante.setErrorDifCambio(rs.getBoolean("errorSaldo"));
                            double diferencia = rs.getDouble("vlr_fact");
                            comprobante.setTotal_credito(diferencia<0?diferencia*-1:diferencia);
                            comprobante.setTipo_valor(diferencia<0?"D":"C");
                            comprobante.setComentario("EGRESO "+rs.getString("document_no")+" DIF EN CAMBIO "+ rs.getString("concept_desc")+" "+rs.getString("documento"));
                            comprobante.setTotal_debito(rs.getDouble("vlr")+diferencia);

                        }
                    }
                    if(rs.getString("concept_code").equals("45")){
                        if(rs.getString("description")!=null){
                            System.out.println(rs.getString("description"));
                            String descripcion[] = rs.getString("description").split("/");
                            String bancoNuevo = descripcion.length>0?descripcion[0]:"";
                            String sucursalNuevo =descripcion.length>1?descripcion[1]:"";;

                            st = con.prepareStatement(this.obtenerSQL( "SQL_BUSCAR_CUENTABANCO" ));
                            st.setString(1, bancoNuevo);
                            st.setString(2, sucursalNuevo);
                            rs2=st.executeQuery();
                            System.out.println("Banco "+st);
                            if(rs2.next()){
                                comprobante.setCmc(rs2.getString("codigo_cuenta")!=null?rs2.getString("codigo_cuenta"):"");
                            }
                        }
                        System.out.println("Termine de buscar la otra cuenta...");
                    }
                    vector.add(comprobante);
                    comprobante = null; //Liberar Espacio JJCastro

                }
            }
            System.out.println("FIN TOTAL DE LA FUNCION...");
        } catch( SQLException e ){

            throw new SQLException( "Error en buscarEgresosDet [ComprobantesDAO]......." + e.getMessage() + " - " + e.getErrorCode() );

        }

finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }

    }

    public String descontabilizarOP( String factura, String numpla ) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_UPDATE_OP";
        String consulta = "";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, numpla);
            st.setString(2, factura);
            st.setString(3, numpla);
            st.setString(4, factura);

            consulta = st.toString();

        } catch (SQLException e) {
            throw new SQLException("Error en descontabilizarOP [ComprobantesDAO]......." + e.getMessage() + " - " + e.getErrorCode());
        }
finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return consulta;
    }

    /**
     *Metodo busca una lista de mov.remesas sin contabilizar dado un tipo de documento
     *@param: Tipo de documento para buscar cmc
     *@autor: Karen Reales
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void buscarMovRemesas(String tipoDoc) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs  =null;
        String query = "SQL_BUSCAR_MOVREMESAS";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, tipoDoc);
            rs=st.executeQuery();
            System.out.println("Query "+st.toString());
            vector = new Vector();
            while (rs.next()){

                Comprobantes comprobante = new Comprobantes();
                comprobante.setNumdoc(rs.getString("numrem"));
                comprobante.setSucursal("OP");
                comprobante.setDetalle("AJUSTE REMESA "+rs.getString("numrem"));
                comprobante.setTercero(rs.getString("cliente"));
                comprobante.setTotal_debito(rs.getDouble("vlrrem2_aj"));
                comprobante.setTotal_credito(rs.getDouble("vlrrem2_aj"));
                comprobante.setMoneda(rs.getString("currency")!=null?rs.getString("currency"):"");
                comprobante.setBase(rs.getString("base"));
                comprobante.setCmc(rs.getString("cuenta")!=null?rs.getString("cuenta"):"");
                comprobante.setAccount_code_c(rs.getString("account_code_i"));
                comprobante.setTipodoc_rel("002");
                comprobante.setNumdoc_rel(rs.getString("numrem"));
                comprobante.setTieneProv(rs.getBoolean("tiene_cliente"));
                comprobante.setNumrem(rs.getString("id"));
                vector.add(comprobante);
                comprobante = null; //Liberar Espacio JJCastro

            }
        } catch( SQLException e ){

            throw new SQLException( "Error en buscarMovRemesas [ComprobantesDAO]......." + e.getMessage() + " - " + e.getErrorCode() );

        }

finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }

    }

    /**
     *Metodo que modifica la remesa y coloca la fecha de contabilizacion
     *el periodo y el numero de la transaccion
     *@autor: Karen Reales
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public String marcarMovRemesa() throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        String sql="";
        String query = "SQL_UPDATE_MOVREMESA";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, comprobante.getPeriodo());
            st.setInt(2, comprobante.getGrupo_transaccion());
            st.setString(3, comprobante.getNumrem());
            st.setString(4, comprobante.getNumdoc());
            sql = st.toString();

        } catch( SQLException e ){
            throw new SQLException( "Error en marcarMovRemesa [ComprobantesDAO]......." + e.getMessage() + " - " + e.getErrorCode() );
        }
finally {
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return sql ;

    }

    //Kreales 01-05-07
    /**
     *Metodo que busca una una planilla dada y los anticipos de la planilla
     *@param: Numero de la planilla
     *@autor: Karen Reales
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void buscarPlanilla(String numpla) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs  =null;
        String query = "SQL_BUSCAR_PLANILLA";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, numpla);
            rs=st.executeQuery();
            vector = new Vector();
            if (rs.next()){
                Comprobantes comprobante = new Comprobantes();
                comprobante.setNumdoc(rs.getString("numpla"));
                comprobante.setSucursal("OP");
                comprobante.setDetalle("CAUSACION PLANILLA "+rs.getString("numpla"));
                comprobante.setTercero(rs.getString("nitpro"));
                comprobante.setTotal_debito(rs.getDouble("vlrpla2"));
                comprobante.setTotal_credito(rs.getDouble("vlrpla2"));
                comprobante.setMoneda(rs.getString("currency"));
                comprobante.setBase(rs.getString("base"));
                comprobante.setCmc("");
                comprobante.setTipo_valor("P");
                comprobante.setCuenta("");
                comprobante.setTipodoc_rel("001");
                comprobante.setNumdoc_rel(rs.getString("numpla"));

                //comprobante.setCmc(rs.getString("cmc"));
                comprobante.setTieneProv(true);
                vector.add(comprobante);
                comprobante = null;//Liberar Espacio JJCastro
            }

           /* st = this.crearPreparedStatement( "SQL_BUSCAR_MOVPLA_PLANILLA" );
            st.setString(1, numpla);
            rs=st.executeQuery();
            while (rs.next()){

                Comprobantes comprobante = new Comprobantes();
                comprobante.setNumdoc(rs.getString("planilla"));
                comprobante.setSucursal("OP");
                comprobante.setDetalle("MOVIMIENTO PLANILLA "+rs.getString("planilla"));
                comprobante.setTercero(rs.getString("nitpro"));
                comprobante.setTotal_debito(rs.getDouble("vlrmp"));
                comprobante.setTotal_credito(rs.getDouble("vlrmp"));
                comprobante.setMoneda(rs.getString("currency"));
                comprobante.setBase(rs.getString("base"));
                comprobante.setCmc("");
                comprobante.setTieneProv(true);
                comprobante.setTipo_valor("M");
                comprobante.setTipodoc_rel("001");
                comprobante.setNumdoc_rel(rs.getString("planilla"));
                comprobante.setCuenta(rs.getString("account"));
                vector.add(comprobante);

            }*/

        } catch( SQLException e ){

            throw new SQLException( "Error en buscarPlanilla [ComprobantesDAO]......." + e.getMessage() + " - " + e.getErrorCode() );

        }

finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }

    }

    /**
     *Metodo que permite actualizar la fecha de aplicacion de un comprobante
     *@autor: David Pi�a
     *@param comprobante contiene la informaci�n o filtros para obtener el detalle de un
     *                   compobante
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void updateAplicacionComprobante( Comprobantes comprobante ) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs  =null;
        String query = "SQL_UPDATE_APLICACION_COMPROBANTE";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString( 1, comprobante.getUsuario() );
            st.setString( 2, comprobante.getUsuario() );
            st.setString( 3, comprobante.getTipodoc() );
            st.setString( 4, comprobante.getNumdoc() );
            st.setInt( 5, comprobante.getGrupo_transaccion() );
            st.executeUpdate();

        } catch( SQLException e ){
            throw new SQLException( "Error en updateAplicacionComprobante [ComprobantesDAO]......." + e.getMessage() + " - " + e.getErrorCode() );
        }
finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
    }

      /**
     *Metodo que busca una lista de egresos sin contabilizar
     *@autor Ing. jose de la rosa
     *@param dstrct Distrito.
     *@param fechai Fecha inicial del per�odo.
     *@param fechaf Fecha final del per�odo.
     *@param tipodoc Tipo documento.
     *@param documento Documento.
     *@throws En caso de que un error de base de datos ocurra.
     */
    public void consultaReporteComprobantes(String dstrct, String fechai, String fechaf, String tipodoc, String documento ) throws SQLException {
        StringStatement st      = null;
        ResultSet       rs      = null;
        Connection      con     = null;
        String          query   = "SQL_CONSULTA_REP_COMPROBANTES";
                        reporte = null;
        try {
            con        = conectarJNDI( query );
            String sql = obtenerSQL  ( query );
            sql        = sql.replaceAll("#TIPO#", !tipodoc.equals("")?" a.tipodoc = '"+tipodoc+"' AND ":"" );
            sql        = sql.replaceAll("#DOCUMENTO#", !documento.equals("")?" a.numdoc = '"+documento+"' AND ":"" );
            st         =   new StringStatement ( sql );
            st.setString( dstrct );
            st.setString( fechai );
            st.setString( fechaf );
            rs  =   con.createStatement().executeQuery( st.getSql() );
            reporte = new Vector();
            while (rs.next()){
                ComprobanteFacturas comprobantes = new ComprobanteFacturas();
                comprobantes.setTipodoc          ( rs.getString("tipodoc") );
                comprobantes.setTipoCmc          ( rs.getString("tipo") );
                comprobantes.setNumdoc           ( rs.getString("numdoc")  );
                comprobantes.setPeriodo          ( rs.getString("periodo")  );
                comprobantes.setGrupo_transaccion( rs.getInt("grupo_transaccion"));
                comprobantes.setDetalle          ( rs.getString("detalle")  );
                comprobantes.setTercero          ( rs.getString("tercero")  );
                comprobantes.setTotal_debito     ( rs.getDouble("total_debito") );
                comprobantes.setTotal_credito    ( rs.getDouble("total_credito")  );
                comprobantes.setFecha_aplicacion ( rs.getString("fecha_aplicacion").equals("0099-01-01")? "" : rs.getString("fecha_aplicacion") );
                reporte.add( comprobantes );
                comprobantes = null; // Liberar Espacio
            }
        } catch( Exception e ){
            e.printStackTrace();
            throw new SQLException( "Error en consultaReporteComprobantes [ComprobantesDAO]......." + e.getMessage()  );
        }
        finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null)  st = null;
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }

    }

    /**
     *Metodo que permite actualizar el periodo de un comprobante
     *@autor: Ing. jose de la rosa
     *@param periodo, tipo documento, documento, grupo transaccion
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void updatePeriodoComprobante( String periodo, String tipo, String documento, int transaccion ) throws SQLException {
        StringStatement st      = null;
        String          query   = "SQL_UPDATE_PERIODO_COMPROBANTES";
        Connection      con     = null;
        try {
            con =   this.conectarJNDI   ( query );
            st  =   new StringStatement ( this.obtenerSQL( query ) );
            st.setString( periodo );
            st.setString( tipo );
            st.setString( documento );
            st.setInt   ( transaccion );
            con.createStatement().executeUpdate( st.getSql() );

        } catch( Exception e ){
            e.printStackTrace();
            throw new SQLException( "Error en updatePeriodoComprobante [ComprobantesDAO]......." + e.getMessage()  );
        }
        finally{
            if (st != null) st = null;
            if (con != null) { try { desconectar(con); } catch (SQLException sqle) {} con = null; }
        }
    }

     /**
     *Metodo que permite obtener la todos los comprobantes con
     *no tengan fecha de aplicaci�n es decir la fecha por defecto
     *del campo
     *@param fechaInicial la fecha de inicio del comprobante
     *@param fechaFinal la fecha final del comprobante
     *@autor: Mario Fontalvo
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void getComprobanteParaMayorizar( String tipodoc, String numdoc, int grupo_transaccion ) throws SQLException {

        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs  =null;
        comprobante = null;
        try {

            con = this.conectarJNDI( "SQL_COMPROBANTES_PARA_MAYORIZAR" );
            st = con.prepareStatement(this.obtenerSQL( "SQL_COMPROBANTES_PARA_MAYORIZAR" ));
            st.setString(1, tipodoc   );
            st.setString(2, numdoc    );
            st.setInt   (3, grupo_transaccion );
            rs = st.executeQuery();
            if (rs.next()){
                comprobante = new Comprobantes();
                comprobante.setDstrct( (rs.getString("dstrct")!=null)?rs.getString("dstrct"):"" );
                comprobante.setTipodoc( (rs.getString("tipodoc")!=null)?rs.getString("tipodoc"):"" );
                comprobante.setNumdoc( (rs.getString("numdoc")!=null)?rs.getString("numdoc"):"" );
                comprobante.setGrupo_transaccion( rs.getInt("grupo_transaccion") );
                comprobante.setPeriodo( (rs.getString("periodo")!=null)?rs.getString("periodo"):"" );
                comprobante.setTercero( (rs.getString("tercero")!=null)?rs.getString("tercero"):"" );
                comprobante.setTotal_items( rs.getInt("total_items") );
                comprobante.setTotal_debito( rs.getDouble("total_debito") );
                comprobante.setTotal_credito( rs.getDouble("total_credito") );
                comprobante.setSum_debito( rs.getDouble("sum_debito_detalle") );
                comprobante.setSum_credito( rs.getDouble("sum_credito_detalle") );
            }
        } catch( Exception e ){
            e.printStackTrace();
            throw new SQLException( "Error en getComprobantesSinAplicacion [ComprobantesDAO]......." + e.getMessage()  );
        }
        finally{
            if (st != null) {
                try { st.close(); } catch (SQLException sqle) {}
                st = null;
            }
            if (rs != null) {
                try { rs.close(); } catch (SQLException sqle) {}
                rs = null;
            }
            if (con != null) {
                try { desconectar(con); } catch (SQLException sqle) {}
                con = null;
            }
        }
    }

    /**
     *Metodo que busca una lista de egresos sin contabilizar
     *@autor: Karen Reales
     *@throws: En caso de que un error de base de datos ocurra.
     */
    // MODIFICADO POR : FVILLACOB  JUN 13 / 07 [ Implemantar JNDI y Optimizaci�n c�digo ]
    public void buscarEgresos() throws SQLException {
        Connection         con     = null;
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        String             query   = "SQL_BUSCAR_EGRESOS";
        try {
            con        = conectarJNDI( query );
            String sql = obtenerSQL  ( query );
            st         = con.prepareStatement( sql );
            System.out.println("Buscar Egresos"+st.toString());
            rs=st.executeQuery();
            vector = new Vector();
            while (rs.next()){
                Comprobantes comprobante = new Comprobantes();
                    comprobante.setNumdoc(rs.getString("document_no"));
                    comprobante.setSucursal("OP");
                    comprobante.setDetalle("EGRESO "+rs.getString("document_no"));
                    comprobante.setTercero(rs.getString("nit"));
                    comprobante.setTotal_debito(rs.getDouble("vlr"));
                    comprobante.setTotal_credito(rs.getDouble("vlr"));
                    comprobante.setMoneda(rs.getString("currency"));
                    comprobante.setBase(rs.getString("base"));
                    comprobante.setBanco(rs.getString("branch_code"));
                    comprobante.setSucursal_banco(rs.getString("bank_account_no"));
                    comprobante.setCuenta(rs.getString("codigo_cuenta")!=null?rs.getString("codigo_cuenta"):"");
                    comprobante.setTipodoc_rel("003");
                    comprobante.setNumdoc_rel(rs.getString("document_no"));
                    comprobante.setRef_1 (rs.getString("branch_code"));
                    comprobante.setRef_2 (rs.getString("bank_account_no"));
                    comprobante.setComentario(rs.getString("comision"));
                    comprobante.setValor_for(rs.getDouble("cuatroxmil"));//Tmolina 2008-09-15
                    comprobante.setDstrct("FINV");
                    comprobante.setTipodoc("EGR");
                    //System.out.println("Query 25 "+rs.getString("comision"));
                vector.add(comprobante);
                comprobante = null; // Liberar Espacio
            }


        } catch( Exception e ){
            throw new SQLException( "Error en buscarEgresos [ComprobantesDAO]......." + e.getMessage()  );
        }
        finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }

    /**
     *Metodo que permite obtener la cuenta de aprovechamineto dependiendo si es comision o cuantro x mil
     *y/o si es un negocio Fenalco o un prestamo.
     *@param tipo tipo de tabla
     *@param codEgreso codigo del egreso
     *@autor tmolina
     *@return cuenta
     *@throws SQLException En caso de que un error de base de datos ocurra.
     */
    public String getCuentaAprovechamiento( String tipo, String codEgreso ) throws SQLException {
        Connection con       = null;
        PreparedStatement st = null;
        ResultSet rs         = null;
        String cuenta        = "";
        String query = "SQL_CUENTA_APROVECHAMIENTO";
        try{
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, codEgreso);
            st.setString(2, tipo);
            rs = st.executeQuery();
            if (rs.next()){
               cuenta = rs.getString("cuenta");
            }
        }catch( Exception e ){
            e.printStackTrace();
            throw new SQLException( "Error en getCuentaAprovechamiento [ComprobantesDAO]......." + e.getMessage()  );
        }finally{
            if (rs  != null){ try{ rs.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage()); }}
            if (st  != null){ try{ st.close();            } catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        return cuenta;
    }
}

