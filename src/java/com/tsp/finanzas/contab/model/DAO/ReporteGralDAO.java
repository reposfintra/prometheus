/********************************************************************
 *      Nombre Clase.................   ReporteGralDAO.java
 *      Descripci�n..................   Data Access Object
 *      Autor........................   Ing. Andr�s Maturana De La Cruz
 *      Fecha........................   21 de junio de 2006
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes S�nchez Polo S.A.
 *******************************************************************/

package com.tsp.finanzas.contab.model.DAO;

import java.sql.*;
import java.util.*;
import com.tsp.finanzas.contab.model.beans.*;
import com.tsp.operation.model.DAOS.MainDAO;

/**
 *
 * @author  Ing. Andr�s Maturana De La Cruz
 */
public class ReporteGralDAO extends MainDAO{
    private Vector vector;
    
    /** Crea una nueva instancia de  ReporteGralDAO */
    public ReporteGralDAO() {
        super("ReporteGralDAO.xml");
    }
    public ReporteGralDAO(String dataBaseName) {
        super("ReporteGralDAO.xml", dataBaseName);
    }
     /**
     * Obtiene las facturas en un per�odo dado.
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param dstrct C�digo del Distrito
     * @param fecha_ini Fecha inicial del per�odo
     * @param Fecha_fin Fecha final del reporte
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void reporteVentasDiario(String dstrct, String fecha_ini, String fecha_fin) throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_REP_VEN_DIARIO";
        
        try{
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, dstrct);
            st.setString(2, fecha_ini);
            st.setString(3, fecha_fin);
            ////////System.out.println("........... REPORTE VENTAS DIARIO \n" + st);
            rs = st.executeQuery();
            this.vector = new Vector();
            System.gc();
            while( rs.next() ){
              //  Hashtable ht = new Hashtable();
                
                RepGral obj = new RepGral();
                obj.Load(rs);
                //logger.info("FACTURA: " + rs.getString("documento"));
                /*ht.put("factura", rs.getString("documento"));
                ht.put("fecha", rs.getString("fecha_creacion"));
                ht.put("nit", rs.getString("nit"));
                ht.put("codcli", rs.getString("codcli"));
                ht.put("valor", rs.getString("valor_factura"));
                ht.put("fec_anulacion", rs.getString("fecha_anulacion"));
                this.vector.add(ht);*/
                this.vector.add(obj);
                
            }
        }
        catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE reporteVentasDiario " + e.getMessage() + " " + e.getErrorCode());
        }finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
    }
    
    /**
     * Obtiene las facturas en un per�odo dado.
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param dstrct C�digo del Distrito
     * @param fecha_ini Fecha inicial del per�odo
     * @param Fecha_fin Fecha final del reporte
     * @param login Login del usuario que cre� la factura
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void reporteFacturasClientes(String dstrct, String fecha_ini, String fecha_fin, String login) throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_REP_FACT_CLI";        
        try{
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, dstrct);
            st.setString(2, fecha_ini);
            st.setString(3, fecha_fin);
            st.setString(4, "%" + login + "%");
            
            ////////System.out.println("........... REPORTE FACTURAS DE CLIENTES \n" + st);
            rs = st.executeQuery();
            
            this.vector = new Vector();
            while( rs.next() ){
                Hashtable ht = new Hashtable();
                //ht.put("", rs.getString(""));
                ht.put("nit", rs.getString("nit"));
                ht.put("nomcli", rs.getString("nomcli"));
                ht.put("numdoc", rs.getString("numdoc"));
                ht.put("gtrans", rs.getString("grupo_transaccion"));
                ht.put("trans", rs.getString("transaccion"));
                
                
                ht.put("periodo", rs.getString("periodo"));
                ht.put("cuenta", rs.getString("cuenta"));
                ht.put("auxiliar", rs.getString("auxiliar"));
                ht.put("detalle", rs.getString("detalle"));
                
                ht.put("vrdeb", rs.getString("valor_debito"));
                ht.put("vrcred", rs.getString("valor_credito"));
                
                ht.put("tercero", rs.getString("tercero"));
                ht.put("docint", rs.getString("docinterno"));
                
                ht.put("fecha", rs.getString("fecha_creacion"));
                ht.put("login", rs.getString("login"));
                
                this.vector.add(ht);
                ht = null;//Liberar Espacio JJCastro
            }
        }
        catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE reporteVentasDiario " + e.getMessage() + " " + e.getErrorCode());
        }finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
    }
    
    /**
     * Getter for property vector.
     * @return Value of property vector.
     */
    public java.util.Vector getVector() {
        return vector;
    }
    
    /**
     * Setter for property vector.
     * @param vector New value of property vector.
     */
    public void setVector(java.util.Vector vector) {
        this.vector = vector;
    }
    
}
