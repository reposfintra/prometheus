
/******************************************************************
 * Nombre ........GrabarComprobanteDAO.java
 * Descripci�n....Clase que maneja las consultas para la grabacion
 *                              de consultas
 * Autor..........Ivan Dario Gomez Vanegas
 * Fecha..........1Created on 7 de junio de 2006, 03:53 PM
 * Versi�n........1.0
 * Coyright.......Transportes Sanchez Polo S.A.
 *******************************************************************/



package com.tsp.finanzas.contab.model.DAO;
import com.tsp.finanzas.contab.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;
import com.tsp.operation.model.DAOS.MainDAO;
import com.tsp.operation.model.beans.*;
import java.sql.*;
import java.util.*;

public class GrabarComprobanteDAO extends MainDAO {
    private boolean existeCuenta;
    /** Creates a new instance of GrabarComprobanteDAO */
    public GrabarComprobanteDAO() {
        super("GrabarComprobanteDAO.xml");
    }
    public GrabarComprobanteDAO(String dataBaseName) {
        super("GrabarComprobanteDAO.xml", dataBaseName);
    }
    
    
    
    /**
     * Metodo buscarTipoDoc , Metodo que Consulta los tipos de documentos
     * @autor   : Ing. Ivan Dario Gomez Vanegas
     * @param   : String dstrct
     * @throws  : SQLException
     * @version : 1.0
     */
    public List buscarTipoDoc(String dstrct)throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        List lista   = new LinkedList();
        String query = "SQL_TIPO_DOC";
        try{
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, dstrct);
            rs = st.executeQuery();
            while(rs.next()){
                Hashtable tipoDoc = new Hashtable();
                tipoDoc.put("codigo",         rs.getString("codigo"));
                tipoDoc.put("descripcion",    rs.getString("descripcion"));
                tipoDoc.put("codigo_interno", rs.getString("codigo_interno"));
                tipoDoc.put("tercero",        rs.getString("tercero"));
                tipoDoc.put("maneja_serie",   rs.getString("maneja_serie"));
                tipoDoc.put("serie_ini",      rs.getString("serie_ini"));
                tipoDoc.put("serie_act",      rs.getString("serie_act"));
                tipoDoc.put("serie_actual",   rs.getString("serie_actual"));
                tipoDoc.put("prefijo",        rs.getString("prefijo"));
                tipoDoc.put("prefijo_anio",   rs.getString("prefijo_anio"));
                tipoDoc.put("prefijo_mes",    rs.getString("prefijo_mes"));
                tipoDoc.put("long_serie",     rs.getString("long_serie"));
                tipoDoc.put("comprodiario",   rs.getString("comprodiario"));
                lista.add(tipoDoc);
                tipoDoc = null;//Liberar Espacio JJCastro
            }
            
            
        }catch(SQLException ex){
            throw new SQLException("Error en buscarTipoDoc --> [GrabarComprobanteDAO] " + ex.getMessage() + " " + ex.getErrorCode());
        }finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return lista;
    }
    
    
    
      /**
     * Metodo buscarDatosTipoDoc , Metodo que busca los datos de un tipo de documento
     * @autor   : Ing. Ivan Dario Gomez Vanegas
     * @param   : String dstrct
     * @throws  : SQLException
     * @version : 1.0
     */
    public Hashtable buscarDatosTipoDoc(String dstrct, String codigo)throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
         Hashtable tipoDoc = null;
         String query = "SQL_TIPO_DOC_DATOS";
        try{
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, dstrct);
            st.setString(2, codigo);
            rs = st.executeQuery();
            if(rs.next()){
                tipoDoc = new Hashtable();
                tipoDoc.put("codigo",         rs.getString("codigo"));
                tipoDoc.put("descripcion",    rs.getString("descripcion"));
                tipoDoc.put("codigo_interno", rs.getString("codigo_interno"));
                tipoDoc.put("tercero",        rs.getString("tercero"));
                tipoDoc.put("maneja_serie",   rs.getString("maneja_serie"));
                tipoDoc.put("serie_ini",      rs.getString("serie_ini"));
                tipoDoc.put("serie_act",      rs.getString("serie_act"));
                tipoDoc.put("prefijo",        rs.getString("prefijo"));
                tipoDoc.put("prefijo_anio",   rs.getString("prefijo_anio"));
                tipoDoc.put("prefijo_mes",    rs.getString("prefijo_mes"));
                tipoDoc.put("codigo_interno", rs.getString("codigo_interno"));
                tipoDoc.put("comprodiario",   rs.getString("comprodiario"));
            }
            
            
        }catch(SQLException ex){
            throw new SQLException("Error en buscarDatosTipoDoc --> [GrabarComprobanteDAO] " + ex.getMessage() + " " + ex.getErrorCode());
        }finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return tipoDoc;
    }
    
    /**
     * Metodo buscarCuenta , para buscar los datos de la cuenta
     * @autor   : Ing. Ivan Dario Gomez Vanegas
     * @param   : String dstrct String cuenta
     * @throws  : SQLException
     * @version : 1.0
     */
    public Hashtable buscarCuenta(String dstrct, String cuenta)throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        Hashtable Cuenta = null;
        String query = "SQL_BUSCAR_CUENTA";
        try{
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, dstrct);
            st.setString(2, cuenta);
            rs = st.executeQuery();
            if(rs.next()){
                this.existeCuenta = true;
                Cuenta = new Hashtable();
                Cuenta.put("cuenta",     rs.getString("cuenta"));
                Cuenta.put("subledger",  rs.getString("subledger"));
                Cuenta.put("tercero",    rs.getString("tercero"));
                Cuenta.put("modulo1",    rs.getString("modulo1"));
            }else
                this.existeCuenta = false;
            
            
        }catch(SQLException ex){
            throw new SQLException("Error en buscarTipoDoc --> [GrabarComprobanteDAO] " + ex.getMessage() + " " + ex.getErrorCode());
        }finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return Cuenta;
    }
    
    /**
     * Getter for property existeCuenta.
     * @return Value of property existeCuenta.
     */
    public boolean isExisteCuenta() {
        return existeCuenta;
    }
    
    
    /**
     * Metodo ExisteMayorizacion , para verificar si existe en la tabla mayor
     * @autor   : Ing. Ivan Dario Gomez Vanegas
     * @param   : String dstrct String cuenta
     * @throws  : SQLException
     * @version : 1.0
     */
    public boolean ExisteMayorizacion( String dstrct, String anio,String cuenta)throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean existe =false;
        String query = "SQL_BUSCAR_CUENTA_MAYOR";
        try{
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, dstrct);
            st.setString(2, anio);
            st.setString(3, cuenta);
            rs = st.executeQuery();
            if(rs.next()){
                existe = true;
            }
            
        }catch(SQLException ex){
            throw new SQLException("Error en ExisteMayorizacion --> [GrabarComprobanteDAO] " + ex.getMessage() + " " + ex.getErrorCode());
        }finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return existe;
    }
    
    
    
    /**
     * Metodo ExisteMayorSubledger , Para verificar si existe en mayorsubledger
     * @autor   : Ing. Ivan Dario Gomez Vanegas
     * @param   : String dstrct String cuenta
     * @throws  : SQLException
     * @version : 1.0
     */
    public boolean ExisteMayorSubledger( String dstrct, String anio,String cuenta,String auxiliar)throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean existe = false;
        String query = "SQL_BUSCAR_CUENTA_MAYOR_SUBLEDGER";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, dstrct);
            st.setString(2, anio);
            st.setString(3, cuenta);
            st.setString(4, auxiliar);
            rs = st.executeQuery();
            if (rs.next()) {
                existe = true;
            }

        }catch(SQLException ex){
            throw new SQLException("Error en ExisteMayorSubledger --> [GrabarComprobanteDAO] " + ex.getMessage() + " " + ex.getErrorCode());
        }finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return existe;
    }
    
    
    
    /**
     * Metodo buscarPeriodos , para buscar los preriodos abiertos
     * @autor   : Ing. Ivan Dario Gomez Vanegas
     * @param   : String dstrct
     * @throws  : SQLException
     * @version : 1.0
     */
    public List buscarPeriodos(String dstrct)throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        Hashtable periodos = null;
        List listaPeriodos = null;
        String query = "SQL_BUSCAR_PERIODOS";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, dstrct);
            rs = st.executeQuery();
            listaPeriodos = new LinkedList();
            while (rs.next()) {

                periodos = new Hashtable();
                periodos.put("periodo", rs.getString("periodo"));
                listaPeriodos.add(periodos);
                periodos = null;//Liberar Espacio JJCastro


            }

        } catch (SQLException ex) {
            throw new SQLException("Error en buscarPeriodos --> [GrabarComprobanteDAO] " + ex.getMessage() + " " + ex.getErrorCode());
        }finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return listaPeriodos;
    }
    
    
    
    
    /**
     * M�todo que actualiza en mayor
     * @autor.......ivan gomez
     * @param....... ComprobanteFacturas comprodet
     * @throws......Exception
     * @version.....1.0.
     **/
    public  String UpdateMayor(ComprobanteFacturas comprodet, double saldo)throws Exception{
        StringStatement  st      = null;
        String sql ="";
        Connection con = null;
        String query = "SQL_UPDATE_MAYOR";
        try{
            con = this.conectarJNDI(query);
            st = new StringStatement(this.obtenerSQL(query).replaceAll("#MES#",comprodet.getPeriodo().substring(4,6)));//JJCastro fase2
            st.setDouble(1,   comprodet.getTotal_debito());
            st.setDouble(2,   comprodet.getTotal_credito());
            st.setDouble(3,   saldo   );
            st.setString(4,   comprodet.getDstrct()  );
            st.setString(5,   comprodet.getPeriodo().substring(0,4 ));
            st.setString(6,   comprodet.getCuenta() );
            
            //////System.out.println("UPDATE -->"+st.toString());
           sql = st.getSql();
            
        }catch(Exception e){
            throw new Exception( "UpdateMayor " + e.getMessage());
        }finally {
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return sql;
    }
    
    
    
    /**
     * M�todo que actualiza en mayor_subledger
     * @autor.......ivan gomez
     * @param....... ComprobanteFacturas comprodet
     * @throws......Exception
     * @version.....1.0.
     **/
    public  String UpdateMayorSubledger(ComprobanteFacturas comprodet, double saldo)throws Exception{
        StringStatement st = null;
        String sql = "";
        Connection con = null;
        String query = "SQL_UPDATE_MAYOR_SUBLEDGER";
        try {
            con = this.conectarJNDI(query);
            st = new StringStatement(this.obtenerSQL(query).replaceAll("#MES#", comprodet.getPeriodo().substring(4, 6)));//JJCastro fase2
            st.setDouble(1, comprodet.getTotal_debito());
            st.setDouble(2, comprodet.getTotal_credito());
            st.setDouble(3, saldo);
            st.setString(4, comprodet.getDstrct());
            st.setString(5, comprodet.getPeriodo().substring(0, 4));
            st.setString(6, comprodet.getCuenta());
            st.setString(7, comprodet.getAuxiliar());
            //////System.out.println("UPDATE -->"+st.toString());
            sql = st.getSql();

        } catch (Exception e) {
            throw new Exception( "UpdateMayorSubledger " + e.getMessage());
        }finally {
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return sql;
    }
    
    /**
     * M�todo que Inserta en mayor
     * @autor.......ivan gomez
     * @param....... ComprobanteFacturas comprodet, double saldo, String usuario
     * @throws......Exception
     * @version.....1.0.
     **/
    public  String InsertMayor(ComprobanteFacturas comprodet, double saldo, String usuario)throws Exception{
        PreparedStatement  st      = null;
        String sql ="";
        Connection con = null;
        String query = "SQL_INSERT_MAYOR";
        try{
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query).replaceAll("#MES#",comprodet.getPeriodo().substring(4,6)));//JJCastro fase2
            st.setString(1,   comprodet.getDstrct());
            st.setString(2,   comprodet.getCuenta());
            st.setString(3,   comprodet.getPeriodo().substring(0,4 ) );
            st.setDouble(4,   comprodet.getTotal_debito()  );
            st.setDouble(5,   comprodet.getTotal_credito());
            st.setString(6,   usuario );
            st.setDouble(7,   saldo   );
            //////System.out.println("INSERT -->"+st.toString());
            return st.toString()+";";
            
        }catch(Exception e){
            throw new Exception( "InsertMayor " + e.getMessage());
        }finally {
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
    }
    
    
    
    
    /**
     * M�todo que Inserta en mayor_subledger
     * @autor.......ivan gomez
     * @param....... ComprobanteFacturas comprodet, double saldo, String usuario
     * @throws......Exception
     * @version.....1.0.
     **/
    public  String InsertMayorSubledger(ComprobanteFacturas comprodet, double saldo, String usuario)throws Exception{
        StringStatement  st      = null;
        String sql ="";
        Connection con = null;
        String query = "SQL_INSERT_MAYOR_SUBLEDGER";
        try{

            con = this.conectarJNDI(query);
            st = new StringStatement(this.obtenerSQL(query).replaceAll("#MES#",comprodet.getPeriodo().substring(4,6)));//JJCastro fase2
            st.setString(1,   comprodet.getDstrct());
            st.setString(2,   comprodet.getCuenta());
            st.setString(3,   comprodet.getPeriodo().substring(0,4 ) );
            st.setDouble(4,   comprodet.getTotal_debito()  );
            st.setDouble(5,   comprodet.getTotal_credito());
            st.setString(6,   usuario );
            st.setString(7,   comprodet.getAuxiliar() );
            st.setDouble(8,   saldo   );
            //////System.out.println("INSERT -->"+st.toString());
            return st.toString()+";";
            
        }catch(Exception e){
            throw new Exception( "InsertMayor " + e.getMessage());
        }finally {
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
    }
    
    /**
     * M�todo que busca las facturas que no esten contabilizadas
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public  List BuscarComprobantes(String dstrct,String tipodoc, String numdoc) throws Exception{
        Connection con = null;
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        PreparedStatement  st2      = null;
        ResultSet          rs2      = null;
        List               lista   = new LinkedList();
        String             query   = "SQL_BUSCAR_COMPROBANTE";
        try{
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, dstrct      );
            st.setString(2, tipodoc     );
            st.setString(3, numdoc      );
            rs = st.executeQuery();
            while (rs.next()){
                ComprobanteFacturas  comprobante = new ComprobanteFacturas();
                comprobante.setDstrct  (rs.getString("dstrct")  );
                comprobante.setTipodoc(rs.getString("tipodoc") );
                comprobante.setNumdoc  (rs.getString("numdoc")  );
                comprobante.setSucursal(rs.getString("sucursal"));
                comprobante.setPeriodo(rs.getString("periodo")  );
                comprobante.setFechadoc(rs.getString("fechadoc"));
                comprobante.setDetalle(rs.getString("detalle")  );
                comprobante.setTercero(rs.getString("tercero")  );
                comprobante.setTipo("C");
                comprobante.setTotal_debito(rs.getDouble("total_debito") );
                comprobante.setTotal_credito(rs.getDouble("total_credito")  );
                comprobante.setMoneda(rs.getString("moneda"));
                comprobante.setAprobador(rs.getString("aprobador"));
                comprobante.setBase(rs.getString("base"));
                comprobante.setGrupo_transaccion(rs.getInt("grupo_transaccion"));
                comprobante.setFechaCreacion(rs.getString("creation_date"));
                comprobante.setFecha_aplicacion(rs.getString("fecha_aplicacion"));
                comprobante.setUsuario_aplicacion(rs.getString("usuario_aplicacion"));
                lista.add( comprobante );
                comprobante = null;//Liberar Espacio JJCastro
            }
            
        }catch(Exception e){
            throw new Exception( "getFacturasNoContabilizadas " + e.getMessage());
        }
  finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return lista;
    }
    
    
    /**
     * M�todo que busca un comprobante
     * @autor.......igomez
     * @throws......Exception
     * param .......String dstrct,String tipodoc, String numdoc,int grupo
     * @version.....1.0.
     **/
    public  ComprobanteFacturas ComprobanteSearch(String dstrct,String tipodoc, String numdoc,int grupo) throws Exception{
        Connection con = null;
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        PreparedStatement  st2      = null;
        ResultSet          rs2      = null;
        String             query   = "SQL_COMPROBANTE";
        String             query2  = "SQL_BUSCAR_COMPRODET";
        ComprobanteFacturas  comprobante = null;
        try{
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, dstrct      );
            st.setString(2, tipodoc     );
            st.setString(3, numdoc      );
            st.setInt   (4, grupo       );
            rs = st.executeQuery();
            if (rs.next()){
                comprobante = new ComprobanteFacturas();
                comprobante.setDstrct  (rs.getString("dstrct")  );
                comprobante.setTipodoc(rs.getString("tipodoc") );
                comprobante.setNumdoc  (rs.getString("numdoc")  );
                comprobante.setSucursal(rs.getString("sucursal"));
                comprobante.setPeriodo(rs.getString("periodo")  );
                comprobante.setFechadoc(rs.getString("fechadoc"));
                comprobante.setDetalle(rs.getString("detalle")  );
                comprobante.setTercero(rs.getString("tercero")  );
                comprobante.setTipo("C");
                comprobante.setTotal_debito(rs.getDouble("total_debito") );
                comprobante.setTotal_credito(rs.getDouble("total_credito")  );
                comprobante.setMoneda(rs.getString("moneda"));
                comprobante.setAprobador(rs.getString("aprobador"));
                comprobante.setBase(rs.getString("base"));
                comprobante.setGrupo_transaccion(rs.getInt("grupo_transaccion"));
                comprobante.setFechaCreacion(rs.getString("creation_date"));
                comprobante.setFecha_aplicacion(rs.getString("fecha_aplicacion"));
                comprobante.setUsuario_aplicacion(rs.getString("usuario_aplicacion"));
                comprobante.setGrupo_transaccion(grupo);


                st2 = con.prepareStatement(this.obtenerSQL(query2));//JJCastro fase2
                st2.setString(1, dstrct      );
                st2.setString(2, tipodoc     );
                st2.setString(3, numdoc      );
                st2.setInt   (4, grupo       );
                rs2 = st2.executeQuery();
                List  items   = new LinkedList();
                int TotalItem = 0;
                while (rs2.next()){
                    TotalItem++;
                    ComprobanteFacturas comprodet = new ComprobanteFacturas();
                    comprodet.setDstrct(dstrct);
                    comprodet.setTipodoc(tipodoc);
                    comprodet.setNumdoc(numdoc);
                    comprodet.setGrupo_transaccion(grupo);
                    comprodet.setPeriodo(rs2.getString("periodo")  );
                    comprodet.setCuenta(rs2.getString("cuenta"));
                    comprodet.setAuxiliar(rs2.getString("auxiliar"));
                    comprodet.setAbc(rs2.getString("abc"));
                    comprodet.setDetalle(rs2.getString("detalle"));
                    comprodet.setTotal_debito(rs2.getDouble("valor_debito"));
                    comprodet.setTotal_credito(rs2.getDouble("valor_credito"));
                    comprodet.setTercero(rs2.getString("tercero"));
                    comprodet.setBase(rs2.getString("base"));
                    comprodet.setDocumento_interno(rs2.getString("documento_interno"));
                    comprodet.setExisteCuenta(true);
                    comprodet.setTipo("D");
                    comprodet.setTdoc_rel( rs2.getString("tipodoc_rel"));
                    comprodet.setNumdoc_rel( rs2.getString("documento_rel"));
                    
                    items.add( comprodet );
                    comprodet = null;//Liberar Espacio JJCastro
                }
                comprobante.setItems(items);
                comprobante.setTotal_items(TotalItem);
                
            }
            
        }catch(Exception e){
            throw new Exception( "ComprobanteSearch " + e.getMessage());
        }
finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}

            if (rs2 != null) {try {rs2.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st2 != null) {try {st2 = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return comprobante;
    }
    
    
     /**
     * M�todo que actualiza en Comprobante
     * @autor.......ivan gomez
     * @param....... ComprobanteFacturas comprodet
     * @throws......Exception
     * @version.....1.0.
     **/
    public  ArrayList<String> UpdateComprobante(ComprobanteFacturas comprobante)throws Exception{
        Connection con = null;
        PreparedStatement  st      = null;
        ArrayList<String>  sql =new ArrayList<>();
        String query = "SQL_UPDATE_COMPROBANTE";
        String query2 ="SQL_DELETE_COMPRODET"  ;
        String query3 = "SQL_INSERT_COMPRODET";
        try{
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1,  comprobante.getFechadoc()           );
            st.setString(2,  comprobante.getDetalle()            );
            st.setString(3,  comprobante.getTercero()            );
            st.setDouble(4,  comprobante.getTotal_debito()       );
            st.setDouble(5,  comprobante.getTotal_credito()      );
            st.setInt   (6,  comprobante.getTotal_items()        );
            st.setString(7,  "now()"                             );
            st.setString(8,  comprobante.getAprobador()          );
            st.setString(9,  comprobante.getDstrct()             );
            st.setString(10, comprobante.getTipodoc()            );
            st.setString(11, comprobante.getNumdoc()             );
            st.setInt   (12, comprobante.getGrupo_transaccion()  );
            
            sql.add(st.toString());
            st.clearParameters();

            st = con.prepareStatement(this.obtenerSQL(query2));//JJCastro fase2
            st.setString(1,  comprobante.getDstrct()             );
            st.setString(2, comprobante.getTipodoc()            );
            st.setString(3, comprobante.getNumdoc()             );
            st.setInt   (4, comprobante.getGrupo_transaccion()  );
            
            sql.add(st.toString());
            st.clearParameters();
            List lista = comprobante.getItems();
            st = con.prepareStatement(this.obtenerSQL(query3));//JJCastro fase2
             for(int i=0;i<lista.size();i++){
                ComprobanteFacturas  comprodet = (ComprobanteFacturas)lista.get(i);
                
                st.setString(1,   comprodet.getDstrct()   );
                st.setString(2,   comprodet.getTipodoc()  );
                st.setString(3,   comprodet.getNumdoc()   );
                st.setInt   (4,   comprobante.getGrupo_transaccion()   );
                st.setString(5,   comprodet.getPeriodo()  );
                st.setString(6,   comprodet.getCuenta()   );
                st.setString(7,   comprodet.getAuxiliar() );
                st.setString(8,   comprodet.getAbc()      );
                st.setString(9,   comprodet.getDetalle()  );
                st.setDouble(10,  comprodet.getTotal_debito()   );
                st.setDouble(11,  comprodet.getTotal_credito()  );
                st.setString(12,  comprodet.getTercero()        );
                st.setString(13,  comprobante.getAprobador()    );
                st.setString(14,  comprodet.getBase()              );
                st.setString(15,  comprodet.getDocumento_interno() );
                st.setString(16,  comprodet.getTdoc_rel()   );
                st.setString(17,  comprodet.getNumdoc_rel() );
                
                sql.add(st.toString());
                st.clearParameters();
                
            }
                        
        }catch(Exception e){
            throw new Exception( "UpdateComprobante " + e.getMessage());
        }finally {
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return sql;
    }
    
    /**
     * M�todo que actualiza la serie
     * @autor.......ivan gomez
     * @param....... dstrct y el tipo de documento
     * @throws......Exception
     * @version.....1.0.
     **/
    public  void UpdateSerie(String dstrct, String codigo)throws Exception{
        Connection con = null;
        PreparedStatement st = null;
        String sql = "";
        String query = "SQL_UPDATE_SERIE";

        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, dstrct);
            st.setString(2, codigo);
            st.executeUpdate();
        }catch(Exception e){
            throw new Exception( "UpdateSerie " + e.getMessage());
        }finally {
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
    }
    
    
    /**
     * Metodo busquedaTercero, busca la lista de terceros
     * @param: valor
     * @autor : Ing. Ivan Dario Gomez
     * @version : 1.0
     */
    public List busquedaTercero(String valor) throws Exception {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        List lista   = null;
        String query = "SQL_BUSCAR_TERCERO";
        try{
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query),ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);//JJCastro fase2
            st.setString(1,"%"+ valor.toUpperCase() +"%");
            st.setString(2,"%"+ valor.toUpperCase() +"%");
            
            lista = new LinkedList();
            //////System.out.println("SQLL --- > "+ st.toString());
            rs = st.executeQuery();
            if(!rs.next()) {
                query = "SQL_BUSCAR_CLIENTES";
                con = this.conectarJNDI(query);
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1,"%"+ valor.toUpperCase() +"%");
                st.setString(2,"%"+ valor.toUpperCase() +"%");
                rs = st.executeQuery();
            } else {
                rs.previous();
            }
            while(rs.next()){
                Cliente cliente = new Cliente();
                cliente.setNomcli(rs.getString("nombre"));
                cliente.setNit   (rs.getString("cedula"));
                lista.add(cliente);
                cliente = null;//Liberar Espacio JJCastro
            }
            
            return lista;
        }catch(Exception e) {
            throw new Exception("Error al busquedaTercero [CXPDocDAO]... \n"+e.getMessage());
        }
finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        
    }
    
    /**
     * Metodo existeDocumento, verifica un documento en la tabla de comprobantes
     * @param: dstrct
     * @param: tipdoc
     * @param: numdoc
     * @autor : Ing. Mario Fontalvo
     * @version : 1.0
     */
    public boolean existeDocumento(String dstrct, String tipdoc, String numdoc) throws Exception {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean existe = false;
        String query = "SQL_BUSCAR_DOCUMENTO";
        try{
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1,dstrct);
            st.setString(2,tipdoc);
            st.setString(3,numdoc);            
            rs = st.executeQuery();
            if (rs.next()){
                existe = true;
            }
        }catch(Exception e) {
            throw new Exception("Error al existeDocumento [CXPDocDAO]... \n"+e.getMessage());
        }finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return existe;
    }
    
}
