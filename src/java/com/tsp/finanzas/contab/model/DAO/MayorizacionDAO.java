/********************************************************************
 *  Nombre Clase.................   MayorizacionDAO.java
 *  Descripci�n..................   DAO de la tabla con.mayor
 *  Autor........................   Ing. Osvaldo P�rez
 *  Fecha........................   12.06.2006
 *  Versi�n......................   1.0
 *  Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/
package com.tsp.finanzas.contab.model.DAO;

import java.util.*;
import java.sql.*;
import com.tsp.finanzas.contab.model.beans.*;
import org.apache.log4j.Logger;
import com.tsp.util.*;
import java.util.Date;
import java.text.*;
import com.tsp.util.connectionpool.PoolManager;
import com.tsp.operation.model.DAOS.MainDAO;
import com.tsp.operation.model.beans.StringStatement;
import com.tsp.operation.model.beans.Usuario;

/**
 *
 * @author  Osvaldo
 */
public class MayorizacionDAO extends MainDAO {

    //String consulta = "";
    Mayorizacion mayor;
    Vector vector;

    /** Creates a new instance of MayorDao */
    public MayorizacionDAO() {
        super("MayorizacionDAO.xml");
    }
    public MayorizacionDAO(String dataBaseName) {
        super("MayorizacionDAO.xml", dataBaseName);
    }

    /**
     *Metodo que permite actualizar la tabla MAYOR con los cr�ditos o d�bitos, y permite
     *actualizar al igual el saldo actual.
     *@autor: David Pi�a
     *@param comprobante contiene la informaci�n o filtros para obtener el detalle de un
     *                   compobante
     *@throws: En caso de que un error de base de datos ocurra.
     *@return Una cadena con el sql obtenido
     */
    public String updateMayorizacionCuenta(Comprobantes comprobante) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        String sql = "";
        String campo = "";
        String anio = "";
        String mes = "";
        String periodo = "";
        double saldoActual = 0;
        double valorCampo = 0;
        String query = "SQL_ACTUALIZAR_MAYOR";
        try {
            periodo = comprobante.getPeriodo();
            //se separan el mes y el a�o en base al periodo
            if (!periodo.equals("''") && periodo.length() == 6 && !periodo.equals("")) {
                anio = periodo.substring(0, 4);
                mes = periodo.substring(4, 6);
            }
            //se asigna el campo que se deses modificar segun el total diferente a cero
            campo = (comprobante.getTotal_credito() == 0) ? "movdeb" + mes : "movcre" + mes;
            //se designa si el campo de total cr�dito o d�bito se va a descontar o adicionar al saldo actual de la tabla MAYOR
            saldoActual = (comprobante.getTotal_credito() == 0) ? comprobante.getTotal_debito() : (comprobante.getTotal_credito() * (-1));
            //se asigna el valor del campo total cr�dito o d�bito donde alguno se diferente de cero
            valorCampo = (comprobante.getTotal_credito() == 0) ? comprobante.getTotal_debito() : comprobante.getTotal_credito();

            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setDouble(1, valorCampo);
            st.setDouble(2, saldoActual);
            //el usuario obtenido del objeto comprobante hace referencia al que se encuentran en la Sesi�n
            st.setString(3, comprobante.getUsuario());
            st.setString(4, comprobante.getDstrct());
            st.setString(5, anio);
            st.setString(6, comprobante.getCuenta());

            sql = st.toString();
            sql = sql.replaceAll("#CAMPOUPDATE#", campo);

        } catch (SQLException e) {
            throw new SQLException("Error en updateMayorizacionCuenta [MayorizacionDAO]......." + e.getMessage() + " - " + e.getErrorCode());
        } finally {
            if (st != null) {
                try {
                    st = null;
                } catch (Exception e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return sql;
    }

    /**
     *Metodo que permite actualizar la tabla MAYORSUBLEDGER con los cr�ditos o d�bitos, y permite
     *actualizar al igual el saldo actual.
     *@autor: David Pi�a
     *@param comprobante contiene la informaci�n o filtros para obtener el detalle de un
     *                   compobante
     *@throws: En caso de que un error de base de datos ocurra.
     *@return Una cadena con el sql obtenido
     */
    public String updateMayorizacionSubledgerCuenta(Comprobantes comprobante) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        String sql = "";
        String campo = "";
        String anio = "";
        String mes = "";
        String periodo = "";
        double saldoActual = 0;
        double valorCampo = 0;
        String query = "SQL_ACTUALIZAR_MAYORSUBLEDGER";
        try {
            periodo = comprobante.getPeriodo();
            //se separan el mes y el a�o en base al periodo
            if (!periodo.equals("''") && periodo.length() == 6 && !periodo.equals("")) {
                anio = periodo.substring(0, 4);
                mes = periodo.substring(4, 6);
            }
            //se asigna el campo que se deses modificar segun el total diferente a cero
            campo = (comprobante.getTotal_credito() == 0) ? "movdeb" + mes : "movcre" + mes;
            //se designa si el campo de total cr�dito o d�bito se va a descontar o adicionar al saldo actual de la tabla MAYORSUBLEDGER
            saldoActual = (comprobante.getTotal_credito() == 0) ? comprobante.getTotal_debito() : (comprobante.getTotal_credito() * (-1));
            //se asigna el valor del campo total cr�dito o d�bito donde alguno se diferente de cero
            valorCampo = (comprobante.getTotal_credito() == 0) ? comprobante.getTotal_debito() : comprobante.getTotal_credito();

            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setDouble(1, valorCampo);
            st.setDouble(2, saldoActual);
            //el usuario obtenido del objeto comprobante hace referencia al que se encuentran en la Sesi�n
            st.setString(3, comprobante.getUsuario());
            st.setString(4, comprobante.getDstrct());
            st.setString(5, anio);
            st.setString(6, comprobante.getCuenta());
            st.setString(7, comprobante.getAuxiliar());

            sql = st.toString();
            sql = sql.replaceAll("#CAMPOUPDATE#", campo);

        } catch (SQLException e) {
            throw new SQLException("Error en updateMayorizacionSubledgerCuenta [MayorizacionDAO]......." + e.getMessage() + " - " + e.getErrorCode());
        } finally {
            if (st != null) {
                try {
                    st = null;
                } catch (Exception e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return sql;
    }

    /**
     *Metodo que obtiene el saldo anterior de la cuenta dada para el mes dado
     *@autor: Osvaldo P�rez Ferrer
     *@param dstrct distrito
     *@param cuenta cuenta
     *@param anio a�o
     *@param mes mes hasta el cual se calcular� el saldo anterior
     *@throws: En caso de que un error de base de datos ocurra.
     *@return double saldo anterior
     */
    public double obtenerSaldoAnteriorCuenta(String dstrct, String cuenta, String anio, int mes) throws SQLException {
        Connection con = null;
        double[] v = new double[25];

        PreparedStatement st = null;
        ResultSet rs = null;
        double saldo = 0;
        String query = "SQL_MAYOR_CUENTA";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, cuenta);
            st.setString(2, anio);
            st.setString(3, dstrct);
            rs = st.executeQuery();

            if (rs.next()) {
                v[0] = rs.getDouble("sal");
                saldo = v[0];
                for (int i = 1, j = 1; i < 13; i++, j += 2) {
                    v[j] = rs.getDouble("d" + i);
                    v[j + 1] = rs.getDouble("c" + i);
                }
            }

            for (int i = 0, j = 1; i < (mes - 1); i++, j += 2) {//(mes-1)
                saldo += v[j];
                saldo -= v[j + 1];
            }

        } catch (SQLException ex) {
            throw new SQLException("Error en obtenerMayorCuenta......." + ex.getMessage() + " - " + ex.getErrorCode());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st = null;
                } catch (Exception e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return saldo;
    }

    /****
     *Metodo que permite obtener las sumatorias de las cuentas ICG de la tabla mayor
     *@autor: David Pi�a
     *@param anio El a�o del periodo
     *@param mes El mes del periodo
     *@throws: En caso de que un error de base de datos ocurra.
     ****/
    public void getTotalesSumatoriasICG(String anio, String mes) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        String sql = "";
        String query = "SQL_TOTALES_SUMATORIAS_ICG";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, anio);

            sql = st.toString();
            sql = sql.replaceAll("#MES#", mes);

            ResultSet rs = st.executeQuery(sql);
            vector = new Vector();
            while (rs.next()) {
                mayor = new Mayorizacion();
                mayor.setCuenta(rs.getString("tipo"));
                mayor.setMovdeb(rs.getDouble("debito"));
                mayor.setMovcre(rs.getDouble("credito"));
                vector.add(mayor);

            }

        } catch (SQLException e) {
            throw new SQLException("Error en getTotalesSumatoriasICG [MayorizacionDAO]......." + e.getMessage() + " - " + e.getErrorCode());
        } finally {
            if (st != null) {
                try {
                    st = null;
                } catch (Exception e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
    }

    /****
     *Metodo que permite obtener las sumatorias de los elementos de gasto de las cuentas I, C o G
     *@autor: David Pi�a
     *@param anio El a�o del periodo
     *@param mes El mes del periodo
     *@param tipo El tipo de cuenta I, C o G
     *@throws: En caso de que un error de base de datos ocurra.
     ****/
    public void getTotalesSumatoriasElementoGasto(String anio, String mes, String tipo) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        String sql = "";
        String query = "SQL_SUMATORIAS_ELEMENTO_GASTO_ICG";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, tipo);
            st.setString(2, anio);

            sql = st.toString();
            sql = sql.replaceAll("#MES#", mes);

            ResultSet rs = st.executeQuery(sql);
            vector = new Vector();
            while (rs.next()) {
                mayor = new Mayorizacion();
                mayor.setCuenta(rs.getString("referencia"));
                mayor.setMovdeb(rs.getDouble("debito"));
                mayor.setMovcre(rs.getDouble("credito"));
                mayor.setDistrito(rs.getString("elemento"));
                vector.add(mayor);

            }

        } catch (SQLException e) {
            throw new SQLException("Error en getTotalesSumatoriasElementoGasto [MayorizacionDAO]......." + e.getMessage() + " - " + e.getErrorCode());
        } finally {
            if (st != null) {
                try {
                    st = null;
                } catch (Exception e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
    }

    /**
     * Getter for property mayor.
     * @return Value of property mayor.
     */
    public com.tsp.finanzas.contab.model.beans.Mayorizacion getMayor() {
        return mayor;
    }

    /**
     * Setter for property mayor.
     * @param mayor New value of property mayor.
     */
    public void setMayor(com.tsp.finanzas.contab.model.beans.Mayorizacion mayor) {
        this.mayor = mayor;
    }

    /**
     * Getter for property vector.
     * @return Value of property vector.
     */
    public java.util.Vector getVector() {
        return vector;
    }

    /**
     * Setter for property vector.
     * @param vector New value of property vector.
     */
    public void setVector(java.util.Vector vector) {
        this.vector = vector;
    }

    /****************************************************************
     *Metodo que obtiene la sumatoria de el debito o el credito de las
     *              cuentas dadas
     *@autor: Osvaldo P�rez Ferrer
     *@param mes mes del cual se quiere obtener el debito o credito
     *@param where expresion de filtro, la palabra where no debe ser
     *             incluida
     *@param debcre debe especificarse movdeb o movcre para debito o credito
     *              respectivamente
     *@throws: En caso de que un error de base de datos ocurra.
     *@return double sumatoria de debito o credito
     ***************************************************************/
    public double obtenerDebitoCreditoCuentas(String mes, String where, String debcre) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        double saldo = 0;
        String sql = "";
        String query = "SQL_GENERAL";

        double v = 0;

        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            sql = st.toString();
            mes = (mes.length() == 1) ? "0" + mes : mes;
            sql = sql.replaceAll("#CAMPOS#", debcre + mes);
            sql = sql.replaceAll("#WHERE#", where);

            ////System.out.println("Consulta obtenerValoresCuenta : \n"+sql);
            rs = st.executeQuery(sql);

            while (rs.next()) {
                v += rs.getDouble(debcre + mes);
            }

        } catch (SQLException ex) {
            throw new SQLException("Error en MayorizacionDAO.obtenerDebitoCreditoCuentas......." + ex.getMessage() + " - " + ex.getErrorCode());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st = null;
                } catch (Exception e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return v;
    }

    public double calcularSaldoAnteriorXCuentas(String mes, String where, String anio) throws Exception {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        double total = 0;
        String sql = "";
        int mesInt = 0;
        String query = "SQL_GENERAL";

        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            sql = st.toString();
            mesInt = Integer.parseInt(mes);
            sql = sql.replaceAll("#CAMPOS#", "*");
            sql = sql.replaceAll("#WHERE#", where);

            ////System.out.println("Consulta calcularXPorCuentas : \n"+sql);
            rs = st.executeQuery(sql);
            String cuenta = "";
            while (rs.next()) {
                cuenta = rs.getString("cuenta");
                total += this.obtenerSaldoAnteriorCuenta("FINV", cuenta, anio, mesInt);
            }

        } catch (SQLException ex) {
            throw new SQLException("Error en MayorizacionDAO.obtenerDebitoCreditoCuentas......." + ex.getMessage() + " - " + ex.getErrorCode());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st = null;
                } catch (Exception e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return total;
    }

    /*****************************************************************************************************************/
    /*****************************************************************************************************************/
    /*****************************************************************************************************************/
    /************************************************************************
     * metodo existeEnMayor: verifica si el registro existe en mayor o mayor_subledger
     * @author: Ing. Osvaldo P�rez Ferrer
     * @param: Comprobantes c, objeto con los datos de la cuenta
     * @param: String tabla, tabla de la que se desea consultar 'mayor' o 'mayor_subledger'
     * @return: true or false
     * @throws: en caso de un error de BD
     ************************************************************************/
    public boolean existeEnMayor(Comprobantes c, String tabla) throws Exception {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean existe = false;
        String query = "SQL_EXISTE_MAYOR";
        String query2 = "SQL_EXISTE_MAYOR_SUBLEDGER";

        try {
            con = this.conectarJNDI(query);


            if (tabla.equals("mayor")) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, c.getDstrct());
                st.setString(2, c.getPeriodo().substring(0, 4));
                st.setString(3, c.getCuenta());
            } else {
                st = con.prepareStatement(this.obtenerSQL(query2));//JJCastro fase2
                st.setString(1, c.getDstrct());
                st.setString(2, c.getPeriodo().substring(0, 4));
                st.setString(3, c.getCuenta());
                st.setString(4, c.getAuxiliar());
            }

            rs = st.executeQuery();

            if (rs.next()) {
                existe = true;
            }

        } catch (SQLException ex) {
            ex.printStackTrace();
            throw new SQLException("Error en MayorizacionDAO.existeEnMayor......." + ex.getMessage() + " - " + ex.getErrorCode());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st = null;
                } catch (Exception e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return existe;
    }

    /************************************************************************
     * metodo insertMayorizacionCuenta, genera SQL para insertar un registro en con.mayor
     * @author: Ing. Osvaldo P�rez Ferrer
     * @param: Comprobantes c, objeto con datos a insertar
     ************************************************************************/
    public String insertMayorizacionCuenta(Comprobantes c, Usuario usuario) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        String sql = "";
        String campo = "";
        String anio = "";
        String mes = "";
        String periodo = "";
        double saldoActual = 0;
        double valorCampo = 0;
        String query = "SQL_INSERT_MAYOR";
        try {
            periodo = c.getPeriodo();
            //se separan el mes y el a�o en base al periodo
            if (!periodo.equals("''") && periodo.length() == 6 && !periodo.equals("")) {
                anio = periodo.substring(0, 4);
                mes = periodo.substring(4, 6);
            }
            //se asigna el campo que se deses modificar segun el total diferente a cero
            campo = (c.getTotal_credito() == 0) ? "movdeb" + mes : "movcre" + mes;
            //se designa si el campo de total cr�dito o d�bito se va a descontar o adicionar al saldo actual de la tabla MAYOR
            saldoActual = (c.getTotal_credito() == 0) ? c.getTotal_debito() : (c.getTotal_credito() * (-1));
            //se asigna el valor del campo total cr�dito o d�bito donde alguno se diferente de cero
            valorCampo = (c.getTotal_credito() == 0) ? c.getTotal_debito() : c.getTotal_credito();

            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, c.getDstrct());
            st.setString(2, c.getCuenta());
            st.setString(3, anio);
            st.setDouble(4, valorCampo);
            st.setDouble(5, saldoActual);
            st.setString(6, usuario.getLogin());
            st.setString(7, "COL");

            sql = st.toString();
            sql = sql.replaceAll("#CAMPOUPDATE#", campo);

        } catch (SQLException e) {
            e.printStackTrace();
            throw new SQLException("Error en MayorizacionDAO.insertMayorizacionCuenta " + e.getMessage() + " - " + e.getErrorCode());
        } finally {
            if (st != null) {
                try {
                    st = null;
                } catch (Exception e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return sql;
    }

    /************************************************************************
     * metodo insertMayorizacionSubledgerCuenta, genera SQL para insertar un 
     *        registro en con.mayor_subledger
     * @author: Ing. Osvaldo P�rez Ferrer
     * @param: Comprobantes c, objeto con datos a insertar
     ************************************************************************/
    public String insertMayorizacionSubledgerCuenta(Comprobantes c, Usuario usuario) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        String sql = "";
        String campo = "";
        String anio = "";
        String mes = "";
        String periodo = "";
        double saldoActual = 0;
        double valorCampo = 0;
        String query = "SQL_INSERT_MAYOR_SUBLEDGER";

        try {
            periodo = c.getPeriodo();
            //se separan el mes y el a�o en base al periodo
            if (!periodo.equals("''") && periodo.length() == 6 && !periodo.equals("")) {
                anio = periodo.substring(0, 4);
                mes = periodo.substring(4, 6);
            }
            //se asigna el campo que se deses modificar segun el total diferente a cero
            campo = (c.getTotal_credito() == 0) ? "movdeb" + mes : "movcre" + mes;
            //se designa si el campo de total cr�dito o d�bito se va a descontar o adicionar al saldo actual de la tabla MAYOR
            saldoActual = (c.getTotal_credito() == 0) ? c.getTotal_debito() : (c.getTotal_credito() * (-1));
            //se asigna el valor del campo total cr�dito o d�bito donde alguno se diferente de cero
            valorCampo = (c.getTotal_credito() == 0) ? c.getTotal_debito() : c.getTotal_credito();

            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, c.getDstrct());
            st.setString(2, c.getCuenta());
            st.setString(3, c.getAuxiliar());
            st.setString(4, anio);
            st.setDouble(5, valorCampo);
            st.setDouble(6, saldoActual);
            st.setString(7, usuario.getLogin());
            st.setString(8, "COL");

            sql = st.toString();
            sql = sql.replaceAll("#CAMPOUPDATE#", campo);

        } catch (SQLException e) {
            e.printStackTrace();
            throw new SQLException("Error en MayorizacionDAO.insertMayorizacionSubledgerCuenta " + e.getMessage() + " - " + e.getErrorCode());
        } finally {
            if (st != null) {
                try {
                    st = null;
                } catch (Exception e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return sql;
    }

    public static void main(String[] abc) {
        MayorizacionDAO m = new MayorizacionDAO();

        try {
            //double v = m.obtenerDebitoCreditoCuentas( "1" , " cuenta like '_1050___'  and anio = '2006' " , "movcre");
            //double v = m.calcularSaldoAnteriorXCuentas( "1",  " cuenta like '_1050___'  and anio = '2006' " ,"2006");
            double v = m.obtenerSaldoAnteriorCuenta("FINV", "11100523", "2006", 1);
            //System.out.println(""+v);
        } catch (Exception ex) {
            //System.out.println(" error en main  "+ex.getMessage());
        }
    }

    /**
     *Metodo que obtiene una lista de cuentas con saldo
     *@autor: TMolina - 22 Dic - 2008
     *@param dstrct distrito
     *@param anio a�o
     *@param mes mes hasta el cual se buscaran cuentas con saldo.
     *@throws: En caso de que un error de base de datos ocurra.
     *@return Vector Vector con las cuentas con saldo.
     */
    public Vector obtenerCuentasConSaldo(String dstrct, String anio, int mes) throws SQLException {
        Connection con = null;
        double[] v = new double[25];
        vector = new Vector();

        PreparedStatement st = null;
        ResultSet rs = null;
        double saldo = 0;
        int sw = 0;
        String query = "SQL_MAYOR_CUENTA_CON_SALDO";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, anio);
            st.setString(2, dstrct);
            rs = st.executeQuery();

            while (rs.next()) {

                for (int i = 1, j = 1; i < (mes) + 1; i++, j += 2) {
                    if (sw == 0) {
                        if (rs.getDouble("sal") != 0 || rs.getDouble("d" + i) != 0 || rs.getDouble("c" + i) != 0) {
                            vector.add(rs.getString("cuenta"));
                            sw = 1;
                        }
                    }
                }
                sw = 0;
            }
        } catch (SQLException ex) {
            throw new SQLException("Error en obtenerCuentasConSaldo......." + ex.getMessage() + " - " + ex.getErrorCode());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st = null;
                } catch (Exception e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return vector;
    }

    /**
     *Metodo que obtiene una lista de cuentas con saldo de un periodo
     *@autor: TMolina - 22 Dic - 2008
     *@param anio a�o
     *@param mes mes hasta el cual se buscaran cuentas con saldo.
     *@throws: En caso de que un error de base de datos ocurra.
     *@return Vector Vector con las cuentas con saldo.
     */
    public Vector obtenerValoresPeriodo(Vector v, String anio, String mes) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;

        Comprobantes c;
        vector = new Vector();
        String s = "";
        String query = "SQL_VALORES_PERIODO";
        try {
            con = this.conectarJNDI(query);

            for (int i = 0; i < v.size(); i++) {

                st = con.prepareStatement(this.obtenerSQL(query).replaceAll("#PERIODO#", mes));//JJCastro fase2
                st.setString(1, (String) v.get(i));
                st.setString(2, anio);
                rs = st.executeQuery();

                if (rs.next()) {
                    c = new Comprobantes();
                    c.setValor_unit(rs.getDouble("saldo_anterior"));
                    c.setCuenta(rs.getString("cuenta"));
                    c.setValor_unit(rs.getDouble("saldo_anterior"));
                    c.setTotal_debito(rs.getDouble("debito"));
                    c.setTotal_credito(rs.getDouble("credito"));
                    c.setDetalle((rs.getString("nombre_largo") == null) ? "" : rs.getString("nombre_largo"));
                    vector.add(c);
                    c = null;//Liberar Espacio JJCastro
                }

            }
        } catch (SQLException ex) {
            throw new SQLException("Error en obtenerValoresPeriodo......." + ex.getMessage() + " - " + ex.getErrorCode());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st = null;
                } catch (Exception e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return vector;
    }

    /**
     * metodo para eliminar la tabla tem.comprobante_x_mayorizar
     * @param comando vector de stringstatements del proceso
     * @author MGarizao - GEOTECH
     * @date 17/02/2010
     * @version 1.0
     * @return vector con el stringstatement
     * @throws SQLException
     */
    public Vector eliminarTemComprobante(Vector comando) throws SQLException {


        StringStatement st = null;



        try {



            st = new StringStatement(this.obtenerSQL("SQL_ELIMINAR_TABLA_TEMCOMPROBANTE"));
            comando.add(st.getSql());





        } catch (Exception e) {
            throw new SQLException("Error en rutina eliminarTemComprobante [mayorizacionDAO]... \n" + e.getMessage());

        } finally {

            if (st != null) {
                st = null;
            }


        }
        return comando;
    }

    /**
     * metodo para obtener los comprobantes del periodo seleccionado
     * @param comando vector con los stringstatements del proceso
     * @param user usuario en session
     * @param periodo periodo de captura
     * @author Mgarizao - GETOECH
     * @date 17/02/2010
     * @version 1.0
     * @return vector comando con el stringstatement
     * @throws SQLException
     */
    public Vector buscarComprobantePeriodo(Vector comando, Usuario user, String periodo) throws SQLException {


        StringStatement st = null;



        try {


            comando = this.eliminarTemComprobante(comando);
            st = new StringStatement(this.obtenerSQL("SQL_COMPROBANTE_PERIODO"));
            st.setString(user.getDstrct());
            st.setString(periodo);
            comando.add(st.getSql());
            comando = this.crearLlaveComprobantePeriodo(comando);





        } catch (Exception e) {
            throw new SQLException("Error en rutina buscarComprobantePeriodo [mayorizacionDAO]... \n" + e.getMessage());

        } finally {

            if (st != null) {
                st = null;
            }


        }
        return comando;
    }

    /**
     * metodo para crear la llave de la tabla tem.comprobante_x_periodo
     * @param comando vector de stringstatements del  proceso
     * @author MGarizao - GEOTECH
     * @date 17/02/2010
     * @version 1.0
     * @return vector con el stringstatement
     * @throws SQLException
     */
    public Vector crearLlaveComprobantePeriodo(Vector comando) throws SQLException {


        StringStatement st = null;



        try {


            st = new StringStatement(this.obtenerSQL("SQL_KEY_COMPROBANTE_PERIODO"));
            comando.add(st.getSql());





        } catch (Exception e) {
            throw new SQLException("Error en rutina crearLlaveComprobantePeriodo [mayorizacionDAO]... \n" + e.getMessage());

        } finally {

            if (st != null) {
                st = null;
            }


        }
        return comando;
    }

    /**
     * metodo para eliminar la tabla tem.detalle_comprobante_x_mayorizar
     * @param comando vector de stringstatements del proceso
     * @author MGarizao - GEOTECH
     * @date 17/02/2010
     * @version 1.0
     * @return vector con el stringstatement
     * @throws SQLException
     */
    public Vector eliminarTemDetalleComprobante(Vector comando) throws SQLException {


        StringStatement st = null;



        try {



            st = new StringStatement(this.obtenerSQL("SQL_ELIMINAR_TEMDETALLECOMPROBANTE"));
            comando.add(st.getSql());





        } catch (Exception e) {
            throw new SQLException("Error en rutina eliminarTemDetalleComprobante [mayorizacionDAO]... \n" + e.getMessage());

        } finally {

            if (st != null) {
                st = null;
            }


        }
        return comando;
    }

    /**
     * metodo para obtener los comprobantes a mayorizar
     * @param comando vector de stringstatements del proceso
     * @author MGarizao - GEOTECH
     * @date 17/02/2010
     * @version 1.0
     * @return vector con el stringstatement
     * @throws SQLException
     */
    public Vector buscarComprobanteMayorizar(Vector comando) throws SQLException {


        StringStatement st = null;



        try {


            comando = this.eliminarTemDetalleComprobante(comando);
            st = new StringStatement(this.obtenerSQL("SQL_COMPROBANTE_MAYORIZAR"));
            comando.add(st.getSql());
            comando = this.creatLlaveComprobanteMayorizar(comando);
            comando = this.crearIndexComprobanteMayorizar(comando, "SQL_INDEX1_COMPROBANTE_MAYORIZAR");
            comando = this.crearIndexComprobanteMayorizar(comando, "SQL_INDEX2_COMPROBANTE_MAYORIZAR");





        } catch (Exception e) {
            throw new SQLException("Error en rutina buscarComprobanteMayorizar [mayorizacionDAO]... \n" + e.getMessage());

        } finally {

            if (st != null) {
                st = null;
            }


        }
        return comando;
    }

    /**
     * metodo para crear la llave de la tabla tem.detalle_x_mayorizar
     * @param comando vector de stringstatements
     * @author MGarizao - GEOTECH
     * @date 17/02/2010
     * @version 1.0
     * @return vector con el stringstatement
     * @throws SQLException
     */
    public Vector creatLlaveComprobanteMayorizar(Vector comando) throws SQLException {


        StringStatement st = null;



        try {


            st = new StringStatement(this.obtenerSQL("SQL_KEY_COMPROBANTE_MAYORIZAR"));
            comando.add(st.getSql());





        } catch (Exception e) {
            throw new SQLException("Error en rutina creatLlaveComprobanteMayorizar [mayorizacionDAO]... \n" + e.getMessage());

        } finally {

            if (st != null) {
                st = null;
            }


        }
        return comando;
    }

    /**
     * metodo para crear indice sobre la tabla tem.detalle_comprobante_x_mayorizar
     * @param comando vector de stringstatements
     * @sql string con el nombre del sql a buscar en el xml
     * @author MGarizao - GEOTECH
     * @date 17/02/2010
     * @version 1.0
     * @return
     * @throws SQLException
     */
    public Vector crearIndexComprobanteMayorizar(Vector comando, String sql) throws SQLException {


        StringStatement st = null;



        try {


            st = new StringStatement(this.obtenerSQL(sql));
            comando.add(st.getSql());





        } catch (Exception e) {
            throw new SQLException("Error en rutina crearIndexComprobanteMayorizar [mayorizacionDAO]... \n" + e.getMessage());

        } finally {

            if (st != null) {
                st = null;
            }


        }
        return comando;
    }

    /**
     * metdo para actualizar los comprobantes sin cuenta en la tabla cuentas
     * @param comando vector con los stringstatements del proceso
     * @author MGarizao - GEOTECH
     * @date 17/02/2010
     * @version 1.0
     * @return vector con el stringstatement
     * @throws SQLException
     */
    public Vector actualizarComprobanteSinCuenta(Vector comando) throws SQLException {


        StringStatement st = null;



        try {



            st = new StringStatement(this.obtenerSQL("SQL_COMPROBANTE_SINCUENTA"));
            comando.add(st.getSql());





        } catch (Exception e) {
            throw new SQLException("Error en rutina actualizarComprobanteSinCuenta [mayorizacionDAO]... \n" + e.getMessage());

        } finally {

            if (st != null) {
                st = null;
            }


        }
        return comando;
    }

    /**
     * Metodo para acutalizar los comprobantes que alguna vez tuvieron cuentas en la tabla cuentas
     * @param comando vector con los stringstatements del proceso
     * @author MGarizao - GEOTECH
     * @date 17/02/2010
     * @version 1.0
     * @return vector con los stringstatements
     * @throws SQLException
     */
    public Vector actualizarComprobanteConCuenta(Vector comando) throws SQLException {


        StringStatement st = null;



        try {



            st = new StringStatement(this.obtenerSQL("SQL_COMPROBANTE_CONCUENTA"));
            comando.add(st.getSql());





        } catch (Exception e) {
            throw new SQLException("Error en rutina actualizarComprobanteConCuenta [mayorizacionDAO]... \n" + e.getMessage());

        } finally {

            if (st != null) {
                st = null;
            }


        }
        return comando;
    }

    /**
     * metodo para actualizar los comprobantes con diferencias en debito y credito
     * @param comando vector con los stringstatements del proceso
     * @author MGarizao - GEOTECH
     * @date 17/02/2010
     * @version 1.0
     * @return vector con el stringstatement
     * @throws SQLException
     */
    public Vector actualizarComprobanteDiferenciaTotales(Vector comando) throws SQLException {


        StringStatement st = null;



        try {



            st = new StringStatement(this.obtenerSQL("SQL_COMPROBANTE_DIFERENCIA_TOTALES"));
            comando.add(st.getSql());





        } catch (Exception e) {
            throw new SQLException("Error en rutina actualizarComprobanteDiferenciaTotales [mayorizacionDAO]... \n" + e.getMessage());

        } finally {

            if (st != null) {
                st = null;
            }


        }
        return comando;
    }

    /**
     * metodo para ingresar el mayo contable
     * @param comando vector con los stringstatements del proceso
     * @param user usuario en session
     * @param ano año de captura
     * @author MGarizao - GEOTECH
     * @date 17/02/2010
     * @version 1.0
     * @return vecto con el stringstatement
     * @throws SQLException
     */
    public Vector ingresarMayor(Vector comando, Usuario user, String ano) throws SQLException {


        StringStatement st = null;



        try {



            st = new StringStatement(this.obtenerSQL("SQL_INGRESAR_MAYOR"));
            st.setString(user.getDstrct());
            st.setString(ano);
            st.setString(user.getLogin());
            st.setString(user.getBase());
            st.setString(user.getDstrct());
            st.setString(ano);
            comando.add(st.getSql());





        } catch (Exception e) {
            throw new SQLException("Error en rutina ingresarMayor [mayorizacionDAO]... \n" + e.getMessage());

        } finally {

            if (st != null) {
                st = null;
            }


        }
        return comando;
    }

    /**
     * metodo para actualizar los totales debito y credito de los comprobantes
     * @param comando vector con los stringstatements del proceso
     * @param user usuario en session
     * @param ano año del periodo
     * @param mes mes del periodo
     * @author MGarizao - GEOTECH
     * @date 17/02/2010
     * @version 1.0
     * @return vector con el stringstatement
     * @throws SQLException
     */
    public Vector actualizarMayor(Vector comando, Usuario user, String ano, String mes) throws SQLException {


        StringStatement st = null;




        try {


            String cadena = this.obtenerSQL("SQL_ACTUALIZAR_CONMAYOR");
            cadena = cadena.replace("#c1#", "movdeb" + mes);
            cadena = cadena.replace("#c2#", "movdeb" + mes);
            cadena = cadena.replace("#c3#", "movcre" + mes);
            cadena = cadena.replace("#c4#", "movcre" + mes);
            st = new StringStatement(cadena);
            st.setString(user.getDstrct());
            st.setString(ano);
            comando.add(st.getSql());





        } catch (Exception e) {
            throw new SQLException("Error en rutina actualizarMayor [mayorizacionDAO]... \n" + e.getMessage());

        } finally {

            if (st != null) {
                st = null;
            }


        }
        return comando;
    }

    /**
     * metodo para actualizar la fecha del comprobante al mayor
     * @param comando vector de con los stringstatements del proceso
     * @param user usuario en session
     * @author MGarizao - GEOTECH
     * @date 17/02/2010
     * @version 1.0
     * @return vector con el stringstatement
     * @throws SQLException
     */
    public Vector actualizarFechaComprobante(Vector comando, Usuario user) throws SQLException {


        StringStatement st = null;




        try {



            st = new StringStatement(this.obtenerSQL("SQL_ACTUALIZAR_COMPROBANTE"));
            st.setString(user.getLogin());
            comando.add(st.getSql());





        } catch (Exception e) {
            throw new SQLException("Error en rutina actualizarFechaComprobante [mayorizacionDAO]... \n" + e.getMessage());

        } finally {

            if (st != null) {
                st = null;
            }


        }
        return comando;
    }

    /**
     * metodo para buscar el resultado de la mayorizacion
     * @param mes mes consultado
     * @param ano año consultado
     * @author MGarizao - GEOTECH
     * @date 17/02/2010
     * @version 1.0
     * @return vector de objetos mayorizados
     * @throws SQLException
     */
    public Vector buscarResultado(String mes, String ano) throws SQLException {


        Vector resultado = new Vector();
        PreparedStatement st = null;
        ResultSet rs = null;
        String SQL_RESULTADO = this.obtenerSQL("SQL_RESULTADO");
        Connection con = null;


        try {
            String cadena = SQL_RESULTADO;
            cadena = cadena.replace("#c1#", "movdeb" + mes);
            cadena = cadena.replace("#c2#", "movcre" + mes);
            cadena = cadena.replace("#c3#", "movdeb" + mes);
            cadena = cadena.replace("#c4#", "movcre" + mes);
            con = this.conectarJNDI("SQL_RESULTADO");
            st = con.prepareStatement(cadena);
            st.setString(1, ano);
            st.setString(2, ano + mes);
            st.setString(3, ano + mes);
            rs = st.executeQuery();

            while (rs.next()) {

                Mayorizacion mayor = new Mayorizacion();
                mayor.setFuente(rs.getString("fuente"));
                mayor.setValor_debito(rs.getFloat("valor_debito"));
                mayor.setValor_credito(rs.getFloat("valor_credito"));
                mayor.setDiferencia(rs.getFloat("diferencia"));
                resultado.add(mayor);
                mayor = null;

            }

        } catch (SQLException e) {
            e.printStackTrace();
            throw new SQLException("Error en rutina buscarResultado [mayorizacionDAO]... \n" + e.getMessage());
        } finally {
            if (st != null) {
                st.close();
            }
            if (rs != null) {
                rs.close();
            }
            this.desconectar(con);
        }
        return resultado;
    }

    /**
     * Metodo para buscar los comprobantes mayorizados
     * @author MGarizao - GEOTECH
     * @date 07/02/2010
     * @version 1.0
     * @return vector de objetos mayorizacion con la informacion
     * @throws SQLException
     */
    public Vector buscarComprobante() throws SQLException {


        Vector resultado = new Vector();
        PreparedStatement st = null;
        ResultSet rs = null;
        String SQL_COMPROBANTE = this.obtenerSQL("SQL_COMPROBANTE");
        Connection con = null;


        try {
            con = this.conectarJNDI("SQL_COMPROBANTE");
            st = con.prepareStatement(SQL_COMPROBANTE);
            rs = st.executeQuery();

            while (rs.next()) {

                Mayorizacion mayor = new Mayorizacion();
                mayor.setTipodoc(rs.getString("tipodoc"));
                mayor.setNumdoc(rs.getString("numdoc"));
                mayor.setGrupotransaccion(rs.getInt("grupo_transaccion"));
                mayor.setValor_debito(rs.getFloat("total_debito"));
                mayor.setValor_credito(rs.getFloat("total_credito"));
                mayor.setInd_deb_cre_cab(rs.getString("diferencia_totales"));
                mayor.setInd_deb_cre_cab_det(rs.getString("diferencia_cabecera_detalle"));
                mayor.setInd_cuenta_invalida(rs.getString("cuenta_invalida"));
                resultado.add(mayor);
                mayor = null;

            }

        } catch (SQLException e) {
            e.printStackTrace();
            throw new SQLException("Error en rutina buscarComprobante [mayorizacionDAO]... \n" + e.getMessage());
        } finally {
            if (st != null) {
                st.close();
            }
            if (rs != null) {
                rs.close();
            }
            this.desconectar(con);
        }
        return resultado;
    }

    /**
     * metodo para buscar el detalle de los comprobantes mayorizados
     * @author MGarizao - GEOTECH
     * @date 17/02/2010
     * @version 1.0
     * @return vector de objetos de mayorizacion con la informacion
     * @throws SQLException
     */
    public Vector buscarDetalleComprobante() throws SQLException {


        Vector resultado = new Vector();
        PreparedStatement st = null;
        ResultSet rs = null;
        String SQL_DETALLE_COMPROBANTE = this.obtenerSQL("SQL_DETALLE_COMPROBANTE");
        Connection con = null;


        try {
            con = this.conectarJNDI("SQL_DETALLE_COMPROBANTE");
            st = con.prepareStatement(SQL_DETALLE_COMPROBANTE);
            rs = st.executeQuery();

            while (rs.next()) {

                Mayorizacion mayor = new Mayorizacion();
                mayor.setTipodoc(rs.getString("tipodoc"));
                mayor.setNumdoc(rs.getString("numdoc"));
                mayor.setGrupotransaccion(rs.getInt("grupo_transaccion"));
                mayor.setTransaccion(rs.getInt("transaccion"));
                mayor.setCuenta(rs.getString("cuenta"));
                mayor.setAuxiliar(rs.getString("auxiliar"));
                mayor.setValor_debito(rs.getFloat("valor_debito"));
                mayor.setValor_credito(rs.getFloat("valor_credito"));
                mayor.setInd_cuenta_invalida(rs.getString("ind_cuenta_invalida"));
                resultado.add(mayor);
                mayor = null;
            }

        } catch (SQLException e) {
            e.printStackTrace();
            throw new SQLException("Error en rutina buscarDetalleComprobante [mayorizacionDAO]... \n" + e.getMessage());
        } finally {
            if (st != null) {
                st.close();
            }
            if (rs != null) {
                rs.close();
            }
            this.desconectar(con);
        }
        return resultado;
    }

    /**
     * metodo para la informacion del proceso de mayorizacion que se ha hecho
     * @param mes mes de consulta
     * @param ano año de consulta
     * @author MGarizao - GEOTECH
     * @date 17/02/2010
     * @version 1.0
     * @return vector de objetos con los vectores de la informacion
     * @throws SQLException
     */
    public Vector buscarProcesoMayorizado(String mes, String ano) throws SQLException {

        Vector mayorizado = new Vector();
        mayorizado.add(this.buscarResultado(mes, ano));
        mayorizado.add(this.buscarComprobante());
        mayorizado.add(this.buscarDetalleComprobante());
        return mayorizado;

    }

    /**
     * metodo para actualizar la talba comprobante en la desmayorizacion
     * @param comando vector de stringstatements del proceso
     * @param user usuario en session
     * @param ano año de captura
     * @param mes mes de carptura
     * @author MGarizao - GEOTECH
     * @date 17/02/2010
     * @version 1.0
     * @return vector con el stringstatement
     * @throws SQLException
     */
    public Vector actualizarDesComprobante(Vector comando, Usuario user, String ano, String mes) throws SQLException {


        StringStatement st = null;




        try {



            st = new StringStatement(this.obtenerSQL("SQL_ACTUALIZAR_DESCOMPROBANTE"));
            st.setString(user.getDstrct());
            st.setString(ano + mes);
            comando.add(st.getSql());





        } catch (Exception e) {
            throw new SQLException("Error en rutina actualizarDesComprobante [mayorizacionDAO]... \n" + e.getMessage());

        } finally {

            if (st != null) {
                st = null;
            }


        }
        return comando;
    }

    /**
     * metodo para obtener la cadena dinamica de los movimientos debitos y creditos en la desmayorizacion
     * @param mes mes de captura
     * @author MGarizao - GEOTECH
     * @date 17/02/2010
     * @version 1.0
     * @return cadena con los totales de los meses a mover
     */
      public String obtenerCadenaMovimiento(String mes) {
        String cadena = "";
        int num = Integer.parseInt(mes);
        switch (num) {
            case 1:
                cadena = "movdeb02 - movcre02" + "+ movdeb03 - movcre03" + "+ movdeb04 - movcre04" + "+ movdeb05 - movcre05" + "+ movdeb06 - movcre06" + "+ movdeb07 - movcre07" + "+ movdeb08 - movcre08" + "+ movdeb09 - movcre09" + "+ movdeb10 - movcre10" + "+ movdeb11 - movcre11" + "+ movdeb12 - movcre12" + "+ movdeb13 - movcre13";
                break;
            case 2:
                cadena = "movdeb01 - movcre01" + "+ movdeb03 - movcre03" + "+ movdeb04 - movcre04" + "+ movdeb05 - movcre05" + "+ movdeb06 - movcre06" + "+ movdeb07 - movcre07" + "+ movdeb08 - movcre08" + "+ movdeb09 - movcre09" + "+ movdeb10 - movcre10" + "+ movdeb11 - movcre11" + "+ movdeb12 - movcre12"+ "+ movdeb13 - movcre13";
                break;
            case 3:
                cadena = "movdeb01 - movcre01" + "+ movdeb02 - movcre02" + "+ movdeb04 - movcre04" + "+ movdeb05 - movcre05" + "+ movdeb06 - movcre06" + "+ movdeb07 - movcre07" + "+ movdeb08 - movcre08" + "+ movdeb09 - movcre09" + "+ movdeb10 - movcre10" + "+ movdeb11 - movcre11" + "+ movdeb12 - movcre12"+ "+ movdeb13 - movcre13";
                break;
            case 4:
                cadena = "movdeb01 - movcre01" + "+ movdeb02 - movcre02" + "+ movdeb03 - movcre03" + "+ movdeb05 - movcre05" + "+ movdeb06 - movcre06" + "+ movdeb07 - movcre07" + "+ movdeb08 - movcre08" + "+ movdeb09 - movcre09" + "+ movdeb10 - movcre10" + "+ movdeb11 - movcre11" + "+ movdeb12 - movcre12"+ "+ movdeb13 - movcre13";
                break;
            case 5:
                cadena = "movdeb01 - movcre01" + "+ movdeb02 - movcre02" + "+ movdeb03 - movcre03" + "+ movdeb04 - movcre04" + "+ movdeb06 - movcre06" + "+ movdeb07 - movcre07" + "+ movdeb08 - movcre08" + "+ movdeb09 - movcre09" + "+ movdeb10 - movcre10" + "+ movdeb11 - movcre11" + "+ movdeb12 - movcre12"+ "+ movdeb13 - movcre13";
                break;
            case 6:
                cadena = "movdeb01 - movcre01" + "+ movdeb02 - movcre02" + "+ movdeb03 - movcre03" + "+ movdeb04 - movcre04" + "+ movdeb05 - movcre05" + "+ movdeb07 - movcre07" + "+ movdeb08 - movcre08" + "+ movdeb09 - movcre09" + "+ movdeb10 - movcre10" + "+ movdeb11 - movcre11" + "+ movdeb12 - movcre12"+ "+ movdeb13 - movcre13";
                break;
            case 7:
                cadena = "movdeb01 - movcre01" + "+ movdeb02 - movcre02" + "+ movdeb03 - movcre03" + "+ movdeb04 - movcre04" + "+ movdeb05 - movcre05" + "+ movdeb06 - movcre06" + "+ movdeb08 - movcre08" + "+ movdeb09 - movcre09" + "+ movdeb10 - movcre10" + "+ movdeb11 - movcre11" + "+ movdeb12 - movcre12"+ "+ movdeb13 - movcre13";
                break;
            case 8:
                cadena = "movdeb01 - movcre01" + "+ movdeb02 - movcre02" + "+ movdeb03 - movcre03" + "+ movdeb04 - movcre04" + "+ movdeb05 - movcre05" + "+ movdeb06 - movcre06" + "+ movdeb07 - movcre07" + "+ movdeb09 - movcre09" + "+ movdeb10 - movcre10" + "+ movdeb11 - movcre11" + "+ movdeb12 - movcre12"+ "+ movdeb13 - movcre13";
                break;
            case 9:
                cadena = "movdeb01 - movcre01" + "+ movdeb02 - movcre02" + "+ movdeb03 - movcre03" + "+ movdeb04 - movcre04" + "+ movdeb05 - movcre05" + "+ movdeb06 - movcre06" + "+ movdeb07 - movcre07" + "+ movdeb08 - movcre08" + "+ movdeb10 - movcre10" + "+ movdeb11 - movcre11" + "+ movdeb12 - movcre12"+ "+ movdeb13 - movcre13";
                break;
            case 10:
                cadena = "movdeb01 - movcre01" + "+ movdeb02 - movcre02" + "+ movdeb03 - movcre03" + "+ movdeb04 - movcre04" + "+ movdeb05 - movcre05" + "+ movdeb06 - movcre06" + "+ movdeb07 - movcre07" + "+ movdeb08 - movcre08" + "+ movdeb09 - movcre09" + "+ movdeb11 - movcre11" + "+ movdeb12 - movcre12"+ "+ movdeb13 - movcre13";
                break;
            case 11:
                cadena = "movdeb01 - movcre01" + "+ movdeb02 - movcre02" + "+ movdeb03 - movcre03" + "+ movdeb04 - movcre04" + "+ movdeb05 - movcre05" + "+ movdeb06 - movcre06" + "+ movdeb07 - movcre07" + "+ movdeb08 - movcre08" + "+ movdeb09 - movcre09" + "+ movdeb10 - movcre10" + "+ movdeb12 - movcre12"+ "+ movdeb13 - movcre13";
                break;
            case 12:
                cadena = "movdeb01 - movcre01" + "+ movdeb02 - movcre02" + "+ movdeb03 - movcre03" + "+ movdeb04 - movcre04" + "+ movdeb05 - movcre05" + "+ movdeb06 - movcre06" + "+ movdeb07 - movcre07" + "+ movdeb08 - movcre08" + "+ movdeb09 - movcre09" + "+ movdeb10 - movcre10" + "+ movdeb11 - movcre11"+ "+ movdeb13 - movcre13";
                break;
            case 13:
                cadena = "movdeb01 - movcre01" + "+ movdeb02 - movcre02" + "+ movdeb03 - movcre03" + "+ movdeb04 - movcre04" + "+ movdeb05 - movcre05" + "+ movdeb06 - movcre06" + "+ movdeb07 - movcre07" + "+ movdeb08 - movcre08" + "+ movdeb09 - movcre09" + "+ movdeb10 - movcre10" + "+ movdeb11 - movcre11"+ "+ movdeb12 - movcre12";
                break;               
        }
        return cadena;
    }

    /**
     * metodo para actualizar la tabla mayor en la desmayorizacion
     * @param comando vector de stringstatements
     * @param user usuario en session
     * @param mes mes de captura
     * @param ano año de catura
     * @author MGarizao - GEOTECH
     * @date 17/02/2010
     * @version 1.0
     * @return vector con el stringstatement
     * @throws SQLException
     */
    public Vector actualizarDesMayor(Vector comando, Usuario user, String mes, String ano) throws SQLException {


        StringStatement st = null;
        String cadena = this.obtenerCadenaMovimiento(mes);


        try {


            String cadenaReplace = this.obtenerSQL("SQL_ACTUALIZAR_DESMAYOR");
            cadenaReplace = cadenaReplace.replace("#c1#", "movdeb" + mes);
            cadenaReplace = cadenaReplace.replace("#c2#", "movcre" + mes);
            cadenaReplace = cadenaReplace.replace("#c3#", cadena);
            st = new StringStatement(cadenaReplace);
            st.setString(user.getDstrct());
            st.setString(ano);
            comando.add(st.getSql());





        } catch (Exception e) {
            throw new SQLException("Error en rutina actualizarDesMayor [mayorizacionDAO]... \n" + e.getMessage());

        } finally {

            if (st != null) {
                st = null;
            }


        }
        return comando;
    }

    /**
     * metodo para acturlizar la tabla mayor_subledger en la desmayorizacion
     * @param comando vector de stringstatements
     * @param user usuario en session
     * @param mes mes de captura
     * @param ano año de caprtura
     * @author MGarizao - GEOTECH
     * @date 17/02/2010
     * @version 1.0
     * @return vector con el stringstatement
     * @throws SQLException
     */
    public Vector actualizarDesSubledger(Vector comando, Usuario user, String mes, String ano) throws SQLException {


        StringStatement st = null;
        String cadena = this.obtenerCadenaMovimiento(mes);


        try {


            String cadenaReplace = this.obtenerSQL("SQL_ACTUALIZAR_DESUBLEDGER");
            cadenaReplace = cadenaReplace.replace("#c1#", "movdeb" + mes);
            cadenaReplace = cadenaReplace.replace("#c2#", "movcre" + mes);
            cadenaReplace = cadenaReplace.replace("#c3#", cadena);
            st = new StringStatement(cadenaReplace);
            st.setString(user.getDstrct());
            st.setString(ano);
            comando.add(st.getSql());





        } catch (Exception e) {
            throw new SQLException("Error en rutina actualizarDesSubledger [mayorizacionDAO]... \n" + e.getMessage());

        } finally {

            if (st != null) {
                st = null;
            }


        }
        return comando;
    }

    /**
     * Metodo para ejecutar el vector de sql y hacer comit
     * @param comando vector con las sentencias sql
     * @author MGarizao - GEOTECH
     * @date 17/02/2010
     * @version 1.0
     * @return mensaje de confirmacion
     * @throws SQLException
     */
    public String ejecutarComando(Vector comando) throws SQLException {

        String msn = "";
        Statement stmt = null;
        Connection con = null;

        con = this.conectarBDJNDI("fintra");
        con.setAutoCommit(false);
        try {
            stmt = con.createStatement();

            for (int i = 0; i < comando.size(); i++) {
                String comandos = (String) comando.elementAt(i);
                stmt.addBatch(comandos);

            }

            stmt.executeBatch();
            stmt.clearBatch();
            con.commit();
            msn = "Transaccion exitosa";
        } catch (SQLException e) {

            con.rollback();
            msn = "ERROR DURANTE LA TRANSACCION LOS CAMBIOS NO SE PRODUJERON EL LA BASE DE DATOS:\n" + e.getMessage() + "La siguiente exception es : ----> " + e.getNextException();
            throw new SQLException("ERROR DURANTE LA TRANSACCION LOS CAMBIOS NO SE PRODUJERON EL LA BASE DE DATOS:\n" + e.getMessage() + "La siguiente exception es : ----> " + e.getNextException());
        } finally {
            if (stmt != null) {
                stmt.close();
            }
            if (con != null) {
                this.desconectar(con);
            }
        }

        return msn;
    }
}
