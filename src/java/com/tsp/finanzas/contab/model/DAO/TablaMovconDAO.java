/********************************************************************
 *  Nombre Clase.................   TablaMovconDAO.java
 *  Descripci�n..................   DAO de la tabla movcon
 *  Autor........................   Ing. Leonardo Parody
 *  Fecha........................   09.02.2006
 *  Versi�n......................   1.0
 *  Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.finanzas.contab.model.DAO;


import java.sql.*;
import java.util.*;
import com.tsp.util.*;
import com.tsp.util.connectionpool.PoolManager;



/**
 *
 * @author  EQUIPO12
 */
public class TablaMovconDAO {
    private Calendar fechadehoy = Calendar.getInstance() ;
    private Calendar fechainicioproceso ;
    private Calendar fechafinalproceso ;
    private String fechaini = "" ;
    private String fechafin = "" ;
    private LinkedList meses = new LinkedList() ;
    /** Creates a new instance of TablaMovconDAO */
    public TablaMovconDAO() {
        fechadehoy.set( 0, 0, 0, 0, 0, 0 ) ;
    }
    
    
    private final String SQL_CONSULT_MSF900 = " SELECT " +
    " ACCOUNT_CODE, " +
    " MIMS_SL_KEY, " +
    " FULL_PERIOD," +
    " TRAN_AMOUNT," +
    " CURRENCY_TYPE," +
    " REC900_TYPE," +
    " PROCESS_DATE," +
    " TRANSACTION_NO" +
    " FROM MSF900 WHERE  DSTRCT_CODE = ? AND PROCESS_DATE = ? AND SUBSTR(ACCOUNT_CODE,1,1) IN ('C','G')" ;
    
    private final String SQL_CONSULT_MSF900_PERIODO = " SELECT " +
    " ACCOUNT_CODE, " +
    " MIMS_SL_KEY, " +
    " FULL_PERIOD," +
    " TRAN_AMOUNT," +
    " CURRENCY_TYPE," +
    " REC900_TYPE," +
    " PROCESS_DATE," +
    " TRANSACTION_NO" +
    " FROM MSF900 WHERE  DSTRCT_CODE = ? AND FULL_PERIOD = ?  AND SUBSTR(ACCOUNT_CODE,1,1) IN ('C','G')"  ;
    
    
    private final String SQL_INSERT_MOVCON = "INSERT INTO con.movcon (cod_cuenta, cod_auxiliar, periodo, valor_debito, valor_credito, " +
    "moneda, tipo_registro, process_date, transaction_no, dstrct, reg_status, creation_date, creation_user," +
    "last_update, user_update, base) VALUES " +
    "(?,?,?,?,?,?,?,?,?,?,'', ?, ?, ?,'',?)";
    
    private final String SQL_SEARCH_GASTOS_ADMIN = "SELECT " +
    "   dstrct, "+
    "   cod_cuenta, " +
    "   periodo,    " +
    "   sum(valor_debito) AS debito, " +
    "   sum(valor_credito) AS credito," +
    "   (sum(valor_debito)-sum(valor_credito)) AS total" +
    " FROM " +
    "   con.movcon" +
    " WHERE " +
    "   dstrct = ? AND " +
    "   creation_date BETWEEN  ? AND  ? AND" +
    "   substr(cod_cuenta,1,1) IN ('C','G')" +
    " GROUP BY " +
    "   cod_cuenta, periodo, dstrct";
    
    private final String SQL_SEARCH_GASTOS_ADMIN_PERIODO = "SELECT " +
    "   dstrct, "+
    "   cod_cuenta, " +
    "   periodo,    " +
    "   sum(valor_debito) AS debito, " +
    "   sum(valor_credito) AS credito," +
    "   (sum(valor_debito)-sum(valor_credito)) AS total" +
    " FROM " +
    "   con.movcon" +
    " WHERE " +
    "   dstrct = ? AND " +
    "   periodo = ? AND "+
    "   substr(cod_cuenta,1,1) IN ('C','G')" +
    " GROUP BY " +
    "   cod_cuenta, periodo, dstrct";
    
    private final String SQL_DELETE_PERIODO_MOVCON = " DELETE FROM con.movcon WHERE periodo = ? ";
    private final String SQL_UPDATE_GASTOS_ADMIN = "UPDATE con.pto_gastos_admin SET vr_ejecutado_";
    private final String SQL_UPDATE_GASTOS_ADMIN1 = " = ? , last_update = now(), user_update = ? WHERE " +
    "    cuenta = ? AND ano = ? ";
    
    private final String SEARCH_CUENTAS_GASTOS_ADMIN = "SELECT " +
    "    * " +
    " FROM    " +
    "    con.pto_gastos_admin " +
    " WHERE" +
    "    cuenta = ? AND ano = ?";
    
    private final String INSERT_GASTOS_ADMIN =  "INSERT INTO con.pto_gastos_admin" +
    "(dstrct, cuenta, ano, creation_user, creation_date, last_update, base, vr_ejecutado_";
    private final String INSERT_GASTOS_ADMIN1 =  ")" +
    "VALUES" +
    "(?,?,?,?,now(),now(),?,?)";
    
    private final String INICIALIZA_PTO_GASTOS_ADMIN = "UPDATE con.pto_gastos_admin SET vr_ejecutado_";
    private final String INICIALIZA_PTO_GASTOS_ADMIN1 = " = ?, last_update = now(), user_update = ? WHERE ano = ? AND dstrct = ?";
    
    /**
     * Metodo InsertCuentaValida , Metodo que Ingresa los registros de la tabla MSF900 en la tabla movcon.
     * @autor : Ing. Leonardo Parody
     * @param : String processdate
     * @param : String dstrct
     * @param : String base
     * @param : String user
     * @param : String dstrct_code
     * @throws : SQLException
     * @version : 1.0
     */
    
    public void InsertMSF900movcon( List processdates, String dstrct, String base, String user ) throws SQLException {
        this.fechaini = Util.fechaActualTIMESTAMP() ;
        //////System.out.println("fechaini = "+fechaini);
        Connection         conOracle    = null ;
        PreparedStatement        st1    = null ;
        ResultSet                rs1    = null ;
        PoolManager     poolManager1    = null ;
        Connection               con    = null ;
        PreparedStatement         st    = null ;
        ResultSet                 rs    = null ;
        PoolManager      poolManager    = null ;
        try{
            poolManager1 = PoolManager.getInstance() ;
            conOracle = poolManager1.getConnection( "oracle" );
            poolManager = PoolManager.getInstance() ;
            con = poolManager.getConnection( "sot" ) ;
            boolean autocommit = con.getAutoCommit() ;
            st = con.prepareStatement( SQL_INSERT_MOVCON ) ;
            //////System.out.println("Autocommit : " + autocommit) ;
            //con.setAutoCommit( false ) ;
            st1 = conOracle.prepareStatement( SQL_CONSULT_MSF900 ) ;
            for ( int i = 0 ; i < processdates.size() ; i++ ) {
                String processdate = ( String ) processdates.get( i ) ;
                ////////System.out.println("processdate = "+processdate+"  index = "+i);
                if ( conOracle != null ) {
                    st1.clearParameters() ;
                    st1.setString( 1, dstrct ) ;
                    st1.setString( 2, processdate ) ;
                    rs = st1.executeQuery() ;
                    int j = 0 ;
                    ////////System.out.println("voy a ejecutar la insercion de los resgistros para el processdate = "+processdate);
                    while ( rs.next() ) {
                        this.fechainicioproceso =  Calendar.getInstance() ;
                        j++ ;
                        if ( con != null ) {
                            st = con.prepareStatement( SQL_INSERT_MOVCON ) ;
                            st.setString( 1, rs.getString( 1 ) ) ;
                            st.setString( 2, rs.getString( 2 ) ) ;
                            st.setString( 3, rs.getString( 3 ) ) ;
                            double amount = rs.getDouble( 4 ) ;
                            if ( amount > 0 ) {
                                st.setDouble( 4, amount ) ;
                                st.setDouble( 5, 0 ) ;
                            }else {
                                st.setDouble( 4, 0 ) ;
                                st.setDouble( 5, amount*( -1 ) ) ;
                            }
                            st.setString( 6, rs.getString( 5 ) ) ;
                            st.setString( 7, rs.getString( 6 ) ) ;
                            st.setString( 8, rs.getString( 7 ) ) ;
                            st.setString( 9, rs.getString( 8 ) ) ;
                            st.setString( 10, dstrct ) ;
                            st.setTimestamp( 11, Util.ConvertiraTimestamp( Util.getFechaActual_String( 6 ) ) ) ;
                            st.setString( 12 , user ) ;
                            st.setTimestamp( 13, Util.ConvertiraTimestamp( Util.getFechaActual_String( 6 ) ) ) ;
                            st.setString( 14, base ) ;
                            st.executeUpdate() ;
                        }
                    }
                    ////////System.out.println("Numero de registros = "+j +"   para el process_date = "+processdate);
                    ////////System.out.println("termine la insercion de los resgistros para el processdate = "+processdate);
                }
            }
            
            //st.executeBatch() ;
            //con.commit() ;
            //con.setAutoCommit( autocommit ) ;
            this.fechafin = Util.fechaActualTIMESTAMP() ;
            ////////System.out.println("fechafin = "+fechafin) ;
            this.fechafinalproceso = Calendar.getInstance() ;
        
        }catch(SQLException e){
            throw new SQLException("ERROR AL REALIZAR EL PASO DE INFORMACION " + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close() ;
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if(st1 != null){
                try{
                    st1.close() ;
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null ){
                poolManager.freeConnection("sot", con );
            }
            if ( conOracle != null ){
                poolManager.freeConnection("oracle", conOracle );
            }
        }
    }
    
    /**
     * Metodo InsertCuentaValida , Metodo que Ingresa los registros de la tabla MSF900 en la tabla movcon.
     * @autor : Ing. Leonardo Parody
     * @param : String periodo
     * @param : String dstrct
     * @param : String base
     * @param : String user
     * @param : String dstrct_code
     * @throws : SQLException
     * @version : 1.0
     */
    public void InsertMSF900movconPeriodo( String periodo, String dstrct, String base, String user ) throws SQLException {
        this.fechaini = Util.fechaActualTIMESTAMP() ;
        ////////System.out.println("fechaini = "+fechaini);
        Connection         conOracle    = null ;
        PreparedStatement        st1    = null ;
        ResultSet                rs1    = null ;
        PoolManager     poolManager1    = null ;
        Connection               con    = null ;
        PreparedStatement         st    = null ;
        ResultSet                 rs    = null ;
        PoolManager      poolManager    = null ;
        try{
            poolManager1 = PoolManager.getInstance() ;
            conOracle = poolManager1.getConnection( "oracle" );
            poolManager = PoolManager.getInstance() ;
            con = poolManager.getConnection( "sot" ) ;
            boolean autocommit = con.getAutoCommit() ;
            st = con.prepareStatement( SQL_INSERT_MOVCON ) ;
            ////////System.out.println("Autocommit : " + autocommit) ;
            con.setAutoCommit( false ) ;
            st1 = conOracle.prepareStatement( SQL_CONSULT_MSF900_PERIODO ) ;
            ////////System.out.println(Runtime.getRuntime().freeMemory()/1024/1024 );
        ////////System.out.println(Runtime.getRuntime().maxMemory()/1024/1024  );
        ////////System.out.println(Runtime.getRuntime().totalMemory()/1024/1024);
            if ( conOracle != null ) {
                st1.clearParameters() ;
                st1.setString( 1, dstrct ) ;
                st1.setString( 2, periodo ) ;
                ////////System.out.println("consulta a oracle por periodo st1 = "+st1);
                rs = st1.executeQuery() ;
                int j = 0 ;
                ////////System.out.println("voy a ejecutar la insercion de los resgistros para el periodo = "+periodo);
                while ( rs.next() ) {
                    this.fechainicioproceso =  Calendar.getInstance() ;
                    j++ ;
                    if ( con != null ) {
                        st.clearParameters() ;
                        st.setString( 1, rs.getString( 1 ) ) ;
                        st.setString( 2, rs.getString( 2 ) ) ;
                        st.setString( 3, rs.getString( 3 ) ) ;
                        double amount = rs.getDouble( 4 ) ;
                        if ( amount > 0 ) {
                            st.setDouble( 4, amount ) ;
                            st.setDouble( 5, 0 ) ;
                        }else {
                            st.setDouble( 4, 0 ) ;
                            st.setDouble( 5, amount*( -1 ) ) ;
                        }
                        st.setString( 6, rs.getString( 5 ) ) ;
                        st.setString( 7, rs.getString( 6 ) ) ;
                        st.setString( 8, rs.getString( 7 ) ) ;
                        st.setString( 9, rs.getString( 8 ) ) ;
                        st.setString( 10, dstrct ) ;
                        st.setTimestamp( 11, Util.ConvertiraTimestamp( Util.getFechaActual_String( 6 ) ) ) ;
                        st.setString( 12 , user ) ;
                        st.setTimestamp( 13, Util.ConvertiraTimestamp( Util.getFechaActual_String( 6 ) ) ) ;
                        st.setString( 14, base ) ;
                        st.addBatch() ;
                    }
                }
               // //////System.out.println("Numero de registros = "+j +"   para el periodo = "+periodo);
                ////////System.out.println("termine la insercion de los resgistros para el processdate = "+processdate);
            }
            st.executeBatch() ;
            con.commit() ;
            con.setAutoCommit( autocommit ) ;
            this.fechafin = Util.fechaActualTIMESTAMP() ;
            ////////System.out.println("fechafin = "+fechafin) ;
            this.fechafinalproceso = Calendar.getInstance() ;
            
        }catch(SQLException e){
            con.rollback() ;
            throw new SQLException("ERROR AL REALIZAR EL PASO DE INFORMACION " + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close() ;
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if(st1 != null){
                try{
                    st1.close() ;
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null ){
                poolManager.freeConnection("sot", con );
            }
            if ( conOracle != null ){
                poolManager.freeConnection("oracle", conOracle );
            }
        }
    }
    
    /**
     * Metodo InsertMovconPresGastosAdmin , Metodo que obtiene los valores ejecutados de la tabla movcon y los ingresa en la tabla pto_gastos_admin.
     * @autor : Ing. Leonardo Parody
     * @param : String a�omes
     * @param : String dstrct
     * @param : String base
     * @param : String user
     * @param : String check
     * @throws : SQLException
     * @version : 1.0
     */
    
    public void InsertMovconPresGastosAdmin(String dstrct, String base, String user, String check, String a�omes) throws SQLException {
        PoolManager     poolManager1    = null ;
        Connection               con    = null ;
        PreparedStatement         st    = null ;
        PreparedStatement        st1    = null ;
        PreparedStatement        st2    = null ;
        PreparedStatement        st3    = null ;
        ResultSet                 rs    = null ;
        ResultSet                rs1    = null ;
        ResultSet                rs2    = null ;
        PoolManager      poolManager    = null ;
        try{
            poolManager = PoolManager.getInstance() ;
            con = poolManager.getConnection("sot") ;
            boolean autocommit = con.getAutoCommit() ;
            /*if ( check.equalsIgnoreCase( "checked" ) ) {
                //Inicializa los meses que el usuario requiera.
                this.InicializaPresGastosAdmin( dstrct, user, a�omes, base ) ;
            }*/
            if ( con != null ) {
                st = con.prepareStatement( SQL_SEARCH_GASTOS_ADMIN ) ;
                st.setString( 1, dstrct ) ;
                st.setString( 2, this.fechaini ) ;
                st.setString( 3, Util.fechaActualTIMESTAMP() ) ;
                //////System.out.println("st = "+st);
                rs = st.executeQuery() ;
                while ( rs.next() ) {
                    String cod_cuenta = rs.getString( "cod_cuenta" ) ;
                    String periodo = rs.getString( "periodo" ) ;
                    String a�o = periodo.substring( 0,4 ) ;
                    String mes = periodo.substring( 4,6 ) ;
                    double valor = rs.getDouble( "total" ) ;
                    String mesdeeje = "" ;
                    String cero = mes.substring( 0,1 ) ;
                    if ( cero.equalsIgnoreCase( "0" ) ) {
                        mesdeeje =  mes.substring( 1,2 ) ;
                    } else {
                        mesdeeje =  mes ;
                    }
                    /*String modificar = "no" ;
                    if ( check.equalsIgnoreCase( "checked" ) ) {
                        for( int i = 0; i < this.meses.size(); i++ ) {
                            String mesbandera = ( String ) this.meses.get( i ) ;
                            //////System.out.println("mesdeeje = "+mesdeeje+"  mesbandera = "+mesbandera);
                            if ( mesdeeje.equalsIgnoreCase( mesbandera ) ) {
                                //////System.out.println("si voy a hacer la modi o inser");
                                modificar = "si";
                            }
                        }
                    } else {
                        modificar = "si" ;
                    }*/
                    /*if ( modificar.equalsIgnoreCase( "si" ) ) {*/
                        if ( con != null ) {
                            
                            st1 = con.prepareStatement( SEARCH_CUENTAS_GASTOS_ADMIN ) ;
                            st1.setString( 1, cod_cuenta ) ;
                            st1.setString( 2, a�o ) ;
                            //////System.out.println( "st1 = " + st1 ) ;
                            rs1 = st1.executeQuery() ;
                            
                        }
                        if ( con != null ) {
                            
                            if ( rs1.next() ) {
                                
                                double ejecutado = 0 ;
                                valor += rs1.getDouble( "vr_ejecutado_" + mesdeeje ) ;
                                st2 = con.prepareStatement( UpdatePtoGastos( mes ) ) ;
                                ////////System.out.println("a�o = "+a�o+" mes = "+mes+"   valor = "+valor);
                                /////////System.out.println("a�o = "+a�o+" mes = "+mes+"   ejecutado = "+ejecutado);
                                st2.setDouble( 1, valor ) ;
                                st2.setString( 2, user ) ;
                                st2.setString( 3, cod_cuenta ) ;
                                st2.setString( 4, a�o ) ;
                                st2.executeUpdate() ;
                                //////System.out.println("st2 = "+st2);
                                
                            } else {
                                //dstrct, cuenta, ano, creation_user, creation_date, last_update, base, vr_ejecutado_
                                st2 = con.prepareStatement( InsertPtoGastos( mes ) ) ;
                                ////////System.out.println("Consulta   =   "+st2);
                                st2.setString( 1, dstrct ) ;
                                st2.setString( 2, cod_cuenta ) ;
                                st2.setString( 3, a�o ) ;
                                st2.setString( 4, user ) ;
                                st2.setString( 5, base ) ;
                                st2.setDouble( 6, valor ) ;
                                st2.executeUpdate() ;
                                ////////System.out.println("st2 = "+st2);
                                
                            }
                        }
                    /*}*/
                }
            }
        }catch( SQLException e ) {
            
            throw new SQLException("ERROR AL REALIZAR EL PASO DE INFORMACION " + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if(st1 != null){
                try{
                    st1.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null ){
                poolManager.freeConnection("sot", con );
            }
        }
    }
    
    
    /**
     * Metodo InicializaPresGastosAdmin , Metodo que inicializa los registros .
     * @autor : Ing. Leonardo Parody
     * @param : String a�omes
     * @param : String dstrct
     * @param : String base
     * @param : String user
     * @throws : SQLException
     * @version : 1.0
     */
    
    public void InicializaPresGastosAdmin(String dstrct, String user, String a�omes, String base) throws SQLException {
        ////////System.out.println("Voy a inicializar los datos");
        
        int meses = 0;
        PoolManager poolManager1 = null ;
        Connection          con  = null ;
        PreparedStatement    st  = null ;
        ResultSet            rs  = null ;
        PoolManager poolManager  = null ;
        try{
            String a�odesde = a�omes.substring( 0,4 ) ;
            String mesdesde = a�omes.substring( 4,6 ) ;
            poolManager = PoolManager.getInstance()   ;
            con = poolManager.getConnection( "sot" )  ;
            boolean autocommit = con.getAutoCommit()  ;
            if ( con != null ) {
                
                ////////System.out.println( "voy a empezar el " ) ;
                this.meses.clear() ;
                st = con.prepareStatement( InicializaPtoGastos( mesdesde ) ) ;
                st.setDouble( 1, 0 ) ;
                st.setString( 2, user ) ;
                st.setString( 3, "" + a�odesde ) ;
                st.setString( 4, dstrct ) ;
                //////System.out.println( "st = " + st ) ;
                st.executeUpdate() ;
 
            }
        }catch(SQLException e){
            
            throw new SQLException("ERROR AL REALIZAR LA INICIALIZACION " + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close() ;
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null ){
                poolManager.freeConnection( "sot", con ) ;
            }
            
        }
    }
    
    /**
     * Metodo InicializarMovcon , Metodo que elimina los registros de un periodo dado .
     * @autor : Ing. Leonardo Parody
     * @param : String a�omes
     * @throws : SQLException
     * @version : 1.0
     */
    
    public void InicializarMovcon( String a�omes ) throws SQLException {
        ////////System.out.println("Voy a inicializar los datos");
        
        int meses = 0;
        PoolManager poolManager1 = null;
        Connection con= null;
        PreparedStatement    st  = null;
        ResultSet            rs  = null;
        PoolManager poolManager  = null;
        try{
            poolManager = PoolManager.getInstance()   ;
            con = poolManager.getConnection( "sot" )  ;
            boolean autocommit = con.getAutoCommit()  ;
            if ( con != null ) {
                    st = con.prepareStatement( this.SQL_DELETE_PERIODO_MOVCON ) ;
                    st.setString( 1, a�omes ) ;
                    //////System.out.println( "st = " + st ) ;
                    st.executeUpdate() ;
                    
            }
        }catch(SQLException e){
            
            throw new SQLException("ERROR AL REALIZAR LA INICIALIZACION " + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close() ;
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null ){
                poolManager.freeConnection( "sot", con ) ;
            }
            
        }
    }
    
    /**
     * Metodo InicializarMovcon , Metodo que elimina los registros de un periodo dado .
     * @autor : Ing. Leonardo Parody
     * @param : String a�omes
     * @throws : SQLException
     * @version : 1.0
     */
    
    public void InsertValoresejecutadosPorPeriodos( String dstrct, String periodo, String base, String user ) throws SQLException {
        ////////System.out.println("Voy a ingresar los valores ejecutados por periodo");
        
        int meses = 0;
        PoolManager poolManager1 = null;
        Connection          con  = null;
        PreparedStatement    st  = null;
        PreparedStatement    st1  = null;
        PreparedStatement    st2  = null;
        ResultSet            rs  = null;
        ResultSet            rs1  = null;
        PoolManager poolManager  = null;
        
        try{
            
            poolManager = PoolManager.getInstance()   ;
            con = poolManager.getConnection( "sot" )  ;
            boolean autocommit = con.getAutoCommit()  ;
            if ( con != null ) {
                
                st = con.prepareStatement( this.SQL_SEARCH_GASTOS_ADMIN_PERIODO ) ;
                st.setString( 1, dstrct ) ;
                st.setString( 2, periodo ) ;
                ////////System.out.println( "st = " + st ) ;
                rs = st.executeQuery() ;
                while ( rs.next() ) {
                    
                    String cod_cuenta = rs.getString( "cod_cuenta" ) ;
                    String a�o = periodo.substring( 0,4 ) ;
                    String mes = periodo.substring( 4,6 ) ;
                    double valor = rs.getDouble( "total" ) ;
                    String mesdeeje = "" ;
                    String cero = mes.substring( 0,1 ) ;
                    if ( cero.equalsIgnoreCase( "0" ) ) {
                        mesdeeje =  mes.substring( 1,2 ) ;
                    } else {
                        mesdeeje =  mes ;
                    }
                    if ( con != null ) {
                            
                            st1 = con.prepareStatement( SEARCH_CUENTAS_GASTOS_ADMIN ) ;
                            st1.setString( 1, cod_cuenta ) ;
                            st1.setString( 2, a�o ) ;
                            ////////System.out.println( "st1 = " + st1 ) ;
                            rs1 = st1.executeQuery() ;
                            
                        }
                        if ( con != null ) {
                            
                            if ( rs1.next() ) {
                                
                                double ejecutado = 0 ;
                                valor += rs1.getDouble( "vr_ejecutado_" + mesdeeje ) ;
                                st2 = con.prepareStatement( UpdatePtoGastos( mes ) ) ;
                                ////////System.out.println("a�o = "+a�o+" mes = "+mes+"   valor = "+valor);
                                ////////System.out.println("a�o = "+a�o+" mes = "+mes+"   ejecutado = "+ejecutado);
                                st2.setDouble( 1, valor ) ;
                                st2.setString( 2, user ) ;
                                st2.setString( 3, cod_cuenta ) ;
                                st2.setString( 4, a�o ) ;
                                st2.executeUpdate() ;
                                ////////System.out.println("st2 = "+st2);
                                
                            } else {
                                //dstrct, cuenta, ano, creation_user, creation_date, last_update, base, vr_ejecutado_
                                st2 = con.prepareStatement( InsertPtoGastos( mes ) ) ;
                                ////////System.out.println("Consulta   =   "+st2);
                                st2.setString( 1, dstrct ) ;
                                st2.setString( 2, cod_cuenta ) ;
                                st2.setString( 3, a�o ) ;
                                st2.setString( 4, user ) ;
                                st2.setString( 5, base ) ;
                                st2.setDouble( 6, valor ) ;
                                st2.executeUpdate() ;
                                ////////System.out.println("st2 = "+st2);
                                
                            }
                        }
                    /*}*/
                }
                    
                }
                
            
            
        }catch(SQLException e){
            
            throw new SQLException("ERROR AL REALIZAR EL INGRESO DE LOS REGISTROS  " + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close() ;
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null ){
                poolManager.freeConnection( "sot", con ) ;
            }
            
        }
    }
    
    /**
     * Metodo UpdatePtoGastos , Metodo que concatena una consulta ingresandole el numero de mes al
     * campo vlr_ejecutado_.
     * @autor : Ing. Leonardo Parody
     * @param : String mes
     * @return : String consulta
     * @throws : SQLException
     * @version : 1.0
     */
    
    private String UpdatePtoGastos(String mes){
        String consulta = "" ;
        String cero = mes.substring( 0,1 ) ;
        if ( cero.equalsIgnoreCase( "0" ) ) {
            consulta = this.SQL_UPDATE_GASTOS_ADMIN + mes.substring( 1, 2 ) + this.SQL_UPDATE_GASTOS_ADMIN1 ;
        } else {
            consulta = this.SQL_UPDATE_GASTOS_ADMIN + mes + this.SQL_UPDATE_GASTOS_ADMIN1 ;
        }
        return consulta ;
    }
    
    /**
     * Metodo InsertPtoGastos , Metodo que concatena una consulta ingresandole el numero de mes al
     * campo vlr_ejecutado_.
     * @autor : Ing. Leonardo Parody
     * @param : String mes
     * @return : String consulta
     * @throws : SQLException
     * @version : 1.0
     */
    
    private String InsertPtoGastos( String mes ) {
        String consulta = "" ;
        String cero = mes.substring( 0,1 ) ;
        if ( cero.equalsIgnoreCase( "0" ) ) {
            consulta = this.INSERT_GASTOS_ADMIN + mes.substring( 1, 2 ) + this.INSERT_GASTOS_ADMIN1 ;
        } else {
            consulta = this.INSERT_GASTOS_ADMIN + mes + this.INSERT_GASTOS_ADMIN1 ;
        }
        return consulta ;
    }
    
    /**
     * Metodo InicializaPtoGastos , Metodo que concatena una consulta ingresandole el numero de mes al
     * campo vlr_ejecutado_.
     * @autor : Ing. Leonardo Parody
     * @param : String mes
    * @return : String consulta
     * @throws : SQLException
     * @version : 1.0
     */
    
    private String InicializaPtoGastos( String mes ){
        String consulta = "" ;
        String cero = mes.substring( 0, 1 ) ;
        if ( cero.equalsIgnoreCase( "0" ) ) {
            consulta = this.INICIALIZA_PTO_GASTOS_ADMIN  + mes.substring( 1, 2 ) + this.INICIALIZA_PTO_GASTOS_ADMIN1 ;
        } else {
            consulta = this.INICIALIZA_PTO_GASTOS_ADMIN  + mes + this.INICIALIZA_PTO_GASTOS_ADMIN1 ;
        }
        return consulta ;
    }
    
    /**
     * Getter for property fechadehoy.
     * @return Value of property fechadehoy.
     */
    
    public Calendar getFechadeHoy() {
        return fechadehoy ;
    }
    
    /**
     * Setter for property fechadehoy.
     * @param fechadehoy New value of property fechadehoy.
     */
    
    public void setFechadeHoy( Calendar Fechadehoy ) {
        
        this.fechadehoy = Fechadehoy ;
    }
    
    /**
     * Getter for property fechainicioproceso.
     * @return Value of property fechainicioproceso.
     */
    
    public Calendar getFechainicioproceso() {
        return fechainicioproceso ;
    }
    
}
