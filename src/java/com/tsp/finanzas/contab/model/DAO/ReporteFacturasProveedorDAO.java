/***********************************************************************
* Nombre ......................ReporteFacturasProveedorDAO.java        *
* Descripci�n..................Clase DAO para el plan de cuentas       *
* Autor........................LREALES                                 *
* Fecha Creaci�n...............24 de junio de 2006, 09:13 AM           *
* Versi�n......................1.0                                     *
* Coyright.....................Transportes Sanchez Polo S.A.           *
***********************************************************************/

package com.tsp.finanzas.contab.model.DAO;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.*;
import com.tsp.finanzas.contab.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;
import com.tsp.operation.model.DAOS.MainDAO;

public class ReporteFacturasProveedorDAO extends MainDAO {
    
    private ReporteFacturasProveedor reporte;
    private Vector vec_reporte;
    
    /** Creates a new instance of ReporteFacturasProveedorDAO */
    public ReporteFacturasProveedorDAO () {
        super ( "ReporteFacturasProveedorDAO.xml" );
    }
    public ReporteFacturasProveedorDAO (String dataBaseName) {
        super ( "ReporteFacturasProveedorDAO.xml", dataBaseName);
    }
    
    /**
     * Getter for property reporte.
     * @return Value of property reporte.
     */
    public ReporteFacturasProveedor getReporte() {
        
        return reporte;
        
    }
    
    /**
     * Setter for property reporte.
     * @param cuentas New value of property reporte.
     */
    public void setReporte( ReporteFacturasProveedor reporte ) {
        
        this.reporte = reporte;
        
    }
    
    /**
     * Getter for property vec_reporte.
     * @return Value of property vec_reporte.
     */
    public java.util.Vector getVec_reporte() {
        
        return vec_reporte;
        
    }
    
    /**
     * Setter for property vec_reporte.
     * @param vec_cuentas New value of property vec_reporte.
     */
    public void setVec_reporte( java.util.Vector vec_reporte ) {
        
        this.vec_reporte = vec_reporte;
        
    }
   
    /**
     * Metodo:          listaFacturasProveedor
     * Descripcion :    M�todo que permite listar la informaci�n del reporte.
     * @autor :         LREALES
     * @param:          la fecha inicial, la fecha final y el usuario.
     * @return:         -
     * @throws:         SQLException
     * @version :       1.0
     */
    public void listaFacturasProveedor ( String fecha_inicial, String fecha_final, String usuario ) throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_LISTA";
        
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString ( 1, fecha_inicial );
            st.setString ( 2, fecha_final );
            st.setString ( 3, "%" + usuario + "%" );            
            rs= st.executeQuery ();            
            vec_reporte = new Vector ();            
            while( rs.next () ){
                
                reporte = new ReporteFacturasProveedor ();
                // seteo
                reporte.setFecha_documento( ( rs.getString("fecha_documento") != null )?rs.getString("fecha_documento"):"" );
                reporte.setProveedor( ( rs.getString("proveedor") != null )?rs.getString("proveedor"):"" );
                reporte.setNom_proveedor( ( rs.getString("nom_proveedor") != null )?rs.getString("nom_proveedor"):"" );
                reporte.setGran_contribuyente( ( rs.getString("gran_contribuyente") != null )?rs.getString("gran_contribuyente"):"" );
                reporte.setAutoret_rfte( ( rs.getString("autoret_rfte") != null )?rs.getString("autoret_rfte"):"" );
                reporte.setAutoret_iva( ( rs.getString("autoret_iva") != null )?rs.getString("autoret_iva"):"" );
                reporte.setAutoret_ica( ( rs.getString("autoret_ica") != null )?rs.getString("autoret_ica"):"" );
                reporte.setClasificacion( ( rs.getString("clasificacion") != null )?rs.getString("clasificacion"):"" );
                reporte.setDocumento( ( rs.getString("documento") != null )?rs.getString("documento"):"" );
                reporte.setItem( ( rs.getString("item") != null )?rs.getString("item"):"" );
                reporte.setCodigo_cuenta( ( rs.getString("codigo_cuenta") != null )?rs.getString("codigo_cuenta"):"" );
                reporte.setVlr( ( rs.getString("vlr") != null )?rs.getString("vlr"):"" );
                reporte.setCod_impuesto( ( rs.getString("cod_impuesto") != null )?rs.getString("cod_impuesto"):"" );
                reporte.setPorcent_impuesto( ( rs.getString("porcent_impuesto") != null )?rs.getString("porcent_impuesto"):"" );
                reporte.setVlr_total_impuesto( ( rs.getString("vlr_total_impuesto") != null )?rs.getString("vlr_total_impuesto"):"" );
                reporte.setCreation_user( ( rs.getString("creation_user") != null )?rs.getString("creation_user"):"" );
                
                vec_reporte.add ( reporte );
                
                
            }            
            
        } catch( SQLException e ){
            
            e.printStackTrace();
            throw new SQLException( "ERROR DURANTE 'listaFacturasProveedor' - [ReporteFacturasProveedorDAO].. " + e.getMessage() + " " + e.getErrorCode() );
            
        }finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        
    }
    
}