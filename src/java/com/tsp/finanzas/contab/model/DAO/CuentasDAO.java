/********************************************************************
 *  Nombre Clase.................   CuentasDAO.java
 *  Descripcion..................   DAO de la tabla Cuentas
 *  Autor........................   Ing. Leonardo Parody Ponce
 *  Fecha........................   26.12.2005
 *  Version......................   1.0
 *  Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/


package com.tsp.finanzas.contab.model.DAO;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.util.Date;
import com.tsp.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.MainDAO;
import java.text.*;
import com.tsp.finanzas.contab.model.beans.*;

/**
 *
 * @author  EQUIPO12
 */
public class CuentasDAO extends MainDAO{

    /** Creates a new instance of CuentasDAO */
    public CuentasDAO() {
        super("CuentasDAO.xml");
    }
    public CuentasDAO(String dataBaseName) {
        super("CuentasDAO.xml", dataBaseName);
    }

    private Cuentas cuentas;

    private TreeMap cuenta = new TreeMap();

    private final String INSERT_CUENTA = "INSERT INTO con.cuentas ( cuenta, nombre_corto, nombre_largo, nombre_observacion, fin_periodo, auxiliar,"+
    "activa, modulo1, modulo2, modulo3, modulo4, modulo5, modulo6, modulo7, modulo8, modulo9, modulo10, "+
    "last_update, user_update, creation_date, creation_user, reg_status, dstrct, base) "+
    "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,now(),?,now(),?,?,?,?)";;

    private final String CONSULT_CUENTA = "SELECT * FROM con.cuentas WHERE cuenta = ?";

    private final String UPDATE_CUENTA = "UPDATE con.cuentas SET nombre_corto = ?, nombre_largo = ?, nombre_observacion = ?,"+
    " fin_periodo = ?, auxiliar = ?, activa = ?, modulo1 = ?, modulo2 = ?, modulo3 = ?, modulo4 = ?, modulo5 = ?,"+
    " modulo6 = ?, modulo7 = ?, modulo8 = ?, modulo9 = ?, modulo10 = ?, user_update = ?, last_update = now(), reg_status = ? WHERE cuenta = ?";
    private final String ANULAR_CUENTA = "UPDATE con.cuentas SET reg_status = 'A' WHERE cuenta = ?";

    private final String LIST_CUENTA = "SELECT * FROM con.cuentas WHERE cuenta LIKE ? ";

    private final String SQL_BUSCAR_CUENTAS = "SELECT cuenta, nombre_largo FROM con.cuentas";
    /**
     * Metodo InsertarCuenta , Metodo que Ingresa una Cuenta.
     * @autor : Ing. Leonardo Parody
     * @throws : SQLException
     * @param : Cuentas cuenta
     * @version : 1.0
     */
    public void InsertarCuenta(Cuentas cuenta) throws SQLException {
        //System.out.println("ESTOY EN EL DAO INSERTAR CUENTAS");
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean sw = false;
        String query = "INSERT_CUENTA" ;
        try{

            con = this.conectarJNDI(query);
            if (con != null){
                //System.out.println("VOY A EJECUTAR EL QUERY");
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setString(1, cuenta.getCuenta());
                st.setString(2, cuenta.getNombre_corto());
                st.setString(3, cuenta.getNombre_largo());
                st.setString(4, cuenta.getObservacion());
                st.setString(5, cuenta.getFin_de_periodo());
                st.setString(6, cuenta.getAuxiliar());
                st.setString(7, cuenta.getActiva());
                st.setString(8, cuenta.getModulo1());
                st.setString(9, cuenta.getModulo2());
                st.setString(10, cuenta.getModulo3());
                st.setString(11, cuenta.getModulo4());
                st.setString(12, cuenta.getModulo5());
                st.setString(13, cuenta.getModulo6());
                st.setString(14, cuenta.getModulo7());
                st.setString(15, cuenta.getModulo8());
                st.setString(16, cuenta.getModulo9());
                st.setString(17, cuenta.getModulo10());
                st.setString(18, cuenta.getUser_update());
                st.setString(19, cuenta.getCreation_user());
                st.setString(20, "");
                st.setString(21, cuenta.getDstrct());
                st.setString(22, cuenta.getBase());
                //System.out.println("Query  "+st);
                st.executeUpdate();
                //System.out.println("YA EJECUTE EL QUERY");
            }

        }catch(SQLException e){
            throw new SQLException("ERROR AL INSERTAR REGISTRO EN CUENTAS" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    e.printStackTrace();
                }
            }
            if ( con != null){
                this.desconectar(con);
            }
        }
    }

    /**
     * Metodo ConsultarCuenta , Metodo que Consulta una cuenta
     * @autor : Ing. Leonardo Parody
     * @throws : SQLException
     * @param : String cuenta
     * @version : 1.0
     */
    public void ConsultarCuenta(String cuenta) throws SQLException {
        //System.out.println("ESTOY EN EL DAO CONSULTAR CUENTAS");
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean sw = false;
        String query = "CONSULT_CUENTA";
        try{

            con = this.conectarJNDI(query);
            if (con != null){
                //System.out.println("VOY A EJECUTAR EL QUERY");
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setString(1,  cuenta);
                //System.out.println("st = "+st);
                rs = st.executeQuery();
                if (rs.next()){
                    this.cuentas = cuentas.Load(rs);
                }
                //System.out.println("Query  "+st);
                //System.out.println("YA EJECUTE EL QUERY");
            }

        }catch(SQLException e){
            throw new SQLException("ERROR AL BUSCAR REGISTRO EN CUENTAS" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                this.desconectar(con);
            }
        }
    }

    /**
     * Metodo ModificarCuenta , Metodo que Modifica una Cuenta.
     * @autor : Ing. Leonardo Parody
     * @throws : SQLException
     * @param : Cuentas cuenta
     * @version : 1.0
     */
    public void ModificarCuenta(Cuentas cuenta) throws SQLException {
        //System.out.println("ESTOY EN EL DAO MODIFICAR CUENTAS");
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean sw = false;
        String query = "UPDATE_CUENTA";
        try{

            con = this.conectarJNDI(query);
            if (con != null){
                //System.out.println("VOY A EJECUTAR EL QUERY");
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setString(1, cuenta.getNombre_corto());
                st.setString(2, cuenta.getNombre_largo());
                st.setString(3, cuenta.getObservacion());
                st.setString(4, cuenta.getFin_de_periodo());
                st.setString(5, cuenta.getAuxiliar());
                st.setString(6, cuenta.getActiva());
                st.setString(7, cuenta.getModulo1());
                st.setString(8, cuenta.getModulo2());
                st.setString(9, cuenta.getModulo3());
                st.setString(10, cuenta.getModulo4());
                st.setString(11, cuenta.getModulo5());
                st.setString(12, cuenta.getModulo6());
                st.setString(13, cuenta.getModulo7());
                st.setString(14, cuenta.getModulo8());
                st.setString(15, cuenta.getModulo9());
                st.setString(16, cuenta.getModulo10());
                st.setString(17, cuenta.getUser_update());
                st.setString(18, "");
                st.setString(19, cuenta.getCuenta());
                //System.out.println("Query  "+st);
                st.executeUpdate();
                //System.out.println("YA EJECUTE EL UPDATE");
            }

        }catch(SQLException e){
            throw new SQLException("ERROR AL MODIFICAR REGISTRO EN CUENTAS" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
                this.desconectar(con);
            }
        }
    }
    /**
     * Metodo AnularCuenta , Metodo que Anula una Cuenta.
     * @autor : Ing. Leonardo Parody
     * @throws : SQLException
     * @param : String cuenta
     * @version : 1.0
     */

    public void AnularCuenta(String cuenta) throws SQLException {
        //System.out.println("ESTOY EN EL DAO ANULAR CUENTAS");
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean sw = false;
        String query = "ANULAR_CUENTA";
        try{

             con = this.conectarJNDI(query);
            if (con != null){
                //System.out.println("VOY A EJECUTAR EL QUERY");
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setString(1, cuenta);
                st.executeUpdate();
                //System.out.println("YA EJECUTE EL UPDATE");
            }

        }catch(SQLException e){
            throw new SQLException("ERROR AL ANULAR REGISTRO EN CUENTAS" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            if ( con != null){
             this.desconectar(con);
            }
        }
    }
    /**
     * Getter for property cuentas.
     * @return Value of property cuentas.
     */
    public Cuentas getCuenta(){
        return this.cuentas;
    }
    /**
     * Metodo ExisteCuenta , Metodo que Consulta una cuenta
     * @autor : Ing. Leonardo Parody
     * @throws : SQLException
     * @param : String cuenta
     * @version : 1.0
     */
    public boolean ExisteCuenta(String cuenta) throws SQLException {
        //System.out.println("ESTOY EN EL DAO CONSULTAR CUENTAS");
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean sw = false;
        String query = "CONSULT_CUENTA";
        try{

             con = this.conectarJNDI(query);
            if (con != null){
                //System.out.println("VOY A EJECUTAR EL QUERY");
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setString(1,  cuenta);
                //System.out.println("st = "+st);
                rs = st.executeQuery();
                if (rs.next()){
                    sw = true;
                }
                //System.out.println("YA EJECUTE EL QUERY");
            }
            return(sw);

        }catch(SQLException e){
            throw new SQLException("ERROR AL BUSCAR REGISTRO EN CUENTAS" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
    }
    /**
     * Metodo ListarCuentas , Metodo que Consulta una cuenta
     * @autor : Ing. Leonardo Parody
     * @throws : SQLException
     * @version : 1.0
     */
    public List ListarCuentas(String iniciocuenta) throws SQLException {
        //System.out.println("ESTOY EN EL DAO CONSULTAR CUENTAS");
        Connection con= null;
        PreparedStatement st = null;
        ResultSet rs = null;
        LinkedList Cuentas = new LinkedList();
        boolean sw = false;
        String query = "LIST_CUENTA" ;
        try{

            con = this.conectarJNDI(query);
            if (con != null){
                //System.out.println("VOY A EJECUTAR EL QUERY");
                st = con.prepareStatement(this.obtenerSQL(query));
                String inicio = iniciocuenta + "%";
                st.setString(1,inicio);
                //System.out.println("st = "+st);
                rs = st.executeQuery();
                Cuentas cuenta = new Cuentas();
                while (rs.next()){
                    cuentas = cuentas.Load(rs);
                    //System.out.println("cuenta Nro = "+cuentas.getCuenta());
                    if (!cuentas.getReg_status().equalsIgnoreCase("A")) {
                        Cuentas.add(cuentas);
                    }
                }
                //System.out.println("YA EJECUTE EL QUERY");
            }
            return(Cuentas);

        }catch(SQLException e){
            throw new SQLException("ERROR AL BUSCAR REGISTROS EN CUENTAS" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
    }
    /**
     * Metodo que busca una lista de cuenta y nombre de las cuentas en la tabla cuenta
     * @autor : Leonardo Parody
     * @throws : SQLException
     * @version : 1.0.
     **/
    public void buscarListaCodigo()throws SQLException{

        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        cuenta = new TreeMap();
        String query = "SQL_BUSCAR_CUENTAS";
        try {
            con = this.conectarJNDI(query);
            if(con!=null){
                st = con.prepareStatement(this.obtenerSQL(query));
                rs = st.executeQuery();
                //cuenta.put(" Seleccione un Item","NADA");
                while(rs.next()){
                    String nombre = rs.getString("nombre_largo");
                    String cuent =  rs.getString("cuenta");
                    //System.out.println("nombre = "+nombre+"  cuenta = "+cuent);
                    cuenta.put(cuent+" - "+nombre , cuent);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LAS CUENTAS " + e.getMessage() + " " + e.getErrorCode());
        }finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
    }
    /**
     * Metodo que obtiene el atributo cuenta
     * @autor.......Leonardo Parody
     * @throws......SQLException
     * @version.....1.0.
     **/
    public java.util.TreeMap getCuentaTree() {
        return cuenta;
    }
     /**
     * Metodo ConsultarCuenta , Metodo que Consulta una cuenta
     * @autor : Ing. Ronald Uribe
     * @throws : SQLException
     * @param : String cuenta
     * @version : 1.0
     */
    public Vector listarCuentaAuxiliar() throws SQLException {
        PreparedStatement st = null;
        ResultSet rs = null;
        String sql = "";
        Vector detalles = new Vector();
        Connection con = null;
        String query = "LISTA_CUENTA_AUXILIAR";
        int sw=0;
        try{

            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            rs = st.executeQuery();
            while (rs.next()){
                Cuentas detalle = new Cuentas();
                detalle.setCuenta            ( rs.getString("cuenta") );
                detalles.add(detalle);
            }
        }catch(Exception ex){
            ex.printStackTrace();
            throw new SQLException( "Error en MayorizacionDAO.SQL_UPDATE_APLICACION_AUXILIARES......." + ex.getMessage() );
        }
        finally{
            if (st != null) {try { st.close(); } catch (SQLException sqle) {}st = null;}
            if (rs != null) {try { rs.close(); } catch (SQLException sqle) {}rs = null;}
            if (con != null) {try { this.desconectar(con); } catch (SQLException sqle) {}}
        }
        return detalles;
    }
     /**
     * Metodo ConsultarCuenta , Metodo que Consulta una cuenta con subledger = SI
     * @autor : Ing. Ronald Uribe
     * @throws : SQLException
     * @param : String cuenta
     * @version : 1.0
     */
    public Vector listarCuentaSubledger() throws SQLException {
        PreparedStatement st = null;
        ResultSet rs = null;
        String sql = "";
        Vector detalles = new Vector();
        Connection con = null;
        String query = "LISTA_CUENTA_SUBLEDGER";
        int sw=0;
        try{
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
           // System.out.println("SQL-->>"+sql);
            rs = st.executeQuery();
            while (rs.next()){
                Cuentas detalle = new Cuentas();
                detalle.setCuenta            ( rs.getString("cuenta") );
                detalles.add(detalle);
            }
        }catch(Exception ex){
            ex.printStackTrace();
            throw new SQLException( "Error en MayorizacionDAO.listarCuentaSubledger......" + ex.getMessage() );
        }
        finally{
            if (st != null) {try { st.close(); } catch (SQLException sqle) {}st = null;}
            if (rs != null) {try { rs.close(); } catch (SQLException sqle) {}rs = null;}
            if (con != null) {try { this.desconectar(con); } catch (SQLException sqle) {}}
        }
        return detalles;
    }

}
