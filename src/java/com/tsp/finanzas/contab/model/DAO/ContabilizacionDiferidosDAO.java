/***************************************
 * Nombre Clase ............. ContabilizacionFacturasDAO.java
 * Descripci�n  .. . . . . .  Permitimos realizar consultas para la contabilizacion de facturas
 * Autor  . . . . . . . . . . ROBERTO ROCHA P
 * Fecha . . . . . . . . . .  13/06/2006
 * versi�n . . . . . . . . .  1.0
 * Copyright ...............  FINTRAVALORES S.A.
 *******************************************/

package com.tsp.finanzas.contab.model.DAO;


import java.io.*;
import java.util.*;
import java.sql.*;
import com.tsp.finanzas.contab.model.beans.*;
import com.tsp.util.*;
import com.tsp.operation.model.DAOS.MainDAO;
import com.tsp.operation.model.beans.StringStatement;
import com.tsp.operation.model.beans.*;



public class ContabilizacionDiferidosDAO extends MainDAO{



    private String APROBADOR        = "ADMIN";
    private String PRE_COMPROBANTE  = "CONTABILIZACION";
    private String PRE_DETALLE      = "ITEM";
    private String PRE_DETALLE_IMP  = "IMPUESTO";
    private String AUX_COMPROBANTE  = "";
    private String OF_PPAL          = "OP";

    public  String CABECERA         = "C";
    public  String DETALLE          = "D";
    public  String SALDO            = "S";


    private String TIPO_FACTURA_AMD  = "FAA";

    private String TIPO_CONTAB_OP    = "COP";   // FACTURAS OPS
    private String TIPO_CONTAB_ADM   = "CFA";   // FACTURAS ADMIN
    private String TIPO_CONTAB_NT    = "CNT";   // FACTURAS ND ND



    private final String FACTURA         = "010";
    private final String NC              = "035";
    private final String ND              = "036";



    public  int   CANTIDAD_IMP_ITEM      =  0;






    public ContabilizacionDiferidosDAO() {
        super("ContabilizacionDiferidosDAO.xml");
    }
    public ContabilizacionDiferidosDAO(String dataBaseName) {
        super("ContabilizacionDiferidosDAO.xml",dataBaseName);
    }



    /**
     * M�todos que setea valores nulos
     * @autor.......fvillacob
     * @version.....1.0.
     **/
    private String reset(String val){
        if(val==null)
            val = "";
        return val;
    }




    /**
     * M�todo que busca si la cta requiere auxiliar
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public  String requiereAUX(String distrito, String cta) throws Exception{
        Connection con = null;
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        String             query   = "SQL_REQUIERE_AUX";
        String             aux     = "";
        try{
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, distrito  );
            st.setString(2, cta       );
            rs = st.executeQuery();
            if( rs.next() )
                cta  =  reset( rs.getString("auxiliar")  );

        }catch(Exception e){
            throw new Exception( "requiereAUX " + e.getMessage());
        }
finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return cta;
    }





    /**
     * M�todo que busca si la cuenta tiene dicho subledger asociado
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public  boolean existeCuentaSubledger(String distrito, String cta, String auxiliar) throws Exception{
        Connection con = null;
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        String             query   = "SQL_CUENTA_SUBLEDGER";
        boolean            estado  = false;
        try{
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, distrito  );
            st.setString(2, cta       );
            st.setString(3, auxiliar  );
            rs = st.executeQuery();
            if( rs.next() )
                estado = true;

        }catch(Exception e){
            throw new Exception( "existeCuentaSubledger " + e.getMessage());
        }
finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return estado;
    }




    /**
     * M�todo que busca si la cuenta tiene dicho subledger asociado
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public  String getAccoundCodeC(String oc) throws Exception{
        Connection con = null;
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        String             query   = "SQL_ACCOUNT_CODE_C";
        String             cta     = "";
        try{
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, oc  );
            rs = st.executeQuery();
            if( rs.next() )
                cta  =  reset( rs.getString("account_code_c")  );

        }catch(Exception e){
            throw new Exception( "getAccoundCodeC " + e.getMessage());
        }
finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return cta;
    }




     /**
     * M�todo que busca la cuenta contable diferencia en cambio a planilla
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public  String getCuenta_DifCambio(String distrito, String concepto) throws Exception{
        Connection con = null;
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        String             query   = "SQL_CUENTA_DIFERENCIA_CAMBIO";
        String             cuenta  = "";
        try{
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, distrito  );
            st.setString(2, concepto  );
            rs = st.executeQuery();
            if(rs.next())
                cuenta = reset( rs.getString("account")  );

        }catch(Exception e){
            throw new Exception( "getCuenta_DifCambio " + e.getMessage());
        }
finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return cuenta;
    }




    // MONEDA : -------------------------------------------------------------------------

    /**
     * M�todo que busca la moneda local
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public  String getMoneda(String distrito) throws Exception{
        Connection con = null;
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        String             query   = "SQL_MONEDA_LOCAL";
        String             moneda  = "";
        try{
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, distrito  );
            rs = st.executeQuery();
            if(rs.next())
                moneda = reset( rs.getString("moneda")  );

        }catch(Exception e){
            throw new Exception( "getMoneda " + e.getMessage());
        }
finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return moneda;
    }









    // COMPROBANTE : ------------------------------------------------------------------

    /**
     * M�todo que busca las facturas que no esten contabilizadas
     * @param ayer
     * @return 
     * @throws java.lang.Exception
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public  List getDiferidosNoContabilizados(String ayer) throws Exception{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        List lista = new LinkedList();
        String query = "SQL_DIFERIDOS_NO_CONTABILIZADOS";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            // st.setString(1, ayer);
            rs = st.executeQuery();
            while (rs.next()) {
                Comprobantes comprobante = loadComprobantes(rs);
                lista.add(comprobante);
            }

        } catch (Exception e) {
            throw new Exception("getNegociosNoContabilizados " + e.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st = null;
                } catch (Exception e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return lista;
    }






    /**
     * M�todo que carga los datos de los negocios
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public  Comprobantes loadComprobantes(ResultSet  rs)throws Exception{
        Comprobantes  comp  = new  Comprobantes();
        try{
           comp.setAbc      ("N/A");
           comp.setBase     (rs.getString("base"));
           comp.setDstrct   (rs.getString("dstrct"));
           comp.setTipodoc  (rs.getString("tipo_doc"));
           comp.setNumdoc   (rs.getString("numdoc"));
           comp.setTercero  (rs.getString("tercero"));
           comp.setPeriodo  (rs.getString("periodo"));
           comp.setCuenta   (rs.getString("cuenta"));
           comp.setTotal_debito(Double.valueOf(rs.getString("valor")).doubleValue());
           comp.setTotal_credito(0.0);
           comp.setMoneda   ("PES");
           comp.setDetalle  ("Intereses de  "+rs.getString("periodo"));
           comp.setRef_1    (rs.getString("descrip"));
           comp.setCmc(rs.getString("hc"));
           comp.setAuxiliar(rs.getString("cuenta_diferidos"));
           comp.setTdoc_rel(reset(rs.getString("tipodocRel")));
           comp.setNumdoc_rel(reset(rs.getString("documento_rel")));
           comp.setAplica_iva(rs.getString("aplica_iva"));
           comp.setId_convenio(rs.getInt("id_convenio"));
        }catch(Exception e){
            throw new Exception( "loadComprobantes " + e.getMessage());
        }
        return comp;
    }


    /**
     * M�todo que busca la cuenta asociada al cmc y tipo doc
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public  Hashtable  getCuenta(String distrito, String tipoDoc, String cmc )throws Exception{
        Connection con = null;
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        String             query   = "SQL_CUENTA_CMC";
        Hashtable          info    = null;
        try{
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, distrito    );
            st.setString(2, tipoDoc     );
            st.setString(3, cmc         );
            rs = st.executeQuery();
            if( rs.next() ){
                info    =  new Hashtable();
                info.put("cuenta",      reset( rs.getString("cuenta")      ) );
                info.put("dbcr",        reset( rs.getString("dbcr")        ) );
                info.put("tipo_cuenta", reset( rs.getString("tipo_cuenta") ) );
            }

        }catch(Exception e){
            throw new Exception( " DAO: getCuenta " + e.getMessage());
        }finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return info;
    }







    // INSERT  UPDATE : ----------------------------------------------------------------------------------







    /**
     * M�todo que inserta el comprobante
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public  void  deleteComprobante(ComprobanteFacturas  comprobante)throws Exception{
        Connection con = null;
        PreparedStatement  st      = null;
        String             query   = "SQL_DELETE_COMPROBANTE";
        try{
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                 st.setString(1,   comprobante.getDstrct()   );
                 st.setString(2,   comprobante.getTipodoc()  );
                 st.setString(3,   comprobante.getNumdoc()   );
                 st.setInt   (4,   comprobante.getGrupo_transaccion() );

            st.execute();
        }catch(Exception e){
            throw new Exception( "deleteComprobante " + e.getMessage());
        }finally {
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
    }





    /**
     * M�todo que inserta el comprobante
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public  String InsertComprobante(Comprobantes  comprobante, String user)throws Exception{//20100524
        Connection con = null;
        StringStatement  st      = null;
        String             query   = "SQL_INSERT_COMPROBANTE";
        String             sql     = "";
        try{
            st = new StringStatement(this.obtenerSQL(query), true);//JJCastro fase2
            st.setString(1,   comprobante.getDstrct()         );
            st.setString(2,   comprobante.getTipodoc()        );
            st.setString(3,   comprobante.getNumdoc()         );
            st.setString(4,   this.OF_PPAL                    );  //  comprobante.getSucursal()
            st.setString(5,   comprobante.getPeriodo()        );
            st.setString(6,   comprobante.getFechadoc()       );
            st.setString(7,   comprobante.getDetalle()        );
            st.setString(8,   comprobante.getTercero()        );
            st.setDouble(9,   comprobante.getTotal_debito()   );
            st.setDouble(10,  comprobante.getTotal_credito()  );
            st.setInt   (11,  comprobante.getTotal_items()    );
            st.setString(12,  comprobante.getMoneda()         );
            st.setString(13,  comprobante.getAprobador()      );
            st.setString(14,  user                            );
            st.setString(15,  comprobante.getBase()           );
            st.setString(16,  comprobante.getTipo_operacion() );
            st.setInt   (17,  comprobante.getGrupo_transaccion()    );

            sql = st.getSql();

        }catch(Exception e){
            throw new Exception( "InsertComprobante " + e.getMessage());
        }finally {
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return sql;
    }




    /**
     * M�todo que inserta el comprobante detalle
     *
     * @param lista
     * @param sec
     * @param user
     * @return 
     * @throws java.lang.Exception 
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     *
     */
    public ArrayList<String> InsertComprobanteDetalle(List lista, int sec, String user) throws Exception {//20100524
        Connection con = null;
        StringStatement st = null;
        String query = "SQL_INSERT_COMPRODET";
        ArrayList<String> sql = new ArrayList<>();
        String consulta = this.obtenerSQL(query);
        try {

            for (int i = 0; i < lista.size(); i++) {
                st = new StringStatement(consulta, true);//JJCastro fase2
                Comprobantes comprodet = (Comprobantes) lista.get(i);
                st.setString(1, comprodet.getDstrct());
                st.setString(2, comprodet.getTipodoc());
                st.setString(3, comprodet.getNumdoc());
                st.setInt(4, sec);
                st.setString(5, comprodet.getPeriodo());
                st.setString(6, comprodet.getCuenta());
                st.setString(7, comprodet.getAuxiliar());
                st.setString(8, comprodet.getAbc());
                st.setString(9, comprodet.getDetalle());
                st.setDouble(10, comprodet.getTotal_debito());
                st.setDouble(11, comprodet.getTotal_credito());
                st.setString(12, comprodet.getTercero());
                st.setString(13, user);
                st.setString(14, comprodet.getBase());
                st.setString(15, comprodet.getDocumento_interno());
                st.setString(16, comprodet.getTdoc_rel());
                st.setString(17, comprodet.getNumdoc_rel());

                sql.add(st.getSql());//JJCastro fase2
                st = null;

            }

        } catch (Exception e) {
            throw new Exception("InsertComprobanteDetalle " + e.getMessage());
        } finally {
            if (st != null) {
                try {
                    st = null;
                } catch (Exception e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return sql;
    }




    /**
     * M�todo que inserta el comprobante
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public  String updateFactura(ComprobanteFacturas  comprobante, String user)throws Exception{
        Connection con = null;
        PreparedStatement  st      = null;
        String             query   = "SQL_UPDATE_FACTURAS_DATOS_CONTABLES";
        String             sql     = "";
        try{
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
          //set:
            st.setString(1, user );
          //  st.setString(2, comprobante.getFechadoc()  );
            st.setString(2, comprobante.getPeriodo()   );
            st.setString(3, String.valueOf(comprobante.getGrupo_transaccion())   );
          //where:
            st.setString(4, comprobante.getDstrct()          );
            st.setString(5, comprobante.getTercero()         );
            st.setString(6, comprobante.getTipodoc_factura() );
            st.setString(7, comprobante.getNumdoc()          );

            sql = st.toString();
        }catch(Exception e){
            throw new Exception( "updateFactura " + e.getMessage());
        }finally {
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return sql;
    }



    /**
     * M�todo que actualiza los moviminetos tipo Q
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public  String updateMovplaTipoQ(ComprobanteFacturas  comprobante, String user)throws Exception{
        Connection con = null;
        PreparedStatement  st      = null;
        String             query   = "SQL_UPDATE_MOVPLA_TIPO_Q";
        String             sql     = "";
        try{

            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
          //set:
            st.setString(1, String.valueOf(comprobante.getGrupo_transaccion())   );
         //   st.setString(2, comprobante.getFechadoc()  );
            st.setString(2, comprobante.getPeriodo()   );

          //where:
            st.setString(3, comprobante.getOc()        );
            st.setString(4, comprobante.getDstrct()    );
            st.setString(5, comprobante.getNumdoc()    );


            sql = st.toString();

        }catch(Exception e){
            throw new Exception( "updateMovplaTipoQ " + e.getMessage());
        }finally {
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return sql;
    }

    /*Metodos Reversi�n Descontabiolizacion*/
    /**
     * M�todo que Vector Comprobantes
     * @autor.......jescandon
     * @throws......Exception
     * @version.....1.0.
     **/
    public  Vector VectorComprobantes( Comprobantes  comprobante )throws Exception{
        Connection con = null;
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        String             query   = "SQL_COMPROBANTESDET";
        Vector             Vcomprobantes = new Vector();
        try{
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, comprobante.getTipodoc());
            st.setString(2, comprobante.getNumdoc());
            st.setInt(3, comprobante.getGrupo_transaccion());
            rs = st.executeQuery();
            while(rs.next()){
                Comprobantes aux = new Comprobantes();
                aux.setTipodoc(rs.getString("tipodoc"));
                aux.setNumdoc(rs.getString("numdoc"));
                aux.setGrupo_transaccion(rs.getInt("grupo_transaccion"));
                aux.setAuxiliar(rs.getString("auxiliar"));
                aux.setCuenta(rs.getString("cuenta"));
                aux.setTotal_debito(rs.getDouble("valor_debito"));
                aux.setTotal_credito(rs.getDouble("valor_credito"));
                aux.setDstrct(rs.getString("dstrct"));
                aux.setPeriodo(rs.getString("periodo"));
                Vcomprobantes.add(aux);
                aux = null; //Liberar Espacio JJCastro
            }
        }catch(Exception e){
            throw new Exception( "Descontabilizar " + e.getMessage());
        }finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return Vcomprobantes;
    }


    /**
     * M�todo que getComprobantes
     * @autor.......jescandon
     * @throws......Exception
     * @version.....1.0.
     **/
    public  ComprobanteFacturas getComprobantes( Comprobantes  comprobante )throws Exception{
        Connection con = null;
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        ComprobanteFacturas       aux     = null;
        String             query   = "SQL_COMPROBANTES";
        try{
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, comprobante.getTipodoc());
            st.setString(2, comprobante.getNumdoc());
            st.setInt(3, comprobante.getGrupo_transaccion());
            rs = st.executeQuery();
            while(rs.next()){
                aux = new ComprobanteFacturas();
                aux.setDstrct(rs.getString("dstrct"));
                aux.setTipodoc(rs.getString("tipodoc"));
                aux.setNumdoc(rs.getString("numdoc"));
                aux.setGrupo_transaccion(rs.getInt("grupo_transaccion"));
                aux.setSucursal(rs.getString("sucursal"));
                aux.setPeriodo(rs.getString("periodo"));
                aux.setFechadoc(rs.getString("fechadoc"));
                aux.setDetalle(rs.getString("detalle"));
                aux.setTercero(rs.getString("tercero"));
                aux.setTotal_debito(rs.getDouble("total_credito"));
                aux.setTotal_credito(rs.getDouble("total_debito"));
                aux.setTotal_items(rs.getInt("total_items"));
                aux.setMoneda(rs.getString("moneda"));
                aux.setAprobador(rs.getString("aprobador"));
                aux.setUsuario(rs.getString("creation_user"));
                aux.setBase(rs.getString("base"));
                aux.setUsuario_aplicacion(rs.getString("usuario_aplicacion"));
            }


        }catch(Exception e){
            throw new Exception( "Descontabilizar " + e.getMessage());
        } finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return aux;
    }


     /**
     * M�todo que Vector Comprobantes
     * @autor.......jescandon
     * @throws......Exception
     * @version.....1.0.
     **/

    public  List ListaComprobantes( Comprobantes  comprobante )throws Exception{
        Connection con = null;
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        String             query   = "SQL_COMPROBANTESDET";
        List               Vcomprobantes = new LinkedList();
        try{
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, comprobante.getTipodoc());
            st.setString(2, comprobante.getNumdoc());
            st.setInt(3, comprobante.getGrupo_transaccion());
            rs = st.executeQuery();
            while(rs.next()){
                ComprobanteFacturas aux = new ComprobanteFacturas();
                aux.setDstrct(rs.getString("dstrct"));
                aux.setTipodoc(rs.getString("tipodoc"));
                aux.setNumdoc(rs.getString("numdoc"));
                aux.setGrupo_transaccion(rs.getInt("grupo_transaccion"));
                aux.setAuxiliar(rs.getString("auxiliar"));
                aux.setCuenta(rs.getString("cuenta"));
                aux.setTotal_debito(rs.getDouble("valor_credito"));
                aux.setTotal_credito(rs.getDouble("valor_debito"));
                aux.setPeriodo(rs.getString("periodo"));
                aux.setDetalle(rs.getString("detalle"));
                aux.setTercero(rs.getString("tercero"));
                aux.setBase(rs.getString("base"));
                aux.setDocumento_interno(rs.getString("documento_interno"));
                Vcomprobantes.add(aux);
                aux = null; //Liberar Espacio JJCastro
            }


        }catch(Exception e){
            throw new Exception( "Descontabilizar " + e.getMessage());
        } finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return Vcomprobantes;
    }

    /**
     * M�todo que borra una lista de comprobantes
     * @autor.......jescandon
     * @throws......Exception
     * @version.....1.0.
     **/
    public  List deleteComprobanteSQL(List lista)throws Exception{
        Connection con = null;
        PreparedStatement  st      = null;
        String             query   = "SQL_DELETE_COMPROBANTE";
        try{
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            for(int i=0;i<lista.size();i++){
                ComprobanteFacturas  comprobante  = (ComprobanteFacturas) lista.get(i);
                    st.setString(1,   comprobante.getDstrct()   );
                    st.setString(2,   comprobante.getTipodoc()  );
                    st.setString(3,   comprobante.getNumdoc()   );
                    st.setInt   (4,   comprobante.getGrupo_transaccion() );
                st.execute();
                st.clearParameters();

            }


        }catch(Exception e){
            throw new Exception( "deleteComprobante " + e.getMessage());
        } finally {
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return lista;
    }

         /**
     * M�todo que DescontabilizarMayor
     * @autor.......jescandon
     * @throws......Exception
     * @version.....1.0.
     **/
public  String DescontabilizarMayor( Comprobantes  comprobante )throws Exception{
        StringStatement  st      = null;
        ResultSet          rs      = null;
        String             query   = "SQL_DESCONTABILIZARMAYOR";
        String             sql     = "";
        try{
            String mes = comprobante.getPeriodo().substring(4,6);
            sql = this.obtenerSQL(query).replaceAll("#MES#", mes);

            st = new StringStatement(sql);

            st.setDouble( comprobante.getTotal_debito());
            st.setDouble( comprobante.getTotal_credito());

            st.setDouble( comprobante.getTotal_debito());
            st.setDouble( comprobante.getTotal_credito());


            st.setString( comprobante.getCuenta());
            st.setString( comprobante.getDstrct());

            String anio = comprobante.getPeriodo().substring(0,4);
            st.setString( anio);

            sql = st.getSql();

        }catch(Exception e){
            e.printStackTrace();
            throw new Exception( "Descontabilizar " + e.getMessage());
        }
        return sql;
    }


    /**
     * M�todo que DescontabilizarMayoSubledger
     * @autor.......jescandon
     * @throws......Exception
     * @version.....1.0.
     **/
    public  String DescontabilizarMayoSubledger( Comprobantes  comprobante )throws Exception{
        StringStatement  st      = null;
        ResultSet          rs      = null;
        String             query   = "SQL_DESCONTABILIZARMAYORSUBLEDGER";
        String             sql     = "";
        Connection con             = null;

        try{

            String mes = comprobante.getPeriodo().substring(4,6);
            sql = this.obtenerSQL(query).replaceAll("#MES#", mes);

            st = new StringStatement(sql);

            st.setDouble( comprobante.getTotal_debito());
            st.setDouble( comprobante.getTotal_credito());

            st.setDouble( comprobante.getTotal_debito());
            st.setDouble( comprobante.getTotal_credito());

            st.setString( comprobante.getCuenta());
            st.setString( comprobante.getDstrct());

            String anio = comprobante.getPeriodo().substring(0,4);
            st.setString( anio);

            st.setString( comprobante.getAuxiliar());

            sql = st.getSql();




        }catch(Exception e){
            e.printStackTrace();
            throw new Exception( "Descontabilizar " + e.getMessage());
        }
        return sql;
    }


     /**
     * M�todo que UpdateFechaAplicacion
     * @autor.......jescandon
     * @throws......Exception
     * @version.....1.0.
     **/
    public  String UpdateFechaAplicacion( Comprobantes  comprobante )throws Exception{
        StringStatement  st      = null;
        ResultSet          rs      = null;
        String             query   = "SQL_UPDATEFECHA";
        String             sql     = "";

        try{

            st = new StringStatement(this.obtenerSQL(query));

            st.setString( comprobante.getTipodoc());
            st.setString( comprobante.getNumdoc());
            st.setInt(    comprobante.getGrupo_transaccion());

           // st.executeUpdate();

            sql = st.getSql();

        }catch(Exception e){
            e.printStackTrace();
            throw new Exception( "Descontabilizar " + e.getMessage());
        }
        return sql;
    }

    public  String getAccountTC(String oc) throws Exception{
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        String             query   = "SQL_CTA_TBLCON";
        String             cta     = "";
        Connection con = null;
        try{
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, oc  );
            rs = st.executeQuery();
            if( rs.next() )
                cta  =  reset( rs.getString("dat")  );

        }catch(Exception e){
            throw new Exception( "getAccountMal " + e.getMessage());
        }
finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return cta;
    }

     public  String getAccountDES(String oc) throws Exception{
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        String             query   = "SQL_DESC_CTA";
        String             cta     = "";
        Connection con = null;
        try{
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, oc  );
            st.setString(2, "FINV");
            rs = st.executeQuery();
            if( rs.next() )
                cta  =  reset( rs.getString("nombre_largo")  );

        }catch(Exception e){
            throw new Exception( "getAccountBCO_Mal " + e.getMessage());
        }
finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return cta;
    }

      public  String getAccountIng(String oc) throws Exception{
          PreparedStatement st = null;
          ResultSet rs = null;
          String query = "SQL_CTA_ING";
          String cta = "";
          Connection con = null;
          try {
              con = this.conectarJNDI(query);
              st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
              st.setString(1, oc);
              rs = st.executeQuery();
              if (rs.next()) {
                  cta = reset(rs.getString("dat"));
              }

          } catch (Exception e) {
              throw new Exception("getAccountBCO_Mal " + e.getMessage());
          }
finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return cta;
    }

      public void updatecont(String ob,int cod,String est,String codn)throws SQLException{
          Connection con = null;
          PreparedStatement st = null;
          String query = "SQL_UPDATE_NEG";
          try {
              con = this.conectarJNDI(query);
              st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
              st.setString(1, ob);
              st.setInt(2, cod);//20100524
              st.setString(3, est);
              st.setString(4, codn);
              st.executeUpdate();
          } catch (SQLException e) {
              //System.out.println("Entra en el error 177");
              throw new SQLException("ERROR DURANTE LA ACTUALIZACION DE NEGOCIOS " + e.getMessage() + " " + e.getErrorCode());
          }
finally {
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        //System.out.println("El nit q le  llega ++++");
    }


}
