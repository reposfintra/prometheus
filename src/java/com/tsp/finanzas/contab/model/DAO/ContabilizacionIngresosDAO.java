/***************************************
 * Nombre Clase ............. ContabilizacionIngresosDAO.java
 * Descripci�n  .. . . . . .  Permitimos realizar consultas para la contabilizacion de facturas de ingresos
 * Autor  . . . . . . . . . . MARIO FONTALVO SOLANO
 * Fecha . . . . . . . . . .  05/02/2007
 * versi�n . . . . . . . . .  1.0
 * Copyright ...............  Transportes Sanchez Polo S.A.
 *******************************************/
package com.tsp.finanzas.contab.model.DAO;

import java.sql.*;
import java.util.*;


import com.tsp.operation.model.DAOS.MainDAO;
import com.tsp.operation.model.beans.Ingreso;
import com.tsp.operation.model.beans.Ingreso_detalle;
import com.tsp.operation.model.beans.factura;
import com.tsp.finanzas.contab.model.beans.Comprobantes;
import com.tsp.util.*;

public class ContabilizacionIngresosDAO extends MainDAO {
    
    /** Creates a new instance of ContabilizacionIngresosDAO */
    public ContabilizacionIngresosDAO() {
        super ("ContabilizacionIngresosDAO.xml");
    }
    public ContabilizacionIngresosDAO(String dataBaseName) {
        super ("ContabilizacionIngresosDAO.xml", dataBaseName);
    }
    
    
    /**
     * Metodo para obtener los ingresos pendientes por contabilizar
     * @autor mfontalvo
     * @throws Exception.
     * @return Vector, lista de ingresos a contabilizar
     */
    private Vector obtenerIngresos(String where) throws Exception {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet         rs = null;
        String            query = "SQL_OBTENER_INGRESOS";
        Vector            lista = new Vector();
        try{
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query).replaceAll("#WHERE#", where ));//JJCastro fase2
                        
            //System.out.println("Query "+ps.toString());
            rs = ps.executeQuery();
            
            while (rs.next()){
                Ingreso in = new Ingreso();
                String cuenta = rs.getString("cuenta");
                in.setReg_status         ( Util.coalesce( rs.getString("reg_status"         ), ""  ) );
                in.setDstrct             ( Util.coalesce( rs.getString("dstrct"             ), ""  ) );
                in.setTipo_documento     ( Util.coalesce( rs.getString("tipo_documento"     ), ""  ) );
                in.setNum_ingreso        ( Util.coalesce( rs.getString("num_ingreso"        ), ""  ) );
                in.setCodcli             ( Util.coalesce( rs.getString("codcli"             ), ""  ) );
                in.setNitcli             ( Util.coalesce( rs.getString("nitcli"             ), ""  ) );
                in.setConcepto           ( Util.coalesce( rs.getString("concepto"           ), ""  ) );
                in.setTipo_ingreso       ( Util.coalesce( rs.getString("tipo_ingreso"       ), ""  ) );
                in.setFecha_consignacion ( Util.coalesce( rs.getString("fecha_consignacion" ), ""  ) );
                in.setNro_consignacion   ( Util.coalesce( rs.getString("nro_consignacion"   ), ""  ) );
                in.setFecha_ingreso      ( Util.coalesce( rs.getString("fecha_ingreso"      ), ""  ) );
                in.setBranch_code        ( Util.coalesce( rs.getString("branch_code"        ), ""  ) );
                in.setBank_account_no    ( Util.coalesce( rs.getString("bank_account_no"    ), ""  ) );
                in.setCodmoneda          ( Util.coalesce( rs.getString("codmoneda"          ), ""  ) );
                in.setAgencia_ingreso    ( Util.coalesce( rs.getString("agencia_ingreso"    ), ""  ) );
                in.setDescripcion_ingreso( Util.coalesce( rs.getString("descripcion_ingreso"), ""  ) );
                in.setPeriodo            ( Util.coalesce( rs.getString("periodo"            ), ""  ) );
                
                in.setVlr_ingreso       ( rs.getDouble("vlr_ingreso"   ) );
                in.setVlr_ingreso_me    ( rs.getDouble("vlr_ingreso_me") );
                in.setVlr_tasa          ( rs.getDouble("vlr_tasa"      ) );
                in.setFecha_tasa        ( Util.coalesce( rs.getString("fecha_tasa"), ""  ) );
                
                in.setCuenta_banco    ( Util.coalesce( rs.getString("cuenta_banco"    ), ""  ) );
                //Mod Tmolina 09-08-2008
		if(cuenta.equals("16252002")){
                    in.setCuenta_cliente("16252001");
                }else{
                    in.setCuenta_cliente  ( Util.coalesce( rs.getString("cuenta_cliente"  ), ""  ) );
                }
                in.setCuenta          ( Util.coalesce( rs.getString("cuenta"  ), ""  ) );
                in.setAuxiliar        ( Util.coalesce( rs.getString("auxiliar"), ""  ) );      
                
                in.setFecha_contabilizacion( rs.getString("fecha_contabilizacion") );
                in.setPeriodo              ( rs.getString("periodo") );
                in.setTransaccion          ( rs.getInt   ("transaccion") );
                
                in.setFecha_anulacion_contabilizacion( rs.getString("fecha_anulacion_contabilizacion") );
                in.setPeriodo_anulacion              ( rs.getString("periodo_anulacion") );
                in.setTransaccion_anulacion          ( rs.getInt   ("transaccion_anulacion") );
                
                in.setNomCliente                    (rs.getString("nomb"));
                 
                lista.add(in);
                in = null; //Liberar Espacio JJCastro
            }
        } catch (Exception ex){
            ex.printStackTrace();
            throw new Exception ( ex.getMessage() );
        } finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (ps != null) {try {ps = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return lista;
    }
    
    
    
    /**
     * Metodo para obtener los detalles de un ingreso
     * @autor mfontalvo
     * @param cab, ingreso a consultar.
     * @throws Exception.
     * @return Vector, lista de detalles de un ingreso
     */
    public Vector obtenerDetalleIngreso(Ingreso cab) throws Exception {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet         rs = null;
        String            query = "SQL_OBTENER_DETALLE_INGRESOS";
        Vector            lista = new Vector();
        try{
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            ps.setString(1, cab.getDstrct()         );
            ps.setString(2, cab.getTipo_documento() );
            ps.setString(3, cab.getNum_ingreso()    );
            
            rs = ps.executeQuery();
            
            while (rs.next()){
                Ingreso_detalle in = new Ingreso_detalle();
                in.setAnulado             ( rs.getBoolean( "reg_status") ) ;
                in.setDistrito            ( Util.coalesce( rs.getString("dstrct") , "" ) );
                in.setTipo_documento      ( Util.coalesce( rs.getString("tipo_documento") , "" ) );
                in.setNumero_ingreso      ( Util.coalesce( rs.getString("num_ingreso") , "" ) );
                in.setItem                ( rs.getInt("item") );
                in.setNit_cliente         ( Util.coalesce( rs.getString("nitcli") , "" ) );
                in.setDescripcion         ( Util.coalesce( rs.getString("descripcion") , "" ) );
                in.setValor_ingreso       ( rs.getDouble("valor_ingreso") );
                in.setValor_ingreso_me    ( rs.getDouble("valor_ingreso_me") );
                in.setValor_diferencia    ( rs.getDouble("valor_diferencia_tasa") );
                
                // retefuente
                in.setCodigo_retefuente   ( Util.coalesce( rs.getString("codigo_retefuente") , "" ) );
                in.setValor_retefuente    ( rs.getDouble ("valor_retefuente") );
                in.setValor_retefuente_me ( rs.getDouble ("valor_retefuente_me") );
                in.setAccount_code_rfte   ( Util.coalesce( rs.getString("account_rfte") , "" ) );
                
                // rica
                in.setCodigo_reteica      ( Util.coalesce( rs.getString("codigo_reteica") , "" ) );
                in.setValor_reteica       ( rs.getDouble ("valor_reteica") );
                in.setValor_reteica_me    ( rs.getDouble ("valor_reteica_me") );
                in.setAccount_code_rica   ( Util.coalesce( rs.getString("account_rica") , "" ) );
                
                in.setTipo_doc      ( Util.coalesce( rs.getString("tipo_doc")      , "" ) );
                in.setDocumento     ( Util.coalesce( rs.getString("documento")     , "" ) );
                in.setFecha_factura ( Util.coalesce( rs.getString("fecha_factura") , "" ) );
                
                // cuenta del cliente
                in.setCuenta        ( Util.coalesce( rs.getString("cuenta")        , "" ) );
                in.setAuxiliar      ( Util.coalesce( rs.getString("auxiliar")      , "" ) );                
                
                lista.add(in);
                in = null; //Liberar Espacio JJCastro
            }
        } catch (Exception ex){
            ex.printStackTrace();
            throw new Exception ( ex.getMessage() );
        } finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (ps != null) {try {ps = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return lista;
    }    
    
    
    
     
    /**
     * Metodo para obtener ingresos pendientes por contabilizar
     * @autor mfontalvo
     * @throws Exception.
     * @return Vector, lista de ingresos.
     */
    public Vector obtenerIngresoSinContabilizar() throws Exception {
        return obtenerIngresos("a.fecha_contabilizacion = '0099-01-01 00:00:00' AND a.reg_status != 'A' AND a.creation_date <= TO_CHAR(now(),'YYYY-MM-DD 00:00:00') ");
    }  
    
    
    /**
     * Metodo para obtener ingresos ya contabilizados , anulados y sin descontabilizar
     * @autor mfontalvo
     * @throws Exception.
     * @return Vector, lista de ingresos.
     */
    public Vector obtenerIngresoSinDescontabilizar() throws Exception {
        return obtenerIngresos("a.fecha_contabilizacion != '0099-01-01 00:00:00' AND a.fecha_anulacion_contabilizacion = '0099-01-01 00:00:00' AND a.reg_status = 'A' AND a.creation_date <= TO_CHAR(now(),'YYYY-MM-DD 00:00:00')  ");
    } 
    
    
    
    
    
    
    
    
    
    
    
    
    /**
     * FUNCIONES DE CONSECUTIVO DEL COMPROBANTE
     */
    

    
    /**
     * Metodo para obtener el proximo numero de comprobante en el sistema
     * @autor mfontalvo
     * @param con, conexion
     * @throws Exception.
     * @return int, proximo numero de comprobante
     */    
    private int nextGroupTransaction (Connection con) throws Exception {
        PreparedStatement ps   = null;
        ResultSet         rs   = null;
        int               next = -1;
        String query = "SQL_NEXT_GROUP_TRANSACTION";
        try{
            if (con == null)
                throw new Exception ("SIN CONEXION.");
            ps = con.prepareStatement( this.obtenerSQL(query));
            rs = ps.executeQuery();
            if (rs.next()){
                next = rs.getInt("last_value");
            }
                
            return next;
        } catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        } finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (ps != null) {try {ps = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
        }
    }
    

    /**
     * Metodo para obtener el ultimo numero de comprobante en el sistema
     * @autor mfontalvo
     * @param con, conexion
     * @throws Exception.
     * @return int, numero del ultimo comprobante
     */
    private int currentGroupTransaction (Connection con) throws Exception {
        PreparedStatement ps   = null;
        ResultSet         rs   = null;
        int               current = -1;
        String query = "SQL_CURRENT_GROUP_TRANSACTION";
        try{
           if (con==null)
               throw new Exception ("SIN CONEXION.");
            
            ps = con.prepareStatement( this.obtenerSQL(query) );
            rs = ps.executeQuery();
            if (rs.next()){
                current = rs.getInt("current_value");
            }
            return current;
        } catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        } finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (ps != null) {try {ps = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
        }
    }


    /**
     * Metodo para modificar el conscutivo de los comprobantes
     * @autor mfontalvo
     * @param value, consecutivo del comprobante
     * @param con, conexion 
     * @throws Exception.
     */
    private void setGroupTransaction (int value, Connection con ) throws Exception {
        PreparedStatement ps   = null;
        String query = "SQL_SET_GROUP_TRANSACTION";
        try{
            if ( con == null )
                throw new Exception("SIN CONEXION");
           
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setInt(1, value );
            ps.executeQuery();

        } catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        } finally{
            if (ps != null) {try {ps = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
        }
    }    
    
    
    
    
    
    
    
    
    
    
    
    
    /**
     * FUNCIONES DE GRABACION DE UN COMPROBANTE CONTABLE.
     */
    
    
    /**
     * Metodo para guardar el comprobante contable con sus detalles
     * @autor mfontalvo
     * @param c, combrobante a guardar
     * @param in, ingreso q se esta procesando.
     * @throws Exception.
     */
    public void saveComprobante (Comprobantes c, Ingreso in) throws Exception {

        if (in == null) {
            throw new Exception("INGRESO NO VALIDO.");
        }

        if (c == null) {
            throw new Exception("COMPROBANTE NO VALIDO.");
        }

        if (c.getLista() == null || c.getLista().isEmpty()) {
            throw new Exception("COMPROBANTE SIN ITEMS.");
        }


        Connection con = null;
        Statement st = null;
        String query = "SQL_INSERT_COMPROBANTE";
        try {

            con = this.conectarJNDI(query);

            // numero de la transaccion
            int next = this.nextGroupTransaction(con);
            int current = next - 1;
            if (next == -1) {
                throw new Exception("NO SE PUDO OBTENER EL GRUPO DE LA TRANSACCION.");
            }

            try {
                ArrayList<String> sql = new ArrayList<>();
                c.setGrupo_transaccion(next);

                sql= this.getInsertComprobante(con, c);
                sql.addAll(this.getUpdateIngreso(con, in, c));
            
                if (sql.isEmpty()) {
                    throw new Exception("NO SE PUDO GENERAR LOS SQL PARA EL PROCESO.");
                }

                st = con.createStatement();
                for (String sql1 : sql) {
                    st.addBatch(sql1);
                }
                st.executeBatch();

            } catch (Exception e) {
                e.printStackTrace();

                // restauramos la transaction
                int currentA = this.currentGroupTransaction(con);
                if (currentA == next) {
                    this.setGroupTransaction(current, con);
                }
                throw new Exception(e);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        } 
finally{
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
    }
    
    
    
    /**
     * Metodo para actualizar lo campos de contabilizacion y descontabilizacion de ingresos
     * @param con, conexion,
     * @param in, ingreso a actualizar
     * @param com, comprobante generado apartir del proces de contabilizacion o descontabilizcion
     * @throws Exception.
     * @return sql, sql de actualiacion a la tabla de ingresos
     */
    public ArrayList<String> getUpdateIngreso(Connection con, Ingreso in, Comprobantes com) throws Exception{
        PreparedStatement ps =  null;
        ArrayList<String> querys =new ArrayList<>();
        try{
            if (con == null)
                throw new Exception("SIN CONEXION");
            
            String queryCAB = "";
            String queryDET = "";
            
            if (!in.getReg_status().trim().equalsIgnoreCase("A")){
                queryCAB = "SQL_UPDATE_CONTABILIZACION";
                queryDET = "SQL_UPDATE_DETALLE_CONTABILIZACION";
            } else {
                queryCAB = "SQL_UPDATE_DESCONTABILIZACION";
                queryDET = "SQL_UPDATE_DETALLE_DESCONTABILIZACION";                
            }            


            ps = con.prepareStatement( this.obtenerSQL( queryCAB ) );
            ps.setInt   (1, com.getGrupo_transaccion() );
            ps.setString(2, com.getFecha_creacion()    );
            ps.setString(3, com.getPeriodo()           );
            ps.setString(4, in.getDstrct()             );
            ps.setString(5, in.getTipo_documento()     );
            ps.setString(6, in.getNum_ingreso()        );
            querys.add(ps.toString());
            
            ps = con.prepareStatement( this.obtenerSQL( queryDET ) );
            ps.setInt   (1, com.getGrupo_transaccion() );
            ps.setString(2, com.getFecha_creacion()    );
            ps.setString(3, com.getPeriodo()           );
            ps.setString(4, in.getDstrct()             );
            ps.setString(5, in.getTipo_documento()     );
            ps.setString(6, in.getNum_ingreso()        );            
            querys.add(ps.toString());
            
        } catch (Exception ex){
            throw new Exception(ex.getMessage());
        } finally {
            if (ps != null) {try {ps = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
        }
        return querys;
    }
    
    
    /**
     * Metodo que devuelve la consulta sql para insertar un comprobante
     * @autor mfontalvo
     * @throws: En caso de que un error de base de datos ocurra.
     * @return Un String con el sql
     */
    public ArrayList<String> getInsertComprobante(Connection con, Comprobantes c) throws Exception {
        if (c==null)
            throw new Exception("COMPROBANTE NO VALIDO.");
        
        if (c.getLista()==null || c.getLista().isEmpty())
            throw new Exception("COMPROBANTE SIN ITEMS.");
        
        PreparedStatement st = null;
        ArrayList<String> result = new ArrayList<>();
        try {
            if (con == null)
                throw new Exception("SIN CONEXION.");
            st = con.prepareStatement( this.obtenerSQL("SQL_INSERT_COMPROBANTE" ) );
            st.setString (1 , "" );
            st.setString (2 , c.getDstrct() );
            st.setString (3 , c.getTipodoc() );
            st.setString (4 , c.getNumdoc() );
            st.setInt    (5 , c.getGrupo_transaccion() );
            st.setString (6 , c.getSucursal() );
            st.setString (7 , c.getPeriodo() );
            st.setString (8 , c.getFechadoc());
            st.setString (9 , c.getDetalle());
            st.setString (10, c.getTercero());
            st.setDouble (11, c.getTotal_debito());
            st.setDouble (12, c.getTotal_credito());
            st.setInt    (13, c.getTotal_items());
            st.setString (14, c.getMoneda());
            st.setString (15, c.getUsuario());
            st.setString (16, c.getFecha_creacion());
            st.setString (17, c.getUsuario());
            st.setString (18, c.getFecha_creacion());
            st.setString (19, c.getBase());
            st.setString (20, c.getTipo_operacion());            
            result.add(st.toString());      
            
            
            Vector comprobantes = c.getLista();
            st = con.prepareStatement( this.obtenerSQL("SQL_INSERT_ITEM_COMPROBANTE" ) );
            for (int i = 0; i < comprobantes.size(); i++){
                Comprobantes item = (Comprobantes) comprobantes.get(i);
                st.clearParameters();
                st.setString (1 , "" );
                st.setString (2 , item.getDstrct() );
                st.setString (3 , item.getTipodoc() );
                st.setString (4 , item.getNumdoc() );
                st.setInt    (5 , c.getGrupo_transaccion()); // grupo transaction
                st.setString (6 , item.getPeriodo() );
                st.setString (7 , item.getCuenta() );            
                st.setString (8 , item.getAuxiliar() );
                st.setString (9 , item.getDetalle());
                st.setDouble (10, item.getTotal_debito());
                st.setDouble (11, item.getTotal_credito());            
                st.setString (12, item.getTercero());
                st.setString (13, item.getDocumento_interno());
                st.setString (14, item.getUsuario());
                st.setString (15, item.getFecha_creacion());
                st.setString (16, item.getUsuario());
                st.setString (17, item.getFecha_creacion());
                st.setString (18, item.getBase());
                st.setString (19, item.getTipodoc_rel());
                st.setString (20, item.getNumdoc_rel ());
                result.add(st.toString());            
            }            
        }catch( Exception e ){            
            e.printStackTrace();
            throw new Exception( e.getMessage() );            
        }finally{            
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
        }
        return result;
    }
    
    
    
    /**
     * Metodo para obtener la equivalencia contable del tipo del documento
     * @autor mfontalvo
     * @param tipo_documento
     * @return equivalencia contable del tipo de documento
     * @throws Exception.
     */
    public TreeMap obtenerEquivalenciaTipoDoc () throws Exception {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet         rs = null;
        String            query = "SQL_OBTENER_EQUIVALENCIAS_VALIDAS";
        TreeMap           equivalencia = new TreeMap();
        try{
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            rs = ps.executeQuery();
            
            while (rs.next()){
                equivalencia.put( rs.getString("codigo_interno"), rs.getString("codigo") );
            }
        } catch (Exception ex){
            ex.printStackTrace();
            throw new Exception ( ex.getMessage() );
        }finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (ps != null) {try {ps = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return equivalencia;        
    }
    

    
    
    
    /**
     * Metodo para buscar un comprobante
     * @autor mfontalvo
     * @param tipo_documento, tipo del documentos del comprobante
     * @param documento, documento del comprobante
     * @transaccion, grupo de la transaccion del comprobante
     * @throws Exception.
     * @return Comprobante contable.
     */
    public Comprobantes getComprobante(String tipo_documento, String documento, int transaccion) throws Exception {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_OBTENER_COMPROBANTE";
        Comprobantes com = null;
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, tipo_documento);
            st.setString(2, documento);
            st.setInt(3, transaccion);
            rs = st.executeQuery();
            while (rs.next()) {
                com = new Comprobantes();
                com.setDstrct(Util.coalesce(rs.getString("dstrct"), ""));
                com.setTipodoc(Util.coalesce(rs.getString("tipodoc"), ""));
                com.setNumdoc(Util.coalesce(rs.getString("numdoc"), ""));
                com.setGrupo_transaccion(rs.getInt("grupo_transaccion"));
                com.setSucursal(Util.coalesce(rs.getString("sucursal"), ""));
                com.setPeriodo(Util.coalesce(rs.getString("periodo"), ""));
                com.setFechadoc(Util.coalesce(rs.getString("fechadoc"), ""));
                com.setDetalle(Util.coalesce(rs.getString("detalle"), ""));
                com.setTercero(Util.coalesce(rs.getString("tercero"), ""));
                com.setTotal_debito(rs.getDouble("total_debito"));
                com.setTotal_credito(rs.getDouble("total_credito"));
                com.setTotal_items(rs.getInt("total_items"));
                com.setMoneda(Util.coalesce(rs.getString("moneda"), ""));
                com.setUsuario(Util.coalesce(rs.getString("creation_user"), ""));
                com.setFecha_creacion(Util.coalesce(rs.getString("creation_date"), ""));
                com.setBase(Util.coalesce(rs.getString("base"), ""));
                com.setTipo_operacion(Util.coalesce(rs.getString("tipo_operacion"), ""));

                // detalles del comprobante
                com.setLista(this.getDetalleComprobante(com, con));
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new SQLException(e.getMessage());
        }      
finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return com;
    }    
 
    
    
    /**
     * Metodo para obtener el detalle de un comprobante 
     * @autor mfontalvo
     * @param cab, cabecera del comprobante
     * @param con, conexion
     * @throws  Exception.
     * @return Vector, detalles (items) del comprobante.
     */
    public Vector getDetalleComprobante( Comprobantes cab , Connection con) throws Exception {
        PreparedStatement st    = null;
        ResultSet         rs    = null;
        Vector            dt    = new Vector();
        String            query = "SQL_OBTENER_DETALLE_COMPROBANTE";
        try {                        
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString( 1, cab.getTipodoc()           );
            st.setString( 2, cab.getNumdoc()            );
            st.setInt   ( 3, cab.getGrupo_transaccion() );
            
            rs = st.executeQuery();
            while (rs.next()){
                Comprobantes detalle = new Comprobantes();
                detalle.setDstrct            ( Util.coalesce( rs.getString("dstrct") , ""));
                detalle.setTipodoc           ( Util.coalesce( rs.getString("tipodoc"), ""));
                detalle.setNumdoc            ( Util.coalesce( rs.getString("numdoc") , ""));
                detalle.setGrupo_transaccion ( rs.getInt    ("grupo_transaccion")         );
                detalle.setTransaccion       ( rs.getInt    ("transaccion"));
                detalle.setPeriodo           ( Util.coalesce( rs.getString("periodo")  , ""));
                detalle.setCuenta            ( Util.coalesce( rs.getString("cuenta")   , ""));
                detalle.setAuxiliar          ( Util.coalesce( rs.getString("auxiliar") , ""));
                detalle.setDetalle           ( Util.coalesce(  rs.getString("detalle") , ""));
                detalle.setTotal_debito      ( rs.getDouble("valor_debito" ) );
                detalle.setTotal_credito     ( rs.getDouble("valor_credito") );
                detalle.setTercero           ( Util.coalesce( rs.getString("tercero")           , ""));
                detalle.setDocumento_interno ( Util.coalesce( rs.getString("documento_interno") , ""));
                detalle.setUsuario           ( Util.coalesce( rs.getString("creation_user" )    , ""));
                detalle.setFecha_creacion    ( Util.coalesce( rs.getString("creation_date" )    , ""));
                detalle.setBase              ( Util.coalesce( rs.getString("base"          )    , ""));                
                detalle.setTipodoc_rel       ( Util.coalesce( rs.getString("tipodoc_rel")       , ""));
                detalle.setNumdoc_rel        ( Util.coalesce( rs.getString("documento_rel")     , ""));
                dt.add( detalle );
                detalle = null; //Liberar Espacio JJCastro
            }
        } catch( Exception e ){  
            e.printStackTrace();
            throw new Exception( e.getMessage() );
        }        
        finally{            
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
        }
        return dt;
    }
    
    

    /**
     * Metodo para obtener valores relacionados a los items q se van a contabilizar
     * @autor mfontalvo
     * @param cab, ingreso a consultar.
     * @throws Exception.
     * @return factura, bean q contiene los datos del la factura relacionada a un item de un ingreso
     */
    public factura obtenerDatosFactura(Ingreso_detalle item) throws Exception {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_DATOS_FACTURA";
        factura f = null;
        try {
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            ps.setString(1, item.getDistrito());
            //ps.setString(2, item.getTipo_doc()   );
            ps.setString(2, item.getDocumento());
            rs = ps.executeQuery();

            while (rs.next()) {
                f = new factura();
                f.setValor_factura(rs.getDouble("valor_factura"));
                f.setValor_abono(rs.getDouble("valor_abono"));
                f.setValor_saldo(rs.getDouble("valor_saldo"));
                f.setValor_facturame(rs.getDouble("valor_facturame"));
                f.setValor_abonome(rs.getDouble("valor_abonome"));
                f.setValor_saldome(rs.getDouble("valor_saldome"));
                f.setMoneda(Util.coalesce(rs.getString("moneda"), ""));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (ps != null) {try {ps = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return f;
    }    
        
    
    
    
    
    
    
 
    /**
     * FUNCIONES DE GRABACION DE UN COMPROBANTE CONTABLE.
     */
    
    /**
     * Metodo para guardar el comprobante contable con sus detalles
     * @autor mfontalvo
     * @param c, combrobante a guardar
     * @param in, ingreso q se esta procesando.
     * @throws Exception.
     */
    public void saveComprobante (Comprobantes c) throws Exception {

        if (c==null)
            throw new Exception("COMPROBANTE NO VALIDO.");
        
        if (c.getLista()==null || c.getLista().isEmpty())
            throw new Exception("COMPROBANTE SIN ITEMS.");
        
        
        Connection  con   = null;
        Statement   st    = null;
        String      query = "SQL_INSERT_COMPROBANTE";            
        try{
            
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            // numero de la transaccion
            int next    = this.nextGroupTransaction(con);
            int current = next-1;            
            if (next==-1) throw new Exception ("NO SE PUDO OBTENER EL GRUPO DE LA TRANSACCION.");            
            
            try{
                ArrayList<String> sql = new ArrayList<>();
                c.setGrupo_transaccion(next);
                
                sql=this.getInsertComprobante(con, c);
                if (sql.isEmpty())
                    throw new Exception("NO SE PUDO GENERAR LOS SQL PARA EL PROCESO.");
                
                st = con.createStatement();
                for (String sql1 : sql) {
                    st.addBatch(sql1);
                }
                st.executeBatch();
                
            } catch (Exception e){
                e.printStackTrace();
                
                // restauramos la transaction
                int currentA = this.currentGroupTransaction(con);
                if (currentA == next ) {
                    this.setGroupTransaction(current, con);
                }
                throw new Exception (e);
            } 
           
        } catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        } 
        finally {
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
    }
    
    public String BancoDAO(String cta) throws Exception {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_DATOS_BANCO";
        factura f = null;
        String a = "";
        try {
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            ps.setString(1, cta);
            rs = ps.executeQuery();
            if (rs.next()) {
                a = rs.getString(1);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        } finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (ps != null) {try {ps = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return a;
    }
    
    public String ClienteDAO(String cta) throws Exception {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet         rs = null;
        String            query = "SQL_DATOS_CLIENTE";
        factura           f = null;
        String a="";
        try{
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            ps.setString(1, cta  );
            rs = ps.executeQuery();
            if (rs.next()){
                a=rs.getString(1);
            }
        } catch (Exception ex){
            ex.printStackTrace();
            throw new Exception ( ex.getMessage() );
        } finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (ps != null) {try {ps = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return a;
    }  
    
    public String CuentaDAO(String cta) throws Exception {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_DATOS_CUENTA";
        factura f = null;
        String a = "";
        try {
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            ps.setString(1, cta);
            rs = ps.executeQuery();
            if (rs.next()) {
                a = rs.getString(1);
            }
        } catch (Exception ex){
            ex.printStackTrace();
            throw new Exception ( ex.getMessage() );
        }  finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (ps != null) {try {ps = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return a;
    } 
        
    public Vector obtenerDetalleIngresoNuevo(Ingreso cab) throws Exception {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet         rs = null;
        String            query = "SQL_OBTENER_DETALLE_INGRESOSREMIX";
        Vector            lista = new Vector();
        try{
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            ps.setString(1, cab.getDstrct()         );
            ps.setString(2, cab.getTipo_documento() );
            ps.setString(3, cab.getNum_ingreso()    );
            
            rs = ps.executeQuery();
            
            while (rs.next()){
                Ingreso_detalle in = new Ingreso_detalle();
                //in.setAnulado             ( rs.getBoolean( "reg_status") ) ;
                in.setReg_status(rs.getString("reg_status"));
                
                in.setDistrito            ( Util.coalesce( rs.getString("dstrct") , "" ) );
                in.setTipo_documento      ( Util.coalesce( rs.getString("tipo_documento") , "" ) );
                in.setNumero_ingreso      ( Util.coalesce( rs.getString("num_ingreso") , "" ) );
                in.setItem                ( rs.getInt("item") );
                in.setNit_cliente         ( Util.coalesce( rs.getString("nitcli") , "" ) );
                in.setDescripcion         ( Util.coalesce( rs.getString("descripcion") , "" ) );
                in.setValor_ingreso       ( rs.getDouble("valor_ingreso") );
                in.setValor_ingreso_me    ( rs.getDouble("valor_ingreso_me") );
                in.setValor_diferencia    ( rs.getDouble("valor_diferencia_tasa") );
                
                // retefuente
                in.setCodigo_retefuente   ( Util.coalesce( rs.getString("codigo_retefuente") , "" ) );
                in.setValor_retefuente    ( rs.getDouble ("valor_retefuente") );
                in.setValor_retefuente_me ( rs.getDouble ("valor_retefuente_me") );
                in.setAccount_code_rfte   ( Util.coalesce( rs.getString("account_rfte") , "" ) );
                
                // rica
                in.setCodigo_reteica      ( Util.coalesce( rs.getString("codigo_reteica") , "" ) );
                in.setValor_reteica       ( rs.getDouble ("valor_reteica") );
                in.setValor_reteica_me    ( rs.getDouble ("valor_reteica_me") );
                in.setAccount_code_rica   ( Util.coalesce( rs.getString("account_rica") , "" ) );
                
                in.setTipo_doc      ( Util.coalesce( rs.getString("tipo_doc")      , "" ) );
                in.setDocumento     ( Util.coalesce( rs.getString("documento")     , "" ) );
                in.setFecha_factura ( Util.coalesce( rs.getString("fecha_factura") , "" ) );
                
                // cuenta del cliente
                in.setCuenta        ( Util.coalesce( rs.getString("cuenta")        , "" ) );
                in.setAuxiliar      ( Util.coalesce( rs.getString("auxiliar")      , "" ) );                
                
                in.setFactura(Util.coalesce( rs.getString("factura")      , "" ) );      
                in.setCreation_date(Util.fechaActualTIMESTAMP());            
                in.setLast_update(Util.fechaActualTIMESTAMP());            
                in.setBase(Util.coalesce( rs.getString("base")      , "" ) );
                in.setFecha_contabilizacion("0099-01-01 00:00:00");
                in.setDescripcion(Util.coalesce( rs.getString("descripcion") , "" ) );
                in.setValor_tasa(Double.parseDouble(rs.getString("valor_tasa")));
                in.setValor_saldo_factura_me(Double.parseDouble(rs.getString("saldo_factura")));
                
                in.setDescripcion_factura(Util.coalesce( rs.getString("descripcion") , "" ) );
                in.setTipo_aux("");
                
                lista.add(in);
                in  = null; //Liberar Espacio JJCastro
            }
        } catch (Exception ex){
            ex.printStackTrace();
            throw new Exception ( ex.getMessage() );
        }finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (ps != null) {try {ps = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return lista;
    }

}
