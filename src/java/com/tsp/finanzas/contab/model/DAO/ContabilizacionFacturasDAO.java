/***************************************
 * Nombre Clase ............. ContabilizacionFacturasDAO.java
 * Descripci�n  .. . . . . .  Permitimos realizar consultas para la contabilizacion de facturas
 * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
 * Fecha . . . . . . . . . .  13/06/2006
 * versi�n . . . . . . . . .  1.0
 * Copyright ...............  Transportes Sanchez Polo S.A.
 *******************************************/

package com.tsp.finanzas.contab.model.DAO;


import java.io.*;
import java.util.*;
import java.sql.*;
import com.tsp.finanzas.contab.model.beans.*;
import com.tsp.util.*;
import com.tsp.operation.model.DAOS.MainDAO;
import com.tsp.operation.model.beans.StringStatement;



public class ContabilizacionFacturasDAO extends MainDAO{
    
    
    
    private String APROBADOR        = "ADMIN";
    private String PRE_COMPROBANTE  = "CONTABILIZACION";
    private String PRE_DETALLE      = "ITEM";
    private String PRE_DETALLE_IMP  = "IMPUESTO";
    private String AUX_COMPROBANTE  = "";
    private String OF_PPAL          = "OP";
    
    public  String CABECERA         = "C";
    public  String DETALLE          = "D";
    public  String SALDO            = "S";
    
    
    private String TIPO_FACTURA_AMD  = "FAA";
    
    private String TIPO_CONTAB_OP    = "COP";   // FACTURAS OPS
    private String TIPO_CONTAB_ADM   = "CFA";   // FACTURAS ADMIN
    private String TIPO_CONTAB_NT    = "CNT";   // FACTURAS ND ND
    
    
    
    private final String FACTURA         = "010";
    private final String NC              = "035";
    private final String ND              = "036";
    
    public  int   CANTIDAD_IMP_ITEM      =  0;
    
    
    
    public ContabilizacionFacturasDAO() {
        super("ContabilizacionFacturasDAO.xml");
    }
    
    public ContabilizacionFacturasDAO(String dataBaseName) {
        super("ContabilizacionFacturasDAO.xml", dataBaseName);
    }
    
    
    /**
     * M�todos que setea valores nulos
     * @autor.......fvillacob
     * @version.....1.0.
     **/
    private String reset(String val){
        if(val==null)
            val = "";
        return val;
    }
    
    
    
    
    /**
     * M�todo que busca si la cta requiere auxiliar
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public  String requiereAUX(String distrito, String cta) throws Exception{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_REQUIERE_AUX";
        String aux = "";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, distrito);
            st.setString(2, cta);
            rs = st.executeQuery();
            if (rs.next()) {
                cta = reset(rs.getString("auxiliar"));
            }

        } catch (Exception e) {
            throw new Exception("requiereAUX " + e.getMessage());
        }
finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return cta;
    }
    
    
    
    
    
    /**
     * M�todo que busca si la cuenta tiene dicho subledger asociado
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public  boolean existeCuentaSubledger(String distrito, String cta, String auxiliar) throws Exception{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_CUENTA_SUBLEDGER";
        boolean estado = false;
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, distrito);
            st.setString(2, cta);
            st.setString(3, auxiliar);
            rs = st.executeQuery();
            if (rs.next()) {
                estado = true;
            }

        } catch (Exception e) {
            throw new Exception("existeCuentaSubledger " + e.getMessage());
        }
finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return estado;
    }
    
    
    
    
    /**
     * M�todo que busca si la cuenta tiene dicho subledger asociado
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public  String getAccoundCodeC(String oc) throws Exception{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_ACCOUNT_CODE_C";
        String cta = "";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, oc);
            rs = st.executeQuery();
            if (rs.next()) {
                cta = reset(rs.getString("account_code_c"));
            }

        } catch (Exception e) {
            throw new Exception("getAccoundCodeC " + e.getMessage());
        }
finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return cta;
    }
    
    
    
    
     /**
     * M�todo que busca la cuenta contable diferencia en cambio a planilla
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public  String getCuenta_DifCambio(String distrito, String concepto) throws Exception{
        Connection con = null;
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        String             query   = "SQL_CUENTA_DIFERENCIA_CAMBIO";
        String             cuenta  = "";
        try{
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, distrito  );
            st.setString(2, concepto  );
            rs = st.executeQuery();
            if(rs.next())
                cuenta = reset( rs.getString("account")  );
            
        }catch(Exception e){
            throw new Exception( "getCuenta_DifCambio " + e.getMessage());
        }
finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return cuenta;
    }
    
    
    
    
    // MONEDA : -------------------------------------------------------------------------
    
    /**
     * M�todo que busca la moneda local
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public  String getMoneda(String distrito) throws Exception{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_MONEDA_LOCAL";
        String moneda = "";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, distrito);
            rs = st.executeQuery();
            if (rs.next()) {
                moneda = reset(rs.getString("moneda"));
            }

        } catch (Exception e) {
            throw new Exception("getMoneda " + e.getMessage());
        }
finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return moneda;
    }
    
    
    
    
    
    
    
    
    
    // COMPROBANTE : ------------------------------------------------------------------
    
    /**
     * M�todo que busca las facturas que no esten contabilizadas
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public  List getFacturasNoContabilizadas(String distrito, String ayer) throws Exception{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        List lista = new LinkedList();
        String query = "SQL_FACTURAS_NO_CONTABILIZADAS";
        try {

            String moneda = this.getMoneda(distrito);
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, distrito);
            st.setString(2, ayer);
            rs = st.executeQuery();
            while (rs.next()) {
                ComprobanteFacturas comprobante = loadComprobantes(rs);
                comprobante.setMoneda(moneda);
                lista.add(comprobante);
                comprobante = null; //Liberar Espacio JJCastro
            }

        } catch (Exception e) {
            throw new Exception("getFacturasNoContabilizadas " + e.getMessage());
        }
finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return lista;
    }
    
    
    
    
    
    
    /**
     * M�todo que carga los datos de la factura
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public  ComprobanteFacturas loadComprobantes(ResultSet  rs)throws Exception{
        ComprobanteFacturas  comp  = new  ComprobanteFacturas();
        try{
            comp.setDstrct           (  reset( rs.getString("dstrct")                  )    );
            comp.setTipodoc          (  reset( rs.getString("sigla_comprobante")       )    );  // Tipo del comprobante
            comp.setTercero          (  reset( rs.getString("tercero")                 )    );
            comp.setNumdoc           (  reset( rs.getString("numdoc")                  )    );
            comp.setTipodoc_factura  (  reset( rs.getString("tipo_doc")                )    );  // Tipo de tabla factura
            comp.setBase             (  reset( rs.getString("base")                    )    );
            comp.setDetalle          (  "CONT " +  comp.getTipodoc_factura() + " " +  comp.getNumdoc() );
            comp.setSucursal         (  OF_PPAL                                             );
            comp.setAprobador        (  APROBADOR                                           );
            comp.setMonedaFac        (  reset( rs.getString("moneda")                  )    );
            comp.setFechaFactura     (  reset( rs.getString("fecha_factura")           )    );  
            
            comp.setHc               (  reset( rs.getString("hc")                      )    );
            comp.setCuenta           (  reset( rs.getString("cuenta")                  )    );
            comp.setCuentaContable   (  reset( rs.getString("cc_contable")             )    );
            comp.setCmc              (  reset( rs.getString("dbcr")                    )    );
            
            comp.setAuxiliar         ( AUX_COMPROBANTE                                      );
            comp.setDocumento_interno( comp.getTipodoc_factura()                            );
            comp.setTipo             ( CABECERA );
            comp.setOc               (  ""  );
            comp.setOrigen           (  "C" );   // Clientes 
            comp.setClaseFactura     (  "4" );
            comp.setPeriodo          (  reset( rs.getString("periodo")                  )   );  // Periodo contable, fecha de creaci�n
            comp.setRef_3            (  reset (rs.getString("nomb_cli")));
            comp.setAbc                 ("N/A");
            comp.setTotal_debito     (  rs.getDouble("debe")                  ); 
            comp.setTdoc_rel(reset(rs.getString("tipo_doc")));
            comp.setNumdoc_rel(reset(rs.getString("numdoc")));
            comp.setTipo_referencia_1("");
            comp.setReferencia_1("");
            comp.setTipo_referencia_2("");
            comp.setReferencia_2("");
            comp.setTipo_referencia_3("");
            comp.setReferencia_3("");
        }catch(Exception e){
            throw new Exception( "loadComprobantes " + e.getMessage());
        }
        return comp;
    }
    
    
    
    
    
    
    
    
    // DETALLE DE COMPROBANTE : ------------------------------------------------------------------
    
    
    /**
     * M�todo que busca los items de las facturas que no esten contabilizadas
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public  List getItems(ComprobanteFacturas  comprobante) throws Exception{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        List lista = new LinkedList();
        String query = "SQL_ITEM_FACTURAS";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, comprobante.getDstrct());
            st.setString(2, comprobante.getTercero());
            st.setString(3, comprobante.getTipodoc_factura());
            st.setString(4, comprobante.getNumdoc());
            rs = st.executeQuery();
            while (rs.next()) {
                ComprobanteFacturas comprodet = loadComproDet(rs);

                // Si en Nota Credito Invertir signo:
                if (comprobante.getTipodoc_factura().equals(NC)) {

                    double cre = comprodet.getTotal_credito();
                    double deb = comprodet.getTotal_debito();

                    comprodet.setTotal_credito(deb);
                    comprodet.setTotal_debito(cre);

                }

                if (comprodet.getVlr() != 0) {
                    lista.add(comprodet);
                }

            }

        }catch (Exception e) {
         throw new Exception("getItems " + e.getMessage());
        }
finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return lista;
    }
    
    
    
    /**
     * M�todo que carga los datos de la factura
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public  ComprobanteFacturas loadComproDet(ResultSet  rs)throws Exception{
        ComprobanteFacturas  comp  = new  ComprobanteFacturas();
        try{
            comp.setDstrct           (  reset( rs.getString("dstrct")           ) );
            comp.setTercero          (  reset( rs.getString("tercero")          ) );
            comp.setTipodoc          (  reset( rs.getString("tipo_doc")         ) );            
            comp.setNumdoc           (  reset( rs.getString("numdoc")           ) );
            comp.setNoItem           (  reset( rs.getString("item")             ) );
            comp.setDetalle          (  reset( rs.getString("detalle")          ) );
            comp.setVlr              (         rs.getDouble("valor")              );
            comp.setVlrME            (         rs.getDouble("valorme")            );
            comp.setCuenta           (  reset( rs.getString("cuenta")           ) );
            comp.setAuxiliar         (  reset( rs.getString("auxiliar")         ) );
            comp.setTipodoc_factura  (  reset( rs.getString("tipo_doc")         ) );
            comp.setOt               (  reset( rs.getString("ot")   )             );   
            comp.setTdoc_rel         (  reset( rs.getString("tipodocRel")       ) );
            comp.setNumdoc_rel       (  reset( rs.getString("documento_rel")    ) ); 
            comp.setDocumento_interno(  comp.getTipodoc_factura()               );
            comp.setTipo             (  DETALLE                                 ); 
            comp.setCmc(             reset( rs.getString("cmc")                ));
            double  vlr        = comp.getVlr();
            double  vlrDebito  = ( vlr>0 )? vlr      :  0 ;
            double  vlrCredito = ( vlr<0 )? vlr * -1 :  0 ;
            comp.setTotal_credito( vlrCredito );
            comp.setTotal_debito ( vlrDebito  );
            comp.setTipo_referencia_1(reset(rs.getString("tipo_referencia_1")));
            comp.setReferencia_1(reset(rs.getString("referencia_1")));
            comp.setTipo_referencia_2(reset(rs.getString("tipo_referencia_2")));
            comp.setReferencia_2(reset(rs.getString("referencia_2")));
            comp.setTipo_referencia_3(reset(rs.getString("tipo_referencia_3")));
            comp.setReferencia_3(reset(rs.getString("referencia_3")));
        }catch(Exception e){
            throw new Exception( "loadComprobantes " + e.getMessage());
        }
        return comp;
    }
    
    
    
    
    
    
    
    /**
     * M�todo que busca los impuestos de los items de las facturas que no esten contabilizadas
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
     public  List getImpuestos(ComprobanteFacturas  comprodet, String agencia , String ano) throws Exception{
         Connection con = null;
         PreparedStatement st = null;
         ResultSet rs = null;
         List lista = new LinkedList();
         String query = "SQL_IMP_ITEM";
         CANTIDAD_IMP_ITEM = 0;
         try {
             con = this.conectarJNDI(query);
             if (con != null) {
             st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
             st.setString(1, comprodet.getDstrct());
             st.setString(2, comprodet.getTercero());
             st.setString(3, comprodet.getTipodoc_factura());
             st.setString(4, comprodet.getNumdoc());
             st.setString(5, comprodet.getNoItem());
             st.setString(6, ano);  // A�o de la factura
             st.setString(7, agencia);  // OP agencia origen, ADM agencia factura
             //System.out.println("Q "+st.toString());
             rs = st.executeQuery();
             while (rs.next()) {
                 CANTIDAD_IMP_ITEM++;
                 ComprobanteFacturas imp = new ComprobanteFacturas();
                 imp.setNoItem(reset(rs.getString("item")));
                 imp.setTipoImpuesto(reset(rs.getString("tipo_impuesto")));
                 imp.setVlr(rs.getDouble("valor"));
                 imp.setCodigoImpuesto(reset(rs.getString("cod_impuesto")));
                 imp.setDetalle(PRE_DETALLE_IMP + " " + reset(rs.getString("item")) + " - " + reset(rs.getString("tipo_impuesto")) + " " + reset(rs.getString("cod_impuesto")) + " " + reset(rs.getString("porcent_impuesto")));

                 if (imp.getVlr() != 0) {
                     lista.add(imp);
                 }

             }

         }} catch (Exception e) {
             throw new Exception("getImpuestos " + e.getMessage());
         }
finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return lista;
}
    
    
    
    
    
    /**
     * M�todo que busca la cuenta del impuesto
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public  String getCuentaImpuesto(String distrito, String codImpuesto, String agencia, String ano)throws Exception{
        Connection con = null;
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        String             query   = "SQL_CUENTA_IMPUESTO";
        String             imp     = "";
        try{            
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, distrito    );
            st.setString(2, codImpuesto );
            st.setString(3, ano         );
            st.setString(4, agencia     );
            rs = st.executeQuery();
            if( rs.next() )
                imp = rs.getString("cod_cuenta_contable");
            
        }catch(Exception e){
            throw new Exception( " DAO: getCuentaImpuesto " + e.getMessage());
        }finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return imp;
    }
    
    
    
    
    
    
    
    /**
     * M�todo que busca la cuenta asociada al cmc y tipo doc
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public  Hashtable  getCuenta(String distrito, String tipoDoc, String cmc )throws Exception{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_CUENTA_CMC";
        Hashtable info = null;
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, distrito);
            st.setString(2, tipoDoc);
            st.setString(3, cmc);
            rs = st.executeQuery();
            if (rs.next()) {
                info = new Hashtable();
                info.put("cuenta", reset(rs.getString("cuenta")));
                info.put("dbcr", reset(rs.getString("dbcr")));
                info.put("tipo_cuenta", reset(rs.getString("tipo_cuenta")));
            }

        }catch(Exception e){
            throw new Exception( " DAO: getCuenta " + e.getMessage());
        }finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return info;
    }
    
    
    
    
    
    
    
    // INSERT  UPDATE : ----------------------------------------------------------------------------------
    
    
    
    
    
    
    
    /**
     * M�todo que inserta el comprobante
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public  void  deleteComprobante(ComprobanteFacturas  comprobante)throws Exception{
        Connection con = null;
        PreparedStatement st = null;
        String query = "SQL_DELETE_COMPROBANTE";
        try {

            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, comprobante.getDstrct());
            st.setString(2, comprobante.getTipodoc());
            st.setString(3, comprobante.getNumdoc());
            st.setInt(4, comprobante.getGrupo_transaccion());

            st.execute();


        }catch(Exception e){
            throw new Exception( "deleteComprobante " + e.getMessage());
        }finally {
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
    }
    
    
    
    
    
      /**
     * M�todo que inserta el comprobante
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public  String InsertComprobante(ComprobanteFacturas  comprobante, String user)throws Exception{
        Connection con = null;
        PreparedStatement  st      = null;
        String             query   = "SQL_INSERT_COMPROBANTE";
        String             sql     = "";
        try{            
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1,   comprobante.getDstrct()         );
            st.setString(2,   comprobante.getTipodoc()        );
            st.setString(3,   comprobante.getNumdoc()         );
            st.setString(4,   this.OF_PPAL                    );  //  comprobante.getSucursal()
            st.setString(5,   comprobante.getPeriodo()        );
            st.setString(6,   comprobante.getFechadoc()       );
            st.setString(7,   comprobante.getDetalle()        );
            st.setString(8,   comprobante.getTercero()        );
            st.setDouble(9,   comprobante.getTotal_debito()   );
            st.setDouble(10,  comprobante.getTotal_credito()  );
            st.setInt   (11,  comprobante.getTotal_items()    );
            st.setString(12,  comprobante.getMoneda()         );
            st.setString(13,  comprobante.getAprobador()      );
            st.setString(14,  user                            );
            st.setString(15,  comprobante.getBase()           );
            st.setString(16,  comprobante.getTipo_operacion() );
            st.setInt   (17,  comprobante.getGrupo_transaccion()    );
            
            sql = st.toString();
           // st.execute();
            
        }catch(Exception e){
            throw new Exception( "InsertComprobante " + e.getMessage());
        }finally {
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return sql;
    }
    
    public  String InsertComprobante(Comprobantes  comprobante, String user)throws Exception{
        Connection con = null;
        PreparedStatement  st      = null;
        String             query   = "SQL_INSERT_COMPROBANTE";
        String             sql     = "";
        try{
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1,   comprobante.getDstrct()         );
            st.setString(2,   comprobante.getTipodoc()        );
            st.setString(3,   comprobante.getNumdoc()         );
            st.setString(4,   this.OF_PPAL                    );  //  comprobante.getSucursal()
            st.setString(5,   comprobante.getPeriodo()        );
            st.setString(6,   comprobante.getFechadoc()       );
            st.setString(7,   comprobante.getDetalle()        );
            st.setString(8,   comprobante.getTercero()        );
            st.setDouble(9,   comprobante.getTotal_debito()   );
            st.setDouble(10,  comprobante.getTotal_credito()  );
            st.setInt   (11,  comprobante.getTotal_items()    );
            st.setString(12,  comprobante.getMoneda()         );
            st.setString(13,  comprobante.getAprobador()      );
            st.setString(14,  user                            );
            st.setString(15,  comprobante.getBase()           );
            st.setString(16,  comprobante.getTipo_operacion() );
            st.setInt   (17,  comprobante.getGrupo_transaccion()    );
            
            sql = st.toString();
            //System.out.println("SQL "+sql);
           // st.execute();
            
        }catch(Exception e){
            throw new Exception( "InsertComprobante " + e.getMessage());
        }finally {
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return sql;
    }
    
    
    
    
    
    /**
     * M�todo que inserta el comprobante detalle
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public  ArrayList<String> InsertComprobanteDetalle(List lista, int sec, String user)throws Exception{
        StringStatement st = null;
        String             query   = "SQL_INSERT_COMPRODET";
        ArrayList<String>  listSql = new ArrayList<String>();
        try{            
            for(int i=0;i<lista.size();i++){
                ComprobanteFacturas  comprodet = (ComprobanteFacturas)lista.get(i);
                st = new StringStatement(this.obtenerSQL(query), true);            
            
                st.setString(1,   comprodet.getDstrct()            );
                st.setString(2,   comprodet.getTipodoc()           );
                st.setString(3,   comprodet.getNumdoc()            );
                st.setInt   (4,   sec                              );
                st.setString(5,   comprodet.getPeriodo()           );
                st.setString(6,   comprodet.getCuenta()            );
                st.setString(7,   comprodet.getAuxiliar()          );
                st.setString(8,   comprodet.getAbc()               );
                st.setString(9,   comprodet.getDetalle()           );
                st.setDouble(10,  comprodet.getTotal_debito()      );
                st.setDouble(11,  comprodet.getTotal_credito()     );
                st.setString(12,  comprodet.getTercero()           );
                st.setString(13,  user                             );
                st.setString(14,  comprodet.getBase()              );
                st.setString(15,  comprodet.getDocumento_interno() );
                st.setString(16,  comprodet.getTdoc_rel()          );
                st.setString(17,  comprodet.getNumdoc_rel()        );
                st.setString(18, comprodet.getTipo_referencia_1());
                st.setString(19, comprodet.getReferencia_1());
                st.setString(20, comprodet.getTipo_referencia_2());
                st.setString(21, comprodet.getReferencia_2());
                st.setString(22, comprodet.getTipo_referencia_3());
                st.setString(23, comprodet.getReferencia_3());
                
                listSql.add(st.getSql());
                st = null;
            }
            
        }catch(Exception e){
            throw new Exception( "InsertComprobanteDetalle " + e.getMessage());
        }finally {
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
        }
        return listSql;
    }
    
     public  ArrayList<String> InsertComprobanteDetalle2(List lista, int sec, String user)throws Exception{
        Connection con = null;
        PreparedStatement  st      = null;
        String             query   = "SQL_INSERT_COMPRODET";
        ArrayList<String> sql     = new ArrayList<>();
        try{
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            for(int i=0;i<lista.size();i++){
                Comprobantes  comprodet = (Comprobantes)lista.get(i);
                
                st.setString(1,   comprodet.getDstrct()            );
                st.setString(2,   comprodet.getTipodoc()           );
                st.setString(3,   comprodet.getNumdoc()            );
                st.setInt   (4,   sec                              );
                st.setString(5,   comprodet.getPeriodo()           );
                st.setString(6,   comprodet.getCuenta()            );
                st.setString(7,   comprodet.getAuxiliar()          );
                st.setString(8,   comprodet.getAbc()               );
                st.setString(9,   comprodet.getDetalle()           );
                st.setDouble(10,  comprodet.getTotal_debito()      );
                st.setDouble(11,  comprodet.getTotal_credito()     );
                st.setString(12,  comprodet.getTercero()           );
                st.setString(13,  user                             );
                st.setString(14,  comprodet.getBase()              );
                st.setString(15,  comprodet.getDocumento_interno() );
                st.setString(16,  comprodet.getTdoc_rel()          );
                st.setString(17,  comprodet.getNumdoc_rel()        );
                st.setString(18, "");
                st.setString(19, "");
                st.setString(20, "");
                st.setString(21, "");
                st.setString(22, "");
                st.setString(23, "");
                sql.add(st.toString());
                //System.out.println("Query Det "+sql);
                st.clearParameters();
                
            }
            
        }catch(Exception e){
            throw new Exception( "InsertComprobanteDetalle " + e.getMessage());
        }finally {
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return sql;
    }
    
    /**
     * M�todo que inserta el comprobante
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public  String updateFactura(ComprobanteFacturas  comprobante, String user)throws Exception{
        Connection con = null;
        PreparedStatement st = null;
        String query = "SQL_UPDATE_FACTURAS_DATOS_CONTABLES";
        String sql = "";
        try {

            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            //set:
            //st.setString(1, user );
            st.setString(1, comprobante.getFechadoc());
            st.setString(2, comprobante.getPeriodo());
            st.setString(3, String.valueOf(comprobante.getGrupo_transaccion()));
            //where:
            st.setString(4, comprobante.getDstrct());
            st.setString(5, comprobante.getTercero());
            st.setString(6, comprobante.getTipodoc_factura());
            st.setString(7, comprobante.getNumdoc());

            sql = st.toString();

        }catch(Exception e){
            throw new Exception( "updateFactura " + e.getMessage());
        }finally {
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return sql;
    }
    
    
    
    /**
     * M�todo que actualiza los moviminetos tipo Q
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public  String updateMovplaTipoQ(ComprobanteFacturas  comprobante, String user)throws Exception{
        Connection con = null;
        PreparedStatement  st      = null;
        String             query   = "SQL_UPDATE_MOVPLA_TIPO_Q";
        String             sql     = "";
        try{
            
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
          //set:           
            st.setString(1, String.valueOf(comprobante.getGrupo_transaccion())   );
         //   st.setString(2, comprobante.getFechadoc()  );
            st.setString(2, comprobante.getPeriodo()   );                        
            
          //where:            
            st.setString(3, comprobante.getOc()        );
            st.setString(4, comprobante.getDstrct()    );
            st.setString(5, comprobante.getNumdoc()    );
            
            
            sql = st.toString();            
            
        }catch(Exception e){
            throw new Exception( "updateMovplaTipoQ " + e.getMessage());
        }finally {
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return sql;
    }
    
    /*Metodos Reversi�n Descontabiolizacion*/
    /**
     * M�todo que Vector Comprobantes
     * @autor.......jescandon
     * @throws......Exception
     * @version.....1.0.
     **/
    public  Vector VectorComprobantes( Comprobantes  comprobante )throws Exception{
        Connection con = null;
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        String             query   = "SQL_COMPROBANTESDET";
        Vector             Vcomprobantes = new Vector();
        try{
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, comprobante.getTipodoc());
            st.setString(2, comprobante.getNumdoc());
            st.setInt(3, comprobante.getGrupo_transaccion());
            rs = st.executeQuery();
            while(rs.next()){
                Comprobantes aux = new Comprobantes();
                aux.setTipodoc(rs.getString("tipodoc"));
                aux.setNumdoc(rs.getString("numdoc"));
                aux.setGrupo_transaccion(rs.getInt("grupo_transaccion"));
                aux.setAuxiliar(rs.getString("auxiliar"));
                aux.setCuenta(rs.getString("cuenta"));
                aux.setTotal_debito(rs.getDouble("valor_debito"));
                aux.setTotal_credito(rs.getDouble("valor_credito"));
                aux.setDstrct(rs.getString("dstrct"));
                aux.setPeriodo(rs.getString("periodo"));
                Vcomprobantes.add(aux);
                aux = null; //Liberar Espacio JJCastro
            }
          

        }catch(Exception e){
            throw new Exception( "Descontabilizar " + e.getMessage());
        }finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return Vcomprobantes;
    }
     /**
     * M�todo que DescontabilizarMayor
     * @autor.......jescandon
     * @throws......Exception
     * @version.....1.0.
     **/
    public  String DescontabilizarMayor( Comprobantes  comprobante )throws Exception{
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_DESCONTABILIZARMAYOR";
        String sql = "";
        Connection con = null;
        try {
            String mes = comprobante.getPeriodo().substring(4, 6);
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query).replaceAll("#MES#", mes));//JJCastro fase2
            st.setDouble(1, comprobante.getTotal_debito());
            st.setDouble(2, comprobante.getTotal_credito());
            st.setDouble(3, comprobante.getTotal_debito());
            st.setDouble(4, comprobante.getTotal_credito());
            st.setString(5, comprobante.getCuenta());
            st.setString(6, comprobante.getDstrct());
            String anio = comprobante.getPeriodo().substring(0, 4);
            st.setString(7, anio);
            sql = st.toString();

            ////System.out.println("SQL " + sql );
        } catch (Exception e) {
            throw new Exception("Descontabilizar " + e.getMessage());
        }finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return sql;
    }
    
    
    /**
     * M�todo que DescontabilizarMayoSubledger
     * @autor.......jescandon
     * @throws......Exception
     * @version.....1.0.
     **/
    public  String DescontabilizarMayoSubledger( Comprobantes  comprobante )throws Exception{
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        String             query   = "SQL_DESCONTABILIZARMAYORSUBLEDGER";
        String             sql     = "";
        Connection con             = null;
        try{
            String mes = comprobante.getPeriodo().substring(4,6);
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query).replaceAll("#MES#", mes));//JJCastro fase2
            st.setDouble(1, comprobante.getTotal_debito());
            st.setDouble(2, comprobante.getTotal_credito());
            st.setDouble(3, comprobante.getTotal_debito());
            st.setDouble(4, comprobante.getTotal_credito());
            st.setString(5, comprobante.getCuenta());
            st.setString(6, comprobante.getDstrct());
            String anio = comprobante.getPeriodo().substring(0,4);
            st.setString(7, anio);
            st.setString(8, comprobante.getAuxiliar());
            sql = st.toString();
        }catch(Exception e){
            throw new Exception( "Descontabilizar " + e.getMessage());
        }finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return sql;
    }
    
    
     /**
     * M�todo que UpdateFechaAplicacion
     * @autor.......jescandon
     * @throws......Exception
     * @version.....1.0.
     **/
    public  String UpdateFechaAplicacion( Comprobantes  comprobante )throws Exception{
        Connection con = null;
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        String             query   = "SQL_UPDATEFECHA";
        String             sql     = "";
        
        try{
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, comprobante.getTipodoc());
            st.setString(2, comprobante.getNumdoc());
            st.setInt(3, comprobante.getGrupo_transaccion());            
            st.executeUpdate();            
            sql = st.toString();
            
        }catch(Exception e){
            throw new Exception( "Descontabilizar " + e.getMessage());
        }finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return sql;
    }
    
    /**
     * M�todo que getComprobantes
     * @autor.......jescandon
     * @throws......Exception
     * @version.....1.0.
     **/
    public  ComprobanteFacturas getComprobantes( Comprobantes  comprobante )throws Exception{
        Connection con = null;
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        ComprobanteFacturas       aux     = null;
        String             query   = "SQL_COMPROBANTES";
        try{
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, comprobante.getTipodoc());
            st.setString(2, comprobante.getNumdoc());
            st.setInt(3, comprobante.getGrupo_transaccion());
            rs = st.executeQuery();
            while(rs.next()){
                aux = new ComprobanteFacturas();
                aux.setDstrct(rs.getString("dstrct"));
                aux.setTipodoc(rs.getString("tipodoc"));
                aux.setNumdoc(rs.getString("numdoc"));
                aux.setGrupo_transaccion(rs.getInt("grupo_transaccion"));
                aux.setSucursal(rs.getString("sucursal"));
                aux.setPeriodo(rs.getString("periodo"));
                aux.setFechadoc(rs.getString("fechadoc"));
                aux.setDetalle(rs.getString("detalle"));
                aux.setTercero(rs.getString("tercero"));
                aux.setTotal_debito(rs.getDouble("total_credito"));
                aux.setTotal_credito(rs.getDouble("total_debito"));
                aux.setTotal_items(rs.getInt("total_items"));
                aux.setMoneda(rs.getString("moneda"));
                aux.setAprobador(rs.getString("aprobador"));
                aux.setUsuario(rs.getString("creation_user"));
                aux.setBase(rs.getString("base"));
                aux.setUsuario_aplicacion(rs.getString("usuario_aplicacion"));
                aux.setTipo_operacion(rs.getString("tipo_operacion"));
            }
          
            
        }catch(Exception e){
            throw new Exception( "Descontabilizar " + e.getMessage());
        }finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return aux;
    }
    
    
     /**
     * M�todo que Vector Comprobantes
     * @autor.......jescandon
     * @throws......Exception
     * @version.....1.0.
     **/
    
    public  List ListaComprobantes( Comprobantes  comprobante )throws Exception{
        Connection con = null;
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        String             query   = "SQL_COMPROBANTESDET";
        List               Vcomprobantes = new LinkedList();
        try{
            
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, comprobante.getTipodoc());
            st.setString(2, comprobante.getNumdoc());
            st.setInt(3, comprobante.getGrupo_transaccion());
            rs = st.executeQuery();
            while(rs.next()){
                ComprobanteFacturas aux = new ComprobanteFacturas();
                aux.setDstrct(rs.getString("dstrct"));
                aux.setTipodoc(rs.getString("tipodoc"));
                aux.setNumdoc(rs.getString("numdoc"));
                aux.setGrupo_transaccion(rs.getInt("grupo_transaccion"));
                aux.setAuxiliar(rs.getString("auxiliar"));
                aux.setCuenta(rs.getString("cuenta"));
                aux.setTotal_debito(rs.getDouble("valor_credito"));
                aux.setTotal_credito(rs.getDouble("valor_debito"));
                aux.setPeriodo(rs.getString("periodo"));
                aux.setDetalle(rs.getString("detalle"));
                aux.setTercero(rs.getString("tercero"));
                aux.setBase(rs.getString("base"));
                aux.setDocumento_interno(rs.getString("documento_interno"));                               
                Vcomprobantes.add(aux);
                aux = null; //Liberar Espacio JJCastro
            }
          
            
        }catch(Exception e){
            throw new Exception( "Descontabilizar " + e.getMessage());
        }finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return Vcomprobantes;
    }
    
    /**
     * M�todo que borra una lista de comprobantes
     * @autor.......jescandon
     * @throws......Exception
     * @version.....1.0.
     **/
    public  List deleteComprobanteSQL(List lista)throws Exception{
        Connection con = null;
        PreparedStatement  st      = null;
        String             query   = "SQL_DELETE_COMPROBANTE";
        try{
            
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            for(int i=0;i<lista.size();i++){
                ComprobanteFacturas  comprobante  = (ComprobanteFacturas) lista.get(i);
                    st.setString(1,   comprobante.getDstrct()   );
                    st.setString(2,   comprobante.getTipodoc()  );
                    st.setString(3,   comprobante.getNumdoc()   );
                    st.setInt   (4,   comprobante.getGrupo_transaccion() );
                st.execute();
                st.clearParameters();
                
            }
            
            
        }catch(Exception e){
            throw new Exception( "deleteComprobante " + e.getMessage());
        }finally {
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return lista;
    }
   
    public  int getCantidadItemsImpuestos(ComprobanteFacturas  comprodet) throws Exception{
        Connection con = null;
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        String             query   = "SQL_CANT_IMP_ITEM";
        int                total   = 0;
        try{
            
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, comprodet.getDstrct()          );
            st.setString(2, comprodet.getTercero()         );
            st.setString(3, comprodet.getTipodoc_factura() );
            st.setString(4, comprodet.getNumdoc()          );
            st.setString(5, comprodet.getNoItem()          );
            System.out.println(st.toString());
            rs = st.executeQuery();
            if( rs.next() )
                total = rs.getInt("total");
            
        }catch(Exception e){
            throw new Exception( "getCantidadItemsImpuestos " + e.getMessage());
        }
finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return total;
    }
}
