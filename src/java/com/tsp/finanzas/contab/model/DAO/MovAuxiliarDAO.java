/***************************************
 * Nombre Clase ............. MovAuxiliarDAO.java
 * Descripci�n  .. . . . . .  Permitimos realizar consultas de los movimientos auxiliar
 * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
 * Fecha . . . . . . . . . .  07/06/2006
 * versi�n . . . . . . . . .  1.0
 * Copyright ...............  Transportes Sanchez Polo S.A.
 *******************************************/




package com.tsp.finanzas.contab.model.DAO;





import java.util.*;
import java.sql.*;
import com.tsp.finanzas.contab.model.beans.*;
import com.tsp.util.*;
import com.tsp.operation.model.DAOS.MainDAO;
import com.tsp.operation.model.beans.Usuario;




public class MovAuxiliarDAO extends MainDAO{






    public MovAuxiliarDAO() {
        super("MovAuxiliarDAO.xml");
    }
    public MovAuxiliarDAO(String dataBaseName) {
        super("MovAuxiliarDAO.xml", dataBaseName);
    }



    /**
     * M�todos que setea valores nulos
     * @autor.......fvillacob
     * @version.....1.0.
     **/
       private String reset(String val){
            if(val==null)
               val = "";
            return val;
       }








       /**
       * M�todo que busca las cuentas dentro del rango
       * @autor.......fvillacob
       * @throws......Exception
       * @version.....1.0.
       **/
      public  List getCuentasRango(String distrito, String cuentaIni, String cuentaFin) throws Exception{
            Connection con = null;
            PreparedStatement  st      = null;
            ResultSet          rs      = null;
            List               lista   = new LinkedList();
            String             query   = "SQL_RANGO_CUENTAS";
            try{

                int longC1  = cuentaIni.length();
                int longC2  = cuentaFin.length();
                int lon     = ( longC1 < longC2 )?longC1:longC2;

                con = this.conectarJNDI(query);
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, distrito  );
                st.setInt   (2, lon       );
                st.setString(3, cuentaIni );
                st.setString(4, cuentaFin );
                rs = st.executeQuery();

                while (rs.next()){
                    String cuenta =  rs.getString("cuenta");

                    List detalle  = getCuentasDetalle(distrito, cuenta);

                    if(detalle.size()>0)
                        lista.addAll(detalle);
                }

            }catch(Exception e){
                  throw new Exception( "getCuentasRango " + e.getMessage());
            }
finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
            return lista;
      }



      /**
       * M�todo que busca las cuentas  de detalle de la cuenta dada
       * @autor.......fvillacob
       * @throws......Exception
       * @version.....1.0.
       **/
      private  List getCuentasDetalle(String distrito, String cuenta ) throws Exception{
            Connection con = null;
            PreparedStatement  st      = null;
            ResultSet          rs      = null;
            List               lista   = new LinkedList();
            String             query   = "SQL_CUENTAS_DET_PERTENECIENTE_CUENTA";
            try{
                con = this.conectarJNDI(query);
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, distrito  );
                st.setString(2, cuenta    );
                rs = st.executeQuery();

                while (rs.next()){
                    Hashtable  cuenteDetalle = this.load(rs);
                    lista.add( cuenteDetalle );
                    cuenteDetalle = null;//Liberar Espacio JJCastro
                }

            }catch(Exception e){
                  throw new Exception( "getCuentasDetalle " + e.getMessage());
            }
finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
            return lista;
      }




       /**
       * M�todo que obtiene informaci�n de la cuenta
       * @autor.......fvillacob
       * @throws......Exception
       * @version.....1.0.
       **/
      public  Hashtable getCuenta(String distrito, String cuenta ) throws Exception{
            Connection con = null;
            PreparedStatement  st            = null;
            ResultSet          rs            = null;
            Hashtable          cuenteDetalle = null;
            String             query         = "SQL_CUENTA";
            try{
                con = this.conectarJNDI(query);
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, distrito  );
                st.setString(2, cuenta    );
                rs = st.executeQuery();
                if( rs.next())
                    cuenteDetalle  = this.load(rs);

            }catch(Exception e){
                  throw new Exception( "getCuenta " + e.getMessage());
            }
finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
            return cuenteDetalle;
      }




      /**
       * M�todo que carga los datos obtenidos del sql
       * @autor.......fvillacob
       * @throws......Exception
       * @version.....1.0.
       **/
      private Hashtable load(ResultSet  rs)throws Exception{
            Hashtable   cuenta = new  Hashtable();
            try{

                cuenta.put("distrito",           reset( rs.getString("dstrct") )           );
                cuenta.put("cuenta",             reset( rs.getString("cuenta") )           );
                cuenta.put("nombre_largo",       reset( rs.getString("nombre_largo") )     );
                cuenta.put("nombre_corto",       reset( rs.getString("nombre_corto") )     );
                cuenta.put("nombre_observacion", reset( rs.getString("nombre_observacion") ));
                cuenta.put("fin_periodo",        reset( rs.getString("fin_periodo") )      );
                cuenta.put("auxiliar",           reset( rs.getString("auxiliar") )         );
                cuenta.put("activa",             reset( rs.getString("activa") )           );
                cuenta.put("base",               reset( rs.getString("base") )             );
                cuenta.put("padre",              reset( rs.getString("cta_dependiente") )  );
                cuenta.put("nivel",              reset( rs.getString("nivel") )            );
                cuenta.put("cta_cierre",         reset( rs.getString("cta_cierre") )       );
                cuenta.put("subledger",          reset( rs.getString("subledger") )        );
                cuenta.put("tercero",            reset( rs.getString("tercero") )          );

            }catch(Exception e){
                  throw new Exception( "load " + e.getMessage());
            }
            return cuenta;
      }





      /**
       * M�todo que determina si la cuenta es del auxiliar o no
       * @autor.......fvillacob
       * @throws......Exception
       * @version.....1.0.
       **/
      public  boolean isDelAuxiliar(String distrito, String cuenta,  String tipoAux, String auxiliar  ) throws Exception{
            Connection con = null;
            PreparedStatement  st      = null;
            ResultSet          rs      = null;
            boolean            estado  = false;
            String             query   = "SQL_CUENTA_PERTENECE_AUX";
            try{

                con = this.conectarJNDI(query);
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, distrito  );
                st.setString(2, cuenta    );
                st.setString(3, tipoAux   );
                st.setString(4, auxiliar  );
                rs = st.executeQuery();
                if(rs.next())
                    estado = true;

            }catch(Exception e){
                  throw new Exception( "isDelAuxiliar " + e.getMessage());
            }
finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
            return estado;
      }



      /**
       * M�todo que busca las cuentas  de detalle de la cuenta dada
       * @autor.......fvillacob
       * @throws......Exception
       * @version.....1.0.
       **/
      public  List getMovimiento(String distrito, String cuenta, String fechaIni, String fechaFin, String tipoAux, String auxiliar  ) throws Exception{
            Connection con = null;
            PreparedStatement  st      = null;
            ResultSet          rs      = null;
            List               lista   = new LinkedList();
            String             query   = "SQL_MOVIMIENTOS";

            try{

                con = this.conectarJNDI(query);

                String  newTipoAux  = (tipoAux.equals (""))?"like '%'" : "='"+ tipoAux   +"'";
                String  newAuxiliar = (auxiliar.equals(""))?"like '%'" : "='"+ auxiliar  +"'";
                String sql = this.obtenerSQL(query).replaceAll("#AUXILIAR#",newAuxiliar).replaceAll("#TIPO#",newTipoAux);

                st = con.prepareStatement(sql);//JJCastro fase2
                st.setString(1, distrito  );
                st.setString(2, cuenta    );
                st.setString(3, fechaIni  );
                st.setString(4, fechaFin  );

                rs = st.executeQuery();

                while (rs.next()){
                    Hashtable  mov = new Hashtable();
                        mov.put("distrito",           reset(  rs.getString("distrito")    ) );
                        mov.put("tipo_doc",           reset(  rs.getString("tipo_doc")    ) );
                        mov.put("documento",          reset(  rs.getString("documento")   ) );
                        mov.put("fecha",              reset(  rs.getString("fecha")       ) );
                        mov.put("periodo",            reset(  rs.getString("periodo")     ) );
                        mov.put("descripcion",        reset(  rs.getString("descripcion") ) );
                        mov.put("tipo_aux",           reset(  rs.getString("tipo_aux")    ) );
                        mov.put("auxiliar",           reset(  rs.getString("auxiliar")    ) );
                        mov.put("tercero",            reset(  rs.getString("tercero")     ) );
                        mov.put("nombre",             reset(  rs.getString("nombre")      ) );
                        mov.put("vlrDebito",          reset(  rs.getString("vlrDebito")   ) );
                        mov.put("vlrCredito",         reset(  rs.getString("vlrCredito")  ) );
                        mov.put("grupo_transaccion",  reset(  rs.getString("grupo_transaccion")  ) );

                        double saldo     =   rs.getDouble("vlrDebito")  - rs.getDouble("vlrCredito");
                        mov.put("vlrSaldo",  String.valueOf(  saldo  )  );


                    lista.add( mov );
                    mov = null;//Liberar Espacio JJCastro
                }

            }catch(Exception e){
                  throw new Exception( "getMovimiento " + e.getMessage());
            }
finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
            return lista;
      }




      /**
       * M�todo que busca el saldo del a�o anterior de una cuenta en la tabla de mayorizaci�n
       * @autor.......fvillacob
       * @throws......Exception
       * @version.....1.0.
       **/
      public  double saldoA�oAnterior_Mayorizacion(String distrito, String cuenta,  String ano ) throws Exception{
          Connection con = null;
          PreparedStatement st = null;
          ResultSet rs = null;
          String query = "SALDO_ANO_ANT_MAYOR";
          double valor = 0;
          try {
              con = this.conectarJNDI(query);
              st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
              st.setString(1, distrito);
              st.setString(2, cuenta);
              st.setString(3, ano);
              rs = st.executeQuery();
              if (rs.next()) {
                  valor = rs.getDouble("saldoant");
              }

          } catch (Exception e) {
              throw new Exception("saldoA�oAnterior_Mayorizacion " + e.getMessage());
          }
finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
            return valor;
      }






      /**
       * M�todo que busca el saldo de meses anteriores de la cuenta en la tabla mayorizacion
       * @autor.......fvillacob
       * @throws......Exception
       * @version.....1.0.
       **/
      public  double  saldoMes_Mayorizacion(String distrito, String cuenta,  String ano, String mes  ) throws Exception{
          PreparedStatement st = null;
          ResultSet rs = null;
          String query = "SALDO_MESES_MAYOR";
          Connection con = null;
          double vlrDebito = 0;
          double vlrCredito = 0;
          double neto = 0;
          try {

              con = this.conectarJNDI(query);//JJCastro fase2
              int Mes = Integer.parseInt(mes);

              for (int i = 1; i < Mes; i++) {
                  String mesBusqueda = Util.mesFormat(i);

                  String sql = this.obtenerSQL(query).replaceAll("#MES#", mesBusqueda);
                  st = con.prepareStatement(sql);
                  st.setString(1, distrito);
                  st.setString(2, cuenta);
                  st.setString(3, ano);
                  rs = st.executeQuery();
                  if (rs.next()) {
                      vlrDebito += rs.getDouble("debito");
                      vlrCredito += rs.getDouble("credito");
                  }

              }

              neto = vlrDebito - vlrCredito;


          } catch (Exception e) {
              throw new Exception("saldoMes_Mayorizacion " + e.getMessage());
          }
finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
            return neto;
      }




      /**
       * M�todo que busca el saldo del a�o anterior de una cuenta para un subledger en la tabla de mayorizaci�n_subledger
       * @autor.......fvillacob
       * @throws......Exception
       * @version.....1.0.
       **/
      public  double saldoA�oAnterior_Mayorizacion_Subledger(String distrito, String cuenta, String auxiliar, String ano  ) throws Exception{
          Connection con = null;
          PreparedStatement  st      = null;
            ResultSet          rs      = null;
            String             query   = "SALDO_ANO_ANT_MAYOR_SUBLEDGER";
            double             valor   = 0;
            try{

                con = this.conectarJNDI(query);
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, distrito  );
                st.setString(2, cuenta    );
                st.setString(3, auxiliar  );
                st.setString(4, ano       );
                rs = st.executeQuery();
                if(rs.next())
                    valor = rs.getDouble("saldoant");

            }catch(Exception e){
                  throw new Exception( "saldoA�oAnterior_Mayorizacion_Subledger " + e.getMessage());
            }
finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
            return valor;
      }


      /**
       * M�todo que busca el saldo de meses anteriores de la cuenta  para un subledger en la tabla mayorizacion_subledger
       * @autor.......fvillacob
       * @throws......Exception
       * @version.....1.0.
       **/
      public  double  saldoMes_Mayorizacion_Subledger(String distrito, String cuenta, String auxiliar, String ano, String mes  ) throws Exception{
          PreparedStatement st = null;
          ResultSet rs = null;
          String query = "SALDO_MESES_MAYOR_SUBLEDGER";
          Connection con = null;
          double vlrDebito = 0;
          double vlrCredito = 0;
          double neto = 0;
          try {

              con = this.conectarJNDI(query);//JJCastro fase2
              int Mes = Integer.parseInt(mes);

              for (int i = 1; i < Mes; i++) {
                  String mesBusqueda = Util.mesFormat(i);

                  String sql = this.obtenerSQL(query).replaceAll("#MES#", mesBusqueda);
                  st = con.prepareStatement(sql);
                  st.setString(1, distrito);
                  st.setString(2, cuenta);
                  st.setString(3, auxiliar);
                  st.setString(4, ano);
                  rs = st.executeQuery();
                  if (rs.next()) {
                      vlrDebito += rs.getDouble("debito");
                      vlrCredito += rs.getDouble("credito");
                  }

              }

              neto = vlrDebito - vlrCredito;


          } catch (Exception e) {
              throw new Exception("saldoMes_Mayorizacion_Subledger " + e.getMessage());
          }
finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
            return neto;
      }





      /**
       * M�todo que busca el nombre del auxiliar
       * @autor.......fvillacob
       * @throws......Exception
       * @version.....1.0.
       **/
      public  String  nombreAuxiliar (String tipoAux, String auxiliar  ) throws Exception{
          Connection con = null;
          PreparedStatement st = null;
          ResultSet rs = null;
          String query = "SQL_NOMBRE_AUXILIAR";
          String name = "";
          try {
              con = this.conectarJNDI(query);
              st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
              st.setString(1, tipoAux);
              st.setString(2, auxiliar);
              rs = st.executeQuery();
              if (rs.next()) {
                  name = rs.getString("nombre");
              }

          } catch (Exception e) {
              throw new Exception("nombreAuxiliar " + e.getMessage());
          }
finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
            return name;
      }






       /**
       * M�todo que busca los distintos tipo de auxiliar
       * @autor.......fvillacob
       * @throws......Exception
       * @version.....1.0.
       **/
      public  List getTiposAuxiliar() throws Exception{
          Connection con = null;
          PreparedStatement st = null;
          ResultSet rs = null;
          List lista = new LinkedList();
          String query = "SQL_TIPO_AUXILIAR";
          try {
              con = this.conectarJNDI(query);
              st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
              rs = st.executeQuery();
              while (rs.next()) {
                  String code = rs.getString("table_code");
                  String desc = rs.getString("descripcion");
                  Hashtable tipo = new Hashtable();
                  tipo.put("codigo", code);
                  tipo.put("descripcion", desc);

                  lista.add(tipo);
                  tipo = null;//Liberar Espacio JJCastro
              }


          } catch (Exception e) {
              throw new Exception("getTiposAuxiliar " + e.getMessage());
          }
finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
            return lista;
      }







       /**
       * M�todo que busca las cuentas para formar el Treeview
       * @autor.......fvillacob
       * @throws......Exception
       * @version.....1.0.
       **/
      public  List getCuentasTreeView(String distrito) throws Exception{
            Connection con = null;
            PreparedStatement  st      = null;
            ResultSet          rs      = null;
            List               lista   = new LinkedList();
            String             query   = "SQL_CUENTAS_FOR_TREEVIEW";
            try{
                con = this.conectarJNDI(query);
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, distrito);
                rs = st.executeQuery();
                while (rs.next()){
                   Hashtable opcion = new Hashtable();
                      opcion.put("item" ,        reset(rs.getString("item")         )  );
                      opcion.put("nombreitem" ,  reset(rs.getString("nombreitem")   )  );
                      opcion.put("folder" ,      reset(rs.getString("folder")       )  );
                      opcion.put("padre" ,       reset(rs.getString("padre")        )  );
                      opcion.put("nombrePadre" , reset(rs.getString("nombrePadre")  )  );
                      opcion.put("nivel" ,       reset(rs.getString("nivel")        )  );

                      if( rs.getString("nivel").equals("1")  ){
                          opcion.put("padre" ,       "menu"   );
                      }

                   lista.add(opcion);
                   opcion  = null;//Liberar Espacio JJCastro
                }


            }catch(Exception e){
                  throw new Exception( "getCuentasTreeView " + e.getMessage());
            }
finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
            return lista;
      }

      /**
     * M�todo que obtiene los los datos de los movimientos auxiliares
     * segun los parametros establecidos
     * @autor: Mario Fontalvo
     * @param cuenta_ri, rango inicial de la cuenta
     * @param cuenta_rf, rango final de la cuenta
     * @param fecha_ri, rango inicial de la fecha
     * @param fecha_ri, rango final de la fecha
     * @throws Exception.
     */
    public Vector obtenerCuentas(Usuario usuario,String dstrct, String cuenta_ri, String cuenta_rf, String fecha_ri, String fecha_rf, String cuenta, String periodo) throws Exception {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_MOVIMENTO_AUXILIAR";
        Vector datos = new Vector();
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));

            if (con == null) {
                throw new Exception("SIN CONEXION.");
            }


            String sql = this.obtenerSQL(query);

            // fechas
//            if (periodo.equals("")) {
//                sql = sql.replace("#FILTROFECHAS#", " AND b.fechadoc BETWEEN ? AND ? ");
//            } else {
//                sql = sql.replace("#FILTROFECHAS#", "AND b.periodo = ? ");
//            }
//
//            // cuentas
//            if (cuenta.equals("")) {
//                sql = sql.replace("#FILTROCUENTAS#", " AND a.cuenta   BETWEEN ? AND ? ");
//            } else {
//                //sql = sql.replace("#FILTROCUENTAS#", " AND a.cuenta  LIKE ? ");
//                 sql = sql.replace("#FILTROCUENTAS#", " AND a.cuenta = ? ");
//            }
//
//
//
//            int i = 1;
//            st = con.prepareStatement(sql);
//            st.setString(i++, dstrct);
//
//            // parametros de la cuenta
//            if (cuenta.equals("")) {
//                st.setString(i++, cuenta_ri);
//                st.setString(i++, cuenta_rf);
//            } else {
//                st.setString(i++, cuenta);
//            }
//
//            // parametros del periodo
//            if (periodo.equals("")) {
//                st.setString(i++, fecha_ri);
//                st.setString(i++, fecha_rf);
//            } else {
//                st.setString(i++, periodo);
//            }

            st.setString(1, usuario.getEmpresa());
            st.setString(2, dstrct);
            st.setString(3, periodo);
            st.setString(4, cuenta);
            st.setString(5, cuenta_ri);
            st.setString(6, cuenta_rf);
            st.setString(7, fecha_ri);
            st.setString(8, fecha_rf);

            rs = st.executeQuery();
            while (rs.next()) {
                ComprobanteFacturas c = new ComprobanteFacturas();
                c.setDstrct(Util.coalesce(rs.getString("dstrct"), ""));
                c.setCuenta(Util.coalesce(rs.getString("cuenta"), ""));
                c.setAuxiliar(Util.coalesce(rs.getString("auxiliar"), ""));
                c.setPeriodo(Util.coalesce(rs.getString("periodo"), ""));
                c.setFechadoc(Util.coalesce(rs.getString("fechadoc"), ""));
                c.setTipodoc(Util.coalesce(rs.getString("tipodoc_desc"), ""));
                c.setTipo(Util.coalesce(rs.getString("tipodoc"), ""));
                c.setNumdoc(Util.coalesce(rs.getString("numdoc"), ""));
                c.setDetalle(Util.coalesce(rs.getString("detalle"), ""));
                c.setAbc(Util.coalesce(rs.getString("abc"), ""));
                c.setTotal_debito(rs.getDouble("valor_debito"));
                c.setTotal_credito(rs.getDouble("valor_credito"));
                c.setTercero(Util.coalesce(rs.getString("tercero"), ""));
                c.setNombre_tercero(Util.coalesce(rs.getString("nombre_tercero"), ""));
                c.setTipodoc_rel(Util.coalesce(rs.getString("tipodoc_rel"), ""));
                c.setNumdoc_rel(Util.coalesce(rs.getString("documento_rel"), ""));
                c.setMoneda_foranea(Util.coalesce(rs.getString("modena_foranea"), ""));
                c.setVlr_item_for(rs.getDouble("vlr_for"));

                c.setTipo_referencia_1(rs.getString("tipo_referencia_1"));
                c.setReferencia_1(rs.getString("referencia_1"));
                c.setTipo_referencia_2(rs.getString("tipo_referencia_2"));
                c.setReferencia_2(rs.getString("referencia_2"));
                c.setTipo_referencia_3(rs.getString("tipo_referencia_3"));
                c.setReferencia_3(rs.getString("referencia_3"));
                //nuevo campo para el reporte de libro auxiliar
                c.setDocumento_rel2(rs.getString("documento_rel2"));
                c.setMultiservicio(rs.getString("multiservicio"));

                datos.add(c);
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(e.getMessage());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (st != null) {
                st.close();
            }
            this.desconectar(con);
        }
        return datos;
    }




}