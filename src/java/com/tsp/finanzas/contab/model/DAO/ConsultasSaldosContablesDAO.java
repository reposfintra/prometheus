/***********************************************************************
* Nombre ......................ConsultasSaldosContablesDAO.java        *
* Descripci�n..................Clase DAO para el plan de cuentas       *
* Autor........................LREALES                                 *
* Fecha Creaci�n...............12 de junio de 2006, 09:32 AM           *
* Versi�n......................1.0                                     *
* Coyright.....................Transportes Sanchez Polo S.A.           *
***********************************************************************/

package com.tsp.finanzas.contab.model.DAO;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.*;
import com.tsp.finanzas.contab.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;
import com.tsp.operation.model.DAOS.MainDAO;

public class ConsultasSaldosContablesDAO extends MainDAO {
    
    private SaldosContables saldos;
    private Vector vec_saldos;
    
    /** Creates a new instance of ConsultasSaldosContablesDAO */
    public ConsultasSaldosContablesDAO () {
        super ( "ConsultasSaldosContablesDAO.xml" );
    }
   public ConsultasSaldosContablesDAO (String dataBaseName) {
        super ( "ConsultasSaldosContablesDAO.xml", dataBaseName);
    }
    
    /**
     * Getter for property saldos.
     * @return Value of property saldos.
     */
    public SaldosContables getSaldos() {
        
        return saldos;
        
    }
    
    /**
     * Setter for property saldos.
     * @param cuentas New value of property saldos.
     */
    public void setSaldos( SaldosContables saldos ) {
        
        this.saldos = saldos;
        
    }
    
    /**
     * Getter for property vec_saldos.
     * @return Value of property vec_saldos.
     */
    public java.util.Vector getVec_saldos() {
        
        return vec_saldos;
        
    }
    
    /**
     * Setter for property vec_saldos.
     * @param vec_cuentas New value of property vec_saldos.
     */
    public void setVec_saldos( java.util.Vector vec_saldos ) {
        
        this.vec_saldos = vec_saldos;
        
    }
    
    /**
     * Metodo:          listaIncluyeSubledger
     * Descripcion :    M�todo que permite listar todos los saldos contables.
     *                  Incluyendo Subledgers!!
     * @autor :         LREALES
     * @param:          el distrito, la fecha inicial, la fecha final, la cuenta inicial y la cuenta final.
     * @return:         -
     * @throws:         SQLException
     * @version :       1.0
     */
    public void listaIncluyeSubledger ( String distrito, String anio_ini, String anio_fin, String cuenta_ini, String cuenta_fin ) throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_LISTA_INCLUYE_SUBLEDGER";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString ( 1, distrito );
            st.setString ( 2, anio_ini );
            st.setString ( 3, anio_fin );
            st.setString ( 4, cuenta_ini );
            st.setString ( 5, cuenta_fin );
            st.setString ( 6, distrito );
            st.setString ( 7, anio_ini );
            st.setString ( 8, anio_fin );
            st.setString ( 9, cuenta_ini );
            st.setString ( 10, cuenta_fin );
            rs= st.executeQuery ();
            vec_saldos = new Vector ();
            
            while( rs.next () ){
                saldos = new SaldosContables ();
                // seteo
                saldos.setNombre_corto( ( rs.getString("nombre_corto") != null )?rs.getString("nombre_corto"):"" );
                saldos.setCuenta( ( rs.getString("cuenta") != null )?rs.getString("cuenta"):"" );
                saldos.setAnio( ( rs.getString("anio") != null )?rs.getString("anio"):"" );
                saldos.setSaldoant( rs.getDouble("saldoant") );
                saldos.setMovdeb01( rs.getDouble("movdeb01") );
                saldos.setMovcre01( rs.getDouble("movcre01") );
                saldos.setFindemes01( saldos.getSaldoant() + saldos.getMovdeb01() - saldos.getMovcre01() );
                saldos.setMovdeb02( rs.getDouble("movdeb02") );
                saldos.setMovcre02( rs.getDouble("movcre02") );
                saldos.setFindemes02( saldos.getFindemes01() + saldos.getMovdeb02() - saldos.getMovcre02() );
                saldos.setMovdeb03( rs.getDouble("movdeb03") );
                saldos.setMovcre03( rs.getDouble("movcre03") );
                saldos.setFindemes03( saldos.getFindemes02() + saldos.getMovdeb03() - saldos.getMovcre03() );
                saldos.setMovdeb04( rs.getDouble("movdeb04") );
                saldos.setMovcre04( rs.getDouble("movcre04") );
                saldos.setFindemes04( saldos.getFindemes03() + saldos.getMovdeb04() - saldos.getMovcre04() );
                saldos.setMovdeb05( rs.getDouble("movdeb05") );
                saldos.setMovcre05( rs.getDouble("movcre05") );
                saldos.setFindemes05( saldos.getFindemes04() + saldos.getMovdeb05() - saldos.getMovcre05() );
                saldos.setMovdeb06( rs.getDouble("movdeb06") );
                saldos.setMovcre06( rs.getDouble("movcre06") );
                saldos.setFindemes06( saldos.getFindemes05() + saldos.getMovdeb06() - saldos.getMovcre06() );
                saldos.setMovdeb07( rs.getDouble("movdeb07") );
                saldos.setMovcre07( rs.getDouble("movcre07") );
                saldos.setFindemes07( saldos.getFindemes06() + saldos.getMovdeb07() - saldos.getMovcre07() );
                saldos.setMovdeb08( rs.getDouble("movdeb08") );
                saldos.setMovcre08( rs.getDouble("movcre08") );
                saldos.setFindemes08( saldos.getFindemes07() + saldos.getMovdeb08() - saldos.getMovcre08() );
                saldos.setMovdeb09( rs.getDouble("movdeb09") );
                saldos.setMovcre09( rs.getDouble("movcre09") );
                saldos.setFindemes09( saldos.getFindemes08() + saldos.getMovdeb09() - saldos.getMovcre09() );
                saldos.setMovdeb10( rs.getDouble("movdeb10") );
                saldos.setMovcre10( rs.getDouble("movcre10") );
                saldos.setFindemes10( saldos.getFindemes09() + saldos.getMovdeb10() - saldos.getMovcre10() );
                saldos.setMovdeb11( rs.getDouble("movdeb11") );
                saldos.setMovcre11( rs.getDouble("movcre11") );
                saldos.setFindemes11( saldos.getFindemes10() + saldos.getMovdeb11() - saldos.getMovcre11() );
                saldos.setMovdeb12( rs.getDouble("movdeb12") );
                saldos.setMovcre12( rs.getDouble("movcre12") );
                saldos.setFindemes12( saldos.getFindemes11() + saldos.getMovdeb12() - saldos.getMovcre12() );
                saldos.setSaldoact( rs.getDouble("saldoact") );
                vec_saldos.add ( saldos );
            }            
            
        } catch( SQLException e ){
            e.printStackTrace();
            throw new SQLException( "ERROR DURANTE 'listaIncluyeSubledger' - [ConsultasSaldosContablesDAO].. " + e.getMessage() + " " + e.getErrorCode() );
        }
finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        
    }
   
    /**
     * Metodo:          listaSaldosContables
     * Descripcion :    M�todo que permite listar todos los saldos contables.
     * @autor :         LREALES
     * @param:          el distrito, la fecha inicial, la fecha final, la cuenta inicial y la cuenta final.
     * @return:         -
     * @throws:         SQLException
     * @version :       1.0
     */
    public void listaSaldosContables ( String distrito, String anio_ini, String anio_fin, String cuenta_ini, String cuenta_fin ) throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_LISTA";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString ( 1, distrito );
            st.setString ( 2, anio_ini );
            st.setString ( 3, anio_fin );
            st.setString ( 4, cuenta_ini );
            st.setString ( 5, cuenta_fin );
            
            rs= st.executeQuery ();
            
            vec_saldos = new Vector ();
            
            while( rs.next () ){
                
                saldos = new SaldosContables ();
                // seteo
                saldos.setNombre_corto( ( rs.getString("nombre_corto") != null )?rs.getString("nombre_corto"):"" );
                
                saldos.setCuenta( ( rs.getString("cuenta") != null )?rs.getString("cuenta"):"" );
                saldos.setAnio( ( rs.getString("anio") != null )?rs.getString("anio"):"" );
                saldos.setSaldoant( rs.getDouble("saldoant") );
                saldos.setMovdeb01( rs.getDouble("movdeb01") );
                saldos.setMovcre01( rs.getDouble("movcre01") );
                saldos.setFindemes01( saldos.getSaldoant() + saldos.getMovdeb01() - saldos.getMovcre01() );
                saldos.setMovdeb02( rs.getDouble("movdeb02") );
                saldos.setMovcre02( rs.getDouble("movcre02") );
                saldos.setFindemes02( saldos.getFindemes01() + saldos.getMovdeb02() - saldos.getMovcre02() );
                saldos.setMovdeb03( rs.getDouble("movdeb03") );
                saldos.setMovcre03( rs.getDouble("movcre03") );
                saldos.setFindemes03( saldos.getFindemes02() + saldos.getMovdeb03() - saldos.getMovcre03() );
                saldos.setMovdeb04( rs.getDouble("movdeb04") );
                saldos.setMovcre04( rs.getDouble("movcre04") );
                saldos.setFindemes04( saldos.getFindemes03() + saldos.getMovdeb04() - saldos.getMovcre04() );
                saldos.setMovdeb05( rs.getDouble("movdeb05") );
                saldos.setMovcre05( rs.getDouble("movcre05") );
                saldos.setFindemes05( saldos.getFindemes04() + saldos.getMovdeb05() - saldos.getMovcre05() );
                saldos.setMovdeb06( rs.getDouble("movdeb06") );
                saldos.setMovcre06( rs.getDouble("movcre06") );
                saldos.setFindemes06( saldos.getFindemes05() + saldos.getMovdeb06() - saldos.getMovcre06() );
                saldos.setMovdeb07( rs.getDouble("movdeb07") );
                saldos.setMovcre07( rs.getDouble("movcre07") );
                saldos.setFindemes07( saldos.getFindemes06() + saldos.getMovdeb07() - saldos.getMovcre07() );
                saldos.setMovdeb08( rs.getDouble("movdeb08") );
                saldos.setMovcre08( rs.getDouble("movcre08") );
                saldos.setFindemes08( saldos.getFindemes07() + saldos.getMovdeb08() - saldos.getMovcre08() );
                saldos.setMovdeb09( rs.getDouble("movdeb09") );
                saldos.setMovcre09( rs.getDouble("movcre09") );
                saldos.setFindemes09( saldos.getFindemes08() + saldos.getMovdeb09() - saldos.getMovcre09() );
                saldos.setMovdeb10( rs.getDouble("movdeb10") );
                saldos.setMovcre10( rs.getDouble("movcre10") );
                saldos.setFindemes10( saldos.getFindemes09() + saldos.getMovdeb10() - saldos.getMovcre10() );
                saldos.setMovdeb11( rs.getDouble("movdeb11") );
                saldos.setMovcre11( rs.getDouble("movcre11") );
                saldos.setFindemes11( saldos.getFindemes10() + saldos.getMovdeb11() - saldos.getMovcre11() );
                saldos.setMovdeb12( rs.getDouble("movdeb12") );
                saldos.setMovcre12( rs.getDouble("movcre12") );
                saldos.setFindemes12( saldos.getFindemes11() + saldos.getMovdeb12() - saldos.getMovcre12() );
                saldos.setSaldoact( rs.getDouble("saldoact") );
                
                vec_saldos.add ( saldos );
                
                
            }            
            
        } catch( SQLException e ){
            e.printStackTrace();
            throw new SQLException( "ERROR DURANTE 'listaSaldosContables' - [ConsultasSaldosContablesDAO].. " + e.getMessage() + " " + e.getErrorCode() );
            
        }
finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        
    }
    
}