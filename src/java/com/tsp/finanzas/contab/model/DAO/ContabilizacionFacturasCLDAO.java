/***************************************
 * Nombre Clase ............. ContabilizacionFacturasCLDAO.java
 * Descripci�n  .. . . . . .  Permitimos realizar consultas para la contabilizacion de facturas de clientes
 * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
 * Fecha . . . . . . . . . .  10/07/2006
 * versi�n . . . . . . . . .  1.0
 * Copyright ...............  Transportes Sanchez Polo S.A.
 *******************************************/




package com.tsp.finanzas.contab.model.DAO;




import java.io.*;
import java.util.*;
import java.sql.*;
import com.tsp.finanzas.contab.model.beans.*;
import com.tsp.util.*;
import com.tsp.finanzas.contab.model.beans.ComprobanteFacturas;
import com.tsp.finanzas.contab.model.DAO.ContabilizacionFacturasDAO;
import com.tsp.operation.model.DAOS.MainDAO;




public class ContabilizacionFacturasCLDAO extends MainDAO{
    
    
    
    
    private String APROBADOR        = "ADMIN";
    private String PRE_COMPROBANTE  = "CONTABILIZACION CLIENTE";
    private String OF_PPAL          = "OP";
    private String AUX_COMPROBANTE  = "";
    public  String CABECERA         = "C";
    public  String DETALLE          = "D";
    
    
    
    
    
    public ContabilizacionFacturasCLDAO() {
        super("ContabilizacionFacturasCLDAO.xml");
    }
    
    public ContabilizacionFacturasCLDAO(String dataBaseName) {
        super("ContabilizacionFacturasCLDAO.xml", dataBaseName);
    }
    
    
     /**
     * M�todos que setea valores nulos
     * @autor.......fvillacob
     * @version.....1.0.
     **/
    private String reset(String val){
        if(val==null)
            val = "";
        return val;
    }
    
    
    
    
    
    /**
     * M�todo que busca la cta del ingreso a la remesa
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public  Hashtable getCostoReembolsable(String distrito, String ot, String cta ) throws Exception{
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_REEMBOLSABLE";
        Hashtable info = null;
        Connection con = null;
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, distrito);
            st.setString(2, ot);
            st.setString(3, cta);
            rs = st.executeQuery();
            if (rs.next()) {
                info = new Hashtable();
                info.put("valor", reset(rs.getString("vlr_costo")));
                info.put("cta1", reset(rs.getString("cuenta1")));
                info.put("cta2", reset(rs.getString("cuenta2")));
                info.put("nit", reset(rs.getString("nit")));
                info.put("ctaIng", reset(rs.getString("ctaING")));
            }


        } catch (Exception e) {
            throw new Exception("getCostoReembolsable " + e.getMessage());
        }
finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return info;
    }
    
    
    
    
    /**
     * M�todo que busca la cta del ingreso a la remesa
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public  String getAccoundCodeI(String ot) throws Exception{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_ACCOUNT_CODE_I";
        String cta = "";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, ot);
            rs = st.executeQuery();
            if (rs.next()) {
                cta = reset(rs.getString("account_code_i"));
            }

        } catch (Exception e) {
            throw new Exception("getAccoundCodeI " + e.getMessage());
        }
finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return cta;
    }
    
    
    
    
    //_________________________________________________________________________________________________
    // FACTURAS   
    //_________________________________________________________________________________________________
    
    
    
    /**
     * M�todo que busca las facturas de clientes que no esten contabilizadas
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public  List getFacturasClientesNoContabilizadas(String distrito, String ayer) throws Exception{
        Connection con = null;
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        List               lista   = new LinkedList();
        String             query   = "SQL_FACTURAS_CLIENTES_NO_CONTABILIZADAS";
        try{
            
            ContabilizacionFacturasDAO  dao = new ContabilizacionFacturasDAO(this.getDatabaseName());
            String moneda = dao.getMoneda(distrito);

            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, distrito  );
            st.setString(2, ayer      );
            //System.out.println("Q- "+st.toString());
            rs = st.executeQuery();
            while (rs.next()){
                ComprobanteFacturas  comprobante = loadComprobantes(rs);
                comprobante.setMoneda( moneda );                
                lista.add( comprobante );
                comprobante = null;//Liberar Espacio JJCastro
            }
            
        }catch(Exception e){
            throw new Exception( "getFacturasClientesNoContabilizadas " + e.getMessage());
        }
finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return lista;
    }
    
    
    
    
    
 /**
     * M�todo que carga los datos de la factura @autor.......fvillacob
     *
     * @throws......Exception
     * @version.....1.0.
     *
     */
    public ComprobanteFacturas loadComprobantes(ResultSet rs) throws Exception {
        ComprobanteFacturas comp = new ComprobanteFacturas();
        try {

            comp.setDstrct(reset(rs.getString("dstrct")));
            comp.setTipodoc(reset(rs.getString("sigla_comprobante")));  // Tipo del comprobante
            comp.setTercero(reset(rs.getString("tercero")));
            comp.setNumdoc(reset(rs.getString("numdoc")));
            comp.setTipodoc_factura(reset(rs.getString("tipo_doc")));  // Tipo de tabla factura
            comp.setBase(reset(rs.getString("base")));
            comp.setSucursal(OF_PPAL);
            comp.setAprobador(APROBADOR);
            comp.setMonedaFac(reset(rs.getString("moneda")));
            comp.setFechaFactura(reset(rs.getString("fecha_documento")));
            comp.setHc(reset(rs.getString("hc")));
            comp.setCuenta(reset(rs.getString("cuenta")));
            comp.setCmc(reset(rs.getString("dbcr")));
            comp.setRef_3(reset(rs.getString("descrip")));
            comp.setDetalle(rs.getString("proveedor"));
            comp.setTercero(rs.getString("nitter"));
            comp.setAuxiliar(AUX_COMPROBANTE);
            comp.setDocumento_interno(comp.getTipodoc_factura());
            comp.setTipo(CABECERA);
            comp.setOc("");
            comp.setOrigen("C");   // Clientes 
            comp.setClaseFactura("4");
            comp.setComentario(varificarDatosComprobante(rs));
            comp.setTipo_referencia_1(rs.getString("tipo_referencia_1"));
            comp.setReferencia_1(reset(rs.getString("referencia_1")));

        } catch (Exception e) {
            throw new Exception("loadComprobantes " + e.getMessage());
        }
        return comp;
    }

    public String varificarDatosComprobante(ResultSet rs) throws Exception {
        String mensaje = "";
        try {

            mensaje += (reset(rs.getString("hc")).equals("") ? " El handle code del documento no esta configurado." : "");

            if (rs.getString("sigla_comprobante") != null) {
                mensaje += (rs.getString("sigla_comprobante").equals("") ? " El tipo de comprobante para el hadle code no esta configurado." : "");
            } else {
                mensaje += " El tipo de comprobante para el hadle code no existe.";
            }

            if (rs.getString("cuenta") != null) {
                mensaje += (rs.getString("cuenta").equals("") ? " La cuenta para el hadle code no esta configurada." : "");
            } else {
                mensaje += " La cuenta para el hadle code no existe.";
            }

            mensaje += (reset(rs.getString("proveedor")).equals("") ? " El documento no presenta nombre de proveedor." : "");
            mensaje += (reset(rs.getString("moneda")).equals("") ? " El documento no presenta moneda." : "");
            mensaje += (reset(rs.getString("nitter")).equals("") ? " No presenta nit de tercero." : "");

        } catch (Exception e) {
            throw new Exception("ERROR VERIFICANDO LOS DATOS DEL COMPROBANTE: varificarDatosComprobante() --> [ContabilizacionFacturasCLDAO]" + e.getMessage());
        }
        return mensaje;
    } 
    
    
    
    
   //_________________________________________________________________________________________________
    // DETALLE   
    //_________________________________________________________________________________________________
    
    
    
    
    /**
     * M�todo que busca los items de las facturas de cliente que no esten contabilizadas
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public  List getItems(ComprobanteFacturas  comprobante) throws Exception{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        List lista = new LinkedList();
        String query = "SQL_DETALLE_FACTURA_CLIENTES";
        try {

            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, comprobante.getDstrct());
            st.setString(2, comprobante.getTercero());
            st.setString(3, comprobante.getTipodoc_factura());
            st.setString(4, comprobante.getNumdoc());
            //System.out.println("QD "+st.toString());
            rs = st.executeQuery();
            while (rs.next()) {
                ComprobanteFacturas comprodet = loadComproDet(rs);
                comprodet.setTercero(comprobante.getTercero());
                lista.add(comprodet);
                comprodet = null;//Liberar Espacio JJCastro
            }

        } catch (Exception e) {
            throw new Exception("getItems " + e.getMessage());
        }
finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return lista;
    }
    
    
    
    
    
    /**
     * M�todo que carga los datos de la factura
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public  ComprobanteFacturas loadComproDet(ResultSet  rs)throws Exception{
        ComprobanteFacturas  comp  = new  ComprobanteFacturas();
        try{
            comp.setDstrct           (  reset( rs.getString("dstrct")    )      );
            comp.setTipodoc          (  reset( rs.getString("tipodoc_factura")  )      );            
            comp.setNumdoc           (  reset( rs.getString("numdoc")    )      );
            comp.setDetalle          (  reset( rs.getString("detalle")   )      );
            comp.setValor              ( Double.valueOf(rs.getString("valor")).doubleValue());
            comp.setCuenta           (  reset( rs.getString("cuenta")    )      );
            comp.setAuxiliar         (  reset( rs.getString("auxiliar")  )      );  
            comp.setTercero          (  reset( rs.getString("tercero")  )      );
            comp.setTipo_valor       (  reset( rs.getString("tipodoc")  )      );
            comp.setNoItem           (  reset( rs.getString("item")  )        );
            comp.setTipodoc_factura (reset( rs.getString("tipodoc_factura")  )      );
            comp.setCmc(reset(rs.getString("planilla2"))); //Mod TMolina 2008-09-15
            double  vlr        = comp.getValor();
            double  vlrDebito  = ( vlr>0 )? vlr      :  0 ;
            double  vlrCredito = ( vlr<0 )? vlr*-1      :  0 ;
            comp.setTotal_credito( vlrCredito );
            comp.setTotal_debito ( vlrDebito  );
            comp.setTdoc_rel(reset(rs.getString("tipodocRel")));
            comp.setNumdoc_rel(reset(rs.getString("documento_rel")));
            comp.setTipo_referencia_1(reset(rs.getString("tipo_referencia_1")));
            comp.setReferencia_1(reset(rs.getString("referencia_1")));
            comp.setTipo_referencia_2(reset(rs.getString("tipo_referencia_2")));
            comp.setReferencia_2(reset(rs.getString("referencia_2")));
            comp.setTipo_referencia_3(reset(rs.getString("tipo_referencia_3")));
            comp.setReferencia_3(reset(rs.getString("referencia_3")));
        }catch(Exception e){
            throw new Exception( "loadComprobantes " + e.getMessage());
        }
        return comp;
    }
    
    
    
    
    
    
    
    
     //_________________________________________________________________________________________________
    // UPDATE   
    //_________________________________________________________________________________________________
    
     
    /**
     * M�todo que actualiza tabla de facturas clientes
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public  String updateFacturaClientes(ComprobanteFacturas  comprobante, String user)throws Exception{
        Connection con = null;
        PreparedStatement  st      = null;
        String             query   = "SQL_UPDATE_FACTURAS_DATOS_CONTABLES";
        String             sql     = "";
        try{
            
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
          //set:
          //  st.setString(1, user );
            st.setString(1, comprobante.getFechadoc()  );
            st.setString(2, comprobante.getPeriodo()   );            
            st.setString(3, String.valueOf(comprobante.getGrupo_transaccion())   );
          //where:            
            st.setString(4, comprobante.getDstrct()          );
            st.setString(5, comprobante.getTercero()         );
            st.setString(6, comprobante.getTipodoc_factura() );
            st.setString(7, comprobante.getNumdoc()          );

            sql = st.toString();            
            
        }catch(Exception e){
            throw new Exception( "updateFacturaClientes " + e.getMessage());
        }finally {
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return sql;
    }
    
    public  String getBco(String codf) throws Exception{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        List lista = new LinkedList();
        String query = "SQL_BANCOS";
        String ret = "";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, codf);
            rs = st.executeQuery();
            if (rs.next()) {
                ret = rs.getString(1);
            }

        } catch (Exception e) {
            throw new Exception("getItems " + e.getMessage());
        }
finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return ret;
    }
    
    public  String getBcoNotaAjuste(String codf) throws Exception{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        List lista = new LinkedList();
        String query = "SQL_BANCOS_NOTAJUSTE";
        String ret = "";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, codf);
            rs = st.executeQuery();
            if (rs.next()) {
                ret = rs.getString(1);
            }

        } catch (Exception e) {
            throw new Exception("getItems " + e.getMessage());
        }
finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return ret;
    }
    
    
}
