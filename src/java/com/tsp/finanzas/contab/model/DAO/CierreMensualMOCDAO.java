/***********************************************************************
* Nombre ......................CierreMensualMOCDAO.java                *
* Descripci�n..................Clase DAO para el cierre mensual de     *
*                              los modulos operativos y contables.     *
* Autor........................LREALES                                 *
* Fecha Creaci�n...............28 de junio de 2006, 04:56 PM           *
* Versi�n......................1.0                                     *
* Coyright.....................Transportes Sanchez Polo S.A.           *
***********************************************************************/

package com.tsp.finanzas.contab.model.DAO;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.*;
import com.tsp.finanzas.contab.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;
import com.tsp.operation.model.DAOS.MainDAO;
import com.tsp.operation.model.beans.StringStatement;

public class CierreMensualMOCDAO extends MainDAO {
    
    private PeriodoContable periodo;
    private Vector vec_periodo;
    
    /** Creates a new instance of CierreMensualMOCDAO */
    public CierreMensualMOCDAO () {
        super ( "CierreMensualMOCDAO.xml" );
    }
    public CierreMensualMOCDAO (String dataBaseName) {
        super ( "CierreMensualMOCDAO.xml", dataBaseName);
    }
    
    /**
     * Getter for property periodo.
     * @return Value of property periodo.
     */
    public com.tsp.finanzas.contab.model.beans.PeriodoContable getPeriodo() {
        return periodo;
    }
    
    /**
     * Setter for property periodo.
     * @param periodo New value of property periodo.
     */
    public void setPeriodo(com.tsp.finanzas.contab.model.beans.PeriodoContable periodo) {
        this.periodo = periodo;
    }
    
    /**
     * Getter for property vec_periodo.
     * @return Value of property vec_periodo.
     */
    public java.util.Vector getVec_periodo() {
        return vec_periodo;
    }
    
    /**
     * Setter for property vec_periodo.
     * @param vec_periodo New value of property vec_periodo.
     */
    public void setVec_periodo(java.util.Vector vec_periodo) {
        this.vec_periodo = vec_periodo;
    }
    
    /**
     * Metodo:          existePeriodo
     * Descripcion :    M�todo que permite verificar si existe o no un periodo.
     * @autor :         LREALES
     * @param:          distrito, a�o del periodo y mes del periodo.
     * @return:         un switch tipo boolean
     * @throws:         SQLException
     * @version :       1.0
     */
    public boolean existePeriodo(String distrito, String anio, String mes) throws SQLException {
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean sw = false;
        String sql = "SQL_BUSCAR";        

        mes = mes.equals("1") ? "01" : mes;
        mes = mes.equals("2") ? "02" : mes;
        mes = mes.equals("3") ? "03" : mes;
        mes = mes.equals("4") ? "04" : mes;
        mes = mes.equals("5") ? "05" : mes;
        mes = mes.equals("6") ? "06" : mes;
        mes = mes.equals("7") ? "07" : mes;
        mes = mes.equals("8") ? "08" : mes;
        mes = mes.equals("9") ? "09" : mes;
        Connection con = null;
        try {

            con = this.conectarJNDI(sql);
            st = con.prepareStatement(this.obtenerSQL(sql));
            st.setString(1, distrito);
            st.setString(2, anio);
            st.setString(3, mes);
            rs = st.executeQuery();
            vec_periodo = new Vector();

            if (rs.next()) {
                sw = true;
            }

        } catch (Exception e) {

            e.printStackTrace();
            throw new SQLException("ERROR DURANTE 'existePeriodo' - [CierreMensualMOCDAO].. " + e.getMessage() + " " + e.getMessage());

        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st = null;
                } catch (Exception e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }

        return sw;

    }
    
    /**
     * Metodo:          buscarPeriodo
     * Descripcion :    M�todo que permite buscar los datos de un periodo en especifico.
     * @autor :         LREALES
     * @param:          distrito, a�o del periodo y mes del periodo.
     * @return:         -
     * @throws:         SQLException
     * @version :       1.0
     */
    public void buscarPeriodo(String distrito, String anio, String mes) throws SQLException {

        PreparedStatement st = null;
        ResultSet rs = null;
        Connection con = null;
        String query = "SQL_BUSCAR";

        mes = mes.equals("1") ? "01" : mes;
        mes = mes.equals("2") ? "02" : mes;
        mes = mes.equals("3") ? "03" : mes;
        mes = mes.equals("4") ? "04" : mes;
        mes = mes.equals("5") ? "05" : mes;
        mes = mes.equals("6") ? "06" : mes;
        mes = mes.equals("7") ? "07" : mes;
        mes = mes.equals("8") ? "08" : mes;
        mes = mes.equals("9") ? "09" : mes;

        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));
            st.setString(1, distrito);
            st.setString(2, anio);
            st.setString(3, mes);
            rs = st.executeQuery();

            vec_periodo = new Vector();

            while (rs.next()) {

                periodo = new PeriodoContable();
                // seteo
                periodo.setDistrito(rs.getString("dstrct"));
                periodo.setAnio(rs.getString("anio"));
                periodo.setMes(rs.getString("mes"));
                periodo.setAc(rs.getString("ac"));
                periodo.setUser_update(rs.getString("user_update"));
                periodo.setLast_update(rs.getString("last_update"));
                vec_periodo.add(periodo);
            }

        } catch (SQLException e) {
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE 'buscarPeriodo' - [CierreMensualMOCDAO].. " + e.getMessage() + " " + e.getErrorCode());
        }
        
        finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        
    }
    
    /**
     * Metodo:          cerrarPeriodo
     * Descripcion :    M�todo que permite cerrar un periodo.
     * @autor :         LREALES
     * @param:          -
     * @return:         -
     * @throws:         SQLException
     * @version :       1.0
     */
    public void cerrarPeriodo () throws SQLException{

        Connection con = null;
        PreparedStatement st = null;
        String query = "SQL_CERRAR";
        try {

            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));

            st.setString(1, periodo.getUser_update());
            st.setString(2, periodo.getDistrito());
            st.setString(3, periodo.getAnio());
            st.setString(4, periodo.getMes());

            st.executeUpdate();

        } catch (SQLException e) {

            e.printStackTrace();
            throw new SQLException("ERROR DURANTE 'cerrarPeriodo' - [CierreMensualMOCDAO].. " + e.getMessage() + " " + e.getErrorCode());

        }        
finally{
            if (st  != null){ try{ st.close();} catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
        
    }
    
}