
/***************************************
 * Nombre Clase ............. ListadoBusquedaDAO.java
 * Descripci�n  .. . . . . .  Permitimos cargar listado de busquedas
 * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
 * Fecha . . . . . . . . . .  09/06/2006
 * versi�n . . . . . . . . .  1.0
 * Copyright ...............  Transportes Sanchez Polo S.A.
 *******************************************/




package com.tsp.finanzas.contab.model.DAO;




import java.io.*;
import java.util.*;
import java.sql.*;
import com.tsp.util.*;
import com.tsp.operation.model.DAOS.MainDAO;


public class ListadoBusquedaDAO extends MainDAO{
    
    
    public ListadoBusquedaDAO() {
        super("ListadoBusquedaDAO.xml");
    }
    public ListadoBusquedaDAO(String dataBaseName) {
        super("ListadoBusquedaDAO.xml",dataBaseName);
    }
    
    
    private String reset(String val){
        if(val==null)
           val = "";
        return val;
    }
    
    
    /**
       * M�todo que busca la lista de valores de la consulta
       * @autor.......fvillacob
       * @throws......Exception
       * @version.....1.0.
       **/
      public  Vector getLista(String query, String filtro) throws Exception{

            PreparedStatement  st      = null;
            ResultSet          rs      = null;
            Vector             lista   = new Vector();
            Connection         con     = null;
            try{
                
                String newFiltro = (filtro.trim().equals(""))?"like '%' " : "='" + filtro +"'";
                newFiltro = newFiltro.replaceAll("-_-", "'");

                con = this.conectarJNDI(query);
                String sql = this.obtenerSQL(query).replaceAll("#FILTRO#", newFiltro);
                //////System.out.println("SQLLL---->"+sql.toString());
                st = con.prepareStatement(sql);//JJCastro fase2
                rs = st.executeQuery();
                
                while (rs.next()){
                    Hashtable  fila = new Hashtable();
                      fila.put("codigo",        reset( rs.getString("codigo") )      );
                      fila.put("descripcion",   reset( rs.getString("descripcion") ) );
                   lista.add( fila );
                   fila = null;//Liberar Espacio JJCastro
                }   
                
                
            }catch(Exception e){
                  throw new Exception( "getLista " + e.getMessage());
            }
finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
            return lista; 
      }
      
      
      
      
    
}
