/********************************************************************
 *  Nombre Clase.................   CmcDAO.java
 *  Descripci�n..................   DAO de la tabla con.cmc_doc
 *  Autor........................   Ing. David Pi�a L�pez
 *  Fecha........................   12.06.2006
 *  Versi�n......................   1.0
 *  Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.finanzas.contab.model.DAO;

import java.util.*;
import java.sql.*;
import com.tsp.finanzas.contab.model.beans.*;
import org.apache.log4j.Logger;
import com.tsp.util.*;
import java.text.*;
import com.tsp.util.connectionpool.PoolManager;
import com.tsp.operation.model.DAOS.MainDAO;

import javax.servlet.http.HttpSession;
import com.tsp.operation.model.beans.StringStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class CmcDAO extends MainDAO {

    private Cmc cmc;
    private TreeMap treemap;
    private Vector vector;

    /** Creates a new instance of CmcDAO */
    public CmcDAO() {
        super( "CmcDAO.xml" );
    }
    public CmcDAO(String dataBaseName) {
        super( "CmcDAO.xml" , dataBaseName);
    }

    /**
     *M�todo que permite insertar un cmc_doc
     *@autor: David Pi�a
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void insertar() throws SQLException{

        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_INSERTAR_CMCDOC";
        try{
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));

            st.setString( 1, cmc.getDstrct() );
            st.setString( 2, cmc.getTipodoc() );
            st.setString( 3, cmc.getCmc() );
            st.setString( 4, cmc.getCuenta() );
            st.setString( 5, cmc.getDbcr() );
            st.setString( 6, cmc.getTipo_cuenta() );
            st.setString( 7, cmc.getUsuario() );
            st.setString( 8, cmc.getUsuario() );
            st.setString( 9, cmc.getBase() );
            st.executeUpdate();
        }catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE insertar() " + e.getMessage() + " " + e.getErrorCode());
        }
finally{
            if (st  != null){ try{ st.close();} catch(SQLException e){ throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage()); }}
            if (con != null){ try{ this.desconectar(con); } catch(SQLException e){ throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage()); }}
        }
    }
    /**
     *M�todo que busca un cmc_doc a trav�s de varios filtros
     *@autor: David Pi�a
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void buscar() throws SQLException{

        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_BUSCAR_CMCDOC";

        try{
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));
            st.setString( 1, "%" + cmc.getTipodoc() + "%" );
            st.setString( 2, "%" + cmc.getCmc() + "%" );
            st.setString( 3, "%" + cmc.getDbcr() + "%" );
            st.setString( 4, "%" + cmc.getTipo_cuenta() + "%" );
            st.setString( 5, "%" + cmc.getCuenta() + "%" );
            rs = st.executeQuery();

            this.cmc = null;
            this.vector = new Vector();
            while ( rs.next() ){
                this.cmc = new Cmc();
                this.cmc.Load( rs );
                this.cmc.setDescripcionCmc( rs.getString( "descmc" )!=null?rs.getString( "descmc" ):"" );
                this.cmc.setDescripcionTipodoc( rs.getString( "desctipodoc" )!=null?rs.getString( "desctipodoc" ):"" );
                this.vector.add( cmc );

            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE buscar() " + e.getMessage() + " " + e.getErrorCode());
        }
finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
    }
    /**
     *M�todo que actualiza un cmc_doc
     *@autor: David Pi�a
     *@param tipoDocumento El tipo de documento
     *@param cmc_old el Viejo Cmc
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void actualizar(String tipoDocumento, String cmc_old) throws SQLException {

        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_ACTUALIZAR_CMCDOC";

        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));
            st.setString(1, cmc.getReg_status());
            st.setString(2, cmc.getTipodoc());
            st.setString(3, cmc.getCmc());
            st.setString(4, cmc.getCuenta());
            st.setString(5, cmc.getDbcr());
            st.setString(6, cmc.getTipo_cuenta());
            st.setString(7, cmc.getUsuario());
            st.setString(8, cmc.getDstrct());
            st.setString(9, tipoDocumento);
            st.setString(10, cmc_old);
            st.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE actualizar( String tipoDocumento, String cmc_new ) " + e.getMessage() + " " + e.getErrorCode());
        }
finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
    }
    /**
     *M�todo que obtiene un cmc_doc
     *@autor: David Pi�a
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void obtenerCmcDoc() throws SQLException{

        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_OBTENER_CMCDOC";
        try {

            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));
            st.setString(1, cmc.getDstrct());
            st.setString(2, cmc.getTipodoc());
            st.setString(3, cmc.getCmc());
            rs = st.executeQuery();

            this.cmc = null;
            if (rs.next()) {
                this.cmc = new Cmc();
                this.cmc.Load(rs);
            }
        } catch (SQLException e) {
            throw new SQLException("ERROR DURANTE obtenerCmcDoc() " + e.getMessage() + " " + e.getErrorCode());
        }
finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
    }
    /**
     *M�todo que obtiene todos los cmc activos
     *@autor: David Pi�a
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void obtenerCmc() throws SQLException{

        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_OBTENER_CMC";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));
            rs = st.executeQuery();
            this.treemap = new TreeMap();
            while (rs.next()) {
                treemap.put(rs.getString("descripcion"), rs.getString("table_code"));
            }
        } catch (SQLException e) {
            throw new SQLException("ERROR DURANTE obtenerCmc() " + e.getMessage() + " " + e.getErrorCode());
        }
finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
    }


    /**
     * Getter for property cmc.
     * @return Value of property cmc.
     */
    public com.tsp.finanzas.contab.model.beans.Cmc getCmc() {
        return cmc;
    }

    /**
     * Setter for property cmc.
     * @param cmc New value of property cmc.
     */
    public void setCmc(com.tsp.finanzas.contab.model.beans.Cmc cmc) {
        this.cmc = cmc;
    }

    /**
     * Getter for property treemap.
     * @return Value of property treemap.
     */
    public java.util.TreeMap getTreemap() {
        return treemap;
    }

    /**
     * Setter for property treemap.
     * @param treemap New value of property treemap.
     */
    public void setTreemap(java.util.TreeMap treemap) {
        this.treemap = treemap;
    }

    /**
     * Getter for property vector.
     * @return Value of property vector.
     */
    public java.util.Vector getVector() {
        return vector;
    }

    /**
     * Setter for property vector.
     * @param vector New value of property vector.
     */
    public void setVector(java.util.Vector vector) {
        this.vector = vector;
    }



    /************************listar HCs*********************************/
    public List<Cmc> ListarCmc(String codigo) throws SQLException {
        List<Cmc> lis = new LinkedList<Cmc>();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_LISTAR_CMC";
        try {
        con = this.conectarJNDI(query);
        if(con!=null){
         ps = con.prepareStatement(this.obtenerSQL(query));
         ps.setString(1, codigo.toUpperCase());
         rs = ps.executeQuery();
            while (rs.next()) {
                Cmc cmc = new Cmc();
                cmc.setCmc(rs.getString("cmc"));
                cmc.setCuenta(rs.getString("cuenta"));
                cmc.setDbcr(rs.getString("dbcr"));
                cmc.setTipo_cuenta(rs.getString("tipo_cuenta"));
                cmc.setTipodoc(rs.getString("tipodoc"));
                lis.add(cmc);
            }


        }
        }
        catch(SQLException e)
        {
             System.out.print("<---->"+e.getMessage());
            throw new SQLException("ERROR DURANTE Listar " + e.getMessage() + " " + e.getErrorCode());
        }
        finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return lis;
    }


























        /************************Validar lista tipodoc*********************************/
    public static List<Cmc> lista_cmc_sesion_notnul(HttpSession session) {
       List<Cmc> lista = new LinkedList<Cmc>();
        if ((List)session.getAttribute("lista_cmc") != null) {
            lista = (List<Cmc>)session.getAttribute("lista_cmc");
        }
        return lista;
    }


        /*******************************  Llenar una lista de tipo doc******************************/
    public static List<Cmc> llenar_lista(String tipo_doc, String cmc,String cuenta,String dbcr,String tipo_cuenta,String usuario,
                                                         List<Cmc> lista) {
       Cmc Objcmc = new Cmc();
       Objcmc.setTipodoc(tipo_doc);
       Objcmc.setCuenta(cuenta);
       Objcmc.setCmc(cmc);
       Objcmc.setDbcr(dbcr);
       Objcmc.setTipo_cuenta(tipo_cuenta);
       Objcmc.setUsuario(usuario);
       lista.add(Objcmc);
        return lista;
    }




/*eliminar un cmc de la lista temporal*/
        public static List deletelista(int index, List lista) {
        if ((index >= 0)) {
            lista.remove(index);
        }
        return lista;
    }




    /*******************************  Validar tipo dco en lista temporal de cmc ******************************/
    public boolean Validar_lista_cmc(List<Cmc> lista_cmc,String tipo_doc) throws SQLException {
        int a = 0;
        boolean sw=false;
        try {
            for (int i = 0; i < lista_cmc.size(); i++)
            {
                if(lista_cmc.get(i).getTipodoc().equals(tipo_doc))
                {
                    sw=true;
                    i=1000000;
                }
            }
        } catch (Exception e) {

        }
        return sw;
    }


    public String Insertar(Cmc cmc) throws SQLException{
        StringStatement st = null;
        Connection con = null;
        String query = "SQL_INSERTAR_CMC";
        String sql = "";

        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = new StringStatement(this.obtenerSQL(query),true);
                st.setString( 1, cmc.getTipodoc() );
                st.setString( 2, cmc.getCmc() );
                st.setString( 3, cmc.getCuenta() );
                st.setString( 4, cmc.getDbcr() );
                st.setString( 5, cmc.getTipo_cuenta() );
                st.setString( 6, cmc.getUsuario() );
                st.setString( 7, cmc.getUsuario() );
                st.setString( 8, cmc.getTipodoc());


                sql = st.getSql();
            }
        }catch(Exception e){
            throw new SQLException("ERROR DURANTE Insertar CMC \n " + e.getMessage());
        } finally {
            if (st != null) {
                st = null;
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return sql;
    }


     public String Actualizar(Cmc cmc) throws SQLException{
        StringStatement st = null;
        Connection con = null;
        String query = "SQL_ACTUALIZAR_CMC";
        String sql = "";

        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = new StringStatement(this.obtenerSQL(query),true);
                st.setString( 1, cmc.getTipodoc() );
                st.setString( 2, cmc.getCuenta() );
                st.setString( 3, cmc.getDbcr() );
                st.setString( 4, cmc.getTipo_cuenta() );
                st.setString( 5, cmc.getUsuario() );
                st.setString( 6, cmc.getTipodoc());
                st.setString( 7, cmc.getCmc());


                sql = st.getSql();
            }
        }catch(Exception e){
            throw new SQLException("ERROR DURANTE Actualizar CMC \n " + e.getMessage());
        } finally {
            if (st != null) {
                st = null;
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return sql;
    }


      public String Eliminar(String cmc) throws SQLException{
        StringStatement st = null;
        Connection con = null;
        String query = "SQL_ELIMINAR_CMCDOC";
        String sql = "";

        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = new StringStatement(this.obtenerSQL(query),true);
                st.setString( 1, cmc );
                sql = st.getSql();
            }
        }catch(Exception e){
            throw new SQLException("ERROR DURANTE ELIMINAR CMC \n " + e.getMessage());
        } finally {
            if (st != null) {
                st = null;
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return sql;
    }


}