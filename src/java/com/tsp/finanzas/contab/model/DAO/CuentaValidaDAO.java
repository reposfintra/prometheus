/********************************************************************
 *  Nombre Clase.................   CuentaValidaDAO.java
 *  Descripcion..................   DAO de la tabla cuenta_valida
 *  Autor........................   Ing. Leonardo Parodi
 *  Fecha........................   19.01.2006
 *  Version......................   1.0
 *  Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.finanzas.contab.model.DAO;


import java.io.*;
import java.sql.*;
import java.util.*;
import java.util.Date;

import com.tsp.util.*;
import com.tsp.finanzas.contab.model.beans.CuentaValida;
//import com.tsp.util.connectionpool.PoolManager;
import com.tsp.operation.model.DAOS.MainDAO;

import javax.swing.*;
import java.text.*;
/**
 *
 * @author  EQUIPO12
 */
public class CuentaValidaDAO extends MainDAO{
        private String consulta="";
        private CuentaValida cuentavalida = new CuentaValida();
        /** Creates a new instance of CuentaValidaDAO */
        public CuentaValidaDAO() {
            super("CuentaValidaDAO.xml");
        }
        public CuentaValidaDAO(String dataBaseName) {
            super("CuentaValidaDAO.xml",dataBaseName);
        }


        /**this.consulta
         * Metodo InsertCuentaValida , Metodo que Ingresa un formato de cuenta valida.
         * @autor : Ing. Leonardo Parody
         * @throws : SQLException
         * @version : 1.0
         */
        public void InsertCuentaValida() throws SQLException {
                Connection con= null;
                PreparedStatement st = null;
                ResultSet rs = null;
              //  PoolManager poolManager = null;
                boolean sw = false;
                String query = "SQL_INSERT";
                try{
                      //  poolManager = PoolManager.getInstance();
                      con = this.conectarJNDI(query);
                      //  con = poolManager.getConnection(this.getSesionUsuario());
                        if (con != null){
                                st = con.prepareStatement(this.obtenerSQL(query));
                                st.setString(1, cuentavalida.getClase());
                                st.setString(2, cuentavalida.getAgencia());
                                st.setString(3, cuentavalida.getUnidad());
                                st.setString(4, cuentavalida.getClienteArea());
                                st.setString(5, cuentavalida.getElemento());
                                st.setString(6, cuentavalida.getCreation_user());
                                st.setString(7, cuentavalida.getDstrct());
                                st.setString(8, cuentavalida.getBase());
                                st.setString(9, cuentavalida.getCuentaEquivalente());
                                st.executeUpdate();

                        }

                }catch(SQLException e){
                        throw new SQLException("ERROR AL REALIZAR EL INSERT" + e.getMessage()+"" + e.getErrorCode());
                }finally{
                        if(st != null){
                                try{
                                        st.close();
                                }
                                catch(SQLException e){
                                        throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                                }
                        }
                        if ( con != null){
                            this.desconectar(con);
                         //       poolManager.freeConnection( con );
                        }
                }
        }

        /**
         * Metodo ConsultaRelacion , Metodo que verifica si existe la relacion grupo, subgrupo, placa.
         * @autor : Ing. Leonardo Parody
         * @throws : SQLException
         * @return : List cuentas
         * @version : 1.0
         */
        public List ConsultaListaCuentaValida(String clase, String agencia, String cliente_area, String elemento, String unidad) throws SQLException{
                LinkedList cuentas = new LinkedList();
                String consulta;
                CuentaValida cuenta = new CuentaValida();
                boolean existe=false;
                Connection con = null;
                PreparedStatement st = null;
                ResultSet rs = null;
               // PoolManager poolManager = null;
                this.consulta = this.consulta(clase, agencia, cliente_area, elemento, unidad);
                try{
                 //       poolManager = PoolManager.getInstance();
                    con = this.conectarJNDI( "SQL_CONSULTLIST");
                 //       con = poolManager.getConnection(this.getSesionUsuario());

                        if (con != null){
                                st = con.prepareStatement(this.consulta);
                                //System.out.println("Lista = "+st);
                                rs = st.executeQuery();
                                while (rs.next()) {
                                        cuenta = cuenta.Load(rs);
                                        cuentas.add(cuenta);
                                }
                        }
                        return cuentas;
                }catch(SQLException e){
                        throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LAS CUENTAS" + e.getMessage() + " " + e.getErrorCode());
                }
                finally{
                        if (st != null){
                                try{
                                        st.close();
                                }
                                catch(SQLException e){
                                        throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                                }
                        }
                        if (con != null){
                           //     poolManager.freeConnection( con);
                            this.desconectar(con);
                        }
                }
        }

        /**
         * Metodo UpdateCuentaValida , Metodo que Modifica una Cuenta Valida.
         * @autor : Ing. Leonardo Parody
         * @param : String clase
         * @param : String agencia
         * @param : String unidad
         * @param : String cliente_area
         * @param : String elemento
         * @throws : SQLException
         * @version : 1.0
         */
        public void UpdateCuentaValida(String clase, String agencia, String unidad, String cliente_area, String elemento) throws SQLException{
                Connection con = null;
                PreparedStatement st = null;
                ResultSet rs = null;
                String query = "SQL_UPDATE";
                //PoolManager poolManager = null;
                try{
                        //poolManager = PoolManager.getInstance();
                    con = this.conectarJNDI(query);
                        //con = poolManager.getConnection(this.getSesionUsuario());
                        if (con != null){
                                st = con.prepareStatement(this.obtenerSQL(query));
                                st.setString(1, cuentavalida.getClase());
                                st.setString(2, cuentavalida.getAgencia());
                                st.setString(3, cuentavalida.getUnidad());
                                st.setString(4, cuentavalida.getClienteArea());
                                st.setString(5, cuentavalida.getElemento());
                                st.setString(6, cuentavalida.getUser_update());
                                st.setString(7, cuentavalida.getCuentaEquivalente());
                                st.setString(8, clase);
                                st.setString(9, agencia);
                                st.setString(10, unidad);
                                st.setString(11, cliente_area);
                                st.setString(12, elemento);
                                //System.out.println("st de Update = "+st);
                                st.executeUpdate();
                        }
                }catch(SQLException e){
                        throw new SQLException("ERROR DURANTE LA MODIFICACI�N DE LA CUENTA" + e.getMessage() + " " + e.getErrorCode());
                }
                finally{
                        if (st != null){
                                try{
                                        st.close();
                                }
                                catch(SQLException e){
                                        throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                                }
                        }
                        if (con != null){
                                //poolManager.freeConnection( con);
                            this.desconectar(con);
                        }
                }
        }

        /**
         * Metodo ExisteCuentaValida , Metodo que verifica si existe la cuenta valida, subgrupo, placa.
         * @autor : Ing. Leonardo Parody
         * @param : String clase
         * @param : String agencia
         * @param : String unidad
         * @param : String cliente_area
         * @param : String elemento
         * @throws : SQLException
         * @return : boolean sw
         * @version : 1.0
         */
        public boolean ExisteCuentaValida(String clase, String agencia, String unidad, String cliente_area, String elemento) throws SQLException{
                boolean sw = false;
                CuentaValida cuenta = new CuentaValida();
                boolean existe=false;
                Connection con = null;
                PreparedStatement st = null;
                ResultSet rs = null;
                String query = "SQL_CONSULT";
                //PoolManager poolManager = null;
                try{
                        //poolManager = PoolManager.getInstance();
                    con = this.conectarJNDI(query);
                        //con = poolManager.getConnection(this.getSesionUsuario());
                        if (con != null){
                                st = con.prepareStatement(this.obtenerSQL(query));
                                st.setString(1, clase);
                                st.setString(2, agencia);
                                st.setString(3, unidad);
                                st.setString(4, cliente_area);
                                st.setString(5, elemento);
                                //System.out.println("st Insertar = "+st);
                                rs = st.executeQuery();
                                while (rs.next()) {
                                        sw = true;
                                }
                        }
                        return sw;
                }catch(SQLException e){
                        throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LA CUENTA" + e.getMessage() + " " + e.getErrorCode());
                }
                finally{
                        if (st != null){
                                try{
                                        st.close();
                                }
                                catch(SQLException e){
                                        throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                                }
                        }
                        if (con != null){
                                //poolManager.freeConnection( con);
                            this.desconectar(con);
                        }
                }
        }

        /**
         * Metodo AnularCuentaValida , Metodo que Anula una Cuenta Valida.
         * @autor : Ing. Leonardo Parody
         * @param : String clase
         * @param : String agencia
         * @param : String unidad
         * @param : String cliente_area
         * @param : String elemento
         * @param : String user
         * @throws : SQLException
         * @version : 1.0
         */
        public void AnularCuentaValida(String clase, String agencia, String unidad, String cliente_area, String elemento, String user) throws SQLException{
                Connection con = null;
                PreparedStatement st = null;
                ResultSet rs = null;
                String query = "SQL_ANULAR" ;
                //PoolManager poolManager = null;
                try{
                        //poolManager = PoolManager.getInstance();
                    con = this.conectarJNDI(query);
                        //con = poolManager.getConnection(this.getSesionUsuario());
                        if (con != null){
                                st = con.prepareStatement(this.obtenerSQL(query));
                                st.setString(1, user);
                                st.setString(2, clase);
                                st.setString(3, agencia);
                                st.setString(4, unidad);
                                st.setString(5, cliente_area);
                                st.setString(6, elemento);
                                //System.out.println("st Anular = "+st);
                                st.executeUpdate();
                        }
                }catch(SQLException e){
                        throw new SQLException("ERROR DURANTE LA MODIFICACI�N DE LA CUENTA" + e.getMessage() + " " + e.getErrorCode());
                }
                finally{
                        if (st != null){
                                try{
                                        st.close();
                                }
                                catch(SQLException e){
                                        throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                                }
                        }
                        if (con != null){
                                //poolManager.freeConnection( con);
                            this.desconectar(con);
                        }
                }
        }

        /**
         * Metodo EliminarCuentaValida , Metodo que Elimina una Cuenta Valida.
         * @autor : Ing. Leonardo Parody
         * @param : String clase
         * @param : String agencia
         * @param : String unidad
         * @param : String cliente_area
         * @param : String elemento
         * @param : String user
         * @throws : SQLException
         * @version : 1.0
         */
        public void EliminarCuentaValida(String clase, String agencia, String unidad, String cliente_area, String elemento) throws SQLException{
                Connection con = null;
                PreparedStatement st = null;
                ResultSet rs = null;
                String query = "SQL_ELIMINAR";
                //PoolManager poolManager = null;
                try{
                        //poolManager = PoolManager.getInstance();
                    con = this.conectarJNDI(query);
                        //con = poolManager.getConnection(this.getSesionUsuario());
                        if (con != null){
                                st = con.prepareStatement(this.obtenerSQL(query));
                                st.setString(1, clase);
                                st.setString(2, agencia);
                                st.setString(3, unidad);
                                st.setString(4, cliente_area);
                                st.setString(5, elemento);
                                st.executeUpdate();
                        }
                }catch(SQLException e){
                        throw new SQLException("ERROR DURANTE LA MODIFICACI�N DE LA CUENTA" + e.getMessage() + " " + e.getErrorCode());
                }
                finally{
                        if (st != null){
                                try{
                                        st.close();
                                }
                                catch(SQLException e){
                                        throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                                }
                        }
                        if (con != null){
                                //poolManager.freeConnection( con);
                            this.desconectar(con);
                        }
                }
        }

        /**
         * Metodo ConsultCuentaValida , Metodo que verifica si existe la cuenta valida, subgrupo, placa.
         * @autor : Ing. Leonardo Parody
         * @param : String clase
         * @param : String agencia
         * @param : String unidad
         * @param : String cliente_area
         * @param : String elemento
         * @throws : SQLException
         * @return : CuentaValida cuenta
         * @version : 1.0
         */
        public CuentaValida ConsultCuentaValida(String clase, String agencia, String unidad, String cliente_area, String elemento) throws SQLException{
                boolean sw = false;
                CuentaValida cuenta = new CuentaValida();
                boolean existe=false;
                Connection con = null;
                PreparedStatement st = null;
                ResultSet rs = null;
                String query = "SQL_CONSULT";
                //PoolManager poolManager = null;
                try{
                        //poolManager = PoolManager.getInstance();
                    con = this.conectarJNDI(query);
                        //con = poolManager.getConnection(this.getSesionUsuario());
                        if (con != null){
                                st = con.prepareStatement(this.obtenerSQL(query));
                                st.setString(1, clase);
                                st.setString(2, agencia);
                                st.setString(3, unidad);
                                st.setString(4, cliente_area);
                                st.setString(5, elemento);
                                rs = st.executeQuery();
                                while (rs.next()) {
                                        cuenta = cuenta.Load(rs);
                                }
                        }
                        return cuenta;
                }catch(SQLException e){
                        throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LA CUENTA" + e.getMessage() + " " + e.getErrorCode());
                }
                finally{
                        if (st != null){
                                try{
                                        st.close();
                                }
                                catch(SQLException e){
                                        throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                                }
                        }
                        if (con != null){
                                //poolManager.freeConnection( con);
                            this.desconectar(con);
                        }
                }
        }


        private String consulta(String clase, String agencia, String cliente_area, String elemento, String unidad){
                int sw = 0;
                String consulta1 = "SELECT                                              "+
                                   "       a.*,                                         "+
                                   "       clase.descripcion AS nombreclase,            "+
                                   "       agencia.descripcion AS nombreagencia,        "+
                                   "       unidad.descripcion AS nombreunidad,          "+
                                   "       COALESCE(cliarea.descripcion, cliente.nomcli,cliarea.descripcion)     AS nombrecliente_area,   "+
                                   "       elemento.descripcion AS nombreelemento       "+
                                   "     FROM                                           "+
                                   "     (                           SQL_ELIMINAR                   "+
                                   "     SELECT                                         "+
                                   "          *                                         "+
                                   "     FROM                                           "+
                                   "      con.cuentas_validas                             "+
                                   "     WHERE                                          ";

                String consulta2 = "  ) a                                                                               "+
                                   "     LEFT JOIN tablagen  AS  clase    ON ( clase.table_code    = a.clase    AND clase.table_type = 'DIV'    )   "+
                                   "     LEFT JOIN tablagen  AS  agencia  ON ( agencia.table_code  = a.cod_agencia AND agencia.table_type = 'CIU'  )   "+
                                   "     LEFT JOIN tablagen  AS  unidad   ON ( unidad.table_code   = a.unidad    AND unidad.table_type = 'UPR'   )   "+
                                   "     LEFT JOIN tablagen  AS  cliarea  ON ( cliarea.table_code  = a.cliente_area AND cliarea.table_type = 'AAA')   "+
                                   "     LEFT JOIN tablagen  AS  elemento ON ( elemento.table_code = a.elemento     )   "+
			           "     LEFT JOIN cliente   AS  cliente  ON ( cliente.codcli     = '000'||a.cliente_area )    ";
                if (!clase.equals("NADA")){
                        consulta1 += "clase = '"+clase+"' ";
                        sw++;
                }
                if (!agencia.equals("NADA")){
                        if (sw==0) {
                                consulta1 += "cod_agencia = '"+agencia+"' ";
                        }else{
                                consulta1 += " AND cod_agencia = '"+agencia+"' ";
                        }
                }
                if (!cliente_area.equals("NADA")){
                        if (sw==0) {
                                consulta1 += "cliente_area = '"+cliente_area+"' ";
                        }else{
                                consulta1 += " AND cliente_area = '"+cliente_area+"' ";
                        }
                }
                if (!elemento.equals("NADA")){
                        if (sw==0) {
                                consulta1 += "elemento = '"+elemento+"' ";
                        }else{
                                consulta1 += " AND elemento = '"+elemento+"' ";
                        }
                }
                 if (!unidad.equals("NADA")){
                        if (sw==0) {
                                consulta1 += "unidad = '"+unidad+"' ";
                        }else{
                                consulta1 += " AND unidad = '"+unidad+"' ";
                        }
                }
                consulta1 += consulta2;
                return consulta1;
        }
         /**
         * Setter for Object cuentavalida.
         * @param cuentavalida New value of Object cuentavalida.
         */
        public void setCuentaValida(CuentaValida cuenta) {
                this.cuentavalida = cuenta;
        }
}