/***********************************************************************
* Nombre ......................PlanDeCuentasDAO.java                   *
* Descripci�n..................Clase DAO para el plan de cuentas       *
* Autor........................LREALES                                 *
* Fecha Creaci�n...............5 de junio de 2006, 06:05 PM            *
* Versi�n......................1.0                                     *
* Coyright.....................Transportes Sanchez Polo S.A.           *
***********************************************************************/

package com.tsp.finanzas.contab.model.DAO;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.*;
import com.tsp.finanzas.contab.model.beans.*;
import com.tsp.operation.model.DAOS.MainDAO;
import org.apache.log4j.*;

public class PlanDeCuentasDAO extends MainDAO {

    private PlanDeCuentas cuentas;
    private Vector vec_cuentas;
    Logger logger = Logger.getLogger(this.getClass());
    private LinkedList lista;

    /** Creates a new instance of PlanDeCuentasDAO */
    public PlanDeCuentasDAO () {
        super ( "PlanDeCuentasDAO.xml" );
    }
    public PlanDeCuentasDAO (String dataBaseName) {
        super ( "PlanDeCuentasDAO.xml", dataBaseName );
    }

    /**
     * Getter for property cuentas.
     * @return Value of property cuentas.
     */
    public PlanDeCuentas getCuentas() {

        return cuentas;

    }

    /**
     * Setter for property cuentas.
     * @param cuentas New value of property cuentas.
     */
    public void setCuentas(PlanDeCuentas cuentas ) {

        this.cuentas = cuentas;

    }

    /**
     * Getter for property vec_cuentas.
     * @return Value of property vec_cuentas.
     */
    public java.util.Vector getVec_cuentas() {

        return vec_cuentas;

    }

    /**
     * Setter for property vec_cuentas.
     * @param vec_cuentas New value of property vec_cuentas.
     */
    public void setVec_cuentas( java.util.Vector vec_cuentas ) {

        this.vec_cuentas = vec_cuentas;

    }

    /**
     * Metodo:          insertCuenta
     * Descripcion :    M�todo que permite ingresar un registro a la tabla cuentas del esquema con.
     * @autor :         LREALES
     * @param:          -
     * @return:         -
     * @throws:         SQLException
     * @version :       1.0
     */
    public void insertCuenta () throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        String query = "SQL_INSERT";
        try{

            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2

            st.setString ( 1, cuentas.getDstrct() );
            st.setString ( 2, cuentas.getCuenta() );
            st.setString ( 3, cuentas.getNombre_largo() );
            st.setString ( 4, cuentas.getNombre_corto() );
            st.setString ( 5, cuentas.getNombre_observacion() );
            st.setString ( 6, cuentas.getAuxiliar() );
            st.setString ( 7, cuentas.getBase() );
            st.setString ( 8, cuentas.getModulo1() );
            st.setString ( 9, cuentas.getModulo2() );
            st.setString ( 10, cuentas.getModulo3() );
            st.setString ( 11, cuentas.getModulo4() );
            st.setString ( 12, cuentas.getModulo5() );
            st.setString ( 13, cuentas.getModulo6() );
            st.setString ( 14, cuentas.getModulo7() );
            st.setString ( 15, cuentas.getModulo8() );
            st.setString ( 16, cuentas.getModulo9() );
            st.setString ( 17, cuentas.getModulo10() );
            st.setString ( 18, cuentas.getCta_dependiente() );
            st.setInt(19, Integer.parseInt(cuentas.getNivel()));
            st.setString ( 20, cuentas.getCta_cierre() );
            st.setString ( 21, cuentas.getSubledger() );
            st.setString ( 22, cuentas.getTercero() );
            st.setString ( 23, cuentas.getUser_update() );
            st.setString ( 24, cuentas.getCreation_user() );
            st.setString ( 25, cuentas.getDetalle() );
            st.execute();

        } catch( SQLException e ){

            e.printStackTrace();
            throw new SQLException( "ERROR DURANTE 'insertCuenta' - [PlanDeCuentaDAO].. " + e.getMessage() + " " + e.getErrorCode() );

        }
finally {
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
    }

    /**
     * Metodo:          searchCuenta
     * Descripcion :    M�todo que permite buscar una cuenta dado unos parametros.
     * @autor :         LREALES
     * @param:          distrito y numero de la cuenta.
     * @return:         -
     * @throws:         SQLException
     * @version :       1.0
     */
    public void searchCuenta ( String distrito, String cuenta )throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        cuentas = null;
        String query = "SQL_SEARCH";
        try {

            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString ( 1, distrito );
            st.setString ( 2, cuenta );
            rs = st.executeQuery ();
            vec_cuentas = new Vector ();

            while( rs.next () ){

                cuentas = new PlanDeCuentas ();
                // seteo
                cuentas.setCuenta( rs.getString ("cuenta") );
                cuentas.setActiva( rs.getString ("activa") );
                cuentas.setNombre_corto( rs.getString ("nombre_corto") );
                cuentas.setNombre_largo( rs.getString ("nombre_largo") );
                cuentas.setNombre_observacion( rs.getString ("nombre_observacion") );
                cuentas.setNivel( rs.getString ("nivel") );
                cuentas.setAuxiliar( rs.getString ("auxiliar") );
                cuentas.setSubledger( rs.getString ("subledger") );
                cuentas.setTercero( rs.getString ("tercero") );
                cuentas.setCta_dependiente( rs.getString ("cta_dependiente") );
                cuentas.setCta_cierre( rs.getString ("cta_cierre") );
                cuentas.setModulo1( rs.getString ("modulo1") );
                cuentas.setModulo2( rs.getString ("modulo2") );
                cuentas.setModulo3( rs.getString ("modulo3") );
                cuentas.setModulo4( rs.getString ("modulo4") );
                cuentas.setModulo5( rs.getString ("modulo5") );
                cuentas.setModulo6( rs.getString ("modulo6") );
                cuentas.setModulo7( rs.getString ("modulo7") );
                cuentas.setModulo8( rs.getString ("modulo8") );
                cuentas.setModulo9( rs.getString ("modulo9") );
                cuentas.setModulo10( rs.getString ("modulo10") );
                cuentas.setLast_update( rs.getString ("last_update") );

                cuentas.setDetalle( rs.getString ("detalle") );

                vec_cuentas.add ( cuentas );


            }

        } catch( SQLException e ){

            e.printStackTrace();
            throw new SQLException( "ERROR DURANTE 'searchCuenta' - [PlanDeCuentaDAO].. " + e.getMessage() + " " + e.getErrorCode() );

        }
finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }

    }

    /**
     * Metodo:          existCuenta
     * Descripcion :    M�todo que permite verificar si existe o no una cuenta.
     * @autor :         LREALES
     * @param:          distrito y numero de la cuenta.
     * @return:         un switch tipo boolean
     * @throws:         SQLException
     * @version :       1.0
     */
    public boolean existCuenta ( String distrito, String cuenta ) throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean sw = false;
        String query = "SQL_SEARCH";

        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, distrito);
            st.setString(2, cuenta);
            rs = st.executeQuery();
            if (rs.next()) {
                sw = true;
            }

        } catch (SQLException e) {

            e.printStackTrace();
            throw new SQLException("ERROR DURANTE 'existCuenta' - [PlanDeCuentaDAO].. " + e.getMessage() + " " + e.getErrorCode());

        }
finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }

        return sw;

    }

    /**
     * Metodo:          listCuenta
     * Descripcion :    M�todo que permite listar todas las cuentas.
     * @autor :         LREALES
     * @param:          -
     * @return:         -
     * @throws:         SQLException
     * @version :       1.0
     */
    public void listCuenta () throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_LIST";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            rs = st.executeQuery();
            vec_cuentas = new Vector();
            while (rs.next()) {

                cuentas = new PlanDeCuentas();
                // seteo
                cuentas.setCuenta(rs.getString("cuenta"));
                cuentas.setNombre_corto(rs.getString("nombre_corto"));
                cuentas.setNombre_largo(rs.getString("nombre_largo"));
                cuentas.setActiva(rs.getString("activa"));
                cuentas.setNivel(rs.getString("nivel"));
                cuentas.setAuxiliar(rs.getString("auxiliar"));
                cuentas.setSubledger(rs.getString("subledger"));
                cuentas.setTercero(rs.getString("tercero"));
                cuentas.setCta_dependiente(rs.getString("cta_dependiente"));
                cuentas.setCta_cierre(rs.getString("cta_cierre"));
                cuentas.setModulo1(rs.getString("modulo1"));
                cuentas.setModulo2(rs.getString("modulo2"));
                cuentas.setModulo3(rs.getString("modulo3"));
                cuentas.setModulo4(rs.getString("modulo4"));
                cuentas.setModulo5(rs.getString("modulo5"));
                cuentas.setModulo6(rs.getString("modulo6"));
                cuentas.setModulo7(rs.getString("modulo7"));
                cuentas.setModulo8(rs.getString("modulo8"));
                cuentas.setModulo9(rs.getString("modulo9"));
                cuentas.setModulo10(rs.getString("modulo10"));
                cuentas.setLast_update(rs.getString("last_update"));

                cuentas.setDetalle(rs.getString("detalle"));

                vec_cuentas.add(cuentas);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE 'listCuenta' - [PlanDeCuentaDAO].. " + e.getMessage() + " " + e.getErrorCode());
        }finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }

    }

    /**
     * Metodo:          updateCuenta
     * Descripcion :    M�todo que permite actualizar una cuenta.
     * @autor :         LREALES
     * @param:          -
     * @return:         -
     * @throws:         SQLException
     * @version :       1.0
     */
    public void updateCuenta () throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        String query = "SQL_UPDATE";
        try{
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString ( 1, cuentas.getNombre_largo() );
            st.setString ( 2, cuentas.getNombre_corto() );
            st.setString ( 3, cuentas.getNombre_observacion() );
            st.setString ( 4, cuentas.getAuxiliar() );
            st.setString ( 5, cuentas.getActiva() );
            st.setString ( 6, cuentas.getModulo1() );
            st.setString ( 7, cuentas.getModulo2() );
            st.setString ( 8, cuentas.getModulo3() );
            st.setString ( 9, cuentas.getModulo4() );
            st.setString ( 10, cuentas.getModulo5() );
            st.setString ( 11, cuentas.getModulo6() );
            st.setString ( 12, cuentas.getModulo7() );
            st.setString ( 13, cuentas.getModulo8() );
            st.setString ( 14, cuentas.getModulo9() );
            st.setString ( 15, cuentas.getModulo10() );
            st.setString ( 16, cuentas.getCta_dependiente() );
            st.setInt(17,Integer.parseInt(cuentas.getNivel()) );
            st.setString ( 18, cuentas.getCta_cierre() );
            st.setString ( 19, cuentas.getSubledger() );
            st.setString ( 20, cuentas.getTercero() );
            st.setString ( 21, cuentas.getUser_update() );

            st.setString ( 22, cuentas.getDetalle() );

            st.setString ( 23, cuentas.getDstrct() );
            st.setString ( 24, cuentas.getCuenta() );

            st.executeUpdate ();

        } catch( SQLException e ){

            e.printStackTrace();
            throw new SQLException( "ERROR DURANTE 'updateCuenta' - [PlanDeCuentaDAO].. " + e.getMessage() + " " + e.getErrorCode() );

        }
finally {
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }

    }

    /**
     * Metodo:          anularCuenta
     * Descripcion :    M�todo que permite anular una cuenta.
     * @autor :         LREALES
     * @param:          -
     * @return:         -
     * @throws:         SQLException
     * @version :       1.0
     */
    public void anularCuenta () throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        String query = "SQL_ANULAR";
        try{
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString ( 1, cuentas.getUser_update() );
            st.setString ( 2, cuentas.getDstrct() );
            st.setString ( 3, cuentas.getCuenta() );
            st.executeUpdate ();
        }catch( SQLException e ){
            e.printStackTrace();
            throw new SQLException( "ERROR DURANTE 'anularCuenta' - [PlanDeCuentaDAO].. " + e.getMessage() + " " + e.getErrorCode() );
        }
        finally {
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
    }

    /**
     * Metodo:          searchDetalleCuenta
     * Descripcion :    M�todo que permite listar cuentas por detalles dados unos parametros.
     * @autor :         LREALES
     * @param:          distrito y numero de la cuenta.
     * @return:         -
     * @throws:         SQLException
     * @version :       1.0
     */
    public void searchDetalleCuenta ( String distrito, String cuenta ) throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        vec_cuentas = null;
        boolean sw = false;
        String query = "SQL_SEARCH_DETALLES";

        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString ( 1, distrito );
            st.setString ( 2, cuenta + "%" );
            rs = st.executeQuery ();
            vec_cuentas = new Vector ();
            while( rs.next () ){
                cuentas = new PlanDeCuentas ();
                // seteo
                cuentas.setCuenta( rs.getString ("cuenta") );
                cuentas.setNombre_corto( rs.getString ("nombre_corto") );
                cuentas.setNombre_largo( rs.getString ("nombre_largo") );
                cuentas.setActiva( rs.getString ("activa") );
                cuentas.setNivel( rs.getString ("nivel") );
                cuentas.setAuxiliar( rs.getString ("auxiliar") );
                cuentas.setSubledger( rs.getString ("subledger") );
                cuentas.setTercero( rs.getString ("tercero") );
                cuentas.setCta_dependiente( rs.getString ("cta_dependiente") );
                cuentas.setCta_cierre( rs.getString ("cta_cierre") );
                cuentas.setModulo1( rs.getString ("modulo1") );
                cuentas.setModulo2( rs.getString ("modulo2") );
                cuentas.setModulo3( rs.getString ("modulo3") );
                cuentas.setModulo4( rs.getString ("modulo4") );
                cuentas.setModulo5( rs.getString ("modulo5") );
                cuentas.setModulo6( rs.getString ("modulo6") );
                cuentas.setModulo7( rs.getString ("modulo7") );
                cuentas.setModulo8( rs.getString ("modulo8") );
                cuentas.setModulo9( rs.getString ("modulo9") );
                cuentas.setModulo10( rs.getString ("modulo10") );
                cuentas.setLast_update( rs.getString ("last_update") );

                cuentas.setDetalle( rs.getString ("detalle") );

                vec_cuentas.add ( cuentas );


            }

        } catch( SQLException e ){

            e.printStackTrace();
            throw new SQLException( "ERROR DURANTE 'searchDetalleCuenta' - [PlanDeCuentaDAO].. " + e.getMessage() + " " + e.getErrorCode() );

        }finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }

    }

    /**
     * Metodo:          existAgencia
     * Descripcion :    M�todo que permite verificar si existe o no una agencia en tablagen.
     * @autor :         LREALES
     * @param:          codigo de la agencia.
     * @return:         un switch tipo boolean
     * @throws:         SQLException
     * @version :       1.0
     */
    public boolean existAgencia ( String codigo ) throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean sw = false;
        String query = "SQL_AGENCIA";

        try{
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString ( 1, codigo );
            rs = st.executeQuery ();
            if ( rs.next () ){
                sw = true;
            }

        } catch( SQLException e ){

            e.printStackTrace();
            throw new SQLException( "ERROR DURANTE 'existAgencia' - [PlanDeCuentaDAO].. " + e.getMessage() + " " + e.getErrorCode() );

        }
finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return sw;

    }


    /**
     * Metodo:          existAgencia
     * Descripcion :    M�todo que devuelve el nombre de una agencia dado el codigo.
     * @autor :         LFRIERI
     * @param:          codigo de la agencia.
     * @throws:         SQLException
     * @version :       1.0
     */
    public String NombreAgencia ( String codigo ) throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String ag = "";
        String query = "SQL_AGENCIA";
        try{
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString ( 1, codigo );
            rs = st.executeQuery ();
            if ( rs.next () ){
                ag = rs.getString("referencia");
            }
        }catch( SQLException e ){
            e.printStackTrace();
            throw new SQLException( "ERROR DURANTE 'existAgencia' - [PlanDeCuentaDAO].. " + e.getMessage() + " " + e.getErrorCode() );
        }finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
         return ag;
     }

    /**
     * Metodo:          existUnidadDeNegocio
     * Descripcion :    M�todo que permite verificar si existe o no una unidad de negocio en tablagen.
     * @autor :         LREALES
     * @param:          codigo de la unidad de negocio.
     * @return:         un switch tipo boolean
     * @throws:         SQLException
     * @version :       1.0
     */
    public boolean existUnidadDeNegocio ( String codigo ) throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean sw = false;
        String query = "SQL_UNIDAD";

        try{
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString ( 1, codigo );
            rs = st.executeQuery ();
            if ( rs.next () ){
                sw = true;
            }

        } catch( SQLException e ){

            e.printStackTrace();
            throw new SQLException( "ERROR DURANTE 'existUnidadDeNegocio' - [PlanDeCuentaDAO].. " + e.getMessage() + " " + e.getErrorCode() );

        }finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }

        return sw;

    }

    /**
     * Metodo:          existUnidadDeNegocio
     * Descripcion :    M�todo que permite verificar si existe o no una unidad de negocio en tablagen.
     * @autor :         LREALES
     * @param:          codigo de la unidad de negocio.
     * @return:         un switch tipo boolean
     * @throws:         SQLException
     * @version :       1.0
     */
    public String NombreUnidadDeNegocio ( String codigo ) throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String un = "";
        String query = "SQL_UNIDAD";

        try{

            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString ( 1, codigo );
            rs = st.executeQuery ();
            if ( rs.next () ){
                un = rs.getString("table_code");
            }

        } catch( SQLException e ){

            e.printStackTrace();
            throw new SQLException( "ERROR DURANTE 'existUnidadDeNegocio' - [PlanDeCuentaDAO].. " + e.getMessage() + " " + e.getErrorCode() );

        }finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }

        return un;

    }

    /**
     * Metodo:          existSeccion
     * Descripcion :    M�todo que permite verificar si existe o no una seccion en tablagen.
     * @autor :         LREALES
     * @param:          codigo de la seccion.
     * @return:         un switch tipo boolean
     * @throws:         SQLException
     * @version :       1.0
     */
    public boolean existSeccion ( String codigo ) throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean sw = false;
        String query = "SQL_SECCION";

        try{
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString ( 1, codigo );
            rs = st.executeQuery ();
            if ( rs.next () ){
                sw = true;
            }

        } catch( SQLException e ){

            e.printStackTrace();
            throw new SQLException( "ERROR DURANTE 'existSeccion' - [PlanDeCuentaDAO].. " + e.getMessage() + " " + e.getErrorCode() );

        }finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }

        return sw;

    }

    /**
     * Metodo:          existCliente
     * Descripcion :    M�todo que permite verificar si existe o no un cliente.
     * @autor :         LREALES
     * @param:          codigo del cliente.
     * @return:         un switch tipo boolean
     * @throws:         SQLException
     * @version :       1.0
     */
    public boolean existCliente ( String codigo ) throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean sw = false;
        String query = "SQL_CLIENTE";
        try{
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            codigo = "000" + codigo;
            st.setString ( 1, codigo );
            rs = st.executeQuery ();
            if ( rs.next () ){
                sw = true;
            }

        } catch( SQLException e ){

            e.printStackTrace();
            throw new SQLException( "ERROR DURANTE 'existCliente' - [PlanDeCuentaDAO].. " + e.getMessage() + " " + e.getErrorCode() );

        }
        finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return sw;

    }


    /**
     * Metodo:          NombreCliente
     * @autor :         LFRIERI
     * @param:          codigo del cliente.
     * @throws:         SQLException
     * @version :       1.0
     */
    public String NombreCliente ( String codigo ) throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String cli = "";
        String query = "SQL_CLIENTE";
        try{

            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            codigo = "000" + codigo;
            st.setString ( 1, codigo );
            rs = st.executeQuery ();
            if ( rs.next () ){
                cli = rs.getString("nomcli");
            }

        } catch( SQLException e ){

            e.printStackTrace();
            throw new SQLException( "ERROR DURANTE 'existCliente' - [PlanDeCuentaDAO].. " + e.getMessage() + " " + e.getErrorCode() );

        }
finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }

        return cli;

    }


    /**
     * Metodo:          existElementoDelGasto
     * Descripcion :    M�todo que permite verificar si existe o no el elemento del gasto en tablagen.
     * @autor :         LREALES
     * @param:          codigo del elemento del gasto.
     * @return:         un switch tipo boolean
     * @throws:         SQLException
     * @version :       1.0
     */
    public boolean existElementoDelGasto ( String codigo ) throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean sw = false;
        String query = "SQL_ELEMENTO";

        try{
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString ( 1, codigo );
            rs = st.executeQuery ();
            if ( rs.next () ){
                sw = true;
            }
        } catch( SQLException e ){

            e.printStackTrace();
            throw new SQLException( "ERROR DURANTE 'existElementoDelGasto' - [PlanDeCuentaDAO].. " + e.getMessage() + " " + e.getErrorCode() );

        }
finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return sw;

    }


     /**
     * Metodo:          existElementoDelGasto
     * Descripcion :    M�todo que permite verificar si existe o no el elemento del gasto en tablagen.
     * @autor :         LREALES
     * @param:          codigo del elemento del gasto.
     * @return:         un switch tipo boolean
     * @throws:         SQLException
     * @version :       1.0
     */
    public String NombreElementoDelGasto ( String codigo ) throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String ele = "";
        String query = "SQL_ELEMENTO";

        try{
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString ( 1, codigo );
            rs = st.executeQuery ();
            if ( rs.next () ){
                ele = rs.getString("descripcion");
            }

        } catch( SQLException e ){

            e.printStackTrace();
            throw new SQLException( "ERROR DURANTE 'existElementoDelGasto' - [PlanDeCuentaDAO].. " + e.getMessage() + " " + e.getErrorCode() );

        }finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }

        return ele;

    }

     /**
     *Metodo que obtiene las cuentas filtrada por el nombre corto
     *@autor: David Pi�a
     *@param vlr Valor del nombre corto
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void obtenerCuentasNombreCorto( String vlr ) throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_OBTENER_CUENTAS_NOMBRECORTO";

        try{
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString( 1, "%" + vlr + "%" );
            rs = st.executeQuery();

            this.vec_cuentas = new Vector();
            while ( rs.next() ){
                this.cuentas = new PlanDeCuentas();
                this.cuentas.setCuenta( rs.getString("cuenta") );
                this.cuentas.setNombre_largo( rs.getString("nombre_largo") );
                this.cuentas.setNombre_corto( rs.getString("nombre_corto") );
                this.vec_cuentas.add( cuentas );

            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE obtenerCuentasNombreCorto( String vlr ) " + e.getMessage() + " " + e.getErrorCode());
        }finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
    }
     /**
     * Metodo:          insertCuenta
     * Descripcion :    M�todo que permite ingresar un registro a la tabla cuentas del esquema con.
     * @autor :         AMATURANA
     * @param:          -
     * @return:         -
     * @throws:         SQLException
     * @version :       1.0
     */
    public String insertCuentaQuery () throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        String query = "SQL_INSERT";

        try{

            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString ( 1, cuentas.getDstrct() );
            st.setString ( 2, cuentas.getCuenta() );
            st.setString ( 3, cuentas.getNombre_largo() );
            st.setString ( 4, cuentas.getNombre_corto() );
            st.setString ( 5, cuentas.getNombre_observacion() );
            st.setString ( 6, cuentas.getAuxiliar() );
            st.setString ( 7, cuentas.getBase() );
            st.setString ( 8, cuentas.getModulo1() );
            st.setString ( 9, cuentas.getModulo2() );
            st.setString ( 10, cuentas.getModulo3() );
            st.setString ( 11, cuentas.getModulo4() );
            st.setString ( 12, cuentas.getModulo5() );
            st.setString ( 13, cuentas.getModulo6() );
            st.setString ( 14, cuentas.getModulo7() );
            st.setString ( 15, cuentas.getModulo8() );
            st.setString ( 16, cuentas.getModulo9() );
            st.setString ( 17, cuentas.getModulo10() );
            st.setString ( 18, cuentas.getCta_dependiente() );
            st.setString ( 19, cuentas.getNivel() );
            st.setString ( 20, cuentas.getCta_cierre() );
            st.setString ( 21, cuentas.getSubledger() );
            st.setString ( 22, cuentas.getTercero() );
            st.setString ( 23, cuentas.getUser_update() );
            st.setString ( 24, cuentas.getCreation_user() );
            st.setString ( 25, cuentas.getDetalle() );

            query = st.toString();
            logger.info("insertCuentaQuery (): " + query);
            return query;

        } catch( SQLException e ){

            e.printStackTrace();
            logger.info("insertCuentaQuery (): NR");
            throw new SQLException( "ERROR DURANTE 'insertCuenta' - [PlanDeCuentaDAO].. " + e.getMessage() + " " + e.getErrorCode() );

        }finally {
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }

    }




    /**
     * Metodo:          reg_status_Cuenta
     * Descripcion :    M�todo que permite verificar si una cuenta esta o no anulada.
     * @autor :         LREALES
     * @param:          distrito y numero de la cuenta.
     * @return:         un string, el reg status
     * @throws:         SQLException
     * @version :       1.0
     */
    public String reg_status_Cuenta ( String distrito, String cuenta ) throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String reg_status = "";
        String query = "SQL_SEARCH_REG_STATUS";

        try{
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString ( 1, distrito );
            st.setString ( 2, cuenta );
            rs = st.executeQuery ();
            if ( rs.next () ){
                reg_status = rs.getString( "reg_status" )!=null?rs.getString( "reg_status" ).toUpperCase():"";
            }

        } catch( SQLException e ){
            e.printStackTrace();
            throw new SQLException( "ERROR DURANTE 'reg_status_Cuenta' - [PlanDeCuentaDAO].. " + e.getMessage() + " " + e.getErrorCode() );

        }finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return reg_status;

    }

   public void consultaCuenta ( String distrito, String cuenta,String Tip,String Age,String Uni,String Cli,String Ele )throws SQLException{
        System.out.println("entro2");
        PreparedStatement st = null;
        ResultSet rs = null;
        lista = new LinkedList();
        String remplazar = "";
        Connection con=null;
        String query = "SQL_CONSULTA_CUENTA";
        try {
            if(!Tip.equals("%")){
                if(Age.equals("%") && Uni.equals("%") && Cli.equals("%") && Ele.equals("%")){
                    remplazar =  " where c.Tip ='"+Tip+"'";
                }else{
                    remplazar =  "where c.Tip ='"+Tip+"' AND";
                }

            }else {
                remplazar =  " where ";
            }

            if(!Age.equals("%")){
                if(Uni.equals("%") && Cli.equals("%") && Ele.equals("%")){
                    remplazar = remplazar +  "  c.Age ='"+Age+"'";
                }else{
                    remplazar = remplazar + "  c.Age ='"+Age+"' AND";
                }
            }
            if(!Uni.equals("%")){
                if(Cli.equals("%") && Ele.equals("%")){
                    remplazar = remplazar + "  c.Uni ='"+Uni+"'";
                }else{
                    remplazar = remplazar + "  c.Uni ='"+Uni+"' AND";
                }
            }
            if(!Cli.equals("%")){
                if(Ele.equals("%")){
                    remplazar = remplazar + "  c.Cli ='"+Cli+"'";
                }else{
                    remplazar = remplazar + "  c.Cli ='"+Cli+"' AND";
                }
            }
            if(!Ele.equals("%")){
                    remplazar = remplazar + "  c.Ele ='"+Ele+"'";
           }



            System.out.println("clave cuenta  "+cuenta);
            String agregar = "";


            String sql = this.obtenerSQL(query);
            agregar = sql.replaceAll("#CONDICION#", remplazar);

            con = this.conectarJNDI(query);
            st = con.prepareStatement(agregar);//JJCastro fase2
            st.setString ( 1, cuenta );
            System.out.println("query  "+st.toString());
            rs = st.executeQuery();

            while( rs.next () ){

                cuentas = new PlanDeCuentas ();
                // seteo
                cuentas.setCuenta( rs.getString ("cuenta") );
                cuentas.setActiva( rs.getString ("activa") );
                cuentas.setNombre_corto( rs.getString ("nombre_corto") );
                cuentas.setNombre_largo( rs.getString ("nombre_largo") );
                cuentas.setNombre_observacion( rs.getString ("nombre_observacion") );
                cuentas.setNivel( rs.getString ("nivel") );
                cuentas.setAuxiliar( rs.getString ("auxiliar") );
                cuentas.setSubledger( rs.getString ("subledger") );
                cuentas.setTercero( rs.getString ("tercero") );
                cuentas.setCta_dependiente( rs.getString ("cta_dependiente") );
                cuentas.setCta_cierre( rs.getString ("cta_cierre") );
                cuentas.setModulo1( rs.getString ("modulo1") );
                cuentas.setModulo2( rs.getString ("modulo2") );
                cuentas.setModulo3( rs.getString ("modulo3") );
                cuentas.setModulo4( rs.getString ("modulo4") );
                cuentas.setModulo5( rs.getString ("modulo5") );
                cuentas.setModulo6( rs.getString ("modulo6") );
                cuentas.setModulo7( rs.getString ("modulo7") );
                cuentas.setModulo8( rs.getString ("modulo8") );
                cuentas.setModulo9( rs.getString ("modulo9") );
                cuentas.setModulo10( rs.getString ("modulo10") );
                cuentas.setLast_update( rs.getString ("last_update") );
                cuentas.setDetalle( rs.getString ("detalle") );
                lista.add ( cuentas );


            }
        } catch( SQLException e ){

            e.printStackTrace();
            throw new SQLException( "ERROR DURANTE 'consultaCuenta' - [PlanDeCuentaDAO].. " + e.getMessage() + " " + e.getErrorCode() );

        }finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
    }


     public void consultaCuentaSinComodin ( String distrito, String cuenta, String cuenta2 )throws SQLException{
        System.out.println("entro2");
        PreparedStatement st = null;
        ResultSet rs = null;
        lista = new LinkedList();
        String remplazar = "";
        Connection con=null;
        String query = "SQL_CONSULTA_CUENTAS";

        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString ( 1, distrito );
            st.setString ( 2, cuenta );
            st.setString ( 3, cuenta2 );
            System.out.println("query  "+st.toString());
            rs = st.executeQuery();
            while( rs.next () ){

                cuentas = new PlanDeCuentas ();
                // seteo
                cuentas.setCuenta( rs.getString ("cuenta") );
                cuentas.setActiva( rs.getString ("activa") );
                cuentas.setNombre_corto( rs.getString ("nombre_corto") );
                cuentas.setNombre_largo( rs.getString ("nombre_largo") );
                cuentas.setNombre_observacion( rs.getString ("nombre_observacion") );
                cuentas.setNivel( rs.getString ("nivel") );
                cuentas.setAuxiliar( rs.getString ("auxiliar") );
                cuentas.setSubledger( rs.getString ("subledger") );
                cuentas.setTercero( rs.getString ("tercero") );
                cuentas.setCta_dependiente( rs.getString ("cta_dependiente") );
                cuentas.setCta_cierre( rs.getString ("cta_cierre") );
                cuentas.setModulo1( rs.getString ("modulo1") );
                cuentas.setModulo2( rs.getString ("modulo2") );
                cuentas.setModulo3( rs.getString ("modulo3") );
                cuentas.setModulo4( rs.getString ("modulo4") );
                cuentas.setModulo5( rs.getString ("modulo5") );
                cuentas.setModulo6( rs.getString ("modulo6") );
                cuentas.setModulo7( rs.getString ("modulo7") );
                cuentas.setModulo8( rs.getString ("modulo8") );
                cuentas.setModulo9( rs.getString ("modulo9") );
                cuentas.setModulo10( rs.getString ("modulo10") );
                cuentas.setLast_update( rs.getString ("last_update") );
                cuentas.setDetalle( rs.getString ("detalle") );
                lista.add ( cuentas );


            }
        } catch( SQLException e ){

            e.printStackTrace();
            throw new SQLException( "ERROR DURANTE 'consultaCuenta' - [PlanDeCuentaDAO].. " + e.getMessage() + " " + e.getErrorCode() );

        }finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
    }

    /**
     * Getter for property lista.
     * @return Value of property lista.
     */
    public java.util.LinkedList getLista() {
        return lista;
    }

    /**
     * Setter for property lista.
     * @param lista New value of property lista.
     */
    public void setLista(java.util.LinkedList lista) {
        this.lista = lista;
    }

    public void consultaCuentaSinComodin ( String distrito, String cuenta )throws SQLException{
        System.out.println("entro2");
        PreparedStatement st = null;
        ResultSet rs = null;
        lista = new LinkedList();
        String remplazar = "";
        Connection con=null;
        String query = "SQL_CONSULTA_CUENTAS";

        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString ( 1, distrito );
            st.setString ( 2, cuenta );
            System.out.println("query  "+st.toString());
            rs = st.executeQuery();

            while( rs.next () ){

                cuentas = new PlanDeCuentas ();
                // seteo
                cuentas.setCuenta( rs.getString ("cuenta") );
                cuentas.setActiva( rs.getString ("activa") );
                cuentas.setNombre_corto( rs.getString ("nombre_corto") );
                cuentas.setNombre_largo( rs.getString ("nombre_largo") );
                cuentas.setNombre_observacion( rs.getString ("nombre_observacion") );
                cuentas.setNivel( rs.getString ("nivel") );
                cuentas.setAuxiliar( rs.getString ("auxiliar") );
                cuentas.setSubledger( rs.getString ("subledger") );
                cuentas.setTercero( rs.getString ("tercero") );
                cuentas.setCta_dependiente( rs.getString ("cta_dependiente") );
                cuentas.setCta_cierre( rs.getString ("cta_cierre") );
                cuentas.setModulo1( rs.getString ("modulo1") );
                cuentas.setModulo2( rs.getString ("modulo2") );
                cuentas.setModulo3( rs.getString ("modulo3") );
                cuentas.setModulo4( rs.getString ("modulo4") );
                cuentas.setModulo5( rs.getString ("modulo5") );
                cuentas.setModulo6( rs.getString ("modulo6") );
                cuentas.setModulo7( rs.getString ("modulo7") );
                cuentas.setModulo8( rs.getString ("modulo8") );
                cuentas.setModulo9( rs.getString ("modulo9") );
                cuentas.setModulo10( rs.getString ("modulo10") );
                cuentas.setLast_update( rs.getString ("last_update") );
                cuentas.setDetalle( rs.getString ("detalle") );
                lista.add ( cuentas );


            }
        } catch( SQLException e ){

            e.printStackTrace();
            throw new SQLException( "ERROR DURANTE 'consultaCuenta' - [PlanDeCuentaDAO].. " + e.getMessage() + " " + e.getErrorCode() );

        }finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
    }

    public String NombreSeccion ( String codigo ) throws SQLException{//20100824
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String ele = "";
        String query = "SQL_SECCION";

        try{
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString ( 1, codigo );
            rs = st.executeQuery ();
            if ( rs.next () ){
                ele = rs.getString("descripcion");
            }

        } catch( SQLException e ){

            e.printStackTrace();
            throw new SQLException( "ERROR DURANTE 'NombreSeccion' - [PlanDeCuentaDAO].. " + e.getMessage() + " " + e.getErrorCode() );

        }finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }

        return ele;

    }

}
