/****
 * Nombre clase :             SubledgerDAO.java
 * Descripcion :              Clase que maneja los DAO ( Data Access Object )
 *                            los cuales contienen los metodos que interactuan
 *                            con la BD.
 * Autor :                    Ing. Diogenes Antonio Bastidas Morales
 * Fecha :                    6 de junio de 2006, 04:45 PM
 * Version :  1.0
 * Copyright : Fintravalores S.A.
 ****/

package com.tsp.finanzas.contab.model.DAO;
import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.*;
import com.tsp.finanzas.contab.model.beans.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;
import com.tsp.exceptions.EncriptionException;
import com.tsp.operation.model.DAOS.MainDAO;




public class SubledgerDAO extends MainDAO {
    private Subledger subledger;
private TreeMap cuentasTipoSubledger;
    private LinkedList listasubledger;
    private TablaGen tsubledger;
    private LinkedList cuentastsubledger;
    
    /** Creates a new instance of SubledgerDAO */
    public SubledgerDAO() {
        super("SubledgerDAO.xml");
    }
    public SubledgerDAO(String dataBaseName) {
        super("SubledgerDAO.xml", dataBaseName);
    }
    
    /**
     * Metodo insertarIngreso, ingresa un registro en la tabla subledger
     * @param:
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    
    public void insertarSubledger() throws Exception {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_INGRESAR";
        
        try{
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, subledger.getDstrct());
            st.setString(2, subledger.getCuenta());
            st.setString(3, subledger.getTipo_subledger());
            st.setString(4, subledger.getId_subledger());
            st.setString(5, subledger.getNombre());
            st.setString(6, subledger.getCreation_user());
            st.setString(7, subledger.getCreation_date());
            st.setString(8, subledger.getUser_update());
            st.setString(9, subledger.getLast_update());
            st.setString(10, subledger.getBase());
            
            
            //////System.out.println(st.toString());
            st.executeUpdate();
        }catch(Exception e) {
            throw new Exception("Error al Insertar Subledger [SubledgerDAO.xml]... \n"+e.getMessage());
        }finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
    }
    
    /**
     * Getter for property subledger.
     * @return Value of property subledger.
     */
    public Subledger getSubledger() {
        return subledger;
    }
    
    /**
     * Setter for property subledger.
     * @param subledger New value of property subledger.
     */
    public void setSubledger(Subledger subledger) {
        this.subledger = subledger;
    }
    
    
    /**
     * Metodo modificarIngreso, mopdifica un registro en la tabla ingreso
     * @param:
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public void modificarSubledger() throws Exception {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_MODIFICAR";
        
        try{
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, subledger.getReg_status());
            st.setString(2, subledger.getCuenta());
            st.setString(3, subledger.getTipo_subledger());
            st.setString(4, subledger.getId_subledger());
            st.setString(5, subledger.getNombre());
            st.setString(6, subledger.getUser_update());
            st.setString(7, subledger.getLast_update());
            st.setString(8, subledger.getDstrct());
            st.setString(9, subledger.getCuenta());
            st.setString(10, subledger.getTipo_subledger());
            st.setString(11, subledger.getId_subledger());
            
            //////System.out.println(st.toString());
            st.executeUpdate();
        }catch(Exception e) {
            throw new Exception("Error al MODIFICAR Ingreso [IngresoDAO.xml]... \n"+e.getMessage());
        }finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
    }
    
    
    /**
     * Metodo que obtiene los tipo subledger de una cuenta
     * @param: numero de cuenta
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public void buscarCuentasTipoSubledger(String cuenta) throws Exception{
        PreparedStatement st = null;
        ResultSet rs = null;
        cuentasTipoSubledger = null;
        cuentastsubledger = null;
        Connection con = null;
        String sql = "";
        String query = "SQL_BUSCARCUENTAS_TIPO_SUBLEDGER" ;
        try {

            String tipos = buscarTSubledgerCuenta(cuenta);
            if (!tipos.equals("")) {
                sql = this.obtenerSQL("SQL_BUSCARCUENTAS_TIPO_SUBLEDGER").replaceAll("#TIPOS#", tipos);
                con = this.conectarJNDI(query);
                st = con.prepareStatement(sql);
                rs = st.executeQuery();
                this.cuentasTipoSubledger = new TreeMap();
                cuentastsubledger = new LinkedList();
                while (rs.next()) {
                    TablaGen tblgen = new TablaGen();
                    tblgen.setTable_code(rs.getString("table_code"));
                    tblgen.setDescripcion(rs.getString("descripcion"));
                    this.cuentasTipoSubledger.put(tblgen.getDescripcion(), tblgen.getTable_code());
                    cuentastsubledger.add(tblgen);
                    tblgen = null;//Liberar Espacio JJCastro
                }
            }
        } catch (Exception e) {
            throw new Exception("Error Buscar los tipo de cuentas Subledger [SubledgerDAO.xml]... \n" + e.getMessage());
        }finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }

    }
    
    /**
     * Metodo existeSubledgerAnulado, retorna true si existe el subledger
     * @param: distrito, cuenta, subledger
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public boolean existeSubledgerAnulado(String dstrct, String cuenta, String tipo, String id_subledger) throws Exception {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean sw = false;
        String query = "SQL_BUSCAR_SUBLEDGER_ANULADO";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, dstrct);
            st.setString(2, cuenta);
            st.setString(3, tipo);
            st.setString(4, id_subledger);
            rs = st.executeQuery();
            if (rs.next()) {
                sw = true;
            }

        } catch (Exception e) {
            throw new Exception("Error al buscar subledger anulado [SubledgerDAO.xml]... \n" + e.getMessage());
        }finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return sw;
    }
    
    /**
     * Getter for property cuentasTipoSubledger.
     * @return Value of property cuentasTipoSubledger.
     */
    public java.util.TreeMap getCuentasTipoSubledger() {
        return cuentasTipoSubledger;
    }
    
    /**
     * Setter for property cuentasTipoSubledger.
     * @param cuentasTipoSubledger New value of property cuentasTipoSubledger.
     */
    public void setCuentasTipoSubledger(java.util.TreeMap cuentasTipoSubledger) {
        this.cuentasTipoSubledger = cuentasTipoSubledger;
    }
    
    /**
     * Metodo busquedaSubledger, busca la lista de subledger
     * @param: distrito, cuenta, subledger
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public void busquedaSubledger(String dstrct, String cuenta, String tipo, String id_subledger) throws Exception {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        listasubledger = null;
        String query = "SQL_BUSQUEDA";
        try{
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, dstrct);
            st.setString(2, cuenta+"%");
            st.setString(3, tipo+"%");
            st.setString(4, id_subledger+"%");
            listasubledger = new LinkedList();            
            rs = st.executeQuery();
            while(rs.next()){
                Subledger sub = new Subledger();
                sub.setReg_status(rs.getString("reg_status"));
                sub.setTipo_subledger(rs.getString("tipo_subledger"));
                sub.setId_subledger(rs.getString("id_subledger"));
                sub.setNombre(rs.getString("nombre"));
                sub.setCuenta(rs.getString("cuenta"));
                sub.setDes_tipo(rs.getString("destipo"));
                sub.setDstrct(rs.getString("dstrct"));
                listasubledger.add(sub);
                sub = null;//Liberar Espacio JJCastro
            }
            
        }catch(Exception e) {
            throw new Exception("Error al listar subledger [SubledgerDAO.xml]... \n"+e.getMessage());
        }finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        
    }
    
    
    /**
     * Metodo buscarSubledger, busca un objeto tipo subledger
     * @param: distrito, cuenta, tipo, id_subledger
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public void buscarSubledger(String dstrct, String cuenta, String tipo, String id_subledger) throws Exception {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        subledger = null;
        String query = "SQL_BUSCAR";
        try{
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, dstrct);
            st.setString(2, cuenta);
            st.setString(3, tipo);
            st.setString(4, id_subledger);
            rs = st.executeQuery();
            if(rs.next()){
                this.subledger = new Subledger();
                subledger.setReg_status(rs.getString("reg_status"));
                subledger.setTipo_subledger(rs.getString("tipo_subledger"));
                subledger.setId_subledger(rs.getString("id_subledger"));
                subledger.setNombre(rs.getString("nombre"));
                subledger.setCuenta(rs.getString("cuenta"));
                subledger.setDes_tipo(rs.getString("destipo"));
            }
            
        }catch(Exception e) {
            throw new Exception("Error al listar subledger [SubledgerDAO.xml]... \n"+e.getMessage());
        }finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        
    }
    
    /**
     * Getter for property listasubledger.
     * @return Value of property listasubledger.
     */
    public java.util.LinkedList getListasubledger() {
        return listasubledger;
    }
    
    /**
     * Setter for property listasubledger.
     * @param listasubledger New value of property listasubledger.
     */
    public void setListasubledger(java.util.LinkedList listasubledger) {
        this.listasubledger = listasubledger;
    }
    
    /**
     * Metodo anularSubledger, Anula un registro en la tabla subledger
     * @param:
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    
    public void anularSubledger() throws Exception {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_ANULAR";
        
        try{
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, subledger.getUser_update());
            st.setString(2, subledger.getLast_update());
            st.setString(3, subledger.getDstrct());
            st.setString(4, subledger.getCuenta());
            st.setString(5, subledger.getTipo_subledger());
            st.setString(6, subledger.getId_subledger());
            
            //////System.out.println(st.toString());
            st.executeUpdate();
        }catch(Exception e) {
            throw new Exception("Error al ANULAR Subledger [SubledgerDAO.xml]... \n"+e.getMessage());
        }finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
    }
    
    /**
     * Metodo que obtiene un vector con los codigos de los tipos de subledger de una cuenta
     * @param: numero de cuenta
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public Vector buscarCodigosCuenta(String cuenta) throws Exception{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        cuentasTipoSubledger = null;
        Vector vec = null;
        String dato = "";
        String query = "SQL_RELACIONCTSUBLEDGER";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, cuenta);
            rs = st.executeQuery();
            vec = new Vector();

            if (rs.next()) {
                dato = rs.getString("dato");
                for (int i = 0; i < dato.length(); i = i + 2) {
                    vec.add(dato.substring(i, i + 2));
                }
            }
        } catch (Exception e) {
            throw new Exception("Error Buscar los codigos de la cuentas Subledger [SubledgerDAO.xml]... \n" + e.getMessage());
        }finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return vec;
    }
    
    
    /**
     * Metodo que obtiene un String con los codigos de los tipos de subledger de una cuenta
     *de la siguinte forma  '01','02'
     * @param: numero de cuenta
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public String buscarTSubledgerCuenta(String cuenta) throws Exception{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        cuentasTipoSubledger = null;
        String dato = "";
        String res = "";
        String query = "SQL_RELACIONCTSUBLEDGER";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, cuenta);
            rs = st.executeQuery();            
            if(rs.next()){
                dato = rs.getString("dato");
                for(int i=0 ; i<dato.length(); i=i+2) {
                    res += "'"+dato.substring(i,i+2)+"',";
                }
                if(res!=""){
                    res = res.substring(0,res.length()-1);
                }
            }
            
        } catch( Exception e ){
            throw new Exception("Error Buscar los codigos de la cuentas Subledger [SubledgerDAO.xml]... \n"+e.getMessage());
        }finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        //////System.out.println(res);
        return res;
    }
    
    /**
     * Metodo que obtiene un String con los codigos de los tipo subledger
     * tal cual como se almacena en la base de datos
     * @param: numero de cuenta
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public String buscarTiposCuenta(String cuenta) throws Exception{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        cuentasTipoSubledger = null;
        String dato = "";
        String query = "SQL_RELACIONCTSUBLEDGER";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, cuenta);
            rs = st.executeQuery();
            
            if(rs.next()){
                dato = rs.getString("dato").trim();
            }
            
        } catch( Exception e ){
            throw new Exception("Error Buscar los codigos de la cuentas Subledger [SubledgerDAO.xml]... \n"+e.getMessage());
        }finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        
        return dato;
    }
    
    /*******************************<CUENTAS TIPO SUBLEDGER>************************************/
    /**
     * Metodo agregarCuentasTSubledger, agrega la relacion cuenta tiposubledger en la tablagen
     * @param:
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public void agregarCuentasTSubledger()throws Exception {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_INGRESAR_TIPOCUENTA_SUBLEDGER";        
        try{
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, tsubledger.getTable_code() );
            st.setString(2, tsubledger.getReferencia());
            st.setString(3, tsubledger.getDescripcion() );
            st.setString(4, tsubledger.getUsuario());
            st.setString(5, tsubledger.getDato());
            //////System.out.println(st.toString());
            st.executeUpdate();
        }catch(Exception e) {
            throw new Exception("Error al Insertar Cuentas tipo Subledger [SubledgerDAO.xml]... \n"+e.getMessage());
        }finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
    }
    /*public static void main(String []args) throws Exception{
     
        try{
            SubledgerDAO prom = new SubledgerDAO();
            //////System.out.println(prom.buscarTSubledgerCuenta("123"));
        }
        catch(Exception e){
            e.printStackTrace();
        }
     
    }*/
    
    /**
     * Getter for property tsubledger.
     * @return Value of property tsubledger.
     */
    public com.tsp.operation.model.beans.TablaGen getTsubledger() {
        return tsubledger;
    }
    
    /**
     * Setter for property tsubledger.
     * @param tsubledger New value of property tsubledger.
     */
    public void setTsubledger(com.tsp.operation.model.beans.TablaGen tsubledger) {
        this.tsubledger = tsubledger;
    }
    
    /**
     * Metodo modificarCuentasTSubledger, modifica la relacion cuenta con tipo subledger
     * @param:
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public void modificarCuentasTSubledger() throws Exception {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_MODIFICAR_TIPOCUENTA_SUBLEDGER";
        
        try{
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, tsubledger.getUsuario()); 
            st.setString(2, tsubledger.getDato());
            st.setString(3, tsubledger.getTable_code() );
            st.executeUpdate();
        }catch(Exception e) {
            throw new Exception("Error al MODIFICAR Cuentas tipo Subledger [SubledgerDAO.xml]... \n"+e.getMessage());
        }finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
    }
   
    
    /**
     * Getter for property cuentastsubledger.
     * @return Value of property cuentastsubledger.
     */
    public java.util.LinkedList getCuentastsubledger() {
        return cuentastsubledger;
    }
    
    /**
     * Setter for property cuentastsubledger.
     * @param cuentastsubledger New value of property cuentastsubledger.
     */
    public void setCuentastsubledger(java.util.LinkedList cuentastsubledger) {
        this.cuentastsubledger = cuentastsubledger;
    }
    
    
    /**
     * Metodo buscarIdSubledger, busca la lista id subledger con sus nombres,
     * realizando la busqueda por id o por nombre
     * @param: distrito, cuenta, tipo, 
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public void buscarIdSubledger(String dstrct, String cuenta, String tipo, String valor) throws Exception {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        listasubledger = null;
        String query = "SQL_BUSCAR_IDSUBLEDGER";
        try{
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, dstrct);
            st.setString(2, cuenta);
            st.setString(3, tipo);
            st.setString(4, valor+"%");
            st.setString(5, valor+"%");
            listasubledger = new LinkedList();
            //////System.out.println(st);
            rs = st.executeQuery();
            while(rs.next()){
                Subledger sub = new Subledger();
                sub.setId_subledger(rs.getString("id_subledger"));
                sub.setNombre(rs.getString("nombre"));
                listasubledger.add(sub);
                sub = null;//Liberar Espacio JJCastro
            }
            
        }catch(Exception e) {
            throw new Exception("Error al listar los id subledger [SubledgerDAO.xml]... \n"+e.getMessage());
        }finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        
    }
    
    /**
     * Metodo existeSubledger, retorna true si existe el subledger
     * @param: distrito, cuenta, subledger
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public boolean existeSubledger(String dstrct, String cuenta, String tipo, String id_subledger) throws Exception {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean sw=false;
        String query = "SQL_EXISTE_SUBLEDGER";
        try{
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, dstrct);
            st.setString(2, cuenta);
            st.setString(3, tipo);
            st.setString(4, id_subledger);
            //////System.out.println(st.toString());
            rs = st.executeQuery();
            if(rs.next()){
                sw=true;
            }
            
        }catch(Exception e) {
            throw new Exception("Error al buscar subledger anulado [SubledgerDAO.xml]... \n"+e.getMessage());
        }finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return sw;
    }
    
     /**
     * Metodo que obtiene los tipo subledger de una cuenta
     * @param: numero de cuenta
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    
     public void busquedaCuentasTipoSubledger(String dstrct ,String cuenta) throws Exception{
        PreparedStatement st = null;
        ResultSet rs = null;
        ResultSet rs1 = null;
        cuentasTipoSubledger = null;
        Connection con =null;
        String sql = "";
        cuentastsubledger = null;
        String query = "SQL_BUSQUEDA_CTSUBLEDGER";
        try {
            con = this.conectarJNDI(query);

            if (con==null)
                throw new Exception ("Sin conexion");

            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, cuenta+"%");
            st.setString(2, dstrct);
            rs = st.executeQuery();
            cuentastsubledger = new  LinkedList();

            while(rs.next()){
                String tipos = buscarTSubledgerCuenta( rs.getString("table_code") );
                sql = this.obtenerSQL("SQL_BUSCARCUENTAS_TIPO_SUBLEDGER").replaceAll("#TIPOS#",tipos);
                st = con.prepareStatement(sql);

                rs1 = st.executeQuery();
                while(rs1.next()){
                    TablaGen tblgen = new TablaGen();
                    tblgen.setTable_code(rs1.getString("table_code"));
                    tblgen.setDescripcion(rs1.getString("descripcion"));
                    tblgen.setTable_type(rs.getString("table_code"));//cuenta
                    tblgen.setDescuenta(rs.getString("nombre_corto"));//descuenta 
                    cuentastsubledger.add( tblgen );
                    tblgen = null;//Liberar Espacio JJCastro
                }
            }
        } catch( Exception e ){
            throw new Exception("Error Buscar los tipo de cuentas Subledger [SubledgerDAO.xml]... \n"+e.getMessage());
        }finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }

    }
     
     
    /**
     * Metodo que obtiene la cuentas de un tipo de subledger
     * @param: buscarCuentas_Tipo
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public Vector buscarCuentas_Tipo(String tipo) throws Exception{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        Vector vec = null;
        String dato = "";
        String query = "SQL_CTSUBLEDGER";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            //System.out.println(st.toString());
            rs = st.executeQuery();
            vec = new Vector();
            
            while(rs.next()){
                dato = rs.getString("dato");
                for(int i=0 ; i<dato.length(); i=i+2) {
                    //System.out.println(dato.substring(i,i+2));
                    if(dato.substring(i,i+2).equals(tipo)){
                        TablaGen tblgen = new TablaGen();
                        tblgen.setTable_code(rs.getString("table_code"));//cuenta
                        vec.add(tblgen);
                        tblgen = null;//Liberar Espacio JJCastro
                    }
                }//System.out.println("");
                
            }
        } catch( Exception e ){
            throw new Exception("Error Buscar los codigos de la cuentas Subledger [SubledgerDAO.xml]... \n"+e.getMessage());
        }finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return vec;
    } 
     
}
