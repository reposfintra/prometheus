/**************************************************************************
 * Nombre: ......................PeriodoContableDAO.java                  *
 * Descripci�n: .................DAO para la tabla periodo_contable       *
 * Autor:........................Osvaldo P�rez Ferrer                     *
 * Fecha:........................17 de Junio de 2006, 09:47 AM            *
 * Versi�n: Java ................1.0                                      *
 * Copyright: Fintravalores S.A. S.A.                                *
 **************************************************************************/

/*
 * PeriodoContableDAO.java
 *
 * Created on 17 de junio de 2006, 09:46 AM
 */

package com.tsp.finanzas.contab.model.DAO;


import com.tsp.finanzas.contab.model.beans.*;
import com.tsp.operation.model.DAOS.MainDAO;
import java.io.*;
import java.util.*;
import java.sql.*;
import com.tsp.util.*;
/**
 *
 * @author  Osvaldo
 */
public class PeriodoContableDAO  extends MainDAO {
    
    private PeriodoContable p;
    
    private Vector periodos;
    /** Creates a new instance of PeriodoContableDAO */
    public PeriodoContableDAO() {
        super("PeriodoContableDAO.xml");
    }
    public PeriodoContableDAO(String dataBaseName) {
        super("PeriodoContableDAO.xml", dataBaseName);
    }
    
    
    /**
     * Metodo: insertarPeriodoContable, permite ingresar un registro a la tabla periodo_contable.
     * @autor : Osvaldo P�rez Ferrer
     * @param :
     * @version : 1.0
     */
    public void insertarPeriodoContable(PeriodoContable p) throws SQLException{
        Connection con = null;
        String query = "SQL_INSERT_PERIODOCONTABLE";
        PreparedStatement st = null;
        
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, p.getDistrito());
            st.setString(2, p.getAnio());
            st.setString(3, p.getMes());
            st.setString(4, p.getAc());
            st.setString(5, p.getIva());
            st.setString(6, p.getFec_pre_iva());
            st.setString(7, p.getRetencion());
            st.setString(8, p.getFec_pre_retencion());
            st.setString(9, p.getComercio());
            st.setString(10, p.getFec_pre_comercio());
            st.setString(11, p.getRenta());
            st.setString(12, p.getFec_pre_renta());
            st.setString(13, p.getCreation_user());
            st.setString(14, p.getBase());
            
            st.executeUpdate();
            
        }catch(SQLException ex){
            throw new SQLException("ERROR DURANTE INSERCION EN periodo_contable " + ex.getMessage() + " " + ex.getErrorCode());
        }
finally{
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
    }
    
    /**
     * Metodo: getAllPeriodos, obtiene todos los registros de la tabla periodo_contable.
     * @autor : Osvaldo P�rez Ferrer
     * @param :
     * @version : 1.0
     */
    public Vector getAllPeriodos() throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        periodos = new Vector();
        String query = "SQL_LISTAR_PERIODOS";

        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            rs = st.executeQuery();

            while (rs.next()) {
                PeriodoContable p = new PeriodoContable();

                p.setDistrito(rs.getString("dstrct"));
                p.setAnio(rs.getString("anio"));
                p.setMes(rs.getString("mes"));
                p.setAc(rs.getString("ac"));
                p.setIva(rs.getString("iva"));
                p.setFec_pre_iva(rs.getString("fec_pre_iva"));
                p.setRetencion(rs.getString("retencion"));
                p.setFec_pre_retencion(rs.getString("fec_pre_retencion"));
                p.setComercio(rs.getString("comercio"));
                p.setFec_pre_comercio(rs.getString("fec_pre_comercio"));
                p.setRenta(rs.getString("renta"));
                p.setFec_pre_renta(rs.getString("fec_pre_renta"));

                periodos.add(p);
                p = null;//Liberar Espacio JJCastro
            }

        } catch (SQLException ex) {
            throw new SQLException("ERROR DURANTE CONSULTA EN periodo_contable " + ex.getMessage() + " " + ex.getErrorCode());
        }
finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return periodos;
    }
    
    /**
     * M�todo que permite obtener un periodo
     *@param dstrct Distrito
     *@param anio A�o del periodo
     *@param mes Mes del periodo contable
     */
    public void obtenerPeriodo( String dstrct, String anio, String mes ) throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_OBTENER_PERIODO";
        
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString( 1, dstrct );
            st.setString( 2, anio );
            st.setString( 3, mes );
            rs = st.executeQuery();
            p = null;
            if(rs.next()){
                p = new PeriodoContable();
                p.setDistrito( rs.getString("dstrct") );
                p.setAnio( rs.getString("anio") );
                p.setMes( rs.getString("mes") );
                p.setAc( rs.getString("ac") );
                p.setIva( rs.getString("iva") );
                p.setFec_pre_iva( rs.getString("fec_pre_iva") );
                p.setRetencion( rs.getString("retencion") );
                p.setFec_pre_retencion( rs.getString("fec_pre_retencion") );
                p.setComercio( rs.getString("comercio") );
                p.setFec_pre_comercio( rs.getString("fec_pre_comercio") );
                p.setRenta( rs.getString("renta") );
                p.setFec_pre_renta( rs.getString("fec_pre_renta") );
            }
            
        }catch(SQLException ex){
            throw new SQLException("ERROR DURANTE CONSULTA EN obtenerPeriodo( String dstrct, String anio, String mes ) " + ex.getMessage() + " " + ex.getErrorCode());
        }
finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
    }
    
    /**
     *M�todo que permite obtener un periodo est� anulado o no
     *@param dstrct Distrito
     *@param anio A�o del periodo
     *@param mes Mes del periodo contable
     */
    public PeriodoContable obtenerTodosPeriodo( String dstrct, String anio, String mes ) throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_BUSCAR_TODOS_PERIODO";
        
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString( 1, dstrct );
            st.setString( 2, anio );
            st.setString( 3, mes );
            rs = st.executeQuery();
            p = null;
            if(rs.next()){
                p = new PeriodoContable();
                p.setDistrito( rs.getString("dstrct") );
                p.setAnio( rs.getString("anio") );
                p.setMes( rs.getString("mes") );
                p.setAc( rs.getString("ac") );
                p.setIva( rs.getString("iva") );
                p.setFec_pre_iva( rs.getString("fec_pre_iva") );
                p.setRetencion( rs.getString("retencion") );
                p.setFec_pre_retencion( rs.getString("fec_pre_retencion") );
                p.setComercio( rs.getString("comercio") );
                p.setFec_pre_comercio( rs.getString("fec_pre_comercio") );
                p.setRenta( rs.getString("renta") );
                p.setFec_pre_renta( rs.getString("fec_pre_renta") );
                p.setReg_status( rs.getString("reg_status") );
            }
            
        }catch(SQLException ex){
            throw new SQLException("ERROR DURANTE CONSULTA EN obtenerTodosPeriodo( String dstrct, String anio, String mes ) " + ex.getMessage() + " " + ex.getErrorCode());
        }
finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return p;
    }
    
    
    /**
     *M�todo que actualiza en la base de datos el objeto dado
     *@param PeriodoContable p
     */
    public void updatePeriodoContable(PeriodoContable p) throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        String query = "SQL_ACTUALIZAR";
        
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString( 1, p.getAc() );
            st.setString( 2, p.getIva() );
            st.setString( 3, p.getFec_pre_iva() );
            st.setString( 4, p.getRetencion() );
            st.setString( 5, p.getFec_pre_retencion() );
            st.setString( 6, p.getComercio() );
            st.setString( 7, p.getFec_pre_comercio() );
            st.setString( 8, p.getRenta() );
            st.setString( 9, p.getFec_pre_renta() );
            st.setString( 10, p.getUser_update() );
            st.setString( 11, p.getAnio() );
            st.setString( 12, p.getMes() );
            st.setString( 13, p.getDistrito() );
            
            st.executeUpdate();
            
        }catch(SQLException ex){
            throw new SQLException("ERROR DURANTE UPDATE en updatePeriodoContable: " + ex.getMessage() + " " + ex.getErrorCode());
        }
finally{
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
    }
    
    /**
     *M�todo que anula en la base de datos el periodo contable
     *@param dist distrito
     *@param anio a�o 
     *@param mes mes
     */
    public void deletePeriodoContable(String dist, String anio, String mes) throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        String query = "SQL_ANULAR";
        
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString( 1, anio );
            st.setString( 2, mes );
            st.setString( 3, dist );
            
            st.executeUpdate();
        }catch(SQLException ex){
            throw new SQLException("ERROR DURANTE DELETE en deletePeriodoContable: " + ex.getMessage() + " " + ex.getErrorCode());
        }
finally{
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
    }
    
    /**
     * Getter for property p.
     * @return Value of property p.
     */
    public com.tsp.finanzas.contab.model.beans.PeriodoContable getP() {
        return p;
    }
    
    /**
     * Setter for property p.
     * @param p New value of property p.
     */
    public void setP(com.tsp.finanzas.contab.model.beans.PeriodoContable p) {
        this.p = p;
    }
      /**
     * Metodo: getAllPeriodos, obtiene todos los registros de la tabla periodo_contable.
     * @autor : Osvaldo P�rez Ferrer
     * @param : String anio: a�o del cual se desean consultar los periodos
     * @version : 1.0
     */
    public Vector getAllPeriodos(String anio) throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        periodos= new Vector();
        String query = "SQL_LISTAR_PERIODOS";        
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString( 1,  anio );
            rs=st.executeQuery();            
            while(rs.next()){
                PeriodoContable p = new PeriodoContable();                
                p.setDistrito(rs.getString("dstrct"));
                p.setAnio(rs.getString("anio"));
                p.setMes(rs.getString("mes"));
                p.setAc(rs.getString("ac"));
                p.setIva(rs.getString("iva"));
                p.setFec_pre_iva(rs.getString("fec_pre_iva"));
                p.setRetencion(rs.getString("retencion"));
                p.setFec_pre_retencion(rs.getString("fec_pre_retencion"));
                p.setComercio(rs.getString("comercio"));
                p.setFec_pre_comercio(rs.getString("fec_pre_comercio"));
                p.setRenta(rs.getString("renta"));
                p.setFec_pre_renta(rs.getString("fec_pre_renta"));
                
                periodos.add(p);
                p = null;//Liberar Espacio JJCastro
            }
            
        }catch(SQLException ex){
            throw new SQLException("ERROR DURANTE CONSULTA EN periodo_contable " + ex.getMessage() + " " + ex.getErrorCode());
        }
finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return periodos;
    }
    
}
