/***************************************
 * Nombre Clase ............. ContabilizacionFacturasDAO.java
 * Descripci�n  .. . . . . .  Permitimos realizar consultas para la contabilizacion de facturas
 * Autor  . . . . . . . . . . ROBERTO ROCHA P
 * Fecha . . . . . . . . . .  13/06/2006
 * versi�n . . . . . . . . .  1.0
 * Copyright ...............  FINTRAVALORES S.A.
 *******************************************/

package com.tsp.finanzas.contab.model.DAO;


import java.io.*;
import java.util.*;
import java.sql.*;
import com.tsp.finanzas.contab.model.beans.*;
import com.tsp.util.*;
import com.tsp.operation.model.DAOS.MainDAO;
import com.tsp.operation.model.beans.StringStatement;
import com.tsp.operation.model.beans.*;



public class ContabilizacionNegociosDAO extends MainDAO{



    private String APROBADOR        = "ADMIN";
    private String PRE_COMPROBANTE  = "CONTABILIZACION";
    private String PRE_DETALLE      = "ITEM";
    private String PRE_DETALLE_IMP  = "IMPUESTO";
    private String AUX_COMPROBANTE  = "";
    private String OF_PPAL          = "OP";

    public  String CABECERA         = "C";
    public  String DETALLE          = "D";
    public  String SALDO            = "S";


    private String TIPO_FACTURA_AMD  = "FAA";

    private String TIPO_CONTAB_OP    = "COP";   // FACTURAS OPS
    private String TIPO_CONTAB_ADM   = "CFA";   // FACTURAS ADMIN
    private String TIPO_CONTAB_NT    = "CNT";   // FACTURAS ND ND



    private final String FACTURA         = "010";
    private final String NC              = "035";
    private final String ND              = "036";



    public  int   CANTIDAD_IMP_ITEM      =  0;






    public ContabilizacionNegociosDAO() {
        super("ContabilizacionNegociosDAO.xml");
    }
    public ContabilizacionNegociosDAO(String dataBaseName) {
        super("ContabilizacionNegociosDAO.xml", dataBaseName);
    }



    /**
     * M�todos que setea valores nulos
     * @autor.......fvillacob
     * @version.....1.0.
     **/
    private String reset(String val){
        if(val==null)
            val = "";
        return val;
    }




    /**
     * M�todo que busca si la cta requiere auxiliar
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public  String requiereAUX(String distrito, String cta) throws Exception{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_REQUIERE_AUX";
        String aux = "";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, distrito);
            st.setString(2, cta);
            rs = st.executeQuery();
            if (rs.next()) {
                cta = reset(rs.getString("auxiliar"));
            }

        } catch (Exception e) {
            throw new Exception("requiereAUX " + e.getMessage());
        }
finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return cta;
    }





    /**
     * M�todo que busca si la cuenta tiene dicho subledger asociado
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public  boolean existeCuentaSubledger(String distrito, String cta, String auxiliar) throws Exception{
        Connection con = null;
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        String             query   = "SQL_CUENTA_SUBLEDGER";
        boolean            estado  = false;
        try{
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, distrito  );
            st.setString(2, cta       );
            st.setString(3, auxiliar  );
            rs = st.executeQuery();
            if( rs.next() )
                estado = true;

        }catch(Exception e){
            throw new Exception( "existeCuentaSubledger " + e.getMessage());
        }
finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return estado;
    }




    /**
     * M�todo que busca si la cuenta tiene dicho subledger asociado
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public  String getAccoundCodeC(String oc) throws Exception{
        Connection con = null;
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        String             query   = "SQL_ACCOUNT_CODE_C";
        String             cta     = "";
        try{

            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, oc  );
            rs = st.executeQuery();
            if( rs.next() )
                cta  =  reset( rs.getString("account_code_c")  );

        }catch(Exception e){
            throw new Exception( "getAccoundCodeC " + e.getMessage());
        }
finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return cta;
    }




     /**
     * M�todo que busca la cuenta contable diferencia en cambio a planilla
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public  String getCuenta_DifCambio(String distrito, String concepto) throws Exception{
        Connection con = null;
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        String             query   = "SQL_CUENTA_DIFERENCIA_CAMBIO";
        String             cuenta  = "";
        try{

            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, distrito  );
            st.setString(2, concepto  );
            rs = st.executeQuery();
            if(rs.next())
                cuenta = reset( rs.getString("account")  );

        }catch(Exception e){
            throw new Exception( "getCuenta_DifCambio " + e.getMessage());
        }
finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return cuenta;
    }




    // MONEDA : -------------------------------------------------------------------------

    /**
     * M�todo que busca la moneda local
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public  String getMoneda(String distrito) throws Exception{
        Connection con = null;
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        String             query   = "SQL_MONEDA_LOCAL";
        String             moneda  = "";
        try{
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, distrito  );
            rs = st.executeQuery();
            if(rs.next())
                moneda = reset( rs.getString("moneda")  );

        }catch(Exception e){
            throw new Exception( "getMoneda " + e.getMessage());
        }
finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return moneda;
    }









    // COMPROBANTE : ------------------------------------------------------------------

    /**
     * M�todo que busca las facturas que no esten contabilizadas
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public  List getNegociosNoContabilizados(String ayer) throws Exception{
        Connection con = null;
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        List               lista   = new LinkedList();
        String             query   = "SQL_NEGOCIOS_NO_CONTABILIZADOS";
        try{
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));
            st.setString(1, ayer);
            System.out.println("Query--->"+st.toString());
            rs = st.executeQuery();
            while (rs.next()){
                Comprobantes  comprobante = loadComprobantes(rs);
                lista.add( comprobante );
                comprobante = null;//Liberar Espacio JJCastro
            }

        }catch(Exception e){
            throw new Exception( "getNegociosNoContabilizados " + e.getMessage());
        }
finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return lista;
    }






    /**
     * M�todo que carga los datos de los negocios
     * @param rs
     * @return 
     * @throws java.lang.Exception
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public  Comprobantes loadComprobantes(ResultSet  rs)throws Exception{
        Comprobantes  comp  = new  Comprobantes();
        try{
           comp.setAbc      ("N/A");
           comp.setBase     ("COL");
           comp.setDstrct   (rs.getString("dist"));
           comp.setTipodoc  ("NEG");
           comp.setNumdoc   (rs.getString("cod_neg"));
           comp.setTercero  (rs.getString("cod_cli"));
           comp.setTotal_debito(Double.parseDouble(rs.getString("tot_pagado")));
           comp.setTotal_credito(Double.parseDouble(rs.getString("vr_desembolso")));
           comp.setMoneda   ("PES");
           comp.setRef_1    (reset(rs.getString("nit_tercero")));//nit del proveedor
           comp.setAuxiliar ("RD-"+rs.getString("cod_cli"));
           comp.setFecha_creacion(reset(rs.getString("fecha_negocio")));
           comp.setDetalle  ("NEGOCIO No "+rs.getString("cod_neg"));
           comp.setRef_2    (rs.getString("NOMB"));
           comp.setRef_5    (rs.getInt("nro_docs"));
           comp.setSucursal ("BQ");
           comp.setCustodia (rs.getDouble("vr_custodia"));
           comp.setRem      (rs.getDouble("vlr_remesa"));
           comp.setModrem   (rs.getString("mod_remesa"));
           comp.setCmc      (rs.getString("cmc"));
           comp.setTpr      (rs.getDouble("porterem"));
           comp.setTdesc    (rs.getDouble("tdescuento"));
           comp.setRef_3(reset(rs.getString("fecha_ap")));//Mod TMOLINA 03-Enero-2008
           comp.setVlr_aval(rs.getDouble("valor_fianza"));
           comp.setId_convenio(rs.getInt("id_convenio"));
           comp.setId_remesa(rs.getInt("id_remesa"));
           comp.setTipoNegocio(rs.getString("tneg"));
           comp.setValor(rs.getDouble("vr_negocio"));
           comp.setRef_6(rs.getBoolean("financia_aval"));
           comp.setCuota_administracion(rs.getDouble("cuota_manejo"));
           comp.setValor_int_coriente(rs.getDouble("interes_corriente"));
           comp.setValor_cat(rs.getDouble("vlr_cat"));
           comp.setSeguro_deudor(rs.getDouble("vlr_seguro"));
        }catch(NumberFormatException | SQLException e){
            throw new Exception( "loadComprobantes " + e.getMessage());
        }
        return comp;
    }








    // DETALLE DE COMPROBANTE : ------------------------------------------------------------------


    /**
     * M�todo que busca los items de las facturas que no esten contabilizadas
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public  List getItems(ComprobanteFacturas  comprobante) throws Exception{
        Connection con = null;
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        List               lista   = new LinkedList();
        String             query   = "SQL_ITEM_FACTURAS";
        try{
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, comprobante.getDstrct()          );
            st.setString(2, comprobante.getTercero()         );
            st.setString(3, comprobante.getTipodoc_factura() );
            st.setString(4, comprobante.getNumdoc()          );
            rs = st.executeQuery();
            while (rs.next()){
                ComprobanteFacturas  comprodet = loadComproDet(rs);

             // Si en Nota Credito Invertir signo:
                if(  comprobante.getTipodoc_factura().equals( NC )  ){

                     double cre =  comprodet.getTotal_credito();
                     double deb =  comprodet.getTotal_debito();

                     comprodet.setTotal_credito( deb  );
                     comprodet.setTotal_debito ( cre  );

                }

                if(  comprodet.getVlr() != 0  )
                     lista.add( comprodet );

            }

        }catch(Exception e){
            throw new Exception( "getItems " + e.getMessage());
        }
finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return lista;
    }



    /**
     * M�todo que carga los datos de la factura
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public  ComprobanteFacturas loadComproDet(ResultSet  rs)throws Exception{
        ComprobanteFacturas  comp  = new  ComprobanteFacturas();
        try{
            comp.setDstrct           (  reset( rs.getString("dstrct")           ) );
            comp.setNumdoc           (  reset( rs.getString("numdoc")           ) );
            comp.setCuenta           (  reset( rs.getString("cuenta")           ) );
            comp.setAuxiliar         (  reset( rs.getString("auxiliar")         ) );
            comp.setDetalle          (  PRE_DETALLE +" "+ reset( rs.getString("detalle") ) );
            comp.setTercero          (  reset( rs.getString("tercero")          ) );
            comp.setTipodoc_factura  (  reset( rs.getString("tipodoc_factura")  ) );
            comp.setVlr              (         rs.getDouble("valor")              );
            comp.setVlrME            (         rs.getDouble("valorME")            );
            comp.setNoItem           (  reset( rs.getString("item") )             );
            comp.setDocumento_interno(  comp.getTipodoc_factura()                 );
            comp.setTipo             (  DETALLE                                   );
            comp.setConcepto         (  reset( rs.getString("concepto")        )  );  // Concepto.
            comp.setAbc              (  reset( rs.getString("abc")             )  );  // Abc

            comp.setTdoc_rel         (  reset( rs.getString("tipodoc")         )  );
            comp.setNumdoc_rel       (  reset( rs.getString("planilla")        )  );



            double  vlr        =   comp.getVlr();

            double  vlrDebito  = (vlr>0)?vlr:0;
            double  vlrCredito = (vlr<0)?( vlr * -1 ):0;    // Siempre valor positivos

            comp.setTotal_credito( vlrCredito );
            comp.setTotal_debito ( vlrDebito  );


        }catch(Exception e){
            throw new Exception( "loadComprobantes " + e.getMessage());
        }
        return comp;
    }





    /**
     * M�todo que busca los impuestos de los items de las facturas que no esten contabilizadas
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public  int getCantidadItemsImpuestos(ComprobanteFacturas  comprodet) throws Exception{
        Connection con = null;
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        String             query   = "SQL_CANT_IMP_ITEM";
        int                total   = 0;
        try{
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, comprodet.getDstrct()          );
            st.setString(2, comprodet.getTercero()         );
            st.setString(3, comprodet.getTipodoc_factura() );
            st.setString(4, comprodet.getNumdoc()          );
            st.setString(5, comprodet.getNoItem()          );
            rs = st.executeQuery();
            if( rs.next() )
                total = rs.getInt("total");

        }catch(Exception e){
            throw new Exception( "getCantidadItemsImpuestos " + e.getMessage());
        }
finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return total;
    }





    /**
     * M�todo que busca los impuestos de los items de las facturas que no esten contabilizadas
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public  List getImpuestos(ComprobanteFacturas  comprodet, String agencia , String ano) throws Exception{
        Connection con = null;
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        List               lista   = new LinkedList();
        String             query   = "SQL_IMP_ITEM";
        CANTIDAD_IMP_ITEM          = 0;
        try{
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, comprodet.getDstrct()          );
            st.setString(2, comprodet.getTercero()         );
            st.setString(3, comprodet.getTipodoc_factura() );
            st.setString(4, comprodet.getNumdoc()          );
            st.setString(5, comprodet.getNoItem()          );
            st.setString(6, ano                            );  // A�o de la factura
            //st.setString(7, agencia                        );  // OP agencia origen, ADM agencia factura

            rs = st.executeQuery();
            while (rs.next()){
                CANTIDAD_IMP_ITEM ++;
                ComprobanteFacturas  imp = new ComprobanteFacturas();
                imp.setNoItem         ( reset( rs.getString("item") )            );
                imp.setTipoImpuesto   ( reset( rs.getString("tipo_impuesto") )   );
                imp.setVlr            (        rs.getDouble("valor")             );
                imp.setCodigoImpuesto ( reset( rs.getString("cod_impuesto") )    );
                imp.setDetalle        ( PRE_DETALLE_IMP +" "+ reset(rs.getString("item")) +" - "+ reset( rs.getString("tipo_impuesto") ) +" "+  reset(rs.getString("cod_impuesto"))  +" "+  reset(rs.getString("porcent_impuesto"))  );

                if( imp.getVlr() !=  0  )
                    lista.add( imp );

            }

        }catch(Exception e){
            throw new Exception( "getImpuestos " + e.getMessage());
        }
finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return lista;
    }





    /**
     * M�todo que busca la cuenta del impuesto
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public  String getCuentaImpuesto(String distrito, String codImpuesto, String agencia, String ano)throws Exception{
        Connection con = null;
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        String             query   = "SQL_CUENTA_IMPUESTO";
        String             imp     = "";
        try{
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, distrito    );
            st.setString(2, codImpuesto );
            st.setString(3, ano         );
            //st.setString(4, agencia     );
            rs = st.executeQuery();
            if( rs.next() )
                imp = rs.getString("cod_cuenta_contable");

        }catch(Exception e){
            throw new Exception( " DAO: getCuentaImpuesto " + e.getMessage());
        }
finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return imp;
    }







    /**
     * M�todo que busca la cuenta asociada al cmc y tipo doc
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public  Hashtable  getCuenta(String distrito, String tipoDoc, String cmc )throws Exception{
        Connection con = null;
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        String             query   = "SQL_CUENTA_CMC";
        Hashtable          info    = null;
        try{
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, distrito    );
            st.setString(2, tipoDoc     );
            st.setString(3, cmc         );
            rs = st.executeQuery();
            if( rs.next() ){
                info    =  new Hashtable();
                info.put("cuenta",      reset( rs.getString("cuenta")      ) );
                info.put("dbcr",        reset( rs.getString("dbcr")        ) );
                info.put("tipo_cuenta", reset( rs.getString("tipo_cuenta") ) );
            }

        }catch(Exception e){
            throw new Exception( " DAO: getCuenta " + e.getMessage());
        }
finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return info;
    }







    // INSERT  UPDATE : ----------------------------------------------------------------------------------







    /**
     * M�todo que inserta el comprobante
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public  void  deleteComprobante(ComprobanteFacturas  comprobante)throws Exception{
        Connection con = null;
        PreparedStatement  st      = null;
        String             query   = "SQL_DELETE_COMPROBANTE";
        try{

            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2

                 st.setString(1,   comprobante.getDstrct()   );
                 st.setString(2,   comprobante.getTipodoc()  );
                 st.setString(3,   comprobante.getNumdoc()   );
                 st.setInt   (4,   comprobante.getGrupo_transaccion() );

            st.execute();


        }catch(Exception e){
            throw new Exception( "deleteComprobante " + e.getMessage());
        }finally{
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
    }





    /**
     * M�todo que inserta el comprobante
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public  String InsertComprobante(Comprobantes  comprobante, String user)throws Exception{
        Connection con = null;
        PreparedStatement  st      = null;
        String             query   = "SQL_INSERT_COMPROBANTE";
        String             sql     = "";
        try{

            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1,   comprobante.getDstrct()         );
            st.setString(2,   comprobante.getTipodoc()        );
            st.setString(3,   comprobante.getNumdoc()         );
            st.setString(4,   this.OF_PPAL                    );  //  comprobante.getSucursal()
            st.setString(5,   comprobante.getPeriodo()        );
            st.setString(6,   comprobante.getFechadoc()       );
            st.setString(7,   comprobante.getDetalle()        );
            st.setString(8,   comprobante.getTercero()        );
            st.setDouble(9,   comprobante.getTotal_debito()   );
            st.setDouble(10,  comprobante.getTotal_credito()  );
            st.setInt   (11,  comprobante.getTotal_items()    );
            st.setString(12,  comprobante.getMoneda()         );
            st.setString(13,  comprobante.getAprobador()      );
            st.setString(14,  user                            );
            st.setString(15,  comprobante.getBase()           );
            st.setString(16,  comprobante.getTipo_operacion() );
            st.setInt   (17,  comprobante.getGrupo_transaccion()    );

            sql = st.toString();
           // st.execute();

        }catch(Exception e){
            throw new Exception( "InsertComprobante " + e.getMessage());
        }finally{
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return sql;
    }




    /**
     * M�todo que inserta el comprobante detalle
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public  ArrayList<String> InsertComprobanteDetalle(List lista, int sec, String user)throws Exception{
        Connection con = null;
        PreparedStatement  st      = null;
        String             query   = "SQL_INSERT_COMPRODET";
        ArrayList<String>  sql     = new ArrayList<String>();
        try{

            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            for(int i=0;i<lista.size();i++){
                Comprobantes  comprodet = (Comprobantes)lista.get(i);

                st.setString(1,   comprodet.getDstrct()            );
                st.setString(2,   comprodet.getTipodoc()           );
                st.setString(3,   comprodet.getNumdoc()            );
                st.setInt   (4,   sec                              );
                st.setString(5,   comprodet.getPeriodo()           );
                st.setString(6,   comprodet.getCuenta()            );
                st.setString(7,   comprodet.getAuxiliar()          );
                st.setString(8,   comprodet.getAbc()               );
                st.setString(9,   comprodet.getDetalle()           );
                st.setDouble(10,  comprodet.getTotal_debito()      );
                st.setDouble(11,  comprodet.getTotal_credito()     );
                st.setString(12,  comprodet.getTercero()           );
                st.setString(13,  user                             );
                st.setString(14,  comprodet.getBase()              );
                st.setString(15,  comprodet.getDocumento_interno() );
                st.setString(16,  comprodet.getTipodoc()  );
                st.setString(17,  comprodet.getDocrelacionado()        );

                sql.add(st.toString());
                st.clearParameters();

            }

        }catch(Exception e){
            throw new Exception( "InsertComprobanteDetalle " + e.getMessage());
        }finally{
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return sql;
    }



    /**
     * M�todo que inserta el comprobante
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public  String updateFactura(ComprobanteFacturas  comprobante, String user)throws Exception{
        Connection con = null;
        PreparedStatement  st      = null;
        String             query   = "SQL_UPDATE_FACTURAS_DATOS_CONTABLES";
        String             sql     = "";
        try{
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
          //set:
            st.setString(1, user );
          //  st.setString(2, comprobante.getFechadoc()  );
            st.setString(2, comprobante.getPeriodo()   );
            st.setString(3, String.valueOf(comprobante.getGrupo_transaccion())   );
          //where:
            st.setString(4, comprobante.getDstrct()          );
            st.setString(5, comprobante.getTercero()         );
            st.setString(6, comprobante.getTipodoc_factura() );
            st.setString(7, comprobante.getNumdoc()          );

            sql = st.toString();
        }catch(Exception e){
            throw new Exception( "updateFactura " + e.getMessage());
        }finally{
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return sql;
    }



    /**
     * M�todo que actualiza los moviminetos tipo Q
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public  String updateMovplaTipoQ(ComprobanteFacturas  comprobante, String user)throws Exception{
        Connection con = null;
        PreparedStatement  st      = null;
        String             query   = "SQL_UPDATE_MOVPLA_TIPO_Q";
        String             sql     = "";
        try{
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
          //set:
            st.setString(1, String.valueOf(comprobante.getGrupo_transaccion())   );
         //   st.setString(2, comprobante.getFechadoc()  );
            st.setString(2, comprobante.getPeriodo()   );

          //where:
            st.setString(3, comprobante.getOc()        );
            st.setString(4, comprobante.getDstrct()    );
            st.setString(5, comprobante.getNumdoc()    );


            sql = st.toString();

        }catch(Exception e){
            throw new Exception( "updateMovplaTipoQ " + e.getMessage());
        }finally{
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return sql;
    }

    /*Metodos Reversi�n Descontabiolizacion*/
    /**
     * M�todo que Vector Comprobantes
     * @autor.......jescandon
     * @throws......Exception
     * @version.....1.0.
     **/
    public  Vector VectorComprobantes( Comprobantes  comprobante )throws Exception{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_COMPROBANTESDET";
        Vector Vcomprobantes = new Vector();
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, comprobante.getTipodoc());
            st.setString(2, comprobante.getNumdoc());
            st.setInt(3, comprobante.getGrupo_transaccion());
            rs = st.executeQuery();
            while (rs.next()) {
                Comprobantes aux = new Comprobantes();
                aux.setTipodoc(rs.getString("tipodoc"));
                aux.setNumdoc(rs.getString("numdoc"));
                aux.setGrupo_transaccion(rs.getInt("grupo_transaccion"));
                aux.setAuxiliar(rs.getString("auxiliar"));
                aux.setCuenta(rs.getString("cuenta"));
                aux.setTotal_debito(rs.getDouble("valor_debito"));
                aux.setTotal_credito(rs.getDouble("valor_credito"));
                aux.setDstrct(rs.getString("dstrct"));
                aux.setPeriodo(rs.getString("periodo"));
                Vcomprobantes.add(aux);
                aux = null; //Liberar Espacio JJCastro
            }


        }catch(Exception e){
            throw new Exception( "Descontabilizar " + e.getMessage());
        }finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return Vcomprobantes;
    }


    /**
     * M�todo que getComprobantes
     * @autor.......jescandon
     * @throws......Exception
     * @version.....1.0.
     **/
    public  ComprobanteFacturas getComprobantes( Comprobantes  comprobante )throws Exception{
        Connection con = null;
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        ComprobanteFacturas       aux     = null;
        String             query   = "SQL_COMPROBANTES";
        try{

            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, comprobante.getTipodoc());
            st.setString(2, comprobante.getNumdoc());
            st.setInt(3, comprobante.getGrupo_transaccion());
            rs = st.executeQuery();
            while(rs.next()){
                aux = new ComprobanteFacturas();
                aux.setDstrct(rs.getString("dstrct"));
                aux.setTipodoc(rs.getString("tipodoc"));
                aux.setNumdoc(rs.getString("numdoc"));
                aux.setGrupo_transaccion(rs.getInt("grupo_transaccion"));
                aux.setSucursal(rs.getString("sucursal"));
                aux.setPeriodo(rs.getString("periodo"));
                aux.setFechadoc(rs.getString("fechadoc"));
                aux.setDetalle(rs.getString("detalle"));
                aux.setTercero(rs.getString("tercero"));
                aux.setTotal_debito(rs.getDouble("total_credito"));
                aux.setTotal_credito(rs.getDouble("total_debito"));
                aux.setTotal_items(rs.getInt("total_items"));
                aux.setMoneda(rs.getString("moneda"));
                aux.setAprobador(rs.getString("aprobador"));
                aux.setUsuario(rs.getString("creation_user"));
                aux.setBase(rs.getString("base"));
                aux.setUsuario_aplicacion(rs.getString("usuario_aplicacion"));
            }


        }catch(Exception e){
            throw new Exception( "Descontabilizar " + e.getMessage());
        }finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return aux;
    }


     /**
     * M�todo que Vector Comprobantes
     * @autor.......jescandon
     * @throws......Exception
     * @version.....1.0.
     **/

    public  List ListaComprobantes( Comprobantes  comprobante )throws Exception{
        Connection con = null;
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        String             query   = "SQL_COMPROBANTESDET";
        List               Vcomprobantes = new LinkedList();
        try{
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, comprobante.getTipodoc());
            st.setString(2, comprobante.getNumdoc());
            st.setInt(3, comprobante.getGrupo_transaccion());
            rs = st.executeQuery();
            while(rs.next()){
                ComprobanteFacturas aux = new ComprobanteFacturas();
                aux.setDstrct(rs.getString("dstrct"));
                aux.setTipodoc(rs.getString("tipodoc"));
                aux.setNumdoc(rs.getString("numdoc"));
                aux.setGrupo_transaccion(rs.getInt("grupo_transaccion"));
                aux.setAuxiliar(rs.getString("auxiliar"));
                aux.setCuenta(rs.getString("cuenta"));
                aux.setTotal_debito(rs.getDouble("valor_credito"));
                aux.setTotal_credito(rs.getDouble("valor_debito"));
                aux.setPeriodo(rs.getString("periodo"));
                aux.setDetalle(rs.getString("detalle"));
                aux.setTercero(rs.getString("tercero"));
                aux.setBase(rs.getString("base"));
                aux.setDocumento_interno(rs.getString("documento_interno"));
                Vcomprobantes.add(aux);
                aux = null; //Liberar Espacio JJCastro
            }


        }catch(Exception e){
            throw new Exception( "Descontabilizar " + e.getMessage());
        }finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return Vcomprobantes;
    }

    /**
     * M�todo que borra una lista de comprobantes
     * @autor.......jescandon
     * @throws......Exception
     * @version.....1.0.
     **/
    public  List deleteComprobanteSQL(List lista)throws Exception{
        Connection con = null;
        PreparedStatement  st      = null;
        String             query   = "SQL_DELETE_COMPROBANTE";
        try{

            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            for(int i=0;i<lista.size();i++){
                ComprobanteFacturas  comprobante  = (ComprobanteFacturas) lista.get(i);
                    st.setString(1,   comprobante.getDstrct()   );
                    st.setString(2,   comprobante.getTipodoc()  );
                    st.setString(3,   comprobante.getNumdoc()   );
                    st.setInt   (4,   comprobante.getGrupo_transaccion() );
                st.execute();
                st.clearParameters();
            }

        }catch(Exception e){
            throw new Exception( "deleteComprobante " + e.getMessage());
        }finally{
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return lista;
    }

         /**
     * M�todo que DescontabilizarMayor
     * @autor.......jescandon
     * @throws......Exception
     * @version.....1.0.
     **/
    public  String DescontabilizarMayor( Comprobantes  comprobante )throws Exception{
        StringStatement  st      = null;
        ResultSet          rs      = null;
        String             query   = "SQL_DESCONTABILIZARMAYOR";
        String             sql     = "";
        try{
            String mes = comprobante.getPeriodo().substring(4,6);
            sql = this.obtenerSQL(query).replaceAll("#MES#", mes);

            st = new StringStatement(sql);
            st.setDouble( comprobante.getTotal_debito());
            st.setDouble( comprobante.getTotal_credito());
            st.setDouble( comprobante.getTotal_debito());
            st.setDouble( comprobante.getTotal_credito());
            st.setString( comprobante.getCuenta());
            st.setString( comprobante.getDstrct());
            String anio = comprobante.getPeriodo().substring(0,4);
            st.setString( anio);
            sql = st.getSql();

        }catch(Exception e){
            e.printStackTrace();
            throw new Exception( "Descontabilizar " + e.getMessage());
        } finally {
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            }
        return sql;
    }


    /**
     * M�todo que DescontabilizarMayoSubledger
     * @autor.......jescandon
     * @throws......Exception
     * @version.....1.0.
     **/
    public  String DescontabilizarMayoSubledger( Comprobantes  comprobante )throws Exception{
        StringStatement   st      = null;
        ResultSet          rs      = null;
        String             query   = "SQL_DESCONTABILIZARMAYORSUBLEDGER";
        String             sql     = "";
        Connection con             = null;

        try{

            String mes = comprobante.getPeriodo().substring(4, 6);
            sql = this.obtenerSQL(query).replaceAll("#MES#", mes);
            st = new StringStatement(sql);
            st.setDouble(comprobante.getTotal_debito());
            st.setDouble(comprobante.getTotal_credito());
            st.setDouble(comprobante.getTotal_debito());
            st.setDouble(comprobante.getTotal_credito());
            st.setString(comprobante.getCuenta());
            st.setString(comprobante.getDstrct());
            String anio = comprobante.getPeriodo().substring(0, 4);
            st.setString(anio);
            st.setString(comprobante.getAuxiliar());
            sql = st.getSql();

        }catch(Exception e){
            e.printStackTrace();
            throw new Exception( "Descontabilizar " + e.getMessage());
        }finally {
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
        }
        return sql;
    }


     /**
     * M�todo que UpdateFechaAplicacion
     * @autor.......jescandon
     * @throws......Exception
     * @version.....1.0.
     **/
    public  String UpdateFechaAplicacion( Comprobantes  comprobante )throws Exception{
        StringStatement st = null;
        ResultSet rs = null;
        String query = "SQL_UPDATEFECHA";
        String sql = "";

        try {
            st = new StringStatement(this.obtenerSQL(query));
            st.setString(comprobante.getTipodoc());
            st.setString(comprobante.getNumdoc());
            st.setInt(comprobante.getGrupo_transaccion());
            // st.executeUpdate();
            sql = st.getSql();

        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Descontabilizar " + e.getMessage());
        }finally {
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
        }

        return sql;
    }

    public  String getAccountTC(String oc) throws Exception{
        Connection con = null;
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        String             query   = "SQL_CTA_TBLCON";
        String             cta     = "";
        try{
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, oc  );
            rs = st.executeQuery();
            if( rs.next() )
                cta  =  reset( rs.getString("dat")  );

        }catch(Exception e){
            throw new Exception( "getAccountMal " + e.getMessage());
        }
finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return cta;
    }

     public  String getAccountDES(String oc) throws Exception{
        Connection con = null;
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        String             query   = "SQL_DESC_CTA";
        String             cta     = "";
        try{
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, oc  );
            st.setString(2, "FINV");
            rs = st.executeQuery();
            if( rs.next() )
                cta  =  reset( rs.getString("nombre_largo")  );

        }catch(Exception e){
            throw new Exception( "getAccountBCO_Mal " + e.getMessage());
        }
finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return cta;
    }

      public  String getAccountIng(String oc) throws Exception{
        Connection con = null;
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        String             query   = "SQL_CTA_ING";
        String             cta     = "";
        try{
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, oc  );
            rs = st.executeQuery();
            if( rs.next() )
                cta  =  reset( rs.getString("dat")  );

        }catch(Exception e){
            throw new Exception( "getAccountBCO_Mal " + e.getMessage());
        }
finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return cta;
    }




      public void updatecont(String ob,int cod,String est,String codn)throws SQLException{
          Connection con = null;
          PreparedStatement st = null;
          String query = "SQL_UPDATE_NEG";
        try
        {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, ob);
            st.setString(2, String.valueOf(cod));
            st.setString(3, est);
            st.setString(4, codn);
            st.executeUpdate();
         }
        catch(SQLException e)
        {
             //System.out.println("Entra en el error 177");
             throw new SQLException("ERROR DURANTE LA ACTUALIZACION DE NEGOCIOS " + e.getMessage() + " " + e.getErrorCode());
        }
finally{
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        //System.out.println("El nit q le  llega ++++");
    }

    public double getSumaInteres(String cod_neg) throws Exception{
        Connection con = null;
        PreparedStatement  st      = null;
        ResultSet          rs      = null;
        String             query   = "SQL_SUM_INT_NEG";
        double             respuesta     = 0;
        try{
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, cod_neg  );
            rs = st.executeQuery();
            if( rs.next() ){
                respuesta  =  rs.getDouble("sum_interes") ;
            }
        }catch(Exception e){
            System.out.println("getSumaInteres error"+e.toString()+"___"+e.getMessage());
            throw new Exception( "getSumaInteres " + e.getMessage());
        }
finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return respuesta;
    }

    /**
     * Metodo que inserta el comprobante detalle
     * @autor.......ivargas
     * @throws......Exception
     * @version.....1.0.
     **/
    public  ArrayList<String> InsertComprobanteDetalle2(List lista, int sec, String user)throws Exception{
        Connection con = null;
        PreparedStatement  st      = null;
        String             query   = "SQL_INSERT_COMPRODET";
        ArrayList<String>  sql     = new ArrayList<>();
        try{

            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            for(int i=0;i<lista.size();i++){
                Comprobantes  comprodet = (Comprobantes)lista.get(i);

                st.setString(1,   comprodet.getDstrct()            );
                st.setString(2,   comprodet.getTipodoc()           );
                st.setString(3,   comprodet.getNumdoc()            );
                st.setInt   (4,   sec                              );
                st.setString(5,   comprodet.getPeriodo()           );
                st.setString(6,   comprodet.getCuenta()            );
                st.setString(7,   comprodet.getAuxiliar()          );
                st.setString(8,   comprodet.getAbc()               );
                st.setString(9,   comprodet.getDetalle()           );
                st.setDouble(10,  comprodet.getTotal_debito()      );
                st.setDouble(11,  comprodet.getTotal_credito()     );
                st.setString(12,  comprodet.getTercero()           );
                st.setString(13,  user                             );
                st.setString(14,  comprodet.getBase()              );
                st.setString(15,  comprodet.getDocumento_interno() );
                st.setString(16,  comprodet.getTdoc_rel()  );
                st.setString(17,  comprodet.getDocrelacionado()        );

                sql.add(st.toString());
                st.clearParameters();

            }

        }catch(Exception e){
            throw new Exception( "InsertComprobanteDetalle " + e.getMessage());
        }finally{
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return sql;
    }


    public ArrayList<ConvenioComision> buscar_comisiones(String cod_neg) throws Exception {
        ArrayList<ConvenioComision> convenio_comision = new ArrayList();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            con = conectarJNDI("SQL_BUSCAR_COMISIONES");
            String sql = this.obtenerSQL("SQL_BUSCAR_COMISIONES");
            ps = con.prepareStatement(sql);
            ps.setString(1, cod_neg);
            rs = ps.executeQuery();
            while (rs.next()) {
                ConvenioComision cnv = new ConvenioComision();
                cnv.setCuenta_comision(rs.getString("cuenta_comision"));
                cnv.setValor_comision(rs.getDouble("valor_comision"));
                cnv.setComision_tercero(rs.getBoolean("comision_tercero"));
                cnv.setIndicador_contra(rs.getBoolean("indicador_contra"));
                cnv.setCuenta_contra(rs.getString("cuenta_contra"));
                convenio_comision.add(cnv);
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
        } finally {
            if (con != null) {
                this.desconectar(con);
            }
            if (ps != null) {
                ps.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        return convenio_comision;
    }
    
  
    public boolean getRenovacion(String cod_neg) throws Exception {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        boolean status = false;
        String query = "SQL_NEGOCIO_RENOVACION";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));
            st.setString(1, cod_neg);
            System.out.println("Query--->" + st.toString());
            rs = st.executeQuery();
            if (rs.next()) {
                status = true;
            }

        } catch (Exception e) {
            throw new Exception("getNegociosNoContabilizados " + e.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st = null;
                } catch (Exception e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return status;
    }
    
    
      
    
    public double getVlrTotalCapitalizado(String cod_neg) throws Exception {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        double valor = 0;
        String query = "SQL_TOTAL_CAPITALIZADO";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));
            st.setString(1, cod_neg);
            System.out.println("Query--->" + st.toString());
            rs = st.executeQuery();
            if (rs.next()) {
                valor = rs.getDouble("vlr_capital");
            }

        } catch (Exception e) {
            throw new Exception("getNegociosNoContabilizados " + e.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st = null;
                } catch (Exception e) {
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return valor;
    }


}
