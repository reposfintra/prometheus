/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.finanzas.contab.model.DAO;

import com.tsp.operation.model.DAOS.MainDAO;
import com.tsp.operation.model.beans.StringStatement;
import com.tsp.finanzas.contab.model.beans.Hc;
import com.tsp.finanzas.contab.model.beans.Tipo_Docto;
import com.tsp.operation.model.beans.Usuario;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.*;
import java.sql.*;

import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author imorales
 */
public class GestionarHcsDAO extends MainDAO {

     public GestionarHcsDAO() {
     super("GestionarHcsDAO.xml");
     }
     public GestionarHcsDAO(String dataBaseName) {
     super("GestionarHcsDAO.xml", dataBaseName);
     }

 /************************listar HCs*********************************/
    public List<Hc> ListarHc() throws SQLException {
        List<Hc> lis = new LinkedList<Hc>();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_LISTAR_HC";
        try {
            /*  1 */ System.out.print("entro a 1");
            /* 2 */ con = this.conectarJNDI(query);
            System.out.print("entro a 2");
            if(con!=null){
            /* 3 */
            System.out.print("entro a 3");
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();



            while (rs.next()) {
                Hc hc = new Hc();

                // hc.setCreationDate(rs.getString("TelAsistentes"));
                //hc.setCreationUser("");
              //  hc.setDato(rs.getString("Dato"));
                hc.setDescripcion(rs.getString("descripcion"));
              //  hc.setReferencia(rs.getString("reg_status"));
                hc.setTableCode(rs.getString("table_Code"));
             //   hc.setTableType(rs.getString("table_type"));
             //   hc.setReferencia(rs.getString("referencia"));
                lis.add(hc);
            }


        }
        }
        catch(SQLException e)
        {
             System.out.print("<---->"+e.getMessage());
            throw new SQLException("ERROR DURANTE Listar " + e.getMessage() + " " + e.getErrorCode());
        }
        finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return lis;
    }



 /************************Validar HCs*********************************/
    public boolean ValidarHc(String codigo) throws SQLException {
       boolean sw=true;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_VALIDAR_HC";
        try {
            con = this.conectarJNDI(query);
            if(con!=null){
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, codigo.toUpperCase());
            rs = ps.executeQuery();
            if(rs.next())
            {
             sw=false;
            }
           }
        }
        catch(SQLException e)
        {
            throw new SQLException("ERROR DURANTE Validar " + e.getMessage() + " " + e.getErrorCode());
        }
        finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return sw;
    }



   /************************Insertar HC en tabla tablegen*********************************/
    public String InsertarHc(Hc hc) throws SQLException{
        StringStatement st = null;
        Connection con = null;
        String query = "SQL_INSERTAR_HC";
        String sql = "";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = new StringStatement(this.obtenerSQL(query),true);
                st.setString(1,hc.getTableCode());
                st.setString(2,hc.getReferencia());
                st.setString(3,hc.getDescripcion());
              //  st.setDate (hc.getLastUpdate());
                st.setString(4,hc.getUserUpdate());
                //st.setString(hc.getCreationDate());
                st.setString(5,hc.getCreationUser());
               // st.setString(hc.getDato());
                sql = st.getSql();
            }
        }catch(Exception e){
            throw new SQLException("ERROR DURANTE Insercion Hc \n " + e.getMessage());
        } finally {
            if (st != null) {
                st = null;
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return sql;
    }






   /************************Actualizar HC en tabla tablegen*********************************/
    public String ActualizarHc(Hc hc) throws SQLException{
        StringStatement st = null;
        Connection con = null;
        String query = "SQL_ACTUALIZAR_HC";
        String sql = "";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = new StringStatement(this.obtenerSQL(query),true);
                st.setString(1,hc.getDescripcion());
                st.setString(2,hc.getUserUpdate());
                st.setString(3,hc.getTableCode());
                sql = st.getSql();
            }
        }catch(Exception e){
            throw new SQLException("ERROR DURANTE Actualizacion Hc \n " + e.getMessage());
        } finally {
            if (st != null) {
                st = null;
            }
            if (con != null) {
                try {
                    this.desconectar(con);
                } catch (SQLException e) {
                    throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return sql;
    }

/************************Buscar HCs*********************************/
    public Hc BuscarHc(String codigo) throws SQLException {
        Hc hc = new Hc();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_BUSCAR_HC";
        try {
            con = this.conectarJNDI(query);
            if(con!=null){
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, codigo.toUpperCase());
            rs = ps.executeQuery();
            while (rs.next()) {

                hc.setDescripcion(rs.getString("descripcion"));
                hc.setTableCode(rs.getString("table_Code"));

            }


        }
        }
        catch(SQLException e)
        {
             System.out.print("<---->"+e.getMessage());
            throw new SQLException("ERROR DURANTE Buscar " + e.getMessage() + " " + e.getErrorCode());
        }
        finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return hc;
    }






}

