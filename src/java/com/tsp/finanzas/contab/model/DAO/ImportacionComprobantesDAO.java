/*
 * ImportacionComprobantesDAO.java
 *
 * Created on 24 de marzo de 2007, 08:14 AM
 */

package com.tsp.finanzas.contab.model.DAO;

import com.tsp.finanzas.contab.model.beans.COMPImportacion;
import com.tsp.finanzas.contab.model.beans.Tipo_Docto;
import com.tsp.operation.model.beans.CIA;
import com.tsp.operation.model.DAOS.MainDAO;
import java.util.*;
import java.sql.*;

public class ImportacionComprobantesDAO extends MainDAO {
    
    /** Creates a new instance of ImportacionComprobantesDAO */
    public ImportacionComprobantesDAO() {
        super("ImportacionComprobantesDAO.xml");
    }
    public ImportacionComprobantesDAO(String dataBaseName) {
        super("ImportacionComprobantesDAO.xml", dataBaseName);
    }
    
    
    
    /**
     * Metodo para obtener los comprobantes pendientes a migrar.
     */
    public Vector obtenerComprobantesPendientes (String usuario) throws Exception {
        Connection con = null;
        PreparedStatement ps    = null;
        ResultSet         rs    = null;
        String            query = "SQL_OBTENER_COMPROBANTES_PENDIENTES";
        Vector            datos = new Vector();
        try {
            con = this.conectarJNDI(query);
            ps = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            ps.setString( 1, usuario );
            rs = ps.executeQuery();
            
            while (rs.next()) {
                COMPImportacion dt = COMPImportacion.load(rs);
                datos.add(dt);
            }
            return datos;
        } catch (Exception ex){
            ex.printStackTrace();
            throw new Exception ( ex.getMessage() );
        }finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (ps != null) {try {ps = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
    }
    
    
    
    /** busca el valor de las monedas de las compa�ias */
    public TreeMap obtenerDistritos() throws Exception {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        TreeMap monedas = new TreeMap();
        String query = "SQL_CIAS";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            rs = st.executeQuery();
            while (rs.next()) {
                CIA cia = new CIA();
                cia.setDistrito(rs.getString("dstrct"));
                cia.setMoneda(rs.getString("moneda"));
                cia.setBase(rs.getString("base"));
                monedas.put(rs.getString("dstrct"), cia);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new Exception("Error en rutina obtenerDistritos [ImportacionComprobantesDAO]... \n" + e.getMessage());
        }
finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return monedas;
    }    
    
    
    /** busca el valor de los documentos validos*/
    public TreeMap obtenerDocumentosContables() throws Exception {
        Connection con = null;
        PreparedStatement st     = null;
        ResultSet         rs     = null;
        TreeMap           doc    = new TreeMap();
        String            query  = "SQL_TIPO_DOC";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            rs = st.executeQuery();
            while (rs.next()){
                Tipo_Docto d = new Tipo_Docto();
                d.setCodigo        ( rs.getString("codigo") );
                d.setDescripcion   ( rs.getString("descripcion"));
                d.setCodigo_interno( rs.getString("codigo_interno"));
                d.setTercero       ( rs.getString("tercero"));
                d.setManeja_serie  ( rs.getString("maneja_serie"));
                d.setPrefijo       ( rs.getString("prefijo"));
                d.setPrefijo_anio  ( rs.getString("prefijo_anio"));
                d.setPrefijo_mes   ( rs.getString("prefijo_mes"));
                d.setSerie_ini     ( rs.getString("serie_ini"));
                d.setSerie_fin     ( rs.getString("serie_fin"));
                d.setSerie_act     ( rs.getString("serie_act"));
                d.setLong_serie    ( rs.getString("long_serie"));                
                
                doc.put (d.getCodigo(),  d );
                d = null;//Liberar Espacio JJCastro
            }
        }
        catch(SQLException e) {
            e.printStackTrace();
            throw new Exception("Error en rutina obtenerDistritos [ImportacionComprobantesDAO]... \n"+e.getMessage());
        }
finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return doc;
    }      
    
    
    /** verifica si un documento ya existe en la tabla de comprobantes */
    public boolean existeDocumento(String dstrct, String tipodoc, String numdoc) throws Exception {
        Connection con = null;
        PreparedStatement st     = null;
        ResultSet         rs     = null;
        boolean           existe = false;
        String            query  = "SQL_VERIFICAR_DOCUMENTO";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, dstrct );
            st.setString(2, tipodoc);
            st.setString(3, numdoc );
            rs = st.executeQuery();
            if (rs.next()){
              existe = true;
            }
        }
        catch(SQLException e) {
            e.printStackTrace();
            throw new Exception("Error en rutina existeDocumento [ImportacionComprobantesDAO]... \n"+e.getMessage());
        }
finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return existe;
    }    
    
    
    /** verifica si un nit ya existe en la tabla de nits*/
    public boolean existeNit(String nit) throws Exception {
        Connection con = null;
        PreparedStatement st     = null;
        ResultSet         rs     = null;
        boolean           existe = false;
        String            query  = "SQL_VERIFICAR_NIT";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, nit);
            rs = st.executeQuery();
            if (rs.next()){
              existe = true;
            }
        }
        catch(SQLException e) {
            e.printStackTrace();
            throw new Exception("Error en rutina existeNit [ImportacionComprobantesDAO]... \n"+e.getMessage());
        }
finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return existe;
    }      
    
    /** captura el estado de un periodo A: abierto, C: cerrado*/
    public String   obtenerEstadoPeriodoContable(String dstrct, String ano, String mes) throws Exception {
        Connection con = null;
        PreparedStatement st     = null;
        ResultSet         rs     = null;
        String            estado = "";
        String            query  = "SQL_ESTADO_PERIODO_CONTABLE";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, dstrct);
            st.setString(2, ano   );
            st.setString(3, mes   );
            //System.out.println(st.toString());
            rs = st.executeQuery();
            if (rs.next()){
              estado =  rs.getString("ac");
              //System.out.println(rs.getString("ac"));
            }
        }
        catch(SQLException e) {
            e.printStackTrace();
            throw new Exception("Error en rutina existeNit [ImportacionComprobantesDAO]... \n"+e.getMessage());
        }
finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return estado;
    }      
    
    /** verifica si un tipo de operacion ya existe*/
    public boolean existeTipoOperacion(String tipo) throws Exception {
        Connection con = null;
        PreparedStatement st     = null;
        ResultSet         rs     = null;
        boolean           existe = false;
        String            query  = "SQL_TIPO_OPERACION";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, tipo);
            rs = st.executeQuery();
            if (rs.next()){
              existe = true;
            }
        }
        catch(SQLException e) {
            e.printStackTrace();
            throw new Exception("Error en rutina existeTipoOperacion [ImportacionComprobantesDAO]... \n"+e.getMessage());
        }
finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return existe;
    }      
    
    
  
    /** de la cuenta y del auxiliar asignado */
    public String searchCuenta(String dstrct, String cuenta, String tipoauxiliar, String auxiliar, String tercero) throws Exception {
        Connection con = null;
        PreparedStatement st      = null;
        ResultSet         rs      = null;
        String            msg     = "Cuenta no encontrada....";
        String            query   = "SQL_CUENTA";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString( 1 , tipoauxiliar  );
            st.setString( 2 , auxiliar      );
            st.setString( 3 , tercero       );
            st.setString( 4 , dstrct        );
            st.setString( 5 , cuenta        );      
            rs = st.executeQuery();
            if (rs.next()){
                
                if ( rs.getString("detalle").equals("N"))
                    msg = "Se requiere una cuenta de detalle, la que esta ingresando no es valida";
                else if ( rs.getString("auxiliar").equals("NOT-FOUND") && rs.getString("subledger").equals("S") )
                    msg = "Auxiliar requerido, o el que asigno no esta relacionado a la cuenta";
                else if ( !auxiliar.equals("") && rs.getString("subledger").equals("N") )
                    msg = "No se requiere auxiliar";
                else if ( rs.getString("tercero").equals("M") && tercero.equals(""))
                    msg = "Se requiere un tercero";
                else if ( rs.getString("tercero").equals("M") && !tercero.equals("") && rs.getString("nit").equals("NOT-FOUND") )
                    msg = "El tercero que ingreso no esta registrado.";                
                else if ( rs.getString("tercero").equals("O") && !tercero.equals("") && rs.getString("nit").equals("NOT-FOUND") )
                    msg = "El tercero que ingreso no esta registrado.";                
                else
                    msg = "OK";
            }
        }
        catch(Exception e) {
            throw new Exception("Error en rutina searchCuenta [ImportacionComprobantesDAO]... \n"+e.getMessage());
        }
finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return msg;
    }    
    
    
    
    
    
    
    
    /** verifica si una planilla ya existe en la tabla de nits*/
    public boolean existePlanilla(String dstrct, String numpla) throws Exception {
        Connection con = null;
        PreparedStatement st     = null;
        ResultSet         rs     = null;
        boolean           existe = false;
        String            query  = "SQL_EXISTE_PLANILLA";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, dstrct );
            st.setString(2, numpla );
            rs = st.executeQuery();
            if (rs.next()){
              existe = true;
            }
        }
        catch(SQLException e) {
            e.printStackTrace();
            throw new Exception("Error en rutina existePlanilla [ImportacionComprobantesDAO]... \n"+e.getMessage());
        }
finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return existe;
    }
    
    /** verifica si una remesa ya existe en la tabla de nits*/
    public boolean existeRemesa(String dstrct, String numrem) throws Exception {
        Connection con = null;
        PreparedStatement st     = null;
        ResultSet         rs     = null;
        boolean           existe = false;
        String            query  = "SQL_EXISTE_REMESA";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, dstrct );
            st.setString(2, numrem );
            rs = st.executeQuery();
            if (rs.next()){
              existe = true;
            }
        }
        catch(SQLException e) {
            e.printStackTrace();
            throw new Exception("Error en rutina existeRemesa [ImportacionComprobantesDAO]... \n"+e.getMessage());
        }
finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return existe;
    }    
    
    
    /** verifica si una remesa ya existe en la tabla de nits*/
    public boolean existeFacturaProveedor(String dstrct, String proveedor, String tipo_documento, String documento) throws Exception {
        Connection con = null;
        PreparedStatement st     = null;
        ResultSet         rs     = null;
        boolean           existe = false;
        String            query  = "SQL_EXISTE_FP";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, dstrct         );
            st.setString(2, proveedor      );
            st.setString(3, tipo_documento );
            st.setString(4, documento      );
            rs = st.executeQuery();
            if (rs.next()){
              existe = true;
            }
        }
        catch(SQLException e) {
            e.printStackTrace();
            throw new Exception("Error en rutina existeFacturaProveedor [ImportacionComprobantesDAO]... \n"+e.getMessage());
        }
finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return existe;
    }        
    
    /** verifica si una remesa ya existe en la tabla de nits*/
    public boolean existeFacturaCliente(String dstrct, String tipo_documento, String documento) throws Exception {
        Connection con = null;
        PreparedStatement st     = null;
        ResultSet         rs     = null;
        boolean           existe = false;
        String            query  = "SQL_EXISTE_FC";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, dstrct         );
            st.setString(2, tipo_documento );
            st.setString(3, documento      );
            rs = st.executeQuery();
            if (rs.next()){
              existe = true;
            }
        }
        catch(SQLException e) {
            e.printStackTrace();
            throw new Exception("Error en rutina existeFacturaCliente [ImportacionComprobantesDAO]... \n"+e.getMessage());
        }
finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return existe;
    }      
    
      
    /** verifica si una remesa ya existe en la tabla de nits*/
    public boolean existeIngreso(String dstrct, String tipo_documento, String num_ingreso) throws Exception {
        Connection con = null;
        PreparedStatement st     = null;
        ResultSet         rs     = null;
        boolean           existe = false;
        String            query  = "SQL_EXISTE_ING";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, dstrct         );
            st.setString(2, tipo_documento );
            st.setString(3, num_ingreso    );
            rs = st.executeQuery();
            if (rs.next()){
              existe = true;
            }
        }
        catch(SQLException e) {
            e.printStackTrace();
            throw new Exception("Error en rutina existeIngreso [ImportacionComprobantesDAO]... \n"+e.getMessage());
        }
finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return existe;
    }      
    
      
    /** verifica si una remesa ya existe en la tabla de nits*/
    public boolean existeEgreso(String dstrct, String tipo_documento, String documento) throws Exception {
        Connection con = null;
        PreparedStatement st     = null;
        ResultSet         rs     = null;
        boolean           existe = false;
        String            query  = "SQL_EXISTE_EGR";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, dstrct         );
            st.setString(2, tipo_documento );
            st.setString(3, documento      );
            rs = st.executeQuery();
            if (rs.next()){
              existe = true;
            }
        }
        catch(SQLException e) {
            e.printStackTrace();
            throw new Exception("Error en rutina existeEgreso [ImportacionComprobantesDAO]... \n"+e.getMessage());
        }
finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return existe;
    }        

/**
     * Metodo para validar un codigo abc
     * @autor mfontalvo
     * @param codigo abc
     * @throws Exception.
     */
    public boolean searchABC(String abc) throws SQLException {
        Connection con = null;
        PreparedStatement st     = null;
        ResultSet         rs     = null;
        boolean           existe = false;
        String query = "SQL_ABC";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString( 1 , abc  );
            rs = st.executeQuery();
            if (rs.next()){
                existe = true;
            }
        }
        catch(SQLException e) {
            throw new SQLException("Error en rutina searchABC [ImportacionCXPDAO]... \n"+e.getMessage());
        }
finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return existe;
    }       
    
}