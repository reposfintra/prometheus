/********************************************************************
 *      Nombre Clase.................   Tipo_DoctoDAO.java
 *      Descripcion..................   Data Access Object
 *      Autor........................   Ing. Andres Maturana De La Cruz
 *      Fecha........................   8 de junio de 2006
 *      Version......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.finanzas.contab.model.DAO;

import java.sql.*;
import java.util.*;
import com.tsp.finanzas.contab.model.beans.*;
import com.tsp.operation.model.DAOS.MainDAO;

/**
 *
 * @author  Ing. Andres Maturana De La Cruz
 */
public class Tipo_DoctoDAO extends MainDAO{
    private Tipo_Docto docto;
    private TreeMap treemap;
    private Vector vector;

    /** Crea una nueva instancia de  Tipo_DoctoDAO */
    public Tipo_DoctoDAO() {
        super("Tipo_DoctoDAO.xml");
    }
    public Tipo_DoctoDAO(String dataBaseName) {
        super("Tipo_DoctoDAO.xml", dataBaseName);
    }

    public List<Tipo_Docto> ListarTipoDoc() throws SQLException {
        List<Tipo_Docto> lis = new LinkedList<Tipo_Docto>();
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_LISTAR_TIPO_DOCTO";
        try {
            con = this.conectarJNDI(query);
            if(con!=null){
            ps = con.prepareStatement(this.obtenerSQL(query));
            rs = ps.executeQuery();
            while (rs.next()) {
                 Tipo_Docto tp = new Tipo_Docto();
                 tp.setCodigo(rs.getString("codigo"));
                 tp.setCodigo_interno(rs.getString("codigo_interno"));
                 tp.setDescripcion(rs.getString("descripcion"));
                 lis.add(tp);
            }


        }
        }
        catch(SQLException e)
        {
             System.out.print("<-LISTA TP--->"+e.getMessage());
            throw new SQLException("ERROR DURANTE Listar " + e.getMessage() + " " + e.getErrorCode());
        }
        finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return lis;
    }




    public Tipo_Docto VerTipo_Docto(String dstrct,String code) throws SQLException{
       Tipo_Docto td=null;
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        String query = "SQL_OBTENER";
        try {
            con = this.conectarJNDI(query);
            if(con!=null)
            {
            ps = con.prepareStatement(this.obtenerSQL(query));
            ps.setString(1, dstrct);
            ps.setString(2, code);
            rs = ps.executeQuery();
            if(rs.next())
            {
             td=new Tipo_Docto();
             td.setDescripcion(rs.getString("descripcion"));
           }
        }
        }
        catch(SQLException e)
        {
             System.out.print("<-OBKJETO TP->"+e.getMessage());
            throw new SQLException("ERROR DURANTE BUscar tp " + e.getMessage() + " " + e.getErrorCode());
        }
        finally {
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return td;
    }

    /**
     * Inserta un nuevo registro en el archivo.
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void insertar() throws SQLException{
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection        con   = null;
        String sql = "SQL_INSERT";
        try{
            con        = conectarJNDI( sql );           
            st         = con.prepareStatement(this.obtenerSQL(sql));
            st.setString(1, docto.getCodigo());
            st.setString(2, docto.getDescripcion());
            st.setString(3, docto.getCodigo_interno());
            st.setString(4, docto.getTercero());
            st.setString(5, docto.getManeja_serie());
            st.setString(6, docto.getPrefijo());
            st.setString(7, docto.getPrefijo_anio());
            st.setString(8, docto.getPrefijo_mes());
            st.setString(9, docto.getUsuario());
            st.setString(10, docto.getUsuario());
            st.setString(11, docto.getBase());
            st.setString(12, docto.getSerie_ini());
            st.setString(13, docto.getSerie_fin());
            st.setString(14, docto.getLong_serie());
            st.setString(15, docto.getDistrito());
            st.setString(16, docto.getEsDiario());

            //////System.out.println("........... INSERT \n" + st);
            st.executeUpdate();
        }
        catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE insertar() " + e.getMessage() + " " + e.getErrorCode());
        }finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
    }

    /**
     * Obtiene un listado de resgitros del archivo tipo de documento.
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void codigosInternos() throws SQLException{
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection        con   = null;
        String sql = "SQL_CODINTERNOS";
        try{
            con        = conectarJNDI( sql );
            st         = con.prepareStatement( this.obtenerSQL(sql) );
            rs = st.executeQuery();

            this.treemap = new TreeMap();
            while( rs.next() ){
                this.treemap.put("[" + rs.getString("document_type") + "] " + rs.getString("document_name"), rs.getString("document_type"));
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE codigosInternos() " + e.getMessage() + " " + e.getErrorCode());
        }finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
    }

    /**
     * Obtiene un registro del archivo.
     * @autor Ing. Andres Maturana De La Cruz
     * @param dstrct Codigo del Distrito
     * @param code Codigo del tipo de comprobante
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void obtener(String dstrct, String code) throws SQLException{
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection        con   = null;
        String sql = "SQL_OBTENER";
        try{
            con        = conectarJNDI( sql );            
            st         = con.prepareStatement( this.obtenerSQL(sql) );

            st.setString(1, dstrct);
            st.setString(2, code);

            //////System.out.println("............... OBTENER: " + st);
            rs = st.executeQuery();

            this.docto = null;
            if ( rs.next() ){
                this.docto = new Tipo_Docto();
                this.docto.Load(rs);
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE obtener(String dstrct, String code) " + e.getMessage() + " " + e.getErrorCode());
        }finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
    }

    /**
     * Realiza la busqueda en el archivo.
     * @autor Ing. Andres Maturana De La Cruz
     * @param dstrct Codigo del Distrito
     * @param code Codigo del tipo de comprobante
     * @param codeint Codigo interno
     * @param descrip Descripcion del tipo de comprobante
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void search(String dstrct, String code, String codeint, String descrip) throws SQLException{
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection        con   = null;
        String sql = "SQL_SEARCH";
        try{
            con        = conectarJNDI( sql );
            st         = con.prepareStatement( this.obtenerSQL(sql) );
            st.setString(1, dstrct);
            st.setString(2, "%" + code + "%");
            st.setString(3, "%" + codeint + "%");
            st.setString(4, "%" + descrip + "%");

            //////System.out.println("............... SEARCH: " + st);
            rs = st.executeQuery();

            this.docto = null;
            this.vector = new Vector();
            while ( rs.next() ){
                this.docto = new Tipo_Docto();
                this.docto.Load(rs);
                this.docto.setDocument_name(rs.getString("document_name"));
                this.vector.add(docto);
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE search " + e.getMessage() + " " + e.getErrorCode());
        }finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
    }

    /**
     * Actualiza un registro en el archivo.
     * @autor Ing. Andres Maturana De La Cruz
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void actualizar() throws SQLException{
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection        con   = null;
        String sql = "SQL_UPDATE";
        try{
            con        = conectarJNDI( sql );
            st         = con.prepareStatement( this.obtenerSQL(sql) );
            st.setString(1, docto.getReg_status());
            st.setString(2, docto.getDescripcion());
            st.setString(3, docto.getCodigo_interno());
            st.setString(4, docto.getTercero());
            st.setString(5, docto.getManeja_serie());
            st.setString(6, docto.getPrefijo());
            st.setString(7, docto.getPrefijo_anio());
            st.setString(8, docto.getPrefijo_mes());
            st.setString(9, docto.getUsuario());
            st.setString(10, docto.getSerie_ini());
            st.setString(11, docto.getSerie_fin());
            st.setString(12, docto.getLong_serie());
            st.setString(13, docto.getEsDiario());
            st.setString(14, docto.getDistrito());
            st.setString(15, docto.getCodigo());

            ////System.out.println("........... ACTUALIZAR \n" + st);
            st.executeUpdate();
        }
        catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE actualizar() " + e.getMessage() + " " + e.getErrorCode());
        }finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
    }

    /**
     * Verifica si un codigo interno ya fue asignado en los comprobantes no diarios.
     * @autor Ing. Andres Maturana De La Cruz
     * @param dstrct Codigo del Distrito
     * @param codeint Codigo del interno del tipo de comprobante
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public boolean isCodigoInternoUsado(String dstrct, String codint) throws SQLException{
        PreparedStatement st = null;
        ResultSet rs = null;
        Connection        con   = null;
        String sql = "SQL_UTILIZACODINT";
        try{
            con        = conectarJNDI( sql );
            st         = con.prepareStatement( this.obtenerSQL(sql) );
            st.setString(1, dstrct);
            st.setString(2, codint);
            rs = st.executeQuery();

            if( rs.next() ){
                return true;
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE isCodigoInternoUsado() " + e.getMessage() + " " + e.getErrorCode());
        }finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }

        return false;
    }

    /**
     * Getter for property docto.
     * @return Value of property docto.
     */
    public com.tsp.finanzas.contab.model.beans.Tipo_Docto getDocto() {
        return docto;
    }

    /**
     * Setter for property docto.
     * @param docto New value of property docto.
     */
    public void setDocto(com.tsp.finanzas.contab.model.beans.Tipo_Docto docto) {
        this.docto = docto;
    }

    /**
     * Getter for property treemap.
     * @return Value of property treemap.
     */
    public java.util.TreeMap getTreemap() {
        return treemap;
    }

    /**
     * Setter for property treemap.
     * @param treemap New value of property treemap.
     */
    public void setTreemap(java.util.TreeMap treemap) {
        this.treemap = treemap;
    }

    /**
     * Getter for property vector.
     * @return Value of property vector.
     */
    public java.util.Vector getVector() {
        return vector;
    }

    /**
     * Setter for property vector.
     * @param vector New value of property vector.
     */
    public void setVector(java.util.Vector vector) {
        this.vector = vector;
    }
    /**
     *Metodo que permite obtener todos los tipo_docto
     *@autor: David Pina
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void ObtenerTipo_docto() throws SQLException{
        PreparedStatement st = null;
        ResultSet rs = null;
         Connection        con   = null;
        String sql = "SQL_OBTENER_TIPODOCTO";
        try{
            con        = conectarJNDI( sql );
            st         = con.prepareStatement( this.obtenerSQL(sql) );
            st.setString( 1, this.docto.getDistrito() );
            rs = st.executeQuery();
            this.treemap = new TreeMap();
            while( rs.next() ){
                this.treemap.put( rs.getString("descripcion"), rs.getString("codigo") );
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE ObtenerTipo_docto() " + e.getMessage() + " " + e.getErrorCode());
        }finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
    }

    /**
     * Metodo que obtiene la consulta para actualizar Tipo_docto
     * @autor David Pina Lopez
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     * @return El sql de actualizar tipo_docto
     */
    public String getActualizar() throws SQLException{
        PreparedStatement st = null;
        ResultSet rs = null;
        String result = "";
        Connection        con   = null;
        String sql = "SQL_UPDATE_SERIE";
        try{
             con        = conectarJNDI( sql );
            
            st         = con.prepareStatement(this.obtenerSQL(sql) );
            st.setString( 1, docto.getUsuario() );
            st.setString( 2, docto.getSerie_act() );
            st.setString( 3, docto.getDistrito() );
            st.setString( 4, docto.getCodigo() );
            result = st.toString();
        }
        catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE getActualizar() " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return result;
    }

}
