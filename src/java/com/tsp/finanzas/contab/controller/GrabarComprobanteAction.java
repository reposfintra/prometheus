/********************************************************************
 *  Nombre Clase.................   PrecintosAnularAction.java
 *  Descripci�n..................   acciones para la grabacion del comprobante
 *  Autor........................   Ing. Ivan Dario Gomez Vanegas
 *  Fecha........................   7 de junio de 2006, 05:30 PM
 *  Versi�n......................   1.0
 *  Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/



package com.tsp.finanzas.contab.controller;

import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.finanzas.contab.model.beans.*;
import com.tsp.finanzas.contab.model.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.*;


public class GrabarComprobanteAction extends Action {
    
    /** Creates a new instance of GrabarComprobanteAction */
    public GrabarComprobanteAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next = "";
        try{
            HttpSession session = request.getSession();
            Usuario usuario = (Usuario) session.getAttribute("Usuario");
            String distrito = (String) session.getAttribute("Distrito");
            String opcion   = (request.getParameter("opcion")!=null)?request.getParameter("opcion"):"";
            String OP = (request.getParameter("OP")!=null)?request.getParameter("OP"):"";
            String maxfila = (request.getParameter("maxfila")!=null)?request.getParameter("maxfila"):"";
            String msg ="";
            String periodo = (String)session.getAttribute("periodo");
            String tipodoc = (request.getParameter("tipodoc")!=null)?request.getParameter("tipodoc"):"";
            String numdoc  = (request.getParameter("numdoc")!=null)?request.getParameter("numdoc"):"";
            String msgError ="";
            
            ////System.out.println("GrabarComprobanteAction...  " + OP);
            
            if(OP.equals("INICIO")){
                model.GrabarComprobanteSVC.buscarPeriodos(distrito);
                next = "/jsp/finanzas/contab/comprobante/Periodo.jsp";
                
            }else if(OP.equals("LISTAR_TIPO_DOC")){
                String per = request.getParameter("periodo");
                session.setAttribute("periodo",per);
                model.GrabarComprobanteSVC.setComprobante(null);
                model.GrabarComprobanteSVC.buscarTipoDoc(usuario.getDstrct());
                next = "/jsp/finanzas/contab/comprobante/grabacionComprobante.jsp";
                
            }else if(OP.equals("VERRIFICAR_NUM_DOC")){
                String codigoTipo = request.getParameter("valor");
                next = "/jsp/finanzas/contab/comprobante/search.jsp?opcion=TIPO&codigo="+codigoTipo;
                
            }else if(OP.equals("VerificarCodContable")){
                String CodContable = request.getParameter("CodContable");
                String Id = request.getParameter("Id");
                model.GrabarComprobanteSVC.buscarCuenta(usuario.getDstrct(), CodContable);
                if(model.GrabarComprobanteSVC.isExisteCuenta()){
                    
                    Hashtable cuenta = model.GrabarComprobanteSVC.getCuenta();
                    if (cuenta.get("modulo1").toString().equalsIgnoreCase("S")){
                        model.subledgerService.buscarCodigosCuenta(CodContable);
                        next = "/jsp/finanzas/contab/comprobante/search.jsp?opcion=CUENTA&campo="+Id;                        
                    }else{
                        msg = "Esta cuenta no esta hailitada para este moodulo!!";
                        next = "/jsp/finanzas/contab/comprobante/search.jsp?opcion=MENSAJE&campo="+Id+"&mensaje="+msg;                        
                    }
                  
                }else{
                    msg = "El numero de cuenta no existe en la base de datos!!";
                    next = "/jsp/finanzas/contab/comprobante/search.jsp?opcion=MENSAJE&campo="+Id+"&mensaje="+msg;
                }
                
            }else if(OP.equals("GUARDAR")){
                
                // Estos son los campos de la cabecera
                String TipoDoc          = (request.getParameter("TipoDoc"        )!=null?request.getParameter("TipoDoc"        ):"");
                String NumDoc           = (request.getParameter("NumDoc"         )!=null?request.getParameter("NumDoc"         ):"");
                String serie            = (request.getParameter("serie"          )!=null?request.getParameter("serie"          ):"");
                String fecha_documento  = (request.getParameter("fecha_documento")!=null?request.getParameter("fecha_documento"):"");
                String terceroCabecera  = (request.getParameter("terceroCabecera")!=null?request.getParameter("terceroCabecera"):"");
                String concepto         = (request.getParameter("concepto"       )!=null?request.getParameter("concepto"       ):"");
                String TotalDebito      = (request.getParameter("TotalDebito"    )!=null?request.getParameter("TotalDebito"    ):"");
                String TotalCredito     = (request.getParameter("TotalCredito"   )!=null?request.getParameter("TotalCredito"   ):"");
                String grupo            = (request.getParameter("grupo"          )!=null?request.getParameter("grupo"          ):"");
                


                ComprobanteFacturas comprobante = new ComprobanteFacturas();
                //////System.out.println("TIPO DOC ---->"+TipoDoc);
                Hashtable DatosTipoDoc = model.GrabarComprobanteSVC.buscarDatosTipoDoc(distrito,TipoDoc);
                boolean manejaserie = ((String)DatosTipoDoc.get("maneja_serie")).equals("S")?true:false;
                boolean aumentarSerie = false;
                
                if(!manejaserie){
                    serie = (serie != null)?serie:"";
                    comprobante.setNumdoc  ( serie+NumDoc );
                }else{
                    comprobante.setNumdoc  ( NumDoc );
                    aumentarSerie = true;
                }

                comprobante.setDstrct  ( distrito               );
                comprobante.setTipodoc ( TipoDoc                );

                comprobante.setSerie   ( serie                  );
                comprobante.setSucursal( usuario.getId_agencia());
                comprobante.setPeriodo ( periodo                );
                comprobante.setFechadoc( fecha_documento        );
                comprobante.setDetalle ( concepto               );
                comprobante.setTercero ( terceroCabecera        );
                comprobante.setTipo("C");
                comprobante.setGrupo_transaccion(Integer.parseInt(grupo));
                TotalDebito  =  (TotalDebito.equals("")?"0":TotalDebito);
                TotalCredito =  (TotalCredito.equals("")?"0":TotalCredito);

                comprobante.setTotal_debito(Double.parseDouble( TotalDebito.replaceAll(",","") )  );
                comprobante.setTotal_credito(Double.parseDouble( TotalCredito.replaceAll(",","") )  );
                String []monedaBase = model.GrabarComprobanteSVC.getMoneda(usuario.getCia()).split("-");
                comprobante.setMoneda(monedaBase[0]);
                comprobante.setAprobador(usuario.getLogin());
                comprobante.setBase(monedaBase[1]);
                comprobante.setDocumento_interno((String)DatosTipoDoc.get("codigo_interno"));
                comprobante.setOrigen("COMPROBANTE");
                comprobante.setTipo_operacion("009");
                List items = new LinkedList();
                boolean Error = false;
                int TotalItem = 0;
                int MaxFi = Integer.parseInt(maxfila);
                for(int i=1; i <= MaxFi; i++){

                    String CodContable  = request.getParameter("CodContable"+i);
                    if(CodContable!= null){
                        TotalItem++;

                        String descripcion  = (request.getParameter("descripcion"+i) !=null?request.getParameter("descripcion"+i        ):"");
                        String tercero      = (request.getParameter("tercero"+i)     !=null?request.getParameter("tercero"+i            ):"");
                        String ReqTercero   = (request.getParameter("ReqTercero"+i)  !=null?request.getParameter("ReqTercero"+i         ):"");
                        String subledger    = (request.getParameter("subledger"+i)   !=null?request.getParameter("subledger"+i          ):"");
                        String idSubledger  = (request.getParameter("idSubledger"+i) !=null?request.getParameter("idSubledger"+i        ):"");
                        String ReqAuxiliar  = (request.getParameter("ReqAuxiliar"+i) !=null?request.getParameter("ReqAuxiliar"+i        ):"");
                        String abc          = (request.getParameter("abc"+i)         !=null?request.getParameter("abc"+i                ):"");
                        String tdoc_rel     = (request.getParameter("tdoc_rel"+i)    !=null?request.getParameter("tdoc_rel"+i           ):"");
                        String doc_rel      = (request.getParameter("doc_rel"+i)     !=null?request.getParameter("doc_rel"+i            ):"");
                        String valorDebito  = (request.getParameter("valorDebito"+i) !=null?request.getParameter("valorDebito"+i        ):"");
                        String valorCredito = (request.getParameter("valorCredito"+i)!=null?request.getParameter("valorCredito"+i       ):"");

                        valorDebito  =  (valorDebito.equals("")?"0":valorDebito);
                        valorCredito =  (valorCredito.equals("")?"0":valorCredito);

                        model.GrabarComprobanteSVC.buscarCuenta(usuario.getDstrct(), CodContable);
                        boolean Existe = model.GrabarComprobanteSVC.isExisteCuenta();
                        ComprobanteFacturas comprodet = new ComprobanteFacturas();
                        comprodet.setDstrct(distrito);
                        comprodet.setTipodoc(TipoDoc);
                        comprodet.setNumdoc(comprobante.getNumdoc());
                        comprodet.setPeriodo(periodo );
                        comprodet.setCuenta(CodContable);
                        comprodet.setAuxiliar(idSubledger);
                        comprodet.setDetalle(descripcion);
                        comprodet.setAbc(abc);
                        comprodet.setTdoc_rel   ( tdoc_rel );
                        comprodet.setNumdoc_rel ( doc_rel  );
                        comprodet.setTotal_debito(Double.parseDouble(valorDebito.replaceAll(",","")));
                        comprodet.setTotal_credito(Double.parseDouble(valorCredito.replaceAll(",","")));
                        comprodet.setTercero(tercero);
                        comprodet.setBase(usuario.getBase());
                        comprodet.setDocumento_interno((String)DatosTipoDoc.get("codigo_interno"));
                        comprodet.setExisteCuenta(Existe);
                        comprodet.setTipo("D");
                        comprodet.setOrigen("COMPROBANTE");
                        Vector tipSubledger = null;
                        if(!Existe){
                            Error = true;
                        }else{
                            Hashtable cuenta = model.GrabarComprobanteSVC.getCuenta();
                            if (cuenta.get("modulo1").toString().equalsIgnoreCase("N")){
                                comprodet.setExisteCuenta(false);
                                Error = true;
                            }
                            model.subledgerService.buscarCodigosCuenta(CodContable);
                            tipSubledger = model.subledgerService.getCodigos();
                        }
                        comprodet.setCodSubledger(tipSubledger);
                        items.add(comprodet);


                    }//fin del if

                }//fin del for
                comprobante.setItems(items);
                comprobante.setTotal_items(TotalItem);
                model.GrabarComprobanteSVC.setComprobante(comprobante);
                List listaContable = new LinkedList();
                listaContable.add(comprobante);
                MaxFi = TotalItem;
                if(!Error){

                    if(!opcion.equals("modificar")){
                        
                        if (model.GrabarComprobanteSVC.existeDocumento(distrito,TipoDoc, NumDoc)){
                            msg = "Este numero de documento ya existe dentro de la base de datos ingrese otro para poder continuar...";
                            next = "/jsp/finanzas/contab/comprobante/grabacionComprobante.jsp?ms="+msg+"&maxfila="+maxfila+"&opcion="+opcion;
                        } else {
                            
                            msg ="El comprobante fue guardado con exito!!";
                            model.ContabilizacionFacSvc.insertar(listaContable, usuario.getLogin());
                            msgError =  model.ContabilizacionFacSvc.getESTADO();
    //                        if((comprobante.getTotal_debito() == comprobante.getTotal_credito())&&(msgError.equals(""))){
    //                            try{
    //                                model.GrabarComprobanteSVC.AgregarMayorizacion(listaContable, usuario.getLogin());
    //                                msg = "El comprobante fue guardado y agregado a mayorizacion!!";
    //                            }catch(Exception e){
    //                            }
    //                        }
                            if(!msgError.equals("")){
                                msg = msgError;
                            }else if(aumentarSerie){
                                model.GrabarComprobanteSVC.UpdateSerie(distrito, TipoDoc);
                            }
                            next = "/jsp/finanzas/contab/comprobante/grabacionComprobante.jsp?ms="+msg+"&maxfila="+MaxFi;                            
                        }
                    }else{
                        model.GrabarComprobanteSVC.UpdateComprobante(comprobante);
                        msg = "El comprobante fue guardado con exito";
                        next = "/jsp/finanzas/contab/comprobante/grabacionComprobante.jsp?ms="+msg+"&maxfila="+MaxFi+"&opcion="+opcion;

                    }

                }else{
                    msg = "Los numeros de cuentas marcados con rojo no existen en la base de datos o no pueden ser usadas dentro de este modulo!!!";
                    next = "/jsp/finanzas/contab/comprobante/grabacionComprobante.jsp?ms="+msg+"&maxfila="+MaxFi+"&opcion="+opcion;
                }

                
                
                // next = "/jsp/finanzas/contab/comprobante/search.jsp?opcion=TIPO&codigo="+codigoTipo;
                
            }else if(OP.equals("MODIFICAR")){
                model.GrabarComprobanteSVC.buscarTipoDoc(usuario.getDstrct());
                
                
                
                if(tipodoc!=null){//para saber si se va ala principal
                    model.GrabarComprobanteSVC.BuscarComprobantes(usuario.getDstrct(), tipodoc,  numdoc);
                    next = "/jsp/finanzas/contab/comprobante/BuscarComprobante.jsp?tipodoc="+tipodoc+"&numdoc"+numdoc;
                }else{
                    next = "/jsp/finanzas/contab/comprobante/BuscarComprobante.jsp";
                    
                }
                
                
            }else if(OP.equals("BUSCAR_COMPROBANTE")){
                String GrupTrans = request.getParameter("grupo");
                String dstrct    = request.getParameter("dstrct");
                int grupo = Integer.parseInt(GrupTrans);
                
                ComprobanteFacturas comprobante = model.GrabarComprobanteSVC.ComprobanteSearch(dstrct, tipodoc, numdoc, grupo);
                List item = comprobante.getItems();
                for(int i=0; item!= null && i< item.size();i++){
                    ComprobanteFacturas comprodet = (ComprobanteFacturas) item.get(i);
                    
                    model.subledgerService.buscarCodigosCuenta(comprodet.getCuenta());
                    Vector tipSubledger = model.subledgerService.getCodigos();
                    comprodet.setCodSubledger(tipSubledger);
                }
                
                session.setAttribute("periodo",comprobante.getPeriodo());
                model.GrabarComprobanteSVC.buscarTipoDoc(usuario.getDstrct());
                model.GrabarComprobanteSVC.setComprobante(comprobante);
                
                model.tipo_doctoSvc.obtener(comprobante.getDstrct(), comprobante.getTipodoc() );
                Tipo_Docto t = model.tipo_doctoSvc.getDocto();
                if(comprobante.getFecha_aplicacion().equals("0099-01-01 00:00:00") && (t!=null && t.getEsDiario().equalsIgnoreCase("S")))
                  next = "/jsp/finanzas/contab/comprobante/grabacionComprobante.jsp?maxfila="+comprobante.getTotal_items()+"&opcion=modificar";
                else
                  next = "/jsp/finanzas/contab/adminComprobantes/verComprobante.jsp?maxfila="+comprobante.getTotal_items()+"&opcion=modificar";
                
            }
            
            else if(OP.equals("VALIDAR")){
                next = "/jsp/finanzas/contab/comprobante/search.jsp?opcion=VALIDAR&maxfila="+maxfila+"&periodo="+periodo+"&op="+opcion;
                
            }else if(OP.equals("BuscarTercero")){
                String idcampo = request.getParameter("idcampo");
                String valor   = request.getParameter("valor");
                model.GrabarComprobanteSVC.busquedaTercero(valor);
                next ="/jsp/finanzas/contab/comprobante/BuscarTercero.jsp?idcampo="+idcampo;
            } else if(OP.equals("VERIFICAR_ABC")){
                String abc = request.getParameter("abc");      
                com.tsp.operation.model.Model modelOp = (com.tsp.operation.model.Model) session.getAttribute("model");
                if (modelOp.ImportacionCXPSvc.searchABC(abc))
                    next   = "/jsp/finanzas/contab/comprobante/search.jsp?opcion=VERIFICAR_ABC&status=ok";
                else
                    next   = "/jsp/finanzas/contab/comprobante/search.jsp?opcion=VERIFICAR_ABC&status=no";
            } else if (OP.equals("VERIFICAR_DOCREL")){
                String tdoc     = request.getParameter("tdoc_rel");
                String doc      = request.getParameter("doc_rel");
                String tercero  = request.getParameter("tercero");
                
                boolean estado = false;
                
                if ( tdoc.equalsIgnoreCase("001") )
                    estado = model.ICSvc.existePlanilla( distrito, doc );
                else if ( tdoc.equalsIgnoreCase("002") )
                    estado = model.ICSvc.existeRemesa( distrito, doc);
                else if ( tdoc.equalsIgnoreCase("010") )
                    estado = model.ICSvc.existeFacturaProveedor( distrito, tercero , tdoc, doc);
                else if ( tdoc.equalsIgnoreCase("FAC") )
                    estado = model.ICSvc.existeFacturaProveedor( distrito, tercero , tdoc, doc);
                else if ( tdoc.equalsIgnoreCase("ING") )
                    estado = model.ICSvc.existeIngreso( distrito, tdoc, doc);
                else if ( tdoc.equalsIgnoreCase("EGR") )
                    estado = model.ICSvc.existeEgreso( distrito, tdoc, doc);
                
                
                if (estado)
                    next   = "/jsp/finanzas/contab/comprobante/search.jsp?opcion=VERIFICAR_DOCREL&status=ok";
                else
                    next   = "/jsp/finanzas/contab/comprobante/search.jsp?opcion=VERIFICAR_DOCREL&status=no";
                
            }
            System.out.println("GC NEXT :" + next);
            
            // Redireccionar a la p�gina indicada.
            this.dispatchRequest(next);            
        }catch (Exception e){
            e.printStackTrace();
            throw new ServletException(e.getMessage());
        }

    }
    
}
