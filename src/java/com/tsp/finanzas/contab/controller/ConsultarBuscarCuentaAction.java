/******************************************************************
 * Nombre ......................ConsultarBuscarCuentaAction.java
 * Descripci�n..................Clase Action para obtener una cuenta
 *                              seg�n el nombre corto dado 
 * Autor........................David Pi�a
 * Fecha........................05-06-2006
 * Versi�n......................1.0
 * Coyright.....................Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.finanzas.contab.controller;


import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.finanzas.contab.model.beans.*;
import com.tsp.finanzas.contab.model.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;
/**
 *
 * @author  David Pi�a
 */
public class ConsultarBuscarCuentaAction extends Action{
    
    /** Creates a new instance of ConsultarBuscarClienteAction */
    public ConsultarBuscarCuentaAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String nombre = request.getParameter("nombrecorto");
        String next="/jsp/finanzas/contab/interfaceContable/resultadoBusquedaCuentas.jsp";
        try{
            model.planDeCuentasService.obtenerCuentasNombreCorto( nombre.toUpperCase() );
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
