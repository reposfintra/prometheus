/*************************************************************************
 * Nombre:        CuentaTipoSudledgerEventoAction.java
 * Descripci�n:   Clase Action para ingresar cuentas tipo subledger
 * Autor:         Ing. Diogenes Antonio Bastidas Morales
 * Fecha:         6 de junio de 2006, 08:18 AM
 * Versi�n        1.0
 * Coyright:      Transportes Sanchez Polo S.A.
 *************************************************************************/

package com.tsp.finanzas.contab.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.finanzas.contab.model.*;
import com.tsp.finanzas.contab.model.beans.*;
import com.tsp.finanzas.contab.model.services.*;
//paquete normal
import com.tsp.operation.model.*;
import com.tsp.operation.model.services.TablaGenManagerService;
import com.tsp.operation.model.beans.*;
import com.tsp.exceptions.*;
import com.tsp.util.*;


public class CuentaTipoSudledgerEventoAction extends Action{
    
    /** Creates a new instance of CuentaTipoSudledgerEventoAction */
    public CuentaTipoSudledgerEventoAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next = request.getParameter("carpeta")+"/"+request.getParameter("pagina");
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String distrito = (String) session.getAttribute("Distrito");
        String evento = (request.getParameter("evento") != null )? request.getParameter("evento"): "";
        String cuenta = request.getParameter("cuenta");
        String tipo  = request.getParameter("tipo");
        String ntipo = "";
        boolean sw = false;
        com.tsp.operation.model.Model modelslt;
        modelslt = (com.tsp.operation.model.Model) session.getAttribute("model");
        
        try{
            if(evento.equals("Ingresar")){
                model.planDeCuentasService.searchCuenta(distrito,cuenta);
                PlanDeCuentas pc = model.planDeCuentasService.getCuentas();
                if( pc != null){
                    if (pc.getSubledger().equals("S")){
                        if(!model.subledgerService.existeTipoSubledger(cuenta,tipo)){
                            TablaGen ctsubledger = new TablaGen();
                            ctsubledger.setTable_code(cuenta);
                            ctsubledger.setReferencia("");
                            ctsubledger.setDescripcion("");
                            ctsubledger.setUsuario(usuario.getLogin());
                            ctsubledger.setDato(tipo);
                            model.subledgerService.agregarCuentasTSubledger(ctsubledger);
                            next+="?mensaje=Cuenta Tipo Subledger Creada";
                        }else{
                            next+="?mensaje=Ya existe la relacion";
                        }
                    }
                    else{
                        next+="?mensaje=La cuenta no aplica Subledger";
                    }
                }
                else{
                    next+="?mensaje=La cuenta no Existe";
                }
            }else if(evento.equals("Busqueda")){
                model.subledgerService.busquedaCuentasTipoSubledger(usuario.getDstrct(),cuenta);
            }
            else if(evento.equals("Modificar")){
                //////System.out.println("Modifica");
                ntipo  = request.getParameter("ntipo");
                if(!ntipo.equals(tipo)){
                    if(!model.subledgerService.existeTipoSubledger(cuenta,ntipo) ){
                        TablaGen ctsubledger = new TablaGen();
                        ctsubledger.setTable_code(cuenta);
                        ctsubledger.setReferencia("");
                        ctsubledger.setDescripcion("");
                        ctsubledger.setUsuario(usuario.getLogin());
                        ctsubledger.setDato(ntipo);
                        model.subledgerService.modificarCuentasTSubledger(ctsubledger,tipo);
                        next+="?mensaje=Cuenta Tipo Subledger Modificada&cuenta="+cuenta+"&tipo="+ntipo+"&ntipo="+ntipo+"&modificado=ok";
                    }else{
                        next+="?mensaje=Ya existe la relacion&cuenta="+cuenta+"&tipo="+tipo+"&ntipo="+ntipo;
                    }
                }else{
                    next+="?mensaje=Cuenta Tipo Subledger Modificada&cuenta="+cuenta+"&tipo="+ntipo+"&ntipo="+ntipo+"&modificado=ok";
                }
            }
            else if(evento.equals("Buscar")){
                modelslt.tablaGenService.buscarRegistros("TSUBLEDGER");
                next+="?cuenta="+cuenta+"&tipo="+tipo+"&ntipo="+tipo;
            }
            else if(evento.equals("Anular")){
                TablaGen ctsubledger = new TablaGen();
                ctsubledger.setTable_code(cuenta);
                ctsubledger.setReferencia("");
                ctsubledger.setDescripcion("");
                ctsubledger.setUsuario(usuario.getLogin());
                ctsubledger.setDato(tipo);
                model.subledgerService.eliminarCuentasTSubledger(ctsubledger);
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        this.dispatchRequest(next);
    }
    
}
