/******************************************************************************
 * Nombre clase :                   UpdatePlanDeCuentasAction.java            *
 * Descripcion :                    Clase que maneja los eventos              *
 *                                  relacionados con el programa de           *
 *                                  Actualizar una Cuenta en la BD.           *
 * Autor :                          AMATURANA                                 *
 * Fecha :                          6 de Junio de 2006, 08:59 AM              *
 * Version :                        1.0                                       *
 * Copyright :                      Fintravalores S.A.                   *
 *****************************************************************************/

package com.tsp.finanzas.contab.controller;

import java.io.*;
import java.util.*;
import com.tsp.exceptions.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.util.Util;
import com.tsp.operation.model.beans.*;
import com.tsp.finanzas.contab.model.beans.*;
import com.tsp.finanzas.contab.model.services.*;
import com.tsp.finanzas.contab.model.threads.*;
import java.text.*;
import java.sql.SQLException;
import com.tsp.operation.model.*;

import org.apache.log4j.*;

public class PlanDeCuentasCargarELEMAction extends Action {
    
    Logger logger = Logger.getLogger(this.getClass());
    
    /** Creates a new instance of UpdatePlanDeCuentasAction */
    public PlanDeCuentasCargarELEMAction() {
    }

    public void run () throws ServletException, InformationException {
        
        String next = "/jsp/finanzas/contab/plan_de_cuentas/ingresar/loadElement.jsp?estado=";
        
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String distrito = (String) session.getAttribute("Distrito");
        String cuenta = request.getParameter("cuenta")!=null ? request.getParameter("cuenta") : "";
        String creation_user = usuario.getLogin().toUpperCase();
            
        String msg = "";      
        String command = request.getParameter("command")!=null ? request.getParameter("command") : "";
        
        logger.info("COMMAND: " + command);
        logger.info("CUENTA: " + cuenta);
        
        try{            
            if( !command.equals("setear") ){
                if( cuenta.length()==13 && (cuenta.toUpperCase().startsWith("I")
                        || cuenta.toUpperCase().startsWith("C") || cuenta.toUpperCase().startsWith("G")) ){
                    logger.info("Cargar elementos del gasto");
                    com.tsp.operation.model.Model modelop = (com.tsp.operation.model.Model) session.getAttribute("model");
                    modelop.tablaGenService.buscarRegistrosOrdenadosByDesc("TELEMENTOS");
                    logger.info("Elementos cargados: " + modelop.tablaGenService.obtenerTablas().size());
                    LinkedList tablas = modelop.tablaGenService.obtenerTablas();
                    request.setAttribute("tablas", tablas);
                } else {
                    next += "&msg=La cuenta no tiene la longitud correcto o no empieza por I, C o G";
                    LinkedList tablas = new LinkedList();
                    request.setAttribute("tablas", tablas);
                }
                //model.planDeCuentasService.setHashtable(new Hashtable());
            } else {
                logger.info("Setar vector de otros elementos.");
                model.planDeCuentasService.setHashtable(new Hashtable());
            model.planDeCuentasService.setElementos(new Vector());
                String[] elems = request.getParameterValues("element");
                Vector el = new Vector();
                Hashtable ht = new Hashtable();
                
                if ( elems != null ){
                    for(int i=0; i<elems.length; i++){
                        logger.info("Se recibio: " + elems[i]);
                        el.add(elems[i]);
                        ht.put(elems[i], elems[i]);
                    }
                }
                
                
                com.tsp.operation.model.Model modelop = (com.tsp.operation.model.Model) session.getAttribute("model");
                modelop.tablaGenService.buscarRegistrosOrdenadosByDesc("TELEMENTOS");
                LinkedList tablas = modelop.tablaGenService.obtenerTablas();
                request.setAttribute("tablas", tablas);
                
                model.planDeCuentasService.setElementos(el);
                model.planDeCuentasService.setHashtable(ht);
                logger.info("HASHTABLE: " + ht);                
                next = "/jsp/finanzas/contab/plan_de_cuentas/ingresar/loadElement.jsp?estado=close";
            }
            
            logger.info("NEXT: " + next);
            
        } catch ( Exception e ){      
            e.printStackTrace();
            throw new ServletException( e.getMessage () );            
        }
        
        this.dispatchRequest( next );
        
    }
    
}
