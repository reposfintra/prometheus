/******************************************************************************
 * Nombre clase :                   InsertPlanDeCuentasAction.java            *
 * Descripcion :                    Clase que maneja los eventos              *
 *                                  relacionados con el programa de           *
 *                                  Insertar una Cuenta en la BD.             *
 * Autor :                          LREALES                                   *
 * Fecha :                          6 de Junio de 2006, 07:42 AM              *
 * Version :                        1.0                                       *
 * Copyright :                      Fintravalores S.A.                   *
 *****************************************************************************/

package com.tsp.finanzas.contab.controller;

import java.io.*;
import java.util.*;
import com.tsp.exceptions.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.util.Util;
import com.tsp.finanzas.contab.model.beans.*;
import com.tsp.operation.model.beans.*;
import com.tsp.finanzas.contab.model.services.*;
import com.tsp.finanzas.contab.model.threads.*;
import java.text.*;
import java.sql.SQLException;
import com.tsp.operation.model.*;

import org.apache.log4j.*;

public class InsertPlanDeCuentasAction extends Action {

    Logger logger = Logger.getLogger(this.getClass());

    /** Creates a new instance of InsertPlanDeCuentasAction */
    public InsertPlanDeCuentasAction() {
    }

    public void run() throws ServletException, InformationException {

        String opcion = request.getParameter("op")!=null?request.getParameter("op"):"";
        String next = "";
        if(opcion.equals("VERIFICAR_CUENTA")){
            try{
                String acount = (request.getParameter("acount")!=null)?request.getParameter("acount"):"";
                String elemento = acount.substring( 9, 13 );

                String ag = model.planDeCuentasService.NombreAgencia(acount.substring(1, 3));
                String un = model.planDeCuentasService.NombreUnidad(acount.substring( 3, 6 ));
                //String cli = model.planDeCuentasService.NombreCliente(acount.substring(6, 9));//20100824
                String seccionx = model.planDeCuentasService.NombreSeccion(acount.substring( 6, 9 ));//20100824
                String elem = model.planDeCuentasService.NombreElemento(acount.substring( 9, 13 ));
                //String nomlargo = ag+"-"+un+"-"+cli+"-"+elem;//20100824
                String nomlargo = ag+"-"+un+"-"+seccionx+"-"+elem;//20100824
                if(nomlargo.length()>=49)
                {
                    nomlargo = nomlargo.substring(0,49);
                }
                String nomcorto = ag+"-"+un;

                next = "/jsp/finanzas/contab/plan_de_cuentas/ingresar/Puente.jsp?noml="+nomlargo+"&nomc="+nomcorto;
            }

            catch (Exception e){
                e.printStackTrace();
                throw new ServletException(e.getMessage());
            }
        }
        else{

        next = "/jsp/finanzas/contab/plan_de_cuentas/ingresar/IngresarPlanDeCuentas.jsp";

        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String distrito = (String) session.getAttribute("Distrito");
        String creation_user = usuario.getLogin().toUpperCase();
        String base = usuario.getBase().toUpperCase();

        String msg = "";

        ////////////////////////////////////
        String logMsg = "";
        Vector vec = model.planDeCuentasService.getElementos();
        logger.info("ELEMENTOS ADICIONALES: " + vec.size());
        ///////////////////////////////////


        // distrito
        String cuenta = (request.getParameter("cuenta") != null)?request.getParameter("cuenta").toUpperCase():"";
        String nom_largo = (request.getParameter("nom_largo") != null)?request.getParameter("nom_largo").toUpperCase():"";
        String nom_corto = (request.getParameter("nom_corto") != null)?request.getParameter("nom_corto").toUpperCase():"";
        String observacion = (request.getParameter("observacion") != null)?request.getParameter("observacion"):"";
        String auxiliar = (request.getParameter("auxiliar") != null)?request.getParameter("auxiliar").toUpperCase():"";
        // base
        String modulo1 = (request.getParameter("modulo1") != null)?request.getParameter("modulo1").toUpperCase():"";
        String modulo2 = (request.getParameter("modulo2") != null)?request.getParameter("modulo2").toUpperCase():"";
        String modulo3 = (request.getParameter("modulo3") != null)?request.getParameter("modulo3").toUpperCase():"";
        String modulo4 = (request.getParameter("modulo4") != null)?request.getParameter("modulo4").toUpperCase():"";
        String modulo5 = (request.getParameter("modulo5") != null)?request.getParameter("modulo5").toUpperCase():"";
        String modulo6 = (request.getParameter("modulo6") != null)?request.getParameter("modulo6").toUpperCase():"";
        String modulo7 = (request.getParameter("modulo7") != null)?request.getParameter("modulo7").toUpperCase():"";
        String modulo8 = (request.getParameter("modulo8") != null)?request.getParameter("modulo8").toUpperCase():"";
        String modulo9 = (request.getParameter("modulo9") != null)?request.getParameter("modulo9").toUpperCase():"";
        String modulo10 = (request.getParameter("modulo10") != null)?request.getParameter("modulo10").toUpperCase():"";
        String cta_dependiente = (request.getParameter("cta_dependiente") != null)?request.getParameter("cta_dependiente").toUpperCase():"";
        String nivel = (request.getParameter("nivel") != null)?request.getParameter("nivel").toUpperCase():"";
        String cta_cierre = (request.getParameter("cta_cierre") != null)?request.getParameter("cta_cierre").toUpperCase():"";
        String subledger = (request.getParameter("subledger") != null)?request.getParameter("subledger").toUpperCase():"";
        String tercero = (request.getParameter("tercero") != null)?request.getParameter("tercero").toUpperCase():"";
        // user_update
        // creation_user

        String detalle = (request.getParameter("detalle") != null)?request.getParameter("detalle").toUpperCase():"";


        int t = 0;
        int w = 0;
        int tam = cuenta.length();
        String caracter_prueba = "";
        String caracter1 = cuenta.substring( 0, 1 );
        int condicion = 0;
        int cond2 = 0;
        int cond3 = 0;
        int cond4 = 0;
        int cond5 = 0;
        String validar = "";

        int cargar = 0;

        try{

            PlanDeCuentas cuentas = new PlanDeCuentas();

            cuentas.setDstrct( distrito );
            cuentas.setCuenta( cuenta );
            cuentas.setNombre_largo( nom_largo );
            cuentas.setNombre_corto( nom_corto );
            cuentas.setNombre_observacion( observacion );
            cuentas.setAuxiliar( auxiliar );
            cuentas.setBase( base );
            cuentas.setModulo1( modulo1 );
            cuentas.setModulo2( modulo2 );
            cuentas.setModulo3( modulo3 );
            cuentas.setModulo4( modulo4 );
            cuentas.setModulo5( modulo5 );
            cuentas.setModulo6( modulo6 );
            cuentas.setModulo7( modulo7 );
            cuentas.setModulo8( modulo8 );
            cuentas.setModulo9( modulo9 );
            cuentas.setModulo10( modulo10 );
            cuentas.setCta_dependiente( cta_dependiente );
            cuentas.setNivel( nivel );
            cuentas.setCta_cierre( cta_cierre );
            cuentas.setSubledger( subledger );
            cuentas.setTercero( tercero );
            cuentas.setUser_update( creation_user );
            cuentas.setCreation_user( creation_user );

            cuentas.setDetalle( detalle );

            /**
             * valida: la estructura de una cuenta que empieza con 'I', 'C' � 'G'.
             * debe tener 13 caracteres
             * 1� = I, C � G
             * 2� y 3� = Codigo de AGENCIA ( tablagen - tagecont )
             * 4�, 5� y 6� = Unidad de Negocio ( tablagen - tunidades )
             * 7�, 8� y 9� = Codigo del cliente � Seccion ( tablagen - tseccion )
             * 10�, 11�, 12� y 13� = Elemento del Gasto ( tablagen - eee )
             */
            if( caracter1.equals( "I" ) || caracter1.equals( "C" ) || caracter1.equals( "G" ) ){

                if ( tam == 13 ){

                    String agencia = cuenta.substring( 1, 3 );
                    String unidad = cuenta.substring( 3, 6 );
                    String cliente = cuenta.substring( 6, 9 );
                    String elemento = cuenta.substring( 9, 13 );

                    int a = ( model.planDeCuentasService.existAgencia( agencia ) == true )?0:1;
                    int u = ( model.planDeCuentasService.existUnidadDeNegocio( unidad ) == true )?0:1;
                    int s = ( model.planDeCuentasService.existSeccion( cliente ) == true )?0:1;
                    int c = ( model.planDeCuentasService.existCliente( cliente ) == true )?0:1;
                    int e = ( model.planDeCuentasService.existElementoDelGasto( elemento ) == true )?0:1;

                    if ( a == 1 ){
                        cond5 = 2;//Error..Numero de Cuenta INVALIDO! La Agencia No Existe!!
                    } else if ( u == 1 ){
                        cond5 = 3;//Error..Numero de Cuenta INVALIDO! La Unidad De Negocio No Existe!!
                    } else if ( ( s == 1 ) && ( c == 1 ) ){
                        cond5 = 4;//Error..Numero de Cuenta INVALIDO! La Seccion o Cliente No Existe!!
                    } else if ( e == 1 ){
                        cond5 = 5;//Error..Numero de Cuenta INVALIDO! El Elemento Del Gasto No Existe!!
                    } else {
                        cond5 = 0;//Todo bien
                    }

                } else{

                    cond5 = 1;//Error..Numero de Cuenta INVALIDO! El Tama�o de Caracteres es Erroneo!!

                }

            }

            // valida: si la cuenta comienza con un numero, toda la cuenta tiene que ser numerica.
            // y si la cuenta comienza con las letras 'I', 'C', � 'G', puede ser alfanumerico el numero de la cuenta.
            if ( tam > 0 ){

                if( caracter1.equals( "I" ) || caracter1.equals( "C" ) || caracter1.equals( "G" ) ){

                    if ( cta_dependiente.equals("") ){
                        validar = "bien1";
                    } else {
                        validar = "error1";
                    }

                    if ( auxiliar.equals("S") || subledger.equals("S") || tercero.equals("N") || tercero.equals("O") ){
                        cond4 = 1;
                    }

                } else if( caracter1.equals( "1" ) || caracter1.equals( "2" ) || caracter1.equals( "3" ) || caracter1.equals( "4" ) || caracter1.equals( "5" ) || caracter1.equals( "6" ) || caracter1.equals( "7" ) || caracter1.equals( "8" ) || caracter1.equals( "9" ) ){

                    if ( tam == 1 ){

                        if ( !cta_dependiente.equals("") ){

                            validar = "error1";

                        }

                    } else{

                        while( w < tam ){

                            caracter_prueba = cuenta.substring( w, w+1 );

                            if( caracter_prueba.equals( "0" ) || caracter_prueba.equals( "1" ) || caracter_prueba.equals( "2" ) || caracter_prueba.equals( "3" ) || caracter_prueba.equals( "4" ) || caracter_prueba.equals( "5" ) || caracter_prueba.equals( "6" ) || caracter_prueba.equals( "7" ) || caracter_prueba.equals( "8" ) || caracter_prueba.equals( "9" ) ){
                                validar = "bien2";
                            } else{
                                condicion = 1;
                            }
                            w++;
                        }

                    }

                } else{

                    condicion = 2;

                }

            }

            // valida: la cuenta dependiente puede ser vacia si la cuenta tiene un solo digito numerico.
            // o si la cuenta empieza por I, C, � G.
            if ( cta_dependiente.equals("") ){
                if (  ( ( tam == 1 ) && ( ( caracter1.equals( "1" ) || caracter1.equals( "2" ) || caracter1.equals( "3" ) || caracter1.equals( "4" ) || caracter1.equals( "5" ) || caracter1.equals( "6" ) || caracter1.equals( "7" ) || caracter1.equals( "8" ) || caracter1.equals( "9" ) ) ) )
                || caracter1.equals( "I" ) || caracter1.equals( "C" ) || caracter1.equals( "G" ) ){//Modific� Ing. ANDRES MATURANA 25/07/2006
                    cond2 = 0;
                } else if ( validar.equals( "bien1" ) ){
                    cond2 = 0;
                } else{
                    cond2 = 1;
                }
            }

            // valida: si la cuenta comienza con 'I', 'C', 'G', '4', '5', � '6', tiene que tener cuenta de cierre.
            if ( caracter1.equals( "I" ) || caracter1.equals( "C" ) || caracter1.equals( "G" ) || caracter1.equals( "4" ) || caracter1.equals( "5" ) || caracter1.equals( "6" ) ){
                if ( !cta_cierre.equals("") ){
                    cond3 = 0;
                } else{
                    cond3 = 1;
                }
            }


            boolean sw = false;
            int val_det = 0;
            if( !caracter1.equals( "I" ) && !caracter1.equals( "C" ) && !caracter1.equals( "G" ) ){// MODIFICO ANDRES MATURANA
                /*
                // valida: que la cuenta dependiente ingresada no tenga detalle
                  model.planDeCuentasService.searchCuenta( distrito, cta_dependiente );
                  String de = model.planDeCuentasService.getCuentas().getDetalle();
                  if( de.equals("S") )
                      val_det = 1;
                */

                // valida: que la cuenta dependiente ingresada no tenga detalle
                //if ( detalle.equals("S") ){
                model.planDeCuentasService.searchCuenta( distrito, cta_dependiente );

                PlanDeCuentas vec_cue = model.planDeCuentasService.getCuentas();
                if ( vec_cue != null ){
                    String de = vec_cue.getDetalle();
                    if(( de.equals("S") ) && (!(caracter1.equals( "C" ))) && (!(caracter1.equals( "G" ))) && (!(caracter1.equals( "I" )))){
                        val_det = 1;
                    }
                } else {
                    next = next + "?msg=Error..la cuenta dependiente ingresada NO EXISTE o NO HA SIDO INGRESADA!";
                    sw = true;
                }

            }

            //////////////////////////////////
            /// MODIFICADO ANDRES MATURANA //
            ////////////////////////////////
            logger.info("CARACTER: " + caracter1 + " - NIVEL: " + nivel);
            if( caracter1.compareTo( "I" )==0 || caracter1.compareTo( "C" )==0 || caracter1.compareTo( "G" )==0 ){// MODIFICO ANDRES MATURANA
                if ( nivel.compareTo("1")!=0 ){
                    next = next + "?msg=Error..El nivel debe ser 1!";
                    sw = true;
                }
            }


            logger.info("SW: " + sw);

            //}
            if ( sw == false ){
                if ( ( distrito != "" ) && ( cuenta != "" ) && ( nom_largo != "" ) && ( nom_corto != "" ) && ( auxiliar != "" ) &&
                ( modulo1 != "" ) && ( modulo2 != "" ) && ( modulo3 != "" ) && ( modulo4 != "" ) && ( modulo5 != "" ) && ( modulo6 != "" ) &&
                ( modulo7 != "" ) && ( modulo8 != "" ) && ( modulo9 != "" ) && ( modulo10 != "" ) && ( nivel != "" ) &&
                ( subledger != "" ) && ( tercero != "" ) && ( creation_user != "" ) ){

                    if ( condicion == 0 ){

                        if ( cond5 == 0 ){

                            if ( !validar.equals( "error1" ) ){

                                if ( val_det == 0 ){

                                    if ( cond2 == 0 ){

                                        if ( cond3 == 0 ){

                                            if ( cond4 == 0 ){

                                                if(!model.planDeCuentasService.existCuenta( distrito, cuenta ) ){

                                                    if( ( ( cta_dependiente.equals("") ) && ( cta_cierre.equals("") ) ) ||
                                                    ( ( cta_dependiente.equals("") ) && ( !cta_cierre.equals("") ) && ( model.planDeCuentasService.existCuenta( distrito, cta_cierre ) ) ) ||
                                                    ( ( !cta_dependiente.equals("") ) && ( cta_cierre.equals("") ) && ( model.planDeCuentasService.existCuenta( distrito, cta_dependiente ) ) ) ||
                                                    ( ( !cta_dependiente.equals("") ) && ( !cta_cierre.equals("") ) && ( model.planDeCuentasService.existCuenta( distrito, cta_cierre ) ) && ( model.planDeCuentasService.existCuenta( distrito, cta_dependiente ) ) )
                                                    ){

                                                        //model.planDeCuentasService.insertCuenta( cuentas );
                                                        //////////////////
                                                        logger.info("COMANDO PPAL: " + cuenta);
                                                        /////////////////
                                                        next = next + "?msg=Se ha ingresado la cuenta exitosamente!!";
                                                        if ( model.planDeCuentasService.reg_status_Cuenta( distrito, cuenta ).equals("A") ) {
                                                            //actualizar
                                                            model.planDeCuentasService.updateCuenta( cuentas );
                                                            logger.info("MODIFICACION CUENTA PPAL: " + cuentas.getCuenta());

                                                        } else {
                                                            //insertar
                                                            logger.info("INSERCION CUENTA PPAL: " + cuentas.getCuenta());
                                                            model.planDeCuentasService.insertCuenta(cuentas);
                                                            logger.info("INSERTO CUENTA PPAL: " + cuentas.getCuenta());
                                                        }
                                                        cargar = 1;

                                                    } else if( ( !caracter1.equals( "I" ) && !caracter1.equals( "C" ) && !caracter1.equals( "G" ) && //MODIFICO ANDRES MATURANA
                                                    !cta_dependiente.equals("") ) && ( !model.planDeCuentasService.existCuenta( distrito, cta_dependiente ) ) ){

                                                        next = next + "?msg=Error..la cuenta dependiente ingresada NO EXISTE o NO HA SIDO INGRESADA!";

                                                    } else if( ( !cta_cierre.equals("") ) && ( !model.planDeCuentasService.existCuenta( distrito, cta_cierre ) ) ){

                                                        next = next + "?msg=Error..la cuenta de cierre ingresada NO EXISTE!";

                                                    }

                                                } else{

                                                    next = next + "?msg=Error..la cuenta ingresada YA EXISTE!";

                                                    if ( vec.size() > 0 ) {//AMATURANA 05.10.2006
                                                        if( ( ( cta_dependiente.equals("") ) && ( cta_cierre.equals("") ) ) ||
                                                        ( ( cta_dependiente.equals("") ) && ( !cta_cierre.equals("") ) && ( model.planDeCuentasService.existCuenta( distrito, cta_cierre ) ) ) ||
                                                        ( ( !cta_dependiente.equals("") ) && ( cta_cierre.equals("") ) && ( model.planDeCuentasService.existCuenta( distrito, cta_dependiente ) ) ) ||
                                                        ( ( !cta_dependiente.equals("") ) && ( !cta_cierre.equals("") ) && ( model.planDeCuentasService.existCuenta( distrito, cta_cierre ) ) && ( model.planDeCuentasService.existCuenta( distrito, cta_dependiente ) ) )
                                                        ){

                                                            cargar = 1;

                                                        } else if( ( !caracter1.equals( "I" ) && !caracter1.equals( "C" ) && !caracter1.equals( "G" ) && //MODIFICO ANDRES MATURANA
                                                        !cta_dependiente.equals("") ) && ( !model.planDeCuentasService.existCuenta( distrito, cta_dependiente ) ) ){

                                                            next = next + "?msg=Error..la cuenta dependiente ingresada NO EXISTE!";

                                                        } else if( ( !cta_cierre.equals("") ) && ( !model.planDeCuentasService.existCuenta( distrito, cta_cierre ) ) ){

                                                            next = next + "?msg=Error..la cuenta de cierre ingresada NO EXISTE!";

                                                        }
                                                    }
                                                }

                                            } else if ( cond4 == 1 ){

                                                next = next + "?msg=Error..esta cuenta no debe tener ni auxiliar, ni subledger y el tercero debe ser igual a mandatorio!";

                                            }

                                        } else if ( cond3 == 1 ){

                                            next = next + "?msg=Error..ingrese la cuenta de cierre!";

                                        }

                                    } else if ( cond2 == 1 ){

                                        next = next + "?msg=Error..la cuenta dependiente ingresada NO EXISTE o NO HA SIDO INGRESADA!";

                                    }

                                } else {

                                    next = next + "?msg=Error..la cuenta dependiente ingresada no puede ser de detalle!";

                                }

                            } else {

                                next = next + "?msg=Error..esta cuenta no debe tener cuenta dependiente!";

                            }

                        } else if ( cond5 == 1 ){

                            next = next + "?msg=Error..Numero de Cuenta INVALIDO! El Tama�o de Caracteres es Erroneo!!";

                        } else if ( cond5 == 2 ){

                            next = next + "?msg=Error..Numero de Cuenta INVALIDO! La Agencia No Existe!!";

                        } else if ( cond5 == 3 ){

                            next = next + "?msg=Error..Numero de Cuenta INVALIDO! La Unidad De Negocio No Existe!!";

                        } else if ( cond5 == 4 ){

                            next = next + "?msg=Error..Numero de Cuenta INVALIDO! La Seccion o Cliente No Existe!!";

                        } else if ( cond5 == 5 ){

                            next = next + "?msg=Error..Numero de Cuenta INVALIDO! El Elemento Del Gasto No Existe!!";

                        }

                    } else if ( ( condicion == 1 ) || ( condicion == 2 ) ){

                        next = next + "?msg=Error..Numero de Cuenta INVALIDO!";

                    }

                } else {

                    next = next + "?msg=Por favor ingrese todos los datos obligatorios!!";

                }

            }
            if ( cargar == 1 ) {

                session.setAttribute( "cuenta", "" );
                session.setAttribute( "nom_largo", "" );
                session.setAttribute( "nom_corto", "" );
                session.setAttribute( "observacion", "" );
                session.setAttribute( "auxiliar", "N" );
                session.setAttribute( "nivel", "1" );
                session.setAttribute( "subledger", "N" );
                session.setAttribute( "tercero", "N" );
                session.setAttribute( "cta_dependiente", "" );
                session.setAttribute( "cta_cierre", "" );
                session.setAttribute( "modulo1", "N" );
                session.setAttribute( "modulo2", "N" );
                session.setAttribute( "modulo3", "N" );
                session.setAttribute( "modulo4", "N" );
                session.setAttribute( "modulo5", "N" );
                session.setAttribute( "modulo6", "N" );
                session.setAttribute( "modulo7", "N" );
                session.setAttribute( "modulo8", "N" );
                session.setAttribute( "modulo9", "N" );
                session.setAttribute( "modulo10", "N" );

                session.setAttribute( "detalle", "N" );

                ////////////////////////////////////////////////////////////////////////// Andr�s Maturana De La Cruz

                if( vec.size() > 0 ){

                    if( cuenta.length()==13 && (cuenta.toUpperCase().startsWith("I")
                    || cuenta.toUpperCase().startsWith("C") || cuenta.toUpperCase().startsWith("G")) ){
                        Vector adic = new Vector();

                        for( int i=0; i<vec.size(); i++){
                            String elem = (String) vec.elementAt(i);
                            logger.info("...ELEMENTO: " + elem);

                            if( cuenta.length()==13 ){
                                String cuenta_adic = cuenta.substring(0,9) + elem;
                                cuentas.setCuenta( cuenta_adic );
                                boolean existe = model.planDeCuentasService.existCuenta( distrito, cuenta_adic);
                                logger.info("...CUENTA ADICIONAL:" + cuenta_adic + " - EXISTE: " + existe);

                                if ( cuenta_adic.compareTo(cuenta)==0 ){
                                    logger.info("......CUENTA ADICIONAL IGUAL A CUENTA PPAL");
                                    next += "<br>No se ingreso la cuenta: " + cuenta_adic + ". La cuenta est� repetida en las cuentas adicionales.";
                                } else if ( existe ){
                                    logger.info("......EXISTE CUENTA ADICIONAL: " + cuenta_adic);
                                    next += "<br>No se ingreso la cuenta: " + cuenta_adic + ". La cuenta ya existe.";
                                } else if( !existe ){
                                    if ( model.planDeCuentasService.reg_status_Cuenta( distrito, cuenta_adic ).equals("A") ) {
                                        logger.info("......MODIFICO CUENTA ADICIONAL: " + cuenta_adic);
                                        model.planDeCuentasService.updateCuenta( cuentas );
                                        logger.info(".........MODIFICO CUENTA ADICIONAL: " + cuenta_adic);
                                    } else {
                                        logger.info("......INSERTO CUENTA ADICIONAL: " + cuenta_adic);
                                        model.planDeCuentasService.insertCuenta(cuentas);
                                        logger.info(".........INSERTO CUENTA ADICIONAL: " + cuenta_adic);
                                    }
                                    next += "<br>Se ingreso exitosamente la cuenta adicional: " + cuenta_adic + ".";
                                }
                            }
                        }
                    } else {
                        next += "<br>No se ingresaron cuentas con elementos adicionales. <br>La cuenta no tiene la longitud correcto o no empieza por I, C o G";
                    }
                }

                model.planDeCuentasService.setHashtable(new Hashtable());
                model.planDeCuentasService.setElementos(new Vector());
                ///////////////////////////////////////////////////////////////////////////////////////////////////////


            } else {

                //model.planDeCuentasService.setHashtable(new Hashtable());
                //model.planDeCuentasService.setElementos(new Vector());

                session.setAttribute( "cuenta", cuenta );
                session.setAttribute( "nom_largo", nom_largo );
                session.setAttribute( "nom_corto", nom_corto );
                session.setAttribute( "observacion", observacion );
                session.setAttribute( "auxiliar", auxiliar );
                session.setAttribute( "nivel", nivel );
                session.setAttribute( "subledger", subledger );
                session.setAttribute( "tercero", tercero );
                session.setAttribute( "cta_dependiente", cta_dependiente );
                session.setAttribute( "cta_cierre", cta_cierre );
                session.setAttribute( "modulo1", modulo1 );
                session.setAttribute( "modulo2", modulo2 );
                session.setAttribute( "modulo3", modulo3 );
                session.setAttribute( "modulo4", modulo4 );
                session.setAttribute( "modulo5", modulo5 );
                session.setAttribute( "modulo6", modulo6 );
                session.setAttribute( "modulo7", modulo7 );
                session.setAttribute( "modulo8", modulo8 );
                session.setAttribute( "modulo9", modulo9 );
                session.setAttribute( "modulo10", modulo10 );

                session.setAttribute( "detalle", detalle );

            }

            model.planDeCuentasService.setHashtable(new Hashtable());
            model.planDeCuentasService.setElementos(new Vector());

        } catch ( Exception e ){

            e.printStackTrace();
            throw new ServletException( e.getMessage() );

        }

    }
        this.dispatchRequest( next );
 }
}
