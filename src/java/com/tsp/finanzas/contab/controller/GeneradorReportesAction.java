/*
 * LoadExcelGeneradoresAction.java
 *
 * Created on 17 de enero de 2007, 12:09 AM
 */

package com.tsp.finanzas.contab.controller;

import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.exceptions.*;
import com.tsp.operation.model.beans.*;
import com.tsp.finanzas.contab.model.threads.HGeneradorReportesMayor;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.finanzas.presupuesto.model.beans.Upload;
import com.tsp.util.UtilFinanzas;
import com.tsp.finanzas.contab.model.*;
import java.util.*;
import java.io.*;

/**
 *
 * @author  Osvaldo P�rez Ferrer
 */
public class GeneradorReportesAction extends Action{
    
    /** Creates a new instance of LoadExcelGeneradoresAction */
    public GeneradorReportesAction() {
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        
        String next    = "/jsp/finanzas/contab/reportes/load_generadores_reporte.jsp";
        try{
            HttpSession session = request.getSession();
            Usuario usuario = (Usuario) session.getAttribute("Usuario");
            String distrito = (String)(session.getAttribute("Distrito"));
            String opcion   = request.getParameter("opcion");
            String Mensaje  = "";
            String ruta = UtilFinanzas.obtenerRuta("ruta", "/exportar/migracion/" + usuario.getLogin());
            
            if( opcion == null ){
                
                if (usuario==null) throw new Exception("Usuario no encontrado en session");
                
                Upload  upload = new Upload( ruta + "/" , request );
                
                upload.load();
                
                List lista = upload.getFilesName();
                List archivo = upload.getFiles();
                
                String filename = lista.get( 0 ).toString();
                File file = (File)archivo.get( 0 );
                
                renombrar( file, ruta );
                
                int pos = file.getName().lastIndexOf( "." );
                String ext = "";
                if ( pos != -1 ) {
                    ext = file.getName().substring( pos + 1, file.getName().length() );
                }
                if( !ext.equalsIgnoreCase( "xls" ) ) {
                    file.delete();
                    request.setAttribute( "mensaje", "No se puede procesar el archivo debido a que no es un archivo en excel." );
                }else{
                                                            
                }
            }else{
                if( opcion.equals("delete") ){
                    int index = Integer.parseInt( request.getParameter("index") );
                    
                    String nombre = (String)model.mayorizacionService.getVector().get(index);
                    
                    File archivo = new File( ruta+"/"+nombre );
                    archivo.delete();                    
                    model.mayorizacionService.getVector().removeElementAt( index );
                }else if( opcion.equals("process") ){   
                    
                    String anio = request.getParameter("anio");
                    String mes  = request.getParameter("mes");
                    
                    com.tsp.operation.model.Model m = new com.tsp.operation.model.Model(usuario.getBd());
                    HGeneradorReportesMayor h = new HGeneradorReportesMayor( anio, mes, m , usuario, model.mayorizacionService.getVector(), ruta);
                    h.start();
                    
                    request.setAttribute("iniciado", "OK");
                    model.mayorizacionService.setVector( new Vector() );
                    
                }else if( opcion.equals("reset") ){
                    
                    Vector v = model.mayorizacionService.getVector();
                    if( v != null ){
                        for( int i=0; i<v.size(); i++ ){
                            
                            File del = new  File( ruta+"/"+v.get(i));
                            del.delete();
                            model.mayorizacionService.setVector(null);
                        }
                    }
                    
                }
            }
            request.setAttribute( "cargados", model.mayorizacionService.getVector() );
            
        }catch( Exception ex ){
            throw new ServletException( "Error en GeneradorReportesAction ...\n" + ex.getMessage() );
        }
        this.dispatchRequest( next );
        
    }
    
    public void renombrar( File f, String ruta ){
        
        File ren = new  File( ruta+"/"+"SIN_PROCESAR-"+f.getName() );
        
        if( ren.exists() ){
            ren.delete();
            
            ren = new  File( ruta+"/"+"SIN_PROCESAR-"+f.getName() );
            f.renameTo(ren);
            
            if( model.mayorizacionService.getVector() == null ){
                model.mayorizacionService.setVector( new Vector() );
                model.mayorizacionService.getVector().add( f.getName() );
            }
                        
        }else{
            f.renameTo(ren);
            if( model.mayorizacionService.getVector() == null ){
                model.mayorizacionService.setVector( new Vector() );
            }
            model.mayorizacionService.getVector().add( "SIN_PROCESAR-"+f.getName() );
        }
        
    }
    
}