/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.finanzas.contab.controller;

import com.tsp.exceptions.InformationException;

import com.tsp.operation.model.beans.Usuario;
import com.tsp.finanzas.contab.model.beans.Cmc;

import com.tsp.finanzas.contab.model.beans.Hc;
import com.tsp.finanzas.contab.model.beans.Tipo_Docto;

import com.tsp.util.ExcelApplication;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;
import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;
import java.util.Vector;
import java.util.*;

/**
 *
 * @author imorales
 */
public class GestionHcAction extends Action {

    private Cmc cmc;
    private Hc hc;
    private Tipo_Docto td;
    List<Cmc> lista_cmc = new LinkedList<Cmc>();
    List<Hc> lista_hc = new LinkedList<Hc>();

    @Override
    public void run() throws ServletException, InformationException {
        boolean redirect = false;
        try {

            String msj = "";
            HttpSession session = request.getSession();
            Usuario usuario = (Usuario) session.getAttribute("Usuario");
            usuario.setBd("fintra");//20100901
            String codigo = request.getParameter("codigo");
            String id = request.getParameter("id");
            String descripcion = request.getParameter("descripcion");
            int opcion = Integer.parseInt(request.getParameter("tipo") != null ? request.getParameter("tipo") : "");//20100901
            String next = "/jsp/finanzas/contab/gestion_de_hc/GestionHc.jsp";

            switch (opcion) {
                case 1:/*Redireccionar a pagina nuevo */

                    session.removeAttribute("lista_cmc");
                    redirect = true;
                    break;

                case 2:/*nuevo Hc */

                    hc = new Hc(codigo, descripcion, usuario.getLogin());
                    lista_cmc = (List<Cmc>) session.getAttribute("lista_cmc");

                    Vector comandos = model.Hcsevice.InsertarHc(lista_cmc, hc);
                    model.applusService.ejecutarSQL(comandos);
                    msj = "El Codigo Contable " + hc.getTableCode() + " - " + hc.getDescripcion() + " Se Guardo Correctamente";
                    next = "/jsp/finanzas/contab/gestion_de_hc/GestionHc.jsp?msj=" + msj;
                    session.removeAttribute("lista_cmc");
                    redirect = true;

                    break;

                case 3:/*Mostrar un  Hc para actualizarlo */

                    hc = model.Hcsevice.Buscar(id);
                    session.setAttribute("hc", hc);
                    lista_cmc = model.cmcService.ListarCmc(id);
                    session.setAttribute("lista_cmc", lista_cmc);

                    next = "/jsp/finanzas/contab/gestion_de_hc/GestionHc.jsp";
                    redirect = true;
                    break;

                case 4:/*Modificar  Hc */

                    int lz = 0;
                    lz = (Integer) session.getAttribute("lz");
                    hc = new Hc(id, descripcion, usuario.getLogin());
                    lista_cmc = (List<Cmc>) session.getAttribute("lista_cmc");

                    comandos = model.Hcsevice.ActualizarHc(lista_cmc, hc);
                    model.applusService.ejecutarSQL(comandos);
                    lista_cmc = model.cmcService.ListarCmc(id);
                    hc = model.Hcsevice.Buscar(id);
                    session.setAttribute("lista_cmc", lista_cmc);
                    session.setAttribute("hc", hc);
                    session.setAttribute("lz", lista_cmc.size());
                    msj = "El Codigo Contable " + hc.getTableCode() + " - " + hc.getDescripcion() + " Se Modific? Correctamente";
                    next = "/jsp/finanzas/contab/gestion_de_hc/GestionHc.jsp?msj=" + msj;
                    redirect = true;

                    break;

                case 5:/*Exportar */

                    String respuesta = "<span class='letra'>Archivo generado exitosamente.<br>Revise su lista de archivos.</span>";
                    try {
                        lista_hc = model.Hcsevice.ListarHc();
                        this.exportarExcel(lista_hc, usuario.getLogin());
                    } catch (Exception e) {
                        respuesta = "<span class='letra'>Ocurrio un error al procesar la peticion.</span>";
                        System.out.println("error al buscar la lista de HC: " + e.toString());
                        e.printStackTrace();
                    }
                    response.setContentType("text/plain; charset=utf-8");
                    response.setHeader("Cache-Control", "no-cache");
                    response.getWriter().println(respuesta);
                    redirect = false;
                    break;
            }

            if (redirect == true) {
                this.dispatchRequest(next);
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new ServletException(e.getMessage(), e);
        }
    }

    private void exportarExcel(List<Hc> listadoHc, String userlogin) throws Exception {
        Hc hc = null;
        Cmc cmc = null;

        List<Cmc> listadoCmc = null;

        try {
            if (listadoHc != null && listadoHc.size() > 0) {
                String ruta = this.directorioArchivo(userlogin, "listado_Hc", "xls");
                ExcelApplication excel = this.instanciar("Codigo Contable");
                this.encabezadoExcel(excel, 1);
                int filaact = 2;
                for (int i = 0; i < listadoHc.size(); i++) {
                    hc = listadoHc.get(i);
                    //escribir ...
                    if (i > 0) {
                        this.encabezadoExcel(excel, filaact);
                        filaact++;
                    }
                    //...
                    int col = 0;
                    excel.setDataCell(filaact, col, hc.getTableCode());
                    excel.setCellStyle(filaact, col++, excel.getStyle("estilo3"));
                    excel.setDataCell(filaact, col, hc.getDescripcion());
                    excel.setCellStyle(filaact, col++, excel.getStyle("estilo3"));
                    filaact++;

                    //recorrer ...
                    filaact++;
                    listadoCmc = model.cmcService.ListarCmc(hc.getTableCode());
                    if ((listadoCmc != null) && (listadoCmc.size() > 0)) {
                        excel.setDataCell(filaact, 1, "Documentos");
                        excel.setCellStyle(filaact, 1, excel.getStyle("estilo2"));
                        filaact++;
                        excel.setDataCell(filaact, 1, "Tipo Documento");
                        excel.setCellStyle(filaact, 1, excel.getStyle("estilo2"));
                        excel.setDataCell(filaact, 2, "Descripcion");
                        excel.setCellStyle(filaact, 2, excel.getStyle("estilo2"));
                        excel.setDataCell(filaact, 3, "Cuenta");
                        excel.setCellStyle(filaact, 3, excel.getStyle("estilo2"));
                        excel.setDataCell(filaact, 4, "Db/Cr");
                        excel.setCellStyle(filaact, 4, excel.getStyle("estilo2"));
                        excel.setDataCell(filaact, 5, "Tipo Cuenta");
                        excel.setCellStyle(filaact, 5, excel.getStyle("estilo2"));
                        filaact++;
                        for (int j = 0; j < listadoCmc.size(); j++) {

                            cmc = listadoCmc.get(j);
                            // String a =  cmc.getTipodoc();
                            td = model.tipo_doctoSvc.VerTipo_Docto("FINV", cmc.getTipodoc());
                            if (td == null) {
                                td = new Tipo_Docto();
                                td.setDescripcion("");
                            }

                            cmc.setDescripcionTipodoc(td.getDescripcion());
                            //escr

                            excel.setDataCell(filaact, 1, cmc.getTipodoc());
                            excel.setCellStyle(filaact, 1, excel.getStyle("estilo3"));
                            excel.setDataCell(filaact, 2, cmc.getDescripcionTipodoc());
                            excel.setCellStyle(filaact, 2, excel.getStyle("estilo3"));
                            excel.setDataCell(filaact, 3, cmc.getCuenta());
                            excel.setCellStyle(filaact, 3, excel.getStyle("estilo3"));
                            excel.setDataCell(filaact, 4, cmc.getDbcr());
                            excel.setCellStyle(filaact, 4, excel.getStyle("estilo3"));
                            excel.setDataCell(filaact, 5, cmc.getTipo_cuenta());
                            excel.setCellStyle(filaact, 5, excel.getStyle("estilo3"));
                            cmc = null;
                            td = null;
                            filaact++;
                        }
                        filaact++;
                    }
                    //hacer espacio para el siguiente ...
                    //filaact++;
                    hc = null;
                    filaact++;
                }
                excel.saveToFile(ruta);
            }
        } catch (Exception e) {
            throw new Exception("Error al general el excel de codigos contables: " + e.toString());
        }
    }

    private String directorioArchivo(String user, String cons, String extension) throws Exception {
        String ruta = "";
        try {
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            ruta = rb.getString("ruta") + "/exportar/migracion/" + user;
            SimpleDateFormat fmt = new SimpleDateFormat("yyyMMdd_hhmmss");
            File archivo = new File(ruta);
            if (!archivo.exists()) {
                archivo.mkdirs();
            }
            ruta = ruta + "/" + cons + "_" + fmt.format(new Date()) + "." + extension;
        } catch (Exception e) {
            throw new Exception("Error al generar el directorio: " + e.toString());
        }
        return ruta;
    }

    /**
     * Crea un objeto tipo ExcelApplication
     *
     * @param descripcion Nombre de la hoja principal del libro
     * @return Objeto ExcelApplication creado
     * @throws Exception Cuando hay error
     */
    private ExcelApplication instanciar(String descripcion) throws Exception {
        ExcelApplication excel = new ExcelApplication();
        try {
            excel.createSheet(descripcion);
            excel.createFont("Titulo", "Arial", (short) 1, true, (short) 12);
            excel.createFont("Subtitulo", "Verdana", (short) 1, true, (short) 10);
            excel.createFont("Contenido", "Verdana", (short) 0, false, (short) 10);

            excel.createColor((short) 11, (byte) 255, (byte) 255, (byte) 255);//Blanco
            excel.createColor((short) 9, (byte) 26, (byte) 126, (byte) 0);//Verde dark
            excel.createColor((short) 10, (byte) 171, (byte) 243, (byte) 169);//Verde light

            excel.createStyle("estilo1", excel.getFont("Titulo"), (short) 10, true, "@");
            excel.createStyle("estilo2", excel.getFont("Subtitulo"), (short) 9, true, "@");
            excel.createStyle("estilo3", excel.getFont("Contenido"), (short) 11, true, "@");

        } catch (Exception e) {
            throw new Exception("Error al instanciar el objeto: " + e.toString());
        }
        return excel;
    }

    public void encabezadoExcel(ExcelApplication excel, int filaact) {
        try {
            if (filaact == 1) {
                excel.setDataCell(0, 0, "HC");
                excel.setCellStyle(0, 0, excel.getStyle("estilo2"));
            }
            int col = 0;
            excel.setDataCell(filaact, col, "Nombre");
            excel.setCellStyle(filaact, col++, excel.getStyle("estilo2"));
            excel.setDataCell(filaact, col, "Descripcion");
            excel.setCellStyle(filaact, col++, excel.getStyle("estilo2"));
        } catch (Exception e) {
            System.out.println("error: " + e.toString());
        }
    }

}
