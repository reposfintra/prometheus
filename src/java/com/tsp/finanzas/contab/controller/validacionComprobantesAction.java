/*
 * validarComprobantesAction.java
 *
 * Created on 17 de junio de 2006, 07:52 PM
 */
/********************************************************************
 *  Nombre Clase.................   validarComprobantesAction.java
 *  Descripci�n..................   Accion que permite genera la validacion de comprobantes
 *  Autor........................   David Pi�a L�pez
 *  Fecha........................   17 de junio de 2006, 07:52 PM
 *  Versi�n......................   1.0
 *  Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.finanzas.contab.controller;

import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.finanzas.contab.model.beans.*;
import com.tsp.finanzas.contab.model.*;
import com.tsp.operation.model.beans.*;
import com.tsp.finanzas.contab.model.threads.*;
/**
 *
 * @author  David
 */
public class validacionComprobantesAction extends Action {
    
    
    /** Creates a new instance of validarComprobantesAction */
    public validacionComprobantesAction() {
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        String next = "";
        String fechaInicio = "";
        String fechaFin = "";
        String user="";
        boolean todas;
        try{
            String mensaje="Proceso de Validaci�n de movimiento contable iniciado";
            
            String tipoQuery = request.getParameter("tipoQuery");
            HttpSession session = request.getSession ();
            Usuario usuario = (Usuario)session.getAttribute ("Usuario");
            user = usuario.getLogin();
            if(!tipoQuery.equals("2")){
                String anioini = request.getParameter("anioini");
                String mesini  = request.getParameter("mesini");
                fechaInicio    = anioini+mesini;
                
                String aniofin = request.getParameter("aniofin");
                String mesfin  = request.getParameter("mesfin");
                fechaFin       = aniofin+mesfin;
            }
            request.setAttribute("mensaje", mensaje);
            
            ValidacionMovimiento hilo = new ValidacionMovimiento();
            hilo.start(usuario, fechaInicio, fechaFin );
            next = "/jsp/finanzas/contab/validacionComprobantes/validacionComprobantes.jsp";
        }catch (Exception e){
            e.printStackTrace();
            throw new ServletException(e.getMessage());
        }
        // Redireccionar a la p�gina indicada.
        this.dispatchRequest(next);
    }
    
}
