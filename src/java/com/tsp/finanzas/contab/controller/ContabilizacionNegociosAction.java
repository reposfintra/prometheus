/********************************************************************
 *  Nombre Clase.................   ContabilizacionFacturasAction.java
 *  Descripci�n..................   Maneja eventos para la contabilizacion de negocios
 *  Autor........................   Ing. ROBERTO ROCHA P.
 *  Fecha........................   06/09/2007
 *  Versi�n......................   1.0
 *  Copyright....................   FINTRA VALORES SA.
 *******************************************************************/
package com.tsp.finanzas.contab.controller;

import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.finanzas.contab.model.threads.HContabilizacionNegocio;

public class ContabilizacionNegociosAction extends Action {

    public void run() throws ServletException, com.tsp.exceptions.InformationException {

        try {
            HttpSession session = request.getSession();
            com.tsp.util.Util.logController(((com.tsp.operation.model.beans.Usuario) session.getAttribute("Usuario")).getLogin(), this.getClass().getName());
            Usuario usuario = (Usuario) session.getAttribute("Usuario");
            String base = "/jsp/fenalco/contabilizacion/";
            String next = "inicio_contab.jsp?msg=";
            String msj = "";
            String fechaCorte = request.getParameter("fechaCorte");
            if (!model.ContabilizacionNegSvc.isProcess()) {
                model.ContabilizacionNegSvc.setProcess(true);
                HContabilizacionNegocio hilo = new HContabilizacionNegocio();
                hilo.start(model, usuario, fechaCorte);
                msj = "Proceso de contabilizaci�n de negocios ha iniciado. Fecha de corte " + fechaCorte;
            } else {
                msj = "Actualmente el proceso se est� realizando, por favor intente mas tarde....";
            }
            dispatchRequest(base + next + msj);

        } catch (Exception e) {
            throw new ServletException(e.getMessage());
        }
    }
}
