/*
 * InterfaceContableManagerAction.java
 *
 * Created on 14 de junio de 2006, 07:34 AM
 */

package com.tsp.finanzas.contab.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.finanzas.contab.model.beans.*;
import com.tsp.finanzas.contab.model.*;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;
import com.tsp.util.Util;

/**
 *
 * @author  David
 */
public class InterfaceContableManagerAction extends Action{    
    
    /** Creates a new instance of InterfaceContableManagerAction */
    public InterfaceContableManagerAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next = "/jsp/finanzas/contab/interfaceContable/ingresarInterfaceContable.jsp";
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String resetear = "";
        try{
            String opc = request.getParameter( "opc" );
            if(opc.equals( "1" )){
                String cuenta = request.getParameter( "cuenta" );
                String tipo_cuenta = request.getParameter( "tipo_cuenta" );
                String cmc = request.getParameter( "cmc" );
                String dbcr = request.getParameter( "dbcr" );
                String tipodoc = request.getParameter( "tipodoc" );
                Cmc c = new Cmc();                
                c.setDstrct( usuario.getCia() );
                c.setTipodoc( tipodoc );
                c.setCmc( cmc );
                c.setCuenta( cuenta );
                c.setDbcr( dbcr );
                c.setTipo_cuenta( tipo_cuenta );
                c.setUsuario( usuario.getLogin() );
                c.setBase( usuario.getBase() );                
                boolean existeCuenta = false;                
                
                if( tipo_cuenta.equalsIgnoreCase("C") ){
                    existeCuenta = model.planDeCuentasService.existCuenta( usuario.getCia(), cuenta );
                }else{                    
                    existeCuenta = model.planDeCuentasService.existElementoDelGasto( cuenta );
                }
                
                if( existeCuenta ){
                    PlanDeCuentas pc = null;
                    model.planDeCuentasService.setCuentas( pc );
                    model.planDeCuentasService.searchCuenta( usuario.getCia(), cuenta );
                    pc = model.planDeCuentasService.getCuentas();
                                        
                    if( (pc != null && !pc.getDetalle().equalsIgnoreCase("N")) || tipo_cuenta.equalsIgnoreCase("E") ){
                        model.cmcService.setCmc( c );
                        model.cmcService.obtenerCmcDoc();
                        Cmc objexiste = model.cmcService.getCmc();
                        if( objexiste == null ){
                            model.cmcService.setCmc( c );
                            model.cmcService.insertar();
                            request.setAttribute("mensaje","Se ha agregado exitosamente");
                            resetear = "&reset=si";
                        }else{
                            if( objexiste.getReg_status().equalsIgnoreCase("A") ){
                                c.setReg_status("");
                                model.cmcService.setCmc( c );                            
                                model.cmcService.actualizar( c.getTipodoc(), c.getCmc() );
                                request.setAttribute("mensaje","La Interface Contable ya exist�a y fu� activada");
                            }else{
                                request.setAttribute("mensaje","La Interface Contable ya existe");
                            }

                        }
                    }else{
                        request.setAttribute("mensaje","La Cuenta no presenta detalle");
                    }
                }else{
                    if( tipo_cuenta.equalsIgnoreCase("C") ){
                        request.setAttribute("mensaje","La Cuenta ingresada no existe");
                    }else{
                        request.setAttribute("mensaje","El Elemento del gasto no existe");
                    }
                }
                
                next = "/jsp/finanzas/contab/interfaceContable/resultadosInterfaceContable.jsp?insertar=ok"+resetear;
            }else if(opc.equals( "2" )){
                String cuenta = request.getParameter( "cuenta" );
                String tipo_cuenta = request.getParameter( "tipo_cuenta" );
                String cmc = request.getParameter( "cmc" );
                String dbcr = request.getParameter( "dbcr" );
                String tipodoc = request.getParameter( "tipodoc" );
                Cmc c = new Cmc();
                c.setCuenta( cuenta );
                c.setTipo_cuenta( tipo_cuenta );
                c.setCmc( cmc );
                c.setDbcr( dbcr );
                c.setTipodoc( tipodoc );
                model.cmcService.setCmc( c );
                model.cmcService.buscar();
                next = "/jsp/finanzas/contab/interfaceContable/listarInterfaceContable.jsp";
            }else if(opc.equals( "3" )){
                String dstrct = request.getParameter( "dstrct" );
                String cmc = request.getParameter( "cmc" );                
                String tipodoc = request.getParameter( "tipodoc" );
                
                Cmc c = new Cmc();                
                c.setDstrct( dstrct );
                c.setTipodoc( tipodoc );
                c.setCmc( cmc );                
                model.cmcService.setCmc( c );
                model.cmcService.obtenerCmcDoc();
                
                Tipo_Docto tipodocto = new Tipo_Docto();                        
                tipodocto.setDistrito( usuario.getCia() );
                model.tipo_doctoSvc.setDocto( tipodocto );
                
                model.tipo_doctoSvc.ObtenerTipo_docto();
                model.cmcService.obtenerCmc();
                
                next = "/jsp/finanzas/contab/interfaceContable/modificarInterfaceContable.jsp";
            }else if(opc.equals( "4" )){
                String dstrctv = request.getParameter( "dstrctv" );
                String cmcv = request.getParameter( "cmcv" );                
                String tipodocv = request.getParameter( "tipodocv" );
                
                String cuenta = request.getParameter( "cuenta" );
                String tipo_cuenta = request.getParameter( "tipo_cuenta" );
                String cmc = request.getParameter( "cmc" );
                String dbcr = request.getParameter( "dbcr" );
                String tipodoc = request.getParameter( "tipodoc" );
                Cmc c = new Cmc();                
                c.setDstrct( dstrctv );
                c.setTipodoc( tipodoc );
                c.setCmc( cmc );
                c.setCuenta( cuenta );
                c.setDbcr( dbcr );
                c.setTipo_cuenta( tipo_cuenta );
                c.setUsuario( usuario.getLogin() );
                c.setReg_status("");
                
                boolean existeCuenta = false;                
                
                if( tipo_cuenta.equalsIgnoreCase("C") ){
                    existeCuenta = model.planDeCuentasService.existCuenta( usuario.getCia(), cuenta );
                }else{                    
                    existeCuenta = model.planDeCuentasService.existElementoDelGasto( cuenta );
                }
                
                if( existeCuenta ){
                    PlanDeCuentas pc = null;
                    model.planDeCuentasService.searchCuenta( usuario.getCia(), cuenta );
                    pc = model.planDeCuentasService.getCuentas();
                    
                    if( (pc != null && !pc.getDetalle().equalsIgnoreCase("N")) || tipo_cuenta.equalsIgnoreCase("E") ){
                        model.cmcService.setCmc( c );
                        model.cmcService.obtenerCmcDoc();
                        Cmc objexiste = model.cmcService.getCmc();
                        if( objexiste == null || ( tipodoc.equalsIgnoreCase(tipodocv) && cmc.equalsIgnoreCase(cmcv) ) ){
                            model.cmcService.setCmc( c );
                            model.cmcService.actualizar( tipodocv, cmcv );                        
                            request.setAttribute("mensaje","Se ha modificado exitosamente");                        
                            resetear = "&reset=ref";
                        }else{                        
                            request.setAttribute("mensaje","La Interface Contable ya existe");
                        }
                    }else{
                        request.setAttribute("mensaje","La Cuenta no presenta detalle");
                    }
                }else{
                    if( tipo_cuenta.equalsIgnoreCase("C") ){
                        request.setAttribute("mensaje","La Cuenta ingresada no existe");
                    }else{
                        request.setAttribute("mensaje","El Elemento del gasto no existe");
                    }
                }
                
                next = "/jsp/finanzas/contab/interfaceContable/resultadosInterfaceContable.jsp?modificar=ok"+resetear;
            }else if(opc.equals( "5" )){
                String dstrct = request.getParameter( "dstrctv" );
                String cmc = request.getParameter( "cmcv" );                
                String tipodoc = request.getParameter( "tipodocv" );                
                
                Cmc c = new Cmc();                
                c.setDstrct( dstrct );
                c.setTipodoc( tipodoc );
                c.setCmc( cmc );
                model.cmcService.setCmc( c );
                model.cmcService.obtenerCmcDoc();
                c = model.cmcService.getCmc();
                c.setUsuario( usuario.getLogin() );
                c.setReg_status("A");
                model.cmcService.setCmc( c );
                model.cmcService.actualizar( tipodoc, cmc );
                next = "/jsp/trafico/mensaje/MsgAnulado.jsp";
            }
            
        }catch(SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);        
    }
    
}
