/******
 * Nombre:        SubledgerBuscarAction.java
 * Descripci�n:   Clase Action para buscar los subledger
 * Autor:         Ing. Diogenes Antonio Bastidas Morales
 * Fecha:         7 de junio de 2006, 10:36 AM
 * Versi�n        1.0
 * Coyright:      Transportes Sanchez Polo S.A.
 *******/


package com.tsp.finanzas.contab.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.finanzas.contab.model.beans.*;
import com.tsp.operation.model.beans.*;
import com.tsp.finanzas.contab.model.*;
import com.tsp.exceptions.*;


public class SubledgerBuscarAction extends Action {
    
    /** Creates a new instance of SubledgerBuscarAction */
    public SubledgerBuscarAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next = "/" + request.getParameter("carpeta")+ "/" + request.getParameter("pagina");
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        
        String cuenta =  request.getParameter("cuenta").toUpperCase();
        String tipo = request.getParameter("tipo");
        String id = request.getParameter("id");
        String evento = request.getParameter("evento");
        
        try{
            if(evento.equals("Listar")){
                model.subledgerService.busquedaSubledger(usuario.getDstrct(), cuenta, tipo, id);
            }
            else if(evento.equals("Buscar")){
                model.subledgerService.buscarSubledger(usuario.getDstrct(), cuenta, tipo, id);
                next+="?sw=Mod";
            }
            else if(evento.equals("BuscarId")){
                model.subledgerService.buscarIdSubledger(usuario.getDstrct(), cuenta, tipo, request.getParameter("valor"));                
            }
            
        }catch (Exception e){
            throw new ServletException(e.getMessage());
        }
        //////System.out.println(next);
        this.dispatchRequest(next);
    }
    
}
