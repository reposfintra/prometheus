/******************************************************************************
 * Nombre clase :      CierreMensualMOCAction.java                            *
 * Descripcion :       Action del CierreMensualMOCAction.java                 *
 * Autor :             LREALES                                                *
 * Fecha :             28 de junio de 2006, 06:22 PM                          *
 * Version :           1.0                                                    *
 * Copyright :         Fintravalores S.A.                                *
 *****************************************************************************/

package com.tsp.finanzas.contab.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.services.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;
import com.tsp.util.*;
import java.util.Vector;
import java.lang.*;
import java.sql.*;
import javax.servlet.ServletException;
import com.tsp.operation.model.threads.*;
import com.tsp.finanzas.contab.model.*;
import com.tsp.finanzas.contab.model.beans.*;
import com.tsp.finanzas.contab.model.threads.*;
import com.tsp.finanzas.contab.model.services.*;
import javax.servlet.http.*;
import com.tsp.util.Contabilidad.*;

public class CierreMensualMOCAction  extends Action {
    
    /** Creates a new instance of CierreMensualMOCAction */
    public CierreMensualMOCAction() { }    
    
    public void run() throws ServletException {
        
        String next = "/jsp/finanzas/contab/cierre_mensual_moc/CierreMensualMOC.jsp";
        
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        
        String distrito = (String) session.getAttribute("Distrito");
        String anio = (request.getParameter("anio") != null)?request.getParameter("anio").toUpperCase():"";
        String mes = (request.getParameter("mes") != null)?request.getParameter("mes").toUpperCase():"";
        String user_update = usuario.getLogin().toUpperCase();
        
        try{
            
            boolean existe = false;
            existe = model.cierreMensualMOCService.existePeriodo( distrito, anio, mes );
            
            if ( existe == true ){
                
                model.cierreMensualMOCService.buscarPeriodo( distrito, anio, mes );
                PeriodoContable periodos = model.cierreMensualMOCService.getPeriodo();
                
                if ( periodos.getAc().equals("C") ){
                 
                    next = next + "?msg=El Periodo " + anio + " - " + mes + " Ya Habia Sido Cerrado Anteriormente!";
                                        
                } else {
                    
                    periodos.setUser_update( user_update );
                    
                    model.cierreMensualMOCService.cerrarPeriodo( periodos );
                    next = next + "?msg=Periodo Cerrado Exitosamente!";
                    
                }
                
                InterfaceCierreMensualMOC icmmoc = new InterfaceCierreMensualMOC(usuario);
                icmmoc.start();
            
            } else{
                
                next = next + "?msg=No Existe El Periodo Ingresado!!";
                
            }
            
        } catch ( Exception e ){
            
            e.printStackTrace();
            
        }
        
        this.dispatchRequest( next );
                        
    }
    
}