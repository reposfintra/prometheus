/********************************************************************
 *      Nombre Clase.................   TipoDoctoManagerAction.java
 *      Descripci�n..................   Maneja los eventos de b�squeda, anulaci�n ey modificaci�n
 *      Autor........................   Ing. Andr�s Maturana De La Cruz
 *      Fecha........................   8 de junio de 2006
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes S�nchez Polo S.A.
 *******************************************************************/

package com.tsp.finanzas.contab.controller;
import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.finanzas.contab.model.*;
import com.tsp.finanzas.contab.model.beans.*;


import org.apache.log4j.Logger;

public class TipoDoctoManagerAction extends Action{
    
    /** Creates a new instance of CuentasInsertAction */
    public TipoDoctoManagerAction() {
    }
    
    public void run () throws ServletException, InformationException {
        Logger log = Logger.getLogger(this.getClass());
        
        String next = "";
        
        HttpSession session = request.getSession();
        Usuario user = (Usuario) session.getAttribute("Usuario");
        
        String opc = request.getParameter("opc");
        
        try{
            
            if( opc.compareTo("SEARCH") == 0 ){
                
                /* BUSQUEDA */
                
                String code = request.getParameter("code");
                String codeint = request.getParameter("cod_interno");
                String desc = request.getParameter("desc");
                
                model.tipo_doctoSvc.search(user.getDstrct(), code, codeint, desc);
                Vector info = model.tipo_doctoSvc.getVector();
                
                request.setAttribute("Info", info);
                next = "/jsp/finanzas/contab/tipo_docto/TipoDoctoInfo.jsp";
                
                ////////System.out.println("............. SE ECONTRO: " + info.size());
            } else if ( opc.compareTo("GET") == 0 ){
                
                /*  CARGAR OBJETO */
                
                String code = request.getParameter("code");
                
                model.tipo_doctoSvc.obtener(user.getDstrct(), code);
                Tipo_Docto tdoc = model.tipo_doctoSvc.getDocto();
                
                request.setAttribute("TDocto", tdoc);
                next = "/jsp/finanzas/contab/tipo_docto/TipoDoctoUpdate.jsp?update=";
                //////System.out.println("..................... SE ENCONTRO: " + tdoc.getCodigo());
                model.tipo_doctoSvc.codigosInternos();
                
            } else if ( opc.compareTo("UPDATE") == 0 ){
                
                /* ACTUALIZAR OBJETO */
                
                String code = request.getParameter("code");
                
                
                Tipo_Docto docto = new Tipo_Docto();
                docto.setCodigo(request.getParameter("code"));
                docto.setBase(user.getBase());
                docto.setCodigo_interno(request.getParameter("cod_interno"));
                docto.setDescripcion(request.getParameter("desc"));
                docto.setDistrito(user.getDstrct());
                docto.setManeja_serie(request.getParameter("mserie"));
                docto.setPrefijo(request.getParameter("prefijo"));
                docto.setPrefijo_anio(request.getParameter("preano"));
                docto.setPrefijo_mes(request.getParameter("premes"));
                docto.setReg_status("");
                docto.setEsDiario(request.getParameter("diario"));
                
                if( docto.getManeja_serie().compareTo("S")==0 ){
                    docto.setSerie_fin(request.getParameter("serie_fin"));
                    docto.setSerie_ini(request.getParameter("serie_ini"));
                    docto.setLong_serie(request.getParameter("long_serie"));
                } else {
                    docto.setSerie_fin("0");
                    docto.setSerie_ini("0");
                    docto.setLong_serie("0");
                }
                
                docto.setUsuario(user.getLogin());
                docto.setTercero(request.getParameter("tercero"));
                
                
                model.tipo_doctoSvc.obtener(user.getDstrct(), docto.getCodigo());
                Tipo_Docto aux = model.tipo_doctoSvc.getDocto();
                
                if( aux.getCodigo_interno().compareTo(docto.getCodigo_interno())!= 0 &&
                        (( docto.getEsDiario().compareTo("S")==0 ||
                            ( docto.getEsDiario().compareTo("N")==0 && 
                                    !model.tipo_doctoSvc.isCodigoInternoUsado(docto.getDistrito(), docto.getCodigo_interno())) )))
                
                {
                    log.info("CODIGO INTERNO DIFERENTES. DIARIO? " + docto.getEsDiario() + ". CODINT: " + docto.getCodigo_interno()
                        + " USADO? " + model.tipo_doctoSvc.isCodigoInternoUsado(docto.getDistrito(), docto.getCodigo_interno()));
                    model.tipo_doctoSvc.setDocto(docto);
                    model.tipo_doctoSvc.actualizar();   
                    next = "/jsp/finanzas/contab/tipo_docto/TipoDoctoUpdate.jsp?msg=Se ha modificado exitosamente el registro.&update=OK";
                } else if ( aux.getCodigo_interno().compareTo(docto.getCodigo_interno())== 0 &&
                        (( docto.getEsDiario().compareTo("S")==0 ||
                            ( docto.getEsDiario().compareTo("N")==0 && 
                                    !model.tipo_doctoSvc.isCodigoInternoUsado(docto.getDistrito(), docto.getCodigo_interno())) )))
                {
                    log.info("CODIGO IGUALES. DIARIO? " + docto.getEsDiario() + ". CODINT: " + docto.getCodigo_interno()
                        + " USADO? " + model.tipo_doctoSvc.isCodigoInternoUsado(docto.getDistrito(), docto.getCodigo_interno()));
                    model.tipo_doctoSvc.setDocto(docto);
                    model.tipo_doctoSvc.actualizar();   
                    next = "/jsp/finanzas/contab/tipo_docto/TipoDoctoUpdate.jsp?msg=Se ha modificado exitosamente el registro.&update=OK";
                }else {
                    log.info("*CODIGO INTERNO DIFERENTES. DIARIO? " + docto.getEsDiario() + ". CODINT: " + docto.getCodigo_interno()
                        + " USADO? " + model.tipo_doctoSvc.isCodigoInternoUsado(docto.getDistrito(), docto.getCodigo_interno()));
                    next = "/jsp/finanzas/contab/tipo_docto/TipoDoctoUpdate.jsp";
                    next += "?msg=No se puede procesar la informaci�n. El c�digo interno no se puede reautilizar " +
                            "en los comprobantes no diarios.";
                }
                
                request.setAttribute("TDocto", docto);
                
                
            } else if ( opc.compareTo("DELETE") == 0 ){
                
                /* ANULAR OBJETO */
                
                Tipo_Docto docto = new Tipo_Docto();
                docto.setCodigo(request.getParameter("code"));
                docto.setBase(user.getBase());
                docto.setCodigo_interno(request.getParameter("cod_interno"));
                docto.setDescripcion(request.getParameter("desc"));
                docto.setDistrito(user.getDstrct());
                docto.setManeja_serie(request.getParameter("mserie"));
                docto.setPrefijo(request.getParameter("prefijo"));
                docto.setPrefijo_anio(request.getParameter("preano"));
                docto.setPrefijo_mes(request.getParameter("premes"));
                docto.setReg_status("A");
                docto.setEsDiario(request.getParameter("diario"));
                
                if( docto.getManeja_serie().compareTo("S")==0 ){
                    docto.setSerie_fin(request.getParameter("serie_fin"));
                    docto.setSerie_ini(request.getParameter("serie_ini"));
                    docto.setLong_serie(request.getParameter("long_serie"));
                } else {
                    docto.setSerie_fin("0");
                    docto.setSerie_ini("0");
                    docto.setLong_serie("0");
                }
                
                docto.setUsuario(user.getLogin());
                docto.setTercero(request.getParameter("tercero"));
                
                model.tipo_doctoSvc.setDocto(docto);
                model.tipo_doctoSvc.actualizar();
                
                next = "/jsp/trafico/mensaje/MsgAnulado.jsp";
            }
        }catch (SQLException e){
            e.printStackTrace ();
            throw new ServletException (e.getMessage ());
        }
        // Redireccionar a la p�gina indicada.
        this.dispatchRequest (next);
    }
    
}
