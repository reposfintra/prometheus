/********************************************************************
 *      Nombre Clase.................   TablaMovcomInsertAction.java
 *                                      TablamovconInsertAction
 *      Descripci�n..................   Extrae Datos de la tabla MSF900 y los inserta
 *                                      en la tabla movcon
 *      Autor........................   Ing. Leonardo Parody
 *      Fecha........................   19.01.2006
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *      Modificado...................   egonzalez2014
 *******************************************************************/

package com.tsp.finanzas.contab.controller;

import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.finanzas.contab.model.beans.*;
import com.tsp.finanzas.contab.model.*;
import com.tsp.finanzas.contab.model.threads.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;
import com.tsp.util.*;
import com.tsp.util.connectionpool.PoolManager;

/**
 *
 * @author  EQUIPO12
 */
public class TablaMovconInsertAction extends Action{
    
    
    /** Creates a new instance of TablaMovcomInsert */
    public TablaMovconInsertAction() {
    }
    
    public void run() throws ServletException, InformationException {
        
               
        String filtro = request.getParameter( "filtro" );        
        String check = "unchecked";
        String inicializa = request.getParameter("inicializar");
        if (inicializa!=null) {
            if (inicializa.equalsIgnoreCase("on")){
                check = "checked";
            }
        }
        String a�omes = request.getParameter("ano")+request.getParameter("mes");
        ////////System.out.println("check = "+check+" a�omes = "+a�omes);
        HiloMFS900movcon hilo = new HiloMFS900movcon();
        String inicializacion = request.getParameter("filtro");
        
        String patch = application.getRealPath("/WEB-INF/classes/com/tsp/finanzas/contab/propiedades");
        int dia = 1;
        String next = "/jsp/finanzas/contab/movimiento_MSF900_movcon/MSF900movconInsert.jsp?msg=Su petici�n ha sido procesada";
        Calendar fechadehoy = Calendar.getInstance();
        fechadehoy.set(0,0,0,0,0,0);
        try{
            
            //fechadehoy = model.tablamovconservice.getFechadeinicioProceso();
            ////////System.out.println("fecha de hoy = " + fechadehoy);
            String Fechacomparacion = Util.getFechaActual_String(6);
            Calendar fechacompara = Calendar.getInstance();
            int yearFC=Integer.parseInt(Fechacomparacion.substring(0,4));
            int monthFC=Integer.parseInt(Fechacomparacion.substring(5,7));
            int dateFC= Integer.parseInt(Fechacomparacion.substring(8,10));
            fechacompara.set(yearFC,monthFC,dateFC,0,0,0);
            ////////System.out.println("fecha comparacion = " + fechacompara);
            
            dia = Util.obtenerDiferenciaEnDias(fechacompara, fechadehoy);
            
        }catch (Exception e){
            
        }
        
        LinkedList processdates = new LinkedList();
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String user = usuario.getLogin();
        String base = usuario.getBase();
        String dstrct = usuario.getDstrct();
        
        ResourceBundle rb = ResourceBundle.getBundle("com/tsp/finanzas/contab/propiedades/tablamovcon");
        String ultimafecha = rb.getString("fechaultimoproceso");
        Calendar FechaSiguiProceso = Calendar.getInstance();
        
        ////////System.out.println("ultimafecha = "+ultimafecha);
        //formato fecha de ultimo proceso
        int yearUP=Integer.parseInt(ultimafecha.substring(0,4));
        int monthUP=Integer.parseInt(ultimafecha.substring(5,7));
        int dateUP= Integer.parseInt(ultimafecha.substring(8,10));
         
        Date FechaUltimoProceso = new Date(yearUP+"/"+monthUP+"/"+dateUP);
        
              
        String Fecha = Util.getFechaActual_String(6);
        
        //formatos de fechasfija y fechadeprocesonuevo
        int year=Integer.parseInt(Fecha.substring(0,4));
        int month=Integer.parseInt(Fecha.substring(5,7));
        int date= Integer.parseInt(Fecha.substring(8,10));
        Date FechaFormat = new Date(year+"/"+month+"/"+date);
        
        //base inicial de la fecha.
        Date FechaFija = new Date("1999/05/01");
        ////////System.out.println("Fecha de ultimo proceso = "+FechaUltimoProceso);
        ////////System.out.println("Fecha de ayer = "+FechaFormat);
        /////////System.out.println("Fecha Fija = "+FechaFija);
        
        //Diferencia de dias entre el ultimo proceso y el nuevo proceso
        long difdias = (long) ( FechaFormat.getTime() - FechaUltimoProceso.getTime() ) / 1000 / 60 /60 / 24;
                
        //////System.out.println("diferencia ultimo proceso y hoy = "+difdias);
        
        long dias = 0;
        
        //base inicial de process_date
        long processdatebase = 7060;
        long processdatenumber = 0;
        
        dias =  (long) (FechaUltimoProceso.getTime() - FechaFija.getTime()) / 1000 / 60 /60 / 24;
        
        ////////System.out.println("Diferencia Dias segun fecha ultimo proceso= "+dias);
        if ( dia != 0 && difdias > 0 ) {
            for (int i=0; i<difdias; i++) {
                processdatenumber = processdatebase + dias + i;
                String Processdate=""+processdatenumber;
                processdates.add(Processdate);
                ////////System.out.println("Process dates agregados = "+Processdate+ "   tama�o processdates = "+processdates.size());
            }
        }
        //hilo.start( user, dstrct, base, processdates, patch, model, inicializacion, check, a�omes);
        if ( check.equalsIgnoreCase( "checked" ) ) {
           // //////System.out.println("Voy a entrar por el checked inicializacion = "+inicializacion);
            hilo.start( user, dstrct, base, processdates, patch, model, inicializacion, check, a�omes);
            
        } else {
            if ( ( dia != 0 ) && ( difdias > 0 ) ) {
               ////////System.out.println("voy a entrar por el unchecked"); 
               hilo.start( user, dstrct, base, processdates, patch, model, inicializacion, check, a�omes);
                
            } else {
                
                next = "/jsp/finanzas/contab/movimiento_MSF900_movcon/MSF900movconInsert.jsp?msg=Este proceso ya fue iniciado el d�a de hoy";
                
            } 
            
        }
        this.dispatchRequest(next);
    }
    
}
