/********************************************************************
 *      Nombre Clase.................   RepVentasDiarioPDFAction.java
 *      Descripci�n..................   Manje la generaci�n del reporet de ventas diario.
 *      Autor........................   Ing. Andr�s Maturana De La Cruz
 *      Fecha........................   21 de junio de 2006, 02:52 PM
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes S�nchez Polo S.A.
 *******************************************************************/

package com.tsp.finanzas.contab.controller;

import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.finanzas.contab.model.threads.*;

//dom4j

//Logger
import org.apache.log4j.*;

/**
 *
 * @author  Ing. Andr�s Maturana De La Cruz
 */
public class RepVentasDiarioPDFAction extends Action implements Serializable{
    
    Logger log = Logger.getLogger (RepVentasDiarioPDFAction.class);
    
    /** Crea una nueva instancia de  RepVentasDiarioPDFAction */
    public RepVentasDiarioPDFAction() {
    }
    
    public void run() throws ServletException, InformationException  {
        try{
            String fecha_ini = request.getParameter("FechaI");
            String fecha_fin = request.getParameter("FechaF");
            
            log.info("Fecha Inicial: " + fecha_ini);
            log.info("Fecha Final: " + fecha_fin);
            
            String next = "";
            HttpSession session = request.getSession();
            Usuario user = (Usuario) session.getAttribute("Usuario");
            
            //Fecha del sistema
            Calendar FechaHoy = Calendar.getInstance();
            Date d = FechaHoy.getTime();
            SimpleDateFormat s = new SimpleDateFormat("yyyyMMdd_kkmm");
            String FechaFormated = s.format(d);
            
            String ruta = application.getRealPath("/") + "/exportar/migracion/" + user.getLogin() + "/";
            File file = new File(ruta);
            file.mkdirs();
            
            File xslt = new File(application.getRealPath("/") + "Templates/RepDiarioVentas.xsl");
            File pdf = new File(ruta + "Reporte_Diario_Ventas_" + FechaFormated + ".pdf");
            
            next = "/jsp/finanzas/contab/reportes/ReporteVentasDiario.jsp?msg=" +
                    "Se ha iniciado la generaci�n del reporte.";
            
            
            RepVentasDiarioTh hilo = new RepVentasDiarioTh(); 
            hilo.start((com.tsp.operation.model.Model) session.getAttribute("model"), model, user, fecha_ini, fecha_fin); 
            
            
//            model.reportes.reporteVentasDiario((String) session.getAttribute("Distrito"), fecha_ini, fecha_fin);
//            model.reportes.reporteVentasDiarioXml();
//            Document documento = model.reportes.getDocumento();
//            System.gc();
//            log.info("FACTURAS ENCONTRADAS: " + model.reportes.getVector().size());
//            
//            com.tsp.operation.model.Model modelOp = (com.tsp.operation.model.Model) session.getAttribute("model");
//            PDFGenerator.generarPDF(xslt, pdf, documento, modelOp, 
//                    "Reporte de Ventas Diario", 
//                    "Generaci�n del Reporte de Ventas Diario en formato PDF.", 
//                    user.getLogin());
            
            System.gc();
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if(rd == null)
                throw new ServletException("No se pudo encontrar "+ next);
            rd.forward(request, response);
            
        }catch(Exception e){
            e.printStackTrace();
            throw new ServletException("Error en RepVentasDiarioPDFAction .....\n"+e.getMessage());
        }
    }
    
}
