/********************************************************************
 *      Nombre Clase.................   CuentaValidaInsertAction.java
 *      Descripci�n..................   Inserta Cuentas Validas
 *      Autor........................   Ing. Leonardo Parody
 *      Fecha........................   19.01.2006
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.finanzas.contab.controller;

import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.finanzas.contab.model.beans.CuentaValida;
import com.tsp.finanzas.contab.model.*;
import com.tsp.operation.model.threads.*;
import org.apache.log4j.Logger;
/**
 *
 * @author  EQUIPO12
 */
public class CuentaValidaInsertAction extends Action{
    
    /** Creates a new instance of CuentaValidaInsertAction */
    public CuentaValidaInsertAction () {
    }
    
    public void run () throws ServletException, InformationException {
        String next = "";
        HttpSession session = request.getSession ();
        Usuario usuario = (Usuario) session.getAttribute ("Usuario");
        String user = usuario.getLogin ();
        String base = usuario.getBase ();
        String dstrct = usuario.getDstrct ();
        String clase = request.getParameter ("clase");
        String agencia = request.getParameter ("agencia");
        String unidad = request.getParameter ("unidad");
        String cliente_area = request.getParameter ("cliente_area");
        String elemento = request.getParameter ("elemento");
        String cuentaequivalente = request.getParameter("cuentaequivalente");
        TreeMap CE = (TreeMap)request.getAttribute("cuentasequivalentes");
        CuentaValida cuenta = new CuentaValida ();
        
        cuenta.setClase (clase);
        cuenta.setAgencia (agencia);
        cuenta.setUnidad (unidad);
        if (cliente_area.length()>3){
                cuenta.setCliente_area (cliente_area.substring(3,6));
        }else{
                cuenta.setCliente_area (cliente_area);
        }
        cuenta.setElemento (elemento);
        cuenta.setDstrct (dstrct);
        cuenta.setCreation_user (user);
        cuenta.setBase (base);
        cuenta.setUser_update (user);
       if (cuentaequivalente!=null) {
                cuenta.setCuentaEquivalente(cuentaequivalente); 
        }else{
                cuenta.setCuentaEquivalente("");
        }
        try {
            request.setAttribute("cuentasequivalentes",  CE);            
            boolean existe = model.cuentavalidaservice.ExisteCuentaValida (clase, agencia, unidad, cuenta.getClienteArea(), elemento);
            //////System.out.println("existe = "+existe);
            if (existe == false){
                model.cuentavalidaservice.InsertCuentaValida (cuenta);
                next = "/jsp/finanzas/Cuentas_Validas/CuentasValidasInsert.jsp?msg=Cuenta Valida Ingresada Satisfactoriamente";
            }else{
                    next = "/jsp/finanzas/Cuentas_Validas/CuentasValidasInsert.jsp?msg=La Cuenta Valida Ya Existe";
            }
        }catch(SQLException e){
            throw new ServletException (e.getMessage ());
        }
        this.dispatchRequest (next);
    }
    
}
