/*
 * AdminComprobantesManagerAction.java
 *
 * Created on 3 de julio de 2006, 10:12 AM
 */
/*********************************************************************************
 * Nombre clase :      AdminComprobantesManagerAction.java                       *
 * Descripcion :       Administra la funciones realizadas desde la interfaz      *
 * Autor :             David Pi�a Lopez                                          *
 * Fecha :             3 de julio de 2006, 10:12 AM                              *
 * Version :           1.0                                                       *
 * Copyright :         Fintravalores S.A.                                   *
 *********************************************************************************/
package com.tsp.finanzas.contab.controller;

import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.finanzas.contab.model.beans.*;
import com.tsp.operation.model.beans.*;
import com.tsp.finanzas.contab.model.threads.*;

/**
 *
 * @author  David
 */
public class AdminComprobantesManagerAction extends Action {
    
    /** Creates a new instance of AdminComprobantesManagerAction */
    public AdminComprobantesManagerAction() {
    }
    
    @Override
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        String next = "";
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        try{
            String opc = request.getParameter( "opc" );
            
            //System.out.println("OPC " + opc);
            
            if(opc.equalsIgnoreCase( "buscar" )){
                String tipodoc = request.getParameter( "tipodoc" );
                String periodo = request.getParameter( "anio" ) + request.getParameter( "mes" );
                String numdoc = request.getParameter( "numdoc" );
                String criterios = request.getParameter( "criterios" );
                String orden = request.getParameter( "orden" );
                String fechaInicial = request.getParameter( "fechaInicial" );
                String fechaFinal = request.getParameter( "fechaFinal" );
                Comprobantes comp = new Comprobantes();
                comp.setTipodoc ( tipodoc );
                comp.setNumdoc  ( numdoc  );
                comp.setPeriodo ( ""      );
                
                if (numdoc.trim().equals("")){
                    comp.setPeriodo( periodo );
                    comp.setAuxiliar( criterios );
                    comp.setTercero( orden );
                    comp.setFecha_aplicacion( fechaInicial );
                    comp.setFecha_creacion( fechaFinal );
                }
                
                model.comprobanteService.setComprobante( comp );
                model.comprobanteService.getComprobantes();
                Vector comprobantes = model.comprobanteService.getVector();
                
                session.setAttribute( "resultado", null );
                request.setAttribute( "mensaje", null );
                if( comprobantes.size() == 0 ){
                    request.setAttribute( "mensaje", "No se encontr� ning�n resultado" );
                }else{
                    session.setAttribute( "resultado", "ok" );
                }
                
                session.setAttribute( "tipodoc", tipodoc );
                session.setAttribute( "anio", request.getParameter( "anio" ) );
                session.setAttribute( "mes", request.getParameter( "mes" ) );
                session.setAttribute( "numdoc", numdoc );
                session.setAttribute( "criterios", criterios );
                session.setAttribute( "orden", orden );
                session.setAttribute( "fechaInicial", fechaInicial );
                session.setAttribute( "fechaFinal", fechaFinal );
                
                next = "/jsp/finanzas/contab/adminComprobantes/buscarComprobantes.jsp?resultado=ok";
            }else if(opc.equalsIgnoreCase( "accion" )){
                Vector comprobantes = new Vector();
                String argumentos[] = request.getParameterValues( "comprobantes" );
                if( argumentos != null ){
                    for( int i = 0; i < argumentos.length; i++ ){
                        String valores[] = argumentos[ i ].split( "&" );
                        Comprobantes comprobante = new Comprobantes();
                        comprobante.setTipodoc( valores[ 0 ] );
                        comprobante.setNumdoc( valores[ 1 ] );
                        comprobante.setGrupo_transaccion( Integer.parseInt( valores[ 2 ] ) );
                        comprobante.setDstrct( valores[ 3 ] );
                        comprobantes.add( comprobante );
                    }
                }                
                
                String item = request.getParameter( "item" );
                if( item.equalsIgnoreCase("contabilizar") ){
                    ActualizacionMayorizacion hilo = new ActualizacionMayorizacion();
                   // hilo.start( usuario.getLogin(), comprobantes );
                    request.setAttribute( "mensaje", "Proceso de Contabilizaci�n Iniciado" );
                }else if( item.equalsIgnoreCase("validar") ){
                    ValidacionMovimiento hilo = new ValidacionMovimiento(); 
                    hilo.start( usuario, comprobantes );
                    request.setAttribute( "mensaje", "Proceso de Validaci�n de Comprobantes Iniciado" );                    
                }else if( item.equalsIgnoreCase("descontabilizar") ){
                    Descontabilizar hilo = new Descontabilizar();
                    hilo.start(comprobantes, usuario);
                    request.setAttribute( "mensaje", "Proceso de Descontabilizacion de Comprobantes Iniciado" );
                }else if( item.equalsIgnoreCase("reversar") ){
                    Reversar hilo = new Reversar();
                    Comprobantes c = (Comprobantes) comprobantes.get(0);
                    c.getTipodoc();
                    
                    Hashtable t = model.GrabarComprobanteSVC.buscarDatosTipoDoc(c.getDstrct(), c.getTipodoc());
                    if (t!=null){
                        if (t.get("comprodiario").toString().equals("S")){
                            hilo.start(comprobantes, usuario);
                            request.setAttribute( "mensaje", "Proceso de Reversion de Comprobantes Iniciado" );                            
                        }
                        else{
                            request.setAttribute( "mensaje", "Solo se pueden reversar comprobantes de diarios." );
                        }
                    }
                    else
                        request.setAttribute( "mensaje", "Tipo de documento no valido." );
                    
                }else if( item.equalsIgnoreCase("imprimir") ){
                    HImpresionComprobantePDF hilo = new HImpresionComprobantePDF();
                    hilo.start(comprobantes, model, usuario);
                    request.setAttribute( "mensaje", "Proceso de Impresion de Comprobantes Iniciado" );
                }
                next = "/jsp/finanzas/contab/adminComprobantes/buscarComprobantes.jsp";
                session.setAttribute( "resultado", null );
                
            }else if(opc.equalsIgnoreCase( "ver" )){
                String GrupTrans = request.getParameter("grupo");
                String dstrct    = request.getParameter("dstrct");
                String tipodoc = request.getParameter("tipodoc");
                String numdoc  = request.getParameter("numdoc");
                int grupo = Integer.parseInt(GrupTrans);
                
                String fechaAplicacion = request.getParameter( "fechaapply" );
                if( fechaAplicacion.equalsIgnoreCase( "0099-01-01" ) ){
                    next = "/controllercontab?estado=Grabar&accion=Comprobante&OP=BUSCAR_COMPROBANTE";
                } else {
                    ComprobanteFacturas comprobante = model.GrabarComprobanteSVC.ComprobanteSearch(dstrct, tipodoc, numdoc, grupo);
                    
                    if (comprobante!=null){
                        List item = comprobante.getItems();
                        for( int i=0; i< item.size();i++ ){
                            ComprobanteFacturas comprodet = (ComprobanteFacturas) item.get(i);

                            model.subledgerService.buscarCodigosCuenta( comprodet.getCuenta() );
                            Vector tipSubledger = model.subledgerService.getCodigos();
                            comprodet.setCodSubledger( tipSubledger );
                        }
                        
                        session.setAttribute( "periodo",comprobante.getPeriodo() );
                        model.GrabarComprobanteSVC.buscarTipoDoc( usuario.getDstrct() );
                        model.GrabarComprobanteSVC.setComprobante( comprobante );
                        next = "/jsp/finanzas/contab/adminComprobantes/verComprobante.jsp?maxfila="+comprobante.getTotal_items()+"&opcion=modificar";                        
                        request.setAttribute("mensaje", "");
                    } else {
                        model.GrabarComprobanteSVC.setComprobante( null );
                        next = "/jsp/finanzas/contab/adminComprobantes/verComprobante.jsp?maxfila=0&opcion=ver";                        
                        request.setAttribute("mensaje", "No se pudo encontrar el comprobante.");
                    }
                }                
            } else if(opc.equalsIgnoreCase( "searchEgreso" )){
                String document    = request.getParameter("document");
                String transaction = request.getParameter("transaction");
                Hashtable eg = model.comprobanteService.searchEgreso(document, transaction);
                next = "/controller?estado=Menu&accion=Cargar&marco=no&opcion=37&carpeta=/jsp/cxpagar/consultaEgresos&pagina=consultarEgresos.jsp";
                if (eg!=null){
                    if (eg.get("reg_status").toString().equals("A"))
                        request.setAttribute("mensaje", "Documento anulado.");
                    else    
                        next = "/controller?estado=ConsultarEgresos&accion=Manager&opc=ver&dstrct="+ eg.get("dstrct").toString() +"&branch_code="+ eg.get("branch_code").toString() +"&bank_account_no="+ eg.get("bank_account_no").toString() +"&document_no="+ eg.get("document_no").toString();
                } else
                    request.setAttribute("mensaje", "Documento no encontrado.");
            }
            else if(opc.equalsIgnoreCase( "searchRPL" )){
                String document    = request.getParameter("document");
                com.tsp.operation.model.Model modelOperation = (com.tsp.operation.model.Model) session.getAttribute("model");
                Vector datos = modelOperation.planillaService.obtenerDatosRPL(document);
                request.setAttribute("datos", datos);
                
                if (datos==null || datos.isEmpty())
                    request.setAttribute("msg", "No se encontraron remesa relacionadas para la planilla");
                
                next = "/datosplanilla/MostrarRPL.jsp";
            }
            
            System.out.println("AC NEXT : " + next);
            this.dispatchRequest(next); 
            
        }catch(Exception e){
            e.printStackTrace();
            throw new ServletException(e.getMessage());
        }
    }
    
}
