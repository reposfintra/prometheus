/******************************************************************************
 * Nombre clase :                   ConsultasSaldosContablesAction.java       *
 * Descripcion :                    Clase que maneja los eventos              *
 *                                  relacionados con el programa de           *
 *                                  Buscar los Saldos Contables en la BD.     *
 * Autor :                          LREALES                                   *
 * Fecha :                          12 de Junio de 2006, 11:02 AM             *
 * Version :                        1.0                                       *
 * Copyright :                      Fintravalores S.A.                   *
 *****************************************************************************/

package com.tsp.finanzas.contab.controller;

import java.io.*;
import java.util.*;
import com.tsp.exceptions.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.util.Util;
import com.tsp.operation.model.beans.*;
import com.tsp.finanzas.contab.model.beans.*;
import com.tsp.finanzas.contab.model.services.*;
import com.tsp.finanzas.contab.model.threads.*;
import java.text.*;
import java.sql.SQLException;
import com.tsp.operation.model.*;

public class ConsultasSaldosContablesAction extends Action {
    
    /** Creates a new instance of ConsultasSaldosContablesAction */
    public ConsultasSaldosContablesAction () {
    }

    public void run () throws ServletException, InformationException {
        
        String next = "/jsp/finanzas/contab/consultas_saldos_contables/FiltroConsultasSaldosContables.jsp";
        
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String distrito = (String) session.getAttribute("Distrito");
        String creation_user = usuario.getLogin().toUpperCase();
        
        String msg = "";
        
        // distrito
        String cuenta_ini = (request.getParameter("cuenta_ini") != null)?request.getParameter("cuenta_ini").toUpperCase():"";
        String cuenta_fin = (request.getParameter("cuenta_fin") != null)?request.getParameter("cuenta_fin").toUpperCase():"";
        
        String anio_ini = (request.getParameter("anio_inicial") != null)?request.getParameter("anio_inicial"):"";
        String anio_fin = (request.getParameter("anio_final") != null)?request.getParameter("anio_final"):"";
        String mes_ini = (request.getParameter("mes_inicial") != null)?request.getParameter("mes_inicial"):"";
        String mes_fin = (request.getParameter("mes_final") != null)?request.getParameter("mes_final"):"";
        //////System.out.println("***anio_ini "+anio_ini);
        //////System.out.println("***anio_fin "+anio_fin);
        //////System.out.println("***mes_ini "+mes_ini);
        //////System.out.println("***mes_fin "+mes_fin);
                
        String tipo_presentacion = (request.getParameter("tipo_presentacion") != null)?request.getParameter("tipo_presentacion"):"";
        
        String incluye_subledger = (request.getParameter("incluye_subledger") != null)?request.getParameter("incluye_subledger"):"";
                        
        int mi = Integer.parseInt( mes_ini );
        int mf = Integer.parseInt( mes_fin );        
        int ai = Integer.parseInt( anio_ini );
        int af = Integer.parseInt( anio_fin );  
        String m = "";
        if ( ai == af ){
            for( mi = Integer.parseInt( mes_ini ); mi <= mf; mi++ ){
                //////System.out.println("* imprime columna : "+ai+" - "+mi);
                m = "" + mi;
                if( m.length() == 1 ){
                    m = "0" + m;
                    //////System.out.println("fdm"+m);
                } else{
                    //////System.out.println("fdm"+m);
                }
            }
        } else if ( ai < af ){
            for( mi = Integer.parseInt( mes_ini ); mi <= 12; mi++ ){                
                //////System.out.println("* imprime columna : "+ai+" - "+mi);
                m = "" + mi;
                if( m.length() == 1 ){
                    m = "0" + m;
                    //////System.out.println("fdm"+m);
                } else{
                    //////System.out.println("fdm"+m);
                }
                
                if ( mi == 12 ){
                    for( int k = 1; k <= mf; k++ ){
                        //////System.out.println("* imprime columna : "+af+" - "+k);
                        m = "" + k;
                        if( m.length() == 1 ){
                            m = "0" + m;
                            //////System.out.println("fdm"+m);
                        } else{
                            //////System.out.println("fdm"+m);
                        }
                    }
                }
            }            
        } else{
            //////System.out.println("ERRRRRRROR!!");
        }
        
        try{
            
            SaldosContables saldos = new SaldosContables();
            
            saldos.setDstrct( distrito );
            saldos.setCuenta_ini( cuenta_ini );
            saldos.setCuenta_fin( cuenta_fin );
            saldos.setFecha_ini( anio_ini );
            saldos.setFecha_fin( anio_fin );
            
            if ( incluye_subledger.equals("no") ){
                                
                model.consultasSaldosContablesService.listaSaldosContables( distrito, anio_ini, anio_fin, cuenta_ini, cuenta_fin );
               
                next = "/jsp/finanzas/contab/consultas_saldos_contables/ConsultasSaldosContables.jsp";
                
            } else if ( incluye_subledger.equals("si") ){
                                
                model.consultasSaldosContablesService.listaIncluyeSubledger( distrito, anio_ini, anio_fin, cuenta_ini, cuenta_fin );
               
                next = "/jsp/finanzas/contab/consultas_saldos_contables/ConsultasSaldosContables.jsp";
                
            } else {
                
                next = next + "?msg=Error!!";

            }
            
            session.setAttribute ( "distrito", distrito );
            session.setAttribute ( "cuenta_ini", cuenta_ini );
            session.setAttribute ( "cuenta_fin", cuenta_fin );
            session.setAttribute ( "anio_ini", anio_ini );
            session.setAttribute ( "anio_fin", anio_fin );
            session.setAttribute ( "mes_ini", mes_ini );
            session.setAttribute ( "mes_fin", mes_fin );
            session.setAttribute ( "tipo_presentacion", tipo_presentacion );
            session.setAttribute ( "incluye_subledger", incluye_subledger );
            
        } catch ( Exception e ){
            
            e.printStackTrace();            
            throw new ServletException( e.getMessage () );
            
        }
        
        this.dispatchRequest( next );
        
    }
    
}