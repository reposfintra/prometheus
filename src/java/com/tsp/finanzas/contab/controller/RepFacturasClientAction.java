/********************************************************************
 *      Nombre Clase.................   RepFacturasClientAction.java
 *      Descripci�n..................   Manje la generaci�n del reporte de facturas de cliente.
 *      Autor........................   Ing. Andr�s Maturana De La Cruz
 *      Fecha........................   27 de junio de 2006, 02:52 PM
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes S�nchez Polo S.A.
 *******************************************************************/

package com.tsp.finanzas.contab.controller;
import com.tsp.exceptions.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.finanzas.contab.model.threads.ReporteFactCliTh;

//dom4j

//Logger
import org.apache.log4j.*;

/**
 *
 * @author  Ing. Andr�s Maturana De La Cruz
 */
public class RepFacturasClientAction extends Action{
    
    Logger log = Logger.getLogger (RepFacturasClientAction.class);
    
    /** Crea una nueva instancia de  RepVentasDiarioPDFAction */
    public RepFacturasClientAction() {
    }
    
    public void run() throws ServletException, InformationException {
        try{
            String fecha_ini = request.getParameter("FechaI");
            String fecha_fin = request.getParameter("FechaF");
            String login = request.getParameter("login");
            
            log.info("Fecha Inicial: " + fecha_ini);
            log.info("Fecha Final: " + fecha_fin);
            log.info("Login: " + login);
            
            String next = "/jsp/finanzas/contab/reportes/FacturasClientes.jsp?msg=Se ha iniciado la generaci�n del reporte exitosamente.";
            HttpSession session = request.getSession();
            Usuario user = (Usuario) session.getAttribute("Usuario");
            
            com.tsp.operation.model.Model modelop = (com.tsp.operation.model.Model) session.getAttribute("model");
            
            ReporteFactCliTh hilo = new ReporteFactCliTh();
            hilo.start(modelop, model, user, fecha_ini, fecha_fin, login);
            
            /* Cargar los usuarios de creaci�n de facturas */
            com.tsp.operation.model.Model modelOp = (com.tsp.operation.model.Model) session.getAttribute("model");
            modelOp.tablaGenService.loadTUsuariosFactura();
            TreeMap tmap = modelOp.tablaGenService.getTreemap();
            request.setAttribute("TUser", tmap);

            RequestDispatcher rd = application.getRequestDispatcher(next);
            if(rd == null)
                throw new ServletException("No se pudo encontrar "+ next);
            rd.forward(request, response);
            
        }catch(Exception e){
            e.printStackTrace();
            throw new ServletException("Error en RepFacturasClientAction .....\n"+e.getMessage());
        }
    }
    
}
