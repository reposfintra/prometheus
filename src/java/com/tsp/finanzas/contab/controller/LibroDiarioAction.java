/*******************************************************************
 *      Nombre Clase.................   LibroDiarioAction.java
 *      Descripci�n..................   Action para rpt Libro Diario
 *      Autor........................   Osvaldo P�rez Ferrer
 *      Fecha........................   25 de Junio de 2006
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

/*
 * LibroDiario.java
 *
 * Created on 25 de junio de 2006, 03:55 PM
 */

package com.tsp.finanzas.contab.controller;


import com.tsp.exceptions.*;
import com.tsp.operation.model.beans.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.finanzas.contab.model.beans.PeriodoContable.*;
import com.tsp.finanzas.contab.model.DAO.*;
import com.tsp.finanzas.contab.model.*;
import com.tsp.finanzas.contab.model.beans.*;
import com.tsp.util.*;
import com.tsp.finanzas.contab.model.threads.LibroDiario;
/**
 *
 * @author  Osvaldo
 */
public class LibroDiarioAction extends Action{
    
    /** Creates a new instance of LibroDiario */
    public LibroDiarioAction() {
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        
        String next = "";
        String mensaje = "";
        String anio = "";
        String mes = "";
        String distrito = "";
        
        String fecha = "";
        
        
        
        try{
            
            HttpSession session = request.getSession();
            Usuario u = (Usuario)session.getAttribute("Usuario");
            
            anio = request.getParameter("anio");
            mes = request.getParameter("mes");          
            
            next = "/jsp/finanzas/reportes/libroDiario.jsp";                
          
            if(model.comprobanteService.existePeriodo(anio+mes)){
               
                LibroDiario lib = new LibroDiario();
                lib.start(u.getLogin(), anio+mes, u.getDstrct());
               
                request.setAttribute("mensaje", "Se ha iniciado la generaci�n del Libro Diario "+anio+mes);
            }
            else{
                request.setAttribute("mensaje", "No existen comprobantes con periodo "+anio+mes);
                              
            }
            
                        
        }catch(Exception ex){
            throw new ServletException(ex.getMessage());
        }        
        this.dispatchRequest(next);
    }
    
    
}
