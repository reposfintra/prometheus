/*
 * realocacionCuentasAction.java
 *
 * Created on 26 de junio de 2006, 07:05 PM
 */

/********************************************************************
 *  Nombre Clase.................   realocacionCuentasAction.java
 *  Descripci�n..................   Acci�n que permite generar la realocacion de cuentas para el periodo dado
 *  Autor........................   David Pi�a L�pez
 *  Fecha........................   26 de junio de 2006, 07:05 PM
 *  Versi�n......................   1.0
 *  Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.finanzas.contab.controller;

import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.finanzas.contab.model.beans.*;
import com.tsp.finanzas.contab.model.*;
import com.tsp.operation.model.beans.*;
import com.tsp.finanzas.contab.model.threads.*;

/**
 *
 * @author  David
 */
public class realocacionCuentasAction extends Action {
    
    /** Creates a new instance of realocacionCuentasAction */
    public realocacionCuentasAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next = "/jsp/finanzas/contab/realocacionCuentas/procesoRealocacion.jsp";                
        try{
            HttpSession session = request.getSession();
            Usuario usuario = (Usuario)session.getAttribute ( "Usuario" );
            
            String anio = request.getParameter("anio");
            String mes  = request.getParameter("mes");            
            request.setAttribute( "mensaje", "Proceso de Realocaci�n de Cuentas Iniciado" );            
            RealocacionCuentas rc = new RealocacionCuentas();
            rc.start( usuario, anio, mes );
            
        }catch (Exception e){
            e.printStackTrace();
            throw new ServletException(e.getMessage());
        }        
        this.dispatchRequest(next);
    }
    
}
