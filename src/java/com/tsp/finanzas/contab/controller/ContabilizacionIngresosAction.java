/*
 * ContabilizacionIngresos.java
 *
 * Created on 5 de marzo de 2007, 12:58 PM
 */

package com.tsp.finanzas.contab.controller;

import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.util.Util;

import com.tsp.operation.model.beans.Usuario;
import com.tsp.finanzas.contab.model.threads.HContabilizacionIngresos;
/**
 *
 * @author  equipo
 */
public class ContabilizacionIngresosAction extends Action{
    
    /** Creates a new instance of ContabilizacionIngresos */
    public ContabilizacionIngresosAction() {
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        try{
            HttpSession session = request.getSession();
            Usuario usuario = (Usuario) session.getAttribute("Usuario");            
            String next = "/jsp/finanzas/contab/contabilizacion/Ingresos.jsp";
            
            
            String opcion = Util.coalesce( (String) request.getParameter("evento") , "");
            
            if (opcion.equalsIgnoreCase("CONTABILIZAR")){
                if ( !model.ContabilizacionIngresosSvc.isProcessRun() ) {
                    HContabilizacionIngresos h = new HContabilizacionIngresos();
                    h.start(usuario,  model.ContabilizacionIngresosSvc );    
                    
                    next += "?msj=Proceso de contabilizacion de Ingresos a iniciado correctamente...";
                } else {
                    next += "?msj=Actualmente el proceso se est� realizando, por favor intente mas tarde....";
                }
            }
            
            this.dispatchRequest(next);
            
        } catch (Exception ex ){
            ex.printStackTrace();
            throw new ServletException (ex.getMessage());
        }
    }
    
}
