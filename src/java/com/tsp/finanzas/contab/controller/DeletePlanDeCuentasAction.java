/******************************************************************************
 * Nombre clase :                   DeletePlanDeCuentasAction.java            *
 * Descripcion :                    Clase que maneja los eventos              *
 *                                  relacionados con el programa de           *
 *                                  Anular una Cuenta en la BD.               *
 * Autor :                          LREALES                                   *
 * Fecha :                          6 de Junio de 2006, 09:32 AM              *
 * Version :                        1.0                                       *
 * Copyright :                      Fintravalores S.A.                   *
 *****************************************************************************/

package com.tsp.finanzas.contab.controller;

import java.io.*;
import java.util.*;
import com.tsp.exceptions.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.util.Util;
import com.tsp.operation.model.beans.*;
import com.tsp.finanzas.contab.model.beans.*;
import com.tsp.finanzas.contab.model.services.*;
import com.tsp.finanzas.contab.model.threads.*;
import java.text.*;
import java.sql.SQLException;
import com.tsp.operation.model.*;

public class DeletePlanDeCuentasAction extends Action {
    
    /** Creates a new instance of DeletePlanDeCuentasAction */
    public DeletePlanDeCuentasAction () {
    }

    public void run () throws ServletException, InformationException {
        
        String next = "/jsp/finanzas/contab/plan_de_cuentas/eliminar/EliminarPlanDeCuentas.jsp";
        
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String distrito = (String) session.getAttribute("Distrito");
        String creation_user = usuario.getLogin().toUpperCase();
        
        String msg = "";
        
        // user_update
        // distrito
        String cuenta = (request.getParameter("cuenta") != null)?request.getParameter("cuenta").toUpperCase():"";
        
        try{
            
            PlanDeCuentas cuentas = new PlanDeCuentas();
            
            cuentas.setUser_update( creation_user );
            cuentas.setDstrct( distrito );
            cuentas.setCuenta( cuenta );
            
            if ( ( distrito != "" ) && ( cuenta != "" ) && ( creation_user != "" ) ){
                
                model.planDeCuentasService.searchCuenta( distrito, cuenta );
                
                int cont = model.planDeCuentasService.getVec_cuentas().size();
                
                if ( cont > 0 ){
                    
                    model.planDeCuentasService.anularCuenta( cuentas );
                    next = next + "?e=si";
                
                }                
                
            } else {
                
                next = next + "?msg=Por favor ingrese todos los datos obligatorios!!&e=no";

            }  
            
        } catch ( Exception e ){
                        
            throw new ServletException( e.getMessage () );
            
        }
        
        this.dispatchRequest( next );
        
    }
    
}