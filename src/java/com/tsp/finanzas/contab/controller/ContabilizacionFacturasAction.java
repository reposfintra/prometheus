/********************************************************************
 *  Nombre Clase.................   ContabilizacionFacturasAction.java
 *  Descripci�n..................   Maneja eventos para la contabilizacion de facturas
 *  Autor........................   Ing. FERNEL VILLACOB DIAZ
 *  Fecha........................   13/06/2006
 *  Versi�n......................   1.0
 *  Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/



package com.tsp.finanzas.contab.controller;




import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.finanzas.contab.model.threads.HContabilizacion;
import com.tsp.operation.model.Model;
import com.tsp.util.*;



public class ContabilizacionFacturasAction extends Action{
    
   
    
    public void run() throws ServletException, com.tsp.exceptions.InformationException {
        
         try{
                    
            
            HttpSession session   = request.getSession();
            Usuario     usuario   = (Usuario) session.getAttribute("Usuario");       
        
            
            String  base    = "/jsp/finanzas/contab/contabilizacionFacturas/";
            String  next    = "Contabilizacion.jsp";
            String  msj     = "";
            
            String  evento  =  request.getParameter("evento");
            
            
            if ( evento!=null){
                
                if(evento.equals("CONTABILIZAR")){
                    
                    //com.tsp.operation.model.Model  mo = new com.tsp.operation.model.Model();
                    //boolean hayTasa = mo.tasaService.isTasaHoy( usuario.getDstrct() );
                    // if( hayTasa ){
                    
                            if( ! model.ContabilizacionFacSvc.isProcess() ){
                                model.ContabilizacionFacSvc.setProcess( true );

                                HContabilizacion  hilo =  new HContabilizacion();
                                hilo.start(model, usuario );

                                msj   = "Proceso de contabilizaci�n ha iniciado...";
                            }  
                            else
                              msj   = "Actualmente el proceso se est� realizando, por favor intente mas tarde....";

                            next += "?msj=" + msj;
                    
                    
                    //}else
                        //next  = "/jsp/cxpagar/Mensaje.jsp?msj=" + mo.tasaService.getMsj();
                  
                }
                
            }
            
            
            dispatchRequest( base + next );
            
        } catch (Exception e){
             throw new ServletException(e.getMessage());
        }
    }
    
}
