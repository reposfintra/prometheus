/*
 * Nombre        SubledgerAnularAction.java
 * Autor         Ing. Diogenes Bastidas
 * Fecha         7 de junio de 2006, 03:45 PM
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */



package com.tsp.finanzas.contab.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.finanzas.contab.model.beans.*;
import com.tsp.operation.model.beans.*;
import com.tsp.finanzas.contab.model.*;
import com.tsp.exceptions.*;
import com.tsp.util.*;

public class SubledgerAnularAction extends Action{
    
    /** Creates a new instance of SubledgerAnularAction */
    public SubledgerAnularAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next = "/jsp/trafico/mensaje/MsgAnulado.jsp";
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        
        String cuenta =  "";
        String tipo = request.getParameter("tipo");
        String id = request.getParameter("id");
        
        try{
            Vector vec = model.subledgerService.buscarCuentas_Tipo(tipo);
            for (int i=0; i<vec.size(); i++){
                TablaGen objtipo = (TablaGen) vec.get(i);
                cuenta = objtipo.getTable_code();
                
                Subledger sub = new Subledger();
                sub.setDstrct(usuario.getDstrct());
                sub.setCuenta(cuenta);
                sub.setId_subledger(id);
                sub.setTipo_subledger(tipo);
                sub.setUser_update(usuario.getLogin());
                sub.setLast_update(Util.fechaActualTIMESTAMP());
                sub.setBase(usuario.getBase());
                sub.setReg_status("");
                model.subledgerService.anularSubledger(sub);
            }
            
        }catch (Exception e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
