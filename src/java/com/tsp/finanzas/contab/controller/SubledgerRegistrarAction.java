/******
 * Nombre:        SubledgerRegistrarAction.java
 * Descripci�n:   Clase Action para registrar los subledger
 * Autor:         Ing. Diogenes Antonio Bastidas Morales
 * Fecha:         6 de junio de 2006, 06:52 PM
 * Versi�n        1.0
 * Coyright:      Transportes Sanchez Polo S.A.
 *******/

package com.tsp.finanzas.contab.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.finanzas.contab.model.beans.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.finanzas.contab.model.*;
import com.tsp.exceptions.*;
import com.tsp.util.*;


public class SubledgerRegistrarAction extends Action{
    
    /** Creates a new instance of SubledgerRegistrarAction */
    public SubledgerRegistrarAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next = "/" + request.getParameter("carpeta")+ "/" + request.getParameter("pagina");
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        
        String cuenta =  "";
        String tipo = request.getParameter("tipo");
        String id = request.getParameter("id");
        String nombre = request.getParameter("nombre");
        boolean sw = false;
        String SQL="";
        int con = 0;
        
        
        
        try{
            
            
            Vector vec = model.subledgerService.buscarCuentas_Tipo(tipo);
            for (int i=0; i<vec.size(); i++){
                TablaGen objtipo = (TablaGen) vec.get(i);
                cuenta = objtipo.getTable_code();
                
                Subledger sub = new Subledger();
                sub.setDstrct(usuario.getDstrct());
                sub.setCuenta(cuenta);
                sub.setId_subledger(id.toUpperCase());
                sub.setTipo_subledger(tipo);
                sub.setNombre(nombre);
                sub.setCreation_user(usuario.getLogin());
                sub.setCreation_date(Util.fechaActualTIMESTAMP());
                sub.setUser_update(usuario.getLogin());
                sub.setLast_update(Util.fechaActualTIMESTAMP());
                sub.setBase(usuario.getBase());
                
                try{
                    model.subledgerService.insertarSubledger(sub);
                }catch (Exception e){
                    sw=true;
                }
                if(sw){
                    sub.setReg_status("");
                    if(model.subledgerService.existeSubledgerAnulado(usuario.getDstrct(),cuenta, tipo, id ) ){
                        model.subledgerService.modificarSubledger(sub);
                    }else{
                        model.subledgerService.modificarSubledger(sub);
                        con++;
                    }
                    
                }
            }
            next+= "?mensaje=Subledger ingresado exitosamente..";
            if(con>0){
                next+="Actualizando "+con +"Susbledger existentes";
            }
            
            
        }catch (Exception e){
            e.printStackTrace();
        }
        //////System.out.println(next);
        this.dispatchRequest(next);
        
    }
    
    
}
