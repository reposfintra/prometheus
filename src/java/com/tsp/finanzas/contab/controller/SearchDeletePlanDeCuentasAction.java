/******************************************************************************
 * Nombre clase :                   SearchDeletePlanDeCuentasAction.java      *
 * Descripcion :                    Clase que maneja los eventos              *
 *                                  relacionados con el programa de           *
 *                                  Buscar una Cuenta en la BD.               *
 * Autor :                          LREALES                                   *
 * Fecha :                          6 de Junio de 2006, 10:01 AM              *
 * Version :                        1.0                                       *
 * Copyright :                      Fintravalores S.A.                   *
 *****************************************************************************/

package com.tsp.finanzas.contab.controller;

import java.io.*;
import java.util.*;
import com.tsp.exceptions.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.util.Util;
import com.tsp.operation.model.beans.*;
import com.tsp.finanzas.contab.model.beans.*;
import com.tsp.finanzas.contab.model.services.*;
import com.tsp.finanzas.contab.model.threads.*;
import java.text.*;
import java.sql.SQLException;
import com.tsp.operation.model.*;

public class SearchDeletePlanDeCuentasAction extends Action {
    
    /** Creates a new instance of SearchDeletePlanDeCuentasAction */
    public SearchDeletePlanDeCuentasAction () {
    }

    public void run () throws ServletException, InformationException {
        
        String next = "/jsp/finanzas/contab/plan_de_cuentas/eliminar/EliminarPlanDeCuentas.jsp";
        
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String distrito = (String) session.getAttribute("Distrito");
        String creation_user = usuario.getLogin().toUpperCase();
        
        String msg = "";
        
        // distrito
        String cuenta = (request.getParameter("cuenta") != null)?request.getParameter("cuenta").toUpperCase():"";
        
        String listar = (request.getParameter("listar") != null)?request.getParameter("listar"):"";
        
        try{
            
            PlanDeCuentas cuentas = new PlanDeCuentas();
            
            cuentas.setDstrct( distrito );
            cuentas.setCuenta( cuenta );
            
            if ( listar.equals("Other") ){
                
                cuenta = "";
                model.planDeCuentasService.searchDetalleCuenta( distrito, cuenta );
                model.planDeCuentasService.getVec_cuentas();
                next = "/jsp/finanzas/contab/plan_de_cuentas/eliminar/BuscarParaEliminarPlanDeCuentas.jsp";
                
            }
            
            if ( listar.equals("True") ){
                
                model.planDeCuentasService.searchDetalleCuenta( distrito, cuenta );
                next = "/jsp/finanzas/contab/plan_de_cuentas/eliminar/BuscarParaEliminarPlanDeCuentas.jsp";
                
            }
            
            if ( listar.equals("False") ){
             
                if ( ( distrito != "" ) && ( cuenta != "" ) ){

                    model.planDeCuentasService.searchCuenta( distrito, cuenta );
                    next = "/jsp/finanzas/contab/plan_de_cuentas/eliminar/EliminarPlanDeCuentas.jsp?e=no";

                } else {

                    next = next + "?msg=Por favor no deje campos vacios!!";

                }
                
            }
            
            session.setAttribute ( "distrito", distrito );
            session.setAttribute ( "cuenta", cuenta );
            
        } catch ( Exception e ){
                        
            throw new ServletException( e.getMessage () );
            
        }
        
        this.dispatchRequest( next );
        
    }
    
}