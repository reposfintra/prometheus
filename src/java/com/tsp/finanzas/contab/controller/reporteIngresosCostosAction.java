/*
 * reporteIngresosCostosAction.java
 *
 * Created on 29 de junio de 2006, 08:39 AM
 */
/********************************************************************
 *  Nombre Clase.................   reporteIngresosCostosAction.java
 *  Descripci�n..................   Accion que permite genera la validacion de comprobantes
 *  Autor........................   David Pi�a L�pez
 *  Fecha........................   29 de junio de 2006, 08:39 AM
 *  Versi�n......................   1.0
 *  Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/
package com.tsp.finanzas.contab.controller;

import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.finanzas.contab.model.beans.*;
import com.tsp.finanzas.contab.model.*;
import com.tsp.operation.model.beans.*;
import com.tsp.finanzas.contab.model.threads.*;
/**
 *
 * @author  David
 */
public class reporteIngresosCostosAction extends Action {
    
    /** Creates a new instance of reporteIngresosCostosAction */
    public reporteIngresosCostosAction() {
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        String next = "";
        String fechaInicial = "";
        String fechaFinal = "";
        String periodo = "";
        String user="";
        boolean todas;
        try{
            String mensaje="Generaci�n de Reporte de Ingresos y Costos Iniciado";
            
            HttpSession session = request.getSession ();
            Usuario usuario = (Usuario)session.getAttribute ("Usuario");
            user = usuario.getLogin();
            
            fechaInicial = request.getParameter("fechai");
            fechaFinal = request.getParameter("fechaf");
            periodo = request.getParameter("anio") + request.getParameter("mes");
            
            request.setAttribute("mensaje", mensaje);
            
            ReporteIngresosCostos reporte = new ReporteIngresosCostos();
            reporte.start(periodo, fechaInicial, fechaFinal, usuario);
            next = "/jsp/finanzas/contab/ReporteIngresosCostos/reporteIngresosCostos.jsp";
        }catch (Exception e){
            e.printStackTrace();
            throw new ServletException(e.getMessage());
        }
        // Redireccionar a la p�gina indicada.
        this.dispatchRequest(next);
    }
    
}
