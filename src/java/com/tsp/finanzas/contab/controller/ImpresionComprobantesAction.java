/**
 * Nombre        ImpresionComprobantesAction.java
 * Descripci�n   Impresion de Comprobantes Contables
 * Autor         Mario Fontalvo Solano
 * Fecha         04 de Julio de 2006, 11:56 PM
 * Version       1.0
 * Coyright      Transportes S�nchez Polo S.A.
 **/
package com.tsp.finanzas.contab.controller;


import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.finanzas.contab.model.threads.HImpresionComprobantePDF;
import com.tsp.operation.model.beans.Usuario;

/**
 *
 * @author  Mario
 */
public class ImpresionComprobantesAction extends Action {
    
    /** Creates a new instance of ImpresionComprobantesAction */
    public ImpresionComprobantesAction() {
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        try{
            HttpSession session = request.getSession();
            Usuario usuario = (Usuario) session.getAttribute("Usuario");
            String next = "/jsp/finanzas/contab/impresion/FormularioImpresion.jsp";
            String tipodoc        = defaultString("tipodoc","");
            String numdoc         = defaultString("numdoc","");
            String numtransaccion = defaultString("numtransaccion","");
            String ano            = defaultString("Ano","");
            String mes            = defaultString("Mes","");
            String periodo        = (ano.equals("")?"":ano+mes);
            String opcion         = defaultString("Opcion","");
            
            if (opcion.equals("Imprimir")){
                HImpresionComprobantePDF h = new HImpresionComprobantePDF();
                h.start(tipodoc, numdoc, numtransaccion, periodo, model, usuario);
                request.setAttribute("msg", "Proceso iniciado con exito.");
            }
            this.dispatchRequest(next);
            
        }catch (Exception ex){
            ex.printStackTrace();
            throw new javax.servlet.ServletException(ex.getMessage());
        }
    }
    
    /**
     * Funcion para obtener un parametro del objeto request
     * y en caso de no existri este devuelve el segundo parametro
     * @autor mfontalvo
     * @param name, nombre del parametro
     * @param opcion, valor opcional a devolver en caso de que nom exista
     * @return Parametro del request
     */
    private String defaultString(String name, String opcion){
        return (request.getParameter(name)==null?opcion:request.getParameter(name));
    }
    
}
