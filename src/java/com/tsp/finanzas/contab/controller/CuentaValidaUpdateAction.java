/********************************************************************
 *      Nombre Clase.................   CuentaValidaUpdateAction.java
 *      Descripci�n..................   Modifica y Anula Cuentas Validas
 *      Autor........................   Ing. Leonardo Parody
 *      Fecha........................   19.01.2006
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.finanzas.contab.controller;

import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.finanzas.contab.model.beans.CuentaValida;
import com.tsp.finanzas.contab.model.*;
import com.tsp.operation.model.threads.*;
import org.apache.log4j.Logger;
/**
 *
 * @author  EQUIPO12
 */
public class CuentaValidaUpdateAction extends Action{
    
    /** Creates a new instance of CuentaValidaUpdateAction */
    public CuentaValidaUpdateAction () {
    }
    
    public void run () throws ServletException, InformationException {
        String next = "";
        HttpSession session = request.getSession ();
        Usuario usuario = (Usuario) session.getAttribute ("Usuario");
        String user = usuario.getLogin ();
        String base = usuario.getBase ();
        String dstrct = usuario.getDstrct ();
        String clase = request.getParameter ("clase1");
        String agencia = request.getParameter ("agencia1");
        String unidad = request.getParameter ("unidad1");
        String cliente_area = request.getParameter ("cliente_area2");
        String elemento = request.getParameter ("elemento1");
        String cuentaequivalente = request.getParameter("cuentaequivalente1");
        String clase1 = request.getParameter ("clase");
        String agencia1 = request.getParameter ("agencia");
        String unidad1 = request.getParameter ("unidad");
        String cliente_area1 = request.getParameter ("cliente_area");
        String cuentaequivalente1 = request.getParameter("cuentaequivalente");
        //////System.out.println("cuentaequivalente = "+cuentaequivalente+"  cuentaequivalente1 = "+cuentaequivalente1);
        int cliar1 = Integer.parseInt(cliente_area1);
        if (cliente_area1.length()<4){
                if ((cliar1<=900)){
                    cliente_area1 = "000"+cliente_area1;
                    ////////System.out.println("cliente area 1 primero = "+cliente_area1);
                }
        }
        String elemento1 = request.getParameter ("elemento");
        String sw = request.getParameter ("sw");
        CuentaValida cuenta = new CuentaValida ();
        cuenta.setClase (clase);
        cuenta.setAgencia (agencia);
        cuenta.setUnidad (unidad);
        if (cliente_area!=null){
                if(cliente_area.length()==6){
                        cliente_area = cliente_area.substring(3,6);
                }
        }
        cuenta.setCliente_area(cliente_area);
        cuenta.setElemento (elemento);
        cuenta.setDstrct (dstrct);
        cuenta.setCreation_user (user);
        cuenta.setBase (base);
        cuenta.setUser_update (user);
        cuenta.setCuentaEquivalente(cuentaequivalente);
        try {
            if (sw.equals ("Mostrar")){
                //////System.out.println("Mostrar cuentaequivalente = "+cuentaequivalente+"  cuentaequivalente1 = "+cuentaequivalente1);
                next = "/jsp/finanzas/Cuentas_Validas/CuentasValidasUpdate.jsp?opcion=32&clase="+clase1+"&agencia="+agencia1+"&unidad="+unidad1+"&elemento="+elemento1+"&cliente_area="+cliente_area1+"&cuentaequivalente="+cuentaequivalente1+"&msg=";
            }else if (sw.equals ("Modificar")){
                boolean existe=false;
                CuentaValida CV = new CuentaValida ();
                existe = model.cuentavalidaservice.ExisteCuentaValida (clase, agencia, unidad, cliente_area, elemento);
                //////System.out.println("Condicional cuentaequivalente = "+cuentaequivalente+"  cuentaequivalente1 = "+cuentaequivalente1);
                //////System.out.println(agencia.equals(agencia1)&&clase.equals(clase1)&&unidad.equals(unidad1)&&cliente_area.equals(cliente_area1)&&elemento.equals(elemento1)&&cuentaequivalente.equals(cuentaequivalente1));
                //////System.out.println(agencia.equals(agencia1)&&clase.equals(clase1)&&unidad.equals(unidad1)&&cliente_area.equals(cliente_area1)&&elemento.equals(elemento1)&&!cuentaequivalente.equals(cuentaequivalente1));
                //////System.out.println("agencia = "+agencia+" agencia1 = "+agencia1+" clase = "+clase+" clase1 = "+clase1+" unidad = "+unidad+" unidad1 = "+unidad1+" elemento = "+elemento+" elemento1 = "+elemento1+" cliente_area = "+cliente_area+" cliente_area1 = "+cliente_area1+" cuentaequivalente = "+cuentaequivalente+" cuentaequivalente1 = "+cuentaequivalente1);
                if (cliente_area1.length()==6){
                        cliente_area1 = cliente_area1.substring(3,6);
                }
                if(agencia.equals(agencia1)&&clase.equals(clase1)&&unidad.equals(unidad1)&&cliente_area.equals(cliente_area1)&&elemento.equals(elemento1)&&cuentaequivalente.equals(cuentaequivalente1)){ 
                    next = "/jsp/finanzas/Cuentas_Validas/MensajesCuentasValidas.jsp?msg=Cuenta Valida Modificada Satisfactoriamente";
                }else if (agencia.equals(agencia1)&&clase.equals(clase1)&&unidad.equals(unidad1)&&cliente_area.equals(cliente_area1)&&elemento.equals(elemento1)&&!cuentaequivalente.equals(cuentaequivalente1)) {
                    model.cuentavalidaservice.UpdateCuentaValida (cuenta, clase1, agencia1, unidad1, cliente_area1, elemento1);
                    next = "/jsp/finanzas/Cuentas_Validas/MensajesCuentasValidas.jsp?reload=ok&msg=Cuenta Valida Modificada Satisfactoriamente";            
                }else if (existe==true){
                    List cuentas = model.cuentavalidaservice.ListCuentaValida(clase, agencia, cliente_area,  elemento, unidad);
                    for (int i = 0; i < cuentas.size(); i++){
                          CV = (CuentaValida)cuentas.get(i);
                    }
                    if (CV.getReg_status ().equals ("")){
                        next = "/jsp/finanzas/Cuentas_Validas/MensajesCuentasValidas.jsp?msg=Esa cuenta Valida ya Existe";
                    }
                }else{
                    ////////System.out.println("cliente area 1 segundo = "+cliente_area1);
                    if (cliente_area1.length()==6){
                        cliente_area1 = cliente_area1.substring(3,6);
                    }
                    model.cuentavalidaservice.UpdateCuentaValida (cuenta, clase1, agencia1, unidad1, cliente_area1, elemento1);
                    next = "/jsp/finanzas/Cuentas_Validas/MensajesCuentasValidas.jsp?reload=ok&msg=Cuenta Valida Modificada Satisfactoriamente";
                }
            }else{
                if (cliente_area1.length()==6){
                        cliente_area1 = cliente_area1.substring(3,6);
                }
                model.cuentavalidaservice.EliminarCuentaValida (clase, agencia, unidad, cliente_area1, elemento);
                next = "/jsp/finanzas/Cuentas_Validas/MensajesCuentasValidas.jsp?reload=ok&msg=Cuenta Eliminada Satisfactoriamente";
            }
            
        }catch(SQLException e){
            throw new ServletException (e.getMessage ());
        }
        this.dispatchRequest (next);
    }
}
