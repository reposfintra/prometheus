/********************************************************************
 *      Nombre Clase.................   ConsultaComprobantesAction.java
 *      Descripci�n..................   Manje la generaci�n del reporet de ventas diario.
 *      Autor........................   Ing. Andr�s Maturana De La Cruz
 *      Fecha........................   21 de junio de 2006, 02:52 PM
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes S�nchez Polo S.A.
 *******************************************************************/

package com.tsp.finanzas.contab.controller;

import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.finanzas.contab.model.beans.*;
import com.tsp.finanzas.contab.model.threads.*;

import org.apache.log4j.*;

/**
 *
 * @author  Ing. Andr�s Maturana De La Cruz
 */
public class ConsultaComprobantesAction extends Action implements Serializable{
    
    Logger logger = Logger.getLogger (this.getClass());
    
    /** Crea una nueva instancia de  RepVentasDiarioPDFAction */
    public ConsultaComprobantesAction() {
    }
    
    @Override
    public void run() throws ServletException, InformationException  {
        try{
            
            String next = "/jsp/finanzas/contab/reportes/ReporteComprobantesRpta.jsp";
            HttpSession session = request.getSession();
            Usuario user = (Usuario) session.getAttribute("Usuario");
            
            String command = request.getParameter("command");
            
            logger.info("ACCION: " + command );
            
            String dstrct = (String) session.getAttribute("Distrito");
            
            if( command.equals("search") ) {
                
                String fecha_ini = request.getParameter("FechaI");
                String fecha_fin = request.getParameter("FechaF");
                
                logger.info("Fecha Inicial: " + fecha_ini);
                logger.info("Fecha Final: " + fecha_fin);
                
                model.comprobanteService.setFecha_ini(fecha_ini);
                model.comprobanteService.setFecha_fin(fecha_fin);
                
                model.comprobanteService.consultaComprobantes(dstrct, fecha_ini, fecha_fin);
                Vector comprobantes = model.comprobanteService.getVector();
                
                logger.info("Resultados encontrados: " + comprobantes.size());
                
                request.setAttribute("fecha_ini", fecha_ini);
                request.setAttribute("fecha_fin", fecha_fin);
                request.setAttribute("info", comprobantes);
            
            } else if ( command.equals("excel") ) { 
                
                logger.info("... Se inicio la generaci�n del Excel");
                
                next = "/jsp/cxpagar/reportes/AnalisisVencMsg.jsp";
                RepComprobantesTh hilo = new RepComprobantesTh();
                
                hilo.start((com.tsp.operation.model.Model) session.getAttribute("model"), model, user, model.comprobanteService.getFecha_ini(), model.comprobanteService.getFecha_fin(), dstrct);
                
            } else {
                
                logger.info("Detalle del comprobante...");
                
                String GrupTrans = request.getParameter("grupo");
                String tipodoc = request.getParameter("tipodoc");
                String numdoc  = request.getParameter("numdoc").replaceAll("-_-", "#");
                
                logger.info( "Grupo: " + GrupTrans );
                logger.info( "Tipodoc: " + tipodoc );
                logger.info( "Numdoc: " + numdoc );
                
                int grupo = Integer.parseInt(GrupTrans);
                
                logger.info(" grupo: " + grupo );
                
                ComprobanteFacturas comprobante = model.GrabarComprobanteSVC.ComprobanteSearch(dstrct, tipodoc, numdoc, grupo);
                List item = comprobante.getItems();
                for( int i=0; i< item.size();i++ ){
                    ComprobanteFacturas comprodet = (ComprobanteFacturas) item.get(i);
                    
                    model.subledgerService.buscarCodigosCuenta( comprodet.getCuenta() );
                    Vector tipSubledger = model.subledgerService.getCodigos();
                    comprodet.setCodSubledger( tipSubledger );
                }
                
                session.setAttribute( "periodo",comprobante.getPeriodo() );
                model.GrabarComprobanteSVC.buscarTipoDoc( user.getDstrct() );
                model.GrabarComprobanteSVC.setComprobante( comprobante );
                next = "/jsp/finanzas/contab/adminComprobantes/verComprobante.jsp?maxfila="+comprobante.getTotal_items()+"&opcion=modificar";
            }
            
            System.gc();
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if(rd == null)
                throw new ServletException("No se pudo encontrar "+ next);
            rd.forward(request, response);
            
        }catch(Exception e){
            e.printStackTrace();
            throw new ServletException("Error en ConsulltaComprobantesAction .....\n"+e.getMessage());
        }
    }
    
}
