/*
 * DescontabilizarDocumentoAction.java
 *
 * Created on 14 de Noviembre de 2006, 10:12 AM
 */
/*********************************************************************************
 * Nombre clase :      DescontabilizarDocumentoAction.java                       *
 * Descripcion :       Interfaz para descontabilizar documentos                  *
 * Autor :             Karen Reales                                              *
 * Fecha :             14 de Noviembre de 2006, 10:12 AM                         *
 * Version :           1.0                                                       *
 * Copyright :         Fintravalores S.A.                                   *
 *********************************************************************************/
package com.tsp.finanzas.contab.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.finanzas.contab.model.beans.*;
import com.tsp.finanzas.contab.model.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;
import com.tsp.util.Util;
import com.tsp.finanzas.contab.model.threads.*;

/**
 *
 * @author  kreales
 */
public class DescontabilizarDocumentoAction extends Action {
    
    /** Creates a new instance of AdminComprobantesManagerAction */
    public DescontabilizarDocumentoAction() {
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        String next = "/jsp/finanzas/contab/adminComprobantes/Descontabilizar.jsp";
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        try{
            String opc = request.getParameter( "opc" );
            if(opc.equalsIgnoreCase( "buscar" )){
                String tipodoc = request.getParameter( "tipodoc" );
                String numdoc = request.getParameter( "numdoc" ).toUpperCase();
                
                if(tipodoc.equals("PLA")){
                    model.comprobanteService.buscarPlanillaDescontabilizar(numdoc);
                    if(model.comprobanteService.getComprobante()==null){
                        next = "/jsp/finanzas/contab/adminComprobantes/Descontabilizar.jsp?mensaje=No se encontraron datos para el documento "+numdoc;
                    }
                    
                }
                else{
                    next = "/jsp/finanzas/contab/adminComprobantes/Descontabilizar.jsp?mensaje=Solo puede descontabilizar planillas en este momento.";
                }
            }else if(opc.equalsIgnoreCase( "accion" )){
                
                Date d = new Date();
                SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");
                String fecha = s.format(d);
                String valorRequest = request.getParameter("flete");
                String valorVec[] = valorRequest.split("/");
                double valorFlete = 0;
                double cantidad = 0;
                String monedaCia ="PES";
                try{
                    
                    valorFlete = Double.parseDouble(valorVec[0]);
                    
                }catch(NumberFormatException ne){
                    valorFlete=0;
                }
                try{
                    cantidad = Double.parseDouble(request.getParameter("pesoreal"));
                }catch(NumberFormatException ne){
                    cantidad=0;
                }
                
                String monedaFlete = valorVec.length>1?valorVec[1]:"";
                
                if(valorFlete>0 && !monedaFlete.equals("") && cantidad>0 ){
                    Comprobantes con =model.comprobanteService.getComprobante();
                    
                    if(con!=null){
                        next = "/jsp/finanzas/contab/adminComprobantes/Descontabilizar.jsp?mensaje=Se descontabilizo con exito la planilla" +con.getNumdoc();
                        
                        double nuevoValorFor =com.tsp.util.Util.redondear(cantidad * valorFlete,0);
                        double nuevoValorLocal=com.tsp.util.Util.redondear(cantidad * valorFlete,0);
                        
                        double valorFor = 0;
                        //SI LA MONEDA DEL FLETE SELECCIONADO ES DISTINTA A PESOS
                        //SE BUSCA LA TASA.
                        int sw=0;
                        if(!monedaCia.equals(monedaFlete)){
                            com.tsp.operation.model.Model modelo= new  com.tsp.operation.model.Model(usuario.getBd());
                            
                            try{
                                
                                modelo.tasaService.buscarValorTasa(monedaCia,monedaFlete,monedaCia,fecha);
                                
                            }catch(Exception et){
                                throw new ServletException(et.getMessage());
                            }
                            
                            com.tsp.operation.model.beans.Tasa tasa = modelo.tasaService.obtenerTasa();
                            if(tasa!=null){
                                valorFor= tasa.getValor_tasa()*valorFlete;
                                nuevoValorLocal = com.tsp.util.Util.redondear(valorFor* cantidad,0);
                            }
                            else{
                                sw=1;
                            }
                            
                        }
                        
                        if(sw==0){
                            
                            con.setCantidad(cantidad);
                            con.setValor(nuevoValorLocal);
                            con.setValor_for(nuevoValorFor);
                            con.setValor_unit(valorFlete);
                            con.setMoneda(monedaFlete);
                            model.comprobanteService.setComprobante(con);
                            model.comprobanteService.descontabilizarPlanilla();
                            model.comprobanteService.setComprobante(null);
                        }
                        else{
                            next = "/jsp/finanzas/contab/adminComprobantes/Descontabilizar.jsp?mensaje=ERROR:No se encontro tasa de cambio para la fecha "+fecha;
                            
                        }
                    }
                    
                }
                else{
                    next = "/jsp/finanzas/contab/adminComprobantes/Descontabilizar.jsp?mensaje=ERROR: No se pudo recalcular el valor de la planilla, no existe valor" +
                    " de flete o moneda.";
                    
                }
                
            }
        }catch(Exception e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
