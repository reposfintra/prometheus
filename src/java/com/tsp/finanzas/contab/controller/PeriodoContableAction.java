/*******************************************************************
 *      Nombre Clase.................   PeriodoContableAction.java
 *      Descripci�n..................   Action para periodo contable
 *      Autor........................   Osvaldo P�rez Ferrer
 *      Fecha........................   20 de Junio de 2006
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/


/*
 * PeriodoContableAction.java
 *
 * Created on 20 de junio de 2006, 08:20 AM
 */

package com.tsp.finanzas.contab.controller;

import com.tsp.exceptions.*;
import com.tsp.operation.model.beans.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.finanzas.contab.model.beans.PeriodoContable.*;
import com.tsp.finanzas.contab.model.DAO.*;
import com.tsp.finanzas.contab.model.*;
import com.tsp.finanzas.contab.model.beans.*;
import com.tsp.util.Util;

/**
 *
 * @author  Osvaldo
 */
public class PeriodoContableAction extends Action{
    
    /** Creates a new instance of PeriodoContableAction */
    public PeriodoContableAction() {
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        
        String next = "";
        String mensaje = "";
        PeriodoContable p;
        PeriodoContable test;
        
        try{
            HttpSession session = request.getSession();
            Usuario u = (Usuario)session.getAttribute("Usuario");
            String opc = request.getParameter("opcion");
            
             String anio = "";
             String mes = "";
            p = new PeriodoContable();
            
            if(opc.equals("add")){
                next = "/jsp/finanzas/contab/PeriodoContable/periodoContableInsert.jsp";
                
                //Creando periodo_contable
                                                
                anio = request.getParameter("anio");
                mes = request.getParameter("mes");
                String abierto = request.getParameter("abierto");
                String iva = request.getParameter("iva");
                String fpiva = (request.getParameter("iva").equals("S") )?request.getParameter("fpiva") : "0099-01-01";
                String retencion = request.getParameter("retencion");
                String fpret = (request.getParameter("retencion").equals("S") )?request.getParameter("fpret") : "0099-01-01";
                String comercio =request.getParameter("comercio");
                String fpcom = (request.getParameter("comercio").equals("S") )?request.getParameter("fpcom") : "0099-01-01";
                String renta = request.getParameter("renta");
                String fpren = (request.getParameter("renta").equals("S"))?request.getParameter("fpren") : "0099-01-01";
                
                p.setAnio(anio);
                p.setMes(mes);
                p.setAc(abierto);
                p.setIva(iva);
                p.setFec_pre_iva(fpiva);
                p.setRetencion(retencion);
                p.setFec_pre_retencion(fpret);
                p.setComercio(comercio);
                p.setFec_pre_comercio(fpcom);
                p.setRenta(renta);
                p.setFec_pre_renta(fpren);
                
                p.setDistrito(u.getDstrct());
                p.setCreation_user(u.getLogin());
                p.setBase(u.getBase());
                
                //verificar si existe
                test=model.periodoContableService.obtenerTodosPeriodo(u.getDstrct(), anio, mes);
                
                
                if(test != null){
                    if(test.getReg_status().equals("A")){
                        p.setUser_update(u.getLogin());
                        model.periodoContableService.updatePeriodoContable(p);
                        mensaje = "Periodo creado exitosamente";
                    }
                    else{
                        mensaje = "Ya existe el periodo contable "+anio+" "+mes+" "+u.getDstrct();
                    }
                }else{
                    model.periodoContableService.insertPeriodoContable(p);
                    mensaje = "Periodo creado exitosamente";
                }
                
                request.setAttribute("mensaje", mensaje);
            }
            else if(opc.equals("list")){
                next = "/jsp/finanzas/contab/PeriodoContable/periodoContableListar.jsp";
                anio = (request.getParameter("anio")!=null)? request.getParameter("anio") :
                       Util.getFechaActual_String(1);                 
                Vector periodos = model.periodoContableService.getAllPeriodos(anio);
                request.setAttribute("periodos" , periodos);
                request.setAttribute("anio" , anio);
            }
            
            else if (opc.equals("update")){
                next = "/jsp/finanzas/contab/PeriodoContable/periodoContableModificar.jsp";
                anio = request.getParameter("anio");
                mes = request.getParameter("mes");
                model.periodoContableService.obtenerPeriodo(u.getDstrct(), anio, mes);
            }
            else if (opc.equals("save")){
                next = "/jsp/finanzas/contab/PeriodoContable/periodoContableModificar.jsp";
                anio = request.getParameter("anio");
                mes = request.getParameter("mes");
                String abierto = request.getParameter("abierto");
                String iva = request.getParameter("iva");
                String fpiva = (request.getParameter("iva").equals("S") )?request.getParameter("fpiva") : "0099-01-01";
                String retencion = request.getParameter("retencion");
                String fpret = (request.getParameter("retencion").equals("S") )?request.getParameter("fpret") : "0099-01-01";
                String comercio =request.getParameter("comercio");
                String fpcom = (request.getParameter("comercio").equals("S") )?request.getParameter("fpcom") : "0099-01-01";
                String renta = request.getParameter("renta");
                String fpren = (request.getParameter("renta").equals("S"))?request.getParameter("fpren") : "0099-01-01";
                
                p=model.periodoContableService.getP();
                model.periodoContableService.obtenerPeriodo(u.getDstrct(), anio, mes);
                
                p.setAc(abierto);
                p.setIva(iva);
                p.setFec_pre_iva(fpiva);
                p.setRetencion(retencion);
                p.setFec_pre_retencion(fpret);
                p.setComercio(comercio);
                p.setFec_pre_comercio(fpcom);
                p.setRenta(renta);
                p.setFec_pre_renta(fpren);
                                
                p.setUser_update(u.getLogin());                                                
                
                model.periodoContableService.updatePeriodoContable(p);
                model.periodoContableService.setP(p);
                request.setAttribute("mensaje", "Periodo actualizado exitosamente");
            }
            else if (opc.equals("delete")){
                next = "/jsp/finanzas/contab/PeriodoContable/periodoContableModificar.jsp";
                anio = request.getParameter("anio");
                mes = request.getParameter("mes");
                model.periodoContableService.deletePeriodoContable(u.getDstrct(), anio, mes);
                request.setAttribute("mensaje", "Periodo "+anio+" "+mes+" Anulado");
                request.setAttribute("anulado", "");
            }
        }catch(Exception ex){
            throw new ServletException(ex.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
