/********************************************************************
 *      Nombre Clase.................   TipoDoctoInsertAction.java
 *      Descripci�n..................   Verifica la informaci�n del tipo de comprobante 
 *                                      para realizar la inserci�n en el archivo.
 *      Autor........................   Ing. Andr�s Maturana De La Cruz
 *      Fecha........................   8 de junio de 2006
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes S�nchez Polo S.A.
 *******************************************************************/

package com.tsp.finanzas.contab.controller;
import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.finanzas.contab.model.*;
import com.tsp.finanzas.contab.model.beans.*;


import org.apache.log4j.Logger;

public class TipoDoctoInsertAction extends Action{
    
    /** Creates a new instance of CuentasInsertAction */
    public TipoDoctoInsertAction() {
    }
    
    public void run () throws ServletException, InformationException {
        Logger log = Logger.getLogger(this.getClass());
        
        String next = "/jsp/finanzas/contab/tipo_docto/TipoDoctoInsert.jsp";
        
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        
        try{
            Tipo_Docto docto = new Tipo_Docto();
            docto.setCodigo(request.getParameter("code"));
            docto.setBase(usuario.getBase());
            docto.setCodigo_interno(request.getParameter("cod_interno"));
            docto.setDescripcion(request.getParameter("desc"));
            docto.setDistrito(usuario.getDstrct());
            docto.setManeja_serie(request.getParameter("mserie"));
            docto.setPrefijo(request.getParameter("prefijo"));
            docto.setPrefijo_anio(request.getParameter("preano"));
            docto.setPrefijo_mes(request.getParameter("premes"));
            docto.setEsDiario(request.getParameter("diario"));
            
            if( docto.getManeja_serie().compareTo("S")==0 ){
                docto.setSerie_fin(request.getParameter("serie_fin"));
                docto.setSerie_ini(request.getParameter("serie_ini"));
                docto.setLong_serie(request.getParameter("long_serie"));
            } else {
                docto.setSerie_fin("0");
                docto.setSerie_ini("0");
                docto.setLong_serie("0");
            }
            
            docto.setUsuario(usuario.getLogin());
            docto.setTercero(request.getParameter("tercero"));
            
            model.tipo_doctoSvc.obtener(usuario.getDstrct(), docto.getCodigo());
            Tipo_Docto aux = model.tipo_doctoSvc.getDocto();            
            
            if( docto.getEsDiario().compareTo("S")==0 ||
                    ( docto.getEsDiario().compareTo("N")==0 && 
                            !model.tipo_doctoSvc.isCodigoInternoUsado(docto.getDistrito(), docto.getCodigo_interno())) ) {
                if( aux!=null ){
                    if( aux.getReg_status().compareTo("A")==0 ){
                        docto.setReg_status("");
                        model.tipo_doctoSvc.setDocto(docto);
                        model.tipo_doctoSvc.actualizar();
                        docto = new Tipo_Docto(docto.getCodigo_interno());
                        next += "?msg=Se ha ingresado el tipo de comprobante y serie exitosamente.";
                    } else {
                        next += "?msg=No se puede ingresar la informaci�n. El tipo de comprobante y serie ya existe.";
                    }
                } else {
                    model.tipo_doctoSvc.setDocto(docto);
                    model.tipo_doctoSvc.insertar();
                    
                    docto = new Tipo_Docto(docto.getCodigo_interno());
                    next += "?msg=Se ha ingresado el tipo de comprobante y serie exitosamente.";
                }
            } else {
                next += "?msg=No se puede procesar la informaci�n. El c�digo interno no se puede reautilizar en los comprobantes no diarios.";
            }
            
            request.setAttribute("TDocto", docto);
            
        }catch (SQLException e){
            e.printStackTrace ();
            throw new ServletException (e.getMessage ());
        }
        // Redireccionar a la p�gina indicada.
        this.dispatchRequest (next);
    }
    
}
