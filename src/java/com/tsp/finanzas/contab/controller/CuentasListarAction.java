/********************************************************************
 *  Nombre Clase.................   PrecintosAnularAction.java
 *  Descripci�n..................   Action de la tabla cuentas
 *  Autor........................   Ing. Leonardo Parody Ponce
 *  Fecha........................   27.12.2005
 *  Versi�n......................   1.0
 *  Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.finanzas.contab.controller;
import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.finanzas.contab.model.beans.*;
import com.tsp.finanzas.contab.model.*;
/**
 *
 * @author  EQUIPO12
 */
public class CuentasListarAction extends Action{
    
    /** Creates a new instance of CuentasListarAction */
    public CuentasListarAction () {
    }
    public void run () throws ServletException, InformationException {
        String next = "";
        List Cuentas;
        try{
            String iniciocuenta = request.getParameter ("iniciocuenta");
            request.setAttribute ("cuenta", iniciocuenta);
            Cuentas = model.cuentaService.ListarCuentas (iniciocuenta);
            request.setAttribute ("Cuentas", Cuentas);
            next = "/jsp/finanzas/contab/cuentas/CuentasListar.jsp";
        }catch (SQLException e){
            e.printStackTrace ();
            throw new ServletException (e.getMessage ());
        }
        // Redireccionar a la p�gina indicada.
        this.dispatchRequest (next);
    }
    
}
