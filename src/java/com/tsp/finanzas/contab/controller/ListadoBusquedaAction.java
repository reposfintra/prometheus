/********************************************************************
 *  Nombre Clase.................   MovAuxiliarAction.java
 *  Descripci�n..................   Action de la consulta de movimiento auxiliar
 *  Autor........................   Ing. FERNEL VILLACOB DIAZ
 *  Fecha........................   07/06/2006
 *  Versi�n......................   1.0
 *  Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/



package com.tsp.finanzas.contab.controller;



import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;


public class ListadoBusquedaAction extends Action{
    
   
    
    public void run() throws ServletException, com.tsp.exceptions.InformationException {
        
         try{
                    
            
                    String  next     = "/jsp/finanzas/contab/ListadoConsulta.jsp";
            
                    String  sql      = request.getParameter("sql");
                    String  tipo     = request.getParameter("tipo");
                    String  elemento = request.getParameter("elemento");
                    String  titulo   = request.getParameter("titulo");
                    
                                        
                    Vector lista     =  model.ListadoBusquedaSvc.getLista(sql,tipo);
                    
                    
                    request.setAttribute("listaBusquedaXXX123", lista);
                    next += "?elemento=" +elemento +"&titulo="+ titulo;
                
            
                   dispatchRequest( next );
            
                   
        } catch (Exception e){
             throw new ServletException(e.getMessage());
        }
    }
    
    
}
