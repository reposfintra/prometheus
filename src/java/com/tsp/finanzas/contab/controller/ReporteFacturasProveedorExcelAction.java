/******************************************************************************
 * Nombre clase :                 ReporteFacturasProveedorExcelAction.java    *
 * Descripcion :                  Clase que maneja los eventos                *
 *                                relacionados con el programa que busca el   *
 *                                reporte de facturas de proveedor en la BD.  *
 * Autor :                        LREALES                                     *
 * Fecha :                        27 de Junio de 2006, 08:23 AM               *
 * Version :                      1.0                                         *
 * Copyright :                    Fintravalores S.A.                     *
 *****************************************************************************/

package com.tsp.finanzas.contab.controller;

import java.util.Vector;
import java.lang.*;
import java.sql.*;
import javax.servlet.ServletException;
import com.tsp.exceptions.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.threads.*;
import com.tsp.finanzas.contab.model.*;
import com.tsp.finanzas.contab.model.beans.*;
import com.tsp.finanzas.contab.model.threads.*;
import com.tsp.finanzas.contab.model.services.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class ReporteFacturasProveedorExcelAction extends Action{
    
    /** Creates a new instance of ReporteFacturasProveedorExcelAction */
    public ReporteFacturasProveedorExcelAction() {
    }
    
    public void run() throws ServletException, InformationException {
        
        String next = "/jsp/finanzas/contab/reporte_facturas_proveedor/FiltroReporteFacturasProveedor.jsp";
        
        HttpSession session = request.getSession();
        Usuario usuario = ( Usuario ) session.getAttribute( "Usuario" );
            
        String fecha_inicial = ( request.getParameter("fecha_ini") != null )?request.getParameter("fecha_ini"):"";
        String fecha_final = ( request.getParameter("fecha_fin") != null )?request.getParameter("fecha_fin"):"";        
        String usuario_factura = ( request.getParameter("usuario_fac") != null )?request.getParameter("usuario_fac").toUpperCase():"";
        try{
            
            model.reporteFacturasProveedorService.listaFacturasProveedor( fecha_inicial, fecha_final, usuario_factura );
            Vector datos = model.reporteFacturasProveedorService.getVec_reporte();  

            if ( datos.size() > 0 ){
                
                HiloReporteFacturasProveedor HRFP = new HiloReporteFacturasProveedor();
                HRFP.start( datos, usuario.getLogin(), fecha_inicial, fecha_final, usuario_factura ); 

                next = next + "?msg=Archivo exportado a excel!!";
            
            } else{
                
                next = next + "?msg=Su busqueda no arrojo resultados!";
                
            }            
            
        } catch ( Exception e ){
            
            throw new ServletException( "Error en Reporte de Facturas de Proveedor Excel Action : " + e.getMessage() );
            
        }        
        
        this.dispatchRequest( next );
        
    }
    
}