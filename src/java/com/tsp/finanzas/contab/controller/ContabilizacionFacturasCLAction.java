/********************************************************************
 *  Nombre Clase.................   ContabilizacionFacturasCLAction.java
 *  Descripci�n..................   Maneja eventos para la contabilizacion de facturas de clientes
 *  Autor........................   Ing. FERNEL VILLACOB DIAZ
 *  Fecha........................   09/06/2006
 *  Versi�n......................   1.0
 *  Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/



package com.tsp.finanzas.contab.controller;



import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.finanzas.contab.model.threads.HContabilizacionCL;
import com.tsp.util.*;



public class ContabilizacionFacturasCLAction extends Action{
    
   
    
    public void run() throws ServletException, com.tsp.exceptions.InformationException {
        
         try{
            
            
            HttpSession session   = request.getSession();
            com.tsp.util.Util.logController(((com.tsp.operation.model.beans.Usuario)session.getAttribute("Usuario")).getLogin(),this.getClass().getName());
            Usuario     usuario   = (Usuario) session.getAttribute("Usuario");       
        
            
            String  base    = "/jsp/finanzas/contab/contabilizacionFacturas/";
            String  next    = "ContabilizacionClientes.jsp";
            String  msj     = "";
            
            String  evento  =  request.getParameter("evento");
            
            
            if ( evento!=null){
                
                if(evento.equals("CONTABILIZAR")){
                    
                    
                    if( ! model.ContabilizacionFacturasCLSvc.isProcess() ){
                        model.ContabilizacionFacturasCLSvc.setProcess( true );
                        
                        HContabilizacionCL  hilo =  new HContabilizacionCL();
                        hilo.start(model, usuario );
                     
                        msj   = "Proceso de contabilizaci�n de Facturas CxP se ha iniciado...";
                    }  
                    else
                      msj   = "Actualmente el proceso se est� realizando, por favor intente mas tarde....";
                    
                    next += "?msj=" + msj;
                   
                  
                }
                
            }
            
            dispatchRequest( base + next );
            
        } catch (Exception e){
             throw new ServletException(e.getMessage());
        }
         
    }
    
}
