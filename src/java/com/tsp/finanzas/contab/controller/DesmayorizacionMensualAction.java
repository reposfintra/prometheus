/********************************************************************************
 * Nombre clase . . . . . .   ReporteRtfeAction.java                            *
 * Descripci�n  .. . . . . .  Saca la mayorizacion mensual                      *
 * Autor . . . . . . . . . .  Ing. Pablo Emilio Bassil Orozco                   *
 * Fecha . . . . . . . . . .  Created on 10 de Febrero de 2009                  *
 * Version . . . . . . . . .  1.0                                               *
 * Copyright ...TSP - TRANSPORTES SANCHEZ POLO S.A.                             *
 ********************************************************************************/

package com.tsp.finanzas.contab.controller;

import com.tsp.finanzas.contab.model.threads.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;

/**
 * Este es el action que activa el hilo que hace la consulta y que crea
 * el archivo de Excel.
 *
 * @author  Pablo Emilio Bassil Orozco
 */
public class DesmayorizacionMensualAction extends Action {

    String next = "";
    
    public DesmayorizacionMensualAction() {
    }

    public void run() throws ServletException, com.tsp.exceptions.InformationException {

        HttpSession session = request.getSession();        

        com.tsp.util.Util.logController(((com.tsp.operation.model.beans.Usuario) session.getAttribute("Usuario")).getLogin(), this.getClass().getName());
        Usuario u = (Usuario) session.getAttribute("Usuario");
                
        try{            
            next = "/jsp/finanzas/contab/mayorizacion2/Desmayorizacion.jsp?mensaje=El proceso se ha iniciado";

            HMayorizacionMensual hmm = new HMayorizacionMensual();
            hmm.setOp(1);
            hmm.init(request.getParameter("a�obox"), request.getParameter("mesbox"), u, this.model);
        }
        catch (Exception ex){
            throw new ServletException("Error en HeadCountAction[CONTROLLER] .....\n"+ex.getMessage());
        }
        this.dispatchRequest(next);
    }
}

