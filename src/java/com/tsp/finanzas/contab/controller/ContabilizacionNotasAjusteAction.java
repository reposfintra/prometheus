/*
 * ContabilizacionNotasAjusteAction.java
 *
 * Created on 24 de junio de 2008, 11:00
 */

package com.tsp.finanzas.contab.controller;

import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.util.Util;

import com.tsp.operation.model.beans.Usuario;
import com.tsp.finanzas.contab.model.threads.HContabilizacionNotasAjuste;
/**
 *
 * @author  navi
 */
public class ContabilizacionNotasAjusteAction extends Action{
    
    /** Creates a new instance of ContabilizacionNotasAjusteAction */
    public ContabilizacionNotasAjusteAction() {
        
        
    }   
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        try{
            HttpSession session = request.getSession();
            Usuario usuario = (Usuario) session.getAttribute("Usuario");            
            String next = "/jsp/finanzas/contab/contabilizacion/notasDajuste.jsp";
            
            
            String opcion  = Util.coalesce( (String) request.getParameter("evento") , "");
            
            if (opcion.equalsIgnoreCase("CONTABILIZAR")){
                if ( !model.ContabilizacionIngresosSvc.isProcessRun() ) {
                    HContabilizacionNotasAjuste h = new HContabilizacionNotasAjuste();
                    h.start(usuario,  model.ContabilizacionNotasAjusteSvc );    
                    
                    next += "?msj=Proceso de contabilizacion de Notas de Ajuste ha iniciado correctamente...";
                } else {
                    next += "?msj=Actualmente el proceso se est� realizando, por favor intente mas tarde....";
                }
            }
            
            this.dispatchRequest(next);
            
        } catch (Exception ex ){
            System.out.println("error en contab de nostas de ajuste "+ex.toString()+"__"+ex.getMessage());
            ex.printStackTrace();
            throw new ServletException (ex.getMessage());
        }
    }
    
}

