/******
 * Nombre:        SSubledgerCargarAction.java
 * Descripci�n:   Clase Action para buscar los tipod de cuentas subledger
 * Autor:         Ing. Diogenes Antonio Bastidas Morales
 * Fecha:         6 de junio de 2006, 09:42 PM
 * Versi�n        1.0
 * Coyright:      Transportes Sanchez Polo S.A.
 *******/


package com.tsp.finanzas.contab.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.finanzas.contab.model.beans.*;
import com.tsp.operation.model.beans.*;
import com.tsp.finanzas.contab.model.*;
import com.tsp.exceptions.*;
import com.tsp.util.*;

public class SubledgerCargarAction extends Action {
    
    /** Creates a new instance of SubledgerCargarAction */
    public SubledgerCargarAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next = "/" + request.getParameter("carpeta")+ "/" + request.getParameter("pagina");
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        try{
            model.subledgerService.buscarCuentasTipoSubledger(request.getParameter("cuenta").toUpperCase());
            
        }catch (Exception e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
