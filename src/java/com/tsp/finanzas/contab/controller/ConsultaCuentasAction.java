/******************************************************************************
 * Nombre clase :                   ConsultaCuentasAction.java                *
 * Descripcion :                    Clase que maneja los eventos              *
 *                                  relacionados con el programa de           *
 *                                  Consultar una Cuenta en la BD.            *
 * Autor :                          JDELAROSA                                 *
 * Fecha :                          18 de enero de 2007, 05:36 PM             *
 * Version :                        1.0                                       *
 * Copyright :                      Fintravalores S.A.                   *
 *****************************************************************************/


package com.tsp.finanzas.contab.controller;

import java.io.*;
import java.util.*;
import com.tsp.exceptions.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.util.Util;
import com.tsp.operation.model.beans.*;
import com.tsp.finanzas.contab.model.beans.*;
import com.tsp.finanzas.contab.model.services.*;
import com.tsp.finanzas.contab.model.threads.*;
import java.text.*;
import java.sql.SQLException;
import com.tsp.operation.model.*;
import org.apache.log4j.*;

public class ConsultaCuentasAction extends Action{
    
    Logger logger = Logger.getLogger(this.getClass());
    
    /** Creates a new instance of ConsultaCuentasAction */
    public ConsultaCuentasAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next = "/jsp/finanzas/contab/plan_de_cuentas/modificar/BuscarPlanDeCuentas.jsp";
        
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String distrito = (String) session.getAttribute("Distrito");
        
       
        String cuenta = (request.getParameter("cuenta") != null)?request.getParameter("cuenta").toUpperCase():"";
        String cuenta2 = (request.getParameter("cuenta2") != null)?request.getParameter("cuenta2").toUpperCase():"";//AMATURANA 30.03.2007
        String tipo = (request.getParameter("tipo") != null)?request.getParameter("tipo"):"";
        
        
        String Plan = (request.getParameter("Plan") != null)?request.getParameter("Plan"):"";
        String ICG = (request.getParameter("ICG") != null)?request.getParameter("ICG"):"";
        
        
        String Tipo = (request.getParameter("Tipo") != null)?request.getParameter("Tipo").toUpperCase():"%";
        String Agencia = (request.getParameter("Agencia") != null)?request.getParameter("Agencia").toUpperCase():"%";
        String Unidad = (request.getParameter("Unidad") != null)?request.getParameter("Unidad").toUpperCase():"%";
        String Cliente = (request.getParameter("Cliente") != null)?request.getParameter("Cliente").toUpperCase():"%";
        String Elemento = (request.getParameter("Elemento") != null)?request.getParameter("Elemento").toUpperCase():"%";
        String UnionCuenta = "";
        if(cuenta.equals("")){
            cuenta = "%";
        }
        if(Tipo.equals("")){
            Tipo = "%";
        }
        if(Agencia.equals("")){
            Agencia = "%";
        }
        if(Unidad.equals("")){
            Unidad = "%";
        }
        if(Cliente.equals("")){
            Cliente = "%";
        }
        if(Elemento.equals("")){
            Elemento = "%";
        }
        
        
        String op= "";
            
        try{
            if(Plan.equals("1")){
               System.out.println("entro cuentas"); 
               op = "1";
                boolean existeCuenta1 = model.planDeCuentasService.existCuenta(distrito, cuenta);
                boolean existeCuenta2 = model.planDeCuentasService.existCuenta(distrito, cuenta2);
                
                if( existeCuenta1 && existeCuenta2 ){
                    model.planDeCuentasService.consultaCuentaSinComodin( distrito, cuenta, cuenta2 ); 
                    if( tipo.equals("consulta") ){
                        next = "/jsp/finanzas/contab/plan_de_cuentas/modificar/ConsultaPlanDeCuentas.jsp?tipo="+tipo;
                    }
                    else{
                        next = "/jsp/finanzas/contab/plan_de_cuentas/modificar/ConsultaPlanDeCuentas.jsp?tipo="+tipo;
                        
                    }
                } else {
                    String msg = "";
                    if( !existeCuenta1 ) msg += "La Cuenta Inicial " + cuenta + " no registra.";
                    if( !existeCuenta2 ) msg += (msg.length()!=0 ? "<br>" : "") + "La Cuenta Final " + cuenta2 + " no registra.";
                    next = "/jsp/finanzas/contab/plan_de_cuentas/modificar/BuscarPlanDeCuentas.jsp?msg="+msg;
                }
                logger.info("?cuenta1: " + cuenta);
                logger.info("?cuenta2: " + cuenta2);
                logger.info("?existe cuenta 1: " + existeCuenta1);
                logger.info("?existe cuenta 2: " + existeCuenta2);
                logger.info("?search cuentas: " + ( existeCuenta1 && existeCuenta2 ));
                logger.info("?next: " + next);
            } 
            if(ICG.equals("1")){
                UnionCuenta = Tipo + Agencia +Unidad + Cliente + Elemento ;  
                System.out.println("Union de Cuenta"+UnionCuenta);
                model.planDeCuentasService.consultaCuenta( distrito, UnionCuenta,Tipo,Agencia,Unidad,Cliente,Elemento);
                if( tipo.equals("consulta") ){
                    next = "/jsp/finanzas/contab/plan_de_cuentas/modificar/ConsultaPlanDeCuentas.jsp?tipo="+tipo;
                }
                else{
                    next = "/jsp/finanzas/contab/plan_de_cuentas/modificar/ConsultaPlanDeCuentas.jsp?tipo="+tipo;
                    
                }
            }
            
        } catch ( Exception e ){
            throw new ServletException( e.getMessage () );
        }
        
        this.dispatchRequest( next );
    }
    
}
