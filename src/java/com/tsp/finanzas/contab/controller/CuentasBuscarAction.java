/********************************************************************
 *  Nombre Clase.................   PrecintosBuscarAction.java
 *  Descripci�n..................   Action de la tabla cuentas
 *  Autor........................   Ing. Leonardo Parody Ponce
 *  Fecha........................   27.12.2005
 *  Versi�n......................   1.0
 *  Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.finanzas.contab.controller;
import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.finanzas.contab.model.beans.*;
import com.tsp.finanzas.contab.model.*;
import com.tsp.operation.model.beans.*;
/**
 *
 * @author  EQUIPO12
 */
public class CuentasBuscarAction extends Action{
        
        /** Creates a new instance of CuentasBuscarAction */
        public CuentasBuscarAction() {
        }
        public void run() throws ServletException, InformationException {
                //////System.out.println("ESTOY EN EL ACTION");
                List precintos = new LinkedList();
                String next="";
                HttpSession session = request.getSession();
                Usuario usuario = (Usuario)session.getAttribute("Usuario");
                String base = usuario.getBase();
                String dstrct = usuario.getDstrct();
                String nombre = usuario.getNombre();
                String cuenta = request.getParameter("cuenta");
                try{   
                        if (model.cuentaService.ExisteCuentas(cuenta)==true){
                                model.cuentaService.ConsultarCuentas(cuenta);
                                Cuentas cuentas = new Cuentas();
                                cuentas = model.cuentaService.getCuenta();
                                request.setAttribute("cuenta", cuentas);
                                if (cuentas.getReg_status().equalsIgnoreCase("A")){
                                        next="/jsp/finanzas/contab/cuentas/CuentasBuscar.jsp?msg=Esta cuenta se encuentra Anulada"; 
                                }else{
                                        next="/jsp/finanzas/contab/cuentas/CuentasModificar.jsp?msg=";
                                }
                        }else{
                                next = "/jsp/finanzas/contab/cuentas/CuentasBuscar.jsp?msg=El codigo de cuenta no existe";
                        }
                }catch (SQLException e){
                        e.printStackTrace();
                        throw new ServletException(e.getMessage());
                }
                 // Redireccionar a la p�gina indicada.
                this.dispatchRequest(next);
        }
        
}
