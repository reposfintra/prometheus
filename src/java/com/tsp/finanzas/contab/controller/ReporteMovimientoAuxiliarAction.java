/********************************************************************
 *      Nombre Clase.................   ReporteMovimientoAuxiliarAction.java
 *      Descripcion..................   Manje la generacion del reporet de ventas diario.
 *      Autor........................   Ing. Andres Maturana De La Cruz
 *      Fecha........................   21 de junio de 2006, 02:52 PM
 *      Version......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.finanzas.contab.controller;

import com.tsp.util.*;
import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.finanzas.contab.model.*;
import com.tsp.finanzas.contab.model.beans.*;
import com.tsp.finanzas.contab.model.threads.HReporteMovimientoAuxiliar;




public class ReporteMovimientoAuxiliarAction extends Action implements Serializable{


    public void run() throws ServletException, InformationException  {
        try{
            String next = "/jsp/finanzas/contab/movauxiliar2/parametros.jsp";
            HttpSession session = request.getSession();
com.tsp.util.Util.logController(((com.tsp.operation.model.beans.Usuario)session.getAttribute("Usuario")).getLogin(),this.getClass().getName());
            Usuario user = (Usuario) session.getAttribute("Usuario");
            com.tsp.operation.model.Model modelOperation = (com.tsp.operation.model.Model) session.getAttribute("model");


            String distrito  = Util.coalesce(request.getParameter("distrito") , "");
            String cuenta_ri = Util.coalesce(request.getParameter("cuenta_ri"), "");
            String cuenta_rf = Util.coalesce(request.getParameter("cuenta_rf"), "");
            String cuenta    = Util.coalesce(request.getParameter("cuenta"), "");

            String fecha_ri  = Util.coalesce(request.getParameter("fecha_ri") , "");
            String fecha_rf  = Util.coalesce(request.getParameter("fecha_rf") , "");
            String Ano       = Util.coalesce(request.getParameter("Ano")  , "");
            String Mes       = Util.coalesce(request.getParameter("Mes")  , "");
            String tipo1     = Util.coalesce(request.getParameter("tipo1")     , "");
            String tipo2     = Util.coalesce(request.getParameter("tipo2")     , "");
            String opcion    = Util.coalesce(request.getParameter("opcion")   , "");

            String periodo = Ano+Mes;

            if (opcion.equalsIgnoreCase("REPORTE")){
                if (tipo1.equals("FECHAS")) periodo = "";
                if (tipo2.equals("RANGO"))  cuenta  = "";

                HReporteMovimientoAuxiliar h = new HReporteMovimientoAuxiliar();
                h.start(model.MovAuxiliarSvc, modelOperation.LogProcesosSvc, distrito, cuenta_ri, cuenta_rf, fecha_ri, fecha_rf, cuenta, periodo, user);
                request.setAttribute("msg", "Su reporte ha iniciado, verifique su estado en el log de proceso");
            }


            this.dispatchRequest(next);

        }catch(Exception e){
            e.printStackTrace();
            throw new ServletException("Error en ReporteMovimientoAuxiliarAction .....\n"+e.getMessage());
        }
    }

}
