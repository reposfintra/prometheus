/********************************************************************
 *  Nombre Clase.................   MovAuxiliarAction.java
 *  Descripci�n..................   Action de la consulta de movimiento auxiliar
 *  Autor........................   Ing. FERNEL VILLACOB DIAZ
 *  Fecha........................   07/06/2006
 *  Versi�n......................   1.0
 *  Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/




package com.tsp.finanzas.contab.controller;



import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.finanzas.contab.model.threads.HExportarMovAuxiliar;
import com.tsp.finanzas.contab.model.services.MovAuxiliarServices;



public class MovAuxiliarAction extends Action{
    
   
    
    public void run() throws ServletException, com.tsp.exceptions.InformationException {
        
        try{
                    
            
            HttpSession session   = request.getSession();
            Usuario     usuario   = (Usuario) session.getAttribute("Usuario");       
        
            
            String  base    = "/jsp/finanzas/contab/movauxiliar/";
            String  next    = "Consulta.jsp";
            String  msj     = "";
            
            String  evento  =  request.getParameter("evento");
            
            
            if ( evento!=null){
                
                if (evento.equals("INIT"))
                    model.MovAuxiliarSvc.searchTipoAuxiliar();
                
                
                if (evento.equals("ATRAS"))
                    model.MovAuxiliarSvc.reset();
                
                if (evento.equals("BUSCAR")){
                    
                     model.MovAuxiliarSvc.loadRequest(request);                                        
                     model.MovAuxiliarSvc.searchRangoCuentas();
                     List list = model.MovAuxiliarSvc.getListCuentas();
                     if(list.size()==0)
                        msj = "No se encontraron registro de cuentas. Asegurese que las cuentas existan o que tengan cuentas de detalle"; 
                    
                     next = "DetalleConsulta.jsp?msj=" + msj;
                }
                
                
                
                if (evento.equals("MOSTRAR")){
                    String cuenta = request.getParameter("cuenta");
                    model.MovAuxiliarSvc.obtenerCuenta(cuenta);
                    
                    next = "DetalleConsulta.jsp";
                }
                         
                
                
                if (evento.equals("EXCEL")){
                    model.MovAuxiliarSvc.formatear();
                    
                    HExportarMovAuxiliar hilo = new HExportarMovAuxiliar();
                    hilo.start(model, usuario.getLogin(),usuario);
                    
                    next = "DetalleConsulta.jsp?msj=EL Proceso de generaci�n a Excel ha iniciado, por favor espere...";
                }
                
                
                if (evento.equals("IMPRIMIR")){
                    
                    next = "DetalleConsulta.jsp";
                }
                
                
                
                if (evento.equals("TREEVIEW")){
                    String elemento = request.getParameter("elemento");    
                    String codeJs   = model.MovAuxiliarSvc.treeViewCuentas(usuario.getDstrct(), elemento);
                    
                    session.setAttribute("codeJsTreeViewCTA",codeJs); 
                    session.setAttribute("elementTreeView",elemento); 
                    next = "TreeViewCuentas.jsp";
                }
                
                
                
                
                
            }
            
            
            dispatchRequest( base + next );
            
        } catch (Exception e){
             throw new ServletException(e.getMessage());
        }
        
        
    }
    
}
