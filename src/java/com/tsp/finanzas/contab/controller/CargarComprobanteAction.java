/*
 * CargarComprobanteAction.java
 *
 * Created on 5 de julio de 2006, 05:55 PM
 */
/**
 * Nombre        CargarComprobanteAction.java
 * Descripci�n   Manejo de importacion directa
 * Autor         David Pi�a Lopez
 * Fecha         5 de julio de 2006, 05:55 PM
 * Version       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 **/
package com.tsp.finanzas.contab.controller;

import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.exceptions.*;
import com.tsp.finanzas.contab.model.beans.*;
import com.tsp.finanzas.contab.model.threads.*;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.finanzas.presupuesto.model.beans.Upload;
import com.tsp.util.UtilFinanzas;
import java.util.*;
import java.io.*;
/**
 *
 * @author  David
 */
public class CargarComprobanteAction extends Action {
    
    /** Creates a new instance of CargarComprobanteAction */
    public CargarComprobanteAction() {
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        String next    = "/jsp/finanzas/contab/ComprobanteNomina/comprobanteNomina.jsp";
        try{
            HttpSession session = request.getSession();
            Usuario usuario = (Usuario) session.getAttribute("Usuario");
            String Mensaje = "";
            
            if (usuario==null) throw new Exception("Usuario no encontrado en session");
            String ruta = UtilFinanzas.obtenerRuta("ruta", "/exportar/migracion/" + usuario.getLogin());
            
            request.setAttribute( "mensaje", "Proceso de Carga de Archivo en el Comprobante Iniciado." );
            
            /**
             * Carga del los archivos de importacion y datos del formulario
             */
            Upload  upload = new Upload(ruta + "/" ,request);
            upload.load();
            List lista = upload.getFilesName();
            List archivo = upload.getFiles();                        
            
            String filename = lista.get(0).toString();
            File file = (File)archivo.get(0);
            
            int pos = file.getName().lastIndexOf(".");
            String ext = "";
            if (pos!=-1) {
                ext = file.getName().substring(pos+1, file.getName().length());
            }
            if( !ext.equalsIgnoreCase("txt") ) {
                file.delete();
                request.setAttribute( "mensaje", "No se puede procesar el archivo debido a que no es un txt." );
            }else{                 
                CargarArchivoComprobante cac = new CargarArchivoComprobante();
                cac.start( usuario, file );
            }            
            
        }catch(Exception ex){
            throw new ServletException ("Error en CargarComprobanteAction ...\n" + ex.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
