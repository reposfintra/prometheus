/********************************************************************
 *  Nombre Clase.................   actualizarMayorizacionAction.java
 *  Descripcion..................   Action para generar proceso de Mayorizacion
 *  Autor........................   Osvaldo Perez Ferrer
 *  Fecha........................   09/06/2006
 *  Version......................   1.0
 *  Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

/*
 * actualizarMayorizacionAction.java
 *
 * Created on 9 de junio de 2006, 04:47 PM
 */

package com.tsp.finanzas.contab.controller;

import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.finanzas.contab.model.beans.*;
import com.tsp.finanzas.contab.model.*;
import com.tsp.operation.model.beans.*;
import com.tsp.finanzas.contab.model.threads.*;
/**
 *
 * @author  Osvaldo
 */
public class actualizarMayorizacionAction extends Action{
    
    /** Creates a new instance of actualizarMayorizacionAction */
    public actualizarMayorizacionAction() {
    }
    
    public void run() throws ServletException, InformationException {
 /*       String next = "";
        String fechaInicio = "";
        String fechaFin = "";
        String user="";
        boolean todas;
        try{
            String mensaje="Proceso de Mayorizacion iniciado!";
            
            String tipoQuery = request.getParameter("tipoQuery");
            HttpSession session = request.getSession ();
            Usuario usuario = (Usuario)session.getAttribute ("Usuario");
            user=usuario.getLogin();
            if(!tipoQuery.equals("2")){
                String anioini = request.getParameter("anioini");
                String mesini  = request.getParameter("mesini");
                fechaInicio    = anioini+mesini;
                
                String aniofin = request.getParameter("aniofin");
                String mesfin  = request.getParameter("mesfin");
                fechaFin       = aniofin+mesfin;
            }
            request.setAttribute("mensaje", mensaje);
            
            model.mayorizacionService.setUsuario(usuario);
            ActualizacionMayorizacion hilo=new ActualizacionMayorizacion();
            hilo.start(user, fechaInicio, fechaFin, model );
            next = "/jsp/finanzas/contab/mayorizacion/procesoMayorizacion.jsp";
        }catch (Exception e){
            e.printStackTrace();
            throw new ServletException(e.getMessage());
        }
        // Redireccionar a la pgina indicada.
        this.dispatchRequest(next);
    }*/

            HttpSession session = request.getSession ();
            Usuario usuario = (Usuario)session.getAttribute ("Usuario");
            com.tsp.operation.model.Model modelOperation = (com.tsp.operation.model.Model) session.getAttribute("model");
            String ano = request.getParameter("cmbAno");
            String mes = request.getParameter("cmbMes");
            String modo = request.getParameter("modo");
            String next = "";

            if(modo != null){

                 int val = Integer.parseInt(modo);

                 switch(val){

                     case 1:
                         HActualizarMayorizacion hiloMayor = new HActualizarMayorizacion();
                         hiloMayor.start(modelOperation.LogProcesosSvc,model.mayorizacionService, usuario, ano, mes);
                         next = "/jsp/mayorizacion/PeriodoMayorizacion.jsp?msn=iniciado&ano="+ano+"&mes="+mes;
                         break;
                     case 2:

                         HActualizarDesmayorizacion hiloDes = new HActualizarDesmayorizacion();
                         hiloDes.start(modelOperation.LogProcesosSvc,model.mayorizacionService, usuario, ano, mes);
                         next = "/jsp/mayorizacion/PeriodoDesmayorizacion.jsp?msn=iniciado&ano="+ano+"&mes="+mes;
                         break;

                 }
            }else{
                   //EL CODIGO DE USTEDES

                String fechaInicio = "";
                String fechaFin = "";
                String user = "";
                boolean todas;
                try {
                    String mensaje = "Proceso de Mayorizacion iniciado!";
                    String tipoQuery = request.getParameter("tipoQuery");
                    user = usuario.getLogin();
                    if (!tipoQuery.equals("2")) {
                        String anioini = request.getParameter("anioini");
                        String mesini = request.getParameter("mesini");
                        fechaInicio = anioini + mesini;

                        String aniofin = request.getParameter("aniofin");
                        String mesfin = request.getParameter("mesfin");
                        fechaFin = aniofin + mesfin;
                    }
                    request.setAttribute("mensaje", mensaje);

                    model.mayorizacionService.setUsuario(usuario);
                    ActualizacionMayorizacion hilo = new ActualizacionMayorizacion();
                    hilo.start(usuario, fechaInicio, fechaFin, model);
                    next = "/jsp/finanzas/contab/mayorizacion/procesoMayorizacion.jsp";
                } catch (Exception e) {
                    e.printStackTrace();
                    throw new ServletException(e.getMessage());
                }

            }

                // Redireccionar a la pgina indicada.
        this.dispatchRequest(next);
    }
}
