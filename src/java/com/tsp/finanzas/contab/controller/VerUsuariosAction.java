/******************************************************************************
 * Nombre clase :      VerUsuariosAction.java                                 *
 * Descripcion :       Action del VerUsuariosAction.java                      *
 * Autor :             LREALES                                                *
 * Fecha :             28 de junio de 2006, 07:35 AM                          *
 * Version :           1.0                                                    *
 * Copyright :         Fintravalores S.A.                                *
 *****************************************************************************/

package com.tsp.finanzas.contab.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.services.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;
import com.tsp.util.*;
import java.util.Vector;
import java.lang.*;
import java.sql.*;
import javax.servlet.ServletException;
import com.tsp.operation.model.threads.*;
import com.tsp.finanzas.contab.model.*;
import com.tsp.finanzas.contab.model.beans.*;
import com.tsp.finanzas.contab.model.threads.*;
import com.tsp.finanzas.contab.model.services.*;
import javax.servlet.http.*;

public class VerUsuariosAction  extends Action {
    
    /** Creates a new instance of VerUsuariosAction */
    public VerUsuariosAction() {
    }    
    
    public void run() throws ServletException {
        
        HttpSession session = request.getSession();
        
        com.tsp.operation.model.Model modelOp = (com.tsp.operation.model.Model) session.getAttribute("model");
        
        String next = "/jsp/finanzas/contab/reporte_facturas_proveedor/VerUsuarios.jsp";
                
        try{
            
            modelOp.tablaGenService.buscarUsuarios();
            modelOp.tablaGenService.loadTUsuariosFactura();

            int cont = modelOp.tablaGenService.getUsuarios().size();

            if ( cont > 0 ){

                next = "/jsp/finanzas/contab/reporte_facturas_proveedor/VerUsuarios.jsp";

            }                
            
        } catch ( Exception e ){
                        
            throw new ServletException( e.getMessage () );
            
        }
        
        this.dispatchRequest( next );
                        
    }
    
}