/*
 * CruceAYSProcesoAction.java
 *
 * Created on 26 de junio de 2005, 11:56 AM
 */

package com.tsp.finanzas.contab.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.finanzas.contab.model.beans.*;
import com.tsp.operation.model.beans.*;
import com.tsp.finanzas.contab.model.*;
import com.tsp.finanzas.contab.model.threads.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  Sandrameg
 */
public class ContabilizacionMovplaAction extends Action {
    
    /** Creates a new instance of CruceAYSProcesoAction */
    public ContabilizacionMovplaAction() {
    }
    
    public void run() throws ServletException,InformationException{
        HttpSession session = request.getSession();
        Usuario u = (Usuario) session.getAttribute("Usuario");
        String msg = "Proceso iniciado!";
        String next = "/jsp/finanzas/contab/contabilizacionManual/contabilizacionMovpla.jsp?msg=" + msg;
        try{
            HContabilizacionMovpla hilo = new HContabilizacionMovpla();
            hilo.start(u.getLogin(),"EPL",model.comprobanteService.getMoneda(u.getDstrct()));
            
            
        }catch(Exception e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
}
