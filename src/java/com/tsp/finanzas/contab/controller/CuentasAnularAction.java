/********************************************************************
 *  Nombre Clase.................   PrecintosAnularAction.java
 *  Descripci�n..................   Action de la tabla cuentas
 *  Autor........................   Ing. Leonardo Parody Ponce
 *  Fecha........................   27.12.2005
 *  Versi�n......................   1.0
 *  Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.finanzas.contab.controller;
import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.finanzas.contab.model.beans.*;
import com.tsp.finanzas.contab.model.*;
import com.tsp.operation.model.beans.*;
/**
 *
 * @author  EQUIPO12
 */
public class CuentasAnularAction extends Action{
    
    /** Creates a new instance of CuentasAnularAction */
    public CuentasAnularAction () {
    }
    public void run () throws ServletException, InformationException {
        //////System.out.println ("ESTOY EN EL ACTION");
        List precintos = new LinkedList ();
        String next="";
        HttpSession session = request.getSession ();
        Usuario usuario = (Usuario)session.getAttribute ("Usuario");
        String base = usuario.getBase ();
        String dstrct = usuario.getDstrct ();
        String nombre = usuario.getNombre ();
        Cuentas cuentas = new Cuentas ();
        cuentas.setCuenta (request.getParameter ("cuenta"));
        cuentas.setActiva (request.getParameter ("activa"));
        cuentas.setAuxiliar (request.getParameter ("auxiliar"));
        cuentas.setNombre_corto (request.getParameter ("nom_corto"));
        cuentas.setNombre_largo (request.getParameter ("nom_largo"));
        cuentas.setFin_de_periodo (request.getParameter ("final_periodo"));
        cuentas.setObservacion (request.getParameter ("observacion"));
        //////System.out.println ("Observacion = "+cuentas.getObservacion ());
        cuentas.setModulo1 (request.getParameter ("Modulo1"));
        cuentas.setModulo2 (request.getParameter ("Modulo2"));
        cuentas.setModulo3 (request.getParameter ("Modulo3"));
        cuentas.setModulo4 (request.getParameter ("Modulo4"));
        cuentas.setModulo5 (request.getParameter ("Modulo5"));
        cuentas.setModulo6 (request.getParameter ("Modulo6"));
        cuentas.setModulo7 (request.getParameter ("Modulo7"));
        cuentas.setModulo8 (request.getParameter ("Modulo8"));
        cuentas.setModulo9 (request.getParameter ("Modulo9"));
        cuentas.setModulo10 (request.getParameter ("Modulo10"));
        cuentas.setBase (base);
        cuentas.setCreation_user (nombre);
        cuentas.setDstrct (dstrct);
        cuentas.setUser_update (nombre);
        try{
            model.cuentaService.AnularCuentas (cuentas.getCuenta ());
            request.setAttribute ("cuenta", cuentas);
            next="/jsp/finanzas/contab/cuentas/CuentasBuscar.jsp?msg=Cuenta Anulada Exitosamente";
        }catch (SQLException e){
            e.printStackTrace ();
            throw new ServletException (e.getMessage ());
        }
        // Redireccionar a la p�gina indicada.
        this.dispatchRequest (next);
    }
    
}
