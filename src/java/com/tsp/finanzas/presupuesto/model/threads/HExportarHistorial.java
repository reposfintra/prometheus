
/*************************************************************
 * Nombre      ............... HExportarHistorial.java
 * Descripcion ............... Exporta el historial de un presupuesto de ventas
 * Autor       ............... mfontalvo
 * Fecha       ............... Diciembre - 05 - 2005
 * Version     ............... 1.0
 * Copyright   ............... Fintravalores S.A. S.A
 ************************************************************/



package com.tsp.finanzas.presupuesto.model.threads;

import com.tsp.util.UtilFinanzas;
import com.tsp.finanzas.presupuesto.model.Model;
import com.tsp.finanzas.presupuesto.model.beans.*;
import com.tsp.operation.model.beans.Usuario;
import java.util.*;
import java.io.*;


import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;


public class HExportarHistorial extends Thread{
    
    
    // parametros de busqueda del historial
    private String Agencia;
    private String Cliente;
    private String Estandar;
    
    private String Ano;
    private String Mes;
    private String Usuario;
    
    private Model model;
    
    // parametros del archivo de excel
    private POIWrite xls;
    HSSFCellStyle titulo1;
    HSSFCellStyle subtitulo1;
    HSSFCellStyle subtitulo2;
    HSSFCellStyle subtitulo3;
    HSSFCellStyle texto;
    HSSFCellStyle textoCentrado;
    
    
    public HExportarHistorial() {}
    
    
    
    
    
    /* Procedimiento inicializador del thread
     * @params Agencia ...... Agencia a consultar
     * @params Cliente ...... Cliente a consultar
     * @params Estandar ..... Estandar a consultar
     * @params Ano .......... Ano a Consultar
     * @params Mes .......... Mes a Consultar
     * @params Usuario ...... usuario en session
     * @see start del thread
     */
    
    
    public void start(String Agencia, String Cliente, String Estandar, String Ano, String Mes, Usuario usuario) throws Exception{
        model = new Model(usuario.getBd());
        this.Agencia = Agencia;
        this.Cliente = Cliente;
        this.Estandar= Estandar;
        this.Ano     = Ano;
        this.Mes     = Mes;
        this.Usuario = usuario.getLogin();
        super.start();
    }
    
    
    
    
    /* Procedimiento run del hilo donde se ejecuta el mismo
     */
    
    public synchronized void run(){
        
        try{
            
            model.ReportesSvc.HistorialMensual(Agencia, Cliente, Estandar, Ano, Mes);
            List  lista = model.ReportesSvc.getListado();
            
            if (!lista.isEmpty()){
                String ruta = UtilFinanzas.obtenerRuta("ruta", "/exportar/migracion/" + Usuario);
                xls = new POIWrite(ruta + "/HistorialDeCambios_"+ Ano + Mes +".xls");
                titulo1       = xls.nuevoEstilo("Book Antiqua", 20, true  , false, "text"        , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
                subtitulo1    = xls.nuevoEstilo("Book Antiqua", 8 , true  , false, "text"        , xls.NONE , HSSFColor.SKY_BLUE.index , HSSFCellStyle.ALIGN_LEFT);
                subtitulo2    = xls.nuevoEstilo("Book Antiqua", 8 , true  , false, "text"        , xls.NONE , HSSFColor.GOLD.index , HSSFCellStyle.ALIGN_LEFT);
                subtitulo3    = xls.nuevoEstilo("Book Antiqua", 8 , true  , false, "text"        , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
                texto         = xls.nuevoEstilo("Book Antiqua", 8 , false , false, "text"        , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
                textoCentrado = xls.nuevoEstilo("Book Antiqua", 8 , false , false, "text"        , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_CENTER);
                
                int fila = 0;
                
                String agenciaAnterior = null;
                String agenciaActual   = null;
                String clienteAnterior = null;
                String clienteActual   = null;
                
                Iterator it = lista.iterator();
                while(it.hasNext()){
                    
                    DatosGeneral dt  = (DatosGeneral) it.next();
                    agenciaActual = (dt.getValor("NOMBREAGENCIA").equals("")?"ANE":dt.getValor("NOMBREAGENCIA"));
                    clienteActual = (dt.getValor("NOMBRECLIENTE").equals("")?"CNE":dt.getValor("NOMBRECLIENTE"));
                    
                    if (agenciaAnterior == null || !agenciaAnterior.equals(agenciaActual ) ){
                        NuevaAgencia( agenciaActual );
                        fila = 5;
                    }
                    agenciaAnterior = agenciaActual;
                    
                    if (clienteAnterior == null || !clienteAnterior.equals(clienteActual)) {
                        xls.adicionarCelda(fila ,1, clienteActual, subtitulo1);
                        xls.combinarCeldas(fila ,1, fila, 5);
                        fila+=2;
                    }
                    clienteAnterior = clienteActual;
                    
                    
                    
                    xls.adicionarCelda(fila   ,1, "Estandar"                  , subtitulo3);
                    xls.adicionarCelda(fila   ,2, dt.getValor("STDJOBNO")   , texto     );
                    xls.combinarCeldas(fila   ,2, fila++, 5);

                    xls.adicionarCelda(fila   ,1, "Descripcion"               , subtitulo3);
                    xls.adicionarCelda(fila   ,2, dt.getValor("STDJOBDESC")   , texto     );
                    xls.combinarCeldas(fila   ,2, fila++, 5);                    
                    
                    xls.adicionarCelda(fila   ,1, "Origen"                    , subtitulo3);
                    xls.adicionarCelda(fila   ,2, dt.getValor("NOMBREORIGEN") , texto     );
                    xls.combinarCeldas(fila   ,2, fila++, 5);
                    
                    xls.adicionarCelda(fila   ,1, "Destino"                    , subtitulo3);
                    xls.adicionarCelda(fila   ,2, dt.getValor("NOMBREDESTINO") , texto     );
                    xls.combinarCeldas(fila   ,2, fila++, 5);
                    
                    xls.adicionarCelda(fila   ,1, "Listado de Reprogramaciones", subtitulo3);
                    xls.combinarCeldas(fila   ,1, fila++, 5);
                    
                    xls.adicionarCelda(fila   ,1, "Dia"                  , subtitulo2);
                    xls.adicionarCelda(fila   ,2, "Viajes"               , subtitulo2);
                    xls.adicionarCelda(fila   ,3, "Fecha Modificacion"   , subtitulo2);
                    xls.adicionarCelda(fila   ,4, "Usuario"              , subtitulo2);
                    xls.adicionarCelda(fila++ ,5, "Causa Reprogramacion" , subtitulo2);
                    
                    model.DPtoSvc.BuscarHistorial( dt.getValor("DISTRITO"), dt.getValor("STDJOBNO"), Ano, Mes);
                    List listaHT = model.DPtoSvc.getHistorial();
                    
                    
                    if (!listaHT.isEmpty()){
                        for (int dia = 0 ; dia < 31; dia++){                            
                            if ( mostrar(dia, listaHT) ){
                                model.ReprogamacionSvc.SEARCH_VIAJE_REPROGRAMADO( dt.getValor("STDJOBNO"), Ano, Mes);
                                List listaRP = model.ReprogamacionSvc.getListaReprogramaciones();
                                
                                DatosGeneral ant = null;
                                
                                boolean valido = false;
                                for (int version = 0; version < listaHT.size() ; version++  ){
                                    DatosGeneral  viaje = (DatosGeneral) listaHT.get(version);
                                    String Dia = String.valueOf(dia);
                                    
                                    if (ant==null || !ant.getValor(Dia).equals( viaje.getValor(Dia))) {
                                        if (mostrarItem(version, dia, listaHT)){
                                            NuevoHistoria(Ano, Mes, dia, fila, viaje, listaRP);
                                            ant = viaje;
                                            fila++;
                                        }
                                    }
                                }
                            }
                            
                        }
                        
                        
                    }
                    fila+=3;
                }
                xls.cerrarLibro();
                
            }
            
            
        }catch (Exception ex){
            ////System.out.println("Error ..." + ex.getMessage());
        }
        
    }
    
    
    
    /* Procedimiento que indica si se debe o no ostrar los datos 
     * del historial de un dia
     * @params dia ....... dia a mostrar
     * @params lista ..... listado de historial que tiene el dia
     * @return devuelve verdadero si el dia tiene historial en caso contrario no.
     */
    
    
    private boolean mostrar(int dia, List lista){
        for (int i=0; i<lista.size(); i++)
            if (Integer.parseInt(((DatosGeneral) lista.get(i)).getValor( String.valueOf(dia))) !=0 )
                return true;
        return false;
    }
    

    
    /* Procedimiento llamado segundo nivel de discrimiancion para
     * determinar si se puede mostrar o no el historial en base a
     * a la informacion del proximo elemento o version del
     * historial.
     * @params item ........ version del pto.
     * @params dia ......... dia a discrimiar
     * @params lista ....... lista de historial
     * @return devuelve si se deve mostrar o no el historial.
     */
    
    private boolean mostrarItem(int item, int dia, List lista){
        
        String Dia = String.valueOf(dia);        
        DatosGeneral act  = (DatosGeneral) lista.get(item);       
        DatosGeneral next = (item>=lista.size()-1?null: (DatosGeneral) lista.get(item+1) );
        
        if ((next==null && !act.getValor(Dia).equals("0") ) || (next!=null && !act.getValor(Dia).equals( next.getValor(Dia) )))
            return true;
        else
            return false;
        

    }
    
    
    /* Procedimiento que obtiene la reprogramacion de un viaje
     * @params FechaViaje ..... fecha del Viaje de estandar
     * @params FechaCreacion .. fecha de creacion del viaje
     * @params ListaGeneral ... Lista de Viaje reprogramados
     * @return reprogramacion
     */
    
    private String obtenerModificaciones(String FechaViaje, String FechaCreacion, List ListaGeneral) {
        // definimos donde comienza y finaliza la sublista
        int inicio = -1, fin = -1;
        // para el detener en el recorido donde comineza la lista
        //Reprogramacion objeto = null;
        String objeto = "";
        for (int i=0; i<ListaGeneral.size();i++ ){
            Reprogramacion rp = (Reprogramacion) ListaGeneral.get(i);
            
            if (rp.getFechaViaje().equals(FechaViaje) && rp.getFechaCreacion().equals(FechaCreacion)){
                objeto = rp.getDescripcion();
                break;
            }
        }
        return objeto;
    }
    
    
    
    /* Procedimiento para crear una nueva cabecera de excel en una hoja 
     * nueva
     * @params nuevaAgencia .... datos de la nueva agencia
     */
    
    private void NuevaAgencia(String nuevaAgencia) throws Exception{
        try{
            xls.obtenerHoja(nuevaAgencia);
            xls.cambiarMagnificacion(4,5);
            
            xls.adicionarCelda(0,0,"TRANSPORTE SANCHEZ POLO", titulo1);
            xls.adicionarCelda(2,0,"Presupuesto de Ventas Periodo " + Ano + Mes, subtitulo3);
            xls.adicionarCelda(3,0,"Agencia   " + nuevaAgencia , subtitulo3);
            
            xls.combinarCeldas(0, 0, 1, 5);
            xls.combinarCeldas(2, 0, 2, 5);
            xls.combinarCeldas(3, 0, 3, 5);
            
            xls.cambiarAnchoColumna(0,  3100); // blanco
            xls.cambiarAnchoColumna(1,  3100); // dia
            xls.cambiarAnchoColumna(2,  3100); // viajes
            xls.cambiarAnchoColumna(3,  4500); // fecha  modificacion
            xls.cambiarAnchoColumna(4,  3100); // usuario
            xls.cambiarAnchoColumna(5,  9000); // causa reprogramacion
        }
        catch (Exception ex){
            throw new Exception("Error en la rutina NuevaAgencia [HExportarHistorial]...\n" + ex.getMessage());
        }
    }
    
    
    /* Procedimiento para ingresar un nuevo registros historico en archivo de excel
     * @params Ano .......... ano del pto
     * @params Mes .......... mes del pto
     * @params dia .......... dia del pto
     * @params fila ......... fila de excel
     * @params viaje ........ viaje presupuesrtado
     * @params listaRP ...... lista de reprogramaciones
     */
    
    
    private void NuevoHistoria (String Ano , String Mes, int dia, int fila, DatosGeneral viaje, List listaRP) throws Exception{
        try{
            String fecha =  Ano + "-" + Mes + "-" + UtilFinanzas.DiaFormat(dia+1);
            
            xls.adicionarCelda(fila  , 1, UtilFinanzas.DiaFormat(dia+1), textoCentrado);
            xls.adicionarCelda(fila  , 2, Integer.parseInt(viaje.getValor( String.valueOf(dia) )) , textoCentrado);
            xls.adicionarCelda(fila  , 3, viaje.getValor("FECHA"    ), textoCentrado);
            xls.adicionarCelda(fila  , 4, viaje.getValor("USUARIO"  ), textoCentrado);
            xls.adicionarCelda(fila  , 5, obtenerModificaciones(fecha ,viaje.getValor("FECHA"), listaRP), texto);
        }catch (Exception e ){
            throw new Exception("Error en la rutina NuevoHistoria [HExportarHistorial]...\n" + e.getMessage());
        }
        
    }
    
    
    /*
    
    public static void main(String[] args) throws Exception {
        
        HExportarHistorial h = new HExportarHistorial();
        h.start("SM","000127","%","2010","01","KREALES");
        
    }
     */
    
}