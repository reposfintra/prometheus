/**
 * Nombre        HExportarVistaPtoXLS.java
 * Descripci�n   Exportacion de vista de Excel
 * Autor         Mario Fontalvo Solano
 * Fecha         5 de junio de 2006, 02:05 PM
 * Version       1.0
 * Coyright      Transportes S�nchez Polo S.A.
 **/

package com.tsp.finanzas.presupuesto.model.threads;

import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import com.tsp.finanzas.presupuesto.model.beans.DatosGeneral;
import com.tsp.operation.model.beans.POIWrite;
import com.tsp.util.UtilFinanzas;
import java.util.*;
import java.text.*;

public class HExportarVistaPtoXLS extends Thread{
    
    com.tsp.operation.model.Model model;
    com.tsp.finanzas.presupuesto.model.Model modelpto;
    String usuario;
    String procesoName = "HExportarVistaPtoXLS";
    private SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMddkkmmss");
    
    /** Crea una nueva instancia de  HExportarVistaPtoXLS */
    public HExportarVistaPtoXLS() {
    }
    
    public void start(com.tsp.operation.model.Model model, com.tsp.finanzas.presupuesto.model.Model modelpto, String usuario){
        this.model    = model;
        this.modelpto = modelpto;
        this.usuario  = usuario;
        run();
    }
    
    public synchronized void run(){
        try{
            
            //INSERTO EN EL LOG DE PROCESO
            model.LogProcesosSvc.InsertProceso(this.procesoName, this.hashCode(), "INICIO DEL PROCESO", usuario);             
            
            
            List lista = modelpto.DPtoSvc.getLista();
            if (lista!=null){
                String tVista  = modelpto.DPtoSvc.getValor("Tipo");
                String Ano     = modelpto.DPtoSvc.getValor("Ano");
                String Mes     = modelpto.DPtoSvc.getValor("Mes");
                int diaInicial = Integer.parseInt(modelpto.DPtoSvc.getValor("diaInicial"));
                int diaFinal   = Integer.parseInt(modelpto.DPtoSvc.getValor("diaFinal"));
                int numcols    = modelpto.DPtoSvc.getNroColumnas();
                TreeMap tasas  = modelpto.DPtoSvc.getListadoTasa();
                DatosGeneral tasa = (DatosGeneral) tasas.get(Mes);
                
                
                String ruta = UtilFinanzas.obtenerRuta ("ruta", "/exportar/migracion/" + usuario);
                int fila = 6;
                
                POIWrite xls = new POIWrite(ruta + "/VistaPresupuesto_"+ Ano + Mes +"_"+ fmt.format(new java.util.Date()) +".xls");
                HSSFCellStyle texto  = xls.nuevoEstilo("Book Antiqua", 8, false , false, "text"        , xls.NONE , xls.NONE , xls.NONE);
                HSSFCellStyle numero = xls.nuevoEstilo("Book Antiqua", 8 , false , false, "_(#,##0.00_);(@_)"      , xls.NONE , xls.NONE , xls.NONE);
                HSSFCellStyle numero2 = xls.nuevoEstilo("Book Antiqua", 8 , false , false, "_(#,###_);(@_)"      , xls.NONE , xls.NONE , xls.NONE);
                HSSFCellStyle fecha  = xls.nuevoEstilo("Book Antiqua", 8, false , false, "yyyy/mm/dd"  , xls.NONE , xls.NONE , xls.NONE );
                
                HSSFCellStyle header      = xls.nuevoEstilo("Arial Black", 18, true  , false, "text"        , HSSFColor.ORANGE.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
                HSSFCellStyle titulo      = xls.nuevoEstilo("Book Antiqua", 9, true  , false, "text"        , HSSFColor.WHITE.index , HSSFColor.DARK_GREEN.index , HSSFCellStyle.ALIGN_CENTER);
                                
                
                
                xls.obtenerHoja("BASE");
                xls.cambiarMagnificacion(3,4);
                
                String [] cabecera = {
                    "DISTRITO",
                    "AGENCIA" , "NOMBRE AGENCIA",
                    "CLIENTE" , "NOMBRE CLIENTE",
                    "ORIGEN"  , "NOMBRE ORIGEN",
                    "DESTINO" , "NOMBRE DESTINO",
                    "ESTANDAR", "DESCRIPCION ESTANDAR",
                    "RECURSO", "TIPO DE VIAJE", "TARIFA",
                    "MONEDA", "VIAJES", "TOTAL PRESUPUESTO"
                };
                short []dimensiones = { 3000, 3000, 6000,3000,6000,3000,6000, 3000, 6000, 3000,7000,5000,3000,4800,4000, 4800,4800};
                for (int i=0; i<cabecera.length; i++){
                    xls.adicionarCelda(fila, i, cabecera[i], titulo);
                    xls.cambiarAnchoColumna(i, dimensiones[i]);
                }
                
                for (int i=0;i<numcols;i++){
                    String tt = (tVista.equals("M")?( Mes.equals("TD")? UtilFinanzas.NombreMes(i+1).substring(0,3): UtilFinanzas.NombreMes( Integer.parseInt(Mes)) ):((tVista.equals("S")?"Sem ": UtilFinanzas.DiaSemana( Ano + Mes + UtilFinanzas.DiaFormat(i+1) ) + " " ) + UtilFinanzas.DiaFormat(i+1)    ));
                    boolean mostrar = (tVista.equals("D") && ((i+1)>diaFinal || (i+1)<diaInicial) ) ? false : true;
                    if (mostrar) {
                        xls.adicionarCelda(fila, i+cabecera.length, tt , titulo);
                        xls.cambiarAnchoColumna(i+cabecera.length, 3000);
                    }
                    
                }                
                
                // cabecera
                xls.adicionarCelda(0,0, "TRANSPORTE SANCHEZ POLO", header);
                xls.combinarCeldas(0, 0, 0, 2);
                xls.adicionarCelda(1,0, "Fecha "   , texto);
                xls.adicionarCelda(1,1, new Date() , fecha);
                xls.adicionarCelda(2,0, "Tasas de cambio"   , texto);
                xls.adicionarCelda(3,0, "DOLAR"             , texto);
                xls.adicionarCelda(3,1, Double.parseDouble(tasa!=null && tasa.getValor("DOL")!=null?tasa.getValor("DOL"):"0")  , numero);
                xls.adicionarCelda(4,0, "BOLIVAR"           , texto);
                xls.adicionarCelda(4,1, Double.parseDouble(tasa!=null && tasa.getValor("BOL")!=null?tasa.getValor("BOL"):"0")  , numero);                
                
                fila++;
                
                Iterator it = lista.iterator();
                while (it.hasNext()){
                    DatosGeneral dt = (DatosGeneral) it.next();
                    int col = 0;
                    xls.adicionarCelda(fila, col++, dt.getValor("DISTRITO")     , texto);
                    xls.adicionarCelda(fila, col++, dt.getValor("CODIGOAGENCIA"), texto);
                    xls.adicionarCelda(fila, col++, dt.getValor("NOMBREAGENCIA"), texto);
                    xls.adicionarCelda(fila, col++, dt.getValor("CODIGOCLIENTE"), texto);
                    xls.adicionarCelda(fila, col++, dt.getValor("NOMBRECLIENTE"), texto);
                    xls.adicionarCelda(fila, col++, dt.getValor("CODIGOORIGEN") , texto);
                    xls.adicionarCelda(fila, col++, dt.getValor("NOMBREORIGEN") , texto);
                    xls.adicionarCelda(fila, col++, dt.getValor("CODIGODESTINO"), texto);
                    xls.adicionarCelda(fila, col++, dt.getValor("NOMBREDESTINO"), texto);
                    xls.adicionarCelda(fila, col++, dt.getValor("STDJOBNO")     , texto);
                    xls.adicionarCelda(fila, col++, dt.getValor("STDJOBDESC")   , texto);
                    xls.adicionarCelda(fila, col++, dt.getValor("RECURSO")      , texto);
                    xls.adicionarCelda(fila, col++, dt.getValor("TIPOVIAJE")    , texto);
                    xls.adicionarCelda(fila, col++, Double.parseDouble(dt.getValor("TARIFA"))       , numero);
                    xls.adicionarCelda(fila, col++, dt.getValor("MONEDA")       , texto );
                    xls.adicionarCelda(fila, col++, Double.parseDouble(dt.getValor("TOTAL"))        , numero2);
                    xls.adicionarCelda(fila, col++, Double.parseDouble(dt.getValor("TOTALPTO"))     , numero);
                    for (int i=0;i<numcols; i++){
                        boolean mostrar = (tVista.equals("D") && ((i+1)>diaFinal || (i+1)<diaInicial) ) ? false : true;
                        if (mostrar) xls.adicionarCelda(fila, col++, Double.parseDouble(dt.getValor(String.valueOf(i))), numero2);
                    }
                    fila++;
                }
                xls.cerrarLibro();
            }
            model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(), usuario, "Exitoso");
        }catch (Exception ex){
            ex.printStackTrace();
            try{
            model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(), usuario, "Error : " + ex.getMessage());
            } catch (Exception e) {}          
        }
    }
    
    
    
/*    public static void main(String []args) throws Exception{
        
        com.tsp.operation.model.Model model = new com.tsp.operation.model.Model();
        com.tsp.finanzas.presupuesto.model.Model modelpto = new com.tsp.finanzas.presupuesto.model.Model();
        
        modelpto.DPtoSvc.ObtenerListado("D", "2006", "01", "%", "%", "%", "%", "%", "%", false, "Ok", "01", "05");
        
        HExportarVistaPtoXLS x = new HExportarVistaPtoXLS();
        x.start(model, modelpto, "mario");
        
    }*/
}
