/*************************************************************
 * Nombre      ............... HImportXLS.java
 * Descripcion ............... Importa el presupuesto de ventas desde un
 *                             archivo de excel (comercial)
 * Autor       ............... mfontalvo
 * Fecha       ............... Junio - 14 - 2005
 * Version     ............... 1.0
 * Copyright   ............... Fintravalores S.A. S.A
 ************************************************************/


package com.tsp.finanzas.presupuesto.model.threads;

import com.tsp.finanzas.presupuesto.model.Model;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.util.*;

import java.io.*;
import java.sql.*;
import java.util.*;
import jxl.*;



public class HImportarXLS extends Thread{
    private String Archivo;
    private String Hoja;
    private String Ano;
    private String Mes;
    private String Usuario;
    private Model  model;
    private String fileLog;
    private String Modo; // mensual // diario
    private String Accion; // A: Actualizar; E: Eliminar y Adicionar
    
    /** Creates a new instance of HImportXLS */
    public HImportarXLS() {
    }
    
    public void start(String Archivo, String Ano, String Mes, String Modo, String Accion, Usuario Usuario) throws Exception{
        this.Hoja       = "BASE";
        this.Archivo    = Archivo;
        this.Ano        = Ano;
        this.Mes        = Mes;
        this.Usuario    = Usuario.getLogin();
        this.Modo       = Modo;
        this.Accion     = Accion;
        this.model      = new Model(Usuario.getBd());
        super.start();
        
    }
    
    public synchronized void run(){
        try{
            
            
            String ruta = UtilFinanzas.obtenerRuta("ruta", "/exportar/migracion/" + Usuario);
            fileLog     = ruta + "/LogImportarPtoXLS_"+ Ano + Mes +".csv";
            
            
            // Inicializando Archivo de log de Errores
            FileWriter fw = new FileWriter(this.fileLog);
            wLog(fw,"Iniciando Proceso ...");
            
            
            try{
                
                // variable de control que almacena los estandares ya procesados
                TreeMap registro = new TreeMap();
                
                
                /////////////////////////////////////////////////////////////////////////////////////////////
                // parametros para el archivo de Excel
                int i,
                Fila            = 6,  // Se empieza a contar desde aqui
                colStdjob       = 1,  // Columnas de inicio de viajes
                colTotalMes     = 11, // Columnas de inicio de viajes
                colInicio       = 12, // Columnas de inicio de viajes
                FilasProcesadas = 0,
                FilasRechazadas = 0,
                TotalFilas      = 0;
                String Distrito = "FINV";
                
                
                /////////////////////////////////////////////////////////////////////////////////////////////
                // eliminacion del presupuesto actual
                if (Accion.equals("E")){
                    wLog(fw, "Buscando Eliminado datos ....");
                    model.DPtoSvc.DELETE(Distrito, Ano, Mes);
                }
                
                /////////////////////////////////////////////////////////////////////////////////////////////
                // Busqueda del archivo y la hoja base
                
                wLog(fw, "Buscando Archivo ["+ Archivo +"]");
                Workbook wb    = Workbook.getWorkbook(new File( Archivo ));
                
                wLog(fw, "Buscando Hoja ["+ this.Hoja +"]");
                Sheet    sheet = wb.getSheet(this.Hoja);
                
                
                
                // si el libro y la hoja estan correctos
                if (sheet!=null){
                    
                    
                    
                    wLog(fw, "Iniciando Lectura");                    
                    wLog(fw, "Fila Archivo,Procesado,Estandar,Viajes Mensuales,Agencia,Cliente,Comentario");
                    
                    
                    
                    TotalFilas = sheet.getRows();
                    for (i=Fila;i< TotalFilas ; i++){
                        
                        String Stdjob   = sheet.getCell(colStdjob  , i).getContents();
                        String CMensual = getViaje(sheet, i, colTotalMes);
                        
                        if (Stdjob!=null && !Stdjob.trim().equals("") && Stdjob.trim().length() == 6 && CMensual!=null && !CMensual.trim().equals("") && !CMensual.trim().equals("0")){
                            
                            // inicializacion de parametros del cliente
                            String Cliente  = "000" + Stdjob.substring(0,3);
                            model.DPtoSvc.AgenciaDuenaCliente(Cliente);
                            String Agencia  = model.DPtoSvc.getInfoCliente();
                            
                            
                            if (!Agencia.trim().equals("")){
                                String CDias[]  = new String [31];
                                if (Modo.equals("D"))
                                    for (int col = colInicio ; col<colInicio + 31; col++){
                                        CDias[(col-colInicio)] = getViaje(sheet, i, col); 
                                    }
                                //////////////////////////////////////////////////////////
                                if (Stdjob.length()==6){
                                    
                                    
                                    // registrando en la base de datos
                                    try{
                                        if (Modo.equals("D"))
                                            model.DPtoSvc.INSERT_DIARIO( Distrito, Ano, Mes, Stdjob, Agencia, Cliente, CMensual, CDias, this.Usuario);
                                        else
                                            model.DPtoSvc.INSERT_MENSUAL( Distrito, Ano, Mes, Stdjob, Agencia, Cliente, CMensual, this.Usuario);
                                    }catch (Exception ex){
                                        wLog(fw, (i+1) +",No," + Stdjob + "," + CMensual + "," + Agencia + ",\"" + Cliente + "\"," + ex.getMessage().replaceAll("\n","") );
                                    }
                                    
                                   
                                    // registro el estado en el log
                                    try{
                                        if (registro.get(Stdjob)==null)
                                            wLog(fw, (i+1) +",Si," + Stdjob + "," + CMensual + "," + Agencia + ",\"" + Cliente + "\"," + "Ok" );
                                        else
                                            wLog(fw, (i+1) +",Si," + Stdjob + "," + CMensual + "," + Agencia + ",\"" + Cliente + "\"," + "Ok - Este Estandar ya se habia registrado - Linea " + registro.get(Stdjob));
                                    } catch (Exception err){
                                        wLog(fw, (i+1) +",No,"+ Stdjob +","+ CMensual +",-,-,No se que paso. " + err.getMessage());
                                    }
                                    
                                    registro.put(Stdjob ,  String.valueOf(i+1));
                                    FilasProcesadas++;
                                    
                                }
                                else{
                                    wLog(fw, (i+1) +",No,"+ Stdjob +","+ CMensual +",-,-,Longitud de estandar incorrecto");
                                    FilasRechazadas++;
                                }
                                //////////////////////////////////////////////////////////
                            }
                            else{
                                wLog(fw, (i+1) +",No,"+ Stdjob +","+ CMensual +",-,\"" + Cliente +"\",Agencia no definida en la base de datos de SOT para el cliente.");
                                FilasRechazadas++;
                            }
                        }
                        else{
                            wLog(fw, (i+1) +",No,"+ Stdjob +","+ CMensual +",-,-,Estandar o cantidad no aplican");
                            FilasRechazadas++;
                        }
                    }
                }else
                    wLog(fw , "No se pudo encontrar la Hoja ["+ this.Hoja +"]");
                wb.close();
                
                wLog(fw, "\nEstado");
                wLog(fw, "Filas Procesadas, "+ FilasProcesadas );
                wLog(fw, "Filas Rechazadas, "+ FilasRechazadas );
                wLog(fw, "Total Filas     ," + TotalFilas      );
                wLog(fw, "Proceso Terminado");
                
            }catch (Exception ex){
                wLog(fw, ex.getMessage());
            }
            finally{
                fw.close();
            }
        }catch (Exception ex){
            ////System.out.println("Error [HImportarXLS]: "+ex.getMessage());
        }
    }
    
    
    /**
     * Procedimiento para imprimir los datos en el log.
     * @param fw .... Apuntador al archivo del log
     * @param msg ... Mensaje a imprimir
     */
    public void wLog(FileWriter fw, String msg) throws Exception{
        if (fw!=null) fw.write( "\n" + (new java.util.Date()).toString() + "," + msg);
    }
    
    
    /**
     * Metodo para obtener los viajes de un estandar;
     * @param sheet, Hoja donde se extraeran los datos
     * @param fila, fila del viaje
     * @param columna, columna del viaje
     * @return String, numero de Viajes
     * @throws Exception.
     */
    public String getViaje (Sheet sheet, int fila, int columna) throws Exception {
        if (sheet==null)
            throw new Exception ( "No se encontro referencia a la hoja del libro, al leer los viajes");
        String content = sheet.getCell(columna, fila).getContents();
        content = String.valueOf( (int) Float.parseFloat(content==null || content.trim().equals("")?"0": content.trim()));
        return content;
    }
    
}
