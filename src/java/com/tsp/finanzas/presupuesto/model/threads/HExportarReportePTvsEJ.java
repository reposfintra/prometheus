/**
 * Nombre        HExportarReportePTvsEJ.java
 * Descripci�n
 * Autor         Mario Fontalvo Solano
 * Fecha         15 de mayo de 2006, 08:21 AM
 * Version       1.0
 * Coyright      Transportes S�nchez Polo S.A.
 **/

package com.tsp.finanzas.presupuesto.model.threads;

import com.tsp.operation.model.beans.POIWrite;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.util.*;

import com.tsp.finanzas.presupuesto.model.Model;
import com.tsp.finanzas.presupuesto.model.beans.ViajesAgencia;
import com.tsp.finanzas.presupuesto.model.beans.ViajesCliente;
import com.tsp.finanzas.presupuesto.model.beans.ViajesStandar;

import java.io.*;
import java.util.*;
import java.text.*;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.util.HSSFColor;


public class HExportarReportePTvsEJ extends Thread {
    
    private Usuario usuario = null;
    private Model   model   =  null;
    private String  tipo    = "";
    
    private String  Ano      = "";
    private String  Mes      = "";
    private String  Distrito = "";
    private String  Agencia  = "";
    private String  Cliente  = "";
    private String  Estandar = "";
    private String  AgenciaDespacho  = "";
    int diaInicial ;
    int diaFinal;
    
    
    
    // variables del archivo de excel
    String   ruta;
    POIWrite xls;
    HSSFCellStyle header  , titulo1, titulo2, titulo3 , titulo4, letra, numero, porcentaje, letraCentrada, numeroNegrita;
    HSSFColor     cAzul   , cVerde, cAmarillo, cGris ;
    int           fila = 0;
    SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
    
    
    
    /** Crea una nueva instancia de  HExportarReportePTvsEJ */
    public HExportarReportePTvsEJ() {
    }
    
    
    public void start(Usuario usuario, String Ano, String Mes, String Distrito, String AgenciaDespacho, String Agencia, String Cliente, String Estandar, int diaInicial , int diaFinal, String tipo) throws Exception {
        model   = new Model(usuario.getBd());
        this.usuario = usuario;
        this.Ano     = Ano;
        this.Mes     = Mes;
        this.tipo    = tipo;
        this.Distrito = Distrito;
        this.Agencia  = Agencia;
        this.Cliente  = Cliente;
        this.Estandar = Estandar;
        this.AgenciaDespacho  = AgenciaDespacho;
        this.diaFinal  = diaFinal;
        this.diaInicial = diaInicial;
        this.run();
    }
    
    
    public synchronized void run(){
        try{
            if (tipo!=null){
                model.ReportesSvc.Presupuestado_VS_Ejecutado(Distrito, AgenciaDespacho, Agencia, Cliente, Estandar, Ano, Mes, diaInicial, diaFinal);
                List lista = model.ReportesSvc.getListadoList();
                
                if (tipo.equalsIgnoreCase("AGENCIA"))
                    generarReporteAgencia(lista);
                else if (tipo.equalsIgnoreCase("CLIENTE"))
                    generarReporteCliente(lista);
                else if (tipo.equalsIgnoreCase("ESTANDAR"))
                    generarReporteEstandar(lista);
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }
    
    
    
    
    
    public void InitArchivo(String nameFile) throws Exception{
        try{
            
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            ruta = rb.getString("ruta") + "/exportar/migracion/" + usuario.getLogin();
            File archivo = new File( ruta );
            if (!archivo.exists()) archivo.mkdirs();     
            
            xls          = new com.tsp.operation.model.beans.POIWrite();
            xls.nuevoLibro( ruta + "/" + nameFile );
            
            // colores
            cAzul       = xls.obtenerColor(153,204,255);
            cVerde      = xls.obtenerColor( 51,153,102);
            cAmarillo   = xls.obtenerColor(255,255,153);
            cGris       = xls.obtenerColor(192,192,192);
            
            // estilos
            header       = xls.nuevoEstilo("Tahoma", 14, true  , false, "text"  , HSSFColor.ORANGE.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
            titulo1      = xls.nuevoEstilo("Tahoma", 8 , true  , false, "text"  , xls.NONE  , xls.NONE , xls.NONE);
            titulo2      = xls.nuevoEstilo("Tahoma", 8 , true  , false, "text"  , HSSFColor.BLACK.index, cAzul.getIndex() , HSSFCellStyle.ALIGN_CENTER, 1);
            titulo3      = xls.nuevoEstilo("Tahoma", 8 , true  , false, "text"  , HSSFColor.WHITE.index, cAmarillo.getIndex() , HSSFCellStyle.ALIGN_CENTER, 2);
            titulo4      = xls.nuevoEstilo("Tahoma", 8 , true  , false, "text"  , HSSFColor.WHITE.index, cVerde.getIndex() , HSSFCellStyle.ALIGN_CENTER, 2);
            letra        = xls.nuevoEstilo("Tahoma", 8 , false , false, ""      , xls.NONE , xls.NONE , xls.NONE);
            letraCentrada= xls.nuevoEstilo("Tahoma", 8 , false , false, ""      , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_CENTER);
            numero       = xls.nuevoEstilo("Tahoma", 8 , false , false, "_(#,###_);(@_)"      , xls.NONE , xls.NONE , xls.NONE);
            numeroNegrita= xls.nuevoEstilo("Tahoma", 8 , true  , false, "_($* #,##0.00_);(@_)"      , xls.NONE , xls.NONE , xls.NONE);
            porcentaje   = xls.nuevoEstilo("Tahoma", 8 , false , false, "0.00%" , xls.NONE , xls.NONE , xls.NONE);
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception("Error en InitArchivo .... \n" + ex.getMessage());
        }
    }
    
    
    /**
     * Metodo para crear el  archivo de excel
     * @autor mfontalvo
     * @param nameFile, Nombre del archivo
     * @param titulo, titulo del archivo de excel
     * @throws Exception.
     */
    private void crearArchivo(String nameFile, String titulo) throws Exception{
        try{
            InitArchivo(nameFile);
            xls.obtenerHoja("Base");
            xls.combinarCeldas(0, 0, 0, 8);
            xls.combinarCeldas(1, 0, 1, 8);
            xls.combinarCeldas(2, 0, 2, 8);
            xls.combinarCeldas(3, 0, 3, 8);
            xls.adicionarCelda(0, 0, titulo, header);
            xls.adicionarCelda(1, 0, "Periodo " + Ano + Mes , titulo1);
            xls.adicionarCelda(2, 0, "Usuario " + usuario.getNombre() , titulo1);
            xls.adicionarCelda(3, 0, "Fecha reporte " + fmt.format( new Date() ) , titulo1);
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception( "Error en crearArchivo ....\n"  + ex.getMessage() );
        }
    }
    
    /**
     * Metodo para cerrar el  archivo de excel
     * @autor mfontalvo
     * @throws Exception.
     */
    private void cerrarArchivo() throws Exception {
        try{
            if (xls!=null)
                xls.cerrarLibro();
        }catch (Exception ex){
            throw new Exception( "Error en crearArchivo ....\n"  + ex.getMessage() );
        }
    }
    
   
    
    public void generarReporteAgencia (List listado) throws Exception{
        try{
            crearArchivo("ReporteAgencia" + Ano + Mes +".xls", "Viajes Presupuestado vs. Ejecutados");
            // cabecera
            int col = 0;
            String [] cabecera = { "Codigo",
                                   "Agencia", 
                                   "Pto Mensual", 
                                   "Pto a la fecha",  
                                   "Ejecutados", 
                                   "Viajes NP",
                                   "Diferencia" , 
                                   "% Cumplimiento"
                                 };
            short [] dimensiones = {
                3000, 6000, 4000, 4000, 4000, 4000, 4000,4000
            };            
            fila=5;
            for ( int i = 0; i<cabecera.length; i++){
                xls.adicionarCelda(fila  ,  i, cabecera[i], titulo2);
                xls.adicionarCelda(fila+1,  i, "", titulo2);
                xls.cambiarAnchoColumna(i, dimensiones [i]);
                xls.combinarCeldas(fila, i,  fila+1, i);
            }
            int inicio = dimensiones.length-1;
            for (int i = diaInicial; i<=(diaFinal*3) ; i+=3){
                xls.adicionarCelda(fila  ,  inicio + i    , UtilFinanzas.DiaSemana(Ano + Mes + UtilFinanzas.DiaFormat((i/3)+1)) + " " + ((i/3)+1), titulo2);
                xls.adicionarCelda(fila  ,  inicio + i+1  , "", titulo2);
                xls.adicionarCelda(fila  ,  inicio + i+2  , "", titulo2);
                xls.combinarCeldas(fila  ,  inicio + i,  fila , inicio + i+2);
                xls.adicionarCelda(fila+1,  inicio + i  , "PT", titulo2);
                xls.adicionarCelda(fila+1,  inicio + i+1, "EJ", titulo2);
                xls.adicionarCelda(fila+1,  inicio + i+2, "NP", titulo2);
                xls.cambiarAnchoColumna(inicio + i  , 3000);
                xls.cambiarAnchoColumna(inicio + i+1, 3000);
                xls.cambiarAnchoColumna(inicio + i+2, 3000);
            }
            fila+=2;   
            ////////////////////////////////////////////////////////////////////
            if (listado.size()>0){
                for (int i=0; i<listado.size(); i++){
                    ViajesAgencia ag = (ViajesAgencia) listado.get(i);
                    xls.adicionarCelda(fila, 0, ag.getAgencia()       , letraCentrada );
                    xls.adicionarCelda(fila, 1, ag.getAgenciaNombre() , letra         );
                    xls.adicionarCelda(fila, 2, ag.getTotalPt()       , numero );
		    xls.adicionarCelda(fila, 3, ag.getTotalPtF()      , numero );
                    xls.adicionarCelda(fila, 4, ag.getTotalEj()       , numero );
		    xls.adicionarCelda(fila, 5, ag.getTotalNP()       , numero );
		    xls.adicionarCelda(fila, 6, ag.getDiferencia()    , numero );  
                    xls.adicionarCelda(fila, 7, (ag.getPorcentajeIncumplimiento()/100) , porcentaje );
                    for (int j = diaInicial; j<=(diaFinal*3) ; j+=3){
                        xls.adicionarCelda(fila,  inicio + j  , ag.getViajePtdo ((j/3)+1), numero);
                        xls.adicionarCelda(fila,  inicio + j+1, ag.getViajeEjdo ((j/3)+1), numero);
                        xls.adicionarCelda(fila,  inicio + j+2, ag.getViajeNP   ((j/3)+1), numero);
                    }
                    fila ++;
                }
            }
            ////////////////////////////////////////////////////////////////////
            cerrarArchivo();
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception ("Error en generarReporteAgencia() " + ex.getMessage());
        }
    }

    
    
    
    public void generarReporteCliente (List listado) throws Exception{
        try{
            
            crearArchivo("ReporteCliente" + Ano + Mes +".xls", "Viajes Presupuestado vs. Ejecutados");
            // cabecera
            int col = 0;
            String [] cabecera = { "Cod Agencia",
                                   "Agencia", 
                                   "Cod Cliente",
                                   "Cliente", 
                                   "Pto Mensual", 
                                   "Pto a la fecha",  
                                   "Ejecutados", 
                                   "Viajes NP",
                                   "Diferencia" , 
                                   "% Cumplimiento"
                                 };
            short [] dimensiones = {
                3000, 6000, 3000, 8000, 4000, 4000, 4000, 4000, 4000,4000
            };            
            fila=5;
            for ( int i = 0; i<cabecera.length; i++){
                xls.adicionarCelda(fila  ,  i, cabecera[i], titulo2);
                xls.adicionarCelda(fila+1,  i, "", titulo2);
                xls.cambiarAnchoColumna(i, dimensiones [i]);
                xls.combinarCeldas(fila, i,  fila+1, i);
            }
            int inicio = dimensiones.length-1;
            for (int i = diaInicial; i<=(diaFinal*3) ; i+=3){
                xls.adicionarCelda(fila  ,  inicio + i    , UtilFinanzas.DiaSemana(Ano + Mes + UtilFinanzas.DiaFormat((i/3)+1)) + " " + ((i/3)+1), titulo2);
                xls.adicionarCelda(fila  ,  inicio + i+1  , "", titulo2);
                xls.adicionarCelda(fila  ,  inicio + i+2  , "", titulo2);
                xls.combinarCeldas(fila  ,  inicio + i,  fila , inicio + i+2);
                xls.adicionarCelda(fila+1,  inicio + i  , "PT", titulo2);
                xls.adicionarCelda(fila+1,  inicio + i+1, "EJ", titulo2);
                xls.adicionarCelda(fila+1,  inicio + i+2, "NP", titulo2);
                xls.cambiarAnchoColumna(inicio + i  , 3000);
                xls.cambiarAnchoColumna(inicio + i+1, 3000);
                xls.cambiarAnchoColumna(inicio + i+2, 3000);
            }
            fila+=2;   
            ////////////////////////////////////////////////////////////////////
            if (listado.size()>0){
                for (int i=0; i<listado.size(); i++){
                    ViajesAgencia ag = (ViajesAgencia) listado.get(i);
                    List listacl = (List) ag.getListadoClientesToList();
                    for (int ix_cl = 0 ; ix_cl < listacl.size(); ix_cl++){
                        ViajesCliente vc = (ViajesCliente) listacl.get(ix_cl);
                        xls.adicionarCelda(fila, 0, ag.getAgencia()       , letraCentrada );
                        xls.adicionarCelda(fila, 1, ag.getAgenciaNombre() , letra         );
                        xls.adicionarCelda(fila, 2, vc.getCliente()       , letraCentrada );
                        xls.adicionarCelda(fila, 3, vc.getClienteNombre() , letra         );
                        xls.adicionarCelda(fila, 4, vc.getTotalPt()       , numero );
                        xls.adicionarCelda(fila, 5, vc.getTotalPtF()      , numero );
                        xls.adicionarCelda(fila, 6, vc.getTotalEj()       , numero );
                        xls.adicionarCelda(fila, 7, vc.getTotalNP()       , numero );
                        xls.adicionarCelda(fila, 8, vc.getDiferencia()    , numero );  
                        xls.adicionarCelda(fila, 9, (vc.getPorcentajeIncumplimiento()/100) , porcentaje );
                        for (int j = diaInicial; j<=(diaFinal*3) ; j+=3){
                            xls.adicionarCelda(fila,  inicio + j  , vc.getViajePtdo ((j/3)+1), numero);
                            xls.adicionarCelda(fila,  inicio + j+1, vc.getViajeEjdo ((j/3)+1), numero);
                            xls.adicionarCelda(fila,  inicio + j+2, vc.getViajeNP   ((j/3)+1), numero);
                        }
                        fila ++;
                    }
                }
            }
            ////////////////////////////////////////////////////////////////////
            cerrarArchivo();
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception ("Error en generarReporteAgencia() " + ex.getMessage());
        }
    }   
    
    
    
    
    
    public void generarReporteEstandar (List listado) throws Exception{
        try{
            crearArchivo("ReporteEstandar" + Ano + Mes +".xls", "Viajes Presupuestado vs. Ejecutados");
            // cabecera
            int col = 0;
            String [] cabecera = { "Cod Agencia",
                                   "Agencia", 
                                   "Cod Cliente",
                                   "Cliente", 
                                   "Estandar",
                                   "Descripcion",
                                   "Pto Mensual", 
                                   "Pto a la fecha",  
                                   "Ejecutados", 
                                   "Viajes NP",
                                   "Diferencia" , 
                                   "% Cumplimiento"
                                 };
            short [] dimensiones = {
                3000, 6000, 3000, 8000, 3000, 8000, 4000, 4000, 4000, 4000, 4000,4000
            };            
            fila=5;
            for ( int i = 0; i<cabecera.length; i++){
                xls.adicionarCelda(fila  ,  i, cabecera[i], titulo2);
                xls.adicionarCelda(fila+1,  i, "", titulo2);
                xls.cambiarAnchoColumna(i, dimensiones [i]);
                xls.combinarCeldas(fila, i,  fila+1, i);
            }
            int inicio = dimensiones.length-1;
            for (int i = diaInicial; i<=(diaFinal*3) ; i+=3){
                xls.adicionarCelda(fila  ,  inicio + i    , UtilFinanzas.DiaSemana(Ano + Mes + UtilFinanzas.DiaFormat((i/3)+1)) + " " + ((i/3)+1), titulo2);
                xls.adicionarCelda(fila  ,  inicio + i+1  , "", titulo2);
                xls.adicionarCelda(fila  ,  inicio + i+2  , "", titulo2);
                xls.combinarCeldas(fila  ,  inicio + i,  fila , inicio + i+2);
                xls.adicionarCelda(fila+1,  inicio + i  , "PT", titulo2);
                xls.adicionarCelda(fila+1,  inicio + i+1, "EJ", titulo2);
                xls.adicionarCelda(fila+1,  inicio + i+2, "NP", titulo2);
                xls.cambiarAnchoColumna(inicio + i  , 3000);
                xls.cambiarAnchoColumna(inicio + i+1, 3000);
                xls.cambiarAnchoColumna(inicio + i+2, 3000);
            }
            fila+=2;   
            ////////////////////////////////////////////////////////////////////
            if (listado.size()>0){
                for (int i=0; i<listado.size(); i++){
                    ViajesAgencia ag = (ViajesAgencia) listado.get(i);
                    List listacl = (List) ag.getListadoClientesToList();
                    for (int ix_cl = 0 ; ix_cl < listacl.size(); ix_cl++){
                        ViajesCliente vc = (ViajesCliente) listacl.get(ix_cl);
                        List listast = (List) vc.getListaStandarToList();
                        for (int ix_st = 0; ix_st<listast.size(); ix_st++){
                            ViajesStandar vs = (ViajesStandar) listast.get(ix_st);
                            xls.adicionarCelda(fila, 0 , ag.getAgencia()       , letraCentrada );
                            xls.adicionarCelda(fila, 1 , ag.getAgenciaNombre() , letra         );
                            xls.adicionarCelda(fila, 2 , vc.getCliente()       , letraCentrada );
                            xls.adicionarCelda(fila, 3 , vc.getClienteNombre() , letra         );
                            xls.adicionarCelda(fila, 4 , vs.getStdJobNo()      , letraCentrada );
                            xls.adicionarCelda(fila, 5 , vs.getStdJobDesc()    , letra         );
                            xls.adicionarCelda(fila, 6 , vs.getTotalPt()       , numero );
                            xls.adicionarCelda(fila, 7 , vs.getTotalPtF()      , numero );
                            xls.adicionarCelda(fila, 8 , vs.getTotalEj()       , numero );
                            xls.adicionarCelda(fila, 9 , vs.getTotalNP()       , numero );
                            xls.adicionarCelda(fila, 10, vs.getDiferencia()    , numero );
                            xls.adicionarCelda(fila, 11, (vs.getPorcentajeIncumplimiento()/100) , porcentaje );
                            for (int j = diaInicial; j<=(diaFinal*3) ; j+=3){
                                xls.adicionarCelda(fila,  inicio + j  , vs.getViajePtdo((j/3)+1), numero);
                                xls.adicionarCelda(fila,  inicio + j+1, vs.getViajeEjdo((j/3)+1), numero);
                                xls.adicionarCelda(fila,  inicio + j+2, vs.getViajeNP  ((j/3)+1), numero);
                            }
                            fila ++;
                        }
                    }
                }
            }
            ////////////////////////////////////////////////////////////////////
            cerrarArchivo();
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception ("Error en generarReporteAgencia() " + ex.getMessage());
        }
    }      
}
