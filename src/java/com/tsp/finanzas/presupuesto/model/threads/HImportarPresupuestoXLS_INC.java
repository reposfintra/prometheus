
/*************************************************************
 * Nombre      ............... HImportarPresupuestoXLS_INC.java
 * Descripcion ............... Importa el presupuesto de ventas de un archivo de Excel
 *                             incrementado en un porcentaje
 * Autor       ............... mfontalvo
 * Fecha       ............... Septiembre - 21 - 2005
 * Version     ............... 1.0
 * Copyright   ............... Fintravalores S.A. S.A
 ************************************************************/



package com.tsp.finanzas.presupuesto.model.threads;



import com.tsp.finanzas.presupuesto.model.beans.*;
import com.tsp.finanzas.presupuesto.model.Model;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.util.UtilFinanzas;

import java.io.*;
import java.util.*;



public class HImportarPresupuestoXLS_INC extends Thread{
    private String Archivo;
    private String Hoja;
    private String Ano;
    private String Mes;
    private String Usuario;
    private Model  model;
    private String fileLog;
    private String Modo; // mensual // diario
    private String Accion; // A: Actualizar; E: Eliminar y Adicionar    
    private double PorcentajeIncremento;
    
    /** Creates a new instance of HImportXLS */
    public HImportarPresupuestoXLS_INC() {
    }
    
    public void start(String Archivo, String Ano, String Mes, String Modo, String Accion, Usuario Usuario, double PorcentajeIncremento) throws Exception{
        this.Hoja       = "BASE";
        this.Archivo    = Archivo;
        this.Ano        = Ano;
        this.Mes        = Mes;
        this.Usuario    = Usuario.getLogin();
        this.Modo       = Modo;
        this.Accion     = Accion;
        this.model      = new Model(Usuario.getBd());
        this.PorcentajeIncremento = PorcentajeIncremento;
        super.start();
        
    }
    
    public synchronized void run(){
        try{
            
            String ruta = UtilFinanzas.obtenerRuta("ruta", "/exportar/migracion/" + Usuario);
            fileLog     = ruta + "/LogImportarPtoINCXLS_"+ Ano + Mes +".csv";
          
            ///////////////////////////////////////////////////////////////////
            
            FileWriter fw = new FileWriter(this.fileLog);
            
            
            wLog(fw,"Iniciando Proceso ...");
            try{
                Hashtable registro = new Hashtable();
                /////////////////////////////////////////////////////////////////////////////////////////////
                int i,
                Fila            = 6,  // Se empieza a contar desde aqui
                colStdjob       = 1,  // Columnas de inicio de viajes
                colTotalMes     = 11, // Columnas de inicio de viajes
                colInicio       = 12, // Columnas de inicio de viajes 
                FilasProcesadas = 0,
                FilasRechazadas = 0,
                TotalFilas      = 0;
                String Distrito = "FINV";
                /////////////////////////////////////////////////////////////////////////////////////////////
                
                if (Accion.equals("E")){
                    wLog(fw, "Buscando Eliminado datos ....");
                    model.DPtoSvc.DELETE(Distrito, Ano, Mes);
                }

                
                /////////////////////////////////////////////////////////////////////////////////////////////
                
                wLog(fw, "Buscando Archivo ["+ Archivo +"]");
                JXLRead xls = new JXLRead ( Archivo );
                
                wLog(fw, "Buscando Hoja ["+ Hoja +"]");
                xls.obtenerHoja( Hoja );
                
                
                if (xls.obtenerHoja() != null){
                    wLog(fw, "Iniciando Lectura");
                    
                    wLog(fw, "Fila Archivo,Procesado,Estandar,Viajes Mensuales,Agencia,Cliente,Comentario");
                    
                    TotalFilas = xls.numeroFilas();
                    for (i=Fila;i< TotalFilas ; i++){
                        
                        String Stdjob   = xls.obtenerDato(i, colStdjob  );
                        String CMensual = xls.obtenerDato(i, colTotalMes);
                        
                        if (Stdjob!=null && !Stdjob.trim().equals("") && Stdjob.trim().length() == 6 && CMensual!=null && !CMensual.trim().equals("") && !CMensual.trim().equals("0")){
                            
                            
                            String Cliente  = "000" + Stdjob.substring(0,3);
                            model.DPtoSvc.AgenciaDuenaCliente(Cliente);
                            String Agencia  = model.DPtoSvc.getInfoCliente();
                            
                            String CDias[]  = new String [31];
                            if (Modo.equals("D"))
                                
                                CMensual = "0";
                                int diaFinal = Integer.parseInt( UtilFinanzas.diaFinal(Ano, Mes) );
                                for (int col = colInicio ; col < colInicio + diaFinal; col++){
                                    
                                    String content = xls.obtenerDato(i, col);
                                    content = (content==null || content.trim().equals("")?"0":content.trim());
                                    int valor = Integer.parseInt(content) +
                                                Incremento( Integer.parseInt(content) );
                                    CDias[(col-colInicio)] = String.valueOf(valor);
                                    CMensual = String.valueOf(Integer.parseInt(String.valueOf(CMensual)) + Integer.parseInt(String.valueOf(valor)));
                                }
                                
                                for (int col = colInicio + diaFinal ; col<colInicio + 31; col++){
                                    
                                    String content = xls.obtenerDato(i, col);
                                    content = (content==null || content.trim().equals("")?"0":content.trim());
                                    int valor = Integer.parseInt(content) +
                                                Incremento( Integer.parseInt(content) );
                                    CDias[(col-colInicio)] = "0";
                                    CDias[diaFinal-1] = String.valueOf(Integer.parseInt(String.valueOf(CDias[diaFinal-1])) + Integer.parseInt(String.valueOf(valor)));
                                    CMensual = String.valueOf(Integer.parseInt(String.valueOf(CMensual)) + Integer.parseInt(String.valueOf(valor)));
                                }                                
                            //////////////////////////////////////////////////////////
                            if (Stdjob.length()==6){
                                if (registro.get(Stdjob)==null)
                                    wLog(fw, (i+1) +",Si," + Stdjob + "," + CMensual + "," + Agencia + ",\"" + Cliente + "\"," + "Ok" );
                                else
                                    wLog(fw, (i+1) +",Si," + Stdjob + "," + CMensual + "," + Agencia + ",\"" + Cliente + "\"," + "Ok - Este Estandar ya se habia registrado - Linea " + registro.get(Stdjob));
                                
                                
                                if (Modo.equals("D"))
                                    model.DPtoSvc.INSERT_DIARIO( Distrito, Ano, Mes, Stdjob, Agencia, Cliente, CMensual, CDias, this.Usuario);
                                else
                                    model.DPtoSvc.INSERT_MENSUAL( Distrito, Ano, Mes, Stdjob, Agencia, Cliente, CMensual, this.Usuario);
                                registro.put(Stdjob ,  String.valueOf(i+1));
                                FilasProcesadas++;
                                
                            }
                            else{
                                wLog(fw, (i+1) +",No,"+ Stdjob +","+ CMensual +",-,-,Longitud de estandar incorrecto");
                                FilasRechazadas++;
                            }
                            //////////////////////////////////////////////////////////
                            
                        }
                        else{
                            wLog(fw, (i+1) +",No,"+ Stdjob +","+ CMensual +",-,-,Estandar o cantidad no aplican");
                            FilasRechazadas++;
                        }
                    }
                }else
                    wLog(fw , "No se pudo encontrar la Hoja ["+ this.Hoja +"]");
                xls.cerrarLibro();
                
                wLog(fw, "\nEstado");
                wLog(fw, "Filas Procesadas, "+ FilasProcesadas );
                wLog(fw, "Filas Rechazadas, "+ FilasRechazadas );
                wLog(fw, "Total Filas     ," + TotalFilas      );
                wLog(fw, "Proceso Terminado");
                
            }catch (Exception ex){
                wLog(fw, ex.getMessage());
            }
            finally{
                fw.close();
            }
        }catch (Exception ex){
            ////System.out.println("Error [HImportarPresupuestoXLS_INC]: "+ex.getMessage());
        }
    }

    public int Incremento(int valor){
        // redondear.mas
        // return (int) Math.ceil ( valor *  (PorcentajeIncremento/100) );
        // solo redondear
        return (int) Math.round( valor *  (PorcentajeIncremento/100) );
    }    
    public void wLog(FileWriter fw, String msg) throws Exception{
        if (fw!=null) fw.write( "\n" + (new java.util.Date()).toString() + "," + msg);
    }
    /*
   public static void main(String args[]) throws Exception{
        HImportarPresupuestoXLS_INC h = new HImportarPresupuestoXLS_INC();
        //h.start("c:/Presupuesto Carbon.xls"   , "2006", "01", "D", "E", "fvillacob", 10);
        //h.start("c:/Presupuesto Carbon.xls"   , "2006", "02", "D", "E", "fvillacob", 13);
        //h.start("c:/Presupuesto Carbon.xls"   , "2006", "03", "D", "E", "fvillacob", 16);
        //h.start("c:/Presupuesto Carbon.xls"   , "2006", "04", "D", "E", "fvillacob", 17);
        //h.start("c:/Presupuesto Carbon.xls"   , "2006", "05", "D", "E", "fvillacob", 18);
        //h.start("c:/Presupuesto Carbon.xls"   , "2006", "06", "D", "E", "fvillacob", 19);
        //h.start("c:/Presupuesto Carbon.xls"   , "2006", "07", "D", "E", "fvillacob", 20);
        //h.start("c:/Presupuesto Carbon.xls"   , "2006", "08", "D", "E", "fvillacob", 21);
        //h.start("c:/Presupuesto Carbon.xls"   , "2006", "09", "D", "E", "fvillacob", 22);
        //h.start("c:/Presupuesto Carbon.xls"   , "2006", "10", "D", "E", "fvillacob", 23);
        h.start("c:/Presupuesto Carbon.xls"   , "2006", "11", "D", "E", "fvillacob", 24);
        //h.start("c:/Presupuesto Carbon.xls"   , "2006", "12", "D", "E", "fvillacob", 25);
        
    }
    */
}
