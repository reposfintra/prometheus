
/*************************************************************
 * Nombre      ............... HImportarPresupuestoXLS.java
 * Descripcion ............... Importa el presupuesto de ventas desde un
 *                             archivo de excel (formato programa exportado de pto)
 * Autor       ............... mfontalvo
 * Fecha       ............... Junio - 14 - 2005
 * Version     ............... 1.0
 * Copyright   ............... Fintravalores S.A. S.A
 ************************************************************/

package com.tsp.finanzas.presupuesto.model.threads;

import com.tsp.finanzas.presupuesto.model.Model;
import com.tsp.util.connectionpool.*;
import com.tsp.finanzas.presupuesto.model.beans.*;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.util.*;

import java.io.*;
import java.text.*;
import java.sql.*;
import java.util.*;
import jxl.Sheet;



public class HImportarPresupuestoXLS extends Thread{
    private String Archivo;
    private String Hoja;
    private String Ano;
    private String Mes;
    private String Usuario;
    private Model  model;
    private String fileLog;
    private String Modo; // mensual // diario
    private String Accion; // A: Actualizar; E: Eliminar y Adicionar    
    private SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMddkkmmss");
    
    /** Creates a new instance of HImportXLS */
    public HImportarPresupuestoXLS() {
    }
    
    public void start(String Archivo, String Ano, String Mes, String Modo, String Accion, Usuario Usuario) throws Exception{
        this.Hoja       = "BASE";
        this.Archivo    = Archivo;
        this.Ano        = Ano;
        this.Mes        = Mes;
        this.Usuario    = Usuario.getLogin();
        this.Modo       = Modo;
        this.Accion     = Accion;
        this.model      = new Model(Usuario.getBd());
        super.start();
        
    }
    
    public synchronized void run(){
        try{
            
            String ruta = UtilFinanzas.obtenerRuta("ruta", "/exportar/migracion/" + Usuario);
            fileLog     = ruta + "/LogImportarPtoXLS_"+ Ano + Mes + "_" + fmt.format(new java.util.Date()) + ".csv";
            
            
            // Inicializando Archivo de log de Errores
            FileWriter fw = new FileWriter(this.fileLog);
            wLog(fw,"Iniciando Proceso ...");
            
            try{
                   
                // variable de control que almacena los estandares ya procesados
                TreeMap registro = new TreeMap();
                
                
                /////////////////////////////////////////////////////////////////////////////////////////////
                // parametros para el archivo de Excel
                int i,
                Fila            = 7,  // Se empieza a contar desde aqui
                colStdjob       = 1,  
                colAgencia      = 3,  
                colCliente      = 5,  
                colTotalMes     = 16, 
                colInicio       = 18, 
                FilasProcesadas = 0,
                FilasRechazadas = 0,
                TotalFilas      = 0;
                String Distrito = "FINV";
                
                
                /////////////////////////////////////////////////////////////////////////////////////////////
                // eliminacion del presupuesto actual
                if (Accion.equals("E")){
                    wLog(fw, "Buscando Eliminado datos ....");
                    model.DPtoSvc.DELETE(Distrito, Ano, Mes);
                }

                /////////////////////////////////////////////////////////////////////////////////////////////
                // Busqueda del archivo y la hoja base
                wLog(fw, "Buscando Archivo ["+ Archivo +"]");
                JXLRead xls = new JXLRead ( Archivo );
                
                wLog(fw, "Buscando Hoja ["+ Hoja +"]");
                xls.obtenerHoja( Hoja );
                
                // si el libro y la hoja estan correctos
                if (xls.obtenerHoja() != null){
                    
                    
                    wLog(fw, "Iniciando Lectura");
                    wLog(fw, "Fila Archivo,Procesado,Estandar,Viajes Mensuales,Agencia,Cliente,Comentario");
                    
                    
                    TotalFilas = xls.numeroFilas();
                    for (i=Fila;i< TotalFilas ; i++){
                        
                        String Stdjob   = xls.obtenerDato(i, colStdjob  );
                        String CMensual = getViaje(xls.obtenerHoja() , i,  colTotalMes);
                        String Agencia  = xls.obtenerDato(i, colAgencia );
                        String Cliente  = xls.obtenerDato(i, colCliente );
                        
                        if (Stdjob!=null && !Stdjob.trim().equals("") && Stdjob.trim().length() == 6 && CMensual!=null && !CMensual.trim().equals("") && !CMensual.trim().equals("0")){
                            
                            String CDias[]  = new String [31];
                            int diasMes = Integer.parseInt(UtilFinanzas.diaFinal(Ano,Mes));
                            if (Modo.equals("D"))
                                for (int col = colInicio ; col<colInicio + diasMes; col++){
                                    CDias[(col-colInicio)] = getViaje(xls.obtenerHoja() , i, col); 
                                }
                            //////////////////////////////////////////////////////////
                            if (Stdjob.length()==6){
                                if (registro.get(Stdjob)==null)
                                    wLog(fw, (i+1) +",Si," + Stdjob + "," + CMensual + "," + Agencia + ",\"" + Cliente + "\"," + "Ok" );
                                else
                                    wLog(fw, (i+1) +",Si," + Stdjob + "," + CMensual + "," + Agencia + ",\"" + Cliente + "\"," + "Ok - Este Estandar ya se habia registrado - Linea " + registro.get(Stdjob));
                                
                                
                                if (Modo.equals("D"))
                                    model.DPtoSvc.INSERT_DIARIO(Distrito, Ano, Mes, Stdjob, Agencia, Cliente, CMensual, CDias, this.Usuario);
                                else
                                    model.DPtoSvc.INSERT_MENSUAL(Distrito, Ano, Mes, Stdjob, Agencia, Cliente, CMensual, this.Usuario);
                                registro.put(Stdjob ,  String.valueOf(i+1));
                                FilasProcesadas++;
                                
                            }
                            else{
                                wLog(fw, (i+1) +",No,"+ Stdjob +","+ CMensual +",-,-,Longitud de estandar incorrecto");
                                FilasRechazadas++;
                            }
                            //////////////////////////////////////////////////////////
                            
                        }
                        else{
                            wLog(fw, (i+1) +",No,"+ Stdjob +","+ CMensual +",-,-,Estandar o cantidad no aplican");
                            FilasRechazadas++;
                        }
                    }
                }else
                    wLog(fw , "No se pudo encontrar la Hoja ["+ this.Hoja +"]");
                xls.cerrarLibro();
                
                wLog(fw, "\nEstado");
                wLog(fw, "Filas Procesadas, "+ FilasProcesadas );
                wLog(fw, "Filas Rechazadas, "+ FilasRechazadas );
                wLog(fw, "Total Filas     ," + TotalFilas      );
                wLog(fw, "Proceso Terminado");
                
            }catch (Exception ex){
                wLog(fw, ex.getMessage());
            }
            finally{
                fw.close();
            }
        }catch (Exception ex){
            ////System.out.println("Error [HImportarPresupuestoXLS]: "+ex.getMessage());
        }
    }
    
    
    
    /**
     * Procedimiento para imprimir los datos en el log.
     * @params fw .... Apuntador al archivo del log
     * @params msg ... Mensaje a imprimir
     */
    public void wLog(FileWriter fw, String msg) throws Exception{
        if (fw!=null) fw.write( "\n" + (new java.util.Date()).toString() + "," + msg);
    }
    
    /**
     * Metodo para obtener los viajes de un estandar;
     * @param sheet, Hoja donde se extraeran los datos
     * @param fila, fila del viaje
     * @param columna, columna del viaje
     * @return String, numero de Viajes
     * @throws Exception.
     */
    public String getViaje (Sheet sheet, int fila, int columna) throws Exception {
        if (sheet==null)
            throw new Exception ( "No se encontro referencia a la hoja del libro, al leer los viajes");
        String content = sheet.getCell(columna, fila).getContents();
        content = String.valueOf( (int) Float.parseFloat(content==null || content.trim().equals("")?"0": content.trim()));
        return content;
    }  
    
}
