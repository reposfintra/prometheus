
/*************************************************************
 * Nombre      ............... HExportarPresupuestoXLS.java
 * Descripcion ............... Exporta el presupuesto de ventas a un archivo de
 *                             excel dado un periodo
 * Autor       ............... mfontalvo
 * Fecha       ............... Septiembre - 21 - 2005
 * Version     ............... 1.0
 * Copyright   ............... Fintravalores S.A. S.A
 ************************************************************/



package com.tsp.finanzas.presupuesto.model.threads;


import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;

import com.tsp.finanzas.presupuesto.model.beans.*;
import com.tsp.finanzas.presupuesto.model.Model;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.util.UtilFinanzas;

import java.util.*;


public class HExportarPresupuestoXLS extends Thread{
    
    String Ano;
    String Mes;
    String Usuario ;
    Model model;
    
    public void start(String Ano, String Mes, Usuario Usuario) throws Exception{
        Model model = new Model(Usuario.getBd());
        this.Ano     = Ano;
        this.Mes     = Mes;
        this.Usuario = Usuario.getLogin();
        super.start();
    }
    
    public synchronized void run(){
        try{
            model.ReportesSvc.VentasPresupuestadas(Ano, Mes);
            List    lista = (List)    model.ReportesSvc.getListadoXLS().get("viajes");
            TreeMap tasas = (TreeMap) model.ReportesSvc.getListadoXLS().get("tasas");
            
            if (lista!=null && lista.size()>0){
                String ruta = UtilFinanzas.obtenerRuta ("ruta", "/exportar/migracion/" + Usuario);
                
                ////System.out.println(ruta);
                POIWrite xls = new POIWrite(ruta + "/Presupuesto_"+ Ano + Mes +".xls");
                //POIWrite xls = new POIWrite("c:/Presupuesto_"+ Ano + Mes +".xls");
                
                
                HSSFCellStyle fecha  = xls.nuevoEstilo("Book Antiqua", 9, false , false, "yyyy/mm/dd"  , xls.NONE , xls.NONE , xls.NONE );
                HSSFCellStyle texto  = xls.nuevoEstilo("Book Antiqua", 9, false , false, "text"        , xls.NONE , xls.NONE , xls.NONE);
                HSSFCellStyle numero = xls.nuevoEstilo("Book Antiqua", 9, false , false, ""            , xls.NONE , xls.NONE , xls.NONE);
                
                HSSFCellStyle header      = xls.nuevoEstilo("Arial Black", 18, true  , false, "text"        , HSSFColor.ORANGE.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
                HSSFCellStyle titulo      = xls.nuevoEstilo("Book Antiqua", 9, true  , false, "text"        , HSSFColor.WHITE.index , HSSFColor.DARK_GREEN.index , HSSFCellStyle.ALIGN_CENTER);
                HSSFCellStyle fechatitle  = xls.nuevoEstilo("Book Antiqua", 9, true  , false, "yyyy/mm/dd"  , HSSFColor.WHITE.index , HSSFColor.DARK_GREEN.index , HSSFCellStyle.ALIGN_CENTER );
                
                
                xls.obtenerHoja("BASE");
                xls.cambiarMagnificacion(3,4);
                
                // cabecera
                
                xls.adicionarCelda(0,0, "TRANSPORTE SANCHEZ POLO", header);
                xls.combinarCeldas(0, 0, 0, 2);
                
                
                xls.adicionarCelda(1,0, "Fecha "   , texto);
                xls.adicionarCelda(1,1, new Date() , fecha);
                
                xls.adicionarCelda(2,0, "Tasas de cambio"   , texto);
                xls.adicionarCelda(3,0, "DOLAR"             , texto);
                xls.adicionarCelda(3,1, Double.parseDouble(tasas.get("DOL").toString())  , numero);
                xls.adicionarCelda(4,0, "BOLIVAR"           , texto);
                xls.adicionarCelda(4,1, Double.parseDouble(tasas.get("BOL").toString())  , numero);
                
                // subtitulos
                
                
                int fila = 6;
                int col  = 0;
                
                xls.adicionarCelda(fila ,col++ , "Distrito"          , titulo );
                xls.adicionarCelda(fila ,col++ , "Num Estandar"      , titulo );
                xls.adicionarCelda(fila ,col++ , "Desc Estandar"     , titulo );
                xls.adicionarCelda(fila ,col++ , "Cod Agencia"       , titulo );
                xls.adicionarCelda(fila ,col++ , "Agencia"           , titulo );
                xls.adicionarCelda(fila ,col++ , "CodCliente"        , titulo );
                xls.adicionarCelda(fila ,col++ , "Cliente"           , titulo );
                xls.adicionarCelda(fila ,col++ , "Cod Origen"        , titulo );
                xls.adicionarCelda(fila ,col++ , "Origen"            , titulo );
                xls.adicionarCelda(fila ,col++ , "Cod Destino"       , titulo );
                xls.adicionarCelda(fila ,col++ , "Destino"           , titulo );
                xls.adicionarCelda(fila ,col++ , "UW"                , titulo );
                xls.adicionarCelda(fila ,col++ , "Tipo Viaje"        , titulo );
                xls.adicionarCelda(fila ,col++ , "Recurso"           , titulo );
                xls.adicionarCelda(fila ,col++ , "Tarifa"            , titulo);
                xls.adicionarCelda(fila ,col++ , "Moneda"            , titulo );
                xls.adicionarCelda(fila ,col++ , "Total"             , titulo );
                xls.adicionarCelda(fila ,col++ , "Total Pto"         , titulo );
                
                Date fechaInicial = new Date(Ano + "/" + Mes +"/01");
                for (int i=0;i<31;i++, fechaInicial.setDate(fechaInicial.getDate()+1)   ) xls.adicionarCelda(fila ,col++ , fechaInicial  , fechatitle );
                
                // datos
                
                Iterator it = lista.iterator();
                
                while(it.hasNext()){
                    fila++;
                    col = 0;
                    DatosGeneral datos = (DatosGeneral) it.next();
                    xls.adicionarCelda(fila ,col++ , datos.getValor("DISTRITO")      , texto );
                    xls.adicionarCelda(fila ,col++ , datos.getValor("STDJOBNO")      , texto );
                    xls.adicionarCelda(fila ,col++ , datos.getValor("STDJOBDESC")    , texto );
                    xls.adicionarCelda(fila ,col++ , datos.getValor("CODIGOAGENCIA") , texto );
                    xls.adicionarCelda(fila ,col++ , datos.getValor("NOMBREAGENCIA") , texto );
                    xls.adicionarCelda(fila ,col++ , datos.getValor("CODIGOCLIENTE") , texto );
                    xls.adicionarCelda(fila ,col++ , datos.getValor("NOMBRECLIENTE") , texto );
                    xls.adicionarCelda(fila ,col++ , datos.getValor("CODIGOORIGEN")  , texto );
                    xls.adicionarCelda(fila ,col++ , datos.getValor("NOMBREORIGEN")  , texto );
                    xls.adicionarCelda(fila ,col++ , datos.getValor("CODIGODESTINO") , texto );
                    xls.adicionarCelda(fila ,col++ , datos.getValor("NOMBREDESTINO") , texto );
                    xls.adicionarCelda(fila ,col++ , datos.getValor("UW")            , texto );
                    xls.adicionarCelda(fila ,col++ , datos.getValor("TIPOVIAJE")     , texto );
                    xls.adicionarCelda(fila ,col++ , datos.getValor("RECURSO")       , texto );
                    xls.adicionarCelda(fila ,col++ , Double.parseDouble(datos.getValor("TARIFA"))   , numero);
                    xls.adicionarCelda(fila ,col++ , datos.getValor("MONEDA")        , texto );
                    xls.adicionarCelda(fila ,col++ , Double.parseDouble(datos.getValor("TOTAL"))    , numero );
                    xls.adicionarCelda(fila ,col++ , Double.parseDouble(datos.getValor("TOTALPTO")) , numero );
                    
                    
                    for (int i=0;i<31;i++) xls.adicionarCelda(fila ,col++ , Double.parseDouble(datos.getValor("V"+ i)),  numero );
                        
                    
                }
                xls.cerrarLibro();
            }
            
        }catch (Exception ex){
            ex.printStackTrace();
            ////System.out.println("Error : " + ex.getMessage());
        }
    }
    
   /* 
    public static void main(String []args)throws Exception{
        
        HExportarPresupuestoXLS hilo = new HExportarPresupuestoXLS();
        hilo.start("2006","01", "KREALES");
        
    }*/
}
