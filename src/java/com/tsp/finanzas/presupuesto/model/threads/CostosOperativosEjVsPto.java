/***************************************************************************
 * Nombre clase :                 CostosOperativosEjVsPto.java             *
 * Descripcion :                  Hilo que permite la creacion del reporte *
 *                                de Presupuesto de Costos Operativos      *
 *                                Ejecutados Vs Presupuestados en un       *
 *                                archivo en excel                         *
 * Autor :                          Ing. Juan Manuel Escandon Perez        *
 * Fecha :                          10 de diciembre de 2005, 04:52 PM      *
 * Version :                        1.0                                    *
 * Copyright :                      Fintravalores S.A.                *
 ***************************************************************************/
package com.tsp.finanzas.presupuesto.model.threads;

import java.text.*;
import java.util.Date;
import java.text.*;
import java.sql.SQLException;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import com.tsp.finanzas.presupuesto.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.util.*;
import java.util.*;
import java.io.*;

/**
 *
 * @author  JESCANDON
 */
public class CostosOperativosEjVsPto extends Thread{
    private List Listado;
    private String ano;
    private String mes;
    private String id;
    private String agencia;
    private String cliente;
    
    /** Creates a new instance of CostosOperativosEjVsPto */
    public CostosOperativosEjVsPto() {
    }
    
    public void start(List lista, String id, String ano, String mes, String Agencia, String Cliente){
        this.id= id;
        this.Listado = lista;
        this.ano= ano;
        this.mes = mes;
        this.agencia = Agencia;
        this.cliente = Cliente;
        super.start();
    }
    public synchronized void run(){
        try{
            if (Listado!=null && Listado.size()>0){
                ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
                String path = rb.getString("ruta") + "/exportar/migracion/"+id;
                File file = new File(path);
                file.mkdirs();
                POIWrite xls = new POIWrite(path+"/CostosOperativosEjVsPto"+agencia+"-"+cliente+"-"+ano+"-"+mes+".xls");
                // fuente, tama�o, negrita, cursiva, formato, color, fondo, alineado
                HSSFCellStyle fecha  = xls.nuevoEstilo("verdana", 12, false , false, "yyyy/mm/dd"  , xls.NONE , xls.NONE , xls.NONE );
                HSSFCellStyle texto  = xls.nuevoEstilo("verdana", 12, false , false, "text"        , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_CENTER);
                HSSFCellStyle texto_fondo  = xls.nuevoEstilo("verdana", 12, false , false, "text"        , xls.NONE , HSSFColor.GREY_25_PERCENT.index , HSSFCellStyle.ALIGN_CENTER);
                HSSFCellStyle numero       = xls.nuevoEstilo("verdana", 12, false , false, ""            , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_RIGHT);
                HSSFCellStyle negrita      = xls.nuevoEstilo("verdana", 12, true  , false, "text"        , HSSFColor.BLACK.index , xls.NONE , xls.NONE);
                HSSFCellStyle header      = xls.nuevoEstilo("verdana", 18, true  , false, "text"        , HSSFColor.WHITE.index , HSSFColor.ORANGE.index , HSSFCellStyle.ALIGN_CENTER);
                HSSFCellStyle titulo      = xls.nuevoEstilo("verdana", 12, true  , false, "text"        , HSSFColor.WHITE.index , HSSFColor.LIGHT_BLUE.index , HSSFCellStyle.ALIGN_CENTER);
                HSSFCellStyle fechatitle  = xls.nuevoEstilo("verdana", 12, true  , false, "yyyy/mm/dd"  , HSSFColor.WHITE.index , HSSFColor.DARK_GREEN.index , HSSFCellStyle.ALIGN_CENTER );
                
                xls.obtenerHoja("PERIODO"+ano+mes);
                xls.cambiarMagnificacion(3,4);
                
                xls.adicionarCelda(0,0, "Fecha Proceso: "   , negrita);
                xls.adicionarCelda(0,1, ano + " - " + mes , fecha);
                xls.adicionarCelda(1,0, "Elaborado Por: "   , negrita);
                xls.adicionarCelda(1,1, id , fecha);
                
                int fila = 3;
                int col  = 0;
                
                xls.adicionarCelda(fila ,col++ , "ESTANDAR"                             , titulo );
                xls.adicionarCelda(fila ,col++ , "DESCRIPCION"                          , titulo );
                xls.adicionarCelda(fila ,col++ , "COD AG"                               , titulo );
                xls.adicionarCelda(fila ,col++ , "AGENCIA"                              , titulo );
                xls.adicionarCelda(fila ,col++ , "COD CL"                               , titulo );
                xls.adicionarCelda(fila ,col++ , "CLIENTE"                              , titulo );
                xls.adicionarCelda(fila ,col++ , "VALOR UNITARIO CO"                    , titulo );
                xls.adicionarCelda(fila ,col++ , "FECHA"                                , titulo );
                xls.adicionarCelda(fila ,col++ , "TIPO DE COSTO OPERATIVO"              , titulo );
                xls.adicionarCelda(fila ,col++ , "# VIAJES"                             , titulo );
                xls.adicionarCelda(fila ,col++ , "VALOR TOTAL OPERATIVO"                , titulo );
                xls.adicionarCelda(fila ,col++ , "ELEMENTO"                             , titulo );
                xls.adicionarCelda(fila ,col++ , "A�O"                                  , titulo );
                xls.adicionarCelda(fila ,col++ , "MES"                                  , titulo );
                xls.adicionarCelda(fila ,col++ , "DIA"                                  , titulo );
                
                int fechaActual = Integer.parseInt(UtilFinanzas.getFechaActual_String(8));
                for (int i_ag = 0 ; i_ag < Listado.size(); i_ag++){
                    
                    ViajesAgencia ag = (ViajesAgencia) Listado.get(i_ag);
                    for (int i_cl = 0 ; i_cl < ag.getListadoClientes().size(); i_cl++)      {
                        ViajesCliente cl = (ViajesCliente) ag.getListadoClientes().get(i_cl);
                        for (int i_st = 0 ; i_st < cl.getListaStandar().size(); i_st++)      {
                            ViajesStandar st = (ViajesStandar) cl.getListaStandar().get(i_st);
                            for (int i_co = 0 ; i_co < st.getListCostosOperativos().size(); i_co++)      {
                                CostosOperativos co = (CostosOperativos) st.getListCostosOperativos().get(i_co);
                                for (int ix_co = 1 ; ix_co <= 31 ; ix_co++ ){
                                    int fec = Integer.parseInt( ano + mes + UtilFinanzas.DiaFormat(ix_co) );
                                    String tipo = ((fec <= fechaActual)?"PRESUPUESTADO":"EJECUTADO");
                                    
                                    if((( st.getViajePtdo(ix_co) > 0 ) && tipo.equals("PRESUPUESTADO"))  || (( st.getViajeEjdo(ix_co) > 0 ) && tipo.equals("EJECUTADO"))  ){
                                        fila++;
                                        col = 0;
                                        
                                        
                                        xls.adicionarCelda(fila ,col++ , st.getStdJobNo()                                       , texto );//Estandart
                                        xls.adicionarCelda(fila ,col++ , st.getStdJobDesc()                                     , texto );//Descripcion Estandart
                                        xls.adicionarCelda(fila ,col++ , ag.getAgencia()                                        , texto );//Cod Agencia
                                        xls.adicionarCelda(fila ,col++ , ag.getAgenciaNombre()                                  , texto );//Agencia nombre
                                        xls.adicionarCelda(fila ,col++ , cl.getCliente()                                        , texto );//Cod Cliente
                                        xls.adicionarCelda(fila ,col++ , cl.getClienteNombre()                                  , texto );//Cliente
                                        xls.adicionarCelda(fila ,col++ , co.getValor()                                          , numero);//Valor unitario
                                        xls.adicionarCelda(fila ,col++ , co.getAno() + "-" + co.getMes()                        , texto );//Periodo
                                        xls.adicionarCelda(fila ,col++ , tipo                                                   , texto );//
                                        xls.adicionarCelda(fila ,col++ , (tipo.equals("EJECUTADO")?st.getViajeEjdo(ix_co):st.getViajePtdo(ix_co)) , numero );//# de Viajes
                                        xls.adicionarCelda(fila ,col++ , (tipo.equals("EJECUTADO")?st.getViajeEjdo(ix_co)*co.getValor():st.getViajePtdo(ix_co)*co.getValor()) , numero );//Costos Operativos
                                        xls.adicionarCelda(fila ,col++ , co.getElemento()                                       , texto );//Elemento
                                        xls.adicionarCelda(fila ,col++ , st.getAno()                                            , texto );//fecha foto placa
                                        xls.adicionarCelda(fila ,col++ , st.getMes()                                            , texto );//hora foto placa
                                        xls.adicionarCelda(fila ,col++ , ix_co                                                  , texto );//fecha foto conductor
                                    }
                                    
                                    
                                }//fin dias
                            }//fin Coperativos
                        }//fin standart
                    }//fin cliente
                }//fin agencia
                xls.cerrarLibro();
            }//fin if
        }catch(Exception e){
            e.getMessage();
        }
        
    }
}
