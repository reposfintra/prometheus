/*************************************************************
 * Nombre      ............... Reprogramacion.java
 * Descripcion ............... bean para encapsular los datos de 
 *                             la reprogramacion
 * Autor       ............... mfontalvo
 * Fecha       ............... Noviembre - 17 - 2005
 * Version     ............... 1.0
 * Copyright   ............... Fintravalores S.A. S.A
 ************************************************************/

package com.tsp.finanzas.presupuesto.model.beans;


public class Reprogramacion {
    
    private String Estado;
    private String Codigo;
    private String Descripcion;
    
    private String Estandar;
    private String FechaCreacion;
    private String UsuarioCreacion;
    private String FechaViaje;
    private String ValorAnterior;
    private String ValorNuevo;
    
    public Reprogramacion() {
    }
    
    /**
     * Obtiene codigo de causa
     * @autor ......... mfontalvo
     * @return .. devuele el codigo de la reprogramacion
     */
    public java.lang.String getCodigo() {
        return Codigo;
    }
    
    /**
     * Modifica el codigo de causa
     * @autor ......... mfontalvo
     * @param Codigo .. Vuevo valor del Codigo de causa.
     */
    public void setCodigo(java.lang.String Codigo) {
        this.Codigo = Codigo;
    }
    
    /**
     * Obtiene la descripcion de la causa.
     * @autor ...... mfontalvo
     * @return ..... devuelve descripcion de la causa
     */
    public java.lang.String getDescripcion() {
        return Descripcion;
    }
    
    /**
     * Modifica la descripcion de la causa
     * @autor ......... mfontalvo
     * @param Descripcion nueva descripcion de la causa
     */
    public void setDescripcion(java.lang.String Descripcion) {
        this.Descripcion = Descripcion;
    }
    
    /**
     * Obtiene el estado de la cuasa
     * @autor ......... mfontalvo
     * @return .. valor del estado de la reprogramacion
     */
    public java.lang.String getEstado() {
        return Estado;
    }
    
    /**
     * Modifica el estado de la reprogramacion
     * @autor ......... mfontalvo
     * @param Estado .. nuevo estado de la reprogramacion
     */
    public void setEstado(java.lang.String Estado) {
        this.Estado = Estado;
    }
    
    
    /**
     * Obtiene El codigo del estandar
     * @autor ......... mfontalvo
     * @return .. Estandar
     */
    public java.lang.String getEstandar() {
        return Estandar;
    }
    
    /**
     * Modifica la el codigo del estandar
     * @autor ......... mfontalvo
     * @param Estandar
     */
    public void setEstandar(java.lang.String Estandar) {
        this.Estandar = Estandar;
    }
    
    
    /**
     * Obtiene la fecha de creacion del presupuesto
     * @autor ......... mfontalvo
     * @return .. FechaCreacion
     */
    public java.lang.String getFechaCreacion() {
        return FechaCreacion;
    }
    
    /**
     * Modifica la fecha de creacion del prosupuesto
     * @autor ......... mfontalvo
     * @param FechaCreacion del proesupuesto
     */
    public void setFechaCreacion(java.lang.String FechaCreacion) {
        this.FechaCreacion = FechaCreacion;
    }
    
    
    /**
     * Obtiene la fecha del diaje donde se genero el cambio
     * @autor ......... mfontalvo
     * @return .. FechaViaje
     */
    public java.lang.String getFechaViaje() {
        return FechaViaje;
    }
    
    /**
     * Modifica la de fecha de Viaje real
     * @autor ......... mfontalvo
     * @param FechaViaje del presupuesto
     */
    public void setFechaViaje(java.lang.String FechaViaje) {
        this.FechaViaje = FechaViaje;
    }
    
    /**
     * Obtiene el anterior valor del viaje
     * @autor ......... mfontalvo
     * @return .. ValorAnterior
     */
    public java.lang.String getValorAnterior() {
        return ValorAnterior;
    }
    
    /**
     * Modifica el valor que tenia registrado anteriormente el prespuesto
     * @autor ......... mfontalvo
     * @param ValorAnterior
     */
    public void setValorAnterior(java.lang.String ValorAnterior) {
        this.ValorAnterior = ValorAnterior;
    }
    
    
    /**
     * Obtiene el nuevo valor de del viaje
     * @autor ......... mfontalvo
     * @return .. ValorNuevo
     */
    public java.lang.String getValorNuevo() {
        return ValorNuevo;
    }
    
    /**
     * Modifica el nuevo valor del presuesto
     * @autor ......... mfontalvo
     * @param ValorNuevo nuevo valor modificado del presupuesto
     */
    public void setValorNuevo(java.lang.String ValorNuevo) {
        this.ValorNuevo = ValorNuevo;
    }
    
    /**
     * Getter for property UsuarioCreacion.
     * @return Value of property UsuarioCreacion.
     */
    public java.lang.String getUsuarioCreacion() {
        return UsuarioCreacion;
    }
    
    /**
     * Setter for property UsuarioCreacion.
     * @param UsuarioCreacion New value of property UsuarioCreacion.
     */
    public void setUsuarioCreacion(java.lang.String UsuarioCreacion) {
        this.UsuarioCreacion = UsuarioCreacion;
    }
    
}
