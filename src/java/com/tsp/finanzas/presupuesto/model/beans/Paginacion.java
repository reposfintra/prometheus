/*************************************************************
 * Nombre      ............... Paginacion.java
 * Descripcion ............... Paginacion
 * Autor       ............... mfontalvo
 * Fecha       ............... Septiembre - 22 - 2005
 * Version     ............... 1.0
 * Copyright   ............... Fintravalores S.A. S.A
 ************************************************************/

package com.tsp.finanzas.presupuesto.model.beans;


import java.util.*;
import java.math.*;
import java.sql.*;



public class Paginacion {
    
    // declaracion de variables
    private String    Accion;
    public  int       Cantidad_Filas   = 0;
    public  int       Cantidad_Indices = 0;
    private int       Cantidad_Vista   = 0;
    private int       Vista_Actual     = 1;        
    private List      Lista            = new LinkedList();    
    private ResultSet rs               = null;
    private boolean   Tipo             = false;
     
    
    /**
     * Procedimeto que indica el tipo de movimiento
     * @autor  mfontalvo
     * @params Opcion ...... Opcion de Movimoemto
     */
    public void Movimiento(String Opcion) {
        if(Opcion.equals("Fin"))    Vista_Actual = Cantidad_Vista;
        if(Opcion.equals("Ant"))    Vista_Actual = ((getInicio()-2)*Cantidad_Indices+1)<1?Vista_Actual:((getInicio()-2)*Cantidad_Indices+1);
        if(Opcion.equals("Sig"))    Vista_Actual = (getInicio()*Cantidad_Indices+1)>Cantidad_Vista?Vista_Actual:(getInicio()*Cantidad_Indices+1);
        if(Opcion.equals("Inicio")) Vista_Actual = 1;
    }
        
    
    /**
     * Procedimeot para modificar los paramrametros generales de la paginacion
     * @autor  mfontalvo
     * @params Accion .... Action
     * @params Cantidad_Indices ... cantidad de indices de la paginacion
     * @params Cantidad_Filas ..... cantidad de Filas de la paginacion
     */
    public void setConfiguracion(String Accion, int Cantidad_Indices, int Cantidad_Filas){
        this.Vista_Actual     = 1;
        this.Accion           = Accion;
        this.Cantidad_Indices = Cantidad_Indices;
        this.Cantidad_Filas   = Cantidad_Filas;
    }
    
    
    /**
     * Porcedimeinto para modificar la lista de la paginacion
     * @autor  mfontalvo
     * @params valor ..... nuevo listado
     */
    public void setLista(List valor){
        this.Lista = valor;
        this.Tipo  = true;
        BigDecimal b1 = new BigDecimal(Lista.size());
        BigDecimal b2 = new BigDecimal(Cantidad_Filas);
        this.Cantidad_Vista = b1.divide(b2,b1.ROUND_UP).intValue();
    }

    /**
     * Porcedimeinto para modificar la lista de la paginacion
     * @autor  mfontalvo
     * @params valor ..... nuevo listado
     */
    public void setLista(ResultSet valor) throws Exception{
        this.rs   = valor;
        this.Tipo = false;
        this.rs.last();
        BigDecimal b1 = new BigDecimal(rs.getRow());
        BigDecimal b2 = new BigDecimal(Cantidad_Filas);
        this.Cantidad_Vista = b1.divide(b2,b1.ROUND_UP).intValue();
    }
    
    /**
     * Funcion para obenetener la lista paginada actual
     * @autor  mfontalvo
     * @return Listado
     * @throws Exception.
     */
    public List getListado() throws Exception{
        List SubList = new LinkedList();
        if(this.Tipo){
            if(Lista!=null && Lista.size()>0)
                SubList = Lista.subList((Vista_Actual-1)*Cantidad_Filas, (Lista.size()>=(Vista_Actual-1)*Cantidad_Filas+Cantidad_Filas)?((Vista_Actual-1)*Cantidad_Filas+Cantidad_Filas):Lista.size());
        }
        else{
            int Cont = 1;   
            if(((Vista_Actual-1)*Cantidad_Filas)==0) rs.beforeFirst();
            else                                     rs.absolute((Vista_Actual-1)*Cantidad_Filas);
            ResultSetMetaData Md = rs.getMetaData();
            while(rs.next()){
                List Datos = new LinkedList();
                for(int i=1;i<=Md.getColumnCount();i++)
                    Datos.add(rs.getString(i));
                SubList.add(Datos);
                if((Cont++)==this.Cantidad_Filas)
                    break;
            }
        }
        return SubList;
    }
    
    
    /**
     * Funcion para obtener el action 
     * @autor  mfontalvo
     * @return Accion
     */
    public String getAccion(){        
        return "?ACCION=/"+this.Accion;
    }
    
    
    /**
     * Funcion que devuelve el objeto paginacion.
     * @autor  mfontalvo
     * @return Objeto paginacion
     */
    public Paginacion getPaginacion() {
        return this;
    }
    
    
    /* Funcion que devuelve el inicio del listado a recorrer
     * @return inicio
     */
    public int getInicio() {
        BigDecimal b1 = new BigDecimal(Vista_Actual);
        BigDecimal b2 = new BigDecimal(Cantidad_Indices);
        return b1.divide(b2,b1.ROUND_UP).intValue();
    }
    
    /**
     * Funcion que devuelve la cantidad de vistas de la pagincaion
     * @autor  mfontalvo
     * @return Cantidad de vistas
     */
    public int getCantidad_Vista() {
        return this.Cantidad_Vista;
    }
    
    /**
     * Funcion que devuelve la cantidad de indices de la paginacion
     * @autor  mfontalvo
     * @throws Exception Cantidad de Indices
     */
    public int getCantidad_Indices() {
        return this.Cantidad_Indices;
    }
    
    
    /**
     * Funcion que devuelve el indice de la vista actual.
     * @autor  mfontalvo
     * @return Vista actual
     */
    public int getVista_Actual(){
        return this.Vista_Actual;
    }
    
    /**
     * Funcion que devuelve la cantidad de filas de la paginacion
     * @autor  mfontalvo
     * @return cantidad de filas
     */
    public int getCantidad_Filas(){
        return this.Cantidad_Filas;
    }
    
    /**
     * Procedimiento para modificar la vista actual
     * @autor  mfontalvo
     * @params valor ..... nueva vista actual.
     */
    public void setVista_Actual(int valor){
        this.Vista_Actual = valor;        
    }
}
