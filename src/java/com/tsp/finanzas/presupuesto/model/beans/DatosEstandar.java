/*************************************************************
 * Nombre      ............... DatosEstandar.java
 * Descripcion ............... Datos del Estandar
 * Autor       ............... mfontalvo
 * Fecha       ............... Agosto - 20 - 2005
 * Version     ............... 1.0
 * Copyright   ............... Fintravalores S.A. S.A
 ************************************************************/

package com.tsp.finanzas.presupuesto.model.beans;


public class DatosEstandar {
    
    
    // declaracion de variables
    private String CAgencia;
    private String DAgencia;
    private String CCliente;
    private String DCliente;
    private String CEstandar;
    private String DEstandar;
    private String COrigen;
    private String DOrigen;
    private String CDestino;
    private String DDestino;
    
    /** Creates a new instance of DatosEstandar */
    public DatosEstandar() {
    }
    
    
    
    /**
     * Procedimiento para modificar el codigo de la agencia
     * @autor  mfontalvo
     * @params valor ..... nuevo valor de la variable
     */
    public void setCodigoAgencia(String valor){
        CAgencia = valor;
    }
    
    
    /**
     * Procedimiento para modificar la descripcion de la agencia
     * @autor  mfontalvo
     * @params valor ..... nuevo valor de la variable
     */
    public void setDescripcionAgencia(String valor){
        DAgencia = valor;
    }
    
    /**
     * Procedimiento para modificar el codigo del cliente
     * @autor  mfontalvo
     * @params valor ..... nuevo valor de la variable
     */
    public void setCodigoCliente(String valor){
        CCliente = valor;
    }
    
    /**
     * Procedimiento para modificar la descripcion del cliente
     * @autor  mfontalvo
     * @params valor ..... nuevo valor de la variable
     */    
    public void setDescripcionCliente(String valor){
        DCliente = valor;
    }
    
    /**
     * Procedimiento para modificar el codigo del estandar
     * @autor  mfontalvo
     * @params valor ..... nuevo valor de la variable
     */
    public void setCodigoEstandar(String valor){
        CEstandar = valor;
    }
    
    /**
     * Procedimiento para modificar la descripcion del estandar
     * @autor  mfontalvo
     * @params valor ..... nuevo valor de la variable
     */    
    public void setDescripcionEstandar(String valor){
        DEstandar = valor;
    }
    
    /**
     * Procedimiento para modificar el codigo de origen
     * @autor  mfontalvo
     * @params valor ..... nuevo valor de la variable
     */    
    public void setCodigoOrigen(String valor){
        COrigen = valor;
    }
    
    /**
     * Procedimiento para modificar la descripcion del origen
     * @autor  mfontalvo
     * @params valor ..... nuevo valor de la variable
     */    
    public void setDescripcionOrigen(String valor){
        DOrigen = valor;
    }
    
    
    /**
     * Procedimiento para modificar el codigo del destino
     * @autor  mfontalvo
     * @params valor ..... nuevo valor de la variable
     */    
    public void setCodigoDestino(String valor){
        CDestino = valor;
    }
    
    /**
     * Procedimiento para modificar la descripcion del destino
     * @autor  mfontalvo
     * @params valor ..... nuevo valor de la variable
     */
    public void setDescripcionDestino(String valor){
        DDestino = valor;
    }
    
    /**
     * Procedimiento para obtener el codigo de la agencia
     * @autor  mfontalvo
     * @return Codigo de la Agencia
     */
    public String getCodigoAgencia(){
        return CAgencia ;
    }

    /**
     * Procedimiento para obtener la descripcion de la agencia
     * @autor  mfontalvo
     * @return descripcion de la Agencia
     */    
    public String getDescripcionAgencia(){
        return DAgencia ;
    }
    
    /**
     * Procedimiento para obtener el codigo del cliente
     * @autor  mfontalvo
     * @return Codigo del cliente
     */
    public String getCodigoCliente(){
        return CCliente ;
    }
    
    /**
     * Procedimiento para obtener la descripcion del cliente
     * @autor  mfontalvo
     * @return Descripcion del cliente
     */    
    public String getDescripcionCliente(){
        return DCliente ;
    }
    
    /**
     * Procedimiento para obtener el codigo del estandar
     * @autor  mfontalvo
     * @return Codigo del estandar
     */    
    public String getCodigoEstandar(){
        return CEstandar ;
    }
    
    /**
     * Procedimiento para obtener la descripcion del estandar
     * @autor  mfontalvo
     * @return Descripcion del estandar
     */
    public String getDescripcionEstandar(){
        return DEstandar ;
    }
    
    
    /**
     * Procedimiento para obtener el codigo del origen
     * @autor  mfontalvo
     * @return Codigo del origen
     */    
    public String getCodigoOrigen(){
        return COrigen ;
    }
    
    /**
     * Procedimiento para obtener la descripcion del origen
     * @autor  mfontalvo
     * @return Codigo de la Agencia
     */    
    public String getDescripcionOrigen(){
        return DOrigen ;
    }
    
    /**
     * Procedimiento para obtener el codigo del destino
     * @autor  mfontalvo
     * @return Codigo de la Agencia
     */    
    public String getCodigoDestino(){
        return CDestino;
    }
    
    /**
     * Procedimiento para obtener la descripcion del destino
     * @autor  mfontalvo
     * @return Codigo de la Agencia
     */    
    public String getDescripcionDestino(){
        return DDestino;
    }
    
}
