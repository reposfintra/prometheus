/*************************************************************
 * Nombre      ............... Tasa.java
 * Descripcion ............... Tasas proyectadas por periodo
 * Autor       ............... fvillacob
 * Fecha       ............... ???????
 * Version     ............... 1.0
 * Copyright   ............... Fintravalores S.A. S.A
 ************************************************************/

package com.tsp.finanzas.presupuesto.model.beans;


public class Tasa {
    
    // declaracion
    private String ano;
    private String mes;
    private double bolivar;
    private double dolar;
    private double dtf;
    private int    id;
    
    public Tasa() {}
    
   
    
    /**
     * Funcion para modificar el id
     * @autor  fvillacob
     * @params val ....... nuevo valor
     */
    public void setId(int val){
        this.id = val;
    }
    
    /**
     * Funcion para modificar el a�o
     * @autor  fvillacob
     * @params YYYY ....... nuevo valor
     */
    public void setAno(String YYYY){
        this.ano = YYYY;
    }
    
    /**
     * Funcion para modificar el mes
     * @autor  fvillacob
     * @params MM ....... nuevo valor
     */
    public void setMes(String MM){
        this.mes = MM;
    }
    
    /**
     * Funcion para modificar el valor del dolar
     * @autor  fvillacob
     * @params dol ....... nuevo valor
     */
    public void setDolar(double dol){
        this.dolar = dol;
    }

    /**
     * Funcion para modificar el valor del bolivar
     * @autor  fvillacob
     * @params bol ....... nuevo valor
     */
    public void setBolivar(double bol){
        this.bolivar = bol;
    }
    
    
    /**
     * Funcion para modificar el valor del DTF
     * @autor  fvillacob
     * @params dtf ....... nuevo valor
     */    
    public void setDTF(double dtf){
        this.dtf = dtf;
    }
    

    
    
    
    /**
     * Funcion para obtener el id
     * @autor  fvillacob
     * @return Id
     */    
    public int getId(){
        return this.id;
    }

    /**
     * Funcion para obtener el ano
     * @autor  fvillacob
     * @return A�o
     */    
    public String getAno(){
        return this.ano;
    }
    
    
    /**
     * Funcion para obtener el Mes
     * @autor  fvillacob
     * @return Mes
     */        
    public String getMes(){
        return this.mes;
    }

    /**
     * Funcion para obtener el dolar
     * @autor  fvillacob
     * @return dolar
     */        
    public double getDolar(){
        return this.dolar;
    }
    
    
    /**
     * Funcion para obtener el bolivar
     * @autor  fvillacob
     * @return bolivar
     */        
    public double getBolivar(){
        return this.bolivar;
    }
    
    
    /**
     * Funcion para obtener la DTF
     * @autor  fvillacob
     * @return dtf
     */        
    public double getDTF(){
        return this.dtf;
    }
    
}
