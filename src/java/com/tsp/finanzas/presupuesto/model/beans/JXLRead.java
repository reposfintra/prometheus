/*************************************************************
 * Nombre      ............... JXLRead.java
 * Descripcion ............... Interfaz para manipular la api JXL
 * Autor       ............... mfontalvo
 * Fecha       ............... Septiembre - 22 - 2005
 * Version     ............... 1.0
 * Copyright   ............... Fintravalores S.A. S.A
 ************************************************************/

package com.tsp.finanzas.presupuesto.model.beans;


import java.io.*;
import jxl.*;


public class JXLRead {
    
    Workbook wb;
    Sheet    sheet; 
    
    /** Creates a new instance of JXLRead */
    public JXLRead() {
    }
    
    
    /**
     * Constructor para inicializar el libro de excel
     * @param file
     * @throws java.lang.Exception
     * @autor  mfontalvo
     * @params file ....... ruta del archivo
     */
    public JXLRead(String file)throws Exception{
        wb = Workbook.getWorkbook(new File( file ));
    }

    /**
     * Procedimiento para abrir un nuevo libro de excel
     * @autor  mfontalvo
     * @params file ....... ruta del archivo
     * @throws Exception.
     */
    public void abrirLibro(String file)throws Exception{
        wb = Workbook.getWorkbook(new File( file ));
    }
    
    
    /**
     * Procedimeinto para obtener una nueva hoja de excel
     * @autor  mfontalvo
     * @params hoja .... hoja a obtener
     * @throws Exception.
     */
    public void obtenerHoja(String hoja) throws Exception{
        if (wb==null)
            throw new Exception("No se pudo obtener la hoja, primero deber� abrir el libro");
        sheet = wb.getSheet(hoja);        
    }
    
    
    /**
     * Procedimiento para cerrar el libro de excel
     * @autor  mfontalvo
     * @throws Exception.
     */
    public void cerrarLibro() throws Exception{
        wb.close();
    }
    
    
    /**
     * Funcion que devuelve la hoja de excel.
     * @autor  mfontalvo
     * @return Hoja de excel
     * @throws Exception.
     */
    public Sheet obtenerHoja() throws Exception{
        return sheet;        
    }    
    
    
    /**
     * Funcion que devuelve el nuemero de filas que ontiene una hoja de excel
     * @autor  mfontalvo
     * @return numero de filas
     * @throws Exception.
     */
    public int numeroFilas()throws Exception{
        if (sheet==null)
            throw new Exception("No se pudo obtener el numero de filas de la hoja, primero deber� cargar una hoja existente");
        return sheet.getRows();        
    }
    
    
    /**
     * Procedimiento para obtener la informacion de una celda de la hoja actual
     * @autor  mfontalvo
     * @params fila ..... fila de la hoja
     * @params columna ..  columna de la hoja
     * @return Informacion de la celda
     * @throws Exception.
     */
   public String obtenerDato (int fila, int columna) throws Exception{
        if (sheet==null)
            throw new Exception("No se pudo obtener el el valor de la celda, primero deber� cargar una hoja existente");
        return sheet.getCell(columna, fila).getContents();
    }
    
   
    /**
     * Procedimiento para obtener una celda de la hoja actual
     * @autor  mfontalvo
     * @params fila ..... fila de la hoja
     * @params columna ..  columna de la hoja
     * @return celda
     * @throws Exception.
     */
   
    public Cell obtenerCelda (int fila, int columna) throws Exception{
        if (sheet==null)
            throw new Exception("No se pudo obtener el el valor de la celda, primero deber� cargar una hoja existente");
        return sheet.getCell(columna, fila);
    }    
    
}
