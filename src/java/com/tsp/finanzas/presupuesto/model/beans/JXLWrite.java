/*************************************************************
 * Nombre      ............... JXLWrite.java
 * Descripcion ............... Interfaz para manipular la api JXL
 * Autor       ............... mfontalvo
 * Fecha       ............... Septiembre - 22 - 2005
 * Version     ............... 1.0
 * Copyright   ............... Fintravalores S.A. S.A
 ************************************************************/

package com.tsp.finanzas.presupuesto.model.beans;

import java.io.*;
import java.util.*;


import jxl.*;
import jxl.write.*;
import jxl.write.WritableFont.*;
import jxl.format.*;
import jxl.biff.*;


public class JXLWrite {
    
    
    WritableWorkbook wb;
    WritableSheet sheet;
    int numhojas  = 0;
    
    
    public static jxl.format.Colour Colores;
    
    
    
    /** Creates a new instance of JXLExcel */
    public JXLWrite() {
    }
    
    
    /**
     * Constructor
     * @autor  mfontalvo
     * @params file .... ruta del archivo
     * @throws Exception.
     */
    public JXLWrite(String file) throws Exception {
        nuevoLibro(file);
    }
    
    
    /**
     * Procedimiento para crear un nuevo libro de excel
     * @autor  mfontalvo
     * @params file ..... nombre del archivo de excel
     * @throws Exception.
     */
    public void nuevoLibro(String file) throws Exception{
        wb = Workbook.createWorkbook(new File(file));
    }
    
    
    /**
     * Procedimiento para cerrar el libro de excel
     * @autor  mfontalvo
     * @throws Exception.
     */
    public void cerrarLibro() throws Exception{
        if (wb==null)
            throw new Exception("No se pudo cerrar el libro, primero deber� crear el libro");
        wb.write();
        wb.close();
        wb = null;
    }
    
    
    /**
     * Procedimieto para obtener una hoja de del libro actual
     * en caso de no existir este la crea.
     * @autor  mfontalvo
     * @params hoja ..... hoja solicitada
     * @throws Exception.
     */
    public void obtenerHoja(String hoja)throws Exception{
        if (wb==null)
            throw new Exception("No se pudo crear la hoja, primero deber� crear el libro");
        
        sheet = wb.getSheet(hoja);
        if (sheet == null) sheet = wb.createSheet(hoja, numhojas++);
        
    }
    
    
    /**
     * Funcion que devuelve la hoja actual.
     * @autor  mfontalvo
     * @return hoja actual.
     * @throws Exception.
     */
    public WritableSheet obtenerHoja()throws Exception{
        return sheet;
    }
    
    
    /**
     * Funcion qu devueve una celda de la hoja actual
     * @autor  mfontalvo
     * @params fila  ...... fila de la celda
     * @params columna .... columan de la celda 
     */
    public Cell obtenerCelda(int fila, int columna)throws Exception{
        if (sheet==null)
            throw new Exception("No se pudo crear la celda, primero deber� crear la hoja");
        
        Cell cell   = sheet.getCell(columna, fila);
        return cell;
    }
    
    
    /**
     * Adicionar una celda a la hoja actual
     * @autor  mfontalvo
     * @params fila ......... fila a adicionar
     * @params columna ...... columna a adicionar.
     * @params value ........ valor a adicionar.
     * @params formato ...... formato de la nueva celda
     * @throws Exception.
     */
    public void adicionarCelda(int fila, int columna, String value, WritableCellFormat formato)throws Exception{
        if (sheet==null)
            throw new Exception("No se pudo crear la celda, primero deber� crear la hoja");
        if (formato!=null)
            sheet.addCell( new jxl.write.Label(columna, fila, value, formato) );
        else
            sheet.addCell( new jxl.write.Label(columna, fila, value) );
    }
    
    
    /**
     * Adicionar una celda a la hoja actual
     * @autor  mfontalvo
     * @params fila ......... fila a adicionar
     * @params columna ...... columna a adicionar.
     * @params value ........ valor a adicionar.
     * @params formato ...... formato de la nueva celda
     * @throws Exception.
     */    
    public void adicionarCelda(int fila, int columna, double value, WritableCellFormat formato)throws Exception{
        if (sheet==null)
            throw new Exception("No se pudo crear la celda, primero deber� crear la hoja");
        if (formato!=null)
            sheet.addCell( new jxl.write.Number(columna, fila, value , formato));
        else
            sheet.addCell( new jxl.write.Number(columna, fila, value));
    }
    
    
    /**
     * Adicionar una celda a la hoja actual
     * @autor  mfontalvo
     * @params fila ......... fila a adicionar
     * @params columna ...... columna a adicionar.
     * @params value ........ valor a adicionar.
     * @params formato ...... formato de la nueva celda
     * @throws Exception.
     */    
    public void adicionarCelda(int fila, int columna, Date value, WritableCellFormat formato)throws Exception{
        if (sheet==null)
            throw new Exception("No se pudo crear la celda, primero deber� crear la hoja");
        if (formato!=null)
            sheet.addCell( new jxl.write.DateTime(columna, fila, value, formato) );
        else
            sheet.addCell( new jxl.write.DateTime(columna, fila, value) );
    }
    
    
    
    /**
     * Procedimiento para combinar un grupo de celdas
     * @autor  mfontalvo
     * @params filaInicial ...... Fila Inicial
     * @params columnaInicial ... Columan Inicial
     * @params filaFinal ........ Fila Final
     * @params columnaFinal ..... Columna Final
     * @throws Exception.
     */
    
    public void combinarCeldas(int filaInicial, int columnaInicial, int filaFinal , int columnaFinal)throws Exception{
        if (sheet==null)
            throw new Exception("No se pudo crear la celda, primero deber� crear la hoja");
        sheet.mergeCells(columnaInicial , filaInicial , columnaFinal , filaFinal);
    }
    
    
    /**
     * Cambiar la manificacion del Libro
     * @autor  mfontalvo
     * @params porcentaje .... porcetaje a magnificar
     * @throws Exception.
     */
    public void cambiarMagnificacion(int porcentaje)throws Exception{
        if (sheet==null)
            throw new Exception("No se pudo modificar la magnificacion, primero deber� crear la hoja");
        
        sheet.getSettings().setZoomFactor(porcentaje);
    }
    
    
    /**
     * Procedimento para cambiar el ancho de una columna
     * @autor  mfontalvo
     * @params columna ... columna a modificar
     * @params ancho ..... ancho a asignar
     * @throws Exception.
     */
    public void cambiarAnchoColumna (int columna, int ancho) throws Exception{
        if (sheet==null)
            throw new Exception("No se pudo cambiar el ancho de la columna, primero deber� crear la hoja");
        sheet.setColumnView( columna,  ancho);
    }   
    
    
    /**
     * Procedimento para cambiar el alto de una fila
     * @autor  mfontalvo
     * @params fila ...... fila a modificar
     * @params ancho ..... ancho a asignar
     * @throws Exception.
     */    
    public void cambiarAltoFila (int fila, int ancho) throws Exception{
        if (sheet==null)
            throw new Exception("No se pudo cambiar alto de la fila, primero deber� crear la hoja");
        sheet.setRowView( fila,  ancho);
        
    }     
    
    /**
     * Procedimiento para defineir un panel en la hoja actual
     * @autor  mfontalvo
     * @params filas ........ cantidad de filas asignar al panel
     * @params columans ..... cantidad de columnas asignar al panel
     * @throws Exception.
     */
    
    public void crearPanel (int filas, int columnas)throws Exception{
        if (sheet==null)
            throw new Exception("No se pudo crear el panel, primero deber� crear la hoja");
        sheet.getSettings().setHorizontalFreeze(columnas);
        sheet.getSettings().setVerticalFreeze(filas);
    }
    
    /**
     * Funcion que devuelve un nuevo formato
     * @autor  mfontalvo
     * @params name ........ fuente
     * @params size ........ tama�o
     * @params bold ........ negrita
     * @params italic ...... Cursiva
     * @params color ....... Color de la fuente
     * @params formato ..... Formato de el nuevo texto
     * @params fondo ....... Fondo del estilo
     * @params alineacion .. Alineacion de la celda
     * @return Nuevo estilo.
     * @throws Exception.
     */
    
    public WritableCellFormat nuevoEstilo(String name, int size, boolean bold, boolean italic, Object color, Object formato, Object fondo, Object align)throws Exception{
        WritableFont fuente = new WritableFont(
        WritableFont.createFont(name) ,
        size,
        (bold ? WritableFont.BOLD: WritableFont.NO_BOLD ) ,
        italic,
        UnderlineStyle.NO_UNDERLINE);
        
        if (color!=null) fuente.setColour((jxl.format.Colour) color);
        
        WritableCellFormat celda = (formato!=null ?
        new WritableCellFormat(fuente, (DisplayFormat) formato):
            new WritableCellFormat(fuente)         );
            
            if (fondo!=null) celda.setBackground( (jxl.format.Colour) fondo);
            if (align!=null) celda.setAlignment ( (jxl.write.Alignment) align);
            
            return celda;
    }
    
    
}
