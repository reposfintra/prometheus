/*************************************************************
 * Nombre      ............... ViajesStandar.java
 * Descripcion ............... Datos de los viajes del Estandar
 * Autor       ............... mfontalvo
 * Fecha       ............... Julio - 31 - 2005
 * Version     ............... 1.0
 * Copyright   ............... Fintravalores S.A. S.A
 ************************************************************/


package com.tsp.finanzas.presupuesto.model.beans;


import java.util.*;
import java.lang.Math;


public class ViajesStandar extends ViajesProceso {
    
    // declaracion de variables
    private String std_job_no;
    private String std_job_desc;
    
    
    //JEscandon 27-01-06
    private List ListCostosOperativos;
    private double valor_total_costos;
    
    /** Creates a new instance of Viajes */
    public ViajesStandar() {
        std_job_no     = "";
        std_job_desc   = "";
    }
    
    /**
     * Procedimiento para obtener el numero del estandar
     * @autor  mfontalvo
     * @params newValue ........  numero del estandar
     */
    public void setStdJobNo(String newValue){
        std_job_no = newValue;
    }
    
    /**
     * Procedimiento para modificar la descripcion del estandar
     * @autor  mfontalvo
     * @params newValue  ....... descripion del estandar
     */
    public void setStdJobDesc(String newValue){
        std_job_desc = newValue;
    }
    
    /**
     * Procedimiento para obtener el numero del estandar
     * @autor  mfontalvo
     * @return Numero del estandar
     */
    public String getStdJobNo(){
        return std_job_no;
    }
    
    /**
     * Procedimiento para obtener la descripcion del estandar
     * @autor  mfontalvo
     * @return descripcion del estandar
     */
    public String getStdJobDesc(){
        return std_job_desc;
    }
    
    
    /**
     * Proceso general que calcula los acumulados de los viajes
     * de los estandares
     * @autor  mfontalvo
     * @see CalcularDiferencia de la clase padre
     */
    public void Calcular(){
        this.CalcularDiferencia(true);
    }
    //2006 - 01  -  28
    
    //JEscandon 27-01-06
    /**
     * Getter for property ListCostosOperativos.
     * @return Value of property ListCostosOperativos.
     */
    public java.util.List getListCostosOperativos() {
        return ListCostosOperativos;
    }
    
    /**
     * Setter for property ListCostosOperativos.
     * @param ListCostosOperativos New value of property ListCostosOperativos.
     */
    public void setListCostosOperativos(java.util.List ListCostosOperativos) {
        this.ListCostosOperativos = ListCostosOperativos;
    }
    
    /**
     * Getter for property valor_total_costos.
     * @return Value of property valor_total_costos.
     */
    public double getValor_total_costos() {
        return valor_total_costos;
    }
    
    /**
     * Setter for property valor_total_costos.
     * @param valor_total_costos New value of property valor_total_costos.
     */
    public void setValor_total_costos(double valor_total_costos) {
        this.valor_total_costos = valor_total_costos;
    }
    
}
