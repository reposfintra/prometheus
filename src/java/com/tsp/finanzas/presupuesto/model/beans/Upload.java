
   /***************************************
    * Nombre Clase ............. Upload.java
    * Descripci�n  .. . . . . .  cargar imagenes recibidas por el request en el directorio especificado, 
                                  y devuelve el listado de archivos montados y los campos.
    * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
    * Fecha . . . . . . . . . .  05/01/2006
    * versi�n . . . . . . . . .  1.0
    * Copyright ...............  Transportes Sanchez Polo S.A.
    *******************************************/




package com.tsp.finanzas.presupuesto.model.beans;



import java.io.*;
import java.lang.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;




public class Upload {
    
    HttpServletRequest request;
    String      folderStore;
    Dictionary  fields;    
    List        files;
    List        filesLoad;
    
    
    
    
    
    public void init(){
        this.fields      = new Hashtable();
        this.files       = new LinkedList();
        this.filesLoad   = new LinkedList();
    }
    
    
   /**
     * Constructor 
     * @autor: ....... Fernel Villacob
     * @version ...... 1.0
     */ 
    public Upload() {
        init();
    }
    
    
    
    
     /**
     * Constructor que carga el request y el directorio
     * @autor: ....... Fernel Villacob
     * @param ........ String directory, HttpServletRequest request
     * @throws ....... Exception
     * @version ...... 1.0
     */
    public Upload(String directory, HttpServletRequest request) throws Exception{
        try{
            this.request     = request;
            this.folderStore = directory;
            init();
        }catch(Exception e){
            throw new Exception(e.getMessage());
        }
    }
    
    
    
    
    /**
     * permite cargar los archivos del request en el directory
     * @autor: ....... Fernel Villacob
     * @param ........ String directory, HttpServletRequest request
     * @throws ....... Exception
     * @version ...... 1.0
     */
     public void load(String directory, HttpServletRequest request)throws Exception{
        try{
             this.request     = request;
             this.folderStore = directory;
             this.load();
        }catch(Exception e){
            throw new Exception(e.getMessage());
        }
     }
    
    
    
     
    
    /**
     * permite cargar los archivos del request en el directory
     * @autor: ....... Fernel Villacob
     * @throws ....... Exception
     * @version ...... 1.0
     */
    public void load()throws Exception{
        
        ServletInputStream     in         = request.getInputStream();
        int                    Length     = request.getContentLength();
        ByteArrayInputStream   bfin       = null;
        ByteArrayOutputStream  bfout      = null;
        byte[]                 line       = new byte[256];
        byte[]                 line2      = new byte[256];
        byte[]                 savedFile  = null;
        String                 newLine    = "";
                        
        try{
        
            int i = in.readLine(line, 0, 256);
            if (i > 2) {
                
                this.create();
                
                int boundaryLength = i - 2;
                String boundary    = new String(line, 0, boundaryLength);                
                while ( i != -1 ) {
                    newLine = new String(line, 0, i); 
                    if (newLine.startsWith("Content-Disposition: form-data; name=\"")) {
                         if (    newLine.indexOf("filename=\"")   != -1 ){
                             String s = new String(line, 0, i-2);
                              
                             if ( s != null ) {
                                 
                                 String filename = this.getName(s);
                                 filesLoad.add(filename);                                 
                                 
                                 i = in.readLine(line, 0, 256);
                                 i = in.readLine(line, 0, 256);
                                 i = in.readLine(line, 0, 256);
                                 
                           //--- obtiene datos binarios file      
                                 newLine = new String(line, 0, i);
                                 bfout   = new ByteArrayOutputStream(); 
                                 while ( i != -1 && !newLine.startsWith(boundary) ) {
                                    line2   = ( byte[] )line.clone();
                                    int xi  = i;
                                    i       = in.readLine(line, 0, 256);
                                    if ( new String(line, 0, i).startsWith(boundary) ) bfout.write( line2, 0, xi-2 );
                                    else                                               bfout.write( line2, 0, xi );
                                    newLine = new String(line, 0, i);
                                 }
                                 savedFile = bfout.toByteArray();
                                 bfout.close();
                                 
                          //--- save the file
                                 bfin = new ByteArrayInputStream( savedFile );  
                                 int data;
                                 File             f    = new File( folderStore +"/"+ filename );
                                 FileOutputStream out  = new FileOutputStream(f);
                                 while( (data = bfin.read()) != -1 )
                                        out.write( data );
                                 bfin.close();
                                 out.close();
                                 
                             }
                             
                         }   
                         else{
                          //--- OTROS CAMPOS
                               int pos                 = newLine.indexOf("name=\"");
                               String fieldName        = newLine.substring(pos+6, newLine.length()-3);
                               i = in.readLine(line, 0, 256);
                               newLine                 = new String(line, 0, i);
                               StringBuffer fieldValue = new StringBuffer(256);   

                               while ( i != -1 && !newLine.startsWith(boundary) ) {
                                   i = in.readLine(line, 0, 256);
                                   if ( (i == boundaryLength+2 || i==boundaryLength+4) && (new String(line,0,i).startsWith(boundary))) 
                                       fieldValue.append(newLine.substring(0, newLine.length()-2));
                                   else 
                                       fieldValue.append(newLine.substring(0, newLine.length()-2));

                                   newLine = new String(line, 0, i);
                                }
                                fields.put(fieldName, fieldValue.toString());
                       } 
                    }  
                   i = in.readLine(line, 0, 256);
                }
                
                loadFile();
                
            }
                
       }catch (Exception e){
                throw new Exception(e.getMessage());
      }
            
}
        
        
        
        
    /**
     * permite obtener el nombre del archivo
     * @autor: ....... Fernel Villacob
     * @param ........ String linea
     * @throws ....... Exception
     * @version ...... 1.0
     */ 
     public String getName(String s)throws Exception{
         String file = "";
         try{
               String filename = "";
               int pos;
               String filepath;                    
               if ( s != null ) {
                    pos =   s.indexOf("filename=\"");
                    if (pos != -1) {
                            filepath = s.substring( pos+10, s.length()-1 );                  
                            pos      = filepath.lastIndexOf("\\");
                            if (pos != -1){
                                file = filepath.substring( pos+1 );
                            }
                            else{
                                file = filepath;                  
                            }
                    }
               }
          }catch (Exception e){
              throw new Exception(e.getMessage());
          }
         return file;
     }
     
     
     
     
     
     
     
   /**
     * permite crear el directorio
     * @autor: ....... Fernel Villacob
     * @throws ....... Exception
     * @version ...... 1.0
     */   
      public  void create() throws Exception{
          try{
            File f = null;
            f      = new File( this.folderStore ); 
            if(! f.exists() ) f.mkdir();
          }catch (Exception e){
              throw new Exception(e.getMessage());
          }
      }
     
     
      
   
   
   
    /**
     * permite cargar los archivos a la lista
     * @autor: ....... Fernel Villacob
     * @throws ....... Exception
     * @version ...... 1.0
     */  
     public void loadFile() throws Exception{   
        try{
            File f      = null;
            f = new File(this.folderStore); 
            if(! f.exists() ) f.mkdir();        
            if (f.isDirectory()){
                File []arc  =  f.listFiles(); 
                for (int i=0;i<arc.length;i++){
                    File archivo = arc[i];
                    for(int j=0;j<this.filesLoad.size();j++)
                        if(filesLoad.get(j).equals( archivo.getName()  ) ){
                            this.files.add( archivo );  
                            break;
                        }
                }
            }
            
        }catch (Exception e){
              throw new Exception(e.getMessage());
        }
     }
 
     
     
     
 
    /**
     * permite ordenar los archivos
     * @autor: ....... Fernel Villacob
     * @param ........ File []arg
     * @throws ....... Exception
     * @version ...... 1.0
     */  
    public static File[] Ordenar(File []arg){
        File temp = null;
        for (int i = 0; i<arg.length;i++)     
            for (int j = i+1; j<arg.length ;j++){
                long fechaJ = arg[j].lastModified();
                long fechaI = arg[i].lastModified();
                if (fechaI < fechaJ ){
                        temp   = arg[i];
                        arg[i] = arg[j];
                        arg[j] = temp; 
                }
            }
        return arg;
    }
      
    
    
    
     
     /**
     * devuelve el listado de archivos subidos
     * @autor: ....... Fernel Villacob
     * @version ...... 1.0
     */ 
     public List getFiles(){
         return this.files;
     }
     
     
     
     /**
     * devuelve el listado de nombre archivos subidos
     * @autor: ....... Fernel Villacob
     * @version ...... 1.0
     */ 
     public List getFilesName(){
         return this.filesLoad;
     }
     
     
     
     
     /**
     * devuelve el listado de campos
     * @autor: ....... Fernel Villacob
     * @version ...... 1.0
     */ 
     public Dictionary getFields(){
         return this.fields;
     }
     
        
        
        
        
        
        
        
        
        
    }
