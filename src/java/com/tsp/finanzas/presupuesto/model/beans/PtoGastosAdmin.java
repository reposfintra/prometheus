/********************************************************************
 *      Nombre Clase.................   PtoGastosAdmin.java  
 *      Descripci�n..................   Bean del archivo pto_gastos_admin
 *      Autor........................   Ing. Tito Andr�s Maturana
 *      Fecha........................   07.02.2006
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.finanzas.presupuesto.model.beans;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.util.*;
import java.sql.ResultSet;
import java.sql.SQLException;


/**
 *
 * @author  Andres
 */
public class PtoGastosAdmin {
    
    private String creation_user;
    private String base;
    private String reg_status;
    private String dstrct;
    
    private String cuenta;
    private String ano;
    private double[] vlr_presupuestado;
    private double[] vlr_ejecutado;
    
    private String agencia;
    private String unidad;
    private String area;
    private String elemento;
    private String nom_distrito;
    private String tipo_cuenta;
    
    
    /** Creates a new instance of PtoGastosAdmin */
    public PtoGastosAdmin() {
        vlr_presupuestado = new double[12];
        vlr_ejecutado = new double[12];
    }
    
    /**
     * Setea las propiedades del objeto a partir de un objeto de la clase <code>ResultSet</code>.
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @param rs <code>ResultSet</code>      
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void Load(ResultSet rs) throws SQLException{
        //rs.getString("");
        //rs.getDouble("");
        this.reg_status = rs.getString("reg_status");
        this.cuenta = rs.getString("cuenta");
        this.dstrct = rs.getString("dstrct");
        this.ano = rs.getString("ano");
        
        this.vlr_presupuestado[0] = rs.getDouble("vr_presupuestado_1");
        this.vlr_presupuestado[1] = rs.getDouble("vr_presupuestado_2");
        this.vlr_presupuestado[2] = rs.getDouble("vr_presupuestado_3");
        this.vlr_presupuestado[3] = rs.getDouble("vr_presupuestado_4");
        this.vlr_presupuestado[4] = rs.getDouble("vr_presupuestado_5");
        this.vlr_presupuestado[5] = rs.getDouble("vr_presupuestado_6");
        this.vlr_presupuestado[6] = rs.getDouble("vr_presupuestado_7");
        this.vlr_presupuestado[7] = rs.getDouble("vr_presupuestado_8");
        this.vlr_presupuestado[8] = rs.getDouble("vr_presupuestado_9");
        this.vlr_presupuestado[9] = rs.getDouble("vr_presupuestado_10");
        this.vlr_presupuestado[10] = rs.getDouble("vr_presupuestado_11");
        this.vlr_presupuestado[11] = rs.getDouble("vr_presupuestado_12");
        
        this.vlr_ejecutado[0] = rs.getDouble("vr_ejecutado_1");
        this.vlr_ejecutado[1] = rs.getDouble("vr_ejecutado_2");
        this.vlr_ejecutado[2] = rs.getDouble("vr_ejecutado_3");
        this.vlr_ejecutado[3] = rs.getDouble("vr_ejecutado_4");
        this.vlr_ejecutado[4] = rs.getDouble("vr_ejecutado_5");
        this.vlr_ejecutado[5] = rs.getDouble("vr_ejecutado_6");
        this.vlr_ejecutado[6] = rs.getDouble("vr_ejecutado_7");
        this.vlr_ejecutado[7] = rs.getDouble("vr_ejecutado_8");
        this.vlr_ejecutado[8] = rs.getDouble("vr_ejecutado_9");
        this.vlr_ejecutado[9] = rs.getDouble("vr_ejecutado_10");
        this.vlr_ejecutado[10] = rs.getDouble("vr_ejecutado_11");
        this.vlr_ejecutado[11] = rs.getDouble("vr_ejecutado_12");
    }
    
    /**
     * Getter for property base.
     * @return Value of property base.
     */
    public java.lang.String getBase() {
        return base;
    }
    
    /**
     * Setter for property base.
     * @param base New value of property base.
     */
    public void setBase(java.lang.String base) {
        this.base = base;
    }
    
    /**
     * Getter for property creation_user.
     * @return Value of property creation_user.
     */
    public java.lang.String getCreation_user() {
        return creation_user;
    }
    
    /**
     * Setter for property creation_user.
     * @param creation_user New value of property creation_user.
     */
    public void setCreation_user(java.lang.String creation_user) {
        this.creation_user = creation_user;
    }
    
    /**
     * Getter for property cuenta.
     * @return Value of property cuenta.
     */
    public java.lang.String getCuenta() {
        return cuenta;
    }
    
    /**
     * Setter for property cuenta.
     * @param cuenta New value of property cuenta.
     */
    public void setCuenta(java.lang.String cuenta) {
        this.cuenta = cuenta;
    }
    
    /**
     * Getter for property dstrct.
     * @return Value of property dstrct.
     */
    public java.lang.String getDstrct() {
        return dstrct;
    }
    
    /**
     * Setter for property dstrct.
     * @param dstrct New value of property dstrct.
     */
    public void setDstrct(java.lang.String dstrct) {
        this.dstrct = dstrct;
    }
    
    /**
     * Getter for property reg_status.
     * @return Value of property reg_status.
     */
    public java.lang.String getReg_status() {
        return reg_status;
    }
    
    /**
     * Setter for property reg_status.
     * @param reg_status New value of property reg_status.
     */
    public void setReg_status(java.lang.String reg_status) {
        this.reg_status = reg_status;
    }
    
    /**
     * Getter for property vlr_ejecutado.
     * @return Value of property vlr_ejecutado.
     */
    public double[] getVlr_ejecutado() {
        return this.vlr_ejecutado;
    }
    
    /**
     * Setter for property vlr_ejecutado.
     * @param vlr_ejecutado New value of property vlr_ejecutado.
     */
    public void setVlr_ejecutado(double[] vlr_ejecutado) {
        this.vlr_ejecutado = vlr_ejecutado;
    }
    
    /**
     * Getter for property vlr_presupuestado.
     * @return Value of property vlr_presupuestado.
     */
    public double[] getVlr_presupuestado() {
        return this.vlr_presupuestado;
    }
    
    /**
     * Setter for property vlr_presupuestado.
     * @param vlr_presupuestado New value of property vlr_presupuestado.
     */
    public void setVlr_presupuestado(double[] vlr_presupuestado) {
        this.vlr_presupuestado = vlr_presupuestado;
    }
        
    /**
     * Getter for property agencia.
     * @return Value of property agencia.
     */
    public java.lang.String getAgencia() {
        return agencia;
    }
    
    /**
     * Setter for property agencia.
     * @param agencia New value of property agencia.
     */
    public void setAgencia(java.lang.String agencia) {
        this.agencia = agencia;
    }
    
    /**
     * Getter for property unidad.
     * @return Value of property unidad.
     */
    public java.lang.String getUnidad() {
        return unidad;
    }
    
    /**
     * Setter for property unidad.
     * @param unidad New value of property unidad.
     */
    public void setUnidad(java.lang.String unidad) {
        this.unidad = unidad;
    }
    
    /**
     * Getter for property area.
     * @return Value of property area.
     */
    public java.lang.String getArea() {
        return area;
    }
    
    /**
     * Setter for property area.
     * @param area New value of property area.
     */
    public void setArea(java.lang.String area) {
        this.area = area;
    }
    
    /**
     * Getter for property elemento.
     * @return Value of property elemento.
     */
    public java.lang.String getElemento() {
        return elemento;
    }
    
    /**
     * Setter for property elemento.
     * @param elemento New value of property elemento.
     */
    public void setElemento(java.lang.String elemento) {
        this.elemento = elemento;
    }
    
    /**
     * Getter for property nom_distrito.
     * @return Value of property nom_distrito.
     */
    public java.lang.String getNom_distrito() {
        return nom_distrito;
    }
    
    /**
     * Setter for property nom_distrito.
     * @param nom_distrito New value of property nom_distrito.
     */
    public void setNom_distrito(java.lang.String nom_distrito) {
        this.nom_distrito = nom_distrito;
    }
    
    /**
     * Getter for property ano.
     * @return Value of property ano.
     */
    public java.lang.String getAno() {
        return ano;
    }
    
    /**
     * Setter for property ano.
     * @param ano New value of property ano.
     */
    public void setAno(java.lang.String ano) {
        this.ano = ano;
    }
    
    /**
     * Getter for property tipo_cuenta.
     * @return Value of property tipo_cuenta.
     */
    public java.lang.String getTipo_cuenta() {
        return tipo_cuenta;
    }
    
    /**
     * Setter for property tipo_cuenta.
     * @param tipo_cuenta New value of property tipo_cuenta.
     */
    public void setTipo_cuenta(java.lang.String tipo_cuenta) {
        this.tipo_cuenta = tipo_cuenta;
    }
    
}
