/*************************************************************
 * Nombre      ............... ViajesAgencia.java
 * Descripcion ............... Viajes agrupados por agencia
 * Autor       ............... mfontalvo
 * Fecha       ............... Julio - 31 - 2005
 * Version     ............... 1.0
 * Copyright   ............... Fintravalores S.A. S.A
 ************************************************************/

package com.tsp.finanzas.presupuesto.model.beans;



import java.util.*;
import java.lang.Math;

public class ViajesAgencia extends ViajesProceso{
    
    
    // declaracion de variables
    private String agencia;
    private String agencia_nombre;
    
    private TreeMap viajesClientes;
    
    //JEscandon 27-01-06
    private double coperativos_agencia;
    
    
    /** Creates a new instance of ViajesAgencia */
    public ViajesAgencia() {
        
        agencia        = "";
        agencia_nombre = "";
        viajesClientes = new TreeMap();
        
    }
    
    /**
     * Procedimeinto que modifica el valor de la agencia
     * @autor  mfontalvo
     * @params newValue ..... nuevo valor
     */
    public void setAgencia(String newValue){
        agencia = newValue;
    }
    
    /**
     * Procedimeinto que modifica el nombre de la agencia
     * @autor  mfontalvo
     * @params newValue ..... nuevo valor
     */
    public void setAgenciaNombre(String newValue){
        agencia_nombre = newValue;
    }
    
    /**
     * Procedimeinto que adiciona un nuevo cliente a la lista
     * @autor  mfontalvo
     * @params cli ..... nuevo cliente
     */
    public void addCliente(ViajesCliente cli){
        viajesClientes.put(cli.getCliente(), cli);
    }
    
    /**
     * Funcion que obtiene el codigo de la agencia
     * @autor  mfontalvo
     * @return Agencia
     */
    public String getAgencia(){
        return agencia;
    }
    
    /**
     * Funcion que obtiene el nombre de la agencia
     * @autor  mfontalvo
     * @return Nombre de la Agencia
     */
    public String getAgenciaNombre(){
        return agencia_nombre;
    }
    
    /**
     * Funcion que obtiene el listado de clientes
     * @autor  mfontalvo
     * @return Listado de Clientes
     */
    public TreeMap getListadoClientes(){
        return viajesClientes;
    }
    
    /**
     * Funcion que obtiene el listado de clientes
     * @autor  mfontalvo
     * @return Listado de Clientes
     */
    public List getListadoClientesToList(){
        return new LinkedList (viajesClientes.values());
    }    
    
    /**
     * Proceso general que calcula los acumulados de los viajes
     * de los clientes
     * @autor  mfontalvo
     * @see CalcularDiferencia de la clase padre
     */
    
    public void Calcular(){
        if (viajesClientes!=null){
            //for (int i = 0; i < viajesClientes.size();i++){
            Iterator it = viajesClientes.keySet().iterator();     
            while ( it.hasNext() )    {
                String key = (String) it.next();
                ViajesCliente vc = (ViajesCliente) viajesClientes.get(key);
                for (int dia = 1 ; dia <= 31; dia++){
                    setViajePtdo(dia, getViajePtdo(dia) + vc.getViajePtdo(dia));
                    setViajeEjdo(dia, getViajeEjdo(dia) + vc.getViajeEjdo(dia));
                    setViajeNP  (dia, getViajeNP  (dia) + vc.getViajeNP  (dia));
                    
                    /*//JEscandon 27-01-06
                    setCostosPtdo(dia, getCostosPtdo(dia) + vc.getCostosPtdo(dia));
                    setCostosEj(dia, getCostosEj(dia) + vc.getCostosEj(dia));*/
                }
            }
        }
        CalcularDiferencia(false);
    }
    //2006 - 01 - 28
    /**
     * Getter for property coperativos_agencia.
     * @return Value of property coperativos_agencia.
     */
    public double getCoperativos_agencia() {
        return coperativos_agencia;
    }
    
    /**
     * Setter for property coperativos_agencia.
     * @param coperativos_agencia New value of property coperativos_agencia.
     */
    public void setCoperativos_agencia(double coperativos_agencia) {
        this.coperativos_agencia = coperativos_agencia;
    }
    
}
