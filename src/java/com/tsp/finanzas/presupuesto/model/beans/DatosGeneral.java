/*************************************************************
 * Nombre      ............... DatosGeneral.java
 * Descripcion ............... Clase generica de Manipulacion de datos
 * Autor       ............... mfontalvo
 * Fecha       ............... Febrero - 23 - 2005
 * Version     ............... 1.0
 * Copyright   ............... Fintravalores S.A. S.A
 ************************************************************/


package com.tsp.finanzas.presupuesto.model.beans;

import java.io.*;
import java.sql.*;
import java.util.*;


public class DatosGeneral implements Serializable{    
    
    
    // declaracion de variables
    private TreeMap Datos;
    
    
    
    // constructor
    public DatosGeneral() {
        Datos = new TreeMap();
    }

    /**
     * Procedimiento para modificar el
     * listado del treemap
     * @autor  mfontalvo
     * @params valor .... nueva lista
     */
    public void setListaValores(TreeMap valor){
        this.Datos = valor;
    }
    
    
    /**
     * Procedimineto para Listado general de datos
     * @autor  mfontalvo
     * @return listado
     */
    public TreeMap getListaDatos(){
        return this.Datos;
    }    
    
    
    
    /**
     * Procedimiento para adicionar una nueva 
     * clave.
     * @autor  mfontalvo
     * @params valor ....... nuevo valor
     * @params key ......... llave
     */
    public void addValor(String valor, String key){
        this.Datos.put(key, valor);
    }
    
    /**
     * Procediento para obtener un valor de la lista
     * @autor  mfontalvo
     * @params key .... llave del valor a obtener.
     */
    public String getValor(String key){
        return ( this.Datos.get(key)!=null ? this.Datos.get(key).toString(): null);
    }
    
}
