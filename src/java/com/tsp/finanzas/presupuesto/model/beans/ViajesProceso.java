/*************************************************************
 * Nombre      ............... ViajesProceso.java
 * Descripcion ............... Procesos generales para los viajes
 * Autor       ............... mfontalvo
 * Fecha       ............... Agosto - 9 - 2005
 * Version     ............... 1.0
 * Copyright   ............... Fintravalores S.A. S.A
 ************************************************************/

package com.tsp.finanzas.presupuesto.model.beans;



import java.util.*;
import com.tsp.util.UtilFinanzas;



public class ViajesProceso {

    
    // declaracion de varaibles generales
    private int [] viajesEj = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
    private int [] viajesPt = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
    private int [] viajesNP = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
    
    //JEscandon 27-01-06
    private double []costosPt = {0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0};
    private double []costosEj = {0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0};

    
    private int    diferencia = 0;
    private double porcentajeInclumplimiento = 0;
    
    private int totalPt  = 0;
    private int totalEj  = 0;
    private int totalNP  = 0;
    private int totalPtF = 0;
    private int totalNPF = 0;
    
    private String ano = "";
    private String mes = "";
    
     //JEscandon 27-01-06
    private double totalCostos = 0.0;
    private double totalCostosPd = 0.0;
    private double totalCostosEj = 0.0;
    
    /** Creates a new instance of ViajesProceso */
    public ViajesProceso() {
    }
    
    
    
    /**
     * Procedimiento que modifica el valor de los viajes presupuestados  en un dia
     * @autor  mfontalvo
     * @params Dia ........ dia a modificar
     * @params newValue ... nuevo valor
     */
    public void setViajePtdo (int Dia, int newValue){
        viajesPt [Dia - 1] = newValue;
    }
    
    /**
     * Procedimiento que modifica el valor de los viajes ejecutados en un dia
     * @autor  mfontalvo
     * @params Dia ........ dia a modificar
     * @params newValue ... nuevo valor
     */
    public void setViajeEjdo (int Dia, int newValue){
        viajesEj [Dia - 1] = newValue;
    }
    
    
    /**
     * Procedimiento que modifica el valor de los viajes no programados en un dia
     * @autor  mfontalvo
     * @params Dia ........ dia a modificar
     * @params newValue ... nuevo valor
     */
    public void setViajeNP (int Dia, int newValue){
        viajesNP [Dia - 1] = newValue;
    }    
    
    /**
     * Procedimiento que modifica el total de los viajes presupuestados
     * @autor  mfontalvo
     * @params newValue ... nuevo valor
     */    
    public void setTotalPt (int newValue){
        totalPt = newValue;
    }
    
    /**
     * Procedimiento que modifica el total de los viajes presupuestados
     * @autor  mfontalvo
     * @params newValue ... nuevo valor
     */    
    public void setTotalPtF (int newValue){
        totalPtF = newValue;
    }
    
    /**
     * Procedimiento que modifica el total de los viajes ejecutados
     * @autor  mfontalvo
     * @params newValue ... nuevo valor
     */    
    public void setTotalEj (int newValue){
        totalEj = newValue;
    }
    
    /**
     * Procedimiento que modifica el total de los viajes no programados
     * @autor  mfontalvo
     * @params newValue ... nuevo valor
     */    
    public void setTotalNP (int newValue){
        totalNP = newValue;
    }    
    
    /**
     * Procedimiento que modifica el total de los viajes no programados a la fecha
     * @autor  mfontalvo
     * @params newValue ... nuevo valor
     */    
    public void setTotalNPF (int newValue){
        totalNPF = newValue;
    } 
    
    
    /**
     * Procedimiento que modifica la diferencia entre ejcutados y pres.
     * @autor  mfontalvo
     * @params newValue ... nuevo valor
     */    
    public void setDiferencia (int newValue){
        diferencia = newValue;
    }
    
    /**
     * Procedimiento que modifica el a�o
     * @autor  mfontalvo
     * @params newValue ... nuevo valor
     */
    public void setAno (String newValue){
        ano = newValue;
    }
    
    /**
     * Procedimiento que modifica el mes
     * @autor  mfontalvo
     * @params newValue ... nuevo valor
     */    
    public void setMes(String newValue){
        mes = newValue;
    }
    
    /**
     * Procedimiento que modifica el porcentaje de cumplimiento
     * @autor  mfontalvo
     * @params newValue ... nuevo valor
     */    
    public void setPorcentajeIncumplimiento (double newValue){
        porcentajeInclumplimiento = newValue;
    }
    
    
    /**
     * Funcion que obtiene el viaje presupuestado de un dia
     * @autor  mfontalvo
     * @params Dia... dia a consultar
     * @return valor del viaje
     */
    
    public int getViajePtdo (int Dia){
        return viajesPt [Dia - 1];
    }
    
    /**
     * Funcion que obtiene el viaje ejecutado de un dia
     * @autor  mfontalvo
     * @params Dia... dia a consultar
     * @return valor del viaje
     */
    
    public int getViajeEjdo (int Dia){
        return viajesEj [Dia - 1];
    }
    
    /**
     * Funcion que obtiene el viaje no programado de un dia
     * @autor  mfontalvo
     * @params Dia... dia a consultar
     * @return valor del viaje
     */
    
    public int getViajeNP (int Dia){
        return viajesNP [Dia - 1];
    }    
    
    
    /**
     * Funcion que obtiene el total de los viaje presupuestado 
     * @autor  mfontalvo
     * @return total de viajes
     */
    public int getTotalPt (){
        return totalPt;
    }
    
    /**
     * Funcion que obtiene el total de los viaje presupuestado 
     * @autor  mfontalvo
     * @return total de viajes
     */
    public int getTotalPtF (){
        return totalPtF;
    }
    
    /**
     * Funcion que obtiene el total de los viaje ejecutado
     * @autor  mfontalvo
     * @return total de viajes
     */    
    public int getTotalEj (){
        return totalEj;
    }
    
    /**
     * Funcion que obtiene el total de los viaje no programados 
     * @autor  mfontalvo
     * @return total de viajes
     */
    public int getTotalNP (){
        return totalNP;
    }
    
    /**
     * Funcion que obtiene el total de los viaje no programados a la fecha 
     * @autor  mfontalvo
     * @return total de viajes
     */
    public int getTotalNPF (){
        return totalNPF;
    }    
    
    /**
     * Funcion que obtiene la diferencia entre ptdo y ejecutado
     * @autor  mfontalvo
     * @return diferencia
     */    
    public int getDiferencia (){
        return diferencia;
    }
    
    /**
     * Funcion que obtiene el porcentaje de cumplimiento
     * @autor  mfontalvo
     * @return porcentaje de cumplimiento
     */
    
    public double getPorcentajeIncumplimiento (){
        return porcentajeInclumplimiento;
    }
    
    /**
     * Funcion que obtiene el a�o
     * @autor  mfontalvo
     * @return a�o
     */
    public String getAno (){
        return ano;
    }
    
    /**
     * Funcion que obtiene el mes
     * @autor  mfontalvo
     * @return mes
     */    
    public String getMes(){
        return mes;
    }
    
    
    /**
     * Proceso que genera los acumulados generales para las clase
     * @autor  mfontalvo
     * @param np , indica si calcula o o no los viajes no programados
     */
    public void CalcularDiferencia( boolean np ){
        int fechaActual = Integer.parseInt(UtilFinanzas.getFechaActual_String(8));
        diferencia = 0;
        porcentajeInclumplimiento = 0;
        totalPt  = 0;
        totalEj  = 0;
        totalPtF = 0;
        totalNP  = 0;
        totalNPF = 0;
        
        //JEscandon 27-01-06
        totalCostos = 0.0;
        totalCostosPd = 0.0;
        totalCostosEj = 0.0;
        
        for (int i=0;i<31;i++){
            
            int vpt = viajesPt[i];
            int vej = viajesEj[i];
            int vnp = viajesNP[i];
            
            if (np && vej > vpt) {
                vnp = vej - vpt;
                vej = vpt;
                viajesEj[i] = vej;
                viajesNP[i] = vnp;
            }
                
            totalPt += vpt;
            totalEj += vej;
            totalNP += vnp;
            
            
            int fecha = Integer.parseInt ( ano + mes + UtilFinanzas.DiaFormat(i+1) );
            if (fecha <= fechaActual ){
                totalPtF += viajesPt[i];
                totalNPF += viajesNP[i];
                totalCostosPd += costosPt[i];                
            }
            else{
                totalCostosEj += costosEj[i];
            }
        }
        
        diferencia += Math.abs(totalPt - totalEj) ;
        if (totalPtF !=0 ) porcentajeInclumplimiento =  (totalEj / (double) totalPtF) * 100;
    }

    
    /**
     * Procedimiento que modifica el valor de los costos presupuestados en un dia
     * @autor  Ing. Juan M. Escandon Perez
     * @params newValue  nuevo valor
     */
    public void setCostosPtdo(int Dia, double newValue){
        costosPt [Dia - 1] = newValue;
    }
    
    /**
     * Funcion que obtiene el Costo operativo presupuestado de un dia
     * @autor  Ing. Juan M. Escandon Perez
     * @params Dia dia a consultar
     * @return valor del Costos
     */
    public double getCostosPtdo(int Dia){
        return costosPt[Dia - 1];
    }
    
    /**
     * Procedimiento que modifica el total de los Costos presupuestados
     * @autor  Ing. Juan M. Escandon Perez
     * @params newValue  nuevo valor
     */
    public void setTotalCostos( double newValue ){
        totalCostos = newValue;
    }
    
    /**
     * Funcion que obtiene el total de los Costos operativos presupuestados
     * @autor  Ing. Juan M. Escandon Perez
     * @return total de Costos operativos
     */
    public double getTotalCostos(){
        return totalCostos;
    }
    
    /**
     * Procedimiento que modifica el valor de los costos operativos presupuestados 
     * @autor  Ing. Juan M. Escandon Perez
     * @params newValue  nuevo valor
     */
    public void setTotalCostosPdo( double newValue ){
        totalCostosPd = newValue;
    }
    
    /**
     * Funcion que obtiene el total de los Costos operativos presupuestados
     * @autor  Ing. Juan M. Escandon Perez
     * @params Dia dia a consultar
     * @return valor del Costos
     */
    public double getTotalCostosPd(){
        return totalCostosPd;
    }
    
    
     /**
     * Funcion que obtiene el total de los Costos operativos ejecutados
     * @autor  Ing. Juan M. Escandon Perez
     * @params Dia dia a consultar
     * @return valor del Costos
     */
    public double getTotalCostosEj(){
        return totalCostosEj;
    }
    
    /**
     * Procedimiento que modifica el valor de los costos operativos ejecutados
     * @autor  Ing. Juan M. Escandon Perez
     * @params newValue  nuevo valor
     */
    public void setTotalCostosEj( double newValue ){
        totalCostosEj = newValue;
    }
    
    /**
     * Procedimiento que modifica el valor de los costos ejecutados en un dia
     * @autor  Ing. Juan M. Escandon Perez
     * @params newValue  nuevo valor
     */
    public void setCostosEj (int Dia, double newValue){
        costosEj [Dia - 1] = newValue;
    }
    
    /**
     * Funcion que obtiene el Costo operativo ejecutado de un dia
     * @autor  Ing. Juan M. Escandon Perez
     * @params Dia dia a consultar
     * @return valor del Costos
     */
    public double getCostosEj(int Dia){
        return costosEj[Dia - 1];
    }
    
}
