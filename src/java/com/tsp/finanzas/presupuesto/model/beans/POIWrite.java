/*************************************************************
 * Nombre      ............... POIWrite.java
 * Descripcion ............... Interfaz para manipular la api POI
 * Autor       ............... mfontalvo
 * Fecha       ............... Septiembre - 22 - 2005
 * Version     ............... 1.0
 * Copyright   ............... Fintravalores S.A. S.A
 ************************************************************/

package com.tsp.finanzas.presupuesto.model.beans;

import java.io.*;
import java.util.*;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;



public class POIWrite {
    
    private String       fileName;
    private HSSFWorkbook wb;
    private HSSFSheet    sheet;
    
    public static final int NONE = -1;

    
    /** Creates a new instance of POIExcel */
    
    
    /**
     * Constructor
     * @autor  mfontalvo
     * @params file .... ruta del archivo
     * @throws Exception.
     */    
    public POIWrite(String file) throws Exception{
        nuevoLibro(file);
    }
    
    public POIWrite() {
    }

    
    /**
     * Procedimiento para crear un nuevo libro de excel
     * @autor  mfontalvo
     * @params file ..... nombre del archivo de excel
     * @throws Exception.
     */    
    public void nuevoLibro (String file) throws Exception{
        wb = new HSSFWorkbook();
        fileName = file;

        
    }
    
    /**
     * procedimiento para cerrar el libro de excel
     * @autor  mfontalvo
     * @throws Exception.
     */    
    public void cerrarLibro () throws Exception{
        if (wb==null)
            throw new Exception("No se pudo cerrar el libro, primero deber� crear el libro");
        
        FileOutputStream fileOut = new FileOutputStream(fileName);
        wb.write(fileOut);
        fileOut.close(); 
        wb = null;
    }
    
    /**
     * Procedimieto para obtener una hoja de del libro actual
     * en caso de no existir este la crea.
     * @autor  mfontalvo
     * @params hoja ..... hoja solicitada
     * @throws Exception.
     */    
    public void obtenerHoja(String hoja)throws Exception{
        if (wb==null)
            throw new Exception("No se pudo crear la hoja, primero deber� crear el libro");
        
        sheet = wb.getSheet(hoja); 
        if (sheet == null) sheet = wb.createSheet(hoja); 
       
    }    
    
    /**
     * Funcion qu devueve una celda de la hoja actual
     * @autor  mfontalvo
     * @params fila  ...... fila de la celda
     * @params columna .... columan de la celda 
     */    
    public HSSFCell obtenerCelda(int fila, int columna)throws Exception{
        if (sheet==null)
            throw new Exception("No se pudo crear la celda, primero deber� crear la hoja");
         
        HSSFRow row  = sheet.getRow(fila);
        if (row==null) row = sheet.createRow( (short) fila);
        
        HSSFCell cell   = row.getCell((short) columna );
        if (cell == null) cell = row.createCell( (short) columna);
        
        return cell;
    }
    
    
    /**
     * Adicionar una celda a la hoja actual
     * @autor  mfontalvo
     * @params fila ......... fila a adicionar
     * @params columna ...... columna a adicionar.
     * @params value ........ valor a adicionar.
     * @params Estilo ....... estilo de la nueva celda
     * @throws Exception.
     */    
    public void adicionarCelda(int fila, int columna, String value, HSSFCellStyle style)throws Exception{
        if (sheet==null)
            throw new Exception("No se pudo crear la celda, primero deber� crear la hoja");
         
        HSSFRow row   = sheet.createRow( (short) fila);
        HSSFCell cell = row.createCell ( (short) columna);
        
        cell.setCellValue(value);
        
        if (style!=null) cell.setCellStyle(style);
    }
    
    /**
     * Adicionar una celda a la hoja actual
     * @autor  mfontalvo
     * @params fila ......... fila a adicionar
     * @params columna ...... columna a adicionar.
     * @params value ........ valor a adicionar.
     * @params Estilo ....... estilo de la nueva celda
     * @throws Exception.
     */    
    public void adicionarCelda(int fila, int columna, double value, HSSFCellStyle style)throws Exception{
        if (sheet==null)
            throw new Exception("No se pudo crear la celda, primero deber� crear la hoja");
         
        HSSFRow row   = sheet.createRow( (short) fila);
        HSSFCell cell = row.createCell ( (short) columna);
        
        cell.setCellValue(value);
        
        if (style!=null) cell.setCellStyle(style);
    }    
    
    
    /**
     * Adicionar una celda a la hoja actual
     * @autor  mfontalvo
     * @params fila ......... fila a adicionar
     * @params columna ...... columna a adicionar.
     * @params value ........ valor a adicionar.
     * @params Estilo ....... estilo de la nueva celda
     * @throws Exception.
     */    
    public void adicionarCelda(int fila, int columna, Date value, HSSFCellStyle style)throws Exception{
        if (sheet==null)
            throw new Exception("No se pudo crear la celda, primero deber� crear la hoja");
         
        HSSFRow row   = sheet.createRow( (short) fila);
        HSSFCell cell = row.createCell ( (short) columna);
        
        cell.setCellValue(value);
        
        if (style!=null) cell.setCellStyle(style);
    } 
    
    
    /**
     * Procedimiento para combinar un grupo de celdas
     * @autor  mfontalvo
     * @params filaInicial ...... Fila Inicial
     * @params columnaInicial ... Columan Inicial
     * @params filaFinal ........ Fila Final
     * @params columnaFinal ..... Columna Final
     * @throws Exception.
     */
    public void combinarCeldas(int filaInicial, int columnaInicial, int filaFinal , int columnaFinal)throws Exception{
        if (sheet==null)
            throw new Exception("No se pudo crear la celda, primero deber� crear la hoja");
                 
        sheet.addMergedRegion(new Region(filaInicial,(short)columnaInicial ,filaFinal,(short)columnaFinal));
     
    }
    
    
    /**
     * Cambiar la manificacion del Libro
     * @autor  mfontalvo
     * @params x .... porcetaje a magnificar
     * @params y .... porcetaje a magnificar
     * @throws Exception.
     */    
    public void cambiarMagnificacion (int x, int y)throws Exception{
        if (sheet==null)
            throw new Exception("No se pudo modificar la magnificacion, primero deber� crear la hoja");
                 
        sheet.setZoom(x,y);   
    }
    
    
    /**
     * Procedimento para cambiar el ancho de una columna
     * @autor  mfontalvo
     * @params columna ... columna a modificar
     * @params ancho ..... ancho a asignar
     * @throws Exception.
     */    
    public void cambiarAnchoColumna (int columna, int ancho) throws Exception{
        if (sheet==null)
            throw new Exception("No se pudo cambiar el ancho de la columna, primero deber� crear la hoja");
        sheet.setColumnWidth((short) columna, (short) ancho);
        
    }
    
//    public void cambiarAltoFila (int fila, int alto) throws Exception{
//        if (sheet==null)
//            throw new Exception("No se pudo cambiar alto de la fila, primero deber� crear la hoja");
//        sheet.setRowWidth((short) fila, (short) alto);
//        
//    }     
    
    /**
     * Procedimiento para defineir un panel en la hoja actual
     * @autor  mfontalvo
     * @params filas ........ cantidad de filas asignar al panel
     * @params columans ..... cantidad de columnas asignar al panel
     * @throws Exception.
     */    
    public void crearPanel (int colSplit, int rowSplit, int leftmostColumn, int topRow)throws Exception{
        if (sheet==null)
            throw new Exception("No se pudo crear el panel, primero deber� crear la hoja");
        sheet.createFreezePane(colSplit, rowSplit , leftmostColumn ,topRow );
    }

     
    /**
     * Funcion que devuelve un nuevo formato
     * @autor  mfontalvo
     * @params name ........ fuente
     * @params size ........ tama�o
     * @params bold ........ negrita
     * @params italic ...... Cursiva
     * @params color ....... Color de la fuente
     * @params formato ..... Formato de el nuevo texto
     * @params fondo ....... Fondo del estilo
     * @params alineacion .. Alineacion de la celda
     * @return Nuevo estilo.
     * @throws Exception.
     */    
    public HSSFCellStyle nuevoEstilo (String name, int size, boolean bold, boolean italic , String formato ,int color, int fondo, int align)throws Exception{
        if (wb==null)
            throw new Exception("No se pudo crear la hoja, primero deber� crear el libro");
        
        HSSFCellStyle style   = wb.createCellStyle();
        HSSFFont      font    = wb.createFont();
        
        font.setFontHeightInPoints((short) size);
        font.setFontName(name);                                 
        if (bold)   font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        font.setItalic(italic);
        if (color!=NONE) font.setColor((short) color);
        
        
        style.setFont(font);
        if (!formato.equals(""))
            style.setDataFormat(wb.createDataFormat().getFormat(formato)); 
        
        if (fondo!=NONE) {
            style.setFillPattern((short) style.SOLID_FOREGROUND);
            style.setFillForegroundColor((short)fondo);
        }
        
        if (align!=NONE) style.setAlignment((short)align);
        return style;
        
    }
    
}
