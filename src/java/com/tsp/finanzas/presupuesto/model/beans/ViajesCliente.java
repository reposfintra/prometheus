/*************************************************************
 * Nombre      ............... ViajesCliente.java
 * Descripcion ............... Viajes agrupados por cliente
 * Autor       ............... mfontalvo
 * Fecha       ............... Julio - 31 - 2005
 * Version     ............... 1.0
 * Copyright   ............... Fintravalores S.A. S.A
 ************************************************************/

package com.tsp.finanzas.presupuesto.model.beans;



import java.util.*;
import java.lang.Math;

public class ViajesCliente extends ViajesProceso{
    
    // declaracion de variables
    private String cliente;
    private String cliente_nombre;
    
    private TreeMap viajesStandar;
    
    //JEscandon 27-01-06
    private double coperativos_cliente;
    
    
    /** Creates a new instance of ViajesCliente */
    public ViajesCliente() {
        
        cliente        = "";
        cliente_nombre = "";
        
        viajesStandar = new TreeMap();
        
    }
    
    /**
     * Procedimeinto que modifica el codigo del Cliente
     * @autor  mfontalvo
     * @params newValue ..... nuevo valor
     */
    public void setCliente(String newValue){
        cliente = newValue;
    }
    
    /**
     * Procedimeinto que modifica el nombre del Cliente
     * @autor  mfontalvo
     * @params newValue ..... nuevo valor
     */
    public void setClienteNombre(String newValue){
        cliente_nombre = newValue;
    }
    
    /**
     * Procedimeinto que adiciona un nuevo estandar al listado
     * @autor  mfontalvo
     * @params std ..... nuevo estandar
     */
    public void addStandar(ViajesStandar std){
        viajesStandar.put(std.getStdJobNo(), std);
    }
    
    
    /**
     * Funcion que obtiene el codigo del cliente
     * @autor  mfontalvo
     * @return Cliente
     */
    public String getCliente(){
        return cliente;
    }
    
    /**
     * Funcion que obtiene el nombre del Cliente
     * @autor  mfontalvo
     * @return Nombre del Cliente
     */
    public String getClienteNombre(){
        return cliente_nombre;
    }
    
    /**
     * Funcion que obtiene el listado de estandares
     * @autor  mfontalvo
     * @return Lista de estandares
     */
    public TreeMap getListaStandar(){
        return viajesStandar;
    }
    
    /**
     * Funcion que obtiene el listado de estandares
     * @autor  mfontalvo
     * @return Lista de estandares
     */
    public List getListaStandarToList(){
        return new LinkedList(viajesStandar.values());
    }    
    
    
    /**
     * Proceso general que calcula los acumulados de los viajes
     * de los clientes
     * @autor  mfontalvo
     * @see CalcularDiferencia de la clase padre
     */
    
    public void Calcular(){
        if (viajesStandar!=null && viajesStandar.size()>0){
            
            //for (int i = 0; i < viajesStandar.size();i++){
            
            Iterator it = viajesStandar.keySet().iterator();     
            while ( it.hasNext() )    {
                String key = (String) it.next();
                ViajesStandar vs = (ViajesStandar) viajesStandar.get(key);            
            
                //ViajesStandar vs = (ViajesStandar) viajesStandar.get(i);
                for (int dia = 1 ; dia <= 31; dia++){
                    this.setViajePtdo(dia, this.getViajePtdo(dia) + vs.getViajePtdo(dia));
                    this.setViajeEjdo(dia, this.getViajeEjdo(dia) + vs.getViajeEjdo(dia));
                    this.setViajeNP  (dia, this.getViajeNP  (dia) + vs.getViajeNP  (dia));
                  
                    /*//JEscandon 27-01-06
                    this.setCostosPtdo(dia, this.getCostosPtdo(dia) + vs.getCostosPtdo(dia));
                    this.setCostosEj(dia, this.getCostosEj(dia) + vs.getCostosEj(dia));*/
                }
            }
            CalcularDiferencia(false);
        }
        
    }
    //2006-01-28
    //JEscandon 27-01-06
    /**
     * Getter for property coperativos_cliente.
     * @return Value of property coperativos_cliente.
     */
    public double getCoperativos_cliente() {
        return coperativos_cliente;
    }
    
    /**
     * Setter for property coperativos_cliente.
     * @param coperativos_cliente New value of property coperativos_cliente.
     */
    public void setCoperativos_cliente(double coperativos_cliente) {
        this.coperativos_cliente = coperativos_cliente;
    }
    
}
