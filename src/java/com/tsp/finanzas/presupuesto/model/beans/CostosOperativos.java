/*****************************************************************************
 * Nombre                       CostosOperativos.java                        *
 * Descripcion                  Maneja los atributos del Objeto              *
 *                              utilizados en el presupuesto                 *
 *                              costos operativos                            *
 * Autor                        Ing. Juan M. Escandon Perez                  *
 * Fecha                        13 de diciembre de 2005, 02:13 PM            *
 * Version                      1.0                                          *
 * Copyright                    Fintravalores S.A. S.A                  *
 *****************************************************************************/
package com.tsp.finanzas.presupuesto.model.beans;
import java.io.*;
import java.sql.*;

public class CostosOperativos {
    private String std_job_no;
    private String ano;
    private String mes;
    private String elemento;
    private double valor;
    
    /** Creates a new instance of CostosOperativos */
    public CostosOperativos() {
    }
    
    /**
     * Metodo load, recibe una variable de tipo ResultSet,
     * permite cargar los atributos de un objeto de tipo CostosOperativos
     * @autor : Ing. Juan Manuel Escandon Perez
     * @param : ResultSet rs
     * @version : 1.0
     */
    public static CostosOperativos loadItem(ResultSet rs)throws Exception{
        CostosOperativos datos = new CostosOperativos();
        datos.setStd_job_no(rs.getString("std_job_no"));
        datos.setAno(rs.getString("ano"));
        datos.setMes(rs.getString("mes"));
        datos.setElemento(rs.getString("elemento"));
        datos.setValor(rs.getDouble("valor"));
        return datos;
    }
    
    /**
     * Getter for property std_job_no.
     * @return Value of property std_job_no.
     */
    public java.lang.String getStd_job_no() {
        return std_job_no;
    }
    
    /**
     * Setter for property std_job_no.
     * @param std_job_no New value of property std_job_no.
     */
    public void setStd_job_no(java.lang.String std_job_no) {
        this.std_job_no = std_job_no;
    }
    
    /**
     * Getter for property ano.
     * @return Value of property ano.
     */
    public java.lang.String getAno() {
        return ano;
    }
    
    /**
     * Setter for property ano.
     * @param ano New value of property ano.
     */
    public void setAno(java.lang.String ano) {
        this.ano = ano;
    }
    
    /**
     * Getter for property mes.
     * @return Value of property mes.
     */
    public java.lang.String getMes() {
        return mes;
    }
    
    /**
     * Setter for property mes.
     * @param mes New value of property mes.
     */
    public void setMes(java.lang.String mes) {
        this.mes = mes;
    }
    
    /**
     * Getter for property elemento.
     * @return Value of property elemento.
     */
    public java.lang.String getElemento() {
        return elemento;
    }
    
    /**
     * Setter for property elemento.
     * @param elemento New value of property elemento.
     */
    public void setElemento(java.lang.String elemento) {
        this.elemento = elemento;
    }
    
    /**
     * Getter for property valor.
     * @return Value of property valor.
     */
    public double getValor() {
        return valor;
    }
    
    /**
     * Setter for property valor.
     * @param valor New value of property valor.
     */
    public void setValor(double valor) {
        this.valor = valor;
    }
    
}
