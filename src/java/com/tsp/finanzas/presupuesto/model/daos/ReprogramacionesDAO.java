/*************************************************************
 * Nombre      ............... ReprogramacionesDAO.java
 * Descripcion ............... Clase de manipulacion de codigos
 *                             de reprogramacion
 * Autor       ............... mfontalvo
 * Fecha       ............... Noviembre - 17 - 2005
 * Version     ............... 1.0
 * Copyright   ............... Fintravalores S.A. S.A
 ************************************************************/

package com.tsp.finanzas.presupuesto.model.daos;


import java.io.*;
import java.sql.*;
import java.util.*;

import com.tsp.util.Util;
import com.tsp.util.connectionpool.PoolManager;
import com.tsp.finanzas.presupuesto.model.beans.*;
import com.tsp.operation.model.DAOS.MainDAO;

public class ReprogramacionesDAO extends MainDAO {
    
    /**
     * Constante que Indica tipo de Creacion
     */    
    public static final int CREATE         = 0;
    /**
     * Constante que Indica tipo Insercion
     */    
    public static final int INSERT         = 1;
    /**
     * Constante que Indica borrado del reg. en la bd
     */    
    public static final int DELETE         = 2;
    /**
     * Constante que Indica tipo Actualizacion del Reg.
     */    
    public static final int UPDATE         = 3;
    /**
     * Constante que Indica tipo Actualizacion
     */    
    public static final int UPDATE_STATE   = 4;
    /**
     * Constante que Indica tipo Consulta
     */    
    public static final int SELECT_LIST    = 5;    
    /**
     * Constante que Indica tipo Consulta de registros activos
     */    
    public static final int SELECT_LIST_ACTIVOS = 6;
    
    
    /**
     * Constante que Indica tipo de Creacion de la tabla de relacion 
     * entre reprogramaciones y pto_venteas
     */        
    public static final int CREATE_RELACION = 7;
    /**
     * Constante que Indica tipo Insercion en veiajes reprogramados
     */    
    public static final int INSERT_RELACION = 8;
    /**
     * Constante que Indica tipo busqueda viajes reprogramados
     */    
    public static final int SEARCH_VIAJE_RP = 9;

    
    
    public ReprogramacionesDAO() {
        super ("PtoReprogramacionesDAO.xml");
    }
    public ReprogramacionesDAO(String dataBaseName) {
        super ("PtoReprogramacionesDAO.xml", dataBaseName);
    }
    

    

    /**
     * Procedimiento ejecuta una consulta dependiendo
     * del tipo de operacion.
     * tipo = 0 -> crear tabla
     * tipo = 1 -> insertar reprogrmacion
     * tipo = 2 -> actualizar reprogramcion
     * tipo = 3 -> borrar reprogramacion
     *
     * @autor ............ Mario Fontalvo
     * @Fecha ............ 2005-10-20
     * @param Tipo ....... Indica el Tipo de operacion
     * @param args ....... Parametros de la consulta
     * @throws SQLException .
     */ 
    
    public void EXECUTE_UPDATE(int Tipo, String args[]) throws SQLException {
        Connection con = null;
        PreparedStatement st          = null;
        String SQL = "";
        try {
            
            // seleccion de tipo de consulta
            SQL = (Tipo == CREATE           )? "SQL_CREATE":
                  (Tipo == INSERT           )? "SQL_INSERT":
                  (Tipo == UPDATE           )? "SQL_UPDATE":
                  (Tipo == UPDATE_STATE     )? "SQL_UPDATE_STATE":
                  (Tipo == DELETE           )? "SQL_DELETE":
                  (Tipo == CREATE_RELACION  )? "SQL_CREATE_RELACION" :
                  (Tipo == INSERT_RELACION  )? "SQL_INSERT_RELACION" : "";
                         
            if (!SQL.equals("")){
                con = this.conectarJNDI(SQL);
                if (con != null) {
                    st = con.prepareStatement(this.obtenerSQL(SQL));//JJCastro fase2
                
                // seteo de parametros
                for (int i=0;i<args.length;i++)
                    st.setString((i+1),  (!args[i].equals("#FECHA#")? args[i]: Util.getFechaActual_String(6) ) );
                    
                // ejecucion de la consulta
                st.executeUpdate();
            }
            else throw new SQLException("Tipo de Operacion no definida . ["+Tipo+"]");
        }}
        catch(SQLException e) {
            if (!(Tipo == CREATE || Tipo == CREATE_RELACION ))  
                throw new SQLException("Error en rutina EXECUTE_UPDATE [ReprogramacionesDAO]... \n"+e.getMessage());
        }finally{
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
    }
    
    
    /**
     * Procedimiento ejecuta una consulta dependiendo
     * del tipo de operacion.
     * tipo = 0 -> consulta todos los registros
     * tipo = 1 -> consulta un registro especifico
     * @autor ............ Mario Fontalvo
     * @Fecha ............ 2005-10-20
     * @param Tipo ....... Indica el Tipo de consulta
     * @param args ....... Parametros de la consulta     
     * @return ........... retorna un elemento tipo Lista
     * @throws SQLException .
     */ 
    
    public List EXECUTE_QUERY(int Tipo, String []args) throws SQLException {
        Connection con = null;
        PreparedStatement st          = null;
        ResultSet         rs          = null;
        List              Resultado   = new LinkedList();        
        String            SQL         = "";        
        try {            
            SQL = (Tipo == SELECT_LIST         ? "SQL_LISTADO"         : 
                   Tipo == SELECT_LIST_ACTIVOS ? "SQL_LISTADO_ACTIVOS" : 
                   Tipo == SEARCH_VIAJE_RP     ? "SQL_SEARCH_VIAJE_RP" : "" );
                          
            if (!SQL.equals("")){

                con = this.conectarJNDI(SQL);
                if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(SQL));//JJCastro fase2
                for (int i=0;i<args.length;i++)
                    st.setString((i+1),  (args[i].equals("ALL")?"%":args[i]));
                rs = st.executeQuery();
                while (rs.next()){
                    Reprogramacion datos = new Reprogramacion ();
                    
                    if (Tipo == SEARCH_VIAJE_RP ){
                        datos.setEstandar        ( rs.getString(1));
                        datos.setFechaCreacion   ( rs.getString(2));
                        datos.setUsuarioCreacion ( rs.getString(3));
                        datos.setFechaViaje      ( rs.getString(4));
                        datos.setValorAnterior   ( rs.getString(5));
                        datos.setValorNuevo      ( rs.getString(6));
                        datos.setCodigo          ( rs.getString(7));
                        datos.setDescripcion     ( rs.getString(8));
                    }
                    else{
                        datos.setEstado     (rs.getString(1));
                        datos.setCodigo     (rs.getString(2));
                        datos.setDescripcion(rs.getString(3));
                    }
                    Resultado.add(datos);


                    datos = null;//Liberar Espacio JJCastro
                }
            }
        }}
        catch(SQLException e) {
            throw new SQLException("Error en rutina EXECUTE_QUERY [ReprogramacionesDAO]... \n"+e.getMessage());
        }finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return Resultado;
    }    
    
    
}
