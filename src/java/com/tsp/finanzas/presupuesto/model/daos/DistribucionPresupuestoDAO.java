/*************************************************************
 * Nombre      ............... DistribucionPresupuestoDAO.java
 * Descripcion ............... Manipulcaion de datos de Presupuesto
 * Autor       ............... mfontalvo
 * Fecha       ............... Abril - 14 - 2005
 * Version     ............... 1.0
 * Copyright   ............... Fintravalores S.A. S.A
 ************************************************************/


package com.tsp.finanzas.presupuesto.model.daos;

import com.tsp.finanzas.presupuesto.model.beans.*;
import com.tsp.util.connectionpool.*;
import com.tsp.util.UtilFinanzas;
import com.tsp.operation.model.DAOS.MainDAO;

import java.sql.*;
import java.util.*;
import java.text.SimpleDateFormat;


public class DistribucionPresupuestoDAO extends MainDAO{
    
    public Vector vecRef_Viaje;
    
    
    /** Creates a new instance of FlujoCajaDAO */
    public DistribucionPresupuestoDAO() {
        super("PtoDistribucionPresupuestoDAO.xml");
    }
    public DistribucionPresupuestoDAO(String dataBaseName) {
        super("PtoDistribucionPresupuestoDAO.xml", dataBaseName);
    }
    
    
    /**
     * Procedimiento que crea la tabla de pto
     * nota: este procedimeinto ya no es invocado desde ningun lado
     * @autor  mfontalvo
     * @throws Exception.
     **/
    public void CREATE_TABLE()throws SQLException {
        Connection con = null;
        PreparedStatement st          = null;
        String query = "SQL_CREATE_PTO_VENTAS";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.executeUpdate();
         }
        }
        catch(SQLException e){
        }finally{
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
    }
    
    
    /**
     * Procedimiento que ingresa los viajes de un estandar para un periodo
     * @autor  mfontalvo
     * @params args ........ Parametros del insert
     * @throws Exception.
     **/
    public String INSERT(String args[]) throws SQLException {

        Connection con = null;
        SimpleDateFormat fmt = new SimpleDateFormat("yyyy/MM/dd kk:mm:ss");
        java.util.Date[] Fechas = {null, null};
        java.util.Date Sistema = new java.util.Date(fmt.format(new java.util.Date()));
        PreparedStatement st = null;
        String query = "SQL_BUSCAR_FECHA_CREACION";
        String query2 = "SQL_ANULAR_PTO_VENTAS";
        String query3 = "SQL_INSERT_PTO_VENTAS";
        try {
            ////////////////////////////////////////////////////////////////////
            // PARAMS PARA OTRAS CONSULTAS:
            // DISTRITO - AGENCIA - CLIENTE - STDJOB - ANO - MES

            String Params[] = {args[1], args[6], args[7], args[5], args[2], args[3]};
            ////////////////////////////////////////////////////////////////////
            // BUSQUEDA FECHA CREACION y ACTUALIZACION
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            for (int i = 0; i < Params.length; i++) {
                st.setString((i + 1), Params[i]);
            }
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                Fechas[0] = new java.util.Date(rs.getString(1).replaceAll("-", "/"));
                Fechas[1] = new java.util.Date(rs.getString(2).replaceAll("-", "/"));
            }

            if (Fechas[1] != null) {
                while (Sistema.getTime() <= Fechas[1].getTime()) {
                    Sistema.setSeconds(Sistema.getSeconds() + 1);
                }
            }
            ////////////////////////////////////////////////////////////////////
            // ANULACION DEL VIAJE

            st = con.prepareStatement(this.obtenerSQL(query2));//JJCastro fase2
            for (int i = 0; i < Params.length; i++) {
                st.setString((i + 1), Params[i]);
            }
            st.executeUpdate();

            ////////////////////////////////////////////////////////////////////
            // INSERT
            st = con.prepareStatement(this.obtenerSQL(query3));//JJCastro fase2
            for (int i = 0; i < args.length; i++) {
                st.setString((i + 1), args[i]);
            }
            st.setString(args.length + 1, (Fechas[0] == null ? UtilFinanzas.getFechaActual_String(6) : fmt.format(Fechas[0])));
            st.setString(args.length + 2, args[args.length - 1]);
            st.setString(args.length + 3, fmt.format(Sistema));
            st.executeUpdate();
            }
        } catch (SQLException e) {
            throw new SQLException("Error en rutina INSERT [DistribuicionPresupuestoDAO]... \n" + e.getMessage());
        }finally{
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return fmt.format(Sistema);
        
    }
    
    
    /**
     * Procedimiento para eliminar el presupuesto dado un periodo
     * @autor  mfontalvo
     * @params Distrito ... Distrito del que se van a eliminar el pto
     * @params Ano ........ A�o del que se va a eliminar
     * @params Mes ........ Mes del que se va a eliminar
     * @throws Exception.
     **/
    public void BORRAR_PTO(String Distrito, String Ano, String Mes)throws SQLException {
        Connection con = null;
        PreparedStatement st          = null;
        String query = "SQL_BORRAR_PTO";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, Distrito);
            st.setString(2, Ano     );
            st.setString(3, Mes     );
            st.executeUpdate();
        }}
        catch(SQLException e){
            throw new SQLException("Error en la rutina BORRAR_PTO [DistribucionPresupuestoDAO] : \n" + e.getMessage());
        }finally{
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
    }
    
    
    /**
     * Funcion que devuelve presupuestadas de un periodo
     * @autor mfontalvo
     * @params Tipo ..... Tipo de Busqueda (0: Vista Mesual, 1: Vista Semanal, 2: Vista Diaria)
     * @params Ano  ..... A�o a consultar
     * @params Mes  ..... Mes a consultar
     * @params Stdjob  .. Numero del Estandar a consultar
     * @params Cliente .. Cliente a consultar
     * @params Agencia .. Agencia a consultar
     * @params Origen .. Origen del estandar a consultar
     * @params Destino .. Destino del estandar a consultar
     * @params AgenciaDespacho .. Agencia despacho del estandar
     * @params Filtrar .. Indican si se deben filtrar solo por aquellos que estan prespuestados
     * throws Exception.
     **/
    public TreeMap BuscarPresupuesto(int Tipo, String Ano, String Mes, String Stdjob, String Cliente , String Agencia , String Origen, String Destino, String AgenciaDespacho, boolean Filtrar) throws Exception {
        Connection con = null;
        TreeMap Registros = new TreeMap();
        List Valores = new LinkedList();
        TreeMap Totales = new TreeMap();
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_ESTANDARES";

        try {
            ////////////////////////////////////////////////////////////////////////////////////////////
            con = this.conectarJNDI(query);
            if(con != null){
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, Agencia);
            st.setString(2, Cliente);
            st.setString(3, Origen);
            st.setString(4, Destino);
            st.setString(5, Stdjob);
            st.setString(6, AgenciaDespacho);

            rs = st.executeQuery();
            while (rs.next()) {
                DatosGeneral datos = new DatosGeneral();
                datos.addValor(rs.getString(1), "DISTRITO");
                datos.addValor(rs.getString(2), "STDJOBNO");
                datos.addValor(rs.getString(3), "STDJOBDESC");
                datos.addValor(rs.getString(4), "CODIGOAGENCIA");
                datos.addValor(rs.getString(5), "NOMBREAGENCIA");
                datos.addValor(rs.getString(6), "CODIGOCLIENTE");
                datos.addValor(rs.getString(7), "NOMBRECLIENTE");
                datos.addValor(rs.getString(8), "CODIGOORIGEN");
                datos.addValor(rs.getString(9), "NOMBREORIGEN");
                datos.addValor(rs.getString(10), "CODIGODESTINO");
                datos.addValor(rs.getString(11), "NOMBREDESTINO");
                datos.addValor(rs.getString(12), "UW");
                datos.addValor(rs.getString(13), "TIPOVIAJE");

                String recurso = rs.getString(14);
                for (int k = 15; k <= 18; k++) {
                    recurso += (!recurso.equals("") && !rs.getString(k).equals("") ? "; " + rs.getString(k) : "");
                }
                datos.addValor(recurso, "RECURSO");

                datos.addValor(rs.getString(19), "UNIDAD_R");
                datos.addValor(rs.getString(20), "CANTIDAD_R");
                datos.addValor(rs.getString(21), "MONEDA");
                datos.addValor(rs.getString(22), "TARIFA");
                datos = this.BuscarVentas(Tipo, Ano, Mes, datos);
                if ((!Filtrar) || (Filtrar && Double.parseDouble(datos.getValor("TOTAL")) > 0)) {
                    datos.addValor(String.valueOf(Double.parseDouble(datos.getValor("TOTAL")) * Double.parseDouble(datos.getValor("TARIFA")) * Double.parseDouble(datos.getValor("CANTIDAD_R"))), "TOTALPTO");
                    Valores.add(datos);
                }
                Totales = UtilFinanzas.Acumulado(Totales, datos.getListaDatos(), (Mes.equals("TD") ? 12 : 1) * (Tipo == 0 ? 1 : (Tipo == 1 ? 5 : 31)));

                datos = null;//Liberar Espacio JJCastro
            }

            Registros.put("nrocols", String.valueOf((Mes.equals("TD") ? 12 : 1) * (Tipo == 0 ? 1 : (Tipo == 1 ? 5 : 31))));
            Registros.put("valores", Valores);
            Registros.put("totales", Totales);
            ////////////////////////////////////////////////////////////////////////////////////////////
        } }catch (Exception e) {
            throw new Exception("Error en rutina BuscarPresupuesto [DistribucionPresupuestoDAO]... \n" + e.getMessage());
        }finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return Registros;
        
    }
    
    
    /**
     * Funcion que devuelve Las ventas actuales y las ventas posteriores
     * de acuerdo a un estandar en un determinado periodo
     * @autor mfontalvo
     * @params Tipo ..... Tipo de Busqueda (Mensual, Diaria, Semana)
     * @params Ano  ..... A�o a consultar
     * @params Mes  ..... Mes a consultar
     * @params DatosGeneral .. bean donde viene seteado los datos del estandar
     * throws Exception.
     **/
    public DatosGeneral BuscarVentas(int Tipo, String Ano, String Mes, DatosGeneral datos) throws Exception{
        PreparedStatement st          = null;
        ResultSet         rs          = null;
        String query = "SQL_VENTASP";
        Connection con = null;
        
        try{
            con = this.conectarJNDI(query);
            if (con==null)
                throw new Exception("Sin conexion");
            
            String SQL_COLUMNA_SEMANAL = ", coalesce(csem#INDICE#,'0')";
            String SQL_COLUMNA_DIARIA  = ", coalesce(cdia#INDICE#,'0')";
            String SQL_VENTASA         = this.obtenerSQL("SQL_VENTASA");
            String SQL_VENTASP         = this.obtenerSQL("SQL_VENTASP");
            
            /****************************************************************************************************************
             * PROCEDIMIENTO MODO MENSUAL
             ****************************************************************************************************************/
            if (Tipo == 0){
                double total = 0;
                for (int i = (Mes.equals("TD")?1:Integer.parseInt(Mes)), j = 0;  i<= (Mes.equals("TD")?12:Integer.parseInt(Mes)) ; i++, j++){
                    datos.addValor("0", String.valueOf(j));          // valor actual
                    datos.addValor("0", "VP"   + String.valueOf(j)); // valor previo
                    datos.addValor("",  "UP"   + String.valueOf(j)); // usuario modificacion previa
                    datos.addValor("",  "FP"   + String.valueOf(j)); // fecha modificacion previa
                    datos.addValor("M", "TIPO" + String.valueOf(j));
                    
                    /////////////////////////////////////////////////////////////////////////////////////
                    // EXTRACCION - VENTAS ACTUALES
                    st = con.prepareStatement(SQL_VENTASA.replaceAll("#RESTO#","" + ", TIPO "));
                    st.setString(1, datos.getValor("DISTRITO"));
                    st.setString(2, datos.getValor("STDJOBNO"));
                    st.setString(3, Ano );
                    st.setString(4, UtilFinanzas.DiaFormat(i) );
                    rs = st.executeQuery();
                    while (rs.next()){
                        datos.addValor(rs.getString(1), String.valueOf(j));
                        datos.addValor(rs.getString(2), "TIPO" + String.valueOf(j));
                        total += rs.getDouble(1);
                        break;
                    }
                    datos.addValor(String.valueOf(total), "TOTAL");
                    
                    
                    /////////////////////////////////////////////////////////////////////////////////////
                    // EXTRACCION - VENTAS PREVIAS
                    st = con.prepareStatement(SQL_VENTASP.replaceAll("#RESTO#",""));
                    st.setString(1, datos.getValor("DISTRITO"));
                    st.setString(2, datos.getValor("STDJOBNO"));
                    st.setString(3, Ano );
                    st.setString(4, UtilFinanzas.DiaFormat(i) );
                    rs = st.executeQuery();
                    while (rs.next()){
                        datos.addValor(rs.getString(1), "VP" + String.valueOf(j));
                        datos.addValor(rs.getString(2), "UP" + String.valueOf(j));
                        datos.addValor(rs.getString(3), "FP" + String.valueOf(j));
                        break;
                    }
                    //////////////////////////////////////////////////////////////////////////////////////
                }
            }
            
            /****************************************************************************************************************
             * PROCEDIMIENTO MODO SEMANAL Y DIARIA
             ****************************************************************************************************************/
            else{
                String Cols = "";
                datos.addValor("0", "TOTAL");
                datos.addValor("",  "UP"); // usuario modificacion previa
                datos.addValor("",  "FP"); // fecha modificacion previa
                
                datos.addValor((Tipo==1?"S":"D"), "TIPO");
                for (int cols=1; cols<= (Tipo==1?5:31) ; cols++){
                    Cols += (Tipo==1? SQL_COLUMNA_SEMANAL : SQL_COLUMNA_DIARIA ).replaceAll("#INDICE#", (cols<10?"0":"")+cols );
                    datos.addValor("0", String.valueOf(cols-1)); // valor actual
                    datos.addValor("0", "VP"    + String.valueOf(cols-1)); // valor previo
                    datos.addValor("",  "UP"    + String.valueOf(cols-1)); // valor previo
                    datos.addValor("",  "FP"    + String.valueOf(cols-1)); // valor previo
                }
                Cols += ", TIPO ";
                
                //////////////////////////////////////////////////////////////////////////////////////////////
                // EXTRACCION - VENTAS ACTUALES
                st = con.prepareStatement(SQL_VENTASA.replaceAll("#RESTO#", Cols));
                st.setString(1, datos.getValor("DISTRITO"));
                st.setString(2, datos.getValor("STDJOBNO"));
                st.setString(3, Ano);
                st.setString(4, Mes);
                rs = st.executeQuery();
                while (rs.next()){
                    datos.addValor(rs.getString(1), "TOTAL");
                    for (int cols=2; cols<= (Tipo==1?5:31)+1 ; cols++)
                        datos.addValor(rs.getString(cols), String.valueOf(cols-2));
                    datos.addValor(rs.getString((Tipo==1?5:31) + 2), "TIPO");
                    break;
                }
                
                //////////////////////////////////////////////////////////////////////////////////////////////
                // EXTRACCION - VENTAS PREVIAS
                st = con.prepareStatement(SQL_VENTASP.replaceAll("#RESTO#", Cols));
                st.setString(1, datos.getValor("DISTRITO"));
                st.setString(2, datos.getValor("STDJOBNO"));
                st.setString(3, Ano);
                st.setString(4, Mes);
                rs = st.executeQuery();
                while (rs.next()){
                    for (int cols=2; cols<= (Tipo==1?5:31)+1 ; cols++){
                        datos.addValor(rs.getString(cols), "VP" + String.valueOf(cols-2));
                        datos.addValor(rs.getString((Tipo==1?5:31) + 3), "UP" + String.valueOf(cols-2));
                        datos.addValor(rs.getString((Tipo==1?5:31) + 4), "FP" + String.valueOf(cols-2));
                    }
                    break;
                }
                ///////////////////////////////////////////////////////////////////////////////////////////////
            }
        }catch(Exception e){
            throw new Exception("Error en la rutina BuscarVentas [DistribucionPresupuestoDAO]...\n"+e.getMessage());
        }
finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return datos;
    }
    
    
    /**
     * Procedimiento para extraer el historial de un estandar
     * @autor: mfontalvo
     * @params Distrito ..... Distrito a buscar
     * @params Estandar ..... Estandar a buscar
     * @params Ano .......... A�o a buscar
     * @params Mes .......... Mes a buscar
     * throws Exception.
     */
    public List Historial(String Distrito, String Estandar, String Ano, String Mes) throws Exception{
        Connection con = null;
        PreparedStatement st          = null;
        ResultSet         rs          = null;
        List lista = new LinkedList();
        String query = "SQL_HISTORIAL";
        try{
            con = this.conectarJNDI(query);
            if (con==null)
                throw new Exception("Sin conexion");
            
            String SQL_COLUMNA_DIARIA  = ", coalesce(cdia#INDICE#,'0')";
            
            // construccion de las columnas para el query
            String Cols = "";
            for (int cols=1; cols<= 31 ; cols++) Cols += SQL_COLUMNA_DIARIA.replaceAll("#INDICE#", UtilFinanzas.DiaFormat(cols));
            
            String sql = this.obtenerSQL(query);
            st         = con.prepareStatement(sql.replaceAll("#RESTO#", Cols));
            st.setString(1, Distrito);
            st.setString(2, Estandar);
            st.setString(3, Ano);
            st.setString(4, Mes);
            rs = st.executeQuery();
            
            while (rs.next()){
                DatosGeneral datos = new DatosGeneral();
                for (int cols=2; cols<= 32 ; cols++)
                    datos.addValor(rs.getString(cols), String.valueOf(cols-2));
                datos.addValor(rs.getString(33), "USUARIO");
                datos.addValor(rs.getString(34), "FECHA");
                lista.add(datos);
                datos = null;//Liberar Espacio JJCastro
            }
            
        }catch(Exception e){
            throw new Exception("Error en la rutina Historial [DistribucionPresupuestoDAO]...\n"+e.getMessage());
        }finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return lista;
    }
    
    
    /**
     * Funcion para obtener las tasa de cambio en un a�o
     * @autor  mfontalvo
     * @params Ano ........ Ano donde se va a consultar la tasa
     * @throws Exception.
     **/
    public TreeMap Tasa(String Ano) throws Exception{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_TASA";
        // Inicializacion
        TreeMap Lista = new TreeMap();
        for (int i = 1; i <= 12; i++) {
            DatosGeneral datos = new DatosGeneral();
            datos.addValor(Ano, "ANO");
            datos.addValor(UtilFinanzas.mesFormat(i), "MES");
            datos.addValor("0.00", "DOL");
            datos.addValor("0.00", "BOL");
            datos.addValor("0.00", "DTF");
            Lista.put((i < 10 ? "0" : "") + String.valueOf(i), datos);
        }

        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, Ano);
                rs = st.executeQuery();
                while (rs.next()) {
                    DatosGeneral datos = new DatosGeneral();
                    datos.addValor(rs.getString(1), "ANO");
                    datos.addValor(rs.getString(2), "MES");
                    datos.addValor(rs.getString(3), "DOL");
                    datos.addValor(rs.getString(4), "BOL");
                    datos.addValor(rs.getString(5), "DTF");
                    Lista.put(rs.getString(2), datos);
                }
            }
        }
        catch(Exception e) {
            throw new Exception("Error en rutina Tasa [DistribucionPresupuestoDAO]... \n"+e.getMessage());
        }finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return Lista;
    }
    
    
    /**
     * Funcion para obtener datos generales de un cliente
     * @autor  mfontalvo
     * @params Cliente ... Codigo de cliente a consultar
     * @throws Exception.
     **/
    public String AgenciaDuenaCliente(String Cliente) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String infoCliente = "";
        String query = "SQL_AGEN_CLIENTE";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, Cliente);
                rs = st.executeQuery();
                if (rs.next()) {
                    infoCliente = rs.getString(1);
                }
            }
        }
        catch(SQLException e) {
            throw new SQLException("Error en rutina AgenciaDuenaCliente [DistribuicionPresupuestoDAO]... \n"+e.getMessage());
        }finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return infoCliente;
    }
    
    
    /**
     * Procedimiento para obtener todos las agencias con sus clientes y a la
     * la vez sus respectivos estandares
     * @autor  mfontalvo
     * @params args ........ Parametros del insert
     * @throws Exception.
     **/
    public List AgenciasClientesStandar(String agencia, String cliente, int Modo)throws Exception{
        Connection con = null;
        PreparedStatement st    = null;
        ResultSet         rs    = null;
        List              Lista = new LinkedList();
        String query = "SQL_DATOS_STDJOB";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, agencia);
            st.setString(2, cliente);
            rs = st.executeQuery();
            
            while(rs.next()){
                DatosEstandar datos = new DatosEstandar();
                datos.setCodigoAgencia      (rs.getString(1));
                datos.setDescripcionAgencia(rs.getString(2));
                datos.setCodigoCliente      (rs.getString(3));
                datos.setDescripcionCliente(rs.getString(4));
                datos.setCodigoEstandar     (rs.getString(5));
                datos.setDescripcionEstandar(rs.getString(6));
                datos.setCodigoOrigen       (rs.getString(7));
                datos.setDescripcionOrigen  (rs.getString(8));
                datos.setCodigoDestino      (rs.getString(9));
                datos.setDescripcionDestino(rs.getString(10));
                Lista.add(datos);
                datos = null;//Liberar Espacio JJCastro
            }
        }}
        catch(Exception e) {
            throw new Exception("Error en rutina AgenciasClientesStandar [DistribucionPresupuestoDAO]... \n"+e.getMessage());
        }finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return Lista;
    }
    
    
    /**
     * Procedimineto para obtener el listado general de clientes
     * @autor  mfontalvo
     * @return Listado de clientes
     * @throws Exception.
     */
    public List ListaClientes() throws Exception{
        Connection con = null;
        PreparedStatement st          = null;
        ResultSet         rs          = null;        
        List lista = new LinkedList();
        String query = "SQL_CLIENTES";
        
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            rs = st.executeQuery();
            while (rs.next()){
                DatosGeneral datos = new DatosGeneral();
                datos.addValor(rs.getString(1), "codigo");
                datos.addValor(rs.getString(2), "descripcion");
                lista.add(datos);
                datos = null;//Liberar Espacio JJCastro
            }
            }
        }catch(Exception e){
            throw new Exception("Error en la rutina ListaClientes [DistribucionPresupuestoDAO]...\n"+e.getMessage());
        }finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return lista;
    }
    
    
    /**
     * Procedimineto para anular el ultimo presupuesto
     * @autor  mfontalvo
     * @fecha  2006-01-24
     * @params Distrito , distrito del estandar
     * @params Estandar , numero del estandar
     * @params Periodo , periodo a�o, mes concatenados
     * @throws Exception.
     */
    public void AnularUltimoViaje(String Distrito, String Estandar, String Periodo) throws Exception{
        Connection con = null;
        PreparedStatement st          = null;
        String query = "SQL_ANULAR_PTO_VENTAS2";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, Distrito);
            st.setString(2, Estandar);
            st.setString(3, Periodo );
            st.executeUpdate();
            
        }}catch(Exception e){
            throw new Exception("Error en la rutina AnularUltimoViaje [DistribucionPresupuestoDAO]...\n"+e.getMessage());
        }finally{
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
    }
    
    
    /**
     * Metodo buscarptoCarbon, busca los viajes planeados de carbon de acuerdo  un standar especifico
     * @autor : Diogenes Bastidas Morales.
     * @param : ano,mes,dia de las 2 fechas y distrito
     * @return: Vector pto carbon
     * @version : 1.0
     */
    public int[] buscarptoCarbon( String ano1, String mes1, String ano2, String mes2,String stdjob,String dstrct  )throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        int [] pto = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
        String CDia="CDIA",a="",M1="",M2="",m="0";
        String query = "SQL_PTOCARBON";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1,dstrct);
            st.setString(2, stdjob);
            st.setString(3,ano1+mes1);
            st.setString(4,ano2+mes2);
            rs = st.executeQuery();
            while (rs.next()){
                for (int i = 1; i<=31; i++ ){  //recorro los campos de los dias de un registro
                    CDia = ( i < 10 )?CDia+"0"+i:CDia+i;
                    pto[i-1] = rs.getInt(CDia);
                    CDia="CDIA";
                }
            }
            
        }}catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA PTO_VENTA " + e.getMessage()+" " + e.getErrorCode());
        }finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return pto;
    }
    
    /**
     * Metodo insertarReferencia, insertar la referencia del viaje
     * @autor : Diogenes Bastidas Morales.
     * @param : objeto tipo Ref_viaje
     * @return: 
     * @version : 1.0
     */
    public String insertarReferencia(Ref_viaje ref_viaje)throws Exception{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String SQL = "SQL_INSERTAR_REF_VIAJE";
        String sql = "";
        try{
            con = this.conectarJNDI(SQL);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(SQL));//JJCastro fase2
            st.setString(1, ref_viaje.getDstrct());
            st.setString(2, ref_viaje.getStd_job_no());
            st.setString(3, ref_viaje.getAno());
            st.setString(4, ref_viaje.getMes());
            st.setInt(5, ref_viaje.getDia());
            st.setInt(6, ref_viaje.getNro_viaje());
            st.setString(7, ref_viaje.getTipo_doc());
            st.setString(8, ref_viaje.getDocumento());
            st.setString(9, ref_viaje.getCreation_user());
            st.setString(10, ref_viaje.getCreation_date());
            st.setString(11, ref_viaje.getUser_update());
            st.setString(12, ref_viaje.getLast_update());
            st.setString(13, ref_viaje.getBase());
            sql = st.toString();
            
        }}catch(Exception e){
            e.printStackTrace();
            throw new SQLException(" "+e.getMessage());
        }finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return sql;
    }
   
   
    
    /**
     * Getter for property vecRef_Viaje.
     * @return Value of property vecRef_Viaje.
     */
    public java.util.Vector getVecRef_Viaje() {
        return vecRef_Viaje;
    }
    
    
    
    /**
     * Metodo modificarReferencia, modifica la referencia del viaje
     * @autor : Diogenes Bastidas Morales.
     * @param : objeto tipo Ref_viaje
     * @return: 
     * @version : 1.0
     */
    public void listarReferenciaViaje (String dstrct, String sj, String ano, String mes, int dia  )throws Exception{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_BUSCAR_REF_VIAJE";
        vecRef_Viaje = new Vector();
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, dstrct);
                st.setString(2, sj);
                st.setString(3, ano);
                st.setString(4, mes);
                st.setInt(5, dia);
                ////System.out.println(st);
                rs = st.executeQuery();
                while (rs.next()) {
                    Ref_viaje ref_viaje = new Ref_viaje();
                    ref_viaje.setDstrct(rs.getString("dstrct"));
                    ref_viaje.setStd_job_no(rs.getString("std_job_no"));
                    ref_viaje.setAno(rs.getString("ano"));
                    ref_viaje.setMes(rs.getString("mes"));
                    ref_viaje.setDia(rs.getInt("dia"));
                    ref_viaje.setNro_viaje(rs.getInt("nro_viaje"));
                    ref_viaje.setTipo_doc(rs.getString("tipo_doc"));
                    ref_viaje.setDocumento(rs.getString("documento"));
                    vecRef_Viaje.add(ref_viaje);
                    ref_viaje = null;//Liberar Espacio JJCastro
                }
            }

        }catch(Exception e){
            e.printStackTrace();
            throw new SQLException(" "+e.getMessage());
        }finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
    }
    
    
    /**
     * Metodo eliminarReferencia, modifica la referencia del viaje
     * @autor : Diogenes Bastidas Morales.
     * @param : objeto tipo Ref_viaje
     * @return: 
     * @version : 1.0
     */
    public String eliminarReferencia(Ref_viaje ref_viaje)throws Exception{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String sql = "";
        String query = "DELETE_REF_VIAJE";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, ref_viaje.getDstrct());
            st.setString(2, ref_viaje.getStd_job_no());
            st.setString(3, ref_viaje.getAno());
            st.setString(4, ref_viaje.getMes());
            st.setInt(5, ref_viaje.getDia());
            sql = st.toString();
            }}catch(Exception e){
            e.printStackTrace();
            throw new SQLException(" "+e.getMessage());
        }finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return sql;
    }
    
    
     /**
     * Metodo modificarReferencia, modifica la referencia del viaje
     * @autor : Diogenes Bastidas Morales.
     * @param : objeto tipo Ref_viaje
     * @return: 
     * @version : 1.0
     */
    public void modificarReferencia(Ref_viaje ref_viaje)throws Exception{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String SQL = "SQL_MODIFICAR_REF_VIAJE";
        try {
            con = this.conectarJNDI(SQL);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(SQL));//JJCastro fase2
                st.setString(1, ref_viaje.getTipo_doc());
                st.setString(2, ref_viaje.getDocumento());
                st.setString(3, ref_viaje.getUser_update());
                st.setString(4, ref_viaje.getLast_update());
                st.setString(5, ref_viaje.getDstrct());
                st.setString(6, ref_viaje.getStd_job_no());
                st.setString(7, ref_viaje.getAno());
                st.setString(8, ref_viaje.getMes());
                st.setInt(9, ref_viaje.getDia());
                st.setInt(10, ref_viaje.getNro_viaje());
                st.executeUpdate();
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new SQLException(" " + e.getMessage());
        }finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
    }
    
}