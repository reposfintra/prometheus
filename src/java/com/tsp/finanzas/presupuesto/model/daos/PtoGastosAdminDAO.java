/********************************************************************
 *  Nombre Clase.................   PtoGastosAdminDAO.java
 *  Descripci�n..................   DAO del archivo pto_gastos_admin
 *  Autor........................   Ing. Tito Andr�s Maturana
 *  Fecha........................   07.02.2006
 *  Versi�n......................   1.0
 *  Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.finanzas.presupuesto.model.daos;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.util.Date;
import com.tsp.util.*;
import com.tsp.finanzas.presupuesto.model.beans.*;
import com.tsp.operation.model.DAOS.MainDAO;
//import com.tsp.util.connectionpool.PoolManager;
import javax.swing.*;
import java.text.*;

/**
 *
 * @author  Andres
 */
public class PtoGastosAdminDAO extends MainDAO{
    private Vector vector;
    private PtoGastosAdmin presupuesto;
    
    
    private static final String SQL_INSERT = 
            "INSERT INTO con.pto_gastos_admin(" +
            "   dstrct, " +
            "   cuenta, " +
            "   ano, " +
            "   vr_presupuestado_1, " +
            "   vr_presupuestado_2, " +
            "   vr_presupuestado_3, " +
            "   vr_presupuestado_4, " +
            "   vr_presupuestado_5, " +
            "   vr_presupuestado_6, " +
            "   vr_presupuestado_7, " +
            "   vr_presupuestado_8, " +
            "   vr_presupuestado_9, " +
            "   vr_presupuestado_10, " +
            "   vr_presupuestado_11, " +
            "   vr_presupuestado_12, " +
            "   creation_date, " +
            "   creation_user, " +
            "   base) " +
            "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, 'now()', ?, ?)";
    
    private static final String SQL_EXISTE =             
            "SELECT reg_status " +
            "FROM con.pto_gastos_admin " +
            "WHERE dstrct=? AND cuenta=? AND ano=?";
    
    private static final String SQL_UPDATE  =   
            "UPDATE con.pto_gastos_admin SET " +
            "   vr_presupuestado_1=?, " +
            "   vr_presupuestado_2=?, " +
            "   vr_presupuestado_3=?, " +
            "   vr_presupuestado_4=?, " +
            "   vr_presupuestado_5=?, " +
            "   vr_presupuestado_6=?, " +
            "   vr_presupuestado_7=?, " +
            "   vr_presupuestado_8=?, " +
            "   vr_presupuestado_9=?, " +
            "   vr_presupuestado_10=?, " +
            "   vr_presupuestado_11=?, " +
            "   vr_presupuestado_12=?, " +
            "   last_update='now()', " +
            "   user_update=?, " +
            "   reg_status=? " +            
            "WHERE " +
            "   dstrct=? AND cuenta=? AND ano=? ";
    
    private static final String SQL_ANULAR  =   
            "UPDATE con.pto_gastos_admin SET " +
            "   reg_status='A', " +
            "   last_update='now()', " +
            "   user_update=? " +            
            "WHERE " +
            "   dstrct=? AND cuenta=? AND ano=? ";
    
    private static final String SQL_LISTAR = 
            "SELECT pto.*, " +
            "       get_destablagen(tcuenta, 'DIV') AS clcuenta, " +
            "       get_destablagen(cagencia, 'CIU') AS agencia, " +
            "       get_destablagen(cunidad, 'UPR') AS unidad, " +
            "       get_destablagen(carea, 'AAA') AS area, " +
            "       get_destablagen(celemento, 'EEE') AS elemento, " +
            "       COALESCE(cia.description, '') AS distrito " +
            "FROM ( SELECT  *, " +
            "               SUBSTRING( cuenta FROM 1 FOR 1 ) AS tcuenta, " +
            "               SUBSTRING( cuenta FROM 2 FOR 2 ) AS cagencia, " +
            "               SUBSTRING( cuenta FROM 4 FOR 3 ) AS cunidad, " +
            "               SUBSTRING( cuenta FROM 7 FOR 3 ) AS carea, " +
            "               SUBSTRING( cuenta FROM 10 FOR 4 )AS celemento " +
            "       FROM    con.pto_gastos_admin ) pto " +
            "LEFT JOIN cia ON ( cia.dstrct = pto.dstrct ) " +
            "WHERE  tcuenta LIKE ? AND cagencia LIKE ? AND cunidad LIKE ? " +
            "       AND carea LIKE ?  AND celemento LIKE ? " +
            "       AND pto.dstrct LIKE ? AND ano LIKE ? AND pto.reg_status!='A' " +
            "ORDER BY cuenta, pto.dstrct, ano ";
    
    private static final String SQL_OBTENER = 
            "SELECT pto.*, " +
            "       get_destablagen(SUBSTRING( cuenta FROM 1 FOR 1 ), 'DIV') AS clcuenta, " +
            "       get_destablagen(SUBSTRING( cuenta FROM 2 FOR 2 ), 'CIU') AS agencia, " +
            "       get_destablagen(SUBSTRING( cuenta FROM 4 FOR 3 ), 'UPR') AS unidad, " +
            "       get_destablagen(SUBSTRING( cuenta FROM 7 FOR 3 ), 'AAA') AS area, " +
            "       get_destablagen(SUBSTRING( cuenta FROM 10 FOR 4 ), 'EEE') AS elemento, " +
            "       COALESCE(cia.description, '') AS distrito " +
            "FROM con.pto_gastos_admin pto " +
            "LEFT JOIN cia ON ( cia.dstrct = pto.dstrct ) " +
            "WHERE cuenta = ? AND pto.dstrct = ? AND ano = ? ";
    
    /** Creates a new instance of PtoGastosAdminDAO */
    public PtoGastosAdminDAO() {
        super("PtoGastosAdminDAO.xml");
    }
    public PtoGastosAdminDAO(String dataBaseName) {
        super("PtoGastosAdminDAO.xml", dataBaseName);
    }
    
    /**
     * Getter for property vector.
     * @return Value of property vector.
     */
    public java.util.Vector getVector() {
        return vector;
    }
    
    /**
     * Setter for property vector.
     * @param vector New value of property vector.
     */
    public void setVector(java.util.Vector vector) {
        this.vector = vector;
    }
    
    /**
     * Getter for property presupuesto.
     * @return Value of property presupuesto.
     */
    public PtoGastosAdmin getPresupuesto() {
        return presupuesto;
    }
    
    /**
     * Setter for property presupuesto.
     * @param presupuesto New value of property presupuesto.
     */
    public void setPresupuesto(PtoGastosAdmin presupuesto) {
        this.presupuesto = presupuesto;
    }
    
    /**
     * Ingresa un registro en el archivo pto_gastos_admin.
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void ingresar() throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_INSERT";
        try {
            con = this.conectarJNDI(query);//JJCastro fase2
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setString(1, presupuesto.getDstrct());
                st.setString(2, presupuesto.getCuenta());
                st.setString(3, presupuesto.getAno());
                double[] vlr_presup = presupuesto.getVlr_presupuestado();
                st.setDouble(4, vlr_presup[0]);
                st.setDouble(5, vlr_presup[1]);
                st.setDouble(6, vlr_presup[2]);
                st.setDouble(7, vlr_presup[3]);
                st.setDouble(8, vlr_presup[4]);
                st.setDouble(9, vlr_presup[5]);
                st.setDouble(10, vlr_presup[6]);
                st.setDouble(11, vlr_presup[7]);
                st.setDouble(12, vlr_presup[8]);
                st.setDouble(13, vlr_presup[9]);
                st.setDouble(14, vlr_presup[10]);
                st.setDouble(15, vlr_presup[11]);
                st.setString(16, presupuesto.getCreation_user());
                st.setString(17, presupuesto.getBase());
                ////System.out.println("....................... ingreso en pto_gastos_admin : " + st.toString());
                st.executeUpdate();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR EN LA INSERCION EN EL ARCHIVO PTO_GASTOS_ADMIN " + e.getMessage() + " " + e.getErrorCode());
        }finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
    }
    
    /**
     * Verifica la existence de un registro en el archivo pto_gastos_admin.
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @param dstrct Distrito
     * @param cuenta C�digo de la cuenta
     * @param ano A�o.
     * @returns El reg_status del registro si existe, de lo contrario null
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public String existe(String dstrct, String cuenta, String ano) throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String reg_status = null;
        String query = "SQL_EXISTE";

        try {
            con = this.conectarJNDI(query);//JJCastro fase2
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setString(1, dstrct);
                st.setString(2, cuenta);
                st.setString(3, ano);

                ////System.out.println("....................... existe en pto_gastos_admin : " + st.toString());

                rs = st.executeQuery();

                if (rs.next()) {
                    reg_status = rs.getString("reg_status");
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR EN LA VERIFICACION EL REGISTRO EN EL ARCHIVO PTO_GASTOS_ADMIN " + e.getMessage() + " " + e.getErrorCode());
        }finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }        
        return reg_status;
    }
    
    /**
     * Actualiza un registro en el archivo pto_gastos_admin.
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void actualizar() throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SQL_UPDATE";
        try {
            con = this.conectarJNDI(query);//JJCastro fase2
            if(con!=null){
                st = con.prepareStatement(this.obtenerSQL(query));
                double[] vlr_presup = presupuesto.getVlr_presupuestado();
                st.setDouble(1, vlr_presup[0]);
                st.setDouble(2, vlr_presup[1]);
                st.setDouble(3, vlr_presup[2]);
                st.setDouble(4, vlr_presup[3]);
                st.setDouble(5, vlr_presup[4]);
                st.setDouble(6, vlr_presup[5]);
                st.setDouble(7, vlr_presup[6]);
                st.setDouble(8, vlr_presup[7]);
                st.setDouble(9, vlr_presup[8]);
                st.setDouble(10, vlr_presup[9]);
                st.setDouble(11, vlr_presup[10]);
                st.setDouble(12, vlr_presup[11]);
                st.setString(13, presupuesto.getCreation_user());
                st.setString(14, presupuesto.getReg_status());
                st.setString(15, presupuesto.getDstrct());
                st.setString(16, presupuesto.getCuenta());
                st.setString(17, presupuesto.getAno());
                ////System.out.println("....................... actualizacion en pto_gastos_admin : " + st.toString());
                st.executeUpdate();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR EN LA ACTUALIZACION EN EL ARCHIVO PTO_GASTOS_ADMIN " + e.getMessage() + " " + e.getErrorCode());
        }finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
    }
    
    /**
     * Anula un registro en el archivo pto_gastos_admin.
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @param dstrct Distrito
     * @param cuenta C�digo de la cuenta
     * @param ano A�o.
     * @param usuario Login del usuario que realiza la anulaci�n.
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void anular(String dstrct, String cuenta, String ano, String user) throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String reg_status = null;
        String query = "SQL_ANULAR";

        try {
            con = this.conectarJNDI(query);//JJCastro fase2
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(cuenta));
                st.setString(1, user);
                st.setString(2, dstrct);
                st.setString(3, cuenta);
                st.setString(4, ano);

                ////System.out.println("....................... anular en pto_gastos_admin : " + st.toString());

                st.executeUpdate();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR EN LA ANULACION DEL REGISTRO EN EL ARCHIVO PTO_GASTOS_ADMIN " + e.getMessage() + " " + e.getErrorCode());
        }finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
    }
    
    /**
     * Realiza una b�squeda por filtros espec�ficos en el archivo pto_gastos_admin
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @param dstrct Distrito
     * @param tcuenta Tipo de cuenta
     * @param agencia C�digo de la Agencia
     * @param unidad C�digo de la unidad
     * @param area C�digo del �rea
     * @param elemento C�digo del elemento del gasto
     * @param ano A�o.
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void listar(String dstrct, String tcuenta, String agencia, String unidad, String area, String elemento, String ano) throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        this.vector = new Vector();
        String query = "SQL_LISTAR";
        
        try {
            con = this.conectarJNDI(query);//JJCastro fase2
            if(con!=null){
                st = con.prepareStatement(this.obtenerSQL(query));
                st.setString(1, tcuenta + "%");
                st.setString(2, agencia + "%");
                st.setString(3, unidad + "%");
                st.setString(4, area + "%");
                st.setString(5, elemento + "%");
                st.setString(6, dstrct + "%");
                st.setString(7, ano + "%");                
                ////System.out.println("....................... listar de pto_gastos_admin : " + st.toString());                
                rs = st.executeQuery();                
                while (rs.next()){
                    this.presupuesto = new PtoGastosAdmin();
                    this.presupuesto.Load(rs);
                    this.presupuesto.setArea(rs.getString("area"));
                    this.presupuesto.setAgencia(rs.getString("agencia"));
                    this.presupuesto.setUnidad(rs.getString("unidad"));                    
                    this.presupuesto.setElemento(rs.getString("elemento"));
                    this.presupuesto.setNom_distrito(rs.getString("distrito"));
                    this.presupuesto.setTipo_cuenta(rs.getString("clcuenta"));
                    this.vector.add(this.presupuesto);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR EN LISTAR  DE EL ARCHIVO PTO_GASTOS_ADMIN " + e.getMessage() + " " + e.getErrorCode());
        }finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
    }
    
    /**
     * Obtiene un registro del archivo pto_gastos_admin
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @param dstrct Distrito
     * @param cuenta C�digo de la cuenta
     * @param ano A�o.
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void obtener(String dstrct, String cuenta, String ano) throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        this.presupuesto = null;
        String query = "SQL_OBTENER";
        
        try {
            con = this.conectarJNDI(query);//JJCastro fase2
            if(con!=null){
                st = con.prepareStatement(this.obtenerSQL(cuenta));
                st.setString(1, cuenta);
                st.setString(2, dstrct);
                st.setString(3, ano);
                ////System.out.println("....................... obtener de pto_gastos_admin : " + st.toString());
                rs = st.executeQuery();
                
                while (rs.next()){
                    this.presupuesto = new PtoGastosAdmin();
                    this.presupuesto.Load(rs);
                    this.presupuesto.setArea(rs.getString("area"));
                    this.presupuesto.setAgencia(rs.getString("agencia"));
                    this.presupuesto.setUnidad(rs.getString("unidad"));                    
                    this.presupuesto.setElemento(rs.getString("elemento"));
                    this.presupuesto.setNom_distrito(rs.getString("distrito"));
                    this.presupuesto.setTipo_cuenta(rs.getString("clcuenta"));
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR AL OBTENER DEL ARCHIVO PTO_GASTOS_ADMIN " + e.getMessage() + " " + e.getErrorCode());
        }
finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
    }
    
}
