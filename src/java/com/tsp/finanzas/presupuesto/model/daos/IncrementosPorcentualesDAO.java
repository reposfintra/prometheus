/*************************************************************
 * Nombre      ............... IncPorcentualesDAO.java
 * Descripcion ............... Incrementos Porcentuales
 * Autor       ............... mfontalvo
 * Fecha       ............... Mayo - 06 - 2005
 * Version     ............... 1.0
 * Copyright   ............... Fintravalores S.A. S.A
 ************************************************************/

package com.tsp.finanzas.presupuesto.model.daos;

import java.sql.*;
import java.util.*;
import com.tsp.finanzas.presupuesto.model.beans.*;
import com.tsp.util.Util;
//import com.tsp.util.connectionpool.PoolManager;
import com.tsp.operation.model.DAOS.MainDAO;


public class IncrementosPorcentualesDAO  extends  MainDAO{
    
    public static final int CREATE       = 0;
    public static final int INSERT       = 1;
    public static final int UPDATE       = 2;
    public static final int UPDATE_STATE = 21;
    public static final int DELETE       = 3;
    public static final int SEARCH_ALL   = 0;
    public static final int SEARCH_ONE   = 1;
    public static final int SEARCH_ANUL  = 2;
    public static final int SEARCH_NANUL = 3;
    
    
    /** Creates a new instance of IncPorcentualesDAO */
    public IncrementosPorcentualesDAO() {
        super("PtoIncrementosPorcentualesDAO.xml");
    }
    public IncrementosPorcentualesDAO(String dataBaseName) {
        super("PtoIncrementosPorcentualesDAO.xml", dataBaseName);
    }
    
    
    /**
     * Procedimiento que ejecuta las querys de manipulacion de tablas de 
     * incrementos
     * @autor: mfontalvo
     * @params Tipo ... Tipo de Operacion ( INSERT; UPDATE; DELETE; UPDATE_STATE; CREATE)
     * @params args ... parametros de las operaciones
     * @throws Exception.
     */
    public void EXECUTE_UPDATE(int Tipo, String args[]) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        String SQL = "";
        try{
            SQL = (Tipo == INSERT) ? "SQL_INSERT_INC_PORC"
                    : (Tipo == UPDATE) ? "SQL_UPDATE_INC_PORC"
                    : (Tipo == DELETE) ? "SQL_DELETE_INC_PORC"
                    : (Tipo == UPDATE_STATE) ? "SQL_UPDATE_ST_INC_PORC"
                    : (Tipo == CREATE) ? "SQL_CREATE_INC_PORC" : "";
            if (!SQL.equals("")) {

                con = this.conectarJNDI(SQL);
                if (con != null) {
                    st = con.prepareStatement(this.obtenerSQL(SQL));//JJCastro fase2
                    for (int i = 0; i < args.length; i++) {
                        st.setString((i + 1), (!args[i].equals("#FECHA#") ? args[i] : Util.getFechaActual_String(6)));
                    }
                    st.executeUpdate();



                } else {
                    throw new SQLException("Tipo de Operacion no definida . [" + Tipo + "]");
                }
            }
        }
        catch(SQLException e) {
            if (Tipo != CREATE)  throw new SQLException("Error en rutina EXECUTE_UPDATE [IncPorcentualesDAO]... \n"+e.getMessage());
        }finally{
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
     } 
    
    
    /**
     * Procedimiento que ejecuta las consultas
     * incrementos
     * @autor: mfontalvo
     * @params Tipo ... Tipo de Operacion ( SQL_BUSCAR_ESP;ANULADSO; NO ANULADOS)
     * @params args ... parametros de las operaciones
     * @throws Exception.
     */
    public List EXECUTE_QUERY (int Tipo, String []args) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        List Resultado = new LinkedList();

        String SQL_COND_ACTIVOS = " WHERE ESTADO <> 'A' AND ANO LIKE (?)";
        String SQL_COND_ANULADOS = " WHERE ESTADO =  'A' AND ANO LIKE (?)";
        String SQL_COND_TODOS = " WHERE ANO LIKE (?) ";
        String SQL_BUSCAR_ESP = " WHERE DSTRCT_CODE = ? AND ANO = ? AND CODIGO_UNIDAD = ? ";

        String query = "SQL_LIST_INC_POR";

        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                String SQL = this.obtenerSQL("SQL_LIST_INC_POR")
                        + ((Tipo == SEARCH_ONE) ? SQL_BUSCAR_ESP
                        : (Tipo == SEARCH_ANUL) ? SQL_COND_ANULADOS
                        : (Tipo == SEARCH_NANUL) ? SQL_COND_ACTIVOS : SQL_COND_TODOS);

                if (!SQL.equals("")) {
                    SQL += " ORDER BY ANO, CODIGO_UNIDAD ";
                    st = con.prepareStatement(SQL);
                    for (int i = 0; i < args.length; i++) {
                        st.setString((i + 1), args[i]);
                    }
                    rs = st.executeQuery();
                    while (rs.next()) {
                        DatosGeneral datos = new DatosGeneral();
                        datos.addValor(rs.getString(1), "ESTADO");
                        datos.addValor(rs.getString(2), "DISTRITO");
                        datos.addValor(rs.getString(3), "ANO");
                        datos.addValor(rs.getString(4), "CUNIDAD");
                        datos.addValor(rs.getString(5), "DUNIDAD");
                        datos.addValor(rs.getString(6), "PANUAL");
                        datos.addValor(rs.getString(7), "PMES01");
                        datos.addValor(rs.getString(8), "PMES02");
                        datos.addValor(rs.getString(9), "PMES03");
                        datos.addValor(rs.getString(10), "PMES04");
                        datos.addValor(rs.getString(11), "PMES05");
                        datos.addValor(rs.getString(12), "PMES06");
                        datos.addValor(rs.getString(13), "PMES07");
                        datos.addValor(rs.getString(14), "PMES08");
                        datos.addValor(rs.getString(15), "PMES09");
                        datos.addValor(rs.getString(16), "PMES10");
                        datos.addValor(rs.getString(17), "PMES11");
                        datos.addValor(rs.getString(18), "PMES12");
                        Resultado.add(datos);
                        datos = null;//Liberar Espacio JJCastro
                    }
                } else {
                    throw new SQLException("Tipo de Operacion no definida . [" + Tipo + "]");
                }
            }
        }
        catch(SQLException e) {
            throw new SQLException("Error en rutina EXECUTE_QUERY [IncPorcentualesDAO]... \n"+e.getMessage());
        }finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return Resultado;
    }
    
    
    /**
     * Procedimiento para Incrementar el presup�esto segun los parametros
     * definidos en la tabla de incrmentos porcentuales.
     * @params UN .... Unidad de Negocio
     * @params AnoI ... A�o a Incrementar
     * @params MesI ... Mes a Incrementar
     * @params AnoB ... A�o base
     * @params MesB ... Mes base
     * @params Usuario . Usuario que realiza el proceso
     * @params model ... Para poder accesar a los insert Mensual del service.
     * @throws Exception.
     */
    public void Incrementar (String UN, String AnoI, String MesI, String AnoB, String MesB, String Usuario, com.tsp.finanzas.presupuesto.model.Model model) throws Exception {
        Connection con = null;
        PreparedStatement st          = null;
        ResultSet         rs          = null;
        String query = "SQL_BUSCAR_IP_UN";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
           
            String SQL = this.obtenerSQL("SQL_BUSCAR_IP_UN");
            List Std = BuscarEstandares(UN);
            Iterator it = Std.iterator();
            
            while (it.hasNext()){
                String STDJOB = (String) it.next();
                st = con.prepareStatement(SQL.replaceAll("#MES#", MesI));
                st.setString(1, STDJOB);
                st.setString(2, AnoB + MesB);
                st.setString(3, UN);
                st.setString(4, AnoI );
                rs = st.executeQuery();
                while (rs.next()){
                    model.DPtoSvc.INSERT_MENSUAL(rs.getString(1), AnoI, MesI, rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(8), Usuario);
                }
            }
        }}
        catch(Exception e) {
            throw new Exception("Error en rutina Incrementar [IncPorcentualesDAO]... \n"+e.getMessage());
        }finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
    }    
    
    
    /**
     * Busca los estandares por unidad de negocio
     * @autor: mfontalvo
     * @params UN ... Unidad de Negocio}
     * @return Listado general de Estandares
     * @throws Exception.
     */
    public List  BuscarEstandares (String UN) throws Exception {
        Connection con = null;
        PreparedStatement st          = null;
        ResultSet         rs          = null;
        List              Resultado   = new LinkedList();
        String query = "SQL_BUSCAR_STD";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, UN);
            rs = st.executeQuery();
            while (rs.next()){
                Resultado.add(rs.getString(1));
            }
        }
        }catch(Exception e) {
            throw new Exception("Error en rutina BuscarEstandares [IncPorcentualesDAO]... \n"+e.getMessage());
        }finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return Resultado;
    }    
    
}
