/*************************************************************
 * Nombre      ............... CostosOperativosDAO.java
 * Descripcion ............... Clase de manipulacion de
 *                             los costros operativos.
 * Autor       ............... mfontalvo
 * Fecha       ............... Diciembre - 12 - 2005
 * Version     ............... 1.0
 * Copyright   ............... Fintravalores S.A. S.A
 ************************************************************/

package com.tsp.finanzas.presupuesto.model.daos;


//import com.tsp.util.connectionpool.PoolManager;
import com.tsp.util.*;
import com.tsp.finanzas.presupuesto.model.beans.*;
import java.util.*;
import java.sql.*;
import com.tsp.operation.model.DAOS.MainDAO;

public class CostosOperativosDAO extends MainDAO {
    
    public CostosOperativosDAO() {
        super("PtoCostosOperativosDAO.xml");
    }
    public CostosOperativosDAO(String dataBaseName) {
        super("PtoCostosOperativosDAO.xml",dataBaseName);
    }   
    
    /**
     * Funcion Que busca las ventas presupuestadas y los costo asociadas a cada
     * una de ellas
     * @autor  mfontalvo
     * @params Ano .......... A�o a buscar
     * @params Mes .......... Mes a buscar
     * @params Agencia ...... Agencia a Buscar
     * @params Cliente ...... Cliente a buscar
     * @return Lista de viajes presupuestados
     * @see getTarifaMoneda()
     * @throws Exception.
     **/
    public TreeMap BuscarVentas(String Ano , String Mes, String Agencia, String Cliente) throws Exception{
        Connection con = null;
        PreparedStatement st  = null;
        ResultSet         rs  = null;        
        TreeMap datos = new TreeMap();        
        List    viajes = new LinkedList();
        TreeMap tasas  = new TreeMap();
        String query = "SQL_GET_VENTAS";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, Ano + Mes);
            st.setString(2, Agencia  );
            st.setString(3, Cliente  );
            rs = st.executeQuery();            
            tasas = getTasa(Ano, Mes);
            
            while(rs.next()){
                DatosGeneral dt = new DatosGeneral();
                dt.addValor( rs.getString("dstrct_code"     ), "DISTRITO"      );
                dt.addValor( rs.getString("agencia"         ), "CODIGOAGENCIA" );
                dt.addValor( rs.getString("nomagencia"      ), "NOMBREAGENCIA" );
                dt.addValor( rs.getString("cliente"         ), "CODIGOCLIENTE" );
                dt.addValor( rs.getString("nomcliente"      ), "NOMBRECLIENTE" );
                dt.addValor( rs.getString("std_job_no"      ), "STDJOBNO"      );
                dt.addValor( rs.getString("std_job_desc"    ), "STDJOBDESC"    );
                dt.addValor( rs.getString("cmensual"        ), "TOTAL"         );
                dt.addValor( rs.getString("origin_code"     ), "CODIGOORIGEN"  );
                dt.addValor( rs.getString("nomorigen"       ), "NOMBREORIGEN"  );
                dt.addValor( rs.getString("destination_code"), "CODIGODESTINO" );
                dt.addValor( rs.getString("nomdestino"      ), "NOMBREDESTINO" );
                dt.addValor( rs.getString("unit_of_work"    ), "UW"            );
                dt.addValor( rs.getString("maint_type"      ), "TIPOVIAJE"     );
                dt.addValor( rs.getString("tarifa"          ), "TARIFA"        );
                dt.addValor( rs.getString("moneda"          ), "MONEDA"        );
                dt.addValor( rs.getString("totalCosto"      ), "TOTALCOSTOUN"  );
                
                String recurso = rs.getString("recurso1");
                for (int k=2;k<=5;k++)
                    recurso += (!recurso.equals("") && !rs.getString("recurso"+k).equals("") ? "; " + rs.getString("recurso"+k): "");
                    dt.addValor( recurso ,  "RECURSO"  );
                    
                    // TASA DE CAMBIO SEGUN LA MONEDA
                    dt.addValor(tasas.get(dt.getValor("MONEDA")).toString(), "TASA");
                    
                    // TOTAL PTO EN PESOS
                    dt.addValor(
                    String.valueOf(
                    Double.parseDouble(dt.getValor("TOTAL"))  *
                    Double.parseDouble(dt.getValor("TARIFA")) *
                    Double.parseDouble(dt.getValor("TASA"))
                    ), "TOTALPTO");
                    
                    // COSTOS OPERATIVOS
                    dt.addValor(
                    String.valueOf(
                    Double.parseDouble(dt.getValor("TOTAL"))  *
                    Double.parseDouble(dt.getValor("TOTALCOSTOUN"))
                    ), "TOTALCOSTO");
                    
                    viajes.add(dt);
                    dt = null;//Liberar Espacio JJCastro
            }
        }
        catch(Exception e) {
            throw new Exception("Error en rutina BuscarVentas [CostosOperativosDAO]... \n"+e.getMessage());
        }finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        datos.put("viajes", viajes);
        datos.put("tasas" , tasas );
        return datos;
    }
    
    
    /**
     * Funcion Que busca las tasa de monedas
     * @autor  mfontalvo
     * @params Ano .......... A�o a buscar
     * @params Mes .......... Mes a buscar
     * @throws Exception.
     **/
    private TreeMap getTasa(String Ano, String Mes) throws Exception{
        Connection con = null;
        TreeMap tasas = new TreeMap();
        tasas.put("DOL", "0");
        tasas.put("BOL", "0");
        tasas.put("DTF", "0");
        tasas.put("PES", "1");
        tasas.put("", "0");
        String query = "SQL_TASA";
        
        PreparedStatement st = null;
        ResultSet         rs = null;
        
        try{
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, Ano);
            st.setString(2, Mes);
            rs = st.executeQuery();
            while(rs.next()){
                tasas.put("DOL", rs.getString(1));
                tasas.put("BOL", rs.getString(2));
                tasas.put("DTF", rs.getString(3));
                break;
            }
        }catch (Exception ex){
            throw new Exception("Error en getTasa [CostosOperativosDAO] ...\n" + ex.getMessage());
        }finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return tasas;
    }
    
    
    
    /**
     * Funcion Que busca todos los elementos del gasto asociados a los
     * costos operativos
     * @autor  mfontalvo
     * @return Listado de elementos del gasto
     * @throws Exception.
     **/
    public List ElementosCostosOperativos() throws Exception{
        Connection con = null;
        PreparedStatement st  = null;
        ResultSet         rs  = null;
        List datos = new LinkedList();
        String query = "SQL_ELEMENTOS_COSTOS_OPERATIVOS";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            rs = st.executeQuery();
            while(rs.next()){
                DatosGeneral dt = new DatosGeneral();
                dt.addValor( rs.getString("elemento"),     "codigo"      );
                dt.addValor( rs.getString("descripcion" ), "descri"      );
                datos.add(dt);
                dt = null;//Liberar Espacio JJCastro
            }
        }
        catch(Exception e) {
            throw new Exception("Error en rutina ElementosCostosOperativos [CostosOperativosDAO]... \n"+e.getMessage());
        }finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return datos;
    }
    
    
    /**
     * Procedimiento que alamacena los costos operativos asociados a un estandar
     * y un periodo
     * @autor  mfontalvo
     * @params Estandar. .......... Estandar a guardar
     * @params Ano ................ Ano a guardar
     * @params Mes ................ Mes a guardar
     * @params CodigosCostos ...... Array de los codigos de costos
     * @params Valores............. Array de valores de los codigos
     * @params Usuario ............ Usuario de Creacion
     * @throws Exception.
     **/
    public void AdicionarCostos(String Estandar, String Ano, String Mes, String[] CodigosCostos, String []Valores, String Usuario) throws Exception {
        Connection con = null;
        PreparedStatement st  = null;
        String query = "SQL_DELETE";
        String query2 = "SQL_INSERT";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1 , "FINV"            );
            st.setString(2 , Estandar         );
            st.setString(3 , Ano              );
            st.setString(4 , Mes              );
            st.executeUpdate();
            
            st = con.prepareStatement(this.obtenerSQL(query2));//JJCastro fase2
            for (int i = 0; CodigosCostos!=null && i < CodigosCostos.length ;  i++ ){
                st.clearParameters();
                st.setString(1 , "FINV"            );
                st.setString(2 , Estandar         );
                st.setString(3 , Ano              );
                st.setString(4 , Mes              );
                st.setString(5 , CodigosCostos[i] );
                st.setString(6 , Valores[i].replaceAll(",",""));
                st.setString(7 , Usuario          );
                st.setString(8 , Util.getFechaActual_String(6));
                st.setString(9 , Usuario          );
                st.setString(10, Util.getFechaActual_String(6));
                st.executeUpdate();
            }
        }
        catch(Exception e) {
            throw new Exception("Error en rutina AdicionarCostos [CostosOperativosDAO]... \n"+e.getMessage());
        }finally{
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
    }
    
    
    /**
     * Funcion que busca los elementos de Costos Operativos.
     * @autor  mfontalvo
     * @params Estandar. .......... Estandar a consultar
     * @params Ano ................ Ano a consultar
     * @params Mes ................ Mes a consultar
     * @return Lista de elementos del gastos que ya tiene asignado un pto
     * @throws Exception.
     **/
    public List BuscarElementosCostosOperativos(String Estandar, String Ano, String Mes) throws Exception{
        Connection con = null;
        PreparedStatement st  = null;
        ResultSet         rs  = null;
        List datos = new LinkedList();
        String query = "SQL_SEARCH";
        try {
        con = this.conectarJNDI(query);
        st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, "FINV"    );
            st.setString(2, Estandar );
            st.setString(3, Ano      );
            st.setString(4, Mes      );
            rs = st.executeQuery();
            while(rs.next()){
                DatosGeneral dt = new DatosGeneral();
                dt.addValor( rs.getString("elemento"), "elemento" );
                dt.addValor( rs.getString("valor"   ), "valor"    );
                datos.add(dt);
                dt = null;//Liberar Espacio JJCastro
            }
        }
        catch(Exception e) {
            throw new Exception("Error en rutina BuscarElementosCostosOperativos [CostosOperativosDAO]... \n"+e.getMessage());
        }finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return datos;
    }
    
    
    
    /**
     * Funcion que busca todos los clientes con su agencia due�a
     * @autor  mfontalvo
     * @return Lista de clientes y sus agencias
     * @throws Exception.
     **/
    public List BuscarAgenciasClientes() throws Exception{
        Connection con = null;
        PreparedStatement st  = null;
        ResultSet         rs  = null;
        List datos = new LinkedList();
        String query = "SQL_AGENCIAS_CLIENTES";
        try {
            con = this.conectarJNDI(query);
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            rs = st.executeQuery();
            while(rs.next()){
                DatosGeneral dt = new DatosGeneral();
                dt.addValor( rs.getString(1), "cag" );
                dt.addValor( rs.getString(2), "nag" );
                dt.addValor( rs.getString(3), "ccl" );
                dt.addValor( rs.getString(4), "ncl" );
                datos.add(dt);
                dt = null;//Liberar Espacio JJCastro
            }
        }
        catch(Exception e) {
            throw new Exception("Error en rutina BuscarAgenciasClientes [CostosOperativosDAO]... \n"+e.getMessage());
        }finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return datos;
    }
    
    
    /**
     * Funcion que genera las vistas de Costos Operativos
     * @autor  mfontalvo
     * @params TipoVista. .......... tipo de Vista de a generar
     * @params Periodo ............. periodo a consultar
     * @params Prolongacion ........ numero de Periodos a ver
     * @params filtros ............. array de filtros para la generacion de la vista
     * @return Lista que contiene los datos de la vista
     * @see    getQueryVista():
     * @throws Exception.
     **/
    public List GeneracionVista(String TipoVista, String Periodo, int  Prolongacion, String []filtros ) throws Exception{
        Connection con = null;
        PreparedStatement st  = null;
        ResultSet         rs  = null;
        List              datos       = new LinkedList();
        try {            
            String Query = getQueryVista(TipoVista, Periodo, Prolongacion, filtros);
            if (Query.equals(""))
                throw new Exception("No se pudo generar la vista.");
            con = this.conectarJNDI("SQL_GET_VENTAS");
            if (con==null)
                throw new Exception("Sin conexion");
            
            st = con.prepareStatement(Query);
            rs = st.executeQuery();
            while(rs.next()){
                DatosGeneral dt = new DatosGeneral();
                dt.addValor( rs.getString(1), "CODIGO" );
                dt.addValor( rs.getString(2), "DESCRIPCION" );
                for (int i=0; i<Prolongacion; i++)
                    dt.addValor( rs.getString(3+i), "v"+i );
                datos.add(dt);
                dt = null;//Liberar Espacio JJCastro
            }
            
            
        }
        catch(Exception e) {
            e.printStackTrace();
            throw new Exception("Error en rutina GeneracionVista [CostosOperativosDAO]... \n"+
            "Parametros : ["+ TipoVista +","+ Periodo +","+ Prolongacion + "," + filtros +"]\n"+e.getMessage());
        }finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return datos;
    }
    
    
    /**
     * Funcion que genera el query de la creacion de la vista
     * @autor  mfontalvo
     * @params TipoVista. .......... tipo de Vista de a generar
     * @params Periodo ............. periodo a consultar
     * @params Prolongacion ........ numero de Periodos a ver
     * @params filtros ............. array de filtros para la generacion de la vista
     * @return Query
     * @throws Exception.
     **/
    private String getQueryVista(String tipoVista, String Periodo, int Prolongacion, String [] Filtro) throws Exception {
        
        String Base = "", newCol = "", newJoin = "";
        
        if (tipoVista.toUpperCase().equals("AG")) {
            Base =
            " SELECT  a.id_agencia, a.nombre, #COLS# FROM agencia a  #JOINS# ORDER BY a.nombre" ;
            
            newCol   = " coalesce(b#IND#.costo,0) ";
            newJoin  = " LEFT JOIN ( SELECT a.agencia, sum(a.cmensual * b.valor) as costo FROM fin.pto_ventas a LEFT JOIN fin.pto_costos_operativos b ON (b.distrito = a.dstrct_code and b.std_job_no = a.std_job_no and b.ano = a.ano and b.mes = a.mes)  WHERE a.ano||a.mes = '#PARAM#' and estado <> 'A' GROUP BY agencia ) b#IND# ON (b#IND#.agencia = a.id_agencia) ";
        }
        else if (tipoVista.toUpperCase().equals("CT")) {
            Base =
            " SELECT a.elemento, a.descripcion, #COLS# " +
            " FROM ( SELECT elemento, descripcion FROM abc.abc_subclas_elem WHERE subclasificaccion in ('02','03','04','05') )  a " +
            " #JOINS# ORDER BY a.descripcion           " ;
            
            newCol   = " coalesce(b#IND#.costo,0) ";
            newJoin  = " LEFT JOIN ( SELECT a.elemento, sum(a.valor * b.cmensual) as costo FROM fin.pto_costos_operativos a left join fin.pto_ventas b on (b.dstrct_code = a.distrito and b.std_job_no = a.std_job_no and b.ano = a.ano and b.mes = a.mes  and b.estado <> 'A' #FILTRO# ) WHERE  a.ano||a.mes = '#PARAM#'  GROUP BY a.elemento ) b#IND# ON (b#IND#.elemento = a.elemento) ";
            
            
            String filtro = "";
            if (!Filtro[0].equals("")) filtro = " and b.agencia = '"+ Filtro[0] +"'" ;
            newJoin = newJoin.replaceAll("#FILTRO#", filtro);
            
        }
        else if (tipoVista.toUpperCase().equals("CL")) {
            Base =
            " SELECT  a.codcli, a.nomcli, #COLS# FROM cliente a  #JOINS# #FILTRO-2# ORDER BY a.nomcli" ;
            
            newCol   = " coalesce(b#IND#.costo,0) ";
            newJoin  = " LEFT JOIN ( SELECT a.cliente, sum(a.cmensual * b.valor) as costo FROM fin.pto_ventas a LEFT JOIN fin.pto_costos_operativos b ON (b.distrito = a.dstrct_code and b.std_job_no = a.std_job_no and b.ano = a.ano and b.mes = a.mes)  WHERE a.ano||a.mes = '#PARAM#' #FILTRO-1# and estado <> 'A' GROUP BY cliente ) b#IND# ON (b#IND#.cliente = a.codcli) ";
            
            String filtro_1 = "";
            if (!Filtro[0].equals("") && !Filtro[1].equals("")) filtro_1 = " and a.agencia = '"+ Filtro[0] +"' and elemento = '"+ Filtro[1] +"' " ;
            String filtro_2 = "";
            if (!Filtro[0].equals("")) filtro_2 = " WHERE a.agduenia = '"+ Filtro[0] +"' ";
            
            
            Base    = Base.replaceAll   ("#FILTRO-2#", filtro_2);
            newJoin = newJoin.replaceAll("#FILTRO-1#", filtro_1);
            
            
        }
        
        
        /////////////////////////////////////////////////////////////////////////
        String Cols  = "";
        String Joins = "";
        for (int i=0; i<Prolongacion; i++ ){
            int Periodo1 = UtilFinanzas.getNextPeriodoNormal(Periodo, i);
            Cols  += newCol.replaceAll("#IND#", String.valueOf(i)) + ",";
            Joins += newJoin.replaceAll("#IND#", String.valueOf(i)).replaceAll("#PARAM#", String.valueOf(Periodo1)) ;
        }
        Cols = Cols.substring(0,Cols.length()-1);
        
        return Base.replaceAll("#COLS#" , Cols ).replaceAll("#JOINS#", Joins);
    }
    
    
    
}
