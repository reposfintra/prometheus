/*************************************************************
 * Nombre      ............... ReportesDAO.java
 * Descripcion ............... Opciones de prespuesto
 * Autor       ............... mfontalvo
 * Fecha       ............... Julio - 31 - 2005
 * Version     ............... 1.0
 * Copyright   ............... Fintravalores S.A. S.A
 ************************************************************/

package com.tsp.finanzas.presupuesto.model.daos;

import java.sql.*;
import java.util.*;
import com.tsp.finanzas.presupuesto.model.beans.*;
import com.tsp.util.UtilFinanzas;
import com.tsp.operation.model.DAOS.MainDAO;



public class ReportesDAO extends MainDAO {


    /** Creates a new instance of ReportesPtoDAO */
    public ReportesDAO() {
        super ("PtoReportesDAO.xml");
    }
    public ReportesDAO(String dataBaseName) {
        super ("PtoReportesDAO.xml", dataBaseName);
    }

    
    /**
     * Procedimiento que toma los viajes prespuestados dados los siguentes
     * parametros
     * @autor  mfontalvo
     * @params Distrito ........ Distrito a consultar
     * @params Agencia ......... Agencia a consultar
     * @params Cliente ......... Cliente a consultar
     * @params Ano ............. A�o a consultar
     * @params Mes ............. Mes a consultar
     * @throws Exception.
     * @return Lista con los viajes presupuestados
     *         segregados en niveles de agencia, cliente y estandar.
     */
    public TreeMap loadViajesPt(String Distrito, String AgenciaDespacho, String Agencia, String Cliente, String Estandar, String Ano , String Mes, int diaInicial, int diaFinal) throws Exception{
        
        PreparedStatement st = null;
        ResultSet rs = null;
        TreeMap viajes = new TreeMap();
        Connection con = null;
        String query = "SQL_VENTAS_PT";
        try {
            con = this.conectarJNDI(query);//JJCastro fase2
            if (con != null) {
                String qry = obtenerSQL("SQL_VENTAS_PT");
                String resto = "";
                if (!AgenciaDespacho.equals("")) {
                    resto += " AND c.agasoc     = '" + AgenciaDespacho + "' ";
                }
                if (!Agencia.equals("")) {
                    resto += " AND v.agencia    = '" + Agencia + "' ";
                }
                if (!Cliente.equals("")) {
                    resto += " AND v.cliente    = '" + Cliente + "' ";
                }
                if (!Estandar.equals("")) {
                    resto += " AND v.std_job_no = '" + Estandar + "' ";
                }
                qry = qry.replace("#RESTO#", resto);
                st = con.prepareStatement(qry);
                st.setString(1, Ano + Mes);
                st.setString(2, Distrito);

                ////System.out.println("PRESUPUESTADO:  "  + st.toString());
                rs = st.executeQuery();
                while (rs.next()) {
                    Actualizar(viajes, rs, "P", diaInicial, diaFinal);
                }
            }
        }
        catch(Exception e) {
            e.printStackTrace();
            throw new SQLException("Error en rutina loadViajesPt [ReportesDAO]... \n"+e.getMessage());
        }finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return viajes;
    }
    
    
    /**
     * Procedimiento que toma los viajes ejecutados dados los siguentes
     * parametros
     * @autor  mfontalvo
     * @params Distrito ........ Distrito a consultar
     * @params Agencia ......... Agencia a consultar
     * @params Cliente ......... Cliente a consultar
     * @params Ano ............. A�o a consultar
     * @params Mes ............. Mes a consultar
     * @params lista ........... lista de Viajes presupuestados
     * @throws Exception.
     * @return Lista con los viajes presupuestados y ejecutados
     *         segregados en niveles de agencia, cliente y estandar.
     * @see Actulizar, Insertar
     */
    public TreeMap loadViajesEj(String Distrito, String AgenciaDespacho, String Agencia, String Cliente, String Estandar, String Ano , String Mes, int diaInicial, int diaFinal, TreeMap viajes) throws Exception{
        
        PreparedStatement st  = null;
        ResultSet         rs  = null;
        Connection        con = null;
        String query = "SQL_VENTAS_EJ";
        
        if (viajes == null)
            viajes = new TreeMap();
        
        try {
            con = this.conectarJNDI(query);//JJCastro fase2
            if (con != null) {

                String qry = obtenerSQL("SQL_VENTAS_EJ");
                String resto1 = "", resto2 = "";
                if (!AgenciaDespacho.equals("")) {
                    resto1 = " AND ci.agasoc = '" + AgenciaDespacho + "' ";
                }

                if (!Agencia.equals("")) {
                    resto2 += " WHERE a.agencia  = '" + Agencia + "' ";
                }
                if (!Cliente.equals("")) {
                    resto2 += " AND a.cliente    = '" + Cliente + "' ";
                }
                if (!Estandar.equals("")) {
                    resto2 += " AND a.std_job_no = '" + Estandar + "' ";
                }
                qry = qry.replace("#RESTO1#", resto1);
                qry = qry.replace("#RESTO2#", resto2);
                st = con.prepareStatement(qry.toString());

                int dia = Integer.parseInt(UtilFinanzas.diaFinal(Ano, Mes));
                if (diaFinal > dia) {
                    diaFinal = dia;
                }
                st.setString(1, Ano + Mes + UtilFinanzas.DiaFormat(diaInicial));
                st.setString(2, Ano + Mes + UtilFinanzas.DiaFormat(diaFinal));
                ////System.out.println("EJECUTADO:  "  + st.toString());
                rs = st.executeQuery();
                while (rs.next()) {
                    Actualizar(viajes, rs, "E", diaInicial, diaFinal);
                }
            }
        }
        catch(Exception e) {
            e.printStackTrace();
            throw new SQLException("Error en rutina loadViajesEj [ReportesDAO]... \n"+e.getMessage());
        }finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return viajes;
    }
    
    
    /**
     * Procedimiento que actualiza los viajes presupuestados con su respectiva
     * contraparte (viajes ejecutados) apartir de los siguientes parametros
     * @autor  mfontalvo
     * @params lista , lista de Viajes presupuestados
     * @params rs, resultset donde vienen los datos
     * @paramas tipo, tipo de Actualizacion de los datos del estandares
     * @throws Exception.
     * @return boolean que indica si se pudo actualizar o no
     */
    private void Actualizar(TreeMap lista, ResultSet rs, String tipo, int diaInicial, int diaFinal ) throws Exception {
        String vAgencia       = rs.getString("agencia");
        String vAgenciaNombre = rs.getString("nomagencia");
        String vCliente       = rs.getString("cliente");
        String vClienteNombre = rs.getString("nomcliente");
        String vStandar       = rs.getString("std_job_no");
        String vDescripcion   = rs.getString("std_job_desc");
        if (lista!=null){
            /////////////////////////////////////////////////////////////////////////////////
            ViajesAgencia ag = null;
	    ViajesCliente cl = null;
            ViajesStandar st = null;
            
            ag = (ViajesAgencia) lista.get(vAgencia);
            if ( ag == null ){
                ag = new ViajesAgencia ();
                ag.setAgencia( vAgencia );
                ag.setAgenciaNombre( vAgenciaNombre );
            }
            
            cl = (ViajesCliente) ag.getListadoClientes().get( vCliente );
            if ( cl == null ){
                cl = new ViajesCliente();
                cl.setCliente( vCliente );
                cl.setClienteNombre( vClienteNombre );
            }
            
            
            st = (ViajesStandar) cl.getListaStandar().get( vStandar );
            if  ( st == null ){
                st = new ViajesStandar();
                st.setStdJobNo   (vStandar);
                st.setStdJobDesc (vDescripcion);
            }
            
            // asignacion de viajes
            if (tipo.equalsIgnoreCase("P")){
                for (int i=diaInicial ; i<=diaFinal ; i++) st.setViajePtdo( i , rs.getInt(i+6));
            }
            else if (tipo.equalsIgnoreCase("E")){
                int vDia    = rs.getInt("dia");
                int vViajes = rs.getInt("viajes");
                st.setViajeEjdo  (vDia, vViajes);
            }
            
            cl.addStandar(st);
            ag.addCliente(cl);
            
            lista.put(ag.getAgencia(), ag);
        }
            
    }
    
    
    
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //  EXPORTACION PRESUPUESTO A EXCEL
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    
    /**
     * Funcion Que busca las ventas presupuestadas y los costo asociadas a cada
     * una de ellas
     * @autor  mfontalvo
     * @params Ano .......... A�o a buscar
     * @params Mes .......... Mes a buscar
     * @return Lista de viajes presupuestados
     * @see getTasa.
     * @throws Exception.
     **/
    public TreeMap BuscarVentas(String Ano , String Mes) throws Exception{
        Connection con = null;
        PreparedStatement st  = null;
        ResultSet         rs  = null;
        TreeMap datos  = new TreeMap();      
        List    viajes = new LinkedList();
        TreeMap tasas  = new TreeMap();
        String query = "SQL_VENTAS";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, Ano + Mes);
            rs = st.executeQuery();
            tasas = getTasa(Ano, Mes);            
            while(rs.next()){
                DatosGeneral dt = new DatosGeneral();                
                dt.addValor( rs.getString("dstrct_code"     ), "DISTRITO"      );
                dt.addValor( rs.getString("agencia"         ), "CODIGOAGENCIA" );
                dt.addValor( rs.getString("nomagencia"      ), "NOMBREAGENCIA" );
                dt.addValor( rs.getString("cliente"         ), "CODIGOCLIENTE" );
                dt.addValor( rs.getString("nomcliente"      ), "NOMBRECLIENTE" );
                dt.addValor( rs.getString("std_job_no"      ), "STDJOBNO"      );
                dt.addValor( rs.getString("std_job_desc"    ), "STDJOBDESC"    );
                dt.addValor( rs.getString("cmensual"        ), "TOTAL"         );
                dt.addValor( rs.getString("origin_code"     ), "CODIGOORIGEN"  );
                dt.addValor( rs.getString("nomorigen"       ), "NOMBREORIGEN"  );
                dt.addValor( rs.getString("destination_code"), "CODIGODESTINO" );
                dt.addValor( rs.getString("nomdestino"      ), "NOMBREDESTINO" );
                dt.addValor( rs.getString("unit_of_work"    ), "UW"            );
                dt.addValor( rs.getString("maint_type"      ), "TIPOVIAJE"     );
                dt.addValor( rs.getString("tarifa"          ), "TARIFA"        );
                dt.addValor( rs.getString("moneda"          ), "MONEDA"        );
                // RECURSOS
                String recurso = rs.getString("recurso1");
                for (int k=2;k<=5;k++)
                    recurso += (!recurso.equals("") && !rs.getString("recurso"+k).equals("") ? "; " + rs.getString("recurso"+k): "");
                    dt.addValor( recurso ,  "RECURSO"  );
                    // TASA DE CAMBIO SEGUN LA MONEDA
                    dt.addValor(tasas.get(dt.getValor("MONEDA")).toString(), "TASA");
                    
                    // TOTAL PTO EN PESOS
                    dt.addValor(
                    String.valueOf(
                    Double.parseDouble(dt.getValor("TOTAL"))  *
                    Double.parseDouble(dt.getValor("TARIFA")) *
                    Double.parseDouble(dt.getValor("TASA"))
                    ), "TOTALPTO");
                    
                    // carga de viajes diarios
                    for (int i=0;i<31;i++)
                        dt.addValor( rs.getString( "cdia" + UtilFinanzas.DiaFormat(i+1) ), "V" + i );
                    viajes.add(dt);
                    dt = null;//Liberar Espacio JJCastro
            }
        }}
        catch(Exception e) {
            throw new Exception("Error en rutina BuscarVentas [ReportesDAO]... \n"+e.getMessage());
        }finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        datos.put("viajes", viajes);
        datos.put("tasas" , tasas );
        
        return datos;
    }
    
    
    /**
     * Funcion Que busca las tasa de monedas
     * @autor  mfontalvo
     * @params Ano .......... A�o a buscar
     * @params Mes .......... Mes a buscar
     * @throws Exception.
     **/
    private TreeMap getTasa(String Ano, String Mes) throws Exception{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet         rs = null;
        
        // inicializacion de parametros a retornar
        TreeMap tasas = new TreeMap();
        tasas.put("DOL", "0");
        tasas.put("BOL", "0");
        tasas.put("DTF", "0");
        tasas.put("PES", "1");
        tasas.put("", "0");
        String query = "SQL_TASA";
        
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, Ano);
            st.setString(2, Mes);
            rs = st.executeQuery();
            while(rs.next()){
                tasas.put("DOL", rs.getString(1));
                tasas.put("BOL", rs.getString(2));
                tasas.put("DTF", rs.getString(3));
                break;
            }
        }}catch (Exception ex){
            throw new Exception("Error en getComplemento [getTasa] ...\n" + ex.getMessage());
        }finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return tasas;
    }
    
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //  EXPORTACION HISTORIAL PRESUPUESTO A EXCEL
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    
    /**
     * funcion que extrae todos los moviminetos presupuestados de un periodo
     * @autor  mfontalvo
     * @params Ano .... Ano a exportar
     * @params Mes .... Mes a exportar
     * @throws Exception.
     **/
    public List HistorialMensual(String Agencia, String Cliente, String Estandar, String Ano , String Mes) throws Exception{
        Connection con = null;
        PreparedStatement st  = null;
        ResultSet         rs  = null;
        List    viajes = new LinkedList();
        String query = "SQL_BUSCAR_TODAS_LAS_VENTAS";
        
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, Agencia);
            st.setString(2, Cliente);
            st.setString(3, Estandar);
            st.setString(4, Ano + Mes);
            rs = st.executeQuery();
            
            while(rs.next()){
                DatosGeneral dt = new DatosGeneral();
                
                dt.addValor( rs.getString("dstrct_code"     ), "DISTRITO"      );
                dt.addValor( rs.getString("agencia"         ), "CODIGOAGENCIA" );
                dt.addValor( rs.getString("nomagencia"      ), "NOMBREAGENCIA" );
                dt.addValor( rs.getString("cliente"         ), "CODIGOCLIENTE" );
                dt.addValor( rs.getString("nomcliente"      ), "NOMBRECLIENTE" );
                dt.addValor( rs.getString("std_job_no"      ), "STDJOBNO"      );
                dt.addValor( rs.getString("std_job_desc"    ), "STDJOBDESC"    );
                dt.addValor( rs.getString("origin_code"     ), "CODIGOORIGEN"  );
                dt.addValor( rs.getString("nomorigen"       ), "NOMBREORIGEN"  );
                dt.addValor( rs.getString("destination_code"), "CODIGODESTINO" );
                dt.addValor( rs.getString("nomdestino"      ), "NOMBREDESTINO" );
                dt.addValor( rs.getString("unit_of_work"    ), "UW"            );
                dt.addValor( rs.getString("maint_type"      ), "TIPOVIAJE"     );
                dt.addValor( rs.getString("tarifa"          ), "TARIFA"        );
                dt.addValor( rs.getString("moneda"          ), "MONEDA"        );
                
                String recurso = rs.getString("recurso1");
                for (int k=2;k<=5;k++)
                    recurso += (!recurso.equals("") && !rs.getString("recurso"+k).equals("") ? "; " + rs.getString("recurso"+k): "");
                dt.addValor( recurso ,  "RECURSO"  );
                viajes.add(dt);


                dt = null;//Liberar Espacio JJCastro
            }
        }}
        catch(Exception e) {
            throw new Exception("Error en rutina HistorialMensual [ReportesDAO]... \n"+e.getMessage());
        }finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return viajes;
    }

    
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //  COSTOS OPERATIVOS
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    
    /**
     * Procedimiento que toma los viajes prespuestados dados los siguentes
     * parametros
     * @autor  Ing. Juan M. Escandon Perez
     * @params Distrito ........ Distrito a consultar
     * @params Agencia ......... Agencia a consultar
     * @params Cliente ......... Cliente a consultar
     * @params Ano ............. A�o a consultar
     * @params Mes ............. Mes a consultar
     * @throws Exception.
     * @return Lista con los Costos Operativos presupuestados
     *         segregados en niveles de agencia, cliente y estandar.
     */
    public List loadCostosPt(String Distrito, String Agencia, String Cliente, String Estandar, String Ano , String Mes) throws Exception{
        Connection con = null;
        PreparedStatement st  = null;
        ResultSet         rs  = null;
        
        List viajes = new LinkedList();
        String query = "SQL_VENTAS_PT";
    /*   con = this.conectarJNDI(query);
        if(con!=null){
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        
        try {
            st = con.prepareStatement("SQL_VENTAS_PT");
            st.setString(1, Ano);
            st.setString(2, Mes);
            st.setString(3, (Distrito.equals("")?"%":Distrito));
            st.setString(4, (Agencia.equals("") ?"%":Agencia ));
            st.setString(5, (Cliente.equals("") ?"%":Cliente ));
            st.setString(6, (Estandar.equals("")?"%":Estandar));
            ////System.out.println("SQL: " + st.toString());
            rs = st.executeQuery();
            
            ViajesAgencia ag  = null;
            ViajesCliente cl  = null;
            ViajesStandar std = null;
            
            while(rs.next()){
                
                std = new ViajesStandar();
                std.setStdJobNo   (rs.getString(3));
                std.setStdJobDesc(getDescripcion(3, std.getStdJobNo()));
                std.setListCostosOperativos(getCostosOperativos(std.getStdJobNo(),Ano, Mes));
                std.setValor_total_costos(getValorTotalCostos(std.getStdJobNo(),Ano, Mes));
                
                for (int i=1 ; i<=31 ; i++){                    
                    if( std.getValor_total_costos() > 0 ){
                        std.setCostosPtdo( i , (rs.getInt(i+3)*std.getValor_total_costos()));
                        std.setViajePtdo( i , rs.getInt(i+3));
                    }
                    else
                        std.setCostosPtdo( i , 0);
                        std.setViajePtdo( i , rs.getInt(i+3));
                }
                if (cl == null || !cl.getCliente().equals(rs.getString(2))){
                    if (cl!=null)  ag.addCliente(cl);
                    cl = new ViajesCliente();
                    cl.setCliente      (rs.getString(2));
                    cl.setClienteNombre(getDescripcion(2, cl.getCliente()));
                    cl.setCoperativos_cliente(getValorTotalCliente(cl.getCliente(),Ano,Mes));
                }
                cl.addStandar(std);
                
                if (ag == null || !ag.getAgencia().equals(rs.getString(1))) {
                    if (ag!=null) viajes.add(ag);
                    
                    ag = new ViajesAgencia();
                    ag.setAgencia      (rs.getString(1));
                    ag.setAgenciaNombre(getDescripcion(1, ag.getAgencia()));
                    ag.setCoperativos_agencia(getValorTotalAgencia(ag.getAgencia(),Ano,Mes));
                }
            }
            if (ag != null) {
                ViajesCliente ult = (ag.getListadoClientes().size()>0?
                (ViajesCliente) ag.getListadoClientes().get(ag.getListadoClientes().size()-1):
                    null);
                    if (cl!=null && (ult==null || !ult.getCliente().equals(cl.getCliente())) ) ag.addCliente(cl);
                    viajes.add(ag);
            }
            
        }
        catch(Exception e) {
            throw new SQLException("Error en rutina loadViajesPt [ReportesDAO]... \n"+e.getMessage());
        }finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }*/
        return viajes;
    }
    
    
    /**
     * Procedimiento que toma los costos operativos prespuestados dados los siguentes
     * parametros
     * @autor  Ing. Juan M. Escandon Perez
     * @params std_job ........  Standard a consultar
     * @params Ano ............. A�o a consultar
     * @params Mes ............. Mes a consultar
     * @throws Exception.
     * @return Lista con los Costos Operativos presupuestados
     *         segregados en nivel de estandar.
     */
    private List getCostosOperativos(String std_job, String ano, String mes) throws Exception{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet         rs = null;
        List Lista = new LinkedList();
        String query = "SQL_COSTOS_OPERATIVOS";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, std_job);
            st.setString(2, ano);
            st.setString(3, mes);
            rs = st.executeQuery();
            while(rs.next()){
                Lista.add(CostosOperativos.loadItem(rs));
            }

            }}catch (Exception ex){
            throw new Exception("Error en getCostosOperativos [ReportesDAO] ...\n" + ex.getMessage());
        }finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return Lista;
    }
    
    
    /**
     * Procedimiento que toma los costos operativos prespuestados dados los siguentes
     * parametros
     * @autor  Ing. Juan M. Escandon Perez
     * @params std_job ........  Standard a consultar
     * @params Ano ............. A�o a consultar
     * @params Mes ............. Mes a consultar
     * @throws Exception.
     * @return double con el valor Costos Operativos presupuestados
     *         segregados en nivel de estandar.
     */
    private double getValorTotalCostos(String std_job, String ano, String mes) throws Exception{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet         rs = null;
        double valor = 0;
        String query = "SQL_TOTAL_COSTOS";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, std_job);
            st.setString(2, ano);
            st.setString(3, mes);
            rs = st.executeQuery();
            if(rs.next()){
                valor = rs.getDouble("total");
            }
            }}catch (Exception ex){
            throw new Exception("Error en getCostosOperativos [ReportesDAO] ...\n" + ex.getMessage());
        }finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return valor;        
    }
    
    
    /**
     * Procedimiento que toma los costos operativos prespuestados dados los siguentes
     * parametros
     * @autor  Ing. Juan M. Escandon Perez
     * @params cliente ........  Cliente a consultar
     * @params Ano ............. A�o a consultar
     * @params Mes ............. Mes a consultar
     * @throws Exception.
     * @return double con el valor Costos Operativos presupuestados
     *         segregados en nivel de cliente.
     */
    private double getValorTotalCliente(String cliente, String ano, String mes) throws Exception{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet         rs = null;
        double valor = 0;
        String query = "SQL_TOTAL_CLIENTE";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, cliente);
            st.setString(2, ano);
            st.setString(3, mes);
            rs = st.executeQuery();
            if(rs.next()){
                valor = rs.getDouble("valor");
            }
            }}catch (Exception ex){
            throw new Exception("Error en getValorTotalCliente [ReportesDAO] ...\n" + ex.getMessage());
        }finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return valor;
    }
    
    
    /**
     * Procedimiento que toma los costos operativos prespuestados dados los siguentes
     * parametros
     * @autor  Ing. Juan M. Escandon Perez
     * @params agencia ........  agencia a consultar
     * @params Ano ............. A�o a consultar
     * @params Mes ............. Mes a consultar
     * @throws Exception.
     * @return double con el valor Costos Operativos presupuestados
     *         segregados en nivel de Agencia.
     */
    private double getValorTotalAgencia(String agencia, String ano, String mes) throws Exception{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet         rs = null;
        double valor = 0;
        String query = "SQL_TOTAL_AGENCIA";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, agencia);
            st.setString(2, ano);
            st.setString(3, mes);
            rs = st.executeQuery();
            if(rs.next()){
                valor = rs.getDouble("valor");
            }
            }}catch (Exception ex){
            throw new Exception("Error en getValorTotalAgencia [ReportesDAO] ...\n" + ex.getMessage());
        }finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return valor;
    } 
    
    /**
     * Metodo para obtener los subtipos de tipos de un nivel superior
     * @autor mfontalvo
     * @param tipo, { IMP | EXP }
     * @return subtipos de importacion o exportacion segun el tipo
     * @throws Exception.
     */
    public String obtenerTipos (String tipo) throws Exception {
        Connection con = null;
        PreparedStatement st    = null;
        ResultSet         rs    = null;
        String            tipos = "";
        String query = "SQL_OBTENER_DOCUMENTOS";
        try{
            con = this.conectarJNDI(query);
            if (con != null) {
            st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
            st.setString(1, tipo);
            rs = st.executeQuery();
            while(rs.next()){
                tipos += (tipos.equals("")?"":"," ) + "'" +rs.getString("table_code") + "'";
            }
            }}catch (Exception ex){
            throw new Exception("Error en obtenerTipos [ReportesDAO] ...\n" + ex.getMessage());
        }finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return tipos; 
    }
    
     /**
     * Metodo para extraer los documentos de importacion y exportacion tanto ejecutados
     * como presupuestado
     * @autor mfontalvo
     * @param Cliente filtro de cliente para el reporte
     * @param Ano filtro de a�o para el reporte
     * @param Mes filtro de mes para el reporte
     * @param tipo filtro de tipo para el reporte
     * @return TreeMap de documentos distribuidos por agencia.
     * @throws Exception.
     */
    public TreeMap obtenerDocumentos(String Cliente, String Ano, String Mes, String tipo) throws Exception{
        Connection        con = null;
        PreparedStatement st  = null;
        ResultSet         rs  = null;
        String            qry = "SQL_INFORME_MABE";
        String            sql = "";
        
        TreeMap datos       = new TreeMap();
        TreeMap referencias = new TreeMap();
        TreeMap origenes    = new TreeMap();
        TreeMap destinos    = new TreeMap();
        try{
            String tipos = this.obtenerTipos(tipo);
            if (!tipos.equals("")) {
                
                con = this.conectarJNDI(qry);
                if (con==null)
                    throw new Exception("Sin conexion");

                sql = this.obtenerSQL(qry);
                sql = sql.replaceAll("#TIPO#"   , tipos    )
                         .replaceAll("#CLIENTE#", Cliente  )
                         .replaceAll("#PERIODO#", Ano + Mes)
                         .replaceAll("#PERIODO_INICIAL#" , Ano + "-" + Mes + "-01")
                         .replaceAll("#PERIODO_FINAL#"   , Ano + "-" + Mes + "-" + com.tsp.util.UtilFinanzas.diaFinal(Ano, Mes));
                st = con.prepareStatement(sql);
                rs = st.executeQuery();

                while (rs.next()){
                    TreeMap dt = (TreeMap) referencias.get( rs.getString("documento") );
                    if (dt == null)
                        dt = new TreeMap();
                    dt.put("documento" , rs.getString("documento"));
                    dt.put(rs.getString("origen") +"-"+ rs.getString("destino")+"-PTO"       , rs.getString("pto"));
                    dt.put(rs.getString("origen") +"-"+ rs.getString("destino")+"-EJE"       , rs.getString("eje"));
                    dt.put(rs.getString("origen") +"-"+ rs.getString("destino")+"-SAL"       , rs.getString("sal"));
                    referencias.put(rs.getString("documento") , dt);
                    origenes.put(rs.getString("origen"),rs.getString("origen"));
                    destinos.put(rs.getString("destino"),rs.getString("destino"));
                }
            }
            datos.put("origenes", origenes);
            datos.put("destinos", destinos);
            datos.put("referencias", referencias);
            
        } catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return datos;
    }
    
    
}

