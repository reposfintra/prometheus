/*************************************************************
 * Nombre      ............... DistribucionPresupuestoDAO.java
 * Descripcion ............... Manipulcaion de datos de Presupuesto
 * Autor       ............... fvillacob
 * Fecha       ............... Abril - 14 - 2005
 * Version     ............... 1.0
 * Copyright   ............... Fintravalores S.A. S.A
 ************************************************************/


package com.tsp.finanzas.presupuesto.model.daos;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.util.Date;
import com.tsp.util.*;
import com.tsp.util.connectionpool.*;
import com.tsp.finanzas.presupuesto.model.beans.*;
import com.tsp.operation.model.DAOS.MainDAO;

public class TasaDAO extends MainDAO {
    
    public TasaDAO() {
        super("PtoTasaDAO.xml");
    }
    public TasaDAO(String dataBaseName) {
        super("PtoTasaDAO.xml", dataBaseName);
    }
    
    
    /**
     * Procedimiento que crea la tabla de tasas
     * nota: este procedimeinto ya no es invocado desde ningun lado
     * @autor  mfontalvo
     * @throws Exception.
     **/
    public void create() throws Exception{
        Connection con = null;
        PreparedStatement st = null;
        String query = "SQL_CREATE";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.execute();
            }
        } catch (Exception e) {
            throw new SQLException(e.getMessage());
        }finally{
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return;
    }
    
    
    
    /**
     * Funcion que busca las tasas proyectadas dado un periodo
     * @autor  mfontalvo
     * @params ano ... a�o a consultar
     * @params mes ... mes a consultar
     * @throws Exception.
     **/
    public List searchTasa(String ano, String mes) throws Exception{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        List lista = null;
        int cont = 1;
        String query = "SQL_SELECT";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, ano);
                // st.setString(2, mes);
                rs = st.executeQuery();
                if (rs != null) {
                    lista = new LinkedList();
                    while (rs.next()) {
                        Tasa tasa = new Tasa();
                        tasa.setId(cont);
                        tasa.setAno(rs.getString(1));
                        tasa.setMes(rs.getString(2));
                        tasa.setDolar(rs.getDouble(3));
                        tasa.setBolivar(rs.getDouble(4));
                        tasa.setDTF(rs.getDouble(5));
                        cont++;
                        lista.add(tasa);
                        tasa = null;//Liberar Espacio JJCastro
                    }
                }
            }
        } catch (Exception e) {
            throw new SQLException(" DAO Error en la busqueda de tasa : " + e.getMessage());
        }finally{
            if (rs != null) {try {rs.close();} catch (SQLException e) {throw new SQLException("ERROR CERRANDO EL RESULSET  " + e.getMessage());}}
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return lista;
    }
    
    
    
    /**
     * procedimineto para ingresar a la base de datos
     * @autor  mfontalvo
     * @params ano ....... a�o a insertar
     * @params mes ....... mes a insertar
     * @params dolar ..... dolar a insertar
     * @params bolivar ... bolivar a insertar
     * @params dtf ....... dtf a insertar
     * @params usuario ... usuario a insertar
     * @throws Exception.
     **/
    public void insert( String ano, String mes, double dolar, double bolivar, double dtf, String usuario) throws Exception{
        Connection con = null;
        PreparedStatement st = null;
        String query = "SQL_INSERT";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, ano);
                st.setString(2, mes);
                st.setDouble(3, dolar);
                st.setDouble(4, bolivar);
                st.setDouble(5, dtf);
                st.setString(6, usuario);
                st.execute();
            }
        } catch (Exception e) {
            throw new SQLException("No se pudo Insertar la tasa: " + e.getMessage());
        }finally{
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return;
    }
    
    
    /**
     * procedimineto para modificar a la base de datos
     * @autor  mfontalvo
     * @params ano ....... a�o a modificar
     * @params mes ....... mes a modificar
     * @params dolar ..... dolar a modificar
     * @params bolivar ... bolivar a modificar
     * @params dtf ....... dtf a modificar
     * @params usuario ... usuario a modificar
     * @throws Exception.
     **/
    public void update( String ano, String mes, double dolar, double bolivar, double dtf, String usuario) throws Exception{
        Connection con = null;
        PreparedStatement st = null;
        String query = "SQL_UPDATE";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setDouble(1, dolar);
                st.setDouble(2, bolivar);
                st.setDouble(3, dtf);
                st.setString(4, usuario);
                st.setString(5, ano);
                st.setString(6, mes);
                st.execute();
            }
        } catch (Exception e) {
            throw new SQLException("No se pudo Modificar la tasa: " + e.getMessage());
        }finally{
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return;
    }
    
    
    /**
     * procedimineto para eliminar en la base de datos
     * @autor  mfontalvo
     * @params ano ....... a�o a eliminar
     * @params mes ....... mes a eliminar
     * @throws Exception.
     **/
    public void delete( String ano, String mes) throws Exception{
        Connection con = null;
        PreparedStatement st = null;
        String query = "SQL_DELETE";
        try {
            con = this.conectarJNDI(query);
            if (con != null) {
                st = con.prepareStatement(this.obtenerSQL(query));//JJCastro fase2
                st.setString(1, ano);
                st.setString(2, mes);
                st.execute();
            }
        } catch (Exception e) {
            throw new SQLException("No se pudo Eliminar la tasa: " + e.getMessage());
        }finally{
            if (st != null) {try {st = null;} catch (Exception e) {throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());}}
            if (con != null){try {this.desconectar(con);} catch (SQLException e) {throw new SQLException("ERROR CERRANDO LA CONEXION  " + e.getMessage());}}
        }
        return;
    }
    
    
}
