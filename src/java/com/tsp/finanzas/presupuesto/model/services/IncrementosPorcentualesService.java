/*************************************************************
 * Nombre      ............... IncrementosPorcentualesService.java
 * Descripcion ............... Clase de manipulacion de codigos
 *                             de reprogramacion
 * Autor       ............... mfontalvo
 * Fecha       ............... Mayo - 06 - 2005
 * Version     ............... 1.0
 * Copyright   ............... Fintravalores S.A. S.A
 ************************************************************/


package com.tsp.finanzas.presupuesto.model.services;

import com.tsp.finanzas.presupuesto.model.daos.IncrementosPorcentualesDAO;
import com.tsp.finanzas.presupuesto.model.Model;
import com.tsp.finanzas.presupuesto.model.beans.*;
import java.util.*;


public class IncrementosPorcentualesService {
    private IncrementosPorcentualesDAO IPDataAccess;
    private List          Listado;
    private DatosGeneral  Dato;
    ///////////////////////////////////////
    private String varJSInc = "";
    public  final String SEPARADOR = "~";
    ///////////////////////////////////////
    
    public static final int SEARCH_ALL   = IncrementosPorcentualesDAO.SEARCH_ALL;
    public static final int SEARCH_ONE   = IncrementosPorcentualesDAO.SEARCH_ONE;
    public static final int SEARCH_ANUL  = IncrementosPorcentualesDAO.SEARCH_ANUL;
    public static final int SEARCH_NANUL = IncrementosPorcentualesDAO.SEARCH_NANUL;
    
    private int SEARCH;
    private String ANO;
    
    /** Creates a new instance of IncrementosPorcentualesService */
    public IncrementosPorcentualesService() {
        IPDataAccess = new IncrementosPorcentualesDAO();
        SEARCH = SEARCH_NANUL;
        ANO    = com.tsp.util.Util.getFechaActual_String(1);
    }
    public IncrementosPorcentualesService(String dataBaseName) {
        IPDataAccess = new IncrementosPorcentualesDAO(dataBaseName);
        SEARCH = SEARCH_NANUL;
        ANO    = com.tsp.util.Util.getFechaActual_String(1);
    }
    
    
    
    /**
     * Procedimiento que inicializa los paramatros para poder
     * manipular la vista de incrementos porcentuales
     * @autor  mfontalvo
     * @see ReiniciarDato(), ReiniciarParametros(), SEARCH()
     */
    public void Init () throws Exception {        
        try{
           //CREATE_TABLE();
           ReiniciarDato();
           ReiniciarParametros();
           SEARCH();
        }
        catch(Exception e){
            throw new Exception("Error en Init [IncrementosPorcentualesService]...\n"+e.getMessage());        
        }        
    }
    
    /**
     * Procedimiento que inicializa los paramatros para poder
     * manipular la vista de incrementos porcentuales
     * @autor  mfontalvo
     * @see GenerarVarJSInc
     */    
    public void Init_AplicarIC () throws Exception {        
        try{
            GenerarVarJSInc();
        }
        catch(Exception e){
            throw new Exception("Error en Init_AplicarIC [IncrementosPorcentualesService]...\n"+e.getMessage());        
        }        
    }    
    
    
    /**
     * Procedimiento para crear la tabla de Incrementos Porcentuales
     * nota: ya no se esta utilizando
     * @autor  mfontalvo
     */
    public void CREATE_TABLE () throws Exception {        
        try{
            //String [] Parametros = {};
            //IPDataAccess.EXECUTE_UPDATE(IPDataAccess.CREATE, Parametros);
        }
        catch(Exception e){
            throw new Exception("Error en CREATE_TABLE [IncrementosPorcentualesService]...\n"+e.getMessage());        
        }        
    }    
    
    
    /**
     * Procedimiento para ingresar un nuevo incremento porcentual a la base de
     * datos segun los siguientes parametros
     * @autor  mfontalvo
     * @params DSTRCT_CODE ........ Distrito a insertar
     * @params ANO ................ A�o a insertar
     * @params CODIGO_UNIDAD ...... Codigo Unidad a insertar
     * @params DESCRIPCION_UNIDAD . Descripcion Unidad a insertar
     * @params PROY_INC_ANUAL ..... Incremento anual a insertar
     * @params PROY_INC_MENSUAL ... Incremento Mensual a insertar
     * @params USUARIO ............ Usuario a insertar
     * @throws Exception.
     * @see EXECUTE_UPDATE de acceso al DAO.
     */
    
    public void INSERT (String DSTRCT_CODE, String ANO, String CODIGO_UNIDAD, String DESCRIPCION_UNIDAD, String PROY_INC_ANUAL, String [] PROY_INC_MENSUAL, String USUARIO) throws Exception {        
        try{
            String [] Parametros = {"", DSTRCT_CODE, ANO, CODIGO_UNIDAD, DESCRIPCION_UNIDAD, PROY_INC_ANUAL, PROY_INC_MENSUAL[0], PROY_INC_MENSUAL[1], PROY_INC_MENSUAL[2], PROY_INC_MENSUAL[3], PROY_INC_MENSUAL[4], PROY_INC_MENSUAL[5], PROY_INC_MENSUAL[6], PROY_INC_MENSUAL[7], PROY_INC_MENSUAL[8], PROY_INC_MENSUAL[9], PROY_INC_MENSUAL[10], PROY_INC_MENSUAL[11], USUARIO, "#FECHA#", USUARIO, "#FECHA#"};
            IPDataAccess.EXECUTE_UPDATE(IPDataAccess.INSERT, Parametros);
        }
        catch(Exception e){
            throw new Exception("Error en INSERT [IncrementosPorcentualesService]...\n"+e.getMessage());        
        }        
    } 
    
    
    /**
     * Procedimiento para actualizar un nuevo incremento porcentual a la base de
     * datos segun los siguientes parametros
     * @autor  mfontalvo
     * @params DSTRCT_CODE ........ Distrito a actualizar
     * @params ANO ................ A�o a actualizar
     * @params CODIGO_UNIDAD ...... Codigo Unidad a actualizar
     * @params DESCRIPCION_UNIDAD . Descripcion Unidad a actualizar
     * @params PROY_INC_ANUAL ..... Incremento anual a actualizar
     * @params PROY_INC_MENSUAL ... Incremento Mensual a actualizar
     * @params USUARIO ............ Usuario a actualizar
     * @throws Exception.
     * @see EXECUTE_UPDATE de acceso al DAO.
     */
    
    
    public void UPDATE (String DSTRCT_CODE, String ANO, String CODIGO_UNIDAD, String DESCRIPCION_UNIDAD, String PROY_INC_ANUAL, String [] PROY_INC_MENSUAL, String USUARIO) throws Exception {        
        try{
            String [] Parametros = {DESCRIPCION_UNIDAD, PROY_INC_ANUAL, PROY_INC_MENSUAL[0], PROY_INC_MENSUAL[1], PROY_INC_MENSUAL[2], PROY_INC_MENSUAL[3], PROY_INC_MENSUAL[4], PROY_INC_MENSUAL[5], PROY_INC_MENSUAL[6], PROY_INC_MENSUAL[7], PROY_INC_MENSUAL[8], PROY_INC_MENSUAL[9], PROY_INC_MENSUAL[10], PROY_INC_MENSUAL[11] , USUARIO, "#FECHA#", DSTRCT_CODE, ANO, CODIGO_UNIDAD};
            IPDataAccess.EXECUTE_UPDATE(IPDataAccess.UPDATE, Parametros);
        }
        catch(Exception e){
            throw new Exception("Error en UPDATE [IncrementosPorcentualesService]...\n"+e.getMessage());        
        }        
    } 
    
    
    /**
     * Procedimiento para elimianr un incremento porcentual a la base de
     * datos segun los siguientes parametros
     * @autor  mfontalvo
     * @params DSTRCT_CODE ........ Distrito a insertar
     * @params ANO ................ A�o a insertar
     * @params CODIGO_UNIDAD ...... Codigo Unidad a insertar
     * @throws Exception.
     * @see EXECUTE_UPDATE de acceso al DAO.
     */  
    
    
    public void DELETE (String DSTRCT_CODE, String ANO, String CODIGO_UNIDAD) throws Exception {        
        try{
            String [] Parametros = {DSTRCT_CODE, ANO, CODIGO_UNIDAD};
            IPDataAccess.EXECUTE_UPDATE(IPDataAccess.DELETE, Parametros);
        }
        catch(Exception e){
            throw new Exception("Error en DELETE [IncrementosPorcentualesService]...\n"+e.getMessage());        
        }        
    }
    
    
    /**
     * Procedimiento para actualizar el estado un incremento porcentual a la base de
     * datos segun los siguientes parametros
     * @autor  mfontalvo
     * @params DSTRCT_CODE ........ Distrito a modificar
     * @params ANO ................ A�o a modificar
     * @params CODIGO_UNIDAD ...... Codigo Unidad a modificar
     * @params VALOR .............. Nuevo Estado a modificar
     * @params USUARIO ............ Usuario a modificar
     * @throws Exception.
     * @see EXECUTE_UPDATE de acceso al DAO.
     */
    
    public void UPDATE_STATE (String DSTRCT_CODE, String ANO, String CODIGO_UNIDAD, String VALOR, String USUARIO) throws Exception {        
        try{
            String [] Parametros = {VALOR, USUARIO, "#FECHA#", DSTRCT_CODE, ANO, CODIGO_UNIDAD};
            IPDataAccess.EXECUTE_UPDATE(IPDataAccess.UPDATE_STATE, Parametros);
        }
        catch(Exception e){
            throw new Exception("Error en UPDATE_STATE [IncrementosPorcentualesService]...\n"+e.getMessage());        
        }        
    } 

    
    /**
     * Procedimiento para buscar un incremento porcentual a la base de
     * datos segun los siguientes parametros
     * @autor  mfontalvo
     * @params DSTRCT_CODE ........ Distrito a consultar
     * @params ANO ................ A�o a consultar
     * @params CODIGO_UNIDAD ...... Codigo Unidad a consultar
     * @throws Exception.
     * @see EXECUTE_QUERY de acceso al DAO.
     */    
    
    public void SEARCH_ONE (String DSTRCT_CODE, String ANO, String CODIGO_UNIDAD) throws Exception {        
        try{
            String [] Parametros = {DSTRCT_CODE, ANO, CODIGO_UNIDAD};
            List tmp = IPDataAccess.EXECUTE_QUERY(SEARCH_ONE, Parametros);
            Dato = (tmp.size()>0)? (DatosGeneral) tmp.get(0): null;
        }
        catch(Exception e){
            throw new Exception("Error en SEARCH_ONE [IncrementosPorcentualesService]...\n"+e.getMessage());        
        }        
    }
    
    
    /**
     * Procedimiento para buscar todos los incremento porcentual a la base de
     * datos segun los siguientes parametros
     * @autor  mfontalvo
     * @params Tipo ............... Tipo de consulta (Anulados, Todos)
     * @params ANO ................ A�o a consultar
     * @throws Exception.
     * @see EXECUTE_QUERY de acceso al DAO.
     */    
    
    
    public void SEARCH (int Tipo, String Ano) throws Exception {        
        try{
            ReiniciarParametros();
            String [] Parametros = { Ano };
            ReiniciarListado();
            Listado = IPDataAccess.EXECUTE_QUERY(Tipo, Parametros);
            SEARCH = Tipo;
            ANO    = Ano;
        }
        catch(Exception e){
            throw new Exception("Error en SEARCH [IncrementosPorcentualesService]...\n"+e.getMessage());        
        }        
    }  
    
    /**
     * Procedimiento para buscar todos los incremento porcentual a la base de
     * datos segun los siguientes parametros previamente almacenados
     * @autor  mfontalvo
     * @throws Exception.
     * @see EXECUTE_QUERY de acceso al DAO.
     */ 
    
    public void SEARCH () throws Exception {        
        try{
            SEARCH (SEARCH, ANO);
        }
        catch(Exception e){}        
    } 
    
    
    /**
     * Procedimiento para generar una variable de JavaScript 
     * de Incrementos porcentuales
     * @autor  mfontalvo
     */     
    
    public void GenerarVarJSInc ()throws Exception {
        try{
            SEARCH(this.SEARCH_NANUL, "%");
            if (Listado != null){
                String varJS = " var datos = [ ";
                Iterator it = this.Listado.iterator();                    
                while(it.hasNext()){
                    DatosGeneral dt = (DatosGeneral) it.next();
                    String item = "\n['"+ dt.getValor("ANO") + SEPARADOR+ dt.getValor("CUNIDAD") + SEPARADOR + dt.getValor("DUNIDAD") + SEPARADOR + dt.getValor("PANUAL")+ SEPARADOR + 
                                          dt.getValor("PMES01") + SEPARADOR + dt.getValor("PMES02") + SEPARADOR + dt.getValor("PMES03") + SEPARADOR + dt.getValor("PMES04") + SEPARADOR + 
                                          dt.getValor("PMES05") + SEPARADOR + dt.getValor("PMES06") + SEPARADOR + dt.getValor("PMES07") + SEPARADOR + dt.getValor("PMES08") + SEPARADOR + 
                                          dt.getValor("PMES09") + SEPARADOR + dt.getValor("PMES10") + SEPARADOR + dt.getValor("PMES11") + SEPARADOR + dt.getValor("PMES12") + SEPARADOR + 
                                          "']"; 
                    varJS += item + ",";
                }
                varJS = varJS.substring(0,varJS.length()-1) + " ]; ";
                this.varJSInc = varJS;
            }
            else
                this.varJSInc = " var datos = []; ";
        }catch (Exception e){
            throw new Exception("Error en GenerarVarJSInc [IncrementosPorcentualesService]...\n"+e.getMessage());     
        }
    }
    
    /**
     * Procedimiento para incrementar el presupuesto segun los
     * siguientes parametros
     * @autor  mfontalvo
     * @params UN ....... Unidad de Negocion
     * @params AnoI ..... A�o a incrementar
     * @params MesI ..... Mes a Incrementar
     * @params AnoB ..... A�o Base
     * @params MesB ..... Mes Base
     * @params Usuario .. Usuario.
     * @params model .... Modelo.
     * @throws Exception
     * @see Incrementar de acceso al DOA
     */
    
    public void IncrementarPresupuesto(String UN, String AnoI, String MesI, String AnoB, String MesB, String Usuario, Model model)throws Exception {
        try{
            IPDataAccess.Incrementar(UN, AnoI , MesI , AnoB , MesB , Usuario , model);    
        }catch (Exception e){
            throw new Exception("Error en IncrementarPresupuesto [IncrementosPorcentualesService]...\n"+e.getMessage());     
        }
    }
    
    
    
    
    /**
     * Procediminento que reinicializa la variable Dato 
     * @autor  mfontalvo
     */
    public void ReiniciarDato(){
        this.Dato = null;
    }
    
    /**
     * Procediminento que reinicializa la variable varJSInc 
     * @autor  mfontalvo
     */
    public void ReiniciarVarJS(){
        this.varJSInc = null;
    }    
    
    /**
     * Procediminento que reinicializa la variable Listado 
     * @autor  mfontalvo
     */
    public void ReiniciarListado(){
        this.Listado = null;
    }
    
    /**
     * Procediminento que reinicializa los parametros de consulta  SEARCH, ANO 
     * @autor  mfontalvo
     */
    public void ReiniciarParametros(){
        SEARCH = SEARCH_NANUL;
        ANO    = com.tsp.util.Util.getFechaActual_String(1);
    }
    
    ///////////////////////////////////////////////////////////////////////////////
    // getter
    
    
    /**
     * Funcion que devuelve la variable Listado 
     * @autor  mfontalvo
     * @return Listado de Incremetnos
     */
    public List getListado(){
        return this.Listado;
    }
    
    /**
     * Funcion que devuelve la variable Dato 
     * @autor  mfontalvo
     * @return Incremento
     */
    public DatosGeneral getDato (){
        return this.Dato;
    }
    
    /**
     * Funcion que devuelve la variable SEARCH 
     * @autor  mfontalvo
     * @return Tipo de listado
     */
    public int getTipoListado(){
        return this.SEARCH;
    }
    
    /**
     * Funcion que devuelve la variable ANO 
     * @autor  mfontalvo
     * @return A�o
     */
    public String getAno(){
        return this.ANO;
    }
    
    /**
     * Funcion que devuelve la variable varJSInc 
     * @autor  mfontalvo
     * @return variable de Incrementos Porcentuales
     */
    public String getVarJSInc(){
        return this.varJSInc;
    }
    
    /**
     * Funcion que devuelve la el caracter separador de Java Script 
     * @autor  mfontalvo
     * @return Separador
     */
    public String getVarJSSeparador(){
        return " var separador = '"+ this.SEPARADOR +"';";
    }
  
}
