/*************************************************************
 * Nombre      ............... ReprogramacionesService.java
 * Descripcion ............... Clase de manipulacion de codigos
 *                             de reprogramacion
 * Autor       ............... mfontalvo
 * Fecha       ............... Noviembre - 17 - 2005
 * Version     ............... 1.0
 * Copyright   ............... Fintravalores S.A. S.A
 ************************************************************/

package com.tsp.finanzas.presupuesto.model.services;

import com.tsp.finanzas.presupuesto.model.daos.ReprogramacionesDAO;
import com.tsp.finanzas.presupuesto.model.beans.Reprogramacion;

import java.util.*;


public class ReprogramacionesService {
    
    List listaReprogramaciones;
    List listaReprogramacionesActivas;
    Reprogramacion dato;
    
    ReprogramacionesDAO RPDataAccess;
    
    
    /** Creates a new instance of ReprogramacionesService */
    public ReprogramacionesService() {
        RPDataAccess = new ReprogramacionesDAO ();
    }
    public ReprogramacionesService(String dataBaseName) {
        RPDataAccess = new ReprogramacionesDAO(dataBaseName);
    }
    
    /**
     * Procedimiento para insertar dentro de las reprogramaciones
     * @param Descripcion . Descripcion de la causa de r.
     * @param Codigo ...... Codigo de causa
     * @param Usuario ..... Usuario en session
     * @throws Exception .
     */    
    public void INSERT(String Codigo, String Descripcion, String Usuario) throws Exception {
        try{
            RPDataAccess.EXECUTE_UPDATE(
                RPDataAccess.INSERT, 
                new String[] {Codigo, Descripcion, Usuario, "#FECHA#", Usuario, "#FECHA#"} 
            );
        }
        catch(Exception e){
            throw new Exception("Error en INSERT [ReprogramacionesService]...\n" + e.getMessage());
        }
    }  
    
    /**
     * Procedimiento para modificar dentro de las reprogramaciones
     * @autor  mfontalvo
     * @param Descripcion . Descripcion de la causa
     * @param Codigo ...... Codigo de causa
     * @param Usuario ..... Usuario en session
     * @throws Exception ..
     */     
    public void UPDATE (String Codigo, String Descripcion, String Usuario) throws Exception {
        try{
            RPDataAccess.EXECUTE_UPDATE(
                RPDataAccess.UPDATE, 
                new String[] { Descripcion, Usuario, "#FECHA#", Codigo } 
            );
        }
        catch(Exception e){
            throw new Exception("Error en UPDATE [ReprogramacionesService]...\n" + e.getMessage());
        }
    }   
    
    /**
     * Procedimiento para modificar el estado dentro de las reprogramaciones
     * @autor  mfontalvo
     * @param Codigo ..... Codigo de causa
     * @param Estado ..... Estado de la causa
     * @param Usuario .... Usuario en session
     * @throws Exception .
     */    
    public void UPDATE_STATE (String Codigo, String Estado, String Usuario) throws Exception {
        try{
            RPDataAccess.EXECUTE_UPDATE(
                RPDataAccess.UPDATE_STATE, 
                new String[] { Estado, Usuario, "#FECHA#", Codigo } 
            );
        }
        catch(Exception e){
            throw new Exception("Error en UPDATE_STATE [ReprogramacionesService]...\n" + e.getMessage());
        }
    }     
    
    /**
     * Procedimiento para eliminar dentro de las reprogramaciones
     * @autor  mfontalvo
     * @param Codigo ..... Codigo de causa
     * @throws Exception .
     */    
    public void DELETE (String Codigo) throws Exception {
        try{
            RPDataAccess.EXECUTE_UPDATE(
                RPDataAccess.DELETE, 
                new String[] { Codigo } 
            );
        }
        catch(Exception e){
            throw new Exception("Error en DELETE [ReprogramacionesService]...\n" + e.getMessage());
        }
    }   
    
    /**
     * Procedimineto para buscar todas la reprogramaciones
     * @autor  mfontalvo
     * @throws Exception .
     */    
    public void SEARCH_ALL () throws Exception {        
        try{
            listaReprogramaciones = 
                RPDataAccess.EXECUTE_QUERY(
                    RPDataAccess.SELECT_LIST, 
                    new String[] { "ALL" } 
                );
        }
        catch(Exception e){
            throw new Exception("Error en SEARCH_ALL [ReprogramacionesService]...\n"+e.getMessage());        
        }        
    }
    
    /**
     * Procedimineto para buscar todas la reprogramaciones activas
     * @autor  mfontalvo
     * @throws Exception .
     */    
    public void SEARCH_ALL_ACTIVAS () throws Exception {        
        try{
            listaReprogramacionesActivas = 
                RPDataAccess.EXECUTE_QUERY(
                    RPDataAccess.SELECT_LIST_ACTIVOS, 
                    new String[] { "ALL" }  
                );
        }
        catch(Exception e){
            throw new Exception("Error en SEARCH_ALL_ACTIVAS [ReprogramacionesService]...\n"+e.getMessage());        
        }        
    }    
    /**
     * Procedimiento para buscar una reprogramacion
     * @autor  mfontalvo
     * @param Codigo .. Codigo de reprogramacion a buscar
     * @throws Exception .
     */    
    public void SEARCH (String Codigo) throws Exception {        
        try{
            List tmp = 
                RPDataAccess.EXECUTE_QUERY(
                    RPDataAccess.SELECT_LIST, 
                    new String[] { Codigo }
                );
            dato = (tmp==null ? null: tmp.size()==0? null: (Reprogramacion) tmp.get(0));
            
        }
        catch(Exception e){
            throw new Exception("Error en SEARCH [ReprogramacionesService]...\n"+e.getMessage());        
        }        
    } 
    
    
    /*****************************************************************************************************/
    
    /**
     * Procedimiento para insertar un viaje reprogramado
     * @autor mfontalvo
     * @param Estandar  .. Codigo del Estandar JOB
     * @param FViaje   ... fecha del viaje
     * @param CodigoR .... codigo de reprogramacion
     * @param Usuario .... usuario en session
     * @throws Exception .
     */    
    public void INSERT_VIAJE_REPROGRAMADO(String Estandar, String FechaViaje, String CodigoRP, String ValorAnterior, String ValorNuevo, String Usuario, String FechaGrabacion) throws Exception {
        try{
            RPDataAccess.EXECUTE_UPDATE(
                RPDataAccess.INSERT_RELACION, 
                new String[] {Estandar, FechaViaje, CodigoRP, ValorAnterior, ValorNuevo, Usuario, FechaGrabacion, Usuario, FechaGrabacion} 
            );
        }
        catch(Exception e){
            throw new Exception("Error en INSERT_VIAJE_REPROGRAMADO [ReprogramacionesService]...\n" + e.getMessage());
        }
    }    
    
    /**
     * Procedimiento para buscar viajes reprogramados
     * @autor  mfontalvo
     * @param Estandar  .. Codigo del Estandar JOB
     * @param Ano   ...... ano de los viajes
     * @param Mes   ...... mes de los viajes
     * @throws Exception .
     */    
    public void SEARCH_VIAJE_REPROGRAMADO(String Estandar, String Ano, String Mes) throws Exception {
        try{
            listaReprogramaciones = RPDataAccess.EXECUTE_QUERY(
                                        RPDataAccess.SEARCH_VIAJE_RP, 
                                        new String[] {Estandar, Ano + "-" + Mes}
                                    );
        }
        catch(Exception e){
            throw new Exception("Error en SEARCH_VIAJE_REPROGRAMADO [ReprogramacionesService]...\n" + e.getMessage());
        }
    }    
    
    
    

    /**
     * Funcion que devuelve el listado actual de codigos de reprogramaciones
     * @autor  mfontalvo
     * @return Listado
     */    
    public List getListaReprogramaciones() {
        return listaReprogramaciones;
    }  
    /**
     * Funcion que devuelve el listado actual de codigos de reprogramaciones
     * @return Listado
     */    
    public List getListaReprogramacionesActivas() {
        return listaReprogramacionesActivas;
    }  

    /**
     * Funcion que devuelve la ultima reprogramacion buscada
     * @autor  mfontalvo
     * @return Reprogramacion
     */    
    public Reprogramacion getDato() {
        return dato;
    }    
    
    
    
    /**
     * Procedimiento para reiniciar el Ultimo dato
     * especifico buscado
     */    
    public void ReiniciarDato(){
        dato = null;
    }
    /**
     * Procedimiento para reiniciar la lista de codigos de reprogramacion
     */    
    public void ReiniciarLista(){
        listaReprogramaciones = null;
    }
    /**
     * Procedimiento para reiniciar la lista de codigos de reprogramacion
     */    
    public void ReiniciarListaActivas(){
        listaReprogramacionesActivas = null;
    }
    
    
}
