/*********************************************************************
 * Nombre      ............... PresupuestoCostosOperativosService.java
 * Descripcion ............... Clase de manipulacion de 
 *                             los nuevos costos operativos
 * Autor       ............... mfontalvo
 * Fecha       ............... Diciembre - 12 - 2005
 * Version     ............... 1.0
 * Copyright   ............... Fintravalores S.A. S.A
 *********************************************************************/

package com.tsp.finanzas.presupuesto.model.services;



import com.tsp.finanzas.presupuesto.model.daos.CostosOperativosDAO;
import com.tsp.finanzas.presupuesto.model.beans.*;
import java.util.*;

public class CostosOperativosService {
    
    
    // declaracion de variables
    CostosOperativosDAO PCODataAccess;
    TreeMap             listado;
    DatosGeneral        datos;
    List                listaCO;
    List                listaCOAsociadas;
    List                listaAgCl;
    
    

    // Parametros de las vistas
    List vistaAg; 
    List vistaCt; 
    List vistaCl; 
    TreeMap ParamsVista;
    
    // Parametros de los filtros
    String Ano;
    String Mes;
    String Agencia = "";
    String Cliente = "";
    
    
    
    public CostosOperativosService() {
        PCODataAccess = new CostosOperativosDAO();
        ParamsVista = new TreeMap();
    }
    public CostosOperativosService(String dataBaseName) {
        PCODataAccess = new CostosOperativosDAO(dataBaseName);
        ParamsVista = new TreeMap();
    }
    
    
    /**
     * Procedimiento inicialiar los parametros del formulario inicial
     * de busqueda por periodos, agencias y clientes.
     * @autor  mfontalvo
     * @throws Exception .
     */     
    public void Init () throws Exception {
        try{
            ReiniciarAll();
            listaAgCl = PCODataAccess.BuscarAgenciasClientes();            
        }catch (Exception ex){
            throw new Exception("Error en Init [CostosOperativosService]...\n" + ex.getMessage());
        }
    }
    
    
    
    /**
     * Procedimiento para buscar las ventas de un periodo
     * @autor  mfontalvo
     * @param Ano ...... Ano a consultar
     * @param Mes ...... Mes a consultar
     * @param Agencia .. Agencia a consultar
     * @param Cliente ...Cliente a consultar
     * @throws Exception .
     */       
    public void BuscarVentas (String Ano, String Mes, String Agencia, String Cliente) throws Exception{
        try{
            listado = PCODataAccess.BuscarVentas(Ano, Mes, Agencia, Cliente);
        }catch (Exception ex){
            throw new Exception("Error en BuscarVentas [CostosOperativosService]...\n" + ex.getMessage());
        }
    }    
    
    
    /**
     * Procedimiento para obtener la vista de agencias y sus costos Operativos
     * acumulados por periodo
     * @autor  mfontalvo
     * @throws Exception .
     */       
    public void GenerarVistaAgencias (String Ano, String Mes, int Prolongacion) throws Exception {
        try{
            vistaAg = PCODataAccess.GeneracionVista("AG", Ano + Mes, Prolongacion, null);
            initParamsView("AG", Ano, Mes, Prolongacion, "", "", "", "");
        }catch (Exception ex){
            throw new Exception("Error en GenerarVistaAgencias [CostosOperativosService]...\n" + ex.getMessage());
        }
    }
    
    
    
    /**
     * Procedimiento para obtener la vista de costos operativos 
     * acumulados por periodo
     * @autor  mfontalvo
     * @throws Exception .
     */       
    public void GenerarVistaCostos (String Ano, String Mes, int Prolongacion, String fltAgencia, String NAgencia) throws Exception{
        try{
            vistaCt = PCODataAccess.GeneracionVista("CT", Ano + Mes, Prolongacion, new String [] { fltAgencia } );
            initParamsView("CT", Ano, Mes, Prolongacion, fltAgencia, NAgencia, "", "");
        }catch (Exception ex){
            throw new Exception("Error en GenerarVistaCostos [CostosOperativosService]...\n" + ex.getMessage());
        }
    }    
        
    
    /**
     * Procedimiento para obtener la vista de clientes que incurren en un COperativo
     * acumulados por periodo
     * @autor  mfontalvo
     * @throws Exception .
     */       
    public void GenerarVistaClientes (String Ano, String Mes, int Prolongacion, String fltAgencia, String NAgencia, String fltCosto, String NCosto) throws Exception{
        try{
            vistaCl = PCODataAccess.GeneracionVista("CL", Ano + Mes, Prolongacion, new String [] { fltAgencia , fltCosto} );
            initParamsView("CL", Ano, Mes, Prolongacion, fltAgencia, NAgencia, fltCosto, NCosto );
        }catch (Exception ex){
            throw new Exception("Error en GenerarVistaClientes [CostosOperativosService]...\n" + ex.getMessage());
        }
    }    
    
    
    
    /**
     * Procedimiento para buscar los elementos del gasto relacionados
     * a los costos operativos.
     * @autor  mfontalvo
     * @throws Exception .
     */       
    public void BuscarElementosCostosOperativos () throws Exception{
        try{
            listaCO = PCODataAccess.ElementosCostosOperativos();
        }catch (Exception ex){
            throw new Exception("Error en BuscarElementosCostosOperativos [CostosOperativosService]...\n" + ex.getMessage());
        }
    }
    
    
    
    /**
     * Procedimiento para buscar los elementos del gasto relacionados
     * a los costos operativos dado un estandar y un periodo.
     * @autor  mfontalvo
     * @throws Exception .
     */       
    public void BuscarElementosCostosOperativos (String Estandar, String Ano, String Mes) throws Exception{
        try{
            listaCOAsociadas = PCODataAccess.BuscarElementosCostosOperativos(Estandar, Ano, Mes);
        }catch (Exception ex){
            throw new Exception("Error en BuscarElementosCostosOperativos [CostosOperativosService]...\n" + ex.getMessage());
        }
    }    
        
    
    /**
     * Procedimiento para asociar costos operativos a un estandar dado un periodo
     * @throws Exception .
     * @autor  mfontalvo
     * @param Estnadar.. Estandar al que se le asociaran los costos
     * @param Ano ...... Ano para los nuevos costos
     * @param Mes ...... Mes para los nuevos costos
     * @param Costos[].. Todos los costos relacionados al viaje
     * @param Valores[]. Todos los Valores relacionados a los costos
     * @param Usuario .. usuario que regitra el cambio
     * @throws Exception .
     */    
    public void InsertCostosOperativos (String Estandar, String Ano, String Mes, String []Costos, String []Valores, String Usuario) throws Exception{
        try{
            PCODataAccess.AdicionarCostos(Estandar, Ano, Mes, Costos, Valores, Usuario);
        }catch (Exception ex){
            throw new Exception("Error en InsertCostosOperativos [CostosOperativosService]...\n" + ex.getMessage());
        }
    }
    
    
    
    
    /**
     * Procedimiento para obtener la cantidad de ocuerrencias que tiene
     * un valor sobre una sublista
     * @autor  mfontalvo
     * @param key ...... llave para iniciar la busqueda
     * @param valor .... valor u ocurrencia
     * @param fromIndex. inicio para crear la sublista
     * @param tolIndex.. fin de la sublista  
     * @throws Exception .
     */
    
    public int getRepetidas (String key, String valor, int fromIndex, int tolIndex) throws Exception {
        int num = 0;
        List lista = getListado().subList(fromIndex, tolIndex );
        if ( !lista.isEmpty() ){
            for (int i = 0 ; i < lista.size() ; i++ ){
                DatosGeneral dt = (DatosGeneral) lista.get(i);
                if ( dt.getValor(key)!=null && dt.getValor(key).equals(valor)  )
                    num++;
            }
        }
        return num;
    }
    
    ////////////////////////////////////////////////////////////////////////////
    
    /**
     * Funcion que obtine el listado de costos operativos general
     * @autor  mfontalvo
     * @return Listado
     */
    public List getListaElementosCostosOperativos(){
        return listaCO;
    }    

    /**
     * Funcion que obtine el listado de costos operativos asociados a un estandar
     * @autor  mfontalvo
     * @return Listado
     */    
    public List getListaElementosCostosOperativosAsociadas(){
        return listaCOAsociadas;
    }    
    
    /**
     * Funcion que obtine el listado de agencias y clientes relacionados
     * @autor  mfontalvo
     * @return Listado
     */
    public List getListaAgCl() {
        return listaAgCl;
    }   

    /**
     * Funcion que obtine el listado de viajes prupuestados y sus costos operativos 
     * asociados
     * @autor  mfontalvo
     * @return Listado
     */
    public List getListado(){
        return (List) listado.get("viajes");
    }
    
    /**
     * Funcion que obtine el listado de tasas proyectadas
     * @autor  mfontalvo
     * @return Listado
     */    
    public TreeMap getTasa(){
        return (TreeMap) listado.get("tasas");
    }      
    
    /**
     * Procedimiento que modifica el objeto donde se mantiene el costo operativo
     * selecionado
     * @autor  mfontalvo
     * @params datos ...... objeto de costos operativos
     */    
    
    public void setDatos (DatosGeneral datos){
        this.datos = datos;
    }
    
    
    /**
     * Funcion que devuelve el objeto de Costos operaivos
     * @autor  mfontalvo
     * @params Costo Operativo seleccionado
     */
    public DatosGeneral getDatos (){
        return datos;
    }  
    
    /**************************************************************
     * Funcion de manipulacion de parametros de las vistas
     **************************************************************/
    
    /**
     * Funcion que devuelve el listado de la vista por agencia
     * @autor  mfontalvo
     * @return Listado por agencias
     */
    public List getVistaAg() {
        return vistaAg;
    }    
    
    /**
     * Funcion que devuelve el listado de la vista por costos
     * @autor  mfontalvo
     * @return Listado por Costos Operativos
     */    
    public List getVistaCt() {
        return vistaCt;
    }    
    
    /**
     * Funcion que devuelve el listado de la vista por clientes
     * @autor  mfontalvo
     * @return Listado por Clientes
     */    
    public List getVistaCl() {
        return vistaCl;
    }    
    
    /**
     * Procedimiento para modificar los parametro de las vista
     * @autor  mfontalvo
     * @params TipoVista .... tipo de vista
     * @params Ano .......... A�o
     * @params mes .......... Mes
     * @params Prolongacion . Proyeccion de la vista
     * @params param0 ....... primer  filtro
     * @params param1 ....... segundo filtro
     * @params param2 ....... tercer  filtro
     * @params param3 ....... cuarto  filtro
     */    
    public void initParamsView (String TipoVista, String Ano, String  Mes, int Prolongacion, String param0, String param1, String param2, String param3){
        ParamsVista.put(TipoVista + "-InitVista"   , new Boolean (true));
        ParamsVista.put(TipoVista + "-Periodo"     , Ano+Mes  );
        ParamsVista.put(TipoVista + "-Ano"         , Ano      );
        ParamsVista.put(TipoVista + "-Mes"         , Mes      );
        ParamsVista.put(TipoVista + "-Prolongacion", new Integer(Prolongacion) );
        ParamsVista.put(TipoVista + "-Param0"      , param0   );
        ParamsVista.put(TipoVista + "-Param1"      , param1   );
        ParamsVista.put(TipoVista + "-Param2"      , param2   );
        ParamsVista.put(TipoVista + "-Param3"      , param3   );
    }
    
    
    /**
     * Funcion que devuelve un filtro 
     * @autor  mfontalvo
     * @params param ....... llave para obtener el valor de un filtro
     * @return Objeto relacionado a la llave.
     */
    public Object getParamView (String param){
        return ParamsVista.get(param); 
    }
    
    
    
    
    /***********************************************************
     * Parametros de los filtros
     ***********************************************************/
    
    /**
     * Procedimiento para obtener el a�o
     * @autor  mfontalvo
     * @return A�o
     */
    public String getAno() {
        return Ano;
    }    
    
    /**
     * Procedimiento para obtener el mes
     * @autor  mfontalvo
     * @return Mes
     */    
    public String getMes() {
        return Mes;
    }    
    
    /**
     * Procedimiento para obtener el Cliente
     * @autor  mfontalvo
     * @return Cliente
     */    
    public String getCliente() {
        return Cliente;
    }
    
    /**
     * Procedimiento para obtener la Agencia
     * @autor  mfontalvo
     * @return Agencia
     */    
    public String getAgencia() {
        return Agencia;
    }    
    
    
    /**
     * Procedimiento para modificar el a�o
     * @autor  mfontalvo
     * @params Ano ...  A�o a modificar
     */     
    public void setAno(String Ano) {
        this.Ano = Ano;
    }  
    
    /**
     * Procedimiento para modificar el mes
     * @autor  mfontalvo
     * @params MES ...  mes a modificar
     */    
    public void setMes(String Mes) {
        this.Mes = Mes;
    }   
    
    /**
     * Procedimiento para modificar el a�o
     * @autor  mfontalvo
     * @params Ano ...  A�o a modificar
     */    
    public void setCliente(String Cliente) {
        this.Cliente = Cliente;
    }   
    
    /**  
     * Procedimiento para modificar el Agencia
     * @autor  mfontalvo
     * @params Agencia ...  Agencia a modificar
     */    
    public void setAgencia(String Agencia) {
        this.Agencia = Agencia;
    }
    
    
    /***********************************************************/
    /**
     * Procedimiemto para reniciar los valores de la vista
     * @autor  mfontalvo
     */
    public void ReiniciarAll (){
        Ano = "";
        Mes = "";
        listado = null;
        datos   = null;
    }

    

    
}
