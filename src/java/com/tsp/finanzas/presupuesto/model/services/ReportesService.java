/*************************************************************
 * Nombre      ............... ReportesService.java
 * Descripcion ............... Opciones de reportes de presupuesto
 * Autor       ............... mfontalvo
 * Fecha       ............... Julio - 31 - 2005
 * Version     ............... 1.0
 * Copyright   ............... Fintravalores S.A. S.A
 ************************************************************/


package com.tsp.finanzas.presupuesto.model.services;


import java.util.*;
import com.tsp.finanzas.presupuesto.model.daos.ReportesDAO;
import com.tsp.finanzas.presupuesto.model.beans.*;


public class ReportesService {
    
    
    // declaracion de varibles 
    private ReportesDAO RptDataAccess;
    private List        Viajes;
    private TreeMap     tViajes;
    private String Ano;
    private String Mes;
    private int diaInicial;
    private int diaFinal;
    
    private TreeMap xlsViajes;
    
    /** Creates a new instance of ReportesService */
    public ReportesService() {
        RptDataAccess = new ReportesDAO();
    }
    public ReportesService(String dataBaseName) {
        RptDataAccess = new ReportesDAO(dataBaseName);
    }
    
    
    /**
     * Procedimiento que extrae los viajes ejecutados y pesupuestados
     * dados los siguientes parametros
     * @autor  mfontalvo
     * @params Distrito ........ Distrito a consultar
     * @params Agencia ......... Agencia a consultar
     * @params Cliente ......... Cliente a consultar
     * @params Ano ............. A�o a consultar
     * @params Mes ............. Mes a consultar
     * @throws Exception.
     * @return Lista con los viajes presupuestados y ejecutados
     *         segregados en niveles de agencia, cliente y estandar.
     * @see GenerarCalculos
     */    
    
    
    public void Presupuestado_VS_Ejecutado(String Distrito, String AgenciaDespacho, String Agencia, String Cliente, String Estandar, String Ano, String Mes, int diaInicial, int diaFinal) throws Exception{
        try{
            tViajes = RptDataAccess.loadViajesPt(Distrito, AgenciaDespacho, Agencia, Cliente, Estandar, Ano, Mes, diaInicial, diaFinal);
            tViajes = RptDataAccess.loadViajesEj(Distrito, AgenciaDespacho, Agencia, Cliente, Estandar, Ano, Mes, diaInicial, diaFinal, tViajes);
            this.Ano        = Ano;
            this.Mes        = Mes;
            this.diaFinal   = diaFinal;
            this.diaInicial = diaInicial;
            GenerarCalculos();
        }catch (Exception ex){
            throw new Exception("Error en la rutina Presupuestado_VS_Ejecutado en [ReportesService] ....\n"+ ex.getMessage());
        }
    }
    
    /**
     * Procedimiento para Generar los totales por agencia y clientes
     * @autor  mfontalvo
     * @params.
     * @return.
     */
    
    public void GenerarCalculos(){
        
        if (tViajes!=null){
            Iterator ita = tViajes.keySet().iterator();
            while(ita.hasNext()){
                String cag = (String) ita.next();
                ViajesAgencia ag = (ViajesAgencia) tViajes.get(cag);
                
                Iterator itc = ag.getListadoClientes().keySet().iterator();
                while(itc.hasNext()){
                    String ccl = (String) itc.next();
                    ViajesCliente cl = (ViajesCliente) ag.getListadoClientes().get(ccl);
                    
                    Iterator its = cl.getListaStandar().keySet().iterator();
                    while(its.hasNext()){
                        String cst = (String) its.next();
                        ViajesStandar st = (ViajesStandar) cl.getListaStandar().get(cst);
                        st.setAno(Ano);
                        st.setMes(Mes);
                        st.Calcular();
                    }
                    cl.setAno(Ano);
                    cl.setMes(Mes);
                    cl.Calcular();
                }
                ag.setAno(Ano);
                ag.setMes(Mes);
                ag.Calcular();
            }
        }
    }
    
    /**
     * Procedimiento para extraer todas las ventas actuales
     * dados los siguietes parametros
     * @autor  mfontalvo
     * @params Ano .... A�o a consultar
     * @params Mes .... Mes a consultar
     * @throws Exception.
     */
    
    public void VentasPresupuestadas(String Ano, String Mes)throws Exception{
        try{
            xlsViajes = RptDataAccess.BuscarVentas(Ano, Mes);
        }catch (Exception ex ){
            throw new Exception("Error en la rutina VentasPresupuestadas en [ReportesService] ....\n"+ ex.getMessage());
        }
    }
        
    /**
     * Procedimiento para extraer todas las ventas actuales y el historial de cada
     * una de ellas con su descripciones de reprogramacion
     * dados los siguietes parametros
     * @autor  mfontalvo
     * @params Agencia ..... A�o a Agencia
     * @params Cliente ..... A�o a Cliente
     * @params Estandar .... A�o a Estandar
     * @params Ano ......... A�o a consultar
     * @params Mes ......... Mes a consultar
     * @throws Exception.
     */
    
    public void HistorialMensual(String Agencia, String Cliente, String Estandar, String Ano, String Mes)throws Exception{
        try{
            Viajes = RptDataAccess.HistorialMensual(Agencia, Cliente, Estandar, Ano, Mes);
        }catch (Exception ex ){
            throw new Exception("Error en la rutina HistoralMensual en [ReportesService] ....\n"+ ex.getMessage());
        }
    } 
    
    /**
     * Funcion que devuelve un elemento List
     * que contiene los viajes presupuestado
     * @return Listado de viajes presupuestados
     * @autor  mfontalvo
     * @return Listado de Viajes
     */
    public List getListado(){
        return Viajes;
    }
    
    /**
     * Funcion que devuelve un elemento TreeMap
     * que contiene los viajes presupuestado
     * @return Listado de viajes presupuestados
     * @autor  mfontalvo
     * @return Listado de Viajes
     */
    public TreeMap getListadoTreeMap(){
        return tViajes;
    }    
    
    /**
     * Funcion que devuelve un elemento TreeMap
     * que contiene los viajes presupuestado
     * @return Listado de viajes presupuestados
     * @autor  mfontalvo
     * @return Listado de Viajes
     */
    public List getListadoList(){
        return new LinkedList (tViajes.values());
    }      
    
    /**
     * Funcion que devuelve un elemento TreeMap
     * @autor  mfontalvo
     * @return Listado a exportar
     */
    public TreeMap getListadoXLS(){
        return xlsViajes;
    }    
    
    /**
     * Funcion que devuelve el a�o en proceso 
     * @autor  mfontalvo
     * @return A�o
     */
    public String getAno(){
        return Ano;
    }
    
    /**
     * Funcion que devuelve el mes en proceso 
     * @autor  mfontalvo
     * @return Mes
     */
    public String getMes(){
        return Mes;
    }
    
//*************************************JEscandon 27-01-06

     /**
     * Procedimiento que extrae los costos operativos presupuestados
     * dados los siguientes parametros
     * @autor  Ing. Juan Manuel Escandon Perez
     * @params Distrito ........ Distrito a consultar
     * @params Agencia ......... Agencia a consultar
     * @params Cliente ......... Cliente a consultar
     * @params Ano ............. A�o a consultar
     * @params Mes ............. Mes a consultar
     * @throws Exception.
     * @return Lista con los Costos operativos presupuestados 
     *         segregados en niveles de agencia, cliente y estandar.
     * @see GenerarCalculos
     */
    
    public void PresupuestoCostosOperativos(String Distrito, String Agencia, String Cliente, String Estandar, String Ano, String Mes) throws Exception{
        try{
            Viajes = RptDataAccess.loadCostosPt(Distrito, Agencia, Cliente, Estandar, Ano, Mes);
            this.Ano = Ano;
            this.Mes = Mes;
            GenerarCalculos();
        }catch (Exception ex){
            throw new Exception("Error en la rutina PresupuestoCostosOperativos en [ReportesService] ....\n"+ ex.getMessage());
        }
    }
    
    
    /**
     * Procedimiento que extrae los Costos operativos ejecutados y pesupuestados
     * dados los siguientes parametros
     * @autor  Ing. Juan Manuel Escandon Perez
     * @params Distrito ........ Distrito a consultar
     * @params Agencia ......... Agencia a consultar
     * @params Cliente ......... Cliente a consultar
     * @params Ano ............. A�o a consultar
     * @params Mes ............. Mes a consultar
     * @throws Exception.
     * @return Lista con los Costos operativos presupuestados y ejecutados 
     *         segregados en niveles de agencia, cliente y estandar.
     * @see GenerarCalculos
     */
    
    public void CostosOperativosEjecVsPdo(String Distrito, String Agencia, String Cliente, String Estandar, String Ano, String Mes) throws Exception{
        try{
            /*tViajes = RptDataAccess.loadCostosPt(Distrito, Agencia, Cliente, Estandar, Ano, Mes);
            tViajes = RptDataAccess.loadViajesEj(Distrito, Agencia, Cliente, Estandar, Ano, Mes, tViajes);
            */this.Ano = Ano;
            this.Mes = Mes;
            GenerarCalculos();
        }catch (Exception ex){
            throw new Exception("Error en la rutina PresupuestoCostosOperativos en [ReportesService] ....\n"+ ex.getMessage());
        }
    }    
    
    /**
     * Getter for property diaInicial.
     * @return Value of property diaInicial.
     */
    public int getDiaInicial() {
        return diaInicial;
    }
    
    /**
     * Setter for property diaInicial.
     * @param diaInicial New value of property diaInicial.
     */
    public void setDiaInicial(int diaInicial) {
        this.diaInicial = diaInicial;
    }
    
    /**
     * Getter for property diaFinal.
     * @return Value of property diaFinal.
     */
    public int getDiaFinal() {
        return diaFinal;
    }
    
    /**
     * Setter for property diaFinal.
     * @param diaFinal New value of property diaFinal.
     */
    public void setDiaFinal(int diaFinal) {
        this.diaFinal = diaFinal;
    }
    
    
    private TreeMap xlsReferencias;	


    /**
     * Metodo para extraer los documentos de importacion y exportacion tanto ejecutados
     * como presupuestado
     * @autor mfontalvo
     * @param Cliente filtro de cliente para el reporte
     * @param Ano filtro de a�o para el reporte
     * @param Mes filtro de mes para el reporte
     * @param tipo filtro de tipo para el reporte
     * @throws Exception.
     */    
    public void obtenerDocumentos (String Cliente, String Ano, String Mes, String tipo) throws Exception{
        try{
            xlsReferencias = RptDataAccess.obtenerDocumentos(Cliente, Ano, Mes, tipo);
        } catch (Exception ex){
            ex.printStackTrace();
            throw new Exception (ex.getMessage());
        }
    }


    /**
     * Getter for property xlsReferencias.
     * @return Value of property xlsReferencias.
     */
    public TreeMap getXLSReferencias() {
        return xlsReferencias;
    }    
    
    
}
