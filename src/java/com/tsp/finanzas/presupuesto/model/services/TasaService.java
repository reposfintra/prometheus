

/*************************************************************
 * Nombre      ............... TasaService.java
 * Descripcion ............... Manejo de tasa
 * Autor       ............... fvillacob
 * Fecha       ............... Abril - 14 - 2005
 * Version     ............... 1.0
 * Copyright   ............... Fintravalores S.A. S.A
 ************************************************************/



package com.tsp.finanzas.presupuesto.model.services;




import com.tsp.finanzas.presupuesto.model.beans.*;
import com.tsp.finanzas.presupuesto.model.daos.*;
import java.util.*;




public class TasaService {
    
    TasaDAO TasaDataAccess;
    List    lista;
    String  ano;
    
    
    public TasaService() {
        TasaDataAccess = new TasaDAO();
        lista          = null;
        ano            = "";
    }
    public TasaService(String dataBaseName) {
        TasaDataAccess = new TasaDAO(dataBaseName);
        lista          = null;
        ano            = "";
    }
    
    
    
    /* Procedimiento que crear la tabla*/
    public void create(){
        try{ //TasaDataAccess.create(); 
        }
        catch(Exception e){}
    }

    
    
    
    /* Procedimiento que devuelve una lista de tasa para ano y mes
     * parametros
     * @autor  fvillacob
     * @params ano    ........ El ano a buscar
     * @params mes    ........ El mes a buscar
     * @throws Exception.
     * @return el listado de tasa
     */
    public List search(String ano, String mes)throws Exception{
        lista = null;
        this.ano   = ano;
        try{ lista = TasaDataAccess.searchTasa(ano, mes);}
        catch(Exception e){ throw new Exception(e.getMessage());}
        return lista;
    }
    
    
    
    /**
     * Procedimiento que devuelve la lista de tasa 
     * @autor  fvillacob
     * @return el listado de tasa
     */ 
    public List getList(){
        return lista;
    }
    
    
    
    
    /**
     * Procedimiento que devuelve al ano
     * @autor  fvillacob
     * @return el ano
     */
    public String getAno(){
        return this.ano;
    }
    
    
    
    /**
     * Procedimiento que setea la lista     
     * @autor  fvillacob
     */
    public void reset(){
        this.lista = null;
    }
    
    
    
    /**
     * Procedimiento que borra la tasa para un ano y mes
     * parametros
     * @autor  fvillacob
     * @params ano    ........ El ano a borrar
     * @params mes    ........ El mes a borrar
     * @throws Exception.
     * @return un comentario del estado de la transaccion
     */
    public String delete(String ano, String mes)throws Exception{
        String comentario="";
        try{ TasaDataAccess.delete(ano, mes);}
        catch(Exception e){ comentario=e.getMessage(); }
        return comentario;
    }
    
    
    
    
    /**
     * Procedimiento que actualiza la tasa
     * parametros
     * @autor  fvillacob
     * @params ano    ........ El ano a buscar
     * @params mes    ........ El mes a buscar
     * @params dol    ........ El valor del dolar
     * @params bol    ........ El valor del bolivar
     * @params dtf    ........ El valor de la dtf
     * @params user   ........ El usuario que actualiza
     * @throws Exception.
     * @return un comentario del estado de la transaccion
     */
    public String update(String ano, String mes,double dol, double bol, double dtf, String user)throws Exception{
        String comentario="";
        try{ TasaDataAccess.update(ano, mes,dol,bol,dtf,user);}
        catch(Exception e){ comentario=e.getMessage(); }
        return comentario;
    }
    
    
    
    /**
     * Procedimiento que inserta la tasa para una ano y mes especifico
     * parametros
     * @autor  fvillacob
     * @params ano    ........ El ano 
     * @params mes    ........ El mes 
     * @params dol    ........ El valor del dolar
     * @params bol    ........ El valor del bolivar
     * @params dtf    ........ El valor de la dtf
     * @params user   ........ El usuario que actualiza
     * @throws Exception.
     * @return un comentario del estado de la transaccion
     */
     public String insert(String ano, String mes,double dol, double bol, double dtf, String user)throws Exception{
        String comentario="";
        try{ TasaDataAccess.insert(ano, mes,dol,bol,dtf,user);}
        catch(Exception e){ comentario=e.getMessage();}
        return comentario;
    }
     
     
     
     
    
  /**
   * Procedimiento que devuelve el objeto correspondiente al id
   * parametros
   * @autor  fvillacob
   * @params id    ........ id a buscar
   * @return un objeto Tasa
   */
   public Tasa getTasa(int id){
       Tasa tasa =null;;
       if(this.lista!=null && this.lista.size()>0){
           Iterator it = this.lista.iterator();
           while(it.hasNext()){
               Tasa objeto = (Tasa)it.next();
               if(objeto.getId()==id){
                   tasa = new Tasa();
                   tasa = objeto;
                   break;
               }
           }
       }
       return tasa;
   }
   
}
