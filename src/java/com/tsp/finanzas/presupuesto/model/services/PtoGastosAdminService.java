/********************************************************************
 *      Nombre Clase.................   PtoGastosAdminService.java   
 *      Descripci�n..................   Service del archivo pto_gastos_admin
 *      Autor........................   Tito Andr�s Maturana
 *      Fecha........................   07.02.2006
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.finanzas.presupuesto.model.services;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.finanzas.presupuesto.model.beans.*;
import com.tsp.finanzas.presupuesto.model.daos.*;

/**
 *
 * @author  Andres
 */
public class PtoGastosAdminService {
    PtoGastosAdminDAO dao;
    
    /** Creates a new instance of PtoGastosAdminService */
    public PtoGastosAdminService() {
        this.dao = new PtoGastosAdminDAO();
    }
    public PtoGastosAdminService(String dataBaseName) {
        this.dao = new PtoGastosAdminDAO(dataBaseName);
    }
    
    /**
     * Ingresa un registro en el archivo pto_gastos_admin.
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @see com.tsp.operation.model.DAOS.PtoGastosAdminDAO#ingresar()
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void ingresar() throws SQLException{
        dao.ingresar();
    }
    
    /**
     * Verifica la existence de un registro en el archivo pto_gastos_admin.
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @param dstrct Distrito
     * @param cuenta C�digo de la cuenta
     * @param ano A�o presupuestado.
     * @returns El reg_status del registro si existe, de lo contrario null
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @see com.tsp.operation.model.DAOS.PtoGastosAdminDAO#existe(String, String, String)
     * @version 1.0
     */
    public String existe(String dstrct, String cuenta, String ano) throws SQLException{
        return dao.existe(dstrct, cuenta, ano);
    }
    
    /**
     * Actualiza un registro en el archivo pto_gastos_admin.
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @see com.tsp.operation.model.DAOS.PtoGastosAdminDAO#actualizar()
     * @version 1.0
     */
    public void actualizar() throws SQLException{
        dao.actualizar();
    }
    
    /**
     * Anula un registro en el archivo pto_gastos_admin.
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @param dstrct Distrito
     * @param cuenta C�digo de la cuenta
     * @param ano A�o presupuestado.
     * @param usuario Login del usuario que realiza la anulaci�n.
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @see com.tsp.operation.model.DAOS.PtoGastosAdminDAO#anular(String, String, String)
     * @version 1.0
     */
    public void anular(String dstrct, String cuenta, String ano, String user) throws SQLException{
        dao.anular(dstrct, cuenta, ano, user);
    }
    
    /**
     * Realiza una b�squeda por filtros espec�ficos en el archivo pto_gastos_admin
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @param dstrct Distrito
     * @param tcuenta Tipo de cuenta
     * @param agencia C�digo de la Agencia
     * @param unidad C�digo de la unidad
     * @param area C�digo del �rea
     * @param elemento C�digo del elemento del gasto
     * @param ano A�o presupuestado.
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @see com.tsp.operation.model.DAOS.PtoGastosAdminDAO#listar(String, String, String)
     * @version 1.0
     */
    public void listar(String dstrct, String tcuenta, String agencia, String unidad, String area, String elemento, String ano) throws SQLException{
        dao.listar(dstrct, tcuenta, agencia, unidad, area, elemento, ano);
    }
    
    /**
     * Obtiene un registro del archivo pto_gastos_admin
     * @autor Ing. Tito Andr�s Maturana De La Cruz
     * @param dstrct Distrito
     * @param cuenta C�digo de la cuenta
     * @param ano A�o presupuestado.
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void obtener(String dstrct, String cuenta, String ano) throws SQLException{
        dao.obtener(dstrct, cuenta, ano);
    }
    
    /**
     * Getter for property vector.
     * @return Value of property vector.
     */
    public java.util.Vector getVector() {
        return dao.getVector();
    }
    
    /**
     * Setter for property vector.
     * @param vector New value of property vector.
     */
    public void setVector(java.util.Vector vector) {
        dao.setVector(vector);
    }
    
    /**
     * Getter for property presupuesto.
     * @return Value of property presupuesto.
     */
    public PtoGastosAdmin getPresupuesto() {
        return dao.getPresupuesto();
    }
    
    /**
     * Setter for property presupuesto.
     * @param presupuesto New value of property presupuesto.
     */
    public void setPresupuesto(PtoGastosAdmin presupuesto) {
        dao.setPresupuesto(presupuesto);
    }
    
}
