/*************************************************************
 * Nombre      ............... DistribucionPresupuestoService.java
 * Descripcion ............... Opciones de prespuesto
 * Autor       ............... mfontalvo
 * Fecha       ............... Abril - 15 - 2005
 * Version     ............... 1.0
 * Copyright   ............... Fintravalores S.A. S.A
 ************************************************************/

package com.tsp.finanzas.presupuesto.model.services;

import com.tsp.finanzas.presupuesto.model.daos.DistribucionPresupuestoDAO;
import com.tsp.finanzas.presupuesto.model.beans.*;
import com.tsp.util.UtilFinanzas;
import java.sql.*;
import java.util.*;

/**
 *
 * @author  Mario
 */
public class DistribucionPresupuestoService {
    ////////////////////////////////////////////////////////////////////////////////////
    private DistribucionPresupuestoDAO DPDataAccess;
    private TreeMap      Listado;
    private TreeMap      Datos;
    private List         Historial;
    private List         ListaClientes;
    public  Paginacion   paginacion = new Paginacion();
    ////////////////////////////////////////////////////////////////////////////////////
    private List         ListaAgenciaClienteStandar;
    private String       varJSAgenciaClienteStandar;
    private TreeMap      ListaTasa;
    private String       varJSTasa;
    private final String SEPARADOR = "~";
    private String       infoCliente;
    ////////////////////////////////////////////////////////////////////////////////////
    private String Origen = "";
    
    
    
    /** Creates a new instance of DistribucionPresupuestoService */
    public DistribucionPresupuestoService() {
        DPDataAccess = new DistribucionPresupuestoDAO();
        paginacion.setConfiguracion("Opciones/Presupuesto", 20, 15);
    }
    public DistribucionPresupuestoService(String dataBaseName) {
         DPDataAccess = new DistribucionPresupuestoDAO(dataBaseName);
        paginacion.setConfiguracion("Opciones/Presupuesto", 20, 15);
    }
    
    
    
    /**
     * Procedimiento que inicializa los clientes y estandares que puede
     * consultar un usuario dependiendo de la agencia del mismo
     * @autor  mfontalvo
     * @params Agencia ... Agencia del usuario
     * @throws Exception
     */
    
    
    public void Init(String Agencia) throws Exception {
        try{
            BuscarAgenciaClienteStandar(Agencia, "%", 0);
        }
        catch(Exception e){
            throw new Exception("Error en Init_II [DistribucionPresupuestoService]...\n"+e.getMessage());
        }
    }
    
    
    
    
    
    /**
     * Procedimientos de insercion a la base de datos segun sea el modo
     * general, mensual, semanal, diario
     * @autor  mfontalvo
     * @params ESTADO ............ estado del nuevo registro
     * @params DSTRCT_CODE ....... Distrito
     * @params ANO  .............. A�o
     * @params MES ............... Mes
     * @params TIPO .............. Tipo de registro {mensual, semanal, diaria}
     * @params STD_JOB_NO ........ Estandar
     * @params AGENCIA ........... agencia del estandar
     * @params CLIENTE ........... Cliente del estandar
     * @params CMENSUAL .......... Cantidad MENSUAL
     * @params CSEM01,,,CSEM05 ... Cantidad SEMANAL
     * @params CDIA01,,,CDIA31 ... Cantidad DIARIA
     * @params USUARIO ........... Usuario en session
     * @see DPDataAccess.INSERT de acceso al DAO
     * @throws Exception.
     **/
    
    public void INSERT(String ESTADO, String DSTRCT_CODE, String ANO, String MES, String TIPO, String STD_JOB_NO, String AGENCIA, String CLIENTE, String CMENSUAL, String CSEM01, String CSEM02, String CSEM03, String CSEM04, String CSEM05, String CDIA01, String CDIA02, String CDIA03, String CDIA04, String CDIA05, String CDIA06, String CDIA07, String CDIA08, String CDIA09, String CDIA10, String CDIA11, String CDIA12, String CDIA13, String CDIA14, String CDIA15, String CDIA16, String CDIA17, String CDIA18, String CDIA19, String CDIA20, String CDIA21, String CDIA22, String CDIA23, String CDIA24, String CDIA25, String CDIA26, String CDIA27, String CDIA28, String CDIA29, String CDIA30, String CDIA31, String USUARIO) throws Exception {
        try{
            String []Parametros = {ESTADO, DSTRCT_CODE, ANO, MES, TIPO, STD_JOB_NO, AGENCIA, CLIENTE, CMENSUAL, CSEM01 ,CSEM02, CSEM03, CSEM04, CSEM05, CDIA01, CDIA02, CDIA03, CDIA04, CDIA05, CDIA06, CDIA07, CDIA08, CDIA09, CDIA10, CDIA11, CDIA12, CDIA13, CDIA14, CDIA15, CDIA16, CDIA17, CDIA18, CDIA19, CDIA20, CDIA21, CDIA22, CDIA23, CDIA24, CDIA25, CDIA26, CDIA27, CDIA28, CDIA29, CDIA30, CDIA31, USUARIO};
            DPDataAccess.INSERT( Parametros);
        }
        catch(Exception e){
            throw new Exception("Error en INSERT [DistribucionPresupuestoService]...\n"+e.getMessage());
        }
    }
    
    
    /**
     * Procedimientos de insercion a la base de datos en modo Mensual
     * @autor  mfontalvo
     * @params ESTADO ............ estado del nuevo registro
     * @params DSTRCT_CODE ....... Distrito
     * @params ANO  .............. A�o
     * @params MES ............... Mes
     * @params TIPO .............. Tipo de registro {mensual, semanal, diaria}
     * @params STD_JOB_NO ........ Estandar
     * @params AGENCIA ........... agencia del estandar
     * @params CLIENTE ........... Cliente del estandar
     * @params CMENSUAL .......... Cantidad MENSUAL
     * @params USUARIO ........... Usuario en session
     * @see DPDataAccess.INSERT de acceso al DAO
     * @throws Exception.
     **/
    public void INSERT_MENSUAL(String DSTRCT_CODE, String ANO, String MES, String STD_JOB_NO, String AGENCIA, String CLIENTE, String CMENSUAL, String USUARIO) throws Exception {
        try{
            String [] Parametros = { "", DSTRCT_CODE, ANO, MES, "M", STD_JOB_NO, AGENCIA, CLIENTE, CMENSUAL, CMENSUAL, "0", "0", "0", "0", CMENSUAL , "0" , "0", "0", "0", "0" , "0", "0" , "0", "0", "0", "0", "0", "0", "0", "0" , "0", "0", "0", "0", "0", "0", "0", "0", "0", "0" , "0", "0", "0", "0", "0", USUARIO };
            DPDataAccess.INSERT( Parametros);
        }
        catch(Exception e){
            throw new Exception("Error en INSERT_MENSUAL [DistribucionPresupuestoService]...\n"+e.getMessage());
        }
    }
    
    
    /**
     * Procedimientos de insercion a la base de datos en modo semanal
     * @autor  mfontalvo
     * @params ESTADO ............ estado del nuevo registro
     * @params DSTRCT_CODE ....... Distrito
     * @params ANO  .............. A�o
     * @params MES ............... Mes
     * @params TIPO .............. Tipo de registro {mensual, semanal, diaria}
     * @params STD_JOB_NO ........ Estandar
     * @params AGENCIA ........... agencia del estandar
     * @params CLIENTE ........... Cliente del estandar
     * @params CMENSUAL .......... Cantidad MENSUAL
     * @params CSEMANAL [] ....... Cantidad SEMANAL
     * @params USUARIO ........... Usuario en session
     * @see DPDataAccess.INSERT de acceso al DAO
     * @throws Exception.
     **/
    public void INSERT_SEMANAL(String DSTRCT_CODE, String ANO, String MES, String STD_JOB_NO, String AGENCIA, String CLIENTE, String CMENSUAL, String CSEMANAL[], String USUARIO) throws Exception {
        try{
            String [] CDIA = UtilFinanzas.CalculoDiario( CSEMANAL );
            String [] Parametros = {"", DSTRCT_CODE, ANO, MES, "S",  STD_JOB_NO, AGENCIA, CLIENTE, CMENSUAL, CSEMANAL[0], CSEMANAL[1],CSEMANAL[2], CSEMANAL[3], CSEMANAL[4], CDIA[0] ,CDIA[1] ,CDIA[2] ,CDIA[3] ,CDIA[4] ,CDIA[5] ,CDIA[6] ,CDIA[7] ,CDIA[8] ,CDIA[9] ,CDIA[10] ,CDIA[11] ,CDIA[12] ,CDIA[13] ,CDIA[14] ,CDIA[15] ,CDIA[16] ,CDIA[17] ,CDIA[18] ,CDIA[19] ,CDIA[20] ,CDIA[21] ,CDIA[22] ,CDIA[23] ,CDIA[24] , CDIA[25] ,CDIA[26] ,CDIA[27] ,CDIA[28] ,CDIA[29] ,CDIA[30] , USUARIO};
            DPDataAccess.INSERT( Parametros);
        }
        catch(Exception e){
            throw new Exception("Error en INSERT_SEMANAL [DistribucionPresupuestoService]...\n"+e.getMessage());
        }
    }
    
    
    /**
     * Procedimientos de insercion a la base de datos en modo diario
     * @autor  mfontalvo
     * @params ESTADO ............ estado del nuevo registro
     * @params DSTRCT_CODE ....... Distrito
     * @params ANO  .............. A�o
     * @params MES ............... Mes
     * @params TIPO .............. Tipo de registro {mensual, semanal, diaria}
     * @params STD_JOB_NO ........ Estandar
     * @params AGENCIA ........... agencia del estandar
     * @params CLIENTE ........... Cliente del estandar
     * @params CMENSUAL .......... Cantidad MENSUAL
     * @params CDIA[] ............ Cantidad DIARIA
     * @params USUARIO ........... Usuario en session
     * @see DPDataAccess.INSERT de acceso al DAO
     * @throws Exception.
     **/
    public String INSERT_DIARIO(String DSTRCT_CODE, String ANO, String MES, String STD_JOB_NO, String AGENCIA, String CLIENTE, String CMENSUAL, String CDIA[], String USUARIO) throws Exception {
        try{
            String [] CSEMANAL = UtilFinanzas.CalculoSemanal( CDIA );
            String [] Parametros = {"", DSTRCT_CODE, ANO, MES, "D" ,STD_JOB_NO, AGENCIA, CLIENTE, CMENSUAL, CSEMANAL[0], CSEMANAL[1],CSEMANAL[2], CSEMANAL[3], CSEMANAL[4], CDIA[0] ,CDIA[1] ,CDIA[2] ,CDIA[3] ,CDIA[4] ,CDIA[5] ,CDIA[6] ,CDIA[7] ,CDIA[8] ,CDIA[9] ,CDIA[10] ,CDIA[11] ,CDIA[12] ,CDIA[13] ,CDIA[14] ,CDIA[15] ,CDIA[16] ,CDIA[17] ,CDIA[18] ,CDIA[19] ,CDIA[20] ,CDIA[21] ,CDIA[22] ,CDIA[23] ,CDIA[24] , CDIA[25] ,CDIA[26] ,CDIA[27] ,CDIA[28] ,CDIA[29] ,CDIA[30] , USUARIO};
            return DPDataAccess.INSERT(Parametros);
        }
        catch(Exception e){
            throw new Exception("Error en INSERT_DIARIO [DistribucionPresupuestoService]...\n"+e.getMessage());
        }
    }
    
    
    /**
     * Procedimiento de Eliminacion del  presupuesto dado
     * el distrito y el periodo
     * @autor mfontalvo
     * @params Distrito ... Distrito a eliminar
     * @params Ano ........ Ano a eliminar
     * @params Mes ........ Mes a eliminar
     * @throws Exception.
     */
    
    public void DELETE(String Distrito, String  Ano, String Mes) throws Exception {
        try{
            DPDataAccess.BORRAR_PTO(Distrito, Ano, Mes);
        }
        catch(Exception e){
            throw new Exception("Error en DELETE [DistribucionPresupuestoService]...\n"+e.getMessage());
        }
    }
    
    /**
     * Procedimineto para anular el ultimo presupuesto
     * @autor  mfontalvo
     * @fecha  2006-01-24
     * @params Distrito , distrito del estandar
     * @params Estandar , numero del estandar
     * @params Ano , a�o del viaje a anular
     * @params Mes , mes del viaje a anular
     * @throws Exception.
     */
    public void ANULAR(String Distrito, String Estandar, String Ano, String Mes) throws Exception{
        try{
            DPDataAccess.AnularUltimoViaje(Distrito, Estandar, Ano + Mes);
        }
        catch(Exception e){
            throw new Exception("Error en ANULAR [DistribucionPresupuestoService]...\n"+e.getMessage());
        }
    }
    
    
    
    
    
    /**
     * Procedimiento para obtener el listado general de estandares con sus
     * respectivos viajes presupuestados segun los siguientes parametros
     * @autor  mfontalvo
     * @params Tipo ..... Tipo de Busqueda (M: Vista Mesual, S: Vista Semanal, D: Vista Diaria)
     * @params Ano  ..... A�o a consultar
     * @params Mes  ..... Mes a consultar
     * @params Stdjob  .. Numero del Estandar a consultar
     * @params Cliente .. Cliente a consultar
     * @params Agencia .. Agencia a consultar
     * @params Origen ... Origen a consultar
     * @params Destino .. Destino a consultar
     * @params AgenciaDespacho .. agencia de despacho del estandar
     * @params Filtro  .. Indican si se deben filtrar solo por aquellos que estan prespuestados
     * @params diaInicial .. dia inicial en mosrtrar
     * @params diaFinal .. dia final en mosrtrar
     * @params MantenerPaginacion  .. Indican si debe mantener la vista actual de la paginacion
     * @throws Exception.
     * @see ReiniciarListado, BuscarPresupuesto de acceso al DAO
     */
    
    public void ObtenerListado(String Tipo, String Ano, String Mes, String Stdjob, String Cliente , String Agencia, String Origen, String Destino, String AgenciaDespacho, boolean MantenerPaginacion, String Filtro, String  diaInicial, String diaFinal ) throws Exception{
        try{
            this.ReiniciarListado();
            this.Listado = DPDataAccess.BuscarPresupuesto(
            (Tipo.equals("M")?0:(Tipo.equals("S")?1:2))  ,
            Ano,
            Mes,
            Stdjob,
            Cliente,
            Agencia,
            Origen,
            Destino,
            AgenciaDespacho ,
            (Filtro.equals("Ok")?true: false)
            );
            this.Listado.put("Stdjob"  , Stdjob  );
            this.Listado.put("Cliente" , Cliente );
            this.Listado.put("Origen"  , Origen  );
            this.Listado.put("Destino" , Destino );
            this.Listado.put("Agencia" , Agencia );
            this.Listado.put("Tipo"    , Tipo    );
            this.Listado.put("Ano"     , Ano     );
            this.Listado.put("Mes"     , Mes     );
            this.Listado.put("Filtro"  , Filtro  );
            this.Listado.put("diaInicial" , diaInicial  );
            this.Listado.put("diaFinal"   , diaFinal    );
            this.Listado.put("AgenciaDespacho" , AgenciaDespacho  );
            paginacion.setLista(this.getLista());
            if (!MantenerPaginacion) paginacion.setVista_Actual(1);
        }catch (Exception e){
            throw new Exception("Error en ObtenerListado [DistribucionPresupuestoService]...\n"+e.getMessage());
        }
        
    }
    
    
    /**
     * Procedimiento para obtener un estandar especifico con sus
     * respectivos viajes presupuestados segun los siguientes parametros
     * @autor  mfontalvo
     * @params Ano  ..... A�o a consultar
     * @params Mes  ..... Mes a consultar
     * @params Stdjob  .. Numero del Estandar a consultar
     * @params Cliente .. Cliente a consultar
     * @params Agencia .. Agencia a consultar
     * @throws Exception.
     * @see ReiniciarDatos , BuscarPresupuesto de acceso al DAO
     */
    
    public void Buscar(String Ano, String Mes, String Stdjob, String Cliente , String Agencia ) throws Exception{
        try{
            this.ReiniciarDatos();
            this.Datos = DPDataAccess.BuscarPresupuesto(2 , Ano,Mes,Stdjob,Cliente,Agencia,"%", "%", "%", false);
            this.Datos.put("Stdjob"  ,Stdjob);
            this.Datos.put("Cliente" ,Cliente);
            this.Datos.put("Origen"  ,"%");
            this.Datos.put("Destino" ,"%");
            this.Datos.put("Agencia" ,Agencia);
            this.Datos.put("Tipo"    ,"D");
            this.Datos.put("Ano"     ,Ano);
            this.Datos.put("Mes"     ,Mes);
        }catch (Exception e){
            throw new Exception("Error en Buscar [DistribucionPresupuestoService]...\n"+e.getMessage());
        }
    }
    
    
    /**
     * Procedimiento buscar el historial de viajes de un estandar
     * @autor  mfontalvo
     * @params Distrito .. Dsitrito a consultar
     * @params Estandar  .. Numero del Estandar a consultar
     * @params Ano  ..... A�o a consultar
     * @params Mes  ..... Mes a consultar
     * @throws Exception.
     * @see ReiniciarHistorial , Historial de acceso al DAO
     */
    
    public void BuscarHistorial(String Distrito, String Estandar, String Ano, String Mes) throws Exception{
        try{
            this.ReiniciarHistorial();
            this.Historial = DPDataAccess.Historial(Distrito, Estandar, Ano, Mes);
        }catch (Exception e){
            throw new Exception("Error en BuscarHistorial [DistribucionPresupuestoService]...\n"+e.getMessage());
        }
    }
    
    
    /**
     * Procedimiento para extraer la agencia due�a de un cliente
     * @autor  mfontalvo
     * @params Cliente .... Codigo del Cliente
     * @throws Exception.
     * @see ReiniciarInfoCliente, AgenciaDuenaCliente de acceso al DAO
     */
    
    public void AgenciaDuenaCliente(String Cliente) throws Exception{
        try{
            this.ReiniciarInfoCliente();
            this.infoCliente = DPDataAccess.AgenciaDuenaCliente(Cliente);
        }catch (Exception e){
            throw new Exception("Error en AgenciaDuenaCliente [DistribucionPresupuestoService]...\n"+e.getMessage());
        }
    }
    
    
    
    /**
     * Procedimiento que busca todas las agencias, los clientes relacionados
     * a la misma y los estandares respectivamente relacionados.
     * @autor  mfontalvo
     * @params agencia ... Agencia a consultar
     * @params cliente ... Cliente a consultar,
     * @params modo  ..... Modo en que se va a ordenar
     */
    public void BuscarAgenciaClienteStandar(String agencia, String cliente, int modo) throws Exception{
        try{
            this.ReiniciarListaAgenciaClienteStandar();
            this.ListaAgenciaClienteStandar = DPDataAccess.AgenciasClientesStandar(agencia, cliente, modo);
        }catch (Exception e){
            throw new Exception("Error en BuscarAgenciaClienteStandar [DistribucionPresupuestoService]...\n"+e.getMessage());
        }
    }
    
    
    /**
     * Procemiento para consultar las tasas de cambios de un a�o
     * @params A�o ..... a�o a consultar
     * @autor  mfontalvo
     * @params Ano ... a�o a consultar
     * @throws Exception.
     * @see ReiniciarTasa, Tasa de acceso al DAO
     */
    public void BuscarTasa(String Ano) throws Exception{
        try{
            this.ReiniciarTasa();
            this.ListaTasa = DPDataAccess.Tasa(Ano);
        }catch (Exception e){
            throw new Exception("Error en BuscarTasa [DistribucionPresupuestoService]...\n"+e.getMessage());
        }
    }
    
    
    
    /**
     * Procedimiento que genera una variable de javascript en base
     * a los datos de tasa de cambio
     * @autor  mfontalvo
     */
    
    public void ConstruirVarJSTasa(){
        if (this.ListaTasa!=null){
            
            String varJS = "var tasa = [ ";
            for (int i=1; i<=12; i++){
                
                
                DatosGeneral ts = (DatosGeneral) ListaTasa.get( (i<10?"0":"") + String.valueOf(i)) ;
                
                
                String item = "\n['"+ ts.getValor("ANO")  + SEPARADOR + ts.getValor("MES")  + SEPARADOR+
                ts.getValor("DOL")  + SEPARADOR + ts.getValor("BOL")  + SEPARADOR +
                ts.getValor("DTF")  + "']";
                varJS += item + ",";
            }
            varJS = varJS.substring(0,varJS.length()-1) + " ]; ";
            this.varJSTasa = varJS;
        }
        else
            this.varJSTasa = " var tasa = []; ";
    }
    
    
    /**
     * Procedimento para buscar el listado general
     * de clientes
     * @autor  mfontalvo
     * @throws Exception.
     * @see ListaClientes de acceso a la base de datos.
     */
    
    
    public void BuscarClientes() throws Exception{
        try{
            this.ListaClientes = DPDataAccess.ListaClientes();
        }catch (Exception e){
            throw new Exception("Error en BuscarTasa [DistribucionPresupuestoService]...\n"+e.getMessage());
        }
    }
    
    
    
    /////////////////////////////////////////////////////////////////////////////
    // setter
    
    /**
     * Devuelve la lista de viajes con sus presupuestos asignado
     * @autor  mfontalvo
     */
    public void ReiniciarListado(){
        this.Listado = null;
    }
    
    /**
     * Devuelve un estandar especifico con su presupuesto asignados
     * @autor  mfontalvo
     */
    
    public void ReiniciarDatos(){
        this.Datos = null;
    }
    
    /**
     * Procedimiento que reinicia el listado general de
     * agencias, clientes y estandares
     * @autor  mfontalvo
     */
    
    public void ReiniciarListaAgenciaClienteStandar(){
        this.ListaAgenciaClienteStandar = null;
    }
    
    /**
     * Procedimiento que reinicia el listados de  tasas
     * @autor  mfontalvo
     */
    
    public void ReiniciarTasa(){
        this.ListaTasa = null;
    }
    
    /**
     * Procedimiento que setea el valor origen que indica el tipo de vista origen
     * @autor  mfontalvo
     * @params valor .... origen de las paginas
     */
    
    public void setOrigen(String valor){
        this.Origen = valor;
    }
    
    /**
     * Procedimiento que reinicia el dato alamacenado en infoCliente
     * @autor  mfontalvo
     */
    
    public void ReiniciarInfoCliente(){
        this.infoCliente = "";
    }
    
    
    /**
     * Procedimiento que reinicia el listado historico de un estandar especifico
     * @autor  mfontalvo
     */
    
    public void ReiniciarHistorial(){
        this.Historial = null;
    }
    
    
    
    
    
    
    
    /////////////////////////////////////////////////////////////////////////////
    // getter
    
    
    /**
     * Funcion que devuelve los viajes presupuestados generales
     * @autor  mfontalvo
     * @return Lista de viajes
     */
    
    public List getLista(){
        return (List) this.Listado.get("valores");
    }
    
    /**
     * Funcion que devuelve los viajes presupuestados acumulados
     * @autor  mfontalvo
     * @return Listado de totales acumulados
     */
    
    public TreeMap getListaTotales(){
        return (TreeMap) this.Listado.get("totales");
    }
    
    /**
     * Funcion que devuelve el numero de columnas que debera manejar la vista
     * debido a que puese ser mensual (1 � 12 meses), semanal o diaria
     * @autor  mfontalvo
     * @return Numero de Columnas de la vista
     */
    
    public int getNroColumnas(){
        return Integer.parseInt(this.Listado.get("nrocols").toString());
    }
    
    /**
     * Funcion que devuelve un valor asigando en el TreeMap Datos
     * @autor  mfontalvo
     * @params key ...... Valor a consultar
     * @return Valor asociada a la llave
     */
    
    public String getValor(String key){
        return (this.Listado.get(key)!=null?this.Listado.get(key).toString():null);
    }
    
    /**
     * Funcion que devuelve los viajes presupuestados de un estandar
     * @autor  mfontalvo
     * @return Lista de Datos
     */
    
    public List getDatos(){
        return (List) this.Datos.get("valores");
    }
    
    /**
     * Funcion que devuelve un valor asigando en el TreeMap Datos
     * @autor  mfontalvo
     * @params key ...... Valor a consultar
     * @return Valor asociada a la llave
     */
    
    public String getDValor(String key){
        return (this.Datos.get(key)!=null?this.Datos.get(key).toString():null);
    }
    
    
    /**
     * Funcion que devuelve el listado de agencia, clientes y estandares
     * @autor  mfontalvo
     * @return Listado de Agencias, Clientes, y estandares relacionados
     */
    public List getListadoAgenciasClientesStandar(){
        return this.ListaAgenciaClienteStandar;
    }
    
    
    /**
     * Funcion que retorna la variable de JavaScript
     * @autor  mfontalvo
     * @return variable javascript de agencias, clientes y estnadares
     */
    public String getVarJSAgenciaClienteStandar(){
        return this.varJSAgenciaClienteStandar;
    }
    
    /**
     * Funcion que devuelve el Listado de Tasas Anuales
     * @autor  mfontalvo
     * @return Listado de Tasas
     */
    public TreeMap getListadoTasa(){
        return this.ListaTasa;
    }
    
    /**
     * Funcion que retorna la variable Javascript generada para las tasas
     * @autor  mfontalvo
     * @return Vartible Tasa de javascript
     */
    public String getVarJSTasa(){
        return this.varJSTasa;
    }
    
    /**
     * Funcion que devuelve el caracter separador utilizado por todas las
     * generaciones de JavaScript
     * @autor  mfontalvo
     * @return Variable separador de Javascript
     */
    
    public String getVarJSSeparador(){
        return " var separador = '"+ this.SEPARADOR +"';";
    }
    
    
    /**
     * Procedimiento para Obtener el Origen de Vista que este realizando el
     * proceso actual de consulta de presupuesto.
     * @autor  mfontalvo
     * @return Origen de las paginas
     */
    public String getOrigen(){
        return this.Origen;
    }
    
    /**
     * Funcion para obtener la informacion actual del cliente  que tenga
     * almacenada la variable infoCliente
     * @autor  mfontalvo
     * @return Informacion del Cliente
     */
    
    public String getInfoCliente(){
        return this.infoCliente;
    }
    
    /**
     * Funcion que retorna el Historial.
     * @autor  mfontalvo
     * @return Listado de Historial
     */
    
    public List getHistorial(){
        return this.Historial;
    }
    
    
    /**
     * Procedimiento que indica si un presupuesto es modificable
     * dependiendo de los siguientes parametros.
     * @params TipoBD .... Tipo de registro en la Base de datos
     * @params TipoVT .... Tipo de vista que se maneja.
     * @return boolean que inidca si es o no modificable.
     */
    
    public boolean Modificable(String TipoBD, String TipoVT){
        int tBD = (TipoBD.equals("M")?0:TipoBD.equals("S")?1:2);
        int tVT = (TipoVT.equals("M")?0:TipoVT.equals("S")?1:2);
        return (tVT >= tBD ? true: false);
    }
    
    
    /**
     * Procedimiento que retorna el
     * listado general de clientes
     * @autor  mfontalvo
     * @return Listado de Clientes
     */
    
    public List getListaClientes() {
        return ListaClientes;
    }
    /**
     * Metodo buscarptoCarbon, busca los viajes planeados de acuerdo aun estandar especifico de un mes
     * @autor : Diogenes Bastidas Morales.
     * @param : fecha inicio, fecha fin y distrito
     * @return: Vector pto carbon
     * @version : 1.0
     */
    public int [] buscarptoCarbon( String fecini , String fecfin, String stdjob, String dstrct  )throws SQLException{
        return DPDataAccess.buscarptoCarbon(fecini.substring(0,4),fecini.substring(5,7),fecfin.substring(0,4),fecfin.substring(5,7),stdjob,dstrct);
        
    }
    
    
    /**
     * Metodo insertarReferencia, insertar la referencia del viaje
     * @autor : Diogenes Bastidas Morales.
     * @param : objeto tipo Ref_viaje
     * @return:
     * @version : 1.0
     */
    public String insertarReferencia(Ref_viaje ref_viaje)throws Exception{
        return DPDataAccess.insertarReferencia(ref_viaje);
    }
    
    /**
     * Metodo modificarReferencia, modifica la referencia del viaje
     * @autor : Diogenes Bastidas Morales.
     * @param : objeto tipo Ref_viaje
     * @return:
     * @version : 1.0
     */
    public void modificarReferencia(Ref_viaje ref_viaje)throws Exception{
        try{
            DPDataAccess.modificarReferencia(ref_viaje);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    
    /**
     * Metodo eliminarReferencia, modifica la referencia del viaje
     * @autor : Diogenes Bastidas Morales.
     * @param : objeto tipo Ref_viaje
     * @return: 
     * @version : 1.0
     */
    public String eliminarReferencia(Ref_viaje ref_viaje)throws Exception{
        return DPDataAccess.eliminarReferencia(ref_viaje);
    }
    
    /**
     * Metodo modificarReferencia, modifica la referencia del viaje
     * @autor : Diogenes Bastidas Morales.
     * @param : objeto tipo Ref_viaje
     * @return:
     * @version : 1.0
     */
    public void listarReferenciaViaje(String dstrct, String sj, String ano, String mes, int dia  )throws Exception{
        try{
            DPDataAccess.listarReferenciaViaje(dstrct, sj, ano, mes, dia);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    
    /**
     * Getter for property vecRef_Viaje.
     * @return Value of property vecRef_Viaje.
     */
    public Vector getVecRef_Viaje() {
        return DPDataAccess.getVecRef_Viaje();
    }
    
}