 

package com.tsp.finanzas.presupuesto.model;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.util.Date;
import com.tsp.finanzas.presupuesto.model.services.*;


public class Model implements Serializable {
    
    public DistribucionPresupuestoService DPtoSvc;
    public IncrementosPorcentualesService IPorcentualesSvc;
    public TasaService                    TasaSvc;
    public ReportesService                ReportesSvc;
    public ReprogramacionesService        ReprogamacionSvc;
    public CostosOperativosService        CostosOperativosSvc;
    
    //tmaturana
    public PtoGastosAdminService presupuesto_gadminSvc;

    public Model() throws Exception {
        try{/*
            DPtoSvc             = new DistribucionPresupuestoService();
            IPorcentualesSvc    = new IncrementosPorcentualesService();
            TasaSvc             = new TasaService();
            ReportesSvc         = new ReportesService();
            ReprogamacionSvc    = new ReprogramacionesService();
            CostosOperativosSvc = new CostosOperativosService();
            
            //tito
            presupuesto_gadminSvc = new PtoGastosAdminService();
        */  
        }
        catch(Exception e){
            throw new Exception("Error en constructor [Model]...\n"+e.getMessage());
        }
    }
    
    public Model(String dataBaseName) throws Exception {
        try{
            DPtoSvc             = new DistribucionPresupuestoService(dataBaseName);
            IPorcentualesSvc    = new IncrementosPorcentualesService(dataBaseName);
            TasaSvc             = new TasaService(dataBaseName);
            ReportesSvc         = new ReportesService(dataBaseName);
            ReprogamacionSvc    = new ReprogramacionesService(dataBaseName);
            CostosOperativosSvc = new CostosOperativosService(dataBaseName);
            presupuesto_gadminSvc = new PtoGastosAdminService(dataBaseName);            
        }
        catch(Exception e){
            throw new Exception("Error en constructor [Model]...\n"+e.getMessage());
        }
    }
}
