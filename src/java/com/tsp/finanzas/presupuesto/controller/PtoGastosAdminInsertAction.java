/********************************************************************
 *      Nombre Clase.................   PtoGastosAdminInsertAction.java
 *      Descripci�n..................   Inserta un registro en el archivo pto_gastos_admin
 *      Autor........................   Ing. Tito Andr�s Maturana
 *      Fecha........................   08.02.2006
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/
package com.tsp.finanzas.presupuesto.controller;

import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.finanzas.presupuesto.model.beans.*;
import com.tsp.finanzas.presupuesto.model.*;
import com.tsp.operation.model.threads.*;
import org.apache.log4j.Logger;
/**
 *
 * @author  Andres
 */
public class PtoGastosAdminInsertAction extends Action{
    
    /** Creates a new instance of PosBancariaRefreshAction */
    public PtoGastosAdminInsertAction() {
    }
    
    public void run() throws ServletException, java.io.IOException {
        
        String next = "/jsp/finanzas/presupuesto/gastos_admin/PtoGastosAdminInsert.jsp?msg=";
        
        String[] pto_vlr = request.getParameterValues("vlr_pto");
        double[] vlr_pto = new double[12];
        
        for(int i=0; i<pto_vlr.length; i++){
            vlr_pto[i] = Double.parseDouble(pto_vlr[i].replaceAll(",",""));
        }
        
        String agencia = request.getParameter("agencia");
        String elemento = request.getParameter("elemento");
        String unidad = request.getParameter("unidad");
        String area = request.getParameter("area");
        String ano = request.getParameter("ano");
        String dstrct = request.getParameter("distrito");
        String cuenta = request.getParameter("clase");
        
        HttpSession session = request.getSession();
        Usuario user = (Usuario) session.getAttribute("Usuario");
                
        PtoGastosAdmin pto = new PtoGastosAdmin();
        pto.setCreation_user(user.getLogin());
        pto.setBase(user.getBase());
        pto.setDstrct(dstrct);
        pto.setAno(ano);
        pto.setCuenta(cuenta + agencia + unidad + area + elemento);
        pto.setVlr_presupuestado(vlr_pto);
        
        try{
            com.tsp.finanzas.contab.model.Model modelcontab = (com.tsp.finanzas.contab.model.Model) session.getAttribute("modelcontab");
            boolean cuentavalida = modelcontab.cuentavalidaservice.ExisteCuentaValida ("C", agencia, unidad, area, elemento);
            
            if (!cuentavalida) {
                next += "Cuenta Inv�lida.";
            } else {
                model.presupuesto_gadminSvc.setPresupuesto(pto);
                String reg_status = model.presupuesto_gadminSvc.existe(dstrct, pto.getCuenta(), pto.getAno());
             
                if( reg_status==null ){
                    model.presupuesto_gadminSvc.ingresar();
                    next += "El presupuesto para el per�odo, la cuenta y el distrito indicado se ha ingresado exitosamente.";
                } else if ( reg_status.compareTo("A")==0 ){
                    pto.setReg_status("");
                    model.presupuesto_gadminSvc.actualizar();
                    next += "El presupuesto para el per�odo, la cuenta y el distrito indicado se ha ingresado exitosamente.";
                } else {
                    next += "El presupuesto para el per�odo, la cuenta y el distrito indicado ya existe.";
                }
            }
            
            
        
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if (rd==null)
                throw new Exception("No se pudo encontrar " + next);
            rd.forward(request, response);
        }catch (Exception e){
            e.printStackTrace();
            throw new ServletException(e.getMessage());
        }        
    }
    
}
