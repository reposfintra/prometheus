/*************************************************************
 * Nombre      ............... MenuAction.java
 * Descripcion ............... Direcciona a la pagina establecida
 * Autor       ............... mfontalvo
 * Fecha       ............... Abril - 14 - 2005
 * Version     ............... 1.0
 * Copyright   ............... Fintravalores S.A. S.A
 ************************************************************/




package com.tsp.finanzas.presupuesto.controller;

import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.Usuario;




public class MenuAction extends Action{
    
    
    /* Procedimiento que ejecuta la accion
     * parametros
     * @params request     ........  HttpServletRequest
     * @params response    ........  HttpServletResponse
     * @throws ServletException, IOException
     */
    
    public void run() throws ServletException, IOException {
        try{
            
            HttpSession session = request.getSession();
            Usuario     usuario = (Usuario)session.getAttribute("Usuario");
            com.tsp.operation.model.Model modelOperation = (com.tsp.operation.model.Model) session.getAttribute("model");
            if (usuario==null) throw new Exception("Usuario no encontrado en session"); 
            
            
            String agusuario = usuario.getId_agencia();
            agusuario = (agusuario.equals("")?"%":agusuario);
            
            
            
            String carpeta = (request.getParameter("FOLDER")!=null?request.getParameter("FOLDER"):"");
            carpeta = (carpeta.equals("none") || carpeta.equals("")?"":carpeta.concat("/"));
            String pagina  = request.getParameter("JSP");
            
            String next = "/jsp/finanzas/presupuesto/"+ carpeta + pagina +".jsp";
            
            ////System.out.println(next);
            
            if(pagina!=null){
                String Jsp = pagina.toUpperCase().trim();
                if(Jsp.equals("PTOFORMCMB"))       {
                    model.DPtoSvc.Init(agusuario);
                    modelOperation.agenciaService.searchAgencia();
                }
                if(Jsp.equals("PTOFORMCLIENTES"))  {
                    model.DPtoSvc.ReiniciarListaAgenciaClienteStandar();
                    model.DPtoSvc.BuscarClientes();
                }
                if(Jsp.equals("INCPORCENTUALES"))       model.IPorcentualesSvc.Init();
                if(Jsp.equals("APLICARINCPORC"))        model.IPorcentualesSvc.Init_AplicarIC();
                if(Jsp.equals("TASAPROYECTADA"))        model.TasaSvc.reset();
                if(Jsp.equals("REPORTESPTO"))       {
                    model.DPtoSvc.Init(agusuario);
                    modelOperation.agenciaService.searchAgencia();
                }
                if(Jsp.equals("REPROGRAMACIONES"))      model.ReprogamacionSvc.SEARCH_ALL();
                if(Jsp.equals("PTOEXPORTARHISTORIAL"))  model.DPtoSvc.Init(agusuario);
                if(Jsp.equals("BUSCARPERIODO"))         model.CostosOperativosSvc.Init();
                   //JEscandon 27-01-06
                if(Jsp.equals("REPORTESCOSTOS"))       model.DPtoSvc.Init(agusuario);
                if(Jsp.equals("COSTOSEJECVSPDO"))      model.DPtoSvc.Init(agusuario);
            }
            
            
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if(rd == null)
                throw new Exception("No se pudo encontrar "+ next);
            rd.forward(request, response);
        }
        catch(Exception e){
            throw new ServletException(e.getMessage());
        }
    }
    
    
}
