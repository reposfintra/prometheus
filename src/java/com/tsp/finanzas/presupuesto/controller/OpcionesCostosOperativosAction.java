/*************************************************************
 * Nombre      ............... OpcionesCostosOperativosAction.java
 * Descripcion ............... Clase de manipulacion de
 *                             operaciones de Costos Operativos
 * Autor       ............... mfontalvo
 * Fecha       ............... Diciembre - 12 - 2005
 * Version     ............... 1.0
 * Copyright   ............... Fintravalores S.A. S.A
 ************************************************************/

package com.tsp.finanzas.presupuesto.controller;

import javax.servlet.*;
import javax.servlet.http.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;
import com.tsp.finanzas.presupuesto.model.beans.*;



public class OpcionesCostosOperativosAction extends Action{
    
    
    
    /* Procedimiento que ejecuta la accion
     * parametros
     * @params request     ........  HttpServletRequest
     * @params response    ........  HttpServletResponse
     * @throws ServletException, IOException
     */
    
    public void run() throws javax.servlet.ServletException, java.io.IOException  {
        try{
            
            
            ////////////////////////////////////////////////////////////////////
            // parametros generales
            String Opcion       = defaultString("Opcion");
            String Agencia      = defaultString("Agencia");
            String NAgencia     = defaultString("NAgencia");
            String Cliente      = defaultString("Cliente");
            String Costo        = defaultString("Costo");
            String NCosto       = defaultString("NCosto");
            String Ano          = defaultString("Ano");
            String Mes          = defaultString("Mes");
            String Item         = defaultString("item");
            String Estandar     = defaultString("Estandar");
            String Vista        = defaultString("Vista");
            String Prolongacion = defaultString("Prolongacion");
            
            String []Costos = request.getParameterValues("CostoOperativo");
            String []Valor  = request.getParameterValues("Valor");
            
            
            String Usuario  = "ghost";
            String next     = "/jsp/finanzas/presupuesto/CostosOperativos/Listado.jsp";
            
            ////////////////////////////////////////////////////////////////////
           
            if (Opcion!=null){
                
                ////////////////////////////////////////////////////////////////////////////////////////
                
                if (Opcion.equals("Buscar")){
                    model.CostosOperativosSvc.setAno(Ano);
                    model.CostosOperativosSvc.setMes(Mes);
                    model.CostosOperativosSvc.setAgencia(Agencia);
                    model.CostosOperativosSvc.setCliente(Cliente);                    
                    
                    Agencia = (Agencia.equals("ALL")?"%":Agencia);
                    Cliente = (Cliente.equals("ALL")?"%":Cliente);
                    model.CostosOperativosSvc.BuscarVentas(Ano, Mes, Agencia, Cliente);
                    if (model.CostosOperativosSvc.getListado().isEmpty()){
                        next = "/jsp/finanzas/presupuesto/CostosOperativos/BuscarPeriodo.jsp?Mensaje=" +
                               "No se encontraron datos, por favor intente con otro tipo de filtro!!!";
                    }
                }
                
                else if (Opcion.equals("BuscarCostos")){
                    next = "/jsp/finanzas/presupuesto/CostosOperativos/Edicion.jsp";
                    DatosGeneral dt = (DatosGeneral) model.CostosOperativosSvc.getListado().get(Integer.parseInt(Item));
                    model.CostosOperativosSvc.setDatos( dt );   
                    model.CostosOperativosSvc.BuscarElementosCostosOperativos(
                        dt.getValor("STDJOBNO"),
                        model.CostosOperativosSvc.getAno(), 
                        model.CostosOperativosSvc.getMes()
                    );
                    
                }
                
                else if ( Opcion.equals("Guardar Cambios") ){
                    String Mensaje = "Cambios efectuados con exito!!!!";
                    next = "/jsp/finanzas/presupuesto/CostosOperativos/Edicion.jsp?Mensaje=" + Mensaje;                   
                    model.CostosOperativosSvc.InsertCostosOperativos(Estandar, Ano, Mes, Costos, Valor, Usuario);
                    model.CostosOperativosSvc.BuscarElementosCostosOperativos(Estandar, Ano, Mes);
                    
                }

                else if ( Opcion.equals("Reiniciar") ){
                    model.CostosOperativosSvc.ReiniciarAll();
                    next = "/jsp/finanzas/presupuesto/CostosOperativos/BuscarPeriodo.jsp";                   
                }                
                
                else if ( Opcion.equals("Init") ){
                    model.CostosOperativosSvc.Init();
                    next = "/jsp/finanzas/presupuesto/CostosOperativos/BuscarPeriodo.jsp";                   
                }
                
                ////////////////////////////////////////////////////////////////
                // vistas de costos operativos
                else if ( Opcion.equals("InitParamsVista") ){                   
                        next = "/jsp/finanzas/presupuesto/CostosOperativos/ParametrosVista.jsp";                                      
                } 
                
                
                else if ( Opcion.equals("InitVista") ){
                    
                    next = "/jsp/finanzas/presupuesto/CostosOperativos/Vista"+ Vista.toUpperCase() +".jsp";
                    if (Vista.toUpperCase().equals("AG"))
                        model.CostosOperativosSvc.GenerarVistaAgencias(Ano, Mes, Integer.parseInt(Prolongacion));
                    else if (Vista.toUpperCase().equals("CT"))
                        model.CostosOperativosSvc.GenerarVistaCostos  (Ano, Mes, Integer.parseInt(Prolongacion), Agencia, NAgencia);
                    else if (Vista.toUpperCase().equals("CL"))
                        model.CostosOperativosSvc.GenerarVistaClientes(Ano, Mes, Integer.parseInt(Prolongacion), Agencia, NAgencia, Costo, NCosto);
                    else
                        next = "/jsp/finanzas/presupuesto/CostosOperativos/ParametrosVista.jsp?Mensaje=Tipo Vista no definida....";
                                       
                }              
                
                
                
            }
            
            
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if (rd == null)
                throw new ServletException("\nNo se pudo encontrar " + next);
            rd.forward(request, response);
            
        }catch (Exception ex){
            throw new ServletException("\nError en OpcionesCostosOperativosAction ...\n"+ex.getMessage());
        }
    }
    
    
    /* Procedimeto para obtener un valor del objeto request
     * y en caso de que este no exista se devuelve vacio en lugar
     * de null.
     * @autor mfontalvo
     * @params parameter .... Parametro a consultar
     * @return valor del objeto request.
     */
    
    private String defaultString (String parameter){
        return ( request.getParameter(parameter)!=null?request.getParameter(parameter):"" );
    }
    
    
    
}
