/*************************************************************
 * Nombre      ............... OpcionesPresupuestoAction.java
 * Descripcion ............... busca presupuesto de acuerdo a los parametros dados
 * Autor       ............... mfontalvo
 * Fecha       ............... mayo - 17 - 2005
 * Version     ............... 1.0
 * Copyright   ............... Fintravalores S.A. S.A
 ************************************************************/




package com.tsp.finanzas.presupuesto.controller;


import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.finanzas.presupuesto.model.threads.HExportarVistaPtoXLS;
import com.tsp.util.UtilFinanzas;





public class OpcionesPresupuestoAction extends Action {
    
    /** Creates a new instance of OpcionesPresupuestoAction */
    public OpcionesPresupuestoAction() {
    }
    
    
    
    /* Procedimiento que ejecuta la accion
     * parametros
     * @params request     ........  HttpServletRequest
     * @params response    ........  HttpServletResponse
     * @throws ServletException, IOException
     */
        
    public void run() throws javax.servlet.ServletException, java.io.IOException {
        try{
            
            /**
             * Obtenemos el model de flujo de la session actual
             * para sacar el login del usuario
             **/
            
            HttpSession session = request.getSession();
            Usuario     usuario = (Usuario)session.getAttribute("Usuario");
            if (usuario==null) throw new Exception("Usuario no encontrado en session");
            
            
            com.tsp.operation.model.Model model1 = (com.tsp.operation.model.Model) session.getAttribute("model");
            String Mensaje = "";
            String Opcion  = getParameter("Opcion","");
            String Origen  = getParameter("Origen","");
            ///////////////////////////////////////////////////
            String Tipo     = getParameter("TipoVista","");
            String Distrito = getParameter("Distrito","");
            String Stdjob   = getParameter("Stdjob","");
            String sOrigen  = getParameter("sOrigen","");
            String sDestino = getParameter("sDestino","");
            String Cliente  = getParameter("Cliente","");
            String Agencia  = getParameter("Agencia","");
            String Ano      = getParameter("Ano","");
            String Mes      = getParameter("Mes","");
            String Filtro   = getParameter("Filtro","");
            String Mov      = getParameter("Mov","");
            String diaInicial = getParameter("diaInicial","01");
            String diaFinal   = getParameter("diaFinal","31");
            String AgenciaDespacho  = getParameter("AgenciaDespacho","");
            
            ///////////////////////////////////////////////////////////
            String [] Viajes    = request.getParameterValues("Viajes");
            String [] CViajes   = request.getParameterValues("CViajes");
            String [] HViajes   = request.getParameterValues("HViajes");
            String Total        = request.getParameter("Total");
            ///////////////////////////////////////////////////////////
            String next ="/jsp/finanzas/presupuesto/PtoListado.jsp?Mensaje="+ Mensaje;
            
            
            if (Opcion!=null){
                // opciones de paginacion
                if(Opcion.equals("Paginar"))
                    model.DPtoSvc.paginacion.Movimiento(Mov);
                if(Opcion.equals("Paginacion"))
                    model.DPtoSvc.paginacion.setVista_Actual(Integer.parseInt(request.getParameter("Vista")));
                
                // busqueda del presupuesto
                if (Opcion.equals("Ver Presupuesto")){
                    
                            
                            
                    model.DPtoSvc.ObtenerListado(
                            Tipo, 
                            Ano, 
                            Mes, 
                            (!Stdjob.equals  ("")?Stdjob  :"%"), 
                            (!Cliente.equals ("")?Cliente :"%"), 
                            (!Agencia.equals ("")?Agencia :"%"), 
                            (!sOrigen.equals ("")?sOrigen :"%"), 
                            (!sDestino.equals("")?sDestino:"%"),
                            (!AgenciaDespacho.equals("")?AgenciaDespacho:"%"),
                            false, 
                            Filtro,
                            diaInicial,
                            diaFinal
                        );
                    model.DPtoSvc.BuscarTasa(Ano);
                    model.DPtoSvc.ConstruirVarJSTasa();
                }
                
                // mostrar la vista calendario
                else if (Opcion.equals("ShowCalendar")){
                    model.DPtoSvc.Buscar(Ano, Mes, Stdjob, Cliente, Agencia);
                    next ="/jsp/finanzas/presupuesto/PtoCalendario.jsp?Mensaje="+Mensaje;
                }
                
                // graba el pto desde la vista calendario
                else if (Opcion.equals("GrabarCalendario") ){
                    if (verificarGrabacion(Viajes, CViajes)){
                        String fechaActualizacion =
                        model.DPtoSvc.INSERT_DIARIO( Distrito,  Ano, Mes, Stdjob, Agencia, Cliente, Total, Viajes, usuario.getLogin());
                        model.DPtoSvc.Buscar(Ano, Mes, Stdjob, Cliente, Agencia);
                        GrabarReprogramaciones(usuario.getLogin(), fechaActualizacion);
                    }
                    next ="/jsp/finanzas/presupuesto/PtoCalendario.jsp?Mensaje="+Mensaje;
                }
                
                
                // anular viaje desde el calendario
                else if (Opcion.equals("AnularCalendario") ){
                    model.DPtoSvc.ANULAR(Distrito, Stdjob, Ano, Mes);
                    model.DPtoSvc.Buscar(Ano, Mes, Stdjob, Cliente, Agencia);
                    Mensaje = "Ultimo viaje anulado";
                    next    = "/jsp/finanzas/presupuesto/PtoCalendario.jsp?Mensaje="+Mensaje;
                }
                
                // graba el presupuesto desde la vista del listado (mensual, semanal, diaria)
                else if (Opcion.equals("Grabar") && verificarGrabacion(Viajes, CViajes) ){
                    if (model.DPtoSvc.getValor("Tipo").equals("D"))
                        model.DPtoSvc.INSERT_DIARIO( Distrito,  Ano, Mes, Stdjob, Agencia, Cliente, Total, Viajes, usuario.getLogin());
                    else if (model.DPtoSvc.getValor("Tipo").equals("S"))
                        model.DPtoSvc.INSERT_SEMANAL( Distrito,  Ano, Mes, Stdjob, Agencia, Cliente, Total, Viajes, usuario.getLogin());
                    else if (model.DPtoSvc.getValor("Tipo").equals("M")){
                        for (int i=0;i<CViajes.length;i++ )
                            if (!CViajes[i].trim().equals(Viajes[i].trim()))
                                model.DPtoSvc.INSERT_MENSUAL(Distrito,  Ano, (model.DPtoSvc.getValor("Mes").equals("TD")? UtilFinanzas.DiaFormat( i+1 ) : model.DPtoSvc.getValor("Mes"))  , Stdjob, Agencia, Cliente, Viajes[i].trim(), usuario.getLogin());
                    }
                    
                    model.DPtoSvc.ObtenerListado(
                            model.DPtoSvc.getValor("Tipo"), 
                            model.DPtoSvc.getValor("Ano"), 
                            model.DPtoSvc.getValor("Mes"), 
                            model.DPtoSvc.getValor("Stdjob"), 
                            model.DPtoSvc.getValor("Cliente"), 
                            model.DPtoSvc.getValor("Agencia"), 
                            model.DPtoSvc.getValor("Origen"), 
                            model.DPtoSvc.getValor("Destino"), 
                            model.DPtoSvc.getValor("AgenciaDespacho"), 
                            true ,
                            model.DPtoSvc.getValor("Filtro"), 
                            model.DPtoSvc.getValor("diaInicial"), 
                            model.DPtoSvc.getValor("diaFinal") 
                        );
                }
                
                // refresca el listado principal
                else if (Opcion.equals("Refrescar"))
                    model.DPtoSvc.ObtenerListado(
                             model.DPtoSvc.getValor("Tipo"), 
                             model.DPtoSvc.getValor("Ano"), 
                             model.DPtoSvc.getValor("Mes"), 
                             model.DPtoSvc.getValor("Stdjob"), 
                             model.DPtoSvc.getValor("Cliente"), 
                             model.DPtoSvc.getValor("Agencia"), 
                             model.DPtoSvc.getValor("Origen"), 
                             model.DPtoSvc.getValor("Destino"), 
                             model.DPtoSvc.getValor("AgenciaDespacho"), 
                             false ,
                             model.DPtoSvc.getValor("Filtro"),
                             model.DPtoSvc.getValor("diaInicial"), 
                             model.DPtoSvc.getValor("diaFinal") 
                         );
                
                
                // consulta el historial de un estandar y lo lleva a la vista historial modo normal
                else if (Opcion.equals("Historial")){
                    model.DPtoSvc.BuscarHistorial(Distrito, Stdjob, Ano, Mes);
                    model.ReprogamacionSvc.SEARCH_VIAJE_REPROGRAMADO(Stdjob, Ano, Mes);
                    
                    next = "/jsp/finanzas/presupuesto/PtoHistorial.jsp";
                }
                
                
                // consulta el historia de un estandar y lo lleva a la vista historial modo edicion
                else if (Opcion.equals("HistorialEdicion")){
                    model.DPtoSvc.BuscarHistorial(Distrito, Stdjob, Ano, Mes);
                    model.ReprogamacionSvc.SEARCH_VIAJE_REPROGRAMADO(Stdjob, Ano, Mes);
                    
                    next = "/jsp/finanzas/presupuesto/PtoHistorialEdicion.jsp";
                }
                
                
                
                // graba el presupuesto desde la vista Hitorial modo Edicion
                else if (Opcion.equals("GrabarHistorial")){
                    if (verificarGrabacion(Viajes, CViajes)){
                        model.DPtoSvc.INSERT_DIARIO( Distrito, Ano, Mes, Stdjob, Agencia, Cliente, Total, Viajes, usuario.getLogin());
                        
                        model.DPtoSvc.BuscarHistorial(Distrito, Stdjob, Ano, Mes);
                        model.ReprogamacionSvc.SEARCH_VIAJE_REPROGRAMADO(Stdjob, Ano, Mes);
                        
                    }
                    next = "/jsp/finanzas/presupuesto/PtoHistorialEdicion.jsp";
                }
                
                // graba el presupuesto desde la vista Hitorial modo Edicion
                else if (Opcion.equals("Exportar")){
                    
                    HExportarVistaPtoXLS h = new HExportarVistaPtoXLS();
                    h.start(model1, model, usuario.getLogin());
                    next ="/jsp/finanzas/presupuesto/PtoListado.jsp?Mensaje=Proceso Iniciado correctamente.";
                    
                }                
                
                
                
            }
            
            if (Origen!=null && !Origen.equals("")) model.DPtoSvc.setOrigen(Origen);
            
            
            // reenvia hacia la pagina
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if(rd == null)
                throw new ServletException("No se pudo encontrar "+ next);
            rd.forward(request, response);
        }catch(Exception e){
            throw new ServletException("Error en OpcionesPresupuestoAction .....\n"+e.getMessage());
        }
    }
    
    
    /* Funcion que inidica si se debe grabar o no el presupuesto
     * criterio son los viajes anterios contra los nuevos
     * @autor mfontalvo
     * @params Viajes ..... Nuevos Viajes
     * @params CViajes .... Viajes Anteriores
     * @return boolean que indica si se puede grabar o no el nuevo pto
     * @throws Exception.
     */
    
    
    public boolean verificarGrabacion(String []Viajes, String []CViajes) throws Exception{
        boolean grabar = false;
        try{
            if (Viajes!=null){
                for (int i=0;i<Viajes.length && !grabar ;i++)
                    if (!Viajes[i].equals(CViajes[i]))
                        grabar = true;
            }
        }catch (Exception ex){
            throw new Exception("Error en verificarGrabacion : "+ ex.getMessage());
        }
        return grabar;
    }
    
    
    /* Funcion para leer un parametro del mobjeto request
     * @autor mfontalvo
     * @params str1 ......... parametro a leer
     * @params str2 ......... devuelve este parametro si str1 es null
     * @return el parametro del objeto request
     */
    
    public String getParameter(String str1, String str2){
        return (request.getParameter(str1)==null?str2:request.getParameter(str1));
    }
    
    
    
    
    /**
     * procedimeniento para poder grabar los codigos de reprogramacion
     * asociados a al viaje del estandar
     * @autor mfontalvo
     * @params Usuario  ........ usuario en session
     * @params FechaGrabacion .. Fecha a la que se le asignara la reprogramacion
     **/
    
    public void GrabarReprogramaciones(String Usuario, String FechaGrabacion) throws Exception {
        try{
            String Estandar  = getParameter("Stdjob","");
            String [] RPLOV = request.getParameterValues("RPLOV"); // codigos de reprogramacion
            String [] RPFEC = request.getParameterValues("RPFEC"); // lista de fechas
            String [] RPVNU = request.getParameterValues("RPVNU"); // valores nuevos
            String [] RPVAN = request.getParameterValues("RPVAN"); // valores viejos
            
            for (int i=0; i<RPLOV.length ; i++){
                model.ReprogamacionSvc.INSERT_VIAJE_REPROGRAMADO(Estandar, RPFEC[i], RPLOV[i], RPVAN[i], RPVNU[i], Usuario, FechaGrabacion);
            }
            
        }
        catch (Exception ex){
            throw new Exception("Error en la rutina GrabarReprogramaciones [Action]...\n" + ex.getMessage());
        }
    }
}
