
/*************************************************************
 * Nombre      ............... OpcionesReprogramacionAction.java
 * Descripcion ............... Codigos de reprogramacion
 * Autor       ............... mfontalvo
 * Fecha       ............... Noviembre - 21 - 2005
 * Version     ............... 1.0
 * Copyright   ............... Fintravalores S.A. S.A
 ************************************************************/


package com.tsp.finanzas.presupuesto.controller;



import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.Usuario;

public class OpcionesReprogramacionAction extends Action {
    
    /** Creates a new instance of OpcionesReprogramacionAction */
    public OpcionesReprogramacionAction() {
    }
    
    
    
    /* Procedimiento que ejecuta la accion
     * parametros
     * @params request     ........  HttpServletRequest
     * @params response    ........  HttpServletResponse
     * @throws ServletException, IOException
     */
    
    
    public void run() throws javax.servlet.ServletException, java.io.IOException {
        try{
            
            HttpSession session = request.getSession();
            Usuario     usuario = (Usuario)session.getAttribute("Usuario");
            if (usuario==null) throw new Exception("Usuario no encontrado en session");
            
            
            String Opcion      = request.getParameter("Opcion");
            String Codigo      = request.getParameter("Codigo");
            String Descripcion = request.getParameter("Descripcion");
            String LOV[]       = request.getParameterValues("LOV");
            String Mensaje     = "";
            
            if (Opcion != null){
                
                if (Opcion.equals("aceptar")){
                    ////////////////////////////////////////////////////////////////
                    model.ReprogamacionSvc.SEARCH(Codigo);
                    if (model.ReprogamacionSvc.getDato()==null){
                        model.ReprogamacionSvc.INSERT(Codigo, Descripcion, usuario.getLogin());
                        Mensaje = "Registro grabado satisfactoriamente";
                    }
                    else
                        Mensaje = "Este codigo de reprogramacion ya existe";
                    ////////////////////////////////////////////////////////////////
                }
                else if (Opcion.equals("modificar")){
                    model.ReprogamacionSvc.UPDATE(Codigo, Descripcion, usuario.getLogin());
                    model.ReprogamacionSvc.SEARCH(Codigo);
                    Mensaje = "Registro modificado satisfactoriamente";
                }
                else if (Opcion.equals("Eliminar")){
                    model.ReprogamacionSvc.ReiniciarDato();
                    for(int i=0;i<LOV.length;i++)
                        model.ReprogamacionSvc.DELETE(LOV[i]);
                }
                else if (Opcion.equals("Anular")){
                    model.ReprogamacionSvc.ReiniciarDato();
                    for(int i=0;i<LOV.length;i++)
                        model.ReprogamacionSvc.UPDATE_STATE(LOV[i],"A", usuario.getLogin());
                }
                else if (Opcion.equals("Activar")){
                    model.ReprogamacionSvc.ReiniciarDato();
                    for(int i=0;i<LOV.length;i++)
                        model.ReprogamacionSvc.UPDATE_STATE(LOV[i],"", usuario.getLogin());
                }
                else if (Opcion.equals("Nuevo"))
                    model.ReprogamacionSvc.ReiniciarDato();
                
                else if (Opcion.equals("Seleccionar"))
                    model.ReprogamacionSvc.SEARCH(Codigo);
                
                else if (Opcion.equals("Ocultar Lista"))
                    model.ReprogamacionSvc.ReiniciarLista();
                
                
                
                if (Opcion.equals("Listado") || model.ReprogamacionSvc.getListaReprogramaciones()!=null)
                    model.ReprogamacionSvc.SEARCH_ALL();

                
            }
            
            
            
            String next = "/jsp/finanzas/presupuesto/Reprogramaciones.jsp?Mensaje=" + Mensaje;
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if (rd == null)
                throw new ServletException("\nNo se pudo encontrar " + next);
            rd.forward(request, response);
            
        }catch (Exception ex){
            throw new ServletException("\nError en OpcionesReprogramacionAction ...\n"+ex.getMessage());
        }
        
    }
    
}
