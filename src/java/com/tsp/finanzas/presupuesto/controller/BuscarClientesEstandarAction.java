/*************************************************************
 * Nombre      ............... BuscarClientesEstandarAction.java
 * Descripcion ............... Busca la agencia y sus estandares para un cliente
 * Autor       ............... mfontalvo
 * Fecha       ............... Abril - 14 - 2005
 * Version     ............... 1.0
 * Copyright   ............... Fintravalores S.A. S.A
 ************************************************************/



package com.tsp.finanzas.presupuesto.controller;



import javax.servlet.*;
import javax.servlet.http.*;





public class BuscarClientesEstandarAction extends Action  {
    
    /** Creates a new instance of BuscarClientesEstandarAction */
    public BuscarClientesEstandarAction() {
    }
    
    
    
/* Procedimiento que ejecuta la accion
 * parametros
 * @params request     ........  HttpServletRequest
 * @params response    ........  HttpServletResponse
 * @throws ServletException, IOException
 */
    
    
    public void run() throws javax.servlet.ServletException, java.io.IOException {
        try{
            String Opcion  = request.getParameter("Opcion");
            String Cliente = request.getParameter("Cliente");
            
            if (Opcion!=null){
                if (Opcion.equals("Buscar"))
                    model.DPtoSvc.BuscarAgenciaClienteStandar("%", Cliente, 1);
            }
            

            
            final String next = "/jsp/finanzas/presupuesto/PtoFormClientes.jsp";
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if(rd == null)
                throw new Exception("No se pudo encontrar "+ next);
            rd.forward(request, response);
        }
        catch(Exception e){
            throw new ServletException("Error en  : BuscarClientesEstandarAction  ....\n" + e.getMessage());
        }
    }
    
}
