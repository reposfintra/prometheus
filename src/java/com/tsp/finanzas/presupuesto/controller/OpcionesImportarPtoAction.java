/*************************************************************
 * Nombre      ............... OpcionesImportarPtoAction.java
 * Descripcion ............... Importa el presupuesto
 * Autor       ............... mfontalvo
 * Fecha       ............... mayo - 17 - 2005
 * Version     ............... 1.0
 * Copyright   ............... Fintravalores S.A. S.A
 ************************************************************/




package com.tsp.finanzas.presupuesto.controller;





import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.finanzas.presupuesto.model.threads.*;

import com.tsp.operation.model.beans.Usuario;
import com.tsp.finanzas.presupuesto.model.beans.*;
import com.tsp.util.*;
import java.util.*;



public class OpcionesImportarPtoAction extends Action {
    
    /** Creates a new instance of OpcionesImportarPtoAction */
    public OpcionesImportarPtoAction() {
    }
    
    
    
/* Procedimiento que ejecuta la accion
 * parametros
 * @params request     ........  HttpServletRequest
 * @params response    ........  HttpServletResponse
 * @throws ServletException, IOException
 */
    public void run() throws ServletException, java.io.IOException {
        try{
            String Mensaje = "";
            
           
            HttpSession session = request.getSession();
            Usuario     usuario = (Usuario)session.getAttribute("Usuario");
            if (usuario==null) throw new Exception("Usuario no encontrado en session");
            String ruta = UtilFinanzas.obtenerRuta("ruta", "/exportar/migracion/" + usuario.getLogin());
            
            
            Upload  upload = new Upload(ruta ,request);
            upload.load();
            List lista        = upload.getFilesName();
            Dictionary fields = upload.getFields();
            
            String Opcion     = fields.get("Opcion").toString();
            String Ano        = fields.get("Ano").toString();
            String Mes        = fields.get("Mes").toString();
            String Modo       = fields.get("Modo").toString();
            String Formato    = fields.get("Formato").toString();
            
            String Accion    = fields.get("Accion").toString();
            
            
            if (Opcion!=null){
                if (Opcion.equals("Importar")){
                    
                    String filename   = lista.get(0).toString();
                    ////System.out.println(ruta + "/" + filename );
                    
                    if (Formato.equals("Original")){
                        HImportarXLS Hilo = new HImportarXLS();
                        Hilo.start(ruta + "/" + filename , Ano, Mes, Modo, Accion, usuario);
                    }else{
                        HImportarPresupuestoXLS Hilo = new HImportarPresupuestoXLS();
                        Hilo.start(ruta + "/" + filename , Ano, Mes, Modo, Accion, usuario);
                    }
                    Mensaje = "Su proceso ha sido iniciado correctamente";
                }
            }
            
            
            
            final String next = "/jsp/finanzas/presupuesto/PtoExcel.jsp?Mensaje="+Mensaje;
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if (rd==null)
                throw new Exception("No se pudo encontrar "+next);
            rd.forward(request, response);
        }catch (Exception ex){
            throw new ServletException("Error en OpcionesImportarPtoAction : "+ex.getMessage());
        }
    }
    
}
