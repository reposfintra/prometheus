
/*************************************************************
 * Nombre      ............... Action.java
 * Descripcion ............... Define varibles de servlet
 * Autor       ............... fvillacob
 * Fecha       ............... Abril - 14 - 2005
 * Version     ............... 1.0
 * Copyright   ............... Fintravalores S.A. S.A
 ************************************************************/




package com.tsp.finanzas.presupuesto.controller;



import com.tsp.finanzas.presupuesto.model.*;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;



public abstract class Action {
    
    protected HttpServletRequest  request;    
    protected HttpServletResponse response;    
    protected ServletContext      application; 
    protected Model               model;
    
    
    public Action() {
    }
    
    public abstract void run() throws ServletException, IOException;
    
    
    
    
    /* Procedimiento que setea el request
     * parametros
     * @params request    ........ El objeto HttpServletRequest
     */
    public void setRequest(HttpServletRequest request){
        this.request = request;
    }
    
    
    
    /* Procedimiento que setea el response
     * parametros
     * @params response    ........ El objeto HttpServletResponse
     */
    public void setResponse(HttpServletResponse response){
        this.response = response;
    }
    
    
    /* Procedimiento que setea el application
     * parametros
     * @params application    ........ El objeto ServletContext
     */
    public void setApplication(ServletContext application){
        this.application = application;
    }    
    
    
    /* Procedimiento que setea el model
     * parametros
     * @params model    ........ El Model
     */
    public void setModel(Model model){
        this.model = model;
    }
    
    
}
