/*************************************************************
 * Nombre      ............... OpcionesExportarPtoAction.java
 * Descripcion ............... Exporta informacion del presupuesto
 * Autor       ............... mfontalvo
 * Fecha       ............... mayo - 17 - 2005
 * Version     ............... 1.0
 * Copyright   ............... Fintravalores S.A. S.A
 ************************************************************/




package com.tsp.finanzas.presupuesto.controller;




import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.finanzas.presupuesto.model.threads.HExportarPresupuestoXLS;
import com.tsp.operation.model.beans.*;




public class OpcionesExportarPtoAction extends Action {
    
    /** Creates a new instance of OpcionesExportarPresupuesto */
    public OpcionesExportarPtoAction() {
    }
    
    
    /* Procedimiento que ejecuta la accion
     * parametros
     * @params request     ........  HttpServletRequest
     * @params response    ........  HttpServletResponse
     * @throws ServletException, IOException
     */
    
    public void run() throws javax.servlet.ServletException, java.io.IOException {
        try{


            HttpSession session = request.getSession();
            Usuario     usuario = (Usuario)session.getAttribute("Usuario");
            if (usuario==null) throw new Exception("Usuario no encontrado en session");            
            
            String Ano     = request.getParameter("Ano");
            String Mes     = request.getParameter("Mes");
            String Opcion  = request.getParameter("Opcion");
            String Mensaje = "";
            
            
            
            if (Opcion!=null){
                if (Opcion.equals("Exportar")){
                    HExportarPresupuestoXLS hilo = new HExportarPresupuestoXLS();
                    hilo.start(Ano, Mes,  usuario );
                    Mensaje = "Su operacion ha iniciado con exito ";
                }
            }
            
            final String next = "/jsp/finanzas/presupuesto/ExportarPto.jsp?Mensaje=" + Mensaje;
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if (rd == null)
                throw new ServletException("No se pudo encontrar "+ next);
            rd.forward(request, response);            
        }catch (Exception ex){
            throw new ServletException("Error en OpcionesExportarPtoAction .....\n"+ex.getMessage());
        }
    }
    
    
    
    
    
}
