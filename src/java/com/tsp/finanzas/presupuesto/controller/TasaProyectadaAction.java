/*************************************************************
 * Nombre      ............... TasaProyectadaAction.java
 * Descripcion ............... maneja la  tasa
 * Autor       ............... fvillacob
 * Fecha       ............... mayo - 17 - 2005
 * Version     ............... 1.0
 * Copyright   ............... Fintravalores S.A. S.A
 ************************************************************/




package com.tsp.finanzas.presupuesto.controller;




import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.finanzas.presupuesto.model.beans.*;




public class TasaProyectadaAction extends Action{
    
  
    
      
/* Procedimiento que ejecuta la accion
 * parametros
 * @params request     ........  HttpServletRequest
 * @params response    ........  HttpServletResponse
 * @throws ServletException, IOException
 */
    public void run() throws ServletException, IOException {
        try{
           String opcion = request.getParameter("opcion");
           String user   = request.getParameter("usuario");
           String comentario="";
           model.TasaSvc.create();
           if(opcion!=null){
              if(opcion.equals("NUEVO")){
                 model.TasaSvc.reset();
              }   
              if(opcion.equals("GUARDAR")){
                try{
                 String ano = request.getParameter("ano");
                 String mes = request.getParameter("mes");
                 double dol = Double.parseDouble( request.getParameter("dol"));
                 double bol = Double.parseDouble( request.getParameter("bol"));
                 double dtf = Double.parseDouble( request.getParameter("dtf"));                 
                 comentario = model.TasaSvc.insert(ano, mes, dol, bol, dtf, user);
                 List lista = model.TasaSvc.search(ano,mes);
                }catch(Exception e){ comentario="Error: "+ e.getMessage();}
              }   
              if(opcion.equals("LISTAR")){
                 String ano = request.getParameter("ano");
                 String mes = request.getParameter("mes");
                 List lista = model.TasaSvc.search(ano,mes);
              } 
              
              
              if(opcion.equals("ELIMINAR")){
                  int total        = Integer.parseInt(request.getParameter("total"));
                  ////System.out.println(total);
                   if(total>0 ){
                      for(int i =1;i<=total;i++){
                          int parametro =  Integer.parseInt(request.getParameter("parametro"+i));
                          Tasa  obj     =  model.TasaSvc.getTasa(parametro);
                          comentario   +=  model.TasaSvc.delete(obj.getAno(),obj.getMes());
                      }
                      model.TasaSvc.search( model.TasaSvc.getAno(),"ALL");
                   }
               }
               
              if(opcion.equals("MODIFICAR")){
                 try{
                  int id     = Integer.parseInt(request.getParameter("objeto"));
                  double dol = Double.parseDouble( request.getParameter("dol"));
                  double bol = Double.parseDouble( request.getParameter("bol"));
                  double dtf = Double.parseDouble( request.getParameter("dtf"));                 
                  Tasa  obj  =  model.TasaSvc.getTasa(id);
                  comentario = model.TasaSvc.update(obj.getAno(), obj.getMes(), dol, bol, dtf, user);
                  model.TasaSvc.search( model.TasaSvc.getAno(),"ALL");
                 }catch(Exception e){ comentario="Error: "+e.getMessage();}
              }
              
           }
           
           final String next = "/jsp/finanzas/presupuesto/TasaProyectada.jsp?comentario="+comentario;
           RequestDispatcher rd = application.getRequestDispatcher(next);
           if(rd == null)
              throw new Exception("No se pudo encontrar "+ next);
           rd.forward(request, response); 
            
        }catch(Exception e){
            throw new ServletException(e.getMessage());
        }
    }
    
}
