/*************************************************************
 * Nombre      ............... OpcionesReportesAction.java
 * Descripcion ............... Realiza Reportes
 * Autor       ............... mfontalvo
 * Fecha       ............... mayo - 17 - 2005
 * Version     ............... 1.0
 * Copyright   ............... Fintravalores S.A. S.A
 ************************************************************/



package com.tsp.finanzas.presupuesto.controller;



import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.finanzas.presupuesto.model.threads.*;
import com.tsp.operation.model.beans.*;


public class OpcionesReportesAction extends Action {
    
    
    /** Creates a new instance of OpcionesReportesAction */
    public OpcionesReportesAction() {
    }
    
    
    
    
    /* Procedimiento que ejecuta la accion
     * parametros
     * @params request     ........  HttpServletRequest
     * @params response    ........  HttpServletResponse
     * @throws ServletException, IOException
     */
    
    public void run() throws javax.servlet.ServletException, java.io.IOException {
        try{
            String Opcion              = coalesce("Opcion","FINV");
            String Distrito            = coalesce("Distrito","");
            String Agencia             = coalesce("Agencia","");
            String AgenciaDespacho     = coalesce("AgenciaDespacho","");
            String Cliente             = coalesce("Cliente","");
            String Estandar            = coalesce("Estandar","");
            String Ano                 = coalesce("Ano","");
            String Mes                 = coalesce("Mes","");
            String Tipo                = coalesce("Tipo","");
            String tipoReporte         = coalesce("tipoReporte","");
            String tipoBusqueda        = coalesce("tipoBusqueda","");
            int diaInicial             = Integer.parseInt( coalesce("diaInicial","01"));
            int diaFinal               = Integer.parseInt( coalesce("diaFinal"  ,"31"));
            
            String next = "/jsp/finanzas/presupuesto/ReportesPto.jsp";
            //JEscandon 27-01-06
            String Mensaje = "";
            
            HttpSession Session           = request.getSession();
            Usuario user                  = new Usuario();
            user                          = (Usuario)Session.getAttribute("Usuario");
            
            if (Tipo!=null){
                if (Tipo.equals("0")) next = "/jsp/finanzas/presupuesto/ReportesAgencia.jsp";
                if (Tipo.equals("1")) next = "/jsp/finanzas/presupuesto/ReportesCliente.jsp";
                if (Tipo.equals("2")) next = "/jsp/finanzas/presupuesto/ReportesStandar.jsp";
                
                /*JEscandon 27-01-06*/
                if (Tipo.equals("3")) next = "/jsp/finanzas/presupuesto/PresupuestoCostosOperativos/ReportesStandar.jsp";                
                if (Tipo.equals("4")) next = "/jsp/finanzas/presupuesto/PresupuestoCostosOperativos/ReportesCostosOperativos.jsp";
                if (Tipo.equals("5")){
                    Mensaje = "El proceso se ha iniciado";
                    Opcion = "GenerarExcel";
                    next = "/jsp/finanzas/presupuesto/PresupuestoCostosOperativos/ReportesCostos.jsp?Mensaje="+Mensaje;
                }
                if (Tipo.equals("6")) next = "/jsp/finanzas/presupuesto/PresupuestoCostosOperativos/ReportesVs.jsp";
                if (Tipo.equals("7")){
                    Mensaje = "El proceso se ha iniciado";
                    Opcion = "GenerarExcelVs"; 
                    next = "/jsp/finanzas/presupuesto/PresupuestoCostosOperativos/CostosEjecVsPdo.jsp?Mensaje="+Mensaje;
                }
                if (Tipo.equals("8")) {
                    Mensaje = "El proceso se ha iniciado";
                    next = "/jsp/finanzas/presupuesto/PresupuestoCostosOperativos/ReportesVs.jsp?Mensaje="+Mensaje;
                }
                if (Tipo.equals("9")) {
                    Mensaje = "El proceso se ha iniciado";
                    next = "/jsp/finanzas/presupuesto/PresupuestoCostosOperativos/ReportesStandar.jsp?Mensaje="+Mensaje;
                }
            }
            
            
            if (Opcion!=null){
                if (Opcion.equals("Generar")){
                    model.ReportesSvc.Presupuestado_VS_Ejecutado(Distrito, AgenciaDespacho, Agencia , Cliente, Estandar, Ano, Mes, diaInicial, diaFinal);
                }
                else if (Opcion.equals("Exportar")){
                    HExportarReportePTvsEJ h = new HExportarReportePTvsEJ();
                    h.start( user, Ano , Mes, Distrito, AgenciaDespacho,  Agencia, Cliente, Estandar, diaInicial, diaFinal, tipoReporte );   
                    request.setAttribute("msg","Reporte inicializado, verifique el Archivo generado en su directorio de archivos WEB");
                }
                
                /*JEscandon 27-01-06*/
                if (Opcion.equals("GenerarCostos")){
                    model.ReportesSvc.PresupuestoCostosOperativos(Distrito, Agencia, Cliente, Estandar, Ano, Mes);
                }
                if (Opcion.equals("GenerarExcel")){
                    PresuspuestoCostosOperativos hilo = new PresuspuestoCostosOperativos();
                    model.ReportesSvc.PresupuestoCostosOperativos(Distrito, Agencia, Cliente, Estandar, Ano, Mes);
                    hilo.start(model.ReportesSvc.getListado(), user.getLogin(), Ano, Mes,Agencia, Cliente);
                }
                if (Opcion.equals("Comparacion")){
                    model.ReportesSvc.CostosOperativosEjecVsPdo(Distrito, Agencia, Cliente, Estandar, Ano, Mes);
                }
                if (Opcion.equals("GenerarExcelVs")){
                    CostosOperativosEjVsPto hilo = new CostosOperativosEjVsPto();
                    model.ReportesSvc.CostosOperativosEjecVsPdo(Distrito, Agencia, Cliente, Estandar, Ano, Mes);
                    hilo.start(model.ReportesSvc.getListado(), user.getLogin(), Ano, Mes,Agencia, Cliente);
                }
                
            }
            
            
            
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if (rd == null)
                throw new ServletException("\nNo se pudo encontrar " + next);
            rd.forward(request, response);
            
        }catch (Exception ex){
            throw new ServletException("\nError en OpcionesReporteAction ...\n"+ex.getMessage());
        }
    }
    
    public String coalesce(String str1, String str2){
        return (request.getParameter(str1)==null?str2:request.getParameter(str1));
    }
    
}
