/********************************************************************
 *      Nombre Clase.................   PtoGastosAdminSearchAction.java
 *      Descripci�n..................   Realiza una b�squeda por filtros espec�ficos en el archivo pto_gastos_admin
 *      Autor........................   Ing. Tito Andr�s Maturana
 *      Fecha........................   08.02.2006
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/
package com.tsp.finanzas.presupuesto.controller;

import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.finanzas.presupuesto.model.beans.*;
import com.tsp.finanzas.presupuesto.model.*;
import com.tsp.operation.model.threads.*;
import org.apache.log4j.Logger;
/**
 *
 * @author  Andres
 */
public class PtoGastosAdminSearchAction extends Action{
    
    /** Creates a new instance of PosBancariaRefreshAction */
    public PtoGastosAdminSearchAction() {
    }
    
    public void run() throws ServletException, java.io.IOException {
        
        String next = "/jsp/finanzas/presupuesto/gastos_admin/PtoGastosAdminBuscar.jsp?msg=";
        
        String agencia = request.getParameter("agencia");
        String elemento = request.getParameter("elemento");
        String unidad = request.getParameter("unidad");
        String area = request.getParameter("area");
        String ano = request.getParameter("ano");
        String dstrct = request.getParameter("distrito");
        String vlrs = request.getParameter("valores");
        String tcuenta = request.getParameter("clase");
        
        try{
            if( request.getParameter("detalles")!=null ){
                /* Se realiza la consulta general, sin filtros */
                dstrct = "";
                tcuenta = "";
                agencia = "";
                unidad = "";
                area = "";
                elemento = "";
                ano = "";
            }
            
            model.presupuesto_gadminSvc.listar(dstrct, tcuenta, agencia, unidad, area, elemento, ano);
            Vector vector = model.presupuesto_gadminSvc.getVector();
            
            if( vector.size()==0 ){
                next += "La b�squeda no produjo ning�n resultado.";
            } else {
                next = "/jsp/finanzas/presupuesto/gastos_admin/PtoGastosAdminListar.jsp?vlrs=" + vlrs;
                request.setAttribute("vector", vector);
            }
            
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if (rd==null)
                throw new Exception("No se pudo encontrar " + next);
            rd.forward(request, response);
        }catch (Exception e){
            e.printStackTrace();
            throw new ServletException(e.getMessage());
        }
    }
    
}
