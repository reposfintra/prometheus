/*************************************************************
 * Nombre      ............... OpcionesIncrementosPorcentualesAction.java
 * Descripcion ............... busca incrementos porcentuales
 * Autor       ............... mfontalvo
 * Fecha       ............... mayo - 17 - 2005
 * Version     ............... 1.0
 * Copyright   ............... Fintravalores S.A. S.A
 ************************************************************/




package com.tsp.finanzas.presupuesto.controller;

import com.tsp.operation.model.beans.*;
import javax.servlet.*;
import javax.servlet.http.*;




public class OpcionesIncrementosPorcentualesAction extends Action {
    
    /** Creates a new instance of OpcionesIncrementosPorcentualesAction */
    public OpcionesIncrementosPorcentualesAction() {
    }
    
    
    
/* Procedimiento que ejecuta la accion
 * parametros
 * @params request     ........  HttpServletRequest
 * @params response    ........  HttpServletResponse
 * @throws ServletException, IOException
 */
    public void run() throws javax.servlet.ServletException, java.io.IOException {
        try{
            
            HttpSession session = request.getSession();
            Usuario     usuario = (Usuario)session.getAttribute("Usuario");
            if (usuario==null) throw new Exception("Usuario no encontrado en session");            

            
            ////////////////////////////////////////////////////////////////
            String Mensaje = "";
            String Opcion  = request.getParameter("Opcion");
            ////////////////////////////////////////////////////////////////    
            String Estado        = request.getParameter("Estado");
            String Distrito      = request.getParameter("Distrito");
            String Ano           = request.getParameter("Ano");
            String CUnidad       = request.getParameter("CUnidad");
            String DUnidad       = request.getParameter("DUnidad");
            String PAnual        = request.getParameter("PAnual");
            String [] PMensuales = request.getParameterValues("PMensuales");
            String Origen        = (request.getParameter("Origen")!=null?request.getParameter("Origen"):"Formulario");
            String [] itemsSel   = (Origen.equals("Formulario")? new String [] {Distrito+"~"+Ano+"~"+CUnidad} :request.getParameterValues("itemsSel"));
            String TipoListado   = request.getParameter("TipoListado");
            /////////////////////////////////////////////////////////////////
            
            
            if (Opcion!=null){
                //////////////////////////////////////////////////////////////////////////////////////////////////
                if (Opcion.equals("Nuevo"))
                    model.IPorcentualesSvc.ReiniciarDato();
                else if (Opcion.equals("Buscar"))
                    model.IPorcentualesSvc.SEARCH_ONE(Distrito, Ano, CUnidad);
                else if (Opcion.equals("Mostrar"))
                    model.IPorcentualesSvc.SEARCH(Integer.parseInt(TipoListado),Ano);
                else if (Opcion.equals("Grabar"))
                    model.IPorcentualesSvc.INSERT(Distrito, Ano, CUnidad, DUnidad, PAnual, PMensuales, usuario.getLogin());
                else if (Opcion.equals("Modificar"))
                    model.IPorcentualesSvc.UPDATE(Distrito, Ano, CUnidad, DUnidad, PAnual, PMensuales, usuario.getLogin());
                else if (Opcion.equals("Eliminar"))
                    for (int i=0;i<itemsSel.length;i++){
                        String [] Parametros = itemsSel[i].split("~");
                        model.IPorcentualesSvc.DELETE(Parametros[0], Parametros[1], Parametros[2]);
                    }   
                else if (Opcion.equals("Anular") || Opcion.equals("Activar"))
                    for (int i=0;i<itemsSel.length;i++){
                        String [] Parametros = itemsSel[i].split("~");
                        model.IPorcentualesSvc.UPDATE_STATE(Parametros[0], Parametros[1], Parametros[2], (Opcion.equals("Anular")?"A":""), usuario.getLogin());
                    }                
                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                if (Opcion.equals("Modificar") || Opcion.equals("Eliminar") || Opcion.equals("Activar") || Opcion.equals("Anular") )
                    model.IPorcentualesSvc.SEARCH_ONE(Distrito, Ano, CUnidad);
                if (Opcion.equals("Grabar") || Opcion.equals("Modificar") || Opcion.equals("Eliminar") || Opcion.equals("Activar") || Opcion.equals("Anular"))
                    model.IPorcentualesSvc.SEARCH();
                
            }
            String next = "/jsp/finanzas/presupuesto/IncPorcentuales.jsp?Mensaje="+ Mensaje;;
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if(rd == null)
                throw new ServletException("No se pudo encontrar "+ next);
            rd.forward(request, response);   
        }catch(Exception e){
            throw new ServletException ("Error en OpcionesIncrementosPorcentualesAction .....\n"+e.getMessage());
        }        
        
    }
    
}
