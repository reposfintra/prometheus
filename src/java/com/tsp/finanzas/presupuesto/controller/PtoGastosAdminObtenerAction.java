/********************************************************************
 *      Nombre Clase.................   PtoGastosAdminObtenerAction.java
 *      Descripci�n..................   Realiza una b�squeda por filtros espec�ficos en el archivo pto_gastos_admin
 *      Autor........................   Ing. Tito Andr�s Maturana
 *      Fecha........................   08.02.2006
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/
package com.tsp.finanzas.presupuesto.controller;

import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.finanzas.presupuesto.model.beans.*;
import com.tsp.finanzas.presupuesto.model.*;
import com.tsp.operation.model.threads.*;
import org.apache.log4j.Logger;
/**
 *
 * @author  Andres
 */
public class PtoGastosAdminObtenerAction extends Action{
    
    /** Creates a new instance of PosBancariaRefreshAction */
    public PtoGastosAdminObtenerAction() {
    }
    
    public void run() throws ServletException, java.io.IOException {
        
        String next = "/jsp/finanzas/presupuesto/gastos_admin/PtoGastosAdminUpdate.jsp";
        
        String cuenta = request.getParameter("cuenta");
        String ano = request.getParameter("ano");
        String dstrct = request.getParameter("dstrct");
                
        try{
            model.presupuesto_gadminSvc.obtener(dstrct, cuenta, ano);
            PtoGastosAdmin p = model.presupuesto_gadminSvc.getPresupuesto();
            request.setAttribute("pto", p);
            
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if (rd==null)
                throw new Exception("No se pudo encontrar " + next);
            rd.forward(request, response);
        }catch (Exception e){
            e.printStackTrace();
            throw new ServletException(e.getMessage());
        }        
    }
    
}
