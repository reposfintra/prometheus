/*************************************************************
 * Nombre      ............... ControllerServlet.java
 * Descripcion ............... Define controller del sitio
 * Autor       ............... fvillacob
 * Fecha       ............... Abril - 14 - 2005
 * Version     ............... 1.0
 * Copyright   ............... Fintravalores S.A. S.A
 ************************************************************/




package com.tsp.finanzas.presupuesto.controller;




import com.tsp.exceptions.SessionExpiredException;
import com.tsp.finanzas.presupuesto.model.*;
import com.tsp.operation.model.beans.Usuario;
import java.io.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.*;
import javax.servlet.http.*;




/**
 *El componente controlador de la arquitectura MVC para el 
 *sistema de registros de entrega y envio de documentos
 */

public class ControllerServlet extends HttpServlet
{    
    
    
    
    /* Procedimiento que recibe la peticion
     * parametros
     * @params request     ........  HttpServletRequest
     * @params response    ........  HttpServletResponse
     * @throws ServletException, IOException
     */
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        doPost(request, response);
    }
       
    
    
    
    
    
    /* Procedimiento que ejecuta la peticion
     * parametros
     * @params request     ........  HttpServletRequest
     * @params response    ........  HttpServletResponse
     * @throws ServletException, IOException
     */
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        HttpSession session = request.getSession();
        Map actionMap = (Map) session.getAttribute("actionMapPto");
        if (actionMap == null) {
            actionMap = new HashMap();
            session.setAttribute("actionMapPto",actionMap);
        }
        ServletContext context = getServletContext();
        try 
        {    
            String pathInfo = request.getParameter("ACCION").trim();
            if (pathInfo == null )
                throw new ServletException
                ("Estado interno invalido - No hay informacion de ruta");
            
            Action action = (Action) actionMap.get(pathInfo);
            if (action == null) 
            {                
                StringTokenizer st = new StringTokenizer(pathInfo,"/");
                
                if (st.countTokens() != 2)
                    throw new ServletException
                    ("Estado interno invalido - No hay informacion de ruta ["
                    + pathInfo + "]");
                
                String state = st.nextToken();
                String event = st.nextToken(); 
                
                String className = "com.tsp.finanzas.presupuesto.controller." + state + event + "Action";
                ////System.out.println("Pto: "+className);
                
                try 
                {
                    Class actionClass = Class.forName(className);
                    action = (Action) actionClass.newInstance();
                }
                catch (ClassNotFoundException e) 
                {
                    throw new ServletException ("No se pudo cargar la clase " + className + "; " + e.getMessage());
                }
                catch (InstantiationException e) 
                {
                    throw new ServletException ("No se pudo crear un ejemplar de " + className + ": " + e.getMessage());
                }
                
                catch (IllegalAccessException e) {
                    throw new ServletException(className + ": " + e.getMessage());
                }                
                
                actionMap.put(pathInfo, action);
            }
                        
            Model model = (Model) session.getAttribute("modelpto");  
            if (model == null) {
                Usuario usuario = (Usuario) session.getAttribute("Usuario");
                if( usuario == null  ) {
                    throw new SessionExpiredException();
                }
                model = new Model(usuario.getBd());
                session.setAttribute("modelpto", model);
            }
            if (model == null)
                throw new ServletException("Modelo no encontrado en la sesion");
                        
            action.setRequest(request);
            action.setResponse(response);
            action.setApplication(context);            
            action.setModel(model);
            action.run();
        }
        catch (ServletException e) 
        {                        
            request.setAttribute("javax.servlet.jsp.jspException",e);
            RequestDispatcher rd = context.getRequestDispatcher("/error/ErrorPage.jsp");
            if (response.isCommitted())
                rd.include(request,response);
            else
                rd.forward(request,response);
        } catch (Exception ex) {
            Logger.getLogger(ControllerServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}