/*************************************************************
 * Nombre               Ref_viajeEventoAction.java
 * Descripcion          eventos de referencia de viaje
 * Autor                Diogenes Bastidas Morales
 * Fecha                5 de septiembre de 2006, 09:09 AM
 * Version              1.0
 * Copyright            Fintravalores S.A. S.A
 ************************************************************/

package com.tsp.finanzas.presupuesto.controller;
import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.finanzas.presupuesto.model.beans.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.util.*;
import com.tsp.exceptions.*;

public class Ref_viajeEventoAction extends Action {
    
    
    public void run() throws javax.servlet.ServletException, java.io.IOException {
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        Model modelito = (Model) session.getAttribute("model");
        String distrito = usuario.getDstrct();
        String opcion = request.getParameter("opcion");
        String sj = request.getParameter("Stdjob");
        String ano =  request.getParameter("Ano");
        String mes = request.getParameter("Mes");
        int dia = Integer.parseInt(request.getParameter("Dia"));
        String viajes = request.getParameter("Viajes");
        String next = "/jsp/finanzas/presupuesto/refViaje.jsp";
        String Consultas = "";
        try{
            modelito.tablaGenService.buscarTipodocumentos();
            if(opcion.equals("verificar")){
                model.DPtoSvc.listarReferenciaViaje(distrito,sj,ano,mes,dia);
                //int numero = model.DPtoSvc.getVecRef_Viaje().size() > Integer.parseInt(viajes) ? model.DPtoSvc.getVecRef_Viaje().size() : Integer.parseInt(viajes); 
                next+="?viajes="+viajes+"&Stdjob="+sj+"&Ano="+ano+"&Mes="+mes+"&Dia="+dia;
            }else if(opcion.equals("Insertar")){
                Ref_viaje ref = new Ref_viaje();
                ref.setDstrct(distrito);
                ref.setStd_job_no(sj);
                ref.setAno(ano);
                ref.setMes(mes);
                ref.setDia(dia);
                //elimino
                Consultas+= model.DPtoSvc.eliminarReferencia(ref);
                for (int i=0; i< Integer.parseInt(viajes); i++  ){
                    ////System.out.println(request.getParameter("doc"+i));
                    if(!request.getParameter("doc"+i).equals("")){
                        ref.setNro_viaje(i+1);
                        ref.setTipo_doc(request.getParameter("tipodoc"+i) );
                        ref.setDocumento(request.getParameter("doc"+i).toUpperCase() );
                        ref.setCreation_user(usuario.getLogin());
                        ref.setCreation_date(Util.getFechaActual_String(6));
                        ref.setUser_update(usuario.getLogin());
                        ref.setLast_update(Util.getFechaActual_String(6));
                        ref.setBase(usuario.getBase());
                        Consultas+= model.DPtoSvc.insertarReferencia(ref);
                    }                    
                }
                TransaccionService  svc = new  TransaccionService(usuario.getBd());
                svc.crearStatement();
                svc.getSt().addBatch( Consultas );
                svc.execute();
                model.DPtoSvc.listarReferenciaViaje(distrito,sj,ano,mes,dia);
                next+="?viajes="+model.DPtoSvc.getVecRef_Viaje().size()+"&Stdjob="+sj+"&Ano="+ano+"&Mes="+mes+"&Dia="+dia+"&men=Información Ingresada Exitosamente";
            }
            
            
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if (rd==null)
                throw new Exception("No se pudo encontrar " + next);
            rd.forward(request, response);
            
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    
}
