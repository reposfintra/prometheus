/********************************************************************
 *      Nombre Clase.................   PtoGastosAdminUpdateAction.java
 *      Descripci�n..................   Actualiza un registro en el archivo pto_gastos_admin
 *      Autor........................   Ing. Tito Andr�s Maturana
 *      Fecha........................   09.02.2006
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/
package com.tsp.finanzas.presupuesto.controller;

import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.finanzas.presupuesto.model.beans.*;
import com.tsp.finanzas.presupuesto.model.*;
import com.tsp.operation.model.threads.*;
import org.apache.log4j.Logger;
/**
 *
 * @author  Andres
 */
public class PtoGastosAdminUpdateAction extends Action{
    
    /** Creates a new instance of PosBancariaRefreshAction */
    public PtoGastosAdminUpdateAction() {
    }
    
    public void run() throws ServletException, java.io.IOException {
        
        String next = "/jsp/finanzas/presupuesto/gastos_admin/PtoGastosAdminUpdate.jsp?msg=";
        
        String ano = request.getParameter("ano");
        String dstrct = request.getParameter("distrito");
        String cuenta = request.getParameter("cuenta");
        
        HttpSession session = request.getSession();
        Usuario user = (Usuario) session.getAttribute("Usuario");
        
        String[] pto_vlr = request.getParameterValues("vlr_pto");
        double[] vlr_pto = new double[12];
        
        for(int i=0; i<pto_vlr.length; i++){
            vlr_pto[i] = Double.parseDouble(pto_vlr[i].replaceAll(",",""));
        }
        
        PtoGastosAdmin pto = new PtoGastosAdmin();
        pto.setCreation_user(user.getLogin());
        pto.setBase(user.getBase());
        pto.setDstrct(dstrct);
        pto.setAno(ano);
        pto.setCuenta(cuenta);
        pto.setVlr_presupuestado(vlr_pto);
        pto.setReg_status("");
        
        try{
            if( request.getParameter("anular")!=null ){
                /* Se recibe el parametro 'anular' que indica que se va a anular el registro */
                model.presupuesto_gadminSvc.anular(dstrct, cuenta, ano, user.getLogin());
                next = "/jsp/trafico/mensaje/MsgAnulado.jsp";
            } else {
                model.presupuesto_gadminSvc.setPresupuesto(pto);
                String reg_status = model.presupuesto_gadminSvc.existe(dstrct, pto.getCuenta(), pto.getAno());
                model.presupuesto_gadminSvc.actualizar();
                
                next += "Modificaci�n exitosa.";
                
                model.presupuesto_gadminSvc.obtener(dstrct, cuenta, ano);
                pto = model.presupuesto_gadminSvc.getPresupuesto();
                request.setAttribute("pto", pto);
            }
            
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if (rd==null)
                throw new Exception("No se pudo encontrar " + next);
            rd.forward(request, response);
        }catch (Exception e){
            e.printStackTrace();
            throw new ServletException(e.getMessage());
        }
    }
}

