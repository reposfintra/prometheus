/*************************************************************
 * Nombre      ............... OpcionesHistorialAction.java
 * Descripcion ............... busca presupuesto de acuerdo a los parametros dados
 * Autor       ............... mfontalvo
 * Fecha       ............... Dicienmbre - 15 - 2005
 * Version     ............... 1.0
 * Copyright   ............... Fintravalores S.A. S.A
 ************************************************************/


package com.tsp.finanzas.presupuesto.controller;


import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.finanzas.presupuesto.model.threads.*;
import com.tsp.util.UtilFinanzas;



public class OpcionesHistorialAction extends Action {
    
    /** Creates a new instance of OpcionesHistorialAction */
    public OpcionesHistorialAction() {
    }
    
    
    /* Procedimiento que ejecuta la accion
     * parametros
     * @params request     ........  HttpServletRequest
     * @params response    ........  HttpServletResponse
     * @throws ServletException, IOException
     */
    
    public void run() throws javax.servlet.ServletException, java.io.IOException {
        
        try{
            
            /**
             * Obtenemos el model de flujo de la session actual
             * para sacar el login del usuario
             **/
            
            HttpSession session = request.getSession();
            Usuario     usuario = (Usuario)session.getAttribute("Usuario");
            if (usuario==null) throw new Exception("Usuario no encontrado en session");
            
            
            
            String Mensaje = "";
            String Opcion  = getParameter("Opcion" ,"");
            ///////////////////////////////////////////////////
            String Estandar= getParameter("Stdjob" ,"");
            String Cliente = getParameter("Cliente","");
            String Agencia = getParameter("Agencia","");
            String Ano     = getParameter("Ano"    ,"" );
            String Mes     = getParameter("Mes"    ,"" );
            ///////////////////////////////////////////////////
            
            
            Agencia  = (Agencia.equals("")?"%":Agencia );
            Cliente  = (Cliente.equals("")?"%":Cliente );
            Estandar = (Estandar.equals("")?"%":Estandar);
            
            if (Opcion!=null){
                // historial pto
                if (Opcion.equals("Exportar")){
                    
                    ////System.out.println("Parmas ");
                    ////System.out.println(Agencia);
                    ////System.out.println(Cliente);
                    ////System.out.println(Estandar);
                    ////System.out.println(Ano);
                    ////System.out.println(Mes);
                    
                    
                    HExportarHistorial h = new HExportarHistorial();
                    h.start( Agencia, Cliente, Estandar, Ano, Mes, usuario);
                    Mensaje = "Su reporte ha iniciado con exito.";
                    
                }
            }
            
            String next ="/jsp/finanzas/presupuesto/PtoExportarHistorial.jsp?Mensaje="+ Mensaje;
            
            // reenvia hacia la pagina
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if(rd == null)
                throw new ServletException("No se pudo encontrar "+ next);
            rd.forward(request, response);
        }catch(Exception e){
            throw new ServletException("Error en OpcionesPresupuestoAction .....\n"+e.getMessage());
        }
    }
    
    
    /* Funcion para leer un parametro del mobjeto request
     * @autor mfontalvo
     * @params str1 ......... parametro a leer
     * @params str2 ......... devuelve este parametro si str1 es null
     * @return el parametro del objeto request
     */
    
    public String getParameter(String str1, String str2){
        return (request.getParameter(str1)==null?str2:request.getParameter(str1));
    }
    
}
