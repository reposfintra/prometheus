/*************************************************************
 * Nombre      ............... OpcionesAplicarIPAction.java
 * Descripcion ............... Aplica incrementos porcentuales
 * Autor       ............... mfontalvo
 * Fecha       ............... mayo - 17 - 2005
 * Version     ............... 1.0
 * Copyright   ............... Fintravalores S.A. S.A
 ************************************************************/



package com.tsp.finanzas.presupuesto.controller;




import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;



public class OpcionesAplicarIPAction extends Action  {
    
    /** Creates a new instance of OpcionesAplicarIPAction */
    public OpcionesAplicarIPAction() {
    }
    
    
    
    
      
/* Procedimiento que ejecuta la accion
 * parametros
 * @params request     ........  HttpServletRequest
 * @params response    ........  HttpServletResponse
 * @throws ServletException, IOException
 */
    public void run() throws javax.servlet.ServletException, java.io.IOException {
        try{
            
            HttpSession session = request.getSession();
            Usuario     usuario = (Usuario)session.getAttribute("Usuario");
            if (usuario==null) throw new Exception("Usuario no encontrado en session");            

            
            String Opcion  = request.getParameter ("Opcion");
            String CUnidad = request.getParameter ("CUnidad");
            String Modo    = request.getParameter ("Modo");
            String AnoI    = request.getParameter ("Ano");
            String MesI    = request.getParameter ("Mes");
            String AnoB    = request.getParameter ("Ano2");
            String MesB    = request.getParameter ("Mes2");
            
            if (Opcion != null){
                if (Opcion.equals("Aplicar Incremento")) {
                    model.IPorcentualesSvc.IncrementarPresupuesto(CUnidad, AnoI, MesI, AnoB, MesB, usuario.getLogin(), model);
                    if (Modo.equals("Cascada"))
                        for (int i = Integer.parseInt(MesI)+1; i<=12;i++){
                            model.IPorcentualesSvc.IncrementarPresupuesto(CUnidad, AnoI, ((i<10?"0":"")+i) , AnoI, (i-1<10?"0":"")+(i-1) , usuario.getLogin(), model);
                            //////System.out.println("Periodo Inc:" +AnoI + ((i<10?"0":"")+i) +" Base: "+ AnoI + ((i-1<10?"0":"")+(i-1) ));
                        }
    
                }
            }
            
            
            final String next = "/jsp/finanzas/presupuesto/AplicarIncPorc.jsp";
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if (rd == null)
                throw new ServletException("No se pudo encontrar "+ next);
            rd.forward(request, response);
                
        }catch (Exception e){
            throw new ServletException("Error en OpcionesAplicarIPAction .....\n"+e.getMessage());
        }
        
        
    }
    
}
