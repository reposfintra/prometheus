/*
 * Sendmailthis.java
 *
 * Created on 4 de enero de 2007, 02:44 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.tsp.util.mailing;

import java.io.*;
import java.util.*;

/**
 * Clase que carga la configuracion del programa de envio de correos en memoria
 * para utilizarla dentro del programa.
 * @author Ing. Ivan Herazo
 */
public final class EmailSenderSetup
{
  private String driverClass;
  private String dbUrl;
  private String dbUser;
  private String dbPassword;
  private String logPath;
  private String externMailServer;
  private String internMailServer;
  private String runtimeMode;
  private int maxAttachmentSize;
  private static Hashtable htKeys;
  private static ResourceBundle rbSetup;
  private static ResourceBundle rbMimeTypes;
  private static EmailSenderSetup setupInstance;
  
  /**
   * Esta clase no puede instanciarse.
   * @throws MissingResourceException Si no se puede cargar el archivo de propiedades
   */
  private EmailSenderSetup() throws MissingResourceException {
    this.load();
  }
  
  /**
   * Obtiene una instancia de la configuracion.
   * @return Instancia singleton de la configuracion del programa de correo
   * @throws MissingResourceException Si no se pueden cargar los archivos de configuracion
   */
  public static EmailSenderSetup getSetupInstance() throws MissingResourceException
  {
    if( setupInstance == null )
      setupInstance = new EmailSenderSetup();
    return setupInstance;
  }
  
  /**
   * Carga el archivo de configuracion del programa de envio de correo.
   * @throws MissingResourceException Si no se puede cargar el archivo en memoria.
   */
  public synchronized void load() throws MissingResourceException
  {
    rbSetup = ResourceBundle.getBundle("com/tsp/util/mailing/resources/SendmailSetup");
    String localLogDir  = rbSetup.getString("com.tsp.util.sendmail.localLogPath");
    String webSrvLogDir = rbSetup.getString("com.tsp.util.sendmail.webServerLogPath");
    File localLogFileObj   = new File(localLogDir);
    this.driverClass       = rbSetup.getString("com.tsp.util.sendmail.driverClass");
    this.dbUrl             = rbSetup.getString("com.tsp.util.sendmail.dbUrl");
    this.dbUser            = rbSetup.getString("com.tsp.util.sendmail.dbUser");
    this.dbPassword        = rbSetup.getString("com.tsp.util.sendmail.dbPassword");
    this.logPath           = (localLogFileObj.exists() ? localLogDir : webSrvLogDir);
    this.externMailServer  = rbSetup.getString("com.tsp.util.sendmail.externMailServer");
    this.internMailServer  = rbSetup.getString("com.tsp.util.sendmail.internMailServer");
    this.runtimeMode       = rbSetup.getString("com.tsp.util.sendmail.runtimeMode");
    this.maxAttachmentSize = Integer.parseInt(rbSetup.getString("com.tsp.util.sendmail.maxAttachmentSize"));
    
    // Cargar los tipos MIME adicionales (en caso de no ser encontrados por el
    // programa, pueden registrarse aqui).
    htKeys = new Hashtable();
    rbMimeTypes = ResourceBundle.getBundle("com/tsp/util/mailing/resources/MimeTypes");
    Enumeration<String> keys = rbMimeTypes.getKeys();
    while( keys.hasMoreElements() )
    {
      String key = keys.nextElement();
      String [] mimeTypeData = rbMimeTypes.getString(key).split("\\s");
      String mimeType   = mimeTypeData[0].trim();
      String extensions = mimeTypeData[1].trim();
      htKeys.put(mimeType, extensions);
    }
  }
  
  /**
   * Busca el tipo MIME de un archivo basandose en su extension, la cual es
   * extraida del nombre. El tipo MIME es buscado en el recurso
   * com/tsp/util/mailing/resources/MimeTypes
   * @param fileName Nombre del archivo
   * @return Tipo MIME o la cadena vacia si no encuentra la extension.
   */
  public String findMimeType(String fileName)
  {
    boolean mimeTypeFound = false;
    String mimeType = "application/octet-stream";
    int fileExtensionIdx = fileName.lastIndexOf(".");
    if( fileExtensionIdx > -1 )
    {
      String fileExtension = fileName.substring(fileExtensionIdx + 1, fileName.length());
      Enumeration mimeTypes = htKeys.keys();
      while( mimeTypes.hasMoreElements() && !mimeTypeFound )
      {
        mimeType = (String) mimeTypes.nextElement();
        String extensions = (String) htKeys.get(mimeType);
        mimeTypeFound = extensions.contains(fileExtension);
      }
    }
    return mimeType;
  }
  
  /**
   * Getter for property driverClass.
   * @return Value of property driverClass.
   */
  public String getDriverClass()
  {
    return this.driverClass;
  }

  /**
   * Setter for property driverClass.
   * @param driverClass New value of property driverClass.
   */
  public void setDriverClass(String driverClass)
  {
    this.driverClass = driverClass;
  }

  /**
   * Getter for property dbServer.
   * @return Value of property dbServer.
   */
  public String getDbUrl()
  {
    return this.dbUrl;
  }

  /**
   * Setter for property dbServer.
   * @param dbServer New value of property dbServer.
   */
  public void setDbUrl(String dbUrl)
  {
    this.dbUrl = dbUrl;
  }

  /**
   * Getter for property dbUser.
   * @return Value of property dbUser.
   */
  public String getDbUser()
  {
    return this.dbUser;
  }

  /**
   * Setter for property dbUser.
   * @param dbUser New value of property dbUser.
   */
  public void setDbUser(String dbUser)
  {
    this.dbUser = dbUser;
  }

  /**
   * Getter for property dbPassword.
   * @return Value of property dbPassword.
   */
  public String getDbPassword()
  {
    return this.dbPassword;
  }

  /**
   * Setter for property dbPassword.
   * @param dbPassword New value of property dbPassword.
   */
  public void setDbPassword(String dbPassword)
  {
    this.dbPassword = dbPassword;
  }

  /**
   * Getter for property logPath.
   * @return Value of property logPath.
   */
  public String getLogPath()
  {
    return this.logPath;
  }

  /**
   * Setter for property logPath.
   * @param logPath New value of property logPath.
   */
  public void setLogPath(String logPath)
  {
    this.logPath = logPath;
  }

  /**
   * Getter for property externMailServer.
   * @return Value of property externMailServer.
   */
  public String getExternMailServer()
  {
    return this.externMailServer;
  }

  /**
   * Setter for property externMailServer.
   * @param externMailServer New value of property externMailServer.
   */
  public void setExternMailServer(String externMailServer)
  {
    this.externMailServer = externMailServer;
  }

  /**
   * Getter for property internMailServer.
   * @return Value of property internMailServer.
   */
  public String getInternMailServer()
  {
    return this.internMailServer;
  }

  /**
   * Setter for property internMailServer.
   * @param internMailServer New value of property internMailServer.
   */
  public void setInternMailServer(String internMailServer)
  {
    this.internMailServer = internMailServer;
  }

  /**
   * Getter for property runtimeMode.
   * @return Value of property runtimeMode.
   */
  public String getRuntimeMode()
  {
    return this.runtimeMode;
  }

  /**
   * Setter for property runtimeMode.
   * @param runtimeMode New value of property runtimeMode.
   */
  public void setRuntimeMode(String runtimeMode)
  {
    this.runtimeMode = runtimeMode;
  }

  /**
   * Getter for property maxAttachmentSize.
   * @return Value of property maxAttachmentSize.
   */
  public int getMaxAttachmentSize()
  {
    return this.maxAttachmentSize;
  }

  /**
   * Setter for property maxAttachmentSize.
   * @param maxAttachmentSize New value of property maxAttachmentSize.
   */
  public void setMaxAttachmentSize(int maxAttachmentSize)
  {
    this.maxAttachmentSize = maxAttachmentSize;
  }
}
