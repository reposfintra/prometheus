
package com.tsp.util.mailing;

import javax.swing.*;
import java.io.*;
import java.sql.*;
import java.util.*;
import javax.mail.*;
import javax.mail.internet.*; 
import com.tsp.exceptions.*;
import java.util.LinkedList;
import java.util.ResourceBundle;

public class SendMail {
  public SendMail()
  {
      
  }

  public static void main(String[] args)
  {
        EmailSenderSetup setup = EmailSenderSetup.getSetupInstance();
        setup.setRuntimeMode("debug");
        PrintWriter logger = null;
        //System.out.println("**** PROGRAMA DE ENVIO DE CORREO AUTOMATICO (SendMail) ****");
        //System.out.println("Inicializando...");
        try {
            //logger = new PrintWriter(new BufferedWriter(new FileWriter(
            //  setup.getLogPath() + (setup.getLogPath().endsWith(File.separator) ? "" : File.separator) + "finv.log", true)), true
            //);
            //EmailSendingEngine.connectToSmtpServers();
            //System.out.println("Inicializacion completada!!");
        }catch (Exception ex){
            System.out.println("errrorrrrrrrrrrrrrrrrrrrrrrrrr"+ex.toString());
            ex.printStackTrace();
            throw new RuntimeException("No se puede ejecutar el programa. Causa: " + ex.getMessage());
        }
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        LinkedList<Email> sentEmailsList = new LinkedList<Email>();
        boolean debug = setup.getRuntimeMode().equalsIgnoreCase("debug");
        String emailContent = "";
        Email emailData = null;
        String tStampStr = new java.util.Date().toString();
        String charGauge = "*";
        long inicio = System.currentTimeMillis();
        try {
            //System.out.println("\nIniciando proceso de envio de correos...");
            if( debug )
                //System.out.println("Cargando e-mails de la cola de correos...");
                Class.forName("org.postgresql.Driver");
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db" );
            String usuariox= rb.getString("fintra.user");
            String passwordx=rb.getString("fintra.password");
            conn = DriverManager.getConnection("jdbc:postgresql://192.168.0.4:5432/fintra", usuariox, passwordx);
            conn.setAutoCommit( true );
            stmt = conn.createStatement(
            ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY
            );
            System.out.println("debug"+debug);
            if( debug )
                //System.out.println("\nContando los mensajes actuales: " + stmt);
                //rs = stmt.executeQuery("SELECT COUNT(emailcode) AS registros,arreglar_client_ws() AS correccion_client_ws FROM sendmail where tipo='W'");
                rs = stmt.executeQuery("SELECT COUNT(emailcode) AS registros FROM sendmail where tipo='W'");//091209
            int cienPorCiento = 0;
            if ( rs.next() )
                cienPorCiento = rs.getInt(1);
            rs.close();
            rs = stmt.executeQuery("SELECT * FROM sendmail where tipo='W' ORDER BY id ");
            int contador = 0;
            int progresoActual = 0;
            int progresoGauge = 0;
            //System.out.println("Estado del proceso ->  0 %");
            int ultimoValor = 0;
            System.out.println("antes de while en SendMail");
            while( rs.next() ) {
                contador++;
                progresoActual = (int)(contador * 99 / ((double)cienPorCiento));
                if( !debug && ultimoValor < progresoActual ) {
                    borrarCaracteres((""+progresoActual).length() + 2);
                    for( int i=0; i< progresoActual/10 - ultimoValor/10; i++ )
                        ultimoValor = progresoActual;
                }
                if ( debug )
                    emailData = Email.load(rs);
                if( debug )
                    emailContent = emailData.getRemarks() + emailData.getEmailbody();
                try {
                    EmailSendingEngine.send(emailData);
                    sentEmailsList.add(emailData);
                }catch (Exception EmailSendingE){
                    if( logger != null )
                        logger.println( "[" + tStampStr +
                        "] INFO  SendMail.java - OCURRIO UN ERROR ENVIANDO EMAIL: " +
                        EmailSendingE.getMessage() +
                        "\n\tEl registro con problemas es el que tiene ID # " +
                        emailData.getEmailId()
                        );
                    if( debug )
                        System.out.println( "[" + tStampStr +
                        "] INFO  SendMail.java - OCURRIO UN ERROR ENVIANDO EMAIL 2: " +
                        EmailSendingE.getMessage() +
                        "\n\tEl registro con problemas es el que tiene ID # " +
                        emailData.getEmailId()
                        );
                }
                emailContent = "";
            }
            rs.close();
            
            // Borrar los e-mails que ya fueron enviados.
            StringBuffer idsCsv = new StringBuffer(0);
            Iterator sentEmailsIt = sentEmailsList.iterator();
            while( sentEmailsIt.hasNext() ) {
                emailData = (Email) sentEmailsIt.next();
                idsCsv.append(emailData.getEmailId() + "," );
            }
            
            if ( idsCsv.length() > 0 )
                idsCsv = (StringBuffer) idsCsv.deleteCharAt(idsCsv.length()-1);
            
            if( idsCsv.length() > 0 ) {
                stmt.executeUpdate(
                "DELETE FROM sendmail " 
                );
                if( debug )
                    System.out.println("IDs de registros enviados = " + idsCsv.toString());
            }
            if( !debug ) {
                borrarCaracteres(4);
                //System.out.print(charGauge);
                //System.out.println(" 100%");
            }else
                System.out.println("Estado del proceso ->  100%");
        }catch (ClassNotFoundException ClassNotFoundE){
            System.out.println("error en sendmail aaaaaaaaaaaaaa"+ClassNotFoundE.toString()+"__"+ClassNotFoundE.getMessage());
            if( logger != null )
                logger.println( "[" + tStampStr +
                "] INFO  SendMail.java - NO SE PUDO CARGAR EL DRIVER DE BASE DE DATOS."
                );
            if( debug )
                System.out.println( "[" + tStampStr +
                "] INFO  SendMail.java - NO SE PUDO CARGAR EL DRIVER DE BASE DE DATOS."
                );
            else
                ClassNotFoundE.printStackTrace();
        }catch (SQLException SQLE){
            System.out.println("error en sendmail aaaaaaaaaaaaaa"+SQLE.toString()+"__"+SQLE.getMessage());
            if( logger != null )
                logger.println( "[" + tStampStr +
                "] INFO  SendMail.java - HUBO UN ERROR AL CONSULTAR LOS DATOS DE LOS " +
                "E-MAILS. CAUSA:\n" + SQLE.getMessage()
                );
            
            if( debug )
                System.out.println( "[" + tStampStr +
                "] INFO  SendMail.java - HUBO UN ERROR AL CONSULTAR LOS DATOS DE LOS " +
                "E-MAILS. CAUSA:\n" + SQLE.getMessage()
                );
            else
                SQLE.printStackTrace();
        }catch (Exception eee){
            System.out.println("error::"+eee.toString()+"__"+eee.getMessage());
        }finally{
            System.out.println("finallyy");
            try {
                if(rs != null) rs.close();
            }catch (SQLException ignore){}
            rs = null;
            
            try {
                if(stmt != null) stmt.close();
            }catch (SQLException ignore){}
            stmt = null;
            
            try {
                if(conn != null) conn.close();
            }catch (SQLException ignore){}
            conn = null;
            
            EmailSendingEngine.disconnectFromSmtpServers();
        }
        //System.out.println("...proceso de envio de correos finalizado!!");
        if ( debug )
            System.out.println("tiempo total empleado "+((System.currentTimeMillis() - inicio)/1000)+" segundos");
    }
    
    private static void borrarCaracteres(int numero) {
        for( int i=0; i< numero; i++ )
            System.out.print((char)8);
        System.out.print(" ");
        System.out.print((char)8);
    }
    
    
}