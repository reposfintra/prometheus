/*
 * SendMail.java
 *
 * Created on 27 de diciembre de 2004, 03:55 PM
 */

package com.tsp.util.mailing;

import java.io.*;
import java.sql.*;
import javax.mail.*;
import javax.mail.util.ByteArrayDataSource;
import javax.mail.internet.*;
import javax.activation.*;
import com.tsp.exceptions.*;

/**
 * Javabean utilizado para representar un correo electr�nico.
 * Permite enviar archivos adjuntos (correos Multipart).
 * @author Ing. Ivan Herazo
 */
public class Email implements Serializable
{
  private int emailId;
  private String recstatus;
  private String emailcode;
  private String emailfrom;
  private String emailto;
  private String emailcopyto;
  private String emailsubject;
  private String emailbody;
  private Timestamp lastupdat;
  private String senderName;
  private String remarks;
  private String tipo;
  private byte[] adjunto;
  private String nombreArchivo;
  private String [] emailtoarray;
  private String [] emailcopytoarray;
  private static ByteArrayOutputStream baos;
  private static EmailSenderSetup mailingSetup;

  static {
    mailingSetup = EmailSenderSetup.getSetupInstance();
    if( mailingSetup.getMaxAttachmentSize() == 0 )
      baos = new ByteArrayOutputStream();
    else
      baos = new ByteArrayOutputStream(mailingSetup.getMaxAttachmentSize());
  }

  /** Crea una nueva instancia de esta clase */
  public Email()
  {
    this.emailId = 0;
    this.recstatus = "A";
    this.emailcode = "";
    this.emailfrom = "";
    this.emailto = "";
    this.emailcopyto = "";
    this.emailsubject = "";
    this.emailbody = "";
    this.lastupdat = null;
    this.senderName = "";
    this.emailtoarray = null;
    this.emailcopytoarray = null;
    this.remarks  = " ";
    this.tipo = "I";
    this.adjunto = null;
    this.nombreArchivo = "";
  }
  
  synchronized public static Email load(ResultSet rs) throws SQLException
  {
    Email emailData = new Email();
    int id = rs.getInt("id");
    emailData.setEmailId(rs.wasNull() ? 0 : id);
    
    String value = rs.getString("recstatus");
    emailData.setRecstatus(rs.wasNull() ? "A" : value);
    
    value = rs.getString("emailcode");
    emailData.setEmailcode(rs.wasNull() ? "" : value);
    
    value = rs.getString("emailfrom");
    emailData.setEmailfrom(rs.wasNull() ? "" : value);
    
    value = rs.getString("emailto");
    emailData.setEmailto(rs.wasNull() ? "" : value);
    
    value = rs.getString("emailcopyto");
    emailData.setEmailcopyto(rs.wasNull() ? "" : value);
    
    value = rs.getString("emailsubject");
    emailData.setEmailsubject(rs.wasNull() ? "" : value);
    
    value = rs.getString("emailbody");
    emailData.setEmailbody(rs.wasNull() ? "" : value);
    
    Timestamp lastupdate = rs.getTimestamp("lastupdat");
    emailData.setLastupdat(rs.wasNull() ? new Timestamp(System.currentTimeMillis()) : lastupdate);
    
    value = rs.getString("sendername");
    emailData.setSenderName(rs.wasNull() ? "" : value);
    
    value = rs.getString("remarks");
    emailData.setRemarks(rs.wasNull() ? "" : value);
    
    value = rs.getString("tipo");
    emailData.setTipo(rs.wasNull() ? "I" : value);
    
    InputStream attachmentStream = rs.getBinaryStream("adjunto");
    if( !rs.wasNull() )
    {
      int nextByte = 0;
      try {
        while( (nextByte = attachmentStream.read()) != -1 )
          baos.write(nextByte);
        byte [] fileData = baos.toByteArray();
        double kbFileSize = ((double)fileData.length) / 1024d;
        if( mailingSetup.getMaxAttachmentSize() != 0 &&
            kbFileSize > mailingSetup.getMaxAttachmentSize() )
        {
          throw new SQLException(
            "Tama�o del archivo adjunto (" + kbFileSize + " KB) sobrepasa el maximo permitido (" +
            mailingSetup.getMaxAttachmentSize() + " KB). ID de registro: " + emailData.getEmailId()
          );
        }
        emailData.setAdjunto(fileData);
        baos.reset();
        
        value = rs.getString("nombrearchivo");
        emailData.setNombreArchivo(rs.wasNull() ? "" : value);
      }catch (IOException ex){
        baos.reset();
        emailData.setAdjunto(null);
        emailData.setNombreArchivo("");
      }
    }
    
    return emailData;
  }
  
  /**
   * Getter for property emailbody.
   * @return Value of property emailbody.
   */
  public String getEmailbody() {
    return emailbody;
  }
  
  /**
   * Setter for property emailbody.
   * @param emailbody New value of property emailbody.
   */
  public void setEmailbody(String emailbody) {
    this.emailbody = emailbody;
  }
  
  /**
   * Getter for property emailcode.
   * @return Value of property emailcode.
   */
  public String getEmailcode() {
    return emailcode;
  }

  /**
   * Setter for property emailcode.
   * @param emailcode New value of property emailcode.
   */
  public void setEmailcode(String emailcode) {
    this.emailcode = emailcode;
  }
  
  /**
   * Getter for property emailfrom.
   * @return Value of property emailfrom.
   */
  public String getEmailfrom() {
    return emailfrom;
  }
  
  /**
   * Setter for property emailfrom.
   * @param emailfrom New value of property emailfrom.
   */
  public void setEmailfrom(String emailfrom) {
    this.emailfrom = emailfrom;
  }
  
  /**
   * Getter for property emailsubject.
   * @return Value of property emailsubject.
   */
  public String getEmailsubject() {
    return emailsubject;
  }
  
  /**
   * Setter for property emailsubject.
   * @param emailsubject New value of property emailsubject.
   */
  public void setEmailsubject(String emailsubject) {
    this.emailsubject = emailsubject;
  }
  
  /**
   * Getter for property emailto.
   * @return Value of property emailto.
   */
  public String getEmailto() {
    return emailto;
  }
  
  /**
   * Indexed getter for property emailto.
   * @return An email.
   */
  public String getEmailto(int idx) {
    return this.emailtoarray[idx];
  }
  
  /**
   * Numero de destinos a copiar.
   * @return An email.
   */
  public int getEmailToLength() {
    return this.emailtoarray.length;
  }
  
  /**
   * Setter for property emailto.
   * @param emailto New value of property emailto.
   */
  public void setEmailto(String emailto) {
    this.emailto = emailto;
    this.emailtoarray = this.emailto.split(";");
    for( int idx = 0; idx < emailtoarray.length; idx++ )
      this.emailtoarray[idx] = this.emailtoarray[idx].trim();
  }
  
  /**
   * Getter for property emailcopyto.
   * @return Value of property emailcopyto.
   */
  public String getEmailcopyto() {
    return emailcopyto;
  }
  
  /**
   * Indexed getter for property emailcopyto.
   * @return An email.
   */
  public String getEmailcopyto(int idx) {
    return this.emailcopytoarray[idx];
  }
  
  /**
   * Numero de destinos a copiar.
   * @return An email.
   */
  public int getEmailCopytoLength() {
    return this.emailcopytoarray.length;
  }
  
  /**
   * Setter for property emailcopyto.
   * @param emailcopyto New value of property emailcopyto.
   */
  public void setEmailcopyto(String emailcopyto) {
    this.emailcopyto = emailcopyto;
    this.emailcopytoarray = this.emailcopyto.split(";");
    for( int idx = 0; idx < emailcopytoarray.length; idx++ )
      this.emailcopytoarray[idx] = this.emailcopytoarray[idx].trim();
  }
  
  /**
   * Getter for property lastupdat.
   * @return Value of property lastupdat.
   */
  public Timestamp getLastupdat() {
    return lastupdat;
  }
  
  /**
   * Setter for property lastupdat.
   * @param lastupdat New value of property lastupdat.
   */
  public void setLastupdat(Timestamp lastupdat) {
    this.lastupdat = lastupdat;
  }
  
  /**
   * Getter for property recstatus.
   * @return Value of property recstatus.
   */
  public String getRecstatus() {
    return recstatus;
  }
  
  /**
   * Setter for property recstatus.
   * @param recstatus New value of property recstatus.
   */
  public void setRecstatus(String recstatus) {
    this.recstatus = recstatus;
  }
  
  /**
   * Getter for property senderName.
   * @return Value of property senderName.
   */
  public String getSenderName() {
    return this.senderName;
  }
  
  /**
   * Setter for property senderName.
   * @param senderName New value of property senderName.
   */
  public void setSenderName(String senderName) {
    this.senderName = senderName;
  }
  
 public void setRemarks(String remarks){
      this.remarks = remarks;
  }

  public String getRemarks(){
      return remarks;
  }

  /**
   * Getter for property adjuntos.
   * @return Value of property adjuntos.
   */
  public byte[] getAdjunto()
  {
    return this.adjunto;
  }

  /**
   * Setter for property adjuntos.
   * @param adjuntos New value of property adjuntos.
   */
  public void setAdjunto(byte[] adjunto)
  {
    this.adjunto = adjunto;
  }

  /**
   * Getter for property emailId.
   * @return Value of property emailId.
   */
  public int getEmailId()
  {
    return this.emailId;
  }

  /**
   * Setter for property emailId.
   * @param emailId New value of property emailId.
   */
  public void setEmailId(int emailId)
  {
    this.emailId = emailId;
  }

  /**
   * Getter for property tipo.
   * @return Value of property tipo.
   */
  public String getTipo()
  {
    return this.tipo;
  }

  /**
   * Setter for property tipo.
   * @param tipo New value of property tipo.
   */
  public void setTipo(String tipo)
  {
    this.tipo = tipo;
  }

  /**
   * Getter for property nombrearchivo.
   * @return Value of property nombrearchivo.
   */
  public String getNombreArchivo()
  {
    return this.nombreArchivo;
  }

  /**
   * Setter for property nombrearchivo.
   * @param nombrearchivo New value of property nombrearchivo.
   */
  public void setNombreArchivo(String nombreArchivo)
  {
    this.nombreArchivo = nombreArchivo;
  }
}
