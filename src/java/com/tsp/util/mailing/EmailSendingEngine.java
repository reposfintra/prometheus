/*
 * EmailSendingEngine.java
 *
 * Created on 5 de enero de 2007, 11:32 AM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package com.tsp.util.mailing;

import javax.mail.*;
import javax.mail.internet.*;
import javax.mail.util.ByteArrayDataSource; 
import javax.activation.*;
import com.tsp.exceptions.EmailSendingException;
import java.util.*;

/**
 * Clase utilizada para enviar correos.
 * @author Ing. Ivan Herazo
 */
public class EmailSendingEngine
{
  private static Session session;
  private static Transport transI;
  private static Transport transE;
  private static EmailSenderSetup mailingSetup;
  
  static {
    mailingSetup = EmailSenderSetup.getSetupInstance();
  }
  
  /** Esta clase no puede instanciarse */
  private EmailSendingEngine(){}
  
  /**
   * Abre una conexion a los servidores de correo (interno y externo).
   * @throws EmailSendingException Si no se puede establecer la conexion.
   */
  public static void connectToSmtpServers() throws EmailSendingException
  {
    // Abrir una sesion de correo
    if( session == null )
      session = Session.getInstance(System.getProperties(), null);
    
    // Conectarse al servidor de correo que efectuar� el env�o.
    try {
      if( transE == null )
      {
        transE = session.getTransport("smtp");
        transE.connect(mailingSetup.getExternMailServer(), "npacheco@geotechsa.net", "12345");
      }
      
      if( transI == null )
      {
        transI = session.getTransport("smtp");
        transI.connect(mailingSetup.getInternMailServer(), "npacheco@geotechsa.net", "12345");
      }
    }catch (NoSuchProviderException ex){
      ex.printStackTrace();
      throw new EmailSendingException(ex.getMessage());
    }catch (MessagingException ex){
      ex.printStackTrace();
      throw new EmailSendingException(ex.getMessage());
    }
  }
  
  /**
   * Cierra las conexiones a los servidores de correo (interno y externo).
   */
  public static void disconnectFromSmtpServers()
  {
    try {
      if( transI != null ) transI.close();
    }catch (MessagingException ex){}
    try {
      if( transE != null ) transE.close();
    }catch (MessagingException ex){}
    transI = null;
    transE = null;
    System.gc();
  }
  
  /**
   * Env�a el correo electronico.
   * @param email Objeto con los datos del correo a ser enviado.
   * @throws EmailSendingException Si ocurre un error durante el env�o de correo.
   */
  public static void send(Email email) throws MessagingException

  {
    long now = System.currentTimeMillis();
    String emailFrom    = email.getEmailfrom().trim();
    String senderName   = email.getSenderName().trim();
    String emailSubject = email.getEmailsubject().trim();
    String emailBody    = email.getEmailbody().trim();
    String emailto      = email.getEmailto();
    boolean debug = false;
    Properties props = new Properties();
    props.put("mail.debug", "true");
    props.setProperty("mail.transport.protocol", "smtp");
    props.setProperty("mail.smtp.auth","true");
    Session mailSession = Session.getDefaultInstance(props, null);
    Transport transport = mailSession.getTransport();
    InternetAddress addressFrom = new InternetAddress("avendries@fintravalores.com");
    MimeMessage message = new MimeMessage(mailSession);
    message.setSubject(emailSubject);
    message.setContent(emailBody, "text/plain");
    message.setFrom(addressFrom);
    message.addRecipient(Message.RecipientType.TO,
    new InternetAddress(emailto));
    transport.connect("smtpout.secureserver.net", "avendries@fintravalores.com", "12345");
    transport.sendMessage(message,
    message.getRecipients(Message.RecipientType.TO));
    transport.close();
  }
}
