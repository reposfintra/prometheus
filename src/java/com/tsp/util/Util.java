//************ clase modificada ***************
/**
 * Clase ............. Util Objetivo .......... Esta es una clase de utilidades
 * que proporciona diversos m�todos de soporte Autor ............. Nestor Parejo
 * Donado Fecha ............. Mayo 26/2003
 */
package com.tsp.util;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import java.text.*;
import java.sql.Timestamp;
import java.util.*;
import javax.crypto.*;
import javax.crypto.spec.*;
import sun.misc.*;
import com.tsp.exceptions.*;
import com.tsp.operation.model.DAOS.MainDAO;
import java.awt.*;
import java.util.List;
import java.util.Iterator;
import com.tsp.operation.model.beans.*;
import java.math.*;
import java.io.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.nio.charset.Charset;
import java.nio.charset.CharsetEncoder;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Types;
import java.time.format.DateTimeFormatter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * M�todos de utilidades usados en el paquete del modelo
 */
public class Util {
    // private LogWriter logWriter;

    private Properties dbProps = new Properties();
    public static final int FORMATO_YYYYMMDD = 8;
    private static final SimpleDateFormat DATE_FORMAT
            = new SimpleDateFormat("yyyy-MM-dd");
    private static final SimpleDateFormat DATE_TIME_FORMAT
            = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    public static LogWriter logWriter;
    public static PrintWriter pw;
    public static String logFile;

    /**
     * Da formato a una fecha utilizando el formato JDBC predeterminado
     */
    public static String dateFormat(Date d) {
        return d == null ? "" : DATE_FORMAT.format(d);
    }

    /**
     * Da formato a timestamp utilizando el formato JDBC predeterminado
     */
    public static String dateTimeFormat(Date d) {
        return d == null ? "" : DATE_TIME_FORMAT.format(d);
    }

    public static String formatDateOracle(String fecha) {

        String ano = fecha.substring(0, 4);
        String mes = fecha.substring(5, 7);
        String dia = fecha.substring(8, 10);
        String cadena = ano + mes + dia;
        return cadena;
    }

    /**
     * Convierte una java.util.Date a una java.sql.Timestamp
     */
    public static Timestamp toTimestamp(Date d) {
        return (d == null)
                ? null
                : new Timestamp(d.getTime());
    }

    /**
     * Encierra una cadena entre comillas si contiene una coma
     *
     * @param s la cadena
     */
    public static String quote(String s) {
        if (s != null) {
            if (s.indexOf(",") > -1) {
                StringBuffer sb = new StringBuffer();
                sb.append('"');
                sb.append(s);
                sb.append('"');
                s = sb.toString();
            }
        }
        return s;
    }

    /**
     * Devuelve true si el ID de evento especificado representa una acci�n "close"
     *
     * @param eventID el ID de evento
     */
    public static final boolean isClosingEvent(String eventID) {
        return (eventID.equals("CNB")
                || eventID.equals("CCP")
                || eventID.equals("CFX"));
    }

    /**
     * M�todo para encriptar una cadena de caracteres. Utiliza el est�ndar de encripci�n digital (DES: Digital Encryption Standard).
     *
     * @param privateKey clave privada.
     * @param decriptedString cadena a encriptar.
     * @return La cadena encriptada.
     * @throws EncriptionException si no se puede encriptar la cadena
     */
    public static final String encript(String privateKey, String decriptedString)
            throws EncriptionException {
        String decriptedStr = null;
        try {
            // Inicializar el cifrador.
            byte[] clave = privateKey.getBytes();
            DESKeySpec key1 = new DESKeySpec(clave);

            SecretKeyFactory factory = SecretKeyFactory.getInstance("DES");

            SecretKey key2 = factory.generateSecret(key1);

            Cipher ecipher = Cipher.getInstance("DES");

            ecipher.init(Cipher.ENCRYPT_MODE, key2);

            // Codificar la cadena en bytes usando UTF-8
            byte[] utf8 = decriptedString.getBytes("UTF8");

            // Encriptar
            byte[] enc = ecipher.doFinal(utf8);

            // Codificar los bytes a base 64 para obtener la cadena encriptada.
            decriptedStr = (new BASE64Encoder()).encode(enc);
        } catch (Exception EncriptE) {
            throw new EncriptionException(EncriptE.getMessage());
        }
        return decriptedStr;
    }

    public static String getFechaActual_String(int valor) {
        SimpleDateFormat FMT = null;
        switch (valor) {
            case 0:
                FMT = new SimpleDateFormat("MMM");
                break; //Mes en espa�ol Ej: ene
            case 1:
                FMT = new SimpleDateFormat("yyyy");
                break; //A�o en 4 digitos Ej: 2001
            case 2:
                FMT = new SimpleDateFormat("WW");
                break; //Semana del mes Ej: 02
            case 3:
                FMT = new SimpleDateFormat("MM");
                break; //Mes en 2 digitos Ej: 01
            case 4:
                FMT = new SimpleDateFormat("yyyy-MM-dd");
                break; //A�o, Mes dia separados por '-' Ej: 2004-10-09
            case 5:
                FMT = new SimpleDateFormat("dd");
                break; //Dia en 2 digitos Ej: 02
            case 6:
                FMT = new SimpleDateFormat("yyyy/MM/dd kk:mm:ss");
                break; //A�o, Mes dia separados por '/' y hora(24), minutos y segundos separado por ':' Ej: 2004/10/09 23:50:30
            case 7:
                FMT = new SimpleDateFormat("yyyy/MM/dd");
                break; //A�o, Mes dia separados por '/' Ej: 2004/10/09
            case FORMATO_YYYYMMDD:
                FMT = new SimpleDateFormat("yyyyMMdd");
                break; //A�o, mes, dia sin separacion
            case 9:
                FMT = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss");
                break; //A�o, Mes dia separados por '/' y hora(24), minutos y segundos separado por ':' Ej: 2004/10/09 23:50:30
            case 10:
                FMT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                break; //Ao, Mes dia separados por '/' y hora(24), minutos y segundos separado por ':' Ej: 2004/10/09 00:50:30//"yyyy-MM-dd HH:mm:ss"//20100729
        }
        String Fecha = FMT.format(new Date());
        return Fecha.toUpperCase();
    }

    public static String fechaActualTIMESTAMP() {
        SimpleDateFormat FMT = null;
        FMT = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss");
        String Fecha = FMT.format(new Date());
        return Fecha.toUpperCase();
    }

    public static String PuntoDeMil(String valor) {
        //Nota: la popiedad indexOf me retorna -1 si no encuantra el caracter '.' en la cadena, osea que si
        //encontro el caracter quiere decir que la cadena es un numero doble o float, en este caso no le aplico el
        //separador de mil, solamente retorno la misma cadena
        String Cadena = "";
        if (valor.indexOf(".") != -1) {
            Cadena = valor;
        } else {
            int cont = 0;
            for (int i = valor.length(); i > 0; i--) {
                ////System.out.println(valor.charAt(i-1));
                if (cont == 3) {
                    Cadena = String.valueOf(valor.charAt(i - 1)) + "." + Cadena;
                    cont = 0;
                } else {
                    Cadena = String.valueOf(valor.charAt(i - 1)) + Cadena;
                }
                cont++;
            }
        }
        return Cadena;
    }

    public static boolean SeparadorDeMiles(String valor) {
        boolean respuesta = false;
        if (valor.indexOf(".") == -1) {
            respuesta = true;
        }
        return respuesta;
    }

    public static String SeparadorDeMiles(String valor, String Numerico) {
        String cadena = valor;
        if (!Numerico.equals("")) {
            cadena = PuntoDeMil(valor);
        }
        return cadena;
    }

    public static String NombreMes(int mes) {
        String nombre = "";
        switch (mes) {
            case 1:
                nombre = "Enero";
                break;
            case 2:
                nombre = "Febrero";
                break;
            case 3:
                nombre = "Marzo";
                break;
            case 4:
                nombre = "Abril";
                break;
            case 5:
                nombre = "Mayo";
                break;
            case 6:
                nombre = "Junio";
                break;
            case 7:
                nombre = "Julio";
                break;
            case 8:
                nombre = "Agosto";
                break;
            case 9:
                nombre = "Septiembre";
                break;
            case 10:
                nombre = "Octubre";
                break;
            case 11:
                nombre = "Noviembre";
                break;
            case 12:
                nombre = "Diciembre";
                break;
            case 13:
                nombre = "Fiscal";
                break;
        }
        return nombre;
    }

    public static String mesFormat(int mes) {
        String nombre = "";
        switch (mes) {
            case 1:
                nombre = "01";
                break;
            case 2:
                nombre = "02";
                break;
            case 3:
                nombre = "03";
                break;
            case 4:
                nombre = "04";
                break;
            case 5:
                nombre = "05";
                break;
            case 6:
                nombre = "06";
                break;
            case 7:
                nombre = "07";
                break;
            case 8:
                nombre = "08";
                break;
            case 9:
                nombre = "09";
                break;
            case 10:
                nombre = "10";
                break;
            case 11:
                nombre = "11";
                break;
            case 12:
                nombre = "12";
                break;
        }
        return nombre;

    }

    public static int AnoActual() {
        java.util.Calendar fecha = java.util.Calendar.getInstance();
        int ano = fecha.get(java.util.Calendar.YEAR);
        return ano;
    }

    public static int MesActual() {
        java.util.Calendar fecha = java.util.Calendar.getInstance();
        int mes = fecha.get(java.util.Calendar.MONTH) + 1;
        return mes;
    }

    public static String fechaActual(String parametro) {

        if (parametro == null) {
            java.util.Date dato = new java.util.Date();
            parametro = dato.toLocaleString().toString();
        }
        return parametro;
    }

    public static String formatoFechaPostgres(String fecha) {
        String formato = "0099-01-01";
        if (fecha != null && !fecha.equals("")) {
            formato = fecha.substring(0, 4) + "-" + fecha.substring(5, 7) + "-" + fecha.substring(8, 10);
        }
        return formato;
    }

    public static boolean Imprimir_Remesa(List Registros, String Remesa, String Planilla, String Usuario) throws Exception {
        boolean Estado = true;
        try {
            Frame f = new Frame("Imprimir");
            f.pack();
            PrintJob pjob = f.getToolkit().getPrintJob(f, "Impresion del Registro", null);
            Graphics pg = pjob.getGraphics();
            pg.setFont(new Font("SansSerif", Font.PLAIN, 12));
            int x = 10, y = 40;
            Iterator it = Registros.iterator();
            while (it.hasNext()) {
                Remesa datos = (Remesa) it.next();
                if (datos.getNumrem().equals(Remesa) && datos.getOc().equals(Planilla)) {
                    //Comentario1 + Codigo de Remesa
                    pg.drawString("Para cualquier informaci�n relacionada con este documento citar la O.T.:" + Remesa, x, y);
                    //Mensaje de imoresion
                    if (datos.getPrinter_Date().equals("0099-01-01 00:00:00")) {
                        pg.drawString("Origial Impreso." + Remesa, x, (y + 12));
                    } else {
                        pg.drawString("Reimpresion." + Remesa, x, (y + 12));
                    }
                    //Agencia de Origen
                    pg.drawString(datos.getAgcRem(), (x + 0), (y + 52));
                    //Fecha
                    pg.drawString(datos.getFecRem().toString().substring(8, 10) + "  " + datos.getFecRem().toString().substring(5, 7) + "  " + datos.getFecRem().toString().substring(2, 4), (x + 98), (y + 55));
                    //Origen de Carga
                    pg.drawString(datos.getOriRem(), (x + 0), (y + 77));
                    //Remitente
                    pg.drawString(datos.getRemitente(), (x + 100), (y + 77));
                    //Destinatario
                    pg.drawString(datos.getDestinatario(), (x + 0), (y + 102));
                    //Direccion Destinatario
                    pg.drawString(datos.getDestinatario(), (x + 244), (y + 102));
                    //Ciudad Destinatario
                    pg.drawString(datos.getCiuDestinatario(), (x + 420), (y + 102));
                    //Planilla
                    pg.drawString(datos.getOc(), (x + 0), (y + 127));
                    //Placa
                    pg.drawString(datos.getPlaca(), (x + 100), (y + 127));
                    //Conductor
                    pg.drawString(datos.getConductor(), (x + 200), (y + 127));
                    //Comentario2
                    pg.drawString("SEG�N DOCUMENTO(S) INTERNO(S) DEL CLIENTE:", (x + 0), (y + 160));
                    //Cliente
                    pg.drawString(datos.getDocInterno(), (x + 0), (y + 172));

                    //Observacion
                    pg.drawString(datos.getObservacion(), (x + 0), (y + 196));

                    if (!datos.getDerechosCedido().equals("")) {
                        //Comentario3
                        pg.drawString("TRANSPORTES SANCHEZ POLO S.A. cede la totalidad de los derechos econ�micos", (x + 0), (y + 222));
                        pg.drawString("derivados del presente contrato de transporte a favor del patrimonio", (x + 0), (y + 234));
                        pg.drawString("aut�nomo FC- TRANSPORTES SANCHEZ POLO, dicha cesi�n no implica de manera", (x + 0), (y + 246));
                        pg.drawString("alguna la cesi�n de sus obligaciones como transportador frente al remitente", (x + 0), (y + 258));
                        pg.drawString("y/o destinatario. El cedido acepta esta cesi�n de derechos econ�micos,", (x + 0), (y + 270));
                        pg.drawString("mediante acto de notificaci�n que se entiende surtido por la sola", (x + 0), (y + 282));
                        pg.drawString("expedici�n de la remesa.", (x + 0), (y + 294));
                    }
                    //Usuario
                    pg.drawString(Usuario, (x + 0), (y + 350));
                    break;
                }
            }
            // Se finaliza la pagina
            pg.dispose();
            // Se hace que la impresora termine el trabajo
            pjob.end();
        } catch (Exception e) {
            Estado = false;
        }
        return Estado;
    }

    //modificado por Fily 22 dic 2006
    public static String customFormat(double value) {
        DecimalFormat df = (DecimalFormat) NumberFormat.getNumberInstance(new Locale("en", "US"));
        df.applyPattern("#,###");
        df.setMaximumFractionDigits(0);
        df.setMinimumFractionDigits(0);
        return df.format(value);
    }

    public static String customFormat(Double value) {
        DecimalFormat df = (DecimalFormat) NumberFormat.getNumberInstance(new Locale("en", "US"));
        df.applyPattern("#,###");
        df.setMaximumFractionDigits(2);
        df.setMinimumFractionDigits(2);
        return df.format(value.doubleValue());
    }

    public static String customFormat(double value, int numDecimales) {
        //numdecimales 2
        DecimalFormat df = (DecimalFormat) NumberFormat.getNumberInstance(new Locale("en", "US"));
        df.applyPattern("#,##");
        df.setMaximumFractionDigits(2);
        df.setMinimumFractionDigits(2);
        return df.format(value);
    }

    public static double redondear(double valor, int escala) {
        double rval = 0;
        BigDecimal bd = null;
        bd = BigDecimal.valueOf(valor).setScale(escala, RoundingMode.HALF_UP);
        //JOptionPane.showMessageDialog(null, );
        rval = bd.doubleValue();

        return rval;
    }

    /**
     * Crea un objeto java.util.Calendar a partir de una cadena en formato PostgreSQL
     *
     * @param fechaEnFormatoPostgres String -> 0099-01-01 00:00:00
     * @return Calendar
     */
    public static Calendar crearCalendar(String fechaEnFormatoPostgres) {
        try {
            String[] v = fechaEnFormatoPostgres.split(" ");
            String vFecha[] = v[0].split("-");
            String vHora[] = v[1].split(":");
            int a�o = Integer.parseInt(vFecha[0]);
            int mes = Integer.parseInt(vFecha[1]) - 1;
            int dia = Integer.parseInt(vFecha[2]);
            int hora = Integer.parseInt(vHora[0]);
            int minuto = Integer.parseInt(vHora[1]);
            int indicePunto = vHora[2].indexOf(".");
            int seg = Integer.parseInt(vHora[2].substring(0, indicePunto > 0 ? indicePunto : vHora[2].length()));
            GregorianCalendar c = new GregorianCalendar(a�o, mes, dia, hora, minuto, seg);
            //c.set(c.
            ////System.out.println(fechaEnFormatoPostgres+" es pm? "+(c.get(c.AM_PM) == c.PM? "si": "no"));
            return c;
        } catch (Exception ex) {
            ////System.out.println("Fecha recibida: "+fechaEnFormatoPostgres);
            ex.printStackTrace();
            return new GregorianCalendar();
        }

    }

    public static Calendar crearCalendarDate(String fecha) {
        try {
            String[] v = fecha.split(" ");
            String vFecha[] = v[0].split("-");
            int a�o = Integer.parseInt(vFecha[0]);
            int mes = Integer.parseInt(vFecha[1]);
            int dia = Integer.parseInt(vFecha[2]);
            GregorianCalendar c = new GregorianCalendar(a�o, mes, dia);
            return c;
        } catch (Exception ex) {
            ////System.out.println("Fecha recibida: " + fecha);
            ex.printStackTrace();
            return new GregorianCalendar();
        }

    }

    public static String crearStringFechaDateLetra(Calendar c) {
        return (c.get(c.DATE) > 9 ? "" + c.get(c.DATE) : "0" + c.get(c.DATE)) + "-"
                + (c.get(c.MONTH) > 9 ? "" + c.get(c.MONTH) : "0" + c.get(c.MONTH)) + "-"
                + c.get(c.YEAR);
    }

    public static String crearStringFechaDateLetra2(Calendar c) {
        String ret = "";
        int sw = 0;

        if ((c.get(c.MONTH) == 1) && (c.get(c.DATE) == 28)) {
            ret = 29 + "-" + "01" + "-" + c.get(c.YEAR);
            sw = 1;
        }

        if ((c.get(c.MONTH) == 2) && (c.get(c.DATE) == 29) && ((c.get(c.YEAR) % 4) != 0)) {
            ret = 28 + "-" + "02" + "-" + c.get(c.YEAR);
            sw = 1;
        }

        if (sw == 0) {
            if (c.get(c.MONTH) == 0) {
                int an = c.get(c.YEAR);
                an = an - 1;
                ret = (c.get(c.DATE) > 9 ? "" + c.get(c.DATE) : "0" + c.get(c.DATE)) + "-"
                        + 12 + "-"
                        + an + "";
            } else {
                ret = (c.get(c.DATE) > 9 ? "" + c.get(c.DATE) : "0" + c.get(c.DATE)) + "-"
                        + (c.get(c.MONTH) > 9 ? "" + c.get(c.MONTH) : "0" + c.get(c.MONTH)) + "-"
                        + c.get(c.YEAR);
            }
        }
        return (ret);
    }

    public static String crearStringFechaDateLetraRemix(Calendar c) {
        String ret = "";
        int sw = 0;

        if ((c.get(c.MONTH) == 2) && (c.get(c.DATE) == 29) && ((c.get(c.YEAR) % 4) != 0)) {
            ret = 28 + "-" + "02" + "-" + c.get(c.YEAR);
            sw = 1;
        }

        if (sw == 0) {
            if (c.get(c.MONTH) == 0) {
                int an = c.get(c.YEAR);
                an = an - 1;
                ret = (c.get(c.DATE) > 9 ? "" + c.get(c.DATE) : "0" + c.get(c.DATE)) + "-"
                        + 12 + "-"
                        + an + "";
            } else {
                ret = (c.get(c.DATE) > 9 ? "" + c.get(c.DATE) : "0" + c.get(c.DATE)) + "-"
                        + (c.get(c.MONTH) > 9 ? "" + c.get(c.MONTH) : "0" + c.get(c.MONTH)) + "-"
                        + c.get(c.YEAR);
            }
        }
        return (ret);
    }

    public static String crearStringFecha(Calendar c) {
        return c.get(c.YEAR) + "-"
                + ((c.get(c.MONTH) + 1) > 9 ? "" + (c.get(c.MONTH) + 1) : "0" + (c.get(c.MONTH) + 1)) + "-"
                + (c.get(c.DATE) > 9 ? "" + c.get(c.DATE) : "0" + c.get(c.DATE)) + " "
                + (c.get(c.HOUR) > 9 ? "" + c.get(c.HOUR) : "0" + c.get(c.HOUR)) + ":"
                + (c.get(c.MINUTE) > 9 ? "" + c.get(c.MINUTE) : "0" + c.get(c.MINUTE)) + ":"
                + (c.get(c.SECOND) > 9 ? "" + c.get(c.SECOND) : "0" + c.get(c.SECOND)) + ".000";
    }

    public static String llenarConCerosALaIzquierda(String str, int tama�oDeseado) {
        StringBuffer res = new StringBuffer(str);
        for (int i = 0; i < tama�oDeseado - str.length(); i++) {
            res.insert(0, "0");
        }
        return res.toString();
    }

    public static String llenarConCerosALaIzquierda(int num, int tama�oDeseado) {
        return llenarConCerosALaIzquierda(String.valueOf(num), tama�oDeseado);
    }

    public static String llenarConEspaciosAlFinal(String str, int tama�oDeseado) {
        StringBuffer res = new StringBuffer(str);
        for (int i = 0; i < tama�oDeseado - str.length(); i++) {
            res.append(" ");
        }
        return res.toString();
    }

    /**
     * sonElMismoDia
     *
     * @param f2 Calendar
     * @param f1 Calendar
     * @return boolean
     */
    public static boolean sonElMismoDia(Calendar f1, Calendar f2) {
        return f1.get(f1.YEAR) == f2.get(f1.YEAR)
                && f1.get(f1.MONTH) == f2.get(f1.MONTH)
                && f1.get(f1.DATE) == f2.get(f1.DATE);
    }

    ///////////////jme
    public static boolean MayorString(String i, String f) {
        int r = i.compareToIgnoreCase(f);
        if (r == 1) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean MenorString(String i, String f) {
        f = f.substring(0, 4) + f.substring(5, 7) + f.substring(8, 10);
        int r = i.compareTo(f);
        if (r < 0) {
            return true;
        } else {
            return false;
        }

    }

    public static boolean MayorIgualString(String i, String f) {
        i = i.substring(0, 10);
        int r = i.compareTo(f);
        if (r > 0) {
            return true;
        } else if (r == 0) {
            return true;
        } else {
            return false;
        }

    }

    public static String FechaMenosDias(String fi, int d) {
        int ano = Integer.parseInt(fi.substring(0, 4));
        int mes = Integer.parseInt(fi.substring(5, 7));
        int dia = Integer.parseInt(fi.substring(8, 10));

        String a = "";
        String m = "";
        String di = "";

        int r = dia - 1;

        a = String.valueOf(ano);
        m = String.valueOf(mes);
        di = String.valueOf(r);

        if (r == 0) {
            di = String.valueOf(DiasMes(mes - 1));
            m = String.valueOf(mes - 1);
        }
        if (mes < 10) {
            m = "0" + m;
        }

        if (dia < 10) {
            di = "0" + di;
        }
        if ((r == 0) && (mes == 1)) {
            ano = ano - 1;
            mes = 12;
            dia = 31;
            a = String.valueOf(ano);
            m = String.valueOf(mes);
            di = String.valueOf(dia);
        }

        String res = a + "-" + m + "-" + di;
        return res;
    }
    //Funcion que te permite calcular de disponibilidad,

    public static String FechaDisp(String fi, float horas) {
        int ano = Integer.parseInt(fi.substring(0, 4));
        int mes = Integer.parseInt(fi.substring(5, 7));
        int dia = Integer.parseInt(fi.substring(8, 10));
        int h = Integer.parseInt(fi.substring(11, 13));
        int m = Integer.parseInt(fi.substring(14, 16));
        int pentera = (int) horas;
        int diasmes = DiasMes(mes);
        float residuo = horas - (float) pentera;
        String fechadisp = String.valueOf(ano);
        String horadisp = String.valueOf(h);

        h = h + pentera;
        m = m + (int) (residuo * 60);

        if (m > 59) {
            m = m - 60;
            h = h + 1;
        }
        if (h > 23) {
            h = h - 24;
            dia = dia + 1;
            horadisp = String.valueOf(h);
        }
        if (dia > diasmes - 1) {
            dia = dia - diasmes;
            mes = mes + 1;
        }
        if (mes > 11) {
            mes = mes - 12;
            ano = ano + 1;
            fechadisp = String.valueOf(ano);
        }
        if (mes < 10) {
            fechadisp = fechadisp + "-0" + String.valueOf(mes);
        } else {
            fechadisp = fechadisp + "-" + String.valueOf(mes);
        }
        if (dia < 10) {
            fechadisp = fechadisp + "-0" + String.valueOf(dia);
        } else {
            fechadisp = fechadisp + "-" + String.valueOf(dia);
        }
        if (h < 10) {
            horadisp = "0" + String.valueOf(h);
        } else {
            horadisp = String.valueOf(h);
        }
        if (m < 10) {
            horadisp = horadisp + ":0" + String.valueOf(m);
        } else {
            horadisp = horadisp + ":" + String.valueOf(m);
        }

        ////System.out.println(fechadisp+" "+horadisp+":"+fi.substring(17,19));
        return fechadisp + " " + horadisp + ":" + fi.substring(17, 19);
    }

    public static int DiasMes(int m) {
        int dias = 0;
        /*Si es biciesto el a�o quedo pendiente*/
        switch (m) {
            case 1:
                dias = 31;
                break;
            case 2:
                dias = 28;
                break;
            case 3:
                dias = 31;
                break;
            case 4:
                dias = 30;
                break;
            case 5:
                dias = 31;
                break;
            case 6:
                dias = 30;
                break;
            case 7:
                dias = 31;
                break;
            case 8:
                dias = 31;
                break;
            case 9:
                dias = 30;
                break;
            case 10:
                dias = 31;
                break;
            case 11:
                dias = 30;
                break;
            case 12:
                dias = 31;
                break;
        }
        return dias;
    }

    public static int AjustaraEntero(double i) {
        int j = (int) i;
        double r, aux;
        String s;
        int t;
        aux = (double) j;
        r = i - aux;
        if (r >= 0.5) {
            r = aux + 1.0;
        } else {
            r = aux;
        }

        s = String.valueOf(r);
        t = Integer.parseInt(s);
        return t;
    }

    public static String cliente(String c) {
        return "000" + c.substring(0, 3);
    }

    public static String clase(String c) {
        return c.substring(1, c.length());
    }

    public static String codTipo(String c) {
        return c.substring(0, 1);
    }

    public static Date ConvertiraDate(String s) {
        int ano = Integer.parseInt(s.substring(0, 4)) - 1900;
        int mes = Integer.parseInt(s.substring(5, 7));
        int dia = Integer.parseInt(s.substring(8, 10));
        int h = Integer.parseInt(s.substring(11, 13));
        int m = Integer.parseInt(s.substring(14, 16));
        int se = Integer.parseInt(s.substring(17, 19));
        Date d = new Date(ano, mes, dia, h, m, se);

        return d;
    }

    public static String ConvertirToString(java.sql.Timestamp t) {
        return t.toString();
    }

    public static java.sql.Timestamp ConvertiraTimestamp(String s) {
        int ano = Integer.parseInt(s.substring(0, 4)) - 1900;
        int mes = Integer.parseInt(s.substring(5, 7)) - 1;
        int dia = Integer.parseInt(s.substring(8, 10));
        int h = Integer.parseInt(s.substring(11, 13));
        int m = Integer.parseInt(s.substring(14, 16));
        int se = Integer.parseInt(s.substring(17, 19));

        java.sql.Timestamp d = new Timestamp(ano, mes, dia, h, m, se, 0);
        return d;
    }

    /////////////apll
    ///Convierte un String en un Calendar
    public static Calendar JCalendar(String fechaEnFormatoDate) {
        String[] v = fechaEnFormatoDate.split(" ");
        String vFecha[] = v[0].split("-");
        int a�o = Integer.parseInt(vFecha[0]);
        int mes = Integer.parseInt(vFecha[1]);
        int dia = Integer.parseInt(vFecha[2]);
        GregorianCalendar c = new GregorianCalendar(a�o, mes, dia);
        return c;
    }
    //Suma n dias al un calendar

    public static Calendar AddCalendar(Calendar c, int d) {
        c.add(Calendar.DAY_OF_MONTH, d);
        return c;
    }

    public static String crearStringFechaDate(Calendar c) {
        return c.get(c.YEAR) + "-"
                + (c.get(c.MONTH) > 9 ? "" + c.get(c.MONTH) : "0" + c.get(c.MONTH)) + "-"
                + (c.get(c.DATE) > 9 ? "" + c.get(c.DATE) : "0" + c.get(c.DATE));
    }

    public static Calendar FechaD(Calendar c, double horas) {
        int h = c.HOUR_OF_DAY;
        int m = c.MINUTE;
        int ph = (int) horas;
        double residuo = h - (double) ph;
        int min = (int) residuo * 60;
        ////System.out.println("HORA " + h + " MINUTOS " + m);
        ////System.out.println("PH " + ph + "RESIDUO" + min);
        c.add(c.MINUTE, min);
        c.add(c.HOUR_OF_DAY, ph);
        return c;
    }//Modificacion alejandro  16-ene-2006

    public static String getFecha(String fecha, int dias) throws ParseException {
        SimpleDateFormat FMT = new SimpleDateFormat("yyyyMMdd");
        Calendar calendario = Calendar.getInstance();
        try {
            calendario.setTime(FMT.parse(fecha));
            calendario.add(Calendar.DATE, dias);
        } catch (Exception e) {
        }
        return FMT.format(calendario.getTime());
    }
    //Modificacion alejandro  16-ene-2006

    public static String getFechaFormatoyyyyMMdd(String fecha) {
        String numero = "0";
        if (!fecha.equals("")) {
            numero = fecha.substring(0, 4) + fecha.substring(5, 7) + fecha.substring(8, 10);
        }
        return numero;
    }

    //METODO QUE RECIVE UN STRING Y RETORNA UNA CLAVE ALFANUMERICA DE 6 DIGITOS
    public static String getClave(String texto1, String texto2) throws UnsupportedEncodingException {
        //FORMA PABON

        String nclave = "No se pudo generar";
        String letras = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                +//26 LETRAS
                "0123456789"
                +//10 NUMEROS
                "012345678901234567890123456789"
                +//26 LETRAS
                "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                +//26 LETRAS
                "0123456789"
                +//10 NUMEROS
                "012345678901234567890123456789"
                +//26 LETRAS
                "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                +//26 LETRAS
                "0123456789"
                +//10 NUMEROS
                "012345678901234567890123456789"
                +//26 LETRAS
                "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                +//26 LETRAS
                "0123456789"
                +//10 NUMEROS
                "012345678901234567890123456789"
                + "01234567";//8 NUMEROS;
        byte[] vec = letras.getBytes("US-ASCII");

        //PRIMERO SE HACE UN XOR ENTRE EL MISMO TEXTO.
        int tamano = texto1.length();
        int mitad = tamano / 2;
        int mod = tamano % 2;

        String texto1a = texto1.substring(0, mitad);
        String texto1b = texto1.substring(mitad, tamano - mod);
        byte k1[] = texto1a.getBytes("US-ASCII");
        byte a1[] = texto1b.getBytes("US-ASCII");
        byte[] e1 = new byte[a1.length];
        for (int i = 0; i < a1.length; i++) {
            e1[i] = (byte) (a1[i] ^ k1[i]);
        }
        byte[] nbyte1 = new byte[8];
        int j = 0;
        for (int i = 0; i < e1.length && j < 8; i++) {
            nbyte1[j] = vec[e1[i]];
            j++;

        }

        String debyte1 = new String(nbyte1);

        //SEGUNDO TEXTO
        tamano = texto2.length();
        mitad = tamano / 2;
        mod = tamano % 2;

        String texto2a = texto2.substring(0, mitad);
        String texto2b = texto2.substring(mitad, tamano - mod);
        byte k2[] = texto2a.getBytes("US-ASCII");
        byte a2[] = texto2b.getBytes("US-ASCII");
        byte[] e2 = new byte[a2.length];
        for (int i = 0; i < a2.length; i++) {
            e2[i] = (byte) (a2[i] ^ k2[i]);
        }
        byte[] nbyte2 = new byte[8];
        j = 0;
        for (int i = 0; i < e2.length && j < 8; i++) {
            nbyte2[j] = vec[e2[i]];
            j++;

        }

        String debyte2 = new String(nbyte2);

        byte k[] = debyte1.getBytes("US-ASCII");
        byte a[] = debyte2.getBytes("US-ASCII");
        byte[] e = new byte[a.length];
        for (int i = 0; i < a.length; i++) {
            e[i] = (byte) (a[i] ^ k[i]);
        }
        byte[] nbyte = new byte[8];
        j = 0;
        for (int i = 0; i < e.length && j < 8; i++) {
            nbyte[j] = vec[e[i]];
            j++;

        }

        String debyte = new String(nbyte);

        nclave = debyte;

        return nclave;
    }

    //METODO QUE RECIVE UN STRING UNA CANTIDAD DE CARACTERES A REMPLAZAR
    //Y LOS REMPLAZA
    public String reemplazar(String clave) {
        String nstring = "";
        for (int i = 0; i < clave.length(); i++) {
            if ((clave.charAt(i) >= 'A' && clave.charAt(i) <= 'Z')
                    || (clave.charAt(i) >= 'a' && clave.charAt(i) <= 'z')
                    || (clave.charAt(i) >= '0' && clave.charAt(i) <= '1')) {
                nstring = nstring + clave.charAt(i);
            }
        }
        return nstring;
    }

    public static String LLamarVentana(String param, String encabezado) {
        return "/auxiliar.jsp?encabezado=" + encabezado + "&dir=" + param.replaceAll("&", "-_-");
    }

    public static String fechaFinal(String fechai, int n) {
        String timeStamp = fechai;
        SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Calendar c = Util.crearCalendar(timeStamp);
        c.add(c.DATE, n);
        java.sql.Timestamp sqlTimestamp = new java.sql.Timestamp(c.getTime().getTime());
        String fechaf = "" + sqlTimestamp;
        return fechaf;
    }

    public static String convertidorParametros(String param) {
        return param.replaceAll("&", "-_-");
    }

    //David 23.12.05
    /**
     * Este m�todo redondea un valor tipo Float dada la escala
     *
     * @autor : Ing. David Lamadrid
     * @version : 1.0
     */
    public static float redondear1(float valor, int escala) {
        float rval = 0;
        BigDecimal bd = null;
        bd = BigDecimal.valueOf(valor).setScale(escala, RoundingMode.HALF_UP);
        //JOptionPane.showMessageDialog(null, );
        rval = bd.floatValue();
        return rval;
    }

    //David 23.12.05
    /**
     * Este m�todo retorna un Calendar con el formato de fecha de sql dada un String con la fecha
     *
     * @autor : Ing. David Lamadrid
     * @version : 1.0
     */
    public static Calendar crearCalendarYYYYMMDD(String fechaEnFormatoPostgres) {
        try {
            String[] v = fechaEnFormatoPostgres.split(" ");
            String vFecha[] = v[0].split("-");

            int a�o = Integer.parseInt(vFecha[0]);
            int mes = Integer.parseInt(vFecha[1]) - 1;
            int dia = Integer.parseInt(vFecha[2]);
            GregorianCalendar c = new GregorianCalendar(a�o, mes, dia);
            //c.set(c.
            //////System.out.println(fechaEnFormatoPostgres+" es pm? "+(c.get(c.AM_PM) == c.PM? "si": "no"));
            return c;
        } catch (Exception ex) {
            ////System.out.println("Fecha recibida: "+fechaEnFormatoPostgres);
            ex.printStackTrace();
            return new GregorianCalendar();
        }

    }

    /**
     * Este m�todo retorna un String con fecha final dada una fecha inicial con el numero de dias (n)
     *
     * @autor : Ing. David Lamadrid
     * @version : 1.0
     */
    public static String fechaFinalYYYYMMDD(String fechai, int n) {
        String timeStamp = fechai;
        SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
        Calendar c = Util.crearCalendarYYYYMMDD(timeStamp);

        c.add(c.DATE, n);
        ////System.out.println("A�o: "+c.getTime().getYear()+"Mes: "+c.getTime().getMonth()+"Dia: "+c.getTime().getDay());
        java.sql.Timestamp sqlTimestamp = new java.sql.Timestamp(c.getTime().getTime());
        ////System.out.println("fecha: "+sqlTimestamp);
        String fechaf = "" + sqlTimestamp;
        return fechaf;
    }

    /**
     *
     *
     */
    public static String sumarHorasFecha(String fechai, double n) {

        String timeStamp = fechai;
        SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Calendar c = Util.crearCalendar(timeStamp);
        double minutos = 60 * n;
        int minutosInt = (int) minutos;
        c.add(c.MINUTE, minutosInt);
        java.sql.Timestamp sqlTimestamp = new java.sql.Timestamp(c.getTime().getTime());
        String fechaf = "" + sqlTimestamp;
        return fechaf;

    }

    /**
     * Obtiene los d�as transcurridos hasta la fecha actual del sistema
     *
     * @autor Ing. Tito Andr�s Maturana
     * @param fecha Fecha de inicio del per�do
     * @returns El n�mero de d�as trancurridos
     * @version 1.0
     */
    public static int diasTranscurridos(String fecha) {
        Calendar fec = crearCalendar(fecha);
        Calendar hoy = Calendar.getInstance();

        long f1ToMilis = fec.getTimeInMillis();
        long f2ToMilis = hoy.getTimeInMillis();

        return (Math.round((f2ToMilis - f1ToMilis) / 86400000));

    }

    /**
     * Obtiene los minutos transcurridos entre dos fechas
     *
     * @autor Ing. Tito Andr�s Maturana
     * @param fechaI Fecha de inicio del per�do
     * @param fechaF Fecha final del per�odo
     * @returns El n�mero de minutos trancurridos
     * @version 1.0
     */
    public static long minutosTranscurridos(String fechaI, String fechaF) {

        Calendar fecI = crearCalendar(fechaI);
        Calendar fecF = crearCalendar(fechaF);

        long f1ToMilis = fecI.getTimeInMillis();
        long f2ToMilis = fecF.getTimeInMillis();

        return (Math.round((f2ToMilis - f1ToMilis) / 60000));

    }

    /**
     * **********************************************************************
     * Metodo : addFecha, decrementa la fecha inicial de la prefactura para poder tomar las boletas viejas
     *
     * @autor : Ing. Ivan Dario Gomez Vanegas
     * @param : la fecha, el incremento en este caso lengthmandamos un numero negativo, y el tipo, si es a�o mes o dia
     * @version : 1.0 ***********************************************************************
     */
    public static String addFecha(String Fecha, int Incremento, int Tipo) {
        String[] Fec = (Fecha).split("-|/");
        SimpleDateFormat FMT = new SimpleDateFormat("yyyy-MM-dd");
        Fec[Tipo] = String.valueOf(Integer.parseInt(Fec[Tipo]) + Incremento);
        return FMT.format(new Date(Integer.parseInt(Fec[0]) - 1900, Integer.parseInt(Fec[1]) - 1, Integer.parseInt(Fec[2]))).toString();
    }

    /**
     * @Metodo que retorna la fecha con el nombre del mes dada en formato de SQL
     * @autor Ing. David Lamadrid
     * @param String cadena:fecha en formato SQL
     * @returns El n�mero de minutos trancurridos
     * @version 1.0
     */
    public static String fechaPorNombre(String cadena) {
        String fecha = "";
        int mes = Integer.parseInt(cadena.substring(5, 7));
        String nM = NombreMes(mes);
        fecha = cadena.substring(8, cadena.length()) + " de " + nM + " de " + cadena.substring(0, 4);
        return fecha;
    }

    /**
     * <PRE>
     *         Displays the current date as a string.
     * </PRE>
     *
     * @return Current date.
     */
    public static String getCurrentDate() {
        return showTimestamp("date");
    }

    /**
     * <PRE>
     *         Displays the current time as a string.
     * </PRE>
     *
     * @return Current time.
     */
    public static String getCurrentTime() {
        return showTimestamp("time");
    }

    /**
     * <PRE>
     *         Retrieves the current date and time as a string.
     * </PRE>
     *
     * @return Current timestamp.
     */
    public static String getFullDate() {
        return (getCurrentDate() + " " + getCurrentTime());
    }

    /**
     * Devuelve un objeto <CODE>java.util.Calendar</CODE> creado a partir de un String en formato AAAAMMDD, sus horas, minutos y segundos son iguales a cero.
     *
     * @param fechaAAAMMDD La fecha en formato AAAAMMDD
     * @return El calendar equivalente al String dado.
     */
    public static Calendar obtenerCalendar(String fechaAAAMMDD) {
        try {
            int a�o = Integer.parseInt(fechaAAAMMDD.substring(0, 4));
            int mes = Integer.parseInt(fechaAAAMMDD.substring(4, 6));
            int dia = Integer.parseInt(fechaAAAMMDD.substring(6, 8));
            Calendar c = new GregorianCalendar(a�o, mes - 1, dia, 0, 0, 0);
            return c;
        } catch (Exception ex) {
            throw new RuntimeException("La fecha '" + fechaAAAMMDD + "' no coincide con el formato exigido AAAAMMDD: " + ex.getMessage());
        }
    }

    /**
     * Permite saber la diferencia en d�as entre dos objetos <CODE>java.util.Calendar</CODE>
     *
     * @param mayor La fecha mayor de las dos.
     * @param menor La fecha menor de las dos
     * @return El n�mero de d�as de diferencia que hay entre la fecha menor y la fecha mayor.
     */
    public static int obtenerDiferenciaEnDias(Calendar mayor, Calendar menor) {
        int dias = 0;
        Calendar menorAux = (Calendar) menor.clone();
        while (menorAux.before(mayor)) {
            menorAux.add(menor.DATE, 1);
            dias++;
        }
        return dias;
    }

    /**
     * Metodo que devuelve la descripcion del tipo de dato de un campo de la base de datos
     *
     * @autor mfontalvo
     * @fecha 2006-01-26
     * @return devuelve en texto el tipo de dato
     * @param Tipo Tipo de dato
     */
    public static String descripcionTipoCampo(int Tipo) {
        String retorno = "";
        switch (Tipo) {
            case java.sql.Types.ARRAY:
                retorno = "ARRAY";
                break;
            case java.sql.Types.BIGINT:
                retorno = "BIGINY";
                break;
            case java.sql.Types.BINARY:
                retorno = "BINARY";
                break;
            case java.sql.Types.BIT:
                retorno = "BIT";
                break;
            case java.sql.Types.BLOB:
                retorno = "BLOB";
                break;
            case java.sql.Types.BOOLEAN:
                retorno = "BOOLEAN";
                break;
            case java.sql.Types.CHAR:
                retorno = "CHAR";
                break;
            case java.sql.Types.CLOB:
                retorno = "CLOB";
                break;
            case java.sql.Types.DATALINK:
                retorno = "DATALINK";
                break;
            case java.sql.Types.DATE:
                retorno = "DATE";
                break;
            case java.sql.Types.DECIMAL:
                retorno = "DECIMAL";
                break;
            case java.sql.Types.DISTINCT:
                retorno = "DISTINCT";
                break;
            case java.sql.Types.DOUBLE:
                retorno = "DOUBLE";
                break;
            case java.sql.Types.FLOAT:
                retorno = "FLOAT";
                break;
            case java.sql.Types.INTEGER:
                retorno = "INTEGER";
                break;
            case java.sql.Types.JAVA_OBJECT:
                retorno = "JAVA_OBJECT";
                break;
            case java.sql.Types.LONGVARBINARY:
                retorno = "LONGVARBINARY";
                break;
            case java.sql.Types.LONGVARCHAR:
                retorno = "LONGVARCHAR";
                break;
            case java.sql.Types.NULL:
                retorno = "NULL";
                break;
            case java.sql.Types.NUMERIC:
                retorno = "NUMERIC";
                break;
            case java.sql.Types.OTHER:
                retorno = "OTHER";
                break;
            case java.sql.Types.REAL:
                retorno = "REAL";
                break;
            case java.sql.Types.REF:
                retorno = "REF";
                break;
            case java.sql.Types.SMALLINT:
                retorno = "SMALLINT";
                break;
            case java.sql.Types.STRUCT:
                retorno = "STRUCT";
                break;
            case java.sql.Types.TIME:
                retorno = "TIME";
                break;
            case java.sql.Types.TIMESTAMP:
                retorno = "TIMESTAMP";
                break;
            case java.sql.Types.TINYINT:
                retorno = "TINYINT";
                break;
            case java.sql.Types.VARBINARY:
                retorno = "VARBINARY";
                break;
            case java.sql.Types.VARCHAR:
                retorno = "VARCHAR";
                break;
            default:
                retorno = "no difined";
                break;
        }
        return retorno;
    }

    /**
     * Convierte a una instancia de tipo <code>java.util.Date</code>
     *
     * @autor Ing. Tito Andr�s Maturana
     * @parama s Fecha de tipo <code>String</code> en formato yyyy-mm-dd kk:mm:ss
     * @version 1.0
     */
    public static Date ConvertiraDate0(String s) {
        int ano = Integer.parseInt(s.substring(0, 4)) - 1900;
        int mes = Integer.parseInt(s.substring(5, 7)) - 1;
        int dia = Integer.parseInt(s.substring(8, 10));
        int h = Integer.parseInt(s.substring(11, 13));
        int m = Integer.parseInt(s.substring(14, 16));
        int se = Integer.parseInt(s.substring(17, 19));
        Date d = new Date(ano, mes, dia, h, m, se);

        return d;
    }

    /**
     * Formatea un long en formato de moneda.
     *
     * @autor Ing. Tito Andr�s Maturana
     * @param value Valor a formatear
     * @returns El valor formateado
     * @version 1.0
     */
    public static String customFormat0(long value) {
        String valor = "" + value;
        String res = "";

        for (int i = valor.length() - 1, j = 0; i >= 0; i--, j++) {
            if (j % 3 == 0 && j != 0) {
                res += ",";
            }
            res += valor.charAt(i);
        }
        //ahora nos toca invertir el numero;

        String aux = "";
        for (int i = res.length() - 1; i >= 0; i--) {
            aux += res.charAt(i);
        }

        return aux;
    }

    /**
     * Metodo que transforma un arreglo de string en un treemap
     *
     * @autor mfontalvo
     * @fecha 2006-08-08
     * @param array arreglo de datos
     * @return devuelve el nuevo TreeMap
     * @throws Exception .
     */
    public static TreeMap ArrayToTreeMap(String[] array) {
        TreeMap nuevo = null;
        if (array != null) {
            nuevo = new TreeMap();
            for (int i = 0; i < array.length; i++) {
                nuevo.put(array[i], array[i]);
            }
        }
        return nuevo;
    }

    /**
     * Convierte a una instancia de tipo <code>java.util.Date</code>
     *
     * @autor Ing. Tito Andr�s Maturana
     * @parama s Fecha de tipo <code>String</code> en formato yyyy-mm-dd kk:mm:ss
     * @version 1.0
     */
    public static Date ConvertiraDate1(String s) {
        int ano = Integer.parseInt(s.substring(0, 4)) - 1900;
        int mes = Integer.parseInt(s.substring(5, 7)) - 1;
        int dia = Integer.parseInt(s.substring(8, 10));
        Date d = new Date(ano, mes, dia);

        return d;
    }

    /**
     * Metodo que convierte un numero decimal a un numero de otra base, considere que la base maxima para este procedimineto es la base 26
     *
     * @autor mfontalvo
     * @param num, numero que desea convertir
     * @param base, base a convertir
     * @return String, que es el numero convertido
     * @see convert(int), metodo para convertir los numeros a letras
     */
    public static String DecimalToBase(String num, int base) {
        StringBuffer numero = new StringBuffer();
        BigInteger aux = new BigInteger(num);
        while (aux.compareTo(BigInteger.valueOf(0)) == 1) {
            numero.append(convert(aux.mod(BigInteger.valueOf(base)).intValue()));
            aux = aux.divide(BigInteger.valueOf(base));
        }
        return ((StringBuffer) numero.reverse()).toString();
    }

    /**
     * Metodo para sacar la equivalencia de un caracter decimal en un caracter de notacion
     *
     * @autor mfontalvo
     */
    public static char convert(int numero) {
        if (numero >= 10) {
            return (char) (numero + 55);
        } else {
            return (char) (numero + 48);
        }
    }

    /**
     * Formatea una fecha en formato YYYYMMDD a YYYY-MM-DD
     *
     * @autor Ing. Mario Fontalvo
     * @parama Fecha en formato YYYYMMDD
     * @version 1.0
     */
    public static String fechaPostgres(String YYYYMMDD) {
        if (YYYYMMDD != null && !YYYYMMDD.trim().equals("")) {
            Calendar cal = obtenerCalendar(YYYYMMDD);
            return DATE_FORMAT.format(cal.getTime());
        } else {
            return "0099-01-01";
        }
    }

    public static String formatoFechaTimestamp(String fecha) {
        String formato = "0099-01-01 00:00";
        if (fecha != null && !fecha.equals("")) {
            formato = fecha.substring(0, 4) + "-" + fecha.substring(5, 7) + "-" + fecha.substring(8, 10) + " " + fecha.substring(11, 13) + ":" + fecha.substring(14, 16);
        }
        return formato;
    }

    /*@Autor: Ivan Gomez
     *@fecha: 20 - Jun - 2006*/

    public static String getPeriodo(int tipo) throws Exception {
        String periodo = "";
        try {
            String a�o = getFechaActual_String(1);
            String mes = getFechaActual_String(3);
            String nomMes = NombreMes(Integer.parseInt(mes)).substring(0, 3);

            switch (tipo) {
                case 1:
                    periodo = a�o + mes;
                    break;
                case 2:
                    periodo = nomMes + "/" + a�o;
                    break;
            }
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }

        return periodo;
    }

    /**
     * M�todo para redondear valores a la cantidad de decimal establecida
     *
     * @autor fvillacob
     * @param double valor a redondear, int cantDecimal cantidad de decimal a redondear dentro del valor
     * @return double (valor redondeado)
     * @version 1.0.
     *
     */
    public static double roundByDecimal(double valor, int cantDecimal) {
        BigDecimal bd = null;
        bd = BigDecimal.valueOf(valor).setScale(cantDecimal, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    /**
     * Metodo para validar campos nulos
     *
     * @autor mfontalvo
     * @param variable variable a procesar
     * @param strdefault campos default a retornar
     */
    public static String coalesce(String variable, String strdefault) {
        return (variable != null ? variable : strdefault);
    }

    /**
     * Metodo que recibe una fecha y retorna la misma.
     *
     * @autor Karen Reales
     * @param Fecha a retornar
     */
    public static String fecha_Zonificada(String fecha, String usuario) {

        String[] v = fecha.split(" ");
        String vFecha[] = v[0].split("-");
        String vHora[] = v[1].split(":");

        int a�o = Integer.parseInt(vFecha[0]);
        int mes = Integer.parseInt(vFecha[1]) - 1;
        int dia = Integer.parseInt(vFecha[2]);
        int hora = Integer.parseInt(vHora[0]);
        int minuto = Integer.parseInt(vHora[1]);

        GregorianCalendar c = new GregorianCalendar(a�o, mes, dia, hora, minuto, 0);
        String nuevaFecha = "";
        Hashtable h = null;
        try {

            com.tsp.operation.model.services.UsuarioService uSer = new com.tsp.operation.model.services.UsuarioService();
            h = uSer.getHoraZonificada(usuario);

        } catch (Exception e) {
            e.printStackTrace();

        }

        if (h != null) {
            //System.out.println("Se encontro el usuario");
            int horas = Integer.parseInt((String) h.get("horas"));
            int minutos = Integer.parseInt((String) h.get("minutos"));
            int segundos = Integer.parseInt((String) h.get("segundos"));

            //System.out.println("Sumar: h:"+horas+" m:"+minutos+" s:"+segundos);
            c.add(c.HOUR, horas);
            c.add(c.MINUTE, minutos);
            c.add(c.SECOND, segundos);
        } else {
            //System.out.println("No encontre al usuario: "+usuario);
        }

        Date d = c.getTime();

        SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        nuevaFecha = s.format(d);

        return nuevaFecha;

    }

    /**
     * Metodo que crea una clase y la inicializa en el constructor
     *
     * @autor mfontalvo
     * @fecha 2006-01-28
     * @param className nombre de la clase
     * @return devuelve la nueva clase inicializada
     * @throws Exception .
     */
    public static Object runClass(String className) throws Exception {
        Class clase = null;
        try {
            clase = Class.forName(className);
            return clase.newInstance();
        } catch (ClassNotFoundException e) {
            throw new Exception("No se pudo cargar la clase " + className + "; " + e.getMessage());
        } catch (InstantiationException e) {
            throw new Exception("No se pudo instanciar " + className + ": " + e.getMessage());
        } catch (IllegalAccessException e) {
            throw new Exception(className + ": " + e.getMessage());
        }
    }

    /**
     * *******************************************************
     * Metodo diasDelMes, retorna el numero de dias que tiene el mes
     *
     * @author: Osvaldo P�rez Ferrer
     * @param: int mes, numero del mes
     * @return: int, numero de dias del mes *******************************************************
     */
    public static int diasDelMes(int mes) {
        Calendar cal = Calendar.getInstance();
        cal.set(cal.get(1), (mes - 1), 10);
        return cal.getActualMaximum(cal.DATE);
    }

    /**
     * *******************************************************
     * Metodo diasDelMes, retorna el numero de dias que tiene el mes
     *
     * @author: Osvaldo P�rez Ferrer
     * @param: int mes, numero del mes
     * @param: int anio, a�o que se desee consultar
     * @return: int, numero de dias del mes *******************************************************
     */
    public static int diasDelMes(int mes, int anio) {
        Calendar cal = Calendar.getInstance();
        cal.set(anio, (mes - 1), 10);
        return cal.getActualMaximum(cal.DATE);
    }

    public static String getFechaActual_Letras() {
        String fecha = NombreMes(Integer.parseInt(getFechaActual_String(3))) + " " + getFechaActual_String(5) + " DE " + getFechaActual_String(1);
        return fecha.toUpperCase();
    }
    //lreales

    public static int diasMes(int m) {
        int dias = 0;
        /*Si es biciesto el a�o quedo pendiente*/
        switch (m) {
            case 1:
                dias = 31;
                break;
            case 2:
                dias = 28;
                break;
            case 3:
                dias = 31;
                break;
            case 4:
                dias = 30;
                break;
            case 5:
                dias = 31;
                break;
            case 6:
                dias = 30;
                break;
            case 7:
                dias = 31;
                break;
            case 8:
                dias = 31;
                break;
            case 9:
                dias = 30;
                break;
            case 10:
                dias = 31;
                break;
            case 11:
                dias = 30;
                break;
            case 12:
                dias = 31;
                break;
        }
        return dias;
    }
    //Lreales

    public static String fechaMenosUnDia(String fi) {

        int ano = Integer.parseInt(fi.substring(0, 4));
        int mes = Integer.parseInt(fi.substring(5, 7));
        int dia = Integer.parseInt(fi.substring(8, 10));

        String a = "";
        String m = "";
        String di = "";

        int r = dia - 1;

        a = String.valueOf(ano);
        m = String.valueOf(mes);
        di = String.valueOf(r);

        if ((r == 0) && (mes != 1)) {
            dia = diasMes(mes - 1);
            di = String.valueOf(dia);
            m = String.valueOf(mes - 1);
        }
        if ((r == 0) && (mes == 1)) {
            ano = ano - 1;
            mes = 12;
            dia = 31;
            a = String.valueOf(ano);
            m = String.valueOf(mes);
            di = String.valueOf(dia);
        }
        mes = Integer.parseInt(m);
        if (mes < 10) {
            m = "0" + m;
        }
        dia = Integer.parseInt(di);
        if (dia < 10) {
            di = "0" + di;
        }
        String res = a + "-" + m + "-" + di;

        return res;
    }

    /**
     * *******************************************************
     * Metodo toHours, retorna el numero de horas que representa la cadena 'interval' dada; la cadena debe ser de tipo interval Ej: '10 days 19:39:00'
     *
     * @author: Osvaldo P�rez Ferrer
     * @param: String interval, cadena interval
     * @return: double, numero de horas, retorna -1 si la cadena es incorrecta *******************************************************
     */
    public static double toHours(String interval) {

        String[] v = interval.split("days|day");

        double horas = 0;

        try {
            if (v.length == 2) {
                horas += Math.abs(Integer.parseInt(v[0].trim()) * 24);

                v = v[1].split(":");
                if (v.length == 3) {
                    horas += Double.parseDouble(v[0].trim());
                    horas += Double.parseDouble(v[1].trim()) / 60;
                    horas += Double.parseDouble(v[2].trim()) / 3600;
                } else {
                    return -1;
                }
            } else {
                v = interval.split(":");
                if (v.length == 3) {
                    horas += Double.parseDouble(v[0].trim());
                    horas += Double.parseDouble(v[1].trim()) / 60;
                    horas += Double.parseDouble(v[2].trim()) / 3600;
                } else {
                    return -1;
                }
            }

        } catch (Exception ex) {
            return -1;
        }

        return horas;
    }

    /**
     * *******************************************************
     * Metodo toVector, recibe un arreglo de string y lo retorna en un Vector
     *
     * @author: Osvaldo P�rez Ferrer
     * @param: String[] arreglo de String
     * @param: boolean si se desea hacer trim a cada elemento
     * @return: Vector, el string[] convertido a Vector *******************************************************
     */
    public static Vector toVector(String[] split, boolean trim) {
        Vector v = new Vector();
        for (int i = 0; i < split.length; i++) {
            v.add(trim == true ? split[i].trim() : split[i]);
        }
        return v;
    }

    public static String getCurrentTime12() {
        return showTimestamp("time12");
    }

    public static String showTimestamp(String timestampValue) {
        Calendar cal = Calendar.getInstance();
        String tStamp = null;
        String month = null;
        switch (cal.get(Calendar.MONTH)) {
            case Calendar.JANUARY:
                month = "January ";
                break;
            case Calendar.FEBRUARY:
                month = "February ";
                break;
            case Calendar.MARCH:
                month = "March ";
                break;
            case Calendar.APRIL:
                month = "April ";
                break;
            case Calendar.MAY:
                month = "May ";
                break;
            case Calendar.JUNE:
                month = "June ";
                break;
            case Calendar.JULY:
                month = "July ";
                break;
            case Calendar.AUGUST:
                month = "August ";
                break;
            case Calendar.SEPTEMBER:
                month = "September ";
                break;
            case Calendar.OCTOBER:
                month = "October ";
                break;
            case Calendar.NOVEMBER:
                month = "November ";
                break;
            case Calendar.DECEMBER:
                month = "December ";
                break;
        }
        if (timestampValue.equalsIgnoreCase("date")) {
            tStamp = month + Integer.toString(cal.get(Calendar.DAY_OF_MONTH)) + ", "
                    + Integer.toString(cal.get(Calendar.YEAR));
        } else if (timestampValue.equalsIgnoreCase("time")) {
            tStamp = Integer.toString(cal.get(Calendar.HOUR_OF_DAY)) + ":"
                    + Integer.toString(cal.get(Calendar.MINUTE)) + ":"
                    + Integer.toString(cal.get(Calendar.SECOND));
        } else if (timestampValue.equalsIgnoreCase("time12")) {
            String estado = (cal.get(Calendar.HOUR_OF_DAY) > 12) ? "p.m" : "a.m";
            String min = (cal.get(Calendar.MINUTE) <= 9) ? "0" + cal.get(Calendar.MINUTE) : Integer.toString(cal.get(Calendar.MINUTE));

            tStamp = Integer.toString(cal.get(Calendar.HOUR)) + ":"
                    + min + " " + estado;

        } else {
            tStamp = "";
        }
        return tStamp;
    }

    public static double redondearByMoneda(double valor, String moneda) {
        double val = 0;
        if (moneda.equals("DOL")) {
            val = com.tsp.util.Util.roundByDecimal(valor, 2);
        } else {
            val = Math.round(valor);
        }
        return val;
    }

    /**
     * *************************************
     * @author: Ing. Osvaldo P�rez Ferrer Metodo que define si la cadena dada corresponde a un numero *************************************
     */
    public static boolean esNumero(String value) {
        try {
            Double.parseDouble(value);
            return true;
        } catch (Exception ex) {
            return false;
        }
    }

    /**
     * *************************************
     * @author: Ing. Osvaldo P�rez Ferrer Metodo que define si el caracter dado corresponde a un numero *************************************
     */
    public static boolean esNumero(char value) {
        try {
            Double.parseDouble(String.valueOf(value));
            return true;
        } catch (Exception ex) {
            return false;
        }
    }

    /**
     * *************************************************************************
     * metodo insertFromSelect: Crea querys de insercion a partir de una consulta
     *
     * @author: Ing. Osvaldo P�rez Ferrer
     * @param: tabla1, tabla en la que se va a insertar<br>
     * @param: tabla2, tabla de la que se va a leer<br>
     * @param: columnas, columnas que se van a insertar( separadas por coma ',' )<br>
     * @param: String[] remplazo, columnas del select que se desean reemplazar con algun valor, la sintaxis es "nombre_columna,valor"<br>
     * @param: where, condicional para agregar al final de la consulta( opcional )<br><br>
     *
     * Ej. con los siguientes parametros:<br>
     * <table border=1>
     *
     * <tr><td> tabla1 = "placa" </td></tr>
     * <tr><td> tabla2 = "placa" </td></tr>
     * <tr><td> columnas = "placa,color,propietario,tenedor, ancho, largo"
     * </td></tr>
     * <tr><td> reemplazo = {"placa,'AKK226'::text","tenedor,'22565923'::text"}
     * </td></tr>
     * where = "WHERE proveedor = '22565923'"
     *
     * </table><br>
     * Se obtiene la siguiente consulta :
     * <br>
     *
     * <table>
     * <tr><td colspan='2'> INSERT INTO placa</td></tr>
     * <tr><td colspan='2'> ( </td></tr>
     * <tr><td colspan='2'> placa,color,propietario,tenedor, ancho, largo</td></tr>
     * <tr><td colspan='2'> ) </td></tr>
     * <tr><td colspan='2'> SELECT </td></tr>
     * <tr><td></td> <td> 'AKK226'::text AS placa, </td>
     * <tr><td></td> <td> color, </td>
     * <tr><td></td> <td> propietario, </td>
     * <tr><td></td> <td> '22565923'::text AS tenedor, </td>
     * <tr><td></td> <td> ancho, </td>
     * <tr><td></td> <td> largo </td>
     * <tr><td colspan='2'> FROM placa </td></tr>
     * <tr><td colspan='2'> WHERE placa = 'AKK225' </td></tr>
     * </table> ************************************************************************
     */
    public static String insertFromSelect(String tabla1, String tabla2, String columnas, String[] reemplazo, String where, String schema1, String schema2) throws Exception {

        String sql = "";

        schema1 = schema1.equals("") ? "" : schema1 + ".";
        schema2 = schema2.equals("") ? "" : schema2 + ".";

        sql = "INSERT INTO " + schema1 + tabla1 + "\n"
                + "(\n"
                + columnas
                + "\n)\n"
                + "SELECT"
                + "\n";

        String select = "";

        Vector cols = Util.toVector(columnas.split(","), true);;
        Vector remp = Util.toVector(reemplazo, true);
        String col = "";
        String rem = "";
        boolean sw = false;
        for (int i = 0; i < cols.size(); i++) {
            col = (String) cols.get(i);
            sw = false;
            for (int j = 0; j < remp.size(); j++) {
                rem = (String) remp.get(j);

                String col_reemp = rem.substring(0, rem.indexOf(","));
                if (col_reemp.equals(col)) {
                    select += "\t" + rem.substring((rem.indexOf(",") + 1), rem.length()) + " AS " + col + ",\n";
                    remp.remove(j);
                    j--;
                    cols.remove(i);
                    i--;
                    sw = true;
                    break;

                }
            }
            if (sw == false) {
                select += "\t" + col + ",\n";
            }
        }

        if (select.length() > 3) {
            select = select.substring(0, select.length() - 2);
        }

        sql += select + "\n";

        sql += " FROM " + schema2 + tabla2 + "\n";

        sql += where;

        return sql;
    }

    //Mfontalvo 30-04-07
    /**
     * metodo para formatear un valor
     *
     * @autor mfontalvo
     * @param pattern, formato,
     * @param value, valor
     * @return String, valor formateado.
     */
    public static String customFormat(String pattern, double value) {
        DecimalFormat df = (DecimalFormat) NumberFormat.getNumberInstance(new Locale("en", "US"));
        df.applyPattern(pattern);
        return df.format(value);
    }

    ////Derly
    public static int buscarDiaFechaValida(int dia, int mes, int ano) {
        boolean biciesto = false;
        int day;

        day = dia;

        if (mes == 2) {
            if (ano % 4 == 0) {
                biciesto = true;
            }

            if (dia > 28) {
                if (biciesto) {
                    if (dia != 29) {
                        day = 28;
                    }
                } else {
                    day = 28;
                }
            }
        } else {

            if ((mes != 1) && (mes != 3) && (mes != 5) && (mes != 7) && (mes != 8) && (mes != 10) && (mes != 12)) {
                if (dia == 31) {
                    day = 30;
                }
            }
        }

        return day;
    }

    public static java.util.Date ConvertirFecha(int dia, int mes, int ano) {

        java.util.Date Fecha = new java.util.Date();
        java.util.Calendar Calendario = Calendar.getInstance();

        Calendario.set(ano, mes, dia);

        Fecha = Calendario.getTime();

        return Fecha;
    }

    public static java.util.Calendar TraerFecha(int fila, int dia, int mes, int ano, String dias_primer_cheque) {
        int Dia_Valido, anio = 0;
        int daysInMonth;
        java.util.Date Fecha;
        java.util.Calendar Calendario = Calendar.getInstance();

        if (fila != 1) {
            int numdias_primer_cheque = Integer.parseInt(dias_primer_cheque);
            if (numdias_primer_cheque < 30) {
                dias_primer_cheque = "30";

            }
            if (numdias_primer_cheque > 30) {
                if (numdias_primer_cheque != 45) {
                    mes = mes - 1;
                }
                dias_primer_cheque = "45";
            }
        }
        Calendario.set(ano, mes, dia);
        if (dias_primer_cheque.equals("30")) {
            if (dia == 31) {

                if ((mes == 8) || (mes == 10) || (mes == 3) || (mes == 5)) {
                    Calendario.add(Calendar.MONTH, fila - 1);
                    Calendario.add(Calendar.DAY_OF_YEAR, -1);
                } else {
                    if ((mes == 1)) {
                        Calendario.add(Calendar.MONTH, fila - 1);
                        anio = Calendario.get(Calendar.YEAR);
                        if (fila == 1) {
                            if (anio % 4 == 0) {
                                Calendario.add(Calendar.DAY_OF_YEAR, -2);
                            } else {
                                Calendario.add(Calendar.DAY_OF_YEAR, -3);
                            }
                        } else {

                            if (ano % 4 == 0) {
                                Calendario.add(Calendar.DAY_OF_YEAR, -2);
                            } else {
                                Calendario.add(Calendar.DAY_OF_YEAR, -3);
                            }
                        }
                    } else {
                        Calendario.add(Calendar.MONTH, fila - 1);
                    }
                }
            } else {
                Calendario.add(Calendar.MONTH, fila - 1);
            }

        } else {
            if (fila == 1) {

                Calendario.add(Calendar.DATE, Integer.parseInt(dias_primer_cheque));

            } else {

                Calendario.add(Calendar.MONTH, (fila));
            }
        }
        return Calendario;
    }

    public static String ObtenerFechaCompleta(java.util.Calendar Calendario) {
        String FechaCompleta = null;
        StringTokenizer parametros = null;

        parametros = new StringTokenizer("" + Calendario.getTime());

        String dia = parametros.nextToken();
        String mes = parametros.nextToken();
        String fecha = parametros.nextToken();
        String hora = parametros.nextToken();
        String gmt = parametros.nextToken();
        String a�o = parametros.nextToken();

        if (dia.equals("Mon")) {
            dia = "Lunes";
        } else if (dia.equals("Tue")) {
            dia = "Martes";
        } else if (dia.equals("Wed")) {
            dia = "Mi�rcoles";
        } else if (dia.equals("Thu")) {
            dia = "Jueves";
        } else if (dia.equals("Fri")) {
            dia = "Viernes";
        } else if (dia.equals("Sat")) {
            dia = "S�bado";
        } else {
            dia = "Domingo";
        }
        if (mes.equals("Jan")) {
            mes = "Enero";
        } else if (mes.equals("Feb")) {
            mes = "Febrero";
        } else if (mes.equals("Mar")) {
            mes = "Marzo";
        } else if (mes.equals("Apr")) {
            mes = "Abril";
        } else if (mes.equals("May")) {
            mes = "Mayo";
        } else if (mes.equals("Jun")) {
            mes = "Junio";
        } else if (mes.equals("Jul")) {
            mes = "Julio";
        } else if (mes.equals("Aug")) {
            mes = "Agosto";
        } else if (mes.equals("Sep")) {
            mes = "Septiempre";
        } else if (mes.equals("Oct")) {
            mes = "Octubre";
        } else if (mes.equals("Nov")) {
            mes = "Noviembre";
        } else {
            mes = "Diciembre";
        }

        FechaCompleta = fecha + " de " + mes + " de " + a�o;

        return FechaCompleta;
    }

    public static int diasTranscurridos(java.util.Calendar fechaMenor, java.util.Calendar fechaMayor) {

        java.text.DateFormat df = java.text.DateFormat.getDateInstance(java.text.DateFormat.MEDIUM);
        java.util.Date fechaInicio = null;
        java.util.Date fechaFinal = null;

        String fechaInicioString = df.format(fechaMenor.getTime());
        try {
            fechaInicio = df.parse(fechaInicioString);
        } catch (java.text.ParseException ex) {
        }

        String fechaFinalString = df.format(fechaMayor.getTime());
        try {
            fechaFinal = df.parse(fechaFinalString);
        } catch (java.text.ParseException ex) {
        }

        long fechaInicioMs = fechaInicio.getTime();
        long fechaFinalMs = fechaFinal.getTime();

        long diferencia = fechaFinalMs - fechaInicioMs;

        double dias = Math.floor(diferencia / (1000 * 60 * 60 * 24));

        return ((int) dias);

        //return (Math.round((f2ToMilis - f1ToMilis)/86400000));
    }

    public static double obtenerValor(double dias, double formula) {
        double valor_calculado;
        double valor_1;
        double valor_2;
        double valor_3;

        valor_1 = (dias / 365);
        valor_2 = (1 + formula);
        valor_3 = Math.pow(valor_2, valor_1);
        valor_calculado = ((1 / valor_3));
        System.out.println("valorcalllllllll = " + valor_calculado);
        /*tring val = customFormat(valor_calculado,15);
         double valor = Double.parseDouble(val);*/
        return valor_calculado;
    }

    /**
     * Obtiene los d�as transcurridos hasta la fecha actual del sistema
     *
     * @autor Ing. Andr�s Maturana De La Cruz
     * @param fecha Fecha de inicio del per�do
     * @returns El n�mero de d�as trancurridos
     * @version 1.0
     */
    public static int diasTranscurridos(String fecha1, String fecha2) {
        //System.out.println(fecha2+" menos "+fecha1);
        float result = -1;
        if (fecha1 != null && fecha2 != null) {
            if (fechaMayor(fecha1, fecha2)) {
                Calendar fec1 = crearCalendarDate(fecha1);
                Calendar fec2 = crearCalendarDate(fecha2);
                float f1ToMilis = fec1.getTimeInMillis();
                float f2ToMilis = fec2.getTimeInMillis();
                result = (float) (f2ToMilis - f1ToMilis) / (float) (86400000.00);
                //System.out.println("entro dias "+result);
            }
        }
        return (int) Math.round(result);

    }

    public static int diasMesB(int a, int m) {
        int dias = 0;
        /*Si es biciesto el a�o quedo pendiente*/
        switch (m) {
            case 1:
                dias = 31;
                break;
            case 2:
                dias = 28;
                if (a % 4 == 0) {
                    dias = 29;
                }
                break;
            case 3:
                dias = 31;
                break;
            case 4:
                dias = 30;
                break;
            case 5:
                dias = 31;
                break;
            case 6:
                dias = 30;
                break;
            case 7:
                dias = 31;
                break;
            case 8:
                dias = 31;
                break;
            case 9:
                dias = 30;
                break;
            case 10:
                dias = 31;
                break;
            case 11:
                dias = 30;
                break;
            case 12:
                dias = 31;
                break;
        }
        return dias;
    }

    /**
     * Obtiene los d�as transcurridos hasta la fecha actual del sistema
     *
     * @autor Ing. Julio Barros
     * @param fecha Fecha de inicio del per�do
     * @returns El n�mero de d�as trancurridos
     * @version 1.0
     */
    public static int diasTranscurridosFintra(String fecha1, String fecha2) {
        Long dias = new Long(0);
        //System.out.println(fecha2+" menos "+fecha1);
        float result = -1;
        if (fecha1 != null && fecha2 != null) {
            if (fechaMayor(fecha1, fecha2)) {
                Calendar fec1 = crearCalendarDate(fecha1);
                Calendar fec2 = crearCalendarDate(fecha2);
                long difms = fec2.getTime().getTime() - fec1.getTime().getTime();
                //Convertimos a dias
                long difd = difms / (1000 * 60 * 60 * 24);
                dias = new Long(difd);
                //System.out.println("numero de dias" + dias.intValue());
            }
        }
        return dias.intValue();
    }

    public static boolean fechaMayor(String fec1, String fec2) {
        boolean F2Mayor;
        int a�o1 = Integer.parseInt(fec1.substring(0, 4));
        int mes1 = Integer.parseInt(fec1.substring(5, 7));
        int dia1 = Integer.parseInt(fec1.substring(8, 10));
        int a�o2 = Integer.parseInt(fec2.substring(0, 4));
        int mes2 = Integer.parseInt(fec2.substring(5, 7));
        int dia2 = Integer.parseInt(fec2.substring(8, 10));
        if (a�o1 <= a�o2) {
            if (((a�o2 - a�o1) >= 1) || (mes1 <= mes2 && (a�o2 - a�o1) == 0)) {
                if (((a�o2 - a�o1) >= 1) || ((mes2 - mes1) >= 1) || (dia1 <= dia2 && (mes2 - mes1) == 0)) {
                    return true;
                }
            }
        }
        return false;
    }

    public static Calendar crearCalendarDateF(String fecha) {
        try {
            String[] v = fecha.split(" ");
            String vFecha[] = v[0].split("-");
            int a�o = Integer.parseInt(vFecha[0]);
            int mes = Integer.parseInt(vFecha[1]);
            int dia = Integer.parseInt(vFecha[2]);
            GregorianCalendar c = new GregorianCalendar(a�o, mes, dia);
            return c;
        } catch (Exception ex) {
            ////System.out.println("Fecha recibida: " + fecha);
            ex.printStackTrace();
            return new GregorianCalendar();
        }

    }

    public static String MFecha(String a) {
        String ret = "No Registra";
        int dia, mes, ano;
        String fechai = "";
        if (!a.equals("0099-01-01 00:00:00")) {
            java.util.Calendar Fecha_Negocio = Util.crearCalendarDate(a);
            fechai = a;
            dia = Integer.parseInt(fechai.substring(8, 10));
            mes = Integer.parseInt(fechai.substring(5, 7));
            ano = Integer.parseInt(fechai.substring(0, 4));
            Fecha_Negocio.set(ano, mes - 1, dia);
            ret = ObtenerFechaCompleta(Fecha_Negocio);
        }
        return ret;
    }

    //by rarp
    public static Calendar crearCalendarDate2(String fecha) {
        try {
            String[] v = fecha.split(" ");
            String vFecha[] = v[0].split("-");
            int a�o = Integer.parseInt(vFecha[0]);
            int mes = Integer.parseInt(vFecha[1]);
            int dia = Integer.parseInt(vFecha[2]);
            GregorianCalendar c = new GregorianCalendar(a�o, mes - 1, dia);
            return c;
        } catch (Exception ex) {
            ////System.out.println("Fecha recibida: " + fecha);
            ex.printStackTrace();
            return new GregorianCalendar();
        }

    }
    //by rarp

    public static String NombreMes2(int mes) {
        String nombre = "";
        switch (mes) {
            case 0:
                nombre = "Enero";
                break;
            case 1:
                nombre = "Febrero";
                break;
            case 2:
                nombre = "Marzo";
                break;
            case 3:
                nombre = "Abril";
                break;
            case 4:
                nombre = "Mayo";
                break;
            case 5:
                nombre = "Junio";
                break;
            case 6:
                nombre = "Julio";
                break;
            case 7:
                nombre = "Agosto";
                break;
            case 8:
                nombre = "Septiembre";
                break;
            case 9:
                nombre = "Octubre";
                break;
            case 10:
                nombre = "Noviembre";
                break;
            case 11:
                nombre = "Diciembre";
                break;
        }
        return nombre;

    }

    public static String NombreMes3(int mes) {
        String nombre = "";
        switch (mes) {
            case 1:
                nombre = "Enero";
                break;
            case 2:
                nombre = "Febrero";
                break;
            case 3:
                nombre = "Marzo";
                break;
            case 4:
                nombre = "Abril";
                break;
            case 5:
                nombre = "Mayo";
                break;
            case 6:
                nombre = "Junio";
                break;
            case 7:
                nombre = "Julio";
                break;
            case 8:
                nombre = "Agosto";
                break;
            case 9:
                nombre = "Septiembre";
                break;
            case 10:
                nombre = "Octubre";
                break;
            case 11:
                nombre = "Noviembre";
                break;
            case 12:
                nombre = "Diciembre";
                break;
        }
        return nombre;

    }

    public static synchronized void logController(String user, String clase) {
        if (logFile == null) {
            pw = new PrintWriter(System.err, true);
            logWriter = new LogWriter("Controller", LogWriter.INFO, pw);
            // lineas adicionada por nparejo
            InputStream is = com.tsp.util.connectionpool.PoolManager.getInstance().getClass().getResourceAsStream("db.properties");
            Properties dbProps = new Properties();
            try {
                dbProps.load(is);
            } catch (Exception e) {
                logWriter.log("No se pudo leer el archivo de propiedades. "
                        + "Asegurese que db.properties esta en el CLASSPATH" + is,
                        LogWriter.ERROR);
                return;
            }
            logFile = dbProps.getProperty("logcontroller");
        }
        if (logFile != null) {
            try {
                pw = new PrintWriter(new FileWriter(logFile, true), true);
                logWriter.setPrintWriter(pw);
                logWriter.log("Usuario: " + user + " Clase:" + clase, LogWriter.INFO);
            } catch (IOException e) {
                logWriter.log("No se pudo abrir el log file: " + logFile
                        + ". Usando el System.err por defecto", LogWriter.ERROR);
            }
        }
    }

    public static java.util.Calendar TraerFecha2(int fila, int dia, int mes, int ano) {
        java.util.Calendar Calendario = Calendar.getInstance();

        Calendario.set(ano, mes, dia);
        Calendario.add(Calendar.MONTH, (fila));
        return Calendario;
    }

    public static String crearStringFechaDateLetraRemix2(Calendar c) {
        SimpleDateFormat f = new SimpleDateFormat("dd-MM-yyyy");
        String ret = f.format(c.getTime());
        return (ret);
    }

    public static double redondear2(double valor, int escala) {
        double rval = 0;
        BigDecimal bd = null;
        bd = BigDecimal.valueOf(valor).setScale(escala, RoundingMode.HALF_UP);
        //JOptionPane.showMessageDialog(null, );

        rval = bd.doubleValue();
        return rval;
    }

    public static String FormatoMiles(double aDouble) {
        //Nota: la popiedad indexOf me retorna -1 si no encuantra el caracter '.' en la cadena, osea que si
        //encontro el caracter quiere decir que la cadena es un numero doble o float, en este caso no le aplico el
        //separador de mil, solamente retorno la misma cadena

        BigDecimal bd = new BigDecimal(aDouble);
        bd = bd.setScale(2, BigDecimal.ROUND_HALF_UP);

        String valor = bd.toString();

        int posDecimal = valor.indexOf(".");

        if (posDecimal == -1) {
            valor = valor + ".00";
        } else {
            int numeroDecimales = valor.length() - posDecimal - 1;
            if (numeroDecimales == 0) {
                valor = valor + "00";
            } else if (numeroDecimales == 1) {
                valor = valor + "0";
            }
        }

        posDecimal = valor.indexOf(".");
        String Cadena = valor.substring(posDecimal);

        int cont = 0;
        for (int i = posDecimal - 1; i >= 0; i--) {
            ////System.out.println(valor.charAt(i-1));
            if (cont == 3) {
                Cadena = String.valueOf(valor.charAt(i)) + "," + Cadena;
                cont = 0;
            } else {
                Cadena = String.valueOf(valor.charAt(i)) + Cadena;
            }
            cont++;
        }

        return Cadena;
    }

    /**
     * Convierte un numero entero en string con ceros a la izquierda
     *
     * @param number Numero entero a convertir en string
     * @param width Largo de la cadena a retornar, caracteres faltantes son rellenados con ceros a la izquierda
     * @return String igual al numero entero pero con ceros a la izquierda
     * @author Alvaro Pabon Martinez
     * @version %I%, %G%
     * @since 1.0
     *
     */
    public static String ceroPad(int number, int width) {
        StringBuffer result = new StringBuffer("");
        for (int i = 0; i < width - Integer.toString(number).length(); i++) {
            result.append("0");
        }

        result.append(Integer.toString(number));
        return result.toString();
    }

    public static String lpad(String valueToPad, char filler, int size) {
        char[] array = new char[size];

        int len = size - valueToPad.length();
        for (int i = 0; i < len; i++) {
            array[i] = filler;
        }

        valueToPad.getChars(0, valueToPad.length(), array, size - valueToPad.length());

        return String.valueOf(array);
    }

    /**
     * Rellena con espacios a la derecha hasta una longitud especificada si la cadena es mayor que la longitud deseada se retorna la misma cadena
     *
     * @param s Cadena a rellenar
     * @param n Largo final de la cadena
     * @return String de una longitud n con espacios a la derecha
     * @author Alvaro Pabon Martinez
     * @version %I%, %G%
     * @since 1.0
     *
     */
    public static String padRight(String s, int n) {

        if (s.length() >= n) {
            return s;
        } else {
            return String.format("%1$-" + n + "s", s);
        }

    }

    public static int diasTranscurridos2(String fi, String ff) {
        final long MILLSECS_PER_DAY = 24 * 60 * 60 * 1000; //Milisegundos al d?a
        java.util.Date hoy = new Date(); //Fecha de hoy

        int ano = Integer.parseInt(fi.substring(0, 4));
        int mes = Integer.parseInt(fi.substring(5, 7));
        int dia = Integer.parseInt(fi.substring(8, 10));

        Calendar fecha1 = new GregorianCalendar(ano, mes - 1, dia);
        java.sql.Date fecha_inicial = new java.sql.Date(fecha1.getTimeInMillis());

        /* ---------------------------------------------------------------------------*/
        ano = Integer.parseInt(ff.substring(0, 4));
        mes = Integer.parseInt(ff.substring(5, 7));
        dia = Integer.parseInt(ff.substring(8, 10));

        Calendar fecha2 = new GregorianCalendar(ano, mes - 1, dia);
        java.sql.Date fecha_final = new java.sql.Date(fecha2.getTimeInMillis());

        long diferencia = (fecha_final.getTime() - fecha_inicial.getTime()) / MILLSECS_PER_DAY;
        System.out.println(diferencia);
        return (int) Math.round(diferencia);

    }

    public static boolean esImpar(int iNumero) {
        if (iNumero % 2 != 0) {
            return true;
        } else {
            return false;
        }
    }

    public static void imprimirTrace(Exception e) {

        e.printStackTrace();
    }

    public static String join(ArrayList strings, String separator) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < strings.size(); i++) {
            if (i != 0) {
                sb.append(separator);
            }
            sb.append(strings.get(i));
        }
        return sb.toString();
    }

    public static String escapeSQL(String texto) {
        return texto.replaceAll("'", "''").replaceAll("\\\\", "\\\\\\\\");
    }

    public static String directorioArchivo(String user) throws Exception {
        String ruta = "";
        try {
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            ruta = rb.getString("ruta") + "/exportar/migracion/" + user.toUpperCase();
            SimpleDateFormat fmt = new SimpleDateFormat("yyyMMdd_hhmmss");
            File archivo = new File(ruta);
            if (!archivo.exists()) {
                archivo.mkdirs();
            }

        } catch (Exception e) {
            throw new Exception("Error al generar el directorio: " + e.toString());
        }
        return ruta;

    }

    public static String formatoTimestamp(Timestamp timestamp, int formato) {
        String fecha = "";
        if (timestamp != null) {
            switch (formato) {
                case 1:
                    fecha = new SimpleDateFormat("dd/MM/yyyy").format(timestamp);
                    break;
                case 2:
                    fecha = new SimpleDateFormat("yyyyMMdd").format(timestamp);
                    break;
                case 3:
                    fecha = new SimpleDateFormat("yyyy/MM").format(timestamp);
                    break;
                case 4:
                    fecha = new SimpleDateFormat("yyyyMM").format(timestamp);
                    break;
            }
        }
        return fecha;
    }

    /**
     * Calcula el numero de dias entre 2 fechas basandose en un a�o de 360 dias(12 meses de 30 d�as)
     *
     * @param fechaInicial
     * @param fechaFinal
     * @return
     */
    public static long dias360(Date fechaInicial, Date fechaFinal) {
        Calendar cal1 = Calendar.getInstance();
        cal1.setTime(fechaInicial);
        Calendar cal2 = Calendar.getInstance();
        cal2.setTime(fechaFinal);

        int D1 = cal1.get(Calendar.DAY_OF_MONTH);
        int M1 = cal1.get(Calendar.MONTH) + 1;
        int Y1 = cal1.get(Calendar.YEAR);

        int D2 = cal2.get(Calendar.DAY_OF_MONTH);
        int M2 = cal2.get(Calendar.MONTH) + 1;
        int Y2 = cal2.get(Calendar.YEAR);

        //Se calcula en numero de dias financieros entre las 2 fechas
        long numDias = 360 * (Y2 - Y1) + 30 * (M2 - M1) + (D2 - D1);
        return numDias;
    }

    /**
     * expresa un valor en miles
     *
     * @param numero valor a convertir
     * @return valor convertido a miles
     * @author darrieta
     */
    public static double convertirMiles(Double numero) {
        return Math.round(numero.doubleValue() / 1000);
    }

    /**
     * expresa un valor en miles y le da formato
     *
     * @param numero valor a convertir
     * @return valor formateado con separador de miles
     * @author darrieta
     */
    public static String convertirMilesString(Double numero) {
        double valor = convertirMiles(numero);
        return customFormat(valor);
    }

    /**
     * Suma a una fecha un numero de dias y devuelve la fecha resultante
     *
     * @param fecha
     * @param dias a sumar
     * @return fecha resultante
     * @author ivargas
     */
    public static String fechaMasDias(String fecha, int dias) {
        int ano = Integer.parseInt(fecha.substring(0, 4));
        int mes = Integer.parseInt(fecha.substring(5, 7));
        int dia = Integer.parseInt(fecha.substring(8, 10));

        Calendar fecha1 = new GregorianCalendar(ano, mes - 1, dia);
        fecha1.add(Calendar.DATE, dias);
        SimpleDateFormat formatoFecha = new SimpleDateFormat("yyyy-MM-dd");
        return formatoFecha.format(fecha1.getTime());
    }

    /**
     * convierte un string con formato yyyy-MM-dd a Date
     *
     * @param fecha String con la fecha
     * @return fecha Date
     * @author ivargas
     */
    public static Date convertiraDate(String strFecha) {
        SimpleDateFormat formatoDelTexto = new SimpleDateFormat("yyyy-MM-dd");
        Date fecha = null;

        try {
            fecha = formatoDelTexto.parse(strFecha);
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        return fecha;
    }

    /**
     * resta dos fechas y devuelve la diferencia en dias
     *
     * @param fechaInicial
     * @param fechaIFinal
     * @return dias
     * @author ivargas
     */
    public static int fechasDiferenciaEnDias(Date fechaInicial, Date fechaFinal) {

        DateFormat df = DateFormat.getDateInstance(DateFormat.MEDIUM);
        String fechaInicioString = df.format(fechaInicial);
        try {
            fechaInicial = df.parse(fechaInicioString);
        } catch (ParseException ex) {
        }

        String fechaFinalString = df.format(fechaFinal);
        try {
            fechaFinal = df.parse(fechaFinalString);
        } catch (ParseException ex) {
        }

        long fechaInicialMs = fechaInicial.getTime();
        long fechaFinalMs = fechaFinal.getTime();
        long diferencia = fechaFinalMs - fechaInicialMs;
        double dias = Math.floor(diferencia / (1000 * 60 * 60 * 24));
        if (dias == 0) {///cuando es cheque al dia le ponemos un 1 si la diferencia es 0.
            dias = 1;
        }
        return ((int) dias);
    }

    /**
     * resta dos fechas y devuelve la diferencia en dias
     *
     * @param fechaInicial
     * @param fechaIFinal
     * @return dias
     * @author egonzalez
     */
    public static int fechasDiferenciaEnDias_v2(Date fechaInicial, Date fechaFinal) {

        DateFormat df = DateFormat.getDateInstance(DateFormat.MEDIUM);
        String fechaInicioString = df.format(fechaInicial);
        try {
            fechaInicial = df.parse(fechaInicioString);
        } catch (ParseException ex) {
        }

        String fechaFinalString = df.format(fechaFinal);
        try {
            fechaFinal = df.parse(fechaFinalString);
        } catch (ParseException ex) {
        }

        long fechaInicialMs = fechaInicial.getTime();
        long fechaFinalMs = fechaFinal.getTime();
        long diferencia = fechaFinalMs - fechaInicialMs;
        double dias = Math.floor(diferencia / (1000 * 60 * 60 * 24));
        return ((int) dias);
    }

    /**
     * regresa la cadena especificada en tipo Date, null si no es posible
     *
     * @param pformat ej. "yyyy-mm-dd"
     * @param pdatestr ej. "1980-06-01"
     * @return Date
     */
    public static Date stringtoDate(String formato, String fecha) {
        Date date = null;
        SimpleDateFormat df = new SimpleDateFormat(formato);
        try {
            date = df.parse(fecha);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return date;
    }

    public static String getFechahoy() {
        Calendar c = new GregorianCalendar();

        String dia, mes, annio;

        int d = c.get(Calendar.DATE);

        int m = c.get(Calendar.MONTH) + 1;
        if (m < 10) {
            mes = "0" + Integer.toString(m);
        } else {
            mes = Integer.toString(m);
        }

        if (d < 10) {
            dia = "0" + Integer.toString(d);
        } else {
            dia = Integer.toString(d);
        }

        annio = Integer.toString(c.get(Calendar.YEAR));
        return annio + "-" + mes + "-" + dia;

    }

    ////jpinedo
    public static String FormatoDecimal(Double valor, String formato)//"#0.000"
    {
        NumberFormat formatter = new DecimalFormat(formato);
        return formatter.format(valor);
    }

    public static String FormatearMiles(double value, int numDecimales) {
        //numdecimales 2

        DecimalFormatSymbols simbolo = new DecimalFormatSymbols();
        simbolo.setDecimalSeparator('.');
        simbolo.setGroupingSeparator(',');
        DecimalFormat df = new DecimalFormat("###,###.##", simbolo);
        df.setMaximumFractionDigits(numDecimales);
        df.setMinimumFractionDigits(numDecimales);
        return df.format(value);
    }

    public static String FormatoMilesSinDecimal(double aDouble) {

        //UTILICE LA FUNCION SOLO CUANDO LOS NUMEROS NO TIENEN DECIMALES
        //Formatea con  FormatoMiles(double aDouble) y luego TRUNCA los decimales
        String cadena = FormatoMiles(aDouble);
        int longitudCadena = cadena.length();

        cadena = cadena.substring(0, longitudCadena - 3);

        return cadena;
    }

    private static HashMap<String, Usuario> usuarioSession = new HashMap<String, Usuario>();

    /* Called by Listener */
    public static void setUsuarioSession(String login, Usuario usuarioSession) {
        Util.usuarioSession.put(login, usuarioSession);
    }

    /* Use this method to access context from any location */

    public static Usuario getUsuarioSession(String login) {
        return Util.usuarioSession.get(login);
    }

    public static void cerrarSession(String login) {
        Util.usuarioSession.remove(login);
    }

    public static String getActualBBDD(String login) {
        String baseDatos = "";
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String query = "SELECT empresa_ultimo_ingreso FROM usuarios WHERE idusuario=?;";
        MainDAO dao = null;
        try {
            if (Util.usuarioSession != null) {
                dao = new MainDAO();
                con = dao.conectarJNDIFintra();
                st = con.prepareStatement(query);
                st.setString(1, login);
                rs = st.executeQuery();
                if (rs.next()) {
                    baseDatos = rs.getString("empresa_ultimo_ingreso");
                }
                System.out.println(login + " >> " + baseDatos);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (Exception e) {
                }
            }
            if (con != null) {
                try {
                    dao.desconectar(con);
                } catch (Exception e) {
                }
            }
            return baseDatos;
        }
    }

    public JsonObject cargarEmpresaSession(String distrito) throws Exception {

        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        JsonObject resp = null;
        String query = "SELECT description, nit, dato AS base_datos, referencia AS proyecto "
                + "FROM cia INNER JOIN tablagen ON table_type = 'BASE' AND tablagen.reg_status != 'A' AND cia.dstrct = table_code "
                + "WHERE cia.dstrct = ? AND cia.reg_status != 'A' AND cia.multi_empresa = 'S'";
        MainDAO dao = null;
        try {
            dao = new MainDAO();
            con = dao.conectarJNDIFintra();
            if (con != null) {
                st = con.prepareStatement(query);
                st.setString(1, distrito);
                rs = st.executeQuery();

                if (rs.next()) {
                    resp = new JsonObject();
                    resp.addProperty("descripcion", rs.getString("description"));
                    resp.addProperty("nit", rs.getString("nit"));
                    resp.addProperty("base_datos", rs.getString("base_datos"));
                    resp.addProperty("proyecto", rs.getString("proyecto"));
                }
            }
        } catch (Exception e) {
            throw new Exception("ERROR DURANTE LA BUSQUEDA DE NOMBRE DE LA EMPRESA " + e.getMessage());
        } finally {//JJCastro fase2
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                    throw new Exception("ERROR CERRANDO EL RESULTSET " + e.getMessage());
                }
            }
            if (st != null) {
                try {
                    st.close();
                } catch (Exception e) {
                    throw new Exception("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null) {
                try {
                    dao.desconectar(con);
                } catch (Exception e) {
                    throw new Exception("ERROR CERRANDO LA CONEXION  " + e.getMessage());
                }
            }
        }
        return resp;
    }

    public static String setCodificacionCadena(String cadena, String codificacion) throws UnsupportedEncodingException {
        // Vemos si el formato entrante es ASCII o UTF8 
        CharsetEncoder isoEncoder = Charset.forName("ISO-8859-1").newEncoder();
        CharsetEncoder utf8Encoder = Charset.forName("UTF-8").newEncoder();
        Boolean isISO = isoEncoder.canEncode(cadena);
        //Boolean isUTF8 = utf8Encoder.canEncode(cadena);

        if (isISO) {
            return cadena;

        }
        return new String(cadena.getBytes(codificacion));

    }

    /**
     * Metodo para generar el centro de costo de los proyectos electricos desde apoteosys
     *
     * @autor egonzalez
     * @fecha 2017-06-29
     * @param nombreProyecto
     * @param tipoProyecto ejemplo : 1=?? ,2 =terc ,3=??
     * @return
     */
    public static String getCentroCostoProy(java.lang.String nombreProyecto, int tipoProyecto) {
        com.fintra.ws.apoteosys.Wsapoteosys_Service service = new com.fintra.ws.apoteosys.Wsapoteosys_Service();
        com.fintra.ws.apoteosys.Wsapoteosys port = service.getWsapoteosysPort();
        return port.getCentroCostoProy(nombreProyecto, tipoProyecto);
    }
    
    /**
     * Metodo ejecutar para montar los comprobantes a apoteosys
     *
     * @autor jzapata
     * @fecha 2020-05-25
     * @param usuario
     * @param passw
     * @param ano
     * @param mes
     * @param proceso
     * @return
     */
    public static String LoadApoteosys(String usuario, String passw, String ano, int mes, String proceso){
        com.fintra.ws.apoteosys.Wsapoteosys_Service service = new com.fintra.ws.apoteosys.Wsapoteosys_Service();
        com.fintra.ws.apoteosys.Wsapoteosys port = service.getWsapoteosysPort();
        return port.loadApoteosys(usuario, passw, ano, mes, proceso);
    }

    /**
     * Ordena una lista de tipo String que contiene o no, n�mero en sus primeros car�cteres
     *
     * @param lista colecci�n a ordenar
     */
    public static void ordenarListaStringConNumeros(List<String> lista) {
        final Pattern p = Pattern.compile("^\\d+");
        Collections.sort(lista, new Comparator<String>() {
            @Override
            public int compare(String object1, String object2) {
                Matcher m = p.matcher(object1);
                Integer number1 = null;
                if (!m.find()) {
                    return object1.compareTo(object2);
                } else {
                    Integer number2 = null;
                    number1 = Integer.parseInt(m.group());
                    m = p.matcher(object2);
                    if (!m.find()) {
                        return object1.compareTo(object2);
                    } else {
                        number2 = Integer.parseInt(m.group());
                        int comparison = number1.compareTo(number2);
                        if (comparison != 0) {
                            return comparison;
                        } else {
                            return object1.compareTo(object2);
                        }
                    }
                }
            }
        });
    }
    
    /**
     * Convierte el resultado de una consulta a un objeto JSON
     * @param rs objecto ResultSet
     * @param json objeto JSON donde se almacenar� el resultado de la consulta
     */
    public static void resultSetToJson(ResultSet rs, JsonObject json) {
        try {
            ResultSetMetaData rsmd = rs.getMetaData();
            int count = rsmd.getColumnCount();

            for (int i = 1; i <= count; i++) {
                switch (rsmd.getColumnType(i)) {
                    case Types.INTEGER:
                        json.addProperty(rsmd.getColumnName(i), rs.getInt(i));
                        break;
                    case Types.NUMERIC:
                        json.addProperty(rsmd.getColumnName(i), rs.getDouble(i));
                        break;
                    case Types.VARCHAR:
                    case Types.DATE:
                        json.addProperty(rsmd.getColumnName(i), rs.getString(i));
                        break;
                    case Types.TIMESTAMP:
                        json.addProperty(rsmd.getColumnName(i), rs.getTimestamp(i).toLocalDateTime().format(DateTimeFormatter.ISO_DATE));
                        break;
                    case Types.BOOLEAN:
                        json.addProperty(rsmd.getColumnName(i), rs.getBoolean(i));
                        break;
                }
            }
        } catch (SQLException e) {
            System.err.println("Error transforming resulset to json: " + e.getMessage());
        }
    }
    
     public synchronized static JsonArray getQueryJsonArray(Connection con, PreparedStatement ps) throws SQLException {
        ResultSet rs=null;
        JsonArray lista = null;
        JsonObject jsonObject = null;
        if (con != null) {
            rs = ps.executeQuery();
            lista = new JsonArray();
            while (rs.next()) {
                jsonObject = new JsonObject();
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                     switch(rs.getMetaData().getColumnTypeName(i)){
                        case "numeric":
                            jsonObject.addProperty(rs.getMetaData().getColumnLabel(i), rs.getDouble(rs.getMetaData().getColumnLabel(i)));
                            break;
                        case "int4":
                            jsonObject.addProperty(rs.getMetaData().getColumnLabel(i), rs.getInt(rs.getMetaData().getColumnLabel(i)));
                            break;    
                        case "bool":
                            jsonObject.addProperty(rs.getMetaData().getColumnLabel(i), rs.getBoolean(rs.getMetaData().getColumnLabel(i)));
                            break;    
                        default:
                            jsonObject.addProperty(rs.getMetaData().getColumnLabel(i), rs.getString(rs.getMetaData().getColumnLabel(i)));
                    }
                   
                }
                lista.add(jsonObject);
            }
        }       
        if (rs != null)
          rs.close();
        
        return lista;
    }
     
     public static String rutaArchivo(String user, String extension,String negocio,String nomarchivo,String ruta) throws Exception {
       
        try {
            File archivo = new File(ruta);
            if (!archivo.exists()) {
                archivo.mkdirs();
            }
            ruta = ruta + "/"+nomarchivo+"." + extension;
        } catch (Exception e) {
            throw new Exception("Error al generar el directorio: " + e.toString());
        }
        return ruta;
    }
     
    public static  String addMonthsDate(String dateAsString, int nbMonths) throws ParseException {
        String format = "yyyy-MM-dd";
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        Date dateAsObj = sdf.parse(dateAsString);
        Calendar cal = Calendar.getInstance();
        cal.setTime(dateAsObj);
        cal.add(Calendar.MONTH, nbMonths);
        Date dateAsObjAfterAMonth = cal.getTime();        
        return sdf.format(dateAsObjAfterAMonth);
    }

}
