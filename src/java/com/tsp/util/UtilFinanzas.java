/*
 * Util.java
 *
 * Created on 12 de octubre de 2004, 09:35 AM
 */

package com.tsp.util;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.text.*;
import java.util.Date;
import javax.crypto.*;
import javax.crypto.spec.*;
import java.security.*;
import java.security.spec.*;
import sun.misc.*;
/**
 *
 * @author  Miguel Celin
 */
public class UtilFinanzas {
    
    /** Creates a new instance of Utilerias */
    public UtilFinanzas() {}
    
    
    
    public static String  convertirFecha(String fecha, int dias,String separador) throws ParseException{
        String formato = "yyyy" + separador + "MM" + separador + "dd";
        SimpleDateFormat FMT = new SimpleDateFormat(formato);
        Calendar calendario  =  Calendar.getInstance();
        try{
            calendario.setTime(FMT.parse(fecha));
            calendario.add(Calendar.DATE,dias);
        }catch(Exception e){}
        return FMT.format(calendario.getTime());
    }
    
    
    public static String rellenar(String cadena, String caracter, int tope){
        int lon = cadena.length();
        if(tope>lon)
            for(int i=lon;i<tope;i++)
                cadena= caracter +cadena;
        return cadena;
    }
    
    public static String completar(String cadena, String caracter, int tope){
        int lon = cadena.length();
        if(tope>lon)
            for(int i=lon;i<tope;i++)
                cadena= cadena + caracter;
        return cadena;
    }
    
    
    
    public static String  convertirMilesgdos(long no) throws ParseException{
        SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Calendar cal = Calendar.getInstance();
        try{
            cal.setTimeInMillis(no);
        }catch(Exception e){}
        return formato.format(cal.getTime());
    }
    
    
    public static final String encript(String privateKey, String decriptedString)
    throws Exception {
        String decriptedStr = null;
        try {
            // Inicializar el cifrador.
            byte[] clave = privateKey.getBytes();
            DESKeySpec key1 = new DESKeySpec(clave);
            SecretKeyFactory factory = SecretKeyFactory.getInstance("DES");
            SecretKey key2 = factory.generateSecret(key1);
            Cipher ecipher = Cipher.getInstance("DES");
            ecipher.init(Cipher.ENCRYPT_MODE, key2);
            
            // Codificar la cadena en bytes usando UTF-8
            byte[] utf8 = decriptedString.getBytes("UTF8");
            
            // Encriptar
            byte[] enc = ecipher.doFinal(utf8);
            
            // Codificar los bytes a base 64 para obtener la cadena encriptada.
            decriptedStr = (new BASE64Encoder()).encode(enc);
        }catch(Exception EncriptE){
            throw new Exception(EncriptE.getMessage());
        }
        return decriptedStr;
    }
    
    
    public static String getHoy(String separador){
        String hoy = getFechaActual_String(1) + separador + getFechaActual_String(3) + separador + getFechaActual_String(5);
        return hoy;
    }
    
    
    public static String getFechaActual_Tipo(String tipo) {
        String fecha="";
      /*  tipo = tipo.toUpperCase();
        if(tipo.equals("A")) fecha= getFechaActual_String(1);
        if(tipo.equals("M")) fecha= getFechaActual_String(1)+ getFechaActual_String(3);
        if(tipo.equals("S")) fecha= getFechaActual_String(1)+ getFechaActual_String(3) + getFechaActual_String(5);
        if(tipo.equals("D")) fecha= getFechaActual_String(1)+ getFechaActual_String(3) + getFechaActual_String(5);*/
        fecha= getFechaActual_String(1)+ getFechaActual_String(3) + getFechaActual_String(5);
        return fecha;
    }
    
    
    //Rutinas de vistas
    public static boolean FechaValida(String fecha) {
        SimpleDateFormat  FMT = new SimpleDateFormat("yyyyMMdd");
        String fecha2 = FMT.format(new Date((100+Integer.parseInt(fecha.substring(1,4))),Integer.parseInt(fecha.substring(4,6))-1,Integer.parseInt(fecha.substring(6,8))));
        return fecha2.equals(fecha)?true:false;
    }
    
    public static String Semana(String fecha) {//ingreso una cadena ej 20040204 donde 2004=a�o, 02=mes y 04=dia y esta funcion entrega la semana de ese dia
        SimpleDateFormat  FMT2     = new SimpleDateFormat("WW");
        String Semana = FMT2.format(new Date((100+Integer.parseInt(fecha.substring(1,4))),Integer.parseInt(fecha.substring(4,6))-1,Integer.parseInt(fecha.substring(6,8))));
        return fecha.substring(0,6)+Semana;
    }
    
    public static String DiaInicialSemana(String fecha) {//ingreso una cadena ej 20040204 donde 2004=a�o, 02=mes y 04=semana y esta funcion entrega el primer dia de esa semana
        String valor = "";
        int i=0;
        for(i=1;i<=31;i++) {
            SimpleDateFormat  FMT2     = new SimpleDateFormat("WW");
            String Semana = FMT2.format(new Date((100+Integer.parseInt(fecha.substring(1,4))),Integer.parseInt(fecha.substring(4,6))-1,i));
            if(fecha.substring(6,8).equals(Semana))
                break;
        }
        return fecha.substring(0,6)+mesFormat(i);
    }
    
    public static String DiaFinalSemana(String fecha) {
        DateFormatSymbols symbols = new DateFormatSymbols();
        String[] capitalDays = {"", "7", "0", "1", "2", "3", "4", "5"};
        symbols.setShortWeekdays(capitalDays);
        SimpleDateFormat  FMT = new SimpleDateFormat("E", symbols);
        String Dia = FMT.format(new Date((100+Integer.parseInt(fecha.substring(1,4))),Integer.parseInt(fecha.substring(4,6))-1,Integer.parseInt(fecha.substring(6,8))));
        int i=0;
        for(i=Integer.parseInt(fecha.substring(6,8));i<Integer.parseInt(fecha.substring(6,8))+(6-Integer.parseInt(Dia));i++) {
            SimpleDateFormat  FMT2     = new SimpleDateFormat("yyyyMM");
            String SubFecha = FMT2.format(new Date((100+Integer.parseInt(fecha.substring(1,4))),Integer.parseInt(fecha.substring(4,6))-1,i));
            if(!fecha.substring(0,6).equals(SubFecha)){
                i--;
                break;
            }
        }
        return fecha.substring(0,6)+mesFormat(i);
    }
    
    public static String ConvertirMes(String Mes) {
        String Valor = null;
        if(Mes.toUpperCase().equals("ENE")) Valor="01";
        if(Mes.toUpperCase().equals("FEB")) Valor="02";
        if(Mes.toUpperCase().equals("MAR")) Valor="03";
        if(Mes.toUpperCase().equals("ABR")) Valor="04";
        if(Mes.toUpperCase().equals("MAY")) Valor="05";
        if(Mes.toUpperCase().equals("JUN")) Valor="06";
        if(Mes.toUpperCase().equals("JUL")) Valor="07";
        if(Mes.toUpperCase().equals("AGO")) Valor="08";
        if(Mes.toUpperCase().equals("SEP")) Valor="09";
        if(Mes.toUpperCase().equals("OCT")) Valor="10";
        if(Mes.toUpperCase().equals("NOV")) Valor="11";
        if(Mes.toUpperCase().equals("DIC")) Valor="12";
        return Valor;
    }
    
    public static String MesValor(String Mes) {
        String Valor = null;
        if(Mes.toUpperCase().equals("01")) Valor="ENE";
        if(Mes.toUpperCase().equals("02")) Valor="FEB";
        if(Mes.toUpperCase().equals("03")) Valor="MAR";
        if(Mes.toUpperCase().equals("04")) Valor="ABR";
        if(Mes.toUpperCase().equals("05")) Valor="MAY";
        if(Mes.toUpperCase().equals("06")) Valor="JUN";
        if(Mes.toUpperCase().equals("07")) Valor="JUL";
        if(Mes.toUpperCase().equals("08")) Valor="AGO";
        if(Mes.toUpperCase().equals("09")) Valor="SEP";
        if(Mes.toUpperCase().equals("10")) Valor="OCT";
        if(Mes.toUpperCase().equals("11")) Valor="NOV";
        if(Mes.toUpperCase().equals("12")) Valor="DIC";
        return Valor;
    }
    //Fin rutinas de vistas
    
    public static String getFechaActual_String(int valor) {
        SimpleDateFormat FMT = null;
        switch(valor) {
            case 0: FMT = new SimpleDateFormat("MMM");                 break; //Mes en espa�ol Ej: ene
            case 1: FMT = new SimpleDateFormat("yyyy");                break; //A�o en 4 digitos Ej: 2001
            case 2: FMT = new SimpleDateFormat("WW");                  break; //Semana del mes Ej: 02
            case 3: FMT = new SimpleDateFormat("MM");                  break; //Mes en 2 digitos Ej: 01
            case 4: FMT = new SimpleDateFormat("yyyy-MM-dd");          break; //A�o, Mes dia separados por '-' Ej: 2004-10-09
            case 5: FMT = new SimpleDateFormat("dd");                  break; //Dia en 2 digitos Ej: 02
            case 6: FMT = new SimpleDateFormat("yyyy/MM/dd kk:mm:ss"); break; //A�o, Mes dia separados por '/' y hora(24), minutos y segundos separado por ':' Ej: 2004/10/09 23:50:30
            case 7: FMT = new SimpleDateFormat("yyyy/MM/dd");          break; //A�o, Mes dia separados por '/' Ej: 2004/10/09
            case 8: FMT = new SimpleDateFormat("yyyyMMdd");            break; //A�o, Mes dia  Ej: 20041009
        }
        String Fecha = FMT.format(new Date());
        return Fecha.toUpperCase();
    }
    
    public static String mesFormat(int mes){
        String nombre="";
        switch(mes){
            case 1:   nombre="01";  break;
            case 2:   nombre="02";  break;
            case 3:   nombre="03";  break;
            case 4:   nombre="04";  break;
            case 5:   nombre="05";  break;
            case 6:   nombre="06";  break;
            case 7:   nombre="07";  break;
            case 8:   nombre="08";  break;
            case 9:   nombre="09";  break;
            case 10:  nombre="10";  break;
            case 11:  nombre="11";  break;
            case 12:  nombre="12";  break;
            default:  nombre=String.valueOf(mes); break;
        }
        return nombre;
        
    }
    
    public static String NombreMes(int mes){
        String nombre="";
        switch(mes){
            case 1:   nombre="Enero";  break;
            case 2:   nombre="Febrero"; break;
            case 3:   nombre="Marzo";break;
            case 4:   nombre="Abril"; break;
            case 5:   nombre="Mayo";break;
            case 6:   nombre="Junio"; break;
            case 7:   nombre="Julio"; break;
            case 8:   nombre="Agosto"; break;
            case 9:   nombre="Septiembre";break;
            case 10:  nombre="Octubre"; break;
            case 11:  nombre="Noviembre"; break;
            case 12:  nombre="Diciembre"; break;
            default:  nombre=String.valueOf(mes); break;
        }
        return nombre;
    }
    
    public static String ValorMes(String Mes){
        String Valor = null;
        if(Mes.toUpperCase().equals("ENERO"))      Valor="01";
        if(Mes.toUpperCase().equals("FEBRERO"))    Valor="02";
        if(Mes.toUpperCase().equals("MARZO"))      Valor="03";
        if(Mes.toUpperCase().equals("ABRIL"))      Valor="04";
        if(Mes.toUpperCase().equals("MAYO"))       Valor="05";
        if(Mes.toUpperCase().equals("JUNIO"))      Valor="06";
        if(Mes.toUpperCase().equals("JULIO"))      Valor="07";
        if(Mes.toUpperCase().equals("AGOSTO"))     Valor="08";
        if(Mes.toUpperCase().equals("SEPTIEMBRE")) Valor="09";
        if(Mes.toUpperCase().equals("ACTUBRE"))    Valor="10";
        if(Mes.toUpperCase().equals("NOVIEMBRE"))  Valor="11";
        if(Mes.toUpperCase().equals("DICIEMBRE"))  Valor="12";
        return Valor;
    }
    
    public static String DiaSemana(String Fecha) {
        Date fec = new Date(Fecha.substring(0,4) + "/" + Fecha.substring(4,6) +  "/" + Fecha.substring(6,8));
        String diaSemana = "";
        switch (fec.getDay()){
            case 0:  diaSemana = "Dom"; break;
            case 1:  diaSemana = "Lun"; break;
            case 2:  diaSemana = "Mar"; break;
            case 3:  diaSemana = "Mie"; break;
            case 4:  diaSemana = "Jue"; break;
            case 5:  diaSemana = "Vie"; break;
            case 6:  diaSemana = "Sab"; break;
        }
        return diaSemana;
    }
    
    public static String DiaFormat(int dia){
        return ((dia<10)?"0":"") + String.valueOf(dia);
    }
    
     public static String customFormat(String pattern, Double value, int CountDecimal) {
        DecimalFormat df = (DecimalFormat) NumberFormat.getNumberInstance(new Locale("en", "US"));
        df.setMaximumFractionDigits(CountDecimal);
        df.setMinimumFractionDigits(CountDecimal);
        df.applyPattern(pattern);
        return df.format(value.doubleValue());
    }
     
    public static String NumeroCuenta(String AccountCode){
        String Cuenta = " ";
        String Inicio = (AccountCode.length()>=2)?AccountCode.substring(0,2):AccountCode;
        if (AccountCode.length()>=4 && (Inicio.equals("12") ||  Inicio.equals("17") || Inicio.equals("26") || Inicio.equals("28")))
            Cuenta = AccountCode.substring(0,4);
        else if (Inicio.equals("15") || Inicio.equals("24"))
            Cuenta = Inicio;
        else if (AccountCode.length()>=4 && Inicio.equals("23") )
            Cuenta = (AccountCode.substring(0,4).equals("2335") && AccountCode.length()>=6)?AccountCode.substring(0,6):AccountCode.substring(0,4);
        else if (Inicio.equals("21") )
            Cuenta = AccountCode.substring(0,2) + AccountCode.substring(AccountCode.length()-2,AccountCode.length());
        else if (Inicio.equals("13") && !AccountCode.equals("13102010"))
            Cuenta = AccountCode.substring(0,4);
        else
            Cuenta = AccountCode;
        return Cuenta;
    }
    
    public static String customFormat(String pattern, double value, int CountDecimal) {
        DecimalFormat df = (DecimalFormat) NumberFormat.getNumberInstance(new Locale("en", "US"));
        df.setMaximumFractionDigits(CountDecimal);
        df.setMinimumFractionDigits(CountDecimal);
        df.applyPattern(pattern);
        return df.format(value);
    }
    public static String customFormat(double value) {
        DecimalFormat df = (DecimalFormat) NumberFormat.getNumberInstance(new Locale("en", "US"));
        df.applyPattern("#,###");
        df.setMaximumFractionDigits(0);
        df.setMinimumFractionDigits(0);
        return df.format(value);
    }
    
    
    public static String customFormat(String value) {
        DecimalFormat df = (DecimalFormat) NumberFormat.getNumberInstance(new Locale("en", "US"));
        df.applyPattern("#,###");
        df.setMaximumFractionDigits(0);
        df.setMinimumFractionDigits(0);
        return df.format(Double.parseDouble(value));
    }
    
    public static String customFormat2(double value) {
        DecimalFormat df = (DecimalFormat) NumberFormat.getNumberInstance(new Locale("en", "US"));
        df.applyPattern("#,###.##");
        df.setMaximumFractionDigits(2);
        df.setMinimumFractionDigits(2);
        return df.format(value);
    }
    
    public static String customFormat2(String value) {
        DecimalFormat df = (DecimalFormat) NumberFormat.getNumberInstance(new Locale("en", "US"));
        df.applyPattern("#,###.##");
        df.setMaximumFractionDigits(2);
        df.setMinimumFractionDigits(2);
        return df.format(Double.parseDouble(value));
    }
    
    
    
    public static Hashtable Acumulado(Hashtable Acumulada, Hashtable Nuevo, int Cantidad) {
        for (int i = 0 ; i< Cantidad ; i++)
            if(Acumulada.get(String.valueOf(i))!=null)
                Acumulada.put(String.valueOf(i) , String.valueOf(Double.parseDouble(Acumulada.get(String.valueOf(i)).toString()) + Double.parseDouble(Nuevo.get( String.valueOf(i) ).toString()))     );
            else
                Acumulada.put(String.valueOf(i) , Nuevo.get( String.valueOf(i) ).toString());
        return Acumulada;
    }
    
    public static TreeMap Acumulado(TreeMap Acumulada, TreeMap Nuevo, int Cantidad) {
        for (int i = 0 ; i< Cantidad ; i++)
            if(Acumulada.get(String.valueOf(i))!=null)
                Acumulada.put(String.valueOf(i) , String.valueOf(Double.parseDouble(Acumulada.get(String.valueOf(i)).toString()) + Double.parseDouble(Nuevo.get( String.valueOf(i) ).toString()))     );
            else
                Acumulada.put(String.valueOf(i) , Nuevo.get( String.valueOf(i) ).toString());
        return Acumulada;
    }
    
    public static String [] CalculoSemanal(String []Viajes){
        String [] Semana = {"0","0","0","0","0","0"};
        for (int i=0, j=0;i<31;i++){
            if (i%7==0 && i!=0) j++;
            Semana[j] = String.valueOf(Integer.parseInt(Semana[j]) + Integer.parseInt(Viajes[i]));
        }
        
        return Semana;
    }
    
    public static String [] CalculoDiario(String []Viajes){
        String [] CDIA = {"0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0"};
        for (int i=0,j=0;i<=30 && j<5;i+=7,j++)
            CDIA[i] = Viajes[j];
        return CDIA;
    }
    
    public static String Trunc(String Cadena, int Longitud){
        return (Cadena==null?Cadena: (Cadena.length()>=Longitud? Cadena.substring(0,Longitud):Cadena  )  );
    }
    public static String diaFinal(String Ano, String Mes)throws Exception{
        try{
            String []  diaFinal = {"31", "28", "31", "30", "31", "30", "31", "31", "30", "31", "30", "31"};
            String [] bdiaFinal = {"31", "29", "31", "30", "31", "30", "31", "31", "30", "31", "30", "31"};
            return (Integer.parseInt(Ano)%4 == 0? bdiaFinal[Integer.parseInt(Mes)-1] : diaFinal[Integer.parseInt(Mes)-1]  );
        }catch (Exception e){
            throw new Exception("Periodo no valido ["+Ano + Mes+"]");
        }
    }
    
    public static String diaFinal(String Periodo)throws Exception{
        try{
            String Ano = Periodo.substring(0,4);
            String Mes = Periodo.substring(4,6);
            String []  diaFinal = {"31", "28", "31", "30", "31", "30", "31", "31", "30", "31", "30", "31"};
            String [] bdiaFinal = {"31", "29", "31", "30", "31", "30", "31", "31", "30", "31", "30", "31"};
            return (Integer.parseInt(Ano)%4 == 0? bdiaFinal[Integer.parseInt(Mes)-1] : diaFinal[Integer.parseInt(Mes)-1]  );
        }catch (Exception e){
            throw new Exception("Periodo no valido ["+Periodo+"]");
        }
    }
    public static int getSeccionPeriodo(int Periodo, int Meses, String Tipo){
        int Mes       = Integer.parseInt(String.valueOf(Periodo).substring(4,6));
        int Seccion   = (Mes/Meses) + (Mes%Meses==0?0:1);
        int MesInicio = (Seccion * Meses) - Meses +1;
        int MesFinal  = (Seccion * Meses);
        return Integer.parseInt(String.valueOf(Periodo).substring(0,4) +
        (Tipo.equals("I")? (MesInicio<10?"0":"") + MesInicio:
            (MesFinal <10?"0":"") + MesFinal
            ));
    }
    public static int getNextPeriodo(int Periodo, int inc){
        String Ano = String.valueOf(Periodo).substring(0,4);
        String Mes = String.valueOf(Periodo).substring(4,6);
        Periodo    = (Integer.parseInt(Mes) + inc > 12)?  Integer.parseInt(String.valueOf(Integer.parseInt(Ano)+1) + "01" ): Integer.parseInt(Ano + (Integer.parseInt(Mes) + inc <10?"0":"") +  String.valueOf(Integer.parseInt(Mes) + inc));
        return Periodo;
    }
    public static int getNextPeriodoNormal(int Periodo, int inc){
        int mes   = Integer.parseInt(String.valueOf(Periodo).substring(4,6)) + inc;
        int ano   = Integer.parseInt(String.valueOf(Periodo).substring(0,4));
        ano += (mes > 12 ? (mes-1)/12 :
            mes <= 0  ? (mes/12)-1 : 0);
            mes  = (mes < 0  ? ((Math.abs(mes/12) + 1) * 12)  - Math.abs(mes) :
                mes > 12 ? Math.abs(mes) - (Math.abs(mes/12) * 12)        : mes );
                return (ano*100) + (mes==0?12:mes);
    }
    public static int getNextPeriodoNormal(String Periodo, int inc){
        return getNextPeriodoNormal(Integer.parseInt(Periodo), inc );
    }
    
    
    public static String addFecha(String Fecha, int Incremento, int Tipo){
        String [] Fec = (Fecha).split("-|/");
        SimpleDateFormat FMT = new SimpleDateFormat("yyyy-MM-dd");
        Fec [Tipo] = String.valueOf( Integer.parseInt(Fec[Tipo]) + Incremento );
        return FMT.format( new  Date( Integer.parseInt(Fec[0])-1900 , Integer.parseInt(Fec[1])-1, Integer.parseInt(Fec[2]))).toString();
    }
    
    public static String addFechaFormat_YYYYMMDD(String Fecha, int Incremento, int Tipo){
        String Fec = Fecha.substring(0,4) + "/" + Fecha.substring(4,6) + "/"  + Fecha.substring(6,8);
        return addFecha(Fec, Incremento, Tipo).replaceAll("-","");
    }
    
    public static String obtenerRuta(String keyPath, String usuario){
        ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
        String path = rb.getString(keyPath);
        File f = new File(path + usuario);
        if ( !f.exists() ) f.mkdirs();
        return path + usuario;
    }
    
    
    // HENRY
    public static String formatoFechaYYYYMMDD( int ano, int mes, int dia ) {
        String mesF = ""+mes;
        String diaF = ""+dia;
        if(mes<10)
            mesF = "0"+mes;
        if(dia<10)
            diaF = "0"+dia;
        String formato = "00990101";
        formato = ""+ano+mesF+diaF;
        return formato;
    }
    
    public static String restarA�oFecha(String fecha) {
        String ano = fecha.substring(0,4);
        String mes = fecha.substring(5,7);
        String dia = fecha.substring(8,10);
        int anoA = Integer.parseInt(ano)-1;
        return ""+anoA+mes+dia;
    }
    public static String fechaInicioProyeccion(String fecha, boolean igual) {
        String ano = fecha.substring(0,4);
        String mes = fecha.substring(5,7);
        String dia = fecha.substring(8,10);
        int mesP=0,anoP=0,diaP=0;
        if(igual) {
            mesP = Integer.parseInt(mes)+1;
            anoP = Integer.parseInt(ano)-1;
            diaP = 1;
        } else {
            mesP = Integer.parseInt(mes);
            anoP = Integer.parseInt(ano)-1;
            diaP = 1;
        }
        if(mesP==13){
            anoP = Integer.parseInt(ano);
            mesP = 1;
        }
        return UtilFinanzas.formatoFechaYYYYMMDD(anoP, mesP, diaP);
    }
    public static int getMesesParaCompletarA�o(String fecha) {
        String mes = fecha.substring(5,7);
        int dias = 12-Integer.parseInt(mes);
        return dias;
    }
    public static String sumarMes(String fecha) {
        String ano = fecha.substring(0,4);
        String mes = fecha.substring(4,6);
        String dia = fecha.substring(6,8);
        int mesP = Integer.parseInt(mes)+1;
        int anoP = Integer.parseInt(ano);
        int diaP = Integer.parseInt(dia);;
        if(mesP==13){
            anoP = Integer.parseInt(ano)+1;
            mesP = 1;
        }
        return UtilFinanzas.formatoFechaYYYYMMDD(anoP, mesP, diaP);
    }
    public static String sumarA�o(String periodo) {
        String ano = periodo.substring(0,4);
        String mes = periodo.substring(4,6);
        int anoP = Integer.parseInt(ano)+1;
        return ""+anoP+mes;
    }
    //Retorna el numero meses que hay entre un periodo y otro
    public static int mesesEntrePeriodos(String per1, String per2){
        int meses = 0,anos=0;
        int anoInicial = Integer.parseInt(per1.substring(0,4));
        int mesInicial = Integer.parseInt(per1.substring(4,6));
        int anoFinal = Integer.parseInt(per2.substring(0,4));
        int mesFinal = Integer.parseInt(per2.substring(4,6));
        anos = anoFinal - anoInicial;
        meses = mesFinal - mesInicial;
        meses = (anos*12) + meses;
        return meses;
    }
    
   public static String customFormatDate(String in, String formatoIn, String formatoOut) throws Exception{
       SimpleDateFormat fmtIn  = new SimpleDateFormat(formatoIn);
       Date date = fmtIn.parse(in);
       return customFormatDate(date,formatoOut);
   }
   
   public static String customFormatDate(Date in, String formatoOut) throws Exception{
       SimpleDateFormat fmtOut = new SimpleDateFormat(formatoOut);
       return fmtOut.format(in);
   } 
   
   public static Date StringToDate (String date, String formato) throws Exception{
       return (new SimpleDateFormat(formato)).parse(date);
   }
   
    public static String customFormat(Double value) {
        DecimalFormat df = (DecimalFormat) NumberFormat.getNumberInstance(new Locale("en", "US"));
        df.applyPattern("#,###");
        df.setMaximumFractionDigits(0);
        df.setMinimumFractionDigits(0);
        return df.format(value.doubleValue());
    }
    
    
    /**
    * Metodo para validar el contenido de una variable
    * @autor mfontalvo
    */

    public static boolean parametroValido(String param){
        return (param!=null && !param.equals(""))?true:false;
    }



}