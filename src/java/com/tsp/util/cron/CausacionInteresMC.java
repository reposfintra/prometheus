/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.util.cron;

import java.io.DataInputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ResourceBundle;

/**
 *
 * @author Ing. Iris Vargas
 */
public class CausacionInteresMC implements Runnable {
    private Thread thread = null;

	private String direccion = null;

	public CausacionInteresMC() {}

	public void init(int op) {
            ResourceBundle bundle = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            switch (op) {
                case 1:
                    this.direccion = bundle.getString("causacion_interes_mc");
                    break;
                case 2:
                    this.direccion = bundle.getString("causacion_interes_mc_fmes");
                    break;
            }

		
		
		this.thread = new Thread(this);
		this.thread.start();
	}
        
    /**
     *
     * @param op
     * @param ciclo
     * @param periodo
     * @throws java.lang.InterruptedException
     */
    public void init(int op, String ciclo, String periodo) throws InterruptedException {
        ResourceBundle bundle = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
        switch (op) {
            case 1:
                this.direccion = bundle.getString("causacion_interes_mc");
                break;
            case 2:
                this.direccion = bundle.getString("causacion_interes_mc_fmes");
                break;
                
            case 3:
                this.direccion = bundle.getString("causacion_interes_mciclo") + "&ciclo=" + ciclo + "&periodo=" + periodo;
                break;
        }

        this.thread = new Thread(this);
        this.thread.start();
        this.thread.join();
    }


    @Override
    public void run() {
        HttpURLConnection conexion = null;
		try {
			URL url = new URL(this.direccion);
			conexion = (HttpURLConnection) url.openConnection();
			conexion.connect();

			switch (conexion.getResponseCode()) {
				case HttpURLConnection.HTTP_OK:
					System.out.println(">>> CONEXION ESTABLECIDA " + url.toString() + "...\n");

					InputStream input = conexion.getInputStream();
					DataInputStream data = new DataInputStream(input);

					String linea = "";
					while ((linea = data.readLine()) != null) {
						System.out.println(linea);
					}
					data.close();
					input.close();
					break;
				case HttpURLConnection.HTTP_NOT_FOUND:
					System.err.println(">>> ERROR: Direccion " + this.direccion + " no encontrada");
					break;
				default:
					System.err.println(">>> ESTADO HTTP: " + conexion.getResponseCode());
					break;
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			conexion.disconnect();
			System.out.println(">>> CRON FINALIZADO.\n");
		}
	 System.out.println(">>> FIN.\n");
    }

    public static void main(String arg[]) {
		CausacionInteresMC a = new CausacionInteresMC();
                if(arg[0]!=null){
		a.init(Integer.parseInt(arg[0]));
                }else{
                   a.init(0);
                }
	}

}
