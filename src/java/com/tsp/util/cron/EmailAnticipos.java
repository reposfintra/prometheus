/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor. jpinedo
 */

package com.tsp.util.cron;
import com.tsp.operation.model.LogProcesosService;
import com.tsp.util.*;
import javax.servlet.*;
import com.tsp.operation.model.beans.ExtractoDetalle;
import java.util.List;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Font;
import com.lowagie.text.PageSize;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import com.lowagie.text.Image;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ResourceBundle;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.Model;
import com.tsp.operation.model.DAOS.*;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Rectangle;


import java.sql.*;
import java.util.LinkedList;
import com.tsp.operation.model.beans.Email2;
import com.tsp.operation.model.services.EmailSendingEngineService;
/**
 *
 * @author jpinedo
 */
public class EmailAnticipos extends Thread{

      private Model     model;
    private String    procesoName;
   // private Usuario   user;


    private String tox,copytox,hiddencopytox;
    private String fromx,asuntox,msgx,userx;
    private String ruta,url_logo;
    private String [] nombre_archivos= new String[2];
    AnticiposPagosTercerosDAO antDAO;
    String img_logo="<img src= 'http://www.fintravalores.com/fintravalores/images/fintrapdf.gif'/> ";
    String correo_salida="fintravalores@geotechsa.com";
    String pass_salida="fintra21";
    ExcelApplication excel;
    int fila=0;
    private String dataBaseName;
   



    public EmailAnticipos() {
        antDAO=new AnticiposPagosTercerosDAO();
        this.dataBaseName="fintra";
    }
    public EmailAnticipos(String dataBaseName) {
        this.dataBaseName=dataBaseName;
        antDAO=new AnticiposPagosTercerosDAO(dataBaseName);
    }



     public void start() {
         try{
      
                //this.user        = usuario;
                this.procesoName = "Envio Correos Anticipos ProntoPago";

                /****************** informacion del correo***********************************/
                fromx="comercial@fintra.co";
                copytox="";
                hiddencopytox="";
                url_logo="fintrapdf.gif";
        
        


                super.start();
        }catch(Exception e){
          
       }
    }





       public synchronized void run(){
       boolean enviado=false;
       LogProcesosService log = new LogProcesosService(this.dataBaseName);
       long inicio = System.currentTimeMillis();
       try
       {
            String hoy  =  Utility.getHoy("-");
            String ayer =  Utility.convertirFecha( hoy, -1);
            boolean generado=false;
            String  directorio = this.directorioArchivo();
            ruta=directorio+"/";
             nombre_archivos[0]="liquidacion.pdf";

              tox="jpinedo@fintra.co;jesus.pinedo@hotmail.com";

              msgx="Estimado transportador : POR FAVOR NO CONTESTAR ES MENSAJE, en caso de requerir un nuevo Prontopago puede "
                       + "contactarnos al telefono (5) 3679901 opci�n 2.<br>"
                       + "Adjunto se encuentra el extracto del prontopago <br><br>"                       ;

            List<AnticiposTerceros> lista_atp_ext = new LinkedList<AnticiposTerceros>();
            List<AnticiposTerceros> lista_atp = new LinkedList<AnticiposTerceros>();
            lista_atp_ext=antDAO.getListaAnticiposEmailPP();
            lista_atp=antDAO.getListaAnticiposEmail();
            int secuencia=0;
            /************************** Correos Prontopagos*******************************/
            for (int i=0;i<lista_atp_ext.size();i++)
            {
            secuencia=lista_atp_ext.get(i).getSecuencia();
            generado=exportarPdf(secuencia);
               if(generado==true)
               {
                   if(this.generarXls(secuencia))
                    {   
                        asuntox = "Extracto Prontopago Liquidacion " + secuencia;
                        tox = lista_atp_ext.get(i).getDato_generico();
                        enviado = this.envia_correo();
                        if (enviado == true) {
                            antDAO.MarcarAnticiposEmail_PP(secuencia);
                        }
                    }

               }

           }



            msgx="<br><br><p style='font-family:Arial, Helvetica, sans-serif'>Reporte Anticipo </p>";

           /************************** Correos Anticipos*******************************/
            String planilla="";
            for (int i=0;i<lista_atp.size();i++)
            {
            planilla=lista_atp.get(i).getPlanilla();
            generado=exportarPdfAticipos(planilla);
               if(generado==true)
               {
               asuntox="Anticipo Planilla "+planilla;
              tox=lista_atp.get(i).getDato_generico();//correo
               enviado=this.envia_correo();
                   if(enviado==true)
                   {
                      antDAO.MarcarAnticiposEmail(planilla);
                   }

               }

           }




    

       }
       catch(Exception e)
       {
           try{
               
               log.finallyProceso(this.procesoName,this.hashCode(), "ADMIN","ERROR Hilo: " + e.getMessage()); }
           catch(Exception f){}
       }
        System.out.println("tiempo total empleado "+((System.currentTimeMillis() - inicio)/1000)+" segundos");
        System.out.println("Enviado "+enviado);
    }



























       private String directorioArchivo( String cons, String extension) throws Exception {
        String ruta = "";
        try {
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            ruta = rb.getString("ruta") + "/exportar/migracion/DOCUMENTOS";
            SimpleDateFormat fmt = new SimpleDateFormat("yyyMMdd_hhmmss");
            File archivo = new File(ruta);
            if (!archivo.exists()) {
                archivo.mkdirs();
            }
            ruta = ruta + "/" + cons +/* "_" + fmt.format(new Date()) +*/ "." + extension;
        } catch (Exception e) {
            throw new Exception("Error al generar el directorio: " + e.toString());
        }
        return ruta;
    }

        private String directorioArchivo() throws Exception {
        String ruta = "";
        try {
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            ruta = rb.getString("ruta") + "/exportar/migracion/DOCUMENTOS";
            SimpleDateFormat fmt = new SimpleDateFormat("yyyMMdd_hhmmss");
            File archivo = new File(ruta);
            if (!archivo.exists()) {
                archivo.mkdirs();
            }

        } catch (Exception e) {
            throw new Exception("Error al generar el directorio: " + e.toString());
        }
        return ruta;
    }


   /*********************************************************************************************************************/


   protected boolean exportarPdf(int secuencia) throws Exception {

           /**********prueba anticipos pronto pago***************/

           //AnticiposPagosTercerosDAO antDAO=new AnticiposPagosTercerosDAO();
           AnticiposTerceros ant =null;
           ant=antDAO.getAnticipo_x_secuencia(secuencia); //164056

/**********prueba extracto pronto pago***************/

           ExtractoDAO etDAO=new ExtractoDAO();
           List<ExtractoDetalle> lista_extracto_detalle = new LinkedList<ExtractoDetalle>();
           List<ExtractoDetalle> lista_doc_extracto_detalle = new LinkedList<ExtractoDetalle>();
           lista_doc_extracto_detalle=etDAO.Lista_doc_extracto_det(ant.getSecuencia(), ant.getPla_owner());
           List<ExtractoDetalle> lista_factura_propietario = new LinkedList<ExtractoDetalle>();
           lista_factura_propietario=etDAO.Lista_facturas_propietario(ant.getSecuencia(), ant.getPla_owner() );




         float tl=0;

       boolean generado = true;
        String directorio = "";String documento_detalle="";String placa="";
        System.out.println("inicia elaboracion pdf extratos");
        ResourceBundle rb = null;
        try {
            rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            directorio = this.directorioArchivo("liquidacion", "pdf");
            System.out.println("elaborando directorio");
            Document documento = null;
            documento = this.createDoc();
            PdfWriter.getInstance(documento, new FileOutputStream(directorio));
            documento.setFooter(this.getMyFooter());
            documento.open();
            documento.newPage();

            Image img= Image.getInstance(rb.getString("ruta")+"/images/"+url_logo);
            //img.setAbsolutePosition(100, 10);
            documento.add(img);


           /*----------------------------------Datos Propietario--------------------------------------*/
            PdfPTable tabla_datos_pro = this.datosPropietario(ant);
            tabla_datos_pro.setWidthPercentage(100);
            documento.add(tabla_datos_pro);

           /*-------------una tabla por cada factura del extrato detalle---------------------*/
            PdfPTable tabla_fac_pro =null;
           for (int j=0;j<lista_doc_extracto_detalle.size();j++)
           {
               documento_detalle=lista_doc_extracto_detalle.get(j).getDocumento();
               placa=lista_doc_extracto_detalle.get(j).getPlaca();
               lista_extracto_detalle=etDAO.Lista_extracto_det(ant.getSecuencia(), ant.getPla_owner(),documento_detalle );
               tabla_fac_pro=this.facturas(lista_extracto_detalle,documento_detalle,placa);
               tabla_fac_pro.setWidthPercentage(100);
               documento.add(tabla_fac_pro);
               tl=tl+lista_extracto_detalle.get(0).getVlr_ppa_item();
            }

            /*-------------facturas propietario-----------------*/
            if(lista_factura_propietario.size()>0)
            {
            PdfPTable tabla_facturas_propietarios = this.facturasPropietario(lista_factura_propietario);
            tabla_facturas_propietarios.setWidthPercentage(100);
            documento.add(tabla_facturas_propietarios);

            }


            PdfPTable datos_generales = this.datosGenerales(tl,this.total_fp(lista_factura_propietario));
           datos_generales.setWidthPercentage(80);
            documento.add(datos_generales);

            //showTextAligned(Element.ALIGN_LEFT,"", 50, 50, 0);
            documento.close();

        }
   catch (Exception e)
        {
            generado = false;
            System.out.println("error al generar pdf solicitud de aval: " + e.toString());
            e.printStackTrace();
        }
        System.out.println("fin elaboracion pdf solicitud de aval");
        //Envio de Correo

        return generado;
    }







   /***********************************************pdf anticipos ************************************************************/
   protected boolean exportarPdfAticipos(String planilla) throws Exception {



           //AnticiposPagosTercerosDAO antDAO=new AnticiposPagosTercerosDAO();
           AnticiposTerceros ant =null;
           ant=antDAO.getAnticipo_x_planilla(planilla); //164056



       boolean generado = true;
        String directorio = "";String documento_detalle="";
        System.out.println("inicia elaboracion pdf anticipos");
        ResourceBundle rb = null;
        try {
            rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            directorio = this.directorioArchivo("liquidacion", "pdf");
            System.out.println("elaborando directorio");
            Document documento = null;
            documento = this.createDoc();
            PdfWriter.getInstance(documento, new FileOutputStream(directorio));
            documento.setFooter(this.getMyFooter());
            documento.open();
            documento.newPage();

            Image img= Image.getInstance(rb.getString("ruta")+"/images/"+url_logo);
            //img.setAbsolutePosition(100, 10);
            documento.add(img);


           /*----------------------------------Datos Anticipos--------------------------------------*/
            PdfPTable tabla_datos_pro = this.datosAnticipos(ant);
            tabla_datos_pro.setWidthPercentage(100);
            documento.add(tabla_datos_pro);         
            documento.close();

        }
   catch (Exception e)
        {
            generado = false;
            System.out.println("error al generar pdf solicitud de aval: " + e.toString());
            e.printStackTrace();
        }
        System.out.println("fin elaboracion pdf solicitud de aval");
        //Envio de Correo

        return generado;
    }
   /*********************************************************************************************************************/







       private Document createDoc() {
        Document doc = new Document(PageSize.LETTER, 30, 30, 30, 30);

        return doc;
    }






         /*--------------------------------------------------------------*/
        protected PdfPTable datosPropietario(AnticiposTerceros anticipo)throws DocumentException {

        PdfPTable tabla_temp = new PdfPTable(4);
        float[] medidaCeldas = {0.150f, 0.200f, 0.190f, 0.460f};
        tabla_temp.setWidths(medidaCeldas);
        Font fuente = new Font(Font.HELVETICA, 10);
        Font fuenteB = new Font(Font.HELVETICA, 10, Font.BOLD);
        PdfPCell celda_temp = new PdfPCell();




        celda_temp.setPhrase(new Phrase("PROPIETARIO", fuenteB));
        celda_temp.setColspan(0);
        celda_temp.setBorder(0);
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);

        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase(anticipo.getPla_owner(), fuente));
        celda_temp.setColspan(0);
        celda_temp.setBorder(0);
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);

        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("NOMBRE ", fuenteB));
        celda_temp.setColspan(0);
        celda_temp.setBorder(0);
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);

        // fila 2
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase(anticipo.getNombrePropietario(), fuente));
        celda_temp.setBorder(0);
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);


        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("SECUENCIA  ", fuenteB));
        celda_temp.setColspan(0);
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        celda_temp.setBorder(0);
        tabla_temp.addCell(celda_temp);

        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase(""+anticipo.getSecuencia(), fuente));
        celda_temp.setColspan(5);
        celda_temp.setBorder(0);
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);

        return tabla_temp;
    }










   /*****************************Datos Anticipos*******************************************/
   protected PdfPTable datosAnticipos(AnticiposTerceros anticipo)throws DocumentException {

        PdfPTable tabla_temp = new PdfPTable(4);
        float[] medidaCeldas = {0.170f, 0.200f, 0.190f, 0.440f};
        tabla_temp.setWidths(medidaCeldas);
        Font fuente = new Font(Font.HELVETICA, 10);
        Font fuenteB = new Font(Font.HELVETICA, 10, Font.BOLD);
        PdfPCell celda_temp = new PdfPCell();



        celda_temp.setPhrase(new Phrase(" ", fuenteB));
        celda_temp.setColspan(5);
        celda_temp.setBorder(0);
        tabla_temp.addCell(celda_temp);

        celda_temp.setPhrase(new Phrase("PROPIETARIO", fuenteB));
        celda_temp.setColspan(0);
        celda_temp.setBorder(0);
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);

        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase(anticipo.getPla_owner(), fuente));
        celda_temp.setColspan(0);
        celda_temp.setBorder(0);
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);

        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("NOMBRE ", fuenteB));
        celda_temp.setColspan(0);
        celda_temp.setBorder(0);
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);

        // fila 2
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase(anticipo.getNombrePropietario(), fuente));
        celda_temp.setBorder(0);
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);


        celda_temp.setPhrase(new Phrase(" ", fuenteB));
        celda_temp.setColspan(5);
        celda_temp.setBorder(0);
        celda_temp.setBorderWidthBottom(1);
        tabla_temp.addCell(celda_temp);

        celda_temp.setPhrase(new Phrase(" ", fuenteB));
        celda_temp.setColspan(5);
        celda_temp.setBorder(0);
        tabla_temp.addCell(celda_temp);


        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("C.C. CONDUCTOR     ", fuenteB));
        celda_temp.setColspan(0);
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        celda_temp.setBorder(0);
        tabla_temp.addCell(celda_temp);

        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase(anticipo.getConductor(), fuente));
        celda_temp.setColspan(4);
        celda_temp.setBorder(0);
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);


        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("CONDUCTOR         ", fuenteB));
        celda_temp.setColspan(0);
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        celda_temp.setBorder(0);
        tabla_temp.addCell(celda_temp);

        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase(""+anticipo.getNombreConductor(), fuente));
        celda_temp.setColspan(4);
        celda_temp.setBorder(0);
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);



        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("PLACA         ", fuenteB));
        celda_temp.setColspan(0);
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        celda_temp.setBorder(0);
        tabla_temp.addCell(celda_temp);

        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase(""+anticipo.getSupplier(), fuente));
        celda_temp.setColspan(4);
        celda_temp.setBorder(0);
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);



        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("PLANILLA         ", fuenteB));
        celda_temp.setColspan(0);
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        celda_temp.setBorder(0);
        tabla_temp.addCell(celda_temp);

        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase(""+anticipo.getPlanilla(), fuente));
        celda_temp.setColspan(4);
        celda_temp.setBorder(0);
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);




        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("AGENCIA         ", fuenteB));
        celda_temp.setColspan(0);
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        celda_temp.setBorder(0);
        tabla_temp.addCell(celda_temp);

        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase(anticipo.getNombreAgencia(), fuente));
        celda_temp.setColspan(4);
        celda_temp.setBorder(0);
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);



        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("DESPACHADOR         ", fuenteB));
        celda_temp.setColspan(0);
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        celda_temp.setBorder(0);
        tabla_temp.addCell(celda_temp);

        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase(anticipo.getUsuario_creacion(), fuente));
        celda_temp.setColspan(4);
        celda_temp.setBorder(0);
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);

        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("FECHA ANTICIPO     ", fuenteB));
        celda_temp.setColspan(0);
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        celda_temp.setBorder(0);
        tabla_temp.addCell(celda_temp);

        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase(anticipo.getFecha_creacion(), fuente));
        celda_temp.setColspan(4);
        celda_temp.setBorder(0);
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);

        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("TIPO ANTICIPO      ", fuenteB));
        celda_temp.setColspan(0);
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        celda_temp.setBorder(0);
        tabla_temp.addCell(celda_temp);

        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase(anticipo.getConcept_code(), fuente));
        celda_temp.setColspan(4);
        celda_temp.setBorder(0);
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);

                celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("VALOR ANTICIPO     ", fuenteB));
        celda_temp.setColspan(0);
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        celda_temp.setBorder(0);
        tabla_temp.addCell(celda_temp);

        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase(Utility.customFormat(anticipo.getVlr()), fuente));
        celda_temp.setColspan(4);
        celda_temp.setBorder(0);
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);

        return tabla_temp;
    }


















        /**************************** tabla facturas*******************************************/


        /**
     * Crea un tabla con los datos de las facturas del propietario de la liquidacion OC
     * @throws DocumentException
     * @author ivargas-Geotech
     * @date 12/04/2011
     * @version 1.0
     */
    protected PdfPTable facturas(List<ExtractoDetalle> lista_extracto_detalle,String documento,String placa) throws DocumentException {

        float vlr=0,rtf=0,rti=0;

        PdfPTable tabla_temp = new PdfPTable(5);
        float[] medidaCeldas = {0.100f, 0.310f, 0.170f, 0.210f,0.210f};
        tabla_temp.setWidths(medidaCeldas);
        Font fuente = new Font(Font.HELVETICA, 10);
        Font fuenteB = new Font(Font.HELVETICA, 10, Font.BOLD);
        PdfPCell celda_temp = new PdfPCell();

        /*---------RECORRER CADA DOCUMETO-----------*/



        celda_temp.setPhrase(new Phrase(" ", fuenteB));
        celda_temp.setColspan(6);
        celda_temp.setBorder(0);
        celda_temp.setBorderWidthBottom(1);
        tabla_temp.addCell(celda_temp);

        celda_temp.setPhrase(new Phrase("FACTURA  "+documento, fuenteB));
        celda_temp.setColspan(2);
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        celda_temp.setBorder(0);
        tabla_temp.addCell(celda_temp);

        celda_temp.setPhrase(new Phrase("PLACA    "+placa, fuenteB));
        celda_temp.setColspan(4);
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        celda_temp.setBorder(0);
        tabla_temp.addCell(celda_temp);

        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase(" ", fuenteB));
        celda_temp.setColspan(6);
        celda_temp.setBorder(0);
        tabla_temp.addCell(celda_temp);
        // fila 2

        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Item", fuenteB));
        celda_temp.setBorder(0);
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Descripcion", fuenteB));
        celda_temp.setBorder(0);
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Valor", fuenteB));
        celda_temp.setBorder(0);
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Retefuente", fuenteB));
        celda_temp.setBorder(0);
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Reteica", fuenteB));
        celda_temp.setBorder(0);
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);

       for (int i=0;i<lista_extracto_detalle.size();i++)
       {
           vlr=vlr+lista_extracto_detalle.get(i).getVlr();
           rtf=rtf+lista_extracto_detalle.get(i).getRetefuente();
           rti=rti+lista_extracto_detalle.get(i).getReteica();


            // fila 3 + contDescuentos

            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase(lista_extracto_detalle.get(i).getConcepto(), fuente));
            celda_temp.setBorder(0);
            celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            tabla_temp.addCell(celda_temp);
            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase(lista_extracto_detalle.get(i).getDescripcion(), fuente));
            celda_temp.setBorder(0);
            tabla_temp.addCell(celda_temp);
            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase(Utility.customFormat(lista_extracto_detalle.get(i).getVlr()), fuente));
            celda_temp.setBorder(0);
            celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
            tabla_temp.addCell(celda_temp);
            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase(Utility.customFormat(lista_extracto_detalle.get(i).getRetefuente()), fuente));
            celda_temp.setBorder(0);
            celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
            tabla_temp.addCell(celda_temp);
            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase(Utility.customFormat(lista_extracto_detalle.get(i).getReteica()), fuente));
            celda_temp.setBorder(0);
            celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
            tabla_temp.addCell(celda_temp);


        }

        // fila 3 + ajustesPropietario.size()
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("TOTAL FACTURAS ", fuenteB));
        celda_temp.setColspan(2);
        celda_temp.setBorder(0);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase(Utility.customFormat(vlr), fuenteB));
        celda_temp.setBorder(0);
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase(Utility.customFormat(rtf), fuenteB));
        celda_temp.setBorder(0);
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase(Utility.customFormat(rti), fuenteB));
        celda_temp.setBorder(0);
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
        tabla_temp.addCell(celda_temp);



        /*--------------------------total a pagar--------------------------------------*/
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("TOTAL A PAGAR ", fuenteB));
        celda_temp.setColspan(4);
        celda_temp.setBorder(0);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase(Utility.customFormat(lista_extracto_detalle.get(0).getVlr_ppa_item()), fuenteB));
        celda_temp.setBorder(0);
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
        tabla_temp.addCell(celda_temp);

        return tabla_temp;
    }













    /************************************** facturas de propietario***************************************************/
  protected PdfPTable facturasPropietario(List<ExtractoDetalle> lista_extracto_detalle) throws DocumentException {

        float vlr=0,ip=0;

        PdfPTable tabla_temp = new PdfPTable(6);
        float[] medidaCeldas = {0.160f, 0.100f, 0.260f, 0.160f, 0.160f, 0.160f};
        tabla_temp.setWidths(medidaCeldas);
        Font fuente = new Font(Font.HELVETICA, 10);
        Font fuenteB = new Font(Font.HELVETICA, 10, Font.BOLD);
        PdfPCell celda_temp = new PdfPCell();

        celda_temp.setPhrase(new Phrase(" ", fuenteB));
        celda_temp.setColspan(6);
        celda_temp.setBorder(0);
        tabla_temp.addCell(celda_temp);

        celda_temp.setPhrase(new Phrase("FACTURAS DE PROPIETARIO", fuenteB));
        celda_temp.setColspan(6);
        celda_temp.setBorder(0);
        tabla_temp.addCell(celda_temp);

        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase(" ", fuenteB));
        celda_temp.setColspan(6);
        celda_temp.setBorder(0);
        celda_temp.setBorderWidthBottom(1);
        tabla_temp.addCell(celda_temp);
        // fila 2
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Factura", fuenteB));
        celda_temp.setBorder(0);
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Item", fuenteB));
        celda_temp.setBorder(0);
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Descripcion", fuenteB));
        celda_temp.setBorder(0);
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Valor", fuenteB));
        celda_temp.setBorder(0);
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Impuestos", fuenteB));
        celda_temp.setBorder(0);
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Saldo", fuenteB));
        celda_temp.setBorder(0);
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);

       for (int i=0;i<lista_extracto_detalle.size();i++)
       {
           vlr=vlr+lista_extracto_detalle.get(i).getVlr();
           ip=ip+ (lista_extracto_detalle.get(i).getRetefuente()+lista_extracto_detalle.get(i).getReteica());



            // fila 3 + contDescuentos
            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase(lista_extracto_detalle.get(i).getDocumento(), fuente));
            celda_temp.setBorder(0);
            celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            tabla_temp.addCell(celda_temp);
            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase(lista_extracto_detalle.get(i).getConcepto(), fuente));
            celda_temp.setBorder(0);
            tabla_temp.addCell(celda_temp);
            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase(lista_extracto_detalle.get(i).getDescripcion(), fuente));
            celda_temp.setBorder(0);
            celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
            tabla_temp.addCell(celda_temp);
            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase(Utility.customFormat(lista_extracto_detalle.get(i).getVlr()), fuente));
            celda_temp.setBorder(0);
            celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
            tabla_temp.addCell(celda_temp);
            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase(Utility.customFormat(lista_extracto_detalle.get(i).getRetefuente()+lista_extracto_detalle.get(i).getReteica()), fuente));
            celda_temp.setBorder(0);
            celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
            tabla_temp.addCell(celda_temp);
            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase(Utility.customFormat(lista_extracto_detalle.get(i).getVlr_ppa_item()), fuente));
            celda_temp.setBorder(0);
            celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
            tabla_temp.addCell(celda_temp);


        }

        // fila 3 + ajustesPropietario.size()
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("TOTAL FACTURAS PROPIETARIO", fuenteB));
        celda_temp.setColspan(3);
        celda_temp.setBorder(0);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase(Utility.customFormat(vlr), fuenteB));
        celda_temp.setBorder(0);
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase(Utility.customFormat(ip), fuenteB));
        celda_temp.setBorder(0);
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase(Utility.customFormat(vlr), fuenteB));
        celda_temp.setBorder(0);
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
        tabla_temp.addCell(celda_temp);

        return tabla_temp;
    }


         /*--------------------------------------------------------------*/
        protected PdfPTable datosGenerales(float tl,float tfp) {

        PdfPTable tabla_temp = new PdfPTable(2);
        Font fuente = new Font(Font.HELVETICA, 10);
        Font fuenteB = new Font(Font.HELVETICA, 10, Font.BOLD);
        PdfPCell celda_temp = new PdfPCell();

        celda_temp.setPhrase(new Phrase(" ", fuenteB));
        celda_temp.setColspan(2);
        celda_temp.setBorder(0);
        tabla_temp.addCell(celda_temp);

        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("DATOS DE GENERALES DE LIQUIDACION", fuenteB));
        celda_temp.setColspan(2);
        celda_temp.setBorder(0);
        tabla_temp.addCell(celda_temp);

        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase(" ", fuenteB));
        celda_temp.setColspan(2);
        celda_temp.setBorder(0);
        celda_temp.setBorderWidthBottom(1);
        tabla_temp.addCell(celda_temp);

        // fila 2
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("TOTAL PLANILLAS", fuente));
        celda_temp.setBorder(0);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase(Utility.customFormat(tl), fuente));
        celda_temp.setBorder(0);
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("FACTURAS CORRIDA NEGATIVA", fuente));
        celda_temp.setBorder(0);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase(Utility.customFormat(0.0), fuente));
        celda_temp.setBorder(0);
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("FACTURAS PROPIETARIO", fuente));
        celda_temp.setBorder(0);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase(Utility.customFormat(tfp), fuente));
        celda_temp.setBorder(0);
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("TOTAL FACTURAS", fuente));
        celda_temp.setBorder(0);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase(Utility.customFormat(tfp), fuente));
        celda_temp.setBorder(0);
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("TOTAL LIQUIDACION", fuenteB));
        celda_temp.setBorder(0);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase(Utility.customFormat(tl+tfp), fuenteB));
        celda_temp.setBorder(0);
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
        tabla_temp.addCell(celda_temp);

        return tabla_temp;
    }




    public float total_fp(List<ExtractoDetalle> lista_extracto_detalle)
    {
    float tfp=0;
           for (int i=0;i<lista_extracto_detalle.size();i++)
            {
             tfp=tfp+lista_extracto_detalle.get(i).getVlr_ppa_item();
           }

    return tfp;
    }






























  public boolean envia_correo()
    {
     boolean enviado=false;
     try
       {


         /************************* Envio de Correo*************************/
           Email2 emailData = null;
           String ahora = new java.util.Date().toString();
            emailData=new Email2();
            emailData.setEmailId(12345);
            emailData.setRecstatus("A");//A
            emailData.setEmailcode( "emailcode");
            emailData.setEmailfrom(fromx);
            emailData.setEmailto(tox );
            emailData.setEmailcopyto(copytox);
            emailData.setEmailHiddencopyto(hiddencopytox);
            emailData.setEmailsubject(asuntox);//"WebServiceMultiple_Fintra2" );
            emailData.setEmailbody(msgx);
            emailData.setLastupdat(new Timestamp(System.currentTimeMillis()) );
            emailData.setSenderName( "sender name" );
            emailData.setRemarks("remark2");
            emailData.setTipo("tipito");
            emailData.setRutaArchivo(ruta);
            emailData.setNombreArchivos(nombre_archivos);



           
            EmailSendingEngineService emailSendingEngineService=new EmailSendingEngineService("smtpbar.une.net.co", "fintravalores@geotechsa.com", "fintra21");
            //.out.println("casi send");
           enviado= emailSendingEngineService.send(emailData);
            emailSendingEngineService=null;//091206
            /*************************Fin Envio de Correo*************************/
           }
           catch(Exception e)
           {

               System.out.println("Error  "+e.getMessage());
           }

     return enviado;
    }


           public HeaderFooter getMyFooter() throws ServletException {
        try {
             Phrase p = new Phrase();
            Font fuente = new Font(Font.HELVETICA, 9);
            Font fuenteB = new Font(Font.HELVETICA, 9, Font.BOLD);
             p.clear();


             p.add(new Paragraph("Carrera 53 # 79 - 01 Local 205 Barranquilla, Colombia\n",fuenteB));
             p.add(new Paragraph("PBX: 57 5 3679900 FAX: 57 5 3679906\n",fuenteB));
             p.add(new Paragraph("www.fintravalores.com\n",fuenteB));
             HeaderFooter header = new HeaderFooter(p, false);
             header.setBorder(Rectangle.NO_BORDER);
             header.setAlignment(Paragraph.ALIGN_CENTER);

             return header;
         } catch (Exception ex) {
            throw new ServletException("Header Error");
         }
     }






/**
     * Crea un objeto tipo ExcelApplication
     * @param descripcion Nombre de la hoja principal del libro
     * @return Objeto ExcelApplication creado
     * @throws Exception Cuando hay error
     */
    private ExcelApplication instanciar(String descripcion) throws Exception{
       excel = new ExcelApplication();

        try{
            excel.createSheet(descripcion);

            excel.createColor((short)11, (byte)255, (byte)255, (byte)255);//Blanco
            excel.createColor((short)9, (byte)26, (byte)126, (byte)0);//Verde dark
            excel.createColor((short)10, (byte)37, (byte)69, (byte)255);//Azul oscuro
            excel.createColor((short)8, (byte)234, (byte)241, (byte)221);//Verde claro
            //(153, 204, 255);

            excel.createFont("Titulo", "Arial", (short)11, true, (short)12);
            excel.createFont("Subtitulo", "Verdana", (short)0, true, (short)10);
            excel.createFont("Titulo", "Verdana", (short)11, true, (short)10);
            excel.createFont("Contenido", "Verdana", (short)0, false, (short)10);
            excel.createFont("t_liqui", "Verdana", (short)0, true, (short)10);//aliniado ala ezquierda negrita
            excel.createFont("Valor_total", "Verdana", (short)0, true, (short)10);//aliniado ala ezquierda negrita

                      
            excel.createStyle("estilo3", excel.getFont("Contenido"), (short)11, true, "@");
            excel.createStyle("estilo2", excel.getFont("Titulo"), (short)9, true, "@",(short)2);
            excel.createStyle("estilo5", excel.getFont("Subtitulo"), (short)8, true, "@",(short)2);//centrado cabecera tabla
            excel.createStyle("estilo1",excel.getFont("Contenido"), (short)11, true, "@",(short)2);//centrado normal


            excel.createStyle("estilo6", excel.getFont("Contenido"), (short)11, true, "@",(short)1);//aliniado ala ezquierda normal
            excel.createStyle("estilo7", excel.getFont("t_liqui"), (short)11, true, "@",(short)1);//aliniado ala ezquierda negrita
            excel.createStyle("estilo8", excel.getFont("Valor_total"), (short)11, true, "@");// contenido negrita
            excel.createStyle("estilo9", excel.getFont("Valor_total"), (short)11, true, "@",(short)1);// contenido negrita centrado
           


        }
        catch(Exception e){
            throw new Exception("Error al instanciar el objeto: "+e.toString());
        }
        return excel;
    }



               /**
     * Genera el xls del estado de cuenta
     * @param user Usuario que lo genera
     * @param listafacts Lista de facturas
     * @param listaing Lista de ingresos
     * @param listahead Datos para el encabezado
     * @param sal Saldo del negocio
     * @param vec Otros datos del cliente que se incluyen
     * @throws Exception Cuando hay un error
     */
    private boolean generarXls(int secuencia) throws Exception{

           /**********anticipos pronto pago***************/

           AnticiposPagosTercerosDAO antDAO=new AnticiposPagosTercerosDAO();
           AnticiposTerceros ant =null;
           ant=antDAO.getAnticipo_x_secuencia(secuencia); //164056

            /********** extracto pronto pago***************/
           ExtractoDAO etDAO=new ExtractoDAO();
           List<ExtractoDetalle> lista_extracto_detalle = new LinkedList<ExtractoDetalle>();
           List<ExtractoDetalle> lista_doc_extracto_detalle = new LinkedList<ExtractoDetalle>();
           lista_doc_extracto_detalle=etDAO.Lista_doc_extracto_det(ant.getSecuencia(), ant.getPla_owner());
           List<ExtractoDetalle> lista_factura_propietario = new LinkedList<ExtractoDetalle>();
           lista_factura_propietario=etDAO.Lista_facturas_propietario(ant.getSecuencia(), ant.getPla_owner() );
           float tl=0;
           boolean generado = true;
           String directorio = "";String documento_detalle="";String placa="";
           ResourceBundle rb = null;
           try
           {
               rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            nombre_archivos[1]="liquidacion.xls";
            directorio = this.directorioArchivo("liquidacion", "xls");
            ExcelApplication excel = this.instanciar("Extracto");
            excel.combinarCeldas(0, 0, 2, 4);
            fila=fila+3;
            String ruta_img = rb.getString("ruta") + "/images/fintrapdf.gif";
            excel.insertaImagen(ruta_img,0,0,10,200,0,0,1,2);

          
            excel.setDataCell(fila, 0, "PROPIETARIO");
            excel.setCellStyle(fila, 0, excel.getStyle("estilo2"));
            excel.setDataCell(fila, 1, ant.getPla_owner());
            excel.setCellStyle(fila, 1, excel.getStyle("estilo2"));
          



            excel.setCellStyle(fila+1, 0, excel.getStyle("estilo2"));
            excel.setDataCell(fila+1, 0, "NOMBRE");
            excel.setDataCell(fila+1, 1,ant.getNombrePropietario() );
            excel.setCellStyle(fila+1, 1, excel.getStyle("estilo2"));
            
            fila = fila +2;


            for (int j = 0; j < lista_doc_extracto_detalle.size(); j++) {
                   documento_detalle = lista_doc_extracto_detalle.get(j).getDocumento();
                   placa = lista_doc_extracto_detalle.get(j).getPlaca();
                   lista_extracto_detalle = etDAO.Lista_extracto_det(ant.getSecuencia(), ant.getPla_owner(), documento_detalle);

                   facturasXls(lista_extracto_detalle, documento_detalle, placa);
                   fila = fila + j;
                   tl = tl + lista_extracto_detalle.get(0).getVlr_ppa_item();


               }

               /*----facturas propietarios-----*/
               if (lista_factura_propietario.size() > 0) {
                   facturasPropietarioXls(lista_factura_propietario);
               }
               datosGeneralesXls(tl, this.total_fp(lista_factura_propietario));

               fila = 0;
               excel.saveToFile(directorio);
        }
        catch (Exception e) {
            generado=false;
            throw new Exception("Error al generar el archivo xls: "+e.toString());

        }


        return generado;
    }


    /**
     * Crea un tabla con los datos de las facturas del propietario de la liquidacion OC
     * @throws DocumentException
     * @author JPINEDO-FINTRA
     * @date 12/04/2011
     * @version 1.0
     */
    protected void facturasXls(List<ExtractoDetalle> lista_extracto_detalle,String documento,String placa) throws DocumentException, Exception {

        float vlr = 0, rtf = 0, rti = 0;
        fila = fila +3;
        /*---------RECORRER CADA DOCUMETO-----------*/
        excel.setDataCell(fila-1, 0, "FACTURA");
        excel.cambiarAnchoColumna(0, 6000);
        excel.setCellStyle(fila-1, 0, excel.getStyle("estilo2"));
        excel.setDataCell(fila-1, 1, documento);
        excel.cambiarAnchoColumna(1, 14000);
        excel.setCellStyle(fila-1, 1, excel.getStyle("estilo2"));

        excel.setDataCell(fila-1, 2, " ");
        excel.cambiarAnchoColumna(2, 6000);
        excel.setCellStyle(fila-1, 2, excel.getStyle("estilo2"));


        excel.setDataCell(fila-1, 3, "PLACA");
        excel.cambiarAnchoColumna(3, 6000);
        excel.setCellStyle(fila-1, 3, excel.getStyle("estilo2"));
        excel.setDataCell(fila-1, 4, placa);
        excel.cambiarAnchoColumna(4, 6000);
        excel.setCellStyle(fila-1, 4, excel.getStyle("estilo2"));


        // fila 4
        excel.setDataCell(fila, 0, "Item");
        excel.setCellStyle(fila, 0, excel.getStyle("estilo5"));
        
        excel.setDataCell(fila, 1, "Descripcion");
        excel.setCellStyle(fila, 1, excel.getStyle("estilo5"));

        excel.setDataCell(fila, 2, "Valor");
        excel.setCellStyle(fila, 2, excel.getStyle("estilo5"));
       
        excel.setDataCell(fila, 3, "Retefuente");
        excel.setCellStyle(fila, 3, excel.getStyle("estilo5"));

        
        excel.setDataCell(fila, 4, "Reteica");
        excel.setCellStyle(fila, 4, excel.getStyle("estilo5"));

       fila=fila+1;
       for (int i=0;i<lista_extracto_detalle.size();i++)
       {
           vlr=vlr+lista_extracto_detalle.get(i).getVlr();
           rtf=rtf+lista_extracto_detalle.get(i).getRetefuente();
           rti=rti+lista_extracto_detalle.get(i).getReteica();
            // fila 3 + contDescuentos

             excel.setDataCell(i+fila, 0, lista_extracto_detalle.get(i).getConcepto());
             excel.setCellStyle(i+fila, 0, excel.getStyle("estilo1"));

             excel.setDataCell(i+fila, 1, lista_extracto_detalle.get(i).getDescripcion());
             excel.setCellStyle(i+fila, 1, excel.getStyle("estilo6"));

             excel.setDataCell(i+fila, 2, Utility.customFormat(lista_extracto_detalle.get(i).getVlr()));
             excel.setCellStyle(i+fila, 2, excel.getStyle("estilo3"));

             excel.setDataCell(i+fila, 3, Utility.customFormat(lista_extracto_detalle.get(i).getRetefuente()));
             excel.setCellStyle(i+fila, 3, excel.getStyle("estilo3"));

             excel.setDataCell(i+fila, 4, Utility.customFormat(lista_extracto_detalle.get(i).getReteica()));
             excel.setCellStyle(i+fila, 4, excel.getStyle("estilo3"));

        }
        fila = fila + lista_extracto_detalle.size();
        // fila 3 + ajustesPropietario.size()

        excel.setDataCell(fila, 0, "TOTAL FACTURAS");
        excel.setCellStyle(fila, 0, excel.getStyle("estilo9"));
        excel.combinarCeldas(fila, 0, fila, 1);

        excel.setDataCell(fila, 2, Utility.customFormat(vlr));
        excel.setCellStyle(fila, 2, excel.getStyle("estilo8"));

        excel.setDataCell(fila, 3, Utility.customFormat(rtf));
        excel.setCellStyle(fila, 3, excel.getStyle("estilo8"));

        excel.setDataCell(fila, 4, Utility.customFormat(rti));
        excel.setCellStyle(fila, 4, excel.getStyle("estilo8"));

        fila=fila+1;
        excel.setDataCell(fila, 0, "TOTAL A PAGAR ");
        excel.setCellStyle(fila, 0, excel.getStyle("estilo9"));
        excel.combinarCeldas(fila, 0, fila, 1);
        excel.setDataCell(fila, 4, Utility.customFormat(lista_extracto_detalle.get(0).getVlr_ppa_item()));
        excel.setCellStyle(fila, 4, excel.getStyle("estilo8"));
        fila=fila+1;

    }


        /**
     * Crea un tabla con los datos de las facturas del propietario de la liquidacion OC
     * @throws DocumentException
     * @author JPINEDO-FINTRA
     * @date 12/04/2011
     * @version 1.0
     */
    protected void facturasPropietarioXls(List<ExtractoDetalle> lista_extracto_detalle) throws DocumentException, Exception {

        float vlr = 0, rtf = 0, rti = 0, ip=0;
        fila = fila +3;



        // fila 4
        excel.setDataCell(fila, 0, "Item");
        excel.setCellStyle(fila, 0, excel.getStyle("estilo5"));

        excel.setDataCell(fila, 1, "Descripcion");
        excel.setCellStyle(fila, 1, excel.getStyle("estilo5"));

        excel.setDataCell(fila, 2, "Valor");
        excel.setCellStyle(fila, 2, excel.getStyle("estilo5"));

        excel.setDataCell(fila, 3, "Impuestos");
        excel.setCellStyle(fila, 3, excel.getStyle("estilo5"));


        excel.setDataCell(fila, 4, "Saldo");
        excel.setCellStyle(fila, 4, excel.getStyle("estilo5"));

       fila=fila+1;
       for (int i=0;i<lista_extracto_detalle.size();i++)
       {
           vlr=vlr+lista_extracto_detalle.get(i).getVlr();
           rtf=rtf+lista_extracto_detalle.get(i).getRetefuente();
           rti=rti+lista_extracto_detalle.get(i).getReteica();
           ip=ip+ (lista_extracto_detalle.get(i).getRetefuente()+lista_extracto_detalle.get(i).getReteica());
            // fila 3 + contDescuentos

             excel.setDataCell(i+fila, 0, lista_extracto_detalle.get(i).getConcepto());
             excel.setCellStyle(i+fila, 0, excel.getStyle("estilo1"));

             excel.setDataCell(i+fila, 1, lista_extracto_detalle.get(i).getDescripcion());
             excel.setCellStyle(i+fila, 1, excel.getStyle("estilo6"));

             excel.setDataCell(i+fila, 2, Utility.customFormat(lista_extracto_detalle.get(i).getVlr()));
             excel.setCellStyle(i+fila, 2, excel.getStyle("estilo3"));

             excel.setDataCell(i+fila, 3, Utility.customFormat(lista_extracto_detalle.get(i).getRetefuente()+lista_extracto_detalle.get(i).getReteica()));
             excel.setCellStyle(i+fila, 3, excel.getStyle("estilo3"));

             excel.setDataCell(i+fila, 4, Utility.customFormat(lista_extracto_detalle.get(i).getReteica()));
             excel.setCellStyle(i+fila, 4, excel.getStyle("estilo3"));

        }
        fila = fila + lista_extracto_detalle.size();
        // fila 3 + ajustesPropietario.size()

        excel.setDataCell(fila, 0, "TOTAL FACTURAS PROPIETARIO");
        excel.setCellStyle(fila, 0, excel.getStyle("estilo9"));
        excel.combinarCeldas(fila, 0, fila, 1);

        excel.setDataCell(fila, 2, Utility.customFormat(vlr));
        excel.setCellStyle(fila, 2, excel.getStyle("estilo8"));

        excel.setDataCell(fila, 3, Utility.customFormat(ip));
        excel.setCellStyle(fila, 3, excel.getStyle("estilo8"));

        excel.setDataCell(fila, 4, Utility.customFormat(vlr));
        excel.setCellStyle(fila, 4, excel.getStyle("estilo8"));
        fila=fila+1;

    }


 /**
     * Crea un tabla con los datos de las facturas del propietario de la liquidacion OC
     * @throws DocumentException
     * @author JPINEDO-FINTRA
     * @date 12/04/2011
     * @version 1.0
     */
     protected void datosGeneralesXls(float tl,float tfp) throws Exception {


        fila = fila +1;
        /*---------RECORRER CADA DOCUMETO-----------*/
        excel.setDataCell(fila, 1, "DATOS DE GENERALES DE LIQUIDACION") ;
        excel.setCellStyle(fila, 1, excel.getStyle("estilo2"));
        excel.combinarCeldas(fila, 1, fila,2);


        fila = fila +1;
        excel.setDataCell(fila, 1, "TOTAL PLANILLAS");
        excel.setCellStyle(fila, 1, excel.getStyle("estilo6"));
        excel.setDataCell(fila, 2, Utility.customFormat(tl));
        excel.setCellStyle(fila, 2, excel.getStyle("estilo3"));

        fila = fila +1;
        excel.setDataCell(fila, 1, "FACTURAS CORRIDA NEGATIVA");
        excel.setCellStyle(fila, 1, excel.getStyle("estilo6"));
        excel.setDataCell(fila, 2,0);
        excel.setCellStyle(fila, 2, excel.getStyle("estilo3"));

        fila = fila +1;
        excel.setDataCell(fila, 1, "FACTURAS PROPIETARIO");
        excel.setCellStyle(fila, 1, excel.getStyle("estilo6"));
        excel.setDataCell(fila, 2,Utility.customFormat(tfp));
        excel.setCellStyle(fila, 2, excel.getStyle("estilo3"));

        fila = fila +1;
        excel.setDataCell(fila, 1, "TOTAL FACTURAS");
        excel.setCellStyle(fila, 1, excel.getStyle("estilo6"));
        excel.setDataCell(fila, 2,Utility.customFormat(tfp));
        excel.setCellStyle(fila, 2, excel.getStyle("estilo3"));

        fila = fila +1;
        excel.setDataCell(fila, 1, "TOTAL LIQUIDACION");
        excel.setCellStyle(fila, 1, excel.getStyle("estilo7"));
        excel.setDataCell(fila, 2,Utility.customFormat(tl+tfp));
        excel.setCellStyle(fila, 2, excel.getStyle("estilo8"));

    }

    



}











