/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.util.cron;

import java.io.DataInputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ResourceBundle;

/**
 *
 * @author Ing. Iris Vargas
 */
public class FacturasCAT implements Runnable {
    private Thread thread = null;

	private String direccion = null;

	public FacturasCAT() {}

	public void init() {
		ResourceBundle bundle = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
		this.direccion = bundle.getString("facturas_cat");
		this.thread = new Thread(this);
		this.thread.start();
	}
        
        public void init(String ciclo,String periodo) throws InterruptedException {
		ResourceBundle bundle = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
		this.direccion = bundle.getString("facturas_cat")+ "?ciclo=" + ciclo + "&periodo=" + periodo;
		this.thread = new Thread(this);
		this.thread.start();
                this.thread.join();
	}



    @Override
    public void run() {
        HttpURLConnection conexion = null;
		try {
			URL url = new URL(this.direccion);
			conexion = (HttpURLConnection) url.openConnection();
			conexion.connect();

			switch (conexion.getResponseCode()) {
				case HttpURLConnection.HTTP_OK:
					System.out.println(">>> CONEXION ESTABLECIDA " + url.toString() + "...\n");

					InputStream input = conexion.getInputStream();
					DataInputStream data = new DataInputStream(input);

					String linea = "";
					while ((linea = data.readLine()) != null) {
						System.out.println(linea);
					}
					data.close();
					input.close();
					break;
				case HttpURLConnection.HTTP_NOT_FOUND:
					System.err.println(">>> ERROR: Direccion " + this.direccion + " no encontrada");
					break;
				default:
					System.err.println(">>> ESTADO HTTP: " + conexion.getResponseCode());
					break;
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			conexion.disconnect();
			System.out.println(">>> CRON FINALIZADO.\n");
		}
	 System.out.println(">>> FIN.\n");
    }

    public static void main(String arg[]) {
		FacturasCAT a = new FacturasCAT();
		a.init();
	}

}
