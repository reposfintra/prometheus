/*
 * HNovedadesClipper.java
 *
 * Created on 8 de abril de 2005, 04:08 PM
 */

package com.tsp.util.cron;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.text.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;

/**
 *
 * @author  KREALES
 */
public class HActualizarDatos extends Thread{
    
    
    private  static final String SQL_ACTUALIZAR_MOVPLA  ="update movpla set vlr_for = vlr where currency ='PES' and base !='COL' AND VLR <0;";
    private  static final String SQL_ACTUALIZAR_REMESA  ="UPDATE remesa SET vlrrem2 = ROUND(vlrrem2) WHERE vlrrem2-TRUNC(vlrrem2)>0; ";
    
    
    /** Creates a new instance of HNovedadesClipper */
    public HActualizarDatos() {
    }
    public synchronized void run(){
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try {
            //System.out.println("Entro al hilo...");
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("sot");
            if(con!=null){
                //System.out.println("genero la consulta ");
                
                st = con.prepareStatement(SQL_ACTUALIZAR_MOVPLA);
                st.executeUpdate();
                
                st = con.prepareStatement(SQL_ACTUALIZAR_REMESA);
                st.executeUpdate();
            }
            
        }catch(Exception e){
            System.out.println("Error "+e.getMessage());
          //  throw new Exception("ERROR ESCRIBIENDO EL ARCHIVO DE NOVEDADES " + e.getMessage() );
            
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(Exception e){
                   // throw new Exception("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("sot", con);
            }
        }
   }
    
    public static void main(String a [])throws Exception{
        try{
            HActualizarDatos hnc = new HActualizarDatos();
            hnc.start();
        }catch(Exception e){
            System.out.println("Error "+e.getMessage());
        }
    }
    
}
