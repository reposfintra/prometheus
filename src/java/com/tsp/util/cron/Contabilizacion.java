/*
 * programa.java
 *
 * Created on 10 de noviembre de 2006, 04:34 PM
 */

package com.tsp.util.cron;


import com.tsp.finanzas.contab.model.threads.*;
import java.util.*;
import java.sql.*;
import com.tsp.util.GeneracionOP.*;
import com.tsp.operation.model.threads.*;
import com.tsp.operation.model.threads.HGenerarOPs;
import com.tsp.util.Utility;

/**
 *
 * @author  rrocha
 */
public class Contabilizacion extends Thread {
    
    
    /** Creates a new instance of programa */
    public Contabilizacion() {
    }
    
    /**
     * @param args the command line arguments
     */
     public void start(){
         super.start();
    }
    
     
    public static void main(String[] args)throws SQLException, Exception {
        int control = 0;        
        
        String tipoDoc = "";
        com.tsp.operation.model.beans.Usuario usuario = new com.tsp.operation.model.beans.Usuario();
        usuario.setLogin("ADMIN");
        usuario.setDstrct("FINV");
        usuario.setBd("");
        com.tsp.finanzas.contab.model.Model modelF = new com.tsp.finanzas.contab.model.Model(usuario.getBd());
        try {
            //Bloque Fac clientes
            System.out.println("Inicializacion contabilizacion facturas CXP...");
            HContabilizacionCL hiloCFC = new HContabilizacionCL();
            hiloCFC.start(modelF, usuario);
            hiloCFC.join();
            System.out.println("Fin contabilizaion facturas CXP...");
            hiloCFC = null;
            
            System.out.println("Iniciando Contabilizacion de Ingresos...");
            HContabilizacionIngresos hilo13 = new HContabilizacionIngresos();
            hilo13.start(usuario, new com.tsp.finanzas.contab.model.services.ContabilizacionIngresosServices()  );
            hilo13.join();
            System.out.println("Fin Contabilizacion de Ingresos...");
            hilo13 = null;
        
            System.out.println("Inicializacion contabilizacion egresos");
            HContabilizacionEgresos hilo5 =  new HContabilizacionEgresos();
            //tipoDoc= modelF.comprobanteService.getTipoDocumento("EGR");
            hilo5.start(usuario,"EGR","PES");
            hilo5.join();
            System.out.println("Fin contabilizaion egresos");
            hilo5 = null;
            
            System.out.println("Inicializacion contabilizacion facturas CXC");
            com.tsp.finanzas.contab.model.threads.HContabilizacion  hilo10 = new com.tsp.finanzas.contab.model.threads.HContabilizacion();
            String ayer =  Utility.convertirFecha( Utility.getHoy("-"), - 1);
            hilo10.start(modelF, usuario);
            hilo10.join();
            System.out.println("Fin contabilizaion facturas CXC");
            hilo10 = null;

        } catch (InterruptedException e) {
            e.printStackTrace();
            System.out.println("Error "+e.getMessage());
        }
        
    }
}