/*
 * ReporteUtilidad.java
 * @autor mfontalvo
 * Created on 22 de febrero de 2007, 08:05 AM
 */

package com.tsp.util.cron;

import com.tsp.operation.model.threads.HReporteUtilidadPlanillas;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.Model;
import com.tsp.util.UtilFinanzas;



public class ReporteUtilidad {
    
    /** Creates a new instance of ReporteUtilidad */
    public ReporteUtilidad() {
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        
         if (args.length==1 && args[0]!=null && args[0].trim().length()>0){
            HReporteUtilidadPlanillas h = new HReporteUtilidadPlanillas();
            String anoActual = UtilFinanzas.customFormatDate(h.fechaActual, "yyyy");
            String mesActual = UtilFinanzas.customFormatDate(h.fechaActual, "MM");
            Usuario u = new Usuario();
            u.setLogin(args[0].toUpperCase());
            u.setNombre("ADMINISTRADOR");
            h.start(new Model(u.getBd()), u, anoActual, mesActual );
        }
        else{
            //System.out.println("Usuario no definido para la generacion del Reporte Utilidad, por favor especifique el valor de este parametro.");
        }
    }
    
}
