package com.tsp.util;

import java.io.File;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
// Importa las clase de log4j
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

public class SetupLog4j extends HttpServlet {
    /** Initializa el servlet.
     */
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        // Lee el directorio donde va a ser colocado el archivo de logs
        String directory = getInitParameter("log-directory");
        // Adiciona el parametro del directorio como un Property del sistema
        // para que pueda ser utilizado dentro del archivo de configuración del Log4J
        System.setProperty("log.directory",directory);
        // Extrae el path donde se encuentra el contexto
        // Asume que el archivo de configuración se encuentra en este directorio
        String prefix = getServletContext().getRealPath("/");
        // Lee el nombre del archivo de configuración de Log4J
        String file = getInitParameter("log4j-init-file");
        if(file == null || file.length() == 0 ||
        !(new File(prefix+file)).isFile()){
          throw new ServletException(
            "ERROR: No puede leer el archivo de configuracion\n" +
            prefix + file +
            "\nDirectorio para Logging: " + directory
          );
        }else{
          log("Archivo de configuracion de log4j = " + prefix + file);
          log("Directorio para logging = " + directory);
        }
        // Revisa otra parámetro de configuración que le indica
        // si debe revisar el archivo de log por cambios.
        String watch = config.getInitParameter("watch");
        // Extrae el parámetro que le indica cada que tiempo debe revisar el archivo de configuración
        String timeWatch = config.getInitParameter("time-watch");
        // Revisa como debe realizar la configuración de Log4J y llama al método adecuado
        if (watch != null && watch.equalsIgnoreCase("true")) {
            if (timeWatch != null) {
                PropertyConfigurator.configureAndWatch(prefix+file,Long.parseLong(timeWatch));
            } else {
                PropertyConfigurator.configureAndWatch(prefix+file);
            }
        } else {
            PropertyConfigurator.configure(prefix+file);
        }
    }
    /** Destruye el servlet.
     */
    public void destroy() {
        super.destroy();
    }
}