/********************************************************************
 *      Nombre Clase.................   VGSeriePlanilla.java
 *      Descripci�n..................   Vigilante de serie de planillas
 *      Autor........................   Ing. Andr�s Maturana De La Cruz
 *      Fecha........................   09.11.2006
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes S�nchez Polo S.A.
 *******************************************************************/

package com.tsp.util.VGSeriePlanilla;

import com.tsp.operation.model.threads.*;
import com.tsp.operation.model.Model;

/**
 *
 * @author  Ing. Andr�s Maturana De La Cruz
 */
public class VGSeriePlanilla {
    
    /** Creates a new instance of VGSeriePlanilla */
    public VGSeriePlanilla() {
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try{
            Model model = new Model();
            VGSeriePlanillaTh  hilo  = new VGSeriePlanillaTh();
            hilo.start(model, "FINV");
        } catch ( Exception e ){
            e.printStackTrace();
        }
    }
    
}
