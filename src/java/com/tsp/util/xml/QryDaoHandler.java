/*
 * qryDaoParser.java
 *
 * Created on 22 de agosto de 2005, 09:50 PM
 */

package com.tsp.util.xml;

import org.xml.sax.*; 
import org.xml.sax.helpers.*;
import javax.xml.parsers.*;
import java.util.*;
import java.net.*;
import java.io.*;
import com.tsp.operation.model.beans.*;
import org.apache.log4j.Logger;

/**
 *
 * @author Armando
 * Clase para leer informacion de los archivos xml que alamcenan los querys para los daos 
 */
public class QryDaoHandler extends DefaultHandler{
    private StringBuffer buffer;
    private Hashtable<String, Sql> qrys; 
    private Sql sql;
    private String params = "";
    private boolean XMLHaSidoValidado = false;
    static transient Logger logger = Logger.getLogger(QryDaoHandler.class);
    private String rutaXML;
    private String debug;
       
    /** Creates a new instance of QryDaoHandler */
    public QryDaoHandler() {
        qrys =  new Hashtable<String, Sql>();
    }
    
    public void startElement(String uri, String localName, String qName, Attributes attrs)throws SAXException{
        if (qName.equals("statement")){
            sql = new Sql();
            sql.setName(attrs.getValue("name"));
            sql.setDb(attrs.getValue("db"));
        }
        else if ( qName.equals("queryDAO") ) {
            debug = attrs.getValue("debug");
        }
        buffer = new StringBuffer();
    }
    
    public String getDebug(){
        return debug;
    }
    
    public void endElement(String uri, String localName, String qName)throws SAXException{
        String text = buffer.toString().trim();
        if (qName.equals("param")){
            params = params + text + " ";
        }
        else if (qName.equals("desc")){
            sql.setDesc(text);
        }else if (qName.equals("sql")){
            sql.setSql(text);            
        }else if (qName.equals("statement")){
            params = params.trim();
            params = params.replace(" ", ",");
            sql.setParams(params.split(","));
            qrys.put(sql.getName(), sql);
            params = "";
            sql = null;
        }        
    }
    
    public void characters(char[] ch, int start, int length)throws SAXException{
        buffer.append(ch, start, length);        
    }
    
    /**
     *Metodo para obtener un objeto <code>Sql</code>
     *que contine informacion de un query.
     *@param name cadena que representa el nombre del
     *query en el archivo XML
     */
    public Sql getQry(String name){
        return qrys.get(name);
    }
    
    /**
     *Metodo para analizar el contenido de un archivo XML
     *definido por la dtd dtdDAO.dtd <code>http://tspweb.tsp.com:8080/sot/dtd/dtdDAO.dtd</code>
     *para almacenamiento de consultas.
     *@param spec Cadena que contine la url de la ubicacion
     *del archivo XML.
     */
    public void parse(String spec){
        this.XMLHaSidoValidado = rutaXML != null && rutaXML.equals(spec);
        if ( XMLHaSidoValidado ){
            return;
        }
        SAXParserFactory factory = SAXParserFactory.newInstance();
        SAXParser saxParser = null;
        URL url = null; 
        InputStream is = null;
        Sql qry = null;
        try{
            saxParser = factory.newSAXParser();
        }
        catch(ParserConfigurationException e){
            ////System.out.println("QryDaoHandler: EL ANALIZADOR XML NO PUEDE SER CREADO PORQUE NO CUMPLE CON LA CONFIGURACION SOLICITADA");
            e.printStackTrace();
            logger.info("QryDaoHandler: EL ANALIZADOR XML NO PUEDE SER CREADO PORQUE NO CUMPLE CON LA CONFIGURACION SOLICITADA",e);
            throw new RuntimeException(e);
        }
        catch(SAXException e){
            e.printStackTrace();
            logger.info("QryDaoHandler: ",e);
            throw new RuntimeException(e);
        }
       
        try{
            url = new URL(spec);
        }
        catch(MalformedURLException e){
            ////System.out.println("QryDaoHandler: LA CADENA DE LA URL ESPECIFICA UN PROTOCOLO DESCONOCIDO");
            logger.info("QryDaoHandler: LA CADENA DE LA URL ESPECIFICA UN PROTOCOLO DESCONOCIDO",e);
            throw new RuntimeException(e);
        }
        
        try{
            is = url.openStream();
        }
        catch(IOException e){
            ////System.out.println("QryDaoHandler: ERROR MIENTRAS SE LEIA EL ARCHIVO XML 1");
            logger.info("QryDaoHandler: ERROR MIENTRAS SE LEIA EL ARCHIVO XML 1",e);
            throw new RuntimeException(e);
        }
        
        try{
            saxParser.parse(is, this);
            XMLHaSidoValidado = true;
            rutaXML = spec;
        }
        catch(SAXException e){
            ////System.out.println("QryDaoHandler: ERROR DURANTE EL ANALISIS DEL ARCHIVO XML");
            e.printStackTrace();
            logger.info("QryDaoHandler: ERROR DURANTE EL ANALISIS DEL ARCHIVO XML",e);
            throw new RuntimeException(e);
        }
        catch(IOException e){
            ////System.out.println("QryDaoHandler: ERROR MIENTRAS SE LEIA EL ARCHIVO XML 2");
            e.printStackTrace();
            logger.info("QryDaoHandler: ERROR MIENTRAS SE LEIA EL ARCHIVO XML 2",e);
            throw new RuntimeException(e);
        }
    }
    
    public boolean XMLHasidoValidado(){
        return this.XMLHaSidoValidado;
    }
}
