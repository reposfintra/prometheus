

package com.tsp.util;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.text.*;
import java.util.Date;
import javax.crypto.*;
import javax.crypto.spec.*;
import java.security.*;
import java.security.spec.*;
import sun.misc.*;

public class Utility {
    
    /** Creates a new instpublic static String getIcono(String base_url,int op)ance of Utility */
    public Utility() {
    }
    
   
    
     public static String customFormat(double value) {
      DecimalFormat df = (DecimalFormat) NumberFormat.getNumberInstance(new Locale("en", "US"));
      df.applyPattern("#,###.##");
      df.setMaximumFractionDigits(2);
      df.setMinimumFractionDigits(2);            
      return df.format(value);
   }

    
    public static String getDate(int valor){
        SimpleDateFormat FMT = null;
        switch(valor)
        {
            case 0: FMT = new SimpleDateFormat("MMM");                 break; //Mes en espa�ol Ej: ene
            case 1: FMT = new SimpleDateFormat("yyyy");                break; //A�o en 4 digitos Ej: 2001
            case 2: FMT = new SimpleDateFormat("WW");                  break; //Semana del mes Ej: 02
            case 3: FMT = new SimpleDateFormat("MM");                  break; //Mes en 2 digitos Ej: 01
            case 4: FMT = new SimpleDateFormat("yyyy-MM-dd");          break; //A�o, Mes dia separados por '-' Ej: 2004-10-09
            case 5: FMT = new SimpleDateFormat("dd");                  break; //Dia en 2 digitos Ej: 02
            case 6: FMT = new SimpleDateFormat("yyyy/MM/dd kk:mm:ss"); break; //A�o, Mes dia separados por '/' y hora(24), minutos y segundos separado por ':' Ej: 2004/10/09 23:50:30
            case 7: FMT = new SimpleDateFormat("yyyy/MM/dd");          break; //A�o, Mes dia separados por '/' Ej: 2004/10/09
            case 8: FMT = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss"); break; //A�o, Mes dia separados por '-' y hora(24), minutos y segundos separado por ':' Ej: 2004-10-09 23:50:30
        }  
        String Fecha = FMT.format(new Date());
        return Fecha.toUpperCase();
    }  

    
    
    public static String getHoy(String separador){
       String hoy = getDate(1) + separador + getDate(3) + separador + getDate(5);
       return hoy;
    }
    
    public static String getAyer(String separador) throws ParseException{  
      String ayer = getHoy(separador);
      try{
       String hoy  = ayer;            
       ayer        = convertirFecha(hoy,-1,separador);
      }catch(Exception e){}
       return ayer;
    }
    
    
    public static String  convertirFecha(String fecha, int dias,String separador) throws ParseException{
       String formato = "yyyy" + separador + "MM" + separador + "dd";
       SimpleDateFormat FMT = new SimpleDateFormat(formato);       
       Calendar calendario  =  Calendar.getInstance();
       try{
           calendario.setTime(FMT.parse(fecha));
           calendario.add(Calendar.DATE,dias);
       }catch(Exception e){}
       return FMT.format(calendario.getTime());
    }   
    
    public static String  convertirFecha(String fecha, int dias) throws ParseException{
       SimpleDateFormat FMT = new SimpleDateFormat("yyyy-MM-dd");       
       Calendar calendario  =  Calendar.getInstance();
       try{
           calendario.setTime(FMT.parse(fecha));
           calendario.add(Calendar.DATE,dias);
       }catch(Exception e){}
       return FMT.format(calendario.getTime());
    }   
    
    
    
   public static String  convertirMilesgdos(long no) throws ParseException{
      SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
      Calendar cal = Calendar.getInstance();
      try{
           cal.setTimeInMillis(no);
      }catch(Exception e){}
     return formato.format(cal.getTime());
   }
    
    
   public static String NombreMes(int mes){
      String nombre="";
      switch(mes){
       case 1:   nombre="Enero";  break;
       case 2:   nombre="Febrero"; break;
       case 3:   nombre="Marzo";break;
       case 4:   nombre="Abril"; break;
       case 5:   nombre="Mayo";break;
       case 6:   nombre="Junio"; break;
       case 7:   nombre="Julio"; break;
       case 8:   nombre="Agosto"; break;
       case 9:   nombre="Septiembre";break;
       case 10:  nombre="Octubre"; break;
       case 11:  nombre="Noviembre"; break;
       case 12:  nombre="Diciembre"; break;
      }
      return nombre;    
   }


public static String mesFormat(int mes){
  String nombre = String.valueOf(mes);
  switch(mes){
   case 1:   nombre="01";  break;
   case 2:   nombre="02";  break;
   case 3:   nombre="03";  break;
   case 4:   nombre="04";  break;
   case 5:   nombre="05";  break;
   case 6:   nombre="06";  break;
   case 7:   nombre="07";  break;
   case 8:   nombre="08";  break;
   case 9:   nombre="09";  break;
   case 10:  nombre="10";  break;
   case 11:  nombre="11";  break;
   case 12:  nombre="12";  break;
  }
  return nombre;
    
}

public static String rellenar(String cadena, int tope){
   int lon = cadena.length();
   if(tope>lon)
     for(int i=lon;i<tope;i++)
        cadena="0"+cadena;   
   return cadena;
}
   

public static String Trunc(String Cadena, int Longitud){
       return (Cadena==null?Cadena: (Cadena.length()>=Longitud? Cadena.substring(0,Longitud):Cadena  )  );
}


    
    /**
     * Retorna un String con el codigo html de la imagen
     * @param   o opcion
     * @return  un String con el codigo html de la imagen
     * @author  Jpinedo
     *
     */
    public static String getIcono(String base_url,int op)
    {
     String img="";
     switch ( op )
        {
            case 1://info
               img="<img src='" + base_url +"/images/images.jpg' width='20' height='20'  title='Error' align='absbottom' />";
            break;

            case 2://error
            img="<img src='" + base_url +"/images/error.png' width='20' height='20'  title='Error' align='absbottom' />";
            break;

            case 3://alert
            img="<img src='" + base_url +"/images/info.png' width='20' height='20'  title='Error' align='absbottom' />";
            break;

            case 4://pdf
            img="<img src='" + base_url +"/images/logo-pdf.gif' width='20' height='20'  title='Error' align='absbottom' />";
            break;

            case 5://check
            img="<img src='" + base_url +"/images/ok2.gif' width='20' height='20'  title='Error' />";
            break;

            case 6://email
            img="<img src='" + base_url +"/images/Mail.png' width='25' height='25'  title='Error' />";
            break;

            case 7://excel
            img="<img src='" + base_url +"/images/excel.gif' width='20' height='20'  title='Exportar' />";
            break;

             case 8://pdf
            img="<img src='" + base_url +"/images/buscar.gif' width='20' height='20'  title='Buscar' />";
            break;
        }

        return img;
      }


}


