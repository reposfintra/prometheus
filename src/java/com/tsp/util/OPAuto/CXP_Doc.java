/********************************************************************
 *      Nombre Clase.................   CXP_Doc .java
 *      Descripci�n..................   Bean de la tabla cxp_doc
 *      Autor........................   David Lamadrid
 *      Fecha........................   20.10.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.util.OPAuto;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.io.Serializable;

public class CXP_Doc implements Serializable{
    
        private String reg_status;
        private String dstrct;
	private String proveedor;         
	private String tipo_documento;
	private String documento;
	private String descripcion;
        private String agencia;
	private String handle_code;
	private String id_mims;        
	private String fecha_documento;
	private String tipo_documento_rel;
	private String documento_relacionado;
	private String fecha_aprobacion;
	private String aprobador;
	private String usuario_aprobacion;
	private String fecha_vencimiento;
	private String ultima_fecha_pago;
	private String banco;
	private String sucursal;
	private String moneda;
	private double vlr_neto;
	private double vlr_total_abonos;
	private double vlr_saldo;
	private double vlr_neto_me;
	private double vlr_total_abonos_me;
	private double vlr_saldo_me;
	private float tasa;
	private String usuario_contabilizo;
	private String fecha_contabilizacion;
	private String usuario_anulo;
	private String fecha_anulacion;
	private String fecha_contabilizacion_anulacion;
	private String observacion;
        private double num_obs_autorizador;
        private double num_obs_pagador ;
        private double num_obs_registra ;
	private String last_update;
	private String user_update;
	private String creation_date;
	private String creation_user;
	private String base;
        private String numDias;
        boolean banderaRoja;
        boolean banderaVerde;        
        boolean responder;
        private String nomProveedor; 
        private String document_name;
        private boolean seleccionado;
        
        private int plazo;
        
        private String corrida;
        //sescalante (generacio op)
        private String tipocant;
        private String cantiCump;
        
        //sescalante (reporte facturas creadas)
        private String ucagencia;
        
        
        
        
        public static CXP_Doc loadCXP_Doc(ResultSet rs)throws SQLException {
            CXP_Doc cXP_Doc = new CXP_Doc();  
            cXP_Doc.setReg_status( rs.getString("reg_status") );
            cXP_Doc.setDstrct( rs.getString("dstrct") );
            cXP_Doc.setProveedor(rs.getString("proveedor") );
            cXP_Doc.setTipo_documento(rs.getString("tipo_documento") );
            cXP_Doc.setDocumento(rs.getString("documento") );
            cXP_Doc.setDescripcion(rs.getString("descripcion") );
            cXP_Doc.setAgencia(rs.getString("agencia"));
            cXP_Doc.setHandle_code(rs.getString("handle_code"));
            cXP_Doc.setId_mims(rs.getString("id_mims"));
            cXP_Doc.setFecha_documento(rs.getString("fecha_documento"));
            cXP_Doc.setTipo_documento_rel(rs.getString("tipo_documento_rel"));
            cXP_Doc.setDocumento_relacionado(rs.getString("documento_relacionado"));
            cXP_Doc.setFecha_aprobacion(rs.getString("fecha_aprobacion"));
            cXP_Doc.setAprobador(rs.getString("aprobador"));
            cXP_Doc.setUsuario_aprobacion(rs.getString("usuario_aprobacion"));
            cXP_Doc.setFecha_vencimiento(rs.getString("fecha_vencimiento"));
            cXP_Doc.setUltima_fecha_pago(rs.getString("ultima_fecha_pago"));
            cXP_Doc.setBanco(rs.getString("banco"));
            cXP_Doc.setSucursal( rs.getString("sucursal") );
            cXP_Doc.setMoneda( rs.getString("moneda") );
            cXP_Doc.setVlr_neto(rs.getDouble("vlr_neto"));
            cXP_Doc.setObservacion(rs.getString("observacion") );
            cXP_Doc.setCreation_user(rs.getString("creation_user") );
            cXP_Doc.setCreation_date(rs.getString("creation_date") );
            return cXP_Doc;
        }
        
    /** Creates a new instance of Documento */
        public CXP_Doc() {
        }
    
        /**
         * Getter for property reg_status.
         * @return Value of property reg_status.
         */
        public java.lang.String getReg_status() {
            return reg_status;
        }        
    
        /**
         * Setter for property reg_status.
         * @param reg_status New value of property reg_status.
         */
        public void setReg_status(java.lang.String reg_status) {
            this.reg_status = reg_status;
        }        
    
        /**
         * Getter for property dstrct.
         * @return Value of property dstrct.
         */
        public java.lang.String getDstrct() {
            return dstrct;
        }
        
        /**
         * Setter for property dstrct.
         * @param dstrct New value of property dstrct.
         */
        public void setDstrct(java.lang.String dstrct) {
            this.dstrct = dstrct;
        }
        
        /**
         * Getter for property proveedor.
         * @return Value of property proveedor.
         */
        public java.lang.String getProveedor() {
            return proveedor;
        }
        
        /**
         * Setter for property proveedor.
         * @param proveedor New value of property proveedor.
         */
        public void setProveedor(java.lang.String proveedor) {
            this.proveedor = proveedor;
        }
        
        /**
         * Getter for property tipo_documento.
         * @return Value of property tipo_documento.
         */
        public java.lang.String getTipo_documento() {
            return tipo_documento;
        }
        
        /**
         * Setter for property tipo_documento.
         * @param tipo_documento New value of property tipo_documento.
         */
        public void setTipo_documento(java.lang.String tipo_documento) {
            this.tipo_documento = tipo_documento;
        }
        
        /**
         * Getter for property documento.
         * @return Value of property documento.
         */
        public java.lang.String getDocumento() {
            return documento;
        }
        
        /**
         * Setter for property documento.
         * @param documento New value of property documento.
         */
        public void setDocumento(java.lang.String documento) {
            this.documento = documento;
        }
        
        /**
         * Getter for property descripcion.
         * @return Value of property descripcion.
         */
        public java.lang.String getDescripcion() {
            return descripcion;
        }
        
        /**
         * Setter for property descripcion.
         * @param descripcion New value of property descripcion.
         */
        public void setDescripcion(java.lang.String descripcion) {
            this.descripcion = descripcion;
        }
        
        /**
         * Getter for property agencia.
         * @return Value of property agencia.
         */
        public java.lang.String getAgencia() {
            return agencia;
        }
        
        /**
         * Setter for property agencia.
         * @param agencia New value of property agencia.
         */
        public void setAgencia(java.lang.String agencia) {
            this.agencia = agencia;
        }
        
        /**
         * Getter for property handle_code.
         * @return Value of property handle_code.
         */
        public java.lang.String getHandle_code() {
            return handle_code;
        }
        
        /**
         * Setter for property handle_code.
         * @param handle_code New value of property handle_code.
         */
        public void setHandle_code(java.lang.String handle_code) {
            this.handle_code = handle_code;
        }
        
        /**
         * Getter for property id_mims.
         * @return Value of property id_mims.
         */
        public java.lang.String getId_mims() {
            return id_mims;
        }
        
        /**
         * Setter for property id_mims.
         * @param id_mims New value of property id_mims.
         */
        public void setId_mims(java.lang.String id_mims) {
            this.id_mims = id_mims;
        }
        
        /**
         * Getter for property fecha_documento.
         * @return Value of property fecha_documento.
         */
        public java.lang.String getFecha_documento() {
            return fecha_documento;
        }
        
        /**
         * Setter for property fecha_documento.
         * @param fecha_documento New value of property fecha_documento.
         */
        public void setFecha_documento(java.lang.String fecha_documento) {
            this.fecha_documento = fecha_documento;
        }
        
        /**
         * Getter for property tipo_documento_rel.
         * @return Value of property tipo_documento_rel.
         */
        public java.lang.String getTipo_documento_rel() {
            return tipo_documento_rel;
        }
        
        /**
         * Setter for property tipo_documento_rel.
         * @param tipo_documento_rel New value of property tipo_documento_rel.
         */
        public void setTipo_documento_rel(java.lang.String tipo_documento_rel) {
            this.tipo_documento_rel = tipo_documento_rel;
        }
        
        /**
         * Getter for property documento_relacionado.
         * @return Value of property documento_relacionado.
         */
        public java.lang.String getDocumento_relacionado() {
            return documento_relacionado;
        }
        
        /**
         * Setter for property documento_relacionado.
         * @param documento_relacionado New value of property documento_relacionado.
         */
        public void setDocumento_relacionado(java.lang.String documento_relacionado) {
            this.documento_relacionado = documento_relacionado;
        }
        
        /**
         * Getter for property fecha_aprobacion.
         * @return Value of property fecha_aprobacion.
         */
        public java.lang.String getFecha_aprobacion() {
            return fecha_aprobacion;
        }
        
        /**
         * Setter for property fecha_aprobacion.
         * @param fecha_aprobacion New value of property fecha_aprobacion.
         */
        public void setFecha_aprobacion(java.lang.String fecha_aprobacion) {
            this.fecha_aprobacion = fecha_aprobacion;
        }
        
        /**
         * Getter for property aprobador.
         * @return Value of property aprobador.
         */
        public java.lang.String getAprobador() {
            return aprobador;
        }
        
        /**
         * Setter for property aprobador.
         * @param aprobador New value of property aprobador.
         */
        public void setAprobador(java.lang.String aprobador) {
            this.aprobador = aprobador;
        }
        
        /**
         * Getter for property usuario_aprobacion.
         * @return Value of property usuario_aprobacion.
         */
        public java.lang.String getUsuario_aprobacion() {
            return usuario_aprobacion;
        }
        
        /**
         * Setter for property usuario_aprobacion.
         * @param usuario_aprobacion New value of property usuario_aprobacion.
         */
        public void setUsuario_aprobacion(java.lang.String usuario_aprobacion) {
            this.usuario_aprobacion = usuario_aprobacion;
        }
        
        /**
         * Getter for property fecha_vencimiento.
         * @return Value of property fecha_vencimiento.
         */
        public java.lang.String getFecha_vencimiento() {
            return fecha_vencimiento;
        }
        
        /**
         * Setter for property fecha_vencimiento.
         * @param fecha_vencimiento New value of property fecha_vencimiento.
         */
        public void setFecha_vencimiento(java.lang.String fecha_vencimiento) {
            this.fecha_vencimiento = fecha_vencimiento;
        }
        
        /**
         * Getter for property ultima_fecha_pago.
         * @return Value of property ultima_fecha_pago.
         */
        public java.lang.String getUltima_fecha_pago() {
            return ultima_fecha_pago;
        }
        
        /**
         * Setter for property ultima_fecha_pago.
         * @param ultima_fecha_pago New value of property ultima_fecha_pago.
         */
        public void setUltima_fecha_pago(java.lang.String ultima_fecha_pago) {
            this.ultima_fecha_pago = ultima_fecha_pago;
        }
        
        /**
         * Getter for property banco.
         * @return Value of property banco.
         */
        public java.lang.String getBanco() {
            return banco;
        }
        
        /**
         * Setter for property banco.
         * @param banco New value of property banco.
         */
        public void setBanco(java.lang.String banco) {
            this.banco = banco;
        }
        
        /**
         * Getter for property sucursal.
         * @return Value of property sucursal.
         */
        public java.lang.String getSucursal() {
            return sucursal;
        }
        
        /**
         * Setter for property sucursal.
         * @param sucursal New value of property sucursal.
         */
        public void setSucursal(java.lang.String sucursal) {
            this.sucursal = sucursal;
        }
        
        /**
         * Getter for property moneda.
         * @return Value of property moneda.
         */
        public java.lang.String getMoneda() {
            return moneda;
        }
        
        /**
         * Setter for property moneda.
         * @param moneda New value of property moneda.
         */
        public void setMoneda(java.lang.String moneda) {
            this.moneda = moneda;
        }
        
        /**
         * Getter for property vlr_neto.
         * @return Value of property vlr_neto.
         */
        public double getVlr_neto() {
            return vlr_neto;
        }
        
        /**
         * Setter for property vlr_neto.
         * @param vlr_neto New value of property vlr_neto.
         */
        public void setVlr_neto(double vlr_neto) {
            this.vlr_neto = vlr_neto;
        }
        
        /**
         * Getter for property vlr_total_abonos.
         * @return Value of property vlr_total_abonos.
         */
        public double getVlr_total_abonos() {
            return vlr_total_abonos;
        }
        
        /**
         * Setter for property vlr_total_abonos.
         * @param vlr_total_abonos New value of property vlr_total_abonos.
         */
        public void setVlr_total_abonos(double vlr_total_abonos) {
            this.vlr_total_abonos = vlr_total_abonos;
        }
        
        /**
         * Getter for property vlr_saldo.
         * @return Value of property vlr_saldo.
         */
        public double getVlr_saldo() {
            return vlr_saldo;
        }
        
        /**
         * Setter for property vlr_saldo.
         * @param vlr_saldo New value of property vlr_saldo.
         */
        public void setVlr_saldo(double vlr_saldo) {
            this.vlr_saldo = vlr_saldo;
        }
        
        /**
         * Getter for property vlr_neto_me.
         * @return Value of property vlr_neto_me.
         */
        public double getVlr_neto_me() {
            return vlr_neto_me;
        }
        
        /**
         * Setter for property vlr_neto_me.
         * @param vlr_neto_me New value of property vlr_neto_me.
         */
        public void setVlr_neto_me(double vlr_neto_me) {
            this.vlr_neto_me = vlr_neto_me;
        }
        
        /**
         * Getter for property vlr_total_abonos_me.
         * @return Value of property vlr_total_abonos_me.
         */
        public double getVlr_total_abonos_me() {
            return vlr_total_abonos_me;
        }
        
        /**
         * Setter for property vlr_total_abonos_me.
         * @param vlr_total_abonos_me New value of property vlr_total_abonos_me.
         */
        public void setVlr_total_abonos_me(double vlr_total_abonos_me) {
            this.vlr_total_abonos_me = vlr_total_abonos_me;
        }
        
        /**
         * Getter for property vlr_saldo_me.
         * @return Value of property vlr_saldo_me.
         */
        public double getVlr_saldo_me() {
            return vlr_saldo_me;
        }
        
        /**
         * Setter for property vlr_saldo_me.
         * @param vlr_saldo_me New value of property vlr_saldo_me.
         */
        public void setVlr_saldo_me(double vlr_saldo_me) {
            this.vlr_saldo_me = vlr_saldo_me;
        }
        
        /**
         * Getter for property tasa.
         * @return Value of property tasa.
         */
        public float getTasa() {
            return tasa;
        }
        
        /**
         * Setter for property tasa.
         * @param tasa New value of property tasa.
         */
        public void setTasa(float tasa) {
            this.tasa = tasa;
        }
        
        /**
         * Getter for property usuario_contabilizo.
         * @return Value of property usuario_contabilizo.
         */
        public java.lang.String getUsuario_contabilizo() {
            return usuario_contabilizo;
        }
        
        /**
         * Setter for property usuario_contabilizo.
         * @param usuario_contabilizo New value of property usuario_contabilizo.
         */
        public void setUsuario_contabilizo(java.lang.String usuario_contabilizo) {
            this.usuario_contabilizo = usuario_contabilizo;
        }
        
        /**
         * Getter for property fecha_contabilizacion.
         * @return Value of property fecha_contabilizacion.
         */
        public java.lang.String getFecha_contabilizacion() {
            return fecha_contabilizacion;
        }
        
        /**
         * Setter for property fecha_contabilizacion.
         * @param fecha_contabilizacion New value of property fecha_contabilizacion.
         */
        public void setFecha_contabilizacion(java.lang.String fecha_contabilizacion) {
            this.fecha_contabilizacion = fecha_contabilizacion;
        }
        
        /**
         * Getter for property usuario_anulo.
         * @return Value of property usuario_anulo.
         */
        public java.lang.String getUsuario_anulo() {
            return usuario_anulo;
        }
        
        /**
         * Setter for property usuario_anulo.
         * @param usuario_anulo New value of property usuario_anulo.
         */
        public void setUsuario_anulo(java.lang.String usuario_anulo) {
            this.usuario_anulo = usuario_anulo;
        }
        
        /**
         * Getter for property fecha_anulacion.
         * @return Value of property fecha_anulacion.
         */
        public java.lang.String getFecha_anulacion() {
            return fecha_anulacion;
        }
        
        /**
         * Setter for property fecha_anulacion.
         * @param fecha_anulacion New value of property fecha_anulacion.
         */
        public void setFecha_anulacion(java.lang.String fecha_anulacion) {
            this.fecha_anulacion = fecha_anulacion;
        }
        
        /**
         * Getter for property fecha_contabilizacion_anulacion.
         * @return Value of property fecha_contabilizacion_anulacion.
         */
        public java.lang.String getFecha_contabilizacion_anulacion() {
            return fecha_contabilizacion_anulacion;
        }
        
        /**
         * Setter for property fecha_contabilizacion_anulacion.
         * @param fecha_contabilizacion_anulacion New value of property fecha_contabilizacion_anulacion.
         */
        public void setFecha_contabilizacion_anulacion(java.lang.String fecha_contabilizacion_anulacion) {
            this.fecha_contabilizacion_anulacion = fecha_contabilizacion_anulacion;
        }
        
        /**
         * Getter for property observacion.
         * @return Value of property observacion.
         */
        public java.lang.String getObservacion() {
            return observacion;
        }
        
        /**
         * Setter for property observacion.
         * @param observacion New value of property observacion.
         */
        public void setObservacion(java.lang.String observacion) {
            this.observacion = observacion;
        }
        
        /**
         * Getter for property num_obs_autorizador.
         * @return Value of property num_obs_autorizador.
         */
        public double getNum_obs_autorizador() {
            return num_obs_autorizador;
        }
        
        /**
         * Setter for property num_obs_autorizador.
         * @param num_obs_autorizador New value of property num_obs_autorizador.
         */
        public void setNum_obs_autorizador(double num_obs_autorizador) {
            this.num_obs_autorizador = num_obs_autorizador;
        }
        
        /**
         * Getter for property num_obs_pagador.
         * @return Value of property num_obs_pagador.
         */
        public double getNum_obs_pagador() {
            return num_obs_pagador;
        }
        
        /**
         * Setter for property num_obs_pagador.
         * @param num_obs_pagador New value of property num_obs_pagador.
         */
        public void setNum_obs_pagador(double num_obs_pagador) {
            this.num_obs_pagador = num_obs_pagador;
        }
        
        /**
         * Getter for property num_obs_registra.
         * @return Value of property num_obs_registra.
         */
        public double getNum_obs_registra() {
            return num_obs_registra;
        }
        
        /**
         * Setter for property num_obs_registra.
         * @param num_obs_registra New value of property num_obs_registra.
         */
        public void setNum_obs_registra(double num_obs_registra) {
            this.num_obs_registra = num_obs_registra;
        }
        
        /**
         * Getter for property last_update.
         * @return Value of property last_update.
         */
        public java.lang.String getLast_update() {
            return last_update;
        }
        
        /**
         * Setter for property last_update.
         * @param last_update New value of property last_update.
         */
        public void setLast_update(java.lang.String last_update) {
            this.last_update = last_update;
        }
        
        /**
         * Getter for property user_update.
         * @return Value of property user_update.
         */
        public java.lang.String getUser_update() {
            return user_update;
        }
        
        /**
         * Setter for property user_update.
         * @param user_update New value of property user_update.
         */
        public void setUser_update(java.lang.String user_update) {
            this.user_update = user_update;
        }
        
        /**
         * Getter for property creation_date.
         * @return Value of property creation_date.
         */
        public java.lang.String getCreation_date() {
            return creation_date;
        }
        
        /**
         * Setter for property creation_date.
         * @param creation_date New value of property creation_date.
         */
        public void setCreation_date(java.lang.String creation_date) {
            this.creation_date = creation_date;
        }
        
        /**
         * Getter for property creation_user.
         * @return Value of property creation_user.
         */
        public java.lang.String getCreation_user() {
            return creation_user;
        }
        
        /**
         * Setter for property creation_user.
         * @param creation_user New value of property creation_user.
         */
        public void setCreation_user(java.lang.String creation_user) {
            this.creation_user = creation_user;
        }
        
        /**
         * Getter for property base.
         * @return Value of property base.
         */
        public java.lang.String getBase() {
            return base;
        }
        
        /**
         * Setter for property base.
         * @param base New value of property base.
         */
        public void setBase(java.lang.String base) {
            this.base = base;
        }
        
        /**
         * Getter for property numDias.
         * @return Value of property numDias.
         */
        public java.lang.String getNumDias() {
            return numDias;
        }
        
        /**
         * Setter for property numDias.
         * @param numDias New value of property numDias.
         */
        public void setNumDias(java.lang.String numDias) {
            this.numDias = numDias;
        }
        
        /**
         * Getter for property banderaRoja.
         * @return Value of property banderaRoja.
         */
        public boolean isBanderaRoja() {
            return banderaRoja;
        }
        
        /**
         * Setter for property banderaRoja.
         * @param banderaRoja New value of property banderaRoja.
         */
        public void setBanderaRoja(boolean banderaRoja) {
            this.banderaRoja = banderaRoja;
        }
        
        /**
         * Getter for property banderaVerde.
         * @return Value of property banderaVerde.
         */
        public boolean isBanderaVerde() {
            return banderaVerde;
        }
        
        /**
         * Setter for property banderaVerde.
         * @param banderaVerde New value of property banderaVerde.
         */
        public void setBanderaVerde(boolean banderaVerde) {
            this.banderaVerde = banderaVerde;
        }
        
        /**
         * Getter for property responder.
         * @return Value of property responder.
         */
        public boolean isResponder() {
            return responder;
        }
        
        /**
         * Setter for property responder.
         * @param responder New value of property responder.
         */
        public void setResponder(boolean responder) {
            this.responder = responder;
        }
        
        /**
         * Getter for property nomProveedor.
         * @return Value of property nomProveedor.
         */
        public java.lang.String getNomProveedor() {
            return nomProveedor;
        }
        
        /**
         * Setter for property nomProveedor.
         * @param nomProveedor New value of property nomProveedor.
         */
        public void setNomProveedor(java.lang.String nomProveedor) {
            this.nomProveedor = nomProveedor;
        }
        
        /**
         * Getter for property document_name.
         * @return Value of property document_name.
         */
        public java.lang.String getDocument_name() {
            return document_name;
        }
        
        /**
         * Setter for property document_name.
         * @param document_name New value of property document_name.
         */
        public void setDocument_name(java.lang.String document_name) {
            this.document_name = document_name;
        }
        
        /**
         * Getter for property seleccionado.
         * @return Value of property seleccionado.
         */
        public boolean isSeleccionado() {
            return seleccionado;
        }
        
        /**
         * Setter for property seleccionado.
         * @param seleccionado New value of property seleccionado.
         */
        public void setSeleccionado(boolean seleccionado) {
            this.seleccionado = seleccionado;
        }
        
        /**
         * Getter for property plazo.
         * @return Value of property plazo.
         */
        public int getPlazo() {
            return plazo;
        }
        
        /**
         * Setter for property plazo.
         * @param plazo New value of property plazo.
         */
        public void setPlazo(int plazo) {
            this.plazo = plazo;
        }
        
        /**
         * Getter for property corrida.
         * @return Value of property corrida.
         */
        public java.lang.String getCorrida() {
            return corrida;
        }
        
        /**
         * Setter for property corrida.
         * @param corrida New value of property corrida.
         */
        public void setCorrida(java.lang.String corrida) {
            this.corrida = corrida;
        }
        
        /**
         * Getter for property tipocant.
         * @return Value of property tipocant.
         */
        public java.lang.String getTipocant() {
            return tipocant;
        }
        
        /**
         * Setter for property tipocant.
         * @param tipocant New value of property tipocant.
         */
        public void setTipocant(java.lang.String tipocant) {
            this.tipocant = tipocant;
        }
        
        /**
         * Getter for property cantiCump.
         * @return Value of property cantiCump.
         */
        public java.lang.String getCantiCump() {
            return cantiCump;
        }
        
        /**
         * Setter for property cantiCump.
         * @param cantiCump New value of property cantiCump.
         */
        public void setCantiCump(java.lang.String cantiCump) {
            this.cantiCump = cantiCump;
        }
        
        /**
         * Getter for property ucagencia.
         * @return Value of property ucagencia.
         */
        public java.lang.String getUcagencia() {
            return ucagencia;
        }
        
        /**
         * Setter for property ucagencia.
         * @param ucagencia New value of property ucagencia.
         */
        public void setUcagencia(java.lang.String ucagencia) {
            this.ucagencia = ucagencia;
        }
        
}

