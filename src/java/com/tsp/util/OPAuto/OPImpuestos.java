/***************************************
 * Nombre Clase ............. OPImpuestos.java
 * Descripci�n  .. . . . . .  Permite Encapsular los Impuestos
 * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
 * Fecha . . . . . . . . . .  21/10/2005
 * versi�n . . . . . . . . .  1.0
 * Copyright ...Transportes Sanchez Polo S.A.
 *******************************************/


package com.tsp.util.OPAuto;


public class OPImpuestos {
    
    private String  codigo;
    private String  tipo;
    private double  porcentaje;
    private double  porcentaje2;
    private String  agencia;
    private String  cuenta;
    private double  valor;
    private double  valorMe;
    
    public OPImpuestos() {
        codigo   =  "";
        tipo     =  "";
        agencia  =  "";
        cuenta   =  "";
    }
    
    /**
     * Setter for property codigo
     * @param tipcliarea New value of property codigo
     */
    public void setCodigo( String codigo) {
        this.codigo      = codigo;
    }
    /**
     * Setter for property porcentaje
     * @param tipcliarea New value of property porcentaje
     */
    public void setPorcentaje( double porcentaje) {
        this.porcentaje  = porcentaje;
    }
    /**
     * Setter for property  porcentaje2
     * @param tipcliarea New value of property porcentaje2
     */
    public void setPorcentaje2( double porcentaje) {
        this.porcentaje2 = porcentaje;
    }
    /**
     * Setter for property agencia
     * @param tipcliarea New value of property agencia
     */
    public void setAgencia( String val) {
        this.agencia    = val;
    }
    /**
     * Setter for property cuenta
     * @param tipcliarea New value of property cuenta
     */
    public void setCuenta( String val){
        this.cuenta      = val;
    }
    /**
     * Setter for property tipo
     * @param tipcliarea New value of property tipo
     */
    public void setTipo  ( String tipo) {
        this.tipo        = tipo;
    }
    /**
     * Setter for property valor
     * @param tipcliarea New value of property valor
     */
    public void setValor( double valor){
        this.valor       = valor ;
    }
    /**
     * Setter for property valorMe
     * @param tipcliarea New value of property valorMe
     */
    public void setValorMe( double valorMe){
        this.valorMe     = valorMe   ;
    }
    
    
    
    /**
     * Getter for property codigo
     * @return Value of property codigo
     */
    public String getCodigo() {
        return codigo;
    }
    /**
     * Getter for property porcentaje
     * @return Value of property porcentaje
     */
    public double getPorcentaje() {
        return porcentaje ;
    }
    /**
     * Getter for property tipo
     * @return Value of property tipo
     */
    public String getTipo() {
        return tipo ;
    }
    /**
     * Getter for property valor
     * @return Value of property valor
     */
    public double getValor() {
        return valor ;
    }
    /**
     * Getter for property valorMe
     * @return Value of property valorMe
     */
    public double getValorMe() {
        return valorMe ;
    }
    /**
     * Getter for property porcentaje2
     * @return Value of property porcentaje2
     */
    public double getPorcentaje2() {
        return this.porcentaje2;
    }
    /**
     * Getter for property agencia
     * @return Value of property agencia
     */
    public String getAgencia() {
        return this.agencia;
    }
    /**
     * Getter for property cuenta
     * @return Value of property cuenta
     */
    public String getCuenta() {
        return this.cuenta ;
    }
}
