/***************************************
    * Nombre Clase ............. OPItems.java
    * Descripci�n  .. . . . . .  Permite Gencapsular los datos necesarios de los Items de  OP.
    * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
    * Fecha . . . . . . . . . .  20/10/2005
    * versi�n . . . . . . . . .  1.0
    * Copyright ...Transportes Sanchez Polo S.A.
    *******************************************/


package com.tsp.util.OPAuto;

import java.util.*;

public class OPItems extends CXPItemDoc{
    
    private String moneda;
    private String banco;
    private String sucursal;
    private String monedaBanco;
    private String indicador;
    private String asignador;    
    private String reteFuente;
    private String reteIva;
    private String reteIca;    
    private String agencia;    
    private List   impuestos;
    private double vlrReteFuente;
    private double vlrReteIva;
    private double vlrReteIca;
    
    //ivan Dario Gomez 2006-03-24  
    private boolean visible;
    private String Cheque;
    
    
    public OPItems() {
        impuestos = null;
        visible   = false;
    }
    
    
    
    
 // 1. SET :
    
    /* M�todos que setea el valor de retefuente
     * @autor.......fvillacob 
     * @version.....1.0.     
     **/ 
    public void setVlrReteFuente(double val){
        this.vlrReteFuente = val;
    }
    
    /* M�todos que setea el valor de reteIva
     * @autor.......fvillacob 
     * @version.....1.0.     
     **/ 
    public void setVlrReteIva(double val){
        this.vlrReteIva = val;
    }
    
    /* M�todos que setea el valor de reteIca
     * @autor.......fvillacob 
     * @version.....1.0.     
     **/ 
    public void setVlrReteIca(double val){
        this.vlrReteIca = val;
    }
    
    
    /* M�todos que setea el valor de Agencia
     * @autor.......fvillacob 
     * @version.....1.0.     
     **/   
    public void setAgencia (String val) {  
        this.agencia     = val;  
    }
    
    /* M�todos que setea el valor de Moneda
     * @autor.......fvillacob 
     * @version.....1.0.     
     **/ 
    public void setMoneda(String val) {  
        this.moneda      = val;  
    }    
    
    /* M�todos que setea el valor de banco
     * @autor.......fvillacob 
     * @version.....1.0.     
     **/ 
    public void setBanco (String val) {  
        this.banco       = val;  
    }
    
    /* M�todos que setea el valor de sucursal
     * @autor.......fvillacob 
     * @version.....1.0.     
     **/ 
    public void setSucursal(String val) {  
        this.sucursal    = val;  
    }
    
    /* M�todos que setea el valor de monedaBanco
     * @autor.......fvillacob 
     * @version.....1.0.     
     **/ 
    public void setMonedaBanco(String val) {  
        this.monedaBanco = val;  
    }   
    
    /* M�todos que setea el valor de indicador
     * @autor.......fvillacob 
     * @version.....1.0.     
     **/ 
    public void setIndicador(String val) {  
        this.indicador   = val;  
    }
    
    /* M�todos que setea el valor de asignador
     * @autor.......fvillacob 
     * @version.....1.0.     
     **/ 
    public void setAsignador(String val) {  
        this.asignador   = val;  
    }
    
    /* M�todos que setea el valor de impuesto
     * @autor.......fvillacob 
     * @version.....1.0.     
     **/ 
    public void setImpuestos(List list)  { 
        this.impuestos = list; 
    }
    
    /* M�todos que setea el valor de retefuente
     * @autor.......fvillacob 
     * @version.....1.0.     
     **/ 
    public void setReteFuente( String val){ 
        this.reteFuente    = val;  
    }  
    
    /* M�todos que setea el valor de retefuente
     * @autor.......fvillacob 
     * @version.....1.0.     
     **/ 
    public void setReteIva ( String val){ 
        this.reteIva       = val;  
    }   
    
    /* M�todos que setea el valor de retefuente
     * @autor.......fvillacob 
     * @version.....1.0.     
     **/ 
    public void setReteIca ( String val){ 
        this.reteIca       = val;  
    } 
    
    
    
    
    
    
    
 // 2. GET:
    
    
     /* M�todos que devuelve el valor de reteFuente
     * @autor.......fvillacob 
     * @version.....1.0.     
     **/ 
    public double getVlrReteFuente(){
        return this.vlrReteFuente ;
    }
    
    /* M�todos que devuelve el valor de reteIva
     * @autor.......fvillacob 
     * @version.....1.0.     
     **/ 
    public double getVlrReteIva(){
        return this.vlrReteIva ;
    }
    
    /* M�todos que devuelve el valor de reteIca
     * @autor.......fvillacob 
     * @version.....1.0.     
     **/ 
    public double getVlrReteIca(){
        return this.vlrReteIca ;
    }
    
    /* M�todos que devuelve el valor de agencia
     * @autor.......fvillacob 
     * @version.....1.0.     
     **/ 
    public String getAgencia() {  
        return this.agencia ;  
    }
    
    /* M�todos que devuelve el valor de moneda
     * @autor.......fvillacob 
     * @version.....1.0.     
     **/ 
    public String getMoneda() {  
        return this.moneda ;  
    }
    
    /* M�todos que devuelve el valor de banco
     * @autor.......fvillacob 
     * @version.....1.0.     
     **/ 
    public String getBanco() {  
        return this.banco ;  
    }
    
    /* M�todos que devuelve el valor de sucursal
     * @autor.......fvillacob 
     * @version.....1.0.     
     **/ 
    public String getSucursal() {  
        return this.sucursal;  
    }
    
    /* M�todos que devuelve el valor de monedabanco
     * @autor.......fvillacob 
     * @version.....1.0.     
     **/ 
    public String getMonedaBanco() {  
        return this.monedaBanco ;  
    }       
    
    /* M�todos que devuelve el valor de indicador
     * @autor.......fvillacob 
     * @version.....1.0.     
     **/ 
    public String getIndicador() { 
        return this.indicador;  
    }
    
    /* M�todos que devuelve el valor de asignador
     * @autor.......fvillacob 
     * @version.....1.0.     
     **/ 
    public String getAsignador() {  
        return this.asignador;  
    }
    
    /* M�todos que devuelve el valor de impuesto
     * @autor.......fvillacob 
     * @version.....1.0.     
     **/ 
    public List   getImpuestos() {  
        return this.impuestos;  
    } 
    
    /* M�todos que devuelve el valor de retefuente
     * @autor.......fvillacob 
     * @version.....1.0.     
     **/ 
    public String getReteFuente( ){ 
        return this.reteFuente ; 
    }
    
    /* M�todos que devuelve el valor de reteIva
     * @autor.......fvillacob 
     * @version.....1.0.     
     **/ 
    public String getReteIva( ){ 
        return this.reteIva ;  
    } 
    
    /* M�todos que devuelve el valor de reteIca
     * @autor.......fvillacob 
     * @version.....1.0.     
     **/ 
    public String getReteIca( ){ 
        return this.reteIca ;  
    } 
    
    /**
     * Getter for property visible.
     * @return Value of property visible.
     */
    public boolean isVisible() {
        return visible;
    }
    
    /**
     * Setter for property visible.
     * @param visible New value of property visible.
     */
    public void setVisible(boolean visible) {
        this.visible = visible;
    }
    
    /**
     * Getter for property Cheque.
     * @return Value of property Cheque.
     */
    public java.lang.String getCheque() {
        return Cheque;
    }
    
    /**
     * Setter for property Cheque.
     * @param Cheque New value of property Cheque.
     */
    public void setCheque(java.lang.String Cheque) {
        this.Cheque = Cheque;
    }
    
}
