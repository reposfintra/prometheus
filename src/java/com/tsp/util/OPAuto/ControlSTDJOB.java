/*
 * Nombre        ControlSTDJOB.java
 * Autor         LREALES
 * Fecha         5 de septiembre de 2006, 08:43 AM
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */

package com.tsp.util.OPAuto;

import java.io.*;
import java.sql.*;
import java.util.*;
import jxl.*;

import com.tsp.util.*;
import com.tsp.finanzas.presupuesto.model.beans.*;// JXLRead
import com.tsp.util.connectionpool.PoolManager;// Conexion BD
import com.tsp.operation.model.beans.POIWrite;// Generar Excel
import com.tsp.operation.model.beans.StdJobMims;// StdJobMims

import org.apache.poi.hssf.usermodel.*;// Generar Excel
import org.apache.poi.hssf.util.*;// Generar Excel

public class ControlSTDJOB extends Thread {
    
    private StdJobMims bean;
    
    private Vector vector_stdjob;
    
    private String Archivo;
    private String Hoja;
    
    /**************************************************************************/
    private String usuario = "LREALES";
    private String fec_ini = "20060801"; // POR MES
    private String fec_fin = "20060831"; // POR MES
    /**************************************************************************/
    
    private static final String SQL_STDJOB =
        "SELECT " +
            "std_job_no, wo_type, work_group, unit_of_work, origin_code, " +
            "destination_code, vlr_freight, currency, account_code_i, stdjobgen, " +
            "unidad, account_code_c, " +
            "tipo_recurso1, recurso1, " +
            "tipo_recurso2, recurso2, " +
            "tipo_recurso3, recurso3, " +
            "tipo_recurso4, recurso4, " +
            "tipo_recurso5, recurso5 " +
        "FROM " +
            "stdjob " +
        "WHERE " +
            "std_job_no = ? " ;

    private static final String SQL_MSF690_MSF071_MSF010 =
        "SELECT " +
            "A.* , " +
            "B.REF_CODE, B.REF_NO, " +
            "SUBSTR ( C.ASSOC_REC, 1, 11 ) VLR_FREIGHT, SUBSTR ( C.ASSOC_REC, 16, 18 ) MONEDA " +
        "FROM " +
            "( " +
                "SELECT " +
                    "STD_JOB_NO, " +
                    "WO_TYPE, " +
                    "WORK_GROUP, " +
                    "ACCOUNT_CODE, " +
                    "REALL_ACCT_CDE, " +
                    "UNIT_OF_WORK " +	
                "FROM " +
                    "MSF690 " +
                "WHERE " +
                    "( CREATION_DATE >= ? AND CREATION_DATE <= ? ) " +
                    "OR ( LAST_MOD_DATE >= ? AND LAST_MOD_DATE <= ? ) " +
            ") A, MSF071 B, MSF010 C " +
        "WHERE " +
            "B.ENTITY_TYPE = '+SJ' " +
            "AND SUBSTR ( B.ENTITY_VALUE, 6, 6 ) = A.STD_JOB_NO " +
            "AND B.REF_NO IN ( '002', '003' ) " +
            "AND C.TABLE_TYPE = 'UW' " +
            "AND C.TABLE_CODE = A.UNIT_OF_WORK " ;
        
    private static final String SQL_MSF071_010 =
        "SELECT " +
            "REF_CODE " +
        "FROM " +
            "MSF071 " +
        "WHERE " +
            "ENTITY_TYPE = '+SJ' " +
            "AND SUBSTR ( ENTITY_VALUE, 6, 6 ) = ? " +
            "AND REF_NO = '010' " ;
        
    private static final String SQL_MSF735_MSF010 =
        "SELECT " +
            "CREW_SIZE, RESOURCE_TYPE, MSF010.TABLE_DESC " +
        "FROM " +
            "MSF735, MSF010 " +
        "WHERE " +
            "REC_735_TYPE = 'ST' " +
            "AND KEY_735_ID = 'FINV' || ? || '001' " +
            "AND MSF010.TABLE_TYPE = 'TT' " +
            "AND MSF010.TABLE_CODE = MSF735.RESOURCE_TYPE " ;
        
    /**
     * Crea una nueva instancia de  ControlSTDJOB
     */
    public ControlSTDJOB () { }
    
    /**
     * Getter for property vector_stdjob.
     * @return Value of property vector_stdjob.
     */
    public java.util.Vector getVector_stdjob() {
        return vector_stdjob;
    }
    
    /**
     * Setter for property vector_stdjob.
     * @param vector_stdjob New value of property vector_stdjob.
     */
    public void setVector_stdjob(java.util.Vector vector_stdjob) {
        this.vector_stdjob = vector_stdjob;
    }
        
    public static void main ( String[]abc ) throws Exception {
        
        try {
            
            ControlSTDJOB csj = new ControlSTDJOB ();
            csj.run();
            
            System.gc();
                
        } catch ( Exception ex ){
            
            ex.printStackTrace ();
            ////System.out.println( "Error en main( String[]abc ) --> " + ex.getMessage () );
            
        }
        
    }
        
    public synchronized void run () {
        
        com.tsp.operation.model.Model modelOperation = new com.tsp.operation.model.Model();
          
        try {
              
            System.gc();
            
            modelOperation.LogProcesosSvc.InsertProceso( "Control STDJOB", this.hashCode(), "Proceso de Reporte de Control de StdJob", usuario );
            
            System.gc();
            ////System.out.println("INICIA PROCESO.. " + Util.fechaActualTIMESTAMP() );
            stdjob_mims ( fec_ini, fec_fin );
            
            System.gc();
            
            exportar();
            ////System.out.println("FINALIZA PROCESO.. " + Util.fechaActualTIMESTAMP() );
            System.gc();
            
            modelOperation.LogProcesosSvc.finallyProceso( "Control STDJOB", this.hashCode(), usuario, "PROCESO EXITOSO" );
            
            System.gc();
            
        } catch ( Exception ex ) {
            
            try {
                
                ////System.out.println( "Error en run() ex --> " + ex.getMessage () );
                ex.printStackTrace();
                modelOperation.LogProcesosSvc.finallyProceso( "Control STDJOB", this.hashCode(), usuario, "ERROR : " + ex.getMessage() );
            
            } catch ( Exception e ){
                
                ////System.out.println( "Error en run() e --> " + e.getMessage () );
                e.printStackTrace();
                
            }            
            
        }
        
    }
    
    /**
     * Obtiene una lista de las remesas con estado 'AN', 'NF' y 'CD'.
     * @autor LREALES
     * @param la fecha inicial y la fecha final.
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void stdjob_mims ( String fec_ini, String fec_fin ) throws SQLException {
        
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        int cont = 0;
        
        try {
            
            poolManager = PoolManager.getInstance ();
            con = poolManager.getConnection ( "oracle" );
            
            if( con != null ){
                
                st = con.prepareStatement ( this.SQL_MSF690_MSF071_MSF010 );
                
                st.setString ( 1, fec_ini );         
                st.setString ( 2, fec_fin ); 
                st.setString ( 3, fec_ini );         
                st.setString ( 4, fec_fin );
                
                rs = st.executeQuery ();
                
                this.vector_stdjob = new Vector ();
                
                this.bean = new StdJobMims ();

                while ( rs.next () ){
                    
                    cont++;
                    
                    String std_job_no_mims = rs.getString(1) != null ? rs.getString(1).toUpperCase() : "";
                    String wo_type_mims = rs.getString(2) != null ? rs.getString(2).toUpperCase() : "";
                    String work_group_mims = rs.getString(3) != null ? rs.getString(3).toUpperCase() : "";
                    String account_code_c_mims = rs.getString(4) != null ? rs.getString(4).toUpperCase() : "";
                    String account_code_i_mims = rs.getString(5) != null ? rs.getString(5).toUpperCase() : "";
                    String unit_of_work_mims = rs.getString(6) != null ? rs.getString(6).toUpperCase() : "";
                    String REF_CODE = rs.getString(7) != null ? rs.getString(7).toUpperCase() : "";
                    String REF_NO = rs.getString(8) != null ? rs.getString(8).toUpperCase() : "";
                    String vlr_freight_mims = rs.getString(9) != null ? rs.getString(9).toUpperCase() : "";
                    double vlr_mims = Double.parseDouble( vlr_freight_mims ) / 10000;
                    vlr_freight_mims = "" + vlr_mims;
                    String currency_mims = rs.getString(10) != null ? rs.getString(10).toUpperCase() : "";
                    
                    bean.setStd_job_no_mims( std_job_no_mims ); // STD_JOB_NO
                    bean.setWo_type_mims( wo_type_mims ); // WO_TYPE               
                    bean.setWork_group_mims( work_group_mims ); // WORK_GROUP
                    bean.setAccount_code_c_mims( account_code_c_mims ); // ACCOUNT_CODE
                    bean.setAccount_code_i_mims( account_code_i_mims ); // REALL_ACCT_CDE
                    bean.setUnit_of_work_mims( unit_of_work_mims ); // UNIT_OF_WORK
                    bean.setVlr_freight_mims( vlr_freight_mims ); // VLR_FREIGHT
                    bean.setCurrency_mims( currency_mims ); // MONEDA
                    if ( REF_NO.equals("002") )
                        bean.setOrigin_code_mims( REF_CODE ); // ORIGEN
                    if ( REF_NO.equals("003") )
                        bean.setDestination_code_mims( REF_CODE ); // DESTINO

                    if ( cont == 2 ) {
                        
                        REF_CODE = buscarStdJobGen ( std_job_no_mims );
                        bean.setStdjobgen_mims( REF_CODE ); // STDJOBGEN
                        
                        buscarRecursos ( std_job_no_mims, bean );
                        
                        if ( existeStdJob ( std_job_no_mims ) ) {
                            
                            buscarStdJob ( bean );

                        } else { // GUARDA STDJOBS Q NO EXISTEN EN web
                            
                            bean.setStd_job_no_mims( "*" + std_job_no_mims ); // * STD_JOB_NO -> Indica q no existen en web.
                            this.vector_stdjob.add ( bean );
                            
                        }
                        
                        cont = 0;
                        this.bean = new StdJobMims ();
                        
                    }
                    
                }
                
            }
            
        } catch ( SQLException e ){
            
            e.printStackTrace ();
            throw new SQLException( "ERROR EN 'stdjob_mims' - [ControlSTDJOB].. " + e.getMessage() + " " + e.getErrorCode() );
        
        }
        
        finally{
            
            if ( st != null ){
                try{
                    st.close ();
                } catch ( SQLException e ){
                    throw new SQLException ( "ERROR CERRANDO EL ESTAMENTO " + e.getMessage() );
                }
            }            
            if ( con != null ){
                poolManager.freeConnection ( "oracle", con );
            }
            
        }
        
    }
    
    /**
     * Se encarga de buscar el stdjob general de un stdjob especifico.
     * @autor LREALES
     * @param el numero del stdjob.
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public String buscarStdJobGen ( String std_job_no_mims ) throws SQLException{
        
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
                
        String stdjobgen = "";
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( "oracle" );
            
            if( con != null ){
                
                st = con.prepareStatement( this.SQL_MSF071_010 );
                
                st.setString( 1, std_job_no_mims ); 
                
                rs = st.executeQuery();
                
                if ( rs.next() ){
                    
                    stdjobgen = rs.getString(1) != null ? rs.getString(1).toUpperCase() : "";
                    
                }
                
            }
                        
        } catch( SQLException e ){
            
            e.printStackTrace();
            throw new SQLException( "ERROR EN 'existeStdJob' - [ControlSTDJOB].. " + e.getMessage() + " - " + e.getErrorCode() );
        
        }
        
        finally{
            
            if ( st != null ){
                try{
                    st.close();
                } catch( SQLException e ){
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO " + e.getMessage() );
                }
            }            
            if ( con != null ){
                poolManager.freeConnection( "oracle", con );
            }
            
        }
        
        return stdjobgen;
        
    }
    
    /**
     * Obtiene una lista de los recursos de ese StdJob.
     * @autor LREALES
     * @param el numero del stdjob.
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void buscarRecursos ( String std_job_no_mims, StdJobMims bean ) throws SQLException {
        
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance ();
            con = poolManager.getConnection ( "oracle" );
            
            if( con != null ){
                
                st = con.prepareStatement ( this.SQL_MSF735_MSF010 );
                
                st.setString ( 1, std_job_no_mims );  
                
                rs = st.executeQuery ();
                
                while ( rs.next () ){
                    
                    String numero = rs.getString(1) != null ? rs.getString(1).toUpperCase() : "";
                    String tipo = rs.getString(2) != null ? rs.getString(2).toUpperCase() : "";
                    String recurso = rs.getString(3) != null ? rs.getString(3).toUpperCase() : "";
                    
                    if ( numero.equals("01") ) {
                        bean.setTipo_recurso1_mims( tipo );
                        bean.setRecurso1_mims( recurso );
                    }
                    if ( numero.equals("02") ) {
                        bean.setTipo_recurso2_mims( tipo );
                        bean.setRecurso2_mims( recurso );
                    }
                    if ( numero.equals("03") ) {
                        bean.setTipo_recurso3_mims( tipo );
                        bean.setRecurso3_mims( recurso );
                    }
                    if ( numero.equals("04") ) {
                        bean.setTipo_recurso4_mims( tipo );
                        bean.setRecurso4_mims( recurso );
                    }
                    if ( numero.equals("05") ) {
                        bean.setTipo_recurso5_mims( tipo );
                        bean.setRecurso5_mims( recurso );
                    }
                    
                }
                
            }
            
        } catch ( SQLException e ){
            
            e.printStackTrace ();
            throw new SQLException( "ERROR EN 'buscarRecursos' - [ControlSTDJOB].. " + e.getMessage() + " " + e.getErrorCode() );
        
        }
        
        finally{
            
            if ( st != null ){
                try{
                    st.close ();
                } catch ( SQLException e ){
                    throw new SQLException ( "ERROR CERRANDO EL ESTAMENTO " + e.getMessage() );
                }
            }            
            if ( con != null ){
                poolManager.freeConnection ( "oracle", con );
            }
            
        }
        
    }
    
    /**
     * Se encarga de verificar si existe o no un stdjob dado.
     * @autor LREALES
     * @param el numero del stdjob.
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public boolean existeStdJob ( String std_job_no_mims ) throws SQLException{
        
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
                
        boolean sw = false;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( "sot" );
            
            if( con != null ){
                
                st = con.prepareStatement( this.SQL_STDJOB );
                
                st.setString( 1, std_job_no_mims ); 
                
                rs = st.executeQuery();
                
                if ( rs.next() ){
                    
                    sw = true;
                    
                }
                
            }
                        
        } catch( SQLException e ){
            
            e.printStackTrace();
            throw new SQLException( "ERROR EN 'existeStdJob' - [ControlSTDJOB].. " + e.getMessage() + " - " + e.getErrorCode() );
        
        }
        
        finally{
            
            if ( st != null ){
                try{
                    st.close();
                } catch( SQLException e ){
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO " + e.getMessage() );
                }
            }            
            if ( con != null ){
                poolManager.freeConnection( "sot", con );
            }
            
        }
        
        return sw;
        
    }
    
    /**
     * Se encarga de buscar la info de un stdjob.
     * @autor LREALES
     * @param el numero del stdjob.
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void buscarStdJob ( StdJobMims bean ) throws SQLException {
        
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
                
        int todo_igual = 0;
        
        try {
            
            poolManager = PoolManager.getInstance ();
            con = poolManager.getConnection ( "sot" );
            
            if( con != null ) {
                
                st = con.prepareStatement ( this.SQL_STDJOB );
                
                st.setString ( 1, bean.getStd_job_no_mims() ); 
                
                rs = st.executeQuery ();
                
                if ( rs.next () ) {
                    
                    String std_job_no_web = rs.getString( "std_job_no" ) != null ? rs.getString( "std_job_no" ).toUpperCase() : "";
                    String wo_type_web = rs.getString( "wo_type" ) != null ? rs.getString( "wo_type" ).toUpperCase() : "";
                    String work_group_web = rs.getString( "work_group" ) != null ? rs.getString( "work_group" ).toUpperCase() : "";                    
                    String unit_of_work_web = rs.getString( "unit_of_work" ) != null ? rs.getString( "unit_of_work" ).toUpperCase() : "";
                    String origin_code_web = rs.getString( "origin_code" ) != null ? rs.getString( "origin_code" ).toUpperCase() : "";
                    String destination_code_web = rs.getString( "destination_code" ) != null ? rs.getString( "destination_code" ).toUpperCase() : "";
                    String vlr_freight_web = rs.getString( "vlr_freight" ) != null ? rs.getString( "vlr_freight" ).toUpperCase() : "";
                    double vlr_web = Double.parseDouble( vlr_freight_web );
                    vlr_freight_web = "" + vlr_web;
                    String currency_web = rs.getString( "currency" ) != null ? rs.getString( "currency" ).toUpperCase() : "";
                    String account_code_c_web = rs.getString( "account_code_c" ) != null ? rs.getString( "account_code_c" ).toUpperCase() : "";
                    String account_code_i_web = rs.getString( "account_code_i" ) != null ? rs.getString( "account_code_i" ).toUpperCase() : "";
                    String stdjobgen_web = rs.getString( "stdjobgen" ) != null ? rs.getString( "stdjobgen" ).toUpperCase() : "";
                    
                    bean.setStd_job_no_web( std_job_no_web );
                    
                    if ( ! wo_type_web.equals ( bean.getWo_type_mims().toString() ) ) {
                        bean.setWo_type_web( wo_type_web );
                    } else {
                        bean.setWo_type_web( "" );
                        bean.setWo_type_mims( "" );
                        todo_igual++;
                    }
                      
                    if ( ! work_group_web.equals ( bean.getWork_group_mims().toString() ) ) {
                        bean.setWork_group_web( work_group_web );
                    } else {
                        bean.setWork_group_web( "" );
                        bean.setWork_group_mims( "" );
                        todo_igual++;
                    }
                                        
                    if ( ! account_code_c_web.equals ( bean.getAccount_code_c_mims().toString() ) ) {
                        bean.setAccount_code_c_web( account_code_c_web );
                    } else {
                        bean.setAccount_code_c_web( "" );
                        bean.setAccount_code_c_mims( "" );
                        todo_igual++;
                    }
                    
                    if ( ! account_code_i_web.equals ( bean.getAccount_code_i_mims().toString() ) ) {
                        bean.setAccount_code_i_web( account_code_i_web );
                    } else {
                        bean.setAccount_code_i_web( "" );
                        bean.setAccount_code_i_mims( "" );
                        todo_igual++;
                    }
                    
                    if ( ! unit_of_work_web.equals ( bean.getUnit_of_work_mims().toString() ) ) {
                        bean.setUnit_of_work_web( unit_of_work_web );
                    } else {
                        bean.setUnit_of_work_web( "" );
                        bean.setUnit_of_work_mims( "" );
                        todo_igual++;
                    }
                    
                    if ( ! vlr_freight_web.equals ( bean.getVlr_freight_mims().toString() ) ) {
                        bean.setVlr_freight_web( vlr_freight_web );
                    } else {
                        bean.setVlr_freight_web( "" );
                        bean.setVlr_freight_mims( "" );
                        todo_igual++;
                    }
                    
                    if ( ! currency_web.equals ( bean.getCurrency_mims().toString() ) ) {
                        bean.setCurrency_web( currency_web );
                    } else {
                        bean.setCurrency_web( "" );
                        bean.setCurrency_mims( "" );
                        todo_igual++;
                    }
                                                             
                    if ( ! stdjobgen_web.equals ( bean.getStdjobgen_mims().toString() ) ) {
                        bean.setStdjobgen_web( stdjobgen_web );
                    } else {
                        bean.setStdjobgen_web( "" );
                        bean.setStdjobgen_mims( "" );
                        todo_igual++;
                    }                                         
                    if ( ! origin_code_web.equals ( bean.getOrigin_code_mims().toString() ) ) {
                        bean.setOrigin_code_web( origin_code_web );
                    } else {
                        bean.setOrigin_code_web( "" );
                        bean.setOrigin_code_mims( "" );
                        todo_igual++;
                    }                                           
                    if ( ! destination_code_web.equals ( bean.getDestination_code_mims().toString() ) ) {
                        bean.setDestination_code_web( destination_code_web );
                    } else {
                        bean.setDestination_code_web( "" );
                        bean.setDestination_code_mims( "" );
                        todo_igual++;
                    }   
                    
                    if ( todo_igual != 10 ) {
                        this.vector_stdjob.add ( bean );
                    }
                    
                }
                
            }
                        
        } catch ( SQLException e ) {
            
            e.printStackTrace();
            throw new SQLException( "ERROR EN 'buscarStdJob' - [ControlSTDJOB].. " + e.getMessage() + " - " + e.getErrorCode() );
        
        }
        
        finally {
            
            if ( st != null ) {
                try{
                    st.close ();
                } catch ( SQLException e ) {
                    throw new SQLException ( "ERROR CERRANDO EL ESTAMENTO " + e.getMessage() );
                }
            }            
            if ( con != null ) {
                poolManager.freeConnection ( "sot", con );
            }
            
        }
        
    }
        
    public synchronized void exportar() {
        
        try{
            
            Util u = new Util();
            
            ResourceBundle rb = ResourceBundle.getBundle( "com/tsp/util/connectionpool/db" );
            String path = rb.getString( "ruta" );
                        
            File file = new File( path + "/exportar/migracion/" + usuario );
            file.mkdirs();
               
            String fecha = Util.getFechaActual_String(6);            
            fecha=fecha.replaceAll( "/", "-" );
            fecha=fecha.replaceAll( ":", "_" );
            
            String nombreArch  = "ControlSTDJOB[" + fecha + "].xls";
            String       Hoja  = "ControlSTDJOB";
            String       Ruta  = path + "/exportar/migracion/" + usuario + "/" +nombreArch; 
            
            HSSFWorkbook wb    = new HSSFWorkbook();
            HSSFSheet    sheet = wb.createSheet( Hoja );
            HSSFRow      row   = null;
            HSSFRow      row2  = null;
            HSSFCell     cell  = null;
            
            for ( int col = 0; col < 41 ; col++ ){ //COLUMNAS
                
                sheet.setColumnWidth( (short) col, (short) ( ( 50 * 8 ) / ( (double) 1 / 15 ) ) ); 
                
            }
            
            /****  ENCABEZADO Y DEFINICION DE ESTILOS ************************************************/
            
            /** ENCABEZADO GENERAL *******************************/
            HSSFFont  fuente1 = wb.createFont();
            fuente1.setFontName("verdana");
            fuente1.setFontHeightInPoints((short)(16)) ;
            fuente1.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            fuente1.setColor(HSSFColor.DARK_RED.index);
            
            HSSFCellStyle estilo1 = wb.createCellStyle();
            estilo1.setFont(fuente1);
            estilo1.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo1.setBottomBorderColor(HSSFColor.WHITE.index);
            estilo1.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo1.setLeftBorderColor  (HSSFColor.WHITE.index);
            estilo1.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo1.setRightBorderColor(HSSFColor.WHITE.index);
            estilo1.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo1.setTopBorderColor   (HSSFColor.WHITE.index);
            
            
            /** SUBTITULO *******************************/
            HSSFFont  fuenteX = wb.createFont();
            fuenteX.setFontName("verdana");
            fuenteX.setFontHeightInPoints((short)(11)) ;
            fuenteX.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            fuenteX.setColor(HSSFColor.DARK_RED.index);
            
            HSSFCellStyle estiloX = wb.createCellStyle();
            estiloX.setFont(fuenteX);
            estiloX.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estiloX.setBottomBorderColor(HSSFColor.WHITE.index);
            estiloX.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estiloX.setLeftBorderColor  (HSSFColor.WHITE.index);
            estiloX.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estiloX.setRightBorderColor(HSSFColor.WHITE.index);
            estiloX.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estiloX.setTopBorderColor   (HSSFColor.WHITE.index);
            
            /** TEXTO EN EL ENCABEAZADO *************************/
            HSSFFont  fuente2 = wb.createFont();
            fuente2.setFontName("verdana");
            fuente2.setFontHeightInPoints((short)(11)) ;
            fuente2.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            fuente2.setColor((short)(0x0));
            
            HSSFCellStyle estilo2 = wb.createCellStyle();
            estilo2.setFont(fuente2);
            estilo2.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo2.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            
            /** ENCABEZADO DE LAS COLUMNAS***********************/
            HSSFFont  fuente3 = wb.createFont();
            fuente3.setFontName("verdana");
            fuente3.setFontHeightInPoints((short)(9)) ;
            fuente3.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            fuente3.setColor(HSSFColor.BLACK.index);
            
            HSSFCellStyle estilo3 = wb.createCellStyle();
            estilo3.setFont(fuente3);
            estilo3.setFillForegroundColor(HSSFColor.GREY_25_PERCENT.index);
            estilo3.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo3.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo3.setBottomBorderColor(HSSFColor.BLACK.index);
            estilo3.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo3.setLeftBorderColor  (HSSFColor.BLACK.index);
            estilo3.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo3.setRightBorderColor(HSSFColor.BLACK.index);
            estilo3.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo3.setTopBorderColor   (HSSFColor.BLACK.index);
            estilo3.setAlignment(HSSFCellStyle.ALIGN_CENTER);            
                        
            /** TEXTO NORMAL ************************************/
            HSSFFont  fuente4 = wb.createFont();
            fuente4.setFontName("verdana");
            fuente4.setFontHeightInPoints((short)(9)) ;
            fuente4.setBoldweight(HSSFFont.BOLDWEIGHT_NORMAL);
            fuente4.setColor((short)(0x0));
            
            HSSFCellStyle estilo4 = wb.createCellStyle();
            estilo4.setFont(fuente4);
            estilo4.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo4.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo4.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo4.setBottomBorderColor(HSSFColor.BLACK.index);
            estilo4.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo4.setLeftBorderColor  (HSSFColor.BLACK.index);
            estilo4.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo4.setRightBorderColor(HSSFColor.BLACK.index);
            estilo4.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo4.setTopBorderColor   (HSSFColor.BLACK.index);
            
            /** NUMEROS ************************************/
            HSSFCellStyle estilo5 = wb.createCellStyle();
            estilo5.setFont(fuente4);
            estilo5.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo5.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo5.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo5.setBottomBorderColor(HSSFColor.BLACK.index);
            estilo5.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo5.setLeftBorderColor  (HSSFColor.BLACK.index);
            estilo5.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo5.setRightBorderColor(HSSFColor.BLACK.index);
            estilo5.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo5.setTopBorderColor   (HSSFColor.BLACK.index);
            estilo5.setAlignment(HSSFCellStyle.ALIGN_RIGHT);
            /****************************************************/
            
            HSSFFont fuente6 = wb.createFont();
            fuente6.setColor((short)0x0);  
            fuente6.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            
            HSSFCellStyle estilo6 = wb.createCellStyle();
            estilo6.setFont(fuente6);
            estilo6.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo6.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo6.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo6.setBottomBorderColor(HSSFColor.WHITE.index);
            estilo6.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo6.setLeftBorderColor  (HSSFColor.WHITE.index);
            estilo6.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo6.setRightBorderColor(HSSFColor.WHITE.index);
            estilo6.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo6.setTopBorderColor   (HSSFColor.WHITE.index);
            
            /****************************************************/  
            /* MONEDA */
            HSSFCellStyle estilo7 = wb.createCellStyle();
            estilo7.setFont(fuente4);
            estilo7.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo7.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo7.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo7.setBottomBorderColor(HSSFColor.BLACK.index);
            estilo7.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo7.setLeftBorderColor  (HSSFColor.BLACK.index);
            estilo7.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo7.setRightBorderColor(HSSFColor.BLACK.index);
            estilo7.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo7.setTopBorderColor   (HSSFColor.BLACK.index);
            estilo7.setAlignment(HSSFCellStyle.ALIGN_RIGHT);
            estilo7.setDataFormat(wb.createDataFormat().getFormat("$#,##0.00"));
            
            /* TEXTO NORMAL CENTRADO */
            HSSFCellStyle estilo9 = wb.createCellStyle();
            estilo9.setFont(fuente4);
            estilo9.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo9.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo9.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo9.setBottomBorderColor(HSSFColor.BLACK.index);
            estilo9.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo9.setLeftBorderColor  (HSSFColor.BLACK.index);
            estilo9.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo9.setRightBorderColor(HSSFColor.BLACK.index);
            estilo9.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo9.setTopBorderColor   (HSSFColor.BLACK.index);
            estilo9.setAlignment(HSSFCellStyle.ALIGN_CENTER);
            
            row  = sheet.createRow((short)(0));
            row  = sheet.createRow((short)(1));
            row  = sheet.createRow((short)(2));
            row  = sheet.createRow((short)(3));
            row  = sheet.createRow((short)(4));
            row  = sheet.createRow((short)(5));
            row  = sheet.createRow((short)(6));
            row  = sheet.createRow((short)(7));
            row  = sheet.createRow((short)(8));
            row  = sheet.createRow((short)(9));
            
            for ( int j = 0; j < 41; j++ ) { //COLUMNAS
                
                row  = sheet.getRow((short)(0));
                cell = row.createCell((short)(j));
                cell.setCellStyle(estilo1);
                row  = sheet.getRow((short)(1));
                cell = row.createCell((short)(j)); 
                cell.setCellStyle(estiloX);
                
            }
            
            row  = sheet.getRow((short)(0));
            cell = row.getCell((short)(0));
            cell.setCellValue("TRANSPORTES SANCHEZ POLO");
                        
            row  = sheet.getRow((short)(1));            
            cell = row.getCell((short)(0));            
            cell.setCellValue("Reporte de Control del StdJob");
            
            for( int i = 2; i < 6; i++ ){ //FILAS
                 
                for ( int j = 0; j < 41; j++ ) { //COLUMNAS
                    
                    row  = sheet.getRow((short)(i));
                    cell = row.createCell((short)(j)); 
                    cell.setCellStyle(estilo6);
                    
                }      
                
            }
            
            //FECHA
            row = sheet.getRow((short)(3));              
            cell = row.createCell((short)(0));            
            cell = row.getCell((short)(0));            
            cell.setCellStyle(estilo6);
            cell.setCellValue("Desde: " + fec_ini + " - Hasta: " + fec_fin);
                        
            row = sheet.getRow((short)(2)); 
            cell = row.createCell((short)(3));            
            cell = row.getCell((short)(3));  
            cell.setCellStyle(estilo6);
            cell.setCellValue("* Indica que ese StdJob NO EXISTE en la Web.");
            
            /*************************************************************************************/
            /***** RECORRER LOS DATOS ******/
            
            int tam = 0;
            tam = getVector_stdjob().size();
            
            ////System.out.println("Numero de Filas Impresas: " + tam);
            
            int Fila = 6; 
            for ( int t = 0; t < tam; t++ ){
                
                StdJobMims info = ( StdJobMims ) getVector_stdjob().elementAt( t );
                
                Fila++;
                row  = sheet.createRow( ( short )( Fila ) );
                
                cell = row.createCell( ( short )( 0 ) );// Std_job_no
                cell.setCellStyle( estilo4 );
                cell.setCellValue( info.getStd_job_no_mims() );
                
                cell = row.createCell( ( short )( 1 ) );// Wo_type_web
                cell.setCellStyle( estilo4 );
                cell.setCellValue( info.getWo_type_web() );
                
                cell = row.createCell( ( short )( 2 ) );// Wo_type_mims
                cell.setCellStyle( estilo4 );
                cell.setCellValue( info.getWo_type_mims() );
                
                cell = row.createCell( ( short )( 3 ) );// Work_group_web
                cell.setCellStyle( estilo4 );
                cell.setCellValue( info.getWork_group_web() );
                
                cell = row.createCell( ( short )( 4 ) );// Work_group_mims
                cell.setCellStyle( estilo4 );
                cell.setCellValue( info.getWork_group_mims() );
                
                cell = row.createCell( ( short )( 5 ) );// Account_code_c_web
                cell.setCellStyle( estilo4 );
                cell.setCellValue( info.getAccount_code_c_web() );
                
                cell = row.createCell( ( short )( 6 ) );// Account_code_c_mims
                cell.setCellStyle( estilo4 );
                cell.setCellValue( info.getAccount_code_c_mims() );
                
                cell = row.createCell( ( short )( 7 ) );// Account_code_i_web
                cell.setCellStyle( estilo4 );
                cell.setCellValue( info.getAccount_code_i_web() );
                
                cell = row.createCell( ( short )( 8 ) );// Account_code_i_mims
                cell.setCellStyle( estilo4 );
                cell.setCellValue( info.getAccount_code_i_mims() );
                
                cell = row.createCell( ( short )( 9 ) );// Unit_of_work_web
                cell.setCellStyle( estilo4 );
                cell.setCellValue( info.getUnit_of_work_web() );
                
                cell = row.createCell( ( short )( 10 ) );// Unit_of_work_mims
                cell.setCellStyle( estilo4 );
                cell.setCellValue( info.getUnit_of_work_mims() );
                
                cell = row.createCell( ( short )( 11 ) );// Vlr_freight_web
                cell.setCellStyle( estilo4 );
                cell.setCellValue( info.getVlr_freight_web() );
                
                cell = row.createCell( ( short )( 12 ) );// Vlr_freight_mims
                cell.setCellStyle( estilo4 );
                cell.setCellValue( info.getVlr_freight_mims() );// OJO HAY Q DIVIDIR ENTRE 10.000
                
                cell = row.createCell( ( short )( 13 ) );// Currency_web
                cell.setCellStyle( estilo4 );
                cell.setCellValue( info.getCurrency_web() );
                
                cell = row.createCell( ( short )( 14 ) );// Currency_mims
                cell.setCellStyle( estilo4 );
                cell.setCellValue( info.getCurrency_mims() );
                
                cell = row.createCell( ( short )( 15 ) );// Stdjobgen_web
                cell.setCellStyle( estilo4 );
                cell.setCellValue( info.getStdjobgen_web() );
                
                cell = row.createCell( ( short )( 16 ) );// Stdjobgen_mims
                cell.setCellStyle( estilo4 );
                cell.setCellValue( info.getStdjobgen_mims() );
                
                cell = row.createCell( ( short )( 17 ) );// Origin_code_web
                cell.setCellStyle( estilo4 );
                cell.setCellValue( info.getOrigin_code_web() );
                
                cell = row.createCell( ( short )( 18 ) );// Origin_code_mims
                cell.setCellStyle( estilo4 );
                cell.setCellValue( info.getOrigin_code_mims() );
                
                cell = row.createCell( ( short )( 19 ) );// Destination_code_web
                cell.setCellStyle( estilo4 );
                cell.setCellValue( info.getDestination_code_web() );
                
                cell = row.createCell( ( short )( 20 ) );// Destination_code_mims
                cell.setCellStyle( estilo4 );
                cell.setCellValue( info.getDestination_code_mims() );
                
                cell = row.createCell( ( short )( 21 ) );// Tipo_recurso1_web
                cell.setCellStyle( estilo4 );
                cell.setCellValue( info.getTipo_recurso1_web() );
                
                cell = row.createCell( ( short )( 22 ) );// Tipo_recurso1_mims
                cell.setCellStyle( estilo4 );
                cell.setCellValue( info.getTipo_recurso1_mims() );
                
                cell = row.createCell( ( short )( 23 ) );// Recurso1_web
                cell.setCellStyle( estilo4 );
                cell.setCellValue( info.getRecurso1_web() );
                
                cell = row.createCell( ( short )( 24 ) );// Recurso1_mims
                cell.setCellStyle( estilo4 );
                cell.setCellValue( info.getRecurso1_mims() );
                
                cell = row.createCell( ( short )( 25 ) );// Tipo_recurso2_web
                cell.setCellStyle( estilo4 );
                cell.setCellValue( info.getTipo_recurso2_web() );
                
                cell = row.createCell( ( short )( 26 ) );// Tipo_recurso2_mims
                cell.setCellStyle( estilo4 );
                cell.setCellValue( info.getTipo_recurso2_mims() );
                
                cell = row.createCell( ( short )( 27 ) );// Recurso2_web
                cell.setCellStyle( estilo4 );
                cell.setCellValue( info.getRecurso2_web() );
                
                cell = row.createCell( ( short )( 28 ) );// Recurso2_mims
                cell.setCellStyle( estilo4 );
                cell.setCellValue( info.getRecurso2_mims() );
                
                cell = row.createCell( ( short )( 29 ) );// Tipo_recurso3_web
                cell.setCellStyle( estilo4 );
                cell.setCellValue( info.getTipo_recurso3_web() );
                
                cell = row.createCell( ( short )( 30 ) );// Tipo_recurso3_mims
                cell.setCellStyle( estilo4 );
                cell.setCellValue( info.getTipo_recurso3_mims() );
                
                cell = row.createCell( ( short )( 31 ) );// Recurso3_web
                cell.setCellStyle( estilo4 );
                cell.setCellValue( info.getRecurso3_web() );
                
                cell = row.createCell( ( short )( 32 ) );// Recurso3_mims
                cell.setCellStyle( estilo4 );
                cell.setCellValue( info.getRecurso3_mims() );
                
                cell = row.createCell( ( short )( 33 ) );// Tipo_recurso4_web
                cell.setCellStyle( estilo4 );
                cell.setCellValue( info.getTipo_recurso4_web() );
                
                cell = row.createCell( ( short )( 34 ) );// Tipo_recurso4_mims
                cell.setCellStyle( estilo4 );
                cell.setCellValue( info.getTipo_recurso4_mims() );
                
                cell = row.createCell( ( short )( 35 ) );// Recurso4_web
                cell.setCellStyle( estilo4 );
                cell.setCellValue( info.getRecurso4_web() );
                
                cell = row.createCell( ( short )( 36 ) );// Recurso4_mims
                cell.setCellStyle( estilo4 );
                cell.setCellValue( info.getRecurso4_mims() );
                
                cell = row.createCell( ( short )( 37 ) );// Tipo_recurso5_web
                cell.setCellStyle( estilo4 );
                cell.setCellValue( info.getTipo_recurso5_web() );
                
                cell = row.createCell( ( short )( 38 ) );// Tipo_recurso5_mims
                cell.setCellStyle( estilo4 );
                cell.setCellValue( info.getTipo_recurso5_mims() );
                
                cell = row.createCell( ( short )( 39 ) );// Recurso5_web
                cell.setCellStyle( estilo4 );
                cell.setCellValue( info.getRecurso5_web() );
                
                cell = row.createCell( ( short )( 40 ) );// Recurso5_mims
                cell.setCellStyle( estilo4 );
                cell.setCellValue( info.getRecurso5_mims() );
                
            }
            
            // Para combinar celdas!!
            // sheet.addMergedRegion(new Region(filaInicial,(short)columnaInicial ,filaFinal,(short)columnaFinal));
            
            row = sheet.createRow((short)(5));            
            row = sheet.getRow( (short)(5) );  
            
            sheet.addMergedRegion( new Region(5, (short)0 , 6, (short)0 ) );
            cell = row.createCell((short)(0));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Std_job_no");
            
            sheet.addMergedRegion( new Region(5, (short)1 , 5, (short)2 ) );
            cell = row.createCell((short)(1));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Wo_type");
            cell = row.createCell((short)(2));
            cell.setCellStyle(estilo3);
            
            sheet.addMergedRegion( new Region(5, (short)3 , 5, (short)4 ) );
            cell = row.createCell((short)(3));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Work_group");
            cell = row.createCell((short)(4));
            cell.setCellStyle(estilo3);
            
            sheet.addMergedRegion( new Region(5, (short)5 , 5, (short)6 ) );
            cell = row.createCell((short)(5));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Account_code_c");
            cell = row.createCell((short)(6));
            cell.setCellStyle(estilo3);
            
            sheet.addMergedRegion( new Region(5, (short)7 , 5, (short)8 ) );
            cell = row.createCell((short)(7));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Account_code_i");
            cell = row.createCell((short)(8));
            cell.setCellStyle(estilo3);
            
            sheet.addMergedRegion( new Region(5, (short)9 , 5, (short)10 ) );
            cell = row.createCell((short)(9));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Unit_of_work");
            cell = row.createCell((short)(10));
            cell.setCellStyle(estilo3);
            
            sheet.addMergedRegion( new Region(5, (short)11 , 5, (short)12 ) );
            cell = row.createCell((short)(11));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Vlr_freight");
            cell = row.createCell((short)(12));
            cell.setCellStyle(estilo3);
            
            sheet.addMergedRegion( new Region(5, (short)13 , 5, (short)14 ) );
            cell = row.createCell((short)(13));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Currency");
            cell = row.createCell((short)(14));
            cell.setCellStyle(estilo3);
            
            sheet.addMergedRegion( new Region(5, (short)15 , 5, (short)16 ) );
            cell = row.createCell((short)(15));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Stdjobgen");
            cell = row.createCell((short)(16));
            cell.setCellStyle(estilo3);
            
            sheet.addMergedRegion( new Region(5, (short)17 , 5, (short)18 ) );
            cell = row.createCell((short)(17));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Origin_code");
            cell = row.createCell((short)(18));
            cell.setCellStyle(estilo3);
            
            sheet.addMergedRegion( new Region(5, (short)19 , 5, (short)20 ) );
            cell = row.createCell((short)(19));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Destination_code");
            cell = row.createCell((short)(20));
            cell.setCellStyle(estilo3);
            
            sheet.addMergedRegion( new Region(5, (short)21 , 5, (short)22 ) );
            cell = row.createCell((short)(21));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Tipo_recurso1");
            cell = row.createCell((short)(22));
            cell.setCellStyle(estilo3);
            
            sheet.addMergedRegion( new Region(5, (short)23 , 5, (short)24 ) );
            cell = row.createCell((short)(23));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Recurso1");
            cell = row.createCell((short)(24));
            cell.setCellStyle(estilo3);
            
            sheet.addMergedRegion( new Region(5, (short)25 , 5, (short)26 ) );
            cell = row.createCell((short)(25));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Tipo_recurso2");
            cell = row.createCell((short)(26));
            cell.setCellStyle(estilo3);
            
            sheet.addMergedRegion( new Region(5, (short)27 , 5, (short)28 ) );
            cell = row.createCell((short)(27));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Recurso2");
            cell = row.createCell((short)(28));
            cell.setCellStyle(estilo3);
            
            sheet.addMergedRegion( new Region(5, (short)29 , 5, (short)30 ) );
            cell = row.createCell((short)(29));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Tipo_recurso3");
            cell = row.createCell((short)(30));
            cell.setCellStyle(estilo3);
            
            sheet.addMergedRegion( new Region(5, (short)31 , 5, (short)32 ) );
            cell = row.createCell((short)(31));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Recurso3");
            cell = row.createCell((short)(32));
            cell.setCellStyle(estilo3);
            
            sheet.addMergedRegion( new Region(5, (short)33 , 5, (short)34 ) );
            cell = row.createCell((short)(33));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Tipo_recurso4");
            cell = row.createCell((short)(34));
            cell.setCellStyle(estilo3);
            
            sheet.addMergedRegion( new Region(5, (short)35 , 5, (short)36 ) );
            cell = row.createCell((short)(35));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Recurso4");
            cell = row.createCell((short)(36));
            cell.setCellStyle(estilo3);
            
            sheet.addMergedRegion( new Region(5, (short)37 , 5, (short)38 ) );
            cell = row.createCell((short)(37));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Tipo_recurso5");
            cell = row.createCell((short)(38));
            cell.setCellStyle(estilo3);
            
            sheet.addMergedRegion( new Region(5, (short)39 , 5, (short)40 ) );
            cell = row.createCell((short)(39));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Recurso5");
            cell = row.createCell((short)(40));
            cell.setCellStyle(estilo3);
            
            
            row = sheet.createRow((short)(6));            
            row = sheet.getRow( (short)(6) );     
            
            cell = row.createCell((short)(1));// Wo_type_web
            cell.setCellStyle(estilo3);
            cell.setCellValue("Valor Web");
            
            cell = row.createCell((short)(2));// Wo_type_mims
            cell.setCellStyle(estilo3);
            cell.setCellValue("Valor Mims");
            
            cell = row.createCell((short)(3));// Work_group_web
            cell.setCellStyle(estilo3);
            cell.setCellValue("Valor Web");
            
            cell = row.createCell((short)(4));// Work_group_mims
            cell.setCellStyle(estilo3);
            cell.setCellValue("Valor Mims");
            
            cell = row.createCell((short)(5));// Account_code_c_web
            cell.setCellStyle(estilo3);
            cell.setCellValue("Valor Web");
            
            cell = row.createCell((short)(6));// Account_code_c_mims
            cell.setCellStyle(estilo3);
            cell.setCellValue("Valor Mims");
            
            cell = row.createCell((short)(7));// Account_code_i_web
            cell.setCellStyle(estilo3);
            cell.setCellValue("Valor Web");
            
            cell = row.createCell((short)(8));// Account_code_i_mims
            cell.setCellStyle(estilo3);
            cell.setCellValue("Valor Mims");
            
            cell = row.createCell((short)(9));// Unit_of_work_web
            cell.setCellStyle(estilo3);
            cell.setCellValue("Valor Web");
            
            cell = row.createCell((short)(10));// Unit_of_work_mims
            cell.setCellStyle(estilo3);
            cell.setCellValue("Valor Mims");
            
            cell = row.createCell((short)(11));// Vlr_freight_web
            cell.setCellStyle(estilo3);
            cell.setCellValue("Valor Web");
            
            cell = row.createCell((short)(12));// Vlr_freight_mims
            cell.setCellStyle(estilo3);
            cell.setCellValue("Valor Mims");
            
            cell = row.createCell((short)(13));// Currency_web
            cell.setCellStyle(estilo3);
            cell.setCellValue("Valor Web");
            
            cell = row.createCell((short)(14));// Currency_mims
            cell.setCellStyle(estilo3);
            cell.setCellValue("Valor Mims");
            
            cell = row.createCell((short)(15));// Stdjobgen_web
            cell.setCellStyle(estilo3);
            cell.setCellValue("Valor Web");
            
            cell = row.createCell((short)(16));// Stdjobgen_mims
            cell.setCellStyle(estilo3);
            cell.setCellValue("Valor Mims");
            
            cell = row.createCell((short)(17));// Origin_code_web
            cell.setCellStyle(estilo3);
            cell.setCellValue("Valor Web");
            
            cell = row.createCell((short)(18));// Origin_code_mims
            cell.setCellStyle(estilo3);
            cell.setCellValue("Valor Mims");
            
            cell = row.createCell((short)(19));// Destination_code_web
            cell.setCellStyle(estilo3);
            cell.setCellValue("Valor Web");
            
            cell = row.createCell((short)(20));// Destination_code_mims
            cell.setCellStyle(estilo3);
            cell.setCellValue("Valor Mims");
            
            cell = row.createCell((short)(21));// Tipo_recurso1_web
            cell.setCellStyle(estilo3);
            cell.setCellValue("Valor Web");
            
            cell = row.createCell((short)(22));// Tipo_recurso1_mims
            cell.setCellStyle(estilo3);
            cell.setCellValue("Valor Mims");
            
            cell = row.createCell((short)(23));// Recurso1_web
            cell.setCellStyle(estilo3);
            cell.setCellValue("Valor Web");
            
            cell = row.createCell((short)(24));// Recurso1_mims
            cell.setCellStyle(estilo3);
            cell.setCellValue("Valor Mims");
            
            cell = row.createCell((short)(25));// Tipo_recurso2_web
            cell.setCellStyle(estilo3);
            cell.setCellValue("Valor Web");
            
            cell = row.createCell((short)(26));// Tipo_recurso2_mims
            cell.setCellStyle(estilo3);
            cell.setCellValue("Valor Mims");
            
            cell = row.createCell((short)(27));// Recurso2_web
            cell.setCellStyle(estilo3);
            cell.setCellValue("Valor Web");
            
            cell = row.createCell((short)(28));// Recurso2_mims
            cell.setCellStyle(estilo3);
            cell.setCellValue("Valor Mims");
            
            cell = row.createCell((short)(29));// Tipo_recurso3_web
            cell.setCellStyle(estilo3);
            cell.setCellValue("Valor Web");
            
            cell = row.createCell((short)(30));// Tipo_recurso3_mims
            cell.setCellStyle(estilo3);
            cell.setCellValue("Valor Mims");
            
            cell = row.createCell((short)(31));// Recurso3_web
            cell.setCellStyle(estilo3);
            cell.setCellValue("Valor Web");
            
            cell = row.createCell((short)(32));// Recurso3_mims
            cell.setCellStyle(estilo3);
            cell.setCellValue("Valor Mims");
            
            cell = row.createCell((short)(33));// Tipo_recurso4_web
            cell.setCellStyle(estilo3);
            cell.setCellValue("Valor Web");
            
            cell = row.createCell((short)(34));// Tipo_recurso4_mims
            cell.setCellStyle(estilo3);
            cell.setCellValue("Valor Mims");
            
            cell = row.createCell((short)(35));// Recurso4_web
            cell.setCellStyle(estilo3);
            cell.setCellValue("Valor Web");
            
            cell = row.createCell((short)(36));// Recurso4_mims
            cell.setCellStyle(estilo3);
            cell.setCellValue("Valor Mims");
            
            cell = row.createCell((short)(37));// Tipo_recurso5_web
            cell.setCellStyle(estilo3);
            cell.setCellValue("Valor Web");
            
            cell = row.createCell((short)(38));// Tipo_recurso5_mims
            cell.setCellStyle(estilo3);
            cell.setCellValue("Valor Mims");
            
            cell = row.createCell((short)(39));// Recurso5_web
            cell.setCellStyle(estilo3);
            cell.setCellValue("Valor Web");
            
            cell = row.createCell((short)(40));// Recurso5_mims
            cell.setCellStyle(estilo3);
            cell.setCellValue("Valor Mims");
            
            /******************************************************************/
            /***** GUARDAR DATOS EN EL ARCHIVO *****/
            FileOutputStream fo = new FileOutputStream ( Ruta );
            wb.write ( fo );
            fo.close ();
        
        } catch ( Exception e ) {    
            
            ////System.out.println ( "ERROR EN 'exportar' - [ControlSTDJOB]" );
            e.printStackTrace ();
            
        }
                
    }
    
}