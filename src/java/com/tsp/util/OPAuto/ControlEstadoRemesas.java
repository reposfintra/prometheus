/*
 * Nombre        ControlEstadoRemesas.java
 * Autor         LREALES
 * Fecha         1 de septiembre de 2006, 02:34 PM
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */

package com.tsp.util.OPAuto;

import java.io.*;
import java.sql.*;
import java.util.*;
import jxl.*;

import com.tsp.util.*;
import com.tsp.finanzas.contab.model.beans.*;// PlanDeCuentas
import com.tsp.finanzas.presupuesto.model.beans.*;// JXLRead
import com.tsp.util.connectionpool.PoolManager;// Conexion BD
import com.tsp.operation.model.beans.POIWrite;// Generar Excel

import org.apache.poi.hssf.usermodel.*;// Generar Excel
import org.apache.poi.hssf.util.*;// Generar Excel

public class ControlEstadoRemesas extends Thread {
    
    private PlanDeCuentas bean;
    
    private Vector vector_imp_no_estan;
    private Vector vector_imp_est_dif;
    private Vector vector_imp_no_anuladas;
    
    private String Archivo;
    private String Hoja;
    
    /**************************************************************************/
    private String usuario = "SESCALANTE";
    private String fec_ini = "20060801";
    private String fec_fin = "20060904";
    /**************************************************************************/
    
    private static final String SQL_REMESA =
        "SELECT " +
            "numrem, estado, reg_status " +
        "FROM " +
            "remesa " +
        "WHERE " +
            "numrem = ? " ;

    private static final String SQL_MSF620 =
        "SELECT " +
            "WORK_ORDER                 REMESA, " +
            "COMPLETED_CODE          ESTADO " +
        "FROM " +
            "MSF620 " +
        "WHERE " +
            "LAST_MOD_DATE >= ? " +
            "AND LAST_MOD_DATE <= ? " ;//+
            //"AND COMPLETED_CODE IN ( 'AN', 'NF', 'CD' ) " ;//+
            //"AND ROWNUM < 10 " ;
        
    /**
     * Crea una nueva instancia de  ControlEstadoRemesas
     */
    public ControlEstadoRemesas () { }
    
    /**
     * Getter for property vector_imp_no_estan.
     * @return Value of property vector_imp_no_estan.
     */
    public java.util.Vector getVector_imp_no_estan() {
        return vector_imp_no_estan;
    }
    
    /**
     * Setter for property vector_imp_no_estan.
     * @param vector_imp_no_estan New value of property vector_imp_no_estan.
     */
    public void setVector_imp_no_estan(java.util.Vector vector_imp_no_estan) {
        this.vector_imp_no_estan = vector_imp_no_estan;
    }
    
    /**
     * Getter for property vector_imp_est_dif.
     * @return Value of property vector_imp_est_dif.
     */
    public java.util.Vector getVector_imp_est_dif() {
        return vector_imp_est_dif;
    }
    
    /**
     * Setter for property vector_imp_est_dif.
     * @param vector_imp_est_dif New value of property vector_imp_est_dif.
     */
    public void setVector_imp_est_dif(java.util.Vector vector_imp_est_dif) {
        this.vector_imp_est_dif = vector_imp_est_dif;
    }
        
    /**
     * Getter for property vector_imp_no_anuladas.
     * @return Value of property vector_imp_no_anuladas.
     */
    public java.util.Vector getvector_imp_no_anuladas() {
        return vector_imp_no_anuladas;
    }
    
    /**
     * Setter for property vector_imp_no_anuladas.
     * @param vector_imp_no_anuladas New value of property vector_imp_no_anuladas.
     */
    public void setvector_imp_no_anuladas(java.util.Vector vector_imp_no_anuladas) {
        this.vector_imp_no_anuladas = vector_imp_no_anuladas;
    }
    
    public static void main ( String[]abc ) throws Exception {
        
        try {
            
            ControlEstadoRemesas cer = new ControlEstadoRemesas ();
            cer.run();
            
            System.gc();
                
        } catch ( Exception ex ){
            
            ex.printStackTrace ();
            ////System.out.println( "Error en main( String[]abc ) --> " + ex.getMessage () );
            
        }
        
    }
        
    public synchronized void run () {
        
        com.tsp.operation.model.Model modelOperation = new com.tsp.operation.model.Model();
          
        try {
              
            System.gc();
            
            modelOperation.LogProcesosSvc.InsertProceso( "Control Estado Remesas", this.hashCode(), "Proceso de Reporte de Control de Estado de las Remesas", usuario );
            
            System.gc();
            ////System.out.println("INICIA PROCESO.. " + Util.fechaActualTIMESTAMP() );
            msf620 ( fec_ini, fec_fin );
            
            System.gc();
            
            exportar();
            ////System.out.println("FINALIZA PROCESO.. " + Util.fechaActualTIMESTAMP() );
            System.gc();
            
            modelOperation.LogProcesosSvc.finallyProceso( "Control Estado Remesas", this.hashCode(), usuario, "PROCESO EXITOSO" );
            
            System.gc();
            
        } catch ( Exception ex ) {
            
            try {
                
                ////System.out.println( "Error en run() ex --> " + ex.getMessage () );
                ex.printStackTrace();
                modelOperation.LogProcesosSvc.finallyProceso( "Control Estado Remesas", this.hashCode(), usuario, "ERROR : " + ex.getMessage() );
            
            } catch ( Exception e ){
                
                ////System.out.println( "Error en run() e --> " + e.getMessage () );
                e.printStackTrace();
                
            }            
            
        }
        
    }
    
    /**
     * Obtiene una lista de las remesas con estado 'AN', 'NF' y 'CD'.
     * @autor LREALES
     * @param la fecha inicial y la fecha final.
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void msf620 ( String fec_ini, String fec_fin ) throws SQLException {
        
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance ();
            con = poolManager.getConnection ( "oracle" );
            
            if( con != null ){
                
                st = con.prepareStatement ( this.SQL_MSF620 );
                
                st.setString ( 1, fec_ini );         
                st.setString ( 2, fec_fin ); 
                
                rs = st.executeQuery ();
                
                this.vector_imp_no_estan = new Vector ();
                
                while ( rs.next () ){
                    
                    String remesa = rs.getString(1) != null ? rs.getString(1).toUpperCase() : "";
                    String estado = rs.getString(2) != null ? rs.getString(2).toUpperCase() : "";
                    /*
                    bean = new PlanDeCuentas ();

                    bean.setNombre_largo ( remesa );// NUMERO REMESA
                    bean.setNombre_corto ( estado );// ESTADO REMESA
                    */
                    if ( existeRemesa ( remesa ) ) {
                        
                        buscarRemesa ( remesa, estado );
                        
                    } else { // GUARDA REMESAS Q NO EXISTEN EN POSTGRES
                        
                        this.vector_imp_no_estan.add ( remesa );
                    
                    }
                }
                
            }
            
        } catch ( SQLException e ){
            
            e.printStackTrace ();
            throw new SQLException( "ERROR EN 'msf620' - [ControlEstadoRemesas].. " + e.getMessage() + " " + e.getErrorCode() );
        
        }
        
        finally{
            
            if ( st != null ){
                try{
                    st.close ();
                } catch ( SQLException e ){
                    throw new SQLException ( "ERROR CERRANDO EL ESTAMENTO " + e.getMessage() );
                }
            }            
            if ( con != null ){
                poolManager.freeConnection ( "oracle", con );
            }
            
        }
        
    }
    
    /**
     * Se encarga de verificar si existe o no una remesa dada.
     * @autor LREALES
     * @param el numero de la remesa.
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public boolean existeRemesa ( String remesa ) throws SQLException{
        
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
                
        boolean sw = false;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( "sot" );
            
            if( con != null ){
                
                st = con.prepareStatement( this.SQL_REMESA );
                
                st.setString( 1, remesa ); 
                
                rs = st.executeQuery();
                
                if ( rs.next() ){
                    
                    sw = true;
                    
                }
                
            }
                        
        } catch( SQLException e ){
            
            e.printStackTrace();
            throw new SQLException( "ERROR EN 'existeRemesa' - [ControlEstadoRemesas].. " + e.getMessage() + " - " + e.getErrorCode() );
        
        }
        
        finally{
            
            if ( st != null ){
                try{
                    st.close();
                } catch( SQLException e ){
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO " + e.getMessage() );
                }
            }            
            if ( con != null ){
                poolManager.freeConnection( "sot", con );
            }
            
        }
        
        return sw;
        
    }
    
    /**
     * Se encarga de buscar la info de una remesa.
     * @autor LREALES
     * @param el numero de la remesa.
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void buscarRemesa ( String remesa, String estado ) throws SQLException {
        
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
                
        try {
            
            poolManager = PoolManager.getInstance ();
            con = poolManager.getConnection ( "sot" );
            
            if( con != null ) {
                
                st = con.prepareStatement ( this.SQL_REMESA );
                
                st.setString ( 1, remesa ); 
                
                rs = st.executeQuery ();
                
                this.vector_imp_est_dif = new Vector ();
                this.vector_imp_no_anuladas = new Vector ();
                
                if ( rs.next () ) {
                    
                    String numrem_pg = rs.getString( "numrem" ) != null ? rs.getString( "numrem" ).toUpperCase() : "";
                    String estado_pg = rs.getString( "estado" ) != null ? rs.getString( "estado" ).toUpperCase() : "";
                    String reg_status_pg = rs.getString( "reg_status" ) != null ? rs.getString( "reg_status" ).toUpperCase() : "";
                    /*
                    bean = new PlanDeCuentas ();

                    bean.setModulo1 ( numrem_pg );
                    bean.setModulo2 ( estado_pg );
                    bean.setModulo3 ( starem_pg );
                    bean.setModulo4 ( reg_status_pg );
                    */
                    if ( ! estado.equals ( estado_pg ) ) {
                        vector_imp_est_dif.add ( numrem_pg + " -> Postgres: " + estado_pg + " - Mims: " + estado );
                    }
                         
                    if ( estado_pg.equals("AN") || estado_pg.equals("NF") || estado_pg.equals("CD") ) {
                        
                        if ( ! reg_status_pg.equals ( "A" ) ) {
                            vector_imp_no_anuladas.add ( numrem_pg + "  -> Reg_status: " + reg_status_pg );
                        }
                        
                    }
                    
                }
                
            }
                        
        } catch ( SQLException e ) {
            
            e.printStackTrace();
            throw new SQLException( "ERROR EN 'buscarRemesa' - [ControlEstadoRemesas].. " + e.getMessage() + " - " + e.getErrorCode() );
        
        }
        
        finally {
            
            if ( st != null ) {
                try{
                    st.close ();
                } catch ( SQLException e ) {
                    throw new SQLException ( "ERROR CERRANDO EL ESTAMENTO " + e.getMessage() );
                }
            }            
            if ( con != null ) {
                poolManager.freeConnection ( "sot", con );
            }
            
        }
        
    }
        
    public synchronized void exportar() {
        
        try{
            
            Util u = new Util();
            
            ResourceBundle rb = ResourceBundle.getBundle( "com/tsp/util/connectionpool/db" );
            String path = rb.getString( "ruta" );
                        
            File file = new File( path + "/exportar/migracion/" + usuario );
            file.mkdirs();
               
            String fecha = Util.getFechaActual_String(6);            
            fecha=fecha.replaceAll( "/", "-" );
            fecha=fecha.replaceAll( ":", "_" );
            
            String nombreArch  = "ControlEstadoRemesas[" + fecha + "].xls";
            String       Hoja  = "ControlEstadoRemesas";
            String       Ruta  = path + "/exportar/migracion/" + usuario + "/" +nombreArch; 
            
            HSSFWorkbook wb    = new HSSFWorkbook();
            HSSFSheet    sheet = wb.createSheet( Hoja );
            HSSFRow      row   = null;
            HSSFRow      row2  = null;
            HSSFCell     cell  = null;
            
            for ( int col = 0; col < 3 ; col++ ){ //COLUMNAS
                
                sheet.setColumnWidth( (short) col, (short) ( ( 50 * 8 ) / ( (double) 1 / 30 ) ) ); 
                
            }
            
            /****  ENCABEZADO Y DEFINICION DE ESTILOS ************************************************/
            
            /** ENCABEZADO GENERAL *******************************/
            HSSFFont  fuente1 = wb.createFont();
            fuente1.setFontName("verdana");
            fuente1.setFontHeightInPoints((short)(16)) ;
            fuente1.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            fuente1.setColor(HSSFColor.DARK_RED.index);
            
            HSSFCellStyle estilo1 = wb.createCellStyle();
            estilo1.setFont(fuente1);
            estilo1.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo1.setBottomBorderColor(HSSFColor.WHITE.index);
            estilo1.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo1.setLeftBorderColor  (HSSFColor.WHITE.index);
            estilo1.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo1.setRightBorderColor(HSSFColor.WHITE.index);
            estilo1.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo1.setTopBorderColor   (HSSFColor.WHITE.index);
            
            
            /** SUBTITULO *******************************/
            HSSFFont  fuenteX = wb.createFont();
            fuenteX.setFontName("verdana");
            fuenteX.setFontHeightInPoints((short)(11)) ;
            fuenteX.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            fuenteX.setColor(HSSFColor.DARK_RED.index);
            
            HSSFCellStyle estiloX = wb.createCellStyle();
            estiloX.setFont(fuenteX);
            estiloX.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estiloX.setBottomBorderColor(HSSFColor.WHITE.index);
            estiloX.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estiloX.setLeftBorderColor  (HSSFColor.WHITE.index);
            estiloX.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estiloX.setRightBorderColor(HSSFColor.WHITE.index);
            estiloX.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estiloX.setTopBorderColor   (HSSFColor.WHITE.index);
            
            /** TEXTO EN EL ENCABEAZADO *************************/
            HSSFFont  fuente2 = wb.createFont();
            fuente2.setFontName("verdana");
            fuente2.setFontHeightInPoints((short)(11)) ;
            fuente2.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            fuente2.setColor((short)(0x0));
            
            HSSFCellStyle estilo2 = wb.createCellStyle();
            estilo2.setFont(fuente2);
            estilo2.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo2.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            
            /** ENCABEZADO DE LAS COLUMNAS***********************/
            HSSFFont  fuente3 = wb.createFont();
            fuente3.setFontName("verdana");
            fuente3.setFontHeightInPoints((short)(9)) ;
            fuente3.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            fuente3.setColor(HSSFColor.BLACK.index);
            
            HSSFCellStyle estilo3 = wb.createCellStyle();
            estilo3.setFont(fuente3);
            estilo3.setFillForegroundColor(HSSFColor.GREY_25_PERCENT.index);
            estilo3.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo3.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo3.setBottomBorderColor(HSSFColor.BLACK.index);
            estilo3.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo3.setLeftBorderColor  (HSSFColor.BLACK.index);
            estilo3.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo3.setRightBorderColor(HSSFColor.BLACK.index);
            estilo3.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo3.setTopBorderColor   (HSSFColor.BLACK.index);
            estilo3.setAlignment(HSSFCellStyle.ALIGN_CENTER);            
                        
            /** TEXTO NORMAL ************************************/
            HSSFFont  fuente4 = wb.createFont();
            fuente4.setFontName("verdana");
            fuente4.setFontHeightInPoints((short)(9)) ;
            fuente4.setBoldweight(HSSFFont.BOLDWEIGHT_NORMAL);
            fuente4.setColor((short)(0x0));
            
            HSSFCellStyle estilo4 = wb.createCellStyle();
            estilo4.setFont(fuente4);
            estilo4.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo4.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo4.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo4.setBottomBorderColor(HSSFColor.BLACK.index);
            estilo4.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo4.setLeftBorderColor  (HSSFColor.BLACK.index);
            estilo4.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo4.setRightBorderColor(HSSFColor.BLACK.index);
            estilo4.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo4.setTopBorderColor   (HSSFColor.BLACK.index);
            
            /** NUMEROS ************************************/
            HSSFCellStyle estilo5 = wb.createCellStyle();
            estilo5.setFont(fuente4);
            estilo5.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo5.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo5.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo5.setBottomBorderColor(HSSFColor.BLACK.index);
            estilo5.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo5.setLeftBorderColor  (HSSFColor.BLACK.index);
            estilo5.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo5.setRightBorderColor(HSSFColor.BLACK.index);
            estilo5.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo5.setTopBorderColor   (HSSFColor.BLACK.index);
            estilo5.setAlignment(HSSFCellStyle.ALIGN_RIGHT);
            /****************************************************/
            
            HSSFFont fuente6 = wb.createFont();
            fuente6.setColor((short)0x0);  
            fuente6.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            
            HSSFCellStyle estilo6 = wb.createCellStyle();
            estilo6.setFont(fuente6);
            estilo6.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo6.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo6.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo6.setBottomBorderColor(HSSFColor.WHITE.index);
            estilo6.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo6.setLeftBorderColor  (HSSFColor.WHITE.index);
            estilo6.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo6.setRightBorderColor(HSSFColor.WHITE.index);
            estilo6.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo6.setTopBorderColor   (HSSFColor.WHITE.index);
            
            /****************************************************/  
            /* MONEDA */
            HSSFCellStyle estilo7 = wb.createCellStyle();
            estilo7.setFont(fuente4);
            estilo7.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo7.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo7.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo7.setBottomBorderColor(HSSFColor.BLACK.index);
            estilo7.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo7.setLeftBorderColor  (HSSFColor.BLACK.index);
            estilo7.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo7.setRightBorderColor(HSSFColor.BLACK.index);
            estilo7.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo7.setTopBorderColor   (HSSFColor.BLACK.index);
            estilo7.setAlignment(HSSFCellStyle.ALIGN_RIGHT);
            estilo7.setDataFormat(wb.createDataFormat().getFormat("$#,##0.00"));
            
            /* TEXTO NORMAL CENTRADO */
            HSSFCellStyle estilo9 = wb.createCellStyle();
            estilo9.setFont(fuente4);
            estilo9.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo9.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo9.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo9.setBottomBorderColor(HSSFColor.BLACK.index);
            estilo9.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo9.setLeftBorderColor  (HSSFColor.BLACK.index);
            estilo9.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo9.setRightBorderColor(HSSFColor.BLACK.index);
            estilo9.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo9.setTopBorderColor   (HSSFColor.BLACK.index);
            estilo9.setAlignment(HSSFCellStyle.ALIGN_CENTER);
            
            row  = sheet.createRow((short)(0));
            row  = sheet.createRow((short)(1));
            row  = sheet.createRow((short)(2));
            row  = sheet.createRow((short)(3));
            row  = sheet.createRow((short)(4));
            row  = sheet.createRow((short)(5));
            row  = sheet.createRow((short)(6));
            row  = sheet.createRow((short)(7));
            row  = sheet.createRow((short)(8));
            row  = sheet.createRow((short)(9));
            
            for ( int j = 0; j < 3; j++ ) { //COLUMNAS
                
                row  = sheet.getRow((short)(0));
                cell = row.createCell((short)(j));
                cell.setCellStyle(estilo1);
                row  = sheet.getRow((short)(1));
                cell = row.createCell((short)(j)); 
                cell.setCellStyle(estiloX);
                
            }
            
            row  = sheet.getRow((short)(0));
            cell = row.getCell((short)(0));
            cell.setCellValue("TRANSPORTES SANCHEZ POLO");
                        
            row  = sheet.getRow((short)(1));            
            cell = row.getCell((short)(0));            
            cell.setCellValue("Reporte de Control del Estado de las Remesas");
            
            for( int i = 2; i < 5; i++ ){ //FILAS
                 
                for ( int j = 0; j < 3; j++ ) { //COLUMNAS
                    
                    row  = sheet.getRow((short)(i));
                    cell = row.createCell((short)(j)); 
                    cell.setCellStyle(estilo6);
                    
                }      
                
            }
            
            //FECHA
            row = sheet.getRow((short)(3));  
            cell = row.createCell((short)(0));            
            cell = row.getCell((short)(0));  
            cell.setCellStyle(estilo6);
            cell.setCellValue("FECHA DEL REPORTE:");
            cell = row.createCell((short)(1));            
            cell = row.getCell((short)(1));            
            cell.setCellStyle(estilo6);
            cell.setCellValue(Util.getFechaActual_String(7));
            
            cell = row.createCell((short)(2));            
            cell = row.getCell((short)(2));            
            cell.setCellStyle(estilo6);
            cell.setCellValue("Rango Final: " + fec_fin);
            row = sheet.getRow((short)(2)); 
            cell = row.createCell((short)(2));            
            cell = row.getCell((short)(2));  
            cell.setCellStyle(estilo6);
            cell.setCellValue("Rango Inicial: " + fec_ini);
            
            /*************************************************************************************/
            /***** RECORRER LOS DATOS ******/
            if ( vector_imp_no_estan != null ) {
                
                int Fila_No_Estan = 5;            
                for ( int vine = 0; vine < vector_imp_no_estan.size(); vine++ ){

                    String no_estan = ( String ) vector_imp_no_estan.elementAt( vine );

                    Fila_No_Estan++;
                    row  = sheet.createRow( ( short )( Fila_No_Estan ) );

                    cell = row.createCell( ( short )( 0 ) );//
                    cell.setCellStyle( estilo4 );
                    cell.setCellValue( no_estan );

                }//end for datos
                
            }
            
            if ( vector_imp_est_dif != null ) {
                
                int Fila_Estados_Diferentes = 5;            
                for ( int vied = 0; vied < vector_imp_est_dif.size(); vied++ ){

                    String est_dif = ( String ) vector_imp_est_dif.elementAt( vied );

                    Fila_Estados_Diferentes++;
                    row  = sheet.createRow( (short)(Fila_Estados_Diferentes) );

                    cell = row.createCell( (short)(1) );//
                    cell.setCellStyle( estilo4 );
                    cell.setCellValue( est_dif );

                }//end for datos
            
            }
            
            if ( vector_imp_no_anuladas != null ) {
                
                int Fila_No_Anuladas = 5;
                for ( int vina = 0; vina < vector_imp_no_anuladas.size(); vina++ ){

                    String no_anuladas = ( String ) vector_imp_no_anuladas.elementAt( vina );

                    Fila_No_Anuladas++;
                    row  = sheet.createRow( (short)(Fila_No_Anuladas) );

                    cell = row.createCell( (short)(2) );//
                    cell.setCellStyle( estilo4 );
                    cell.setCellValue( no_anuladas );

                }//end for datos
                 
            }
            
            row = sheet.createRow((short)(5));
            
            row = sheet.getRow( (short)(5) );     
            
            cell = row.createCell((short)(0));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Remesas que deberian estar");
            
            cell = row.createCell((short)(1));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Estados Diferentes");
            
            cell = row.createCell((short)(2));
            cell.setCellStyle(estilo3);
            cell.setCellValue("No Anuladas en Reg_status");
                        
            /*************************************************************************************/
            /**** GUARDAR DATOS EN EL ARCHIVO  ***/
            FileOutputStream fo = new FileOutputStream ( Ruta );
            wb.write ( fo );
            fo.close ();
        
        } catch ( Exception e ) {    
            
            ////System.out.println ( "ERROR EN 'exportar' - [ControlEstadoRemesas]" );
            e.printStackTrace ();
            
        }
                
    }
    
}
