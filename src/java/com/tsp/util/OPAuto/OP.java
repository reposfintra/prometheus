/***************************************
 * Nombre Clase ............. OP.java
 * Descripci�n  .. . . . . .  Permite Gencapsular los datos necesarios de la OP.
 * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
 * Fecha . . . . . . . . . .  20/10/2005
 * versi�n . . . . . . . . .  1.0
 * Copyright ...Transportes Sanchez Polo S.A.
 *******************************************/


package com.tsp.util.OPAuto;


import java.util.*;


public class OP extends CXP_Doc{
    
    private String  oc;
    private String  placa;
    private String  nameProveedor;
    private String  fechaCumplido;
    private double  vlrOc;
    private double  vlrOc_me;
    private String  monedaOC;
    private List    items;
    
    private double  vlrOcNeto;
    private double  vlrMov;
    private double  vlrNeto;
    
    private String cuentaContable;
    private String abc;
    
    private String reteFuente;
    private String reteIva;
    private String reteIca;
    
    private String retenedor;
    private String contribuyente;
    
    private String codeOrigenOC;
    
    private String regStatus;
    private String ot;
    private String std;
    private double vlrUnitario;
    
    //sescalante 27-03-2006
    private String tipocant;
    private String cantiCump;
    
    
    
    public OP() {
        items = null;
    }
    
    
    
    
    
    // SET:
    
    
    
    /**
     * M�todos que setea el regStatus de la oc
     * @autor.......    fvillacob
     * @parameter...... val
     * @version.....1.0.
     **/
    public void setOT( String val){
        this.ot = val;
    }
    
    
    
    /**
     * M�todos que setea el regStatus de la oc
     * @autor.......    fvillacob
     * @parameter...... val
     * @version.....1.0.
     **/
    public void setStd( String val){
        this.std = val;
    }
    
    
    
    
    /**
     * M�todos que setea la oc
     * @autor.......    fvillacob
     * @parameter...... val
     * @version.....1.0.
     **/
    
    public void setOc( String val){
        this.oc  = val;
    }
    
    
    
    /**
     * M�todos que setea el regStatus de la oc
     * @autor.......    fvillacob
     * @parameter...... val
     * @version.....1.0.
     **/
    
    public void setRegStatus( String val){
        this.regStatus  = val;
    }
    
    
    
    /**
     * M�todos que setea el valorUnitario de la oc
     * @autor.......    fvillacob
     * @parameter...... val
     * @version.....1.0.
     **/
    
    public void setVlrUnitarioOC( double val){
        this.vlrUnitario = val;
    }
    
    
    
    /**
     * M�todos que setea el valor placa
     * @autor.......    fvillacob
     * @parameter...... val
     * @version.....1.0.
     **/
    public void setPlaca( String val){
        this.placa = val;
    }
    
    
    /**
     * M�todos que setea el valor fechaCumplido
     * @autor.......    fvillacob
     * @parameter...... val
     * @version.....1.0.
     **/
    public void setFechaCumplido( String val){
        this.fechaCumplido = val;
    }
    
    
    /**
     * M�todos que setea el valor nombreProveedor
     * @autor.......    fvillacob
     * @parameter...... val
     * @version.....1.0.
     **/
    public void setNameProveedor( String val){
        this.nameProveedor = val;
    }
    
    
    /**
     * M�todos que setea el valor oc
     * @autor.......    fvillacob
     * @parameter...... val
     * @version.....1.0.
     **/
    public void setVlrOc( double val){
        this.vlrOc = val;
    }
    
    
    /**
     * M�todos que setea el valor oc
     * @autor.......    fvillacob
     * @parameter...... val
     * @version.....1.0.
     **/
    public void setVlrOc_Me( double val){
        this.vlrOc_me = val;
    }
    
    
    
    /**
     * M�todos que setea el valor moneda oc
     * @autor.......    fvillacob
     * @parameter...... val
     * @version.....1.0.
     **/
    public void setMonedaOC( String val){
        this.monedaOC = val;
    }
    
    
    /**
     * M�todos que setea el item
     * @autor.......    fvillacob
     * @parameter...... val
     * @version.....1.0.
     **/
    public void setItem( List  list){
        this.items = list;
    }
    
    
    
    /**
     * M�todos que setea el valor  neto oc
     * @autor.......    fvillacob
     * @parameter...... val
     * @version.....1.0.
     **/
    public void setVlrOcNeto( double val){
        this.vlrOcNeto     = val;
    }
    
    
    /**
     * M�todos que setea el valor mov
     * @autor.......    fvillacob
     * @parameter...... val
     * @version.....1.0.
     **/
    public void setVlrMov( double val){
        this.vlrMov        = val;
    }
    
    
    /**
     * M�todos que setea el valor neto de op
     * @autor.......    fvillacob
     * @parameter...... val
     * @version.....1.0.
     **/
    public void setVlrNeto( double val){
        this.vlrNeto       = val;
    }
    
    
    /**
     * M�todos que setea el valor reteFurnte
     * @autor.......    fvillacob
     * @parameter...... val
     * @version.....1.0.
     **/
    public void setReteFuente( String val){
        this.reteFuente    = val;
    }
    
    
    /**
     * M�todos que setea el valor reteIva
     * @autor.......    fvillacob
     * @parameter...... val
     * @version.....1.0.
     **/
    public void setReteIva( String val){
        this.reteIva       = val;
    }
    
    
    /**
     * M�todos que setea el valor reteIca
     * @autor.......    fvillacob
     * @parameter...... val
     * @version.....1.0.
     **/
    public void setReteIca( String val){
        this.reteIca       = val;
    }
    
    
    /**
     * M�todos que setea el valor cuenta
     * @autor.......    fvillacob
     * @parameter...... val
     * @version.....1.0.
     **/
    public void setCuenta( String val){
        this.cuentaContable = val;
    }
    
    
    /**
     * M�todos que setea el valor abc
     * @autor.......    fvillacob
     * @parameter...... val
     * @version.....1.0.
     **/
    public void setABC( String val){
        this.abc            = val;
    }
    
    
    /**
     * M�todos que setea el valor retenedor
     * @autor.......    fvillacob
     * @parameter...... val
     * @version.....1.0.
     **/
    public void setRetenedor( String val){
        this.retenedor    = val;
    }
    
    
    /**
     * M�todos que setea el valor contribuyente
     * @autor.......    fvillacob
     * @parameter...... val
     * @version.....1.0.
     **/
    public void setContribuyente( String val){
        this.contribuyente   = val;
    }
    
    
    /**
     * M�todos que setea el valor origen oc
     * @autor.......    fvillacob
     * @parameter...... val
     * @version.....1.0.
     **/
    public void setOrigenOC( String val){
        this.codeOrigenOC  = val;
    }
    
    
    
    
    
    
    
    
    
    
    // GET:
    
    
    
    /**
     * M�todos que setea el regStatus de la oc
     * @autor.......    fvillacob
     * @parameter...... val
     * @version.....1.0.
     **/
    public String getOT(){
        return  this.ot ;
    }
    
    
    
    /**
     * M�todos que setea el regStatus de la oc
     * @autor.......    fvillacob
     * @parameter...... val
     * @version.....1.0.
     **/
    public String getStd(){
        return  this.std ;
    }
    
    
    
    /**
     * M�todos que devuelve el valor oc
     * @autor.......    fvillacob
     * @version.....1.0.
     **/
    public String getOc( ){
        return this.oc ;
    }
    
    
    /**
     * M�todos que devuelve el status de  la oc
     * @autor.......    fvillacob
     * @version.....1.0.
     **/
    
    public String getRegStatus( ){
        return this.regStatus ;
    }
    
    
    
    /**
     * M�todos que devuelve el valorUnitario de la oc
     * @autor.......    fvillacob
     * @version.....1.0.
     **/
    public double getVlrUnitarioOC( ){
        return  this.vlrUnitario;
    }
    
    
    
    
    
    /**
     * M�todos que devuelve el valor placa
     * @autor.......    fvillacob
     * @version.....1.0.
     **/
    public String getPlaca( ){
        return this.placa ;
    }
    
    
    /**
     * M�todos que devuelve el valor fechaCumplida
     * @autor.......    fvillacob
     * @version.....1.0.
     **/
    public String getFechaCumplido( ){
        return this.fechaCumplido;
    }
    
    
    /**
     * M�todos que devuelve el valor nombreProveedor
     * @autor.......    fvillacob
     * @version.....1.0.
     **/
    public String getNameProveedor( ){
        return this.nameProveedor;
    }
    
    
    /**
     * M�todos que devuelve el valor oc
     * @autor.......    fvillacob
     * @version.....1.0.
     **/
    public double getVlrOc( ){
        return this.vlrOc ;
    }
    
    
    /**
     * M�todos que devuelve el valor oc_me
     * @autor.......    fvillacob
     * @version.....1.0.
     **/
    public double getVlrOc_Me( ){
        return this.vlrOc_me ;
    }
    
    
    
    
    /**
     * M�todos que devuelve el valor Moneda oc
     * @autor.......    fvillacob
     * @version.....1.0.
     **/
    public String getMonedaOC( ){
        return this.monedaOC ;
    }
    
    
    /**
     * M�todos que devuelve el valor item
     * @autor.......    fvillacob
     * @version.....1.0.
     **/
    public List   getItem( ){
        return this.items ;
    }
    
    
    /**
     * M�todos que devuelve el valor neto oc
     * @autor.......    fvillacob
     * @version.....1.0.
     **/
    public double getVlrOcNeto( ){
        return this.vlrOcNeto ;
    }
    
    
    /**
     * M�todos que devuelve el valor mov
     * @autor.......    fvillacob
     * @version.....1.0.
     **/
    public double getVlrMov( ){
        return this.vlrMov ;
    }
    
    
    /**
     * M�todos que devuelve el valor neto de la op
     * @autor.......    fvillacob
     * @version.....1.0.
     **/
    public double getVlrNeto( ){
        return this.vlrNeto ;
    }
    
    
    /**
     * M�todos que devuelve el valor reteFuente
     * @autor.......    fvillacob
     * @version.....1.0.
     **/
    public String getReteFuente( ){
        return this.reteFuente ;
    }
    
    /**
     * M�todos que devuelve el valor reteIva
     * @autor.......    fvillacob
     * @version.....1.0.
     **/
    public String getReteIva( ){
        return this.reteIva ;
    }
    
    
    /**
     * M�todos que devuelve el valor reteIca
     * @autor.......    fvillacob
     * @version.....1.0.
     **/
    public String getReteIca( ){
        return this.reteIca ;
    }
    
    
    /**
     * M�todos que devuelve el valor cuenta
     * @autor.......    fvillacob
     * @version.....1.0.
     **/
    public String getCuenta( ){
        return this.cuentaContable ;
    }
    
    
    /**
     * M�todos que devuelve el valor abc
     * @autor.......    fvillacob
     * @version.....1.0.
     **/
    public String getABC( ){
        return this.abc ;
    }
    
    
    /**
     * M�todos que devuelve el valor retenedor
     * @autor.......    fvillacob
     * @version.....1.0.
     **/
    public String getRetenedor( ){
        return this.retenedor    ;
    }
    
    
    /**
     * M�todos que devuelve el valor contribuyente
     * @autor.......    fvillacob
     * @version.....1.0.
     **/
    public String getContribuyente(){
        return this.contribuyente  ;
    }
    
    /**
     * M�todos que devuelve el valor origen oc
     * @autor.......    fvillacob
     * @version.....1.0.
     **/
    public String getOrigenOC( ){
        return  this.codeOrigenOC ;
    }
    
    
    /**
     * M�todos que forma el primer item de la op
     * @autor.......    fvillacob
     * @version.....1.0.
     **/
    
    public  OPItems getItemOC(){
        OPItems item = new OPItems();
        item.setDstrct         ( this.getDstrct()         );
        item.setProveedor      ( this.getProveedor()      );
        item.setTipo_documento( this.getTipo_documento() );
        item.setDocumento      ( this.getDocumento()      );
        item.setItem           ( "001"                    );
        item.setDescripcion    ( "00"                     );
        item.setPlanilla       ( this.getOc()             );
        item.setCodigo_cuenta  ( this.getCuenta()         );
        item.setCodigo_abc     ( this.getABC()            );
        item.setBase           ( this.getBase()           );
        item.setAsignador      ( "V"                      );  // al valor
        item.setIndicador      ( "V"                      );
        item.setProveedor      ( this.getProveedor()      );
        item.setTipo_documento( this.getTipo_documento() );
        item.setDocumento      ( this.getDocumento()      );
        item.setPlanilla       ( this.getOc()             );
        item.setReteFuente     ( this.getReteFuente()     );
        item.setReteIca        ( this.getReteIca()        );
        item.setReteIva        ( this.getReteIva()        );
        item.setCodigo_cuenta  ( this.getCuenta()         );
        item.setCodigo_abc     ( this.getABC()            );
        item.setAgencia        ( this.getOrigenOC()       );
        item.setMoneda         ( this.getMoneda()         );
        item.setDescconcepto   ( "VALOR PLANILLA " + this.getOc() );//sescalante
        return item;
    }
    
    /**
     * Getter for property tipocant.
     * @return Value of property tipocant.
     */
    public java.lang.String getTipocant() {
        return tipocant;
    }    
    
    /**
     * Setter for property tipocant.
     * @param tipocant New value of property tipocant.
     */
    public void setTipocant(java.lang.String tipocant) {
        this.tipocant = tipocant;
    }
    
    /**
     * Getter for property cantiCump.
     * @return Value of property cantiCump.
     */
    public java.lang.String getCantiCump() {
        return cantiCump;
    }
    
    /**
     * Setter for property cantiCump.
     * @param cantiCump New value of property cantiCump.
     */
    public void setCantiCump(java.lang.String cantiCump) {
        this.cantiCump = cantiCump;
    }
    
}
