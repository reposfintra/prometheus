/********************************************************************
 *      Nombre Clase.................   GenerarOPAuto.java
 *      Descripci�n..................   Generaci�n de OP en Proceso Autom�tico
 *      Autor........................   Ing. Andr�s Maturana De La Cruz
 *      Fecha........................   5 de junio de 2006
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes S�nchez Polo S.A.
 *******************************************************************/

package com.tsp.util.OPAuto;

import java.util.*;
import java.sql.*;
import com.tsp.util.connectionpool.PoolManager;
import java.sql.*;
import java.io.*;
import java.text.SimpleDateFormat;

import com.tsp.util.PropertyReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.DriverManager;

/**
 *
 * @author  Ing. Andr�s Maturana De La Cruz
 */
public class GenerarOPAuto {
    private static final String TIPO_DOC       = "010"; //FACTURA
    private static final String PRE_DOC        = "OP";   //prefijo de la OP
    private static final String DESC_OP        = "LIQUIDACION OC: "; //prefijo de la descripcion de la OP
    private static final String APROBADOR      = "AUTPROCESS"; //valor definido para OPs automaticas
    public  static       double TASA           = 0;
    public  static final String COLOMBIA       = "FINV";   
    OPImpuestos     IMP_RETEFUENTE;
    OPImpuestos     IMP_RETEIVA;
    OPImpuestos     IMP_RETEICA;
    public  static final String CODERETEFUENTE = "RT01"; 
    public  static final String RETEIVA        = "RIVA"; //tipo de impuesto RETEIVA
    public  static final String RETEICA        = "RICA"; //tipo de impuesto RETEICA    
    public  static final String RETEFUENTE     = "RFTE";
    
    private  FileWriter      fw;
    private  BufferedWriter  bf;
    private  PrintWriter     linea;

    Connection  con = null;
    PrintWriter logger = null;
    
    /** Crea una nueva instancia de  GenerarOPAuto */
    public GenerarOPAuto() {
    }
    
    public static void main(String[] args)throws Exception  {
        try{
            GenerarOPAuto vs = new GenerarOPAuto();
            vs.run();
        }catch (Exception ex){
            ////System.out.println("ERROR:" + ex.getMessage());
            ex.printStackTrace();
        }
    }
    
    public void run() throws Exception {
        try{
            // cargamos el archivo de configuraci�n
            PropertyReader pr = PropertyReader.obtenerInstancia(GenerarOPAuto.class.getResource("config.properties"));
            File localHost = new File(pr.obtenerTexto("localLogPath"));
            File webServer = new File(pr.obtenerTexto("webServerLogPath"));
            if( localHost.exists() ){// si la ruta local existe
                logger = new PrintWriter(
                        new BufferedWriter(
                            new FileWriter(localHost.getPath() + "/" + "sot.log", true)
                        ),
                        true );
                ////System.out.println("INICIO DEL PROCESO GENERACION DE OP AUTOMATICO");
            }
            else if( webServer.exists() ){// si la ruta del servidor existe
                logger = new PrintWriter(
                        new BufferedWriter(
                            new FileWriter(webServer.getPath() + "/" + "sot.log", true)
                        ),
                        true );
                ////System.out.println("INICIO DEL PROCESO GENERACION DE OP AUTOMATICO");
            }
            else {// sino entonces imprimimos los detalles por la consola
                logger = new PrintWriter(System.out);
                ////System.out.println("INICIO DEL PROCESO GENERACION DE OP AUTOMATICO");
            }
            
            logger.println("INICIO DEL PROCESO GENERACION DE OP AUTOMATICO");
            long inicio = System.currentTimeMillis();
            // nos conectamos a la bd
            Class.forName(pr.obtenerTexto("driverClass"));
            con = DriverManager.getConnection(  pr.obtenerTexto("db_url"),
                        pr.obtenerTexto("db_user"),
                        pr.obtenerTexto("db_pass")
                    );
            
            
            //obtene OCs cuyo proveedor no se encuentra resgitrado
            List lista = this.getOPsinProvRegistrado();
            List provs = new LinkedList();
            if (lista != null && lista.size()> 0){
                //exclusion de proveedores repetidos
                provs = this.getExcluirRepetidos(lista);
            }
            
            if ( lista != null && lista.size() > 0){
                //generamos txt en el directorio de archivos y envio de mail
                this.NProveedoresNoReg(lista, provs, "AUTOPROCESS", pr.obtenerTexto("ruta"));
            }
            
            //generacion de OPs
            List listOp = generateOP();
            listOp = recorrerLista(listOp);
            
            
            if (listOp != null && listOp.size() > 0){
                insertList(listOp, "AUTPROCESS", "file");
            }
            
            ////System.out.println("........... TERMINO EL PROCESO");
            logger.println("FIN DEL PROCESO GENERACION DE OP AUTOMATICO");
        } catch(Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        } finally {
            if ( con != null ) {con.close();}
        }
        
    }
    
    
    /**
     * M�todo getOPsinProvRegistrado, obtiene las planillas cuyo nitpro no se encuentra registrado en proveedor
     * @autor   fvillacob
     * @param   Fecha inicial, fecha final (String)
     * @return  List
     * @throws  Exception
     * @version 1.0.
     **/
    public List getOPsinProvRegistrado() throws Exception{
        PreparedStatement st        = null;
        ResultSet         rs        = null;
        List              lista     = null;
        try {
            String sql =
                    "SELECT numpla," +
                    "       nitpro " +
                    "FROM   planilla " +
                    "WHERE  nitpro NOT IN ( SELECT nit FROM proveedor ) " +
                    "       AND reg_status = 'C'";
            
            st= con.prepareStatement(sql);
            rs=st.executeQuery();
            
            //////System.out.println("OC sin prov "+ st);
            lista = new LinkedList();
            while(rs.next()){
                Planilla pla  = new Planilla();
                pla.setNumpla(rs.getString("numpla"));
                pla.setNitpro(rs.getString("nitpro"));
                lista.add(pla);
            }
        } catch(SQLException e){
            throw new SQLException(" DAO: Error en la busqueda de las planillas sin proveedor registrado en PROVEEDORES--> "+ e.getMessage());
        } finally{
            if ( rs != null ) {rs.close();}
            if ( st != null ) {st.close();}
        }
        return lista;
    }
    
    /**
     * M�todo getExcluirRepetidos, excluye proveedores repetidos de la lista
     * @autor sesclanate
     * @param lista de planillas y proveedores no registrados en sistemas
     * @return List
     * @throws Exception
     * @version 1.0.
     **/
    public List getExcluirRepetidos(List lista)throws Exception{
        List listProv = new LinkedList();
        try{
            
            if(lista!=null && lista.size()>0){
                for(int i=0;i<lista.size();i++){
                    Planilla p  = (Planilla)lista.get(i);
                    String prov  = p.getNitpro();
                    int    sw  = 0;
                    for(int j=0; j < listProv.size(); j++){
                        Planilla pl  = (Planilla)listProv.get(j);
                        if( pl.getNitpro().equals(prov) ) {
                            sw = 1;
                            break;
                        }
                    }
                    if(sw==0)
                        listProv.add(p);
                }
            }
            
        }catch( Exception e){
            e.printStackTrace();
            throw new Exception(" getExcluirRepetidos: "+e.getMessage());
        }
        return listProv;
    }
    
    /**
     * M�todo getOP, busqueda de las planillas cumplidas en el rango de fecha dado
     * @autor   fvillacob
     * @param   Fecha inicial, fecha final (String)
     * @return  List
     * @throws  Exception
     * @version 1.0.
     **/
    public List generateOP() throws Exception{
        PreparedStatement st        = null;
        ResultSet         rs        = null;
        List              lista     = null;
        try {
            
            String sql =
                    "SELECT oc.*, " +
                    "       pr.payment_name as  name, " +
                    "       pr.id_mims as idmims, " +
                    "       bc.agency_id as agencia, " +
                    "       pr.branch_code as banco, " +
                    "       pr.bank_account_no as sucursal, " +
                    "       pr.hc as  handlecode, " +
                    "       pr.autoret_rfte as  retefuente, " +
                    "       pr.autoret_iva as  reteiva, " +
                    "       pr.autoret_ica as reteica,  " +
                    "       pr.gran_contribuyente as  gran_contribuyente, " +
                    "       pr.agente_retenedor as  agente_retenedor, " +
                    "       pr.plazo as  plazo,  " +
                    "       bc.currency as  monedabanco, " +
                    "       now() as fechadocumento, " +
                    "      (to_char(to_date( now() ,'YYYY-MM-DD HH24:MI:SS')+(  case when pr.plazo=1 then 7 else pr.plazo end ::int),'YYYY-MM-DD HH24:MI:SS')) as fechavencimiento  " +
                    "FROM (" +
                    "   SELECT  distinct(a.numpla ) as  oc, " +
                    "           a.cia as distrito, " +
                    "           case when (a.vlrpla=0) then  a.vlrpla2 else  a.vlrpla end as valoroc, " +
                    "           a.currency as monedaoc, " +
                    "           a.nitpro as propietario, " +
                    "           a.plaveh as placa, " +
                    "           a.feccum as feccum, " +
                    "           SUBSTR(c.unit_of_work,1,3) as unidad, " +
                    "           SUBSTR(c.cliente,4,3) as cliente, " +
                    "           a.oripla as origenoc, " +
                    "           a.reg_status as reg_status, " +
                    "           c.numrem as ot, " +
                    "           c.std_job_no as std, " +
                    "           a.unit_cost as unit_cost, " +
                    "           a.base as base, " +
                    "           a.unit_vlr as tipo_cant, " +
                    "           substr(c.cliente,4,3) as cliente, " +
                    "           substr(b.account_code_c,1,6) as cuenta" +
                    "   FROM " +
                    "           planilla  a, " +
                    "           plarem    b, " +
                    "           remesa    c " +
                    "   WHERE   " +
                    "           a.reg_status = 'C' " +
                    "           AND  a.base != '' " +
                    "           AND  trim (a.factura) = '' " +
                    "           AND  a.numpla = b.numpla " +
                    "           AND  c.numrem = b.numrem " +
                    "           AND  c.estado != 'AN' )oc  " +
                    "JOIN  proveedor     pr   ON  ( oc.propietario = pr.nit ) " +
                    "LEFT JOIN   banco   bc   ON  ( bc.branch_code = pr.branch_code AND bc.bank_account_no =  pr.bank_account_no ) " +
                    "";
            
            st= con.prepareStatement(sql);
            rs=st.executeQuery();
            //////System.out.println("PLANILLA "+ st);
            while(rs.next()){
                if( lista==null) lista = new LinkedList();
                OP  op  = this.loadOp(rs);
                lista.add(op);
            }
        } catch(SQLException e){
            throw new SQLException(" DAO: Error en la busqueda de las planillas sin proveedor registrado en PROVEEDORES--> "+ e.getMessage());
        } finally{
            if ( rs != null ) {rs.close();}
            if ( st != null ) {st.close();}
        }
        return lista;
    }
    
    /**
     * M�todo loadOP, instancia un objeto OP con resultado de la consulta
     *  Utilizado en Generacion de OP y Liquidacion de OC
     * @autor   fvillacob
     * @param   Resultado de la consulta (ResulSet)
     * @return  OP
     * @throws  Exception
     * @version 1.0.
     **/
    public OP  loadOp(ResultSet  rs) throws Exception{
        OP  op = new OP();
        try{
            op.setDstrct            ( getValor  ( rs.getString("distrito")        ));
            op.setOc                ( getValor  ( rs.getString("oc")              ));
            op.setVlrOc             ( rs.getDouble("valorOc") );
            op.setMonedaOC          ( getValor( rs.getString("monedaOc")          ));
            op.setProveedor         ( getValor( rs.getString("propietario")       ));
            op.setPlaca             ( getValor( rs.getString("placa")             ));
            op.setFechaCumplido     ( getValor( rs.getString("feccum")            ));
            op.setNameProveedor     ( getValor( rs.getString("name")              ));
            op.setId_mims           ( getValor( rs.getString("idMims")            ));
            op.setAgencia           ( getValor( rs.getString("agencia")           ));
            op.setBanco             ( getValor( rs.getString("banco")             ));
            op.setSucursal          ( getValor( rs.getString("sucursal")          ));
            op.setHandle_code       ( getValor( rs.getString("handleCode")        ));
            op.setPlazo             ( rs.getInt("plazo") );
            op.setMoneda            ( getValor( rs.getString("monedaBanco")       ));
            op.setFecha_documento   ( getValor( rs.getString("fechaDocumento")    ));
            op.setFecha_vencimiento( getValor( rs.getString("fechaVencimiento")  ));
            op.setReteFuente        ( getValor( rs.getString("reteFuente")        ));
            op.setReteIca           ( getValor( rs.getString("reteIca")           ));
            op.setReteIva           ( getValor( rs.getString("reteIva")           ));
            op.setRetenedor         ( getValor( rs.getString("agente_retenedor")  ));
            op.setContribuyente     ( getValor( rs.getString("gran_contribuyente")));
            op.setOrigenOC          ( getValor( rs.getString("origenOC")          ));
            op.setRegStatus         ( getValor( rs.getString("reg_status")        ));
            op.setOT                ( getValor( rs.getString("ot")                ));
            op.setStd               ( getValor( rs.getString("std")               ));
            op.setVlrUnitarioOC     ( rs.getDouble("unit_cost")                    );
            op.setBase              ( getValor(rs.getString("base"))               );
            op.setTipo_documento    ( this.TIPO_DOC                );
            
            String OP =  buscarNumeroFactura(op.getDstrct(), op.getProveedor(),this.PRE_DOC + op.getOc() );
            
            op.setDocumento         ( OP                           );
            op.setDescripcion       ( this.DESC_OP + op.getOc()    );
            op.setABC               ( this.getABC(op.getAgencia()) );
            String Cliente          = rs.getString("cliente");
            String Cuenta           = rs.getString("cuenta");
            op.setCuenta            ( getValor(Cuenta+""+Cliente+"8005"));
            op.setTipocant          ( getValor( rs.getString("tipo_cant")));//sescalante
        }catch(Exception e){
            throw new Exception(e.getMessage());
        }
        return op;
    }
    
    /**
     * M�todo getValor, vsetea el valor de los campos
     * @autor   fvillacob
     * @param   valor (String)
     * @return  String
     * @throws  Exception
     * @version 1.0.
     **/
    public String getValor(String valor){
        String cadena = " ";
        if( valor!=null && !valor.trim().equals("") )
            cadena = valor;
        return cadena;
    }
    
    /**
     * M�todo buscarNumeroFactura,Busca numero de la factura siguiente si existe
     * @autor   fvillacob
     * @param
     * @return  List
     * @throws  Exception
     * @version 1.0.
     **/
    public String buscarNumeroFactura(String Distrito,String Proveedor, String OP) throws Exception{
        PreparedStatement st        = null;
        ResultSet         rs        = null;
        String             Codigo   = "";
        try {
            String sql =
                    "SELECT * " +
                    "FROM fin.cxp_doc " +
                    "WHERE  " +
                    "       dstrct = ? " +
                    "       AND proveedor =? " +
                    "       AND tipo_documento='010' " +
                    "       AND documento =?";
            st= con.prepareStatement(sql);
            st.setString(1, Distrito );
            st.setString(2, Proveedor);
            st.setString(3, OP);
            rs = st.executeQuery();
            
            if(rs.next()){
                
                for(int i=1; i<= 2000;i++){
                    st.clearParameters();
                    st.setString(1, Distrito );
                    st.setString(2, Proveedor);
                    st.setString(3, OP+"_"+i);
                    rs = st.executeQuery();
                    if(!rs.next()){
                        Codigo= OP+"_"+i;
                        break;
                    }
                }
                
            }else{
                Codigo = OP;
            }
        } catch(SQLException e){
            throw new SQLException(" DAO: Error en la verificacion de la existencia de la op "+ e.getMessage());
        } finally{
            if ( rs != null ) {rs.close();}
            if ( st != null ) {st.close();}
        }
        return Codigo;
    }
    
    /**
     * M�todo getABC, devuelve el valor ABC para la agencia
     * @autor   fvillacob
     * @param   agencia (String)
     * @return  List
     * @throws  Exception
     * @version 1.0.
     **/
    public String getABC(String agencia) throws Exception{
        PreparedStatement st        = null;
        ResultSet         rs        = null;
        String            abc       = agencia;
        try {
            String sql = 
                    "SELECT  area_abc " +
                    "FROM    abc.abc_etapas " +
                    "WHERE   codigo_area = ? ";            
            st= con.prepareStatement(sql);
            st.setString(1, agencia);
            rs=st.executeQuery();
            while(rs.next())
                abc  = rs.getString(1);
        }
        catch(SQLException e){
            throw new SQLException(" DAO: No se pudo Buscar el ABC.-->"+ e.getMessage());
        } finally{
            if ( rs != null ) {rs.close();}
            if ( st != null ) {st.close();}
        }
        
        return abc;
    }
    
    
    /**
     * M�todo insertList, registra la OP y define el archivo excel para control de generacion de OP
     * @autor   fvillacob
     * @param lista de OP (List), usuario, ruta (String)
     * @throws  Exception
     * @version 1.0.
     **/
    public void insertList(List lista, String user, String url) throws Exception{
        try{
            
            /*this.Excel  = new POIWrite(url);
            this.configXLS();*/
            
            String sql1 = "";
            String sql2 = "";
            String sql3 = "";
            String sql4 = "";
            String sql5 = "";
            
            String sqlTodos = "";
            
            if(lista!=null  && lista.size()>0){
                
                Iterator it = lista.iterator();
                while(it.hasNext()){
                    OP  op  = (OP)it.next();
                    sql1 += insertOP(op, user);
                    
                    //Grabamos la OP al xls
                    //this.addOPExcel(op);
                    
                    //ITEMS DE LA OP
                    List items  = op.getItem();
                    if( items!=null && items.size()>0 ){
                        for(int i=0;i<items.size();i++){
                            OPItems item = (OPItems) items.get(i);
                            // Insertamos el Item:
                            sql2 += insertItem( item, user); 
                            
                            // Grabamos al archivo  el item de la op
                            //addItemOPExcel(item );
                            //incItem();
                            
                            // Insertamos sus Impuestos
                            List impuestos = item.getImpuestos();
                            if(impuestos!=null && impuestos.size()>0){
                                for(int j=0;j<impuestos.size();j++){
                                    OPImpuestos imp  = (OPImpuestos)impuestos.get(j);                                    
                                    
                                    double vlr_imp      = 0;
                                    double vlr_me_imp   = 0;
                                   
                                    if(imp.getTipo().equals("RFTE") ){
                                        vlr_imp = item.getVlrReteFuente();
                                    }
                                    if(imp.getTipo().equals("RICA") ){
                                        vlr_imp = item.getVlrReteIca();
                                    }
                                    
                                    vlr_me_imp =  getValorMoneda(item.getMoneda(), vlr_imp, op.getMoneda(), getFechaActual_String(4), item.getDstrct() );
                                    
                                    sql3 += insertImpItem( item.getDstrct(), item.getProveedor(), item.getTipo_documento(), item.getDocumento(), item.getItem(), imp.getCodigo(), imp.getPorcentaje(), vlr_imp , vlr_me_imp , user, item.getBase());
                                }
                            }
                        }
                    }
                    
                    //IMPUESTOS DE FACTURA:  agrupamos impuestos por item y lo insertamos.
                    //sql4 += searchImpByDoc(op, user);
                    
                    //ACTUALIZAR PLANILLA: la asignamos el numero de factura op correspondiente
                    sql5 += updatePlanilla(op);
                    
                    // Incremento fila de Excel
                    //inc();
                }
                
            }else{
                //sinDatos();
            }
            //Excel.cerrarLibro();
            
            sqlTodos = sql1 + sql2 + sql3 /*+ sql4*/ + sql5;
                       
            execute(sqlTodos);
            
            //IMPUESTOS DE FACTURA:  agrupamos impuestos por item y lo insertamos.
            if(lista!=null  && lista.size()>0){
                
                Iterator it = lista.iterator();
                while(it.hasNext()){
                    OP  op  = (OP)it.next();
                    sql4 += searchImpByDoc(op, user);
                }
            } 
            execute(sql4);
            
            
        }catch(Exception e){
            throw new Exception( " insertList: " + e.getMessage());
        }
    }
    
    /**
     * M�todo insertOP, registra la cabecera de la factura en cxp_doc
     * @autor   fvillacob
     * @param   orden de pago (OP), usuario (String)
     * @throws  Exception
     * @version 1.0.
     **/    
    public String insertOP(OP op,  String user) throws Exception{
        PreparedStatement st        = null;
        String sql                  = "";
        try{
            
            //--- CABECERA:
            sql = 
                    "INSERT INTO fin.cxp_doc (" +
                    "       dstrct, " +
                    "       proveedor, " +
                    "       tipo_documento, " +
                    "       documento, " +
                    "       descripcion, " +
                    "       agencia, " +
                    "       handle_code, " +
                    "       id_mims, " +
                    "       fecha_documento, " +
                    "       fecha_aprobacion, " +
                    "       aprobador, " +
                    "       fecha_vencimiento, " +
                    "       banco, " +
                    "       sucursal, " +
                    "       moneda, " +
                    "       vlr_neto, " +
                    "       vlr_saldo, " +
                    "       vlr_neto_me, " +
                    "       vlr_saldo_me, " +
                    "       tasa, " +
                    "       creation_user, " +
                    "       base, " +
                    "       usuario_aprobacion ) " +
                    "VALUES ( ?,?,?,?,?,?,?,?,?,now(),?,?,?,?,?,?,?,?,?,?,?,?,?)";
            
            
            st= con.prepareStatement(sql);
            st.setString(1,   op.getDstrct()            );
            st.setString(2,   op.getProveedor()         );
            st.setString(3,   op.getTipo_documento()    );
            st.setString(4,   op.getDocumento()         );
            st.setString(5,   op.getDescripcion()       );
            st.setString(6,   op.getAgencia()           );
            st.setString(7,   op.getHandle_code()       );
            st.setString(8,   op.getId_mims()           );
            st.setString(9,   op.getFecha_documento()   );
            st.setString(10,  this.APROBADOR            );
            st.setString(11,  (op.getFecha_vencimiento()==null || op.getFecha_vencimiento().trim().equals("") )?op.getFecha_documento() :op.getFecha_vencimiento()  );            
            st.setString(12,  op.getBanco()             );
            st.setString(13,  op.getSucursal()          );
            st.setString(14,  op.getMoneda()            );
            st.setDouble(15,  op.getVlrNeto()           );  // valor Neto
            st.setDouble(16,  op.getVlrNeto()           );  // saldo
            st.setDouble(17,  op.getVlr_neto_me()       );  // valor Neto_me
            st.setDouble(18,  op.getVlr_neto_me()       );  // saldo me
            st.setDouble(19,  op.getTasa()              );
            st.setString(20,  user                      );
            st.setString(21,  op.getBase()              );
            st.setString(22,  this.APROBADOR            );
            
            //////System.out.println("............. INSERT OP: " + st);
           // st.execute();
            sql = st.toString()+";";
            
        } catch(SQLException e){
            throw new SQLException(" DAO:(insertOP) No se pudo insertar la OP.-->"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
        }
        return sql;
    }
    
    /**
     * M�todo insertItem, registra los items de la factura
     * @autor   fvillacob
     * @param   item de la OP (OPItems), usuario (String)
     * @throws  Exception
     * @version 1.0.
     **/
    public String insertItem(  OPItems item,String user) throws Exception {
        PreparedStatement st        = null;
        String sql ="";
        try{
            sql = 
                    "INSERT INTO fin.cxp_items_doc ( " +
                    "   dstrct, " +
                    "   proveedor, " +
                    "   tipo_documento, " +
                    "   documento, " +
                    "   item, " +
                    "   descripcion, " +
                    "   vlr, " +
                    "   vlr_me, " +
                    "   codigo_cuenta, " +
                    "   codigo_abc, " +
                    "   planilla, " +
                    "   creation_user, " +
                    "   base  ) " +
                    "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?) ";
                
            st= con.prepareStatement(sql);
            st.setString(1,   item.getDstrct()         );
            st.setString(2,   item.getProveedor()      );
            st.setString(3,   item.getTipo_documento() );
            st.setString(4,   item.getDocumento()      );
            st.setString(5,   item.getItem()           );
            st.setString(6,   item.getDescripcion()+"-"+item.getDescconcepto() );
            st.setDouble(7,   item.getVlr()            );
            st.setDouble(8,   item.getVlr_me()         );
            st.setString(9,   item.getCodigo_cuenta()  );
            st.setString(10,  item.getCodigo_abc()     );
            st.setString(11,  item.getPlanilla()       );
            st.setString(12,  user                     );
            st.setString(13,  item.getBase()           );
            //st.execute();
            //////System.out.println("................. INSERT ITEM OP: " + st);
            sql = st.toString()+";";
            
        } catch(SQLException e){
            throw new SQLException(" DAO:(insertItem) No se pudo insertar el Item de la OP.-->"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
        } 
        return sql;
    }
    
    /**
     * M�todo getValorMoneda, realiza el proceso de convesion de un valor de una moneda a otra
     *  y define el valor de la tasa
     * @autor   fvillacob
     * @param   moneda original del valor (String), valor (double), moneda a convertir (string), fecha de creacion dela tasa (String), distrito de la OP (String)
     * @return  double
     * @throws  Exception
     * @version 1.0.
     **/
    
    public double getValorMoneda(String moneda,  double valor, String monedaToConvertir, String fecha, String dstrct) throws Exception{
        PreparedStatement st        = null;
        ResultSet         rs        = null;
        double     conversion       = valor;
        this.TASA                   = 1;
        String  monedaPeso          = "PES";
        try {
            moneda            = moneda.trim().toUpperCase();
            monedaToConvertir = monedaToConvertir.trim().toUpperCase();
            if(! moneda.equals(monedaToConvertir) ){
                
                if(fecha==null)
                    fecha = getHoy("-");
                
                // puente para paso de BOL <-> DOL
                if(!moneda.equals(monedaPeso) &&   !monedaToConvertir.equals(monedaPeso) ){
                    double vlr            = getValorMoneda(monedaPeso, valor, moneda,fecha, dstrct);
                    valor                 = valor * TASA;
                    conversion            = valor;
                    moneda                = monedaPeso;
                }
                
                String sql = 
                        "SELECT 1/vlr_conver " +
                        "FROM   tasa " +
                        "WHERE " +
                        "       cia = ? " +
                        "       AND moneda1 = ? " +
                        "       AND moneda2= ? " +
                        "       AND fecha  = ? " +
                        "       AND estado != 'A'";
                st= con.prepareStatement(sql);
                
                st.setString(1, dstrct);
                st.setString(2, moneda);
                st.setString(3, monedaToConvertir);
                st.setString(4, fecha);
                //////System.out.println("Primera busqueda de moneda " + st);
                rs = st.executeQuery();
                
                if (rs.next()){
                    this.TASA = rs.getDouble(1);
                }else {
                    sql = 
                            "SELECT  1/vlr_conver " +
                            "FROM    tasa " +
                            "WHERE   " +
                            "       cia = ?" +
                            "       AND moneda1 = ? " +
                            "       AND moneda2 = ? " +
                            "       AND estado != 'A' " +
                            "ORDER BY fecha";
                    
                    st= con.prepareStatement(sql);
                    st.setString(1, dstrct);
                    st.setString(2, moneda);
                    st.setString(3, monedaToConvertir);
                    //////System.out.println("Segunda busqueda de monesa " + st);
                    rs = st.executeQuery();
                    
                    if (rs.next()){
                        this.TASA = rs.getDouble(1);
                    }
                }
                conversion *=  this.TASA;
            }
        }
        catch(SQLException e){
            throw new SQLException(" DAO: No se pudo Convertir la Moneda.-->"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
        }
        return conversion;
    }
    
    public static String getHoy(String separador){
       String hoy = getDate(1) + separador + getDate(3) + separador + getDate(5);
       return hoy;
    }
    
    public static String getDate(int valor){
        SimpleDateFormat FMT = null;
        switch(valor)
        {
            case 0: FMT = new SimpleDateFormat("MMM");                 break; //Mes en espa�ol Ej: ene
            case 1: FMT = new SimpleDateFormat("yyyy");                break; //A�o en 4 digitos Ej: 2001
            case 2: FMT = new SimpleDateFormat("WW");                  break; //Semana del mes Ej: 02
            case 3: FMT = new SimpleDateFormat("MM");                  break; //Mes en 2 digitos Ej: 01
            case 4: FMT = new SimpleDateFormat("yyyy-MM-dd");          break; //A�o, Mes dia separados por '-' Ej: 2004-10-09
            case 5: FMT = new SimpleDateFormat("dd");                  break; //Dia en 2 digitos Ej: 02
            case 6: FMT = new SimpleDateFormat("yyyy/MM/dd kk:mm:ss"); break; //A�o, Mes dia separados por '/' y hora(24), minutos y segundos separado por ':' Ej: 2004/10/09 23:50:30
            case 7: FMT = new SimpleDateFormat("yyyy/MM/dd");          break; //A�o, Mes dia separados por '/' Ej: 2004/10/09
            case 8: FMT = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss"); break; //A�o, Mes dia separados por '-' y hora(24), minutos y segundos separado por ':' Ej: 2004-10-09 23:50:30
        }  
        String Fecha = FMT.format(new java.util.Date());
        return Fecha.toUpperCase();
    }  
    
    public static String getFechaActual_String(int valor) {
        SimpleDateFormat FMT = null;
        switch(valor) {
            case 0: FMT = new SimpleDateFormat("MMM");                 break; //Mes en espa�ol Ej: ene
            case 1: FMT = new SimpleDateFormat("yyyy");                break; //A�o en 4 digitos Ej: 2001
            case 2: FMT = new SimpleDateFormat("WW");                  break; //Semana del mes Ej: 02
            case 3: FMT = new SimpleDateFormat("MM");                  break; //Mes en 2 digitos Ej: 01
            case 4: FMT = new SimpleDateFormat("yyyy-MM-dd");          break; //A�o, Mes dia separados por '-' Ej: 2004-10-09
            case 5: FMT = new SimpleDateFormat("dd");                  break; //Dia en 2 digitos Ej: 02
            case 6: FMT = new SimpleDateFormat("yyyy/MM/dd kk:mm:ss"); break; //A�o, Mes dia separados por '/' y hora(24), minutos y segundos separado por ':' Ej: 2004/10/09 23:50:30
            case 7: FMT = new SimpleDateFormat("yyyy/MM/dd");          break; //A�o, Mes dia separados por '/' Ej: 2004/10/09
            case 8:
                FMT = new SimpleDateFormat( "yyyyMMdd" );
                break; //A�o, mes, dia sin separacion
        }
        String Fecha = FMT.format(new java.util.Date());
        return Fecha.toUpperCase();
    }
    
    /**
     * M�todo insertImpItem, registra los impuestos aplicados a los items
     * @autor   fvillacob
     * @param   distrito, proveedor, tipo de documento, documento, item, codigo del impuesto (String)
     *          porcentaje del impuesto, valor total del impuesto, valor total del impuesto me (double)
     *          usuario, base (String)
     * @throws  Exception
     * @version 1.0.
     **/
    public String insertImpItem(
            String dstrct ,String proveedor,String tipo_documento,String documento,String item ,String cod_impuesto ,
            double porcent_impuesto , double vlr_total_impuesto ,double vlr_total_impuesto_me ,String user ,String base
    ) throws Exception {
        
        PreparedStatement st        = null;
        String sql ="";
        try{
            sql = 
                    "INSERT INTO fin.cxp_imp_item  ( " +
                    "       dstrct, " +
                    "       proveedor, " +
                    "       tipo_documento, " +
                    "       documento, " +
                    "       item, " +
                    "       cod_impuesto, " +
                    "       porcent_impuesto, " +
                    "       vlr_total_impuesto, " +
                    "       vlr_total_impuesto_me, " +
                    "       creation_user, " +
                    "       base) VALUES (?,?,?,?,?,?,?,?,?,?,? )";
            
            st= con.prepareStatement(sql);
            st.setString(1,   dstrct);
            st.setString(2,   proveedor);
            st.setString(3,   tipo_documento);
            st.setString(4,   documento);
            st.setString(5,   rellenar(item,3) );
            st.setString(6,   cod_impuesto);
            st.setDouble(7,   porcent_impuesto);
            st.setDouble(8,   vlr_total_impuesto);
            st.setDouble(9,   vlr_total_impuesto_me);
            st.setString(10,  user);
            st.setString(11,  base);
            //st.execute();
            //////System.out.println("..................... INSERT IMPUESTO OP: " + st);
            sql = st.toString()+";";
            
        } catch(SQLException e){
            throw new SQLException(" DAO:(insertImpItem) No se pudo insertar Impuesto del Item de la OP.-->"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
        }
        return sql;
    }
    
    public static String rellenar(String cadena, int tope){
        int lon = cadena.length();
        if(tope>lon)
            for(int i=lon;i<tope;i++)
                cadena="0"+cadena;
        return cadena;
    }
    
    /**
     * M�todo searchImpByDoc, busca los impuestos aplicados a los items y los registra en la tabla cxp_imp_doc
     * @autor   fvillacob
     * @param   orden de pago (OP), usuario (String)
     * @return  List
     * @throws  Exception
     * @version 1.0.
     **/
    public String searchImpByDoc(OP op, String user) throws Exception{
        PreparedStatement st        = null;
        PreparedStatement st2       = null;
        ResultSet         rs        = null;
        String sql ="";     
        try{
            
            String sql_1 = 
                    "SELECT dstrct as distrito, " +
                    "       proveedor as proveedor, " +
                    "       tipo_documento as tipo, " +
                    "       documento as factura, " +
                    "       cod_impuesto as codigo, " +
                    "       porcent_impuesto as porcentaje, " +
                    "       sum(vlr_total_impuesto) as vlr, " +
                    "       sum(vlr_total_impuesto_me) as vlr_me " +
                    "FROM   fin.cxp_imp_item " +
                    "WHERE  " +
                    "       dstrct =? " +
                    "       AND proveedor=? " +
                    "       AND tipo_documento = ? " +
                    "       AND documento = ? " +
                    "GROUP BY dstrct, proveedor, tipo_documento, documento, cod_impuesto, porcent_impuesto;";  
            
            String sql_2 = 
                    "INSERT INTO fin.cxp_imp_doc (  " +
                    "       dstrct, " +
                    "       proveedor, " +
                    "       tipo_documento, " +
                    "       documento, " +
                    "       cod_impuesto, " +
                    "       porcent_impuesto, " +
                    "       vlr_total_impuesto, " +
                    "       vlr_total_impuesto_me, " +
                    "       creation_user, base) " +
                    "VALUES( ?,?,?,?,?,?,?,?,?,?); ";
            
            st  = con.prepareStatement(sql_1);
            st2 = con.prepareStatement(sql_2);
            
            st.setString(1, op.getDstrct()        );
            st.setString(2, op.getProveedor()     );
            st.setString(3, op.getTipo_documento());
            st.setString(4, op.getDocumento()     );
            
            //////System.out.println(".......... searchImpByDoc ---> " + st);
            
            rs=st.executeQuery();
            while(rs.next()){
                
                st2.setString(1,  op.getDstrct()            );
                st2.setString(2,  op.getProveedor()         );
                st2.setString(3,  op.getTipo_documento()    );
                st2.setString(4,  op.getDocumento()         );
                st2.setString(5,  rs.getString("codigo")    );
                st2.setDouble(6,  rs.getDouble("porcentaje"));
                st2.setDouble(7,  rs.getDouble("vlr")       );
                st2.setDouble(8,  rs.getDouble("vlr_me")    );
                st2.setString(9,  user);
                st2.setString(10, op.getBase());
                //////System.out.println(".......... searchImpByDoc2 ---> " + st);
                //st2.execute();
                sql += st2.toString()+";";
                st2.clearParameters();
            }
        }catch(SQLException e){
            throw new SQLException(" DAO: (searchImpByDoc) No se pudo obtener el impuesto-->"+ e.getMessage());
        }
        finally{
            if(st!=null)  st.close();
            if(st2!=null) st2.close();
            if(rs!=null)  rs.close();
        }
        return sql;
    }
    
    /**
     * M�todo updatePlanilla, registra el numero de la OP en el campo factura en planilla
     * @autor   fvillacob
     * @param   orden de pago (OP)
     * @throws  Exception
     * @version 1.0.
     **/
    public String updatePlanilla(OP op) throws Exception {
        PreparedStatement st        = null;
        String sql ="";
        try {
            sql = 
                    "UPDATE  planilla " +
                    "   SET  factura = ? " +
                    "WHERE   numpla = ?  ";
            st= con.prepareStatement(sql);
            st.setString(1, op.getDocumento());
            st.setString(2, op.getOc());
            //st.execute();
            sql = st.toString()+";";
        }
        catch(SQLException e){
            throw new SQLException(" DAO: No se pudo Actualizar Planilla.-->"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
        }
        return sql;
    }
    
    public void execute(String sql)throws SQLException{
        Statement st = null;
        
        try{
            st = con.createStatement();
            st.addBatch(sql);
            //////System.out.println("............... MEGA SQL: " + sql);
            
            if(st!=null){
                boolean autocommit = con.getAutoCommit();
                con.setAutoCommit(false);
                st.toString();
                st.executeBatch();
                con.commit();
                con.setAutoCommit(autocommit);
            }
            
        }
        catch(SQLException e){
            con.rollback();
            throw new SQLException("ERROR DURANTE LA TRANSACCION LOS CAMBIOS NO SE PRODUJERON EL LA BASE DE DATOS " + e.getMessage() + " " + e.getErrorCode()+" <br> La siguiente exception es : ----"+e.getNextException());
            
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
        }
        
    }
    
    /**
     * M�todo recorrerLista, realiza el proceso de liquidacion de OC
     * @autor fvillacob
     * @see liquidarOC - liquidarOCDAO
     * @param List lista de ocs
     * @return List
     * @throws Exception
     * @version 1.0.
     **/
    public List recorrerLista(List lista)throws  Exception{
        try{
            if(lista!=null && lista.size()>0 ){
                
                // Excluimos oc que se repiten ya sea por tener varias ots:
                lista = this.getExcluirRepetidosLiquidacion(lista);               
                
                for(int i=0;i<lista.size();i++){
                    OP op = (OP)lista.get(i);
                    //////System.out.println("PLANILA ----- " + op.getOc());
                    // Descuentos para las planillas de carbon
                    List descuentosCarbon = agregarDescuentos_movpla(op);
                    if(descuentosCarbon!=null){
                        deleteDescuentos(op.getStd(), op.getOc());
                        InsertMovimientoMovpla(descuentosCarbon );
                    }
                    
                    // Movimientos de la oc
                    List movimientos = getItemOP(op);
                    op.setItem(movimientos);
                    
                    // Realizamos calculos de Liquidacion:
                    op = this.formarliquidar(op);
                    //colocar la funcion patra excluir las que no se pueden ver en la jsp
                    
                }
            }
            
        }catch( Exception e){
            e.printStackTrace();
            throw new Exception(" recorrerLista: "+e.getMessage());
        }
        return lista;
    }
    
    /**
     * M�todo agreagrDescuentos_movpla, busca movimientos de la planilla segun el estadar (carbon)
     * @autor   fvillacob
     * @param   orden de pago (OP)
     * @return  List
     * @throws  Exception
     * @version 1.0.
     **/
    public List agregarDescuentos_movpla(OP op)throws Exception{
        PreparedStatement st         = null;
        ResultSet         rs         = null;
        List              descuentos = null;
        String            hoy        = getHoy("-");
        try {
            if( existeStd_stdjobsel(op.getStd()) ){
                String sql = 
                        "SELECT a.dstrct, " +
                        "       a.std_job_no, " +
                        "       a.concept_code, " +
                        "       b.ind_application, " +
                        "       a.type, " +
                        "       a.vlr, " +
                        "       a.currency " +
                        "FROM " +
                        "       tbldes a, " +
                        "       tblcon b " +
                        "WHERE " +
                        "       a.std_job_no = ? " +
                        "       AND  a.reg_status != 'A' " +
                        "       AND trim(b.concept_code) = trim(a.concept_code)";
                st= con.prepareStatement(sql);
                st.setString(1, op.getStd());
                //////System.out.println("MOV PLANILLA CARBON " + st);
                rs=st.executeQuery();
                int cont = 1;
                while(rs.next()){
                    if(descuentos==null)descuentos = new LinkedList();
                    
                    Movpla  mov = new Movpla();
                    mov.setDstrct          (  rs.getString("dstrct")        );
                    mov.setAgency_id       (  op.getAgencia()               );
                    mov.setDocument_type   (  "001"                         );
                    mov.setDocument        (  op.getStd()                   );
                    mov.setItem            (  rellenar(String.valueOf(cont),3) );
                    mov.setConcept_code    (  rs.getString("concept_code")  );
                    mov.setPlanilla        (  op.getOc()                    );
                    mov.setSupplier        (  op.getPlaca()                 );
                    mov.setDate_doc        (  hoy                           );
                    mov.setApplication_ind(  rs.getString("ind_application"));
                    mov.setApplicated_ind  ( "N" );
                    mov.setInd_vlr         (  rs.getString("type")           );
                    mov.setVlr             (  rs.getFloat("vlr")            );
                    mov.setCurrency        (  rs.getString("currency")       );
                    mov.setProveedor       (  op.getProveedor()              );
                    mov.setBranch_code     (  op.getBanco()                  );
                    mov.setBank_account_no(  op.getSucursal()               );
                    
                    descuentos.add(mov);
                    cont++;
                }
            }
            
        } catch(SQLException e){
            throw new SQLException(" DAO: agregarDescuentos_movpla.-->"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
        }
        return descuentos;
    }
    
    /**
     * M�todo existeStd_stdjobsel, verifica si un estandar es de carbon (existencia en la tabla stdjobsel)
     * @autor   fvillacob
     * @param   StandarJob (String)
     * @return  boolean
     * @throws  Exception
     * @version 1.0.
     **/
    public boolean existeStd_stdjobsel(String std)throws Exception{
        PreparedStatement st        = null;
        ResultSet         rs        = null;
        boolean           estado    = false;
        try {
            String sql = 
                    "SELECT sj " +
                    "FROM stdjobsel " +
                    "WHERE   sj = ? ";
            st = con.prepareStatement(sql);
            st.setString(1, std);
            //////System.out.println("Estandar de carbon " + st);
            rs=st.executeQuery();
            if(rs.next()){
                estado = true;
            }
        } catch(SQLException e){
            throw new SQLException(" DAO: existeStd_stdjobsel.-->"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
        }
        return estado;
    }
    
    /**
     * M�todo deleteDescuentos, elimina movimientos de planilla segun parametros
     * @autor   fvillacob
     * @param   StandarJob, numero de la planilla (String)
     * @throws  Exception
     * @version 1.0.
     **/
    public void deleteDescuentos(String std, String oc)throws Exception{
        PreparedStatement st        = null;
        try {
            String sql = 
                    "DELETE FROM  " +
                    "               movpla " +
                    "WHERE " +
                    "               planilla = ? " +
                    "               AND document = ?";
            st = con.prepareStatement(sql);
            st.setString(1, oc);
            st.setString(2, std);
            //////System.out.println("BORRAR MOVPLA CARBON " + st);
            st.execute();
            ////System.out.println("Eliminando Antiuos Movimientos de la OC....");
        } catch(SQLException e){
            throw new SQLException(" DAO: deleteDescuentos.-->"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
        }
    }
    
    /**
     * M�todo InsertMovimientoMovpla, registra movimientos de la planilla en movpla
     * @autor   fvillacob
     * @param   lista de movimientos (List)
     * @throws  Exception
     * @version 1.0.
     **/
    public void InsertMovimientoMovpla(List lista)throws Exception{
        PreparedStatement st         = null;
        try {
            ////System.out.println("Insertando Moviemintos actuales de la OC " + lista.size() );
            String sql =             
                    "INSERT INTO movpla ( " +
                    "       dstrct, " +
                    "       agency_id, " +
                    "       document_type, " +
                    "       document, " +
                    "       item, " +
                    "       concept_code, " +
                    "       planilla, " +
                    "       supplier, " +
                    "       date_doc, " +
                    "       applicated_ind, " +
                    "       application_ind, " +
                    "       ind_vlr, " +
                    "       vlr, " +
                    "       currency, " +
                    "       proveedor_anticipo, " +
                    "       branch_code, " +
                    "       bank_account_no) " +
                    "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ";
            
            st = con.prepareStatement(sql);
            
            for(int i=0;i< lista.size();i++){
                Movpla  mov = (Movpla) lista.get(i);
                st.setString(1 , mov.getDstrct()        );
                st.setString(2 , mov.getAgency_id()     );
                st.setString(3 , mov.getDocument_type() );
                st.setString(4 , mov.getDocument()      );
                st.setString(5 , mov.getItem()          );
                st.setString(6 , mov.getConcept_code()  );
                st.setString(7 , mov.getPlanilla()      );
                st.setString(8 , mov.getSupplier()      );
                st.setString(9 , mov.getDate_doc()      );
                st.setString(10, mov.getApplicated_ind());
                st.setString(11, mov.getApplication_ind());
                st.setString(12, mov.getInd_vlr()        );
                st.setFloat(13, mov.getVlr()            );
                st.setString(14, mov.getCurrency()       );
                st.setString(15, mov.getProveedor()      );
                st.setString(16, mov.getBranch_code()    );
                st.setString(17, mov.getBank_account_no());
                //////System.out.println("INSERTAR NUEVOS Moviemintos "  + st);
                st.execute();
                st.clearParameters();
            }
            
        } catch(SQLException e){
            throw new SQLException(" DAO: InsertMovimientoMovpla.-->"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
        }
    }
    
    /**
     * M�todo getItemOP, busca los movimientos de la planilla, instancia los items con la informacion
     *  resultante y los ingresa en una lista
     * @autor   fvillacob
     * @param   orden de pago (OP)
     * @return  List
     * @throws  Exception
     * @version 1.0.
     **/
    public List getItemOP( OP  op) throws Exception{
        PreparedStatement st        = null;
        ResultSet         rs        = null;
        List              lista     = null;
        try {
            
            ////System.out.println("Leyendo Movimientos.....");
            // Sacar los movimientos:
            String sql = 
                    "SELECT " +
                    "       a.document as cheque, " +
                    "       a.dstrct as distrito, " +
                    "       a.concept_code as concepto, " +
                    "       coalesce ( d.concept_desc, 'NO REGISTRA') as descripcion, " +
                    "       a.agency_id as agencia, " +
                    "       coalesce ( " +
                    "           case when (a.vlr=0) " +
                    "           then a.vlr_for * d.ind_signo  " +
                    "           else a.vlr * d.ind_signo end, 0 " +
                    "       ) as valor, " +
                    "       a.application_ind as asignador, " +
                    "       a.ind_vlr as indicador, " +
                    "       a.currency " +
                    "       as moneda, " +
                    "       get_codigocuenta(a.dstrct,a.proveedor_anticipo,a.concept_code,a.planilla) as cuenta, " +
                    "       a.proveedor_anticipo as proveedor " +
                    "FROM   " +
                    "       movpla a " +
                    "LEFT JOIN proveedor c ON (a.proveedor_anticipo = c.nit ) " +
                    "LEFT JOIN tblcon d ON ( a.concept_code = d.concept_code ) " +
                    "WHERE " +
                    "       a.planilla = ? " +
                    "       AND a.reg_status != 'A' " +
                    "ORDER BY a.ind_vlr desc";
            st= con.prepareStatement(sql);
            st.setString(1, op.getOc() );
            
            //////System.out.println("MOV DEFINITIVOS " + st);
            rs=st.executeQuery();
            int cont = 2;
            if (lista==null) lista = new LinkedList();
            while(rs.next()){
                
                OPItems  item = new OPItems();
                item.setItem         ( rellenar( String.valueOf(cont) ,3)    );
                item.setDstrct       ( this.getValor(rs.getString("distrito"))       );
                item.setDescripcion  ( this.getValor(rs.getString("concepto"))       );
                item.setDescconcepto( this.getValor(rs.getString("descripcion"))    );//sescalante
                item.setVlr          (               rs.getDouble("valor")           );
                item.setIndicador    ( this.getValor(rs.getString("indicador")  )    );
                item.setAsignador    ( this.getValor(rs.getString("asignador")  )    );
                item.setMoneda       ( this.getValor(rs.getString("moneda")     )    );
                item.setAgencia      ( this.getValor(rs.getString("agencia")    )    );
                item.setCodigo_cuenta( this.getValor(rs.getString("cuenta")     )    );
                item.setCodigo_abc   ( this.getABC  ( item.getAgencia())             );
                item.setCheque       ( (rs.getString("cheque")!=null)?rs.getString("cheque"):"");//ivan Gomez
                if( item.getIndicador().toUpperCase().equals("P") )
                    item.setMoneda( op.getMoneda());
                
                item.setProveedor     ( op.getProveedor()        );
                item.setTipo_documento( op.getTipo_documento()   );
                item.setDocumento     ( op.getDocumento()        );
                item.setPlanilla      ( op.getOc()               );
                item.setReteFuente    ( op.getReteFuente()       );
                item.setReteIca       ( op.getReteIca()          );
                item.setReteIva       ( op.getReteIva()          );
                item.setBase          ( op.getBase()             );//sescalante
                if(!(item.getDescripcion().equals("01") && item.getCheque().equals("")) ){//Ivan DArio Gomez Vanegas 2006-03-28
                    lista.add(item);
                    cont++;
                }
            }
        }
        catch(SQLException e){
            e.printStackTrace();
            throw new SQLException(" DAO: No se pudo buscar Items de OP.-->"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
        }
        return lista;
    }
    
    /**
     * M�todo formarliquidar, complementa la Op en la Liquidacion
     * @autor...fvillacob
     * @param op objeto resultado de la busqueda de la informacion para liquidar una oc
     * @return OP (orden de pago Objeto)
     * @throws..Exception
     * @version..1.0.
     **/
    public OP  formarliquidar(OP op) throws Exception{
        
        try{
            
            ////System.out.println("Proceso de Liquidacion.");
            
            //1. Obtenemos la cantidad cumplida  de la planilla segun la base
            double cantidad = 1;
            
            if (!op.getBase().equals("COL"))
                cantidad = buscarCantidadCumplidaCarbon(op.getOc());
            else
                cantidad = cantidadCumplida( op.getOc() );
            
            op.setCantiCump(String.valueOf(cantidad));
            
            //2. Define el valor de la OC, si esta cumplida se recalcula asi vlr = valor Unitario * cantidad            
            double valorPlanilla  = op.getVlrOc();
            //////System.out.println("VALOR DE LA PLANILLA NO CUMPLIDA " + valorPlanilla);
            
            if(op.getRegStatus().toUpperCase().equals("C")){
                valorPlanilla   = op.getVlrUnitarioOC() * cantidad;
                //////System.out.println("VALOR PLANILLA CUMPLIDA " + valorPlanilla + " Cantidad " + cantidad + " UNIT_COST " + op.getVlrUnitarioOC());
            }
            op.setVlrOc( valorPlanilla );
            
            
            //3. Coversion de la moneda de la OC Moneda OC -> Moneda Banco -> Moneda Local (cte PES)
            //////System.out.println("MONEDA BANCO " + monedaBanco + " MONEDA OC " + op.getMonedaOC() + " MONEDA LOCAL " + monedaLocal);            
            
            // Obtenemos el tipo de moneda local (moneda del distrito - tabla cia)
            String datosCia       = getDatosCia( op.getDstrct());
            String[] vec          = datosCia.split("-");
            String monedaLocal    = vec[0];
            
            double tasa           = 1;
            double vlrItemOc      = op.getVlrOc();
            double vlrItemOc_me   = vlrItemOc;
            String monedaBanco    = op.getMoneda();
            
            //conversion del Moneda OC -> Moneda Banco
            if( ! monedaBanco.equals( op.getMonedaOC() ) ){
                vlrItemOc   = getValorMoneda( op.getMonedaOC(), vlrItemOc, monedaBanco , getHoy("-"), op.getDstrct() );
                tasa        = this.TASA;
                op.setMonedaOC( monedaBanco );
            }
                         
            // convertir Moneda Banco -> Moneda Local 
            if( !monedaLocal.equals( monedaBanco ) ){
                vlrItemOc   = getValorMoneda( monedaBanco, vlrItemOc, monedaLocal, getHoy("-"), op.getDstrct() );
                tasa        = this.TASA;
                op.setMoneda( monedaLocal );
            }
            
            op.setTasa    ( (float) tasa );
            op.setVlrOc   ( vlrItemOc    );
            op.setVlrOc_Me( vlrItemOc_me );
            
            
            //4. Formar la lista de movimientos, insertamos el valor de la planilla como primer item de los movimientos
            List mov  =  formatMovimientos(op);
            
            //5. Calcular valor de movimiento INDICADOR = P
            mov =  calcularPorcentajeItems(mov);
            
            //6. Calculo de los impuestos a aplicar sobre los movimientos 
            
            //Obtenemos el codigo ica de la ciudad origen de la OC
            String datosAgencia   = getAgenciaAsociada(op.getOrigenOC());
            String[] vecAge       = datosAgencia.split("-");
            String codeRicaAge    = vecAge[2];
            
            //Obtenemos los impuestos
            String hoy             = getHoy("-");
            IMP_RETEFUENTE         = getImpuesto( CODERETEFUENTE, RETEFUENTE, hoy, op.getDstrct());
            IMP_RETEICA            = getImpuesto( codeRicaAge,    RETEICA,    hoy, op.getDstrct());
            //////System.out.println("RETEFUENTE " + IMP_RETEFUENTE.getPorcentaje());
            //////System.out.println("RETEICA "  +IMP_RETEICA.getPorcentaje());
            
            //Obtenemos valores retenedor y contibuyente del proveedor (nitpro) de la OC
            String retenedor       = op.getRetenedor().toUpperCase();
            String contribuyente   = op.getContribuyente().toUpperCase();
                                   
            //7. Calculos valores de los movimientos -> definicion de Item del la OP
            List listMovimientos = new LinkedList();
            
            if(mov!=null && mov.size()>0){
                
                for(int i=0; i<mov.size();i++){
                    
                    OPItems item = (OPItems) mov.get(i);
                    double vlrMov     = item.getVlr();
                    String monedaItem = item.getMoneda();
                    item.setVlr_me  ( vlrMov );
                    
                    //7.1 Conversion Moneda de los movimientos: Moneda Mov -> Moneda Banco -> Moneda Local
                    //////System.out.println("MONEDA MOV " + monedaItem + " MONEDA BANCO " + monedaBanco + " MONEDA LOCAL " + monedaLocal);
                    
                    // convertir moneda movimiento a la del banco
                    if( !monedaBanco.equals( item.getMoneda() ))  {
                        vlrMov     = getValorMoneda( item.getMoneda(), vlrMov, monedaBanco, getHoy("-"), op.getDstrct() );
                        monedaItem = monedaBanco;
                    }
                    
                    // convertir moneda banco a la local
                    if( !monedaLocal.equals( monedaBanco )){
                        vlrMov     = getValorMoneda( monedaBanco, vlrMov, monedaLocal, getHoy("-"), op.getDstrct() );
                        monedaItem = monedaLocal;
                    }
                    
                    item.setVlr     ( vlrMov    );
                    item.setMoneda  ( monedaItem);
                                        
                    //7.2 Si el proveedor NO ES agente retenedor...se debe calcular y definir los impuestos al item
                    if( !retenedor.equals("S")  ){
                        item  = calculoImpuesto(item, op.getAgencia());
                    }
                    
                    listMovimientos.add(item);
                }
            }
            
            
            //8. Definimos al objeto OP la lista de items
            op.setItem( listMovimientos );
            
            //9. Calculamos Valor Neto OC = sumatoria Items tipo asigandor: V-valor
            op.setVlrOcNeto(  calcularTipoValores( listMovimientos , "V" ) );
            //////System.out.println("Calculo valor total de la OC (Mov tipo Valor V) " + op.getVlrOcNeto());
            
            //10. Calculamos Valor de los Movimientos = sumatoria Items tipo asignador S-saldo
            op.setVlrMov  (   calcularTipoValores( listMovimientos , "S" ) );
            //////System.out.println("Calculo valor total de los mvimientos (Mov tipo Saldo S) " + op.getVlrMov());
            
            //11. Calculamos y definimos Neto a pagar en la OP = (sumatoria valores de los items) - (sumatoria valores de los impuestos aplicados a los items)
            op.setVlrNeto( calculoNetoLiquidacion( listMovimientos ) );
            op.setVlr_neto_me( op.getVlrNeto() );
            
            
        }catch(Exception e){
            throw new Exception( " liquidar OC " + e.getMessage());
        }
        return op;
    }
    
    
    /**
     * M�todo buscarCantidadCumplidaCarbon, obtiene la cantidad cumplida de una planilla de carbon
     * @autor   sescalante
     * @param   numero de la planilla (String)
     * @return  double
     * @throws  Exception
     * @version 1.0.
     **/
    public double buscarCantidadCumplidaCarbon(String planilla)throws Exception{
        double cantidad = 0;
        PreparedStatement st        = null;
        ResultSet         rs        = null;
        try{
            
            String sql = 
                    "SELECT " +
                    "       pesoreal as cantidad " +
                    "FROM " +
                    "       planilla " +
                    "WHERE   numpla = ?";
            st = con.prepareStatement(sql);
            st.setString(1, planilla);
            //////System.out.println("Cantidad cumplida carbon (pesoreal) " + st);
            rs = st.executeQuery();
            if (rs.next()){
                cantidad = rs.getDouble("cantidad");
            }
        }catch(SQLException e){
            throw new SQLException(" DAO: No se pudo obtener la cantidad cumplida de la planilla de carbon -->"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
        }
        return cantidad;
    }
    
    /**
     * M�todo cantidadCumplida, obtiene la cantidad cumplida de una planilla de carga general
     * @autor   fvillacob
     * @param   numero de la planilla (String)
     * @return  double
     * @throws  Exception
     * @version 1.0.
     **/
    public double  cantidadCumplida(String oc) throws Exception{
        PreparedStatement st        = null;
        ResultSet         rs        = null;
        double            cantidad  = 0;
        try {
            String sql = 
                    "SELECT " +
                    "       cantidad " +
                    "FROM   " +
                    "       cumplido " +
                    "WHERE  " +
                    "       tipo_doc = '001' " +
                    "       AND cod_doc  = ? ";
            st= con.prepareStatement(sql);
            st.setString(1, oc);
            //////System.out.println("Cantidad cumplida cga gral " + st);
            rs=st.executeQuery();
            while(rs.next()){
                cantidad = rs.getDouble(1);
            }
        } catch(SQLException e){
            throw new SQLException(" DAO: cantidadCumplida.-->"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
        }
        return cantidad;
    }
    
    /**
     * M�todo getDatosCia, obtiene la informacion de una compania
     * @autor   fvillacob
     * @param   codigo de la compania (String)
     * @return  String
     * @throws  Exception
     * @version 1.0.
     **/
    public String getDatosCia(String cia) throws Exception{
        PreparedStatement st        = null;
        ResultSet         rs        = null;
        String            datos     = " - ";
        try {
            String sql = 
                    "SELECT " +
                    "       moneda, " +
                    "       base " +
                    "FROM " +
                    "       cia " +
                    "WHERE " +
                    "       dstrct= ? " +
                    "       AND reg_status != 'A'";            
            st= con.prepareStatement(sql);
            st.setString(1, cia);
            rs=st.executeQuery();
            while(rs.next())
                datos  = this.getValor(rs.getString(1)) +"-"+
                this.getValor(rs.getString(2));
        }
        catch(SQLException e){
            throw new SQLException(" DAO: No se pudo Buscar datos de la Cia.-->"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
        }
        return datos;
    }
    
    /**
     * M�todo que forma la lista de mov de la oc incluyendo su primer item
     * @autor fvillacob
     * @param op objeto resultado de la busqueda de la informacion para liquidar una oc
     * @return List
     * @throws Exception
     * @version 1.0.
     **/
    public List formatMovimientos(OP  op) throws Exception{
        List lista = new LinkedList();
        
        try{
            // Seteamos el valor de la planilla como primer item de la lista
            OPItems itemOC =  op.getItemOC();
            itemOC.setVlr   ( op.getVlrOc()     );
            itemOC.setVlr_me( op.getVlrOc_Me()  );
            itemOC.setCheque( "" );//ivan Dario Gomez
            
            // Grabamos como primer item en la lista
            lista.add(itemOC);
            
            // Recorremos los Movimientos (demas items) para adicionarlos a la lista de resultado
            List movimientos  = op.getItem();
            if(movimientos!=null && movimientos.size()>0){
                
                for(int i=0; i<movimientos.size();i++){
                    OPItems itemMov = (OPItems) movimientos.get(i);
                    lista.add(itemMov);
                }
                
            }
            
        }catch(Exception e){
            throw new Exception( e.getMessage() );
        }
        return lista;
    }
    
    /**
     * M�todo calcularPorcentajeItems, calcula el valor del movimiento de planilla Indicador P con respecto a la sumatoria de los movimientos Asigando V
     * @autor...fvillacob
     * @param op objeto resultado de la busqueda de la informacion para liquidar una oc
     * @return OP (orden de pago Objeto)
     * @throws..Exception
     * @version..1.0.
     **/
    public List calcularPorcentajeItems(List lista) throws Exception{
        try{
            
            double vlrocTotal  = 0;
            if( lista !=null  && lista.size()>0 ){
                
                //Sumatoria de los items ASIGNADOR = V
                for(int i=0;i<lista.size();i++){
                    
                    OPItems  item  = (OPItems) lista.get(i);
                    if( item.getAsignador().toUpperCase().equals("V")){
                        vlrocTotal  += item.getVlr();
                        //////System.out.println("ITEM V " + item.getVlr());
                        //////System.out.println("TOTAL " + vlrocTotal);
                    }
                }
                
                // calculamos valor item indicador P  = ( sumatoria items asignador V * valor item P ) /100
                for(int i=0;i<lista.size();i++){
                    OPItems  item  = (OPItems) lista.get(i);
                    if( item.getIndicador().toUpperCase().equals("P")){
                        //////System.out.println("ANTIGUO VALOR ITEM P " + item.getVlr());
                        item.setVlr(  (vlrocTotal  * item.getVlr())/100 );
                        //////System.out.println("TOTAL ITEMS ASIGNADOR " + vlrocTotal);
                        //////System.out.println("CALCULO ITEM INDICADOR P " + item.getVlr());
                    }
                }
            }
            
        }catch(Exception e){
            throw new Exception( " calcularPorcentajeItems "+ e.getMessage());
        }
        return lista;
    }
    
    /**
     * M�todo getAgenciaAsociada, obtiene la informacion de una agencia asociada
     * @autor   fvillacob
     * @param   codigo de la agencia (ciudad) (String)
     * @return  String
     * @throws  Exception
     * @version 1.0.
     **/
    public String getAgenciaAsociada(String agencia) throws Exception{
        PreparedStatement st        = null;
        ResultSet         rs        = null;
        String            asoc     = " - ";
        try {
            String sql = 
                    "SELECT " +
                    "       pais as pais, " +
                    "       agasoc as asociada, " +
                    "       codica as codica " +
                    "FROM   ciudad " +
                    "WHERE  codciu = ?";
            st= con.prepareStatement(sql);
            st.setString(1, agencia);
            //////System.out.println("Agencia asociada " + st);
            rs=st.executeQuery();
            while(rs.next())
                asoc  = this.getValor(  rs.getString("pais")     )   +"-"+
                this.getValor(  rs.getString("asociada") )   +"-"+
                this.getValor(  rs.getString("codica")   );
        }
        catch(SQLException e){
            throw new SQLException(" DAO: No se pudo Buscar Agencia Relacionada.-->"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
        }
        return asoc;
    }
    
    /**
     * M�todo calculoImpuesto, calcula valor impuestos a los item
     * @autor...fvillacob
     * @param item, origenOC
     * @return OPItems
     * @throws..Exception
     * @version..1.0.
     **/
    public OPItems calculoImpuesto(OPItems item, String origenOC) throws Exception{        
        try{
            
            List impuestos = new LinkedList();
            
            if(  item.getAsignador().toUpperCase().equals("V")){   //  V:valor  S: saldo
                
                if( item.getDstrct().equals( COLOMBIA )){
                    
                    //Obtenemos el pais de la agencia del item
                    
                    String datosAgencia   = getAgenciaAsociada( item.getAgencia() );
                    String[] vec          = datosAgencia.split("-");
                    String pais           = vec[0];
                    //String asociada       = vec[1];
                    
                    double       vlrImp    = 0;
                    
                    //RETEFUENTE...Si el proveedor (nitpro OC) es agente retefuente                    
                    if( item.getReteFuente().equals("S")){
                        
                        if ( pais.toUpperCase().equals("CO") ){
                            
                            //Calculamos el valor de este impuesto = ( valor del item * porcetaje del impuesto ) /100
                            vlrImp     =  ( item.getVlr()     *  this.IMP_RETEFUENTE.getPorcentaje() ) /100;
                            item.setVlrReteFuente(vlrImp);
                            impuestos.add(IMP_RETEFUENTE);
                            //////System.out.println("VALOR ITEM RETEFUENTE " + item.getVlrReteFuente());
                        }
                    }
                    
                    
                    //RETEICA...Si el proveedor (nitpro OC) es agente reteica 
                    if( item.getReteIca().equals("S")){
                        
                        //Calculamos el valor de este impuesto = ( valor del item * porcetaje del impuesto ) /100
                        vlrImp     =  ( item.getVlr()     * this.IMP_RETEICA.getPorcentaje() ) /100;
                        item.setVlrReteIca(vlrImp);
                        impuestos.add(IMP_RETEICA);
                        //////System.out.println("VALOR ITEM RETEICA " + item.getVlrReteIca());
                    }
                    
                }
            }
            //registramos los impuestos del item
            item.setImpuestos(impuestos);
            
        }catch(Exception e){
            throw new Exception( e.getMessage());
        }
        return item;
    }
    
    /**
     * M�todo getImpuesto, busca un impuesto
     * @autor   fvillacob
     * @param   codigo del impuesto, tipo de impuesto, fecha, distrito de la OP (String)
     * @return  OPImpuestos
     * @throws  Exception
     * @version 1.0.
     **/
    public OPImpuestos getImpuesto( String codeImpuesto, String tipo, String fecha, String dstrct ) throws Exception{
        PoolManager poolManager = PoolManager.getInstance();
        PreparedStatement st        = null;
        ResultSet         rs        = null;
        OPImpuestos impuesto        = new OPImpuestos();
        try{
            String sql = 
                    "SELECT " +
                    "       codigo_impuesto as codigo, " +
                    "       tipo_impuesto as tipo, " +
                    "       porcentaje1 as porc1, " +
                    "       porcentaje2 as porc2, " +
                    "       agencia as agencia, " +
                    "       cod_cuenta_contable  as cuenta  " +
                    "FROM " +
                    "       tipo_de_impuesto " +
                    "WHERE " +
                    "       dstrct = ? " +
                    "       AND codigo_impuesto = ? " +
                    "       AND '#FECHA#' <=  fecha_vigencia  " +
                    "       AND tipo_impuesto  = ? " +
                    "       AND reg_status != 'A' ";
            sql = sql.replaceAll("#FECHA#", fecha);
            st= con.prepareStatement( sql );
            st.setString(1, dstrct);
            st.setString(2, codeImpuesto);
            st.setString(3, tipo);
            //////System.out.println("IMPUESTO " + tipo + " " + st);
            rs=st.executeQuery();
            while(rs.next()){
                impuesto.setCodigo      ( rs.getString("codigo")  );
                impuesto.setAgencia     ( rs.getString("agencia") );
                impuesto.setCuenta      ( rs.getString("cuenta")  );
                impuesto.setPorcentaje  ( rs.getDouble("porc1")   );
                impuesto.setPorcentaje2( rs.getDouble("porc2")   );
                impuesto.setTipo        ( tipo );
                break;
            }
        }catch(SQLException e){
            throw new SQLException(" DAO: No se pudo obtener el impuesto-->"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
        }
        return impuesto;
    }
    
    /**
     * M�todo calcularTipoValores, calcula el valor de los items Tipo V o S
     * @autor...fvillacob
     * @param lista (lista de items), tipo (tipo de asignador)
     * @return double
     * @throws..Exception
     * @version..1.0.
     **/
    public double calcularTipoValores(List lista, String tipo) throws Exception{
        double vlrTotal  = 0;
        try{
            
            if( lista !=null  && lista.size()>0 ){
                for(int i=0;i<lista.size();i++){
                    OPItems  item  = (OPItems) lista.get(i);
                    if( item.getAsignador().toUpperCase().equals( tipo ))
                        vlrTotal  += item.getVlr();
                }
            }
            
        }catch(Exception e){
            throw new Exception( " calcularTipoValores "+ e.getMessage());
        }
        return vlrTotal;
    }
    
    /**
     * M�todo calculoNetoLiquidacion, calcula NETO A PAGAR de la OP
     * @autor fvillacob
     * @param listItem lista de items
     * @return double
     * @throws..Exception
     * @version..1.0.
     **/
    public double calculoNetoLiquidacion(List listItem) throws Exception{
        double valor = 0;
        double val   = 0;
        double imp   = 0;
        try{
            if(listItem!=null && listItem.size()>0){
                for(int j=0;j<listItem.size();j++){
                    OPItems  item = (OPItems)listItem.get(j);
                    val    += item.getVlr();
                    imp    += item.getVlrReteFuente() + item.getVlrReteIca() + item.getVlrReteIva();
                    //////System.out.println("ITEM " + item.getItem() + " VALOR " + item.getVlr());
                    //////System.out.println(" IMPUESTOS DEL ITEM: RETEFUENTE " +  item.getVlrReteFuente() + " RETEICA " + item.getVlrReteIca()+ " RETEIVA " + item.getVlrReteIva());
                }
            }
            //////System.out.println("TOTAL ITEMS " + val + " TOTAL IMP ITEMS " + imp);
            valor = val - imp;
            //////System.out.println("VALOR NETO LIQ " + valor);
        }catch(Exception e){
            throw new Exception( e.getMessage());
        }
        return valor;
    }
    
    /**
     * M�todo getExcluirRepetidos, excluye oc repetidas en una consulta
     * @autor fvillacob
     * @see liquidarOC - liquidarOCDAO
     * @param lista lista de ocs
     * @return List
     * @throws Exception
     * @version 1.0.
     **/
    public List getExcluirRepetidosLiquidacion(List lista)throws Exception{
        List listOP = new LinkedList();
        try{
            
            if(lista!=null && lista.size()>0){
                for(int i=0;i<lista.size();i++){
                    OP     op  = (OP)lista.get(i);
                    String oc  = op.getOc();
                    int    sw  = 0;
                    for(int j=0; j<listOP.size(); j++){
                        OP  copyOP  = (OP)listOP.get(j);
                        if( copyOP.getOc().equals(oc) ) {
                            sw = 1;
                            break;
                        }
                    }
                    if(sw==0)
                        listOP.add(op);
                }
            }
            
        }catch( Exception e){
            throw new Exception(" getExcluirRepetidos: "+e.getMessage());
        }
        return listOP;
    }
    
    public void NProveedoresNoReg(List lista, List lprov, String usuario, String ruta) throws Exception{
                
        try{
            
            if (lista != null && lista.size() > 0 && lprov != null && lprov.size() > 0){
                String mensaje = "";
                
                String path = ruta + "/exportar/migracion/" + usuario;
                String path2 = path + "/ProveedoresNoEncontrado" + this.getHoy("") + ".txt";
                
                File dir = new File(path);
                dir.mkdirs();
                File file = new File(path2);
                
                this.fw     = new FileWriter    (path2);
                this.bf     = new BufferedWriter(this.fw);
                this.linea  = new PrintWriter   (this.bf);
                
                String msg = "Para las siguientes planillas NO SE GENERARON OPs porque los proveedores no se encontraron registrados en el sistema";
                mensaje += msg + "\n\nNo. PLANILLAS \n-----------------\n";
                linea.println(msg);
                linea.println(" ");
                linea.println("No. PLANILLAS");
                linea.println("-----------------");
                for(int i=0;i<lista.size();i++){
                    Planilla p = (Planilla) lista.get(i);
                    String  dato = p.getNumpla();
                    linea.println( dato );
                    mensaje += dato + "\n";
                }
                
                linea.println(" ");
                msg = "Los siguientes proveedores no se encuentran registrados en el sistema";
                mensaje += "\n\n" + msg + "\n\nNo. PROVEEDORES \n-----------------\n";
                linea.println(" ");
                linea.println("No. PROVEEDORES");
                linea.println("-----------------");
                
                for(int i=0;i<lprov.size();i++){
                    Planilla p = (Planilla) lprov.get(i);
                    String  dato = p.getNitpro();
                    linea.println( dato );
                    mensaje += dato + "\n";
                }
                
                this.linea.close();
                
                ////System.out.println("MENSAJE MAIL " + mensaje);
                
                saveMail("procesos@mail.tsp.com", buscarEmailPerfil("ADMIN"), "GENERACION OP", mensaje );
                
                //generamos mail de notificacion
                /*String correos = model.usuarioService.buscarEmailPerfil("ADMIN");
                
                if(!correos.equals("") ){
                    Email mail = new Email();
                    mail.setEmailfrom   ( "procesos@mail.tsp.com" );
                    mail.setEmailto     ( correos);
                    mail.setEmailsubject( "GENERACION OP");
                    mail.setEmailbody   ( mensaje );
                    try{
                        model.emailService.saveMail(mail);
                    }
                    catch(Exception e){
                    }
                }*/                
            }
        }catch(Exception e){
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }
    
    public void saveMail(String Emailfrom, String Emailto, String Emailsubject, String Emailbody) throws Exception{
        PreparedStatement st        = null;
        ResultSet         rs        = null;
        try {
            String INSERT =
                    " INSERT INTO sendmail     "+
                    " (                        "+
                    "   emailcode ,            "+
                    "   emailfrom ,            "+
                    "   emailto,               "+
                    "   emailcopyto ,          "+
                    "   emailsubject ,         "+
                    "   emailbody ,            "+
                    "   sendername ,           "+
                    "   remarks                "+
                    " )                        "+
                    " VALUES (?,?,?,?,?,?,?,?) ";
            st= con.prepareStatement(INSERT);
            st.setString(1, "" );
            st.setString(2, Emailfrom    );
            st.setString(3, Emailto );
            st.setString(4, "" );
            st.setString(5, Emailsubject );
            st.setString(6, Emailbody );
            st.setString(7, "PROCESO AUTOMATICO" );
            st.setString(8, "" );
            ////System.out.println("........... SAVE MAIL: " + st);
            st.executeUpdate();
            
        }catch(Exception e){
            throw new Exception(" DAO[saveMail] : No se pudo guardar el Mail -->"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
        }
    }
    
    public String buscarEmailPerfil(String perfil)throws SQLException{
        PreparedStatement st = null;
        ResultSet rs = null;
        String correos="";
        boolean sw = false;
        
        try {
            String BUSCAR_EMAIL = 
                    "SELECT email " +
                    "FROM usuarios "+
                    "WHERE perfil = ? " +
                    "      AND email LIKE '%@%.com%' ";
            
            if(con!=null){
                
                st = con.prepareStatement(BUSCAR_EMAIL);
                st.setString(1,perfil);
                rs = st.executeQuery();
                
                while(rs.next()){
                    correos= correos + rs.getString("email")+";";
                    sw = true;
                }
                if(sw){
                    correos.substring(0,correos.length()-1);
                }
                
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA EMAIL DE USUARIO" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
        }
        
        return correos;        
    }
}
