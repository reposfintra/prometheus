/********************************************************************
 *      Nombre Clase.................   CXPItemDoc.java
 *      Descripci�n..................   Bean de la tabla cxp_items_doc
 *      Autor........................   David Lamadrid
 *      Fecha........................   20.10.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.util.OPAuto;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.io.Serializable;

public class CXPItemDoc implements Serializable{
    
        private String reg_status;
        private String dstrct;
	private String proveedor;
	private String tipo_documento;
	private String documento;
	private String descripcion;
        private String descconcepto;//sescalante
        private String item;
        private double vlr;
	private double vlr_me;
	private double vlr_item;
	private double vlr_item_me;
	private String codigo_cuenta;
	private String codigo_abc;
	private String planilla;
	private String last_update;
	private String user_update;
	private String creation_date;
	private String creation_user;
	private String base;        
        private String codcliarea;
        private String tipcliarea;
        private String descliarea;
        //campos para saber si un Item tiene observacion
        boolean banderaRoja;
        boolean banderaVerde;        
        boolean responder;
        private Vector vItems;
        
        private String textoactivo;
    
    /** Creates a new instance of Documento */
        public CXPItemDoc() {
        }
    
    /**
     * Getter for property reg_status.
     * @return Value of property reg_status.
     */
    public java.lang.String getReg_status() {
        return reg_status;
    }
    
    /**
     * Setter for property reg_status.
     * @param reg_status New value of property reg_status.
     */
    public void setReg_status(java.lang.String reg_status) {
        this.reg_status = reg_status;
    }
    
    /**
     * Getter for property dstrct.
     * @return Value of property dstrct.
     */
    public java.lang.String getDstrct() {
        return dstrct;
    }
    
    /**
     * Setter for property dstrct.
     * @param dstrct New value of property dstrct.
     */
    public void setDstrct(java.lang.String dstrct) {
        this.dstrct = dstrct;
    }
    
    /**
     * Getter for property proveedor.
     * @return Value of property proveedor.
     */
    public java.lang.String getProveedor() {
        return proveedor;
    }
    
    /**
     * Setter for property proveedor.
     * @param proveedor New value of property proveedor.
     */
    public void setProveedor(java.lang.String proveedor) {
        this.proveedor = proveedor;
    }
    
    /**
     * Getter for property tipo_documento.
     * @return Value of property tipo_documento.
     */
    public java.lang.String getTipo_documento() {
        return tipo_documento;
    }
    
    /**
     * Setter for property tipo_documento.
     * @param tipo_documento New value of property tipo_documento.
     */
    public void setTipo_documento(java.lang.String tipo_documento) {
        this.tipo_documento = tipo_documento;
    }
    
    /**
     * Getter for property documento.
     * @return Value of property documento.
     */
    public java.lang.String getDocumento() {
        return documento;
    }
    
    /**
     * Setter for property documento.
     * @param documento New value of property documento.
     */
    public void setDocumento(java.lang.String documento) {
        this.documento = documento;
    }
    
    /**
     * Getter for property descripcion.
     * @return Value of property descripcion.
     */
    public java.lang.String getDescripcion() {
        return descripcion;
    }
    
    /**
     * Setter for property descripcion.
     * @param descripcion New value of property descripcion.
     */
    public void setDescripcion(java.lang.String descripcion) {
        this.descripcion = descripcion;
    }  
   
        
    /**
     * Getter for property creation_date.
     * @return Value of property creation_date.
     */
    public java.lang.String getCreation_date() {
        return creation_date;
    }
    
    /**
     * Setter for property creation_date.
     * @param creation_date New value of property creation_date.
     */
    public void setCreation_date(java.lang.String creation_date) {
        this.creation_date = creation_date;
    }
    
    /**
     * Getter for property creation_user.
     * @return Value of property creation_user.
     */
    public java.lang.String getCreation_user() {
        return creation_user;
    }
    
    /**
     * Setter for property creation_user.
     * @param creation_user New value of property creation_user.
     */
    public void setCreation_user(java.lang.String creation_user) {
        this.creation_user = creation_user;
    }
    
    /**
     * Getter for property base.
     * @return Value of property base.
     */
    public java.lang.String getBase() {
        return base;
    }
    
    /**
     * Setter for property base.
     * @param base New value of property base.
     */
    public void setBase(java.lang.String base) {
        this.base = base;
    }
  
    /**
     * Getter for property banderaRoja.
     * @return Value of property banderaRoja.
     */
    public boolean isBanderaRoja() {
        return banderaRoja;
    }
    
    /**
     * Setter for property banderaRoja.
     * @param banderaRoja New value of property banderaRoja.
     */
    public void setBanderaRoja(boolean banderaRoja) {
        this.banderaRoja = banderaRoja;
    }
    
    /**
     * Getter for property banderaVerde.
     * @return Value of property banderaVerde.
     */
    public boolean isBanderaVerde() {
        return banderaVerde;
    }
    
    /**
     * Setter for property banderaVerde.
     * @param banderaVerde New value of property banderaVerde.
     */
    public void setBanderaVerde(boolean banderaVerde) {
        this.banderaVerde = banderaVerde;
    }
    
    /**
     * Getter for property responder.
     * @return Value of property responder.
     */
    public boolean isResponder() {
        return responder;
    }
    
    /**
     * Setter for property responder.
     * @param responder New value of property responder.
     */
    public void setResponder(boolean responder) {
        this.responder = responder;
    }
    
    /**
     * Getter for property vlr.
     * @return Value of property vlr.
     */
    public double getVlr() {
        return vlr;
    }
    
    /**
     * Setter for property vlr.
     * @param vlr New value of property vlr.
     */
    public void setVlr(double vlr) {
        this.vlr = vlr;
    }
    
    /**
     * Getter for property item.
     * @return Value of property item.
     */
    public java.lang.String getItem() {
        return item;
    }
    
    /**
     * Setter for property item.
     * @param item New value of property item.
     */
    public void setItem(java.lang.String item) {
        this.item = item;
    }
    
    /**
     * Getter for property codigo_abc.
     * @return Value of property codigo_abc.
     */
    public java.lang.String getCodigo_abc() {
        return codigo_abc;
    }
    
    /**
     * Setter for property codigo_abc.
     * @param codigo_abc New value of property codigo_abc.
     */
    public void setCodigo_abc(java.lang.String codigo_abc) {
        this.codigo_abc = codigo_abc;
    }
    
    /**
     * Getter for property codigo_cuenta.
     * @return Value of property codigo_cuenta.
     */
    public java.lang.String getCodigo_cuenta() {
        return codigo_cuenta;
    }
    
    /**
     * Setter for property codigo_cuenta.
     * @param codigo_cuenta New value of property codigo_cuenta.
     */
    public void setCodigo_cuenta(java.lang.String codigo_cuenta) {
        this.codigo_cuenta = codigo_cuenta;
    }
    
    /**
     * Getter for property planilla.
     * @return Value of property planilla.
     */
    public java.lang.String getPlanilla() {
        return planilla;
    }
    
    /**
     * Setter for property planilla.
     * @param planilla New value of property planilla.
     */
    public void setPlanilla(java.lang.String planilla) {
        this.planilla = planilla;
    }
    
    /**
     * Getter for property vItems.
     * @return Value of property vItems.
     */
    public java.util.Vector getVItems() {
        return vItems;
    }
    
    /**
     * Setter for property vItems.
     * @param vItems New value of property vItems.
     */
    public void setVItems(java.util.Vector vItems) {
        this.vItems = vItems;
    }
    
    /**
     * Getter for property user_update.
     * @return Value of property user_update.
     */
    public java.lang.String getUser_update() {
        return user_update;
    }
    
    /**
     * Setter for property user_update.
     * @param user_update New value of property user_update.
     */
    public void setUser_update(java.lang.String user_update) {
        this.user_update = user_update;
    }
    
    /**
     * Getter for property vlr_me.
     * @return Value of property vlr_me.
     */
    public double getVlr_me() {
        return vlr_me;
    }
    
    /**
     * Setter for property vlr_me.
     * @param vlr_me New value of property vlr_me.
     */
    public void setVlr_me(double vlr_me) {
        this.vlr_me = vlr_me;
    }
    
    /**
     * Getter for property textoactivo.
     * @return Value of property textoactivo.
     */
    public java.lang.String getTextoactivo() {
        return textoactivo;
    }
    
    /**
     * Setter for property textoactivo.
     * @param textoactivo New value of property textoactivo.
     */
    public void setTextoactivo(java.lang.String textoactivo) {
        this.textoactivo = textoactivo;
    }
    
    /**
     * Getter for property codcliarea.
     * @return Value of property codcliarea.
     */
    public java.lang.String getCodcliarea () {
        return codcliarea;
    }
    
    /**
     * Setter for property codcliarea.
     * @param codcliarea New value of property codcliarea.
     */
    public void setCodcliarea (java.lang.String codcliarea) {
        this.codcliarea = codcliarea;
    }
    
    /**
     * Getter for property tipcliarea.
     * @return Value of property tipcliarea.
     */
    public java.lang.String getTipcliarea () {
        return tipcliarea;
    }
    
    /**
     * Setter for property tipcliarea.
     * @param tipcliarea New value of property tipcliarea.
     */
    public void setTipcliarea (java.lang.String tipcliarea) {
        this.tipcliarea = tipcliarea;
    }
    
    /**
     * Getter for property descliarea.
     * @return Value of property descliarea.
     */
    public java.lang.String getDescliarea () {
        return descliarea;
    }
    
    /**
     * Setter for property descliarea.
     * @param descliarea New value of property descliarea.
     */
    public void setDescliarea (java.lang.String descliarea) {
        this.descliarea = descliarea;
    }
    
    /**
     * Getter for property descconcepto.
     * @return Value of property descconcepto.
     */
    public java.lang.String getDescconcepto() {
        return descconcepto;
    }
    
    /**
     * Setter for property descconcepto.
     * @param descconcepto New value of property descconcepto.
     */
    public void setDescconcepto(java.lang.String descconcepto) {
        this.descconcepto = descconcepto;
    }
    
}
