/*
 * Nombre        ControlEstadoRemesas.java
 * Autor         LREALES
 * Fecha         1 de septiembre de 2006, 02:34 PM
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */

package com.tsp.util.OPAuto;

import java.io.*;
import java.sql.*;
import java.util.*;
import jxl.*;

import com.tsp.util.*;
import com.tsp.finanzas.contab.model.beans.*;// PlanDeCuentas
import com.tsp.finanzas.presupuesto.model.beans.*;// JXLRead
import com.tsp.util.connectionpool.PoolManager;// Conexion BD
import com.tsp.operation.model.beans.POIWrite;// Generar Excel

import org.apache.poi.hssf.usermodel.*;// Generar Excel
import org.apache.poi.hssf.util.*;// Generar Excel

public class ControlStandarCostFull extends Thread {
    
    private PlanDeCuentas bean;
    
    private Vector vector_imp_no_estan;
    private Vector vector_imp_est_dif;
    private Vector vector_imp_no_anuladas;
    
    private String Archivo;
    private String Hoja;
    
    /**************************************************************************/
    private String usuario = "LREALES";
    private String fec_ini = "20060801"; 
    private String fec_fin = "20060801"; 
    /**************************************************************************/
    
    private static final String SQL_MIMS =
        "SELECT "+
        "	b.codigo_st, "+
        "	substr(a.assoc_rec,1,1) cxv, "+
        "	substr(a.assoc_rec,2,11) vxv, "+
        "	substr(a.assoc_rec,13,1) cxu, "+
        "	substr(a.assoc_rec,14,11) vxu, "+
        "	substr(a.assoc_rec,25,3) ut, "+
        "	substr(a.assoc_rec,29,3) mon, "+
        "	a.table_code codigo_ft, "+
        "	b.origen, "+
        "	c.table_desc nom_orig, "+
        "	b.destino, "+
        "	d.table_desc nom_dest, "+
        "	b.codigo_cf "+
        "FROM "+
        "	msf010 a, "+
        "	msf010 c, "+
        "	msf010 d, "+
        "	( "+
        "	SELECT "+
        "		substr ( b.assoc_rec,1,6 ) codigo, "+
        "		substr ( b.assoc_rec,1,2 ) origen, "+
        "		substr ( b.assoc_rec,3,2 ) destino, "+
        "		b.table_code codigo_cf, "+
        "		substr ( b.table_code,1,6 ) codigo_st, "+
        "		b.table_desc, "+
        "		b.table_type "+
        "	FROM "+
        "		msf010 b "+
        "	WHERE "+
        "		b.table_type = '+CF' "+
        "	) b "+
        "WHERE "+
        "	a.table_type = '+FT' AND "+
        "	a.table_code = b.codigo AND "+
        "	b.origen = c.table_code AND "+
        "	c.table_type = '+CI' AND "+
        "	b.destino = d.table_code AND "+
        "	d.table_type = '+CI' " ;

    private static final String SQL_STDJCF =
        "SELECT " +
        "       * " +
        "FROM " +
        "       stdjobcostfull " +
        "WHERE " +
        "       dstrct = 'FINV' AND " +
        "       sj = ? AND " +
        "       cf_code = ? " ;

    /**
     * Crea una nueva instancia de  ControlEstadoRemesas
     */
    public ControlStandarCostFull () { }
    
    /**
     * Getter for property vector_imp_no_estan.
     * @return Value of property vector_imp_no_estan.
     */
    public java.util.Vector getVector_imp_no_estan() {
        return vector_imp_no_estan;
    }
    
    /**
     * Setter for property vector_imp_no_estan.
     * @param vector_imp_no_estan New value of property vector_imp_no_estan.
     */
    public void setVector_imp_no_estan(java.util.Vector vector_imp_no_estan) {
        this.vector_imp_no_estan = vector_imp_no_estan;
    }
    
    /**
     * Getter for property vector_imp_est_dif.
     * @return Value of property vector_imp_est_dif.
     */
    public java.util.Vector getVector_imp_est_dif() {
        return vector_imp_est_dif;
    }
    
    /**
     * Setter for property vector_imp_est_dif.
     * @param vector_imp_est_dif New value of property vector_imp_est_dif.
     */
    public void setVector_imp_est_dif(java.util.Vector vector_imp_est_dif) {
        this.vector_imp_est_dif = vector_imp_est_dif;
    }
        
    /**
     * Getter for property vector_imp_no_anuladas.
     * @return Value of property vector_imp_no_anuladas.
     */
    public java.util.Vector getvector_imp_no_anuladas() {
        return vector_imp_no_anuladas;
    }
    
    /**
     * Setter for property vector_imp_no_anuladas.
     * @param vector_imp_no_anuladas New value of property vector_imp_no_anuladas.
     */
    public void setvector_imp_no_anuladas(java.util.Vector vector_imp_no_anuladas) {
        this.vector_imp_no_anuladas = vector_imp_no_anuladas;
    }
    
    public static void main ( String[]abc ) throws Exception {
        
        try {
            
            ControlStandarCostFull cer = new ControlStandarCostFull ();
            cer.run();
            
            System.gc();
                
        } catch ( Exception ex ){
            ex.printStackTrace ();
        }
        
    }
        
    public synchronized void run () {
        
        com.tsp.operation.model.Model modelOperation = new com.tsp.operation.model.Model();
          
        try {
            
            System.gc();
            
            modelOperation.LogProcesosSvc.InsertProceso( "Control Standar Job Cost Full", this.hashCode(), "Proceso de Reporte de Control de Estado de las Remesas", usuario );
            
            System.gc();
            
            exportar();
            
            System.gc();
            
            modelOperation.LogProcesosSvc.finallyProceso( "Control Standar Job Cost Full", this.hashCode(), usuario, "PROCESO EXITOSO" );
            
            System.gc();
            
        } catch ( Exception ex ) {
            
            try {
                ex.printStackTrace();
                modelOperation.LogProcesosSvc.finallyProceso( "Control Standar Job Cost Full", this.hashCode(), usuario, "ERROR : " + ex.getMessage() );
            } catch ( Exception e ){
                e.printStackTrace();
            }            
            
        }
        
    }
    
    /**
     * Obtiene una lista de las remesas con estado 'AN', 'NF' y 'CD'.
     * @autor LREALES
     * @param la fecha inicial y la fecha final.
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public Vector DatosComparados ( ) throws SQLException {
        
        Connection conOracle = null, conPostgres = null;
        PreparedStatement st = null, ps = null;
        ResultSet rs = null, rs2 = null;
        PoolManager poolManager = null;
        Vector vecMims = new Vector();
        try {
            poolManager = PoolManager.getInstance ();
            conOracle = poolManager.getConnection ( "oracle" );
            conPostgres = poolManager.getConnection ( "sot" );
            if( conOracle != null || conPostgres != null ){
                st = conOracle.prepareStatement ( this.SQL_MIMS );
                rs = st.executeQuery ();
                while ( rs.next () ){
                    Vector vecStdJob = new Vector();
                    ps = conPostgres.prepareStatement ( this.SQL_STDJCF );
                    ps.setString(1, rs.getString(1) );
                    ps.setString(2, rs.getString(13) );
                    rs2 = ps.executeQuery ();
                    if ( rs2.next () ){
                        vecStdJob.add ( rs.getString(1) );
                        vecStdJob.add ( rs2.getString("sj") );
                        
                        vecStdJob.add ( rs.getString(13) );
                        vecStdJob.add ( rs2.getString("cf_code") );
                        
                        vecStdJob.add ( rs.getString(8) );
                        vecStdJob.add ( rs2.getString("ft_code") );
                        
                        vecStdJob.add ( String.valueOf (rs.getDouble(3)/100) );
                        vecStdJob.add ( rs2.getString("cost") );
                        
			
                        vecStdJob.add ( String.valueOf (rs.getDouble(5)/10000) );
                        vecStdJob.add ( rs2.getString("unit_cost") );  
                        
                        vecStdJob.add ( rs.getString(6) );
                        vecStdJob.add ( rs2.getString("unit_transp") );
                        
                        vecStdJob.add ( rs.getString(7) );
                        vecStdJob.add ( rs2.getString("currency") );
                        
                        vecMims.add ( vecStdJob );
                    }
                }
            }
            
        } catch ( SQLException e ){
            
            e.printStackTrace ();
            throw new SQLException( "ERROR EN - [DatosComparados].. " + e.getMessage() + " " + e.getErrorCode() );
        
        }
        
        finally{
            
            if ( st != null ){
                try{
                    st.close ();
                } catch ( SQLException e ){
                    throw new SQLException ( "ERROR CERRANDO EL ESTAMENTO " + e.getMessage() );
                }
            }            
            if ( conOracle != null || conPostgres != null ){
                poolManager.freeConnection ( "oracle", conOracle );
                poolManager.freeConnection ( "sot", conPostgres );
            }
            
        }
        return vecMims;
    }
    

        
    public synchronized void exportar() {
        
        try{
            
            Util u = new Util();
            
            ResourceBundle rb = ResourceBundle.getBundle( "com/tsp/util/connectionpool/db" );
            String path = rb.getString( "ruta" );
                        
            File file = new File( path + "/exportar/migracion/" + usuario );
            file.mkdirs();
               
            String fecha_actual = Util.getFechaActual_String(6);            
            fecha_actual=fecha_actual.replaceAll( "/", "-" );
            fecha_actual=fecha_actual.replaceAll( ":", "_" );
            
            String nombreArch  = "ControlSTDJOBCOSTFULL[" + fecha_actual + "].xls";
            String       Hoja  = "ControlSTDJOBCOSTFULL";
            String       Ruta  = path + "/exportar/migracion/" + usuario + "/" +nombreArch; 
            
            // fuente, tama�o, negrita, cursiva, formato, color, fondo, alineado
            com.tsp.operation.model.beans.POIWrite xls = new com.tsp.operation.model.beans.POIWrite (Ruta);
            HSSFWorkbook wb = new HSSFWorkbook ();
            HSSFCellStyle fecha  = xls.nuevoEstilo ("verdana", 12, false , false, "yyyy/mm/dd"  , xls.NONE , xls.NONE , xls.NONE );
            HSSFCellStyle texto  = xls.nuevoEstilo ("verdana", 12, false , false, "text"        , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle numero = xls.nuevoEstilo ("verdana", 12, false , false, ""            , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle numero_negrita = xls.nuevoEstilo ("verdana", 12, true , false, "#,##0.00"            , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle numero_negrita_sin = xls.nuevoEstilo ("verdana", 12, true , false, ""            , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle negrita      = xls.nuevoEstilo ("verdana", 12, true  , false, "text"        , HSSFColor.BLACK.index , xls.NONE , xls.NONE);
            HSSFCellStyle header      = xls.nuevoEstilo ("verdana", 18, true  , false, "text"        , HSSFColor.WHITE.index , HSSFColor.DARK_RED.index , HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle titulo      = xls.nuevoEstilo ("verdana", 12, true  , false, "text"        , HSSFColor.BLACK.index , HSSFColor.GREY_25_PERCENT.index , HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle fechatitle  = xls.nuevoEstilo ("verdana", 12, true  , false, "yyyy/mm/dd"  , HSSFColor.WHITE.index , HSSFColor.DARK_GREEN.index , HSSFCellStyle.ALIGN_CENTER );
            
            int fila = 5;
            int col  = 0;
            xls.obtenerHoja ("GENERAL");
            xls.cambiarMagnificacion (3,4);
            // cabecera
            
            xls.adicionarCelda (0,0, "REPORTE DE CONTROL STDJOB COST FULL "  , header);
            xls.combinarCeldas (0, 0, 0, 7);
            
            // subtitulos
            xls.adicionarCelda (2,0, "Fecha Proceso: "   , negrita);
            xls.adicionarCelda (2,1, fecha_actual , fecha);
            xls.adicionarCelda (3,0, "Elaborado Por: "   , negrita);
            xls.adicionarCelda (3,1, usuario , fecha);
            
            fila++;
            xls.adicionarCelda (fila ,0 , "Stdjob"                       , titulo );
            //xls.combinarCeldas (fila, 0, fila, 1); 
            
            xls.adicionarCelda (fila ,1 , "Codigo CF"                  , titulo );
            xls.combinarCeldas (fila, 1, fila, 2);
            
            xls.adicionarCelda (fila ,3 , "Codigo FT"                  , titulo );
            xls.combinarCeldas (fila, 3, fila, 4);
            
            xls.adicionarCelda (fila ,5 , "Cost"                       , titulo );
            xls.combinarCeldas (fila, 5, fila, 6);
            
            xls.adicionarCelda (fila ,7 , "Unit Cost"                  , titulo );
            xls.combinarCeldas (fila, 7, fila, 8);
            
            xls.adicionarCelda (fila ,9  , "Unit Transp"                , titulo );
            xls.combinarCeldas (fila, 9 , fila, 10);
            
            xls.adicionarCelda (fila ,11 , "Moneda"                     , titulo );
            xls.combinarCeldas (fila, 11, fila, 12);
            
            fila++;
            col=0;
            xls.adicionarCelda (fila ,col++ , "Stdjob"                          , titulo );
            //xls.adicionarCelda (fila ,col++ , "Stdjob WEB"                      , titulo );
            xls.adicionarCelda (fila ,col++ , "MIMS"                            , titulo );
            xls.adicionarCelda (fila ,col++ , "WEB"                             , titulo );
            xls.adicionarCelda (fila ,col++ , "MIMS"                            , titulo );
            xls.adicionarCelda (fila ,col++ , "WEB"                             , titulo );
            xls.adicionarCelda (fila ,col++ , "MIMS"                            , titulo );
            xls.adicionarCelda (fila ,col++ , "WEB"                             , titulo );
            xls.adicionarCelda (fila ,col++ , "MIMS"                            , titulo );
            xls.adicionarCelda (fila ,col++ , "WEB"                             , titulo );
            xls.adicionarCelda (fila ,col++ , "MIMS"                            , titulo );
            xls.adicionarCelda (fila ,col++ , "WEB"                             , titulo );
            xls.adicionarCelda (fila ,col++ , "MIMS"                            , titulo );
            xls.adicionarCelda (fila ,col++ , "WEB"                             , titulo );
            
            Vector vec = DatosComparados();
            for (int i = 0; i < vec.size (); i++ ){
                col=0;
                fila++;
		Vector datos = (Vector) vec.get (i);
                xls.adicionarCelda (fila ,col++ , (String)datos.get (0)               , texto );
                //xls.adicionarCelda (fila ,col++ , (String)datos.get (1)               , texto );
                
                String var2 = (String)datos.get (2), var3 = (String)datos.get (3);
                if( !var2.equals ( var3 ) ){
                    xls.adicionarCelda (fila ,col++ , (String)datos.get (2)                 , texto );
                    xls.adicionarCelda (fila ,col++ , (String)datos.get (3)                 , texto );
                }
                else{
                    xls.adicionarCelda (fila ,col++ , ""                                    , texto );
                    xls.adicionarCelda (fila ,col++ , ""                                    , texto );
                }
                
                String var4 = (String)datos.get (4), var5 = (String)datos.get (5);
                if( !var4.equals ( var5 ) ){
                    xls.adicionarCelda (fila ,col++ , (String)datos.get (4)                 , texto );
                    xls.adicionarCelda (fila ,col++ , (String)datos.get (5)                 , texto );
                }
                else{
                    xls.adicionarCelda (fila ,col++ , ""                                    , texto );
                    xls.adicionarCelda (fila ,col++ , ""                                    , texto );
                }
                
                double var6 = Double.parseDouble ( (String)datos.get (6) ), var7 = Double.parseDouble ( (String)datos.get (7) );
                if( var6 != var7 ){
                    xls.adicionarCelda (fila ,col++ , var6                                  , numero );
                    xls.adicionarCelda (fila ,col++ , var7                                  , numero );
                }
                else{
                    xls.adicionarCelda (fila ,col++ , ""                                    , texto );
                    xls.adicionarCelda (fila ,col++ , ""                                    , texto );
                }
                
                double var8 = Double.parseDouble ( (String)datos.get (8) ), var9 = Double.parseDouble ( (String)datos.get (9) );
                if( var8 != var9 ){
                    xls.adicionarCelda (fila ,col++ , var8                                  , numero );
                    xls.adicionarCelda (fila ,col++ , var9                                  , numero );
                }
                else{
                    xls.adicionarCelda (fila ,col++ , ""                                    , texto );
                    xls.adicionarCelda (fila ,col++ , ""                                    , texto );
                }
                
                String var10 = (String)datos.get (10), var11 = (String)datos.get (11);
                if( !var10.equals ( var11 ) ){
                    xls.adicionarCelda (fila ,col++ , (String)datos.get (10)                , texto );
                    xls.adicionarCelda (fila ,col++ , (String)datos.get (11)                , texto );
                }
                else{
                    xls.adicionarCelda (fila ,col++ , ""                                    , texto );
                    xls.adicionarCelda (fila ,col++ , ""                                    , texto );
                }
                
                String var12 = (String)datos.get (12), var13 = (String)datos.get (13);
                if( !var12.equals ( var13 ) ){
                    xls.adicionarCelda (fila ,col++ , (String)datos.get (12)              , texto );
                    xls.adicionarCelda (fila ,col++ , (String)datos.get (13)              , texto );
                }
                else{
                    xls.adicionarCelda (fila ,col++ , ""                                    , texto );
                    xls.adicionarCelda (fila ,col++ , ""                                    , texto );
                }
            }
            xls.cerrarLibro ();
        } catch ( Exception e ) {    
            e.printStackTrace ();
        }
                
    }
    
}