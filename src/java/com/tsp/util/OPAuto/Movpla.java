package com.tsp.util.OPAuto;

import java.io.*;
import java.sql.*;
import java.util.*;

public class Movpla implements Serializable{
    
    private String dstrct;
    private String sucursal;
    private String agency_id;
    private String document_type;
    private String document;
    private String item;
    private String concept_code;
    private String pla_owner;
    private String planilla;
    private String supplier;
    private String date_doc;
    private String ind_vlr="V";
    private String ap_ind;
    private float vlr_disc;
    private float vlr_for;
    private float vlr;
    private String currency;
    private String creation_date;
    private String creation_user;
    private String proveedor;
    private String banco="";
    private String cuenta="";
    
    //ALEJO
    private String reg_status="";
    private String applicated_ind;
    private String application_ind;
    private String last_update;
    private String user_update;
    private String proveedor_anticipo;
    private String base;
    private String corte;
    private String branch_code;
    private String bank_account_no;
    private String fech_anul;
    private String user_anul;
    private String fecha_cheque;
    private String ch_remplazo;
    private String branch_code_remplazo;
    private String bank_account_no_remplazo;
    private String fecha_migracion="0099-01-01";
    private float cantidad=0;
    private boolean impresa=false;
    private String reanticipo="N";
    
    //ANULACION DE CHEQUE
    private String causa="";
    private String tipo_rec="N";
    private String observacion="";
    
    
    public Movpla() {
    }
    
    
    
    public static Movpla load(ResultSet rs)throws SQLException{
        
        Movpla movpla = new Movpla();
        
        movpla.setDstrct(rs.getString("Dstrct"));
        movpla.setAgency_id(rs.getString("Agency_id"));
        movpla.setDocument_type(rs.getString("Document_type"));
        movpla.setDocument(rs.getString("Document"));
        movpla.setItem(rs.getString("Item"));
        movpla.setConcept_code(rs.getString("Concept_code"));
        movpla.setPla_owner(rs.getString("Pla_owner"));
        movpla.setPlanilla(rs.getString("Planilla"));
        movpla.setSupplier(rs.getString("Supplier"));
        movpla.setDate_doc(rs.getString("Date_doc"));
        movpla.setInd_vlr(rs.getString("Ind_vlr"));
        movpla.setVlr_disc(rs.getFloat("Vlr_disc"));
        movpla.setVlr(rs.getFloat("Vlr"));
        movpla.setAp_ind(rs.getString("application_ind"));
        movpla.setCurrency(rs.getString("Currency"));
        movpla.setCreation_date(rs.getString("Creation_date"));
        movpla.setCreation_user(rs.getString("Creation_user"));
        movpla.setProveedor(rs.getString("proveedor_anticipo"));
        movpla.setSucursal(rs.getString("sucursal"));
        
        return movpla;
    }
    
    //============================================
    //		Metodos de acceso a propiedades
    //============================================
    public void setProveedor(String proveedor){
        
        this.proveedor=proveedor;
    }
    
    public String getProveedor(){
        
        return proveedor;
    }
    public void setSucursal(String sucursal){
        
        this.sucursal=sucursal;
    }
    
    public String getSucursal(){
        
        return sucursal;
    }
    
    public void setAp_ind(String ap_ind){
        
        this.ap_ind=ap_ind;
    }
    
    public String getAp_ind(){
        
        return ap_ind;
    }
    
    public void setDstrct(String dstrct){
        
        this.dstrct=dstrct;
    }
    
    public String getDstrct(){
        
        return dstrct;
    }
    
    public void setAgency_id(String agency_id){
        
        this.agency_id=agency_id;
    }
    
    public String getAgency_id(){
        
        return agency_id;
    }
    
    public void setDocument_type(String document_type){
        
        this.document_type=document_type;
    }
    
    public String getDocument_type(){
        
        return document_type;
    }
    
    public void setDocument(String document){
        
        this.document=document;
    }
    
    public String getDocument(){
        
        return document;
    }
    
    public void setItem(String item){
        
        this.item=item;
    }
    
    public String getItem(){
        
        return item;
    }
    
    public void setConcept_code(String concept_code){
        
        this.concept_code=concept_code;
    }
    
    public String getConcept_code(){
        
        return concept_code;
    }
    /**
     * Getter for property banco.
     * @return Value of property banco.
     */
    public java.lang.String getBanco() {
        return banco;
    }
    /**
     * Setter for property banco.
     * @param banco New value of property banco.
     */
    public void setBanco(java.lang.String banco) {
        this.banco = banco;
    }
    
    public void setPla_owner(String pla_owner){
        
        this.pla_owner=pla_owner;
    }
    
    public String getPla_owner(){
        
        return pla_owner;
    }
    
    public void setPlanilla(String planilla){
        
        this.planilla=planilla;
    }
    
    public String getPlanilla(){
        
        return planilla;
    }
    
    public void setSupplier(String supplier){
        
        this.supplier=supplier;
    }
    
    public String getSupplier(){
        
        return supplier;
    }
    
    public void setDate_doc(String date_doc){
        
        this.date_doc=date_doc;
    }
    
    public String getDate_doc(){
        
        return date_doc;
    }
    
    public void setInd_vlr(String ind_vlr){
        
        this.ind_vlr=ind_vlr;
    }
    
    public String getInd_vlr(){
        
        return ind_vlr;
    }
    
    public void setVlr_disc(float vlr_disc){
        
        this.vlr_disc=vlr_disc;
    }
    
    public float getVlr_disc(){
        
        return vlr_disc;
    }
    
    public void setVlr(float vlr){
        
        this.vlr=vlr;
    }
    
    public float getVlr(){
        
        return vlr;
    }
    public void setVlr_for(float vlr_for){
        
        this.vlr_for=vlr_for;
    }
    
    public float getVlr_for(){
        
        return vlr_for;
    }
    
    public void setCurrency(String currency){
        
        this.currency=currency;
    }
    
    public String getCurrency(){
        
        return currency;
    }
    
    public void setCreation_date(String creation_date){
        
        this.creation_date=creation_date;
    }
    
    public String getCreation_date(){
        
        return creation_date;
    }
    
    public void setCreation_user(String creation_user){
        
        this.creation_user=creation_user;
    }
    
    public String getCreation_user(){
        
        return creation_user;
    }
    
    /**
     * Getter for property cuenta.
     * @return Value of property cuenta.
     */
    public java.lang.String getCuenta() {
        return cuenta;
    }
    
    /**
     * Setter for property cuenta.
     * @param cuenta New value of property cuenta.
     */
    public void setCuenta(java.lang.String cuenta) {
        this.cuenta = cuenta;
    }
    
    
    public String getFech_anul() {
        return fech_anul;
    }
    
    
    public String getBranch_code() {
        return branch_code;
    }
    
    
    public String getBank_account_no() {
        return bank_account_no;
    }
    
    public String getUser_anul() {
        return user_anul;
    }
    
    public String getFecha_cheque() {
        return fecha_cheque;
    }
    
    
    public String getProveedor_anticipo() {
        return proveedor_anticipo;
    }
    
    
    public String getApplication_ind() {
        return application_ind;
    }
    
    public String getReg_status() {
        return reg_status;
    }
    
    public String getBank_account_no_remplazo() {
        return bank_account_no_remplazo;
    }
    
    
    
    
    public String getBase() {
        return base;
    }
    
    
    
    public String getLast_update() {
        return last_update;
    }
    
    
    
    public String getCh_remplazo() {
        return ch_remplazo;
    }
    
    public String getBranch_code_remplazo() {
        return branch_code_remplazo;
    }
    
    public String getApplicated_ind() {
        return applicated_ind;
    }
    
    public String getCorte() {
        return corte;
    }
    
    public String getFecha_migracion() {
        return fecha_migracion;
    }
    
    public String getUser_update() {
        return user_update;
    }
    
    
    
    public void setFech_anul( String fech_anul ) {
        this.fech_anul = fech_anul;
    }
    
    
    public void setBranch_code( String branch_code ) {
        this.branch_code = branch_code;
    }
    
    
    public void setBank_account_no( String bank_account_no ) {
        this.bank_account_no = bank_account_no;
    }
    
    public void setUser_anul( String user_anul ) {
        this.user_anul = user_anul;
    }
    
    public void setFecha_cheque( String fecha_cheque ) {
        this.fecha_cheque = fecha_cheque;
    }
    
    public void setProveedor_anticipo( String proveedor_anticipo ) {
        this.proveedor_anticipo = proveedor_anticipo;
    }
    
    
    public void setApplication_ind( String application_ind ) {
        this.application_ind = application_ind;
    }
    
    public void setReg_status( String reg_status ) {
        this.reg_status = reg_status;
    }
    
    public void setBank_account_no_remplazo( String bank_account_no_remplazo ) {
        this.bank_account_no_remplazo = bank_account_no_remplazo;
    }
    
    
    public void setBase( String base ) {
        this.base = base;
    }
    
    
    public void setLast_update( String last_update ) {
        this.last_update = last_update;
    }
    
    
    public void setCh_remplazo( String ch_remplazo ) {
        this.ch_remplazo = ch_remplazo;
    }
    
    public void setBranch_code_remplazo( String branch_code_remplazo ) {
        this.branch_code_remplazo = branch_code_remplazo;
    }
    
    public void setApplicated_ind( String applicated_ind ) {
        this.applicated_ind = applicated_ind;
    }
    
    public void setCorte( String corte ) {
        this.corte = corte;
    }
    
    public void setFecha_migracion( String fecha_migracion ) {
        this.fecha_migracion = fecha_migracion;
    }
    
    public void setUser_update( String user_update ) {
        this.user_update = user_update;
    }
    
    /**
     * Getter for property cantidad.
     * @return Value of property cantidad.
     */
    public float getCantidad() {
        return cantidad;
    }
    
    /**
     * Setter for property cantidad.
     * @param cantidad New value of property cantidad.
     */
    public void setCantidad(float cantidad) {
        this.cantidad = cantidad;
    }
    
    /**
     * Getter for property impresa.
     * @return Value of property impresa.
     */
    public boolean isImpresa() {
        return impresa;
    }
    
    /**
     * Setter for property impresa.
     * @param impresa New value of property impresa.
     */
    public void setImpresa(boolean impresa) {
        this.impresa = impresa;
    }
    
    //ALEJO
    
    public String toString(){
        return "creation_date = '"+this.creation_date+"' and concept_code = '"+this.concept_code+"' and planilla = '"+this.planilla+"'and pla_owner = '"+this.pla_owner+"' and document = '"+this.document+"'";
    }
    
    /**
     * Getter for property reanticipo.
     * @return Value of property reanticipo.
     */
    public java.lang.String getReanticipo() {
        return reanticipo;
    }
    
    /**
     * Setter for property reanticipo.
     * @param reanticipo New value of property reanticipo.
     */
    public void setReanticipo(java.lang.String reanticipo) {
        this.reanticipo = reanticipo;
    }
    
    /**
     * Getter for property causa.
     * @return Value of property causa.
     */
    public java.lang.String getCausa() {
        return causa;
    }
    
    /**
     * Setter for property causa.
     * @param causa New value of property causa.
     */
    public void setCausa(java.lang.String causa) {
        this.causa = causa;
    }
    
    /**
     * Getter for property tipo_rec.
     * @return Value of property tipo_rec.
     */
    public java.lang.String getTipo_rec() {
        return tipo_rec;
    }
    
    /**
     * Setter for property tipo_rec.
     * @param tipo_rec New value of property tipo_rec.
     */
    public void setTipo_rec(java.lang.String tipo_rec) {
        this.tipo_rec = tipo_rec;
    }
    
    /**
     * Getter for property observacion.
     * @return Value of property observacion.
     */
    public java.lang.String getObservacion() {
        return observacion;
    }
    
    /**
     * Setter for property observacion.
     * @param observacion New value of property observacion.
     */
    public void setObservacion(java.lang.String observacion) {
        this.observacion = observacion;
    }
    
}



