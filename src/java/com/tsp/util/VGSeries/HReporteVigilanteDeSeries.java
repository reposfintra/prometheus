/**************************************************************************
 * Nombre clase: HReporteVigilanteDeSeries.java                            *
 * Descripci�n: Clase que envia los datos del reporte a excel                          *
 * Autor: Ing. Ivan Dario Gomez Vanegas                                    *
 * Fecha: Created on 9 de marzo de 2006, 05:11 PM                          *
 * Versi�n: Java 1.0                                                       *
 * Copyright: Fintravalores S.A. S.A.                                 *
 ***************************************************************************/
 

package com.tsp.util.VGSeries;

import com.tsp.util.*;
/**
 *
 * @author  Igomez
 */


import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;

import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.Model;

import java.util.*;
import java.lang.*;
import java.io.*;


public class HReporteVigilanteDeSeries extends Thread{

   Model   model;
   String  usuario;
   List listado;
   List listadoBuenos;
   
   //Log Procesos
   private String procesoName;
   private String des;
   
    public void init(List listado,List listadoBuenos, Model modelo){
        this.model           = modelo;
        this.usuario         = "CPENA";
        this.listado         = listado;
        this.listadoBuenos   = listadoBuenos;
        this.procesoName     = "Vigilante Series";
        this.des = "Vigilante de Series";
        super.start();
    }
    
    
    public synchronized void run(){
        try{
            
            //INSERTO EN EL LOG DE PROCESO
            model.LogProcesosSvc.InsertProceso(this.procesoName, this.hashCode(), this.des, usuario);  
            
           //if (lista!=null && lista.size()>0){
                
                ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
                String path = rb.getString("ruta") + "/exportar/migracion";

                String ruta = path + "/" + usuario + "/";// + model.movplaService.obtenerFechaActualEnFormato("YYYYMM") ;

                File f = new File(ruta); 
                if ( !f.exists() ){
                    f.mkdirs();
                    ////System.out.println("directorios creados en: "+f.getAbsolutePath());
                }
                
                
                com.tsp.operation.model.beans.POIWrite xls = new com.tsp.operation.model.beans.POIWrite( ruta + "/ReporteVGSeries.xls");
                ////System.out.println(" RUTA ---------- " + ruta + "ReporteVGSeries.xls");
               
                // Definicion de Estilos para la hoja de excel
               
                HSSFCellStyle fecha   = xls.nuevoEstilo("Book Antiqua", 9  , false   , false, "yyyy/mm/dd" , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_CENTER );
                HSSFCellStyle texto   = xls.nuevoEstilo("Book Antiqua", 10 , true    , false, "text"       , xls.NONE , xls.NONE , xls.NONE);
                HSSFCellStyle texto3  = xls.nuevoEstilo("Book Antiqua", 9  , false   , false, "text"       , xls.NONE , HSSFColor.GREY_25_PERCENT.index, HSSFCellStyle.ALIGN_CENTER);
                HSSFCellStyle texto4  = xls.nuevoEstilo("Book Antiqua", 9  , false   , false, "text"       , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_CENTER);
                HSSFCellStyle texto2  = xls.nuevoEstilo("Book Antiqua", 10 , true    , false, "text"       , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_RIGHT);
                HSSFCellStyle total   = xls.nuevoEstilo("Book Antiqua", 10 , true    , false, "text"       , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_LEFT );
                HSSFCellStyle titulo2 = xls.nuevoEstilo("Book Antiqua", 9  , true    , false, "text"       , HSSFColor.WHITE.index , HSSFColor.TEAL.index, HSSFCellStyle.ALIGN_CENTER);
                HSSFCellStyle numero  = xls.nuevoEstilo("Book Antiqua", 9  , false   , false, ""           , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_RIGHT);
                HSSFCellStyle titulo  = xls.nuevoEstilo("Book Antiqua", 9  , true    , false, "text"       , HSSFColor.BLACK.index , HSSFColor.GREY_25_PERCENT.index, HSSFCellStyle.ALIGN_CENTER);
                xls.obtenerHoja("ReporteVGSeries");
                int fila = 1;
                int col  = 0;
                xls.adicionarCelda(fila++ ,col  , "TRANSPORTE SANCHEZ POLO" , texto );
                xls.adicionarCelda(fila++ ,col  , "Fecha de Proceso: "+Util.getFechaActual_String(6) , texto );
                xls.adicionarCelda(fila++ ,col  , "Reporte De Series " , texto );
                xls.adicionarCelda(fila   ,col++ , "Distrito"            , titulo2 );
                xls.cambiarAnchoColumna(col, 4500);
                xls.adicionarCelda(fila   ,col++ , "Agencia"                 , titulo2 );
                xls.cambiarAnchoColumna(col, 4500);
                xls.adicionarCelda(fila   ,col++ , "Tipo De Documento"                 , titulo2 );
                xls.cambiarAnchoColumna(col, 4500);
                xls.adicionarCelda(fila   ,col++ , "Banco"               , titulo2 );
                xls.cambiarAnchoColumna(col, 4500);
                xls.adicionarCelda(fila   ,col++ , "Nombre De Cuenta"              , titulo2 );
                xls.cambiarAnchoColumna(col, 4500);
                xls.adicionarCelda(fila   ,col++ , "Serie Inicial"                   , titulo2 );
                xls.cambiarAnchoColumna(col, 4500);
                xls.adicionarCelda(fila   ,col++ , "Serie Final"                   , titulo2 );
                xls.cambiarAnchoColumna(col, 4500);
                xls.adicionarCelda(fila   ,col++ , "Ultimo Numero"                   , titulo2 );
                xls.cambiarAnchoColumna(col, 4500);
                xls.adicionarCelda(fila   ,col++ , "Restantes"                   , titulo2 );
                 xls.cambiarAnchoColumna(col, 4500);
                xls.adicionarCelda(fila   ,col++ , "Fecha Creaci�n"                   , titulo2 );
                 xls.cambiarAnchoColumna(col, 4500);
                xls.adicionarCelda(fila   ,col++ , "Usuario Creaci�n"                   , titulo2 );
                 
                Iterator it = listado.iterator();

                while(it.hasNext()){
                     String[]Datos =  (String[]) it.next();
                    fila++;
                    col = 0;
                     
                    xls.adicionarCelda(fila ,col++ , Datos[col-1]    , texto3);
                    xls.adicionarCelda(fila ,col++ , Datos[col-1]    , texto3);
                    xls.adicionarCelda(fila ,col++ , Datos[col-1]    , texto3);
                    xls.adicionarCelda(fila ,col++ , Datos[col-1]    , texto3);
                    xls.adicionarCelda(fila ,col++ , Datos[col-1]    , texto3);
                    xls.adicionarCelda(fila ,col++ , Datos[col-1]    , texto3);
                    xls.adicionarCelda(fila ,col++ , Datos[col-1]    , texto3);
                    xls.adicionarCelda(fila ,col++ , Datos[col-1]    , texto3);
                    xls.adicionarCelda(fila ,col++ , Datos[col-1]    , texto3);
                    xls.adicionarCelda(fila ,col++ , Datos[col-1]    , texto3);
                    xls.adicionarCelda(fila ,col++ , Datos[col-1]    , texto3);
                  

                }
                 Iterator ite = listadoBuenos.iterator();

                while(ite.hasNext()){
                     String[]DatosBuenos =  (String[]) ite.next();
                    fila++;
                    col = 0;
                     
                    xls.adicionarCelda(fila ,col++ , DatosBuenos[col-1]    , texto4);
                    xls.adicionarCelda(fila ,col++ , DatosBuenos[col-1]    , texto4);
                    xls.adicionarCelda(fila ,col++ , DatosBuenos[col-1]    , texto4);
                    xls.adicionarCelda(fila ,col++ , DatosBuenos[col-1]    , texto4);
                    xls.adicionarCelda(fila ,col++ , DatosBuenos[col-1]    , texto4);
                    xls.adicionarCelda(fila ,col++ , DatosBuenos[col-1]    , texto4);
                    xls.adicionarCelda(fila ,col++ , DatosBuenos[col-1]    , texto4);
                    xls.adicionarCelda(fila ,col++ , DatosBuenos[col-1]    , texto4);
                    xls.adicionarCelda(fila ,col++ , DatosBuenos[col-1]    , texto4);
                    xls.adicionarCelda(fila ,col++ , DatosBuenos[col-1]    , texto4);
                    xls.adicionarCelda(fila ,col++ , DatosBuenos[col-1]    , texto4);
                  

                }     
                
                xls.cerrarLibro();
                
            //}
             ////System.out.println("Listo..");
             model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(), usuario, "PROCESO EXITOSO");
        }catch (Exception ex){            
            ////System.out.println("Error : " + ex.getMessage());
           try{            
                model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(),usuario ,"ERROR :" + ex.getMessage());
            }
            catch(Exception f){
                try{               
                    model.LogProcesosSvc.finallyProceso(this.procesoName,this.hashCode(),usuario,"ERROR :");
                }catch(Exception p){    }
            }
        }
    }
}
