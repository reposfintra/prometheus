/******************************************************************
 * Nombre ......................VigilanteDeSeries.java
 * Descripci�n..................Programa vigilante de series
 * Autor........................Armando Oviedo
 * Fecha........................07/11/2005
 * Versi�n......................1.0
 * Coyright.....................Transportes Sanchez Polo S.A.
 *******************************************************************/
package com.tsp.util.VGSeries;

import java.util.*;
import java.sql.*;
import com.tsp.util.connectionpool.PoolManager;
import java.sql.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.operation.*;
//import com.tsp.operation.model.threads.*;

import com.tsp.util.PropertyReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.DriverManager;


import java.io.*;

public class VigilanteDeSeries {
    Connection  con         = null;
    private     Vector      listaGrupos = null;
    PrintWriter logger = null;
    private LinkedList tablas;
    /** Creates a new instance of VigilanteDeSeries */
    public VigilanteDeSeries() {
        
    }
    
    /**
     * M�todo principal que se ejecuta en consola y obtiene las series que tienen estado rojo.
     * @autor.......Armando Oviedo
     * @throws......Exception
     * @version.....1.0.
     **/
    public static void main(String[] args)throws Exception  {
        try{
            VigilanteDeSeries vs = new VigilanteDeSeries();
            vs.run();
           
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }
    
    
    
    
    
    public void run() throws Exception {
        try{
            
            String gruposSeries = "";
            Model model = new Model();
            List listado = new LinkedList();
            List listadoBuenos = new LinkedList();
            
            // creamos el logger
            // cargamos el archivo de configuraci�n
            PropertyReader pr = PropertyReader.obtenerInstancia(VigilanteDeSeries.class.getResource("config.properties"));
            File localHost = new File(pr.obtenerTexto("localLogPath"));
            File webServer = new File(pr.obtenerTexto("webServerLogPath"));
            if( localHost.exists() ){// si la ruta local existe
                logger = new PrintWriter(
                new BufferedWriter(
                new FileWriter(localHost.getPath() + "/" + "sot.log", true)
                ),
                true );
                
            }
            else if( webServer.exists() ){// si la ruta del servidor existe
                logger = new PrintWriter(
                new BufferedWriter(
                new FileWriter(webServer.getPath() + "/" + "sot.log", true)
                ),
                true );
               
            }
            else {// sino entonces imprimimos los detalles por la consola
                logger = new PrintWriter(System.out);
            }
            
            logger.println("INICIO DEL PROCESO  PARA LA VERIFICACION DE SERIES");
            long inicio = System.currentTimeMillis();
            // nos conectamos a la bd
            Class.forName(pr.obtenerTexto("driverClass"));
            con = DriverManager.getConnection(  pr.obtenerTexto("db_url"),
            pr.obtenerTexto("db_user"),
            pr.obtenerTexto("db_pass")
            );
            
            BuscarTodosGrupos();
            
            for(int i=0;i<listaGrupos.size();i++){
                Series stmp = (Series)(listaGrupos.elementAt(i));
                String[] vec = getActualState(stmp).split(",");
                String estado = vec[0];
                SerieGroupValidation serie =new SerieGroupValidation();
                
                if(estado.equalsIgnoreCase("rojo")){
                    gruposSeries += "Distrito: "+stmp.getDistrito()+", Agencia : "+stmp.getAgency_id()+", Tipo De Documento: "+stmp.getNombre_Documento()+", Banco: "+stmp.getBranch_code()+", Nombre De Cuenta: "+stmp.getBank_account_no()+", Inicial : "+stmp.getN_Inicial()+", Final : "+stmp.getN_Final()+", Ultimo Numero: "+stmp.getL_Numero() +", Restantes: "+vec[1]+"\n";
                    String[] Datos = new String[11];
                    Datos[0]  = stmp.getDistrito();
                    Datos[1]  = stmp.getAgency_id();
                    Datos[2]  = stmp.getNombre_Documento();
                    Datos[3]  = stmp.getBranch_code();
                    Datos[4]  = stmp.getBank_account_no();
                    Datos[5]  = stmp.getN_Inicial();
                    Datos[6]  = stmp.getN_Final();
                    Datos[7]  = stmp.getL_Numero();
                    Datos[8]  = vec[1];
                    Datos[9]  = String.valueOf(stmp.getCreation_date());
                    Datos[10] = stmp.getCreation_user();
                    listado.add(Datos);
                }else{
                    String[]DatosBuenos = new String[11];
                    DatosBuenos[0]  = stmp.getDistrito();
                    DatosBuenos[1]  = stmp.getAgency_id();
                    DatosBuenos[2]  = stmp.getNombre_Documento();
                    DatosBuenos[3]  = stmp.getBranch_code();
                    DatosBuenos[4]  = stmp.getBank_account_no();
                    DatosBuenos[5]  = stmp.getN_Inicial();
                    DatosBuenos[6]  = stmp.getN_Final();
                    DatosBuenos[7]  = stmp.getL_Numero();
                    DatosBuenos[8]  = vec[1];
                    DatosBuenos[9]  = String.valueOf(stmp.getCreation_date());
                    DatosBuenos[10] = stmp.getCreation_user();
                    listadoBuenos.add(DatosBuenos);
                }
               
            }
            String cuerpomail = "Se�ores TSP:\n\nSe requiere su inmediata atenci�n para las series pertenecientes a las cuentas:\n\n";
            cuerpomail += gruposSeries; 
            ////System.out.println(cuerpomail);
            if(!gruposSeries.equalsIgnoreCase("")){
                 String Copia = "";
                 LinkedList lista = obtenerInfoTablaGen ("EMAIL", "VGSERIES");
                 if(lista!=null){
                     Iterator it = lista.iterator ();
                     while(it.hasNext ()){
                         TablaGen t = (TablaGen)it.next ();
                         if(!it.hasNext()){
                             Copia +=   t.getDescripcion ();
                         }else{
                             Copia +=   t.getDescripcion ()+";";
                         }
                     }
                 }
                 ////System.out.println(Copia);
                //Enviar mail....       
                 model.seriesService.sendMailMessage("nparejo@mail.tsp.com", Copia, cuerpomail);
                 HReporteVigilanteDeSeries hilo = new HReporteVigilanteDeSeries(); 
                 hilo.init(listado,listadoBuenos,model);
                
            }
            
            
        }
        catch(Exception ex){
            ex.printStackTrace();
        } finally {
            if ( con != null ) {con.close();}
            if ( logger != null ) {logger.close();}
        }
    }
    /**
     * M�todo que busca y llena un vector con todos los grupos
     * @autor.......Armando Oviedo
     * @throws......SQLException
     * @version.....1.0.
     **/
    public void BuscarTodosGrupos() throws Exception{
        PreparedStatement ps = null;
        ResultSet rs = null;
        listaGrupos = new Vector();
        try{
            ps = con.prepareStatement("SELECT dstrct, agency_id, document_type, branch_code, bank_account_no,serial_initial_no, serial_fished_no, last_number, creation_date, creation_user from series where reg_status <> 'A' and document_type='004'");
            rs = ps.executeQuery();
            while(rs.next()){
                Series tmp = new Series();
                tmp = tmp.loadV(rs);
                if(!existeGrupo(tmp)){
                    listaGrupos.add(tmp);
                    tmp.setNombre_Doucmento(getNombreDocumento(tmp.getDocument_type()));
                    BuscarSeries(tmp);
                }
            }
        }catch(Exception ex){
            ex.printStackTrace();
            throw new Exception("Error en BuscarTodasSeries. "+ex.getMessage());
        }finally{
            if(ps != null){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            
        }
    }
    
    /**
     * M�todo que comprueba si existe un grupo
     * @autor.......Armando Oviedo
     * @throws......SQLException
     * @version.....1.0.
     * @param.......Serie s
     **/
    public boolean existeGrupo(Series s){
        boolean existe = false;
        for(int i=0;i<listaGrupos.size();i++){
            Series tmp = new Series();
            tmp = (Series)(listaGrupos.elementAt(i));
            if(tmp.getDistrito().equalsIgnoreCase(s.getDistrito()) && tmp.getAgency_id().equalsIgnoreCase(s.getAgency_id()) && tmp.getDocument_type().equalsIgnoreCase(s.getDocument_type()) && tmp.getBranch_code().equalsIgnoreCase(s.getBranch_code()) && tmp.getBank_account_no().equalsIgnoreCase(s.getBank_account_no())){
                existe = true;
                
            }
            
        }
        return existe;
    }
    
    /**
     * M�todo que guetea el nombre de un documento
     * @autor.......Armando Oviedo
     * @throws......SQLException
     * @version.....1.0.
     * @param.......String tipodocumento (doctype)
     **/
    public String getNombreDocumento(String doctype) throws Exception{
        String nombre = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try{
            ps = con.prepareStatement("SELECT document_name FROM tbldoc WHERE document_type=?");
            ps.setString(1,doctype);
            rs = ps.executeQuery();
            while(rs.next()){
                nombre = rs.getString(1);
            }
        }catch(Exception ex){
            ex.printStackTrace();
        }finally{
            if(ps != null){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            
        }
        return nombre;
    }
    
    
    /**
     * M�todo que busca todas las series de un grupo
     * @autor.......Armando Oviedo
     * @throws......SQLException
     * @version.....1.0.
     * @param.......Series s
     **/
    public void BuscarSeries(Series s) throws Exception{
        PreparedStatement ps = null;
        ResultSet rs;
        try{
            if(listaGrupos!=null){
                ps = con.prepareStatement("SELECT serial_initial_no, serial_fished_no, last_number FROM SERIES WHERE dstrct=? AND agency_id=? AND document_type=? AND branch_code=? AND bank_account_no=? AND reg_status!='A' ORDER BY serial_initial_no");
                ps.setString(1, s.getDistrito());
                ps.setString(2, s.getAgency_id());
                ps.setString(3, s.getDocument_type());
                ps.setString(4, s.getBranch_code());
                ps.setString(5, s.getBank_account_no());
                rs = ps.executeQuery();
                while(rs.next()){
                    SerieGroupValidation tmp = new SerieGroupValidation();
                    tmp = tmp.load(rs);
                    int restantes = Integer.parseInt(tmp.getFinalSerialNo()) - tmp.getLastNumber();
                    //////System.out.println(s.getAgency_id()+" - "+s.getBranch_code()+" - "+s.getBank_account_no()+" - "+restantes);
                    
                    tmp.setRestantes(restantes);
                    s.addListaSeriesElement(tmp);
                }
            }
        }
        catch(Exception ex){
            ex.printStackTrace();
            throw new Exception("Error en BuscarTodasSeries. "+ex.getMessage());
        }finally{
            if(ps != null){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            
        }
    }
    
    /**
     * M�todo que devuelve el estado y el valor restante de la serie
     * @autor.......Armando Oviedo
     * @throws......SQLException
     * @version.....1.0.
     * @see.........SeriesDAO.java
     * @param....... beans serie
     **/
    public String getActualState(Series s){
        String state = "";
        String rest ="";
        Vector ls = s.getListaSeries();
        if(ls.size()==1){
            SerieGroupValidation tmp = (SerieGroupValidation)(ls.elementAt(0));
            int restantes = tmp.getRestantes();
            if(restantes <= 10) state = "Rojo";
            else if(restantes < 30) state = "Amarillo";
            else state = "Verde";
            
            rest = String.valueOf(restantes);
        }
        else{
            int cont=0;
            for(int i=0;i<ls.size();i++){
                SerieGroupValidation tmp = (SerieGroupValidation)(ls.elementAt(i));
                cont += tmp.getRestantes();
            }
            if(cont <= 10) state = "Rojo";
            else if(cont < 30) state = "Amarillo";
            else state = "Verde";
            
            rest = String.valueOf(cont);
        }
        return state+","+rest;
    }
    
    
      /**
     * Obtiene el registro correspondiente a una tabla sin sus registros, es decir
     * el nombre de la tabla y su descripci�n
     * @param type el nombre de la tabla, o el tipo
     * @throws SQLException si algun error ocurre con el acceso a la base de datos.
     * @return Un bean <CODE>TablaGen</CODE> con los datos de la tabla
     */    
    public LinkedList obtenerInfoTablaGen(String type, String programa)throws SQLException{
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = con.prepareStatement("SELECT    " +
            "                          distinct a.table_code, a.descripcion, a.referencia  " +
            "                          FROM  tablagen a,                                   " +
            "                                tablagen_prog b                               " +
            "                          WHERE 	a.REG_STATUS!='A'                          " +
            "                          AND a.table_type=?                                  " +
            "                          AND a.table_type=b.table_type                       " +
            "                          AND a.table_code = b.table_code" +
            "                          AND b.program =?                                    ");
            
                   
                   
                   
                   
            ps.setString(1, type);
            ps.setString(2, programa);
            rs = ps.executeQuery();
            tablas = new LinkedList();
            while (rs.next()){
                TablaGen t = new TablaGen();
                t.setTable_type(rs.getString("table_code"));
                t.setDescripcion(rs.getString("descripcion"));
                t.setReferencia (rs.getString ("referencia"));
                tablas.add (t);
            }
        }
        catch( Exception ex ){
            throw new SQLException("ERROR BUSCANDO TABLA DE TABLAGEN: "+ex.getMessage());
        }
        finally{
            if ( rs != null ) {rs.close();}
            if ( ps != null ) {ps.close();}
           
        }
        return tablas;
    }
    
}
