/*
 * Nombre        ExportarIngresoTraficoXLS.java
 * Autor         Osvaldo P�rez Ferrer
 * Fecha         2 de agosto de 2006, 11:26 AM
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */

package com.tsp.util.trafico;

import java.util.*;
import java.io.*;
import java.lang.*;
import java.text.*;

import com.tsp.operation.model.Model;
import com.tsp.operation.model.beans.POIWrite;

import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.RepMovTraficoDAO;
import com.tsp.util.*;

public class ExportarIngresoTraficoXLS {
    
    private Vector datos;
    private String path;
    
    String   ruta;
    POIWrite xls;
    HSSFCellStyle header  , titulo1, titulo2, titulo3 , titulo4, letra, numero, porcentaje, letraCentrada, numeroNegrita;
    HSSFColor     cAzul   , cVerde, cAmarillo, cGris ;
    int           fila = 0;
    SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
    /**
     * Crea una nueva instancia de  ExportarIngresoTraficoXLS
     */
    public ExportarIngresoTraficoXLS() {
        Model model;
    }
    
    
    public void generarExcel(){
        try{
            
            RepMovTraficoDAO r = new RepMovTraficoDAO();
            
            r.obtenerIngresoTrafico();
            datos = r.getReportesPlanilla();
            
            if(datos.size()>0){
                
                ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
                ruta = rb.getString("ruta") + "/exportar/migracion/LDAVILA" ;
                File archivo = new File( ruta );
                if (!archivo.exists()) archivo.mkdirs();
                
                String fecha = Util.getFechaActual_String(6);
                fecha = fecha.replaceAll("/","-");
                fecha = fecha.replaceAll(":","");
                this.crearArchivo("Trafico "+fecha,"Ingreso Tr�fico");
                this.escribirEnArchivo(datos);
                this.cerrarArchivo();
                /************************************************************************************/
                /**** GUARDAR DATOS EN EL ARCHIVO  ***/
                
            }
        }catch(Exception ex){
            ////System.out.println("error en ExportarIngresoTrafico: "+ex.getMessage());
        }
    }
    
    public void InitArchivo(String nameFile) throws Exception{
        try{
            
            xls          = new com.tsp.operation.model.beans.POIWrite();
            xls.nuevoLibro( ruta + "/" + nameFile + ".xls" );
            
            // colores
            cAzul       = xls.obtenerColor(204,255,255);
            cVerde      = xls.obtenerColor( 51,153,102);
            cAmarillo   = xls.obtenerColor(255,255,153);
            cGris       = xls.obtenerColor(192,192,192);
            
            // estilos
            header       = xls.nuevoEstilo("Tahoma", 14, true  , false, "text"  , HSSFColor.ORANGE.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
            titulo1      = xls.nuevoEstilo("Tahoma", 8 , true  , false, "text"  , xls.NONE  , xls.NONE , xls.NONE);
            titulo2      = xls.nuevoEstilo("Tahoma", 8 , true  , false, "text"  , HSSFColor.BLACK.index, cAzul.getIndex() , HSSFCellStyle.ALIGN_CENTER, 1);
            titulo3      = xls.nuevoEstilo("Tahoma", 8 , true  , false, "text"  , HSSFColor.BLACK.index, cAmarillo.getIndex() , HSSFCellStyle.ALIGN_CENTER, 1);
            titulo4      = xls.nuevoEstilo("Tahoma", 8 , true  , false, "text"  , HSSFColor.WHITE.index, cVerde.getIndex() , HSSFCellStyle.ALIGN_CENTER, 2);
            letra        = xls.nuevoEstilo("Tahoma", 8 , false , false, ""      , xls.NONE , xls.NONE , xls.NONE);
            letraCentrada= xls.nuevoEstilo("Tahoma", 8 , false , false, ""      , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_CENTER);
            numero       = xls.nuevoEstilo("Tahoma", 8 , false , false, "_(#,##0.00_);(@_)"      , xls.NONE , xls.NONE , xls.NONE);
            numeroNegrita= xls.nuevoEstilo("Tahoma", 8 , true  , false, "_($* #,##0.00_);(@_)"      , xls.NONE , xls.NONE , xls.NONE);
            porcentaje   = xls.nuevoEstilo("Tahoma", 8 , false , false, "0.00%" , xls.NONE , xls.NONE , xls.NONE);
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception("Error en InitArchivo .... \n" + ex.getMessage());
        }
    }
    private void crearArchivo(String nameFile, String titulo) throws Exception{
        try{
            InitArchivo(nameFile);
            xls.obtenerHoja("Base");
            xls.combinarCeldas(0, 0, 0, 45);
            xls.combinarCeldas(1, 0, 1, 45);
            xls.combinarCeldas(2, 0, 2, 45);
            xls.adicionarCelda(0, 0, titulo, header);
            //xls.adicionarCelda(1, 0, "Usuario " + usuario.getNombre() , titulo1);
            xls.adicionarCelda(2, 0, "Fecha " + fmt.format( new Date() ) , titulo1);
            
            
            int col = 0;
            String [] cabecera = { "REG_STATUS", "DISTRITO", "PLANILLA",  "PLACA", "CED CONDUCTOR",//5
            "NOMBRE CONDUCTOR", "CED PROPIETARIO", "NOMBRE PROPIETARIO", "ORIGEN", "NOMBRE ORIGEN",//10
            "DESTINO", "NOMBRE DESTINO", "ZONA", "NOMBRE ZONA", "ESCOLTA", //15
            "CARAVANA","FECHA DESPACHO","PTO CONTROL ULT REPORTE","NOMBRE PTO CONTROL ULT REPORTE","FECHA ULT REPORTE",//20
            "PTO CONTROL PROX REPORTE","NOMBRE PTO CONTROL PROX REPORTE","FECHA PROX REPORTE","ULTIMA OBSERVACION","LAST UPDATE",//25
            "USER UPDATE", "FECHA CREACION", "USUARIO CREACION", "BASE" ,"DEMORA",//30
            "VIA", "FECHA SALIDA", "CEL CONDUCTOR", "CLIENTE", "TIPO DESPACHO", //35
            "TIPO REPORTE", "NOMBRE REPORTE", "NOMBRE CIUDAD", "CODIGO CIUDAD","CED CONDUCTOR ANTERIOR",//40
            "NOMBRE CONDUCTOR ANTERIOR","CED PROPIETARIO ANTERIOR","NOMBRE PROPIETARIO ANTERIOR","PLACA ANTERIOR","VIA ANTERIOR","CADENA"};//46
            short [] dimensiones = {
                
                3200, 3000, 3000, 3200, 4200,//5
                10000  , 4500, 10000, 3100, 5000,//10
                3000, 4500, 2000, 4800, 3000, //15
                4800, 4800, 9000,9000, 6000,//20
                8500, 9000, 6500 , 10000 , 5500,//25
                4000, 5500, 4800, 2500, 3000,//30
                4800, 4800, 4800,4800,4800,//35
                4800, 4800, 4800,4800,4800,//40
                9000, 4800, 9000,4800,4800,4800//46
            };
            
            fila=4;
            
            for ( int i = 0; i<cabecera.length; i++){
                
                xls.adicionarCelda(fila,  i, cabecera[i], titulo2);               
                xls.cambiarAnchoColumna(i, dimensiones [i]);
            }
            
            fila++;
            
            
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception( "Error en crearArchivo ....\n"  + ex.getMessage() );
        }
    }
    
    
    private void escribirEnArchivo(Vector datos) throws Exception{
        try{
            
            for(int i = 0; i<datos.size(); i++){
                
                Ingreso_Trafico it = (Ingreso_Trafico) datos.get(i);
                int col =0 ;
                xls.adicionarCelda(fila, col++, it.getReg_status()                , letraCentrada );
                xls.adicionarCelda(fila, col++, it.getDstrct()                    , letraCentrada );
                xls.adicionarCelda(fila, col++, it.getPlanilla()                  , letraCentrada );
                xls.adicionarCelda(fila, col++, it.getPlaca()                     , letraCentrada );
                xls.adicionarCelda(fila, col++, it.getCedcon()                    , letra         );//5
                xls.adicionarCelda(fila, col++, it.getNomcond()                   , letra         );
                xls.adicionarCelda(fila, col++, it.getCedprop()                   , letra         );
                xls.adicionarCelda(fila, col++, it.getNomprop()                   , letra         );
                xls.adicionarCelda(fila, col++, it.getOrigen()                    , letraCentrada );
                xls.adicionarCelda(fila, col++, it.getNomorigen()                 , letra         );//10
                xls.adicionarCelda(fila, col++, it.getDestino()                   , letraCentrada );
                xls.adicionarCelda(fila, col++, it.getNomdestino()                , letra         );
                xls.adicionarCelda(fila, col++, it.getZona()                      , letraCentrada );
                xls.adicionarCelda(fila, col++, it.getNomzona()                   , letra         );
                xls.adicionarCelda(fila, col++, it.getEscolta()                   , letraCentrada );//15
                xls.adicionarCelda(fila, col++, it.getCaravana()                  , letraCentrada );
                xls.adicionarCelda(fila, col++, it.getFecha_despacho()            , letra         );
                xls.adicionarCelda(fila, col++, it.getPto_control_ultreporte()    , letraCentrada );
                xls.adicionarCelda(fila, col++, it.getNompto_control_ultreporte() , letra         );
                xls.adicionarCelda(fila, col++, it.getFecha_ult_reporte()         , letra         );//20
                xls.adicionarCelda(fila, col++, it.getPto_control_proxreporte()   , letraCentrada );
                xls.adicionarCelda(fila, col++, it.getNompto_control_proxreporte(), letra         );
                xls.adicionarCelda(fila, col++, it.getFecha_prox_reporte()        , letra         );
                xls.adicionarCelda(fila, col++, it.getUlt_observacion()           , letra         );
                xls.adicionarCelda(fila, col++, it.getLast_update()               , letra         );//25
                xls.adicionarCelda(fila, col++, it.getUser_update()               , letra         );
                xls.adicionarCelda(fila, col++, it.getCreation_date()             , letra         );
                xls.adicionarCelda(fila, col++, it.getCreation_user()             , letra         );
                xls.adicionarCelda(fila, col++, it.getBase()                      , letra         );
                xls.adicionarCelda(fila, col++, it.getDemora()                    , letraCentrada );//30
                xls.adicionarCelda(fila, col++, it.getVia()                       , letra         );
                xls.adicionarCelda(fila, col++, it.getFecha_salida()              , letra         );
                xls.adicionarCelda(fila, col++, it.getCel_cond()                  , letra         );
                xls.adicionarCelda(fila, col++, it.getCliente()                   , letra         );
                xls.adicionarCelda(fila, col++, it.getTipo_despacho()             , letra         );//35
                xls.adicionarCelda(fila, col++, it.getTipo_reporte()              , letra         );
                xls.adicionarCelda(fila, col++, it.getNom_reporte()               , letra         );
                xls.adicionarCelda(fila, col++, it.getNom_ciudad()                , letra         );
                xls.adicionarCelda(fila, col++, it.getCod_ciudad()                , letra         );
                xls.adicionarCelda(fila, col++, it.getCedcon_anterior()           , letra         );//40
                xls.adicionarCelda(fila, col++, it.getNomcond_anterior()          , letra         );
                xls.adicionarCelda(fila, col++, it.getCedprop_anterior()          , letra         );
                xls.adicionarCelda(fila, col++, it.getNomprop_anterior()          , letra         );
                xls.adicionarCelda(fila, col++, it.getPlaca_anterior()            , letraCentrada );
                xls.adicionarCelda(fila, col++, it.getVia_anterior()              , letra         );
                xls.adicionarCelda(fila, col++, it.getCadena()                    , letra         );
                fila++;
            }
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
        
    }
    
    private void cerrarArchivo() throws Exception {
        try{
            if (xls!=null)
                xls.cerrarLibro();
        }catch (Exception ex){
            throw new Exception( "Error en crearArchivo ....\n"  + ex.getMessage() );
        }
    }
    
    public static void main(String[]abcde){
        ExportarIngresoTraficoXLS ex = new ExportarIngresoTraficoXLS();
        ex.generarExcel();
    }
    
}
