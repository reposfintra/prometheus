/****************************************************************************
 *  Nombre Clase.................   PuenteReporteControl.java               *
 *  Descripci�n..................   se encarga de hacer un puente entre el  *
 *                                  HiloReporteControl y el Action.         *
 *  Autor........................   LREALES                                 *
 *  Fecha........................   28.08.2006                              *
 *  Versi�n......................   1.0                                     *
 *  Copyright....................   Transportes Sanchez Polo S.A.           *
 ****************************************************************************/

package com.tsp.util.Contabilidad;

import com.tsp.finanzas.contab.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;
import java.sql.*;
import java.util.*;
import com.tsp.util.*;
import com.tsp.finanzas.contab.model.threads.*;
import com.tsp.operation.model.threads.HReporteUtilidadPlanillas;
import com.tsp.operation.model.beans.Usuario;

public class PuenteReporteControl extends Thread {
    
    
    private Usuario usu = null;
    private String  ano = "";
    private String  mes = "";
    private int num_anio = 0;
    private int num_anio_act = 0;
    private int num_mes = 0;
    private int num_mes_act = 0;
    private String archivo = "";
    private String usuario = "";
    
    /** Creates a new instance of PuenteReporteControl */
    public PuenteReporteControl () { }
        
    public void start ( Usuario usu, String ano, String mes, int num_anio, int num_anio_act, int num_mes, int num_mes_act, String archivo, String usuario ) {
        
        this.usu = usu;
        this.ano = ano;
        this.mes = mes;
        this.num_anio = num_anio;
        this.num_anio_act = num_anio_act;
        this.num_mes = num_mes;
        this.num_mes_act = num_mes_act;
        this.archivo = archivo;
        this.usuario = usuario;
        
        super.start();
        
    }
    
    public synchronized void run () {
        ////System.out.println("******ENTRO PUENTEEEEEEEEEEEE");
        com.tsp.operation.model.Model modelOperation = new com.tsp.operation.model.Model();
                
        try{
            
            ////System.out.println("INICIO Reporte Utilidad Planillas");
            // llamo a Reporte Utilidad - MFONTALVO
            HReporteUtilidadPlanillas hru = new HReporteUtilidadPlanillas();
            hru.start( modelOperation, usu, ano, mes );
            // obtengo el valor que me determina si termino o no el proceso de mario.
            boolean validar_finalizo = false;
            ////System.out.println("PASO Reporte Utilidad Planillas");

            int i = 0; 
            while ( i == 0 ){

                validar_finalizo = hru.isValidar_finalizo();

                if ( validar_finalizo ) {

                    String fecha = "";
                    String fecha_final = "";
                    String fecha_inicial = "";

                    if ( num_anio == num_anio_act && num_mes == num_mes_act ){
                        fecha = Util.getFechaActual_String(6);            
                        fecha=fecha.replaceAll( "/", "-" );
                        fecha_inicial = fecha.substring(0,8) + "01";//  inicio de mes
                        fecha_final = fecha.substring(0,10);// fin de mes                    
                    } else {
                        fecha_inicial = ano + "-" + mes + "-01";
                        fecha_final = ano + "-" + mes + "-" + com.tsp.util.UtilFinanzas.diaFinal( ano, mes );
                    }
                    ////System.out.println("INCIO Reporte Control");
                    // llamo a Reporte Control - LREALES
                    HiloReporteControl hrc = new HiloReporteControl();
                    hrc.leer( usuario, archivo, fecha_inicial, fecha_final );
                    ////System.out.println("PASO Reporte Control");

                    i = 1;

                }

            }
                
        } catch ( Exception e ){
            
            e.printStackTrace();
                           
        }
        
    }

}