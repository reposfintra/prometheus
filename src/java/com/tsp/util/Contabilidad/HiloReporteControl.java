/*
 * Nombre        HiloReporteControl.java
 * Autor         LREALES
 * Fecha         19 de agosto de 2006, 11:34 AM
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */

package com.tsp.util.Contabilidad;


import java.io.*;
import java.sql.*;
import java.util.*;
import jxl.*;

import com.tsp.util.*;
import com.tsp.finanzas.contab.model.beans.*;// PlanDeCuentas
import com.tsp.finanzas.presupuesto.model.beans.*;// JXLRead
import com.tsp.util.connectionpool.PoolManager;// Conexion BD
import com.tsp.operation.model.beans.POIWrite;// Generar Excel

import org.apache.poi.hssf.usermodel.*;// Generar Excel
import org.apache.poi.hssf.util.*;// Generar Excel

public class HiloReporteControl extends Thread {
    
    private PlanDeCuentas bean;
    
    private Vector vector_remesa;
    private Vector vector_planilla;
    
    private Vector vector_imp_remesa;
    private Vector vector_imp_planilla;
    private Vector vector_imp_rem_no_tan;
    private Vector vector_imp_pla_no_tan;
    
    private Vector vector_reg_status_pla;
    private Vector vector_reg_status_rem;
    
    private Vector vector_plarem;
    
    private String Archivo;
    private String Hoja;
        
    private static final String SQL_REMESAS =
        "SELECT * " +
        "FROM remesa " +
        "WHERE fecrem >= ? AND fecrem <= ? " +
        "AND ( base = 'COL' OR base = 'col' OR base = '' )" ;

    private static final String SQL_PLANILLAS =
        "SELECT * " +
        "FROM planilla " +
        "WHERE fecpla >= ? AND fecpla <= ? " +
        "AND ( base = 'COL' OR base = 'col' OR base = '' )" ;
    
    private static final String SQL_PLAREM_X_REMESA =
        "SELECT * " +
        "FROM plarem " +
        "WHERE numrem = ? " ;
    
     private static final String SQL_INFO_REPCONTROL =
        "SELECT " +
            "* " +
        "FROM " +
            "tablagen " +
        "WHERE " +
            "table_type = 'REPCONTROL' " +
        "ORDER BY " +
            "last_update DESC " ;
    
    /**
     * Crea una nueva instancia de  HiloReporteControl
     */
    public HiloReporteControl () { }
    
    /**
     * Getter for property vector_remesa.
     * @return Value of property vector_remesa.
     */
    public java.util.Vector getVector_remesa() {
        return vector_remesa;
    }
    
    /**
     * Setter for property vector_remesa.
     * @param vector_remesa New value of property vector_remesa.
     */
    public void setVector_remesa(java.util.Vector vector_remesa) {
        this.vector_remesa = vector_remesa;
    }
    
    /**
     * Getter for property vector_planilla.
     * @return Value of property vector_planilla.
     */
    public java.util.Vector getVector_planilla() {
        return vector_planilla;
    }
    
    /**
     * Setter for property vector_planilla.
     * @param vector_planilla New value of property vector_planilla.
     */
    public void setVector_planilla(java.util.Vector vector_planilla) {
        this.vector_planilla = vector_planilla;
    }
    
    /**
     * Getter for property vector_imp_remesa.
     * @return Value of property vector_imp_remesa.
     */
    public java.util.Vector getVector_imp_remesa() {
        return vector_imp_remesa;
    }
    
    /**
     * Setter for property vector_imp_remesa.
     * @param vector_imp_remesa New value of property vector_imp_remesa.
     */
    public void setVector_imp_remesa(java.util.Vector vector_imp_remesa) {
        this.vector_imp_remesa = vector_imp_remesa;
    }
    
    /**
     * Getter for property vector_imp_planilla.
     * @return Value of property vector_imp_planilla.
     */
    public java.util.Vector getVector_imp_planilla() {
        return vector_imp_planilla;
    }
    
    /**
     * Setter for property vector_imp_planilla.
     * @param vector_imp_planilla New value of property vector_imp_planilla.
     */
    public void setVector_imp_planilla(java.util.Vector vector_imp_planilla) {
        this.vector_imp_planilla = vector_imp_planilla;
    }
    
    /**
     * Getter for property vector_imp_rem_no_tan.
     * @return Value of property vector_imp_rem_no_tan.
     */
    public java.util.Vector getVector_imp_rem_no_tan() {
        return vector_imp_rem_no_tan;
    }
    
    /**
     * Setter for property vector_imp_rem_no_tan.
     * @param vector_imp_rem_no_tan New value of property vector_imp_rem_no_tan.
     */
    public void setVector_imp_rem_no_tan(java.util.Vector vector_imp_rem_no_tan) {
        this.vector_imp_rem_no_tan = vector_imp_rem_no_tan;
    }
    
    /**
     * Getter for property vector_imp_pla_no_tan.
     * @return Value of property vector_imp_pla_no_tan.
     */
    public java.util.Vector getVector_imp_pla_no_tan() {
        return vector_imp_pla_no_tan;
    }
    
    /**
     * Setter for property vector_imp_pla_no_tan.
     * @param vector_imp_pla_no_tan New value of property vector_imp_pla_no_tan.
     */
    public void setVector_imp_pla_no_tan(java.util.Vector vector_imp_pla_no_tan) {
        this.vector_imp_pla_no_tan = vector_imp_pla_no_tan;
    }
        
    /**
     * Getter for property vector_reg_status_pla.
     * @return Value of property vector_reg_status_pla.
     */
    public java.util.Vector getVector_reg_status_pla() {
        return vector_reg_status_pla;
    }
    
    /**
     * Setter for property vector_reg_status_pla.
     * @param vector_reg_status_pla New value of property vector_reg_status_pla.
     */
    public void setVector_reg_status_pla(java.util.Vector vector_reg_status_pla) {
        this.vector_reg_status_pla = vector_reg_status_pla;
    }
    
    /**
     * Getter for property vector_reg_status_rem.
     * @return Value of property vector_reg_status_rem.
     */
    public java.util.Vector getVector_reg_status_rem() {
        return vector_reg_status_rem;
    }
    
    /**
     * Setter for property vector_reg_status_rem.
     * @param vector_reg_status_rem New value of property vector_reg_status_rem.
     */
    public void setVector_reg_status_rem(java.util.Vector vector_reg_status_rem) {
        this.vector_reg_status_rem = vector_reg_status_rem;
    }
    
    
    
    public void leer( String usuario, String archivo, String fecha_inicial, String fecha_final ) {
        
        try {
            
            System.gc();
            String ruta = UtilFinanzas.obtenerRuta( "ruta", "/exportar/migracion/" + usuario );
            this.Archivo = ruta  + "/" + archivo;
            this.start ();
            System.gc();
            this.run ( usuario, fecha_inicial, fecha_final );
            System.gc();
            
        } catch ( Exception ex ) {
            
            ex.printStackTrace ();
            
        }
        
    }
    
    public void start () {
        
        this.Hoja       = "Base";
        super.start();
        
    }
    
    public synchronized void run ( String usuario, String fecha_inicial, String fecha_final ) {
        
        com.tsp.operation.model.Model modelOperation = new com.tsp.operation.model.Model();
        
        try {
              
            System.gc();
            
            modelOperation.LogProcesosSvc.InsertProceso( "Reporte de Control", this.hashCode(), "Proceso de Reporte de Control", usuario );
            
            int fr = 0, // contador fila de la remesa
            fp = 0, // contador fila de la planilla
            Fila = 6, // Se empieza a contar desde aqui.. numero de la fila
            colRemesa = 0, // numero de la columna de la remesa
            colPlanilla = 24; // numero de la columna de la planilla // 20
            
            JXLRead xls = new JXLRead ( Archivo );
            
            xls.obtenerHoja ( Hoja );
            
            int TotalFilas = xls.numeroFilas ();
            
            String contentRem;
            String contentPla;
            
            this.vector_imp_remesa = new Vector();
            this.vector_imp_planilla = new Vector();
            this.vector_imp_rem_no_tan = new Vector();
            this.vector_imp_pla_no_tan = new Vector();
            
            this.vector_reg_status_pla = new Vector();
            this.vector_reg_status_rem = new Vector();
            
            this.vector_plarem = new Vector();
            
            System.gc();
            
            /*********** PROCESO DE REMESAS ************/
            buscarRemesas ( fecha_inicial, fecha_final );
            int cont_rem = 0;
            cont_rem = getVector_remesa().size();
            
            for ( int cr = 0; cr < cont_rem; cr++ ){

                PlanDeCuentas registro = ( PlanDeCuentas ) getVector_remesa().elementAt( cr );

                String numrem = registro.getModulo1();
                String estado = registro.getModulo2();
                String n_facturable = registro.getModulo3();
                String vlrrem = registro.getModulo4();
                double num_vlrrem = Double.parseDouble( vlrrem );
                String reg_status = registro.getReg_status();
                
                fr = 0;
                int si_rem = 0;
                for ( fr = Fila; fr < TotalFilas ; fr++ ) {

                    contentRem = xls.obtenerHoja ().getCell ( colRemesa, fr ).getContents ();

                    if ( numrem.equals( contentRem ) ) {
                        //////System.out.println("numrem = contentRem.."+numrem+"="+contentRem);
                        si_rem++;
                        if ( estado.equals("AN") || estado.equals("CD") ) {
                            if ( num_vlrrem != 0 ) {
                                vector_imp_remesa.add( contentRem );
                            }
                        }

                        if ( estado.equals("NF") && n_facturable.equals("S") ) {
                            if ( num_vlrrem != 0 ) {
                                vector_imp_remesa.add( contentRem );
                            }
                        }

                    }
                    
                    if ( ( fr + 1 ) == TotalFilas ){
                        
                        if ( si_rem == 0 ){
                            
                            if ( !estado.equals("AN") && !estado.equals("CD") ) {
                                if ( !estado.equals("NF") && !n_facturable.equals("S") ) {
                                    if ( num_vlrrem != 0 ) {
                                        //////System.out.println("no se encontro!!.."+numrem);
                                        vector_imp_rem_no_tan.add( numrem );
                                        vector_reg_status_rem.add( reg_status );
                                        String s = buscarPlaRemXRemesa( numrem );
                                        vector_plarem.add( s );
                                        
                                    }   
                                }
                            }
                            
                        }
                        
                    }

                }

            }
               
            System.gc();
            
            /*********** PROCESO DE PLANILLAS ************/
            buscarPlanillas ( fecha_inicial, fecha_final );
            int cont_pla = 0; 
            cont_pla = getVector_planilla().size();
            
            for ( int cp = 0; cp < cont_pla; cp++ ){

                PlanDeCuentas registro = ( PlanDeCuentas ) getVector_planilla().elementAt( cp );

                String numpla = registro.getModulo5();
                String reg_status = registro.getModulo6();
                String vlrpla = registro.getModulo7();
                double num_vlrpla = Double.parseDouble( vlrpla );
                
                fp = 0;
                int si_pla = 0;
                for ( fp = Fila; fp < TotalFilas ; fp++ ) {

                    contentPla = xls.obtenerHoja ().getCell ( colPlanilla, fp ).getContents ();

                    if ( !contentPla.equals("ESTIMADA") ) {

                        if ( numpla.equals( contentPla ) ) {
                            
                            si_pla++;
                            if ( reg_status.equals("A") ) {
                                if ( num_vlrpla != 0 ) {
                                    vector_imp_planilla.add( contentPla );
                                }
                            }
                        }

                        if ( ( fp + 1 ) == TotalFilas ){
                            
                            if ( si_pla == 0 ){                                
                                if ( !reg_status.equals("A") ) {
                                    if ( num_vlrpla != 0 ) {
                                        
                                        vector_imp_pla_no_tan.add( numpla );
                                        vector_reg_status_pla.add( reg_status );
                                        
                                    }
                                }
                            }
                        }
                        
                    }
                    
                }

            }                
                   
            System.gc();
            
            exportar( usuario );
            
            System.gc();
            
            modelOperation.LogProcesosSvc.finallyProceso( "Reporte de Control", this.hashCode(), usuario, "PROCESO EXITOSO" );
            
        } catch ( Exception ex ) {
            
            try {
                
                ex.printStackTrace();
                modelOperation.LogProcesosSvc.finallyProceso( "Reporte de Control", this.hashCode(), usuario, "ERROR : " + ex.getMessage() );
            
            } catch ( Exception e ){
                
                e.printStackTrace();
                
            }            
            
        }
        
    }
        
    /**
     * Se encarga de buscar las remesas de la web.
     * @autor LREALES
     * @param la fecha inicial y la fecha final.
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void buscarRemesas ( String fec_ini, String fec_fin ) throws SQLException{
        
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
                
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( "sot" );
            
            if( con != null ){
                
                st = con.prepareStatement( this.SQL_REMESAS );
                
                st.setString( 1, fec_ini );         
                st.setString( 2, fec_fin );  
                
                rs = st.executeQuery();
                
                this.vector_remesa = new Vector();
                
                while ( rs.next() ){
                    
                    bean = new PlanDeCuentas ();
                    
                    String numrem = rs.getString( "numrem" ) != null ? rs.getString( "numrem" ) : "";
                    String estado = rs.getString( "estado" ) != null ? rs.getString( "estado" ) : "";
                    String n_facturable = rs.getString( "n_facturable" ) != null ? rs.getString( "n_facturable" ) : "";
                    String vlrrem = rs.getString( "vlrrem" ) != null ? rs.getString( "vlrrem" ) : "0";
                    String reg_status = rs.getString( "reg_status" ) != null ? rs.getString( "reg_status" ) : "";
                    
                    bean.setModulo1( numrem );
                    bean.setModulo2( estado );
                    bean.setModulo3( n_facturable );
                    bean.setModulo4( vlrrem );
                    bean.setReg_status( reg_status );
                    
                    vector_remesa.add ( bean );
                    
                }
                
            }
                        
        } catch( SQLException e ){
            
            e.printStackTrace();
            throw new SQLException( "ERROR EN 'buscarRemesas'.. " + e.getMessage() + " - " + e.getErrorCode() );
        
        }
        
        finally{
            
            if ( st != null ){
                try{
                    st.close();
                } catch( SQLException e ){
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO " + e.getMessage() );
                }
            }            
            if ( con != null ){
                poolManager.freeConnection( "sot", con );
            }
            
        }
        
    }
    
    /**
     * Se encarga de buscar las planillas de la web.
     * @autor LREALES
     * @param la fecha inicial y la fecha final.
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void buscarPlanillas ( String fec_ini, String fec_fin ) throws SQLException{
        
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
                
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( "sot" );
            
            if( con != null ){
                
                st = con.prepareStatement( this.SQL_PLANILLAS );
                
                st.setString( 1, fec_ini );         
                st.setString( 2, fec_fin );  
                
                rs = st.executeQuery();
                
                this.vector_planilla = new Vector();
                
                while ( rs.next() ){
                    
                    bean = new PlanDeCuentas ();
                    
                    String numpla = rs.getString( "numpla" ) != null ? rs.getString( "numpla" ) : "";
                    String reg_status = rs.getString( "reg_status" ) != null ? rs.getString( "reg_status" ) : "";
                    String vlrpla = rs.getString( "vlrpla" ) != null ? rs.getString( "vlrpla" ) : "0";
                    
                    bean.setModulo5( numpla );
                    bean.setModulo6( reg_status );
                    bean.setModulo7( vlrpla );
                    
                    vector_planilla.add ( bean );
                    
                }
                
            }
                        
        } catch( SQLException e ){
            
            e.printStackTrace();
            throw new SQLException( "ERROR EN 'buscarPlanillas'.. " + e.getMessage() + " - " + e.getErrorCode() );
        
        }
        
        finally{
            
            if ( st != null ){
                try{
                    st.close();
                } catch( SQLException e ){
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO " + e.getMessage() );
                }
            }            
            if ( con != null ){
                poolManager.freeConnection( "sot", con );
            }
            
        }
        
    }
    
    /**
     * Se encarga de buscar las planillas en plarem por una remesa de la web.
     * @autor LREALES
     * @param la remesa.
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public String buscarPlaRemXRemesa ( String remesa ) throws SQLException{
        
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
             
        String planillas = "";
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( "sot" );
            
            if( con != null ){
                
                st = con.prepareStatement( this.SQL_PLAREM_X_REMESA );
                
                st.setString( 1, remesa );
                
                rs = st.executeQuery();
                
                while ( rs.next() ){
                    
                    String numpla = rs.getString( "numpla" ) != null ? rs.getString( "numpla" ) : "";
                    String reg_status = rs.getString( "reg_status" ) != null ? rs.getString( "reg_status" ) : "";  
                    
                    planillas += numpla + " " + reg_status + ",";     
                    
                }
                
            }
                        
        } catch( SQLException e ){
            
            e.printStackTrace();
            throw new SQLException( "ERROR EN 'buscarPlaRemXRemesa'.. " + e.getMessage() + " - " + e.getErrorCode() );
        
        }
        
        finally{
            
            if ( st != null ){
                try{
                    st.close();
                } catch( SQLException e ){
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO " + e.getMessage() );
                }
            }            
            if ( con != null ){
                poolManager.freeConnection( "sot", con );
            }
            
        }
        
        String validacion_planillas = planillas.length() <= 0 ? "-" : planillas.substring( 0, planillas.length() - 1 );
        
        return validacion_planillas;
        
    }
    
    public synchronized void exportar( String usuario ) {
        
        try{
                        
            Util u = new Util();
            
            ResourceBundle rb = ResourceBundle.getBundle( "com/tsp/util/connectionpool/db" );
            String path = rb.getString( "ruta" );
                        
            File file = new File( path + "/exportar/migracion/" + usuario );
            file.mkdirs();
               
            String fecha = Util.getFechaActual_String(6);            
            fecha=fecha.replaceAll( "/", "-" );
            fecha=fecha.replaceAll( ":", "_" );
            
            String nombreArch  = "ReporteControl[" + fecha + "].xls";
            String       Hoja  = "ReporteControl";
            String       Ruta  = path + "/exportar/migracion/" + usuario + "/" +nombreArch; 
            
            HSSFWorkbook wb    = new HSSFWorkbook();
            HSSFSheet    sheet = wb.createSheet( Hoja );
            HSSFRow      row   = null;
            HSSFRow      row2  = null;
            HSSFCell     cell  = null;
            
            for ( int col = 0; col < 7 ; col++ ){
                
                sheet.setColumnWidth( (short) col, (short) ( ( 50 * 8 ) / ( (double) 1 / 18 ) ) ); 
                
            }
            
            /****  ENCABEZADO Y DEFINICION DE ESTILOS ************************************************/
            
            /** ENCABEZADO GENERAL *******************************/
            HSSFFont  fuente1 = wb.createFont();
            fuente1.setFontName("verdana");
            fuente1.setFontHeightInPoints((short)(16)) ;
            fuente1.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            fuente1.setColor(HSSFColor.DARK_RED.index);
            
            HSSFCellStyle estilo1 = wb.createCellStyle();
            estilo1.setFont(fuente1);
            estilo1.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo1.setBottomBorderColor(HSSFColor.WHITE.index);
            estilo1.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo1.setLeftBorderColor  (HSSFColor.WHITE.index);
            estilo1.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo1.setRightBorderColor(HSSFColor.WHITE.index);
            estilo1.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo1.setTopBorderColor   (HSSFColor.WHITE.index);
            
            
            /** SUBTITULO *******************************/
            HSSFFont  fuenteX = wb.createFont();
            fuenteX.setFontName("verdana");
            fuenteX.setFontHeightInPoints((short)(11)) ;
            fuenteX.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            fuenteX.setColor(HSSFColor.DARK_RED.index);
            
            HSSFCellStyle estiloX = wb.createCellStyle();
            estiloX.setFont(fuenteX);
            estiloX.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estiloX.setBottomBorderColor(HSSFColor.WHITE.index);
            estiloX.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estiloX.setLeftBorderColor  (HSSFColor.WHITE.index);
            estiloX.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estiloX.setRightBorderColor(HSSFColor.WHITE.index);
            estiloX.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estiloX.setTopBorderColor   (HSSFColor.WHITE.index);
            
            /** TEXTO EN EL ENCABEAZADO *************************/
            HSSFFont  fuente2 = wb.createFont();
            fuente2.setFontName("verdana");
            fuente2.setFontHeightInPoints((short)(11)) ;
            fuente2.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            fuente2.setColor((short)(0x0));
            
            HSSFCellStyle estilo2 = wb.createCellStyle();
            estilo2.setFont(fuente2);
            estilo2.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo2.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            
            /** ENCABEZADO DE LAS COLUMNAS***********************/
            HSSFFont  fuente3 = wb.createFont();
            fuente3.setFontName("verdana");
            fuente3.setFontHeightInPoints((short)(9)) ;
            fuente3.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            fuente3.setColor(HSSFColor.BLACK.index);
            
            HSSFCellStyle estilo3 = wb.createCellStyle();
            estilo3.setFont(fuente3);
            estilo3.setFillForegroundColor(HSSFColor.GREY_25_PERCENT.index);
            estilo3.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo3.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo3.setBottomBorderColor(HSSFColor.BLACK.index);
            estilo3.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo3.setLeftBorderColor  (HSSFColor.BLACK.index);
            estilo3.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo3.setRightBorderColor(HSSFColor.BLACK.index);
            estilo3.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo3.setTopBorderColor   (HSSFColor.BLACK.index);
            estilo3.setAlignment(HSSFCellStyle.ALIGN_CENTER);            
                        
            /** TEXTO NORMAL ************************************/
            HSSFFont  fuente4 = wb.createFont();
            fuente4.setFontName("verdana");
            fuente4.setFontHeightInPoints((short)(9)) ;
            fuente4.setBoldweight(HSSFFont.BOLDWEIGHT_NORMAL);
            fuente4.setColor((short)(0x0));
            
            HSSFCellStyle estilo4 = wb.createCellStyle();
            estilo4.setFont(fuente4);
            estilo4.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo4.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo4.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo4.setBottomBorderColor(HSSFColor.BLACK.index);
            estilo4.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo4.setLeftBorderColor  (HSSFColor.BLACK.index);
            estilo4.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo4.setRightBorderColor(HSSFColor.BLACK.index);
            estilo4.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo4.setTopBorderColor   (HSSFColor.BLACK.index);
            
            /** NUMEROS ************************************/
            HSSFCellStyle estilo5 = wb.createCellStyle();
            estilo5.setFont(fuente4);
            estilo5.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo5.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo5.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo5.setBottomBorderColor(HSSFColor.BLACK.index);
            estilo5.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo5.setLeftBorderColor  (HSSFColor.BLACK.index);
            estilo5.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo5.setRightBorderColor(HSSFColor.BLACK.index);
            estilo5.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo5.setTopBorderColor   (HSSFColor.BLACK.index);
            estilo5.setAlignment(HSSFCellStyle.ALIGN_RIGHT);
            /****************************************************/
            
            HSSFFont fuente6 = wb.createFont();
            fuente6.setColor((short)0x0);  
            fuente6.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            
            HSSFCellStyle estilo6 = wb.createCellStyle();
            estilo6.setFont(fuente6);
            estilo6.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo6.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo6.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo6.setBottomBorderColor(HSSFColor.WHITE.index);
            estilo6.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo6.setLeftBorderColor  (HSSFColor.WHITE.index);
            estilo6.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo6.setRightBorderColor(HSSFColor.WHITE.index);
            estilo6.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo6.setTopBorderColor   (HSSFColor.WHITE.index);
            
            /****************************************************/  
            /* MONEDA */
            HSSFCellStyle estilo7 = wb.createCellStyle();
            estilo7.setFont(fuente4);
            estilo7.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo7.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo7.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo7.setBottomBorderColor(HSSFColor.BLACK.index);
            estilo7.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo7.setLeftBorderColor  (HSSFColor.BLACK.index);
            estilo7.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo7.setRightBorderColor(HSSFColor.BLACK.index);
            estilo7.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo7.setTopBorderColor   (HSSFColor.BLACK.index);
            estilo7.setAlignment(HSSFCellStyle.ALIGN_RIGHT);
            estilo7.setDataFormat(wb.createDataFormat().getFormat("$#,##0.00"));
            
            /* TEXTO NORMAL CENTRADO */
            HSSFCellStyle estilo9 = wb.createCellStyle();
            estilo9.setFont(fuente4);
            estilo9.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo9.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo9.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo9.setBottomBorderColor(HSSFColor.BLACK.index);
            estilo9.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo9.setLeftBorderColor  (HSSFColor.BLACK.index);
            estilo9.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo9.setRightBorderColor(HSSFColor.BLACK.index);
            estilo9.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo9.setTopBorderColor   (HSSFColor.BLACK.index);
            estilo9.setAlignment(HSSFCellStyle.ALIGN_CENTER);
            
            row  = sheet.createRow((short)(0));
            row  = sheet.createRow((short)(1));
            row  = sheet.createRow((short)(2));
            row  = sheet.createRow((short)(3));
            row  = sheet.createRow((short)(4));
            row  = sheet.createRow((short)(5));
            row  = sheet.createRow((short)(6));
            row  = sheet.createRow((short)(7));
            row  = sheet.createRow((short)(8));
            row  = sheet.createRow((short)(9));
            
            for ( int j = 0; j < 7; j++ ) {
                
                row  = sheet.getRow((short)(0));
                cell = row.createCell((short)(j));
                cell.setCellStyle(estilo1);
                row  = sheet.getRow((short)(1));
                cell = row.createCell((short)(j)); 
                cell.setCellStyle(estiloX);
                
            }
            
            row  = sheet.getRow((short)(0));
            cell = row.getCell((short)(0));
            cell.setCellValue("TRANSPORTES SANCHEZ POLO");
                        
            row  = sheet.getRow((short)(1));            
            cell = row.getCell((short)(0));            
            cell.setCellValue("Reporte de Control");
            
            for( int i = 2; i < 5; i++ ){
                 
                for ( int j = 0; j < 7; j++ ) {
                    
                    row  = sheet.getRow((short)(i));
                    cell = row.createCell((short)(j)); 
                    cell.setCellStyle(estilo6);
                    
                }      
                
            }
            
            //FECHA
            row = sheet.getRow((short)(3));  
            cell = row.createCell((short)(0));            
            cell = row.getCell((short)(0));  
            cell.setCellStyle(estilo6);
            cell.setCellValue("FECHA DEL REPORTE:");
            cell = row.createCell((short)(1));            
            cell = row.getCell((short)(1));            
            cell.setCellStyle(estilo6);
            cell.setCellValue(Util.getFechaActual_String(7));
                      
            /*************************************************************************************/
            /***** RECORRER LOS DATOS ******/
            int Fila_Planilla = 5;            
            for ( int vp = 0; vp < vector_imp_planilla.size(); vp++ ){
                
                String numpla = ( String ) vector_imp_planilla.elementAt( vp );
                
                Fila_Planilla++;
                row  = sheet.createRow( (short)(Fila_Planilla) );
                
                cell = row.createCell( (short)(0) );//
                cell.setCellStyle( estilo4 );
                cell.setCellValue( numpla );
                                
            }//end for datos
            
            int Fila_Pla_no_tan = 5;            
            for ( int vpnt = 0; vpnt < vector_imp_pla_no_tan.size(); vpnt++ ){
                
                String numpla_no_tan = ( String ) vector_imp_pla_no_tan.elementAt( vpnt );
                
                Fila_Pla_no_tan++;
                row  = sheet.createRow( (short)(Fila_Pla_no_tan) );
                
                cell = row.createCell( (short)(1) );//
                cell.setCellStyle( estilo4 );
                cell.setCellValue( numpla_no_tan );
                                
            }//end for datos
            
            int Fila_Reg_Status_Pla_no_tan = 5;            
            for ( int rsp = 0; rsp < vector_reg_status_pla.size(); rsp++ ){
                
                String reg_status_pla = ( String ) vector_reg_status_pla.elementAt( rsp );
                
                Fila_Reg_Status_Pla_no_tan++;
                row  = sheet.createRow( (short)(Fila_Reg_Status_Pla_no_tan) );
                
                cell = row.createCell( (short)(2) );//
                cell.setCellStyle( estilo4 );
                cell.setCellValue( reg_status_pla );
                                
            }//end for datos
            
            int Fila_Remesa = 5;
            for ( int vr = 0; vr < vector_imp_remesa.size(); vr++ ){
                
                String numrem = ( String ) vector_imp_remesa.elementAt( vr );
                
                Fila_Remesa++;
                row  = sheet.createRow( (short)(Fila_Remesa) );
                
                cell = row.createCell( (short)(3) );//
                cell.setCellStyle( estilo4 );
                cell.setCellValue( numrem );
                                
            }//end for datos
            
            int Fila_Rem_no_tan = 5;
            for ( int vrnt = 0; vrnt < vector_imp_rem_no_tan.size(); vrnt++ ){
                
                String numrem_no_tan = ( String ) vector_imp_rem_no_tan.elementAt( vrnt );
                
                Fila_Rem_no_tan++;
                row  = sheet.createRow( (short)(Fila_Rem_no_tan) );
                
                cell = row.createCell( (short)(4) );//
                cell.setCellStyle( estilo4 );
                cell.setCellValue( numrem_no_tan );
                                
            }//end for datos

            int Fila_Reg_Status_Rem_no_tan = 5;
            for ( int rsr = 0; rsr < vector_reg_status_rem.size(); rsr++ ){
                
                String reg_status_rem = ( String ) vector_reg_status_rem.elementAt( rsr );
                
                Fila_Reg_Status_Rem_no_tan++;
                row  = sheet.createRow( (short)(Fila_Reg_Status_Rem_no_tan) );
                
                cell = row.createCell( (short)(5) );//
                cell.setCellStyle( estilo4 );
                cell.setCellValue( reg_status_rem );
                                
            }//end for datos
            
            int Fila_PlaRem = 5;
            for ( int pr = 0; pr < vector_plarem.size(); pr++ ){
                
                String plarem = ( String ) vector_plarem.elementAt( pr );
                
                Fila_PlaRem++;
                row  = sheet.createRow( (short)(Fila_PlaRem) );
                
                cell = row.createCell( (short)(6) );//
                cell.setCellStyle( estilo4 );
                cell.setCellValue( plarem );
                                
            }//end for datos
            
            row = sheet.createRow((short)(5));
            
            row = sheet.getRow( (short)(5) );     
            
            cell = row.createCell((short)(0));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Planillas Erroneas");
            
            cell = row.createCell((short)(1));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Planillas que deberian estar");
            
            cell = row.createCell((short)(2));
            cell.setCellStyle(estilo3);
            cell.setCellValue("=> Reg_Status Planilla");
            
            cell = row.createCell((short)(3));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Remesas Erroneas");
            
            cell = row.createCell((short)(4));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Remesas que deberian estar");
            
            cell = row.createCell((short)(5));
            cell.setCellStyle(estilo3);
            cell.setCellValue("=> Reg_Status Remesa");
            
            cell = row.createCell((short)(6));
            cell.setCellStyle(estilo3);
            cell.setCellValue("==> Planilla(s) x Remesa");
            
            /*************************************************************************************/
            /**** GUARDAR DATOS EN EL ARCHIVO  ***/
            FileOutputStream fo = new FileOutputStream( Ruta );
            wb.write( fo );
            fo.close();
                    
        } catch( Exception e ){    
            
            e.printStackTrace();
            
        }
                
    }
    //Lisset 03-oct-2006
        /* LREALES - CREADO 01 OCTUBRE 2006 */
   
    

    /* LREALES - CREADO 01 OCTUBRE 2006 */
    
    /**
     * Se encarga de buscar el login del usuario para el reporte de control
     * @autor LREALES
     * @param -
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public String loginReporteControl () throws SQLException {
        
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        String table_code = "";
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( "sot" );
            
            if( con != null ){
                
                st = con.prepareStatement( this.SQL_INFO_REPCONTROL );
                                
                rs = st.executeQuery();
                
                if ( rs.next() ){
                    
                    table_code = rs.getString( "table_code" ) != null ? rs.getString( "table_code" ) : "";
                          
                }
                
            }
                        
        } catch( SQLException e ){
            
            e.printStackTrace();
            throw new SQLException( "ERROR EN 'infoReporteControl'.. " + e.getMessage() + " - " + e.getErrorCode() );
        
        }
        
        finally{
            
            if ( st != null ){
                try{
                    st.close();
                } catch( SQLException e ){
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO " + e.getMessage() );
                }
            }            
            if ( con != null ){
                poolManager.freeConnection( "sot", con );
            }
            
        }
        
        return table_code;
        
    }
    
    /**
     * Se encarga de buscar el nombre del archivo para el reporte de control
     * @autor LREALES
     * @param -
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public String archivoReporteControl () throws SQLException {
        
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        String referencia = "";
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( "sot" );
            
            if( con != null ){
                
                st = con.prepareStatement( this.SQL_INFO_REPCONTROL );
                                
                rs = st.executeQuery();
                
                if ( rs.next() ){
                    
                    referencia = rs.getString( "referencia" ) != null ? rs.getString( "referencia" ) : "";
                          
                }
                
            }
                        
        } catch( SQLException e ){
            
            e.printStackTrace();
            throw new SQLException( "ERROR EN 'archivoReporteControl'.. " + e.getMessage() + " - " + e.getErrorCode() );

        }
        
        finally{
            
            if ( st != null ){
                try{
                    st.close();
                } catch( SQLException e ){
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO " + e.getMessage() );
                }
            }            
            if ( con != null ){
                poolManager.freeConnection( "sot", con );
            }
            
        }
        
        return referencia;
        
    }

   /* LREALES - MODIFICADO 05 OCTUBRE 2006 */
    public static void main ( String[]abc ) {
        
        try {
            /* LREALES - 01 OCTUBRE 2006 */
            HiloReporteControl h = new HiloReporteControl ();
            System.gc();
            String login_usuario_rep_control = h.loginReporteControl();
            System.gc();
            String nombreArchivo_rep_control = h.archivoReporteControl();
            System.gc();
            /* LREALES - 05 OCTUBRE 2006 */
            String fecha_inicial = "";
            String fecha_final = "";
            
            if ( !nombreArchivo_rep_control.equals("") ) {
                
                fecha_inicial = nombreArchivo_rep_control.substring(16,24);// fin de mes
                fecha_final = nombreArchivo_rep_control.substring(25,33);//  inicio de mes  
                nombreArchivo_rep_control = nombreArchivo_rep_control + ".xls";
                System.gc();
                h.leer ( login_usuario_rep_control, nombreArchivo_rep_control, fecha_inicial, fecha_final );
                System.gc();
                
            } else {
                
                //System.out.println("El proceso de Reporte de Control no pudo ejecutarse por inconsistencias en el Reporte de Utilidad.");
                
            }
                
        } catch ( Exception ex ){
            
            ex.printStackTrace ();
            //System.out.println( "Error en main( String[]abc ) --> " + ex.getMessage () );
            
        }
        
    }
    
}