/****************************************************************************
 *  Nombre Clase.................   CierreMensualMOC.java                   *
 *  Descripci�n..................   se encarga de cerrar mensualmente los   *
 *                                  modulos operativos y contables.         *
 *  Autor........................   LREALES                                 *
 *  Fecha........................   28.06.2006                              *
 *  Versi�n......................   1.0                                     *
 *  Copyright....................   Transportes Sanchez Polo S.A.           *
 ****************************************************************************/

package com.tsp.util.Contabilidad;

import com.tsp.finanzas.contab.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;
import java.sql.*;
import java.util.*;
import com.tsp.util.*;
import com.tsp.finanzas.contab.model.threads.*;
import com.tsp.operation.model.beans.Usuario;

public class CierreMensualMOC {
    
    private Vector vector;
    
    private static final String SQL_BUSCAR = 
            "SELECT "+
                "* "+
            "FROM "+
                "con.periodo_contable "+
            "WHERE "+
                "dstrct = ? "+
                "AND anio = ? "+
                "AND mes = ? "+
                "AND reg_status != 'A'";
           
    private static final String SQL_CERRAR = 
            "UPDATE "+
                "con.periodo_contable "+
            "SET "+
                "ac = 'C', last_update = 'now()', user_update = ? "+
            "WHERE "+
                "dstrct = ? "+
                "AND anio = ? "+
                "AND mes = ?";
                
    private String variable = "";
    
    private String distrito = "FINV";
    private String anio = "";// calcular
    private String mes = "";// calcular
    private Usuario user;
    
    /** Creates a new instance of CierreMensualMOC */
    public CierreMensualMOC() {
    }
    public CierreMensualMOC(Usuario usuario) {
        this.user = usuario;
    }
    
    /**
     * Getter for property vector.
     * @return Value of property vector.
     */
    public java.util.Vector getVector() {
        return vector;
    }
    
    /**
     * Setter for property vector.
     * @param vector New value of property vector.
     */
    public void setVector(java.util.Vector vector) {
        this.vector = vector;
    } 
    
    public static void main ( String[] args ) throws Exception {
        
        try{
            
            CierreMensualMOC cmmoc = new CierreMensualMOC();
            cmmoc.run();
            
        } catch ( Exception e ){ 
            
            e.printStackTrace();
            throw new Exception( e.getMessage() );
            
        }
        
    }
    
    public void run() throws Exception {
        
        com.tsp.operation.model.Model modelOperation = new com.tsp.operation.model.Model(user.getBd());
        
        try{
            
            modelOperation.LogProcesosSvc.InsertProceso( "Cierre Mensual de Modulos Operativos y Contables", this.hashCode(), "Proceso de Cierre Mensual de los Modulos Operativos y Contables", "ADMIN" );
            
            String fecha = Utility.getHoy( "-" );
            anio = fecha.substring( 0, 4 );
            mes = fecha.substring( 5, 7 );

            int anio_int = Integer.parseInt ( anio );
            int mes_int = Integer.parseInt ( mes );

            if ( mes_int == 1 ){

                mes_int = 12;
                anio_int = anio_int - 1;

            } else{

                mes_int = mes_int - 1;

            }

            mes = "" + mes_int;
            anio = "" + anio_int;

            boolean validar = false;

            validar = existePeriodo ();

            if ( validar == true ){

                cerrarPeriodo ();

            }
            
            InterfaceCierreMensualMOC icmmoc = new InterfaceCierreMensualMOC(this.user);
            icmmoc.start();
            
            modelOperation.LogProcesosSvc.finallyProceso( "Cierre Mensual de Modulos Operativos y Contables", this.hashCode(), "ADMIN", "PROCESO EXITOSO" );
        
        } catch ( Exception e ){            
            
            try {
                
                modelOperation.LogProcesosSvc.finallyProceso( "Cierre Mensual de Modulos Operativos y Contables", this.hashCode(), "ADMIN", "ERROR : " + e.getMessage() );
            
            } catch ( Exception ex ){
                
                ex.printStackTrace();
                
            }
            
        }
        
    }
  
    /**
     * Metodo:          existePeriodo
     * Descripcion :    M�todo que permite verificar si existe o no un periodo.
     * @autor :         LREALES
     * @param:          distrito, a�o del periodo y mes del periodo.
     * @return:         un switch tipo boolean
     * @throws:         SQLException
     * @version :       1.0
     */
    public boolean existePeriodo ()throws SQLException{
        
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        boolean sw = false;
        
        mes = mes.equals("1")?"01":mes;
        mes = mes.equals("2")?"02":mes;
        mes = mes.equals("3")?"03":mes;
        mes = mes.equals("4")?"04":mes;
        mes = mes.equals("5")?"05":mes;
        mes = mes.equals("6")?"06":mes;
        mes = mes.equals("7")?"07":mes;
        mes = mes.equals("8")?"08":mes;
        mes = mes.equals("9")?"09":mes;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( "sot" );
            
            if( con != null ){
                
                st = con.prepareStatement( this.SQL_BUSCAR );
                
                st.setString ( 1, distrito );
                st.setString ( 2, anio );
                st.setString ( 3, mes );
                
                rs = st.executeQuery();
                            
                if ( rs.next () ){
                    sw = true;
                }   
                
            }
            
        } catch( SQLException e ){
            
            e.printStackTrace();
            throw new SQLException( "ERROR DURANTE 'existePeriodo' - [CierreMensualMOC].. " + e.getMessage() + " " + e.getErrorCode() );
            
        }
        
        finally{
            
            if ( st != null ){
                try{
                    st.close ();
                } catch( SQLException e ){
                    throw new SQLException ( "ERROR CERRANDO EL ESTAMENTO: " + e.getMessage () );
                }
            }
            if ( con != null ){
                poolManager.freeConnection( "sot", con );
            }
            
        }
        
        return sw;
        
    }
    
    /**
     * Metodo:          cerrarPeriodo
     * Descripcion :    M�todo que permite cerrar un periodo.
     * @autor :         LREALES
     * @param:          -
     * @return:         -
     * @throws:         SQLException
     * @version :       1.0
     */
    public void cerrarPeriodo () throws SQLException{
        
        Connection con = null;
        PreparedStatement st = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( "sot" );
            
            if( con != null ){
                
                st = con.prepareStatement( this.SQL_CERRAR );
            
                st.setString ( 1, user.getLogin() );
                st.setString ( 2, distrito );
                st.setString ( 3, anio );
                st.setString ( 4, mes );
                
                st.executeUpdate ();
                
            }

        } catch( SQLException e ){
            
            e.printStackTrace();
            throw new SQLException( "ERROR DURANTE 'cerrarPeriodo' - [CierreMensualMOC].. " + e.getMessage() + " " + e.getErrorCode() );
            
        }
        
        finally{
            
            if ( st != null ){
                try{
                    st.close ();
                } catch( SQLException e ){
                    throw new SQLException ( "ERROR CERRANDO EL ESTAMENTO: " + e.getMessage () );
                }
            }
            if ( con != null ){
                poolManager.freeConnection( "sot", con );
            }
            
        }
        
    }
                    
}