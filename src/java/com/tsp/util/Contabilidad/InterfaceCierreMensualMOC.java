/****************************************************************************
 *  Nombre Clase.................   InterfaceCierreMensualMOC.java          *
 *  Descripci�n..................   se encarga de cerrar mensualmente los   *
 *                                  modulos operativos y contables.         *
 *  Autor........................   LREALES                                 *
 *  Fecha........................   28.06.2006                              *
 *  Versi�n......................   1.0                                     *
 *  Copyright....................   Transportes Sanchez Polo S.A.           *
 ****************************************************************************/

package com.tsp.util.Contabilidad;

import com.tsp.finanzas.contab.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;
import java.sql.*;
import java.util.*;
import com.tsp.util.*;
import com.tsp.finanzas.contab.model.threads.*;
import com.tsp.operation.model.beans.Usuario;

public class InterfaceCierreMensualMOC extends Thread {
    private Usuario usuario;
    
    /** Creates a new instance of InterfaceCierreMensualMOC */
    public InterfaceCierreMensualMOC() {
    }
        
    public InterfaceCierreMensualMOC(Usuario usuario) {
        this.usuario = usuario;
    }
        
    public static void main ( String[] args ) throws Exception {
        
        try{
            
            InterfaceCierreMensualMOC icmmoc = new InterfaceCierreMensualMOC();
            icmmoc.run();
            
        } catch ( Exception e ){ 
            
            e.printStackTrace();
            throw new Exception( e.getMessage() );
            
        }
        
    }
        
    public void run() {
        
        com.tsp.operation.model.Model modelOperation = new com.tsp.operation.model.Model(usuario.getBd());
        
        try{
            
            modelOperation.LogProcesosSvc.InsertProceso( "Cierre Mensual de Modulos Operativos y Contables", this.hashCode(), "Proceso de Cierre Mensual de los Modulos Operativos y Contables", usuario.getLogin() );
            
            ValidacionMovimiento vm = new ValidacionMovimiento();
            vm.start( usuario, "", "" );
            boolean validar_finalizo = vm.isValidar_finalizo();
            
            if ( validar_finalizo == true ){
                
                boolean validar_movimiento = vm.isValidar_movimiento();
                
                if ( validar_movimiento == true ){
                    
                    ActualizacionMayorizacion am = new ActualizacionMayorizacion();
                   // am.start( "ADMIN", "", "" );
                    
                } 
                
            }
                
            modelOperation.LogProcesosSvc.finallyProceso( "Cierre Mensual de Modulos Operativos y Contables", this.hashCode(), "ADMIN", "PROCESO EXITOSO" );
            
        } catch ( Exception e ){
            
            try {
                
                modelOperation.LogProcesosSvc.finallyProceso( "Cierre Mensual de Modulos Operativos y Contables", this.hashCode(), "ADMIN", "ERROR : " + e.getMessage() );
            
            } catch ( Exception ex ){
                
                ex.printStackTrace();
                
            }
            
        }
        
    }

}