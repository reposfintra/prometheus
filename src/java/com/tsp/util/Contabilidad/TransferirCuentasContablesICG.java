/****************************************************************************
 *  Nombre Clase.................   TransferirCuentasContablesICG.java      *
 *  Descripci�n..................   se encarga de migrar los datos de mims  *
 *                                  ( msf960 ) a la web ( con.cuentas )     *
 *  Autor........................   LREALES                                 *
 *  Fecha........................   23.06.2006                              *
 *  Versi�n......................   1.0                                     *
 *  Copyright....................   Transportes Sanchez Polo S.A.           *
 ****************************************************************************/

package com.tsp.util.Contabilidad;

import com.tsp.util.connectionpool.PoolManager;
import java.sql.*;
import java.util.*;

public class TransferirCuentasContablesICG {
    
    private Vector vector;
    
    private static final String SQL_MSF960 = 
            "SELECT DISTINCT "+
                    "ACCOUNT_CODE, "+
                    "DSTRCT_CODE "+
            "FROM "+
                    "MSF960 "+
            "WHERE "+
                    "DSTRCT_CODE = 'FINV' "+
                    "AND CURRENCY_IND = 'L' "+
                    "AND CURRENCY = 'PES' "+
                    "AND POSTING_TYPE = '00' "+
                    "AND SUBSTR ( ACCOUNT_CODE, 1, 1 ) IN ( 'I', 'C', 'G' ) "+
                    "AND CCYY_IND LIKE '%' ";
           
    private static final String SQL_LIMPIAR = 
            "DELETE "+
            "FROM "+
                    "con.cuentas "+
            "WHERE "+
                    "SUBSTRING ( cuenta FROM 0 FOR 2 ) = 'I' "+
                    "OR SUBSTRING ( cuenta FROM 0 FOR 2 ) = 'C' "+
                    "OR SUBSTRING ( cuenta FROM 0 FOR 2 ) = 'G'";
    
    private static final String SQL_CUENTAS = 
            "INSERT INTO con.cuentas "+
                    "( reg_status, dstrct, cuenta, nombre_largo, nombre_corto,"+
                    "nombre_observacion, fin_periodo, auxiliar, activa, base,"+
                    "modulo1, modulo2, modulo3, modulo4, modulo5,"+
                    "modulo6, modulo7, modulo8, modulo9, modulo10,"+
                    "cta_dependiente, nivel, cta_cierre, subledger, tercero,"+
                    "last_update, user_update, creation_date, creation_user, detalle ) "+
            "VALUES "+
                    "( ?, ?, ?, ?, ?,"+
                      "?, ?, ?, ?, ?,"+
                      "?, ?, ?, ?, ?,"+
                      "?, ?, ?, ?, ?,"+
                      "?, ?, ?, ?, ?,"+
                      "?, ?, ?, ?, ? )";
                
    private String reg_status = "";
    //private String dstrct = "";
    //private String cuenta = "";
    private String nombre_largo = "";
    private String nombre_corto = "";
    private String nombre_observacion = "";
    private String fin_periodo = "";
    private String auxiliar = "N";
    private String activa = "S";
    private String base = "COL";
    private String modulo1 = "N";
    private String modulo2 = "N";
    private String modulo3 = "N";
    private String modulo4 = "N";
    private String modulo5 = "N";
    private String modulo6 = "N";
    private String modulo7 = "N";
    private String modulo8 = "N";
    private String modulo9 = "N";
    private String modulo10 = "N";
    private String cta_dependiente = "";
    private String nivel = "1";
    private String cta_cierre = "";
    private String subledger = "N";
    private String tercero = "N";
    private String last_update = "now()";
    private String user_update = "ADMIN";
    private String creation_date = "now()";
    private String creation_user = "ADMIN";
    private String detalle = "S";
    
    /** Creates a new instance of TransferirCuentasContablesICG */
    public TransferirCuentasContablesICG() {
    }
    
    public static void main ( String[] args ) throws Exception {
        
        try{
            
            TransferirCuentasContablesICG tcc = new TransferirCuentasContablesICG();
            tcc.run();
            
        } catch ( Exception e ){ 
            
            e.printStackTrace();
            throw new Exception( e.getMessage() );
            
        }
        
    }
    
    public void run() throws Exception {
        
        try{
           
            limpiar();
            
            msf960();
            
            String caracter_prueba = "";
            String caracter1 = "";
                    
            int cont = getVector().size();
            
            for ( int i = 0; i < cont; i++ ){
                
                Hashtable ht = ( Hashtable ) vector.get( i );
                
                String cue = ( String ) ht.get( "cuenta" );
                String dis = ( String ) ht.get( "distrito" );
                 
                ht.put( "nombre_largo", cue );
                ht.put( "nombre_corto", cue );
                ht.put( "activa", activa );
                ht.put( "detalle", detalle );              
                ht.put( "cta_dependiente", cta_dependiente );
                ht.put( "nivel", nivel );
                ht.put( "cta_cierre", cta_cierre );               
                ht.put( "reg_status", reg_status );
                ht.put( "nombre_observacion", nombre_observacion );
                ht.put( "fin_periodo", fin_periodo );
                ht.put( "auxiliar", auxiliar );
                ht.put( "base", base );
                ht.put( "modulo1", modulo1 );
                ht.put( "modulo2", modulo2 );
                ht.put( "modulo3", modulo3 );
                ht.put( "modulo4", modulo4 );
                ht.put( "modulo5", modulo5 );
                ht.put( "modulo6", modulo6 );
                ht.put( "modulo7", modulo7 );
                ht.put( "modulo8", modulo8 );
                ht.put( "modulo9", modulo9 );
                ht.put( "modulo10", modulo10 );
                ht.put( "subledger", subledger );
                ht.put( "tercero", tercero );
                ht.put( "last_update", last_update );
                ht.put( "user_update", user_update );
                ht.put( "creation_date", creation_date );
                ht.put( "creation_user", creation_user );
                                
                this.vector.add( ht );
                
                transferir ( ht );
                
            }
                
        } catch ( Exception e ){
            e.printStackTrace();
            throw new Exception( e.getMessage() );
        }
        
    }
    /**
     * Getter for property vector.
     * @return Value of property vector.
     */
    public java.util.Vector getVector() {
        return vector;
    }
    
    /**
     * Setter for property vector.
     * @param vector New value of property vector.
     */
    public void setVector(java.util.Vector vector) {
        this.vector = vector;
    }    
    
    /**
     * Se encarga de limpiar las cuentas I, C, y G de la tabla 'cuentas' 
     * del esquema 'con' en la web.
     * @autor LREALES
     * @param -
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void limpiar () throws SQLException{
        
        Connection con = null;
        PreparedStatement st = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( "sot" );
            
            if( con != null ){
                
                st = con.prepareStatement( this.SQL_LIMPIAR );
                
                st.execute();
                
            }
                        
        } catch( SQLException e ){
            
            e.printStackTrace();
            throw new SQLException( "ERROR EN 'limpiar' - [TransferirCuentasContablesICG].. " + e.getMessage() + " " + e.getErrorCode() );
        
        }
        
        finally{
            
            if ( st != null ){
                try{
                    st.close();
                } catch( SQLException e ){
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO " + e.getMessage() );
                }
            }            
            if ( con != null ){
                poolManager.freeConnection( "sot", con );
            }
            
        }
        
    }
    
    /**
     * Obtiene una lista de las cuentas contables I, C, y G en mims.
     * @autor LREALES
     * @param -
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void msf960 () throws SQLException{
        
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( "oracle" );
            
            if( con != null ){
                
                st = con.prepareStatement( this.SQL_MSF960 );
                
                rs = st.executeQuery();
                
                this.vector = new Vector();
                
                while ( rs.next() ){
                    
                    String cuenta = rs.getString(1).toUpperCase();
                    String distrito = rs.getString(2).toUpperCase();
                    
                    Hashtable ht = new Hashtable();
                    
                    ht.put("cuenta", cuenta);
                    ht.put("distrito", distrito);
                                        
                    this.vector.add( ht );
                    
                }
                
            }
            
        } catch( SQLException e ){
            
            e.printStackTrace();
            throw new SQLException( "ERROR EN 'msf960' - [TransferirCuentasContablesICG] " + e.getMessage() + " " + e.getErrorCode() );
        
        }
        
        finally{
            
            if ( st != null ){
                try{
                    st.close();
                } catch( SQLException e ){
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO " + e.getMessage() );
                }
            }            
            if ( con != null ){
                poolManager.freeConnection( "oracle", con );
            }
            
        }
        
    }
    
    /**
     * M�todo que permite ingresar los registros I, C, y G a la tabla cuentas del esquema con.
     * @autor :         LREALES
     * @param:          -
     * @throws:         SQLException
     * @version :       1.0
     */ 
    public void transferir ( Hashtable ht ) throws SQLException{
        
        Connection con = null;
        PreparedStatement st = null;
        PoolManager poolManager = null;
        
        try{            
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( "sot" );
            
            if( con != null ){
                
                st = con.prepareStatement( this.SQL_CUENTAS );
            
                st.setString ( 1, ht.get( "reg_status" ).toString() );
                st.setString ( 2, ht.get( "distrito" ).toString() );
                st.setString ( 3, ht.get( "cuenta" ).toString() );
                st.setString ( 4, ht.get( "nombre_largo" ).toString() );        
                st.setString ( 5, ht.get( "nombre_corto" ).toString() );
                st.setString ( 6, ht.get( "nombre_observacion" ).toString() );
                st.setString ( 7, ht.get( "fin_periodo" ).toString() );
                st.setString ( 8, ht.get( "auxiliar" ).toString() ); 
                st.setString ( 9, ht.get( "activa" ).toString() ); 
                st.setString ( 10, ht.get( "base" ).toString() ); 
                st.setString ( 11, ht.get( "modulo1" ).toString() ); 
                st.setString ( 12, ht.get( "modulo2" ).toString() ); 
                st.setString ( 13, ht.get( "modulo3" ).toString() ); 
                st.setString ( 14, ht.get( "modulo4" ).toString() ); 
                st.setString ( 15, ht.get( "modulo5" ).toString() ); 
                st.setString ( 16, ht.get( "modulo6" ).toString() ); 
                st.setString ( 17, ht.get( "modulo7" ).toString() ); 
                st.setString ( 18, ht.get( "modulo8" ).toString() ); 
                st.setString ( 19, ht.get( "modulo9" ).toString() ); 
                st.setString ( 20, ht.get( "modulo10" ).toString() ); 
                st.setString ( 21, ht.get( "cta_dependiente" ).toString() ); 
                st.setString ( 22, ht.get( "nivel" ).toString() ); 
                st.setString ( 23, ht.get( "cta_cierre" ).toString() ); 
                st.setString ( 24, ht.get( "subledger" ).toString() );
                st.setString ( 25, ht.get( "tercero" ).toString() );
                st.setString ( 26, ht.get( "last_update" ).toString() ); 
                st.setString ( 27, ht.get( "user_update" ).toString() ); 
                st.setString ( 28, ht.get( "creation_date" ).toString() ); 
                st.setString ( 29, ht.get( "creation_user" ).toString() );
                st.setString ( 30, ht.get( "detalle" ).toString() );
                
                st.execute();
                
            }
            
        } catch( SQLException e ){
            
            e.printStackTrace();
            throw new SQLException( "ERROR DURANTE 'transferir' - [TransferirCuentasContablesICG].. " + e.getMessage() + " " + e.getErrorCode() );
            
        }
        
        finally{
            
            if ( st != null ){
                try{
                    st.close ();
                } catch( SQLException e ){
                    throw new SQLException ( "ERROR CERRANDO EL ESTAMENTO" + e.getMessage () );
                }
            }
            if ( con != null ){
                poolManager.freeConnection( "sot", con );
            }
            
        }
        
    }
    
}