/*
 * Nombre        ActualizarSaldosContables.java
 * Descripci�n   Verifica la integridad de las tablas de contabilidad (con)
 *               comprobante, y comprodet
 * Autor         David Pi�a L�pez
 * Fecha         21 de junio de 2006, 02:30 PM
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */
package com.tsp.util.Contabilidad;

import java.sql.*;
import com.tsp.util.connectionpool.PoolManager;
import java.util.*;
import com.tsp.util.*;
/**
 *
 * @author  David
 */
public class ActualizarSaldosContables {
    
    public static final String SQL_SELECT_SALDOS_CONTABLES =
    "SELECT                "+
    " ACCOUNT_CODE,        "+
    " AMOUNT_ITEMX1,       "+
    " AMOUNT_ITEMX2,       "+
    " AMOUNT_ITEMX3,       "+
    " AMOUNT_ITEMX4,       "+
    " AMOUNT_ITEMX5,       "+
    " AMOUNT_ITEMX6,       "+
    " AMOUNT_ITEMX7,       "+
    " AMOUNT_ITEMX8,       "+
    " AMOUNT_ITEMX9,       "+
    " AMOUNT_ITEMX10,      "+
    " AMOUNT_ITEMX11,      "+
    " AMOUNT_ITEMX12,      "+
    " CCYY_IND,            "+        
    " DSTRCT_CODE,         "+
    " OPEN_BALANCE         "+    
    "FROM                  "+
    " MSF960               "+
    "WHERE                 "+
    " DSTRCT_CODE LIKE 'FINV' "+
    "AND CURRENCY_IND = 'L' "+
    "AND CURRENCY = 'PES'  "+    
    "AND POSTING_TYPE = '00' "+
    "AND ACCOUNT_CODE LIKE '%' "+
    "AND CCYY_IND >= '2005' ";
    
    public static final String SQL_DELETE_MAYOR =
    "DELETE                "+
    " FROM con.mayor       " ;
    
    public static final String SQL_INSERT_MAYOR =
    "INSERT INTO con.mayor   "+
    "( dstrct              , "+
    "  cuenta              , "+
    "  anio                , "+
    "  saldoant            , "+
    "  movdeb01            , "+    
    "  movcre01            , "+
    "  movdeb02            , "+    
    "  movcre02            , "+
    "  movdeb03            , "+    
    "  movcre03            , "+
    "  movdeb04            , "+    
    "  movcre04            , "+
    "  movdeb05            , "+    
    "  movcre05            , "+
    "  movdeb06            , "+    
    "  movcre06            , "+
    "  movdeb07            , "+    
    "  movcre07            , "+
    "  movdeb08            , "+    
    "  movcre08            , "+
    "  movdeb09            , "+    
    "  movcre09            , "+
    "  movdeb10            , "+    
    "  movcre10            , "+
    "  movdeb11            , "+    
    "  movcre11            , "+
    "  movdeb12            , "+    
    "  movcre12            , "+
    "  saldoact            , "+
    "  user_update         , "+
    "  creation_user       , "+    
    "  base                  "+
    ") "+
    "VALUES( ?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,'ADMIN','ADMIN','COL' ) ";
    
    public static final String SQL_SELECT_PRUEBA =
    "SELECT                "+    
    " SUM(AMOUNT_ITEMX1) as mes01,  "+
    " SUM(AMOUNT_ITEMX2) as mes02,  "+
    " SUM(AMOUNT_ITEMX3) as mes03,       "+
    " SUM(AMOUNT_ITEMX4) as mes04,       "+
    " SUM(AMOUNT_ITEMX5) as mes05,       "+
    " SUM(AMOUNT_ITEMX6) as mes06,       "+
    " SUM(AMOUNT_ITEMX7) as mes07,       "+
    " SUM(AMOUNT_ITEMX8) as mes08,       "+
    " SUM(AMOUNT_ITEMX9) as mes09,       "+
    " SUM(AMOUNT_ITEMX10) as mes10,      "+
    " SUM(AMOUNT_ITEMX11) as mes11,      "+
    " SUM(AMOUNT_ITEMX12) as mes12, ACCOUNT_CODE "+    
    "FROM                  "+
    " MSF960               "+
    "WHERE                 "+
    " DSTRCT_CODE LIKE 'FINV' "+
    "AND CURRENCY_IND = 'L' "+
    "AND CURRENCY = 'PES'  "+    
    "AND POSTING_TYPE = '00' "+
    "AND ACCOUNT_CODE LIKE '%' "+
    "AND CCYY_IND >= '2005' AND rownum < 6 GROUP BY ACCOUNT_CODE";
        
    private Hashtable registro;    
    
    /** Creates a new instance of ActualizarSaldosContables */
    public ActualizarSaldosContables()throws SQLException {
        this.eliminarDatosMayor();
        this.cargarDatosMims();
    }
    /**
     *M�todo que permite eliminar los datos de la tabla mayor
     *@autor: David Pi�a     
     *@throws: En caso de que un error de base de datos ocurra.     
     */
    public void eliminarDatosMayor()throws SQLException{
        Connection con = null;
        PreparedStatement st = null;        
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("sot");
            if(con!=null){
                st = con.prepareStatement( SQL_DELETE_MAYOR );  
                ////System.out.println( st.toString() );
                st.executeUpdate();                
            }            
        }catch(SQLException e){
            throw new SQLException("ERROR LA ELIMINACI�N DE DATOS EN MIMS eliminarDatosMayor()" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }          
            if (con != null){
                poolManager.freeConnection("sot", con);
            }
        }
        
    }
    /**
     *M�todo que permite cargar los datos de mims que se van a migrar a postgress
     *@autor: David Pi�a     
     *@throws: En caso de que un error de base de datos ocurra.     
     */
    public void cargarDatosMims()throws SQLException{        
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        double saldoActual = 0;        
        com.tsp.operation.model.Model modelOperation = new com.tsp.operation.model.Model();
        try {            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("oracle");
            if( con != null ){
                modelOperation.LogProcesosSvc.InsertProceso("Migraci�n Saldos Contables", this.hashCode(), "Proceso de Migraci�n de Saldos Contables", "ADMIN");
                st = con.prepareStatement( SQL_SELECT_SALDOS_CONTABLES );
                ////System.out.println( st.toString() );
                rs= st.executeQuery();
                while(rs.next()){
                    saldoActual = rs.getDouble(16);
                    registro = new Hashtable();
                    registro.put( "dstrct", rs.getString( 15 ) );
                    registro.put( "cuenta", rs.getString( 1 ) );
                    registro.put( "anio", rs.getString( 14 ) );
                    registro.put( "saldoant", new Double( rs.getDouble( 16 ) ) );
                    for( int i = 1; i<=12; i++ ){
                        double valorIngresar = rs.getDouble( i + 1 );
                        if( valorIngresar == 0 ){
                            registro.put( "movdeb"+Util.mesFormat(i) , new Double( 0 ) );
                            registro.put( "movcre"+Util.mesFormat(i) , new Double( 0 ) );
                        }else if( valorIngresar > 0 ){
                            registro.put( "movdeb"+Util.mesFormat(i) , new Double( valorIngresar ) );
                            registro.put( "movcre"+Util.mesFormat(i) , new Double( 0 ) );                            
                        }else{
                            registro.put( "movdeb"+Util.mesFormat(i) , new Double( 0 ) );
                            registro.put( "movcre"+Util.mesFormat(i) , new Double( valorIngresar * (-1) ) );                            
                        }                        
                        saldoActual += valorIngresar;                        
                    }
                    registro.put( "saldoact", new Double( saldoActual ) );
                    insertarMayor();                    
                }                
            }
        }catch(SQLException e){
            modelOperation.LogProcesosSvc.finallyProceso("Migraci�n Saldos Contables", this.hashCode(),"ADMIN","ERROR :" + e.getMessage());
            throw new SQLException("ERROR DURANTE EL PROCESO OBTENCION DE INFORMACION DESDE ORACLE cargarDatosMims()" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (rs != null) rs.close();
            if (con != null){
                poolManager.freeConnection("oracle", con);
            }
            modelOperation.LogProcesosSvc.finallyProceso("Migraci�n Saldos Contables", this.hashCode(), "ADMIN", "PROCESO EXITOSO");
        }        
    }
    /**
     *M�todo que permite agregar en la tabla mayor
     *@autor: David Pi�a     
     *@throws: En caso de que un error de base de datos ocurra.     
     */
    public void insertarMayor()throws SQLException{        
        Connection con = null;
        PreparedStatement st = null;        
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("sot");
            if( con != null ){
                st = con.prepareStatement( SQL_INSERT_MAYOR );
                st.setString( 1, (String)registro.get("dstrct") );
                st.setString( 2, (String)registro.get("cuenta") );
                st.setString( 3, (String)registro.get("anio") );                
                st.setDouble( 4, ( (Double)registro.get("saldoant") ).doubleValue() );
                int j = 4;                
                for( int i = 1; i<=12; i++, j++ ){
                    ////System.out.println( (i + j)+"-"+"movdeb"+Util.mesFormat(i) );
                    ////System.out.println( (i + (j+1))+"-"+"movcre"+Util.mesFormat(i) );
                    st.setDouble( (i + j), ( (Double)registro.get("movdeb"+Util.mesFormat(i)) ).doubleValue() );
                    st.setDouble( (i + (j+1)), ( (Double)registro.get("movcre"+Util.mesFormat(i)) ).doubleValue() );                    
                }
                st.setDouble( 29, ( (Double)registro.get("saldoact") ).doubleValue() );
                ////System.out.println( st.toString() );
                st.executeUpdate();                
            }            
        }catch(SQLException e){
            throw new SQLException("ERROR EN LA INSERCI�N DE DATOS insertarMayor()" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }          
            if (con != null){
                poolManager.freeConnection("sot", con);
            }
        }
        
    }
    //m�todo de pruebas
    public void pruebaTotalesMims()throws SQLException{        
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        double saldoActual = 0;        
        com.tsp.operation.model.Model modelOperation = new com.tsp.operation.model.Model();
        try {            
            String sql = "";
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("oracle");
            if( con != null ){             
                st = con.prepareStatement( SQL_SELECT_PRUEBA );
                ////System.out.println( st.toString() );
                rs= st.executeQuery();                
                while(rs.next()){
                    ////System.out.println("CUENTA---- "+rs.getString(13) );
                    ////System.out.println("mes 01="+rs.getString(1) );
                    ////System.out.println("mes 02"+rs.getString(2) );
                    ////System.out.println("mes 03="+rs.getString(3) );
                    ////System.out.println("mes 04="+rs.getString(4) );
                    ////System.out.println("mes 05="+rs.getString(5) );
                    ////System.out.println("mes 06="+rs.getString(6) );
                    ////System.out.println("mes 07="+rs.getString(7) );
                    ////System.out.println("mes 08="+rs.getString(8) );
                    ////System.out.println("mes 09="+rs.getString(9) );
                    ////System.out.println("mes 10="+rs.getString(10) );
                    ////System.out.println("mes 11="+rs.getString(11) );
                    ////System.out.println("mes 12="+rs.getString(12) );
                }                
            }
        }catch(SQLException e){            
            throw new SQLException("ERROR DURANTE EL PROCESO OBTENCION DE INFORMACION DESDE ORACLE cargarDatosMims()" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (rs != null) rs.close();
            if (con != null){
                poolManager.freeConnection("oracle", con);
            }            
        }
    }
    public static void main( String args[] ) throws SQLException{
        ActualizarSaldosContables as = new ActualizarSaldosContables();      
    }
    
}
