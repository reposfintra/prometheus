/****************************************************************************
 *  Nombre Clase.................   MigracionFacturas.java                  *
 *  Descripci�n..................   se encarga de migrar los datos de mims  *
 *                                  ( msf260, msf26A y msf263 ) a la web    *
 *                                  ( fin.cxp_doc, fin.cxp_items_doc,       *
 *                                   fin.cxp_imp_item y fin.cxp_imp_doc ).  *
 *  Autor........................   LREALES                                 *
 *  Fecha........................   12.07.2006                              *
 *  Modificado...................   30.10.2006                              *
 *  Versi�n......................   1.0                                     *
 *  Copyright....................   Transportes Sanchez Polo S.A.           *
 ****************************************************************************/

package com.tsp.util.Contabilidad;

import com.tsp.util.connectionpool.PoolManager;

import java.io.*;
import java.sql.*;
import java.util.*;

import jxl.*;

import com.tsp.util.*;
import com.tsp.operation.model.DAOS.OpDAO;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.beans.POIWrite;// Generar Excel
import com.tsp.operation.model.services.*;

import java.text.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;

import org.apache.poi.hssf.usermodel.*;// Generar Excel
import org.apache.poi.hssf.util.*;// Generar Excel
import org.apache.log4j.*;

public class MigracionFacturas extends Thread {
    
    /***************
     * MODIFICAR AL EJECUTAR
     * usuario = Login del usuario que ejecuta el programa.
     * moneda_del_distrito = Moneda del distrito en que nos encontramos.
     ***************/
    private String usuario = "SESCALANTE";
    private String moneda_del_distrito = "PES";
    /***************
     * FIN
     ***************/
    
    /* INICIO MOD PA TRANSACCION */    
    private int total_imp_cabecera_migrados = 0;  
    private String temporal = "";
    /* FIN */
    
    private  FileWriter        fw;
    private  BufferedWriter    bf;
    private  PrintWriter       linea;
    
    Logger logger = Logger.getLogger(this.getClass());
    
    /***** INICIO MODIFICACION CXP_IMP_DOC *****/
    private double valor_retencion_IMP_DOC = 0;
    private double valor_retencion_me_IMP_DOC = 0;
    /***** FIN MODIFICACION CXP_IMP_DOC *****/
    
    private String fact_anterior = null;
    private String fact_item = null;
    private double fact_items = 0;
    private double fact_retefuente = 0;
    private double fact_impuestos = 0;
    private double fact_iva = 0;
    private double fact_items_me = 0;
    private double fact_retefuente_me = 0;
    private double fact_impuestos_me = 0;
    private double fact_iva_me = 0;
    
    double vlr_d_abonos = 0;
    double vlrme_d_abonos = 0;
    
    private FacturasMims factura;
    
    private OpDAO OpDataAccess;
    
    private Vector vector;
    
    private Vector otro_vector;
    
    private int total_facturas_migradas = 0;
    private int total_items_migrados = 0;
    private int total_impuestos_migrados = 0;
    
    boolean existe_nit = false;
    boolean existe_pro = false;
    
    private TreeMap tree_map_imp_nit;
    private TreeMap tree_map_imp_pro;
    
    private com.tsp.operation.model.Model model = new  com.tsp.operation.model.Model();
    
    private static final String SQL_LIMPIAR_CXP_DOC =
    "DELETE "+
    "FROM "+
    "fin.cxp_doc "+
    "WHERE " +
    "dstrct = ? "+
    "AND proveedor = ? "+
    "AND tipo_documento = ? "+
    "AND documento = ? ";
    
    private static final String SQL_LIMPIAR_CXP_ITEMS_DOC =
    "DELETE "+
    "FROM "+
    "fin.cxp_items_doc "+
    "WHERE " +
    "dstrct = ? "+
    "AND proveedor = ? "+
    "AND tipo_documento = ? "+
    "AND documento = ? ";
    
    private static final String SQL_LIMPIAR_CXP_IMP_ITEM =
    "DELETE "+
    "FROM "+
    "fin.cxp_imp_item "+
    "WHERE " +
    "dstrct = ? "+
    "AND proveedor = ? "+
    "AND tipo_documento = ? "+
    "AND documento = ? ";
    
    private static final String SQL_LIMPIAR_CXP_IMP_DOC =
    "DELETE "+
    "FROM "+
    "fin.cxp_imp_doc "+
    "WHERE " +
    "dstrct = ? "+
    "AND proveedor = ? "+
    "AND tipo_documento = ? "+
    "AND documento = ? ";
    
    /****************************************************************************************************************************/
    private static final String SQL_FACTURAS_CANCELADAS =
    
    "SELECT  D.* , E.TAX_FILE_NO     " +
    "FROM  " +
    "(  " +
    
    "SELECT  " +
    "A.DSTRCT_CODE                 DISTRITO,     " +
    "A.SUPPLIER_NO                 PLACA,         " +
    "A.INV_NO                      FACTURA,        " +
    "A.EXT_INV_NO                  EXTFACTURA,   " +
    "A.LOC_INV_ORIG                VLRFACT,        " +
    "A.FOR_INV_ORIG                VLRMEFACT,       " +
    "A.SUPP_TO_PAY                 IDMIMS,        " +
    "A.INV_TYPE                    TIPO,          " +
    "A.CURRENCY_TYPE               MONEDA,         " +
    "A.LOADED_DATE                 LOADED_DATE,    " +
    "A.LAST_MOD_DATE               LAST_MOD_DATE,   " +
    "A.LAST_MOD_TIME               LAST_MOD_TIME,   " +
    
    "B.INV_ITEM_NO                 ITEM,       " +
    "B.INV_ITEM_DESC               DESCRIPCIONITEM,  " +
    "B.LOC_VAL_INVD                VLRITEM,   " +
    "B.FOR_VAL_INVD                VLRMEITEM,    " +
    "B.ACCOUNT_CODE                ACCOUNT,   " +
    "B.LAST_MOD_USER               LAST_MOD_USER,  " +
    "B.PO_NO                       PLANILLA,  " +
    "B.AUTHSD_BY                   APROBADOR,  " +
    
    "C.ATAX_CODE                   CODIGOIMP,    " +
    "C.ATAX_RATE_9                 PORCENTAJEIMP,  " +
    "C.ATAX_AMOUNT_L               VLRIMP,    " +
    "C.ATAX_AMOUNT_F               VLRMEIMP,  " +
    
    "NVL(IVA.ATAX_AMOUNT_L ,0)     IVA_VLR,    " +
    
    "A.BRANCH_CODE                 BANCO,   " +
    "A.BANK_ACCT_NO                SUCURSAL,     " +
    "A.HANDLE_CDE                  HC,       " +
    "A.INV_DATE                    FECFACTURA,   " +
    "A.PAID_DATE                   FECPAGO,    " +
    "A.DUE_DATE                    FECVENCIMIENTO,   " +
    "A.APPROVED_DATE               FECAPROBACION,    " +
    "A.CHEQUE_RUN_NO               CORRIDA,  " +
    "A.CHEQUE_NO                   CHEQUE,  " +
    "A.LAST_MOD_USER               CREATION_USER, " +
    "A.PMT_STATUS                  ESTADO, " +
    "B.PROJECT_NO                  CODIGO_ABC, " +
    "B.PP_AMT_LOC                  RETENCION_ITEM, " +
    "B.SUBLEDGER_TYPE              TIPO_SUBLEDGER, " +
    "B.SUBLEDGER_ACCT              CUENTA_SUBLEDGER " +
    
    "FROM  " +
    "MSF260 A,  " +
    "MSF26A B,  " +
    "MSF263 C,  " +
    "MSF263 IVA  " +
    
    "WHERE  " +
    "A.DSTRCT_CODE = 'FINV'   " +
    
    "AND (A.LAST_MOD_DATE >= ? AND A.LAST_MOD_TIME >= ?) " +
    
    /**************************************************************/
    //"AND (A.LAST_MOD_DATE >= '20061216' AND A.LAST_MOD_TIME >= '000000') " +
    //"AND (A.LAST_MOD_DATE <= '20061217' AND A.LAST_MOD_TIME <= '235959') " +
    /**************************************************************/
    
    "AND SUBSTR (A.EXT_INV_NO , 0 , 1) != 'W'  " +
    
    "AND A.INV_TYPE = '4'   " +
    "AND A.PMT_STATUS < 50 " +
    //"AND A.INV_TYPE != 'A'   " +
    //"AND A.PMT_STATUS <= 50 " +
    
    "AND A.LOC_INV_ORIG != '0'  " +
    
    "AND A.DSTRCT_CODE = B.DSTRCT_CODE(+)  " +
    "AND A.SUPPLIER_NO = B.SUPPLIER_NO(+)  " +
    "AND A.INV_NO      = B.INV_NO(+)  " +

    "AND B.DSTRCT_CODE = C.INV_DSTRCT_CODE(+)  " +
    "AND B.SUPPLIER_NO = C.INV_SUPPLIER_NO(+)  " +
    "AND B.INV_NO      = C.INV_NO(+)  " +
    "AND B.INV_ITEM_NO = C.INV_ITEM_NO(+)  " +
    
    "AND B.DSTRCT_CODE = IVA.INV_DSTRCT_CODE(+)  " +
    "AND B.SUPPLIER_NO = IVA.INV_SUPPLIER_NO(+)  " +
    "AND B.INV_NO      = IVA.INV_NO(+)  " +
    "AND B.INV_ITEM_NO = IVA.INV_ITEM_NO(+)  " +
    "AND 'IVA'         = IVA.TAX_REF(+)  " +
    
    ") D ,  " +
    "MSF203 E  " +
    
    "WHERE  E.DSTRCT_CODE = D.DISTRITO  " +
    "AND E.SUPPLIER_NO = D.IDMIMS " +
    
    //"AND ROWNUM < 100 " +
    
    "ORDER BY E.TAX_FILE_NO, D.EXTFACTURA, D.PLACA, D.ITEM";
    /****************************************************************************************************************************/

    private static final String SQL_AGENCIA =
    "SELECT DISTINCT "+
    "agency_id "+
    "FROM "+
    "banco "+
    "WHERE "+
    "dstrct = ? "+
    "AND branch_code = ? " +
    "AND bank_account_no = ? ";
    
    private static final String SQL_MONEDA_BANCO =
    "SELECT DISTINCT "+
    "currency "+
    "FROM "+
    "banco "+
    "WHERE "+
    "dstrct = ? "+
    "AND branch_code = ? "+
    "AND bank_account_no = ? ";
    
    private static final String SQL_CXP_DOC =
    "INSERT INTO fin.cxp_doc "+
    "( reg_status, dstrct, proveedor, tipo_documento, documento, "+
    "descripcion, agencia, handle_code, id_mims, tipo_documento_rel, "+
    "documento_relacionado, fecha_aprobacion, aprobador, usuario_aprobacion, banco, "+
    "sucursal, moneda, vlr_neto, vlr_total_abonos, vlr_saldo, "+
    "vlr_neto_me, vlr_total_abonos_me, vlr_saldo_me, tasa, usuario_contabilizo, "+
    "fecha_contabilizacion, usuario_anulo, fecha_anulacion, fecha_contabilizacion_anulacion, observacion, "+
    "num_obs_autorizador, num_obs_pagador, num_obs_registra, last_update, user_update, "+
    "creation_date, creation_user, base, corrida, cheque, "+
    "periodo, fecha_contabilizacion_ajc, fecha_contabilizacion_ajv, periodo_ajc, periodo_ajv, "+
    "usuario_contabilizo_ajc, usuario_contabilizo_ajv, transaccion_ajc, transaccion_ajv, fecha_procesado, "+
    "clase_documento, transaccion, moneda_banco, fecha_documento, fecha_vencimiento, "+
    "ultima_fecha_pago ) "+
    "VALUES "+
    "(?, ?, ?, ?, ?, "+
    "?, ?, ?, ?, ?, "+
    "?, ?, ?, ?, ?, "+
    "?, ?, ?, ?, ?, "+
    "?, ?, ?, ?, ?, "+
    "?, ?, ?, ?, ?, "+
    "?, ?, ?, ?, ?, "+
    "?, ?, ?, ?, ?, "+
    "?, ?, ?, ?, ?, "+
    "?, ?, ?, ?, ?, "+
    "?, ?, ?, ?, ?, "+
    "? ) ";
    
    private static final String SQL_CXP_ITEMS_DOC =
    "INSERT INTO fin.cxp_items_doc "+
    "( reg_status, dstrct, proveedor, tipo_documento, documento, "+
    "item, descripcion, vlr, vlr_me, codigo_cuenta, "+
    "codigo_abc, planilla, last_update, user_update, creation_date, "+
    "creation_user, base, codcliarea, tipcliarea, concepto, "+
    "auxiliar ) "+
    "VALUES "+
    "(?, ?, ?, ?, ?, "+
    "?, ?, ?, ?, ?, "+
    "?, ?, ?, ?, ?, "+
    "?, ?, ?, ?, ?, "+
    "? ) ";
    
    private static final String SQL_CXP_IMP_ITEM =
    "INSERT INTO fin.cxp_imp_item "+
    "( reg_status, dstrct, proveedor, tipo_documento, documento, "+
    "item, cod_impuesto, porcent_impuesto, vlr_total_impuesto, vlr_total_impuesto_me, "+
    "last_update, user_update, creation_date, creation_user, base ) "+
    "VALUES "+
    "( ?, ?, ?, ?, ?, "+
    "?, ?, ?, ?, ?, "+
    "?, ?, ?, ?, ? ) ";
    
    private static final String SQL_CXP_IMP_DOC =
    "INSERT INTO fin.cxp_imp_doc "+
    "( reg_status, dstrct, proveedor, tipo_documento, documento, "+
    "cod_impuesto, porcent_impuesto, vlr_total_impuesto, vlr_total_impuesto_me, "+
    "last_update, user_update, creation_date, creation_user, base ) "+
    "VALUES "+
    "( ?, ?, ?, ?, ?, "+
    "?, ?, ?, ?, "+
    "?, ?, ?, ?, ? ) ";
    
    private static final String SQL_FECPROCESO_MIG260 =
    "SELECT "+
    "referencia "+
    "FROM "+
    "tablagen "+
    "WHERE "+
    "table_type = 'FECPROCESO' "+
    "AND table_code = 'MIG260' ";
    
    private static final String SQL_UPDATE_FECPROCESO_MIG260 =
    "UPDATE "+
    "tablagen "+
    "SET "+
    "referencia = ?, last_update = 'now()', user_update = 'LREALES' "+
    "WHERE "+
    "table_type = 'FECPROCESO' "+
    "AND table_code = 'MIG260' ";
    
    private static final String SQL_EXISTE_CXP_ITEMS_DOC =
    "SELECT "+
    "* "+
    "FROM "+
    "fin.cxp_items_doc "+
    "WHERE "+
    "dstrct = ? "+
    "AND proveedor = ? "+
    "AND tipo_documento = ? "+
    "AND documento = ? "+
    "AND item = ? ";
    
    private static final String SQL_EXISTE_CXP_DOC =
    "SELECT "+
    "* "+
    "FROM "+
    "fin.cxp_doc "+
    "WHERE "+
    "dstrct = ? "+
    "AND proveedor = ? "+
    "AND tipo_documento = ? "+
    "AND documento = ? ";
    
    private static final String SQL_EXISTE_CXP_IMP_ITEM =
    "SELECT "+
    "* "+
    "FROM "+
    "fin.cxp_imp_item "+
    "WHERE "+
    "dstrct = ? "+
    "AND proveedor = ? "+
    "AND tipo_documento = ? "+
    "AND documento = ? "+
    "AND item = ? "+
    "AND cod_impuesto = ? ";
    
    private static final String SQL_EXISTE_CXP_IMP_DOC =
    "SELECT "+
    "* "+
    "FROM "+
    "fin.cxp_imp_doc "+
    "WHERE "+
    "dstrct = ? "+
    "AND proveedor = ? "+
    "AND tipo_documento = ? "+
    "AND documento = ? "+
    "AND cod_impuesto = ? ";
    
    private static final String SQL_FACTURAS_A_BORRAR =
    "SELECT "+
    "dstrct, proveedor, tipo_documento, documento, descripcion "+
    "FROM "+
    "fin.cxp_doc "+
    "WHERE "+
    "SUBSTRING( descripcion FROM 0 FOR 13 ) = 'FACTURA MIMS' ";
    
    private static final String SQL_UPDATE_CXP_IMP_ITEM =
    "UPDATE "+
    "fin.cxp_imp_item "+
    "SET "+
    "reg_status = '', porcent_impuesto = ?, "+
    "vlr_total_impuesto = ?, vlr_total_impuesto_me = ?, "+
    "last_update = ?, user_update = ? "+
    "WHERE "+
    "dstrct = ? "+
    "AND proveedor = ? "+
    "AND tipo_documento = ? "+
    "AND documento = ? "+
    "AND item = ? "+
    "AND cod_impuesto = ? ";
    
    private static final String SQL_UPDATE_CXP_IMP_DOC =
    "UPDATE "+
    "fin.cxp_imp_doc "+
    "SET "+
    "reg_status = '', porcent_impuesto = ?, "+
    "vlr_total_impuesto = ?, vlr_total_impuesto_me = ?, "+
    "last_update = ?, user_update = ? "+
    "WHERE "+
    "dstrct = ? "+
    "AND proveedor = ? "+
    "AND tipo_documento = ? "+
    "AND documento = ? "+
    "AND cod_impuesto = ? ";
    
    private static final String SQL_UPDATE_CXP_ITEMS_DOC =
    "UPDATE "+
    "fin.cxp_items_doc "+
    "SET "+
    "reg_status = '', descripcion = ?, "+
    "vlr = ?, vlr_me = ?, "+
    "codigo_cuenta = ?, planilla = ?, "+
    "last_update = ?, user_update = ? "+
    "WHERE "+
    "dstrct = ? "+
    "AND proveedor = ? "+
    "AND tipo_documento = ? "+
    "AND documento = ? "+
    "AND item = ? ";
    
    private static final String SQL_UPDATE_CXP_DOC =
    "UPDATE "+
    "fin.cxp_doc "+
    "SET "+
    "reg_status = '', descripcion = ?, agencia = ?, handle_code = ?, id_mims = ?, "+
    "fecha_aprobacion = ?, aprobador = ?, usuario_aprobacion = ?, banco = ?, sucursal = ?, "+
    "moneda = ?, vlr_neto = ?, vlr_saldo = ?, vlr_neto_me = ?, vlr_saldo_me = ?, "+
    "tasa = ?, last_update = ?, user_update = ?, corrida = ?, cheque = ?, "+
    "periodo = ?, clase_documento = ?, moneda_banco = ?, fecha_documento = ?, fecha_vencimiento = ?, "+
    "ultima_fecha_pago = ?, vlr_total_abonos = ?, vlr_total_abonos_me = ? "+
    
    "WHERE "+
    "dstrct = ? "+
    "AND proveedor = ? "+
    "AND tipo_documento = ? "+
    "AND documento = ? ";
    
    private static final String SQL_ABONOS =
    "SELECT "+
    "vlr_total_abonos, vlr_total_abonos_me "+
    "FROM "+
    "fin.cxp_doc "+
    "WHERE "+
    "dstrct = ? "+
    "AND proveedor = ? "+
    "AND tipo_documento = ? "+
    "AND documento = ? ";
    
    private static final String SQL_COD_IMP =
    "SELECT "+
    "referencia "+ // cod_imp_post
    "FROM "+
    "tablagen "+
    "WHERE "+
    "table_type = 'IMPMP' "+ // tabla relacion impuestos mims - postgres
    "AND table_code = ? "; // cod_imp_mims
    
    private static final String SQL_INSERT_SENDMAIL =
    "INSERT INTO sendmail "+
    "(emailfrom, "+
    "emailto," +
    "emailcopyto, "+
    "emailsubject, "+
    "emailbody, "+
    "tipo) "+
    "VALUES "+
    "( 'procesos@sanchezpolo.com', "+
    "'soporte@sanchezpolo.com', "+
    "'', "+
    "'PROCESO DE BAJADA DE FACTURAS DE PROVEEDORES DE MIMS A POSTGRES', "+
    "?, "+
    "'E' ) ";
    
    private static final String SQL_EXISTE_PLANILLA =
    "SELECT "+
    "* "+
    "FROM "+
    "planilla "+
    "WHERE "+
    "numpla = ? ";
    
    private static final String SQL_UPDATE_PLANILLA =
    "UPDATE "+
    "planilla "+
    "SET "+
    "factura = ?, reg_status = 'C' "+
    "WHERE "+
    "numpla = ? ";
    
    private static final String SQL_EXISTE_NIT =
    "SELECT "+
    "* "+
    "FROM "+
    "nit "+
    "WHERE "+
    "cedula = ? ";
    
    private static final String SQL_EXISTE_PROVEEDOR =
    "SELECT "+
    "* "+
    "FROM "+
    "proveedor "+
    "WHERE "+
    "nit = ? ";
    
    private static final String SQL_SIGNO_IMP =
    "SELECT "+
    "ind_signo "+
    "FROM "+
    "tipo_de_impuesto "+
    "WHERE "+
    "codigo_impuesto = ? ";
    
    private static final String SQL_LOGIN_APROBADOR =
    "SELECT "+
    "FIRST_NAME      NOMBRE, "+
    "SURNAME         APELLIDO "+
    "FROM "+
    "MSF810 "+
    "WHERE "+
    "EMPLOYEE_ID = ? ";
    
    /* INICIO MOD PA TRANSACCION */
    private static final String SQL_UPDATE_CODIGO_CUENTA =
    "UPDATE  "+
	"fin.cxp_items_doc "+
    "SET     "+
        "codigo_cuenta = a.account_code_c "+
    "FROM "+
	"( "+
	"SELECT "+
            "i.dstrct AS dis, i.proveedor AS pro, i.tipo_documento AS tip, i.documento AS doc, i.item AS ite, i.descripcion, i.codigo_cuenta, "+
            "p.numpla, p.numrem, p.account_code_c AS cuenta_plarem_c, p.account_code_i AS cuenta_plarem_i, "+
            "COALESCE ( s.account_code_c, '') AS account_code_c "+
	"FROM "+
            "fin.cxp_items_doc i "+
            "LEFT JOIN plarem p ON ( p.numpla = i.planilla ) "+
            "LEFT JOIN remesa r ON ( r.numrem = p.numrem ) "+
            "LEFT JOIN stdjob s ON ( s.std_job_no = r.std_job_no ) "+
	"WHERE "+
            "i.codigo_cuenta = '' AND p.numpla != '' "+
	"ORDER BY "+
            "i.documento, i.item "+
	") a "+
    "WHERE "+
	"dstrct = a.dis "+
	"AND proveedor = a.pro "+
	"AND tipo_documento = a.tip "+
	"AND documento = a.doc "+
	"AND item = a.ite ";
    /* FIN */
    
    /** Creates a new instance of MigracionFacturas */
    public MigracionFacturas() {
        
        OpDataAccess  =  new OpDAO();
        
    }
    
    public static void main( String[] args ) throws Exception {
        
        try{
            
            MigracionFacturas mfc = new MigracionFacturas();
            
            mfc.run();
            
            System.gc();
            
        } catch ( Exception e ){
            
            e.printStackTrace();
            throw new Exception( e.getMessage() );
            
        }
        
    }
    
    private String ruta;
    private String rutalog;
    
    public void run() {
        ////System.out.println("INICIO..");
        com.tsp.operation.model.Model modelOperation = new com.tsp.operation.model.Model();
        
        try{
            
            modelOperation.LogProcesosSvc.InsertProceso("Migracion de Facturas", this.hashCode(), "Proceso de Transferencia de Facturas de Mims a PostgreSQL", "ADMIN");
            
            // Traigo el Dia y la Hora de la ULTIMA EJECUCION
            String referencia = buscarFecProcesoMig260();
            String dia_ult_ejecucion = referencia.substring( 0, 8 );
            String hora_ult_ejecucion = referencia.substring( 8, 14 );
            String ref = referencia.substring( 14 );
            
            // Traigo el Dia y la Hora ACTUAL
            String fechaactual_postgres = Util.fechaActualTIMESTAMP();
            String dia_actual_oracle = Util.formatDateOracle( fechaactual_postgres );
            String hh_act = fechaactual_postgres.substring( 11, 13 );
            String mm_act = fechaactual_postgres.substring( 14, 16 );
            String ss_act = fechaactual_postgres.substring( 17, 19 );
            String hora_actual_oracle = hh_act + mm_act + ss_act;
            
            actualizarFecProcesoMig260( dia_actual_oracle, hora_actual_oracle );
            
            if ( referencia.equals("00990101000000INICIAR") ) {
                
                facturasABorrar();
                
                for ( int o_c = 0; o_c < getOtro_vector().size(); o_c++ ){
                    
                    FacturasMims otro_registro = ( FacturasMims ) getOtro_vector().elementAt( o_c );
                    
                    String dist = otro_registro.getOtro_distrito();
                    String prov = otro_registro.getOtro_proveedor();
                    String tipo_docu = otro_registro.getOtro_tipo_documento();
                    String docu = otro_registro.getOtro_documento();
                    
                    limpiarCXP_DOC( dist, prov, tipo_docu, docu );
                    limpiarCXP_ITEMS_DOC( dist, prov, tipo_docu, docu );
                    limpiarCXP_IMP_ITEM( dist, prov, tipo_docu, docu );
                    
                }
                
                String ini1 = "INICIO PROCESO: " + Util.fechaActualTIMESTAMP();
                
                System.gc();
                facturasCanceladas( "INICIAR", "00990101", "000000", "19990630", "235959" );
                System.gc();
                facturasCanceladas( "INICIAR", "19990701", "000000", "19991231", "235959" );
                System.gc();
                facturasCanceladas( "INICIAR", "20000101", "000000", "20000630", "235959" );
                System.gc();
                facturasCanceladas( "INICIAR", "20000701", "000000", "20001231", "235959" );
                System.gc();
                facturasCanceladas( "INICIAR", "20010101", "000000", "20010630", "235959" );
                System.gc();
                facturasCanceladas( "INICIAR", "20010701", "000000", "20011231", "235959" );
                System.gc();
                facturasCanceladas( "INICIAR", "20020101", "000000", "20020630", "235959" );
                System.gc();
                facturasCanceladas( "INICIAR", "20020701", "000000", "20021231", "235959" );
                System.gc();
                facturasCanceladas( "INICIAR", "20030101", "000000", "20030630", "235959" );
                System.gc();
                facturasCanceladas( "INICIAR", "20030701", "000000", "20031231", "235959" );
                System.gc();
                facturasCanceladas( "INICIAR", "20040101", "000000", "20040630", "235959" );
                System.gc();
                facturasCanceladas( "INICIAR", "20040701", "000000", "20041231", "235959" );
                System.gc();
                facturasCanceladas( "INICIAR", "20050101", "000000", "20050630", "235959" );
                System.gc();
                facturasCanceladas( "INICIAR", "20050701", "000000", "20051231", "235959" );
                System.gc();
                facturasCanceladas( "INICIAR", "20060101", "000000", "20060630", "235959" );
                System.gc();
                facturasCanceladas( "INICIAR", "20060701", "000000", dia_actual_oracle, hora_actual_oracle );
                System.gc();
                String fin1 = "FINALIZO PROCESO: " + Util.fechaActualTIMESTAMP();
                
                String body = ini1 + " - " + fin1 + " - No Facturas: " + total_facturas_migradas + " - No Items: " + total_items_migrados + " - No Impuestos: " + total_impuestos_migrados;
                
                sendMail( body );
                
            } else {
                
                String ini2 = "INICIO PROCESO: " + Util.fechaActualTIMESTAMP();
                
                System.gc();
                
                facturasCanceladas( ref, dia_ult_ejecucion, hora_ult_ejecucion, dia_actual_oracle, hora_actual_oracle );
                
                System.gc();
                
                String fin2 = "FINALIZO PROCESO: " + Util.fechaActualTIMESTAMP();
                
                actualizarCodigosDeCuentasVacios();
                
                System.gc();
                
                String body = ini2 + " - " + fin2 + " - No Facturas: " + total_facturas_migradas + " - No Items: " + total_items_migrados + " - No Impuestos Item: " + total_impuestos_migrados + " - No Impuestos Documento: " + total_imp_cabecera_migrados;
                ////System.out.println( "TOTAL: " + body );
                sendMail( body );
                
                System.gc();
                
                exportar( usuario );
                
                System.gc();
                
            }
            
            modelOperation.LogProcesosSvc.finallyProceso("Migracion de Facturas", this.hashCode(), "ADMIN", "PROCESO EXITOSO");
            
        } catch ( Exception e ){
            
            try {
                
                String body = "ERROR AL MIGRAR FACTURAS";
                
                sendMail( body );
                
                modelOperation.LogProcesosSvc.finallyProceso("Migracion de Facturas", this.hashCode(),"ADMIN","ERROR : " + e.getMessage() );
                
            } catch ( Exception ex ){
                
                ex.printStackTrace();
                
            }
            
        }
        
    }
    
    /**
     * Getter for property vector.
     * @return Value of property vector.
     */
    public java.util.Vector getVector() {
        
        return vector;
        
    }
    
    /**
     * Setter for property vector.
     * @param vector New value of property vector.
     */
    public void setVector( java.util.Vector vector ) {
        
        this.vector = vector;
        
    }
    
    
    /**
     * Getter for property otro_vector.
     * @return Value of property otro_vector.
     */
    public java.util.Vector getOtro_vector() {
        
        return otro_vector;
        
    }
    
    /**
     * Setter for property otro_vector.
     * @param otro_vector New value of property otro_vector.
     */
    public void setOtro_vector(java.util.Vector otro_vector) {
        
        this.otro_vector = otro_vector;
        
    }
    
    /**
     * Getter for property tree_map_imp_nit.
     * @return Value of property tree_map_imp_nit.
     */
    public java.util.TreeMap getTree_map_imp_nit() {
        
        return tree_map_imp_nit;
        
    }
    
    /**
     * Setter for property tree_map_imp_nit.
     * @param tree_map_imp_nit New value of property tree_map_imp_nit.
     */
    public void setTree_map_imp_nit(java.util.TreeMap tree_map_imp_nit) {
        
        this.tree_map_imp_nit = tree_map_imp_nit;
        
    }
    
    /**
     * Getter for property tree_map_imp_pro.
     * @return Value of property tree_map_imp_pro.
     */
    public java.util.TreeMap getTree_map_imp_pro() {
        
        return tree_map_imp_pro;
        
    }
    
    /**
     * Setter for property tree_map_imp_pro.
     * @param tree_map_imp_pro New value of property tree_map_imp_pro.
     */
    public void setTree_map_imp_pro(java.util.TreeMap tree_map_imp_pro) {
        
        this.tree_map_imp_pro = tree_map_imp_pro;
        
    }
    
    /**
     * Se encarga de limpiar la tabla 'cxp_doc' del esquema 'fin' en la web.
     * @autor LREALES
     * @param el distrito, el proveedor, el tipo de documento y el documento.
     * @throws Exception En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void limpiarCXP_DOC( String dist, String prov, String tipo_docu, String docu ) throws Exception{
        
        Connection con = null;
        PreparedStatement st = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( "sot" );
            
            if( con != null ){
                
                st = con.prepareStatement( this.SQL_LIMPIAR_CXP_DOC );
                
                st.setString( 1, dist );
                st.setString( 2, prov );
                st.setString( 3, tipo_docu );
                st.setString( 4, docu );
                
                /* INICIO MOD PA TRANSACCION */
                //ejecutar = st.toString() + ";";
                /* FIN */
                
                st.execute();
                
            }
            
        } catch( Exception e ){
            
            e.printStackTrace();
            throw new Exception( "ERROR EN 'limpiarCXP_DOC' - [MigracionFacturas].. " + e.getMessage()  );
            
        }
        
        finally{
            
            if ( st != null ){
                try{
                    st.close();
                } catch( Exception e ){
                    throw new Exception( "ERROR CERRANDO EL ESTAMENTO " + e.getMessage() );
                }
            }
            if ( con != null ){
                poolManager.freeConnection( "sot", con );
            }
            
        }
        
    }
    
    /**
     * Se encarga de limpiar la tabla 'cxp_items_doc' del esquema 'fin' en la web.
     * @autor LREALES
     * @param el distrito, el proveedor, el tipo de documento y el documento.
     * @throws Exception En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void limpiarCXP_ITEMS_DOC( String dist, String prov, String tipo_docu, String docu ) throws Exception{
        
        Connection con = null;
        PreparedStatement st = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( "sot" );
            
            if( con != null ){
                
                st = con.prepareStatement( this.SQL_LIMPIAR_CXP_ITEMS_DOC );
                
                st.setString( 1, dist );
                st.setString( 2, prov );
                st.setString( 3, tipo_docu );
                st.setString( 4, docu );
                
                /* INICIO MOD PA TRANSACCION */
                //ejecutar = st.toString() + ";";
                /* FIN */
                
                st.execute();
                
            }
            
        } catch( Exception e ){
            
            e.printStackTrace();
            throw new Exception( "ERROR EN 'limpiarCXP_ITEMS_DOC' - [MigracionFacturas].. " + e.getMessage() );
            
        }
        
        finally{
            
            if ( st != null ){
                try{
                    st.close();
                } catch( Exception e ){
                    throw new Exception( "ERROR CERRANDO EL ESTAMENTO " + e.getMessage() );
                }
            }
            if ( con != null ){
                poolManager.freeConnection( "sot", con );
            }
            
        }
        
    }
    
    /**
     * Se encarga de limpiar la tabla 'cxp_imp_item' del esquema 'fin' en la web.
     * @autor LREALES
     * @param el distrito, el proveedor, el tipo de documento y el documento.
     * @throws Exception En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void limpiarCXP_IMP_ITEM( String dist, String prov, String tipo_docu, String docu ) throws Exception{
        
        Connection con = null;
        PreparedStatement st = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( "sot" );
            
            if( con != null ){
                
                st = con.prepareStatement( this.SQL_LIMPIAR_CXP_IMP_ITEM );
                
                st.setString( 1, dist );
                st.setString( 2, prov );
                st.setString( 3, tipo_docu );
                st.setString( 4, docu );
                
                /* INICIO MOD PA TRANSACCION */
                //ejecutar = st.toString() + ";";
                /* FIN */
                
                st.execute();
                
            }
            
        } catch( Exception e ){
            
            e.printStackTrace();
            throw new Exception( "ERROR EN 'limpiarCXP_IMP_ITEM' - [MigracionFacturas].. " + e.getMessage() );
            
        }
        
        finally{
            
            if ( st != null ){
                try{
                    st.close();
                } catch( Exception e ){
                    throw new Exception( "ERROR CERRANDO EL ESTAMENTO " + e.getMessage() );
                }
            }
            if ( con != null ){
                poolManager.freeConnection( "sot", con );
            }
            
        }
        
    }
    
    /**
     * Se encarga de limpiar la tabla 'cxp_imp_doc' del esquema 'fin' en la web.
     * @autor LREALES
     * @param el distrito, el proveedor, el tipo de documento y el documento.
     * @throws Exception En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void limpiarCXP_IMP_DOC( String dist, String prov, String tipo_docu, String docu ) throws Exception{
        
        Connection con = null;
        PreparedStatement st = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( "sot" );
            
            if( con != null ){
                
                st = con.prepareStatement( this.SQL_LIMPIAR_CXP_IMP_DOC );
                
                st.setString( 1, dist );
                st.setString( 2, prov );
                st.setString( 3, tipo_docu );
                st.setString( 4, docu );
                
                /* INICIO MOD PA TRANSACCION */
                //ejecutar = st.toString() + ";";
                /* FIN */
                
                st.execute();
                
            }
            
        } catch( Exception e ){
            
            e.printStackTrace();
            throw new Exception( "ERROR EN 'limpiarCXP_IMP_DOC' - [MigracionFacturas].. " + e.getMessage() );
            
        }
        
        finally{
            
            if ( st != null ){
                try{
                    st.close();
                } catch( Exception e ){
                    throw new Exception( "ERROR CERRANDO EL ESTAMENTO " + e.getMessage() );
                }
            }
            if ( con != null ){
                poolManager.freeConnection( "sot", con );
            }
            
        }
        
    }
    
    /**
     * Obtiene una lista de las facturas de cuentas canceladas en mims.
     * @autor LREALES
     * @param -
     * @throws Exception En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void facturasCanceladas( String ref, String dia_ult_ejecucion, String hora_ult_ejecucion, String dia_actual_oracle, String hora_actual_oracle ) throws Exception{
        
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        String sql ="";
        String anterior = null;
        
        String temp_prov = "";
        String temp_docu = "";
        String temp_item = "";
        String temp_cimp = "";
        String temp_plan = "";
        
        this.tree_map_imp_nit = new TreeMap();
        this.tree_map_imp_pro = new TreeMap();
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( "oracle" );
            
            if( con != null ){
                
                
                st = con.prepareStatement( this.SQL_FACTURAS_CANCELADAS );

                st.setString ( 1, dia_ult_ejecucion );
                st.setString ( 2, hora_ult_ejecucion );
                
                rs = st.executeQuery();
                
                this.vector = new Vector();
                
                ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
                ruta = rb.getString("ruta");
                String fecha = Util.getFechaActual_String(6);
                fecha = fecha.replaceAll( "/", "-" );
                fecha = fecha.replaceAll( ":", "_" );
                rutalog = ruta + "/exportar/migracion/" + usuario + "/log_migracion_facturas_" +  fecha +".txt";
                
                this.fw             = new FileWriter    ( rutalog );
                this.bf             = new BufferedWriter(this.fw);
                this.linea          = new PrintWriter   (this.bf);
                
                linea.println("FACTURAS NO MIGRADAS :");
                linea.println("");
                linea.println("");
                linea.println("*   El Banco con la Sucursal especificada, no se encuentra en Postgres.");
                linea.println("");
                linea.println("**  El Nit del proveedor se encuentra vacio en Mims.");
                //linea.println("");
                //linea.println("-   Esta factura es de CARBON.");
                linea.println("");
                linea.println("*** El valor del item es igual a cero.");
                linea.println("");
                linea.println("");
                linea.println("");
                
                total_facturas_migradas = 0;
                total_items_migrados = 0;
                total_impuestos_migrados = 0;
                
                while ( rs.next() ){
                    
                    String distrito = rs.getString(1)!=null?rs.getString(1).toUpperCase():"";
                    String placa = rs.getString(2)!=null?rs.getString(2).toUpperCase():"";
                    String documento = rs.getString(3)!=null?rs.getString(3).toUpperCase():"";
                    String documento2 = rs.getString(4)!=null?rs.getString(4).toUpperCase():"";
                    
                    /*-documento-*/
                    documento2 = documento2.trim().equals("")?documento:documento2;
                    /*-documento-*/
                    
                    String vlr_fact = rs.getString(5)!=null?rs.getString(5):"0";
                    String vlrme_fact = rs.getString(6)!=null?rs.getString(6):"0";
                    String id_mims = rs.getString(7)!=null?rs.getString(7).toUpperCase():"";
                    String clase_doc = rs.getString(8)!=null?rs.getString(8).toUpperCase():"";
                    String moneda = rs.getString(9)!=null?rs.getString(9).toUpperCase():"";
                    String fec_grabacion = rs.getString(10)!=null?rs.getString(10).toUpperCase():"";
                    fec_grabacion = fec_grabacion.equals("")?"00990101":fec_grabacion;
                    fec_grabacion = fec_grabacion.substring( 0, 4 ) + "-" + fec_grabacion.substring( 4, 6 ) + "-" + fec_grabacion.substring( 6, 8 );
                    
                    String dia_modificacion = rs.getString(11)!=null?rs.getString(11).toUpperCase():"";
                    String hora_modificacion = rs.getString(12)!=null?rs.getString(12).toUpperCase():"";
                    String item = rs.getString(13)!=null?rs.getString(13).toUpperCase():"";
                    String descripcion = rs.getString(14)!=null?rs.getString(14).toUpperCase():"";
                    String vlr_item = rs.getString(15)!=null?rs.getString(15):"0";
                    String vlrme_item = rs.getString(16)!=null?rs.getString(16):"0";
                    String cuenta = rs.getString(17)!=null?rs.getString(17).toUpperCase():"";
                    String user_update = rs.getString(18)!=null?rs.getString(18).toUpperCase():"";
                    String planilla = rs.getString(19)!=null?rs.getString(19).toUpperCase():"";
                    String aprobador = rs.getString(20)!=null?rs.getString(20).trim().toUpperCase():"";
                    
                    String cod_imp = rs.getString(21)!=null?rs.getString(21).toUpperCase():"";
                    String porc_imp = rs.getString(22)!=null?rs.getString(22):"0";
                    String vlr_imp = rs.getString(23)!=null?rs.getString(23):"0";
                    String vlrme_imp = rs.getString(24)!=null?rs.getString(24):"0";
                    String vlr_iva = rs.getString(25)!=null?rs.getString(25):"0";
                    String banco = rs.getString(26)!=null?rs.getString(26).toUpperCase():"";
                    String sucursal = rs.getString(27)!=null?rs.getString(27).toUpperCase():"";
                    String hc = rs.getString(28)!=null?rs.getString(28).toUpperCase():"";
                    String fecdocumento = rs.getString(29)!=null?rs.getString(29).toUpperCase():"";
                    String fecpago = rs.getString(30)!=null?rs.getString(30).toUpperCase():"";
                    
                    String fecvencimiento = rs.getString(31)!=null?rs.getString(31).toUpperCase():"";
                    String fecaprobacion = rs.getString(32)!=null?rs.getString(32).toUpperCase():"";
                    String corrida = rs.getString(33)!=null?rs.getString(33).toUpperCase():"";
                    String cheque = rs.getString(34)!=null?rs.getString(34).toUpperCase():"";
                    String creation_user = rs.getString(35)!=null?rs.getString(35).toUpperCase():"";
                    String estado = rs.getString(36)!=null?rs.getString(36).toUpperCase():"";
                    
                    /**************************************************************************************************************/
                    String codigo_abc = rs.getString(37)!=null?rs.getString(37).toUpperCase():"";
                    String retencion = rs.getString(38)!=null?rs.getString(38).toUpperCase():"";
                    /**************************************************************************************************************/
                    
                    /**************************************************************************************************************/
                    /**************************************************************************************************************/
                    String tipo_subledger = rs.getString(39)!=null?rs.getString(39).trim().toUpperCase():"";
                    String cuenta_subledger = rs.getString(40)!=null?rs.getString(40).trim().toUpperCase():"";
                    String auxiliar = tipo_subledger.equals( "" )||cuenta_subledger.equals( "" )?"":tipo_subledger + "-" + cuenta_subledger;
                    /**************************************************************************************************************/
                    /**************************************************************************************************************/
                    
                    String proveedor = rs.getString(41)!=null?rs.getString(41).toUpperCase():"";
                    
                    
                    /*--- MOD 20 NOV ---*/
                    if ( fecaprobacion.trim().equals("") ) {
                        if ( estado.equals("30") || estado.equals("40") || estado.equals("50") ) {
                            fecaprobacion = fec_grabacion;
                        }
                    }
                    /*--- ---*/
                    
                    
                    if ( !moneda.equals("DOL") ) {
                        
                        double v_fact_d = Double.parseDouble( vlr_fact );
                        int v_fact_i = (int)  Math.round( v_fact_d );
                        vlr_fact = "" + v_fact_i;
                        
                        double vme_fact_d = Double.parseDouble( vlrme_fact );
                        int vme_fact_i = (int)  Math.round( vme_fact_d );
                        vlrme_fact = "" + vme_fact_i;
                        
                        double v_item_d = Double.parseDouble( vlr_item );
                        int v_item_i = (int)  Math.round( v_item_d );
                        vlr_item = "" + v_item_i;
                        
                        double vme_item_d = Double.parseDouble( vlrme_item );
                        int vme_item_i = (int)  Math.round( vme_item_d );
                        vlrme_item = "" + vme_item_i;
                        
                        double v_imp_d = Double.parseDouble( vlr_imp );
                        int v_imp_i = (int)  Math.round( v_imp_d );
                        vlr_imp = "" + v_imp_i;
                        
                        double vme_imp_d = Double.parseDouble( vlrme_imp );
                        int vme_imp_i = (int)  Math.round( vme_imp_d );
                        vlrme_imp = "" + vme_imp_i;
                        
                        double v_iva_d = Double.parseDouble( vlr_iva );
                        int v_iva_i = (int)  Math.round( v_iva_d );
                        vlr_iva = "" + v_iva_i;
                        
                        /*****************************************************************************************************/
                        double retencion_d = Double.parseDouble( retencion );
                        int retencion_i = (int)  Math.round( retencion_d );
                        retencion = "" + retencion_i;
                        /*****************************************************************************************************/
                        
                    }
                    
                    factura = new FacturasMims();
                    
                    factura.setEstado( estado );
                    
                    distrito = distrito.length() > 15?distrito.substring( 0,15 ):distrito;
                    factura.setDistrito( distrito );//15
                    factura.setPlaca( placa );// � 12
                    
                    //int cont_prueba = 0;
                    //if ( ( anterior != null ) && ( anterior.equals( proveedor + "|" + documento + "| ite " + item + "|" + cod_imp ) ) ) {
                    /***********************************************************************************************************/
                    /*-documento-*/
                    if ( documento2.length() >= 2 ) {
                        
                        if ( documento2.substring(0,2).equals("OP") ) {

                            documento2 = documento2 + " " + placa;

                        }
                        
                    }
                    /*-documento-*/
                    //cont_prueba = 1;
                    /***********************************************************************************************************/
                    //}
                    
                    /*-documento-*/
                    documento2 = documento2.length() > 30?documento2.substring( 0,30 ):documento2;
                    factura.setDocumento( documento2 );//30
                    /*-documento-*/
                    factura.setDocumento2( documento2 );
                    factura.setVlr_fact( vlr_fact );
                    factura.setVlrme_fact( vlrme_fact );
                    id_mims = id_mims.length() > 15?id_mims.substring( 0,15 ):id_mims;
                    factura.setId_mims( id_mims );//15
                    clase_doc = clase_doc.length() > 1?clase_doc.substring( 0,1 ):clase_doc;
                    factura.setClase_doc( clase_doc );//1
                    moneda = moneda.length() > 15?moneda.substring( 0,15 ):moneda;
                    factura.setMoneda( moneda );//15
                    factura.setFec_grabacion( fec_grabacion );
                    
                    factura.setDia_modificacion( dia_modificacion );
                    factura.setHora_modificacion( hora_modificacion );
                    item = item.length() > 30?item.substring( 0,30 ):item;
                    factura.setItem( item );//30
                    factura.setDescripcion( descripcion );
                    factura.setVlr_item( vlr_item );
                    factura.setVlrme_item( vlrme_item );
                    cuenta = cuenta.length() > 30?cuenta.substring( 0,30 ):cuenta;
                    factura.setCuenta( cuenta );//30
                    creation_user = creation_user.length() > 15?creation_user.substring( 0,15 ):creation_user;
                    factura.setCreation_user( creation_user );//15
                    factura.setUser_update( user_update );
                    planilla = planilla.length() > 15?planilla.substring( 0,15 ):planilla;
                    factura.setPlanilla( planilla );//15
                    factura.setAprobador( aprobador );//15
                    
                    /************************************************************************/
                    cod_imp = cod_imp.length() > 15?cod_imp.substring( 0,15 ):cod_imp;
                    
                    String cod_imp_post = buscarCodImp( cod_imp );
                    
                    factura.setCod_imp( cod_imp_post );//15
                    
                    /**/
                    String sig_str_r = buscarSignoImp( "RT01" ).equals("")?"1":buscarSignoImp( "RT01" );
                    int signo_r = Integer.parseInt( sig_str_r );
                    
                    double vlr_ret_r = Double.parseDouble( retencion );
                    
                    if ( vlr_ret_r > 0 ) {
                        vlr_ret_r = vlr_ret_r * signo_r;
                    }
                    retencion = "" + vlr_ret_r;
                    /**/
                    String sig_str = buscarSignoImp( cod_imp_post ).equals("")?"1":buscarSignoImp( cod_imp_post );
                    int signo = Integer.parseInt( sig_str );
                    
                    
                    double vlr_imp_d = Double.parseDouble( vlr_imp );
                    
                    if ( signo == -1 && vlr_imp_d < 0 )
                        vlr_imp_d = vlr_imp_d;
                    else
                        vlr_imp_d = vlr_imp_d * signo;
                    
                    vlr_imp = "" + vlr_imp_d;
                    
                    
                    double vlrme_imp_d = Double.parseDouble( vlrme_imp );
                    
                    if ( signo == -1 && vlrme_imp_d < 0 )
                        vlrme_imp_d = vlrme_imp_d;
                    else
                        vlrme_imp_d = vlrme_imp_d * signo;
                    
                    vlrme_imp = "" + vlrme_imp_d;
                    /************************************************************************/
                    
                    factura.setPorc_imp( porc_imp );//
                    factura.setVlr_imp( vlr_imp );
                    factura.setVlrme_imp( vlrme_imp );
                    factura.setVlr_iva( vlr_iva );
                    banco = banco.length() > 30?banco.substring( 0,30 ):banco;
                    factura.setBanco( banco );//30
                    sucursal = sucursal.length() > 30?sucursal.substring( 0,30 ):sucursal;
                    factura.setSucursal( sucursal );//30
                    hc = hc.length() > 15?hc.substring( 0,15 ):hc;
                    factura.setHc( hc );//15
                    factura.setFecdocumento( fecdocumento );
                    
                    factura.setFecpago( fecpago );
                    factura.setFecvencimiento( fecvencimiento );
                    factura.setFecaprobacion( fecaprobacion );
                    corrida = corrida.length() > 10?corrida.substring( 0,10 ):corrida;
                    factura.setCorrida( corrida );//10
                    cheque = cheque.length() > 30?cheque.substring( 0,30 ):cheque;
                    factura.setCheque( cheque );//30
                    proveedor = proveedor.length() > 15?proveedor.substring( 0,15 ):proveedor;
                    factura.setProveedor( proveedor );//15
                    
                    /**************************************************************************************************************/
                    codigo_abc = codigo_abc.length() > 30?codigo_abc.substring( 0,30 ):codigo_abc;
                    factura.setCodigo_abc( codigo_abc );//30
                    
                    double vlr_retencion = Double.parseDouble( retencion );
                    double vlr_retencion_me = Double.parseDouble( retencion );
                    
                    Tasa tasa = model.tasaService.buscarValorTasa( moneda_del_distrito, moneda, moneda_del_distrito, fec_grabacion );
                    if ( tasa != null ) {
                        vlr_retencion = tasa.getValor_tasa() * vlr_retencion;
                    }
                    
                    factura.setRetencion( "" + vlr_retencion );//
                    factura.setRetencion_me( "" + vlr_retencion_me );//
                    /**************************************************************************************************************/
                    
                    auxiliar = auxiliar.length() > 25?auxiliar.substring( 0,25 ):auxiliar;
                    factura.setAuxiliar( auxiliar );
                    
                    vector.add( factura );
                    
                    if ( ( anterior==null ) || ( !anterior.equals( proveedor + "|" + documento2 + "| ite " + item + "|" + cod_imp ) ) ) {
                        
                        calcular( temp_plan, linea );
                        vector = new Vector();
                        
                    }
                    
                    anterior = proveedor + "|" + documento2 + "| ite " + item + "|" + cod_imp;
                    
                    temp_prov = proveedor;
                    /*-documento-*/
                    temp_docu = documento2;
                    /*-documento-*/
                    temp_item = item;
                    temp_cimp = cod_imp;
                    
                    /*** 7 NOVIEMBRE 2006 ***/
                    if ( !planilla.trim().equals("") )
                        temp_plan = planilla;
                    /*** ***/
                    
                }
                
                linea.close();
                
            }
            
        } catch( Exception e ){
            
            e.printStackTrace();
            throw new Exception( "ERROR EN 'facturasCanceladas' - [MigracionFacturas] " + e.getMessage() );
            
        }
        
        finally{
            
            if ( st != null ){
                try{
                    st.close();
                } catch( Exception e ){
                    throw new Exception( "ERROR CERRANDO EL ESTAMENTO " + e.getMessage() );
                }
            }
            if ( con != null ){
                poolManager.freeConnection( "oracle", con );
            }
            
        }
        
    }
    
    /**
     * Se encarga de buscar la agencia segun el banco y la sucursal.
     * @autor LREALES
     * @param distrito, banco y sucursal.
     * @throws Exception En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public String buscarAgencia( String dstrct, String banco, String sucursal ) throws Exception{
        
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        String agencia = "";
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( "sot" );
            
            if( con != null ){
                
                st = con.prepareStatement( this.SQL_AGENCIA );
                
                st.setString( 1, dstrct );
                st.setString( 2, banco );
                st.setString( 3, sucursal );
                
                rs = st.executeQuery();
                
                if ( rs.next() ){
                    
                    agencia = rs.getString( "agency_id" ).toUpperCase();
                    
                }
                
            }
            
        } catch( Exception e ){
            
            e.printStackTrace();
            throw new Exception( "ERROR EN 'buscarAgencia' - [MigracionFacturas].. " + e.getMessage() );
            
        }
        
        finally{
            
            if ( st != null ){
                try{
                    st.close();
                } catch( Exception e ){
                    throw new Exception( "ERROR CERRANDO EL ESTAMENTO " + e.getMessage() );
                }
            }
            if ( con != null ){
                poolManager.freeConnection( "sot", con );
            }
            
        }
        
        return agencia;
        
    }
    
    /**
     * Se encarga de buscar la moneda del banco segun el banco y la sucursal.
     * @autor LREALES
     * @param distrito, banco y sucursal.
     * @throws Exception En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public String buscarMonedaBanco( String dstrct, String banco, String sucursal ) throws Exception{
        
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        String moneda_banco = "";
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( "sot" );
            
            if( con != null ){
                
                st = con.prepareStatement( this.SQL_MONEDA_BANCO );
                
                st.setString( 1, dstrct );
                st.setString( 2, banco );
                st.setString( 3, sucursal );
                
                rs = st.executeQuery();
                
                if ( rs.next() ){
                    
                    moneda_banco = rs.getString( "currency" ).toUpperCase();
                    
                }
                
            }
            
        } catch( Exception e ){
            
            e.printStackTrace();
            throw new Exception( "ERROR EN 'buscarAgencia' - [MigracionFacturas].. " + e.getMessage() );
            
        }
        
        finally{
            
            if ( st != null ){
                try{
                    st.close();
                } catch( Exception e ){
                    throw new Exception( "ERROR CERRANDO EL ESTAMENTO " + e.getMessage() );
                }
            }
            if ( con != null ){
                poolManager.freeConnection( "sot", con );
            }
            
        }
        
        return moneda_banco;
        
    }
    
    /**
     * Se encarga de buscar el nombre y apellido del aprobador.
     * @autor LREALES
     * @param id de mims.
     * @throws Exception En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public String buscarLoginAprobador( String autorizador ) throws Exception{
        
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        String nombre1 = "";
        String apellido1 = "";
        String login = "";
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( "oracle" );
            
            if( con != null ){
                
                st = con.prepareStatement( this.SQL_LOGIN_APROBADOR );
                
                st.setString( 1, autorizador );
                
                rs = st.executeQuery();
                
                if ( rs.next() ){
                    
                    nombre1 = rs.getString(1)!=null?rs.getString(1).trim().toUpperCase():"";
                    nombre1 = nombre1.equals("")?"":nombre1.substring( 0, 1 );
                    apellido1 = rs.getString(2)!=null?rs.getString(2).trim().toUpperCase():"";
                    
                }
                
                login = nombre1 + apellido1;
                
            }
            
        } catch( Exception e ){
            
            e.printStackTrace();
            throw new Exception( "ERROR EN 'buscarLoginAprobador' - [MigracionFacturas].. " + e.getMessage() );
            
        }
        
        finally{
            
            if ( st != null ){
                try{
                    st.close();
                } catch( Exception e ){
                    throw new Exception( "ERROR CERRANDO EL ESTAMENTO " + e.getMessage() );
                }
            }
            if ( con != null ){
                poolManager.freeConnection( "oracle", con );
            }
            
        }
        
        return login;
        
    }
    
    /**
     * M�todo que permite ingresar los registros a la tabla cxp_doc del esquema fin.
     * @autor :         LREALES
     * @param:          1 Objeto.
     * @throws:         Exception
     * @version :       1.0
     */
    public void transferirCXP_DOC( FacturasMims fm ) throws Exception{
        
        Connection con = null;
        PreparedStatement st = null;
        PoolManager poolManager = null;
        
        boolean sw_vlr_neto1 = false;
        boolean sw_vlr_neto_me1 = false;
        
        try{
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( "sot" );
            
            if( con != null ){
                
                st = con.prepareStatement( this.SQL_CXP_DOC );
                
                double vlr_neto1 = Double.parseDouble( fm.getVlr_neto() );
                double vlr_total_abonos1 = Double.parseDouble( fm.getVlr_total_abonos() );
                double vlr_saldo1 = Double.parseDouble( fm.getVlr_saldo() );
                double vlr_neto_me1 = Double.parseDouble( fm.getVlr_neto_me() );
                double vlr_total_abonos_me1 = Double.parseDouble( fm.getVlr_total_abonos_me() );
                double vlr_saldo_me1 = Double.parseDouble( fm.getVlr_saldo_me() );
                double tasa1 = Double.parseDouble( fm.getTasa() );
                
                if ( vlr_neto1 == 0 ) {
                    tasa1 = 0;
                } else {
                    tasa1 = ( vlr_neto_me1 / vlr_neto1 );
                }
                
                int num_obs_autorizador1 = Integer.parseInt( fm.getNum_obs_autorizador() );
                int num_obs_pagador1 = Integer.parseInt( fm.getNum_obs_pagador() );
                int num_obs_registra1 = Integer.parseInt( fm.getNum_obs_registra() );
                
                int transaccion_ajc1 = Integer.parseInt( fm.getTransaccion_ajc() );
                int transaccion_ajv1 = Integer.parseInt( fm.getTransaccion_ajv() );
                
                int transaccion1 = Integer.parseInt( fm.getTransaccion() );
                
                double vlr_total_impuesto1 = Double.parseDouble( fm.getVlr_imp() );
                double vlr_total_impuesto_me1 = Double.parseDouble( fm.getVlrme_imp() );
                
                vlr_saldo1 = vlr_neto1 - vlr_total_abonos1;
                
                vlr_saldo_me1 = vlr_neto_me1 - vlr_total_abonos_me1;
                
                st.setString( 1, fm.getReg_status() );
                st.setString( 2, fm.getDistrito() );
                st.setString( 3, fm.getProveedor() );
                st.setString( 4, fm.getTipo_documento() );
                st.setString( 5, fm.getDocumento() );
                st.setString( 6, fm.getDescripcionfact() );
                st.setString( 7, fm.getAgencia() );
                st.setString( 8, fm.getHandle_code() );
                st.setString( 9, fm.getId_mims() );
                st.setString( 10, fm.getTipo_documento_rel() );
                st.setString( 11, fm.getDocumento_relacionado() );
                st.setString( 12, fm.getFecha_aprobacion() );
                st.setString( 13, fm.getAprobador() );
                st.setString( 14, fm.getUsuario_aprobacion() );
                st.setString( 15, fm.getBanco() );
                st.setString( 16, fm.getSucursal() );
                st.setString( 17, fm.getMoneda() );
                st.setDouble( 18, vlr_neto1 );
                st.setDouble( 19, vlr_total_abonos1 );
                st.setDouble( 20, vlr_saldo1 );
                st.setDouble( 21, vlr_neto_me1 );
                st.setDouble( 22, vlr_total_abonos_me1 );
                st.setDouble( 23, vlr_saldo_me1 );
                st.setDouble( 24, tasa1 );
                st.setString( 25, fm.getUsuario_contabilizo() );
                st.setString( 26, fm.getFecha_contabilizacion() );
                st.setString( 27, fm.getUsuario_anulo() );
                st.setString( 28, fm.getFecha_anulacion() );
                st.setString( 29, fm.getFecha_contabilizacion_anulacion() );
                st.setString( 30, fm.getObservacion() );
                st.setInt( 31, num_obs_autorizador1 );
                st.setInt( 32, num_obs_pagador1 );
                st.setInt( 33, num_obs_registra1 );
                st.setString( 34, fm.getLast_update() );
                st.setString( 35, fm.getUser_update() );
                st.setString( 36, fm.getCreation_date() );
                st.setString( 37, fm.getCreation_user() );
                st.setString( 38, fm.getBase() );
                st.setString( 39, fm.getCorrida() );
                st.setString( 40, fm.getCheque() );
                st.setString( 41, fm.getPeriodo() );
                st.setString( 42, fm.getFecha_contabilizacion_ajc() );
                st.setString( 43, fm.getFecha_contabilizacion_ajv() );
                st.setString( 44, fm.getPeriodo_ajc() );
                st.setString( 45, fm.getPeriodo_ajv() );
                st.setString( 46, fm.getUsuario_contabilizo_ajc() );
                st.setString( 47, fm.getUsuario_contabilizo_ajv() );
                st.setInt( 48, transaccion_ajc1 );
                st.setInt( 49, transaccion_ajv1 );
                st.setString( 50, fm.getFecha_procesado() );
                st.setString( 51, fm.getClase_documento() );
                st.setInt( 52, transaccion1 );
                st.setString( 53, fm.getMoneda_banco() );
                st.setString( 54, fm.getFecdocumento() );
                st.setString( 55, fm.getFecha_vencimiento() );
                st.setString( 56, fm.getUltima_fecha_pago() );
                
                st.execute();
                
                total_facturas_migradas++;
                
                existe_nit = this.existeNit( fm.getProveedor() );
                if ( !existe_nit ) {
                    
                    this.tree_map_imp_nit.put( fm.getProveedor(), fm.getProveedor() );
                    
                }
                
                existe_pro = this.existeProveedor( fm.getProveedor() );
                if ( !existe_pro ) {
                    
                    this.tree_map_imp_pro.put( fm.getProveedor(), fm.getProveedor() );
                    
                }
                
            }
            
        } catch( Exception e ){
            
            e.printStackTrace();
            throw new Exception( "ERROR DURANTE 'transferirCXP_DOC' - [MigracionFacturas].. " + e.getMessage() );
            
        }
        
        finally{
            
            if ( st != null ){
                try{
                    st.close();
                } catch( Exception e ){
                    throw new Exception( "ERROR CERRANDO EL ESTAMENTO " + e.getMessage() );
                }
            }
            if ( con != null ){
                poolManager.freeConnection( "sot", con );
            }
            
        }
        
    }
    
    /**
     * M�todo que permite ingresar los registros a la tabla cxp_items_doc del esquema fin.
     * @autor :         LREALES
     * @param:          1 Objeto.
     * @throws:         Exception
     * @version :       1.0
     */
    public void transferirCXP_ITEMS_DOC( FacturasMims fm ) throws Exception{
        
        Connection con = null;
        PreparedStatement st = null;
        PoolManager poolManager = null;
        
        try{
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( "sot" );
            
            if( con != null ){
                
                st = con.prepareStatement( this.SQL_CXP_ITEMS_DOC );
                
                double vlr1 = Double.parseDouble( fm.getVlritem() );
                double vlr_me1 = Double.parseDouble( fm.getVlrmeitem() );
                
                st.setString( 1, fm.getReg_status() );
                st.setString( 2, fm.getDistrito() );
                st.setString( 3, fm.getProveedor() );
                st.setString( 4, fm.getTipo_documento() );
                st.setString( 5, fm.getDocumento() );
                st.setString( 6, fm.getItem() );
                st.setString( 7, fm.getDescripcion() );
                st.setDouble( 8, vlr1 );
                st.setDouble( 9, vlr_me1 );
                st.setString( 10, fm.getCuenta() );
                st.setString( 11, fm.getCodigo_abc() );
                st.setString( 12, fm.getPlanilla() );
                st.setString( 13, fm.getLast_update() );
                st.setString( 14, fm.getUser_update() );
                st.setString( 15, fm.getCreation_date() );
                st.setString( 16, fm.getCreation_user() );
                st.setString( 17, fm.getBase() );
                st.setString( 18, fm.getCodcliarea() );
                st.setString( 19, fm.getTipcliarea() );
                st.setString( 20, fm.getConcepto() );
                st.setString( 21, fm.getAuxiliar() );
                
                st.execute();
                
                total_items_migrados++;
                
            }
            
        } catch( Exception e ){
            
            e.printStackTrace();
            throw new Exception( "ERROR DURANTE 'transferirCXP_ITEMS_DOC' - [MigracionFacturas].. " + e.getMessage() );
            
        }
        
        finally{
            
            if ( st != null ){
                try{
                    st.close();
                } catch( Exception e ){
                    throw new Exception( "ERROR CERRANDO EL ESTAMENTO " + e.getMessage() );
                }
            }
            if ( con != null ){
                poolManager.freeConnection( "sot", con );
            }
            
        }
        
    }
    
    /**
     * M�todo que permite ingresar los registros a la tabla cxp_imp_item del esquema fin.
     * @autor :         LREALES
     * @param:          1 Objeto.
     * @throws:         Exception
     * @version :       1.0
     */
    public void transferirCXP_IMP_ITEM( FacturasMims fm  ) throws Exception{
        
        Connection con = null;
        PreparedStatement st = null;
        PoolManager poolManager = null;
        
        try{
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( "sot" );
            
            if ( !fm.getCod_imp().equals("") ){
                
                if( con != null ){
                    
                    st = con.prepareStatement( this.SQL_CXP_IMP_ITEM );
                    
                    double vlr_total_impuesto1 = Double.parseDouble( fm.getVlr_imp() );
                    double vlr_total_impuesto_me1 = Double.parseDouble( fm.getVlrme_imp() );
                    double porcent_impuesto1 = Double.parseDouble( fm.getPorc_imp() );
                    
                    st.setString( 1, fm.getReg_status() );
                    st.setString( 2, fm.getDistrito() );
                    st.setString( 3, fm.getProveedor() );
                    st.setString( 4, fm.getTipo_documento() );
                    st.setString( 5, fm.getDocumento() );
                    st.setString( 6, fm.getItem() );
                    st.setString( 7, fm.getCod_imp() );
                    st.setDouble( 8, porcent_impuesto1 );
                    st.setDouble( 9, vlr_total_impuesto1 );
                    st.setDouble( 10, vlr_total_impuesto_me1 );
                    st.setString( 11, fm.getLast_update() );
                    st.setString( 12, fm.getUser_update() );
                    st.setString( 13, fm.getCreation_date() );
                    st.setString( 14, fm.getCreation_user() );
                    st.setString( 15, fm.getBase() );
                    
                    st.execute();
                    
                    total_impuestos_migrados++;
                    
                }
                
            }
            
        } catch( Exception e ){
            
            e.printStackTrace();
            throw new Exception( "ERROR DURANTE 'transferirCXP_IMP_ITEM' - [MigracionFacturas].. " + e.getMessage() );
            
        }
        
        finally{
            
            if ( st != null ){
                try{
                    st.close();
                } catch( Exception e ){
                    throw new Exception( "ERROR CERRANDO EL ESTAMENTO " + e.getMessage() );
                }
            }
            if ( con != null ){
                poolManager.freeConnection( "sot", con );
            }
            
        }
        
    }
    
    /**
     * M�todo que permite ingresar los registros a la tabla cxp_imp_doc del esquema fin.
     * @autor :         LREALES
     * @param:          1 Objeto.
     * @throws:         Exception
     * @version :       1.0
     */
    public void transferirCXP_IMP_DOC( FacturasMims fm  ) throws Exception{
        
        Connection con = null;
        PreparedStatement st = null;
        PoolManager poolManager = null;
        
        try{
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( "sot" );
            
            if ( !fm.getCod_imp().equals("") ){
                
                if( con != null ){
                    
                    st = con.prepareStatement( this.SQL_CXP_IMP_DOC );
                    
                    double vlr_total_impuesto1 = Double.parseDouble( fm.getVlr_imp() );
                    double vlr_total_impuesto_me1 = Double.parseDouble( fm.getVlrme_imp() );
                    double porcent_impuesto1 = Double.parseDouble( fm.getPorc_imp() );
                    
                    st.setString( 1, fm.getReg_status() );
                    st.setString( 2, fm.getDistrito() );
                    st.setString( 3, fm.getProveedor() );
                    st.setString( 4, fm.getTipo_documento() );
                    st.setString( 5, fm.getDocumento() );
                    st.setString( 6, fm.getCod_imp() );
                    st.setDouble( 7, porcent_impuesto1 );
                    st.setDouble( 8, vlr_total_impuesto1 );
                    st.setDouble( 9, vlr_total_impuesto_me1 );
                    st.setString( 10, fm.getLast_update() );
                    st.setString( 11, fm.getUser_update() );
                    st.setString( 12, fm.getCreation_date() );
                    st.setString( 13, fm.getCreation_user() );
                    st.setString( 14, fm.getBase() );
                    
                    st.execute();
                    
                    total_imp_cabecera_migrados++;
                    
                }
                
            }
            
        } catch( Exception e ){
            
            e.printStackTrace();
            throw new Exception( "ERROR DURANTE 'transferirCXP_IMP_DOC' - [MigracionFacturas].. " + e.getMessage() );
            
        }
        
        finally{
            
            if ( st != null ){
                try{
                    st.close();
                } catch( Exception e ){
                    throw new Exception( "ERROR CERRANDO EL ESTAMENTO " + e.getMessage() );
                }
            }
            if ( con != null ){
                poolManager.freeConnection( "sot", con );
            }
            
        }
        
    }
    
    /**
     * Se encarga de buscar la referencia de la fecha del proceso de la migracion de la 260.
     * @autor LREALES
     * @param -
     * @throws Exception En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public String buscarFecProcesoMig260() throws Exception{
        
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        String referencia = "";
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( "sot" );
            
            if( con != null ){
                
                st = con.prepareStatement( this.SQL_FECPROCESO_MIG260 );
                
                rs = st.executeQuery();
                
                if ( rs.next() ){
                    
                    referencia = rs.getString( "referencia" ).toUpperCase();
                    
                }
                
            }
            
        } catch( Exception e ){
            
            e.printStackTrace();
            throw new Exception( "ERROR EN 'buscarFecProcesoMig260' - [MigracionFacturas].. " + e.getMessage() );
            
        }
        
        finally{
            
            if ( st != null ){
                try{
                    st.close();
                } catch( Exception e ){
                    throw new Exception( "ERROR CERRANDO EL ESTAMENTO " + e.getMessage() );
                }
            }
            if ( con != null ){
                poolManager.freeConnection( "sot", con );
            }
            
        }
        
        return referencia;
        
    }
    
    /**
     * Se encarga de buscar la referencia de la fecha del proceso de la migracion de la 260.
     * @autor LREALES
     * @param -
     * @throws Exception En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void actualizarFecProcesoMig260( String dia_actual_oracle, String hora_actual_oracle ) throws Exception{
        
        Connection con = null;
        PreparedStatement st = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( "sot" );
            
            if( con != null ){
                
                st = con.prepareStatement( this.SQL_UPDATE_FECPROCESO_MIG260 );
                
                st.setString( 1, dia_actual_oracle + hora_actual_oracle );
                
                st.executeUpdate();
                
            }
            
        } catch( Exception e ){
            
            e.printStackTrace();
            throw new Exception( "ERROR EN 'actualizarFecProcesoMig260' - [MigracionFacturas].. " + e.getMessage() );
            
        }
        
        finally{
            
            if ( st != null ){
                try{
                    st.close();
                } catch( Exception e ){
                    throw new Exception( "ERROR CERRANDO EL ESTAMENTO " + e.getMessage() );
                }
            }
            if ( con != null ){
                poolManager.freeConnection( "sot", con );
            }
            
        }
        
    }
    
    /**
     * Se encarga de verificar si existe o no la Factura en CXP_DOC.
     * @autor LREALES
     * @param el distrito, el proveedor, el tipo_documento, y el documento.
     * @throws Exception En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public boolean existeCXP_DOC( String distrito, String proveedor, String tipo_documento, String documento ) throws Exception{
        
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        boolean sw = false;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( "sot" );
            
            if( con != null ){
                
                st = con.prepareStatement( this.SQL_EXISTE_CXP_DOC );
                
                st.setString( 1, distrito );
                st.setString( 2, proveedor );
                st.setString( 3, tipo_documento );
                st.setString( 4, documento );
                
                rs = st.executeQuery();
                
                if ( rs.next() ){
                    
                    sw = true;
                    
                }

            }
            
        } catch( Exception e ){
            
            e.printStackTrace();
            throw new Exception( "ERROR EN 'existeCXP_DOC' - [MigracionFacturas].. " + e.getMessage() );
            
        }
        
        finally{
            
            if ( st != null ){
                try{
                    st.close();
                } catch( Exception e ){
                    throw new Exception( "ERROR CERRANDO EL ESTAMENTO " + e.getMessage() );
                }
            }
            if ( con != null ){
                poolManager.freeConnection( "sot", con );
            }
            
        }
        
        return sw;
        
    }
    
    /**
     * Se encarga de verificar si existe o no el item en CXP_TEMS_DOC.
     * @autor LREALES
     * @param el distrito, el proveedor, el tipo_documento, el documento y el item.
     * @throws Exception En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public boolean existeCXP_ITEMS_DOC( String distrito, String proveedor, String tipo_documento, String documento, String item ) throws Exception{
        
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        boolean sw = false;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( "sot" );
            
            if( con != null ){
                
                st = con.prepareStatement( this.SQL_EXISTE_CXP_ITEMS_DOC );
                
                st.setString( 1, distrito );
                st.setString( 2, proveedor );
                st.setString( 3, tipo_documento );
                st.setString( 4, documento );
                st.setString( 5, item );
                
                rs = st.executeQuery();
                
                if ( rs.next() ){
                    
                    sw = true;
                    
                }
                
            }
            
        } catch( Exception e ){
            
            e.printStackTrace();
            throw new Exception( "ERROR EN 'existeCXP_ITEMS_DOC' - [MigracionFacturas].. " + e.getMessage() );
            
        }
        
        finally{
            
            if ( st != null ){
                try{
                    st.close();
                } catch( Exception e ){
                    throw new Exception( "ERROR CERRANDO EL ESTAMENTO " + e.getMessage() );
                }
            }
            if ( con != null ){
                poolManager.freeConnection( "sot", con );
            }
            
        }
        
        return sw;
        
    }
    
    /**
     * Se encarga de verificar si existe o no el impuesto en CXP_IMP_ITEM.
     * @autor LREALES
     * @param el distrito, el proveedor, el tipo_documento, el documento, el item y el codigo del impuesto.
     * @throws Exception En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public boolean existeCXP_IMP_ITEM( String distrito, String proveedor, String tipo_documento, String documento, String item, String cod_impuesto ) throws Exception{
        
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        boolean sw = false;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( "sot" );
            
            if( con != null ){
                
                st = con.prepareStatement( this.SQL_EXISTE_CXP_IMP_ITEM );
                
                st.setString( 1, distrito );
                st.setString( 2, proveedor );
                st.setString( 3, tipo_documento );
                st.setString( 4, documento );
                st.setString( 5, item );
                st.setString( 6, cod_impuesto );
                
                rs = st.executeQuery();
                
                if ( rs.next() ){
                    
                    sw = true;
                    
                }
                
            }
            
        } catch( Exception e ){
            
            e.printStackTrace();
            throw new Exception( "ERROR EN 'existeCXP_IMP_ITEM' - [MigracionFacturas].. " + e.getMessage() );
            
        }
        
        finally{
            
            if ( st != null ){
                try{
                    st.close();
                } catch( Exception e ){
                    throw new Exception( "ERROR CERRANDO EL ESTAMENTO " + e.getMessage() );
                }
            }
            if ( con != null ){
                poolManager.freeConnection( "sot", con );
            }
            
        }
        
        return sw;
        
    }
    
    /**
     * Se encarga de verificar si existe o no el impuesto en CXP_IMP_DOC.
     * @autor LREALES
     * @param el distrito, el proveedor, el tipo_documento, el documento y el codigo del impuesto.
     * @throws Exception En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public boolean existeCXP_IMP_DOC( String distrito, String proveedor, String tipo_documento, String documento, String cod_impuesto ) throws Exception {
        
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        boolean sw = false;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( "sot" );
            
            if( con != null ){
                
                st = con.prepareStatement( this.SQL_EXISTE_CXP_IMP_DOC );
                
                st.setString( 1, distrito );
                st.setString( 2, proveedor );
                st.setString( 3, tipo_documento );
                st.setString( 4, documento );
                st.setString( 5, cod_impuesto );
                
                rs = st.executeQuery();
                
                if ( rs.next() ){
                    
                    sw = true;
                    
                }
                
            }
            
        } catch( Exception e ){
            
            e.printStackTrace();
            throw new Exception( "ERROR EN 'existeCXP_IMP_DOC' - [MigracionFacturas].. " + e.getMessage() );
            
        }
        
        finally{
            
            if ( st != null ){
                try{
                    st.close();
                } catch( Exception e ){
                    throw new Exception( "ERROR CERRANDO EL ESTAMENTO " + e.getMessage() );
                }
            }
            if ( con != null ){
                poolManager.freeConnection( "sot", con );
            }
            
        }
        
        return sw;
        
    }
    
    /**
     * Se encarga de obtener el valor de la tabla CXP_IMP_DOC.
     * @autor LREALES
     * @param el distrito, el proveedor, el tipo_documento, el documento y el codigo del impuesto.
     * @throws Exception En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public String valor_CXP_IMP_DOC( String distrito, String proveedor, String tipo_documento, String documento, String cod_impuesto ) throws Exception {
        
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        String valor = "0";
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( "sot" );
            
            if( con != null ){
                
                st = con.prepareStatement( this.SQL_EXISTE_CXP_IMP_DOC );
                
                st.setString( 1, distrito );
                st.setString( 2, proveedor );
                st.setString( 3, tipo_documento );
                st.setString( 4, documento );
                st.setString( 5, cod_impuesto );
                
                rs = st.executeQuery();
                
                if ( rs.next() ){
                    
                    valor = rs.getString( "vlr_total_impuesto" )!=null?rs.getString( "vlr_total_impuesto" ):"0";
                    
                }
                
            }
            
        } catch( Exception e ){
            
            e.printStackTrace();
            throw new Exception( "ERROR EN 'valor_CXP_IMP_DOC' - [MigracionFacturas].. " + e.getMessage() );
            
        }
        
        finally{
            
            if ( st != null ){
                try{
                    st.close();
                } catch( Exception e ){
                    throw new Exception( "ERROR CERRANDO EL ESTAMENTO " + e.getMessage() );
                }
            }
            if ( con != null ){
                poolManager.freeConnection( "sot", con );
            }
            
        }
        
        return valor;
        
    }
    
    /**
     * Se encarga de obtener el valor en la moneda extranjera de la tabla CXP_IMP_DOC.
     * @autor LREALES
     * @param el distrito, el proveedor, el tipo_documento, el documento y el codigo del impuesto.
     * @throws Exception En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public String valor_me_CXP_IMP_DOC( String distrito, String proveedor, String tipo_documento, String documento, String cod_impuesto ) throws Exception {
        
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        String valor_me = "0";
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( "sot" );
            
            if( con != null ){
                
                st = con.prepareStatement( this.SQL_EXISTE_CXP_IMP_DOC );
                
                st.setString( 1, distrito );
                st.setString( 2, proveedor );
                st.setString( 3, tipo_documento );
                st.setString( 4, documento );
                st.setString( 5, cod_impuesto );
                
                rs = st.executeQuery();
                
                if ( rs.next() ){
                    
                    valor_me = rs.getString( "vlr_total_impuesto_me" )!=null?rs.getString( "vlr_total_impuesto_me" ):"0";
                    
                }
                
            }
            
        } catch( Exception e ){
            
            e.printStackTrace();
            throw new Exception( "ERROR EN 'valor_me_CXP_IMP_DOC' - [MigracionFacturas].. " + e.getMessage() );
            
        }
        
        finally{
            
            if ( st != null ){
                try{
                    st.close();
                } catch( Exception e ){
                    throw new Exception( "ERROR CERRANDO EL ESTAMENTO " + e.getMessage() );
                }
            }
            if ( con != null ){
                poolManager.freeConnection( "sot", con );
            }
            
        }
        
        return valor_me;
        
    }
    
    /**
     * Obtiene una lista de las facturas que van a ser borradas.
     * @autor LREALES
     * @param -
     * @throws Exception En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void facturasABorrar() throws Exception{
        
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( "sot" );
            
            if( con != null ){
                
                st = con.prepareStatement( this.SQL_FACTURAS_A_BORRAR );
                
                rs = st.executeQuery();
                
                this.otro_vector = new Vector();
                
                while ( rs.next() ){
                    
                    String otro_distrito = rs.getString( "dstrct" )!=null?rs.getString( "dstrct" ).toUpperCase():"";
                    String otro_proveedor = rs.getString( "proveedor" )!=null?rs.getString( "proveedor" ).toUpperCase():"";
                    String otro_tipo_documento = rs.getString( "tipo_documento" )!=null?rs.getString( "tipo_documento" ).toUpperCase():"";
                    String otro_documento = rs.getString( "documento" )!=null?rs.getString( "documento" ).toUpperCase():"";
                    String otro_descripcion = rs.getString( "descripcion" )!=null?rs.getString( "descripcion" ).toUpperCase():"";
                    
                    factura = new FacturasMims();
                    
                    factura.setOtro_distrito( otro_distrito );
                    factura.setOtro_proveedor( otro_proveedor );
                    factura.setOtro_tipo_documento( otro_tipo_documento );
                    factura.setOtro_documento( otro_documento );
                    factura.setOtro_descripcion( otro_descripcion );
                    
                    this.otro_vector.add( factura );
                    
                }
                
            }
            
        } catch( Exception e ){
            
            e.printStackTrace();
            throw new Exception( "ERROR EN 'facturasABorrar' - [MigracionFacturas] " + e.getMessage() );
            
        }
        
        finally{
            
            if ( st != null ){
                try{
                    st.close();
                } catch( Exception e ){
                    throw new Exception( "ERROR CERRANDO EL ESTAMENTO " + e.getMessage() );
                }
            }
            if ( con != null ){
                poolManager.freeConnection( "sot", con );
            }
            
        }
        
    }
    
    /**
     * Se encarga de actualizar los campos de la tabla CPX_IMP_ITEM.
     * @autor LREALES
     * @param 1 Objeto.
     * @throws Exception En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void actualizarCPX_IMP_ITEM( FacturasMims fm ) throws Exception{
        
        Connection con = null;
        PreparedStatement st = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( "sot" );
            
            if( con != null ){
                
                st = con.prepareStatement( this.SQL_UPDATE_CXP_IMP_ITEM );
                
                double vlr_total_impuesto1 = Double.parseDouble( fm.getVlr_imp() );
                double vlr_total_impuesto_me1 = Double.parseDouble(fm.getVlrme_imp() );
                double porcent_impuesto1 = Double.parseDouble( fm.getPorc_imp() );
                
                st.setDouble( 1, porcent_impuesto1 );
                st.setDouble( 2, vlr_total_impuesto1 );
                st.setDouble( 3, vlr_total_impuesto_me1 );
                st.setString( 4, fm.getLast_update() );
                st.setString( 5, fm.getUser_update() );
                st.setString( 6, fm.getDistrito() );
                st.setString( 7, fm.getProveedor() );
                st.setString( 8, fm.getTipo_documento() );
                st.setString( 9, fm.getDocumento() );
                st.setString( 10, fm.getItem() );
                st.setString( 11, fm.getCod_imp() );
                
                st.executeUpdate();
                
            }
            
        } catch( Exception e ){
            
            e.printStackTrace();
            throw new Exception( "ERROR EN 'actualizarCPX_IMP_ITEM' - [MigracionFacturas].. " + e.getMessage() );
            
        }
        
        finally{
            
            if ( st != null ){
                try{
                    st.close();
                } catch( Exception e ){
                    throw new Exception( "ERROR CERRANDO EL ESTAMENTO " + e.getMessage() );
                }
            }
            if ( con != null ){
                poolManager.freeConnection( "sot", con );
            }
            
        }
        
    }
    
    /**
     * Se encarga de actualizar los campos de la tabla CPX_IMP_DOC.
     * @autor LREALES
     * @param 1 Objeto, el valor anterior del impuesto y el valor en moneda extranjera anterior del impuesto.
     * @throws Exception En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void actualizarCPX_IMP_DOC( FacturasMims fm, String valor, String valor_me ) throws Exception{
        
        Connection con = null;
        PreparedStatement st = null;
        PoolManager poolManager = null;
        
        double v = Double.parseDouble( valor );
        double v_me = Double.parseDouble( valor_me );
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( "sot" );
            
            if( con != null ){
                
                st = con.prepareStatement( this.SQL_UPDATE_CXP_IMP_DOC );
                
                double vlr_total_impuesto1 = Double.parseDouble( fm.getVlr_imp() ) + v;
                double vlr_total_impuesto_me1 = Double.parseDouble(fm.getVlrme_imp() ) + v_me;
                double porcent_impuesto1 = Double.parseDouble( fm.getPorc_imp() );
                
                st.setDouble( 1, porcent_impuesto1 );
                st.setDouble( 2, vlr_total_impuesto1 );
                st.setDouble( 3, vlr_total_impuesto_me1 );
                st.setString( 4, fm.getLast_update() );
                st.setString( 5, fm.getUser_update() );
                st.setString( 6, fm.getDistrito() );
                st.setString( 7, fm.getProveedor() );
                st.setString( 8, fm.getTipo_documento() );
                st.setString( 9, fm.getDocumento() );
                st.setString( 10, fm.getCod_imp() );
                
                st.executeUpdate();
                
            }
            
        } catch( Exception e ){
            
            e.printStackTrace();
            throw new Exception( "ERROR EN 'actualizarCPX_IMP_DOC' - [MigracionFacturas].. " + e.getMessage() );
            
        }
        
        finally{
            
            if ( st != null ){
                try{
                    st.close();
                } catch( Exception e ){
                    throw new Exception( "ERROR CERRANDO EL ESTAMENTO " + e.getMessage() );
                }
            }
            if ( con != null ){
                poolManager.freeConnection( "sot", con );
            }
            
        }
        
    }
    
    /**
     * Se encarga de actualizar los campos de la tabla CPX_ITEMS_DOC.
     * @autor LREALES
     * @param 1 Objeto.
     * @throws Exception En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void actualizarCPX_ITEMS_DOC( FacturasMims fm ) throws Exception{
        
        Connection con = null;
        PreparedStatement st = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( "sot" );
            
            if( con != null ){
                
                st = con.prepareStatement( this.SQL_UPDATE_CXP_ITEMS_DOC );
                
                double vlr1 = Double.parseDouble( fm.getVlritem() );
                double vlr_me1 = Double.parseDouble( fm.getVlrmeitem() );
                
                st.setString( 1, fm.getDescripcion() );
                st.setDouble( 2, vlr1 );
                st.setDouble( 3, vlr_me1 );
                st.setString( 4, fm.getCuenta() );
                st.setString( 5, fm.getPlanilla() );
                st.setString( 6, fm.getLast_update() );
                st.setString( 7, fm.getUser_update() );
                st.setString( 8, fm.getDistrito() );
                st.setString( 9, fm.getProveedor() );
                st.setString( 10, fm.getTipo_documento() );
                st.setString( 11, fm.getDocumento() );
                st.setString( 12, fm.getItem() );
                
                st.executeUpdate();
                
            }
            
        } catch( Exception e ){
            
            e.printStackTrace();
            throw new Exception( "ERROR EN 'actualizarCPX_ITEMS_DOC' - [MigracionFacturas].. " + e.getMessage() );
            
        }
        
        finally{
            
            if ( st != null ){
                try{
                    st.close();
                } catch( Exception e ){
                    throw new Exception( "ERROR CERRANDO EL ESTAMENTO " + e.getMessage() );
                }
            }
            if ( con != null ){
                poolManager.freeConnection( "sot", con );
            }
            
        }
        
    }
    
    /**
     * Se encarga de actualizar los campos de la tabla CPX_DOC.
     * @autor LREALES
     * @param 1 Objeto.
     * @throws Exception En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void actualizarCPX_DOC( FacturasMims fm ) throws Exception{
        
        Connection con = null;
        PreparedStatement st = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( "sot" );
            
            if( con != null ){
                
                st = con.prepareStatement( this.SQL_UPDATE_CXP_DOC );
                double vlr_neto1 = Double.parseDouble( fm.getVlr_neto() );
                double vlr_neto_me1 = Double.parseDouble( fm.getVlr_neto_me() );
                
                double vlr_total_abonos2 = Double.parseDouble( fm.getVlr_total_abonos() );
                double vlr_total_abonos_me2 = Double.parseDouble( fm.getVlr_total_abonos_me() );
                
                double tasa1 = Double.parseDouble( fm.getTasa() );
                
                if ( vlr_neto1 == 0 ) {
                    tasa1 = 0;
                } else {
                    tasa1 = ( vlr_neto_me1 / vlr_neto1 );
                }
                
                double vlr_total_impuesto1 = Double.parseDouble( fm.getVlr_imp() );
                double vlr_total_impuesto_me1 = Double.parseDouble( fm.getVlrme_imp() );
                
                double vlr_saldo1 = vlr_neto1 - vlr_total_abonos2;
                
                double vlr_saldo_me1 = vlr_neto_me1 - vlr_total_abonos_me2;
                
                st.setString( 1, fm.getDescripcionfact() );
                st.setString( 2, fm.getAgencia() );
                st.setString( 3, fm.getHandle_code() );
                st.setString( 4, fm.getId_mims() );
                st.setString( 5, fm.getFecha_aprobacion() );
                st.setString( 6, fm.getAprobador() );
                st.setString( 7, fm.getUsuario_aprobacion() );
                st.setString( 8, fm.getBanco() );
                st.setString( 9, fm.getSucursal() );
                st.setString( 10, fm.getMoneda() );
                st.setDouble( 11, vlr_neto1 );
                st.setDouble( 12, vlr_saldo1 );
                st.setDouble( 13, vlr_neto_me1 );
                st.setDouble( 14, vlr_saldo_me1 );
                st.setDouble( 15, tasa1 );
                st.setString( 16, fm.getLast_update() );
                st.setString( 17, fm.getUser_update() );
                st.setString( 18, fm.getCorrida() );
                st.setString( 19, fm.getCheque() );
                st.setString( 20, fm.getPeriodo() );
                st.setString( 21, fm.getClase_documento() );
                st.setString( 22, fm.getMoneda_banco() );
                st.setString( 23, fm.getFecdocumento() );
                st.setString( 24, fm.getFecha_vencimiento() );
                st.setString( 25, fm.getUltima_fecha_pago() );
                
                st.setString( 26, "" + vlr_total_abonos2 );
                st.setString( 27, "" + vlr_total_abonos_me2 );
                
                st.setString( 28, fm.getDistrito() );
                st.setString( 29, fm.getProveedor() );
                st.setString( 30, fm.getTipo_documento() );
                st.setString( 31, fm.getDocumento() );
                
                st.executeUpdate();
                
                existe_nit = this.existeNit( fm.getProveedor() );
                if ( !existe_nit ) {
                    
                    this.tree_map_imp_nit.put( fm.getProveedor(), fm.getProveedor() );
                    
                }
                
                existe_pro = this.existeProveedor( fm.getProveedor() );
                if ( !existe_pro ) {
                    
                    this.tree_map_imp_pro.put( fm.getProveedor(), fm.getProveedor() );
                    
                }
                
            }
            
        } catch( Exception e ){
            
            e.printStackTrace();
            throw new Exception( "ERROR EN 'actualizarCPX_DOC' - [MigracionFacturas].. " + e.getMessage() );
            
        }
        
        finally{
            
            if ( st != null ){
                try{
                    st.close();
                } catch( Exception e ){
                    throw new Exception( "ERROR CERRANDO EL ESTAMENTO " + e.getMessage() );
                }
            }
            if ( con != null ){
                poolManager.freeConnection( "sot", con );
            }
            
        }
        
    }
    
    /**
     * Se encarga de buscar el abono de una factura especifica.
     * @autor LREALES
     * @param 1 objeto.
     * @throws Exception En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public String buscarAbono( FacturasMims fm ) throws Exception{
        
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        String abono = "";
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( "sot" );
            
            if( con != null ){
                
                st = con.prepareStatement( this.SQL_ABONOS );
                
                st.setString( 1, fm.getDistrito() );
                st.setString( 2, fm.getProveedor() );
                st.setString( 3, fm.getTipo_documento() );
                st.setString( 4, fm.getDocumento() );
                
                rs = st.executeQuery();
                
                if ( rs.next() ){
                    
                    abono = rs.getString( "vlr_total_abonos" ).toUpperCase();
                    
                }
                
            }
            
        } catch( Exception e ){
            
            e.printStackTrace();
            throw new Exception( "ERROR EN 'buscarAbono' - [MigracionFacturas].. " + e.getMessage() );
            
        }
        
        finally{
            
            if ( st != null ){
                try{
                    st.close();
                } catch( Exception e ){
                    throw new Exception( "ERROR CERRANDO EL ESTAMENTO " + e.getMessage() );
                }
            }
            if ( con != null ){
                poolManager.freeConnection( "sot", con );
            }
            
        }
        
        return abono;
        
    }
    
    /**
     * Se encarga de buscar el abono en moneda extranjera de una factura especifica.
     * @autor LREALES
     * @param 1 Objeto.
     * @throws Exception En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public String buscarAbonoME( FacturasMims fm ) throws Exception{
        
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        String abono_me = "";
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( "sot" );
            
            if( con != null ){
                
                st = con.prepareStatement( this.SQL_ABONOS );
                
                st.setString( 1, fm.getDistrito() );
                st.setString( 2, fm.getProveedor() );
                st.setString( 3, fm.getTipo_documento() );
                st.setString( 4, fm.getDocumento() );
                
                rs = st.executeQuery();
                
                if ( rs.next() ){
                    
                    abono_me = rs.getString( "vlr_total_abonos_me" ).toUpperCase();
                    
                }
                
            }
            
        } catch( Exception e ){
            
            e.printStackTrace();
            throw new Exception( "ERROR EN 'buscarAbonoME' - [MigracionFacturas].. " + e.getMessage() );
            
        }
        
        finally{
            
            if ( st != null ){
                try{
                    st.close();
                } catch( Exception e ){
                    throw new Exception( "ERROR CERRANDO EL ESTAMENTO " + e.getMessage() );
                }
            }
            if ( con != null ){
                poolManager.freeConnection( "sot", con );
            }
            
        }
        
        return abono_me;
        
    }
    
    /**
     * Se encarga de buscar el codigo de impuestos equivalente en postgres.
     * @autor LREALES
     * @param el codigo de impuestos de mims
     * @throws Exception En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public String buscarCodImp( String cod_imp_mims ) throws Exception{
        
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        String cod_imp_post = "";
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( "sot" );
            
            if( con != null ){
                
                st = con.prepareStatement( this.SQL_COD_IMP );
                
                st.setString( 1, cod_imp_mims );
                
                rs = st.executeQuery();
                
                if ( rs.next() ){
                    
                    cod_imp_post = rs.getString( "referencia" ).toUpperCase();
                    
                }
                
            }
            
        } catch( Exception e ){
            
            e.printStackTrace();
            throw new Exception( "ERROR EN 'buscarCodImp' - [MigracionFacturas].. " + e.getMessage() );
            
        }
        
        finally{
            
            if ( st != null ){
                try{
                    st.close();
                } catch( Exception e ){
                    throw new Exception( "ERROR CERRANDO EL ESTAMENTO " + e.getMessage() );
                }
            }
            if ( con != null ){
                poolManager.freeConnection( "sot", con );
            }
            
        }
        
        return cod_imp_post;
        
    }
    
    /**
     * Se encarga de calcular el resto de datos del objeto.
     * @autor LREALES
     * @param
     * @throws Exception En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void calcular( String temp_planilla, PrintWriter linea ) throws Exception{
        
        try {
            
            String ant_planilla = temp_planilla;
            
            if ( getVector().size() > 0 ){
                
                FacturasMims registro = ( FacturasMims ) getVector().elementAt( 0 );
                
                //************************************************************/
                // DATOS FACTURA
                
                String estado = registro.getEstado();
                
                String distrito = registro.getDistrito();
                String placa = registro.getPlaca();
                String documento = registro.getDocumento();/***************************/
                String documento2 = registro.getDocumento2();
                String vlr_fact = registro.getVlr_fact();
                String vlrme_fact = registro.getVlrme_fact();
                String id_mims = registro.getId_mims();
                String clase_doc = registro.getClase_doc();/***************************/
                String moneda = registro.getMoneda();
                String fec_grabacion = registro.getFec_grabacion();
                String dia_modificacion = registro.getDia_modificacion();
                dia_modificacion = Util.fechaPostgres( dia_modificacion );
                String hora_modificacion = registro.getHora_modificacion();
                String hh = hora_modificacion.substring( 0, 2 );
                String mm = hora_modificacion.substring( 2, 4 );
                String ss = hora_modificacion.substring( 4, 6 );
                hora_modificacion = hh + ":" + mm + ":" + ss;
                String creation_date = dia_modificacion + " " + hora_modificacion;
                String item = registro.getItem();/***************************/
                String descripcion = registro.getDescripcion();
                String vlr_item = registro.getVlr_item()!=null?registro.getVlr_item():"0";
                String vlrme_item = registro.getVlrme_item()!=null?registro.getVlrme_item():"0";
                String cuenta = registro.getCuenta();
                String user_update = registro.getUser_update();
                String planilla = registro.getPlanilla();/***************************/
                
                /*********************************************************************************/
                /*********************************************************************************/
                String aprobador = registro.getAprobador();
                aprobador = buscarLoginAprobador( aprobador );
                aprobador = !aprobador.trim().equals("")?aprobador:"ADMMIMS";
                aprobador = aprobador.replaceAll( " ", "" );
                /*********************************************************************************/
                /*********************************************************************************/
                
                String cod_imp = registro.getCod_imp();
                String porc_imp = registro.getPorc_imp();
                String vlr_imp = registro.getVlr_imp();
                String vlrme_imp = registro.getVlrme_imp();
                String vlr_iva = registro.getVlr_iva();
                String banco = registro.getBanco();
                String sucursal = registro.getSucursal();
                String hc = registro.getHc();
                String fecdocumento = registro.getFecdocumento();
                fecdocumento = Util.fechaPostgres( fecdocumento );
                String fecpago = registro.getFecpago();
                fecpago = Util.fechaPostgres( fecpago );
                String fecvencimiento = registro.getFecvencimiento();
                fecvencimiento = Util.fechaPostgres( fecvencimiento );//
                String fecaprobacion = registro.getFecaprobacion();
                fecaprobacion = fecaprobacion.length() == 8?Util.fechaPostgres( fecaprobacion ) + " 00:00:00":fecaprobacion;
                fecaprobacion = fecaprobacion.trim().equals("")?"0099-01-01 00:00:00":fecaprobacion;
                String corrida = registro.getCorrida().equals("000000")?"":registro.getCorrida();
                String cheque = registro.getCheque().equals("000000")?"":registro.getCheque();
                String creation_user = registro.getCreation_user();
                String proveedor = registro.getProveedor();
                // DATOS CALCULADOS
                String periodo = registro.getFec_grabacion();
                periodo = periodo.substring( 0, 4 ) + periodo.substring( 5, 7 );
                String descripcionfact = "FACTURA MIMS " + documento2 + " " + id_mims + " " + placa;
                String agencia = buscarAgencia( registro.getDistrito(), registro.getBanco(), registro.getSucursal() );
                agencia = !agencia.equals("")?agencia:"";
                String moneda_banco = buscarMonedaBanco( registro.getDistrito(), registro.getBanco(), registro.getSucursal() );
                moneda_banco = !moneda_banco.equals("")?moneda_banco:"";
                String tipo_documento = "010";
                
                String auxiliar = registro.getAuxiliar();
                
                if ( !proveedor.trim().equals("") ) {
                    
                    if ( !moneda_banco.equals("") && !agencia.equals("") ) {
                        
                        /***************************************************************************************************************/
                        String codigo_abc = registro.getCodigo_abc().trim().equals("")?"":registro.getCodigo_abc();
                        String retencion = registro.getRetencion();
                        String retencion_me = registro.getRetencion_me();
                        
                        double vlr_neto_real = 0;
                        double vlr_neto_me_real = 0;
                        double vlr_total_item = Double.parseDouble( vlr_item );
                        double vlr_total_retencion = Double.parseDouble( retencion );
                        double vlr_total_imp = Double.parseDouble( vlr_imp );
                        double vlr_total_item_me = Double.parseDouble( vlrme_item );
                        double vlr_total_retencion_me = Double.parseDouble( retencion_me );
                        double vlr_total_imp_me = Double.parseDouble( vlrme_imp );
                        
                        String fact = proveedor + "|" + documento;
                        if ( !fact.equals( fact_anterior ) ){
                            fact_anterior = proveedor + "|" + documento;
                            fact_items = vlr_total_item;
                            fact_retefuente = vlr_total_retencion;
                            fact_impuestos = vlr_total_imp;
                            fact_items_me = vlr_total_item_me;
                            fact_retefuente_me = vlr_total_retencion_me;
                            fact_impuestos_me = vlr_total_imp_me;
                            
                            /*** 7 NOVIEMBRE 2006 ***/
                            /* BORRAR LA FACTURA Q VAYA A BAJAR PARA EVITAR ERRORES EN LA ACTUALIZACION */
                            limpiarCXP_DOC( distrito, proveedor, tipo_documento, documento );
                            limpiarCXP_ITEMS_DOC( distrito, proveedor, tipo_documento, documento );
                            limpiarCXP_IMP_ITEM( distrito, proveedor, tipo_documento, documento );
                            /*** ***/
                            
                            /***** INICIO MODIFICACION CXP_IMP_DOC *****/
                            valor_retencion_IMP_DOC = vlr_total_retencion;
                            valor_retencion_me_IMP_DOC = vlr_total_retencion_me;
                            limpiarCXP_IMP_DOC( distrito, proveedor, tipo_documento, documento );
                            /***** FIN MODIFICACION CXP_IMP_DOC *****/
                        } else {
                            fact_impuestos = fact_impuestos + vlr_total_imp;
                            fact_impuestos_me = fact_impuestos_me + vlr_total_imp_me;
                            
                            if ( !item.equals( fact_item ) ){
                                fact_items = fact_items + vlr_total_item;
                                fact_items_me = fact_items_me + vlr_total_item_me;
                                /***************************************************************************/
                                /***************************************************************************/
                                /***************************************************************************/
                                fact_retefuente = fact_retefuente + vlr_total_retencion;
                                fact_retefuente_me = fact_retefuente_me + vlr_total_retencion_me;
                                /***************************************************************************/
                                /***************************************************************************/
                                /***************************************************************************/
                                
                                /***** INICIO MODIFICACION CXP_IMP_DOC *****/
                                valor_retencion_IMP_DOC = vlr_total_retencion + valor_retencion_IMP_DOC;
                                valor_retencion_me_IMP_DOC = vlr_total_retencion_me + valor_retencion_me_IMP_DOC;
                                /***** FIN MODIFICACION CXP_IMP_DOC *****/
                            }
                        }
                        
                        fact_anterior = fact;
                        fact_item = item;
                        /***************************************************************************************************************/
                        
                        double t = fact_impuestos + fact_retefuente + fact_items;
                        String vlr_total_neto = "" + t;
                        
                        double tme = fact_impuestos_me + fact_retefuente_me + fact_items_me;
                        String vlrme_total_neto = "" + tme;
                        
                        String vlr_total_saldo = "0";
                        String vlrme_total_saldo = "0";
                        String vlr_total_abonos = "0";
                        String vlrme_total_abonos = "0";
                        
                        if ( estado.equals("40") || estado.equals("50") ) {
                            
                            vlr_total_abonos = vlr_total_neto;
                            vlrme_total_abonos = vlrme_total_neto;
                            
                        }
                        
                        // Busca la planilla de las facturas que no traigan planilla.
                        if ( planilla.trim().equals("") ){
                            planilla = "";
                            if ( clase_doc.equals("0") || clase_doc.equals("1") ){
                                planilla = ant_planilla;
                                
                            }
                            
                        }
                        
                        // Busca la cuenta de las facturas que no traigan cuenta.
                        if ( cuenta.trim().equals("") ){
                            
                            if ( clase_doc.equals("0") || clase_doc.equals("1") ){
                                
                                cuenta = OpDataAccess.getCuenta( distrito, proveedor, "010", planilla );
                                
                            }
                            
                        }
                        
                        /******************************************************************************************************/
                        if ( item.equals("001") ){
                            if ( clase_doc.equals("0") || clase_doc.equals("1") ){
                                if( existePlanilla( planilla ) ){
                                    actualizarPlanilla( documento, planilla );
                                }
                            }
                        }
                        /******************************************************************************************************/
                        
                        ant_planilla = planilla;
                        
                        //***** AGREGA AL HASTABLE PARTE 1 *****/
                        
                        factura.setEstado( estado );
                        
                        factura.setReg_status( "" );
                        factura.setDistrito( distrito );
                        factura.setProveedor( proveedor );
                        factura.setTipo_documento( "010" );
                        factura.setDocumento( documento );
                        factura.setDescripcionfact( descripcionfact );
                        agencia = agencia.length() > 15?agencia.substring( 0,15 ):agencia;
                        factura.setAgencia( agencia );//
                        hc = hc.length() > 15?hc.substring( 0,15 ):hc;
                        factura.setHc( hc );//
                        factura.setHandle_code( hc );
                        factura.setId_mims( id_mims );
                        factura.setTipo_documento_rel( "" );
                        
                        factura.setDocumento_relacionado( "" );
                        factura.setFecaprobacion( fecaprobacion );
                        factura.setFecha_aprobacion( fecaprobacion );
                        aprobador = aprobador.length() > 15?aprobador.substring( 0,15 ):aprobador;
                        factura.setAprobador( aprobador );
                        factura.setUsuario_aprobacion( aprobador );
                        factura.setBanco( banco );
                        factura.setSucursal( sucursal );
                        moneda = moneda.length() > 15?moneda.substring( 0,15 ):moneda;
                        factura.setMoneda( moneda );//
                        factura.setVlr_neto( vlr_total_neto );
                        factura.setVlr_total_abonos( vlr_total_abonos );///////////////////
                        factura.setVlr_saldo( vlr_total_saldo );
                        
                        factura.setVlr_neto_me( vlrme_total_neto );
                        factura.setVlr_total_abonos_me( vlrme_total_abonos );/////////////////////
                        factura.setVlr_saldo_me( vlrme_total_saldo );
                        factura.setTasa( "0" );
                        factura.setUsuario_contabilizo( "ADMIN" );
                        factura.setFecha_contabilizacion( "now()" );
                        factura.setUsuario_anulo( "" );
                        factura.setFecha_anulacion( "0099-01-01 00:00:00" );
                        factura.setFecha_contabilizacion_anulacion( "0099-01-01 00:00:00" );
                        factura.setObservacion( "" );
                        
                        factura.setNum_obs_autorizador( "0" );
                        factura.setNum_obs_pagador( "0" );
                        factura.setNum_obs_registra( "0" );
                        factura.setLast_update( "now()" );
                        factura.setUser_update( user_update );
                        factura.setCreation_date( creation_date );
                        creation_user = creation_user.length() > 15?creation_user.substring( 0,15 ):creation_user;
                        factura.setCreation_user( creation_user );//
                        factura.setBase( "COL" );
                        factura.setCorrida( corrida );
                        factura.setCheque( cheque );
                        
                        periodo = periodo.length() > 6?periodo.substring( 0,6 ):periodo;
                        factura.setPeriodo( periodo );//6
                        factura.setFecha_contabilizacion_ajc( "0099-01-01 00:00:00" );
                        factura.setFecha_contabilizacion_ajv( "0099-01-01 00:00:00" );
                        factura.setPeriodo_ajc( "" );
                        factura.setPeriodo_ajv( "" );
                        factura.setUsuario_contabilizo_ajc( "" );
                        factura.setUsuario_contabilizo_ajv( "" );
                        factura.setTransaccion_ajc( "0" );
                        factura.setTransaccion_ajv( "0" );
                        factura.setFecha_procesado( "0099-01-01 00:00:00" );
                        
                        factura.setClase_documento( clase_doc );
                        factura.setTransaccion( "0" );
                        moneda_banco = moneda_banco.length() > 3?moneda_banco.substring( 0,3 ):moneda_banco;
                        factura.setMoneda_banco( moneda_banco );//3
                        factura.setFecha_documento( fecdocumento );
                        factura.setFecha_vencimiento( fecvencimiento );
                        factura.setUltima_fecha_pago( fecpago );
                        
                        //***** AGREGA AL HASTABLE PARTE 2 *****/
                        factura.setItem( item );
                        factura.setDescripcion( descripcion );
                        factura.setVlritem( vlr_item );
                        factura.setVlrmeitem( vlrme_item );
                        factura.setVlr_item( vlr_item );
                        factura.setVlrme_item( vlrme_item );
                        
                        factura.setCuenta( cuenta );
                        factura.setCodigo_abc( codigo_abc );
                        factura.setPlanilla( planilla );
                        factura.setCodcliarea( "" );
                        factura.setTipcliarea( "" );
                        factura.setConcepto( "" );
                        factura.setAuxiliar( auxiliar );
                        
                        //***** AGREGA AL HASTABLE PARTE 3 *****/
                        factura.setCod_imp( cod_imp );// definir codigos de impuestos
                        factura.setPorc_imp( porc_imp );
                        factura.setVlr_imp( vlr_imp );
                        factura.setVlrme_imp( vlrme_imp );
                        
                        //***** AGREGA AL HASTABLE VARIAS PARTES *****/
                        factura.setVlr_iva( vlr_iva );
                        
                        factura.setRetencion( retencion );
                        
                        this.vector.setElementAt( factura, 0 );
                        
                        if ( planilla.length() < 1 ) {
                            planilla = "000000";
                        }
                        
                        //if ( documento.length() < 2 ) {
                            //temporal = documento;
                            //documento = "000000";
                        //}
                        
                        /*** 7 NOVIEMBRE 2006 ***/
                        if ( documento.length() >= 2 && documento.substring( 0, 2 ).equals("OP") && ( !planilla.substring(0,1).equals("0") && !planilla.substring(0,1).equals("1") && !planilla.substring(0,1).equals("2") && !planilla.substring(0,1).equals("3") && !planilla.substring(0,1).equals("4") && !planilla.substring(0,1).equals("5") && !planilla.substring(0,1).equals("6") && !planilla.substring(0,1).equals("7") && !planilla.substring(0,1).equals("8") && !planilla.substring(0,1).equals("9") ) ) {
                            // agregar al log de errores y no insertar
                            //linea.println("-   PROVEEDOR : "+proveedor+" - DOCUMENTO : "+documento+" - ITEM : "+item+" - PLACA : "+placa+" - ID_MIMS : "+id_mims+" - BANCO : "+banco+" - SUCURSAL : "+sucursal);
                            
                        } else {
                            
                            //documento = temporal;
                            
                            if ( !vlr_total_neto.equals("0.0") ) {
                                
                                if ( !existeCXP_DOC( distrito, proveedor, tipo_documento, documento ) ) {
                                    transferirCXP_DOC( factura );
                                } else {
                                    actualizarCPX_DOC( factura );
                                }
                                
                                if ( !existeCXP_ITEMS_DOC( distrito, proveedor, tipo_documento, documento, item ) ) {
                                    transferirCXP_ITEMS_DOC( factura );
                                } else {
                                    actualizarCPX_ITEMS_DOC( factura );
                                }
                                
                                if ( !existeCXP_IMP_ITEM( distrito, proveedor, tipo_documento, documento, item, cod_imp ) ) {
                                    transferirCXP_IMP_ITEM( factura );
                                } else {
                                    actualizarCPX_IMP_ITEM( factura );
                                }
                                
                                if ( !retencion.equals("0.0") ){
                                    if ( !existeCXP_IMP_ITEM( distrito, proveedor, tipo_documento, documento, item, "RT01" ) ) {
                                        insertRetefuente( factura );
                                    } else {
                                        actualizarRetefuente( factura );
                                    }
                                }
                                
                                /***** INICIO MODIFICACION CXP_IMP_DOC *****/
                                if ( !cod_imp.equals("") ){
                                    if ( !existeCXP_IMP_DOC( distrito, proveedor, tipo_documento, documento, cod_imp ) ) {
                                        transferirCXP_IMP_DOC( factura );
                                    } else {
                                        String valor = valor_CXP_IMP_DOC( distrito, proveedor, tipo_documento, documento, cod_imp );
                                        String valor_me = valor_me_CXP_IMP_DOC( distrito, proveedor, tipo_documento, documento, cod_imp );
                                        actualizarCPX_IMP_DOC( factura, valor, valor_me );
                                    }
                                }
                                
                                if ( !retencion.equals("0.0") ){
                                    if ( !existeCXP_IMP_DOC( distrito, proveedor, tipo_documento, documento, "RT01" ) ) {
                                        insertRetefuente_CXP_IMP_DOC( factura );
                                    } else {
                                        actualizarRetefuente_CXP_IMP_DOC( factura, valor_retencion_IMP_DOC, valor_retencion_me_IMP_DOC );
                                    }
                                }
                                /***** FIN MODIFICACION CXP_IMP_DOC *****/
                                
                            } else {
                                
                                linea.println("*** PROVEEDOR : "+proveedor+" - DOCUMENTO : "+documento+" - ITEM : "+item+" - PLACA : "+placa+" - ID_MIMS : "+id_mims+" - BANCO : "+banco+" - SUCURSAL : "+sucursal);
                            
                            }
                            
                        }
                        
                    } else {
                        // agregar al log de errores y no insertar
                        linea.println("*  PROVEEDOR : "+proveedor+" - DOCUMENTO : "+documento+" - ITEM : "+item+" - PLACA : "+placa+" - ID_MIMS : "+id_mims+" - BANCO : "+banco+" - SUCURSAL : "+sucursal);
                        
                    }
                    
                } else {
                    // agregar al log de errores y no insertar
                    linea.println("**   PROVEEDOR : "+proveedor+" - DOCUMENTO : "+documento+" - ITEM : "+item+" - PLACA : "+placa+" - ID_MIMS : "+id_mims+" - BANCO : "+banco+" - SUCURSAL : "+sucursal);
                    
                }
                
            }
            
        } catch( Exception e ){
            
            e.printStackTrace();
            throw new Exception( "ERROR EN 'calcular' - [MigracionFacturas].. " + e.getMessage() );
            
        }
        
    }
    
    /**
     * M�todo que envia un mail a DIT informando acerca del proceso de migracion.
     * @autor :         LREALES
     * @param:          -
     * @throws:         Exception
     * @version :       1.0
     */
    public void sendMail( String body ) throws Exception{
        
        Connection con = null;
        PreparedStatement st = null;
        PoolManager poolManager = null;
        
        try{
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( "sot" );
            
            if( con != null ){
                
                st = con.prepareStatement( this.SQL_INSERT_SENDMAIL );
                
                st.setString( 1, body );
                
                st.execute();
                
            }
            
            
        } catch( Exception e ){
            
            e.printStackTrace();
            throw new Exception( "ERROR DURANTE 'sendMail' - [MigracionFacturas].. " + e.getMessage() );
            
        }
        
        finally{
            
            if ( st != null ){
                try{
                    st.close();
                } catch( Exception e ){
                    throw new Exception( "ERROR CERRANDO EL ESTAMENTO " + e.getMessage() );
                }
            }
            if ( con != null ){
                poolManager.freeConnection( "sot", con );
            }
            
        }
        
    }
    
    /**
     * Se encarga de verificar si existe o no una planilla.
     * @autor LREALES
     * @param el numero de la planilla.
     * @throws Exception En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public boolean existePlanilla( String planilla ) throws Exception{
        
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        boolean sw = false;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( "sot" );
            
            if( con != null ){
                
                st = con.prepareStatement( this.SQL_EXISTE_PLANILLA);
                
                st.setString( 1, planilla );
                
                rs = st.executeQuery();
                
                if ( rs.next() ){
                    
                    sw = true;
                    
                }
                
            }
            
        } catch( Exception e ){
            
            e.printStackTrace();
            throw new Exception( "ERROR EN 'existePlanilla' - [MigracionFacturas].. " + e.getMessage()  );
            
        }
        
        finally{
            
            if ( st != null ){
                try{
                    st.close();
                } catch( Exception e ){
                    throw new Exception( "ERROR CERRANDO EL ESTAMENTO " + e.getMessage() );
                }
            }
            if ( con != null ){
                poolManager.freeConnection( "sot", con );
            }
            
        }
        
        return sw;
        
    }
    
    /**
     * Se encarga de actualizar el campo de factura de la planilla.
     * @autor LREALES
     * @param el documento y la planilla.
     * @throws Exception En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void actualizarPlanilla( String documento, String planilla ) throws Exception{
        
        Connection con = null;
        PreparedStatement st = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( "sot" );
            
            if( con != null ){
                
                st = con.prepareStatement( this.SQL_UPDATE_PLANILLA );
                
                st.setString( 1, documento );
                st.setString( 2, planilla );
                
                st.executeUpdate();
                
            }
            
        } catch( Exception e ){
            
            e.printStackTrace();
            throw new Exception( "ERROR EN 'actualizarPlanilla' - [MigracionFacturas].. " + e.getMessage() );
            
        }
        
        finally{
            
            if ( st != null ){
                try{
                    st.close();
                } catch( Exception e ){
                    throw new Exception( "ERROR CERRANDO EL ESTAMENTO " + e.getMessage() );
                }
            }
            if ( con != null ){
                poolManager.freeConnection( "sot", con );
            }
            
        }
        
    }
    
    /**
     * Se encarga de verificar si existe o no un proveedor.
     * @autor LREALES
     * @param el numero de la cedula.
     * @throws Exception En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public boolean existeNit( String cedula ) throws Exception{
        
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        boolean sw = false;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( "sot" );
            
            if( con != null ){
                
                st = con.prepareStatement( this.SQL_EXISTE_NIT );
                
                st.setString( 1, cedula );
                
                rs = st.executeQuery();
                
                if ( rs.next() ){
                    
                    sw = true;
                    
                }
                
            }
            
        } catch( Exception e ){
            
            e.printStackTrace();
            throw new Exception( "ERROR EN 'existeNit' - [MigracionFacturas].. " + e.getMessage()  );
            
        }
        
        finally{
            
            if ( st != null ){
                try{
                    st.close();
                } catch( Exception e ){
                    throw new Exception( "ERROR CERRANDO EL ESTAMENTO " + e.getMessage() );
                }
            }
            if ( con != null ){
                poolManager.freeConnection( "sot", con );
            }
            
        }
        
        return sw;
        
    }
    
    /**
     * Se encarga de verificar si existe o no un proveedor.
     * @autor LREALES
     * @param el numero de la cedula.
     * @throws Exception En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public boolean existeProveedor( String cedula ) throws Exception{
        
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        boolean sw = false;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( "sot" );
            
            if( con != null ){
                
                st = con.prepareStatement( this.SQL_EXISTE_PROVEEDOR );
                
                st.setString( 1, cedula );
                
                rs = st.executeQuery();
                
                if ( rs.next() ){
                    
                    sw = true;
                    
                }
                
            }
            
        } catch( Exception e ){
            
            e.printStackTrace();
            throw new Exception( "ERROR EN 'existeProveedor' - [MigracionFacturas].. " + e.getMessage()  );
            
        }
        
        finally{
            
            if ( st != null ){
                try{
                    st.close();
                } catch( Exception e ){
                    throw new Exception( "ERROR CERRANDO EL ESTAMENTO " + e.getMessage() );
                }
            }
            if ( con != null ){
                poolManager.freeConnection( "sot", con );
            }
            
        }
        
        return sw;
        
    }
    
    public synchronized void exportar( String usuario ) {
        
        try{
            
            Util u = new Util();
            
            ResourceBundle rb = ResourceBundle.getBundle( "com/tsp/util/connectionpool/db" );
            String path = rb.getString( "ruta" );
            
            File file = new File( path + "/exportar/migracion/" + usuario );
            file.mkdirs();
            
            String fecha = Util.getFechaActual_String(6);
            fecha=fecha.replaceAll( "/", "-" );
            fecha=fecha.replaceAll( ":", "_" );
            
            String nombreArch  = "ControlNitProveedorOP[" + fecha + "].xls";
            String       Hoja  = "ControlNitProveedorOP";
            String       Ruta  = path + "/exportar/migracion/" + usuario + "/" +nombreArch;
            
            HSSFWorkbook wb    = new HSSFWorkbook();
            HSSFSheet    sheet = wb.createSheet( Hoja );
            HSSFRow      row   = null;
            HSSFRow      row2  = null;
            HSSFCell     cell  = null;
            
            for ( int col = 0; col < 2 ; col++ ){
                
                sheet.setColumnWidth( (short) col, (short) ( ( 50 * 8 ) / ( (double) 1 / 30 ) ) );
                
            }
            
            /****  ENCABEZADO Y DEFINICION DE ESTILOS ************************************************/
            
            /** ENCABEZADO GENERAL *******************************/
            HSSFFont  fuente1 = wb.createFont();
            fuente1.setFontName("verdana");
            fuente1.setFontHeightInPoints((short)(16)) ;
            fuente1.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            fuente1.setColor(HSSFColor.DARK_RED.index);
            
            HSSFCellStyle estilo1 = wb.createCellStyle();
            estilo1.setFont(fuente1);
            estilo1.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo1.setBottomBorderColor(HSSFColor.WHITE.index);
            estilo1.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo1.setLeftBorderColor  (HSSFColor.WHITE.index);
            estilo1.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo1.setRightBorderColor(HSSFColor.WHITE.index);
            estilo1.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo1.setTopBorderColor   (HSSFColor.WHITE.index);
            
            
            /** SUBTITULO *******************************/
            HSSFFont  fuenteX = wb.createFont();
            fuenteX.setFontName("verdana");
            fuenteX.setFontHeightInPoints((short)(11)) ;
            fuenteX.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            fuenteX.setColor(HSSFColor.DARK_RED.index);
            
            HSSFCellStyle estiloX = wb.createCellStyle();
            estiloX.setFont(fuenteX);
            estiloX.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estiloX.setBottomBorderColor(HSSFColor.WHITE.index);
            estiloX.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estiloX.setLeftBorderColor  (HSSFColor.WHITE.index);
            estiloX.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estiloX.setRightBorderColor(HSSFColor.WHITE.index);
            estiloX.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estiloX.setTopBorderColor   (HSSFColor.WHITE.index);
            
            /** TEXTO EN EL ENCABEAZADO *************************/
            HSSFFont  fuente2 = wb.createFont();
            fuente2.setFontName("verdana");
            fuente2.setFontHeightInPoints((short)(11)) ;
            fuente2.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            fuente2.setColor((short)(0x0));
            
            HSSFCellStyle estilo2 = wb.createCellStyle();
            estilo2.setFont(fuente2);
            estilo2.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo2.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            
            /** ENCABEZADO DE LAS COLUMNAS***********************/
            HSSFFont  fuente3 = wb.createFont();
            fuente3.setFontName("verdana");
            fuente3.setFontHeightInPoints((short)(9)) ;
            fuente3.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            fuente3.setColor(HSSFColor.BLACK.index);
            
            HSSFCellStyle estilo3 = wb.createCellStyle();
            estilo3.setFont(fuente3);
            estilo3.setFillForegroundColor(HSSFColor.GREY_25_PERCENT.index);
            estilo3.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo3.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo3.setBottomBorderColor(HSSFColor.BLACK.index);
            estilo3.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo3.setLeftBorderColor  (HSSFColor.BLACK.index);
            estilo3.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo3.setRightBorderColor(HSSFColor.BLACK.index);
            estilo3.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo3.setTopBorderColor   (HSSFColor.BLACK.index);
            estilo3.setAlignment(HSSFCellStyle.ALIGN_CENTER);
            
            /** TEXTO NORMAL ************************************/
            HSSFFont  fuente4 = wb.createFont();
            fuente4.setFontName("verdana");
            fuente4.setFontHeightInPoints((short)(9)) ;
            fuente4.setBoldweight(HSSFFont.BOLDWEIGHT_NORMAL);
            fuente4.setColor((short)(0x0));
            
            HSSFCellStyle estilo4 = wb.createCellStyle();
            estilo4.setFont(fuente4);
            estilo4.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo4.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo4.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo4.setBottomBorderColor(HSSFColor.BLACK.index);
            estilo4.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo4.setLeftBorderColor  (HSSFColor.BLACK.index);
            estilo4.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo4.setRightBorderColor(HSSFColor.BLACK.index);
            estilo4.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo4.setTopBorderColor   (HSSFColor.BLACK.index);
            
            /** NUMEROS ************************************/
            HSSFCellStyle estilo5 = wb.createCellStyle();
            estilo5.setFont(fuente4);
            estilo5.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo5.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo5.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo5.setBottomBorderColor(HSSFColor.BLACK.index);
            estilo5.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo5.setLeftBorderColor  (HSSFColor.BLACK.index);
            estilo5.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo5.setRightBorderColor(HSSFColor.BLACK.index);
            estilo5.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo5.setTopBorderColor   (HSSFColor.BLACK.index);
            estilo5.setAlignment(HSSFCellStyle.ALIGN_RIGHT);
            /****************************************************/
            
            HSSFFont fuente6 = wb.createFont();
            fuente6.setColor((short)0x0);
            fuente6.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            
            HSSFCellStyle estilo6 = wb.createCellStyle();
            estilo6.setFont(fuente6);
            estilo6.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo6.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo6.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo6.setBottomBorderColor(HSSFColor.WHITE.index);
            estilo6.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo6.setLeftBorderColor  (HSSFColor.WHITE.index);
            estilo6.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo6.setRightBorderColor(HSSFColor.WHITE.index);
            estilo6.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo6.setTopBorderColor   (HSSFColor.WHITE.index);
            
            /****************************************************/
            /* MONEDA */
            HSSFCellStyle estilo7 = wb.createCellStyle();
            estilo7.setFont(fuente4);
            estilo7.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo7.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo7.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo7.setBottomBorderColor(HSSFColor.BLACK.index);
            estilo7.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo7.setLeftBorderColor  (HSSFColor.BLACK.index);
            estilo7.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo7.setRightBorderColor(HSSFColor.BLACK.index);
            estilo7.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo7.setTopBorderColor   (HSSFColor.BLACK.index);
            estilo7.setAlignment(HSSFCellStyle.ALIGN_RIGHT);
            estilo7.setDataFormat(wb.createDataFormat().getFormat("$#,##0.00"));
            
            /* TEXTO NORMAL CENTRADO */
            HSSFCellStyle estilo9 = wb.createCellStyle();
            estilo9.setFont(fuente4);
            estilo9.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo9.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo9.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo9.setBottomBorderColor(HSSFColor.BLACK.index);
            estilo9.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo9.setLeftBorderColor  (HSSFColor.BLACK.index);
            estilo9.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo9.setRightBorderColor(HSSFColor.BLACK.index);
            estilo9.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo9.setTopBorderColor   (HSSFColor.BLACK.index);
            estilo9.setAlignment(HSSFCellStyle.ALIGN_CENTER);
            
            row  = sheet.createRow((short)(0));
            row  = sheet.createRow((short)(1));
            row  = sheet.createRow((short)(2));
            row  = sheet.createRow((short)(3));
            row  = sheet.createRow((short)(4));
            row  = sheet.createRow((short)(5));
            row  = sheet.createRow((short)(6));
            row  = sheet.createRow((short)(7));
            row  = sheet.createRow((short)(8));
            row  = sheet.createRow((short)(9));
            
            for ( int j = 0; j < 2; j++ ) {
                
                row  = sheet.getRow((short)(0));
                cell = row.createCell((short)(j));
                cell.setCellStyle(estilo1);
                row  = sheet.getRow((short)(1));
                cell = row.createCell((short)(j));
                cell.setCellStyle(estiloX);
                
            }
            
            row  = sheet.getRow((short)(0));
            cell = row.getCell((short)(0));
            cell.setCellValue("TRANSPORTES SANCHEZ POLO");
            
            row  = sheet.getRow((short)(1));
            cell = row.getCell((short)(0));
            cell.setCellValue("Reporte de Control de Nit de Proveedor OP");
            
            for( int i = 2; i < 5; i++ ){
                
                for ( int j = 0; j < 2; j++ ) {
                    
                    row  = sheet.getRow((short)(i));
                    cell = row.createCell((short)(j));
                    cell.setCellStyle(estilo6);
                    
                }
                
            }
            
            //FECHA
            row = sheet.getRow((short)(3));
            cell = row.createCell((short)(0));
            cell = row.getCell((short)(0));
            cell.setCellStyle(estilo6);
            cell.setCellValue("FECHA DEL REPORTE:");
            cell = row.createCell((short)(1));
            cell = row.getCell((short)(1));
            cell.setCellStyle(estilo6);
            cell.setCellValue(Util.getFechaActual_String(7));
            
            /*************************************************************************************/
            /***** RECORRER LOS DATOS ******/
            int Fila_Nit = 5;
            Iterator it_nit = tree_map_imp_nit.values().iterator();
            while ( it_nit.hasNext() ){
                
                String cedula_nit = ( String ) it_nit.next();
                
                Fila_Nit++;
                row  = sheet.createRow( (short)(Fila_Nit) );
                
                cell = row.createCell( (short)(0) );//
                cell.setCellStyle( estilo4 );
                cell.setCellValue( cedula_nit );
                
            }
            
            int Fila_Pro = 5;
            Iterator it_pro = tree_map_imp_pro.values().iterator();
            while ( it_pro.hasNext() ){
                
                String cedula_pro = ( String ) it_pro.next();
                
                Fila_Pro++;
                row  = sheet.createRow( (short)(Fila_Pro) );
                
                cell = row.createCell( (short)(1) );//
                cell.setCellStyle( estilo4 );
                cell.setCellValue( cedula_pro );
                
            }
            
            row = sheet.createRow((short)(5));
            
            row = sheet.getRow( (short)(5) );
            
            cell = row.createCell((short)(0));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Nit(s) que no existen en la WEB");
            
            cell = row.createCell((short)(1));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Proveedor(es) que no existen en la WEB");
            
            /*************************************************************************************/
            /**** GUARDAR DATOS EN EL ARCHIVO  ***/
            FileOutputStream fo = new FileOutputStream( Ruta );
            wb.write( fo );
            fo.close();
            
        } catch( Exception e ){
            
            e.printStackTrace();
            
        }
        
    }
    
    /*************************************************************************************************************************/
    /**
     * M�todo que permite ingresar la retefuente a la tabla cxp_imp_item del esquema fin.
     * @autor :         LREALES
     * @param:          1 Objeto.
     * @throws:         Exception
     * @version :       1.0
     */
    public void insertRetefuente( FacturasMims fm  ) throws Exception {
        
        Connection con = null;
        PreparedStatement st = null;
        PoolManager poolManager = null;
        
        try{
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( "sot" );
            
            if( con != null ){
                
                st = con.prepareStatement( this.SQL_CXP_IMP_ITEM );
                
                double vlr_retencion = Double.parseDouble( fm.getRetencion() );
                double vlr_retencion_me = Double.parseDouble( fm.getRetencion_me() );
                double vlr_item = Double.parseDouble( fm.getVlritem() );
                double porcent_retencion = vlr_retencion/vlr_item;
                
                if ( porcent_retencion < 0 )
                    porcent_retencion = porcent_retencion * ( -1 );
                
                st.setString( 1, fm.getReg_status() );
                st.setString( 2, fm.getDistrito() );
                st.setString( 3, fm.getProveedor() );
                st.setString( 4, fm.getTipo_documento() );
                st.setString( 5, fm.getDocumento() );
                st.setString( 6, fm.getItem() );
                st.setString( 7, "RT01" );// CODIGO DEL IMPUESTO ( RETEFUENTE )
                st.setDouble( 8, porcent_retencion );// porcentaje
                st.setDouble( 9, vlr_retencion ); // valor
                st.setDouble( 10, vlr_retencion_me );// valor me
                st.setString( 11, fm.getLast_update() );
                st.setString( 12, fm.getUser_update() );
                st.setString( 13, fm.getCreation_date() );
                st.setString( 14, fm.getCreation_user() );
                st.setString( 15, fm.getBase() );
                
                st.execute();
                
                total_impuestos_migrados++;
                
            }
            
        } catch( Exception e ){
            
            e.printStackTrace();
            
        }
        
        finally{
            
            if ( st != null ){
                try{
                    st.close();
                } catch( Exception e ){
                    throw new Exception( "ERROR CERRANDO EL ESTAMENTO " + e.getMessage() );
                }
            }
            if ( con != null ){
                poolManager.freeConnection( "sot", con );
            }
            
        }
        
    }
    
    /**
     * Se encarga de actualizar los campos del retefuente de la tabla CPX_IMP_ITEM.
     * @autor LREALES
     * @param 1 Objeto.
     * @throws Exception En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void actualizarRetefuente( FacturasMims fm ) throws Exception{
        
        Connection con = null;
        PreparedStatement st = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( "sot" );
            
            if( con != null ){
                
                st = con.prepareStatement( this.SQL_UPDATE_CXP_IMP_ITEM );
                
                double vlr_retencion = Double.parseDouble( fm.getRetencion() );
                double vlr_retencion_me = Double.parseDouble( fm.getRetencion_me() );
                double vlr_item = Double.parseDouble( fm.getVlritem() );
                double porcent_retencion = vlr_retencion/vlr_item;
                
                if ( porcent_retencion < 0 )
                    porcent_retencion = porcent_retencion * ( -1 );
                
                st.setDouble( 1, porcent_retencion );
                st.setDouble( 2, vlr_retencion );
                st.setDouble( 3, vlr_retencion_me );
                st.setString( 4, fm.getLast_update() );
                st.setString( 5, fm.getUser_update() );
                st.setString( 6, fm.getDistrito() );
                st.setString( 7, fm.getProveedor() );
                st.setString( 8, fm.getTipo_documento() );
                st.setString( 9, fm.getDocumento() );
                st.setString( 10, fm.getItem() );
                st.setString( 11, "RT01" );
                
                st.executeUpdate();
                
            }
            
        } catch( Exception e ){
            
            e.printStackTrace();
            
        }
        
        finally{
            
            if ( st != null ){
                try{
                    st.close();
                } catch( Exception e ){
                    throw new Exception( "ERROR CERRANDO EL ESTAMENTO " + e.getMessage() );
                }
            }
            if ( con != null ){
                poolManager.freeConnection( "sot", con );
            }
            
        }
        
    }
    /**
     * M�todo que permite ingresar la retefuente a la tabla CXP_IMP_DOC del esquema fin.
     * @autor :         LREALES
     * @param:          1 Objeto.
     * @throws:         Exception
     * @version :       1.0
     */
    public void insertRetefuente_CXP_IMP_DOC( FacturasMims fm  ) throws Exception {
        
        Connection con = null;
        PreparedStatement st = null;
        PoolManager poolManager = null;
        
        try{
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( "sot" );
            
            if( con != null ){
                
                st = con.prepareStatement( this.SQL_CXP_IMP_DOC );
                
                double vlr_retencion = Double.parseDouble( fm.getRetencion() );
                double vlr_retencion_me = Double.parseDouble( fm.getRetencion_me() );
                double vlr_item = Double.parseDouble( fm.getVlritem() );
                double porcent_retencion = vlr_retencion/vlr_item;
                
                if ( porcent_retencion < 0 )
                    porcent_retencion = porcent_retencion * ( -1 );
                
                st.setString( 1, fm.getReg_status() );
                st.setString( 2, fm.getDistrito() );
                st.setString( 3, fm.getProveedor() );
                st.setString( 4, fm.getTipo_documento() );
                st.setString( 5, fm.getDocumento() );
                st.setString( 6, "RT01" );// CODIGO DEL IMPUESTO ( RETEFUENTE )
                st.setDouble( 7, porcent_retencion );// porcentaje
                st.setDouble( 8, vlr_retencion ); // valor
                st.setDouble( 9, vlr_retencion_me );// valor me
                st.setString( 10, fm.getLast_update() );
                st.setString( 11, fm.getUser_update() );
                st.setString( 12, fm.getCreation_date() );
                st.setString( 13, fm.getCreation_user() );
                st.setString( 14, fm.getBase() );
                
                st.execute();
                
                total_imp_cabecera_migrados++;
                
            }
            
        } catch( Exception e ){
            
            e.printStackTrace();
            
        }
        
        finally{
            
            if ( st != null ){
                try{
                    st.close();
                } catch( Exception e ){
                    throw new Exception( "ERROR CERRANDO EL ESTAMENTO " + e.getMessage() );
                }
            }
            if ( con != null ){
                poolManager.freeConnection( "sot", con );
            }
            
        }
        
    }
    
    /**
     * Se encarga de actualizar los campos del retefuente de la tabla CXP_IMP_DOC.
     * @autor LREALES
     * @param 1 Objeto, el valor anterior del impuesto y el valor en moneda extranjera anterior del impuesto.
     * @throws Exception En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void actualizarRetefuente_CXP_IMP_DOC( FacturasMims fm, double valor, double valor_me ) throws Exception{
        
        Connection con = null;
        PreparedStatement st = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( "sot" );
            
            if( con != null ){
                
                st = con.prepareStatement( this.SQL_UPDATE_CXP_IMP_DOC );
                
                double vlr_retencion = valor;
                double vlr_retencion_me = valor_me;
                
                double vlr_item = Double.parseDouble( fm.getVlritem() );
                double porcent_retencion = Double.parseDouble( fm.getRetencion() )/vlr_item;
                
                if ( porcent_retencion < 0 )
                    porcent_retencion = porcent_retencion * ( -1 );
                
                st.setDouble( 1, porcent_retencion );
                st.setDouble( 2, vlr_retencion );
                st.setDouble( 3, vlr_retencion_me );
                st.setString( 4, fm.getLast_update() );
                st.setString( 5, fm.getUser_update() );
                st.setString( 6, fm.getDistrito() );
                st.setString( 7, fm.getProveedor() );
                st.setString( 8, fm.getTipo_documento() );
                st.setString( 9, fm.getDocumento() );
                st.setString( 10, "RT01" );
                
                st.executeUpdate();
                
            }
            
        } catch( Exception e ){
            
            e.printStackTrace();
            
        }
        
        finally{
            
            if ( st != null ){
                try{
                    st.close();
                } catch( Exception e ){
                    throw new Exception( "ERROR CERRANDO EL ESTAMENTO " + e.getMessage() );
                }
            }
            if ( con != null ){
                poolManager.freeConnection( "sot", con );
            }
            
        }
        
    }
    
    /**
     * Se encarga de buscar el codigo de impuestos equivalente en postgres.
     * @autor LREALES
     * @param el codigo de impuestos de mims
     * @throws Exception En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public String buscarSignoImp( String cod_imp_mims ) throws Exception{
        
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        String signo_imp = "";
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( "sot" );
            
            if( con != null ){
                
                st = con.prepareStatement( this.SQL_SIGNO_IMP );
                
                st.setString( 1, cod_imp_mims );
                
                rs = st.executeQuery();
                
                if ( rs.next() ){
                    
                    signo_imp = rs.getString( "ind_signo" ).toUpperCase();
                    
                }
                
            }
            
        } catch( Exception e ){
            
            e.printStackTrace();
            throw new Exception( "ERROR EN 'buscarSignoImp' - [MigracionFacturas].. " + e.getMessage()  );
            
        }
        
        finally{
            
            if ( st != null ){
                try{
                    st.close();
                } catch( Exception e ){
                    throw new Exception( "ERROR CERRANDO EL ESTAMENTO " + e.getMessage() );
                }
            }
            if ( con != null ){
                poolManager.freeConnection( "sot", con );
            }
            
        }
        
        return signo_imp;
        
    }
    
    /*************************************************************************************************************************/
    
    /**
     * Se encarga de actualizar el campo de codigo de cuenta de la tabla cxp_items_doc, cuando no exista codigo de cuenta.
     * @autor LREALES
     * @param -
     * @throws Exception En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void actualizarCodigosDeCuentasVacios () throws Exception {
        
        Connection con = null;
        PreparedStatement st = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( "sot" );
            
            if ( con != null ) {
                
                st = con.prepareStatement( this.SQL_UPDATE_CODIGO_CUENTA );
                
                st.executeUpdate();
                
            }
            
        } catch( Exception e ) {
            
            e.printStackTrace();
            throw new Exception( "ERROR EN 'actualizarCodigosDeCuentasVacios' - [MigracionFacturas].. " + e.getMessage() );
            
        }
        
        finally{
            
            if ( st != null ){
                try{
                    st.close();
                } catch( Exception e ){
                    throw new Exception( "ERROR CERRANDO EL ESTAMENTO " + e.getMessage() );
                }
            }
            if ( con != null ){
                poolManager.freeConnection( "sot", con );
            }
            
        }
        
    }
    
}
