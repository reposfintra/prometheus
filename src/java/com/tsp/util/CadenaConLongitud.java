/*
 * CadenaConLongitud.java
 *
 * Created on 4 de septiembre de 2005, 03:44 PM
 */

package com.tsp.util;

/**
 *
 * @author  Alejandro Payares
 */
public class CadenaConLongitud {
        private String str;
        private int longitud;

        public String getStr() {
            return str;
        }

        public int getLongitud() {
            return longitud;
        }

        public CadenaConLongitud( String cadena, int longitud ) {
            this.str = cadena;
            this.longitud = longitud;
        }
        
        public CadenaConLongitud( String cadena ){
            this(cadena,cadena.length());
        }

        public String toString(){
            return str + "("+longitud+")";
        }
    }