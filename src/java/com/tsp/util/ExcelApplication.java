/*
 * ExcelApplication.java
 *
 * Created on 31 de julio de 2005, 07:23 PM
 */

package com.tsp.util;

import org.apache.poi.hssf.usermodel.*;

import java.io.*;
import java.util.*;
import org.apache.poi.hssf.util.Region;



/**
 * Clase para manejar la creacion de archivos de Excel por el API POI
 * @author Armando
 */
public class ExcelApplication {
    
    private HSSFWorkbook workBook;
    private HSSFSheet sheet;
    private Hashtable<String, HSSFFont> fonts;
    private Hashtable<String, HSSFCellStyle> cellStyles;
    private HSSFDataFormat format;
    private HSSFPalette palette;
   
         
    /**
     * Creates a new instance of ExcelApplication 
     */
    public ExcelApplication() {
        workBook = new HSSFWorkbook();
        fonts = new Hashtable<String, HSSFFont>();
        cellStyles = new Hashtable<String, HSSFCellStyle>();
        format = workBook.createDataFormat();
        palette = workBook.getCustomPalette();

    }
    
    public void openWorkBook(String path){
        FileInputStream fis = null;
        workBook = null;
        try{
            fis = new FileInputStream(path);
            workBook = new HSSFWorkbook(fis);
        }
        catch(FileNotFoundException fnfe){
            fnfe.printStackTrace();
        }
        catch(IOException e){
            e.printStackTrace();
        }
        finally{
            try {fis.close();}catch(IOException ioe){ioe.printStackTrace();}
        }
    }
    
    public void createFont(String fontId, String fontName, short color, boolean bold, short size){
        HSSFFont font = workBook.createFont();
        font.setFontName(fontName);
        font.setColor(color);
        if (bold) font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        font.setFontHeightInPoints(size);
        fonts.put(fontId, font);
    }
    
    public void createStyle(String styleId, HSSFFont font, short color, boolean fullBorder, String format){
        HSSFCellStyle cellStyle = workBook.createCellStyle();
        cellStyle.setFont(font);
        cellStyle.setFillForegroundColor(color);
        cellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        if (fullBorder){
            cellStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
            cellStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
            cellStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
            cellStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
            cellStyle.setAlignment(color);

        }
        cellStyle.setDataFormat(this.format.getFormat(format));
        cellStyles.put(styleId, cellStyle);
    }



        public void createStyle(String styleId, HSSFFont font, short color, boolean fullBorder, String format,short alignment){
        HSSFCellStyle cellStyle = workBook.createCellStyle();
        cellStyle.setFont(font);
        cellStyle.setFillForegroundColor(color);
        cellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        if (fullBorder){
            cellStyle.setBorderBottom(HSSFCellStyle.BORDER_THIN);
            cellStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);
            cellStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
            cellStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
            cellStyle.setAlignment(alignment);

        }
        cellStyle.setDataFormat(this.format.getFormat(format));
        cellStyles.put(styleId, cellStyle);
    }
    
    public void saveToFile(String path){
        FileOutputStream fos = null;
        try{
            fos = new FileOutputStream(path);
            //System.out.println((new Date())+" INICIA ESCRITURA DEL LIBRO");
            workBook.write(fos);
            //System.out.println((new Date())+" FIN ESCRITURA DEL LIBRO");
        }
        catch(FileNotFoundException fnfe){
            fnfe.printStackTrace();
        }
        catch(IOException ioe){
            ioe.printStackTrace();
        }
        finally{
            try {fos.close();}catch(IOException ioe){
                //System.out.println("EXPORTACION A EXCEL: 'ERROR MIENTRAS SE CERRABA EL ARCHIVO'");
            }
        }
    }

    
    public void createSheet(String sheetName){
        sheet = workBook.createSheet(sheetName);
 
    }
    
    public void setActiveSheet(String sheetName){
        sheet = workBook.getSheet(sheetName);
        sheet.addMergedRegion(new Region(1,(short)1,1,(short)2));

    }
    
    public void setDataCell(int rowIndex, int colIndex, String info){
        HSSFRow row = sheet.getRow(rowIndex); 
        if (row == null)
            row = sheet.createRow(rowIndex); 
        HSSFCell cell = row.getCell((short)colIndex);
        if (cell == null)
            cell = row.createCell((short)colIndex);
        cell.setCellValue(info);
    }
    
    public void setDataCell(int rowIndex, int colIndex, double info){
        HSSFRow row = sheet.getRow(rowIndex); 
        if (row == null)
            row = sheet.createRow(rowIndex); 
        HSSFCell cell = row.getCell((short)colIndex);
        if (cell == null)
            cell = row.createCell((short)colIndex);
        cell.setCellValue(info);
    }
    
    public void setDataCell(int rowIndex, int colIndex, int info){
        HSSFRow row = sheet.getRow(rowIndex); 
        if (row == null)
            row = sheet.createRow(rowIndex); 
        HSSFCell cell = row.getCell((short)colIndex);
        if (cell == null)
            cell = row.createCell((short)colIndex);
        cell.setCellValue(info);
    }
    
    public void setCellStyle(int rowIndex, int colIndex, HSSFCellStyle cellStyle){
        HSSFRow row = null;
        HSSFCell cell = null;
        row = sheet.getRow(rowIndex);
        if (row == null)
            row = sheet.createRow(rowIndex);
        cell = row.getCell((short)colIndex);
        if (cell == null)
            cell = row.createCell((short)colIndex);
        cell.setCellStyle(cellStyle) ;
 
    }
    
    /**Funcion que coloca el formato de celda en NUMERIC
     *@author rmartinez
     *@param fila la fila de la celda
     *@param columna la columna de la celda
     */   
    public void setNumericByDefault(int fila,int columna){
         HSSFRow row = sheet.getRow(fila); 
        if (row == null)
            row = sheet.createRow(fila); 
        HSSFCell cell = row.getCell((short)columna);
        if (cell == null)
            cell = row.createCell((short)columna);
        cell.setCellType(cell.CELL_TYPE_NUMERIC);
    }
    
    public HSSFFont getFont(String fontId){
        return fonts.get(fontId);
    }
    
    public HSSFCellStyle getStyle(String styleId){
        return cellStyles.get(styleId);  
    } 
    
    public void createColor(short index, byte red, byte green, byte blue){
        palette.setColorAtIndex(index, red, green, blue);
    }
    
    public int getSheetCount(){
        return workBook.getNumberOfSheets();
    }

        public void cambiarAnchoColumna (int columna, int ancho) throws Exception
        {
        if (sheet==null)
            throw new Exception("No se pudo cambiar el ancho de la columna, primero deber� crear la hoja");
        sheet.setColumnWidth((short) columna, (short) ancho);
        }

    public void combinarCeldas(int filaInicial, int columnaInicial, int filaFinal , int columnaFinal)throws Exception
    {
        if (sheet==null)
            throw new Exception("No se pudo crear la celda, primero deber� crear la hoja");

        sheet.addMergedRegion(new Region(filaInicial,(short)columnaInicial ,filaFinal,(short)columnaFinal));
        
    }

    public void insertaImagen(String ruta,int x1, int y1, int x2, int y2, int col1, int row1, int col2, int row2)throws Exception
    {
        HSSFPatriarch patriarch = sheet.createDrawingPatriarch();
        HSSFClientAnchor anchor  = new HSSFClientAnchor(x1,y1,x2,y2,(short)col1,row1,(short)col2,row2);
       
        anchor.setAnchorType(2);
        java.io.File fichero = new java.io.File(ruta);
        HSSFPicture picture =  patriarch.createPicture(anchor, loadPicture(fichero, workBook ));
       

    }

     //Metodo que permite insertar imagenes a el excel

     private static int loadPicture( File path, HSSFWorkbook wb ) throws IOException

    {

    int pictureIndex;

     FileInputStream fis = null;

     ByteArrayOutputStream bos = null;

      try

     {

       // read in the image file

        fis = new FileInputStream(path);

         bos = new ByteArrayOutputStream( );

         int c;

         // copy the image bytes into the ByteArrayOutputStream

          while ( (c = fis.read()) != -1)

             bos.write( c );
          // add the image bytes to the workbook

           pictureIndex = wb.addPicture(bos.toByteArray(), HSSFWorkbook.PICTURE_TYPE_PNG );



     }

     finally

      {

          if (fis != null)

              fis.close();

          if (bos != null)

              bos.close();

      }

     return pictureIndex;

      }


    /** Leer un Archivo excel
     * JPAcosta
     * @param sheetNum numero de la hoja
     * @return matriz* de los valores en la hoja de calculo.
     */   
    public ArrayList<ArrayList<String>> getDatos(int sheetNum) {
        sheet = workBook.getSheetAt(sheetNum);
        if ( sheet == null) return null;
        ArrayList<ArrayList<String>> aux = new ArrayList<ArrayList<String>>();
        Iterator rows = sheet.rowIterator();
        //XSSFRow row;
        while(rows.hasNext()) {
             ArrayList<String> datosCelda = new ArrayList<String>();
            Iterator cells = ((HSSFRow) rows.next()).cellIterator();
            while (cells.hasNext()) {
                HSSFCell celda = (HSSFCell)cells.next();
                if (celda.getCellType() == HSSFCell.CELL_TYPE_STRING) {
                    datosCelda.add(celda.getStringCellValue());
                } else if (celda.getCellType() == HSSFCell.CELL_TYPE_NUMERIC) {
                    try {
                        datosCelda.add((int)celda.getNumericCellValue() + "");
                    } catch(Exception a) {}
                } else if (celda.getCellType() == HSSFCell.CELL_TYPE_BLANK) {
                    datosCelda.add("");
                } else if (celda.getCellType() == HSSFCell.CELL_TYPE_FORMULA) {
                    datosCelda.add(celda.getCellFormula());
                } else if (celda.getCellType() == HSSFCell.CELL_TYPE_BOOLEAN) {
                    datosCelda.add("" + celda.getBooleanCellValue());
                } else {
                    datosCelda.add("");
                }
            } aux.add(datosCelda);
        }
        return aux;
    }







}
