/********************************************************************
 *      Nombre Clase.................   GenerarOPAuto.java
 *      Descripci�n..................   Generaci�n de OP en Proceso Autom�tico
 *      Autor........................   Ing. Andr�s Maturana De La Cruz
 *      Fecha........................   5 de junio de 2006
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes S�nchez Polo S.A.
 *******************************************************************/

package com.tsp.util.GeneracionOP;

import com.tsp.operation.model.threads.*;
import com.tsp.operation.model.Model;
import com.tsp.operation.model.beans.Usuario;

/**
 *
 * @author  Ing. Andr�s Maturana De La Cruz
 */
public class GenerarOPAuto {
    
    /** Creates a new instance of GenerarOPAuto */
    public GenerarOPAuto() {
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        try{
            Model model = new Model();
            Usuario user = new Usuario();
            user.setLogin("ADMIN");
            HGenerarOPs  hilo  = new HGenerarOPs();
            hilo.start(model, "", "", user, "", "FINV");
        } catch ( Exception e ){
            e.printStackTrace();
            throw new Exception( e.getMessage() );
        }
    }
    
}
