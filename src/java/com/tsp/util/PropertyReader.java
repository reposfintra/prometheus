/*
 * PropertyReader.java
 *
 * Created on 29 de octubre de 2005, 05:47 PM
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package com.tsp.util;

import java.util.*;
import java.io.*;

/**
 * Clase que facilita el trabajo de leer datos de un archivo .properties
 * @author Alejandro Payares
 */
public class PropertyReader {
    
    /**
     * Instancia Singleton de la clase.
     */
    private static PropertyReader instancia;
    
    /**
     * Objeto encargado de leer los datos del archivo .properties
     */
    private Properties rb;
    
    
    /**
     * Objeto que representa el archivo .properties
     */
    private File file;
    
    /**
     * Constructor
     */
    private PropertyReader(){
        
    }
    
    /**
     * Devuelve la instancia Singleton de la clase.
     * @param el nombre del archivo .properties
     */
    public static PropertyReader obtenerInstancia(String nombreArchivo) throws Exception{
        if ( instancia == null ){
            instancia = new PropertyReader();
            instancia.file = new File(nombreArchivo);
            instancia.abrirArchivo();
        }
        return instancia;
    }
    
    /**
     * Devuelve la instancia Singleton de la clase.
     * @param un URL con la ruta del archiivo .properties
     */
    public static PropertyReader obtenerInstancia(java.net.URL url) throws Exception{
        if ( instancia == null ){
            instancia = new PropertyReader();
            instancia.file = new File(new java.net.URI(url.toExternalForm()));
            instancia.abrirArchivo();
        }
        return instancia;
    }

    /**
     * Abre el archivo y carga los datos en el archivo Properties
     */
    public void abrirArchivo() throws Exception{
        try {

            rb = new Properties();

            if(file.exists()) {
                FileInputStream istream = new FileInputStream(file);
                rb.load(istream);
                istream.close();

            }
        }
        catch( IOException ex ){
            ex.printStackTrace();
            throw new Exception(ex);
        }
        
    }
    
    /**
     * Lee del archivo .properties el valor de la clave recibida.
     * @return el valor correspondiente a la clave en el archivo .properties
     */
    public synchronized String obtenerTexto(String clave){
        String res = rb.getProperty(clave);
        return res;
    }
    

}
