package com.tsp.util;

import java.io.*;
import java.util.*;

public class LogWriter
{
   public static final int NONE = 0;
   public static final int ERROR = 1;
   public static final int INFO = 2;
   public static final int DEBUG = 3;

   private static final String ERROR_TEXT = "error";
   private static final String INFO_TEXT = "info";
   private static final String DEBUG_TEXT = "debug";
    
   private PrintWriter pw;
   private String owner;
   private int logLevel;



    public LogWriter(String rutaDbProperties,String usuario, String archivo, String owner) throws Exception
   {

       try {

           java.text.SimpleDateFormat formatoFecha;
           String fechaDocumento;
           java.util.Date fecha;

           // Abriendo un log de error

           ResourceBundle rb   = ResourceBundle.getBundle(rutaDbProperties);

           String rutaInformes = rb.getString("ruta")+ "/exportar/migracion/"+usuario;

           File file = new File(rutaInformes);
           file.mkdirs();
           this.pw        = new PrintWriter(System.err, true);

           Calendar fechaProceso  = Calendar.getInstance();
           fecha                  = fechaProceso.getTime();
           formatoFecha           = new java.text.SimpleDateFormat("yyyy-MM-dd HHmm");
           fechaDocumento         = formatoFecha.format(fecha);

           String logFile   = rutaInformes + archivo +fechaDocumento+".txt";

           this.pw        = new PrintWriter(new FileWriter(logFile, true), true);

       }
       catch (Exception e) {

       }

       this.owner = owner;
       this.logLevel = 2;

       setPrintWriter(pw);

   }




    
   public LogWriter(String owner, int logLevel, PrintWriter pw)
   {
      this.pw = pw;
      this.owner = owner;
      this.logLevel = logLevel;
   }

   public LogWriter(String owner, int logLevel)
   {
      this(owner, logLevel, null);
   }
    
   public int getLogLevel()
   {
      return logLevel;
   }
    
   public PrintWriter getPrintWriter()
   {
      return pw;
   }
    
   public void setLogLevel(int logLevel)
   {
      this.logLevel = logLevel;
   }
    
   public void setPrintWriter(PrintWriter pw)
   {
      this.pw = pw;
   }
    
   public void log(String msg, int severityLevel)
   {
      if (pw != null)
      {
         if (severityLevel <= logLevel)
         {
            pw.println("[" + new Date() + "]  " +
            getSeverityString(severityLevel) + ": " +
            owner + ": " + msg);
         }
      }
   }

   public void log(Throwable t, String msg, int severityLevel)
   {
      log(msg + " : " + toTrace(t), severityLevel);
   }
    
   private String getSeverityString(int severityLevel)
   {
      switch (severityLevel)
      {
         case ERROR:
            return ERROR_TEXT;
         case INFO:
            return INFO_TEXT;
         case DEBUG:
            return DEBUG_TEXT;
         default:
            return "Unknown";
      }
   }
    
   private String toTrace(Throwable e)
   {
      StringWriter sw = new StringWriter();
      PrintWriter pw = new PrintWriter(sw);
      e.printStackTrace(pw);
      pw.flush();
      return sw.toString();
   }
   
   public void log(String msg)
   {
      if (pw != null)
      {
            pw.println( msg);
      }
   }



      public void tituloFinal () {

       log("****************************************************************",LogWriter.INFO);
       log("*                      FINAL DEL PROCESO                       *",LogWriter.INFO);
       log("****************************************************************",LogWriter.INFO);
   }



      public void tituloInicial (String titulo) {

       String tituloNuevo     = "";
       int longitudTotal      = 64;
       int longitudFaltante   = 0;
       int longitudTitulo     = titulo.length();

       if (longitudTitulo > longitudTotal - 2) {
           tituloNuevo = "*" + titulo.substring(0,62) + "*";
       }
       else {
           longitudFaltante = longitudTotal - 2 - longitudTitulo;
           int k = 0;
           while  (k < longitudFaltante   )  {
               titulo = " " +titulo;
               k++;
               if (k<longitudFaltante) {
                   titulo = titulo + " ";
                   k++;
               }

           }
           tituloNuevo = "*"+titulo+"*";
       }

       log("****************************************************************",LogWriter.INFO);
       log( tituloNuevo ,LogWriter.INFO);
       log("****************************************************************" +"\n",LogWriter.INFO);
   }

}