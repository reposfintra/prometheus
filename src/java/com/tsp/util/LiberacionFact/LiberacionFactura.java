/*
 * LiberacionFactura.java
 *
 * Created on 25 de septiembre de 2006, 03:46 PM
 */

package com.tsp.util.LiberacionFact;

import com.tsp.util.connectionpool.PoolManager;
import java.sql.*;
import java.util.*;
import org.apache.log4j.*;

/**
 *
 * @author  Ing. Andr�s Maturana De La Cruz
 */
public class LiberacionFactura {
    
    Logger logger = Logger.getLogger(this.getClass());
    
    private static final String FRA_NO_APROBADAS =
            "SELECT              " +
            "       dstrct ,     " +
            "       corrida ,    " +
            "       tipo_documento , " +
            "       documento , " +
            "       beneficiario " +
            "FROM   fin.corridas  " +
            "WHERE  " +
            "       dstrct = ? " +
            "       AND   to_char(pago,'YYYY-MM-DD') = '0099-01-01' ";
    
    private static final String DEL_FRA_CORRIDA =
            "DELETE  FROM  " +
            "   fin.corridas " +
            "WHERE " +
            "   dstrct = ? " +
            "   AND corrida = ? " +
            "   AND tipo_documento = ? " +
            "   AND documento = ? " +
            "   AND beneficiario = ? ;";
    
    private static final String UPDATE_FRA = 
            "UPDATE  fin.cxp_doc " +
            "SET   corrida='' " +
            "WHERE " +
            "dstrct = ? " +
            "AND proveedor = ? " +
            "AND tipo_documento = ? " +
            "AND documento = ? ; ";
    
    private static final String FRA_APROB = 
            "SELECT " +
            "       corrida, " +
            "       beneficiario, " +
            "       banco, " +
            "       sucursal, " +
            "       sum(valor_me) " +
            "FROM   fin.corridas " +
            "WHERE  " +
            "       dstrct = ? " +
            "       AND to_char(pago,'YYYY-MM-DD')  != '0099-01-01' " +
            "       AND cheque = '' " +
            "GROUP BY 1,2,3,4 " +
            "HAVING  sum(valor_me)<=0 ";
    
    private static final String FRA_APROB_DET = 
            "SELECT * " +
            "FROM fin.corridas " +
            "WHERE  " + 	
            "       dstrct = ? " +
            "       AND corrida = ? " +
            "       AND beneficiario = ? " +
            "       AND banco = ? " +
            "       AND sucursal = ? " +
            "       AND to_char(pago,'YYYY-MM-DD')  != '0099-01-01' " +
            "       AND cheque = '' ";
    
    /** Creates a new instance of LiberacionFactura */
    public LiberacionFactura() {
    }
    
    public void run() throws Exception {
        
        try{
            
            List listaFacturas  =  this.getFacturasParaLiberar("FINV");
            logger.info("FACTURAS PARA LIBERAR: " + listaFacturas.size());
            String sql = this.obtenerSQLLiberacion(listaFacturas);
            
            /* AMATURANA 14.11.2006 */
            logger.info("INICIO DE LIBERACION DE FRAS APROBADAS");
            List bloques = this.getBloqueFacturasParaLiberarAprob( "FINV" );
            
            for(int j=0; j<bloques.size(); j++){
                Hashtable bloque = (Hashtable) bloques.get(j);
                
                String corrida = (String) bloque.get("corrida");
                String prov = (String) bloque.get("proveedor");
                String banco = (String) bloque.get("banco");
                String sucursal = (String) bloque.get("sucursal");
                
                logger.info("CORRIDA: " + corrida );
                logger.info("PROVEEDOR: " + prov );
                logger.info("BANCO: " + banco);
                logger.info("SUCURSAL: " + sucursal);
                
                List listaFacturasAprob  =  this.getFacturasParaLiberarAprobadas( "FINV", corrida, prov, banco, sucursal);
                String sql2 =  this.obtenerSQLLiberacion( listaFacturasAprob );
                logger.info("SQL2: " + sql2);
                sql += sql2;
            }
            /***************************/
            
            this.execute(sql);
        } catch ( Exception e ){
            e.printStackTrace();
            throw new Exception( e.getMessage() );
        }
        
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        
        try{
            
            LiberacionFactura liberarFra = new LiberacionFactura();
            liberarFra.run();
            
        } catch ( Exception e ){ 
            
            e.printStackTrace();
            throw new Exception( e.getMessage() );
            
        }
    }
    
    /**
     * M�todo que obtiene las facturas para ser liberadas
     * @autor       fvillacob
     * @modified    Ing. Andr�s Maturana
     * @param       String distrito
     * @return      List
     * @throws      Exception
     * @version     1.1
     **/
    public List getFacturasParaLiberar(String distrito) throws Exception{
        ResultSet rs = null;
        String query = this.FRA_NO_APROBADAS;
        List lista = new LinkedList();
        Connection con = null;
        PreparedStatement st = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( "sot" );
            st= con.prepareStatement(query);
            st.setString(1, distrito);
            
            rs = st.executeQuery();
            while(rs.next()){
                Hashtable  factura = new Hashtable();
                factura.put("distrito",       rs.getString("dstrct")         );
                factura.put("corrida",        rs.getString("corrida")        );
                factura.put("proveedor",      rs.getString("beneficiario")   );
                factura.put("tipo_documento", rs.getString("tipo_documento") );
                factura.put("documento",      rs.getString("documento")      );
                lista.add( factura );
            }
            
        }
        catch(SQLException e){
            throw new SQLException(" DAO: getFacturasParaLiberar.-->"+ e.getMessage());
        }
        finally{
            
            if ( st != null ){
                try{
                    st.close();
                } catch( SQLException e ){
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO " + e.getMessage() );
                }
            }            
            if ( con != null ){
                poolManager.freeConnection( "sot", con );
            }
        }
        return lista;
    }
    
    /**
     * M�todo que obtiene las facturas para ser liberadas
     * @autor   fvillacob
     * @param   String distrito
     * @return  List
     * @throws  Exception
     * @version 1.0.
     **/
    public String obtenerSQLLiberacion(List lista) throws Exception{
        PreparedStatement stDelete = null;
        PreparedStatement stUpdate = null;
        String queryDelete = DEL_FRA_CORRIDA;
        String queryUpdate = UPDATE_FRA;
        String sql = "";
        Connection con = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( "sot" );
            
            stDelete = con.prepareStatement(queryDelete);
            stUpdate = con.prepareStatement(queryUpdate);
            
            
            for(int i=0;i<lista.size();i++){
                Hashtable factura   = (Hashtable)lista.get(i);
                String distrito       = (String) factura.get("distrito");
                String corrida        = (String) factura.get("corrida");
                String proveedor      = (String) factura.get("proveedor");
                String tipo_documento = (String) factura.get("tipo_documento");
                String documento      = (String) factura.get("documento");
                
                
                // Eliminamos:
                stDelete.setString(1, distrito       );
                stDelete.setString(2, corrida        );
                stDelete.setString(3, tipo_documento );
                stDelete.setString(4, documento      );
                stDelete.setString(5, proveedor      );
                
                sql += stDelete.toString();
                
                
                // Actualizamos:
                stUpdate.setString(1, distrito       );
                stUpdate.setString(2, proveedor      );
                stUpdate.setString(3, tipo_documento );
                stUpdate.setString(4, documento      );
                
                sql += stUpdate.toString();
                
                
                stDelete.clearParameters();
                stUpdate.clearParameters();
                
            }
            
            
        }
        catch(SQLException e){
            throw new SQLException(" DAO: obtenerSQLLiberacion.-->"+ e.getMessage());
        }
        finally{
            if(stDelete!=null) stDelete.close();
            if(stUpdate!=null) stUpdate.close();            
            if ( con != null ){
                poolManager.freeConnection( "sot", con );
            }
        }
        
        return sql;
    }
    
    public void execute(String sql)throws SQLException{
        
        Connection con = null;
        Statement st = null;
        PoolManager poolManager = null;
                
        try{
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("sot");
            st = con.createStatement();
            
            st.addBatch(sql);
        
            if(st!=null){
                boolean autocommit = con.getAutoCommit();
                con.setAutoCommit(false);
                st.toString();
                st.executeBatch();
                con.commit();
                con.setAutoCommit(autocommit);
            }
            
        } catch(SQLException e) {
            con.rollback();
            throw new SQLException("ERROR DURANTE LA TRANSACCION LOS CAMBIOS NO SE PRODUJERON EL LA BASE DE DATOS " + e.getMessage() + " " + e.getErrorCode()+" <br> La siguiente exception es : ----" + e.getNextException());
            
        } finally {
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("sot", con);
            }
        }
        
    }
    
    /**
     * M�todo que obtiene las facturas para ser liberadas que son aprobadas
     * y cuya suma agrupadas por proveedor, banco y sucursal es negativa
     * @autor   Ing. Andr�s Maturana De La Cruz
     * @param   dstrct distrito
     * @param   corrida Numero de la corrida
     * @return  List
     * @throws  Exception
     * @version 1.0.
     **/
    public List getBloqueFacturasParaLiberarAprob(String distrito) throws Exception{
        ResultSet rs = null;
        String query = this.FRA_APROB;
        List lista = new LinkedList();
        Connection con = null;
        PreparedStatement st = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( "sot" );
            st= con.prepareStatement(query);
            st.setString(1, distrito);
            logger.info("SQL BLOQUE: " + st);
            
            rs = st.executeQuery();
            while(rs.next()){
                Hashtable  factura = new Hashtable();
                factura.put("corrida",       rs.getString("corrida"));
                factura.put("proveedor",     rs.getString("beneficiario"));
                factura.put("banco",         rs.getString("banco") );
                factura.put("sucursal",      rs.getString("sucursal"));
                lista.add( factura );
            }
            
        } catch(SQLException e) {
            con.rollback();
            throw new SQLException("ERROR DURANTE LA TRANSACCION LOS CAMBIOS NO SE PRODUJERON EL LA BASE DE DATOS " + e.getMessage() + " " + e.getErrorCode()+" <br> La siguiente exception es : ----" + e.getNextException());
            
        } finally {
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("sot", con);
            }
        }
        return lista;
    }
    
    /**
     * M�todo que obtiene las facturas para ser liberadas que son aprobadas
     * y cuya suma agrupadas por proveedor, banco y sucursal es negativa
     * @autor   Ing. Andr�s Maturana De La Cruz
     * @param   dstrct distrito
     * @param   corrida Numero de la corrida
     * @param   prov Nit del proveedor
     * @param   banco Nombre del banco
     * @param   sucursal Nombre de la sucursal
     * @return  List
     * @throws  Exception
     * @version 1.0.
     **/
    public List getFacturasParaLiberarAprobadas(String distrito, String corrida, String prov, String banco, String sucursal) throws Exception{
        ResultSet rs = null;
        String query = this.FRA_APROB_DET;
        List lista = new LinkedList();
        Connection con = null;
        PreparedStatement st = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( "sot" );
            st= con.prepareStatement(query);
            st.setString(1, distrito);
            st.setString(2, corrida);
            st.setString(3, prov);
            st.setString(4, banco);
            st.setString(5, sucursal);
            
            logger.info("SQL DET BLOQUE: " + st);
            rs = st.executeQuery();
            while(rs.next()){
                Hashtable  factura = new Hashtable();
                factura.put("distrito",       rs.getString("dstrct")         );
                factura.put("corrida",        rs.getString("corrida")        );
                factura.put("proveedor",      rs.getString("beneficiario")   );
                factura.put("tipo_documento", rs.getString("tipo_documento") );
                factura.put("documento",      rs.getString("documento")      );
                lista.add( factura );
            }
            
        } catch(SQLException e) {
            con.rollback();
            throw new SQLException("ERROR DURANTE LA TRANSACCION LOS CAMBIOS NO SE PRODUJERON EL LA BASE DE DATOS " + e.getMessage() + " " + e.getErrorCode()+" <br> La siguiente exception es : ----" + e.getNextException());
            
        } finally {
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("sot", con);
            }
        }
        return lista;
    }
    
}
