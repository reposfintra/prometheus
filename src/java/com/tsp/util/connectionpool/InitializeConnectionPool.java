package com.tsp.util.connectionpool;

// Servlet imports
import javax.servlet.ServletContextListener;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContext;
// SQL Utility Imports
//import com.tsp.util.NamingService;

/**
 * This listenter is used to initialize the JDBC Connection Pool for a
 * given Web Application.
 */
public class InitializeConnectionPool implements ServletContextListener {

   /**
    * This method creates the connection pool.
     * @param sce
    */
   @Override
   public void contextInitialized(ServletContextEvent sce) {
      ServletContext context = sce.getServletContext();

      // Intialize the connection pool object
      try {
	// Get a reference to the NamingService singleton
        PoolManager poolMgr = PoolManager.getInstance();
	// Create ConnectionPool object
         
	// Store the connection pool in the "application" scope.
	context.log("InitializeConnectionPool ** Connection pool created **");
      
      } catch (RuntimeException e) {
         context.log("There was an error in creating the "
                     + "connection pool for " + e );
      }
   }

    /**
     *
     * @param sce
     */
    @Override
   public void contextDestroyed(ServletContextEvent sce) {
      ServletContext context = sce.getServletContext();

      // Get a reference to the NamingService singleton.
      PoolManager poolMgr = PoolManager.getInstance();

      // Shutdown the connection pool.
      poolMgr.release();

      context.log("Connection pool shutdown.");
   }
}
