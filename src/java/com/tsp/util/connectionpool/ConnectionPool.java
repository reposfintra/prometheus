package com.tsp.util.connectionpool;

import java.sql.*;
import java.util.*;
import java.io.*;
import com.tsp.util.LogWriter;


public class ConnectionPool {
  
  private String name;
  private String URL;
  private String user;
  private String password;
  private int maxConns;
  private int timeOut;
  private String dataSourceName;
  private LogWriter logWriter;
  private int checkedOut;
  private Vector freeConnections = new Vector();
  
  public ConnectionPool(
    String name, String URL, String user, String password,
    int maxConns, int initConns, int timeOut, String dataSrc,
    PrintWriter pw, int logLevel
  ){
    
    this.name = name;
    this.URL = URL;
    this.user = user;
    this.password = password;
    this.maxConns = maxConns;
    this.timeOut = timeOut > 0 ? timeOut : 5;
    this.dataSourceName = dataSrc;
    
    logWriter = new LogWriter(name, logLevel, pw);
    initPool(initConns);
    
    //logWriter.lo("Un nuevo pool ha sido creado", LogWriter.INFO);
    String lf = System.getProperty("line.separator");
    /*logWriter.lo(lf +
    " url=" + URL + lf +
    " user=" + user + lf +
    " password=" + password + lf +
    " initconns=" + initConns + lf +
    " maxconns=" + maxConns + lf +
    " logintimeout=" + this.timeOut, LogWriter.DEBUG);*/
    //logWriter.lo(getStats(), LogWriter.DEBUG);
  }
  
  private void initPool(int initConns) {
    for (int i = 0; i < initConns; i++) {
      try {
        Connection pc = newConnection();
        freeConnections.addElement(pc);
      }catch (SQLException e){ }
    }
  }
  
  public Connection getConnection() throws SQLException {
    //logWriter.lo("Solicitud de conexion", LogWriter.DEBUG);
    try {
      return getConnection(timeOut * 1000);
    }catch (SQLException e) {
      e.printStackTrace();
      logWriter.log(e, "Error de Exception al solicitar una conexion",
      LogWriter.ERROR);
      throw e;
    }
  }
  
  private synchronized Connection getConnection(long timeout)
  throws SQLException {
    
    // Get a pooled Connection from the cache or a new one.
    // Wait if all are checked out and the max limit has
    // been reached.
    long startTime = System.currentTimeMillis();
    long remaining = timeout;
    Connection conn = null;
    while ((conn = getPooledConnection()) == null) {
      try {
        //logWriter.lo("Esperando por una conexi�n. Timeout=" + remaining,
        //LogWriter.INFO);
        wait(remaining);
      }catch (InterruptedException e){ 
      e.printStackTrace();
      }
      remaining = timeout - (System.currentTimeMillis() - startTime);
      if (remaining <= 0) {
        // Timeout has expired
        logWriter.log("Time-out mientras espera por una conexi�n",
        LogWriter.DEBUG);
        throw new SQLException("getConnection() timed-out");
      }
    }
    
    // Check if the Connection is still OK
    if (!isConnectionOK(conn)) {
      // It was bad. Try again with the remaining timeout
      logWriter.log("Removiendo una mala conexion del pool", LogWriter.ERROR);
      return getConnection(remaining);
    }
    checkedOut++;
    //logWriter.lo("Conexi�n enviada desde el pool", LogWriter.DEBUG);
    //logWriter.lo(getStats(), LogWriter.INFO);
    return conn;
  }
  
  private boolean isConnectionOK(Connection conn) {
    boolean isConnOk = true;
    Statement testStmt = null;
    ResultSet testRs   = null;
    try {
      // Try to execute a query to see if it's really alive in the server side.
      if (!conn.isClosed()) {
        testStmt = conn.createStatement();
        testRs   = testStmt.executeQuery("SELECT * FROM " + this.dataSourceName);
      }else
        isConnOk = false;
    }catch (SQLException e) {
        e.printStackTrace();
      isConnOk = false;
      logWriter.log(e, "Pooled Connection was not okay", LogWriter.ERROR);
    }finally{
      if (testStmt != null) {
        try {
          testStmt.close();
        }catch (SQLException se){ }
      }
      if (testRs != null) {
        try {
          testRs.close();
        }catch (SQLException se){ }
      }
      if( !isConnOk )
      {
        try {
          conn.close();
          conn = null;
        }catch (Exception ignore){
          conn = null;
        }
      }
    }
    return isConnOk;
  }
  
  private Connection getPooledConnection() throws SQLException {
    Connection conn = null;
    if (freeConnections.size() > 0) {
      // Pick the first Connection in the Vector
      // to get round-robin usage
      conn = (Connection) freeConnections.firstElement();
      
      // Check the connection.
      
      freeConnections.removeElementAt(0);
    }else if (maxConns == 0 || checkedOut < maxConns) {
      conn = newConnection();
    }
    return conn;
  }
  
  private Connection newConnection() throws SQLException {
    Connection conn = null;
    if (user == null) {
      conn = DriverManager.getConnection(URL);
    }else{
      conn = DriverManager.getConnection(URL, user, password);
    }
    //logWriter.lo("Nueva conexion abierta", LogWriter.INFO);
    return conn;
  }
  
  public synchronized void freeConnection(Connection conn) {
    // Put the connection at the end of the Vector
    freeConnections.addElement(conn);
    checkedOut--;
    notifyAll();
    //logWriter.lo("Conexion retornada al pool", LogWriter.DEBUG);
    //logWriter.lo(getStats(), LogWriter.INFO);
  }
  
  public synchronized void release() {
    Enumeration allConnections = freeConnections.elements();
    while (allConnections.hasMoreElements()) {
      Connection con = (Connection) allConnections.nextElement();
      try {
        con.close();
        //logWriter.lo("Conexion cerrada", LogWriter.INFO);
      }catch (SQLException e) {
        logWriter.log(e, "No se pudo cerrar la conexion", LogWriter.ERROR);
      }
    }
    freeConnections.removeAllElements();
  }
  
  private String getStats() {
    return "Total conexiones: " +
    (freeConnections.size() + checkedOut) +
    " Disponibles: " + freeConnections.size() +
    " Utilizadas  : " + checkedOut;
  }
}