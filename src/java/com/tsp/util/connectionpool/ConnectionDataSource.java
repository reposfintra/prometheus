/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.util.connectionpool;

import java.beans.PropertyVetoException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.DataSource;

import org.apache.commons.dbcp2.BasicDataSource;

public class ConnectionDataSource {

    private static long startTime = 0;
    private static long endTime = 0;
    private static long allTime = 0;
    public static final String DRIVER = "org.postgresql.Driver";
    public static final String URL = "jdbc:postgresql://162.242.200.185:5432/fintra";
    public static final String USERNAME = "postgres";
    public static final String PASSWORD = "bdversion17";
    private BasicDataSource dataSource = null;
    private static ConnectionDataSource connectionDataSource = null;
    private DataSource ds;

    public ConnectionDataSource() {
        iniciarDataSource();
    }

    public void iniciarDataSource() {
        dataSource = new BasicDataSource();
        dataSource.setDriverClassName(ConnectionDataSource.DRIVER);
        dataSource.setUrl(ConnectionDataSource.URL);
        dataSource.setUsername(ConnectionDataSource.USERNAME);
        dataSource.setPassword(ConnectionDataSource.PASSWORD);

        dataSource.setTestWhileIdle(true);
        dataSource.setTestOnReturn(true);
        dataSource.setTestOnBorrow(true);
        dataSource.setMaxIdle(25);
        dataSource.setMinIdle(8);
        dataSource.setInitialSize(10);
        dataSource.setMaxWaitMillis(1000);
        dataSource.setValidationQuery("SELECT 1");
        dataSource.setValidationQueryTimeout(10);
        dataSource.setRemoveAbandonedTimeout(20);
        dataSource.setMaxTotal(50);

    }

    /**
     * @return the dataSource
     */
    public BasicDataSource getDataSource() {
        return dataSource;
    }

    /**
     * @param dataSource the dataSource to set
     */
    public void setDataSource(BasicDataSource dataSource) {
        this.dataSource = dataSource;
    }

    public Connection getConnection() throws SQLException {
        return this.dataSource.getConnection();
    }

    public static ConnectionDataSource getInstance() throws IOException, SQLException, PropertyVetoException {
        if (connectionDataSource == null) {

            connectionDataSource = new ConnectionDataSource();
            startTime = System.currentTimeMillis();
            return connectionDataSource;
        } else {
            System.out.println("/********************************************************/\n");
            System.out.println("isClosed: " + connectionDataSource.dataSource.isClosed());
            System.out.println("TestOnReturn: " + connectionDataSource.getDataSource().getTestOnReturn());
            System.out.println("TestWhileIdle: " + connectionDataSource.getDataSource().getTestWhileIdle());
            System.out.println("NumActive: " + connectionDataSource.getDataSource().getNumActive());
            System.out.println("CacheState: " + connectionDataSource.getDataSource().getCacheState());
            System.out.println("isAccessToUnderlyingConnectionAllowed: " + connectionDataSource.getDataSource().isAccessToUnderlyingConnectionAllowed());
            System.out.println("/********************************************************/\n");

            endTime = System.currentTimeMillis();
            System.out.println("enTime:" + endTime);
            validarDisponibilidad(endTime, connectionDataSource.getStartTime());

            return connectionDataSource;
        }
    }

    public void close() {
        try {
            getConnection().close();
        } catch (SQLException ex) {
            Logger.getLogger(ConnectionDataSource.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private static void validarDisponibilidad(long endTime, long starTime) {
        allTime = (endTime - starTime);
       
        long milisegundos = allTime;
        long hora, minuto, segundo;
        long restohora, restominuto, restosegundo;

        hora = milisegundos / 3600000;
        restohora = milisegundos % 3600000;

        minuto = restohora / 60000;
        restominuto = restohora % 60000;

        segundo = restominuto / 1000;
        restosegundo = restominuto % 1000;

        System.out.println("Tiempo de creacion del pool: "+hora + ":" + minuto + ":" + segundo + "." + restosegundo);

      
        System.out.println("Resta: " + allTime);
        if (allTime > 3600000) {
            System.out.println("MAs de una hora de creacion del pool. ");
        }

    }

    /**
     * @return the startTime
     */
    public long getStartTime() {
        return startTime;
    }

    /**
     * @param startTime the startTime to set
     */
    public void setStartTime(long startTime) {
        ConnectionDataSource.startTime = startTime;
    }

    /**
     * @return the endTime
     */
    public long getEndTime() {
        return endTime;
    }

    /**
     * @param endTime the endTime to set
     */
    public void setEndTime(long endTime) {
        ConnectionDataSource.endTime = endTime;
    }

}
