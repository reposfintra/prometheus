package com.tsp.util.connectionpool;

// Servlet imports
import javax.servlet.ServletContextListener;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContext;
// SQL Utility Imports
import sl314.util.sql.ConnectionPool;
import java.sql.SQLException;

/**
 * This listenter is used to initialize the JDBC Connection Pool for a
 * given Web Application.
 */
public class InitializeConnectionPoolOLD implements ServletContextListener {

   /**
    * This method creates the connection pool.
    */
   public void contextInitialized(ServletContextEvent sce) {
      ServletContext context = sce.getServletContext();
      String jdbcDriver = context.getInitParameter("jdbcDriver");
      String jdbcURL = context.getInitParameter("jdbcURL");
      String jdbcUserName = context.getInitParameter("jdbcUserName");
      String jdbcPassword = context.getInitParameter("jdbcPassword");
      int minimumConnections
	= Integer.parseInt(context.getInitParameter("minimumConnections"));
      int maximumConnections
	= Integer.parseInt(context.getInitParameter("maximumConnections"));
      ConnectionPool connectionPool;

      // Intialize the connection pool object
      try {
	// Get a reference to the NamingService singleton
        NamingService nameSvc = NamingService.getInstance();

	// Create ConnectionPool object
        connectionPool = new ConnectionPool(jdbcDriver, jdbcURL,
                                            jdbcUserName, jdbcPassword,
                                            minimumConnections,
                                            maximumConnections);

	// Store the connection pool in the "application" scope.
        nameSvc.setAttribute("connectionPool", connectionPool);

	context.log("Connection pool created for URL=" + jdbcURL);

      } catch (ClassNotFoundException cnfe) {
         context.log("The JDBC Driver class " + jdbcDriver
                     + " was no tfound.", cnfe);

      } catch (InstantiationException ie) {
         context.log("The JDBC Driver class " + jdbcDriver + " could "
                     + "no tmake an object.", ie);

      } catch (IllegalAccessException iae) {
         context.log("The JDBC Driver (" + jdbcDriver + ") class "
                     + "or initializer is no taccessible.", iae);

      } catch (SQLException sex) {
         context.log("There was an error in creating the "
                     + "connection pool for " + jdbcURL, sex);
      }
   }

   public void contextDestroyed(ServletContextEvent sce) {
      ServletContext context = sce.getServletContext();

      // Get a reference to the NamingService singleton.
      NamingService nameSvc = NamingService.getInstance();

      // Retrieve the connection pool from the naming service.
      ConnectionPool connectionPool
        = (ConnectionPool) nameSvc.getAttribute("connectionPool");

      // Shutdown the connection pool.
      connectionPool.shutdown();

      context.log("Connection pool shutdown.");
   }
}
