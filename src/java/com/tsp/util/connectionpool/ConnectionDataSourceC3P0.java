/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.util.connectionpool;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import com.tsp.util.LogWriter;
import static com.tsp.util.Util.logWriter;
import java.beans.PropertyVetoException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

/**
 *
 * @author hcuello
 */
public class ConnectionDataSourceC3P0 {
    
    private static ConnectionDataSourceC3P0 datasource;
    
    private ComboPooledDataSource cpds;

    public ConnectionDataSourceC3P0() throws PropertyVetoException {
        InputStream is = getClass().getResourceAsStream("db.properties");
        Properties dbProps = new Properties();
        try {
            dbProps.load(is);
        } catch (Exception e) {
            logWriter.log("No se pudo leer el archivo de propiedades. "
                    + "Asegurese que db.properties esta en el CLASSPATH" + is,
                    LogWriter.ERROR);
            return;
        }
        
       
        cpds = new ComboPooledDataSource();
        cpds.setDriverClass(dbProps.getProperty("drivers")); //loads the jdbc driver
        cpds.setJdbcUrl(dbProps.getProperty("fintra.url"));
        cpds.setUser(dbProps.getProperty("fintra.user"));
        cpds.setPassword(dbProps.getProperty("fintra.password"));
        //Opciones iniciales C3P0
        cpds.setInitialPoolSize(1);
        cpds.setMinPoolSize(1);
        cpds.setAcquireIncrement(1);
        cpds.setMaxPoolSize(100);
        cpds.setMinPoolSize(30);
        cpds.setMaxStatements(300);
        cpds.setCheckoutTimeout(10000);
        cpds.setUnreturnedConnectionTimeout(600);
        cpds.setIdleConnectionTestPeriod(300);        
        cpds.setAutoCommitOnClose(true);
        cpds.setPreferredTestQuery("SELECT 1");        
        cpds.setAcquireRetryAttempts(1);
        cpds.setBreakAfterAcquireFailure(true);
        cpds.setTestConnectionOnCheckin(true);
       
    }
    
    /**
     *
     * @return
     * @throws IOException
     * @throws SQLException
     * @throws PropertyVetoException
     */
    public static synchronized ConnectionDataSourceC3P0 getInstance() throws IOException, SQLException, PropertyVetoException {
        if (getDatasource() == null) {
            setDatasource(new ConnectionDataSourceC3P0());
            return getDatasource();
        } else {
            
            return getDatasource();
        }
    }
    
    public synchronized Connection getConnection() throws SQLException {
        return this.getCpds().getConnection();
    }

    /**
     * @return the cpds
     */
    public ComboPooledDataSource getCpds() {
        return cpds;
    }

    /**
     * @param cpds the cpds to set
     */
    public void setCpds(ComboPooledDataSource cpds) {
        this.cpds = cpds;
    }
    
  /**
     * @return the datasource
     */
    public static ConnectionDataSourceC3P0 getDatasource() {
        return datasource;
    }

    /**
     * @param aDatasource the datasource to set
     */
    public static void setDatasource(ConnectionDataSourceC3P0 aDatasource) {
        datasource = aDatasource;
    }  
}
