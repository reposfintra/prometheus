package com.tsp.util.connectionpool;

import java.sql.*;
import java.io.*;
import java.util.*;

import com.tsp.util.LogWriter;

public class PoolManager {
  
  static private PoolManager instance;
  static private int clients;
  
  private LogWriter logWriter;
  private PrintWriter pw;
  
  private Vector drivers = new Vector();
  private Hashtable pools = new Hashtable();
  
  private PoolManager() throws RuntimeException {
    init();
  }
  
  public static synchronized PoolManager getInstance() throws RuntimeException {
    if (instance == null) {
      instance = new PoolManager();
    }
    clients++;
    return instance;
  }
  
  private void init() throws RuntimeException {
    // Log to System.err until we have read the logfile property
    pw = new PrintWriter(System.err, true);
    logWriter = new LogWriter("PoolManager", LogWriter.INFO, pw);
    // lineas adicionada por nparejo
    InputStream is = getClass().getResourceAsStream("db.properties");
    Properties dbProps = new Properties();
    try {
      dbProps.load(is);
    }
    catch (Exception e) {
      logWriter.log("No se pudo leer el archivo de propiedades. " +
      "Asegurese que db.properties esta en el CLASSPATH"+is,
      LogWriter.ERROR);
      return;
    }
    String logFile = dbProps.getProperty("logfile");
    if (logFile != null) {
      try {
        pw = new PrintWriter(new FileWriter(logFile, true), true);
        logWriter.setPrintWriter(pw);
      }
      catch (IOException e) {
        logWriter.log("No se pudo abrir el log file: " + logFile +
        ". Usando el System.err por defecto", LogWriter.ERROR);
      }
    }
    loadDrivers(dbProps);
    createPools(dbProps);
  }
  
  private void loadDrivers(Properties props) {
    String driverClasses = props.getProperty("drivers");
    StringTokenizer st = new StringTokenizer(driverClasses);
    while (st.hasMoreElements()) {
      String driverClassName = st.nextToken().trim();
      try {
        Driver driver = (Driver)
        Class.forName(driverClassName).newInstance();
        DriverManager.registerDriver(driver);
        drivers.addElement(driver);
        logWriter.log("Registrando el JDBC driver " + driverClassName,
        LogWriter.INFO);
      }
      catch (Exception e) {
        logWriter.log(e, "No se pudo registrar el JDBC driver: " +
        driverClassName, LogWriter.ERROR);
      }
    }
  }
  
  private void createPools(Properties props) throws RuntimeException {
    Enumeration propNames = props.propertyNames();
    String poolName, name, url, user, password, maxConns, initConns
         , loginTimeOut, logLevelProp, connPoolDataSrc;
    int max, init, timeOut, logLevel;
    
    while (propNames.hasMoreElements()) {
      name = (String) propNames.nextElement();
      if (name.endsWith(".url")) {
        poolName = name.substring(0, name.lastIndexOf("."));
        url = props.getProperty(poolName + ".url");
        if (url == null) {
          logWriter.log("No se especifico el URL para " + poolName,
          LogWriter.ERROR);
          continue;
        }
        
        user = props.getProperty(poolName + ".user");
        password = props.getProperty(poolName + ".password");
        maxConns = props.getProperty(poolName + ".maxconns", "0");
        try {
          max = Integer.valueOf(maxConns).intValue();
        }
        catch (NumberFormatException e) {
          logWriter.log("Invalido numero maximo de conexiones " + maxConns +
          " para " + poolName, LogWriter.ERROR);
          max = 0;
        }
        
        initConns = props.getProperty(poolName + ".initconns", "0");
        try {
          init = Integer.valueOf(initConns).intValue();
        }
        catch (NumberFormatException e) {
          logWriter.log("Numero de conexiones iniciales invalidas " + initConns +
          " para " + poolName, LogWriter.ERROR);
          init = 0;
        }
        
        loginTimeOut = props.getProperty(poolName + ".logintimeout", "5");
        try {
          timeOut = Integer.valueOf(loginTimeOut).intValue();
        }
        catch (NumberFormatException e) {
          logWriter.log("Valor invalido para logintimeout " + loginTimeOut +
                        " for " + poolName, LogWriter.ERROR);
          timeOut = 5;
        }
        
        logLevelProp = props.getProperty( poolName + ".loglevel"
                                        , String.valueOf(LogWriter.ERROR) );
        logLevel = LogWriter.INFO;
        if (logLevelProp.equalsIgnoreCase("none")) {
          logLevel = LogWriter.NONE;
        }
        else if (logLevelProp.equalsIgnoreCase("error")) {
          logLevel = LogWriter.ERROR;
        }
        else if (logLevelProp.equalsIgnoreCase("debug")) {
          logLevel = LogWriter.DEBUG;
        }
        
        connPoolDataSrc = props.getProperty("connpooldatasrc", "");
        if( connPoolDataSrc.equals("") )
          throw new RuntimeException (
          "Se debe especificar una fuente de datos para el pool \"" +
          poolName + "\", en la propiedad \"connpooldatasrc\"."
          );
        ConnectionPool pool = new ConnectionPool(
          poolName, url, user, password, max, init,
          timeOut, connPoolDataSrc, pw, logLevel
        );
        pools.put(poolName, pool);
      }
    }
  }
  
  public Connection getConnection(String name) {
    Connection conn = null;
    ConnectionPool pool = (ConnectionPool) pools.get(name);
    if (pool != null) {
      try {
        conn = pool.getConnection();
      }
      catch (SQLException e) {
        e.printStackTrace();  
        logWriter.log(e, "Se consiguio error de Exception desde la conexion  " +
        name, LogWriter.ERROR);
      }
    }
    return conn;
  }
  
  public void freeConnection(String name, Connection con) {
    ConnectionPool pool = (ConnectionPool) pools.get(name);
    if (pool != null) {
      pool.freeConnection(con);
    }
  }
  
  public synchronized void release() {
    // Wait until called by the last client
    if (--clients != 0) {
      return;
    }
    
    Enumeration allPools = pools.elements();
    while (allPools.hasMoreElements()) {
      ConnectionPool pool = (ConnectionPool) allPools.nextElement();
      pool.release();
    }
    
    Enumeration allDrivers = drivers.elements();
    while (allDrivers.hasMoreElements()) {
      Driver driver = (Driver) allDrivers.nextElement();
      try {
        DriverManager.deregisterDriver(driver);
        logWriter.log("Eliminado registro del JDBC driver " +
        driver.getClass().getName(), LogWriter.INFO);
      }
      catch (SQLException e) {
        logWriter.log(e, "No pudo eliminar registro del JDBC driver: " +
        driver.getClass().getName(), LogWriter.ERROR);
      }
    }
  }
}