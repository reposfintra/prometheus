/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//package com.tsp.util;

/**
 *
 * @author haroldc
 */
/*
public class MyLogger {
    
}*/


/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.util;

import java.io.IOException;
import java.util.Properties;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class MyLogger {
    
    private static FileHandler handler;
    private static SimpleFormatter formatterTxt;
    
   
    /**
     *
     * @param propiedades
     * @param mensaje
     * @param level
     */
    static public void setUp(Properties propiedades, String mensaje, Level level) {
        // Create Logger  

        try {
            handler = new FileHandler(propiedades.getProperty("ruta_log"), true);
            //Create Logger
            Logger logger = Logger.getLogger("LOG API CREDIT");
            logger.setLevel(level);
            formatterTxt = new SimpleFormatter();
            handler.setFormatter(formatterTxt);
            logger.addHandler(handler);
            logger.info(mensaje);

        } catch (IOException | SecurityException ex) {
            throw new RuntimeException("Error al inicializar el logger. " + ex.getLocalizedMessage());
        } finally {
            handler.close();
        }
    }
 
}



