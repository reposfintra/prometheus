/*
 * Rhymbox.java
 *
 * Created on 5 de enero de 2005, 12:59 PM
 */
package com.tsp.util.rhymbox;

import java.util.*;
import java.net.*;
import java.io.*;
import org.jabber.jabberbeans.*;
import org.jabber.jabberbeans.util.JID;
import org.jabber.jabberbeans.Extension.*;
/**
 *
 * @author  EQUIPO
 */
public class Rhymbox {
    
    private String user;
    private String password;
    private String resourse;
    private String server;
    private Enumeration adressees; 
    private Properties direcciones;
    private String body; 
    
    private Message msg;
    private MessageBuilder mb;
    private InetAddress dir;
    private ConnectionBean cb;
    private InfoQueryBuilder iqb;
    private InfoQuery iq;
    private IQAuthExtensionBuilder iqAuthb;
    
    /** Creates a new instance of Rhymbox */
    public Rhymbox() {
        direcciones = new Properties();
    }
    
    /**
     * Getter for property password.
     * @return Value of property password.
     */
    public java.lang.String getPassword() {
        return password;
    }
    
    /**
     * Setter for property password.
     * @param password New value of property password.
     */
    public void setPassword(java.lang.String password) {
        this.password = password;
    }
    
    /**
     * Getter for property resourse.
     * @return Value of property resourse.
     */
    public java.lang.String getResourse() {
        return resourse;
    }
    
    /**
     * Setter for property resourse.
     * @param resourse New value of property resourse.
     */
    public void setResourse(java.lang.String resourse) {
        this.resourse = resourse;
    }
    
    /**
     * Getter for property server.
     * @return Value of property server.
     */
    public java.lang.String getServer() {
        return server;
    }
    
    /**
     * Setter for property server.
     * @param server New value of property server.
     */
    public void setServer(java.lang.String server) {
        this.server = server;
    }
    
    /**
     * Getter for property user.
     * @return Value of property user.
     */
    public java.lang.String getUser() {
        return user;
    }
    
    /**
     * Setter for property user.
     * @param user New value of property user.
     */
    public void setUser(java.lang.String user) {
        this.user = user;
    }
    
    //CARGA VALORES DEL ARCHIVO DE PROPIEDADES
    private void load(){
        try{
           direcciones.load(getClass().getResourceAsStream("direcciones.properties"));
        }
        catch(IOException e){
            ////System.out.println("RHYMBOX: ERROR LEYENDO ARCHIVO DE PROPIEDADES");
        }
        setAdressees(direcciones.keys());
        setUser(direcciones.getProperty("1"));
        setPassword(direcciones.getProperty("2"));
        setServer(direcciones.getProperty("3"));
    }
    
    //CONECTA AL SERVIDOR
    private void connect(){
        try{
           dir = InetAddress.getByName(getServer());
           cb = new ConnectionBean();
           cb.connect(dir);
        }
        catch(UnknownHostException e){
            ////System.out.println("*** SERVIDOR NO ENCONTRADO ***");
        }
        catch(IOException ex){
            ////System.out.println("*** ERROR DE COMUNIACCIONES ***");
        }
    }
    
    //AUTENTICA EL USUARIO
    private void authenticate(){
        
        connect();
        
        iqb = new InfoQueryBuilder();
        iqAuthb = new IQAuthExtensionBuilder();
        
        // Configure to set.
        iqb.setType("set");
        // Set data for the login to a Jabber server.
        iqAuthb.setUsername(getUser());
        iqAuthb.setPassword(getPassword());
        iqAuthb.setResource("sendit");
        try{
           iqb.addExtension(iqAuthb.build());
           iq = (InfoQuery)iqb.build();  
        }
        catch(InstantiationException e){
            ////System.out.println("*** ERROR CARGANDO LA CLASE ***: " + e.getMessage());
        }
        cb.send(iq);
    }
    
    //ENVIA EL MENSAJE CONTENIDO EN BODY
    public void sendMessage(){
        load();
        authenticate();
        mb = new MessageBuilder();
        int i = 4;
        try{
            for (i = 4; i<= direcciones.size(); i++){
                mb.setToAddress(new JID(direcciones.getProperty(Integer.toString(i)), getServer(), null));
                mb.setBody(getBody());
                msg=(Message)mb.build();
                cb.send(msg);
            }
        }
        catch(InstantiationException e){
            ////System.out.println("ERROR CREANDO MENSAJE");
        }
        //disconnect();
    }
    
    //ENVIA EL MENSAJE CONTENIDO EN BODY AL OPERADOR DE LA ZONA ESPECIFICADA 
    public void sendMessageZona(String zona){
        load();
        authenticate();
        mb = new MessageBuilder();
        try{
            if (zona.equals("001")){          
                mb.setToAddress(new JID(direcciones.getProperty("6"), getServer(), null));
                mb.setBody(getBody());
                msg=(Message)mb.build();
                cb.send(msg);
            }
            if (zona.equals("002")){          
                mb.setToAddress(new JID(direcciones.getProperty("8"), getServer(), null));
                mb.setBody(getBody());
                msg=(Message)mb.build();
                cb.send(msg);
            }
            if (zona.equals("003")){          
                mb.setToAddress(new JID(direcciones.getProperty("9"), getServer(), null));
                mb.setBody(getBody());
                msg=(Message)mb.build();
                cb.send(msg);
            }
            if (zona.equals("004")){          
                mb.setToAddress(new JID(direcciones.getProperty("7"), getServer(), null));
                mb.setBody(getBody());
                msg=(Message)mb.build();
                cb.send(msg);
            }
        }
        catch(InstantiationException e){
            ////System.out.println("ERROR CREANDO MENSAJE");
        }
        //disconnect();
    }
    
    private void disconnect(){
        cb.disconnect();
    }
    /**
     * Getter for property body.
     * @return Value of property body.
     */
    public java.lang.String getBody() {
        return body;
    }    
    
    /**
     * Setter for property body.
     * @param body New value of property body.
     */
    public void setBody(java.lang.String body) {
        this.body = body;
    }
    
    /**
     * Getter for property adressees.
     * @return Value of property adressees.
     */
    public java.util.Enumeration getAdressees() {
        return adressees;
    }
    
    /**
     * Setter for property adressees.
     * @param adressees New value of property adressees.
     */
    public void setAdressees(java.util.Enumeration adressees) {
        this.adressees = adressees;
    }
    
}
