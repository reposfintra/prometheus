/*
* Nombre        DesactivadorDeUsuarios.java
* Descripción   Clase que inicia el proceso de desactivación de usuarios por inactividad en el sistema.
* Autor         Alejandro Payares
* Fecha         11 de enero de 2006, 02:50 PM
* Versión       1.0
* Coyright      Transportes Sanchez Polo S.A.
*/

package com.tsp.util.user_unactivator;


import com.tsp.util.PropertyReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.DriverManager;

import java.io.*;

/**
 * Clase que inicia el proceso de desactivación de usuarios por inactividad
 * en el sistema.
 * @author  Alejandro Payares
 */
public class DesactivadorDeUsuarios {
    
    /** Creates a new instance of DesactivadorDeUsuarios */
    public DesactivadorDeUsuarios() {
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        PrintWriter logger = null;
        try {
            // creamos el logger
            // cargamos el archivo de configuración
            PropertyReader pr = PropertyReader.obtenerInstancia(DesactivadorDeUsuarios.class.getResource("config.properties"));
            File localHost = new File(pr.obtenerTexto("localLogPath"));
            File webServer = new File(pr.obtenerTexto("webServerLogPath"));
            if( localHost.exists() ){// si la ruta local existe
                logger = new PrintWriter(
                new BufferedWriter(
                new FileWriter(localHost.getPath() + "/" + "sot.log", true)
                ),
                true );
                ////System.out.println("INICIO DEL PROCESO DE INACTIVACION DE USUARIOS");
            }
            else if( webServer.exists() ){// si la ruta del servidor existe
                logger = new PrintWriter(
                new BufferedWriter(
                new FileWriter(webServer.getPath() + "/" + "sot.log", true)
                ),
                true );
                ////System.out.println("INICIO DEL PROCESO DE INACTIVACION DE USUARIOS");
            }
            else {// sino entonces imprimimos los detalles por la consola
                logger = new PrintWriter(System.out);
            }
            
            logger.println("INICIO DEL PROCESO DE INACTIVACION DE USUARIOS");
            long inicio = System.currentTimeMillis();
            // nos conectamos a la bd
            Class.forName(pr.obtenerTexto("driverClass"));
            con = DriverManager.getConnection(  pr.obtenerTexto("db_url"),
                                                pr.obtenerTexto("db_user"), 
                                                pr.obtenerTexto("db_pass")
                                                );
            con.setAutoCommit( true );
            ps = con.prepareStatement("SELECT inactivar_usuarios() AS respuesta");
            ResultSet rs = ps.executeQuery();
            if ( rs.next() ) {// procesamos la respuesta de la función
                String res = rs.getString("respuesta");
                //la respuesta viene en el siguiente formato: 
                // numeroDeUsuariosInactivados:lista de id's de usuarios desactivados separados por coma
                int indice2puntos = res.indexOf(':');
                String numeroDeUsuarios = res.substring(0,indice2puntos);
                if ( !numeroDeUsuarios.equals("0") ) {
                    String listaDeUsuarios [] = res.substring(indice2puntos + 1, res.length() -1 ).split(",");
                    for( int i=0; i< listaDeUsuarios.length; i++ ){
                        logger.println("Usuario "+(i+1)+" de "+numeroDeUsuarios+" inactivados: idusuario = "+listaDeUsuarios[i]);
                    }
                }
                else {
                    logger.println("NINGUN USUARIO FUE INACTIVADO POR EL PROCESO");
                }
                ps.close();
                rs.close();
                ps = con.prepareStatement("SELECT TO_CHAR(now(),'Day, month DD of YYYY - HH24:MI:SS.MS') as hoy");
                rs = ps.executeQuery();
                rs.next();
                logger.println("FECHA Y HORA DE INACTIVACION DE LOS "+numeroDeUsuarios+" USUARIOS: "+rs.getString("hoy"));
                logger.println("TIEMPO EMPLEADO EN EL PROCESO: "+(System.currentTimeMillis()-inicio)+" milisegundos");
                ////System.out.println("PROCESO TERMINADO. REVISE EL LOG PARA VER LOS DETALLES DEL PROCESO");
                ////System.out.println("que rayos pasa aqui?");
            }
            else {
                logger.println("NO SE PUDO TERMINAR EL PROCESO DE INACTIVACION DE USUARIOS");
            }
        }
        catch( Exception ex ){
            if ( logger != null ) {
                logger.println("ERROR EN EL PROCESO DE INACTIVACION DE USUARIOS");
                StackTraceElement vec [] = ex.getStackTrace();
                for( int i=0; i< vec.length; i++ ){
                    logger.println(vec[i]);
                }
            }
            ex.printStackTrace();
        }
        finally {
            if ( ps != null ) {ps.close();}
            if ( con != null ) {con.close();}
            if ( logger != null ) {logger.close();}
        }
    }
    
    
    
}




