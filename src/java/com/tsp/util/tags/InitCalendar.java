/*
 * InitCalendar.java
 *
 * Created on 8 de noviembre de 2005, 09:17 AM
 */

package com.tsp.util.tags;

import java.io.IOException;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.BodyContent;
import javax.servlet.jsp.tagext.BodyTagSupport;
import javax.servlet.http.*;
import java.util.LinkedList;

/**
 * Generated tag handler class.
 * @author  Alejandro
 * @version
 */

public class InitCalendar extends BodyTagSupport {
    
    /**
     * Initialization of rutaTema property.
     */
    private java.lang.String rutaTema;
    
    /**
     * Initialization of listaDeJavaScript property.
     */
    private java.lang.Object listaDeJavaScript;
    
    private String [] archivosJS;
    
    /** Creates new instance of tag handler */
    public InitCalendar() {
        super();
    }
    
    
    
    /**
     * This method is called after the JSP engine finished processing the tag.
     * @return EVAL_PAGE if the JSP engine should continue evaluating the JSP page, otherwise return SKIP_PAGE.
     * This method is automatically generated. Do not modify this method.
     * Instead, modify the methods that this method calls.
     */
    public int doEndTag() throws JspException {
        JspWriter out = this.pageContext.getOut();
        try {
            String contexto = ((HttpServletRequest)this.pageContext.getRequest()).getContextPath()+"/";
            LinkedList lista = (LinkedList) this.pageContext.getAttribute("listaDeId");
            ////System.out.println("InitCalendar: Tama�o de la lista = "+lista.size());
            for( int i=0; i<lista.size(); i+=2 ){
                out.println("<iframe width=188 height=166 name='gToday:normal:agenda.js:"+lista.get(i)+":"+lista.get(i+1)+"' id='gToday:normal:agenda.js:"+lista.get(i)+":"+lista.get(i+1)+"' src='"+contexto+"js/Calendario/ipopeng.htm' scrolling='no' frameborder='0' style='visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;'> </iframe>");
            }
            /*out.println("<link href='"+contexto+(this.rutaTema != null?rutaTema:"js/jscalendar/skins/aqua/theme.css")+"' rel='stylesheet' type='text/css'>");
            if ( this.listaDeJavaScript != null ){
                for(int i=0; i<this.archivosJS.length; i++){
                    out.println("<script type='text/javascript' src='"+archivosJS[i]+"'></script>");
                }
            } else {
                out.println("<script type='text/javascript' src='"+contexto+"js/jscalendar/calendar.js'></script>");
                out.println("<script type='text/javascript' src='"+contexto+"js/jscalendar/lang/calendar-es.js'></script>");
                out.println("<script type='text/javascript' src='"+contexto+"js/jscalendar/calendar-setup.js'></script>");
            }
            this.pageContext.setAttribute("fueInicializadoElCalendar","true");*/
        } finally {
            return EVAL_PAGE;
        }
    }
    
    
    /**
     * Setter for the rutaTema attribute.
     */
    public void setRutaTema(java.lang.String value) {
        this.rutaTema = value;
    }
    
    /**
     * Setter for the listaDeJavaScript attribute.
     */
    public void setListaDeJavaScript(java.lang.Object value) throws JspException{
        if ( value instanceof String ){
            this.archivosJS = ((String)value).split(",");
        } else if ( value instanceof String[] ){
            this.archivosJS = (String[]) value;
        } else if ( value instanceof java.util.AbstractCollection ){
            Object [] datos = ((java.util.AbstractCollection)value).toArray();
            this.archivosJS = new String[datos.length];
            for(int i=0; i<datos.length; i++){
                this.archivosJS[i] = datos[i].toString();
            }
        } else if ( value instanceof java.util.Map ){
            java.util.Collection values = ((java.util.Map)value).values();
            java.util.Iterator ite = values.iterator();
            this.archivosJS = new String[values.size()];
            int i=0;
            while(ite.hasNext()){
                this.archivosJS[i] = ite.next().toString();
                i++;
            }
        } else {
            throw new JspException("La lista de Archivos java script debe ser una lista con las rutas de los archivos JavaScript necesarios para que se ejecute el calendar, esta lista puede ser una instacia de String (separados por comas), String[], java.util.AbstractCollection, java.util.Map");
        }
        this.listaDeJavaScript = value;
    }
    
}
