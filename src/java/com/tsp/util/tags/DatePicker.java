/*
 * DatePicker.java
 *
 * Created on 8 de noviembre de 2005, 08:54 AM
 */

package com.tsp.util.tags;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.BodyTagSupport;
import javax.servlet.http.*;
import java.util.LinkedList;

import org.apache.taglibs.input.*;
import javax.servlet.jsp.tagext.Tag;
import javax.servlet.jsp.tagext.TagSupport;
import javax.servlet.jsp.JspTagException;

/**
 * Generated tag handler class.
 * @author  Alejandro Payares
 * @version
 */

public class DatePicker extends BodyTagSupport {
    
    /**
     * Initialization of name property.
     */
    private java.lang.String name;
    
    /**
     * Initialization of id property.
     */
    private java.lang.String id;
    
    /**
     * Initialization of class property.
     */
    private java.lang.String clase;
    
    
    /**
     * Initialization of otros property.
     */
    private java.lang.String otros;
    
    /**
     * Initialization of fechaInicial property.
     */
    private java.lang.String fechaInicial;
    
    /**
     * Initialization of showTime property.
     */
    private java.lang.Boolean showTime = new Boolean(false);
    
    /**
     * Initialization of timeFormat property.
     */
    private java.lang.String timeFormat;
    
    /** Creates new instance of tag handler */
    public DatePicker() {
        super();
    }
    
    
    ////////////////////////////////////////////////////////////////
    ///                                                          ///
    ///   Tag Handler interface methods.                         ///
    ///                                                          ///
    ///   Do not modify these methods; instead, modify the       ///
    ///   methods that they call.                                ///
    ///                                                          ///
    ////////////////////////////////////////////////////////////////
    
    private String today(){
        java.util.Calendar c = new java.util.GregorianCalendar();
        return c.get(c.YEAR) + "-" + (c.get(c.MONTH)+1) + "-" + c.get(c.DAY_OF_MONTH);
    }
    
    /**
     * This method is called after the JSP engine finished processing the tag.
     * @return EVAL_PAGE if the JSP engine should continue evaluating the JSP page, otherwise return SKIP_PAGE.
     * This method is automatically generated. Do not modify this method.
     * Instead, modify the methods that this method calls.
     */
    public int doEndTag() throws JspException {
        /*if ( this.pageContext.getAttribute("fueInicializadoElCalendar") == null ){
            throw new JspException("Debe incluir el tag InitCalendar antes de llamar a un tag Date, el tag InitCalendar puede ir ubicado dentro del tag <head> de esta pagina");
        }*/
        JspWriter out = this.pageContext.getOut();
        try {
            
            String contexto = ((HttpServletRequest)this.pageContext.getRequest()).getContextPath();
            String idCampo = (id != null?id:name);
            out.println("<input value='"+inicializarValue()+"' type='text' name='"+name+"' class='"+(clase != null?clase:"textbox")+"' id='"+idCampo+"' "+(otros != null?otros:"")+" readonly/>");
            String idAux = name+(id != null?id:name);
            out.println("<a href='javascript:void(0)' onclick=\"if(self.funcion"+idCampo+")funcion"+idCampo+".fPopCalendar(document."+formulario+"."+idCampo+");return false;\" HIDEFOCUS>");
            out.println("        <img src='"+contexto+"/"+(imagenBoton!=null?imagenBoton:"images/cal.gif")+"' width='16' height='16' border='0' alt='Haga click aqui para escoger la fecha'>");
            out.println("</a>");
            fechaInicial = null;
            LinkedList lista;
            if ( this.pageContext.getAttribute("listaDeId") == null ){
                lista = new LinkedList();
                this.pageContext.setAttribute("listaDeId",lista);
            }
            else {
                lista = (LinkedList) this.pageContext.getAttribute("listaDeId");
            }
            lista.add("funcion"+idCampo);
            if ( showTime.booleanValue() ){
                ////System.out.println("tratando de mostrar la hora de una fecha en formato "+timeFormat);
                if ( timeFormat == null ) {
                    throw new JspException("Cuando en el atributo showTime tiene valor 'true' debe espec�ficar un timeFormat que puede ser igual a 12 (para hora en formato AM PM o 24 para formato de 24 horas");
                }
                lista.add("plugins_"+timeFormat+".js");
            }
            else {
                lista.add("plugins.js");
            }
            /*out.println("<img style='cursor:hand' name='popcal"+idAux+"' id='popcal"+idAux+"' align='"+(align != null?align:"absmiddle")+"' src='"+contexto+"/"+(imagenBoton!=null?imagenBoton:"images/cal.gif")+"'>");
            out.println("<script type='text/javascript'>");
            out.println("Calendar.setup(");
            out.println("{");
            out.println("inputField : '"+(id != null?id:name)+"', // ID of the input field");
            if ( showTime.booleanValue() && ifFormat == null ){
                out.println("ifFormat : '%Y-%m-%d %H:%M', // the date format");
            }
            else {
                out.println("ifFormat : '" + ( ifFormat != null? ifFormat : "%Y-%m-%d" ) + "', // the date format");
            }
            out.println("button : 'popcal"+idAux+"', // ID of the button");
            out.println("showsTime : '"+showTime+"', // mostrar hora");
            out.println("timeFormat : '"+(timeFormat  != null?timeFormat:"24")+"' // tipo de hora 24 | 12");
            out.println("}");
            out.println(");");
            out.println("</script>");*/
        } finally {
            return EVAL_PAGE;
        }
    }
    
    private String inicializarValue() throws JspException{
        if ( fechaInicial == null ){
            String beanId = defaultFormBeanId(this);
            String beanValue = (beanId != null ? beanPropertyValue(
            pageContext.findAttribute(beanId), name) : null);
            fechaInicial = beanValue;
            return fechaInicial == null?"":fechaInicial;
        }
        else if ( fechaInicial.equals("hoy") || fechaInicial.equals("today") ){
            return today();
        }
        try {
            java.util.Calendar c = new java.util.GregorianCalendar();
            String [] tokens = fechaInicial.split(" ");
            if ( tokens.length == 4 ){
                String signo = tokens[1];
                int cantidad = Integer.parseInt(tokens[2]);
                String unidad = tokens[3];
                if ( signo.equals("+") ){
                    if ( unidad.equalsIgnoreCase("dias") || unidad.equalsIgnoreCase("dia") ){
                        c.add(c.DAY_OF_MONTH, cantidad);
                    }
                    else if ( unidad.equalsIgnoreCase("a�os")|| unidad.equalsIgnoreCase("a�o")){
                        c.add(c.YEAR, cantidad);
                    }
                }
                else if ( signo.equals("-") ){
                    if ( unidad.equalsIgnoreCase("dias") || unidad.equalsIgnoreCase("dia") ){
                        c.add(c.DAY_OF_MONTH, cantidad*-1);
                    }
                    else if ( unidad.equalsIgnoreCase("a�os")|| unidad.equalsIgnoreCase("a�o")){
                        c.add(c.YEAR, cantidad*-1);
                    }
                }
                return c.get(c.YEAR) + "-" + (c.get(c.MONTH)+1) + "-" + c.get(c.DAY_OF_MONTH);
            }
            return fechaInicial;
        }
        catch(Exception ex){
            return fechaInicial;
        }
    }
    
    /**
     * Initialization of imagenBoton property.
     */
    private java.lang.String imagenBoton;
    
    /**
     * Initialization of align property.
     */
    private java.lang.String align;
    
    /**
     * Holds value of property formulario.
     */
    private String formulario;
    
    /**
     * Setter for the name attribute.
     */
    public void setName(java.lang.String value) {
        this.name = value;
    }
    
    /**
     * Setter for the id attribute.
     */
    public void setId(java.lang.String value) {
        this.id = value;
    }
    
    /**
     * Setter for the class attribute.
     */
    public void setClase(java.lang.String value) {
        this.clase = value;
    }
    
    /**
     * Setter for the imagenBoton attribute.
     */
    public void setImagenBoton(java.lang.String value) {
        this.imagenBoton = value;
    }
    
    /**
     * Setter for the align attribute.
     */
    public void setAlign(java.lang.String value) {
        this.align = value;
    }
    
    
    /**
     * Setter for the align attribute.
     */
    public void setOtros(java.lang.String value) {
        this.otros = value;
    }
    
    /**
     * Setter for the fechaInicial attribute.
     */
    public void setFechaInicial(java.lang.String value) {
        this.fechaInicial = value;
    }
    
    /**
     * Setter for the fechaInicial attribute.
     */
    public void setShowTime(java.lang.Boolean value) {
        this.showTime = value;
    }
    
    /**
     * Setter for the fechaInicial attribute.
     */
    public void setTimeFormat(java.lang.String value) {
        this.timeFormat = value;
    }
    
    /**
     * Setter for property formulario.
     * @param formulario New value of property formulario.
     */
    public void setFormulario(String formulario) {
        this.formulario = formulario;
    }
    
    /**
     * Finds the input:form tag enclosing the given tag and returns it.
     */
    public Form findFormTag(Tag tag) {
        Tag formTag = TagSupport.findAncestorWithClass(tag, Form.class);
        if (formTag != null) {
            return (Form) formTag;
        } else {
            return null;
        }
    }
    
    /**
     * Finds the input:form tag enclosing the given tag and returns the "bean"
     * property from it, that is the default bean for this form, possibly null.
     */
    public String defaultFormBeanId(Tag tag) {
        Form form = findFormTag(tag);
        if (form != null) {
            return form.getBean();
        } else {
            return null;
        }
    }
    
    /**
     * Gets a named property from a JavaBean and returns its value as a String,
     * possibly null.
     */
    public String beanPropertyValue(Object bean, String name)
    throws JspTagException {
        Object value = beanPropertyValueObject(bean, name);
        return (value != null ? value.toString() : null);
    }
    
    /**
     * Gets a named property from a JavaBean and returns its value as an Object,
     * possibly null.
     */
    public Object beanPropertyValueObject(Object bean, String name)
    throws JspTagException {
        if (bean != null) {
            Method reader = null;
            Object[] params = null;
            
            // Try to find a reader method for the named property
            try {
                PropertyDescriptor prop = new PropertyDescriptor(name, bean
                .getClass());
                reader = prop.getReadMethod();
            } catch (IntrospectionException e) {
                // No property exists with that name, try a generic get method
                // Object get( Object key )
                try {
                    reader = bean.getClass().getMethod("get",
                    new Class[] { Object.class });
                    params = new Object[] { name };
                } catch (NoSuchMethodException f) {
                    // Try an Object get( String key) method
                    try {
                        reader = bean.getClass().getMethod("get",
                        new Class[] { String.class });
                        params = new Object[] { name };
                    } catch (NoSuchMethodException g) {
                        // Give up
                    }
                }
            }
            
            // If a reader method has been found
            if (reader != null) {
                try {
                    return reader.invoke(bean, params);
                } catch (IllegalAccessException e) {
                } catch (IllegalArgumentException e) {
                } catch (InvocationTargetException e) {
                    throw new JspTagException("Exception getting property \""
                    + name + "\" from bean "
                    + bean.getClass().getName() + ": "
                    + e.getTargetException());
                }
            }
        }
        
        return null;
    }
    
}
