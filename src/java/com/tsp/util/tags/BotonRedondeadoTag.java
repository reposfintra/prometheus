/*
 * BotonRedondeadoTag.java
 *
 * Created on 21 de octubre de 2005, 03:11 PM
 */

package com.tsp.util.tags;

import javax.servlet.jsp.tagext.*;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.PageContext;
import javax.servlet.http.HttpServletRequest;
import java.util.Hashtable;
import com.tsp.util.PropertyReader;

/**
 *
 * @author  Alejandro Payares
 * @version 1.0
 */

public class BotonRedondeadoTag extends BodyTagSupport {
    
    /**
     * Initialization of value property.
     */
    private java.lang.String value;
    
    /**
     * Initialization of name property.
     */
    private java.lang.String name;
    
    /**
     * Initialization of type property.
     */
    private java.lang.String type = "button";
    
    /**
     * Initialization of id property.
     */
    private java.lang.String id;
    
    /**
     * Initialization of onclick property.
     */
    private java.lang.String onclick;
    
    /**
     * Initialization of otrosAtributos property.
     */
    private java.lang.String otrosAtributos;
    
    /**
     * Initialization of rutaImagenes property.
     */
    private java.lang.String rutaImagenes;
    
    /**
     * Initialization of width property.
     */
    private java.lang.Integer width;
    
    /**
     * Initialization of height property.
     */
    private java.lang.Integer height;
    
    private PropertyReader pr;
    
    private String [] tipos;
    
    
    private void inicializarImagenes(){
        String aux = pr.obtenerTexto("tipos");
        tipos = aux.split(",");
    }
    
    private boolean estaElValue(){
        for(int i=0; i<tipos.length; i++){
            if ( value.equals(tipos[i]) ){
                return true;
            }
        }
        return false;
    }
    
    /**Called by the container to invoke this tag.
     * The implementation of this method is provided by the tag library developer,
     * and handles all tag processing, body iteration, etc.
     */
    public int doEndTag() throws JspException {
        
        JspWriter out=pageContext.getOut();
        try {
            pr = PropertyReader.obtenerInstancia(pageContext.getServletContext().getRealPath("/WEB-INF/classes/com/tsp/util/recursos/botones.properties"));
            inicializarImagenes();
            if ( !estaElValue() ){
                throw new JspException("el valor del atributo value debe ser uno de los siguientes: "+pr.obtenerTexto("tipos"));
            }
            if ( rutaImagenes == null ){
                rutaImagenes = ((HttpServletRequest)((PageContext)pageContext).getRequest()).getContextPath()+pr.obtenerTexto("rutaImagenes")+(type!=null && type.equals("mini")?"iconos/":"");
            } else {
                rutaImagenes = rutaImagenes.charAt(rutaImagenes.length()-1) == '/'?rutaImagenes:rutaImagenes+"/";
            }
            out.println(crearBoton());
            return EVAL_PAGE;
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new JspException(ex);
        }
        
    }
    
    private String crearBoton(){
        StringBuffer sb = new StringBuffer("<img ");
        Object [] valoresDeAtributos;
        String prefijoImagen = rutaImagenes+value;
        String extension = pr.obtenerTexto("extension");
        if ( disabled != null && disabled.booleanValue() ){
            valoresDeAtributos = new Object [] {
                    "src",prefijoImagen+pr.obtenerTexto("disabled")+"."+extension,
                    "name",name,
                    "width",width,
                    "height",height,
                    "style",style
            };
        } else {
            valoresDeAtributos = new Object [] {"src",prefijoImagen+"."+extension,
                    "name",name,
                    "width",width,
                    "height",height,
                    "style",style,
                    "onclick",type!= null && type.equals("reset")?"document.forms[0].reset();":onclick,
                    "onmouseover","this.src='"+prefijoImagen+pr.obtenerTexto("over")+"."+extension+"'",
                    "onmouseout","this.src='"+prefijoImagen+"."+extension+"'"};
        }
        for(int i=0; i < valoresDeAtributos.length; i+=2 ){
            if ( valoresDeAtributos[i+1] != null ){
                sb.append(" " + valoresDeAtributos[i] + " = \"" + valoresDeAtributos[i+1] + "\"");
            }
        }
        if ( otrosAtributos != null ){
            sb.append(" "+otrosAtributos);
        }
        sb.append(">");
        ////System.out.println(sb);
        return sb.toString();
    }
    
    /**
     * Setter for the value attribute.
     */
    public void setValue(java.lang.String value) {
        this.value = value;
    }
    
    /**
     * Setter for the name attribute.
     */
    public void setName(java.lang.String value) {
        this.name = value;
    }
    
    /**
     * Setter for the type attribute.
     */
    public void setType(java.lang.String value) {
        this.type = value;
    }
    
    /**
     * Setter for the id attribute.
     */
    public void setId(java.lang.String value) {
        this.id = value;
    }
    
    /**
     * Setter for the onclick attribute.
     */
    public void setOnclick(java.lang.String value) {
        this.onclick = value;
    }
    
    /**
     * Setter for the otrosAtributos attribute.
     */
    public void setOtrosAtributos(java.lang.String value) {
        this.otrosAtributos = value;
    }
    
    /**
     * Setter for the rutaImagenes attribute.
     */
    public void setRutaImagenes(java.lang.String value) {
        this.rutaImagenes = value;
    }
    
    /**
     * Setter for the width attribute.
     */
    public void setWidth(java.lang.Integer value) {
        this.width = value;
    }
    
    /**
     * Setter for the height attribute.
     */
    public void setHeight(java.lang.Integer value) {
        this.height = value;
    }
    
    /**
     * Holds value of property disabled.
     */
    private java.lang.Boolean disabled;
    
    /**
     * Setter for property disabled.
     * @param disabled New value of property disabled.
     */
    public void setDisabled(java.lang.Boolean disabled) {
        this.disabled = disabled;
    }

    /**
     * Holds value of property style.
     */
    private String style = "cursor:hand";

    /**
     * Setter for property style.
     * @param style New value of property style.
     */
    public void setStyle(String style) {

        this.style = style;
    }
}
