/*
 * TimeoutReader.java
 *
 * Created on 31 de octubre de 2005, 11:43 AM
 */

package com.tsp.util.tags;

import java.io.*;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.BodyContent;
import javax.servlet.jsp.tagext.BodyTagSupport;

/**
 * Generated tag handler class.
 * @author  Alejandro
 * @version
 */

public class TimeoutReader extends BodyTagSupport {
    
    /**
     * Initialization of var property.
     */
    private java.lang.String var;
    
    /** Creates new instance of tag handler */
    public TimeoutReader() {
        super();
    }
    
    
    ////////////////////////////////////////////////////////////////
    ///                                                          ///
    ///   Tag Handler interface methods.                         ///
    ///                                                          ///
    ///   Do not modify these methods; instead, modify the       ///
    ///   methods that they call.                                ///
    ///                                                          ///
    ////////////////////////////////////////////////////////////////
    
    
    
    /**
     * This method is called after the JSP engine finished processing the tag.
     * @return EVAL_PAGE if the JSP engine should continue evaluating the JSP page, otherwise return SKIP_PAGE.
     * This method is automatically generated. Do not modify this method.
     * Instead, modify the methods that this method calls.
     */
    public int doEndTag() throws JspException, JspException {
        try {
            int timeout = leerSessionTimeout(pageContext.getServletContext().getRealPath("/WEB-INF/web.xml"));
            if ( toMiliseconds != null && toMiliseconds.booleanValue() ){
                timeout *= 60000;
            }
            if ( var != null ){
                pageContext.setAttribute(var,""+timeout);
            }
            else {
                JspWriter out = this.pageContext.getOut();
                out.print(timeout);
            }
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
        return SKIP_BODY;
    }
    
    private int leerSessionTimeout(String archivo){
        try {
            ////System.out.println("leyendo archivo: "+archivo);
            File f = new File(archivo);
            FileInputStream fis = new FileInputStream(f);
            LineNumberReader reader = new LineNumberReader(new InputStreamReader(fis));
            String linea = "";
            StringBuffer tagSessionTimeOut = null;
            while((linea = reader.readLine()) != null ){
                if ( linea.indexOf("<session-timeout>") != -1 ){
                    tagSessionTimeOut = new StringBuffer();
                }
                if ( tagSessionTimeOut != null ){
                    tagSessionTimeOut.append(linea);
                }
                if ( linea.indexOf("</session-timeout>") != -1){
                    break;
                }
                
            }
            if ( tagSessionTimeOut != null ){
                int inicioTag = tagSessionTimeOut.toString().indexOf(">")+1;
		int finTag = tagSessionTimeOut.indexOf("</session-timeout>");
                String valor = tagSessionTimeOut.substring(inicioTag,finTag);
                return Integer.parseInt(valor.trim());
            }            
        }catch(Exception ex){
            ex.printStackTrace();
        }
        return 30;
    }
    
    /**
     * Setter for the var attribute.
     */
    public void setVar(java.lang.String value) {
        this.var = value;
    }

    /**
     * Holds value of property toMiliseconds.
     */
    private Boolean toMiliseconds;

    /**
     * Setter for property toMiliseconds.
     * @param toMiliseconds New value of property toMiliseconds.
     */
    public void setToMiliseconds(Boolean toMiliseconds) {

        this.toMiliseconds = toMiliseconds;
    }
    
}
