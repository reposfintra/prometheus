/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.util;

import com.tsp.operation.model.LogProcesosService;
import com.tsp.operation.model.beans.Usuario;
import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mcamargo
 */
public class EnviarSms implements Runnable {

      private final String lote;
      private final Usuario   user;
     /**
     *
     * @param lote
     * @param usuario
     */
    public EnviarSms(String lote, Usuario usuario) {
        this.lote =lote;
        this.user=usuario;
    }
    
    
    
    @Override
    public void run() {
        LogProcesosService log = new LogProcesosService(user.getBd());
       try {
           log.InsertProceso("Envio extractos digitales por sms", this.hashCode(), "Proceso envio de extractos por mensaje de texto" ,this.user.getLogin() ); 
            String result= sendSmsExtracto(this.lote);         
            Thread.sleep(10000);
            log.finallyProceso("Envio extractos digitales por sms", this.hashCode(), this.user.getLogin(),result); 
        } catch (InterruptedException ex) {
          Logger.getLogger(EnviarSms.class.getName()).log(Level.SEVERE, null, ex);
        }catch(Exception e){
          Logger.getLogger(EnviarSms.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    private String sendSmsExtracto(String lote) {
        HttpURLConnection conn = null;
        String body;
        try {
                URL url = new URL("http://zeus.fintra.co:3000/sms/pdf");
                conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Content-Type", "application/json");
                body = "{\"lote\": \"" + lote + "\"}";
                conn.setDoOutput(true);
                OutputStream os = conn.getOutputStream();
                os.write(body.getBytes());
                os.flush();
                if (conn.getResponseCode() != 200) {
                    return "Error envio extractos: "+conn.getResponseCode();
                }
               
            } catch (IOException ex) {
              Logger.getLogger(EnviarSms.class.getName()).log(Level.SEVERE, null, ex);
              return ex.toString();
            }
          return "Proceso Envio de extractos por sms iniciado";
    }
    
}
