/*
 * RemPlaDAO.java
 *
 * Created on 23 de noviembre de 2004, 10:24 AM
 */

package com.tsp.operation.model;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;
import com.tsp.operation.model.DAOS.MainDAO;
import org.apache.log4j.Logger;
/**
 *
 * @author  KREALES
 */
public class RemPlaDAO extends MainDAO {
    
    static Logger logger = Logger.getLogger(RemPlaDAO.class);
    /** Creates a new instance of RemPlaDAO */
    private RemPla rempla;
    private String SQL_ESTAREMPLA="SELECT * FROM plarem where numpla=? and numrem=? and reg_status<>'A' ";
    private String SQL_ESTAREMPLA_SINREG="SELECT * FROM plarem where numpla=? and numrem=?";
    private String SQL_INSERT="insert into plarem (numrem,numpla,cia,base,feccum,porcent,Account_code_c,Account_code_i) values(?,?,?,?,?,?,?,?)";
    private String SQL_UPDATE="update plarem set reg_status = '', porcent=? where numrem =? and numpla=? ";
    
    public RemPlaDAO() {
        super("RemplaDAO.xml");
    }
    
    public void setRemPla(RemPla rempla){
        this.rempla = rempla;
    }
    
    public String insertRemPla(String base)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        String sql = "";
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                boolean esta = false;
                st = con.prepareStatement(SQL_ESTAREMPLA_SINREG);
                st.setString(1,rempla.getPlanilla());
                st.setString(2,rempla.getRemesa());
                rs = st.executeQuery();
                if(rs.next()){
                    esta= true;
                }
                if(!esta){
                    st = con.prepareStatement(SQL_INSERT);
                    st.setString(1,rempla.getRemesa());
                    st.setString(2,rempla.getPlanilla());
                    st.setString(3,rempla.getDstrct());
                    st.setString(4,base);
                    st.setString(5,rempla.getFecha());
                    st.setFloat(6,rempla.getPorcent());
                    st.setString(7,rempla.getAccount_code_c());
                    st.setString(8,rempla.getAccount_code_i());
                    sql = st.toString();
                }
                else{
                    st = con.prepareStatement(SQL_UPDATE);
                    st.setFloat(1, rempla.getPorcent());
                    st.setString(2,rempla.getRemesa());
                    st.setString(3,rempla.getPlanilla());
                    sql = st.toString();
                }
                //st.executeUpdate();
                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA INSERCCION DE LAS PLANILLA-REMESA " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return sql;
    }
    public void searchRempla(String planilla, String remesa )throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        rempla= null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("SELECT * FROM plarem where numpla=? and numrem=?");
                st.setString(1,planilla);
                st.setString(2,remesa);
                rs = st.executeQuery();
                if(rs.next()){
                    rempla=RemPla.load(rs);
                    
                }
                
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LA PLKGRU " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
        
    }
    public void searchRempla(String planilla )throws SQLException{
        
        PreparedStatement st = null;
        ResultSet rs = null;
        rempla= null;
        
        try {
            
            st = this.crearPreparedStatement("SQL_BUSCARREMPLA");
            st.setString(1,planilla);
            
            rs = st.executeQuery();
            if(rs.next()){
                rempla=RemPla.load(rs);
                rempla.setRemesa(rs.getString("numrem"));
                
            }
            
            
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LA PLKGRU " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            
            if (st  != null) st.close();
            if (rs  != null) rs.close();
            this.desconectar("SQL_BUSCARREMPLA");
        }
        
        
    }
    public boolean estaRempla(String planilla, String remesa )throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw=false;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement(SQL_ESTAREMPLA);
                st.setString(1,planilla);
                st.setString(2,remesa);
                rs = st.executeQuery();
                
                if(rs.next()){
                    sw=true;
                    
                }
                
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LA PLKGRU " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return sw;
        
    }
    
    public boolean estaRemplaImpNoCump(String planilla, String remesa )throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw=false;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("SELECT pr.* FROM plarem pr, planilla pla, remesa rem where    pr.numpla=? and pr.numrem=?  and pla.numpla = pr.numpla and rem.numrem = pr.numrem and pla.printer_date!='0099-01-01 00:00:00' and rem.printer_date!='0099-01-01 00:00:00' and pr.feccum like '0%'");
                st.setString(1,planilla);
                st.setString(2,remesa);
                rs = st.executeQuery();
                
                if(rs.next()){
                    sw=true;
                    
                }
                
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LA PLKGRU " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return sw;
        
    }
    
    public boolean estaRemplaImp(String planilla, String remesa )throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw=false;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("SELECT pr.* FROM plarem pr, planilla pla, remesa rem where    pr.numpla=? and pr.numrem=?  and pla.numpla = pr.numpla and rem.numrem = pr.numrem and pla.printer_date!='0099-01-01 00:00:00' and rem.printer_date!='0099-01-01 00:00:00'");
                st.setString(1,planilla);
                st.setString(2,remesa);
                rs = st.executeQuery();
                
                if(rs.next()){
                    sw=true;
                    
                }
                
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LA PLKGRU " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return sw;
        
    }
    public void searchRemplaRem(String remesa )throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        rempla= null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("SELECT * FROM plarem where numrem=?");
                st.setString(1,remesa);
                rs = st.executeQuery();
                if(rs.next()){
                    rempla=RemPla.load(rs);
                    
                }
                
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LA PLKGRU " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
        
    }
    
    public RemPla getRemPla(){
        return this.rempla;
    }
    
    public String anularRemPla()throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        String sql = "";
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("update plarem set reg_status='A', porcent=0 where numpla=? and numrem=?");
                st.setString(1,rempla.getPlanilla());
                st.setString(2,rempla.getRemesa());
                
                sql = st.toString();
                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA ANULACION DE LAS PLANILLAS- REMESAS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return sql;
    }
    

    
    public String cumplirRemPla()throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        String sql="";
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("update plarem set feccum=?, quvrcm=?, observacion=? where numpla=? and numrem=?");
                st.setString(1,rempla.getFecha());
                st.setFloat(2,rempla.getQuemcm());
                st.setString(3,rempla.getObservacion());
                st.setString(4,rempla.getPlanilla());
                st.setString(5,rempla.getRemesa());
                
                //st.executeUpdate();
                sql = st.toString();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE EL CUMPLIDO DE LAS PLANILLAS- REMESAS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return sql;
    }
    
    public void anularRemPlaR()throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("update plarem set reg_status='A' where numrem=? and numpla=?");
                st.setString(1,rempla.getRemesa());
                st.setString(2,rempla.getPlanilla());
                
                st.executeUpdate();
                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA ANULACION DE LAS PLANILLAS- REMESAS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    public String updateRemPla()throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        String sql = "";
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("update plarem set porcent=? where numrem=? and numpla=?");
                st.setFloat(1,rempla.getPorcent());
                st.setString(2,rempla.getRemesa());
                st.setString(3,rempla.getPlanilla());
                //st.executeUpdate();
                sql = st.toString();
                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA ACTUALIZACION DE PORCENTAJES DE LAS PLANILLAS- REMESAS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return sql;
        
    }

    /**
     *Funcion para anular todas las remesas relacionadas a una planilla
     *@autor: Karen Reales
     *@param: numpla es el numreo de la planilla con la cual se relaciona la remesa
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public String anularListaPlanilla(String numpla)throws SQLException{
        
        PreparedStatement st = null;
        ResultSet rs = null;
        String sql = "";
        try {
            st = this.crearPreparedStatement("SQL_ANULARLISTAREM");
            st.setString(1,numpla);
            sql = st.toString();
            //st.executeUpdate();
            
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA ANULACION DE LAS PLANILLAS- REMESAS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            
            if (st  != null) st.close();
            if (rs  != null) rs.close();
            this.desconectar("SQL_ANULARLISTAREM");
        }
        return sql;
    }
}
