/*
 * PlanViajeService.java
 *
 * Created on 22 de noviembre de 2004, 03:47 PM
 *
 */
package com.tsp.operation.model;

import java.sql.*;
import com.tsp.operation.model.beans.*;
import java.util.*;
/**
 *
 * @author  AMENDEZ
 */
public class PlanViajeService {
    
    private PlanViajeDAO planViajeDataAccess;
    /** Creates a new instance of PlanViajeService */
    public PlanViajeService() {
        planViajeDataAccess = new PlanViajeDAO();
    }
    
    public boolean planillaExist(String distrito, String planilla) throws SQLException{
        return planViajeDataAccess.planillaExist(distrito, planilla);
    }
    
    public boolean planViajeExist(String distrito, String planilla) throws SQLException{
        return planViajeDataAccess.planViajeExist(distrito, planilla);
    }
    
    public void getPlanVjInfoNew(String distrito, String planilla) throws SQLException{
        planViajeDataAccess.getPlanVjInfoNew(distrito, planilla);
    }
    
    public void getPlanVjInfoOld(String distrito, String planilla) throws SQLException{
        planViajeDataAccess.getPlanVjInfoOld(distrito, planilla);
    }
    
    public void createPlanVj(PlanViaje planViaje) throws Exception{
        planViajeDataAccess.createPlanVj(planViaje);
    }
    
    public void updatePlanVj(PlanViaje planViaje) throws Exception{
        planViajeDataAccess.updatePlanVj(planViaje);
    }
    
    public PlanViaje getPlanViaje(){
        return planViajeDataAccess.getPlanViaje();
    }
    
    public void planViajeQryAg(String fechaini, String fechafin, String agencia, String dpto) throws SQLException{
        planViajeDataAccess.planViajeQryAg(fechaini, fechafin, agencia, dpto);
    }
    
    public void planViajeQryZn(String fechaini, String fechafin, String zona, String dpto) throws SQLException{
        planViajeDataAccess.planViajeQryZn(fechaini, fechafin, zona, dpto);
    }
    
    public List getPlanVjQrys(){
        return planViajeDataAccess.getPlanVjQrys();
    }
    
    public void regUserQry(String distrito, String planilla, String usuario) throws SQLException{
        planViajeDataAccess.regUserQry(distrito, planilla, usuario);
    }
    
    public void leer(String distrito, String planilla) throws SQLException{
        planViajeDataAccess.leer(distrito, planilla);
    }
    
    public void getQryUsers(String distrito, String planilla) throws SQLException{
        planViajeDataAccess.getQryUsers(distrito, planilla);
    }
    
    public TreeMap getCbxQryUsers() {
        return planViajeDataAccess.getCbxQryUsers();
    }
    
    public String getZona(String distrito, String planilla) throws SQLException{
        return planViajeDataAccess.getZona(distrito, planilla);
    }
    
     public boolean tieneMovimiento(String planilla) throws Exception{
        return planViajeDataAccess.tieneMovimientos(planilla);
    }
}
