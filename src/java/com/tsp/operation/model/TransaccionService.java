/*
 * TransaccionService.java
 *
 * Created on 21 de enero de 2005, 04:23 PM
 */

package com.tsp.operation.model;

import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author  KREALES
 */
public class TransaccionService {
    
    TransaccionDAO t;
    /** Creates a new instance of TransaccionService */
    public TransaccionService() {
        t = new TransaccionDAO();
    }
    public TransaccionService(String dataBaseName) {
        t = new TransaccionDAO(dataBaseName);
    }
        
    public Statement getSt(){
        return t.getSt();
    }
    public void crearStatement()throws SQLException{
        t.crearStatement();
    }
     public void execute()throws SQLException{
         t.execute();
     }
     public void closeAll () throws Exception{
        t.cerrarTodo();
    }
     
     
     public void crearStatement2()throws SQLException{
        t.crearStatement2();  
    }
     public void execute2()throws SQLException{
         t.execute2(); 
     }
     public void closeAll2 () throws Exception{
        t.cerrarTodo2(); 
    }
    
}
