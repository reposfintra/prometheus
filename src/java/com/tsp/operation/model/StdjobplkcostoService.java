/*
 * StdjobplkcostoService.java
 *
 * Created on 25 de noviembre de 2004, 11:34 AM
 */

package com.tsp.operation.model;

/**
 *
 * @author  KREALES
 */
import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;

public class StdjobplkcostoService {
    
    private StdjobplkcostoDAO std;
    
    /** Creates a new instance of StdjobplkcostoService */
    public StdjobplkcostoService() {
        std= new StdjobplkcostoDAO();
    }
    
    public Stdjobplkcosto getStd()throws SQLException{
        
        return std.getStd();
        
    }
    public void buscaStd(String group)throws SQLException{
        try{
            std.searchStdjobplkcosto(group);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    public Vector getGruposVec()throws SQLException{
        std.listar();
        return std.getGruposVec();
    }
    public void insert()throws SQLException{
        std.insert();
    }
    public void update()throws SQLException{
        std.update();
    }
    public void delete()throws SQLException{
        std.delete();
    }
    public void setStd(Stdjobplkcosto st){
        std.setStd(st);
    }
    public TreeMap listar()throws SQLException{
        std.listar();
        return std.getGrupos();
    }
    
    
}
