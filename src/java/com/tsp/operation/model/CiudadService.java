 /*
  * CiudadService.java
  *
  * Created on 1 de diciembre de 2004, 03:06 PM
  */

package com.tsp.operation.model;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
/**
 *
 * @author  KREALES
 */
public class CiudadService {
    private CiudadDAO ciudad;
    private CiudadDAO ciudad_d;
    private List listaC;    
    private TreeMap ciudadTM = new TreeMap();
    private TreeMap ciudadTC = new TreeMap();
    private List ciudadO;
    private List ciudadD;
    /** Creates a new instance of CiudadService */
    public CiudadService() {
        ciudad= new CiudadDAO();
        ciudad_d = new CiudadDAO();
    }
    
    public CiudadService(String dataBaseName) {
        ciudad= new CiudadDAO(dataBaseName);
        ciudad_d = new CiudadDAO(dataBaseName);
    }
    
    /**
     * Getter for property ciudades
     * @return Value of property ciudades
     */
    public TreeMap getCiudades()throws SQLException{
        TreeMap ciudades=null;
        ciudad.searchCiudades();
        ciudades = ciudad.getCiudades();
        return ciudades;
    }
    
    /**
     * Metodo <tt>existCiudades</tt>, verifica la existencia de las ciudades
     * @autor : Ing. Karen Reales
     * @return boolean
     * @see existCiudades()
     * @version : 1.0
     */
    public boolean existCiudades()throws SQLException{
        return ciudad.existCiudades();
    }
    
    /**
     * Metodo <tt>obtenerNombreCiudad</tt>, obtiene el nombre de una ciudad
     * @autor : Ing. Karen Reales         
     * @return String
     * @see obtenerNombreCiudad(String codigo)
     * @version : 1.0
     */
    public String obtenerNombreCiudad(String codigo) throws SQLException {
        return ciudad.obtenerNombreCiudad(codigo);
    }
    
    /**
     * Metodo <tt>insertarCiudad</tt>, registra una ciudad en el sistema
     * @autor : Ing. Diogenes Bastidas  
     * @see insertarCiudad(Ciudad ciudad)    
     * @version : 1.0
     */
    public void insertarCiudad(Ciudad ciudad)throws SQLException {
       try{
           ciudad_d.setCiudad(ciudad);
           ciudad_d.insertarCiudad();
       }
       catch(SQLException e){
           throw new SQLException(e.getMessage());
       }
    }
    
    /**
    * Metodo <tt>existeCiudad</tt>, verifica la existencia de una ciudad
    * @autor : Ing. Diogenes Bastidas     
    * @see existeCiudad (String cod_pais, String cod_depto, String codciu ) 
    * @return boolean
    * @version : 1.0
    */
    public boolean existeCiudad (String cod_pais, String cod_depto, String codciu ) throws SQLException {
       return ciudad_d.existeCiudad(cod_pais, cod_depto, codciu );
    }
    
    /**
    * Metodo <tt>existeCiudadAnulado</tt>, verifica la existencia de una ciudad anulada
    * @autor : Ing. Diogenes Bastidas     
    * @see existeCiudadAnulado (String cod_pais, String cod_depto, String codciu )
    * @return boolean
    * @version : 1.0
    */
    public boolean existeCiudadAnulado (String cod_pais, String cod_depto, String codciu ) throws SQLException {
       return ciudad_d.existeCiudadAnulado(cod_pais, cod_depto, codciu );
    }
    
    /**
    * Metodo <tt>existeCiudadnom</tt>, verifica la existencia de una ciudad dado el nombre
    * @autor : Ing. Diogenes Bastidas     
    * @see existeCiudadnom (String nom_pais, String nom_depto, String nomciu )
    * @version : 1.0
    */
    public boolean existeCiudadnom (String nom_pais, String nom_depto, String nomciu ) throws SQLException {
       return ciudad_d.existeCiudadnom(nom_pais, nom_depto, nomciu);
    } 
    
    /**
     * Getter for property Ciudades
     * @return Value of property Ciudades
     */
    public List obtenerCiudades() throws SQLException {
       List Ciudades = null;
       Ciudades = ciudad_d.obtenerCiudades();
       return Ciudades;
    }   
    
    /**
    * Metodo <tt>listarCiudad</tt>, obtiene la lista de las ciudades dado pais y estado
    * @autor : Ing. Diogenes Bastidas     
    * @see listarCiudad (String pais, String estado)
    * @version : 1.0
    */
    public void listarCiudad (String pais, String estado) throws SQLException {
       try{
           ciudad_d.listarCiudad(pais, estado);
       }
       catch (SQLException e){
           throw new SQLException (e.getMessage()); 
       }
    }
    
    /**
    * Metodo <tt>listarCiudadGeneral</tt>, obtiene la lista de las ciudades del sistema
    * @autor : Ing. Diogenes Bastidas 
    * @see listarCiudadGeneral()            
    * @version : 1.0
    */
    public void listarCiudadGeneral ( ) throws SQLException {
       try{
           ciudad_d.listarCiudadGeneral();
       }
       catch (SQLException e){
           throw new SQLException (e.getMessage()); 
       }
    }
    
    /**
     * Getter for property ciudad
     * @return Value of property ciudad
     */
    public Ciudad obtenerCiudad() throws SQLException{
       return ciudad_d.obtenerCiudad();
    }  
    
    /**
     * Getter for property VecCiudades
     * @return Value of property VecCiudades
     */
    public Vector obtCiudades() throws SQLException{
       return ciudad_d.obtCiudades();
    }   
    
    /**
    * Metodo <tt>buscarCiudad</tt>, obtiene la ciudad dado pais, estado, ciudad
    * @autor : Ing. Diogenes Bastidas             
    * @see buscarCiudad (String cod_pais, String cod_depto, String codciu )
    * @version : 1.0
    */
    public void buscarCiudad (String cod_pais, String cod_depto, String codciu ) throws SQLException {
       try{
           ciudad_d.buscarCiudad(cod_pais, cod_depto, codciu); 
       }
       catch(SQLException e){
           throw new SQLException ( e.getMessage()); 
       }       
    }
    
    /**
    * Metodo <tt>buscarCiudadesXnom</tt>, obtiene las ciudades dado unos parametros de busqueda
    * @autor : Ing. Diogenes Bastidas             
    * @see buscarCiudadesXnom (String nom_pais, String nom_depto, String nomciu )
    * @version : 1.0
    */
    public void buscarCiudadesXnom (String nom_pais, String nom_depto, String nomciu ) throws SQLException {
       try{
           ciudad_d.buscarCiudadesXnom(nom_pais, nom_depto, nomciu); 
       }
       catch(SQLException e){
           throw new SQLException ( e.getMessage()); 
       }       
    }
    
    /**
    * Metodo <tt>modificarCiudad</tt>, actualizar informacion de una ciudad
    * @autor : Ing. Diogenes Bastidas                 
    * @see modificarCiudad (Ciudad ciudad)
    * @version : 1.0
    */
    public void modificarCiudad (Ciudad ciudad, String antest) throws SQLException {
       try{
           ciudad_d.setCiudad(ciudad);
           ciudad_d.modificarCiudad(antest);
       }
       catch(SQLException e){
           throw new SQLException(e.getMessage());
       }
    }
    
    /**
    * Metodo <tt>activarCiudad</tt>, activa una ciudad antes anulada, actualiza el rec_status = ''
    * @autor : Ing. Diogenes Bastidas                 
    * @see activarCiudad (Ciudad ciudad)
    * @version : 1.0
    */
     public void activarCiudad (Ciudad ciudad) throws SQLException {
       try{
           ciudad_d.setCiudad(ciudad);
           ciudad_d.activarCiudad();
       }
       catch(SQLException e){
           throw new SQLException(e.getMessage());
       }
    }
    
    /**
    * Metodo <tt>anularCiudad</tt>, anular ciudad, actualiza el rec_status = 'A'
    * @autor : Ing. Diogenes Bastidas                 
    * @see anularCiudad(Ciudad ciudad)
    * @version : 1.0
    */ 
    public void anularCiudad(Ciudad ciudad)throws SQLException {
       try{
           ciudad_d.setCiudad(ciudad);
           ciudad_d.anularCiudad();
       }
       catch(SQLException e){
           throw new SQLException(e.getMessage());
       }
    }
    
    /**
    * Metodo <tt>existeCiudadesxEstado</tt>, verifica la existencia de una ciudad
    *   dado el pais y el estado
    * @autor : Ing. Diogenes Bastidas                 
    * @see existeCiudadesxEstado( String cpais, String cest )
    * @version : 1.0
    */
    public boolean existeCiudadesxEstado( String cpais, String cest ) throws SQLException {
       return ciudad_d.existeCiudadesxEstado( cpais, cest ); 
   }
   
   /**
    * Metodo <tt>listarCiudades</tt>, obtiene la lista de las ciudades dado pais y estado    
    * @autor : Ing. Diogenes Bastidas                 
    * @see listarCiudades (String pais, String estado)
    * @version : 1.0
    */
    public void listarCiudades (String pais, String estado) throws SQLException {
       try{
           ciudad_d.listarCiudades(pais, estado);
       }
       catch (SQLException e){
           throw new SQLException (e.getMessage()); 
       }
    }
    
    /**
    * Metodo <tt>listarCiudadesxpais</tt>, obtiene la lista de las ciudades dado pais    
    * @autor : Ing. Jesus Cuesta                 
    * @see listarCiudadesxpais (String pais)
    * @version : 1.0
    */
    public void listarCiudadesxpais (String pais)throws SQLException{
        ciudad_d.listarCiudadesxpais(pais);
    }
    public void listarCiudadesxpaisC (String pais)throws SQLException{
        ciudad_d.listarCiudadesxpaisC(pais);
    }
    
    /**
    * Metodo <tt>listarAgencias</tt>, obtiene las agencias registradas en agasoc
    * @autor : Ing. Juan Escandon    
    * @see ListarAgencias()
    * @return List
    * @version : 1.0
    */
    public List ListarAgencias() throws Exception {
        try{
           this.ReiniciarLista();
           this.listaC = ciudad.listarAgencias();
       }
       catch(Exception e){
            throw new Exception("Error en ListarAgencias [CiudadService]...\n"+e.getMessage());
        }
        return this.listaC;
    }
    
    /**
    * Metodo <tt>ReiniciarLista</tt>, inicializa la lista de agencias
    * @autor : Ing. Juan Escandon    
    * @see ReiniciarLista()    
    * @version : 1.0
    */
    public void ReiniciarLista(){
        this.listaC = null;
    }
    
    /**
     * Getter for property ListaC
     * @return Value of property ListaC
     */
    public List getList(){
        return this.listaC;
    }
    
    /**
    * Metodo <tt>listarCiudadesxPais</tt>, obtiene las agencias
    * @autor : Ing. Jesus Cuesta   
    * @see buscarCiudadxPais(String cod_pais, String codciu )
    * @version : 1.0
    */
    public void buscarCiudadxPais(String cod_pais, String codciu ) throws SQLException {
        try{
           ciudad_d.buscarCiudadxPais(cod_pais, codciu); 
       }
       catch(SQLException e){
           throw new SQLException ( e.getMessage()); 
       }       
    }
    
    /**
    * Metodo <tt>buscarNomCiudadxCodigo</tt>, obtiene el nombre de una ciudad dado un codigo
    * @autor : Ing. Jesus Cuesta   
    * @see buscarNomCiudadxCodigo(String codigo)
    * @version : 1.0
    */
    public String buscarNomCiudadxCodigo(String codigo) throws SQLException{
        return ciudad_d.buscarNomCiudadxCodigo(codigo);
    }
    
    /**
    * Metodo <tt>buscarCiudadesGeneral</tt>, obtiene la lista de las ciudades
    * @autor : Ing. Henry Osorio   
    * @see buscarCiudadesGeneral(String var)
    * @version : 1.0
    */
    public void buscarCiudadesGeneral(String var) throws SQLException {
        ciudad_d.buscarCiudadesGeneral(var);
    }
    
   /**
    * Metodo <tt>existeCiudad</tt>, verifica la existencia de una ciudad dado un codigo
    * @autor : Ing. Henry Osorio   
    * @see existeCiudad(String codciu ) 
    * @version : 1.0
    */
    public boolean existeCiudad(String codciu ) throws SQLException {
        return ciudad_d.existeCiudad(codciu);
    }
    
    /**
     * Metodo buscarIndicativoCiudad, busca el indicativo de la ciudad
     * de acuerdo al codigo de la ciudad
     * @autor : Ing. Diogenes Bastidas Morales
     * @param : String codigo ciudad
     * @see:  buscarIndicativoCiudad(String cod), CiudadDAO
     * @version : 1.0
     */    
    public int buscarIndicativoCiudad(String cod) throws SQLException {
        try{
            return ciudad_d.buscarIndicativoCiudad(cod);
        }
        catch(SQLException e){
           throw new SQLException ( e.getMessage()); 
       } 
    }
    
    /**
     * Metodo que busca una ciudad dado su codigo
     * @autor: Karen Reales
     * @see buscarCiudad(String codciu )     
     */
    public void buscarCiudad(String codciu ) throws SQLException {
        ciudad_d.buscarCiudad(codciu);
    }
    
    // Los siguientes metodos y/o atributos fueron agregados de la clase CiudadDAO
    // de la aplicaci�n SOT. Alejandro Payares, nov 23 de 2005.
    
    /**
     * Obtiene una ciudad dado su codigo.
     * @see obtenerCiudad(String codciu)
     * @return la ciudad buscada     
     */
    public Ciudad obtenerCiudad(String codciu) throws SQLException {
        return ciudad_d.obtenerCiudad(codciu);
    }
    
    /**
     * Obtiene las ciudades registradas     
     * @autor Alejandro Payares     
     * @see getCiudadSearch()
     */
    public void getCiudadSearch() throws SQLException {
        ciudad_d.getCiudadSearch();
    }
    
    /**
     * Getter for property ciudadList
     * @return Value of property ciudadList
     */
    public List getCiudadList() {
        return ciudad_d.getCiudadList();
    }
    
    /**
     * Obtiene un listado de las ciudades que son agencias
     * y las alamacena en un objeto de tipo <code>TreeMap</code>
     * @autor Ing. Tito Andr�s Maturana D.
     * @see com.tsp.operation.model.CiudadService#ListarAgencias()
     * @returns <code>Treemap</code> con los nombres y c�digos de las agencias
     * @version 1.0
     **/
    public TreeMap listadoAgencias() throws Exception, SQLException{
        TreeMap ags = new TreeMap();
        List lst = this.ListarAgencias();
        
        for( int i=0; i<lst.size(); i++){
            Ciudad c = (Ciudad) lst.get(i);
            ags.put(c.getNomCiu(),  c.getCodCiu());
        }        
        return ags;
    }
    
    
    /**
     * Metodo <tt>searchTreeMapCiudades()</tt>, obtiene la lista de las ciudades 
     *  registradas en el sistema
     * @autor : Ing. Karen Reales
     * @see searchCiudades()
     * @version : 1.0
     */
    public void searchTreMapCiudades()throws SQLException{
        ciudad.searchCiudades();
    }
    
     /**
     * Getter for property ciudades
     * @return Value of property ciudades
     */
    public TreeMap getTreMapCiudades(){
        return ciudad.getCiudades();
    }
    /**
     * Carga en una lista las agencias     
     * @autor Ing. Juan M Escandon
     * @see com.tsp.operation.model.CiudadService#ListarAgencias()     
     * @version 1.0
     **/
    public void ListaAgencias() throws Exception {
        try{
            this.ReiniciarLista();
            this.listaC = ciudad.listarAgencias();
        }
        catch(Exception e){
            throw new Exception("Error en ListarAgencias [CiudadService]...\n"+e.getMessage());
        }
    }
         /**
     * Busca la lista de fronteras de un pais en la tabla ciudad
     * donde el campo frontera = Y o S
     * @param pais. Codigo del pais
     * @throws SQLException si aparece un error en la base de datos
     */
    public void listarFronteras(String pais) throws SQLException {
        ciudad.listarFronteras(pais);
    }
     /**
    * Carga un <code>TreeMap</code> con las ciudades de un pa�s
    * @autor Ing. Andr�s Maturana De La Cruz
    * @throws SQLException
    * @version 1.0
    */
    public void loadCiudades(String pais) throws SQLException{
        this.ciudadTM = new TreeMap();
        this.ciudad_d.listarCiudadesxpais(pais);
        
        if( this.ciudad_d.obtenerCiudades()!=null ){
            Object[] lst = this.ciudad_d.obtenerCiudades().toArray();            
            
            for( int i=0; i<lst.length; i++){
                Ciudad ciu = (Ciudad) lst[i];
                this.ciudadTM.put(ciu.getNomCiu(), ciu.getCodCiu());
            }
        }
    }
    
    public void loadCiudadesC(String pais) throws SQLException{
        this.ciudadTM = new TreeMap();
        this.ciudad_d.listarCiudadesxpaisC(pais);
        
        if( this.ciudad_d.obtenerCiudades()!=null ){
            Object[] lst = this.ciudad_d.obtenerCiudades().toArray();            
            
            for( int i=0; i<lst.length; i++){
                Ciudad ciu = (Ciudad) lst[i];
                this.ciudadTM.put(ciu.getNomCiu(), ciu.getCodCiu());
            }
        }
    }
    
    public void loadCiudadesCE(String pais) throws SQLException{
        this.ciudadTC = new TreeMap();
        this.ciudad_d.listarCiudadesxpaisC(pais);
        
        if( this.ciudad_d.obtenerCiudades()!=null ){
            Object[] lst = this.ciudad_d.obtenerCiudades().toArray();            
            
            for( int i=0; i<lst.length; i++){
                Ciudad ciu = (Ciudad) lst[i];
                this.ciudadTC.put(ciu.getNomCiu(), ciu.getCodCiu());
            }
        }
    }
    
    /**
     * Getter for property ciudadTM.
     * @return Value of property ciudadTM.
     */
    public java.util.TreeMap getCiudadTM() {
        return ciudadTM;
    }
    
    /**
     * Setter for property ciudadTM.
     * @param ciudadTM New value of property ciudadTM.
     */
    public void setCiudadTM(java.util.TreeMap ciudadTM) {
        this.ciudadTM = ciudadTM;
    }
    
    /**
     * Obtiene un listado de las ciudades que son agencias
     * y las alamacena en un objeto de tipo <code>TreeMap</code>
     * @autor Ing. Andr�s Maturana D.
     * @see com.tsp.operation.model.CiudadService#ListarAgencias()
     * @returns <code>Treemap</code> con los nombres y c�digos de las agencias
     * @version 1.0
     **/
    public void loadListadoAgencias() throws Exception, SQLException{
        this.ciudadTM = this.listadoAgencias();
    }
    
    /**
     * Getter for property ciudadTC.
     * @return Value of property ciudadTC.
     */
    public java.util.TreeMap getCiudadTC() {
        return ciudadTC;
    }
    
    /**
     * Setter for property ciudadTC.
     * @param ciudadTC New value of property ciudadTC.
     */
    public void setCiudadTC(java.util.TreeMap ciudadTC) {
        this.ciudadTC = ciudadTC;
    }
    
    /**
     * Getter for property ciudadO.
     * @return Value of property ciudadO.
     */
    public java.util.List getCiudadO() {
        return ciudadO;
    }
    
    /**
     * Setter for property ciudadO.
     * @param ciudadO New value of property ciudadO.
     */
    public void setCiudadO(java.util.List ciudadO) {
        this.ciudadO = ciudadO;
    }
    
    /**
     * Getter for property ciudadD.
     * @return Value of property ciudadD.
     */
    public java.util.List getCiudadD() {
        return ciudadD;
    }
    
    /**
     * Setter for property ciudadD.
     * @param ciudadD New value of property ciudadD.
     */
    public void setCiudadD(java.util.List ciudadD) {
        this.ciudadD = ciudadD;
    }
    
    public void ciudadesIntermedias(String origen, String destino ) throws SQLException {
        ciudad_d.ciudadesIntermedias(origen, destino);
    }
    public void  viasxCiudad(String origen, String destino, String ciudades ) throws SQLException {
        ciudad_d.viasxCiudad(origen, destino, ciudades);
    }
    
    /**
     * Getter for property vias.
     * @return Value of property vias.
     */
    public java.util.TreeMap getVias() {
        return ciudad_d.getVias();
    }
    
    /**
     * Setter for property vias.
     * @param vias New value of property vias.
     */
    public void setVias(java.util.TreeMap vias) {
        ciudad_d.setVias(vias);
    }


        public String getCodDtp (String ciudad ) throws Exception
        {
       return ciudad_d.getCodDtp(ciudad);
    }


          /**
     * Genera una lista de departamentos para colocar en un objeto select
     * @return ArrayList
     * @throws Exception cuando hay error
     */
    public ArrayList listadoDeps() throws Exception{
        ArrayList cadena = new ArrayList ();
        try {
            cadena = ciudad_d.listadoDeps();
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error al listar ciudades: "+e.toString());
        }
        return cadena;
    }


    /**
     * Genera una lista de ciudades para colocar en un objeto select
     * @return ArrayList
     * @throws Exception cuando hay error
     */
    public ArrayList listadoCiudades(String coddept) throws Exception{
        ArrayList cadena =  new ArrayList ();
        try {
            cadena = ciudad_d.listadoCiudades(coddept);
        }
        catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Error al listar ciudades: "+e.toString());
        }
        return cadena;
    }

}
