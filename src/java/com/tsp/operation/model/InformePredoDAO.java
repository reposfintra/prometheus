/*
 * InformePredoDAO.java
 *
 * Created on 18 de julio de 2005, 10:04 AM
 */

package com.tsp.operation.model;

/**
 *
 * @author  kreales
 */
import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;
import java.text.DecimalFormat;

public class InformePredoDAO {
    
    ReportePlaneacion rep;
    Vector datos;
    Vector agencias;
    Vector historial;
    /*Diogenes 05/05/2006*/
    
       private String SQL_REPORTE_FECHAS= "select coalesce(nit.nombre,'') as nomcond,	a.*," +
    "                			b.nomcli as clientAsig,  " +
    "                                	c.nomcli as clientRec,  " +
    "                                	d.nomciu as origAsig,  " +
    "                                	e.nomciu as desAsig,	  " +
    "                                	f.nomciu as origRec,  " +
    "                                	g.nomciu as destRec,  " +
    "                                	h.descri as descripcionRec, " +
    "                                       coalesce(i.descri,'') as descripcionRecAsoc, " +
    "                                       case when TO_DATE(fecha_asig_rec,'YYYY-MM-DD')>TO_DATE(fecha_disp_req,'YYYY-MM-DD') then true else false end as aplazado " +
    "                                 from 	(Select a.recurso_rec, " +
    "                				to_char(a.fecha_dispxag,'YYYY-MM-DD') as fechaAux, " +
    "                				a.numpla_req, " +
    "                				a.numpla_rec,   " +
    "                                		a.distri_rec,   " +
    "                                               a.distri_req,   " +
    "                                               a.num_sec_req,   " +
    "                                               a.placa_rec as placa,   " +
    "                                		a.recurso_rec as recurso,   " +
    "                                		a.tipo_recurso_rec,   " +
    "                                		a.fecha_asig_rec,   " +
    "                                		a.fecha_disp_req,   " +
    "                                		a.fecha_disp_rec as fecha_disp,   " +
    "                                		a.std_job_no_req as std_asig,   " +
    "                				a.cliente_req,   " +
    "                                		a.cod_cliente_rec,  " +
    "                                		a.origen_req,   " +
    "                				a.destino_req,  " +
    "                                		b.origen as origen_rec, " +
    "                				b.destino as destino_rec, " +
    "                				a.std_job_desc_req,   " +
    "                				a.std_job_desc_rec,   " +
    "                				a.tipo_rec1_req ||' '||a.recurso1_req || ' ' ||CASE WHEN a.prioridad1_req='0' THEN '' ELSE a.prioridad1_req END as rec1,   " +
    "                				a.tipo_rec2_req ||' '||a.recurso2_req || ' ' ||CASE WHEN a.prioridad2_req='0' THEN '' ELSE a.prioridad2_req END as rec2,   " +
    "                				a.tipo_rec3_req ||' '||a.recurso3_req || ' ' ||CASE WHEN a.prioridad3_req='0' THEN '' ELSE a.prioridad3_req END as rec3,   " +
    "                                		a.tipo_rec4_req ||' '||a.recurso4_req || ' ' ||CASE WHEN a.prioridad4_req='0' THEN '' ELSE a.prioridad4_req END as rec4,   " +
    "                                		a.tipo_rec5_req ||' '||a.recurso5_req || ' ' ||CASE WHEN a.prioridad5_req='0' THEN '' ELSE a.prioridad5_req END as rec5, " +
    "                                                a.reg_status_rec, " +
    "                                                a.equipo_aso_rec, " +
    "                                                a.tipo_aso_rec " +
    "                			from    (select r.* " +
    "						 from   registro_asignacion r" +
    "						 where  fecha_dispxag between ? and ? " +
    "							and r.agasoc like ?  " +
    "							and r.cliente_req like ? " +
    "							and r.tipo_recurso_rec IN ('C','')" +
    "						) a left outer join recursos_disp b on  (b.placa = a.placa_rec and b.fecha_disp=a.fecha_disp_rec) " +
    "                                					  " +
    "                                	)a  " +
    "                                 left join ( select nomcli,  " +
    "                                		   codcli  " +
    "                                	    from   cliente)b  " +
    "                                 on (a.cliente_req = b.codcli)  " +
    "                                 left join ( select nomcli,  " +
    "                                		   codcli  " +
    "                                	    from   cliente)c  " +
    "                                 on (a.cod_cliente_rec = c.codcli)  " +
    "                                 left join ( select nomciu,  " +
    "                                		   codciu  " +
    "                                	    from   ciudad)d  " +
    "                                 on (a.origen_req = d.codciu)  " +
    "                                 left join ( select nomciu,  " +
    "                                		   codciu  " +
    "                                	    from   ciudad)e  " +
    "                                 on (a.destino_req = e.codciu)  " +
    "                                 left join ( select nomciu,  " +
    "                                		   codciu  " +
    "                                	    from   ciudad)f  " +
    "                                 on (a.origen_rec = f.codciu)  " +
    "                                 left join ( select nomciu,  " +
    "                                		   codciu  " +
    "                                	    from   ciudad)g  " +
    "                                 on (a.destino_rec = g.codciu)  " +
    "                                 left join ( select descri,recurso  " +
    "                                	    from   recursos)h  " +
    "                                 on (a.recurso_rec = h.recurso)  " +
    "                                 left join ( select descri,recurso  " +
    "                                	    from   recursos)i  " +
    "                                 on (a.tipo_aso_rec = i.recurso) " +
    "                                 left join placa on (placa.placa = a.placa)" +
    "                                 left join nit on (nit.cedula = placa.conductor)" +
    "                                order by a.fechaAux,  A.reg_status_rec desc ,a.num_sec_req desc, placa, a.std_asig, h.descri";
    
    private String SQL_REPORTE= "select coalesce(nit.nombre,'') as nomcond,	a.*," +
    "                			b.nomcli as clientAsig,  " +
    "                                	c.nomcli as clientRec,  " +
    "                                	d.nomciu as origAsig,  " +
    "                                	e.nomciu as desAsig,	  " +
    "                                	f.nomciu as origRec,  " +
    "                                	g.nomciu as destRec,  " +
    "                                	h.descri as descripcionRec, " +
    "                                       coalesce(i.descri,'') as descripcionRecAsoc, " +
    "                                       case when TO_DATE(fecha_asig_rec,'YYYY-MM-DD')>TO_DATE(fecha_disp_req,'YYYY-MM-DD') then true else false end as aplazado " +
    "                                 from 	(Select a.recurso_rec, " +
    "                				to_char(a.fecha_dispxag,'YYYY-MM-DD') as fechaAux, " +
    "                				a.numpla_req, " +
    "                				a.numpla_rec,   " +
    "                                		a.distri_rec,   " +
    "                                               a.distri_req,   " +
    "                                               a.num_sec_req,   " +
    "                                               a.placa_rec as placa,   " +
    "                                		a.recurso_rec as recurso,   " +
    "                                		a.tipo_recurso_rec,   " +
    "                                		a.fecha_asig_rec,   " +
    "                                		a.fecha_disp_req,   " +
    "                                		a.fecha_disp_rec as fecha_disp,   " +
    "                                		a.std_job_no_req as std_asig,   " +
    "                				a.cliente_req,   " +
    "                                		a.cod_cliente_rec,  " +
    "                                		a.origen_req,   " +
    "                				a.destino_req,  " +
    "                                		b.origen as origen_rec, " +
    "                				b.destino as destino_rec, " +
    "                				a.std_job_desc_req,   " +
    "                				a.std_job_desc_rec,   " +
    "                				a.tipo_rec1_req ||' '||a.recurso1_req || ' ' ||CASE WHEN a.prioridad1_req='0' THEN '' ELSE a.prioridad1_req END as rec1,   " +
    "                				a.tipo_rec2_req ||' '||a.recurso2_req || ' ' ||CASE WHEN a.prioridad2_req='0' THEN '' ELSE a.prioridad2_req END as rec2,   " +
    "                				a.tipo_rec3_req ||' '||a.recurso3_req || ' ' ||CASE WHEN a.prioridad3_req='0' THEN '' ELSE a.prioridad3_req END as rec3,   " +
    "                                		a.tipo_rec4_req ||' '||a.recurso4_req || ' ' ||CASE WHEN a.prioridad4_req='0' THEN '' ELSE a.prioridad4_req END as rec4,   " +
    "                                		a.tipo_rec5_req ||' '||a.recurso5_req || ' ' ||CASE WHEN a.prioridad5_req='0' THEN '' ELSE a.prioridad5_req END as rec5, " +
    "                                                a.reg_status_rec, " +
    "                                                a.equipo_aso_rec, " +
    "                                                a.tipo_aso_rec " +
    "                			from    (select r.* " +
    "						 from   registro_asignacion r" +
    "						 where  r.agasoc like ?  " +
    "							and r.cliente_req like ? " +
    "							and r.tipo_recurso_rec IN ('C','')" +
    "						) a left outer join recursos_disp b on  (b.placa = a.placa_rec and b.fecha_disp=a.fecha_disp_rec) " +
    "                                					  " +
    "                                	)a  " +
    "                                 left join ( select nomcli,  " +
    "                                		   codcli  " +
    "                                	    from   cliente)b  " +
    "                                 on (a.cliente_req = b.codcli)  " +
    "                                 left join ( select nomcli,  " +
    "                                		   codcli  " +
    "                                	    from   cliente)c  " +
    "                                 on (a.cod_cliente_rec = c.codcli)  " +
    "                                 left join ( select nomciu,  " +
    "                                		   codciu  " +
    "                                	    from   ciudad)d  " +
    "                                 on (a.origen_req = d.codciu)  " +
    "                                 left join ( select nomciu,  " +
    "                                		   codciu  " +
    "                                	    from   ciudad)e  " +
    "                                 on (a.destino_req = e.codciu)  " +
    "                                 left join ( select nomciu,  " +
    "                                		   codciu  " +
    "                                	    from   ciudad)f  " +
    "                                 on (a.origen_rec = f.codciu)  " +
    "                                 left join ( select nomciu,  " +
    "                                		   codciu  " +
    "                                	    from   ciudad)g  " +
    "                                 on (a.destino_rec = g.codciu)  " +
    "                                 left join ( select descri,recurso  " +
    "                                	    from   recursos)h  " +
    "                                 on (a.recurso_rec = h.recurso)  " +
    "                                 left join ( select descri,recurso  " +
    "                                	    from   recursos)i  " +
    "                                 on (a.tipo_aso_rec = i.recurso) " +
    "                                 left join placa on (placa.placa = a.placa)" +
    "                                 left join nit on (nit.cedula = placa.conductor)" +
    "                                order by a.fechaAux,  A.reg_status_rec desc ,a.num_sec_req desc, placa, a.std_asig, h.descri";

    private String SQL_REM_DSP = "select count (numrem)as remdespachadas "+
                                              "   from remesa r,                      "+
                                              "       (select nomciu                  "+
                                              "               from ciudad             "+
                                              "        where codciu = ? )a            "+
                                              " where                                 "+ 
                                              "       r.fecrem between ? and ?        "+
                                              "   and r.orirem = a.nomciu             "+
                                              "   and r.starem=''                     ";
    private String SQL_AGASOC =  " select nomciu, a.*                            "+
                                           " from ciudad c,                                "+   
                                           " (select DISTINCT                              "+
                                           "        agasoc                                 "+ 
                                           "        from registro_asignacion               "+
                                           "        where fecha_dispxag between ? and ? )a "+
                                           " where                                         "+
                                           "       c.codciu = a.agasoc                     "+
                                           "   and c.agasoc = a.agasoc                     "+
                                           "  order by nomciu                              ";
    private String CONTAR_VEH = "SELECT count(placa_rec)as vehiculos from registro_asignacion where placa_rec <> '' and agasoc = ? ";
    
    private String VEH_UTILIZADOS = "select count(placa_rec)as vehiculoutil from registro_asignacion where placa_rec <> '' and agasoc = ?  and fecha_asig_rec <> '0099-01-01 00:00:00'";
    
  
    private String SQL_TODOS= "				select coalesce(nit.nombre,'') as nomcond,	a.*,  " +
    "                			b.nomcli as clientAsig,  " +
    "                                	c.nomcli as clientRec,  " +
    "                                	d.nomciu as origAsig,  " +
    "                                	e.nomciu as desAsig,	  " +
    "                                	f.nomciu as origRec,  " +
    "                                	g.nomciu as destRec,  " +
    "                                	h.descri as descripcionRec, " +
    "					coalesce(i.descri,'') as descripcionRecAsoc" +
    "                                 from 	(Select a.recurso_rec, " +
    "                				to_char(a.fecha_dispxag,'YYYY-MM-DD') as fechaAux, " +
    "                				a.numpla_req, " +
    "                				a.numpla_rec,   " +
    "                                		a.distri_rec,   " +
    "                                               a.distri_req,   " +
    "                                               a.num_sec_req,   " +
    "                                               a.placa_rec as placa,   " +
    "                                		a.recurso_rec as recurso,   " +
    "                                		a.tipo_recurso_rec,   " +
    "                               		a.fecha_asig_rec,   " +
    "                                		a.fecha_disp_req,   " +
    "                               		a.fecha_disp_rec as fecha_disp,   " +
    "                                		a.std_job_no_req as std_asig,   " +
    "                				a.cliente_req,   " +
    "                                		a.cod_cliente_rec,  " +
    "                                		a.origen_req,   " +
    "                				a.destino_req,  " +
    "                                		b.origen as origen_rec, " +
    "                				b.destino as destino_rec, " +
    "                				a.std_job_desc_req,   " +
    "                				a.std_job_desc_rec,   " +
    "                				a.tipo_rec1_req ||' '||a.recurso1_req || ' ' ||CASE WHEN a.prioridad1_req='0' THEN '' ELSE a.prioridad1_req END as rec1,   " +
    "                				a.tipo_rec2_req ||' '||a.recurso2_req || ' ' ||CASE WHEN a.prioridad2_req='0' THEN '' ELSE a.prioridad2_req END as rec2,   " +
    "              				a.tipo_rec3_req ||' '||a.recurso3_req || ' ' ||CASE WHEN a.prioridad3_req='0' THEN '' ELSE a.prioridad3_req END as rec3,   " +
    "                                		a.tipo_rec4_req ||' '||a.recurso4_req || ' ' ||CASE WHEN a.prioridad4_req='0' THEN '' ELSE a.prioridad4_req END as rec4,   " +
    "                                		a.tipo_rec5_req ||' '||a.recurso5_req || ' ' ||CASE WHEN a.prioridad5_req='0' THEN '' ELSE a.prioridad5_req END as rec5, " +
    "                                               a.reg_status_rec, " +
    "                                               a.equipo_aso_rec, " +
    "                                               a.tipo_aso_rec " +
    "                			from    (select * from registro_asignacion where  tipo_recurso_rec IN ('C','') and fecha_asig_rec = '0099-01-01 00:00') a " +
    "						left outer join recursos_disp b on  (b.placa = a.placa_rec  " +
    "                                				and b.fecha_disp=a.fecha_disp_rec  " +
    "                					) " +
    "                                					  " +
    "                                	)a  " +
    "                                 left join ( select nomcli,  " +
    "                                		   codcli  " +
    "                                	    from   cliente)b  " +
    "                                 on (a.cliente_req = b.codcli)  " +
    "                                 left join ( select nomcli,  " +
    "                                		   codcli  " +
    "                                	    from   cliente)c  " +
    "                                 on (a.cod_cliente_rec = c.codcli)  " +
    "                                 left join ( select nomciu,  " +
    "                                		   codciu  " +
    "                                	    from   ciudad)d  " +
    "                                 on (a.origen_req = d.codciu)  " +
    "                                 left join ( select nomciu,  " +
    "                                		   codciu  " +
    "                                	    from   ciudad)e  " +
    "                                 on (a.destino_req = e.codciu)  " +
    "                                 left join ( select nomciu,  " +
    "                                		   codciu  " +
    "                                	    from   ciudad)f  " +
    "                                 on (a.origen_rec = f.codciu)  " +
    "                                 left join ( select nomciu,  " +
    "                                		   codciu  " +
    "                                	    from   ciudad)g  " +
    "                                 on (a.destino_rec = g.codciu)  " +
    "                                 left join ( select descri,recurso  " +
    "                                	    from   recursos)h  " +
    "                                 on (a.recurso_rec = h.recurso)  " +
    "                                 left join ( select descri,recurso  " +
    "                                	    from   recursos)i  " +
    "                                 on (a.tipo_aso_rec = i.recurso) " +
    "                                 left join placa on (placa.placa = a.placa)   " +
    "                                 left join nit on (nit.cedula = placa.conductor) " +
    "                                order by a.fechaAux,  A.reg_status_rec desc ,a.num_sec_req desc, placa, a.std_asig, h.descri";
    /** Creates a new instance of InformePredoDAO */
    public InformePredoDAO() {
    }
    
     public void llenarInforme(String fechai, String fechaf, String agencia, String tipo,String cliente, String zona)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        ResultSet rs1 = null;
        datos = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                
                if(tipo.equals("FECHAS")){
                    st = con.prepareStatement(this.SQL_REPORTE_FECHAS);
                    st.setString(1, fechai+" 00:00");
                    st.setString(2, fechaf+" 23:59");
                    st.setString(3, agencia+"%");
                    st.setString(4, cliente+"%");
                    
                
                }
                else{
                    st = con.prepareStatement(this.SQL_REPORTE);
                    st.setString(1, agencia+"%");
                    st.setString(2, cliente+"%");
                    
                
                   
                }
                rs = st.executeQuery();
                datos= new Vector();
                while(rs.next()){
                    rep = new ReportePlaneacion();
                    rep.setNombrecond(rs.getString("nomcond"));
                    rep.setWgroup(wgroupPlaca(rs.getString("placa")));
                    rep.setAplazado(rs.getBoolean("aplazado"));
                    //AQUI SE LLENAN TODAS
                    //Datos del cliente
                    rep.setCliente(rs.getString("clientAsig"));
                    if(rs.getString("clientAsig")!=null){
                        /*String c1 = rs.getString("rec1").equals("   ")?"":rs.getString("rec2").equals("   ")?"":",";
                        String c2 = rs.getString("rec2").equals("   ")?"":rs.getString("rec3").equals("   ")?"":",";
                        String c3 = rs.getString("rec3").equals("   ")?"":rs.getString("rec4").equals("   ")?"":",";
                        String c4 = rs.getString("rec4").equals("   ")?"":",";*/
                        
                        String vecC[]= new String[4];
                        String vecT[]= new String[4];
                        int posC  =0;
                        int posT  =0;
                        for(int k = 0; k<4; k++){
                            int reg = k+1;
                            // //System.out.println("Valor de rec"+reg+": "+rs.getString("rec"+reg));
                            if(rs.getString("rec"+reg).indexOf("C")==0){
                                vecC[posC]=rs.getString("rec"+reg);
                                posC++;
                            }
                            else if(rs.getString("rec"+reg).indexOf("T")==0){
                                vecT[posT]=rs.getString("rec"+reg);
                                posT++;
                            }
                        }
                        
                        if(!rs.getString("clientAsig").equals("")){
                            String recursos ="";
                            for(int k = 0; k<posC; k++){
                                if(vecC[k]!=null&&!vecC[k].equals("")){
                                    recursos=recursos+vecC[k]+",";
                                }
                            }
                            for(int k = 0; k<posT; k++){
                                if(vecT[k]!=null&&!vecT[k].equals("")){
                                    recursos=recursos+vecT[k]+",";
                                }
                            }
                            //  //System.out.println("Ordenado "+recursos);
                            rep.setRecurso_req(recursos);
                        }
                        /*if(!rs.getString("clientAsig").equals("")){
                            rep.setRecurso_req(rs.getString("rec1")+c1+rs.getString("rec2")+c2+rs.getString("rec3")+c3+rs.getString("rec4")+c4+rs.getString("rec5"));
                        }
                        else{
                            rep.setRecurso_req("");
                        }*/
                    }
                    else{
                        rep.setRecurso_req("");
                    }
                    
                   
                    rep.setFechadispC(rs.getTimestamp("fecha_disp_req"));
                    rep.setOrigenC(rs.getString("origAsig"));
                    rep.setDestinoC(rs.getString("desAsig"));
                    rep.setNumpla(rs.getString("numpla_rec"));
                    rep.setEqasorec(rs.getString("equipo_aso_rec"));
                    rep.setTipo_asoc(rs.getString("descripcionRecAsoc"));
                    
                    // alejo
                    //  //System.out.println("fecha dispo cliente: "+rep.getFechadispC());
                    rep.setDstrct_code(rs.getString("distri_req"));
                    rep.setNum_sec(rs.getInt("num_sec_req"));
                    rep.setStd_job_no(rs.getString("std_asig"));
                    rep.setNumpla(rs.getString("numpla_req"));
                    //  //System.out.println("numpla cliente: "+rep.getNumpla());
                    
                    //Datos de la placa
                    rep.setPlaca(rs.getString("placa"));
                    //rep.setClase(rs.getString("descripcionRec"));
                    rep.setClase(rs.getString("recurso_rec"));
                    rep.setOrigenR(rs.getString("origRec"));
                    rep.setDestinoR(rs.getString("destRec"));
                    rep.setClienteR(rs.getString("clientRec"));
                    
                    rep.setFechadispR(rs.getTimestamp("fecha_disp"));
                    
                    // alejo
                    rep.setDstrct(rs.getString("distri_rec"));
                    rep.setStd_job_desc_rec(rs.getString("std_job_desc_rec"));
                    rep.setStd_job_desc_req(rs.getString("std_job_desc_req"));
                    rep.setFecha_dispxag(rs.getString("fechaAux"));
                    rep.setOrigen_req(rs.getString("origAsig"));
                    rep.setDestino_req(rs.getString("desAsig"));
                    //ASIGNACION
                    if(!rs.getString("fecha_asig_rec").equals("0099-01-01 00:00:00")){
                        rep.setFechaasig(rs.getTimestamp("fecha_asig_rec"));
                    }
                    //INICIALIZO VALORES EJECUTADOS
                    rep.setNumplaejec("");
                    rep.setCliejec("");
                    rep.setOriEjec("");
                    rep.setDesEjec("");
                    rep.setStdejec("");
                    
                    if(rep.getPlaca()!=null && rep.getFechadispR()!=null ){
                        
                        st = con.prepareStatement( "Select p.numpla,   " +
                        "                            r.std_job_no, " +
                        "                               c.nomcli, " +
                        "                               p.fecdsp, " +
                        "                               get_nombreciudad (p.oripla )as ori, " +
                        "                               get_nombreciudad( p.despla) as des " +
                        "                         from   planilla p" +
                        "				inner join plarem pr on (pr.numpla=p.numpla)" +
                        "				inner join remesa r on (r.numrem = pr.numrem)" +
                        "				inner join cliente c on (c.codcli = r.cliente)" +
                        "                         where  p.plaveh = ?" +
                        "                               and fecdsp>=?" +
                        "			order by fecdsp desc");
                        st.setString(1, rep.getPlaca());
                        st.setString(2, ""+rep.getFechadispR());
                        rs1 = st.executeQuery();
                        
                        
                        
                        if(rs1.next()){
                            rep.setNumplaejec(rs1.getString("numpla"));
                            rep.setCliejec(rs1.getString("nomcli"));
                            rep.setOriEjec(rs1.getString("ori"));
                            rep.setDesEjec(rs1.getString("des"));
                            rep.setFecdspejec(rs1.getTimestamp("fecdsp"));
                            rep.setStdejec(rs1.getString("std_job_no"));
                        }
                    }
                    datos.add(rep);
                }
                
                
            }
        }catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA CREACION DEL INFORME " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
        
    }
    
    public void llenarInforme(String fechai, String fechaf, String agencia, String tipo)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        ResultSet rs1 = null;
        datos = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                
                if(tipo.equals("FECHAS")){
                    st = con.prepareStatement(this.SQL_REPORTE_FECHAS);
                    st.setString(1, agencia);
                    st.setString(2, fechai+" 00:00");
                    st.setString(3, fechaf+" 23:59");
                
                }
                else{
                    st = con.prepareStatement(this.SQL_REPORTE);
                    st.setString(1, agencia);
                   
                }
                // ////System.out.println("query: "+st);
                rs = st.executeQuery();
                datos= new Vector();
                while(rs.next()){
                    rep = new ReportePlaneacion();
                    rep.setNombrecond(rs.getString("nomcond"));
                    rep.setWgroup(wgroupPlaca(rs.getString("placa")));
                    rep.setAplazado(rs.getBoolean("aplazado"));
                    //AQUI SE LLENAN TODAS
                    //Datos del cliente
                    rep.setCliente(rs.getString("clientAsig"));
                    if(rs.getString("clientAsig")!=null){
                        /*String c1 = rs.getString("rec1").equals("   ")?"":rs.getString("rec2").equals("   ")?"":",";
                        String c2 = rs.getString("rec2").equals("   ")?"":rs.getString("rec3").equals("   ")?"":",";
                        String c3 = rs.getString("rec3").equals("   ")?"":rs.getString("rec4").equals("   ")?"":",";
                        String c4 = rs.getString("rec4").equals("   ")?"":",";*/
                        
                        String vecC[]= new String[4];
                        String vecT[]= new String[4];
                        int posC  =0;
                        int posT  =0;
                        for(int k = 0; k<4; k++){
                            int reg = k+1;
                            // ////System.out.println("Valor de rec"+reg+": "+rs.getString("rec"+reg));
                            if(rs.getString("rec"+reg).indexOf("C")==0){
                                vecC[posC]=rs.getString("rec"+reg);
                                posC++;
                            }
                            else if(rs.getString("rec"+reg).indexOf("T")==0){
                                vecT[posT]=rs.getString("rec"+reg);
                                posT++;
                            }
                        }
                        
                        if(!rs.getString("clientAsig").equals("")){
                            String recursos ="";
                            for(int k = 0; k<posC; k++){
                                if(vecC[k]!=null&&!vecC[k].equals("")){
                                    recursos=recursos+vecC[k]+",";
                                }
                            }
                            for(int k = 0; k<posT; k++){
                                if(vecT[k]!=null&&!vecT[k].equals("")){
                                    recursos=recursos+vecT[k]+",";
                                }
                            }
                            //  ////System.out.println("Ordenado "+recursos);
                            rep.setRecurso_req(recursos);
                        }
                        /*if(!rs.getString("clientAsig").equals("")){
                            rep.setRecurso_req(rs.getString("rec1")+c1+rs.getString("rec2")+c2+rs.getString("rec3")+c3+rs.getString("rec4")+c4+rs.getString("rec5"));
                        }
                        else{
                            rep.setRecurso_req("");
                        }*/
                    }
                    else{
                        rep.setRecurso_req("");
                    }
                    
                   
                    rep.setFechadispC(rs.getTimestamp("fecha_disp_req"));
                    rep.setOrigenC(rs.getString("origAsig"));
                    rep.setDestinoC(rs.getString("desAsig"));
                    rep.setNumpla(rs.getString("numpla_rec"));
                    rep.setEqasorec(rs.getString("equipo_aso_rec"));
                    rep.setTipo_asoc(rs.getString("descripcionRecAsoc"));
                    
                    // alejo
                    //  ////System.out.println("fecha dispo cliente: "+rep.getFechadispC());
                    rep.setDstrct_code(rs.getString("distri_req"));
                    rep.setNum_sec(rs.getInt("num_sec_req"));
                    rep.setStd_job_no(rs.getString("std_asig"));
                    rep.setNumpla(rs.getString("numpla_req"));
                    //  ////System.out.println("numpla cliente: "+rep.getNumpla());
                    
                    //Datos de la placa
                    rep.setPlaca(rs.getString("placa"));
                    //rep.setClase(rs.getString("descripcionRec"));
                    rep.setClase(rs.getString("recurso_rec"));
                    rep.setOrigenR(rs.getString("origRec"));
                    rep.setDestinoR(rs.getString("destRec"));
                    rep.setClienteR(rs.getString("clientRec"));
                    
                    rep.setFechadispR(rs.getTimestamp("fecha_disp"));
                    
                    // alejo
                    rep.setDstrct(rs.getString("distri_rec"));
                    rep.setStd_job_desc_rec(rs.getString("std_job_desc_rec"));
                    rep.setStd_job_desc_req(rs.getString("std_job_desc_req"));
                    rep.setFecha_dispxag(rs.getString("fechaAux"));
                    rep.setOrigen_req(rs.getString("origAsig"));
                    rep.setDestino_req(rs.getString("desAsig"));
                    //ASIGNACION
                    if(!rs.getString("fecha_asig_rec").equals("0099-01-01 00:00:00")){
                        rep.setFechaasig(rs.getTimestamp("fecha_asig_rec"));
                    }
                    //INICIALIZO VALORES EJECUTADOS
                    rep.setNumplaejec("");
                    rep.setCliejec("");
                    rep.setOriEjec("");
                    rep.setDesEjec("");
                    rep.setStdejec("");
                    
                    if(rep.getPlaca()!=null && rep.getFechadispR()!=null ){
                        
                        st = con.prepareStatement( "Select p.numpla,   " +
                        "                            r.std_job_no, " +
                        "                               c.nomcli, " +
                        "                               p.fecdsp, " +
                        "                               get_nombreciudad (p.oripla )as ori, " +
                        "                               get_nombreciudad( p.despla) as des " +
                        "                         from   planilla p" +
                        "				inner join plarem pr on (pr.numpla=p.numpla)" +
                        "				inner join remesa r on (r.numrem = pr.numrem)" +
                        "				inner join cliente c on (c.codcli = r.cliente)" +
                        "                         where  p.plaveh = ?" +
                        "                               and fecdsp>=?" +
                        "			order by fecdsp desc");
                        st.setString(1, rep.getPlaca());
                        st.setString(2, ""+rep.getFechadispR());
                        rs1 = st.executeQuery();
                        
                        
                        
                        if(rs1.next()){
                            rep.setNumplaejec(rs1.getString("numpla"));
                            rep.setCliejec(rs1.getString("nomcli"));
                            rep.setOriEjec(rs1.getString("ori"));
                            rep.setDesEjec(rs1.getString("des"));
                            rep.setFecdspejec(rs1.getTimestamp("fecdsp"));
                            rep.setStdejec(rs1.getString("std_job_no"));
                        }
                    }
                    datos.add(rep);
                }
                
                
            }
        }catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA CREACION DEL INFORME " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
        
    }
    public void llenarInforme()throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        ResultSet rs1 = null;
        datos = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                
                st = con.prepareStatement(this.SQL_TODOS);
                // ////System.out.println("query: "+st);
                rs = st.executeQuery();
                datos= new Vector();
                while(rs.next()){
                    rep = new ReportePlaneacion();
                    rep.setNombrecond(rs.getString("nomcond"));
                    rep.setWgroup(wgroupPlaca(rs.getString("placa")));
                    //AQUI SE LLENAN TODAS
                    //Datos del cliente
                    rep.setCliente(rs.getString("clientAsig"));
                    if(rs.getString("clientAsig")!=null){
                        /*String c1 = rs.getString("rec1").equals("   ")?"":rs.getString("rec2").equals("   ")?"":",";
                        String c2 = rs.getString("rec2").equals("   ")?"":rs.getString("rec3").equals("   ")?"":",";
                        String c3 = rs.getString("rec3").equals("   ")?"":rs.getString("rec4").equals("   ")?"":",";
                        String c4 = rs.getString("rec4").equals("   ")?"":",";*/
                        
                        String vecC[]= new String[4];
                        String vecT[]= new String[4];
                        int posC  =0;
                        int posT  =0;
                        for(int k = 0; k<4; k++){
                            int reg = k+1;
                            // ////System.out.println("Valor de rec"+reg+": "+rs.getString("rec"+reg));
                            if(rs.getString("rec"+reg).indexOf("C")==0){
                                vecC[posC]=rs.getString("rec"+reg);
                                posC++;
                            }
                            else if(rs.getString("rec"+reg).indexOf("T")==0){
                                vecT[posT]=rs.getString("rec"+reg);
                                posT++;
                            }
                        }
                        
                        if(!rs.getString("clientAsig").equals("")){
                            String recursos ="";
                            for(int k = 0; k<posC; k++){
                                if(vecC[k]!=null&&!vecC[k].equals("")){
                                    recursos=recursos+vecC[k]+",";
                                }
                            }
                            for(int k = 0; k<posT; k++){
                                if(vecT[k]!=null&&!vecT[k].equals("")){
                                    recursos=recursos+vecT[k]+",";
                                }
                            }
                            //  ////System.out.println("Ordenado "+recursos);
                            rep.setRecurso_req(recursos);
                        }
                        /*if(!rs.getString("clientAsig").equals("")){
                            rep.setRecurso_req(rs.getString("rec1")+c1+rs.getString("rec2")+c2+rs.getString("rec3")+c3+rs.getString("rec4")+c4+rs.getString("rec5"));
                        }
                        else{
                            rep.setRecurso_req("");
                        }*/
                    }
                    else{
                        rep.setRecurso_req("");
                    }
                    
                   
                    rep.setFechadispC(rs.getTimestamp("fecha_disp_req"));
                    rep.setOrigenC(rs.getString("origAsig"));
                    rep.setDestinoC(rs.getString("desAsig"));
                    rep.setNumpla(rs.getString("numpla_rec"));
                    rep.setEqasorec(rs.getString("equipo_aso_rec"));
                    rep.setTipo_asoc(rs.getString("descripcionRecAsoc"));
                    
                    // alejo
                    //  ////System.out.println("fecha dispo cliente: "+rep.getFechadispC());
                    rep.setDstrct_code(rs.getString("distri_req"));
                    rep.setNum_sec(rs.getInt("num_sec_req"));
                    rep.setStd_job_no(rs.getString("std_asig"));
                    rep.setNumpla(rs.getString("numpla_req"));
                    //  ////System.out.println("numpla cliente: "+rep.getNumpla());
                    
                    //Datos de la placa
                    rep.setPlaca(rs.getString("placa"));
                    //rep.setClase(rs.getString("descripcionRec"));
                    rep.setClase(rs.getString("recurso_rec"));
                    rep.setOrigenR(rs.getString("origRec"));
                    rep.setDestinoR(rs.getString("destRec"));
                    rep.setClienteR(rs.getString("clientRec"));
                    
                    rep.setFechadispR(rs.getTimestamp("fecha_disp"));
                    
                    // alejo
                    rep.setDstrct(rs.getString("distri_rec"));
                    rep.setStd_job_desc_rec(rs.getString("std_job_desc_rec"));
                    rep.setStd_job_desc_req(rs.getString("std_job_desc_req"));
                    rep.setFecha_dispxag(rs.getString("fechaAux"));
                    rep.setOrigen_req(rs.getString("origAsig"));
                    rep.setDestino_req(rs.getString("desAsig"));
                    datos.add(rep);
                }
                
                
            }
        }catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA CREACION DEL INFORME DE TODOS LOS NO ASIGNADOS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
        
    }
    
    
    public void llenarInforme(String fechai, String fechaf)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        ResultSet rs1 = null;
        datos = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                
                //LO ASIGNADO - campos agregados al quey numpla_req,distri_rec,distri_req,num_sec_req,tipo_recurso_rec
                st = con.prepareStatement( "select coalesce(nit.nombre,'') as nomcond,	a.*, " +
                "			b.nomcli as clientAsig, " +
                "                	c.nomcli as clientRec, " +
                "                	d.nomciu as origAsig, " +
                "                	e.nomciu as desAsig,	 " +
                "                	f.nomciu as origRec, " +
                "                	g.nomciu as destRec, " +
                "                	h.descri as descripcionRec," +
                "                       coalesce(i.descri,'') as descripcionRecAsoc," +
                "                       case when TO_DATE(fecha_asig_rec,'YYYY-MM-DD')>TO_DATE(fecha_disp_req,'YYYY-MM-DD') then true else false end as aplazado" +
                "                 from 	(Select a.recurso_rec," +
                "				to_char(a.fecha_dispxag,'YYYY-MM-DD') as fechaAux," +
                "				a.numpla_req," +
                "				a.numpla_rec,  " +
                "                		a.distri_rec,  " +
                "                               a.distri_req,  " +
                "                               a.num_sec_req,  " +
                "                               a.placa_rec as placa,  " +
                "                		a.recurso_rec as recurso,  " +
                "                		a.tipo_recurso_rec,  " +
                "                		a.fecha_asig_rec,  " +
                "                		a.fecha_disp_req,  " +
                "                		a.fecha_disp_rec as fecha_disp,  " +
                "                		a.std_job_no_req as std_asig,  " +
                "				a.cliente_req,  " +
                "                		a.cod_cliente_rec, " +
                "                		a.origen_req,  " +
                "				a.destino_req, " +
                "                		b.origen as origen_rec," +
                "				b.destino as destino_rec," +
                "				a.std_job_desc_req,  " +
                "				a.std_job_desc_rec,  " +
                "				a.tipo_rec1_req ||' '||a.recurso1_req || ' ' ||CASE WHEN a.prioridad1_req='0' THEN '' ELSE a.prioridad1_req END as rec1,  " +
                "				a.tipo_rec2_req ||' '||a.recurso2_req || ' ' ||CASE WHEN a.prioridad2_req='0' THEN '' ELSE a.prioridad2_req END as rec2,  " +
                "				a.tipo_rec3_req ||' '||a.recurso3_req || ' ' ||CASE WHEN a.prioridad3_req='0' THEN '' ELSE a.prioridad3_req END as rec3,  " +
                "                		a.tipo_rec4_req ||' '||a.recurso4_req || ' ' ||CASE WHEN a.prioridad4_req='0' THEN '' ELSE a.prioridad4_req END as rec4,  " +
                "                		a.tipo_rec5_req ||' '||a.recurso5_req || ' ' ||CASE WHEN a.prioridad5_req='0' THEN '' ELSE a.prioridad5_req END as rec5," +
                "                               a.reg_status_rec," +
                "                               a.equipo_aso_rec," +
                "                               a.tipo_aso_rec" +
                "			from    (select * from registro_asignacion where fecha_dispxag between ? and ?" +
                "                                               and tipo_recurso_rec IN ('C','')) a left outer join recursos_disp b on  (b.placa = a.placa_rec " +
                "                				and b.fecha_disp=a.fecha_disp_rec " +
                "					)" +
                "                					 " +
                "                	)a " +
                "                 left join ( select nomcli, " +
                "                		   codcli " +
                "                	    from   cliente)b " +
                "                 on (a.cliente_req = b.codcli) " +
                "                 left join ( select nomcli, " +
                "                		   codcli " +
                "                	    from   cliente)c " +
                "                 on (a.cod_cliente_rec = c.codcli) " +
                "                 left join ( select nomciu, " +
                "                		   codciu " +
                "                	    from   ciudad)d " +
                "                 on (a.origen_req = d.codciu) " +
                "                 left join ( select nomciu, " +
                "                		   codciu " +
                "                	    from   ciudad)e " +
                "                 on (a.destino_req = e.codciu) " +
                "                 left join ( select nomciu, " +
                "                		   codciu " +
                "                	    from   ciudad)f " +
                "                 on (a.origen_rec = f.codciu) " +
                "                 left join ( select nomciu, " +
                "                		   codciu " +
                "                	    from   ciudad)g " +
                "                 on (a.destino_rec = g.codciu) " +
                "                 left join ( select descri,recurso " +
                "                	    from   recursos)h " +
                "                 on (a.recurso_rec = h.recurso) " +
                "                 left join ( select descri,recurso " +
                "                	    from   recursos)i " +
                "                 on (a.tipo_aso_rec = i.recurso)" +
                "                 left join placa on (placa.placa = a.placa)  " +
                "                 left join nit on (nit.cedula = placa.conductor)" +
                "                order by a.fechaAux,  A.reg_status_rec desc ,a.num_sec_req desc, placa, a.std_asig, h.descri");
                st.setString(1, fechai+" 00:00");
                st.setString(2, fechaf+" 23:59");
                // ////System.out.println("query: "+st);
                rs = st.executeQuery();
                datos= new Vector();
                while(rs.next()){
                    rep = new ReportePlaneacion();
                    rep.setNombrecond(rs.getString("nomcond"));
                    //rep.setWgroup(wgroupPlaca(rs.getString("placa")));
                    rep.setAplazado(rs.getBoolean("aplazado"));
                    //AQUI SE LLENAN TODAS
                    //Datos del cliente
                    rep.setCliente(rs.getString("clientAsig"));
                    if(rs.getString("clientAsig")!=null){
                        /*String c1 = rs.getString("rec1").equals("   ")?"":rs.getString("rec2").equals("   ")?"":",";
                        String c2 = rs.getString("rec2").equals("   ")?"":rs.getString("rec3").equals("   ")?"":",";
                        String c3 = rs.getString("rec3").equals("   ")?"":rs.getString("rec4").equals("   ")?"":",";
                        String c4 = rs.getString("rec4").equals("   ")?"":",";*/
                        
                        String vecC[]= new String[4];
                        String vecT[]= new String[4];
                        int posC  =0;
                        int posT  =0;
                        for(int k = 0; k<4; k++){
                            int reg = k+1;
                            // ////System.out.println("Valor de rec"+reg+": "+rs.getString("rec"+reg));
                            if(rs.getString("rec"+reg).indexOf("C")==0){
                                vecC[posC]=rs.getString("rec"+reg);
                                posC++;
                            }
                            else if(rs.getString("rec"+reg).indexOf("T")==0){
                                vecT[posT]=rs.getString("rec"+reg);
                                posT++;
                            }
                        }
                        
                        if(!rs.getString("clientAsig").equals("")){
                            String recursos ="";
                            for(int k = 0; k<posC; k++){
                                if(vecC[k]!=null&&!vecC[k].equals("")){
                                    recursos=recursos+vecC[k]+",";
                                }
                            }
                            for(int k = 0; k<posT; k++){
                                if(vecT[k]!=null&&!vecT[k].equals("")){
                                    recursos=recursos+vecT[k]+",";
                                }
                            }
                            //  ////System.out.println("Ordenado "+recursos);
                            rep.setRecurso_req(recursos);
                        }
                        /*if(!rs.getString("clientAsig").equals("")){
                            rep.setRecurso_req(rs.getString("rec1")+c1+rs.getString("rec2")+c2+rs.getString("rec3")+c3+rs.getString("rec4")+c4+rs.getString("rec5"));
                        }
                        else{
                            rep.setRecurso_req("");
                        }*/
                    }
                    else{
                        rep.setRecurso_req("");
                    }
                    
                   
                    rep.setFechadispC(rs.getTimestamp("fecha_disp_req"));
                    rep.setOrigenC(rs.getString("origAsig"));
                    rep.setDestinoC(rs.getString("desAsig"));
                    rep.setNumpla(rs.getString("numpla_rec"));
                    rep.setEqasorec(rs.getString("equipo_aso_rec"));
                    rep.setTipo_asoc(rs.getString("descripcionRecAsoc"));
                    
                    // alejo
                    //  ////System.out.println("fecha dispo cliente: "+rep.getFechadispC());
                    rep.setDstrct_code(rs.getString("distri_req"));
                    rep.setNum_sec(rs.getInt("num_sec_req"));
                    rep.setStd_job_no(rs.getString("std_asig"));
                    rep.setNumpla(rs.getString("numpla_req"));
                    //  ////System.out.println("numpla cliente: "+rep.getNumpla());
                    
                    //Datos de la placa
                    rep.setPlaca(rs.getString("placa"));
                    //rep.setClase(rs.getString("descripcionRec"));
                    rep.setClase(rs.getString("recurso_rec"));
                    rep.setOrigenR(rs.getString("origRec"));
                    rep.setDestinoR(rs.getString("destRec"));
                    rep.setClienteR(rs.getString("clientRec"));
                    
                    rep.setFechadispR(rs.getTimestamp("fecha_disp"));
                    
                    // alejo
                    rep.setDstrct(rs.getString("distri_rec"));
                    rep.setStd_job_desc_rec(rs.getString("std_job_desc_rec"));
                    rep.setStd_job_desc_req(rs.getString("std_job_desc_req"));
                    rep.setFecha_dispxag(rs.getString("fechaAux"));
                    rep.setOrigen_req(rs.getString("origAsig"));
                    rep.setDestino_req(rs.getString("desAsig"));
                    //ASIGNACION
                    if(!rs.getString("fecha_asig_rec").equals("0099-01-01 00:00:00")){
                        rep.setFechaasig(rs.getTimestamp("fecha_asig_rec"));
                    }
                    //INICIALIZO VALORES EJECUTADOS
                    rep.setNumplaejec("");
                    rep.setCliejec("");
                    rep.setOriEjec("");
                    rep.setDesEjec("");
                    rep.setStdejec("");
                    
                    if(rep.getPlaca()!=null && rep.getFechadispR()!=null ){
                        
                        st = con.prepareStatement( "Select p.numpla," +
                        "       r.std_job_no," +
                        "       c.nomcli," +
                        "       p.fecdsp," +
                        "       co.nomciu as ori," +
                        "       cd.nomciu as des" +
                        " from   planilla p," +
                        "       remesa r," +
                        "       plarem pr," +
                        "       cliente c," +
                        "       ciudad co," +
                        "       ciudad cd" +
                        " where fecdsp>=?" +
                        "       and p.plaveh = ?" +
                        "       and pr.numpla = p.numpla" +
                        "       and r.numrem =pr.numrem" +
                        "       and c.codcli = r.cliente" +
                        "       and co.codciu = p.oripla" +
                        "	and cd.codciu = p.despla" );
                        st.setString(1, ""+rep.getFechadispR());
                        st.setString(2, rep.getPlaca());
                        rs1 = st.executeQuery();
                        
                        
                        
                        if(rs1.next()){
                            rep.setNumplaejec(rs1.getString("numpla"));
                            rep.setCliejec(rs1.getString("nomcli"));
                            rep.setOriEjec(rs1.getString("ori"));
                            rep.setDesEjec(rs1.getString("des"));
                            rep.setFecdspejec(rs1.getTimestamp("fecdsp"));
                            rep.setStdejec(rs1.getString("std_job_no"));
                        }
                    }
                    datos.add(rep);
                }
                
                
            }
        }catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE LA CREACION DEL INFORME " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
        
    }
    public void buscarEjecutado(ReportePlaneacion rep)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        datos = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                if(rep!=null){
                    st = con.prepareStatement( "Select p.numpla," +
                    "       r.std_job_no," +
                    "       c.nomcli," +
                    "       p.fecdsp," +
                    "       co.nomciu as ori," +
                    "       cd.nomciu as des" +
                    " from   planilla p," +
                    "       remesa r," +
                    "       plarem pr," +
                    "       cliente c," +
                    "       ciudad co," +
                    "       ciudad cd" +
                    " where  p.plaveh = ?" +
                    "       and pr.numpla = p.numpla" +
                    "       and r.numrem =pr.numrem" +
                    "       and c.codcli = r.cliente" +
                    "       and co.codciu = p.oripla" +
                    "	and cd.codciu = p.despla" +
                    "       and fecdsp>=?");
                    st.setString(1, rep.getPlaca());
                    st.setString(2, ""+rep.getFechadispR());
                    rs = st.executeQuery();
                    
                    //INICIALIZO VALORES
                    rep.setNumplaejec("");
                    rep.setCliejec("");
                    rep.setOriEjec("");
                    rep.setDesEjec("");
                    rep.setStdejec("");
                    
                    if(rs.next()){
                        rep.setNumplaejec(rs.getString("numpla"));
                        rep.setCliejec(rs.getString("nomcli"));
                        rep.setOriEjec(rs.getString("ori"));
                        rep.setDesEjec(rs.getString("des"));
                        rep.setFecdspejec(rs.getTimestamp("fecdsp"));
                        rep.setStdejec(rs.getString("std_job_no"));
                    }
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA CREACION DEL INFORME BUSCANDO LO EJECUTADO " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
        
    }
    
    /**
     * Getter for property datos.
     * @return Value of property datos.
     */
    public java.util.Vector getDatos() {
        return datos;
    }
    
    /**
     * Setter for property datos.
     * @param datos New value of property datos.
     */
    public void setDatos(java.util.Vector datos) {
        this.datos = datos;
    }
    
    /**
     * Getter for property rep.
     * @return Value of property rep.
     */
    public com.tsp.operation.model.beans.ReportePlaneacion getRep() {
        return rep;
    }
    
    /**
     * Setter for property rep.
     * @param rep New value of property rep.
     */
    public void setRep(com.tsp.operation.model.beans.ReportePlaneacion rep) {
        this.rep = rep;
    }
    
    //Diogenes (Informe predo por agencias )
    
    public java.util.Vector getAgencias() {
        return agencias;
    }
    public void setAgencias(java.util.Vector agen) {
        this.agencias = agen;
    }
    
    //llena la columna de agencia asociadas
    public void LlenarAgasoc( )throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                //asoc rec
                st = con.prepareStatement( "UPDATE REGISTRO_ASIGNACION set agasoc = a.AGASOC from" +
                " (SELECT C.AGASOC," +
                "	c.codciu" +
                " FROM   CIUDAD C" +
                " )a" +
                " where  a.CODCIU = CASE WHEN ORIGEN_REQ <> '' then ORIGEN_REQ else DESTINO_REC end;" +
                " UPDATE REGISTRO_ASIGNACION set fecha_dispxag = a.fecha from" +
                " (SELECT CASE WHEN fecha_asing_req <> '0099-01-01 00:00:00' then fecha_asing_req else  CASE WHEN fecha_disp_req  <> '0099-01-01 00:00:00' then fecha_disp_req else fecha_disp_rec end  end as fecha," +
                "        oid" +
                " FROM   REGISTRO_ASIGNACION" +
                " )a" +
                " where  a.oid = REGISTRO_ASIGNACION.oid");
                st.executeUpdate();
                
                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE EL LLENADO DE AGASOC " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    
    
    public Vector ListarAgencias(String fec1,String fec2 )throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        ResultSet rs1 = null;
        agencias = null;
        PoolManager poolManager = null;
        double utilflota=0;
        double retorno=0;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            ////System.out.println("Con "+con);
            if(con!=null){
                
                st = con.prepareStatement(SQL_AGASOC);
                st.setString(1,fec1+" 00:00");
                st.setString(2,fec2+" 23:59");
                ////System.out.println(st);
                rs = st.executeQuery();
                agencias= new Vector();
                
                while(rs.next()){
                    rep = new ReportePlaneacion();
                    rep.setAgasoc(rs.getString("agasoc") );
                    rep.setNomAgasoc(rs.getString("nomciu") );
                    //cuenta los vehiculos
                    st = con.prepareStatement(CONTAR_VEH);
                    st.setString(1,rep.getAgasoc());
                    
                    rs1 = st.executeQuery();
                    if(rs1.next()){
                        rep.setVehiculos(rs1.getInt("Vehiculos"));                        
                    }
                    //cuenta vehiculos utilizados
                    st = con.prepareStatement(VEH_UTILIZADOS);
                    st.setString(1,rep.getAgasoc());
                    
                    
                    rs1 = st.executeQuery();
                    if(rs1.next()){
                        rep.setVehiculoutil(rs1.getInt("vehiculoutil"));                        
                    }
                    
                    st = con.prepareStatement(SQL_REM_DSP);
                    st.setString(1,rep.getAgasoc());
                    st.setString(2,fec1);
                    st.setString(3,fec2);
                    //////System.out.println("query3: "+st);
                    rs1 = st.executeQuery();
                    if(rs1.next()){
                        rep.setRemDespachada(rs1.getInt("remdespachadas"));
                    }
                    ////System.out.println(" Vehiculoutil "+rep.getVehiculoutil()+"Remesa "+rep.getRemDespachada());
                   
                    if (rep.getRemDespachada()>0){
                        utilflota = (((double)rep.getVehiculoutil())/((double)rep.getRemDespachada()))*100d;
                    }
                    DecimalFormat df = new DecimalFormat("0.#");
                    rep.setUtilflota(Double.parseDouble(df.format(utilflota)));
                    if (rep.getVehiculos()>0){
                        retorno = (((double)rep.getVehiculoutil())/((double)rep.getVehiculos()))*100d; //ojo dijito                        
                    }
                    rep.setRetorno(Double.parseDouble(df.format(retorno)));
                    agencias.add(rep);
                }
            }
        }catch(SQLException e){
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE EL Listar AGENCIAS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return agencias;
    }
    
    
    public void ListarxAgencia(String agencia, String fec1,String fec2 )throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        ResultSet rs1 = null;
        agencias = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                
                st = con.prepareStatement("     select a.*,                     "+
                "            b.nomcli,                "+
                "            c.nomciu as orirec,      "+
                "            d.nomciu as desrec       "+
                "        from                         "+
                "        (select                      "+
                "               placa_rec,            "+
                "               TO_CHAR(fecha_disp_rec,'DD-MM-YYYY HH:MM:SS') as fecha_disp_rec,       "+
                "               numpla_rec,           "+
                "               origen_rec,           "+
                "               destino_rec,          "+
                "               tipo_recurso_rec,     "+
                "               recurso_rec,          "+
                "               TO_CHAR(fecha_asig_rec,'DD-MM-YYYY HH:MM:SS') as fecha_asig_rec,       "+
                "               equipo_aso_rec,              "+
                "               cliente_req                  "+
                "               from registro_asignacion     "+
                "            where agasoc = ?                "+
                "                and fecha_dispxag between ? and ? "+
                "            order by fecha_disp_rec desc)a        "+
                "      left join ( select nomcli,                  "+
                "		   codcli                            "+
                "	    from   cliente)b                         "+
                "      on (a.cliente_req = b.codcli)               "+
                "      left join ( select nomciu,                  "+
                " 		   codciu                            "+
                "	    from   ciudad)c                          "+
                "      on (a.origen_rec = c.codciu)                "+
                "      left join ( select nomciu,                  "+
                " 	  	   codciu                            "+
                "  	    from   ciudad)d                          "+
                "      on (a.destino_rec = d.codciu)               "+
                "      order by b.nomcli                           ");
                st.setString(1,agencia);
                st.setString(2,fec1+" 00:00");
                st.setString(3,fec2+" 23:59");
                
              //  ////System.out.println(st);
                
                rs = st.executeQuery();
                agencias= new Vector();
                
                while(rs.next()){
                    rep = new ReportePlaneacion();
                    rep.setPlaca(rs.getString("placa_rec") );
                    rep.setFecdisrec(rs.getString("fecha_disp_rec") );
                    rep.setNumpla(rs.getString("numpla_rec") );
                    rep.setNomorirec(rs.getString("orirec") );
                    rep.setNomDesrec(rs.getString("desrec") );
                    rep.setTipo_rec_rec(rs.getString("tipo_recurso_rec") );
                    rep.setRecurso_rec(rs.getString("recurso_rec") );
                    rep.setFecasigrec(rs.getString("fecha_asig_rec") );
                    rep.setEqasorec(rs.getString("equipo_aso_rec") );
                    rep.setNomClienreq(rs.getString("nomcli") );
                    agencias.add(rep);
                    
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE Al BUSCAR LA LISTA DE LA AGENCIA X " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    
    
     public void buscarHistorial(String placa)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        ResultSet rs1 = null;
        historial = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                
                st = con.prepareStatement("select 	fecha_disp_rec," +
                "	placa_rec, " +
                "	o.nomciu as orirec," +
                "	d.nomciu as desrec," +
                "	std_job_desc_req," +
                "	fecha_asig_rec," +
                "       nit.nombre" +
                " from 	registro_asignacion r" +
                "	left join ciudad o on (r.origen_rec=o.codciu)" +
                "	left join ciudad d on (r.destino_rec=d.codciu)" +
                "       left join placa on (placa.placa =placa_rec)" +
                "	left join nit on (placa.conductor=nit.cedula)" +
                " where 	placa_rec = ?" +
                " ORDER BY fecha_disp_rec");
                st.setString(1,placa);
              //  ////System.out.println(st);
                
                rs = st.executeQuery();
                historial= new Vector();
                
                while(rs.next()){
                    rep = new ReportePlaneacion();
                    rep.setPlaca(rs.getString("placa_rec") );
                    rep.setFecdisrec(rs.getString("fecha_disp_rec") );
                    rep.setNomorirec(rs.getString("orirec") );
                    rep.setNomDesrec(rs.getString("desrec") );
                    rep.setFecasigrec(rs.getString("fecha_asig_rec") );
                    rep.setStd_job_desc_rec(rs.getString("std_job_desc_req"));
                    rep.setNombrecond(rs.getString("nombre"));
                    historial.add(rep);
                    
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR BUSCANDO EL HISTORIAL DE UNA PLACA.." + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    
     /**
      * Getter for property historial.
      * @return Value of property historial.
      */
     public java.util.Vector getHistorial() {
         return historial;
     }     
    
     /**
      * Setter for property historial.
      * @param historial New value of property historial.
      */
     public void setHistorial(java.util.Vector historial) {
         this.historial = historial;
     }
     public String  wgroupPlaca (String placa)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        ResultSet rs1 = null;
        PoolManager poolManager = null;
        String wgroup="";
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                
                st = con.prepareStatement("select * from wgroup_placa where placa =?");
                st.setString(1,placa);
              //  ////System.out.println(st);
                
                rs = st.executeQuery();
                while(rs.next()){
                  wgroup =wgroup+rs.getString("work_group")+",";   
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR BUSCANDO EL WORK GROUP DE UNA PLACA.." + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return wgroup;
        
    }
     
}
