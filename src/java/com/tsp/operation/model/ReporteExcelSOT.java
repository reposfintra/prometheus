/*
* Nombre        ReporteSOT.java
* Descripci�n   Clase para implementar las configuraciones tipicas de un reporte en excel de SOT.
* Autor         Alejandro Payares
* Fecha         6 de octubre de 2005, 09:37 AM
* Versi�n       1.0
* Coyright      Transportes Sanchez Polo S.A.
*/

package com.tsp.operation.model;

import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;

/**
 * Clase para implementar las configuraciones tipicas de un reporte en excel de SOT.
 * @author  Alejandro Payares
 */
public abstract class ReporteExcelSOT extends DocumentoExcel {
    
    /**
     * El estilo de letra y colores para las celdas que muestran datos normales.
     * Es el estilo por default de una hoja de excel
     */
    protected   HSSFCellStyle  estiloNormal;
    
    /**
     * Es el mismo estilo normal pero la celda tiene ajuste de linea o wrap
     */
    protected   HSSFCellStyle  estiloNormalWrapped;
    
    /**
     * El mismo estilo normal sin ajuste de linea y con letra m�s grande (12 puntos).
     */
    protected   HSSFCellStyle  estiloNormalGrande;
    
    /**
     * Estilo de celda con fondo azul.
     */
    protected   HSSFCellStyle  estiloAzul;
    
    /**
     * Estilo de celda con fondo azul y letra grande (12 puntos).
     */
    protected   HSSFCellStyle  estiloAzulGrande;
    
    /**
     * Estilo de celda con fondo amarillo.
     */
    protected   HSSFCellStyle  estiloAmarillo;
    
    /**
     * Estilo de celda con fondo amarillo y letra grande (12 puntos).
     */
    protected   HSSFCellStyle  estiloAmarilloGrande;
    
    /**
     * Estilo de celda con fondo verde.
     */
    protected   HSSFCellStyle  estiloVerde;
    
    /**
     * Estilo de celda con fondo verde y letra grande (12 puntos).
     */
    protected   HSSFCellStyle  estiloVerdeGrande;
    
    /**
     * Estilo de celda con fondo rojo.
     */
    protected   HSSFCellStyle  estiloRojo;
    
    /**
     * Estilo de celda con fondo rojo y letra grande (12 puntos).
     */
    protected   HSSFCellStyle  estiloRojoGrande;
    
    /**
     * Estilo de celda con fondo resaltado.
     */
    protected   HSSFCellStyle  estiloResaltado;
    
    /**
     * Estilo de celda fondo blanco con color de letra rojo
     */
    protected   HSSFCellStyle  estiloLetraRoja;
    
    /** Creates a new instance of ReporteSOT */
    public ReporteExcelSOT() {
    }
    
    /**
     * Modifica la paleta por default de una hoja de excel para crear los colores
     * usados en los reportes de SOT.
     */
    protected void crearColores(){
        HSSFPalette palette = libroExcel.getCustomPalette();
        
        /* azul = 175,219,246
         * amarillo = 255,255,185
         * verde = 200,247,213
         * fondo = 236,233,216
         */
        //REEMPLAZAMOS EL AZUL STANDAR DE LA PALETA POR EL DEL REPORTE
        palette.setColorAtIndex(HSSFColor.BLUE.index,
        (byte) 175,  //RGB red (0-255)
        (byte) 219,    //RGB green
        (byte) 246     //RGB blue
        );
        
        // EL AMARILLO
        palette.setColorAtIndex(HSSFColor.YELLOW.index,
        (byte) 255,  //RGB red (0-255)
        (byte) 255,    //RGB green
        (byte) 185     //RGB blue
        );
        // EL VERDE
        palette.setColorAtIndex(HSSFColor.GREEN.index,
        (byte) 200,  //RGB red (0-255)
        (byte) 247,    //RGB green
        (byte) 213     //RGB blue
        );
        // EL MARRON
        palette.setColorAtIndex(HSSFColor.BROWN.index,
        (byte) 236,  //RGB red (0-255)
        (byte) 233,    //RGB green
        (byte) 216     //RGB blue
        );
        
        // EL ROJO
        palette.setColorAtIndex(HSSFColor.RED.index,
        (byte) 255,  //RGB red (0-255)
        (byte) 136,    //RGB green
        (byte) 136     //RGB blue
        );
        
        // HACEMOS COPIA DEL ROJO A OTRO COLOR MENOS USADO PARA PODER USAR EL ROJO VIVO
        palette.setColorAtIndex(HSSFColor.CORNFLOWER_BLUE.index,
        (byte) 255,  //RGB red (0-255)
        (byte) 0,    //RGB green
        (byte) 0     //RGB blue
        );
    }
    
    /**
     * Crea los estilos usados por los reportes de SOT.
     */
    public void crearEstilos(){
        HSSFFont font = libroExcel.createFont();
        font.setFontHeightInPoints((short)10);        //tamano letra
        font.setFontName("Arial");                    //Tipo letra
        font.setColor(HSSFColor.BLACK.index);                    //color letra negra
        
        estiloNormal = libroExcel.createCellStyle();
        this.configurarEstilo(estiloNormal, font, false, HSSFColor.BROWN.index);
        
        estiloNormalWrapped = super.clonarEstilo(estiloNormal);
        estiloNormalWrapped.setWrapText(true);
        
        estiloAzul = libroExcel.createCellStyle();
        this.configurarEstilo(estiloAzul, font, true, HSSFColor.BLUE.index);
        
        estiloAmarillo = libroExcel.createCellStyle();
        this.configurarEstilo(estiloAmarillo, font, true, HSSFColor.YELLOW.index);
        
        estiloVerde = libroExcel.createCellStyle();
        this.configurarEstilo(estiloVerde, font, true, HSSFColor.GREEN.index);
        
        estiloRojo = libroExcel.createCellStyle();
        this.configurarEstilo(estiloRojo, font, true, HSSFColor.RED.index);
        
        font = libroExcel.createFont();
        font.setFontHeightInPoints((short)12);        //tamano letra
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        
        estiloNormalGrande = libroExcel.createCellStyle();
        this.configurarEstilo(estiloNormalGrande, font, true, HSSFColor.BROWN.index);
        
        estiloAzulGrande = libroExcel.createCellStyle();
        this.configurarEstilo(estiloAzulGrande, font, true, HSSFColor.BLUE.index);
        
        estiloAmarilloGrande = libroExcel.createCellStyle();
        this.configurarEstilo(estiloAmarilloGrande, font, true, HSSFColor.YELLOW.index);
        
        estiloVerdeGrande = libroExcel.createCellStyle();
        this.configurarEstilo(estiloVerdeGrande, font, true, HSSFColor.GREEN.index);
        
        estiloRojoGrande = libroExcel.createCellStyle();
        this.configurarEstilo(estiloRojoGrande, font, true, HSSFColor.RED.index);
        
        font = libroExcel.createFont();
        font.setFontHeightInPoints((short)12);        //tamano letra
        font.setFontName("Arial");                    //Tipo letra
        font.setColor(HSSFColor.WHITE.index);                    //color letra blanca
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        estiloResaltado = libroExcel.createCellStyle();
        configurarEstilo(estiloResaltado, font, true, HSSFColor.RED.index);
        
        estiloLetraRoja = libroExcel.createCellStyle();
        font = libroExcel.createFont();
        font.setFontHeightInPoints((short)10);        //tamano letra
        font.setFontName("Arial");                    //Tipo letra
        font.setColor(HSSFColor.CORNFLOWER_BLUE.index); 
        configurarEstilo(estiloLetraRoja, font, false, HSSFColor.WHITE.index);
    }
    
    /**
     * Metodo abstracto que debe ser implementado en las clases hijas. En este m�todo se deben
     * crear los datos de los titulos del reporte, que var�an en cada reporte.
     * @throws Exception si algun error ocurre
     */    
    public abstract void crearTitulos() throws Exception;
}
