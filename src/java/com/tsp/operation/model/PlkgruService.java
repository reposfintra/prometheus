/*
 * PlkgruService.java
 *
 * Created on 25 de noviembre de 2004, 11:09 AM
 */

package com.tsp.operation.model;

/**
 *
 * @author  KREALES
 */
import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;

public class PlkgruService {
    
    private PlkgruDAO plkgru;
    
    /** Creates a new instance of PlkgruService */
    public PlkgruService() {
        plkgru = new PlkgruDAO();
    }
    
    public Plkgru getPlkgru()throws SQLException{
        
        return plkgru.getPlkgru();
        
    }
    public void buscaPlkgru(String pla, String sj)throws SQLException{
        try{
            plkgru.searchPlkgr(pla,sj);
        }
        catch(SQLException e){
            throw new SQLException(e.getMessage());
        }
    }
    
    
}
