/*
 * PlkgruDAO.java
 *
 * Created on 25 de noviembre de 2004, 10:56 AM
 */

package com.tsp.operation.model;

/**
 *
 * @author  KREALES
 */
import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;

public class PlkgruDAO {
    
    private Plkgru plkgr;
    /** Creates a new instance of PlkgruDAO */
    public PlkgruDAO() {
    }
    
    public void searchPlkgr(String placa, String sj )throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        plkgr= null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("SELECT sj, supplier, group_code FROM plkgru where sj=? and supplier=?");
                st.setString(1,sj);
                st.setString(2,placa);
                rs = st.executeQuery();
                if(rs.next()){
                    plkgr=Plkgru.load(rs);
                    
                }
                
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LA PLKGRU " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
        
    }
    
    public Plkgru getPlkgru(){
        return this.plkgr;
    }
}
