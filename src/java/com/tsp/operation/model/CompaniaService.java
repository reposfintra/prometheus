/*
 * CompaniaService.java
 *
 * Created on 20 de enero de 2005, 04:52 PM
 */

package com.tsp.operation.model;

import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
/**
 *
 * @author  KREALES
 */
public class CompaniaService {
    
    private CompaniaDAO cia;
    /** Creates a new instance of CompaniaService */
    public CompaniaService() {
        cia = new CompaniaDAO();
    }
    public CompaniaService(String dataBaseName) {
        cia = new CompaniaDAO(dataBaseName);
    }
    
    public List getList()throws SQLException{
        cia.listar();
        return cia.getCompa�ias();
    }
    
    public Compania getCompania(){
        //System.out.println("get compa�ia "+cia.getCia().getMoneda());
        return cia.getCia();
    }
    
    public void buscarCia(String distrito)throws SQLException{
        cia.buscar(distrito);
    }
    
    public void buscarLista()throws SQLException{
        cia.listar();
    }
    
    public void setCia(Compania compania)throws SQLException{
        cia.setCia(compania);
    }
    
    public void insertCia(Compania compania)throws SQLException{
        cia.setCia(compania);
        cia.insert();
        
    }
    public void updateCia(Compania compania)throws SQLException{
        cia.setCia(compania);
        cia.update();
        
    }
    public void anularCia(Compania compania)throws SQLException{
            cia.setCia(compania);
            cia.anularCompa�ia();
    }
    
    // LOS SIGUIENTES M�TODOS Y/O ATRIBUTOS FUERON COPIADOS DE LA APLICACI�N SOT
    // PARA IMPLEMENTAR EL MODULO DE ADMINISTRACI�N DE USUARIOS.
    // Alejandro Payares - nov 21 de 2005 - 3:33 pm
    
    public TreeMap getCbxCia(){
        return cia.getCbxCia();
    }
    
    public void setCbxCia(TreeMap cias){
        cia.setCbxCia(cias);
    }
    
    public void listarDistritos() throws SQLException{
        cia.listarDistritos();
    }
    
    public TreeMap getBases(){
        return cia.getBases();
    }
    
    public void listarBases() throws SQLException{
        cia.listarBases();
    }
    public float maxValorMercancia(String dstrct) throws SQLException{
        return cia.maxValorMercancia(dstrct);
    }
    
}
