//*************************************** clase modificada
package com.tsp.operation.model;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.swing.*;
import com.tsp.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;


public class RegistroAsignacionDAO {
    
    
    private static final String sqlCrearRegistroDeAsignacion =
    "insert into registro_asignacion "+
    "(distri_rec,placa_rec,fecha_disp_rec,reg_status_rec,numpla_rec,origen_rec,destino_rec,tipo_recurso_rec,"+
    "recurso_rec,fecha_asig_rec,tiempo_rec,tipo_asig_rec,cod_cliente_rec,equipo_aso_rec,tipo_aso_rec,distri_req,"+
    "estado_req,num_sec_req,std_job_no_req,numpla_req,fecha_disp_req,cliente_req,origen_req,destino_req,clase_req,"+
    "tipo_rec1_req,tipo_rec2_req,tipo_rec3_req,tipo_rec4_req,tipo_rec5_req,recurso1_req,recurso2_req,recurso3_req,"+
    "recurso4_req,recurso5_req,prioridad1_req,prioridad2_req,prioridad3_req,prioridad4_req,prioridad5_req,"+
    "tipo_asing_req,fecha_asing_req,nuevafecha_dispo_req,id_rec_cab_req,id_rec_tra_req,fecha_posibleentrega_req,"+
    "nuevo_origen,creation_user,user_update,std_job_desc_req,std_job_desc_rec)"+
    " values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
    
    private static final String SQL_CREAR_NUEVO_RECURSO_SOLO =
    "insert into registro_asignacion "+
    "(distri_rec,placa_rec,fecha_disp_rec,reg_status_rec,numpla_rec,origen_rec,destino_rec,tipo_recurso_rec,"+
    "recurso_rec,fecha_asig_rec,tiempo_rec,tipo_asig_rec,equipo_aso_rec,tipo_aso_rec,fecha_disp_req,std_job_desc_rec)"+
    " values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
    
    private static final String SQL_DESASIGNAR_RECURSO_DE_REGISTRO= "update registro_asignacion set "+
    "fecha_asing_req = '0099-01-01 00:00:00', fecha_asig_rec = '0099-01-01 00:00:00',estado_req = '',num_sec_req = 0,std_job_no_req = '',numpla_req = '',cliente_req = '',origen_req = '',destino_req = '',clase_req = '',"+
    "tipo_rec1_req = '',tipo_rec2_req = '',tipo_rec3_req = '',tipo_rec4_req = '',tipo_rec5_req = '',recurso1_req = '',recurso2_req = '',recurso3_req = '',"+
    "recurso4_req = '',recurso5_req = '',prioridad1_req = '',prioridad2_req = '',prioridad3_req = '',prioridad4_req = '',prioridad5_req = '',"+
    "tipo_asing_req = '',id_rec_cab_req = '',id_rec_tra_req = '',std_job_desc_req = '' where distri_rec = ? and placa_rec = ? and fecha_disp_rec = ?";//modif-distrito
    private static final String SQL_ELIMINAR_RECURSOS_POSTERIORES = "delete from recursos_disp where dstrct = ? and placa = ? and fecha_disp > ?";
    private static final String SQL_DESASIGNAR_REQUERIMIENTO_DE_REGISTRO = "update registro_asignacion set distri_rec = '',placa_rec = ''," +
    "fecha_asing_req = '0099-01-01 00:00:00', fecha_asig_rec = '0099-01-01 00:00:00',reg_status_rec = '',numpla_rec = '',origen_rec = '',destino_rec = '',tipo_recurso_rec = '',"+
    "recurso_rec = '',tipo_asig_rec = '',cod_cliente_rec = '',equipo_aso_rec = '',tipo_aso_rec = '',std_job_desc_rec = '' where " +
    "distri_req = ? and num_sec_req = ? and std_job_no_req = ? and numpla_req = ? and fecha_disp_req = ?";
    
    
    private static final String SQL_DUPLICAR_RECURSOS_SOLOS_ASIGNADOS =
    "INSERT INTO registro_asignacion ( distri_rec, placa_rec, fecha_disp_rec, reg_status_rec, numpla_rec, origen_rec, destino_rec,"+
    "tipo_recurso_rec, recurso_rec, fecha_asig_rec, tiempo_rec, tipo_asig_rec, cod_cliente_rec, equipo_aso_rec, tipo_aso_rec,distri_req, "+
    "estado_req, num_sec_req, std_job_no_req, numpla_req, fecha_disp_req, cliente_req, origen_req, destino_req, tipo_rec1_req, "+
    "tipo_rec2_req, tipo_rec3_req, tipo_rec4_req, tipo_rec5_req, recurso1_req, recurso2_req, recurso3_req, recurso4_req, recurso5_req, "+
    "prioridad1_req, prioridad2_req, prioridad3_req, prioridad4_req, prioridad5_req, tipo_asing_req, fecha_asing_req, nuevafecha_dispo_req, "+
    "id_rec_cab_req, id_rec_tra_req, std_job_desc_req,std_job_desc_rec, "+
    "creation_user, creation_date, user_update, last_update ) (select distri_rec, placa_rec, fecha_disp_rec, reg_status_rec, numpla_rec, " +
    "origen_rec, destino_rec,tipo_recurso_rec, recurso_rec, fecha_asig_rec, tiempo_rec, tipo_asig_rec, cod_cliente_rec, equipo_aso_rec," +
    " tipo_aso_rec,'', '', 0, '', '', fecha_disp_rec, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', "+
    "'0099-01-01 00:00:00', '0099-01-01 00:00:00', '', '', '0099-01-01 00:00:00', '', agasoc, fecha_dispxag, b.std_job_desc  as dessttd,"+
    "'', 'ADMIN', 'now()', '', 'now()' from req_cliente,stdjob where estado = '' and req_cliente.std_job_no = stdjob.std_job_no)";
    
    private final static String SQL_LIMPIAR_RECURSOS_DE_REGISTRO = "update registro_asignacion set estado_req = '',distri_rec = '',placa_rec = ''," +
    "fecha_asing_req = '0099-01-01 00:00:00', fecha_asig_rec = '0099-01-01 00:00:00', reg_status_rec = '',numpla_rec = '',origen_rec = '',destino_rec = '',tipo_recurso_rec = '',"+
    "recurso_rec = '',tipo_asig_rec = '',cod_cliente_rec = '',equipo_aso_rec = '',tipo_aso_rec = '', std_job_desc_rec = '' where " +
    "std_job_no_req <> '' and distri_rec = ? and placa_rec = ? and fecha_disp_rec >= ?";
    private static final String SQL_ELIMINAR_ASIGNACIONES = "delete from registro_asignacion where placa_rec = '' and std_job_no_req = ''";
    private static final String SQL_DESASIGNAR_RECURSO= "update recursos_disp set reg_status = '',tipo_asig = '', fecha_asig = '0099-01-01 00:00:00', last_update = 'now()', dstrct_code_req = '', num_sec_req = 0, std_job_no_req = '', numpla_req = '', fecha_dispo_req = '0099-01-01 00:00:00', cod_cliente = '', std_job_desc_rec = '' where dstrct = ? and placa = ? and fecha_disp = ?";
    private static final String SQL_DESASIGNAR_REQUERIMIENTO = "update req_cliente set estado = '', fecha_asing = '0099-01-01 00:00:00', fecha_posibleentrega = '0099-01-01 00:00:00', fecha_actualizacion = 'now()', std_job_desc_req = '' where dstrct_code = ? and num_sec = ? and std_job_no = ? and fecha_dispo = ? and numpla = ?";
    
    private static final String SQL_OBTENER_RECURSO_DE_REGISTRO = "select * from recursos_disp where dstrct= ? and placa = ? and fecha_disp = ?";
    
    private static final String SQL_OBTENER_REQUERIMIENTO_DE_REGISTRO = "select * from registro_asignacion  where " +
    "distri_req = ? and num_sec_req = ? and std_job_no_req = ? and numpla_req = ? and fecha_disp_req = ?";
    private static final String SQL_ASIGNAR_RECURSO_A_REQUERIMIENTO = "update registro_asignacion set distri_rec = ?,placa_rec = ?," +
    "fecha_disp_rec = ?,reg_status_rec = ?,numpla_rec = ?,origen_rec = ?,destino_rec = ?,tipo_recurso_rec = ?,"+
    "recurso_rec = ?,tipo_asig_rec = ?,cod_cliente_rec = ?,equipo_aso_rec = ?,tipo_aso_rec = ?,fecha_asig_rec = 'now()', std_job_desc_rec = ? where " +
    "distri_req = ? and num_sec_req = ? and std_job_no_req = ? and numpla_req = ? and fecha_disp_req = ?";
    
    private static final String SQL_GUARDAR_REQUERIMIENTOS_NO_ASIGNADOS = "INSERT INTO registro_asignacion ( distri_rec, placa_rec, fecha_disp_rec, reg_status_rec, numpla_rec, origen_rec, destino_rec,"+
    "tipo_recurso_rec, recurso_rec, fecha_asig_rec, tiempo_rec, tipo_asig_rec, cod_cliente_rec, equipo_aso_rec, tipo_aso_rec,distri_req, "+
    "estado_req, num_sec_req, std_job_no_req, numpla_req, fecha_disp_req, cliente_req, origen_req, destino_req, tipo_rec1_req, "+
    "tipo_rec2_req, tipo_rec3_req, tipo_rec4_req, tipo_rec5_req, recurso1_req, recurso2_req, recurso3_req, recurso4_req, recurso5_req, "+
    "prioridad1_req, prioridad2_req, prioridad3_req, prioridad4_req, prioridad5_req, tipo_asing_req, fecha_asing_req, nuevafecha_dispo_req, "+
    "id_rec_cab_req, id_rec_tra_req, std_job_desc_req ) "+
    "(select '', '', fecha_dispo, '', '', '', '', '', '', '0099-01-01 00:00:00', 0.0, "+
    "'', '', '', '',req_cliente.dstrct_code, "+
    "estado, num_sec, req_cliente.std_job_no, numpla, fecha_dispo, cliente, origen, destino, tipo_rec1, "+
    "tipo_rec2, tipo_rec3, tipo_rec4, tipo_rec5, req_cliente.recurso1, req_cliente.recurso2, req_cliente.recurso3, req_cliente.recurso4, req_cliente.recurso5, "+
    "req_cliente.prioridad1, req_cliente.prioridad2, req_cliente.prioridad3, req_cliente.prioridad4, req_cliente.prioridad5, tipo_asing, fecha_asing, nuevafecha_dispo, "+
    "id_rec_cab, id_rec_tra, std_job_desc from req_cliente,stdjob where estado = '' and req_cliente.std_job_no = stdjob.std_job_no)";
    
    
    private static final String SQL_GUARDAR_RECURSOS_NO_ASIGNADOS = " INSERT INTO registro_asignacion (distri_rec, placa_rec, fecha_disp_rec, reg_status_rec," +
    " numpla_rec, origen_rec, destino_rec, tipo_recurso_rec, recurso_rec, fecha_asig_rec, tiempo_rec, " +
    "tipo_asig_rec, cod_cliente_rec, equipo_aso_rec, tipo_aso_rec,std_job_desc_rec,  distri_req,estado_req, num_sec_req, " +
    "std_job_no_req, numpla_req, fecha_disp_req, cliente_req, origen_req, destino_req, clase_req, " +
    "tipo_rec1_req,tipo_rec2_req, tipo_rec3_req, tipo_rec4_req, tipo_rec5_req, recurso1_req, " +
    "recurso2_req, recurso3_req, recurso4_req, recurso5_req, prioridad1_req, prioridad2_req, " +
    "prioridad3_req, prioridad4_req, prioridad5_req, tipo_asing_req, fecha_asing_req," +
    " nuevafecha_dispo_req,id_rec_cab_req, id_rec_tra_req, fecha_posibleentrega_req, " +
    "nuevo_origen, agasoc, fecha_dispxag, std_job_desc_req,  creation_user, creation_date, user_update, last_update) " +
    "(select a.*, '', '', 0, '', '', a.fecha_disp, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '0099-01-01 00:00:00', '0099-01-01 00:00:00', '', '', '0099-01-01 00:00:00', '', '', '0099-01-01 00:00:00','', 'ADMIN', 'now()', '', 'now()' " +
    "from" +
    " (select a.dstrct, a.placa, a.fecha_disp, a.reg_status, numpla, origen, destino,  tiporecurso, recurso, fecha_asig, tiempo, tipo_asig, cod_cliente,  equipo_aso, tipo_aso, coalesce(b.std_job_desc,'') " +
    "from recursos_disp a left join stdjob b on(b.std_job_no = a.std_job_no_rec)  where a.reg_status = '')a) " ;
    
    
    //KREALES
    
    private static final String SQL_INFORME="";
    
    private static final String SQL_BORRAR="Delete from registro_asignacion";
    
    public RegistroAsignacionDAO() {
    }
    
    public void guardarRegistroAsignacion(RegistroAsignacion reg) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( "fintra" );
            if ( con != null ){
                ////System.out.println("guardando asignacion...");
                ps = con.prepareStatement(sqlCrearRegistroDeAsignacion);
                ps.setString(1,reg.getDistri_rec());
                ps.setString(2,reg.getPlaca_rec());
                ps.setString(3,reg.getFecha_disp_rec());
                ps.setString(4,reg.getReg_status_rec());
                ps.setString(5,reg.getNumpla_rec());
                ps.setString(6,reg.getOrigen_rec());
                ps.setString(7,reg.getDestino_rec());
                ps.setString(8,reg.getTipo_recurso_rec());
                ps.setString(9,reg.getRecurso_rec());
                ps.setString(10,reg.getFecha_asig_rec());
                ps.setFloat(11,reg.getTiempo_rec());
                ps.setString(12,reg.getTipo_asig_rec());
                ps.setString(13,reg.getCod_cliente_rec());
                ps.setString(14,reg.getEquipo_aso_rec());
                ps.setString(15,reg.getTipo_aso_rec());
                ps.setString(16,reg.getDistri_req());
                ps.setString(17,reg.getEstado_req());
                ps.setInt(18,reg.getNum_sec_req());
                ps.setString(19,reg.getStd_job_no_req());
                ps.setString(20,reg.getNumpla_req());
                ps.setString(21,reg.getFecha_disp_req());
                ps.setString(22,reg.getCliente_req());
                ps.setString(23,reg.getOrigen_req());
                ps.setString(24,reg.getDestino_req());
                ps.setString(25,reg.getClase_req());
                ps.setString(26,reg.getTipo_rec1_req());
                ps.setString(27,reg.getTipo_rec2_req());
                ps.setString(28,reg.getTipo_rec3_req());
                ps.setString(29,reg.getTipo_rec4_req());
                ps.setString(30,reg.getTipo_rec5_req());
                ps.setString(31,reg.getRecurso1_req());
                ps.setString(32,reg.getRecurso2_req());
                ps.setString(33,reg.getRecurso3_req());
                ps.setString(34,reg.getRecurso4_req());
                ps.setString(35,reg.getRecurso5_req());
                ps.setString(36,reg.getPrioridad1_req());
                ps.setString(37,reg.getPrioridad2_req());
                ps.setString(38,reg.getPrioridad3_req());
                ps.setString(39,reg.getPrioridad4_req());
                ps.setString(40,reg.getPrioridad5_req());
                ps.setString(41,reg.getTipo_asing_req());
                ps.setString(42,reg.getFecha_asing_req());
                ps.setString(43,reg.getNuevafecha_dispo_req());
                ps.setString(44,reg.getId_rec_cab_req());
                ps.setString(45,reg.getId_rec_tra_req());
                ps.setString(46,reg.getFecha_posibleentrega_req());
                ps.setString(47,reg.getNuevo_origen());
                ps.setString(48,reg.getCreation_user());
                ps.setString(49,reg.getUser_update());
                ps.setString(50, reg.getStd_job_desc_req());
                ps.setString(51, reg.getStd_job_desc_rec());
                ////System.out.println(ps);
                ps.execute();
                ////System.out.println("asignaci�n guardada...!");
            }
        }catch(Exception ex){
            ex.printStackTrace();
            throw new SQLException("ERROR AL GUARDAR REGISTRO DE ASIGNACION: "+ex.getMessage());
        }
        finally{
            if (ps != null){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    //KAREN REALES
    public void generarReporte(String fi, String ff) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( "fintra" );
            if ( con != null ){
                ps = con.prepareStatement(this.SQL_INFORME);
                ps.setString(1, fi);
                ps.setString(2, ff);
                rs= ps.executeQuery();
                
            }
        }catch(Exception ex){
            throw new SQLException("ERROR AL GENERAR EL INFORME DE PROGRAMACION Y EJECUCION DIARIA DE LA OPERACION : "+ex.getMessage());
        }
        finally{
            if (ps != null){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    
    /**
     * desasignarRecursosYRequerimientos
     *
     * @param recurso Recursosdisp
     * @param requerimiento ReqCliente
     */
    public void desasignarRecursosYRequerimientos( Recursosdisp recurso, ReqCliente requerimiento, boolean reasignar ) throws SQLException {
        Connection con = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            ////System.out.println("Desasignando recurso: "+recurso+" y\n requerimiento: "+requerimiento);
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( "fintra" );
            if ( con != null ){
                if ( reasignar && recurso != null && requerimiento != null ){
                    //1� Limpiamos el registro del recurso y los posteriores
                    ps = con.prepareStatement(SQL_LIMPIAR_RECURSOS_DE_REGISTRO);
                    ps.setString(1, recurso.getDistrito());
                    ps.setString(2, recurso.getPlaca());
                    ps.setString(3, recurso.getaFecha_disp());
                    ////System.out.println("query LIMPIAR recurso seleccionado: "+ps);
                    ps.execute();
                    
                    //2� Buscamos el recurso asociado actualmente al requerimiento
                    ps = con.prepareStatement( SQL_OBTENER_REQUERIMIENTO_DE_REGISTRO );
                    ps.setString( 1, requerimiento.getdstrct_code() );
                    ps.setInt( 2, requerimiento.getnum_sec() );
                    ps.setString( 3, requerimiento.getstd_job_no() );
                    ps.setString( 4, requerimiento.getnumpla() );
                    ps.setString( 5, requerimiento.getfecha_dispo() );
                    ////System.out.println("query obtener requerimiento: "+ps);
                    rs = ps.executeQuery();
                    if ( rs.next() ) {// si existe el recurso asociado
                        Recursosdisp r = this.crearRecurso( rs );
                        //3� Limpiamos el registro del recurso asociado y los posteriores
                        ps = con.prepareStatement(SQL_LIMPIAR_RECURSOS_DE_REGISTRO);
                        ps.setString(1, r.getDistrito());
                        ps.setString(2, r.getPlaca());
                        ps.setString(3, r.getaFecha_disp());
                        ////System.out.println("query LIMPIAR recurso solo: "+ps);
                        ps.execute();
                        //4� Creamos un nuevo registro para el recurso que qued� solo
                        crearNuevoRecursoSolo(con,ps,r);
                    }
                    //3 � 5� Asociamos el recurso al requerimiento
                    ps = con.prepareStatement(SQL_ASIGNAR_RECURSO_A_REQUERIMIENTO);
                    ps.setString(1, recurso.getDistrito());
                    ps.setString(2, recurso.getPlaca());
                    ps.setString(3, recurso.getaFecha_disp());
                    ps.setString(4, "M");
                    ps.setString(5, recurso.getNumpla());
                    ps.setString(6, recurso.getOrigen());
                    ps.setString(7, recurso.getDestino());
                    ps.setString(8, recurso.getClase());
                    ps.setString(9, recurso.getTipo());
                    ps.setString(10, "M");
                    ps.setString(11, requerimiento.getcliente());
                    ps.setString(12, recurso.getEquipo_aso());
                    ps.setString(13, recurso.getTipo_aso());
                    //14 *******************************************************************************
                    
                    PreparedStatement st = con.prepareStatement("select std_job_desc,stdjob.std_job_no as numero from planilla,plarem,remesa,stdjob where planilla.numpla = ? and planilla.numpla = plarem.numpla and plarem.numrem = remesa.numrem and remesa.std_job_no = stdjob.std_job_no");
                    st.setString(1, recurso.getNumpla());
                    rs = st.executeQuery();
                    ps.setString(14, rs.next()? (rs.getString("std_job_desc").length()>1?rs.getString("std_job_desc"):rs.getString("numero")):"(estandar no encontrado)");
                    
                    
                    ps.setString(15, requerimiento.getdstrct_code());
                    ps.setInt(16, requerimiento.getnum_sec());
                    ps.setString(17, requerimiento.getstd_job_no());
                    ps.setString(18, requerimiento.getnumpla());
                    ps.setString(19, requerimiento.getfecha_dispo());
                    ////System.out.println("Query asignar recurso: "+ps);
                    ps.execute();
                    
                    
                }
                else if ( !reasignar && recurso != null && requerimiento != null){
                    //1� Limpiamos el registro del recurso y los posteriores
                    ps = con.prepareStatement(SQL_LIMPIAR_RECURSOS_DE_REGISTRO);
                    ps.setString(1, recurso.getDistrito());
                    ps.setString(2, recurso.getPlaca());
                    ps.setString(3, recurso.getaFecha_disp());
                    ////System.out.println("query LIMPIAR recursoS: "+ps);
                    ps.execute();
                    
                    crearNuevoRecursoSolo(con,ps,recurso);
                }
                //distri_rec = ? and placa_rec = ? and fecha_disp_rec = ?
                /*if ( recurso != null ){
                    ps = con.prepareStatement(SQL_DESASIGNAR_RECURSO_DE_REGISTRO);
                    ps.setString(1, recurso.getDistrito());
                    ps.setString(2, recurso.getPlaca());
                    ps.setString(3, recurso.getaFecha_disp());
                    ////System.out.println("query desasignar recurso: "+ps);
                    ps.execute();
                    /*ps = con.prepareStatement(SQL_ELIMINAR_RECURSOS_POSTERIORES);
                    ps.setString(1, recurso.getDistrito());
                    ps.setString(2, recurso.getPlaca());
                    ps.setString(3, recurso.getaFecha_disp());
                    ////System.out.println("Query eliminar recursos: "+ps);
                    ps.execute();
                    ps = con.prepareStatement(SQL_ELIMINAR_ASIGNACIONES);
                    ps.setString(1, recurso.getDistrito());
                    ps.setString(2, recurso.getPlaca());
                    ps.setTimestamp(3, recurso.getFecha_disp());
                    ////System.out.println("Query eliminar asignaciones: "+ps);
                    ps.execute();*/
                /*}
                if ( requerimiento != null ){
                    ////distri_req = ? and num_sec_req = ? and std_job_no_req = ? and numpla_req = ? and fecha_disp_req = ?
                    ps = con.prepareStatement(SQL_DESASIGNAR_REQUERIMIENTO_DE_REGISTRO);
                    ps.setString(1,requerimiento.getdstrct_code());
                    ps.setInt(2,requerimiento.getnum_sec());
                    ps.setString(3,requerimiento.getstd_job_no());
                    ps.setString(4,requerimiento.getnumpla());
                    ps.setString(5,requerimiento.getfecha_dispo());
                    ps.execute();
                }*/
                ps = con.prepareStatement(SQL_ELIMINAR_ASIGNACIONES);
                ////System.out.println("Query eliminar asignaciones: "+ps);
                ps.execute();
            }
        }catch(Exception ex){
            ex.printStackTrace();
            throw new SQLException("ERROR AL GENERAR EL INFORME DE PROGRAMACION Y EJECUCION DIARIA DE LA OPERACION : "+ex.getMessage());
        }
        finally{
            if (ps != null){
                try{
                    ps.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    public Recursosdisp obtenerRecursoDeRegistro(String distrito, String placa, String fechaDisponibilidad) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        Recursosdisp rd = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( "fintra" );
            
            if ( con != null ) {
                st = con.prepareStatement( SQL_OBTENER_RECURSO_DE_REGISTRO );
                st.setString(1, distrito);
                st.setString(2, placa);
                st.setString(3, fechaDisponibilidad);
                ////System.out.println("query recurso: "+st);
                rs = st.executeQuery();
                if ( rs.next() ) {
                    rd = crearRecurso(rs);
                /*    rd.setDstrct_code_req( rs.getString( "dstrct_code_req" ) );
                    rd.setNum_sec_req( rs.getInt( "num_sec_req" ) );
                    rd.setStd_job_no_req( rs.getString( "std_job_no_req" ) );
                    rd.setNumpla_req( rs.getString( "numpla_req" ) );
                    rd.setFecha_dispo_req( rs.getString( "fecha_dispo_req" ) );
                 */}
            }
           
        }
        catch ( SQLException e ) {
            e.printStackTrace();
            throw new SQLException(
            "ERROR DURANTE EL PROCESO DE LISTAR LOS RECURSOS DISPONIBLES " +
            e.getMessage() + " " + e.getErrorCode() );
        }
        finally {
            if ( st != null ) {
                try {
                    st.close();
                }
                catch ( SQLException e ) {
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO" +
                    e.getMessage() );
                }
            }
            
            if ( con != null ) {
                poolManager.freeConnection( "fintra", con );
            }
        }
        return rd;
    }
    
    //dstrct_code, num_sec, std_job_no, fecha_dispo, numpla
    public ReqCliente obtenerRequerimientoDeRegistro( String dstrct_code, String num_sec, String std_job_no,
    String fecha_dispo, String numpla ) throws SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        ReqCliente r = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( "fintra" );
            if ( con != null ) {
                st = con.prepareStatement( SQL_OBTENER_REQUERIMIENTO_DE_REGISTRO );
                st.setString( 1, dstrct_code );
                st.setString( 2, num_sec );
                st.setString( 3, std_job_no );
                st.setString( 4, numpla );
                st.setString( 5, fecha_dispo );
                ////System.out.println("query obtener requerimiento: "+st);
                rs = st.executeQuery();
                if ( rs.next() ) {
                    ////System.out.println("encontro el registro del requerimiento");
                    r = this.crearRequerimiento( rs );
                    ////System.out.println("requerimiento creado..."+r);
                }
                
            }
        }
        catch ( SQLException e ) {
            e.printStackTrace();
            
            throw new SQLException( "ERROR AL SELECCIONAR EN LA TABLA REQCLIENTE" +
            e.getMessage() + "" + e.getErrorCode() );
        }
        finally {
            if ( st != null ) {
                try {
                    st.close();
                }
                catch ( SQLException e ) {
                    throw new SQLException( "ERROR AL CERRAR EL ESTAMENTO" +
                    e.getMessage() );
                }
            }
            if ( con != null ) {
                poolManager.freeConnection( "fintra", con );
            }
        }
        return r;
    }
    
    private ReqCliente crearRequerimiento( ResultSet rs ) throws SQLException {
        ReqCliente r = new ReqCliente();
        r.setdstrct_code( rs.getString( "distri_req" ) );
        r.setestado( rs.getString( "estado_req" ) );
        r.setnum_sec( rs.getInt( "num_sec_req" ) );
        r.setstd_job_no( rs.getString( "std_job_no_req" ) );
        r.setnumpla( rs.getString( "numpla_req" ) );
        r.setfecha_dispo( rs.getString( "fecha_disp_req" ) );
        r.setcliente( rs.getString( "cliente_req" ) );
        r.setorigen( rs.getString( "origen_req" ) );
        r.setdestino( rs.getString( "destino_req" ) );
        r.setclase_req( rs.getString( "clase_req" ) );
        r.setTipo_recurso1( rs.getString( "tipo_rec1_req" ) );
        r.setTipo_recurso2( rs.getString( "tipo_rec2_req" ) );
        r.setTipo_recurso3( rs.getString( "tipo_rec3_req" ) );
        r.setTipo_recurso4( rs.getString( "tipo_rec4_req" ) );
        r.setTipo_recurso5( rs.getString( "tipo_rec5_req" ) );
        r.setrecurso1( rs.getString( "recurso1_req" ) );
        r.setrecurso2( rs.getString( "recurso2_req" ) );
        r.setrecurso3( rs.getString( "recurso3_req" ) );
        r.setrecurso4( rs.getString( "recurso4_req" ) );
        r.setrecurso5( rs.getString( "recurso5_req" ) );
        r.setprioridad1( rs.getString( "prioridad1_req" ) );
        r.setprioridad2( rs.getString( "prioridad2_req" ) );
        r.setprioridad3( rs.getString( "prioridad3_req" ) );
        r.setprioridad4( rs.getString( "prioridad4_req" ) );
        r.setprioridad5( rs.getString( "prioridad5_req" ) );
        r.setfecha_asign( rs.getString( "fecha_asing_req" ) );
        r.setnuevafecha_dispo( rs.getString( "nuevafecha_dispo_req" ) );
        r.setid_rec_cab( rs.getString( "id_rec_cab_req" ) );
        r.setid_rec_tra( rs.getString( "id_rec_tra_req" ) );
        r.setfecha_posibleentrega( rs.getString( "fecha_posibleentrega_req" ) );
        r.settipo_asign(rs.getString("tipo_asing_req"));
        return r;
    }
    
    private Recursosdisp crearRecurso(ResultSet rs) throws SQLException {
        Recursosdisp rd = new Recursosdisp();
        rd.setDistrito( rs.getString( "dstrct" ) );///modif -distrito
        rd.setPlaca( rs.getString( "placa" ) );
        rd.setaFecha_disp( rs.getString( "fecha_disp" ) );
        rd.setReg_status( rs.getString( "reg_status" ) );
        rd.setNumpla( rs.getString( "numpla" ) );
        rd.setOrigen( rs.getString( "origen" ) );
        rd.setDestino( rs.getString( "destino" ) );
        rd.setClase( rs.getString( "tiporecurso" ) );//modif -tipo_recurso-
        rd.setTipo( rs.getString( "recurso" ) );
        rd.setaFecha_asig( rs.getString( "fecha_asig" ) );
        rd.setTiempo( rs.getFloat( "tiempo" ) );
        rd.setTipo_asig( rs.getString( "tipo_asig" ) );
        rd.setCod_cliente( rs.getString( "cod_cliente" ) );
        rd.setEquipo_aso( rs.getString( "equipo_aso" ) );
        rd.setTipo_aso( rs.getString( "tipo_aso" ) );
        rd.setDstrct_code_req( rs.getString( "dstrct_code_req" ) );
        rd.setNum_sec_req( rs.getInt( "num_sec_req" ) );
        rd.setStd_job_no_req( rs.getString( "std_job_no_req" ) );
        rd.setNumpla_req( rs.getString( "numpla_req" ) );
        rd.setFecha_dispo_req( rs.getString( "fecha_dispo_req" ) );
        rd.setFecha_creacion( rs.getTimestamp( "creation_date" ) );
        rd.setFecha_actualizacion( rs.getTimestamp(
        "last_update" ) );
        rd.setUsuario_creacion( rs.getString( "creation_user" ) );
        rd.setUsuario_actualizacion( rs.getString(
        "user_update" ) );
        rd.setStd_job_no_rec(rs.getString("std_job_no_rec"));
        
        return rd;
    }
    
    private void crearNuevoRecursoSolo(Connection con, PreparedStatement ps, Recursosdisp recurso) throws SQLException {
        ps = con.prepareStatement(SQL_CREAR_NUEVO_RECURSO_SOLO);
        ps.setString(1, recurso.getDistrito());
        ps.setString(2, recurso.getPlaca());
        ps.setString(3, recurso.getaFecha_disp());
        ps.setString(4, "");
        ps.setString(5, recurso.getNumpla());
        ps.setString(6, recurso.getOrigen());
        ps.setString(7, recurso.getDestino());
        ps.setString(8, recurso.getClase());
        ps.setString(9, recurso.getTipo());
        ps.setString(10, "now()");
        ps.setDouble(11, recurso.getTiempo());
        ps.setString(12, "");
        ps.setString(13, recurso.getEquipo_aso());
        ps.setString(14, recurso.getTipo_aso());
        ps.setString(15, recurso.getaFecha_disp());
        PreparedStatement st = con.prepareStatement("select std_job_desc,stdjob.std_job_no as numero from planilla,plarem,remesa,stdjob where planilla.numpla = ? and planilla.numpla = plarem.numpla and plarem.numrem = remesa.numrem and remesa.std_job_no = stdjob.std_job_no");
        st.setString(1, recurso.getNumpla());
        ResultSet rs = st.executeQuery();
        ps.setString(16, rs.next()? (rs.getString("std_job_desc").length()>1?rs.getString("std_job_desc"):rs.getString("numero")):"(estandar no encontrado)");
        ////System.out.println("Creando nuevo recurso solo: "+ps);
        ps.execute();
    }
    
    public void insertarRegistrosNoAsignados() throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        ReqCliente r = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( "fintra" );
            if ( con != null ) {
                st = con.prepareStatement( "delete from registro_asignacion where reg_status_rec=''" );
                ////System.out.println("Borro todo lo q no esta asignado: "+st);
                st.executeUpdate();
                
                st = con.prepareStatement( SQL_GUARDAR_REQUERIMIENTOS_NO_ASIGNADOS );
                ////System.out.println("query requerimientos no asignados: "+st);
                st.executeUpdate();
                st = con.prepareStatement( SQL_GUARDAR_RECURSOS_NO_ASIGNADOS );
                ////System.out.println("query recursos no asignados: "+st);
                st.executeUpdate();
            }
        }
        catch ( SQLException e ) {
            e.printStackTrace();
            throw new SQLException( "ERROR AL SELECCIONAR EN LA TABLA REQCLIENTE" +
            e.getMessage() + "" + e.getErrorCode() );
        }
        finally {
            if ( st != null ) {
                try {
                    st.close();
                }
                catch ( SQLException e ) {
                    throw new SQLException( "ERROR AL CERRAR EL ESTAMENTO" +
                    e.getMessage() );
                }
            }
            if ( con != null ) {
                poolManager.freeConnection( "fintra", con );
            }
        }
    }
    
    
    public void limpiarAsignacion(Recursosdisp rec, ReqCliente req) throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        ReqCliente r = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( "fintra" );
            if ( con != null ) {
                st = con.prepareStatement( "Delete from registro_asignacion where placa_rec=?  and fecha_disp_rec=? and std_job_no_req=? and fecha_disp_req=?" );
                st.setString(1,rec.getPlaca());
                st.setString(2, rec.getaFecha_disp());
                st.setString(3, req.getstd_job_no());
                st.setString(4, req.getfecha_dispo());
                ////System.out.println("Limpiar 1: " +st.toString());
                st.executeUpdate();
                
                st = con.prepareStatement( "Delete from registro_asignacion where placa_rec=?  and fecha_disp_rec=? " );
                st.setString(1,rec.getPlaca());
                st.setString(2, rec.getaFecha_disp());
                ////System.out.println("Limpiar 2: " +st.toString());
                st.executeUpdate();
                
                st = con.prepareStatement( "Delete from registro_asignacion where  std_job_no_req=? and fecha_disp_req=?" );
                st.setString(1, req.getstd_job_no());
                st.setString(2, req.getfecha_dispo());
                ////System.out.println("Limpiar 3: " +st.toString());
                st.executeUpdate();
                
            }
        }
        catch ( SQLException e ) {
            e.printStackTrace();
            throw new SQLException( "ERROR AL SELECCIONAR EN LA TABLA REQCLIENTE" +
            e.getMessage() + "" + e.getErrorCode() );
        }
        finally {
            if ( st != null ) {
                try {
                    st.close();
                }
                catch ( SQLException e ) {
                    throw new SQLException( "ERROR AL CERRAR EL ESTAMENTO" +
                    e.getMessage() );
                }
            }
            if ( con != null ) {
                poolManager.freeConnection( "fintra", con );
            }
        }
    }
    /**
     * Metodo que borra toda la taba de registro_asignacion
     * @throws SQLException
     */
    public void limpiarAsignacionCompleta() throws SQLException{
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        ReqCliente r = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( "fintra" );
            if ( con != null ) {
                st = con.prepareStatement( SQL_BORRAR );
                st.executeUpdate();
            }
        }
        catch ( SQLException e ) {
            e.printStackTrace();
            throw new SQLException( "ERROR AL BORRAR LA PLANEACION" +
            e.getMessage() + "" + e.getErrorCode() );
        }
        finally {
            if ( st != null ) {
                try {
                    st.close();
                }
                catch ( SQLException e ) {
                    throw new SQLException( "ERROR AL CERRAR EL ESTAMENTO" +
                    e.getMessage() );
                }
            }
            if ( con != null ) {
                poolManager.freeConnection( "fintra", con );
            }
        }
    }
}
