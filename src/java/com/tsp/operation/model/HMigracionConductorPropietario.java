
/**
 *
 *Modificado: egonzalez2014
 */

package com.tsp.operation.model;

import com.tsp.util.*;
import java.io.*;
import java.sql.*;
import java.util.*;
import java.lang.*;
import com.tsp.operation.model.beans.*;

import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;


public class HMigracionConductorPropietario extends Thread{
    
    //____________________________________________________________________________________________
    //                                          ATRIBUTOS
    //____________________________________________________________________________________________
    
    
    //--- variables de configuracion de archivos TXT
    
    
    private   String      unidad;
    private   String      name;
    private   String      fecha;
    private   String      Path_FileTxt;
    private   String      linea;
    private   FileReader  fileReader;
    private   int         posInitialPlaca ;
    private   int         posFishedPlaca  ;
    private   int         posInitialCedula ;
    private   int         posFishedCedula ;
    private   int         posInitialRemision ;
    private   int         posFishedRemision ;
    
    
    private  String          unidadWrite;
    private  String          nameFileRemision;
    private  String          nameFileCreacion;
    private  FileWriter      fileRemision;
    private  FileWriter      fileCreacion;
    private  BufferedWriter  bufferRemision;
    private  BufferedWriter  bufferCreacion;
    private  PrintWriter     lineaRemision;
    private  PrintWriter     lineaCreacion;
    
    private  FileWriter      fwC810;
    private  FileWriter      fwC760;
    private  BufferedWriter  bfC810;
    private  BufferedWriter  bfC760;
    private  PrintWriter     lineC810;
    private  PrintWriter     lineC760;
    
    
    
    
    //--- otras variables
    private Model model;
    private List listado760;
    private List listado810;
    
    
    //____________________________________________________________________________________________
    //                                          METODOS
    //____________________________________________________________________________________________
    
    
    
    public HMigracionConductorPropietario(){}
    
    
    
    public void iniVar(Model model, String fecha1){
        this.model      = model;
        this.listado760 =null;
        this.listado810 =null;
        this.fecha =  fecha1 ;
    }
    
    
    public void iniTxt(){
        try{
            // archivo  de lectura
            this.unidad="c:/exportar/masivo/Cortes/";
            // this.unidad="C:/";
            
            this.name  ="MS8205_"+ fecha;
            this.Path_FileTxt     = unidad + name  +".txt";
            ////System.out.println(Path_FileTxt+"\n");
            this.posInitialPlaca  = 22;
            this.posFishedPlaca   = posInitialPlaca + 12;
            this.posInitialCedula = 43;
            this.posFishedCedula  = posInitialCedula + 10;
            this.posInitialRemision = 0;
            this.posFishedRemision  = this.posInitialRemision + 8;
            
            
            // archivos de escrituras
            
            this.unidadWrite="c:/exportar/masivo/Cortes/";
           // this.unidadWrite="C:/Excel/";
            this.nameFileRemision= unidadWrite + "RemProp"+ fecha +".txt";
            this.nameFileCreacion= unidadWrite + "PlacaPropCond"+fecha+".txt";
            
            
            this.fileCreacion    = new FileWriter(this.nameFileCreacion);
            this.bufferCreacion  = new BufferedWriter(this.fileCreacion);
            this.lineaCreacion   = new PrintWriter(this.bufferCreacion);
            
            this.fileRemision    = new FileWriter(this.nameFileRemision);
            this.bufferRemision  = new BufferedWriter(this.fileRemision);
            this.lineaRemision   = new PrintWriter(this.bufferRemision);
            
            
            this.fwC810      = new FileWriter(unidadWrite +"C810.csv");
            this.bfC810      = new BufferedWriter(this.fwC810);
            this.lineC810    = new PrintWriter(this.bfC810);
            
            this.fwC760      = new FileWriter(unidadWrite +"C760.csv");
            this.bfC760      = new BufferedWriter(this.fwC760);
            this.lineC760    = new PrintWriter(this.bfC760);
            
            
        }
        catch(Exception e){
            ////System.out.print("Enumerationiniciar variables TXT :" + e.getMessage());
        }
        
    }
    
    
    
    
    public void saveTXT(){
        this.lineaCreacion.close();
        this.lineaRemision.close();
    }
    
    
    public void save(){
        this.lineC810.close();
        this.lineC760.close();
    }
    
    
    public void start(Model model,String fecha1){
        iniVar(model, fecha1);
        iniTxt();
        super.start();
    }
    
    
    
    public  void run(){
//        try{
//            ////System.out.println("Inicio");
//            String cedula="";
//            String placa="";
//            String remision ="";
//            fileReader = new FileReader(Path_FileTxt);
//            BufferedReader archivo = new BufferedReader(fileReader);
//            listado760  = new LinkedList();
//            listado810  = new LinkedList();
//            
//            //--- Recorremos el archivo
//            while( (linea=archivo.readLine()) !=null){
//                int longLine  = linea.length();
//                if(longLine>=this.posFishedCedula){
//                    //--- obtenemos las varibles del archivo
//                    int longCedula= 0;
//                    int longCedulaPR= 0;
//                    cedula    = linea.substring(posInitialCedula,posFishedCedula).trim();
//                    String cedula2    = cedula;
//                    String cedula2txt = cedula;
//                    placa     = linea.substring(posInitialPlaca,posFishedPlaca).trim();
//                    remision  = linea.substring(posInitialRemision,posFishedRemision).trim();
//                    String cedulaPropietario = model.MigracionConductorSvc.getCCPropietario(remision);
//                    String ccPR    = cedulaPropietario;
//                    String ccPRtxt = cedulaPropietario;
//                    //---  realizamos el relleno de espacios
//                    longCedula = cedula.length();
//                    if(longCedula<10)
//                        for(int i=1;i<=(10-longCedula);i++){
//                            cedula2="0"+cedula2;
//                        }
//                    
//                    longCedulaPR = cedulaPropietario.length();
//                    if(longCedulaPR<10)
//                        for(int i=1;i<=(10-longCedulaPR);i++){
//                            ccPR="0"+ccPR;
//                        }
//                    //--- realizamos la existencia de la cedula en MIMS  y adicionamos en las listas para los archivos de Excel
//                    if(!cedula.equals("")            && !model.MigracionConductorSvc.exist760(cedula2))    add760(cedula,"CO");
//                    if(!cedula.equals("")            && !model.MigracionConductorSvc.exist810(cedula2))    add810(cedula);
//                    if(!cedulaPropietario.equals("") && !model.MigracionConductorSvc.exist760(ccPR))       add760(cedulaPropietario,"PR");
//                    if(!cedulaPropietario.equals("") && !model.MigracionConductorSvc.exist810(ccPR))       add810(cedulaPropietario);
//                    
//                    //--- verificamos que la cedula del propietario exista en la MSF200, y insertamos en el archivo CreacionPlaca.TXT
//                    if( !cedulaPropietario.equals("")  &&  !model.MigracionConductorSvc.exist200(ccPR))
//                        this.lineaCreacion.println(placa +","+ccPRtxt +","+cedula2txt+","+remision );
//                    //--- Insertamos en el archivo Remision.TXT
//                    this.lineaRemision.println(remision +","+ cedulaPropietario);
//                }
//            }
//            this.saveTXT();
//            if(listado810.size()>0 || listado760.size()>0)
//                generarFile();
//            
//            ////System.out.println("\n\n Finalizado....  \n");
//        }catch(Exception e){
//            ////System.out.println("ERROR :" +e.getMessage());
//        }finally{
//            destroy();
//        }
    }
    
    
    public void add760(String cedula, String tipo){
       if(!existe(listado760, cedula)){ 
          MigracionConductorPropietario migracion = new MigracionConductorPropietario();
          migracion.format();
          migracion.setCedula(cedula);
          migracion.setTipo(tipo);
          listado760.add(migracion);
       }
    }
    
    public void add810(String cedula){
      if(!existe(listado810, cedula)){
         MigracionConductorPropietario migracion = new MigracionConductorPropietario();
         migracion.format();
         migracion.setCedula(cedula);
         listado810.add(migracion);
      }
    }
    
    
    
    // Funcion para Verificar la Existencia de la cedula en el listado
    public boolean existe(List lista, String id){
       boolean estado = false;
       if(lista!=null && lista.size()>0){
          Iterator it = lista.iterator();
          while(it.hasNext()){
              MigracionConductorPropietario migracion = (MigracionConductorPropietario) it.next();
              if(migracion.getCedula().equals(id)){
                  estado = true;
                  break;
              }  
          }
       }
       return estado;
    }
    
    
    
    
    
    public void generarFile() throws Exception{
        try{
            
            
            //--- El listado de la 760
            Iterator it760 = this.listado760.iterator();
            if(it760!=null){
                ConductorService  Svc = new ConductorService();
                while(it760.hasNext()){
                    MigracionConductorPropietario migracion = (MigracionConductorPropietario)it760.next();
                    String posicion = (migracion.getTipo().equals("CO"))?"CONDUCTOR":"CONDUCTOR";
                    migracion.setPosicion(posicion);
                    //--- buscamos los datos en nit
                    Svc.searchNit(migracion.getCedula());
                    Nit nit = Svc.getNit();
                    if(nit!=null)
                        migracion = insertNit(migracion,nit);
                    //--- buscamos los datos de conductores
                    Svc.searchConductor( migracion.getCedula());
                    Conductor conductor = Svc.getConductor();
                    if(conductor!=null)
                        migracion = insertConductor(migracion, conductor);
                    
                    copy760(migracion,posicion);
                }
            }
            
            
            //--- El listado de la 810
            Iterator it810 = this.listado810.iterator();
            if(it810!=null){
                ConductorService  Svc = new ConductorService();
                while(it810.hasNext()){
                    MigracionConductorPropietario migracion = (MigracionConductorPropietario)it810.next();
                    //--- buscamos los datos en nit
                    Svc.searchNit(migracion.getCedula());
                    Nit nit = Svc.getNit();
                    if(nit!=null)
                        migracion = insertNit(migracion,nit);
                    copy810(migracion);
                }
            }
            
            this.save();
            
        }catch(Exception e){
            throw new Exception("Rutina generarExcel():"+ e.getMessage());
        }
        
    }
    
    
    
    public MigracionConductorPropietario insertNit(MigracionConductorPropietario migracion,Nit nit){
        if(migracion!=null && nit !=null){
            ////System.out.print(nit.getAddress() + nit.getAgCode() + nit.getName() + nit.getNit()  );
            migracion.setCiudad( nit.getAgCode());
            migracion.setDireccion( nit.getAddress());
            String genero = ( (nit.getSex()==null) || (nit.getSex().equals("")) )?"U":nit.getSex();
            migracion.setGenero( genero );
            migracion.setFechaNacimiento(nit.getFechaNacido());
            String name = nit.getName().trim();
            String vector[] = name.split(" ");
            String nombre1="";
            String nombre2="";
            String nombre3="";
            String nombre4="";
            for(int i=0; i<=vector.length-1;i++){
                if(!vector[i].equals("")){
                    if(nombre1.equals(""))
                        nombre1 = vector[i];
                    else if(nombre3.equals(""))
                        nombre3 = vector[i];
                    else if(nombre2.equals("")){
                        nombre2 = vector[i];
                    }
                    else
                        nombre3= vector[i];
                    
                }
                else
                    i++;
            }
            
            if(vector.length==3){
                nombre4 = nombre3;
                nombre3 = nombre2;
                nombre2 = "";
            }
            
            migracion.setNombre1(nombre1);
            migracion.setNombre2(nombre2);
            migracion.setApellido1(nombre3);
            migracion.setApellido2(nombre4);
        }
        return migracion;
    }
    
    
    
    public MigracionConductorPropietario insertConductor(MigracionConductorPropietario migracion,Conductor conductor){
        if(migracion!=null && conductor!=null){
            migracion.setFechaIngreso(conductor.getInitDate());
            migracion.setFechaNacimiento( conductor.getDateBirth());
        }
        return migracion;
    }
    
    
    
    
    public void copy810( MigracionConductorPropietario migracion){
        String nombre1 = "NN";
        String apellido1="NN";
        
        if(!migracion.getNombre1().trim().equals("")){
            nombre1=migracion.getNombre1();
        }
        if(!migracion.getApellido1().trim().equals("")){
            apellido1 = migracion.getApellido1();
        }
        
        
        this.lineC810.println(migracion.getCedula()    + "," +
        
        apellido1 + "," +
        migracion.getApellido2() + "," +
        nombre1   + "," +
        migracion.getNombre2()   + "," +
        "0"+ "," +
        "SM"
        );
    }
    
    
    
    public void copy760( MigracionConductorPropietario migracion,String posicion){
        java.util.Date d = new java.util.Date();
        Calendar c = Calendar.getInstance();
        c.setTime(d);
        c.set(c.DATE,c.DATE-1);
        d = c.getTime();
        java.text.SimpleDateFormat s = new java.text.SimpleDateFormat("dd/MM/yy");
        String hoy = s.format(d);
        String genero = "U";
        if(!migracion.getGenero().trim().equals("")){
            genero =migracion.getGenero();
        }
        this.lineC760.println( migracion.getCedula() + "," +
        "CONDUCTOR"             + "," +
        genero + "," +
        "CO"   + "," +
        "SM" + "," +
        "01/01/69" + "," +
        hoy
        );
    }
    
    
    public  String getFormatDate(String fecha){
        String formato="";
        if(!fecha.equals("")){
            String  ano = fecha.substring(2,4);
            String  mes = fecha.substring(5,7);
            String  dia = fecha.substring(8,10);
            formato     = dia+"/"+mes+"/"+ano;
        }
        return formato;
    }
    
    
}

