/***************************************
 * Nombre Clase ............. ImpresionChequeService.java
 * Descripci�n  .. . . . . .  Permite Interactuar con el dao de la impresion de cheques
 * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
 * Fecha . . . . . . . . . .  26/01/2006
 * versi�n . . . . . . . . .  1.0
 * Copyright ...............  Transportes Sanchez Polo S.A.
 *******************************************/


package com.tsp.operation.model;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.util.Date;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.SeriesChequesDAO;
import com.tsp.util.Util;





public class ImpresionChequeService {
    
    private ImpresionChequeDAO  ChequeDataAccess;
    private List    listadoCheques;
    private List    listaAuxiliar;
    private List    listadoImpresion;
    private String  llave;
    private int     activo;
    private boolean verificacion = false;
    private String  SEPARADOR    = "<BR>";
    
    private ImpresionCheque chequeToImprimir;
    
    
    private SeriesChequesDAO  serieDAO;
    
    private final String CONCEPTO_SERIE = "ANT";
    
    
    
    // Inicializamos las variables
    public ImpresionChequeService() {
        ChequeDataAccess = new  ImpresionChequeDAO();
        serieDAO         = new  SeriesChequesDAO();
        listadoCheques   = null;
        listadoImpresion = null;
        listaAuxiliar    = null;
        chequeToImprimir = null;
        llave            = "";
        activo           = 0;
    }
    public ImpresionChequeService(String dataBaseName) {
        ChequeDataAccess = new  ImpresionChequeDAO(dataBaseName);
        serieDAO         = new  SeriesChequesDAO(dataBaseName);
        listadoCheques   = null;
        listadoImpresion = null;
        listaAuxiliar    = null;
        chequeToImprimir = null;
        llave            = "";
        activo           = 0;
    }
    
    
    
    
    /**
     * Metodo que setea listadoCheques
     * @version ...... 1.0
     */
    public void reset(){
        this.listadoCheques = this.listaAuxiliar;
    }
    
    
    
    /**
     * Metodo que setea listadoImpresion
     * @version ...... 1.0
     */
    public void resetListImpresion(){
        if(listadoImpresion!=null && listadoImpresion.size()>0){
            for(int i=0;i<listadoImpresion.size();i++){
                ImpresionCheque cheque = (ImpresionCheque) listadoImpresion.get(i);
                cheque.setImpresion(false);
                cheque.setSeleted(false);
            }
        }
        listadoImpresion = null;
    }
    
    
    
    /**
     * M�todo que carga la lista de cheques a imprimir para el usuario
     * @autor....... fvillacob
     * @throws...... Exception
     * @parameter... String distrito, String agencia, String base, String usuario
     * @version..... 1.0.
     **/
    public List searchCheques(String distrito, String agencia, String base, String usuario) throws SQLException{
        try{
            listadoCheques = ChequeDataAccess.ListCheques(distrito,agencia,base,usuario);
            listadoCheques = this.agrupar( listadoCheques );
        }catch(Exception e){
            throw new SQLException( e.getMessage() );
        }
        return listadoCheques;
    }
    
    
    
    
  
    
    
    
    
    
    
    /**
     * M�todo que saca de la lista, todos aquellos queches que esten ceros o negativos
     * @autor....... fvillacob
     * @throws...... Exception
     * @parameter... List lista
     * @version..... 1.0.
     **/
    public List sacarChequesValorCero(List lista)throws Exception{
        List listaChequesValidos = new LinkedList();
        try{
            
            if(lista!=null && lista.size()>0){
                int cont = 1;
                for(int i=0;i<lista.size();i++){
                    ImpresionCheque cheque = (ImpresionCheque)lista.get(i);
                    if(cheque.getValor()>0){
                        cheque.setId(cont);
                        listaChequesValidos.add(cheque);
                        cont++;
                    }
                }
            }
            
        }catch(Exception e){
            throw new Exception("Service: agrupar --->" + e.getMessage());
        }
        return listaChequesValidos;
    }
    
    
    
    
    
    
    /**
     * M�todo que devuelve el objeto del id, de la lista general
     * @autor....... fvillacob
     * @throws...... Exception
     * @parameter... String id
     * @version..... 1.0.
     **/
    public ImpresionCheque getChequeListado(String id)throws Exception{
        ImpresionCheque cheque = new ImpresionCheque();
        if(listadoCheques!=null && listadoCheques.size()>0){
            for(int i=0; i<listadoCheques.size();i++){
                ImpresionCheque chequeList = (ImpresionCheque)listadoCheques.get(i);
                if(chequeList.getId()==Integer.parseInt(id)){
                    cheque = chequeList;
                    break;
                }
            }
        }
        return cheque;
    }
    
    
    
    
    
    
    
    /**
     * M�todo que devuelve el la lista de cheques a imprimir
     * @autor....... fvillacob
     * @throws...... Exception
     * @version..... 1.0.
     **/
    public List getListImpresion(){
        listadoImpresion=null;
        if(listadoCheques!=null && listadoCheques.size()>0){
            Iterator it = listadoCheques.iterator();
            int sw = 0;
            while(it.hasNext()){
                ImpresionCheque cheque = (ImpresionCheque)it.next();
                if(cheque.getSeleted() && !cheque.getImpresion()){
                    if(sw==0) listadoImpresion = new LinkedList();
                    sw++;
                    listadoImpresion.add(cheque);
                    
                }
            }
        }
        return listadoImpresion;
    }
    
    
    
    
    
    /**
     * M�todo que elimina de la lista de impresion el cheque, y actualiza los semas con inf. de banco
     * @autor....... fvillacob
     * @throws...... Exception
     * @parameter... ImpresionCheque chequeDelete
     * @version..... 1.0.
     **/
    public void update(ImpresionCheque chequeDelete) throws SQLException{
        try{
            List lista = null;
            
            if(listadoCheques!=null && listadoCheques.size()>0){
                lista = new LinkedList();
                for(int i=0;i<listadoCheques.size();i++){
                    ImpresionCheque cheque = (ImpresionCheque)listadoCheques.get(i);
                    if(cheque.getId()!=chequeDelete.getId()){
                        lista.add(cheque);
                    }
                }
            }
            listadoCheques = lista;
        }catch(Exception e){
            throw new SQLException(e.getMessage());
        }
    }
    
    
    
    
    
    /**
     * M�todo que actualiza la bd con inf. delcheque
     * @autor....... fvillacob
     * @throws...... Exception
     * @parameter... ImpresionCheque cheque, String usuario
     * @version..... 1.0.
     **/
    public synchronized String updateRecord(ImpresionCheque cheque, String usuario) throws SQLException{
        try{
            //String sql = ChequeDataAccess.ActualizarMOVOC(cheque,usuario) +"; "+ ChequeDataAccess.ActualizarSeries(cheque,usuario);
            String id = String.valueOf( cheque.getId_serie() );
            String sql = ChequeDataAccess.ActualizarMOVOC(cheque,usuario) +"; "+  serieDAO.updateSerieSQL( id, usuario);
            return sql;
            
        }catch(Exception e){
            throw new SQLException(e.getMessage());
        }
    }
    
    
    
    
    /**
     * M�todo que finaliza la serie del banco para el cheque
     * @autor....... fvillacob
     * @throws...... Exception
     * @parameter... ImpresionCheque cheque, String usuario
     * @version..... 1.0.
     **/
    public void finallySerie(ImpresionCheque cheque, String usuario) throws SQLException{
        try{
            ChequeDataAccess.FinalizarSeries(cheque,usuario);
        }catch(Exception e){
            throw new SQLException(e.getMessage());
        }
    }
    
    
    
    
    /**
     * M�todo que devuelve la Serie para el banco
     * @autor....... fvillacob
     * @throws...... Exception
     * @parameter... String distrito, String agencia, String banco, String agenciaBanco, String cuenta
     * @version..... 1.0.
     **/
    public synchronized Series getSerie(String distrito, String agencia, String banco, String agenciaBanco, String cuenta)throws SQLException{
        Series serie=null;
        try{
            serie = ChequeDataAccess.getSeries(distrito, agencia, banco, agenciaBanco, cuenta);
        }catch(Exception e){
            throw new SQLException(e.getMessage());
        }
        return serie;
    }
    
    
    
    
    /**
     * M�todo que cancela el cheque a imprimir, cambiando su estado de impresion
     * @autor....... fvillacob
     * @parameter... int id
     * @version..... 1.0.
     **/
    public void cancel(int id){
        Iterator it = listadoCheques.iterator();
        if(it!=null){
            while(it.hasNext()){
                ImpresionCheque cheque = (ImpresionCheque)it.next();
                if(cheque.getId()==id){
                    cheque.setImpresion(false);
                    cheque.setSeleted(false);
                    break;
                }
            }
        }
    }
    
    
    
    
    /**
     * M�todo que devuelve el cheque para ser impreso
     * @autor....... fvillacob
     * @parameter... int id
     * @version..... 1.0.
     **/
    public ImpresionCheque getCheque(int id){
        ImpresionCheque nextCheque=null;
        if(listadoImpresion!=null && listadoImpresion.size()>0){
            Iterator it = listadoImpresion.iterator();
            if(it!=null){
                while(it.hasNext()){
                    ImpresionCheque cheque = (ImpresionCheque)it.next();
                    if(cheque.getId()==id){
                        nextCheque = cheque;
                        break;
                    }
                }
            }
        }
        return nextCheque;
    }
    
    
    
    
    /**
     * M�todo que activa el cheque para que sea impreso, activa su campo de Impresion
     * @autor....... fvillacob
     * @parameter... int id, int idbanco
     * @version..... 1.0.
     **/
    public void active(int id, int idbanco, String laser){
        if(listadoCheques!=null){
            Iterator it = listadoCheques.iterator();
            if(it!=null){
                while(it.hasNext()){
                    ImpresionCheque cheque = (ImpresionCheque)it.next();
                    if(cheque.getId()==id){
                        cheque.setImpresion(false);
                        cheque.setSeleted(true);
                        cheque.setIdBanco(idbanco);
                        cheque.setLaser( laser );
                        cheque.setCopias( laser.equals("S")? 2 : 1 );                        
                        break;
                    }
                }
            }
        }
    }
    
    
    
    
    /**
     * M�todo que formatea parametros del cheque enviado
     * @autor....... fvillacob
     * @parameter... ImpresionCheque cheque
     * @version..... 1.0.
     **/
    public ImpresionCheque format(ImpresionCheque cheque, String user) throws SQLException {
        try{
            Series serie=null;
            Banco banco=null;
            banco = ChequeDataAccess.searchBanco(cheque.getBanco() + "/" + cheque.getAgenciaBanco());
            if(banco!=null){
              
                serie = serieDAO.getSeries(cheque.getDistrito(), banco.getBanco(), banco.getNombre_Agencia(), CONCEPTO_SERIE , cheque.getAgencia(), user  );
                if(serie!=null){
               
                    cheque.setId_serie    ( serie.getId()             );
                    cheque.setBanco       ( banco.getBanco()          );
                    cheque.setAgenciaBanco( banco.getNombre_Agencia() );
                    cheque.setCuenta      ( banco.getCuenta()         );
                    cheque.setNumber      ( serie.getLast_number()    );
                    cheque.setPrefix      ( serie.getPrefijo()        );
                    cheque.setTope        ( Integer.parseInt( serie.getSerial_fished_no())   );
                    cheque.setCheque      ( serie.getPrefijo() + serie.getLast_number()      );
                    cheque.setAno         ( Util.getFechaActual_String(1)  );
                    cheque.setMes         ( Util.getFechaActual_String(3)  );
                    cheque.setDia         ( Util.getFechaActual_String(5)  );
                    cheque.setInicioSerie ( serie.getSerial_initial_no()   );
                    
                    
                }
                else{
                    cheque.setNoSerie(true);
                }
            }
        }catch(Exception e){
            throw new SQLException( e.getMessage() );
        }
        return cheque;
    }
    
    
    
    
    
    /**
     * M�todo que busca el  esquema de impresion para un banco
     * @autor....... fvillacob
     * @parameter... String distrito, String banco
     * @version..... 1.0.
     **/
    public List getEsquema(String distrito, String banco)throws SQLException{
        List esquema = null;
        try{
            esquema = ChequeDataAccess.searchEsquema(distrito, banco);
        }catch(Exception e){
            throw new SQLException( e.getMessage() );
        }
        return esquema;
    }
    
    
    
    
    
    
    /**
     * M�todo que busca el  BANCO
     * @parameter... String bank
     * @version..... 1.0.
     **/
    Banco  searchBanco(String bank) throws Exception{
        return ChequeDataAccess.searchBanco(bank);
    }
    
    
    /**
     * M�todo que cambia el banco para la planilla
     * @parameter... String planilla, String banco
     * @version..... 1.0.
     **/
    public void  cambiarBanco(String planilla,String bancoAnt, String bancoNew, String dstrct, String moneda) throws SQLException{
        try{
            
            String[] vec = planilla.split( this.SEPARADOR );
            BancoService b = new BancoService();
            com.tsp.operation.model.services.TasaService t = new com.tsp.operation.model.services.TasaService();
            String banvec[] = bancoNew.split("/");
            String banco=banvec[0];
            String cuenta=banvec[1];
            String monedaCia = "PES";
            
            java.util.Date fecha_d = new java.util.Date();
            java.text.SimpleDateFormat s =  new java.text.SimpleDateFormat("yyyy-MM-dd");
            String fecha = s.format(fecha_d);
            
            for(int i=0;i<vec.length;i++){
                String oc = vec[i];
                
                Banco bancoOj = b.obtenerBanco(dstrct,banco ,cuenta);
                String moneda2 = bancoOj.getMoneda();
                double vlrtasa = 1;
                
                
                try{
                    t.buscarValorTasa(monedaCia,moneda,moneda2,fecha);
                }catch(Exception et){
                    throw new SQLException(et.getMessage());
                }
                
                com.tsp.operation.model.beans.Tasa tasa = t.obtenerTasa();
                if(tasa!=null){
                    vlrtasa= tasa.getValor_tasa();
                }
                
                ChequeDataAccess.cambiarBanco(oc, bancoAnt, bancoNew, vlrtasa, moneda2);
            }
            
        }catch(Exception e){
            throw new SQLException( e.getMessage() );
        }
    }
    
    
    
    // SET:
    
    
    
    /**
     * Encapsula el cheque a Imprimir
     * @parameter... String llave
     * @version..... 1.0.
     **/
    public void setChequeToPrint(ImpresionCheque cheque){
        chequeToImprimir = cheque;
    }
    
    
    
    
    /**
     * llave de busqueda entre los seleccionados y no impresos
     * @parameter... String llave
     * @version..... 1.0.
     **/
    public void setLLave(String llave){
        this.llave = llave;
    }
    
    
    /**
     * metodo que setea verificacion
     * @parameter... boolean valor
     * @version..... 1.0.
     **/
    public void setVerificacion(boolean valor) {
        this.verificacion = valor;
    }
    
    
    /**
     * Metodo que setea listaAuxiliar
     * @param ........ List lista
     * @version ...... 1.0
     */
    public void setAuxiliar(List lista){
        this.listaAuxiliar = lista;
    }
    
    
    /**
     * Metodo que setea activo
     * @param ........ int id
     * @version ...... 1.0
     */
    public void setActivo(int id){
        this.activo = id;
    }
    
    
    
    /**
     * Metodo que setea  listadoCheques
     * @version ...... 1.0
     */
    public void setCheques(List lista){
        listadoCheques = lista;
    }
    
    
    
    
    
    // GET:
    
    
    
    /**
     * Metodo que devuelve el cheque  a imprimir
     * @version..... 1.0.
     **/
    public ImpresionCheque getChequeToPrint(){
        return chequeToImprimir ;
    }
    
    
    /**
     * Metodo que devuelve llave
     * @version ...... 1.0
     */
    public String getLlave(){
        return this.llave;
    }
    
    
    
    /**
     * Metodo que devuelve verificacion
     * @version ...... 1.0
     */
    public boolean getVerificacion() {
        return this.verificacion;
    }
    
    
    
    /**
     * Metodo que devuelve listaAuxiliar
     * @version ...... 1.0
     */
    public List getAuxiliar(){
        return this.listaAuxiliar;
    }
    
    
    /**
     * Metodo que devuelve activo
     * @version ...... 1.0
     */
    public int getActivo(){
        return this.activo;
    }
    
    
    /**
     * Metodo que devuelve listadoCheques
     * @version ...... 1.0
     */
    public List getCheques(){
        return listadoCheques;
    }
    
    /**
     * M�todo que verifica si existe el banco al usuario
     * @autor....... jdelarosa
     * @throws...... Exception
     * @parameter... String user
     * @version..... 1.0.
     **/
    public boolean getBancoUsuario (String banco, String sucursal, String user) throws Exception{
        try{
            return ChequeDataAccess.getBancoUsuario( banco, sucursal, user );
        }catch(Exception e){
            throw new SQLException( e.getMessage() );
        }
    }
    
    /**
     * M�todo que carga la lista de cheques a imprimir para el usuario
     * @autor....... fvillacob
     * @throws...... Exception
     * @parameter... String distrito, String agencia, String base, String usuario
     * @version..... 1.0.
     **/
    public List searchChequesProntoPago(String distrito, String agencia, String base, String usuario, String []ids) throws SQLException{
        try{
            listadoCheques = ChequeDataAccess.ListChequesPorProntoPago(distrito,agencia,base,usuario, ids);
            listadoCheques = this.agrupar( listadoCheques );
        }catch(Exception e){
            throw new SQLException( e.getMessage() );
        }
        return listadoCheques;
    }
    
    
 /**
     * M�todo que actualiza la bd con inf. delcheque
     * @autor....... fvillacob
     * @throws...... Exception
     * @parameter... ImpresionCheque cheque, String usuario
     * @version..... 1.0.
     **/
    public synchronized String updateRecordProntoPago(ImpresionCheque cheque, String usuario) throws SQLException{
        try{
            String sql = ChequeDataAccess.ActualizarMOVOC_PRONTO_PAGO (cheque,usuario) +"; "+ ChequeDataAccess.ActualizarSeries(cheque,usuario);
            return sql;
            
        }catch(Exception e){
            throw new SQLException(e.getMessage());
        }
    }    
    
     /**
     * M�todo que carga el objeto cheque con los datos del registro de  la consulta
     * @autor....... fvillacob
     * @throws...... Exception
     * @parameter... List lista
     * @version..... 1.0.
     **/
    public List agrupar(List lista)throws Exception{
        List listaAgrupada = new LinkedList();
        try{
            if(lista!=null){
                int cont = 1;
                for(int i=0;i<lista.size();i++){
                    ImpresionCheque cheque = (ImpresionCheque)lista.get(i);
                    int sw = 0;
                    for(int j=0;j<listaAgrupada.size();j++){
                        ImpresionCheque chequeAgrupado = (ImpresionCheque)listaAgrupada.get(j);
                        if (
                        chequeAgrupado.getAgencia().     equals( cheque.getAgencia()      )
                        && chequeAgrupado.getUsuario().     equals( cheque.getUsuario()      )
                        && chequeAgrupado.getBanco().       equals( cheque.getBanco()        )
                        && chequeAgrupado.getAgenciaBanco().equals( cheque.getAgenciaBanco() )
                        && chequeAgrupado.getCedula().      equals( cheque.getCedula()       )
                        && chequeAgrupado.getMoneda().      equals( cheque.getMoneda()       )
                        ){
                            chequeAgrupado.setPlanilla( chequeAgrupado.getPlanilla()   + this.SEPARADOR + cheque.getPlanilla() );
                            chequeAgrupado.setValores( chequeAgrupado.getValores()    + this.SEPARADOR + cheque.getValor()    );
                            chequeAgrupado.setValor   ( chequeAgrupado.getValor()                       + cheque.getValor()    );
                            chequeAgrupado.setAuxiliar( chequeAgrupado.getAuxiliar() + this.SEPARADOR + cheque.getFecha_imp_oc() + "/" + cheque.getPlanilla()    );
                            sw = 1;
                            break;
                        }
                    }
                    if(sw==0){
                        if( getBancoUsuario( cheque.getBanco(), cheque.getAgenciaBanco(), cheque.getUsuario() ) )
                            cheque.setActivado ( true );
                        else
                            cheque.setActivado ( false );
                        cheque.setId(cont);
                        listaAgrupada.add(cheque);
                        cont++;
                    }
                }
                
                
                listaAgrupada = this.sacarChequesValorCero(listaAgrupada);
                
                
            }
        }catch(Exception e){
            throw new Exception("Service: agrupar --->" + e.getMessage());
        }
        return listaAgrupada;
    }
    
        /**
     * M�todo que busca el  esquema de impresion para un banco
     * @autor....... Osvaldo P�rez
     * @parameter... String distrito, String banco
     * @version..... 1.0.
     **/
    public List getEsquemaCMS(String banco)throws SQLException{
        List esquema = null;
        try{
            esquema = ChequeDataAccess.searchEsquemaCMS(banco);
        }catch(Exception e){
            throw new SQLException( e.getMessage() );
        }
        return esquema;
    }
    
    
    
}
