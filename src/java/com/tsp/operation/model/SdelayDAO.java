/*
 * SdelayDAO.java
 *
 * Created on 3 de diciembre de 2004, 04:04 PM
 */

package com.tsp.operation.model;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;
/**
 *
 * @author  KREALES
 */
public class SdelayDAO {
    
    private List sdelay;
    private TreeMap demoras;
    private Sjdelay sjd;
    
    /** Creates a new instance of SdelayDAO */
    public SdelayDAO() {
    }
    
    public List getList(){
        return sdelay;
    }
    public TreeMap getDemoras(){
        return demoras;
    }
    public void set(Sjdelay sjd){
        this.sjd = sjd;
    }
    public Sjdelay getSj(){
        return sjd;
    }
    
    public boolean exist(String sj, String cf )throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw=false;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("Select  *  from sjdelay where sj = ? and delay_code=? and reg_status='' ");
                st.setString(1,sj);
                st.setString(2,cf);
                
                rs = st.executeQuery();
                
                if(rs.next()){
                    sw=true;
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL SJ DELAY " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return sw;
        
    } 
    
    public boolean existList(String dstr)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw=false;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("Select  *  from sjdelay where dstrct = ? and reg_status='' ");
                st.setString(1,dstr);
                
                rs = st.executeQuery();
                
                if(rs.next()){
                    sw=true;
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL SJ DELAY " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return sw;
        
    } 
    public void search(String sj, String cf )throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw=false;
        sjd=null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("Select  s.*, t.description  from sjdelay s , tbldelay t where s.reg_status='' and s.delay_code=t.delay_code and s.sj=?  and s.delay_code=?");
                st.setString(1,sj);
                st.setString(2,cf);
                
                rs = st.executeQuery();
                
                if(rs.next()){
                    sjd = (Sjdelay.load(rs));
                    
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL SJ DELAY " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    } 
    
    public void searchSjs(String dstrct)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw=false;
        sjd=null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("Select  s.*, t.description  from sjdelay s , tbldelay t where s.dstrct = ? and s.reg_status='' and s.delay_code=t.delay_code ");
                st.setString(1,dstrct);
                
                rs = st.executeQuery();
                sdelay = new LinkedList();
                while(rs.next()){
                   sdelay.add(Sjdelay.load(rs));
                    
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL SJ DELAY LISTA " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    } 
    public void listDelays(String dstrct)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        boolean sw=false;
        demoras=null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("Select  *  from tbldelay where dstrct = ? and reg_status='' ");
                st.setString(1,dstrct);
                rs = st.executeQuery();
                demoras = new TreeMap();
                while(rs.next()){
                    demoras.put(rs.getString("description"), rs.getString("delay_code"));
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL SJ DELAY LISTA " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    } 
    
    public void anular()throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("update sjdelay set reg_status='A' where sj=? and delay_code=?");
                st.setString(1,sjd.getSj());
                st.setString(2,sjd.getCf());
                st.executeUpdate();
                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA ANULACION DEL SJDELAY " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
     public void insert()throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("insert into sjdelay (delay_code,dstrct,sj,creation_user,creation_date,last_update) values(?,?,?,?,'now()','now()')");
                st.setString(1, sjd.getCf());
                st.setString(2,sjd.getDstrct());
                st.setString(3, sjd.getSj());
                st.setString(4,sjd.getUser());
                st.executeUpdate();
                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA INSERCCION DE LAS SJDELAY " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    
}
