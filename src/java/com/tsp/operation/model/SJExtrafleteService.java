/*
 * SJExtrafleteService.java
 *
 * Created on 15 de julio de 2005, 9:44
 */
package com.tsp.operation.model;

import com.tsp.operation.model.beans.*;
import java.io.*;
import java.sql.*;
import java.util.*;

/**
 *
 * @author  Henry
 */
public class SJExtrafleteService {
    
    private SJExtrafleteDAO dao = new SJExtrafleteDAO();
    /** Creates a new instance of SJExtrafleteService */
    public SJExtrafleteService() {
    }
    public void insertarSJExtraflete(SJExtraflete s) throws SQLException{
        dao.setSJExtraflete(s);
        dao.insertarExtraflete();
    }
    public void actualizarSJExtraflete(SJExtraflete s) throws SQLException{
        dao.setSJExtraflete(s);
        dao.updateExtraflete();
    }
    public void eliminarSJExtraflete(SJExtraflete s) throws SQLException{
        dao.setSJExtraflete(s);
        dao.deleteExtraflete();
    }
    public boolean existeSJExtraflete(String cod, String std, String codextra) throws SQLException{
        //System.out.println("Codcli: "+cod+"cod Std: "+std+"extra: "+codextra);
        return dao.existeSJExtraflete(cod, std, codextra);
    }
    public Vector listarSJExtraflete(String var)throws SQLException {
        return dao.listarSJExtraflete(var);
    }
    public SJExtraflete obtenerSJExtraflete(String codcli, String std_job_no, String codextra) throws SQLException {
        return dao.obtenerSJExtraflete(codcli,std_job_no,codextra);
    }
    public void listaFletes(String no_sj, String agencia) throws SQLException {
        dao.listaFletes(no_sj,agencia);
    }
    public java.util.Vector getFletes() {
        return dao.getFletes();
    }
    public boolean existeExtraflete( String codextra) throws SQLException {
        return dao.existeExtraflete(codextra);
    }
    public void setFletes(java.util.Vector fletes) {
        dao.setFletes(fletes);
    }
    public com.tsp.operation.model.beans.SJExtraflete getSj() {
        return dao.getSj();
    }
    
    public void setSj(com.tsp.operation.model.beans.SJExtraflete sj) {
        dao.setSj(sj);
    }
    public void searchExtraflete( String sj_no,String cod) throws SQLException {
        dao.searchExtraflete(sj_no, cod);
    }
    public void searchExtrafletePla( String planilla,String cod) throws SQLException {
        dao.searchExtrafletePla(planilla, cod);
    }
    
    //FUNCION QUE PERMITE BUSCAR TODOS LOS EXTRAFLETES APLICADOS A UNA PLANILLA,
    //SE MODIFICA LA LISTA DE EXTRAFLETES Y SE SELECCIONAN SOLO LOS APLICADOS A LA
    //PLANILLA.
    public void buscarExtafletesPla(String planilla, String sj, String agencia)throws SQLException{
        try{
            dao.listaFletes(sj,agencia);
            Vector fletes = dao.getFletes();
            Vector Mfletes = new Vector();
            for(int i =0; i<fletes.size();i++){
                SJExtraflete ext = (SJExtraflete)fletes.elementAt(i);
                dao.searchExtrafletePla(planilla,ext.getCod_extraflete());
                if(dao.getSj()!=null){
                    SJExtraflete extN = dao.getSj();
                    ext.setCantidad(extN.getCantidad());
                    ext.setSelec(true);
                }
                Mfletes.add(ext);
            }
            dao.setFletes(Mfletes);
        }catch(SQLException e){
            throw new SQLException("ERROR AL SELECCIONAR LOS EXTRAFLETES DE LA PLANILLA A MODIFICAR " + e.getMessage()+"" + e.getErrorCode());
        }
        
    }
    public float valorTotalFlete( String planilla) throws SQLException {
        return dao.valorTotalFlete(planilla);
    }
    public void listaCostos(String no_sj, String agencia) throws SQLException {
        dao.listaCostos(no_sj,agencia);
    }
    public java.util.Vector getC_reembolsables() {
        return dao.getC_reembolsables();
    }
    public void setC_reembolsables(java.util.Vector c_reembolsables) {
        dao.setC_reembolsables(c_reembolsables);
    }
    public String insertarCostoReembolsable() throws SQLException {
        return dao.insertarCostoReembolsable();
    }
    /**
     * Metodo listaCostosPlanilla, lista todos los extrafletes y costos reembolsables
     * aplicados a una planilla.
     * @param: Numero de la planilla
     * @autor : Ing. Karen Reales.
     * @version : 1.0
     */
    public void listaCostosPlanilla(String planilla, String tipo) throws SQLException {
        dao.listaCostosPlanilla(planilla, tipo);
    }
    /**
     * Metodo que anula los registros de extrafletes y costos reembolsables
     * relacionados a una planilla.
     * @param: Numero de la planilla
     * @autor : Ing. Karen Reales.
     * @version : 1.0
     */
    public String anularCosto(String numpla) throws SQLException {
        return dao.anularCosto(numpla);
    }
    /**
     * Metodo que lista los registros de extrafletes
     * relacionados a una planilla y remesa.
     * @param: Numero de la planilla
     * @param: Numero de la remesa
     * @param: Tipo
     * @param: Std job
     * @autor : Ing. Karen Reales.
     * @version : 1.0
     */
    public void listaExtraFletesAplicados(String numpla,String numrem,String stdjob, String agencia) throws SQLException {
        dao.listaFletesAplicados(numpla, numrem, "E", stdjob, agencia);
    }
    /**
     * Metodo que lista los registros de  costos reembolsables
     * relacionados a una planilla y remesa.
     * @param: Numero de la planilla
     * @param: Numero de la remesa
     * @param: Tipo
     * @param: Std job
     * @autor : Ing. Karen Reales.
     * @version : 1.0
     */
    public void listaCostosAplicados(String numpla,String numrem,String stdjob,String agencia) throws SQLException {
        dao.listaFletesAplicados(numpla, numrem, "R", stdjob,agencia);
    }
    /**
     * Getter for property vlrtotalE.
     * @return Value of property vlrtotalE.
     */
    public float getVlrtotalE() {
        return dao.getVlrtotalE();
    }
    
    /**
     * Setter for property vlrtotalE.
     * @param vlrtotalE New value of property vlrtotalE.
     */
    public void setVlrtotalE(float vlrtotalE) {
        dao.setVlrtotalE(vlrtotalE);
    }
    
    /**
     * Getter for property vlrtotalCR.
     * @return Value of property vlrtotalCR.
     */
    public float getVlrtotalCR() {
        return dao.getVlrtotalCR();
    }
    
    /**
     * Setter for property vlrtotalCR.
     * @param vlrtotalCR New value of property vlrtotalCR.
     */
    public void setVlrtotalCR(float vlrtotalCR) {
        dao.setVlrtotalCR(vlrtotalCR);
    }
    /**
     * Metodo que actualiza los registros de extrafletes y costos reembolsables
     * relacionados a una planilla y remesa.
     * @autor : Ing. Karen Reales.
     * @version : 1.0
      *@return : Retorna el sql del update,
     */
    public String ActualizarAplicados() throws SQLException {
        return dao.ActualizarAplicados();
    }
     /**
     * Metodo que anula un registro dado en la tabla de costos_reembolsables
     * @autor : Ing. Karen Reales.
     * @version : 1.0
     */
    public String anularCosto() throws SQLException {
        return dao.anularCosto();
    }
    /**
     * Funcion que busca un costo reembolsable o un extraflete relacionado a una planilla
     * para saber si ha sido o no aplicada, retorna true o false
     * @return : true si encuentra el costo, false de lo contrario
     * @autor : Ing. Karen Reales.
     * @version : 1.0
     */
    public boolean estaCostoAplicado() throws SQLException {
        return dao.estaCostoAplicado();
    }
    /**
     * Metodo buscarExtrafletes, lista los exatrafletes dependiendo de una planilla, stdjob y dstrict,
     * @param: distrito, numpla, standar
     * @autor : Ing. Diogenes Bastidas Morales
     * @version : 1.0
     */
    public void buscarExtrafletes(String dstrct, String numpla, String stdjob ) throws SQLException {
        dao.buscarExtrafletes(dstrct, numpla, stdjob);
    }    
        
}
