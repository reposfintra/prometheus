
/***
 *
 *Modificado :egonzalez2014
 */

package com.tsp.operation.model;

import java.io.*;
import java.util.*;

import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.*;
import java.sql.SQLException;

public class HExport_Migracion extends Thread {
    
    public static String nombreConexion = "fintra";
    public static final String rutaArchivo2 = "/exportar/migracion/";
    public static final String rutaArchivo = rutaArchivo2;
    private PrintStream archivoPlano;
    private PrintStream archivoPlano2;
    private HSSFWorkbook wb;
    private HSSFSheet sheet;
    private HSSFWorkbook wb2 = new HSSFWorkbook();
    private HSSFSheet sheet2 = wb2.createSheet( "Reporte" );
    private HSSFCellStyle estiloNormal;
    private int filas = 0;
    private Model model;
    private String ruta;
    private FileOutputStream fo;
    private String path;
    private String usuario;
    private String procesoName = "Registros Contables";
    String rutaMs ="";
    String fechaProceso = "";
    
    public HExport_Migracion() {
    }
    public HExport_Migracion(String dataBaseName) {
        model = new Model(dataBaseName);
        nombreConexion = dataBaseName;
    }
    
    
   
    public void start( String u){
        
        this.usuario = u;
        super.start();
    }
    
    public void run() {
//        try {
//            model.LogProcesosSvc.InsertProceso(this.procesoName, this.hashCode(), "INICIO DEL PROCESO", usuario);            
//            
//            Vector marcados = new Vector(); 
//            int agencias = 0;
//            //sandrameg 190905
//            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
//            path = rb.getString("ruta");
//            
//            ruta = path + rutaArchivo2 + usuario    + "/";
//            rutaMs=path + rutaArchivo2 + "RAYYEPES" + "/";
//            
//            File f = new File(ruta);
//            if ( !f.exists() ){
//                f.mkdirs();
//                ////System.out.println("directorios creados en: "+f.getAbsolutePath());
//            }
//            
//            f = new File(rutaMs);
//            if ( !f.exists() ){
//                f.mkdirs();
//                ////System.out.println("directorios creados en: "+f.getAbsolutePath());
//            }
//            
//            fechaProceso = Util.dateTimeFormat( new Date() );
//            ////System.out.println("FechaProceso : " + fechaProceso ); 
//            
//            model.LogProcesosSvc.updateProceso(this.procesoName, this.hashCode(), usuario, "Buscando movimentos para cheques");
//            Vector movimientos = model.movplaService.obtenerMovimientosNoMigrados( fechaProceso );
//            
//            ////System.out.println("movimientos: "+movimientos.size());
//            if ( !movimientos.isEmpty() ){
//                
//                model.LogProcesosSvc.updateProceso(this.procesoName, this.hashCode(), usuario, "Grabando movimientos para cheques");
//                String mesActual = model.movplaService.obtenerFechaActualEnFormato("MM");
//                String a�oActual = model.movplaService.obtenerFechaActualEnFormato("YY");
//                Serie serieCHAux = null;
//                String agenciaAnterior = null;
//                
//                
//                // creando consecutivos de series
//                // mfontalvo
//                // 2005-11-25
//                Serie serieCH = model.servicioSerie.obtenerSerie( "006" );
//                
//                
//                for ( int i = 0, c = 0, contadorCH = 0; i < movimientos.size() ; i++ ) {
//                    
//                    Movpla mp = ( Movpla ) movimientos.elementAt( i );
//                    Planilla p = model.planillaService.obtenerPlanillaPorNumero( mp.getPlanilla() );
//                    if ( p == null ){
//                        continue;
//                    }
//                    
//                    
//                    // cuando cambia de agencia se genera
//                    // una nueva serie se guardan los archivos
//                    // y se abre otro para el proximo
//                    // mfontalvo
//                    // 2005-11-25
//                    
//                    if ( agenciaAnterior == null || !agenciaAnterior.equals(mp.getAgency_id() ) ){
//                        if ( agenciaAnterior != null ){
//                            actualizarSerie(serieCH);
//                            contadorCH = 0;
//                        }
//                        
//                        // mfontalvo
//                        // para que solo genere un solo archivo                            
//                        if (agenciaAnterior == null)
//                            crearArchivosParaAgencia(mp.getAgency_id());
//
//                        agenciaAnterior = mp.getAgency_id();
//                        agencias++;
//                        serieCHAux = serieCH;
//                    }
//                    ////////////////////////////////////////////////////////////
//                    
//                    String consecutivoCH =
//                        serieCH.getPrefix() +
//                        Util.llenarConCerosALaIzquierda( serieCH.getLast_number(), 3 ) +
//                        mesActual +
//                        a�oActual.charAt(1);
//                   
//                   
//                    String comprobanteCH =
//                        serieCH.getPrefix() +
//                        a�oActual +
//                        mesActual +
//                        Util.llenarConCerosALaIzquierda( serieCH.getLast_number(), 3 );
//
//
//                    ////////////////////////////////////////////////////////////
//                    Hashtable fila = new Hashtable();                    
//                    
//                    fila.put( new Integer( 1 ), new CadenaConLongitud( consecutivoCH, 8 ) );
//                    fila.put( new Integer( 5 ), new CadenaConLongitud( "22010101", 24 ) );
//
//                    
//                    // busqueda del codigo contable
//                    Banco banco = model.servicioBanco.obtenerBanco(mp.getDstrct(),mp.getBranch_code(),mp.getBank_account_no());
//                    String codigoContableAux = (banco!=null?banco.getCodigo_cuenta():"");
//                    fila.put( new Integer( 17 ), new CadenaConLongitud( comprobanteCH, 10 ) );
//                    fila.put( new Integer( 18 ),
//                        new CadenaConLongitud(
//                            mp.getPlanilla() +
//                            p.getPlaveh() +
//                            p.getCedcon(), 40 ) 
//                        );
//
//                    contadorCH++;
//                    c = contadorCH;
//                    ///////////////////////////////////////////////////////////////////////////////
//
//                    ////System.out.println("Registros: "+fila);
//                    
//                    
//                    if ( !fila.isEmpty() ) {
//                        
//                        fila.put( new Integer( 2 ),
//                        new CadenaConLongitud( Util.llenarConCerosALaIzquierda( c, 4 ), 4 ) );
//                        fila.put( new Integer( 3 ), new CadenaConLongitud( mp.getDstrct(), 4 ) );
//                        fila.put( new Integer( 4 ), new CadenaConLongitud( "MJV", 3 ) );
//                        // el valor 5 lo cambiamos al final
//                        fila.put( new Integer( 6 ), new CadenaConLongitud( "", 2 ) );
//                        fila.put( new Integer( 7 ), new CadenaConLongitud( "", 10 ) );
//                        fila.put( new Integer( 8 ),
//                        new CadenaConLongitud( a�oActual + mesActual, 4 ) ); // MMYY
//                        
//                        // el valor 9 lo cambiamos al final
//                        fila.put( new Integer( 9  ), new CadenaConLongitud( String.valueOf( mp.getVlr()     ), 17 ) );                        
//                        fila.put( new Integer( 10 ), new CadenaConLongitud( !mp.getCurrency().equals("PES")? String.valueOf(mp.getVlr_for()) : "" , 17 ) );
//                        fila.put( new Integer( 11 ), new CadenaConLongitud( !mp.getCurrency().equals("PES")? String.valueOf(mp.getVlr_for()) : "" , 17 ) );
//                        fila.put( new Integer( 12 ), new CadenaConLongitud( !mp.getCurrency().equals("PES")? mp.getCurrency(): "" ,  4 ) );
//                        fila.put( new Integer( 13 ), new CadenaConLongitud( fecha, 8 ) );
//                        fila.put( new Integer( 14 ), new CadenaConLongitud( "", 8 ) );
//                        // el valor 15 lo cambiamos al final
//                        fila.put( new Integer( 15 ), new CadenaConLongitud( "", 8 ) );
//                        fila.put( new Integer( 16 ), new CadenaConLongitud( "", 12 ) );
//                        // el valor 17 y 18 fue calculado arriba
//                        fila.put( new Integer( 19 ), new CadenaConLongitud( mp.getDocument(), 8 ) );
//                        fila.put( new Integer( 20 ), new CadenaConLongitud( "Y", 1 ) );
//                        fila.put( new Integer( 21 ), new CadenaConLongitud( "", 17 ) );
//                        fila.put( new Integer( 22 ), new CadenaConLongitud( "7777777", 10 ) );
//                        fila.put( new Integer( 23 ), new CadenaConLongitud( "", 8 ) );
//                        fila.put( new Integer( 24 ), new CadenaConLongitud( "N", 1 ) );
//                        fila.put( new Integer( 25 ), new CadenaConLongitud( "N", 1 ) );
//                        fila.put( new Integer( 26 ), new CadenaConLongitud( "N", 1 ) );
//                        fila.put( new Integer( 27 ), new CadenaConLongitud( "", 64 ) );
//                        fila.put( new Integer( 28 ), new CadenaConLongitud( "", 2 ) );
//                        fila.put( new Integer( 29 ), new CadenaConLongitud( "", 3 ) );
//                        if (!fueInicializadoElArchivoExcel){
//                            inicializarArchivoExcel(fila);
//                        }
//                        guardarFilaEnArchivoPlano( archivoPlano, fila, "" );
//                        guardarFilaEnArchivoExcel( sheet,fila );
//                        
//                        // hacemos los cambios para la fila credito
//                        fila.put( new Integer( 2  ), new CadenaConLongitud( Util.llenarConCerosALaIzquierda( ++c, 4 ), 4 ) );
//                        fila.put( new Integer( 5  ), new CadenaConLongitud( codigoContableAux, 24 ) );
//                        
//                        
//                        fila.put( new Integer( 9  ), new CadenaConLongitud( String.valueOf( -mp.getVlr()     ), 17 ) );
//                        fila.put( new Integer( 10 ), new CadenaConLongitud( !mp.getCurrency().equals("PES")? String.valueOf(-mp.getVlr_for()) : "" , 17 ) );
//                        fila.put( new Integer( 11 ), new CadenaConLongitud( !mp.getCurrency().equals("PES")? String.valueOf(-mp.getVlr_for()) : "" , 17 ) );
//                        fila.put( new Integer( 15 ), new CadenaConLongitud( "", 8 ) );
//                        
//                        guardarFilaEnArchivoPlano( archivoPlano, fila, "" );
//                        guardarFilaEnArchivoExcel( sheet,fila );
//                        
//                        marcados.add(mp);
//                        contadorCH++;
//                    }
//                }
//                
//                // cierra los ultimos archivos abiertos
//                if ( agencias != 0 ){
//                    archivoPlano.flush();
//                    archivoPlano.close();
//                    wb.write( fo );
//                    fo.close();
//                    actualizarSerie(serieCHAux);
//                }
//            }
//            
//
//                       
//            
//            // fin migracion de cheques
//            ////System.out.println("Fin Migracion de Cheques");
//            model.LogProcesosSvc.updateProceso(this.procesoName, this.hashCode(), usuario, "Finalizando Generacion de Cheques");
//            
//            
//            
//            
//            
//            /** codigo ms266 */
//            model.LogProcesosSvc.updateProceso(this.procesoName, this.hashCode(), usuario, "Generacion Formato MS266");
//            generarArchivoMS266();
//            /////////////////////////////////////////////////////////////////////
//            
//            
//            
//            for(int k =0; k< marcados.size();k++){
//                Movpla mp = (Movpla) marcados.elementAt(k);
//                model.movplaService.actualizarMovplaMigradoCHEQUE(mp, fechaProceso );
//            } 
//            
//            
//            
//            model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(), usuario, "Exitoso");
//        }
//        catch ( Exception ex ) {
//            ex.printStackTrace();
//            try{
//                model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(), usuario, "Error : " + ex.getMessage());
//            } catch (Exception e) {}            
//        }
//    }
//    
//    public void crearArchivosParaAgencia(String agencia) throws Exception {
//        File f = new File(ruta);
//        if ( !f.exists() ){
//            f.mkdirs();
//            ////System.out.println("directorios creados en: "+f.getAbsolutePath());
//        }
//        
//        fecha = model.movplaService.obtenerFechaActualEnFormato("YYYYMMDD_HHMIss");
//        
//        /*
//        // este bloque se reemplazo por el bloque de abajo
//        // mfontalvo
//         
//        archivoPlano = new PrintStream( new FileOutputStream( ruta + "/Cheques" + agencia + fecha + ".txt", true ), true );
//         
//        wb = new HSSFWorkbook();
//        sheet = wb.createSheet( "Cheques migrados agencia: "+agencia );
//        fo = new FileOutputStream( ruta + "/Cheques" + agencia + fecha + ".xls" );
//         */
//        
//        
//        
//        ////////////////////////////////////////////////////////////////////////
//        // mfontalvo
//        // Noviembre 28 2005
//        // modificacion para  generar solo un archivo de agencias
//        archivoPlano = new PrintStream( new FileOutputStream( ruta + "/Cheques" + fecha + ".txt", true ), true );
//        
//        wb = new HSSFWorkbook();
//        sheet = wb.createSheet( "Cheques migrados agencia: " + agencia );
//        fo = new FileOutputStream( ruta + "/Cheques" + fecha + ".xls" );
//        
//        fueInicializadoElArchivoExcel = false;
//        fecha = model.movplaService.obtenerFechaActualEnFormato("YYYYMMDD");
//        /////////////////////////////////////////////////////////////////////////
//        
//        
//    }
//    
//    
//    /**
//     * actualizarSerie
//     *
//     * @param s Serie
//     */
//    private void actualizarSerie( Serie s ) throws SQLException {
//        int num = Integer.parseInt(s.getLast_number())+1;
//        s.setLast_number(String.valueOf(num));
//        model.servicioSerie.actualizarSerie(s);
//    }
//    
//    private String fecha;
//    private boolean fueInicializadoElArchivoExcel = false;
//    
//    private void inicializarArchivoExcel(Hashtable fila) {
//        try {
//            filas = 0;
//            HSSFRow row = null;
//            HSSFCell cell = null;
//            for ( int col = 0; col < 29; col++ ) {
//                sheet.setColumnWidth( ( short ) col, (short)(((CadenaConLongitud)fila.get(new Integer(col+1))).getLongitud() * 500) );
//            }
//            HSSFFont fuente1 = wb.createFont();
//            fuente1.setFontName( "Arial" );
//            fuente1.setFontHeightInPoints( ( short ) ( 10 ) );
//            fuente1.setBoldweight( HSSFFont.BOLDWEIGHT_BOLD );
//            fuente1.setColor( HSSFColor.BLACK.index );
//            
//            HSSFCellStyle estiloEncabezado = wb.createCellStyle();
//            estiloEncabezado.setFont( fuente1 );
//            //estilo1.setFillForegroundColor( HSSFColor.ORANGE.index );
//            //estilo1.setFillPattern( ( short ) ( estilo1.SOLID_FOREGROUND ) );
//            /** TEXTO EN EL ENCABEAZADO *************************/
//            HSSFFont fuente2 = wb.createFont();
//            fuente2.setFontName( "Arial" );
//            fuente2.setFontHeightInPoints( ( short ) ( 10 ) );
//            fuente2.setColor( HSSFColor.BLACK.index );
//            
//            estiloNormal = wb.createCellStyle();
//            estiloNormal.setFont( fuente2 );
//            estiloNormal.setFillForegroundColor( HSSFColor.WHITE.index );
//            estiloNormal.setFillPattern( ( short ) ( estiloNormal.SOLID_FOREGROUND ) );
//            
//            String [] encabezados = {"NO. CORRIDA","SECU","FIJO","FIJO","CODIGO CUENTA","TABLA","SUBLEDGER","PERIO","VALOR","FIJO","FIJO","FIJO","FECHA","No. OT","No. Proy","No. Equipo","NO.COMPRO","DESCRIPCION","Referencia","F","FIJO","FIJO","FIJO","F","F","F","FIJO","FI","FI"};
//            
//            row  = sheet.createRow((short)filas++);
//            //row = sheet.getRow( ( short ) ( 0 ) );
//            for (int i = 0; i < encabezados.length; i++) {
//                cell = row.createCell( ( short ) i );
//                cell.setCellStyle( estiloEncabezado );
//                cell.setCellValue( encabezados[i] );
//            }
//            fueInicializadoElArchivoExcel = true;
//        }
//        catch ( Exception e ) {
//            ////System.out.println( e.getMessage() );
//        }
//        
//        
//    }
//    
//    private void inicializarArchivoExcel2(Hashtable fila) {
//        try {
//            String [] encabezados = {"BANK BRANCH","BANK ACCOUNT","ACCOUNTANT","SUPPLIER","INVOICE NO","DATE","DUE DATE","INVOICE AMT","CURRENCY","ADD TAX AMT","DESC1","VALUE1","WH TAX1","AUTH BY","PURCHASE ORDER1","ADD TAX CODE1","ACCOUNT1","WORK ORDER1","DESC 2","VALUE 2","WH TAX2","PURCHARSE ORDER 2","ADD TAX CODE2","ACCOUNT2","WORK ORDER2","DESC3","VALUE3","WH TAX3","PURCHARSE ORDER3","ADD TAX CODE3","ACCOUNT3","WORK ORDER3","PROY_IND"};
//            filas = 0;
//            HSSFRow row = null;
//            HSSFCell cell = null;
//            for ( int col = 0; col < 33; col++ ) {
//                int l = ((CadenaConLongitud)fila.get(new Integer(col+1))).getLongitud();
//                sheet2.setColumnWidth( ( short ) col, (short)((l == 0?10:Math.max(l,encabezados[col].length())) * 400) );
//            }
//            HSSFFont fuente1 = wb2.createFont();
//            fuente1.setFontName( "Arial" );
//            fuente1.setFontHeightInPoints( ( short ) ( 10 ) );
//            fuente1.setBoldweight( HSSFFont.BOLDWEIGHT_BOLD );
//            fuente1.setColor( HSSFColor.BLACK.index );
//            
//            HSSFCellStyle estiloEncabezado = wb2.createCellStyle();
//            estiloEncabezado.setFont( fuente1 );
//            estiloEncabezado.setFillBackgroundColor(HSSFColor.GREY_25_PERCENT.index);
//            estiloEncabezado.setBorderLeft((short)2);
//            estiloEncabezado.setBorderRight((short)2);
//            estiloEncabezado.setBorderBottom((short)2);
//            estiloEncabezado.setBorderTop((short)2);
//            //estiloEncabezado.setFillPattern( ( short ) ( estiloEncabezado. ) );
//            /** TEXTO EN EL ENCABEAZADO *************************/
//            HSSFFont fuente2 = wb2.createFont();
//            fuente2.setFontName( "Arial" );
//            fuente2.setFontHeightInPoints( ( short ) ( 10 ) );
//            fuente2.setColor( HSSFColor.BLACK.index );
//            estiloNormal = wb2.createCellStyle();
//            estiloNormal.setFont( fuente2 );
//            estiloNormal.setFillForegroundColor( HSSFColor.WHITE.index );
//            estiloNormal.setFillPattern( ( short ) ( estiloNormal.SOLID_FOREGROUND ) );
//            
//            
//            
//            row  = sheet2.createRow((short)filas++);
//            //row = sheet.getRow( ( short ) ( 0 ) );
//            for (int i = 0; i < encabezados.length; i++) {
//                cell = row.createCell( ( short ) i );
//                cell.setCellStyle( estiloEncabezado );
//                cell.setCellValue( encabezados[i] );
//            }
//        }
//        catch ( Exception e ) {
//            ////System.out.println( e.getMessage() );
//        }
//        
//        
//    }
//    
//    /**
//     * guardarFilaEnArchivoExcel
//     *
//     * @param fila Hashtable
//     */
//    private void guardarFilaEnArchivoExcel( HSSFSheet hoja,Hashtable fila ) {
//        
//        HSSFRow row = hoja.createRow((short)filas++);
//        //row = sheet.getRow(filas);
//        for ( int i = 1; i <= fila.size(); i++ ) {
//            CadenaConLongitud str = ( CadenaConLongitud ) fila.get( new Integer( i ) );
//            if ( str == null ){
//                break;
//            }
//            HSSFCell cell = row.createCell( ( short ) (i-1) );
//            cell.setCellStyle( estiloNormal );
//            cell.setCellValue( str.getStr() );
//        }
//        
//    }
//    
//    /**
//     * guardarFilaEnArchivoPlano
//     *
//     * @param fila Hashtable
//     */
//    private void guardarFilaEnArchivoPlano( PrintStream archivo, Hashtable fila, String separador ) {
//        StringBuffer linea = new StringBuffer();
//        int i;
//        for ( i = 1; i <= fila.size(); i++ ) {
//            CadenaConLongitud str = ( CadenaConLongitud ) fila.get( new Integer( i ) );
//            if ( str == null ){
//                break;
//            }
//            linea.append( Util.llenarConEspaciosAlFinal( str.getStr(), str.getLongitud() ) );
//            linea.append(separador);
//        }
//        if ( separador.length() > 0 ){
//            linea.deleteCharAt(linea.length()-separador.length());
//        }
//        archivo.println( linea.toString() );
//        //////System.out.println("Linea: "+linea);
//    }
//    
//    
//    
//    /**
//     *Metodo que genera un archivo CSV para la migracion de ajustes y/o facturas
//     *@autor: Karen Reales
//     *@throws: En caso de que un error de escritura ocurra.
//     */
//    private void generarArchivoMS266() throws Exception{
//        try{
//            /**
//             *Se llena el archivo que migra ajustes
//             */
//            
//            Vector  marcados = new Vector();
//            java.text.SimpleDateFormat s = new java.text.SimpleDateFormat("MMdd_hhmmss");           
//            String nombreArchivo =  rutaMs + "/MS266_"+ s.format( new Date() ) +".csv";
//            archivoPlano2 = new PrintStream(nombreArchivo);
//            
//            
//            
//            Vector movimientos = model.movplaService.obtenerDatosInterfaceContable( fechaProceso );
//            if ( !movimientos.isEmpty() ){
//                for( int i=0; i<movimientos.size(); i++ ){
//                    Hashtable fila = (Hashtable) movimientos.elementAt(i);
//                    Movpla mp = new Movpla();
//                    mp.setCreation_date(fila.get("creation_date").toString());
//                    mp.setConcept_code(fila.get("concept_code").toString());
//                    mp.setPlanilla(fila.get("planilla").toString());
//                    mp.setPla_owner(fila.get("pla_owner").toString());
//                    mp.setDocument(fila.get("document").toString());
//                    mp.setCausa( mp.getConcept_code() +","+mp.getPla_owner() + "," + mp.getCreation_date()  );
//                    String linea="";
//                    for(int j = 1; j<=33; j++){
//                        CadenaConLongitud cadena =(CadenaConLongitud)fila.get(new Integer(j));
//                        linea =linea + cadena.getStr() +(j==33?"":",");
//                    }
//                    archivoPlano2.println(linea);
//                    marcados.add(mp);
//                }
//                
//            }
//            archivoPlano2.close();
//            
//            
//            // actualizacion de movimientos migrados
//            for(int k =0; marcados!=null && k<marcados.size();k++){
//                Movpla mp = (Movpla) marcados.elementAt(k);
//                model.movplaService.actualizarMovplaMigrado(mp, fechaProceso );
//            }
//            
//        } catch (Exception e){
//            e.printStackTrace();
//            throw new Exception (e.getMessage());
//        }
//        
//    }
//    
//    
//    
//    //FERNELL 251005
//    /*  MIGRACION OP __________________________________________________________*/
//    
//    public void migrarOP() throws Exception{
//        
////        try{
////            model.MigrarOP266Svc.serachOP();
////            List lista = model.MigrarOP266Svc.getListadoOP();
////            if(  lista!=null && lista.size()>0  ){
////                Iterator it = lista.iterator();
////                while ( it.hasNext() ){
////                    OP266  op  = (OP266) it.next();
////                    List items = op.getItems();
////                    if( items!=null  && items.size()>0){
////                        String leneaHead =  op.getBanco()        + ","+ op.getSucursal()     + ","+  op.getGrabador()     + ","+ op.getPlaca()        + ","+ op.getOP()           + ","+ op.getFechaCumplido()+ ","+ op.getFechaPago()    + ","+ op.getValor()        + ","+ op.getMoneda()       + ","+ op.getVlrIca()       ;
////                        int contItem     = 0;
////                        int intSec       = 0;
////                        int swLinea      = 0;
////                        Iterator itI     = items.iterator();
////                        String lineaItem = "";
////                        while (itI.hasNext() ){
////                            Items  item  = (Items) itI.next();
////                            contItem++;
////                            swLinea      = 1;
////                            if(contItem>3){
////                                this.archivoPlano2.println( leneaHead + "," +  lineaItem );
////                                lineaItem = "";
////                                contItem  = 1;
////                                intSec    ++;
////                                leneaHead =  op.getBanco()   + ","+ op.getSucursal()  + ","+  op.getGrabador()  + ","+ op.getPlaca()         + ","+  op.getOP()+"_"+intSec  + ","+ op.getFechaCumplido()   + ","+ op.getFechaPago()     + ","+ op.getValor()     + ","+ op.getMoneda()   + ","+ op.getVlrIca()  ;
////                                swLinea   = 0;
////                            }
////                            lineaItem     +=  item.getItemDesc() + ","+ item.getValor()   + ","+ item.getWhTax()     + ","+ item.getAutorizado() + ","+ item.getOC()            + ","+item.getReteIca()        + ","+ item.getAccountCode() + ","+  item.getOT() ;
////                        }
////                        if( swLinea==1)
////                            this.archivoPlano2.println( leneaHead + "," +  lineaItem );
////                    }
////                }
////                model.MigrarOP266Svc.updateDocumento();
////            }
////        }catch(Exception e){
////            throw new Exception( "Migrando OP: " +e.getMessage() );
////        }
    }
    
    
    public static void main(String a []){
        HExport_Migracion hilo = new HExport_Migracion();
        hilo.start("MARIO");
    }    
}
//Entregado a Mario 16 febrero de 2007
