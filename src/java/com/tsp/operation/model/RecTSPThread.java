
/*
 * RecTSP.java
 *
 * Created on 24 de junio de 2005, 01:11 PM
 */

package com.tsp.operation.model;

import java.io.*;
import java.util.*;
import java.util.Date;
import java.text.*;
import com.tsp.operation.model.beans.*;

/**
 *
 * @author  Sandrameg
 */
public class RecTSPThread extends Thread{
    
    private Model  model;
    private String fchi;    
    private String us;
    private String ds;
    private String tp;
    private MainRD recTSP;
    private String procesoName;
    private String des;
    private String hoy;
    
    /** Creates a new instance of RecTSP */
    public RecTSPThread() {
    }
    
    public void star ( String u, String fi, String d, String t) {
        this.us = u;
        this.fchi = fi;        
        this.ds = d;
        this.tp = t;
        SimpleDateFormat s=null;
        s = new SimpleDateFormat("yyyy-MM-dd");
        hoy = s.format(new java.util.Date());

        if (tp.equals("nuevo")){
            this.procesoName = "Nuevo Proceso Recurso"; 
            this.des = "Periodo desde "+fi+" Hasta "+hoy;
        }
        else{
            this.procesoName = "Actualizar Proceso Recurso";
            this.des = "Actualizando";
        }
        
        super.start();
    }
      
    public synchronized void run(){
        model = new Model();
        try {
            model.LogProcesosSvc.InsertProceso(this.procesoName, this.hashCode(), des, this.us);  
            recTSP = new MainRD( us, fchi, ds, tp);
            recTSP.principalRecTSP();
            model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(), this.us, "PROCESO EXITOSO");
        }
        catch(Exception e){
            try{
                 model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(),this.us,"ERROR :" + e.getMessage()); 
             }
             catch(Exception f){ 
                try{ 
                    model.LogProcesosSvc.finallyProceso(this.procesoName,this.hashCode(),this.us,"ERROR :"); 
                }catch(Exception p){    }
             }
        }
    }
}
