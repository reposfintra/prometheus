/*
 * HMigracionRemesasAnuladas.java
 *
 * Created on 16 de julio de 2005, 04:37 PM
 */

package com.tsp.operation.model;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.text.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;
import com.tsp.util.Util;
import java.util.Vector;
import java.io.*;

/**
 *
 * @author  Tito Andr�s Maturana
 */
public class HMigracionRemesasAnuladas extends Thread{
    private Vector remesas_anuladas;
    
    private FileWriter fw;
    private BufferedWriter bffw;
    private PrintWriter pntw;
    
    
    /** Creates a new instance of HMigracionRemesasAnuladas */
    public HMigracionRemesasAnuladas() {
    }
    
    public void start(Vector remesas_anuladas) {
        this.remesas_anuladas = remesas_anuladas;
        
        try{
            String nombre_archivo = "";
            String ruta = "/exportar/migracion/";
            String fecha_actual = Util.getFechaActual_String(6);
            
            ruta += fecha_actual.substring(0,4) + fecha_actual.substring(5,7);
            nombre_archivo = ruta + "/anula620" + fecha_actual.replace(":","").replace("/","").replace(' ','_') + ".csv";
            
            File archivo = new File(ruta.trim());
                
            archivo.mkdirs();
            FileOutputStream file = new FileOutputStream(nombre_archivo.trim());
            
            fw = new FileWriter(nombre_archivo);
            bffw = new BufferedWriter(fw);
            pntw = new PrintWriter(bffw);
        }
        catch(java.io.IOException e){
            ////System.out.println(e.toString());
        }
        
        super.start();
    }
    
    public synchronized void run(){
       this.writeFile();
    }
    
    protected void writeFile() {    	
    	String linea = "";          
    	pntw.println("estoy aqui"); 
        pntw.println("estoy aqui");  
        pntw.println(remesas_anuladas.size());
        for(int i=0; i<remesas_anuladas.size(); i++){
            RemesaAnulada remesa_anulada = (RemesaAnulada) remesas_anuladas.elementAt(i);
            linea = remesa_anulada.getNumero_remesa() + "," + remesa_anulada.getFecha_creacion().substring(0,10) + "," +
                remesa_anulada.getUsuario_creacion() + ",AN";
            ////System.out.println(linea);
            pntw.println(linea);
        }

        pntw.close();
        	    	    	
    }
    
    protected String getFormatDate(String fecha){
        String formato="";
        if(!fecha.equals("")){
            String  ano = fecha.substring(2,4);
            String  mes = fecha.substring(5,7);
            String  dia = fecha.substring(8,10);
            formato     = dia+"/"+mes+"/"+ano;
        }
        return formato;
    }
}
