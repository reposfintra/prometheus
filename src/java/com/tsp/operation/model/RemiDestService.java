/*
 * RemiDestService.java
 *
 * Created on 9 de diciembre de 2004, 10:03 AM
 */
package com.tsp.operation.model;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
/**
 *
 * @author  KREALES
 */
public class RemiDestService {
    RemiDestDAO rd= new RemiDestDAO();
    /** Creates a new instance of RemiDestService */
    
    public RemiDestService() {
    }
    
    public TreeMap getCiudades(){
        return rd.getCiudades();
    }
    
    public void searchCiudades(String cod)throws SQLException{
        rd.searchCiudades(cod);
    }
    public void searchCiudadesr(String cod, String origen)throws SQLException{
        rd.searchCiudadesr(cod,origen);
    }
    public List getRemitentes(String codcli)throws SQLException{
        rd.searchListaR(codcli);
        return rd.getListR();
    }
    public List getRemitentes(String codcli,String ciudad)throws SQLException{
        rd.searchListaR(codcli,ciudad);
        return rd.getListR();
    }
    public Vector getVectorRemiDest(){
        return rd.getVecRd();
    }
    
    public List getDestinatarios(String codcli)throws SQLException{
        rd.searchListaD(codcli);
        return rd.getListD();
    }
    
    public List getDestinatarios(String codcli, String ciudad, String nombre)throws SQLException{
        rd.searchListaD(codcli,ciudad,nombre);
        return rd.getListD();
    }
    
    public String getNombre(String codcli)throws SQLException{
         return rd.searchNombre(codcli);
    }
    /**
     * Metodo insertarRemitenteDestinatario, metodo para accesar al metodo que accede a la 
     * Base de datos para ingesar un Remitente o Detinatario
     * @autor : Ing. Henry A. Osorio Gonz�lez
     * @see : insertarRemitenteDestinatario  -  RemiDestDAO
     * @param : RemiDest RemitenteDestinatario
     * @version : 1.0
     */
    public void insertarRemitenteDestinatario(RemiDest remdest) throws SQLException{        
        rd.insertarRemitenteDestinatario(remdest);
    }
    /**
     * Metodo existeRemitenteDestinatario, determina si un codigo de remtiente o destinatario 
     * existe accesando al metodo ubicado en el DataAccessObject
     * @autor : Ing. Henry A. Osorio Gonz�lez
     * @see : existeRemitenteDestinatario  -  RemiDestDAO
     * @param : String codigo
     * @return : true si el codigo existe � false si no existe
     * @version : 1.0
     */
    public boolean existeRemitenteDestinatario(String codigo) throws SQLException{    
        return rd.existeRemitenteDestinatario(codigo);
    }
    /**
     * Metodo updateRemitenteDestinatario, recibe el objeto Remitente Destinatario 
     * y actualiza el registro con los nuevos valores del objeto en la BD
     * @autor : Ing. Henry A. Osorio Gonz�lez
     * @see : updateRemitenteDestinatario  -  RemiDestDAO
     * @param : RemiDest RemitenteDestinatario
     * @version : 1.0
     */
    public void updateRemitenteDestinatario(RemiDest remdest)throws SQLException {    
        rd.updateRemitenteDestinatario(remdest);
    }
    /**
     * Metodo listarRemitentesDestinatarios, busca los remitentes y los destinatarios
     * que cumplan con el criterio de busqueda
     * @autor : Ing. Henry A. Osorio Gonz�lez
     * @param : String codigo
     * @version : 1.0
     */
    public void listarRemitentesDestinatarios(String codigo)throws SQLException{      
        rd.listarRemitentesDestinatarios(codigo);
    }
    
     // LOS SIGUIENTES M�TODOS Y/O ATRIBUTOS FUERON COPIADOS DE LA APLICACI�N SOT
    // PARA IMPLEMENTAR EL MODULO DE ADMINISTRACI�N DE USUARIOS.
    // Alejandro Payares - 13.12.05 - 11:05 am
    
    public void destinatarioSearch() throws SQLException {
        rd.destinatarioSearch();
    }
    
    /**
     * Crea una lista de objetos de destinatarios asociados a un determinado
     * cliente.
     * @param codCliente El c&oacute;digo del cliente.
     * @throws SQLException Si un error de acceso a la base de datos ocurre.
     * @autor Alejandro Payares.
     */
    public void destinatarioSearch(String codCliente) throws SQLException {
        rd.destinatarioSearch(codCliente);
    }
    
    /**
     * Crea una lista o TreeMap de objetos de destinatarios asociados a un determinado
     * cliente.
     * @param codCliente El c&oacute;digo del cliente.
     * @param guardarEnTreeMap bandera para saber si se quieren guardar los detinatarios en un TreeMap o no
     * si la bandera es falsa se guardan en una lista.
     * @throws SQLException Si un error de acceso a la base de datos ocurre.
     * @autor Alejandro Payares.
     */
    public void destinatarioSearch(String codCliente, boolean guardarEnTreeMap) throws SQLException {
        rd.destinatarioSearch(codCliente,guardarEnTreeMap);
    }
    
    /**
     * Devuelve un java.util.TreeMap (llave=codigo del destinatario; valor= nombre del destinatario)
     * el cual es creado por el m�todo destinatarioSearch(String codCliente, boolean guardarEnTreeMap)
     * cuando el parametro guardarEnTreeMap es igual a true.
     * @return el TreeMap con los codigos y nombre de los remitentes encontrados.
     * @autor Alejandro Payares.
     */
    public TreeMap getTgetDestinatarios(){
        return rd.getTgetDestinatarios();
    }

    /**
     * Devuelve una java.util.List con los objetos com.tsp.operation.model.beans.RemiDest
     * encontrados por los m�todos destinatarioSearch(String codCliente, boolean guardarEnTreeMap)
     * cuando el parametro guardarEnTreeMap es igual a false o tambi�n llamando al m�todo
     * destinatarioSearch(String codCliente) que por default guarda en una lista.
     * @return la List con los remitentes encontrados.
     * @autor Alejandro Payares.
     */
    public List getDestinatarios() {
        return rd.getDestinatarios();
    }
    /**
     * Metodo searchListaD, busca un vector de destinatarios 
     * dado el cliente y una ciudad
     * @autor : Ing. Karen Reales
     * @param : String codclo, ciudad
     * @version : 1.0
     */
    public void searchListaD(String codcli,String ciudad, String nombre )throws SQLException{
        rd.searchListaD(codcli,ciudad, nombre);
    }
    
     public void setVectorRemiDest(Vector v){
        rd.setVecRd(v);
    }
     
          /**
     * Metodo listaDestinatariosPla, busca un vector de destinatarios 
     * dado el cliente y una ciudad
     * @autor : Ing. Karen Reales
     * @param : String codclo, ciudad
     * @version : 1.0
     */
    public void listaDestinatariosPla(String numrem )throws SQLException{
        rd.listaDestinatariosPla(numrem);
    }
    /**
     * Metodo searchRemiDest, busca los datos de un destinatario especifico
     * @autor : Ing. Karen Reales
     * @param : String codigo del destinatario
     * @version : 1.0
     */
    public void searchRemiDest(String codigo )throws SQLException{
        rd.searchRemiDest(codigo);
    }
      /**
     * Getter for property rd.
     * @return Value of property rd.
     */
    public com.tsp.operation.model.beans.RemiDest getRd() {
        return rd.getRd();
    }
    
    /**
     * Setter for property rd.
     * @param rd New value of property rd.
     */
    public void setRd(com.tsp.operation.model.beans.RemiDest rd1) {
        rd.setRd(rd1);
    }
     
     
}
