/*
 * PlanillaImpresionService.java
 *
 * Created on 17 de diciembre de 2004, 7:39
 */

package com.tsp.operation.model;

import com.tsp.operation.model.beans.*;
import java.util.*;
/**
 *
 * @author  Mario Fontalvo
 */
public class PlanillaImpresionService {
    
    private PlanillaImpresionDAO PlanImpDataAccess;
    //variables de consulta    
    private CIA  Cia;
    private List Planillas;
    private DatosPlanillaImpPDF datos;//JEscandon

    ///////////////////////////////////////////////
    private List PlanExport;
    
    /** Creates a new instance of PlanillaImpresionService */
    public PlanillaImpresionService() throws Exception {
        try{
           PlanImpDataAccess = new PlanillaImpresionDAO();
           datos = new  DatosPlanillaImpPDF();
        }catch(Exception ex){
                throw new Exception ("Error en PlanillaImpresionService [RegistroTiempoService]...\n"+ex.getMessage());
        }            
    }
    
    public void BuscarDatosCIA()throws Exception{
        try{           
	    this.Cia = PlanImpDataAccess.BuscarCIA("FINV");
	}catch(Exception ex){
	    throw new Exception ("Error en BuscarDatosCIA [PlanillaImpresionService]...\n"+ex.getMessage());
	}        
    }
    
    public void BuscarPlaNoImp(String Agencia,String base,String usuario)throws Exception{  
        try{           
            this.ReiniciarPlanillas();
	    this.Planillas = PlanImpDataAccess.BuscarPlanillasNoImp("",usuario,"2");
	}catch(Exception ex){
	    throw new Exception ("Error en BuscarPlaNoImp [PlanillaImpresionService]...\n"+ex.getMessage());
	}        
    }
    
    public void BuscarPlanilla(String Agencia,String NumeroPlanilla,String base,String usuario)throws Exception{  
        try{      
            this.ReiniciarPlanillas();
	    this.Planillas = PlanImpDataAccess.BuscarPlanillasNoImp(NumeroPlanilla,"","1");
	}catch(Exception ex){
	    throw new Exception ("Error en BuscarPlanilla [PlanillaImpresionService]...\n"+ex.getMessage());
	}        
    }
    
    public void ActualizarFechaImpresion(String NumeroPlanilla)throws Exception{  
        try{      
            PlanImpDataAccess.ActualizarFechaImpresion(NumeroPlanilla);
	}catch(Exception ex){
	    throw new Exception ("Error en ActualizarFechaImpresion [PlanillaImpresionService]...\n"+ex.getMessage());
	}        
    }
    
    ///////////////////////////////////////////////
    public void BuscarPlanillasExport(String FechaCreacionOC)throws Exception{  
        try{      
            this.ReiniciarPlanillasExoprt();
	    this.setPlanillasExport(PlanImpDataAccess.BuscarPlanillas(FechaCreacionOC));
	}catch(Exception ex){
	    throw new Exception ("Error en BuscarPlanillasExport (MS230828)  [PlanillaImpresionService]...\n"+ex.getMessage());
	}        
    }
    
    
    
    
    //setter
    public void ReiniciarCIA(){
        this.Cia = null;
    }
    public void ReiniciarPlanillas(){
        this.Planillas = null;
    }        
    public void setPlanillas(List valor){
        this.Planillas = valor;
    }
    
    ////////////////////////////////////////
    public void ReiniciarPlanillasExoprt(){
        this.PlanExport = null;
    }            
    public void setPlanillasExport(List valor){
        this.PlanExport = valor;
    }
    
    //getter
    public CIA getCIA(){
        return this.Cia;
    }
    public List getPlanillas(){
        return this.Planillas;
    }
    /////////////////////////////////////////
    public List getPlanillasExport(){
        return this.PlanExport;
    }
    
    //FUNCIONES DE KAREN REALES
    public void searchPlanillas(String Fecha,String base)throws Exception{
        PlanImpDataAccess.searchPlanillas(Fecha,base);
    }
    
    public java.util.Vector getPlas() {
        return PlanImpDataAccess.getPlas();
    }
    public void searchPlanillas_CGAGEN(String Fecha,String base)throws Exception{
        PlanImpDataAccess.searchPlanillas_CGAGEN(Fecha, base);
    }
    
    ///juanm    
    public void BuscarPlaNoImpRemesa(String NumRemesa)throws Exception{  
        try{           
            this.ReiniciarPlanillas();
	    this.Planillas = PlanImpDataAccess.BuscarPlanillasNoImpRemesa(NumRemesa);
	}catch(Exception ex){
	    throw new Exception ("Error en BuscarPlaNoImp [PlanillaImpresionService]...\n"+ex.getMessage());
	}        
    }
     
      /**
     * Getter for property datos.
     * @return Value of property datos.
     */
    public com.tsp.operation.model.beans.DatosPlanillaImpPDF getDatos() {
        return datos;
    }
    
    /**
     * Setter for property datos.
     * @param datos New value of property datos.
     */
    public void setDatos(com.tsp.operation.model.beans.DatosPlanillaImpPDF datos) {
        this.datos = datos;
    }
    
    public void ReiniciarDatos(){
        this.datos = null;
    }


  /**
     * Metodo BuscarDatosCIA, buscar los datos correspondientes a la compa�ia
     * @autor : Ing. Juan Manuel Escandon Perez
     * @see   : listVehPerdidos - ReporteVehPerdidosDAO
     * @version : 1.0
     */
    public void DatosCIA( String Distrito )throws Exception{
        try{
            //datos = new DatosPlanillaImpPDF();
            datos = PlanImpDataAccess.BuscarDatosCIA(Distrito);
            
        }catch(Exception ex){
            throw new Exception("Error en DatosCIA [PlanillaImpresionService]...\n"+ex.getMessage());
        }
    }
    
    /**
     * Metodo BuscarDatosPlanilla, buscar los datos correspondientes a la planilla
     * @autor : Ing. Juan Manuel Escandon Perez
     * @see   : listVehPerdidos - ReporteVehPerdidosDAO
     * @version : 1.0
     */
    public void DatosPlanilla( String Numpla )throws Exception{
        try{
            PlanImpDataAccess.BuscarDatosPlanilla( Numpla, datos );
            
        }catch(Exception ex){
            throw new Exception("Error en DatosPlanilla [PlanillaImpresionService]...\n"+ex.getMessage());
        }
    }
    
      /**
     * Funcion para actualizar el registro en el movpla con la fecha de migracion
     * @autor : Ing. Karen Reales
     * @param : String numero de la planilla
     * @version : 1.0
     */
    public void actualizarMigracion(String numpla)throws Exception{
        PlanImpDataAccess.actualizarMigracion(numpla);
    }
    
        /**
     * Funcion para actualizar el registro en el movpla con la fecha de migracion
     * @autor : Ing. Karen Reales
     * @param : String numero de la planilla
     * @version : 1.0
     */
    public String actualizarMigracion(String numpla, String creacion, String concept_code, String cheque)throws Exception{
        return PlanImpDataAccess.actualizarMigracion(numpla,creacion,concept_code, cheque);
    }
    
    /*Jescandon 30-04-07*/
    public void BuscarPlaNoImpPlanViaje(String Agencia,String base,String usuario)throws Exception{  
        try{           
            this.ReiniciarPlanillas();
	    this.Planillas = PlanImpDataAccess.BuscarPlaNoImpPlanViaje("",usuario,"2");
	}catch(Exception ex){
	    throw new Exception ("Error en BuscarPlaNoImpPlanViaje [PlanillaImpresionService]...\n"+ex.getMessage());
	}        
    }
    
}
