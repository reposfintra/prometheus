/*
 * Conciliacion.java
 *
 * Created on 13 de junio de 2005, 02:57 PM
 */

package com.tsp.operation.model;
import java.sql.*;
import java.io.*;
import java.util.*;
import com.tsp.util.connectionpool.PoolManager;
/**
 *
 * @author  Administrador
 */
public class Conciliacion {
    Connection conn = null;
    static FileOutputStream fos;
    String dataBaseName = "fintra";
    /** Creates a new instance of Conciliacion */
    public Conciliacion(){
         
    }
    public Conciliacion(String dataBaseName){
         this.dataBaseName = dataBaseName;
    }

    
    public String compararconPlacas(String line, String fch)throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PreparedStatement st1 = null;
        ResultSet rs1 = null;
        PoolManager poolManager = null;
        
        String [] testString = null;
        String patternString = ";";
        
        int swp = 0;//0= Placa vacia 1=Placa llena
        
        String resul= "";
        
        ///linea -> vector
        testString = line.split(patternString);
        
        //imprimiendo placas
        String placa = "";
        for (int i = 0; i < testString.length; i++){
            if ( i == 16 ){
                if (!testString[i].equalsIgnoreCase("")){
                    //obtengo placa de archivo
                    placa =  testString[i];
                    swp = 1;
                }
            }
        }
        
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.dataBaseName);       
                       
            ///sincronizacion con placas en bd
            if ( swp == 1 ){
                if(con!=null){
                    st = con.prepareStatement("Select placa from placa where placa = '" + placa +"'");
                    rs = st.executeQuery();
                    
                    //si la placa esta en la bd
                    if(rs.next()){                        
                        //buscar placa en la planilla
                        if(con != null){
                            st1 = con.prepareStatement("SELECT pla.plaveh, pla.fecdsp, pla.numpla as planilla FROM PLANILLA pla  WHERE  pla.corte like ? and pla.base = 'spo' and plaveh = ?");
                            st1.setString(1, fch + "%");
                            st1.setString(2, placa);
                            rs1 = st1.executeQuery();
                            
                            if (!rs1.next()){
                                resul = " -> " + placa + " \n";
                                //////System.out.println(" NO ESTA EN LA PLANILLA: " + placa) ;
                            }
                            else{                                
                                resul = "vacio";
                                //////System.out.println(" SI ESTA EN LA PLANILLA: " + placa) ;
                            }
                        }
                    }
                }
            }
        }catch(SQLException e){            
            throw new SQLException("ERROR DURANTE LA CONCILIACION DE LAS PLACAS " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{            
            if (st != null){
                try{                
                    st.close();
                }
                catch(SQLException e){                    
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            if (st1 != null){
                try{                
                    st1.close();
                }
                catch(SQLException e){                    
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
        }
        if (con != null){            
            poolManager.freeConnection(this.dataBaseName, con);
        }
        return resul;
    }
    
    public void enviarCorreo(String placas, String fecha, String ruta)throws SQLException{
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection(this.dataBaseName);
            if(con!=null){
                st = con.prepareStatement("insert into sendmail (emailfrom, emailto, emailcopyto, emailsubject, emailbody, sendername) values(?,?,'',?,?,?)"); 
                st.setString(1, "masivo@mail.tsp.com");
                st.setString(2, "kreales@mail.tsp.com");
                st.setString(3, "Masivo Informe de Conciliacion");
                st.setString(4, "La conciliacion del Archivo " + ruta + "\n en la fecha " + fecha + " arrojo como resultado: \n Las siguientes placas no se encuentran en las planillas asociadas segun los datos ingresados: \n " + placas);
                st.setString(5, "- Transporte Masivo -");
                st.executeUpdate();
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA INSERCCION DEL CORREO " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO " + e.getMessage());
                }
            }
            if (con != null){
                poolManager.freeConnection(this.dataBaseName, con);
            }
        }
    }
}
