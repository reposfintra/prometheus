/*
 * TiporecDAO.java
 *
 * Created on 18 de junio de 2005, 09:11 AM
 */

package com.tsp.operation.model;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.swing.*;
import com.tsp.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;
/**
 *
 * @author  Jm
 */
public class TiporecDAO {
    Tiporec tp;
    Vector tiposr;
    /** Creates a new instance of TiporecDAO */
    public TiporecDAO() {
    }
    
    /**
     * Getter for property tiposr.
     * @return Value of property tiposr.
     */
    public java.util.Vector getTiposr() {
        return tiposr;
    }
    
    /**
     * Setter for property tiposr.
     * @param tiposr New value of property tiposr.
     */
    public void setTiposr(java.util.Vector tiposr) {
        this.tiposr = tiposr;
    }
    
    /**
     * Getter for property tp.
     * @return Value of property tp.
     */
    public Tiporec getTp() {
        return tp;
    }
    
    /**
     * Setter for property tp.
     * @param tp New value of property tp.
     */
    public void setTp(Tiporec tp) {
        this.tp = tp;
    }
    
    public void list(String codtipo)throws SQLException{
        String consulta = "select * from tipo_rec where codigo = ?";
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement(consulta);
                st.setString(1, codtipo);
                rs= st.executeQuery();
                tiposr = new Vector();
                while(rs.next()){
                    tp = new Tiporec();
                    
                    tp.setDistrito(rs.getString("dstrct"));
                    tp.setCodigo(rs.getString("codigo"));
                    tp.setTipo(rs.getString("tipo"));
                    
                    tiposr.add(tp);
                    
                    
                }
            }
            
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE EL PROCESO DE LISTAR LOS TIPOS DE RECURSOS" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        
    }
    
    
    
}
