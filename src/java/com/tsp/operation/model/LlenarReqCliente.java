/*
 * LenarReqCliente.java
 *
 * Created on 25 de mayo de 2005, 10:12 PM
 */
package com.tsp.operation.model;
import java.io.*;
import java.sql.*;
import java.util.*;
import java.io.*;
import javax.swing.*;
import java.text.*;
import com.tsp.operation.model.beans.*;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import java.net.URL;
import com.tsp.util.*;
import com.tsp.operation.model.threads.*;


/**
 *
 * @author  DIOGENES
 */
public class LlenarReqCliente {
    
    private static Properties dbProps = new Properties();
    private static String dstrct="";
    private static String hoy="";
    private static String fin_pro="";
    private static String CDia="";
    private static String Usuario="";
    private static Vector vreq = new Vector();
    private static Vector vreqF = new Vector();
    //beans
    private static Pto_ventas pto_venta;
    private static ReqCliente reqcliente;
    private static ReqFrontera reqfrontera;
    private static StdJob stdjob;
    private static Model model = new Model();
    //lista error
    private List ListaIncosistencias = new LinkedList();
    ////sandrameg
    private static String fi = "";
    private static String ff = "";
    private static String distrito = "";
    private static String tipo = "";
    private static
    int NoInfo = 0,info=0,a=0;
    
    
    /** Creates a new instance of LenarReqCliente */
    public LlenarReqCliente( String u, String fi, String ff, String d, String tp) {
        this.Usuario = u;
        this.fi = fi;
        this.ff = ff;
        this.distrito = d;
        this.tipo = tp;
    }
    
    public void cargarPropiedades() throws IOException{
        InputStream is = getClass().getResourceAsStream("Tiempos.properties");
        dbProps.load(is);
    }
    
    
     public void crearRecursoFrontera(ReqFrontera  reqfrontera, String fecha)throws Exception {
        boolean recurso = false;
        try{
            Recursosdisp rec = new Recursosdisp();
            rec.setDistrito(dstrct);
            rec.setNumpla(reqfrontera.getnumpla());
            rec.setOrigen(reqfrontera.getoripla());
            rec.setDestino(reqfrontera.getdespla());
            rec.setFecha_disp(Util.ConvertiraTimestamp(reqfrontera.getfecdispo()));
            rec.setStd_job_no_rec(reqfrontera.getstd_job_no());
            rec.setPlaca(reqfrontera.getId_rec_tra());
            rec.setEquipo_aso(reqfrontera.getEquipo_aso());
            rec.setFecha_creacion(Util.ConvertiraTimestamp(fecha));
            rec.setFecha_actualizacion(Util.ConvertiraTimestamp(fecha));
            rec.setUsuario_creacion(Usuario);
            rec.setCod_cliente(reqfrontera.getcliente());
            
            //busco el tramo
            model.trSvc.list(reqfrontera.getoripla(), reqfrontera.getdespla());
            Tramo tr = model.trSvc.getTr();
            if(tr!=null){
                if( tr.getTiempo() != 0.0){
                    rec.setTiempo( tr.getTiempo() );
                    rec.setOrigen( tr.getOrigen() );
                    rec.setDestino( tr.getDestino() );
                    model.vehSvc.listVeh(reqfrontera.getId_rec_tra());
                    Placa p = model.vehSvc.getV();
                    
                    if(p!=null){
                        //rec.setAgencia(p.getAgencia());
                        rec.setEquipo_propio( p.getPropietario().equals("890103161") ? "S" : "N" );
                        //busca la clase y tipo
                        Hashtable info = model.reqclienteServices.buscarTipoClaseRecurso( p.getRecurso() );
                        
                        if(info != null){
                            rec.setClase( (String)info.get("Clase") );
                            rec.setTipo( (String)info.get("Tipo") );
                            model.rdSvc.setRdisp(rec);
                            //busca el nro viajes
                            model.rdSvc.Search_record(reqfrontera.getId_rec_tra());
                            rec.setNo_Viajes(model.rdSvc.getRdisp().getNo_Viajes());
                            //busca Work_gorup
                            rec.setWork_gorup(model.rdSvc.Search_Wgroup(reqfrontera.getId_rec_tra()));
                            rec.setAgencia(model.reqclienteServices.Buscar_agasocXcod(reqfrontera.getoripla()));
                            recurso = true;
                        }
                        else{
                            RegistrarInconsistencia(6, reqfrontera.getnumpla(), p.getRecurso(), reqfrontera.getId_rec_tra() );
                        }
                        
                    }
                    else{
                        RegistrarInconsistencia(4, reqfrontera.getnumpla(), reqfrontera.getId_rec_tra(), reqfrontera.getId_rec_tra() );
                    }
                }else{
                    RegistrarInconsistencia(3, reqfrontera.getoripla()+"-"+reqfrontera.getdespla(), String.valueOf(tr.getTiempo()), reqfrontera.getId_rec_tra());
                }
            }
            else{
                RegistrarInconsistencia(1, reqfrontera.getnumpla(), reqfrontera.getoripla()+"-"+reqfrontera.getdespla(), reqfrontera.getId_rec_tra());
            }
            if(recurso){
                if( !model.rdSvc.BuscarRecurso(reqfrontera.getId_rec_tra())){
                    model.rdSvc.insertFrontera();
                }
                else
                    model.rdSvc.Update();
            }
        }catch ( Exception e ) {
            //throw new Exception("Error al accesar con la bd "+e.getMessage());
            e.printStackTrace();
        }
    }
    //verifico si existe otra oc con esa ot y si su (age_aso_des) OC es <>  (age_aso_des) OT
    // es true si no tiene oc y si la agencia asociada destino de la OT es diferente a la agencia asocida de destino de la OC
    // es false si la agencia asociada destino de la OT es igual a la agencia asocida de destino de la OC
    public boolean verificarOC(Vector vec, String ot, int pos, String desAgOT) throws Exception{
        boolean estado=false;
        String desAgOC="";
        int cont=0;
        ReqFrontera reqfron;
        int i=0;
        for (int j=pos; j < vec.size(); j++ ){
            reqfron = (ReqFrontera) vec.elementAt(j);
            if ( reqfron.getnumrem().equalsIgnoreCase(ot) ){
                cont++;
                desAgOC = model.reqclienteServices.Buscar_agasocXcod( reqfron.getdespla() );
                if ( ! desAgOC.equalsIgnoreCase(desAgOT) ){
                    estado = true;
                }
            }
            
        }
        if ( cont == 0 )
            estado = true;
        
        return estado;
    }
    
    public void ElimReqCliVen(Vector vec) throws Exception {
        for ( int i=0; i<vec.size(); i++ ){
            pto_venta = (Pto_ventas) vec.elementAt(i);
            model.reqclienteServices.EliminarReq_venAct(pto_venta.getano()+"-"+pto_venta.getmes()+"-"+pto_venta.getdia()+" 08:00:00", pto_venta.getstd_job_no(), dstrct);
        }
    }
    public boolean verificarPrioridad(int [] Vec, int cant ){
        int i;
        int a [] = new int[5];
        a[0] = 0;
        a[1] = 0;
        a[2] = 0;
        a[3] = 0;
        a[4] = 0;
        boolean sw = true;
        for (i=0;i<5;i++){
            if( Vec[i] >0 ){
                if(Vec[i]<=cant){
                    if (a[Vec[i]]==0){
                        a[Vec[i]] = Vec[i];
                        sw = true;
                    }
                    else{
                        sw = false;
                        break;
                    }
                }
                else{
                    sw = false;
                    break;
                }
            }
            else{
                if(i == 0 && cant > 0)
                    sw = false;
                break;
            }
        }
        return sw;
    }
    
    public ReqCliente Reasignar(ReqCliente reqcli, String [] trec , String val){
        int con=1,i;
        for (i=0; i<5 ; i++){
            if(trec[i].equalsIgnoreCase(val)){
                if(i==0){
                    reqcli.setprioridad1(""+con);
                    reqcli.setObservacion(reqcli.getObservacion()+" Prioridad "+(i+1) );
                }
                else if (i==1){
                    reqcli.setprioridad2(""+con);
                    reqcli.setObservacion(reqcli.getObservacion()+" Prioridad "+(i+1) );
                }
                else if (i==2){
                    reqcli.setprioridad3(""+con);
                    reqcli.setObservacion(reqcli.getObservacion()+" Prioridad "+(i+1) );
                }
                else if (i==3){
                    reqcli.setprioridad4(""+con);
                    reqcli.setObservacion(reqcli.getObservacion()+" Prioridad "+(i+1) );
                }
                else {
                    reqcli.setprioridad5(""+con);
                    reqcli.setObservacion(reqcli.getObservacion()+" Prioridad "+(i+1) );
                }
                con++;
            }
        }
        return reqcli;
    }
    
    public ReqCliente FiltroReq(ReqCliente reqcli) throws Exception{
        String [] trec = new String [5];
        String [] rec = new String [5];
        int vprio [] = new int[5];
        int T [] = new int[5];
        int C [] = new int[5];
        int O [] = new int[5];
        int i;
        int cT=0,cC=0,cO=0;
        
        String fecpla="",fecdispo="";
        Calendar fpla = Calendar.getInstance();
        Calendar fdis = Calendar.getInstance();
        
        trec [0] = reqcli.getTipo_recurso1();
        trec [1] = reqcli.getTipo_recurso2();
        trec [2] = reqcli.getTipo_recurso3();
        trec [3] = reqcli.getTipo_recurso4();
        trec [4] = reqcli.getTipo_recurso5();
        
        rec[0] = reqcli.getrecurso1();
        rec[1] = reqcli.getrecurso2();
        rec[2] = reqcli.getrecurso3();
        rec[3] = reqcli.getrecurso4();
        rec[4] = reqcli.getrecurso5();
        
        vprio[0]= Integer.parseInt(reqcli.getprioridad1());
        vprio[1]= Integer.parseInt(reqcli.getprioridad2());
        vprio[2]= Integer.parseInt(reqcli.getprioridad3());
        vprio[3]= Integer.parseInt(reqcli.getprioridad4());
        vprio[4]= Integer.parseInt(reqcli.getprioridad5());
        
        T[0] = 0;
        T[1] = 0;
        T[2] = 0;
        T[3] = 0;
        T[4] = 0;
        C[0] = 0;
        C[1] = 0;
        C[2] = 0;
        C[3] = 0;
        C[4] = 0;
        O[0] = 0;
        O[1] = 0;
        O[2] = 0;
        O[3] = 0;
        O[4] = 0;
        for (i=0;i<5;i++){
            if( trec[i].equalsIgnoreCase("T") ){
                T[cT]= vprio[i];
                cT++;
            }
            else if(trec[i].equalsIgnoreCase("C")){
                C[cC]= vprio[i];
                cC++;
            }
            else if(trec[i].equalsIgnoreCase("O")){
                O[cO]=vprio[i];
                cO++;
            }
        }
        if ( !verificarPrioridad(T,cT) ){
            reqcli = Reasignar(reqcli, trec , "T");
        }
        if( !verificarPrioridad(C,cC) ){
            reqcli = Reasignar(reqcli, trec , "C");
        }
        if( !verificarPrioridad(O,cO) ){
            reqcli = Reasignar(reqcli, trec , "O");
        }
        
        for (i=0; i<5;i++){
            if ( trec[i].trim().equals("")  && i==0 ){
                if (reqcli.getError().equals(""))
                    reqcli.setError("Tipo Recurso "+(i+1)+" en Blanco");
            }
            else{
                if ( !trec[i].trim().equals("") ){
                    if (!rec[i].trim().equals("")){
                        if (reqcli.getError().equals(""))
                            reqcli.setError("Ok");
                    }
                    else{
                        reqcli.setError(reqcli.getError()+" Recurso"+(i+1)+" en Blanco");
                    }
                }
                else{
                    if (reqcli.getError().equals(""))
                        reqcli.setError("Ok");
                }
            }
            if ( trec[i].equalsIgnoreCase(" ") && rec[i].equalsIgnoreCase(" ")){
                if (reqcli.getError().equals(""))
                    reqcli.setError("Ok");
            }
            
        }
        
        if (reqcli.getclase_req().equalsIgnoreCase("FS")){
            fecpla=reqcli.getFecpla();
            fecdispo = reqcli.getfecha_dispo();
            fpla.set(Integer.parseInt(fecpla.substring(0,4)),Integer.parseInt(fecpla.substring(5,7)),Integer.parseInt(fecpla.substring(8,10)));
            fdis.set(Integer.parseInt(fecdispo.substring(0,4)),Integer.parseInt(fecdispo.substring(5,7)),Integer.parseInt(fecdispo.substring(8,10)));
            if ( fpla.getTime().after(fdis.getTime()) ){
                reqcli.setError("Fecha Disponible Incorrecta");
            }
        }
        return reqcli;
    }
    
    public void escribirxls(Vector vec,String fecha, String clase, String u) throws Exception{
        String nombreArch= "ReqCliente"+clase+"["+fecha+"].xls";
        String       Hoja  = "Error";
        HSSFWorkbook wb    = new HSSFWorkbook();
        HSSFSheet    sheet = wb.createSheet(Hoja);
        HSSFRow      row   = null;
        HSSFCell     cell  = null;
        
        //sandrameg190905
        //obtenmos la cabecera de la ruta para generar log
        ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
        String path = rb.getString("ruta");
        
        String Ruta  = path + "/exportar/migracion/" + u + "/";
        File file = new File(Ruta);
        file.mkdirs();
        Ruta  = Ruta + nombreArch;
        //encabezado
        HSSFFont  fuente = wb.createFont();
        fuente.setFontName("verdana");
        fuente.setFontHeightInPoints((short)(11)) ;
        fuente.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        fuente.setColor((short)(0x1));
        
        HSSFCellStyle estilo = wb.createCellStyle();
        estilo.setFont(fuente);
        estilo.setFillForegroundColor(HSSFColor.BLUE.index);
        estilo.setFillPattern((short)(estilo.SOLID_FOREGROUND));
        
        sheet.createFreezePane(0,1);
        
        row  = sheet.createRow((short)(0));
        
        for (int j=0;j<7;j++) {
            cell = row.createCell((short)(j));
        }
        cell = row.createCell((short)(0));
        cell.setCellStyle(estilo);
        cell.setCellValue("ERROR");
        
        cell = row.createCell((short)(1));
        cell.setCellStyle(estilo);
        cell.setCellValue("OBSERVACIÓN");
        
        cell = row.createCell((short)(2));
        cell.setCellStyle(estilo);
        cell.setCellValue("DSTRCT");
        
        cell = row.createCell((short)(3));
        cell.setCellStyle(estilo);
        cell.setCellValue("NUMSEC");
        
        cell = row.createCell((short)(4));
        cell.setCellStyle(estilo);
        cell.setCellValue("STDJOB");
        
        cell = row.createCell((short)(5));
        cell.setCellStyle(estilo);
        cell.setCellValue("NUMPLA");
        
        cell = row.createCell((short)(6));
        cell.setCellStyle(estilo);
        cell.setCellValue("FEC_DISPONIBLE");
        
        cell = row.createCell((short)(7));
        cell.setCellStyle(estilo);
        cell.setCellValue("CLIENTE");
        
        cell = row.createCell((short)(8));
        cell.setCellStyle(estilo);
        cell.setCellValue("ORIGEN");
        
        cell = row.createCell((short)(9));
        cell.setCellStyle(estilo);
        cell.setCellValue("DESTINO");
        
        cell = row.createCell((short)(10));
        cell.setCellStyle(estilo);
        cell.setCellValue("CLASE REQ");
        
        cell = row.createCell((short)(11));
        cell.setCellStyle(estilo);
        cell.setCellValue("TIPO REC1");
        
        cell = row.createCell((short)(12));
        cell.setCellStyle(estilo);
        cell.setCellValue("TIPO REC2");
        
        cell = row.createCell((short)(13));
        cell.setCellStyle(estilo);
        cell.setCellValue("TIPO REC3");
        
        cell = row.createCell((short)(14));
        cell.setCellStyle(estilo);
        cell.setCellValue("TIPO REC4");
        
        cell = row.createCell((short)(15));
        cell.setCellStyle(estilo);
        cell.setCellValue("TIPO REC5");
        
        cell = row.createCell((short)(16));
        cell.setCellStyle(estilo);
        cell.setCellValue("RECURSO1");
        
        cell = row.createCell((short)(17));
        cell.setCellStyle(estilo);
        cell.setCellValue("RECURSO2");
        
        cell = row.createCell((short)(18));
        cell.setCellStyle(estilo);
        cell.setCellValue("RECURSO3");
        
        cell = row.createCell((short)(19));
        cell.setCellStyle(estilo);
        cell.setCellValue("RECURSO4");
        
        cell = row.createCell((short)(20));
        cell.setCellStyle(estilo);
        cell.setCellValue("RECURSO5");
        
        cell = row.createCell((short)(21));
        cell.setCellStyle(estilo);
        cell.setCellValue("PRIORIDAD1");
        
        cell = row.createCell((short)(22));
        cell.setCellStyle(estilo);
        cell.setCellValue("PRIORIDAD2");
        
        cell = row.createCell((short)(23));
        cell.setCellStyle(estilo);
        cell.setCellValue("PRIORIDAD3");
        
        cell = row.createCell((short)(24));
        cell.setCellStyle(estilo);
        cell.setCellValue("PRIORIDAD4");
        
        cell = row.createCell((short)(25));
        cell.setCellStyle(estilo);
        cell.setCellValue("PRIORIDAD5");
        
        cell = row.createCell((short)(26));
        cell.setCellStyle(estilo);
        cell.setCellValue("USUARIO CREACION");
        
        cell = row.createCell((short)(27));
        cell.setCellStyle(estilo);
        cell.setCellValue("FECHA CREACION");
        
        
        
        for (int i=0; i < vec.size(); i++ ){
            reqcliente = (ReqCliente) vec.elementAt(i);
            row  = sheet.createRow((short)(i+1));
            
            cell = row.createCell((short)(0));
            cell.setCellValue(reqcliente.getError());
            
            cell = row.createCell((short)(1));
            cell.setCellValue(reqcliente.getObservacion());
            
            cell = row.createCell((short)(2));
            cell.setCellValue(reqcliente.getdstrct_code());
            
            cell = row.createCell((short)(3));
            cell.setCellValue(reqcliente.getnum_sec());
            
            cell = row.createCell((short)(4));
            if( clase.equals("Frontera") ){
                cell.setCellValue(reqcliente.getStdjob_esp());
            }else{
                cell.setCellValue(reqcliente.getstd_job_no());
            }
            
            cell = row.createCell((short)(5));
            cell.setCellValue(reqcliente.getnumpla());
            
            cell = row.createCell((short)(6));
            cell.setCellValue(reqcliente.getfecha_dispo());
            
            cell = row.createCell((short)(7));
            cell.setCellValue(reqcliente.getcliente());
            
            cell = row.createCell((short)(8));
            cell.setCellValue(reqcliente.getorigen());
            
            cell = row.createCell((short)(9));
            cell.setCellValue(reqcliente.getdestino());
            
            cell = row.createCell((short)(10));
            cell.setCellValue(reqcliente.getclase_req());
            
            cell = row.createCell((short)(11));
            cell.setCellValue(reqcliente.getTipo_recurso1());
            
            cell = row.createCell((short)(12));
            cell.setCellValue(reqcliente.getTipo_recurso2());
            
            cell = row.createCell((short)(13));
            cell.setCellValue(reqcliente.getTipo_recurso3());
            
            cell = row.createCell((short)(14));
            cell.setCellValue(reqcliente.getTipo_recurso4());
            
            cell = row.createCell((short)(15));
            cell.setCellValue(reqcliente.getTipo_recurso5());
            
            cell = row.createCell((short)(16));
            cell.setCellValue(reqcliente.getrecurso1());
            
            cell = row.createCell((short)(17));
            cell.setCellValue(reqcliente.getrecurso2());
            
            cell = row.createCell((short)(18));
            cell.setCellValue(reqcliente.getrecurso3());
            
            cell = row.createCell((short)(19));
            cell.setCellValue(reqcliente.getrecurso4());
            
            cell = row.createCell((short)(20));
            cell.setCellValue(reqcliente.getrecurso5());
            
            cell = row.createCell((short)(21));
            cell.setCellValue(reqcliente.getprioridad1());
            
            cell = row.createCell((short)(22));
            cell.setCellValue(reqcliente.getprioridad2());
            
            cell = row.createCell((short)(23));
            cell.setCellValue(reqcliente.getprioridad3());
            
            cell = row.createCell((short)(24));
            cell.setCellValue(reqcliente.getprioridad4());
            
            cell = row.createCell((short)(25));
            cell.setCellValue(reqcliente.getprioridad5());
            
            cell = row.createCell((short)(26));
            cell.setCellValue(reqcliente.getUsuario_creacion());
            
            cell = row.createCell((short)(27));
            cell.setCellValue(reqcliente.getfecha_creacion());
            
        }
        
        FileOutputStream fo = new FileOutputStream(Ruta);
        wb.write(fo);
        fo.close();
        
        
    }
    public void ReqclienteVen(Vector vec, String fecF_pro)throws Exception{
        ReqCliente reqclien;
        int sw=0;
        for ( int i=0; i< vec.size(); i++ ){
            pto_venta = (Pto_ventas) vec.elementAt(i);
            //busco el orige y destino en la tabla stdjob
            stdjob = model.stdjobServices.BuscarStdjob(pto_venta.getdstrct_code(), pto_venta.getstd_job_no() );
            if (stdjob.getorigin_code() != null ){
                for(int j=0; j < pto_venta.getviaje(); j++ ){
                    info++;
                    //creo un objeto  req_cliente
                    reqcliente = new ReqCliente();
                    reqcliente.setorigen(stdjob.getorigin_code());
                    reqcliente.setdestino(stdjob.getdestination_code());
                    reqcliente.setnum_sec(j+1);
                    reqcliente.setcliente(pto_venta.getcliente());
                    reqcliente.setstd_job_no(pto_venta.getstd_job_no());
                    reqcliente.setfecha_dispo(pto_venta.getano()+"-"+pto_venta.getmes()+"-"+pto_venta.getdia()+" "+"08:00:00");
                    reqcliente.setTipo_recurso1(stdjob.getTipo_recurso1());
                    reqcliente.setTipo_recurso2(stdjob.getTipo_recurso2());
                    reqcliente.setTipo_recurso3(stdjob.getTipo_recurso3());
                    reqcliente.setTipo_recurso4(stdjob.getTipo_recurso4());
                    reqcliente.setTipo_recurso5(stdjob.getTipo_recurso5());
                    
                    if (stdjob.getrecurso1().length() > 1){
                        reqcliente.setrecurso1(stdjob.getrecurso1().substring(1,stdjob.getrecurso1().length()) ); //stdjob.getrecurso1()
                    }
                    else{
                        reqcliente.setrecurso1(stdjob.getrecurso1() ); //stdjob.getrecurso1()
                    }
                    
                    if (stdjob.getrecurso2().length() > 1){
                        reqcliente.setrecurso2(stdjob.getrecurso2().substring(1,stdjob.getrecurso2().length()) );
                    }
                    else{
                        reqcliente.setrecurso2(stdjob.getrecurso2() );
                    }
                    
                    if (stdjob.getrecurso3().length() > 1){
                        reqcliente.setrecurso3(stdjob.getrecurso3().substring(1,stdjob.getrecurso3().length()) );
                    }
                    else{
                        reqcliente.setrecurso3(stdjob.getrecurso3() );
                    }
                    
                    if (stdjob.getrecurso4().length() > 1){
                        reqcliente.setrecurso4(stdjob.getrecurso4().substring(1,stdjob.getrecurso4().length()) );
                    }
                    else{
                        reqcliente.setrecurso4(stdjob.getrecurso4());
                    }
                    
                    if (stdjob.getrecurso5().length() > 1){
                        reqcliente.setrecurso5(stdjob.getrecurso5().substring(1,stdjob.getrecurso5().length()) );
                    }
                    else{
                        reqcliente.setrecurso5(stdjob.getrecurso5());
                    }
                    
                    if (stdjob.getprioridad1()!= null && stdjob.getprioridad1().indexOf(" ") == -1 ){
                        reqcliente.setprioridad1(stdjob.getprioridad1()); //stdjob.getprioridad1()
                    }
                    else{
                        reqcliente.setprioridad1("0");
                    }
                    if (stdjob.getprioridad2()!= null && stdjob.getprioridad2().indexOf(" ") == -1){
                        reqcliente.setprioridad2(stdjob.getprioridad2());
                    }
                    else{
                        reqcliente.setprioridad2("0");
                    }
                    if(stdjob.getprioridad3() !=null && stdjob.getprioridad3().indexOf(" ") == -1){
                        reqcliente.setprioridad3(stdjob.getprioridad3());
                    }
                    else{
                        reqcliente.setprioridad3("0");
                    }
                    if(stdjob.getprioridad4() !=null && stdjob.getprioridad4().indexOf(" ") == -1){
                        reqcliente.setprioridad4(stdjob.getprioridad4());
                    }
                    else{
                        reqcliente.setprioridad4("0");
                    }
                    if (stdjob.getprioridad5()!=null && stdjob.getprioridad5().indexOf(" ") == -1){
                        reqcliente.setprioridad5(stdjob.getprioridad5());
                    }
                    else{
                        reqcliente.setprioridad5("0");
                    }
                    reqcliente.setclase_req("VE");
                    reqcliente.setError("");
                    reqcliente.setObservacion("");
                    reqcliente.setnumpla("");
                    reqcliente.setId_rec_tra("");
                    reqcliente.setestado(pto_venta.getestado());
                    reqcliente.setUsuario_creacion(Usuario);
                    reqcliente.setUsuario_actualizacion(Usuario);
                    reqcliente.setdstrct_code(pto_venta.getdstrct_code());
                    reqcliente.setfecha_creacion(fecF_pro);
                    reqcliente.setfecha_actualizacion(fecF_pro);
                    if ( !reqcliente.getestado().equalsIgnoreCase("A") ){
                        reqclien = FiltroReq(reqcliente);
                        if( reqclien.getError().equalsIgnoreCase("Ok") ){
                            try{
                                model.reqclienteServices.llenarReqcliente(reqclien);
                            }
                            catch (SQLException e){
                                sw=1;
                            }
                            if (sw==1){
                                model.reqclienteServices.ModificarReqcliente(reqclien);
                            }
                        }
                        else{
                            vreq.add(reqclien);
                            a++;
                        }
                        if (!reqclien.getObservacion().equalsIgnoreCase("")){
                            vreq.add(reqclien);
                        }
                    }
                }
            }
            else{
                reqcliente = new ReqCliente();
                reqcliente.setorigen("");
                reqcliente.setdestino("");
                reqcliente.setnum_sec(0);
                reqcliente.setcliente(pto_venta.getcliente());
                reqcliente.setstd_job_no(pto_venta.getstd_job_no());
                reqcliente.setfecha_dispo(pto_venta.getano()+"-"+pto_venta.getmes()+"-"+pto_venta.getdia()+" "+"08:00:00");
                reqcliente.setclase_req("VE");
                reqcliente.setTipo_recurso1("");
                reqcliente.setTipo_recurso2("");
                reqcliente.setTipo_recurso3("");
                reqcliente.setTipo_recurso4("");
                reqcliente.setTipo_recurso5("");
                reqcliente.setrecurso1("");
                reqcliente.setrecurso2("");
                reqcliente.setrecurso3("");
                reqcliente.setrecurso4("");
                reqcliente.setrecurso5("");
                reqcliente.setprioridad1("");
                reqcliente.setprioridad2("");
                reqcliente.setprioridad3("");
                reqcliente.setprioridad4("");
                reqcliente.setprioridad5("");
                reqcliente.setnumpla("");
                reqcliente.setestado(pto_venta.getestado());
                reqcliente.setUsuario_creacion(Usuario);
                reqcliente.setUsuario_actualizacion(Usuario);
                reqcliente.setdstrct_code(pto_venta.getdstrct_code());
                reqcliente.setfecha_creacion(fecF_pro);
                reqcliente.setfecha_actualizacion(fecF_pro);
                reqcliente.setError("No InfoStdjob");
                vreq.add(reqcliente);
                NoInfo=NoInfo+ pto_venta.getviaje();
            }
        }
        
    }
    
    public void ReqclienteFron(Vector vec, String fecF_pro) throws Exception {
        String nfec=" ";
        String tiempo="";
        int sw=0;
        ReqCliente reqclien;
        
        
        for (int i=0; i < vec.size(); i++ ){
            reqfrontera = (ReqFrontera) vec.elementAt(i);
            reqcliente = new ReqCliente();
            reqcliente.setorigen( reqfrontera.getdespla() );
            reqcliente.setdestino( reqfrontera.getdesrem() );
            reqcliente.setcliente( reqfrontera.getcliente() );
            reqcliente.setstd_job_no( reqfrontera.getstd_job_no() );
            reqcliente.setnumpla( reqfrontera.getnumpla());
            reqcliente.setfecha_dispo(reqfrontera.getfecdispo());
            reqcliente.setFecpla(reqfrontera.getfecpla());
            reqcliente.setTipo_recurso1(reqfrontera.getTipo_recurso1());
            reqcliente.setTipo_recurso2(reqfrontera.getTipo_recurso2());
            reqcliente.setTipo_recurso3(reqfrontera.getTipo_recurso3());
            reqcliente.setTipo_recurso4(reqfrontera.getTipo_recurso4());
            reqcliente.setTipo_recurso5(reqfrontera.getTipo_recurso5());
            
            if (reqfrontera.getrecurso1().length() > 1){
                reqcliente.setrecurso1(reqfrontera.getrecurso1().substring(1,reqfrontera.getrecurso1().length()) ); //stdjob.getrecurso1()
            }
            else{
                reqcliente.setrecurso1(reqfrontera.getrecurso1() );
            }
            
            if (reqfrontera.getrecurso2().length() > 1){
                reqcliente.setrecurso2(reqfrontera.getrecurso2().substring(1,reqfrontera.getrecurso2().length()) );
            }
            else{
                reqcliente.setrecurso2(reqfrontera.getrecurso2() );
            }
            
            if (reqfrontera.getrecurso3().length() > 1){
                reqcliente.setrecurso3(reqfrontera.getrecurso3().substring(1,reqfrontera.getrecurso3().length()) );
            }
            else{
                reqcliente.setrecurso3(reqfrontera.getrecurso3() );
            }
            
            if (reqfrontera.getrecurso4().length() > 1){
                reqcliente.setrecurso4(reqfrontera.getrecurso4().substring(1,reqfrontera.getrecurso4().length()) );
            }
            else{
                reqcliente.setrecurso4(reqfrontera.getrecurso4());
            }
            
            if (reqfrontera.getrecurso5().length() > 1){
                reqcliente.setrecurso5(reqfrontera.getrecurso5().substring(1,reqfrontera.getrecurso5().length()) );
            }
            else{
                reqcliente.setrecurso5(reqfrontera.getrecurso5());
            }
            if (reqfrontera.getprioridad1()!= null && reqfrontera.getprioridad1().indexOf(" ") == -1 ){
                reqcliente.setprioridad1(reqfrontera.getprioridad1()); //stdjob.getprioridad1()
            }else{
                reqcliente.setprioridad1("0");
            }
            if (reqfrontera.getprioridad2()!= null && reqfrontera.getprioridad2().indexOf(" ") == -1){
                reqcliente.setprioridad2(reqfrontera.getprioridad2());
            }
            else{
                reqcliente.setprioridad2("0");
            }
            if(reqfrontera.getprioridad3() !=null && reqfrontera.getprioridad3().indexOf(" ") == -1){
                reqcliente.setprioridad3(reqfrontera.getprioridad3());
            }
            else{
                reqcliente.setprioridad3("0");
            }
            if(reqfrontera.getprioridad4() !=null && reqfrontera.getprioridad4().indexOf(" ") == -1){
                reqcliente.setprioridad4(reqfrontera.getprioridad4());
            }
            else{
                reqcliente.setprioridad4("0");
            }
            if (reqfrontera.getprioridad5()!=null && reqfrontera.getprioridad5().indexOf(" ") == -1){
                reqcliente.setprioridad5(reqfrontera.getprioridad5());
            }
            else{
                reqcliente.setprioridad5("0");
            }
            
            reqcliente.setclase_req("FS");
            
            reqcliente.setStdjob_esp( reqfrontera.getStdjob_esp() ); 

            reqcliente.setError(reqfrontera.getError());
            reqcliente.setId_rec_tra(reqfrontera.getId_rec_tra());
            reqcliente.setObservacion("");
            reqcliente.setestado("");
            reqcliente.setnum_sec(0);
            reqcliente.setdstrct_code(dstrct);
            reqcliente.setUsuario_creacion(Usuario);
            reqcliente.setUsuario_actualizacion(Usuario);
            reqcliente.setfecha_creacion(fecF_pro);
            reqcliente.setfecha_actualizacion(fecF_pro);
            
            reqclien = FiltroReq(reqcliente);
            if( reqclien.getError().equalsIgnoreCase("Ok") ){
                try{
                    model.reqclienteServices.llenarReqcliente(reqclien);
                }
                catch (SQLException e){
                    sw=1;
                }
                if (sw==1){
                    model.reqclienteServices.ModificarReqcliente(reqclien);
                }
                //Creacion de Recurso
                if(!reqcliente.getId_rec_tra().equals("")){
                    crearRecursoFrontera(reqfrontera,fecF_pro);
                }
            }
            else{
                vreqF.add(reqclien);
            }
            if (!reqclien.getObservacion().equalsIgnoreCase("")&& reqclien.getError().equalsIgnoreCase("Ok")){
                vreqF.add(reqclien);
            }
            /*    }
            }*/
        }
    }
    
    /*
     */
    public void RegistrarInconsistencia( int Tipo, String np, String reg, String pla ){
        Inconsistencia inc = new Inconsistencia();
        String ITramo = "EL TRAMO NO EXISTE EN EL ARCHIVO TRAMO";
        String IFechaposllegada = "LA FECHA DE POSIBLE LLEGADA NO ES VALIDA";
        String ITiempo = "EL TIEMPO EN EL ARCHIVO VIA NO ES VALIDO";
        String IPlaca = "LA PLACA DEL VEHICULO NO EXISTE EN EL ARCHIVO PLACA";
        String INovalida = "EL RECURSO NO ES RECURSO DISPONIBLE";
        String TIPOREC = "EL TIPO RECURSO NO SE ENCONTRO";
        String Descripcion ="";
        String Columna ="";
        String Archivo = "";
        if( Tipo == 1 ){
            Descripcion = ITramo;
            Columna = "Origen - Destino";
            Archivo = "Via";
        }
        else if( Tipo == 2){
            Descripcion = IFechaposllegada;
            Columna = "Fechaposllegada";
            Archivo = "Planilla";
        }
        else if( Tipo == 3){
            Descripcion = ITiempo;
            Columna = "Tiempo";
            Archivo = "Via";
        }
        else if( Tipo == 4){
            Descripcion = IPlaca;
            Columna = "Placa";
            Archivo = "Placa";
        }
        else if( Tipo == 5){
            Descripcion = INovalida;
            Columna = "Numpla";
            Archivo = "Planilla";
        }
        else if( Tipo == 6){
            Descripcion = TIPOREC;
            Columna = "Numpla";
            Archivo = "Placa";
        }
        
        inc.load(Tipo, Descripcion, np, reg, Columna, Archivo, pla );
        
        this.ListaIncosistencias.add(inc);
        
        
    }
    
    
    
    //elimina la OC que esta ren reqcliente si esiste otra OC relacionada con la misma OT y si su agencia aso de destino son iguales (OC-OT)
    public void Elim_ReqCliFronOC(Vector vec ,Vector vecreq ) throws Exception {
        for (int i=0; i < vecreq.size(); i++ ){
            reqfrontera = (ReqFrontera) vecreq.elementAt(i);
            String oriAgOT = model.reqclienteServices.Buscar_agasocXnom(reqfrontera.getorirem());
            String desAgOT = model.reqclienteServices.Buscar_agasocXnom(reqfrontera.getdesrem());
            if ( ! verificarOC(vec, reqfrontera.getnumrem(), 0,desAgOT ) ){
                model.reqclienteServices.EliminarReq_fronAct(reqfrontera.getnumpla(), reqfrontera.getstd_job_no() );
            }
            
        }
    }
    
    //elimina las oc actualizas
    public void ElimReqCliFron(Vector vec) throws Exception {
        for (int i=0; i < vec.size(); i++ ){
            reqfrontera = (ReqFrontera) vec.elementAt(i);
            model.reqclienteServices.EliminarReq_fronAct(reqfrontera.getnumpla(), reqfrontera.getstd_job_no() );
        }
    }
    
    
    
    public void CrearLog(String usuario) throws Exception{
        try{
            LogExcelRDFrontera hilo = new LogExcelRDFrontera();
            hilo.star(ListaIncosistencias,usuario );
        } catch(Exception e){ 
            e.printStackTrace();
            ////System.out.println("ERROR: Creacion LOGEXCEL"+e.getMessage());
        }
    }
    
    
    /**
     * @param args the command line arguments     */
    public void principalReqCliente( ) throws  java.io.IOException,java.sql.SQLException,Exception {
        
        SimpleDateFormat s=null;
        
        LlenarReqCliente cs = new LlenarReqCliente(Usuario, fi, ff, distrito, tipo);
        Vector vec = new Vector();
        Vector vecreq = new Vector();
        stdjob = new StdJob();
        ReqFrontera reqfron;
        //fechas de PTO_Venta
        String Fpto_ini="";
        String Fpto_fin="";
        String FI_procpto="";
        String FF_procpto="";
        //fechas req frontera
        
        String Ffron_fin="";
        String FI_procfron="";
        String FF_procfron="";
        
        s = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        hoy = s.format(new java.util.Date());
        
        /////sandrameg
        // cargo las fechas
        cs.cargarPropiedades();
        
        
        dstrct = distrito;
        
        //cargo todas las fechas pto
        Fpto_ini = dbProps.getProperty("Fpto_ini");
        Fpto_fin = dbProps.getProperty("Fpto_fin");
        FI_procpto = dbProps.getProperty("FI_procpto");
        FF_procpto = dbProps.getProperty("FF_procpto");
        //fecha fron
        
        Ffron_fin = dbProps.getProperty("Ffron_fin");;
        FI_procfron = dbProps.getProperty("FF_procfron");
        FF_procfron = dbProps.getProperty("FI_procfron");;
        
        String ano=hoy.substring(0,4);
        String mes=hoy.substring(5,7);
        int dia= Integer.parseInt(hoy.substring(8,10));
        
        
        try{
            if (tipo.equals("nuevo_pto")){
                
                Fpto_ini = fi;
                Fpto_fin = ff;
                FI_procpto = dbProps.getProperty("FF_procpto");
                FF_procpto = hoy;
                
                
                String ano1=Fpto_ini.substring(0,4);
                String mes1=Fpto_ini.substring(5,7);
                int dia1= Integer.parseInt(Fpto_ini.substring(8,10));
                String ano2=Fpto_fin.substring(0,4);
                String mes2=Fpto_fin.substring(5,7);
                int dia2= Integer.parseInt(Fpto_fin.substring(8,10));
                
                vec = model.reqclienteServices.listarPto_Venta( ano1,mes1,dia1,ano2,mes2,dia2,dstrct );
                
                model.reqclienteServices.EliminarReq_venN(Fpto_ini, Fpto_fin);
                cs.ReqclienteVen(vec,FF_procpto);
                if (vreq.size()>0){
                    cs.escribirxls(vreq,ano+"-"+mes+"-"+dia,"Venta",Usuario);
                }
            }
            
            else if (tipo.equals("act_pto")){
                
                
                
                Fpto_ini = Fpto_fin;
                Fpto_fin = ano+"-"+mes+"-"+hoy.substring(8,10);
                FI_procpto = dbProps.getProperty("FF_procpto");
                FF_procpto = hoy;
                
                String ano1=Fpto_ini.substring(0,4);
                String mes1=Fpto_ini.substring(5,7);
                int dia1= Integer.parseInt(Fpto_ini.substring(8,10));
                
                vec = model.reqclienteServices.listarPto_VentaAct(dstrct,FI_procpto,FF_procpto);
                
                cs.ElimReqCliVen(vec);
                
                cs.ReqclienteVen(vec, FF_procpto);
                
                
                vec = model.reqclienteServices.listarPto_VentaA( ano1,mes1,dia1,ano,mes,dia,dstrct,FI_procpto );
                //elimino
                //cs.ElimReqCliVen(vec);
                //inserto
                cs.ReqclienteVen(vec,FF_procpto);
                if (vreq.size()>0){
                    cs.escribirxls(vreq,ano+"-"+mes+"-"+dia,"Venta",Usuario);
                }
            }
            
            /***********************************************************
             *            Requerimirnto por frontera                   */
            
            else if ( tipo.equals("nuevo_fron") ){
                
                Ffron_fin = ano+"-"+mes+"-"+hoy.substring(8,10);
                FI_procfron = dbProps.getProperty("FF_procfron");
                FF_procfron = hoy;
                
                vec = model.reqclienteServices.listarReqFrontera( dstrct );
                
                model.reqclienteServices.EliminarReq_fronN();
                
                cs.ReqclienteFron(vec, hoy);
                
                if (vreqF.size()>0){
                    
                    cs.escribirxls(vreqF,ano+"-"+mes+"-"+dia,"Frontera",Usuario);
                    cs.CrearLog(Usuario);
                }
                
            }
            
            else if ( tipo.equals("act_fron") ){
                
                
                
                Ffron_fin = ano+"-"+mes+"-"+hoy.substring(8,10);
                FI_procfron = dbProps.getProperty("FF_procfron");
                FF_procfron = hoy;
                
                // obtengo los requerimientos por fontera modificados
                vec = model.reqclienteServices.listarReqFronAct( FI_procfron, FF_procfron,  dstrct);
                // ////System.out.println("Modificadas "+vec.size());
                //los modificados los  elimino de req_cliente
                cs.ElimReqCliFron(vec);
                //////System.out.println("Inserto las modificadas");
                //inserto los modificados
                cs.ReqclienteFron(vec,hoy);
                
                
                //////System.out.println("obtengo las nuevas");
                // obtengo los requerimiento por frontera creados
                vec = model.reqclienteServices.listarReqFrontera( dstrct );
                //////System.out.println("obtengo los reqcliente registrados ");
                cs.ElimReqCliFron(vec);
                //////System.out.println( "Inserto los nuevos" );
                // inserto los creados
                cs.ReqclienteFron(vec,hoy);
                
                if (vreqF.size()>0){
                    cs.escribirxls(vreqF,ano+"-"+mes+"-"+dia,"Frontera",Usuario);
                }
            }
            
            
            // modifico la fecha del terminacion del proceso
            dbProps.setProperty("FI_procpto", FI_procpto );
            dbProps.setProperty("FF_procpto", FF_procpto);
            dbProps.setProperty("Fpto_ini", Fpto_ini);
            dbProps.setProperty("Fpto_fin", Fpto_fin);
            dbProps.setProperty("FI_procfron", FI_procfron );
            dbProps.setProperty("FF_procfron", FF_procfron);
            
            dbProps.setProperty("Ffron_fin", Ffron_fin);
            //dbProps.store(new FileOutputStream("C:/Tomcat5/webapps/slt2/WEB-INF/classes/com/tsp/operation/model/Tiempos.properties"),"");
            URL u = getClass().getResource("Tiempos.properties");
            dbProps.store(new FileOutputStream(u.getPath()),"");
            
            
        }catch ( Exception e ) {
            //throw new Exception("Error al accesar con la bd "+e.getMessage());
            e.printStackTrace();
        }
        ////System.out.println("Proceso Exitoso");
    }
    
    
    
    
}
