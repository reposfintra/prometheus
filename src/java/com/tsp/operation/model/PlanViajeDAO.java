/*
 * PlanViajeDAO.java
 *
 * Created on 19 de noviembre de 2004, 01:25 PM
 */
package com.tsp.operation.model;

import com.tsp.util.connectionpool.PoolManager;
import com.tsp.operation.model.beans.*;
import java.sql.*;
import java.util.*;

import com.tsp.operation.model.DAOS.MainDAO;
import com.tsp.exceptions.InformationException;
import com.tsp.operation.model.DAOS.PlanDeViajeDAO;



/**
 *
 * @author  AMENDEZ
 */
public class PlanViajeDAO extends MainDAO{
    private static final String BURCAR_PLAN_VIAJE =
    "SELECT planilla " +
      "FROM planviaje " +
     "WHERE planilla = ? " +
       "AND cia = ?"; 
    
    private static final String PLAN_VIAJE_INFO =
    "SELECT a.planilla, a.placa, a.producto, "+
        "a.trailer, a.contenedor, a.fecha, "+
        "a.destinatario, a.ruta, "+
        "a.cedcon, c.nombre, "+
        "c.direccion, c.telefono, c.codciu, "+
        "a.al1, a.at1, a.al2, a.at2, "+
        "a.al3, a.at3, a.pl1, a.pt1, "+
        "a.pl2, a.pt2, a.pl3, a.pt3, "+
        "a.radio, a.celular, a.avantel, a.telefono, "+
        "a.cazador, a.movil, a.otro, "+
        "a.qlo, a.qto, a.qld, a.qtd, "+
        "a.ql1, a.qt1, a.ql2, a.qt2, "+
        "a.ql3, a.qt3, "+
	"a.tl1, a.tt1, a.tl2, a.tt2, "+
        "a.tl3, a.tt3, "+
        "a.nomfam, a.phonefam, "+
        "a.nitpro, d.nombre, "+
        "d.direccion, d.telefono, d.codciu, "+
        "a.comentario1, a.comentario2, a.comentario3, " +
        "e.nombre, a.tipocarga, TO_CHAR(a.creation_date, 'YYYY-MM-DD HH:MI:SS a.m.'), a.retorno, b.plareal, a.cia "+
   "FROM planviaje a LEFT OUTER JOIN plamanual b ON (a.planilla = b.numpla), nit c, nit d, usuarios e " +
  "WHERE a.planilla = ? " +
    "AND a.cia = ? "+
    "AND a.usuario = e.idusuario "+
    "AND a.cedcon = c.cedula "+
    "AND a.nitpro = d.cedula";
  	
 
    
    private static final String BURCAR_PLANILLA = 
    "SELECT numpla FROM planilla WHERE numpla = ? AND SUBSTR(cia, 1, 3) = ?";
    
    private static final String CREAR_PLAN_VIAJE = 
    "INSERT INTO planviaje (planilla, placa, producto, trailer, contenedor, fecha, destinatario, ruta, cedcon, al1, at1, al2, at2, al3, at3, pl1, " +
                           "pt1, pl2, pt2, pl3, pt3, radio, celular, avantel, telefono, cazador, movil, otro, qlo, qto, qld, qtd, ql1, qt1, " +
                           "ql2, qt2, ql3, qt3, nomfam, phonefam, nitpro, comentario1, comentario2, comentario3, tl1, tt1, tl2, tt2, tl3, tt3, " +
                           "last_mod_user, last_mod_date, last_mod_time, usuario, leido, tipocarga, retorno, cia) " +
                   "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, " +
                           "?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, 'NO', ?, ?, ?)";
    
    private static final String MOD_PLAN_VIAJE =
    "UPDATE planviaje " +
    "SET placa = ?, producto = ?, trailer = ?, contenedor = ?, fecha = ?, destinatario = ?, ruta = ?, cedcon = ?, al1 = ?, at1 = ?, al2 = ?, at2 = ?, al3 = ?, at3 = ?, " +
    "pl1 = ?, pt1 = ?, pl2 = ?, pt2 = ?, pl3 = ?, pt3 = ?, radio = ?, celular = ?, avantel = ?, telefono = ?, cazador = ?, movil = ?, otro = ?, " +
    "qlo = ?, qto = ?, qld = ?, qtd = ?, ql1 = ?, qt1 = ?, ql2 = ?, qt2 = ?, ql3 = ?, qt3 = ?, nomfam = ?, phonefam = ?, nitpro = ?, comentario1 = ?, " +
    "comentario2 = ?, comentario3 = ?, tl1 = ?, tt1 = ?, tl2 = ?, tt2 = ?, tl3 = ?, tt3 = ?, last_mod_user = ?, last_mod_date = ?, last_mod_time = ?, retorno = ? " +
    "WHERE planilla = ? " +
      "AND cia = ? ";
                   
    private static final String CONSULTAR_PLAN_VIAJE_AGENCIA =
    "SELECT d.desczona, f.nomciu, (a.last_mod_date || ' ' || a.last_mod_time) as fecha, a.planilla, a.placa, b.descripcion, a.leido, a.cia "+
    "FROM planviaje a,  via b, ciudad c, ciudad f, zona d, usuarios e "+
    "WHERE a.last_mod_date >= ? "+
      "AND a.last_mod_date <= ? "+
      "AND a.ruta = b.origen || b.destino || b.secuencia "+
      "AND b.origen = c.codciu "+
      "AND c.zona = d.codzona "+
      "AND e.idusuario =  a.usuario "+
      "AND e.id_agencia like ? "+
      "AND e.id_agencia = f.codciu " +
      "ORDER BY fecha DESC";
    
    private static final String CONSULTAR_PLAN_VIAJE_ZONA = 
    "SELECT d.desczona, f.nomciu, (a.last_mod_date || ' ' || a.last_mod_time) as fecha, a.planilla, a.placa, b.descripcion, a.leido, a.cia "+
    "FROM planviaje a,  via b, ciudad c, ciudad f, zona d, usuarios e "+
    "WHERE a.last_mod_date >= ? "+
      "AND a.last_mod_date <= ? "+
      "AND a.ruta = b.origen || b.destino || b.secuencia "+
      "AND b.origen = c.codciu "+
      "AND c.zona like ? "+
      "AND c.zona = d.codzona "+
      "AND e.idusuario =  a.usuario "+
      "AND e.id_agencia = f.codciu "+ 
      "ORDER BY fecha DESC";
        
    /**
     *Query pars extraer informacion relacionada a una planilla
     *como la placa del cabezote, la cedula del conductor y la 
     *remesa relacionada a la planilla.
     */
    private static final String SQL_INF_PLANILLA =
    "SELECT a.cia, " +
           "a.numpla, "+
           "a.plaveh, "+
           "a.platlr, "+
           "a.oripla||a.despla, "+
           "a.cedcon, "+
           "a.nitpro, "+
           "a.contenedores, "+
           "c.destinatario, "+
           "c.codtipocarga, "+
           "pt.date_time_traffic as fecha  "+
      "FROM plarem b, remesa c, planilla a " +
      "     LEFT OUTER JOIN (   SELECT pla,date_time_traffic " +
      "                         FROM planilla_tiempo " +
      "                         WHERE time_code = 'SALI') as pt ON a.numpla = pt.pla  "+
     "WHERE a.numpla = ? "+
       "AND a.cia = ? "+
       "AND b.numpla = a.numpla "+
       "AND b.cia = a.cia "+
       "AND c.numrem = b.numrem";
    
    
    private static final String SQL_INFO_NIT =
    "SELECT a.cedula, " +
           "a.nombre, " + 
           "a.direccion, "+
           "a.telefono, "+
           "a.codciu "+
      "FROM nit a "+ 
     "WHERE a.cedula = ?";
   
    private static final String SQL_PROPIETARIO = 
    "SELECT b.propietario " +
      "FROM placa b "+
     "WHERE b.placa = ? ";
    
    //CONSULTAS PARA MANIPULAR AUDITORIA
    private static final String SQL_REG_USER_QUERY =
    "INSERT INTO auditoriapv (planviaje, usuario, last_qry_date, cia) VALUES (?, ?, now(), ?)";
    
    private static final String SQL_MOD_USER_QUERY =
    "UPDATE auditoriapv SET last_qry_date = now() WHERE planviaje = ? AND usuario = ? AND cia = ?";
    
    private static final String SQL_FIND_USER_QUERY =
    "SELECT * FROM auditoriapv WHERE planviaje = ? AND usuario = ? AND cia = ? ";
    
    //LISTA TODOS LOS USUARIOS QUE HAN LEIDO UN PLAN DE VIAJE
    private static final String SQL_USER_LIST =
    "SELECT a.idusuario, a.nombre "+ 
      "FROM usuarios a, auditoriapv b "+ 
     "WHERE b.planviaje = ?" +
       "AND b.usuario = a.idusuario " +
       "AND b.cia = ?";
    
    //ACTUALIZA EL ESTADO DE LEIDO DEL PLAN DE VIAJE
    private static final String SQL_LEIDO =
    "UPDATE planviaje SET leido = 'SI' WHERE planilla = ? AND cia = ?";
   
    //OBTIENE LA DESCRIPCION DEL TIPO DE CARGA
    private static final String SQL_DESC_TIPOCARGA = 
    "SELECT desctipocarga FROM tipocarga WHERE codtipocarga = ?";
    
    //QUERY PARA OBTENER LA ZONA DE UNA PLANILLA
    private static final String SQL_ZONA =
    "SELECT zona "+
      "FROM planilla, ciudad "+
     "WHERE numpla = ? "+
       "AND cia = ? "+
       "AND oripla = codciu";
    
    private PlanViaje planViaje;
    private List planVjQrys;
    //LISTADO DE USUARIOS QUE HAN CONSULTADO LOS PLANES DE VIAJE
    private TreeMap cbxQryUsers;
    
    /** Creates a new instance of PlanViajeDAO */
    public PlanViajeDAO() {
        super("PlanViajeDAO.xml");
    }
    
    public void createPlanVj(PlanViaje planViaje) throws Exception{
        Connection con = null;
        PreparedStatement psttm = null;
        PoolManager poolManager = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if ((con == null) || con.isClosed())
                throw new SQLException("NO HAY CONEXION A LA BASE DE DATOS");
            psttm = con.prepareStatement(CREAR_PLAN_VIAJE);
            psttm.setString(1, planViaje.getPlanilla());
            psttm.setString(2, planViaje.getPlaca());
            psttm.setString(3, planViaje.getProducto());
            psttm.setString(4, planViaje.getTrailer());
            psttm.setString(5, planViaje.getContenedor());
            psttm.setString(6, planViaje.getFecha());
            psttm.setString(7, planViaje.getDestinatario());
            psttm.setString(8, planViaje.getRuta());
            psttm.setString(9, planViaje.getCedcon());
            psttm.setString(10, planViaje.getAl1());
            psttm.setString(11, planViaje.getAt1());
            psttm.setString(12, planViaje.getAl2());
            psttm.setString(13, planViaje.getAt2());
            psttm.setString(14, planViaje.getAl3());
            psttm.setString(15, planViaje.getAt3());
            psttm.setString(16, planViaje.getPl1());
            psttm.setString(17, planViaje.getPt1());
            psttm.setString(18, planViaje.getPl2());
            psttm.setString(19, planViaje.getPt2());
            psttm.setString(20, planViaje.getPl3());
            psttm.setString(21, planViaje.getPt3());
            psttm.setString(22, planViaje.getRadio());
            psttm.setString(23, planViaje.getCelular());
            psttm.setString(24, planViaje.getAvantel());
            psttm.setString(25, planViaje.getTelefono());
            psttm.setString(26, planViaje.getCazador());
            psttm.setString(27, planViaje.getMovil());
            psttm.setString(28, planViaje.getOtro());
            psttm.setString(29, planViaje.getQlo());
            psttm.setString(30, planViaje.getQto());
            psttm.setString(31, planViaje.getQld());
            psttm.setString(32, planViaje.getQtd());
            psttm.setString(33, planViaje.getQl1());
            psttm.setString(34, planViaje.getQt1());
            psttm.setString(35, planViaje.getQl2());
            psttm.setString(36, planViaje.getQt2());
            psttm.setString(37, planViaje.getQl3());
            psttm.setString(38, planViaje.getQt3());
            psttm.setString(39, planViaje.getNomfam());
            psttm.setString(40, planViaje.getPhonefam());
            psttm.setString(41, planViaje.getNitpro());
            psttm.setString(42, planViaje.getComentario1());
            psttm.setString(43, planViaje.getComentario2());
            psttm.setString(44, planViaje.getComentario3());
            psttm.setString(45, planViaje.getTl1());
            psttm.setString(46, planViaje.getTt1());
            psttm.setString(47, planViaje.getTl2());            
            psttm.setString(48, planViaje.getTt2());
            psttm.setString(49, planViaje.getTl3());
            psttm.setString(50, planViaje.getTt3());
            psttm.setString(51, planViaje.getLast_mod_user());
            psttm.setString(52, planViaje.getLast_mod_date());
            psttm.setString(53, planViaje.getLast_mod_time());
            psttm.setString(54, planViaje.getUsuario());
            psttm.setString(55, planViaje.getCodtipocarga());
            psttm.setString(56, planViaje.getRetorno());
            psttm.setString(57, planViaje.getCia());
                        
            psttm.executeUpdate();
            actualizarFechaSalidaTrafico(planViaje);
            
        }
        catch(SQLException e){
            if (e.getSQLState().equals("23505"))
                updatePlanVj(planViaje);
            else
                throw new SQLException("ERROR DURANTE LA CREACION DEL PLANDE VIAJE " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (psttm != null) psttm.close();
            if (con != null) poolManager.freeConnection("fintra", con);
        }
    }
    
    
    
    public void planViajeQryAg(String fechaini, String fechafin, String agencia, String dpto) throws SQLException{
        Connection con = null;
        PreparedStatement psttm = null;
        PoolManager poolManager = null;
        ResultSet rs = null;
        PlanVjQry temp = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if ((con == null) || con.isClosed())
                throw new SQLException("NO HAY CONEXION A LA BASE DE DATOS");
            psttm = con.prepareStatement(CONSULTAR_PLAN_VIAJE_AGENCIA);
            planVjQrys = null;
            
            psttm.setString(1, fechaini);
            psttm.setString(2, fechafin);
            psttm.setString(3, agencia);
            
            rs = psttm.executeQuery();
            planVjQrys = new LinkedList();
            while (rs.next()){
                temp = PlanVjQry.load(rs);
                temp.setDpto(dpto);
                if ("traf".equals(temp.getDpto())){
                    if(temp.getLeido().equals("NO"))
                        temp.setClase("TableRowDecorationAlert");
                    else
                        temp.setClase("TableRowDecoration");
                }
                else
                    temp.setClase("TableRowDecoration");
                
                planVjQrys.add(temp);
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA CONSULTA PLAN DE VIAJE AGENCIA " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (psttm != null) psttm.close();
            if (con != null) poolManager.freeConnection("fintra", con);
        }
    }
    
    public void planViajeQryZn(String fechaini, String fechafin, String zona, String dpto) throws SQLException{
        Connection con = null;
        PreparedStatement psttm = null;
        PoolManager poolManager = null;
        ResultSet rs = null;
        PlanVjQry temp = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if ((con == null) || con.isClosed())
                throw new SQLException("NO HAY CONEXION A LA BASE DE DATOS");
            psttm = con.prepareStatement(CONSULTAR_PLAN_VIAJE_ZONA);
            planVjQrys = null;
            
            psttm.setString(1, fechaini);
            psttm.setString(2, fechafin);
            psttm.setString(3, zona);
            
            rs = psttm.executeQuery();
            planVjQrys = new LinkedList();
            while (rs.next()){
                temp = PlanVjQry.load(rs);
                temp.setDpto(dpto);
                if ("traf".equals(temp.getDpto())){
                    if(temp.getLeido().equals("NO"))
                        temp.setClase("TableRowDecorationAlert");
                    else
                        temp.setClase("TableRowDecoration");
                }
                else
                    temp.setClase("TableRowDecoration");
                
                planVjQrys.add(temp);
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA CONSULTA PLAN DE VIAJE ZONA " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (psttm != null) psttm.close();
            if (con != null) poolManager.freeConnection("fintra", con);
        }
    }
    
    public boolean planViajeExist(String distrito, String planilla) throws SQLException{
        Connection con = null;
        PreparedStatement psttm = null;
        PoolManager poolManager = null;
        ResultSet rs = null;
        boolean sw = false;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if ((con == null) || con.isClosed())
                throw new SQLException("NO HAY CONEXION A LA BASE DE DATOS");
            psttm = con.prepareStatement(BURCAR_PLAN_VIAJE);
            psttm.setString(1, planilla);
            psttm.setString(2, distrito);
            rs = psttm.executeQuery();
            if (rs.next()){
               sw = true; 
            } 
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL PLAN DE VIAJE" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (psttm != null) psttm.close();
            if (con != null) poolManager.freeConnection("fintra", con);
        }
        return sw;
    }
    
    public boolean planillaExist(String distrito, String planilla) throws SQLException{
        Connection con = null;
        PreparedStatement psttm = null;
        PoolManager poolManager = null;
        ResultSet rs = null;
        boolean sw = false;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if ((con == null) || con.isClosed())
                throw new SQLException("NO HAY CONEXION A LA BASE DE DATOS");
            psttm = con.prepareStatement(BURCAR_PLANILLA);
            psttm.setString(1, planilla);
            psttm.setString(2, distrito);
            rs = psttm.executeQuery();
            if (rs.next()){
               sw = true; 
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR AL BUSCAR PLANILLA: " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (psttm != null) psttm.close();
            if (con != null) poolManager.freeConnection("fintra", con);
        }
        return sw;
    }
    
    public void getPlanVjInfoOld(String distrito, String planilla) throws SQLException{
        Connection con = null;
        PreparedStatement psttm = null;
        PoolManager poolManager = null;
        ResultSet rs = null;
        planViaje = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if ((con == null) || con.isClosed())
                throw new SQLException("NO HAY CONEXION A LA BASE DE DATOS");
            psttm = con.prepareStatement(PLAN_VIAJE_INFO);
            psttm.setString(1, planilla);
            psttm.setString(2, distrito);
            rs = psttm.executeQuery();
            planViaje = new PlanViaje();
            if (rs.next()){

                planViaje.setPlanilla(rs.getString(1));
                planViaje.setPlaca(rs.getString(2));
                planViaje.setProducto(rs.getString(3));
                planViaje.setTrailer(rs.getString(4));
                planViaje.setContenedor(rs.getString(5));
                planViaje.setFecha(rs.getString(6));
                planViaje.setDestinatario(rs.getString(7));
                planViaje.setRuta(rs.getString(8));

                planViaje.setCedcon(rs.getString(9));
                planViaje.setNomcon(rs.getString(10));
                planViaje.setDircon(rs.getString(11));
                planViaje.setPhonecon(rs.getString(12));
                planViaje.setCiucon(rs.getString(13));

                planViaje.setAl1(rs.getString(14));
                planViaje.setAt1(rs.getString(15));
                planViaje.setAl2(rs.getString(16));
                planViaje.setAt2(rs.getString(17));
                planViaje.setAl3(rs.getString(18));
                planViaje.setAt3(rs.getString(19));

                planViaje.setPl1(rs.getString(20));
                planViaje.setPt1(rs.getString(21));
                planViaje.setPl2(rs.getString(22));
                planViaje.setPt2(rs.getString(23));
                planViaje.setPl3(rs.getString(24));
                planViaje.setPt3(rs.getString(25));

                planViaje.setTl1(rs.getString(43));
                planViaje.setTt1(rs.getString(44));
                planViaje.setTl2(rs.getString(45));
                planViaje.setTt2(rs.getString(46));
                planViaje.setTl3(rs.getString(47));
                planViaje.setTt3(rs.getString(48));

                planViaje.setQlo(rs.getString(33));
                planViaje.setQto(rs.getString(34));
                planViaje.setQld(rs.getString(35));
                planViaje.setQtd(rs.getString(36));
                planViaje.setQl1(rs.getString(37));
                planViaje.setQt1(rs.getString(38));
                planViaje.setQl2(rs.getString(39));
                planViaje.setQt2(rs.getString(40));
                planViaje.setQl3(rs.getString(41));
                planViaje.setQt3(rs.getString(42));

                planViaje.setRadio(rs.getString(26));                  
                planViaje.setCelular(rs.getString(27));
                planViaje.setAvantel(rs.getString(28));
                planViaje.setCazador(rs.getString(29));
                planViaje.setTelefono(rs.getString(30));
                planViaje.setMovil(rs.getString(31));
                planViaje.setOtro(rs.getString(32));

                planViaje.setNomfam(rs.getString(49));
                planViaje.setPhonefam(rs.getString(50));

                planViaje.setNitpro(rs.getString(51));
                planViaje.setNompro(rs.getString(52));
                planViaje.setDirpro(rs.getString(53));
                planViaje.setPhonepro(rs.getString(54));
                planViaje.setCiupro(rs.getString(55));

                planViaje.setComentario1(rs.getString(56));
                planViaje.setComentario2(rs.getString(57));
                planViaje.setComentario3(rs.getString(58));
                planViaje.setUsuario(rs.getString(59));
                planViaje.setCodtipocarga(rs.getString(60));
                planViaje.setTipocarga(getDescTipoCarga(planViaje.getCodtipocarga()));
                planViaje.setCreation_date(rs.getString(61));
                planViaje.setRetorno(rs.getString(62));
                planViaje.setPlareal(rs.getString(63)); 
                planViaje.setCia(rs.getString(64));
            }   
        }
        catch(SQLException e){
            throw new SQLException("ERROR OBTENIEDO LA INFORMACION DEL PLAN DE VIAJE DEL SOT : " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (psttm != null) psttm.close();
            if (con != null) poolManager.freeConnection("fintra", con);
        }
    }
    
    public void getPlanVjInfoNew(String distrito, String planilla) throws SQLException{
        PreparedStatement psttm = null;
        ResultSet rs = null;
        planViaje = null;
        NitSot nit = null;
        String tipoCarga = "";
        try{ 
            //OBTIENE INFORMACION DE LA PLANILLA
            try{
                psttm = this.crearPreparedStatement("SQL_INF_PLANILLA");
                psttm.setString(1, planilla);
                psttm.setString(2, distrito);
                ////System.out.println("query plan viaje: "+psttm);
                rs = psttm.executeQuery();
                planViaje = new PlanViaje();
                if (rs.next()){
                    planViaje.setCia(rs.getString(1));
                    planViaje.setPlanilla(rs.getString(2));
                    planViaje.setPlaca(rs.getString(3));
                    planViaje.setTrailer(rs.getString(4));
                    planViaje.setRuta(rs.getString(5));
                    planViaje.setCedcon(rs.getString(6));
                    planViaje.setNitpro(rs.getString(7));
                    planViaje.setContenedor(rs.getString(8));
                    planViaje.setDestinatario(rs.getString(9));
                    planViaje.setCodtipocarga(rs.getString(10));
                    planViaje.setFecha(rs.getString("fecha"));
                }
            }
            catch(SQLException e){
                throw new SQLException("ERROR OBTENIENDO INFORMACION DE PLANILLA DEL PV: " + e.getMessage() + " " + e.getErrorCode());
            }
            finally {
                if ( psttm != null ) {psttm.close();}
            }
   
            //BUSCA EL TIPO DE CARGA
            if ((planViaje.getCodtipocarga() != null) && !planViaje.getCodtipocarga().trim().equals("")){
                tipoCarga = getDescTipoCarga(planViaje.getCodtipocarga());
                planViaje.setTipocarga(tipoCarga);
            }

            //BUSCA INFORMACIN DEL CONDUCTOR
            nit = getInfoNit(planViaje.getCedcon());
            if (nit != null){
                planViaje.setCedcon(nit.getCedula());
                planViaje.setNomcon(nit.getNombre());
                planViaje.setDircon(nit.getDireccion());
                planViaje.setPhonecon(nit.getTelefono());
                planViaje.setCiucon(nit.getCodciu());
            }
            
            //BUSCA INFORMACION DEL PROPIETARIO
            try{
                psttm = this.crearPreparedStatement("SQL_PROPIETARIO");
                psttm.setString(1, planViaje.getPlaca());
                rs = psttm.executeQuery();
                if (rs.next()){
                    planViaje.setNitpro(rs.getString(1));
                    if ((planViaje.getNitpro() != null) && !planViaje.getNitpro().trim().equals("")){
                       nit = getInfoNit(planViaje.getNitpro());
                       if (nit != null){
                           planViaje.setNitpro(nit.getCedula());
                           planViaje.setNompro(nit.getNombre());
                           planViaje.setDirpro(nit.getDireccion());
                           planViaje.setPhonepro(nit.getTelefono());
                           planViaje.setCiupro(nit.getCodciu());                   
                       }
                    }
                }
            }
            catch(SQLException e){
                throw new SQLException("ERROR OBTENIEDO INFORMACION DEL PROPIETARIO: "+e.getMessage());
            }
        }
        finally{
            if (psttm != null) psttm.close();
            this.desconectar("SQL_INF_PLANILLA");
            this.desconectar("SQL_PROPIETARIO");
        }
    }
    /**
     * Getter for property planViaje.
     * @return Value of property planViaje.
     */
    public PlanViaje getPlanViaje() {
        return planViaje;
    }
    
    /**
     * Setter for property planViaje.
     * @param planViaje New value of property planViaje.
     */
    public void setPlanViaje(PlanViaje planViaje) {
        this.planViaje = planViaje;
    }
    
    
    
    public void updatePlanVj(PlanViaje planViaje) throws Exception {
        Connection con = null;
        PreparedStatement psttm = null;
        PoolManager poolManager = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if ((con == null) || con.isClosed())
                throw new SQLException("NO HAY CONEXION A LA BASE DE DATOS");
            
            
            psttm = con.prepareStatement(MOD_PLAN_VIAJE);
            psttm.setString(1, planViaje.getPlaca());
            psttm.setString(2, planViaje.getProducto());
            psttm.setString(3, planViaje.getTrailer());
            psttm.setString(4, planViaje.getContenedor());
            psttm.setString(5, planViaje.getFecha());
            psttm.setString(6, planViaje.getDestinatario());
            psttm.setString(7, planViaje.getRuta());
            psttm.setString(8, planViaje.getCedcon());
            psttm.setString(9, planViaje.getAl1());
            psttm.setString(10, planViaje.getAt1());
            psttm.setString(11, planViaje.getAl2());
            psttm.setString(12, planViaje.getAt2());
            psttm.setString(13, planViaje.getAl3());
            psttm.setString(14, planViaje.getAt3());
            psttm.setString(15, planViaje.getPl1());
            psttm.setString(16, planViaje.getPt1());
            psttm.setString(17, planViaje.getPl2());
            psttm.setString(18, planViaje.getPt2());
            psttm.setString(19, planViaje.getPl3());
            psttm.setString(20, planViaje.getPt3());
            psttm.setString(21, planViaje.getRadio());
            psttm.setString(22, planViaje.getCelular());
            psttm.setString(23, planViaje.getAvantel());
            psttm.setString(24, planViaje.getTelefono());
            psttm.setString(25, planViaje.getCazador());
            psttm.setString(26, planViaje.getMovil());
            psttm.setString(27, planViaje.getOtro());
            psttm.setString(28, planViaje.getQlo());
            psttm.setString(29, planViaje.getQto());
            psttm.setString(30, planViaje.getQld());
            psttm.setString(31, planViaje.getQtd());
            psttm.setString(32, planViaje.getQl1());
            psttm.setString(33, planViaje.getQt1());
            psttm.setString(34, planViaje.getQl2());
            psttm.setString(35, planViaje.getQt2());
            psttm.setString(36, planViaje.getQl3());
            psttm.setString(37, planViaje.getQt3());
            psttm.setString(38, planViaje.getNomfam());
            psttm.setString(39, planViaje.getPhonefam());
            psttm.setString(40, planViaje.getNitpro());
            psttm.setString(41, planViaje.getComentario1());
            psttm.setString(42, planViaje.getComentario2());
            psttm.setString(43, planViaje.getComentario3());
            psttm.setString(44, planViaje.getTl1());
            psttm.setString(45, planViaje.getTt1());
            psttm.setString(46, planViaje.getTl2());            
            psttm.setString(47, planViaje.getTt2());
            psttm.setString(48, planViaje.getTl3());
            psttm.setString(49, planViaje.getTt3());
            psttm.setString(50, planViaje.getLast_mod_user());
            psttm.setString(51, planViaje.getLast_mod_date());
            psttm.setString(52, planViaje.getLast_mod_time());
            psttm.setString(53, planViaje.getRetorno());
            psttm.setString(54, planViaje.getPlanilla());
            psttm.setString(55, planViaje.getCia());
            
            psttm.executeUpdate();
            actualizarFechaSalidaTrafico(planViaje);
            
            
        }
        catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA ACTUALIZACION DEL PLANDE VIAJE" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (psttm != null) psttm.close();
            if (con != null) poolManager.freeConnection("fintra", con);
        }
    }
    
    /**
     * Getter for property planVjQrys.
     * @return Value of property planVjQrys.
     */
    public java.util.List getPlanVjQrys() {
        return planVjQrys;
    }
    
    /**
     * Setter for property planVjQrys.
     * @param planVjQrys New value of property planVjQrys.
     */
    public void setPlanVjQrys(java.util.List planVjQrys) {
        this.planVjQrys = planVjQrys;
    }
    
    /**
     *Registra el usuario que consulta el plan de viaje
     */
    public void regUserQry(String distrito, String planilla, String usuario) throws SQLException{
        Connection con = null;
        PreparedStatement psttm = null;
        PoolManager poolManager = null;
        ResultSet rs = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if ((con == null) || con.isClosed())
                throw new SQLException("NO HAY CONEXION A LA BASE DE DATOS");
            psttm = con.prepareStatement(SQL_FIND_USER_QUERY);
            psttm.setString(1, planilla);
            psttm.setString(2, usuario);
            psttm.setString(3, distrito);
            rs = psttm.executeQuery();
            if (rs.next()){
                psttm = con.prepareStatement(SQL_MOD_USER_QUERY);
                psttm.setString(1, planilla);
                psttm.setString(2, usuario);
                psttm.setString(3, distrito);
                psttm.executeUpdate();
            }
            else{
                psttm = con.prepareStatement(SQL_REG_USER_QUERY);
                psttm.setString(1, planilla);
                psttm.setString(2, usuario);
                psttm.setString(3, distrito);
                psttm.executeUpdate();
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR CREANDO AUDITORIA DE PLAN VIAJE: " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (psttm != null) psttm.close();            
            if (con != null) poolManager.freeConnection("fintra", con);
        }
    }
    
    /**
     *CAMBIA EL VALOR DEL CAMPO LEIDO A 'SI' ES DECIR INDICA QUE EL PLAN DE VIAJE HA SIDO LEIDO
     */
    public void leer(String distrito, String planilla) throws SQLException{
        Connection con = null;
        PreparedStatement psttm = null;
        PoolManager poolManager = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if ((con == null) || con.isClosed())
                throw new SQLException("NO HAY CONEXION A LA BASE DE DATOS");
            psttm = con.prepareStatement(SQL_LEIDO);
            psttm.setString(1, planilla);
            psttm.setString(2, distrito);
            psttm.executeUpdate();
        }
        catch(SQLException e){
            throw new SQLException("ERROR CAMBIANDO EL ESTADO DE LEIDO: " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (psttm != null) psttm.close();
            if (con != null) poolManager.freeConnection("fintra", con);
        }
    }
    
    /**
     *obtiene los usuarios que han consultado el plan de viaje
     */
    public void getQryUsers(String distrito, String planilla) throws SQLException{
        Connection con = null;
        PreparedStatement psttm = null;
        PoolManager poolManager = null;
        ResultSet rs = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if ((con == null) || con.isClosed())
                throw new SQLException("NO HAY CONEXION A LA BASE DE DATOS");
            psttm = con.prepareStatement(SQL_USER_LIST);
            psttm.setString(1, planilla);
            psttm.setString(2, distrito);
            rs = psttm.executeQuery();
            cbxQryUsers = null;
            cbxQryUsers = new TreeMap();
            while(rs.next()){
                cbxQryUsers.put(rs.getString(2), rs.getString(1));
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR LISTANDO USUARIOS: " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (psttm != null) psttm.close();
            if (con != null) poolManager.freeConnection("fintra", con);
        }
    }
 
    private String getDescTipoCarga(String codTipoCarga) throws SQLException{
        Connection con = null;
        PreparedStatement psttm = null;
        PoolManager poolManager = null;
        ResultSet rs = null;
        String descTipoCarga = "";
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if ((con == null) || con.isClosed())
                throw new SQLException("NO HAY CONEXION A LA BASE DE DATOS");
            psttm = con.prepareStatement(SQL_DESC_TIPOCARGA);
            psttm.setString(1, codTipoCarga);
            rs = psttm.executeQuery();
            if (rs.next()){
                descTipoCarga = rs.getString(1);
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR BUSCANDO LA DESCRIPCION DEL TIPO DE CARGA: " + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (psttm != null) psttm.close();
            if (con != null) poolManager.freeConnection("fintra", con);                
        }
        return descTipoCarga;
    }
    
    public NitSot getInfoNit(String cedula) throws SQLException{
        Connection con = null;
        PreparedStatement psttm = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        NitSot nit = null;
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if ((con == null) || con.isClosed())
                throw new SQLException("NO HAY CONEXION A LA BASE DE DATOS");
            psttm = con.prepareStatement(SQL_INFO_NIT);
            psttm.setString(1, cedula);
            rs = psttm.executeQuery();
            if (rs.next()){
                nit = new NitSot();
                nit.setCedula(rs.getString(1));
                nit.setNombre(rs.getString(2));
                nit.setDireccion(rs.getString(3));
                nit.setTelefono(rs.getString(4));
                nit.setCodciu(rs.getString(5));
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR OBTENIENDO LA INFORMACION DEL NIT: "+cedula+" "+e.getMessage());
        }
        finally{
            if (psttm != null) psttm.close();
            if (con != null) poolManager.freeConnection("fintra", con);
        }
        return nit;
    }
    
    public String getZona(String distrito, String planilla) throws SQLException{
        Connection con = null;
        PreparedStatement psttm = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        String zona = "";
        try{
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if ((con == null) || con.isClosed())
                throw new SQLException("NO HAY CONEXION A LA BASE DE DATOS");
            psttm = con.prepareStatement(SQL_ZONA);
            psttm.setString(1, planilla);
            psttm.setString(2, distrito);
            rs = psttm.executeQuery();
            if (rs.next()){
                zona = rs.getString(1);
            }
        }
        catch(SQLException e){
            throw new SQLException("ERROR BUSCANDO ZONA: "+e.getMessage());
        }
        finally{
            if (psttm != null) psttm.close();
            if (con != null) poolManager.freeConnection("fintra", con);
        }
        return zona;
    }
    /**
     * Getter for property cbxQryUsers.
     * @return Value of property cbxQryUsers.
     */
    public java.util.TreeMap getCbxQryUsers() {
        return cbxQryUsers;
    }
    
    /**
     * Setter for property cbxQryUsers.
     * @param cbxQryUsers New value of property cbxQryUsers.
     */
    public void setCbxQryUsers(java.util.TreeMap cbxQryUsers) {
        this.cbxQryUsers = cbxQryUsers;
    }
    
    
    
    
    
    
    
    
    
    
    /**
     * Listado de pc para la via seleccionada
     * @autor.......fvillacob          
     * @throws......Exception
     * @version.....1.0.     
     **/  
     public List searchPC( String via)throws Exception{
          PreparedStatement st        = null;
          ResultSet         rs        = null;
          String            query     = "SQL_PC_VIA";          
          List              lista     = new LinkedList();
          try {
              
              st= this.crearPreparedStatement(query);              
              st.setString(1, via);
              rs = st.executeQuery();
              if( rs.next() ){
                    String  pcs = rs.getString("via");
                  
                    PlanDeViajeDAO  dao  =  new  PlanDeViajeDAO();
                    
                    for (int i=0; i <pcs.length(); i += 2 ){  
                         Hashtable  pc =  new Hashtable();
                        
                         String puesto   = pcs.substring( i  , i+2 );
                         String nombre   = dao.nombreCiudad(puesto);
                         
                         String datoZona = dao.getZona(puesto); 
                         String vec[]    = datoZona.split(":");
                         String zona     = vec[0];
                         String descZona = "";
                         if( vec.length>1)
                             descZona = vec[1];
                         
                         pc.put("puesto",     puesto    );
                         pc.put("nombre",     nombre    );
                         pc.put("zona",       zona      );
                         pc.put("descZona",   descZona  );
                         
                         lista.add( pc );  
                    } 
                  
              }
              
              
          } catch(SQLException e){
              throw new SQLException(" DAO: searchPC.-->"+ e.getMessage());
          }  
          finally{
             if(st!=null) st.close();
             if(rs!=null) rs.close();
             this.desconectar(query);
          } 
          return lista;
    }
     
     
     
     
     
     /**
     * Busca datos de la agencia asociada a la ciudad
     * @autor.......fvillacob          
     * @throws......Exception
     * @version.....1.0.     
     **/  
     public Hashtable getDatosAgencia( String agencia)throws Exception{
          PreparedStatement st        = null;
          ResultSet         rs        = null;
          String            query     = "SQL_DATOS_AGENCIA"; 
          Hashtable         datos     = null;
          try {
              
              st= this.crearPreparedStatement(query);              
              st.setString(1, agencia);
              rs = st.executeQuery();
              if( rs.next() ){
                  datos  =  new Hashtable();
                  
                  datos.put("codciu",           reset( rs.getString("codciu")     ) );
                  datos.put("nomciu",           reset( rs.getString("nomciu")     ) );
                  datos.put("codica",           reset( rs.getString("codica")     ) );
                  datos.put("zona",             reset( rs.getString("zona")       ) );
                  datos.put("agasoc",           reset( rs.getString("agasoc")     ) );
                  datos.put("tipociu",          reset( rs.getString("tipociu")    ) );
                  datos.put("tiene_rep_urbano", reset( rs.getString("tiene_rep_urbano") ) );
                  datos.put("frontasoc",        reset( rs.getString("frontasoc")  ) );
                  datos.put("indicativo",       reset( rs.getString("indicativo") ) );
                  
              }
                  
              
          } catch(SQLException e){
              throw new SQLException(" DAO: getDatosAgencia.-->"+ e.getMessage());
          }  
          finally{
             if(st!=null) st.close();
             if(rs!=null) rs.close();
             this.desconectar(query);
          } 
          return datos;
    }
     
     
     
     
     public String reset(String val){
         if(val==null)
            val="";
         return val;
     }
     
     
    
     
     /**
     * Busca la agencia asociada a la ciudad
     * @autor.......fvillacob          
     * @throws......Exception
     * @version.....1.0.     
     **/  
     public String getAgenciaAsociada( String ciudad)throws Exception{
          PreparedStatement st        = null;
          ResultSet         rs        = null;
          String            query     = "SQL_AGEN_RELACIONADA"; 
          String            agencia   = "";
          try {
              
              st= this.crearPreparedStatement(query);              
              st.setString(1, ciudad);
              rs = st.executeQuery();
              if( rs.next() )
                  agencia = rs.getString(1);
              
          } catch(SQLException e){
              throw new SQLException(" DAO: getAgenciaAsociada.-->"+ e.getMessage());
          }  
          finally{
             if(st!=null) st.close();
             if(rs!=null) rs.close();
             this.desconectar(query);
          } 
          return agencia.toUpperCase();
    }
     
     
     
     
     
    
    
    /**
     * Actualizamos datos de tablas de traficos
     * @autor.......fvillacob          
     * @throws......Exception
     * @version.....1.0.     
     **/ 
    public void actualizarFechaSalidaTrafico(PlanViaje planViaje)throws Exception {
        PreparedStatement ps    = null;
        String            query = "SQL_ACTUALIZAR_SALIDA_TRAFICO-INGRESO_TRAFICO";
        try {
            
            
            String planilla      = planViaje.getPlanilla();
            String fecha         = planViaje.getFecha();      // Fecha plan de viaje: salida
            String nombre_origen = "";
            String observacion   ="Salida";
            
         // Buscamos PC y su Zona
            String proxPC        = "";
            String desc_proxPC   = "";
            String zonaPC        = "";
            String desc_zonaPC   = "";
            String fechaPC       = "";
            
            
            String origen        =  ""; 
            String destino       =  ""; 
            
            String    via        = planViaje.getRuta();            
            List      pcs        = searchPC(via); 
            if( pcs.size()>0){
                
             // ORIGEN:
                Hashtable  pcORI = (Hashtable)pcs.get(0);
                origen           = (String) pcORI.get("puesto");
                nombre_origen    = (String) pcORI.get("nombre");
                
             // DESTINO:
                Hashtable  pcDES = (Hashtable)pcs.get( pcs.size() - 1 );
                destino          = (String) pcDES.get("puesto");
                
             // PC :
                Hashtable  pc = (Hashtable)pcs.get(1);                
                proxPC        = (String) pc.get("puesto");
                desc_proxPC   = (String) pc.get("nombre");
                zonaPC        = (String) pc.get("zona");
                desc_zonaPC   = (String) pc.get("descZona");
                
                PlanDeViajeDAO  dao  =  new  PlanDeViajeDAO();                
                fechaPC       = dao.getDuracion(fecha, origen, proxPC );                
            }
            
            
        // Validar urbano:
           String ageAso_origen  =  this.getAgenciaAsociada( origen  );
           String ageAso_destino =  this.getAgenciaAsociada( destino );
           if( ageAso_origen.equals( ageAso_destino  ) ){
               String agenciaSeleccion =  ageAso_origen;
               Hashtable  datos        =  this.getDatosAgencia( agenciaSeleccion );
               if( datos!=null ){
                   String aplica_urbano =  (String) datos.get("tiene_rep_urbano");  
                   if( aplica_urbano.trim().toUpperCase().equals("S")  ){
                       zonaPC       =  (String) datos.get("codciu");
                       desc_zonaPC  =  (String) datos.get("nomciu");
                   }                   
               }
               
           }
            
            observacion +=" "+nombre_origen+" "+fecha+" "+planViaje.getComentario1()+" "+planViaje.getComentario2()+" "+planViaje.getComentario3();
            ps = this.crearPreparedStatement( query );
            
        // Ingreso Trafico:
            ps.setString(1,  fecha        );
            ps.setString(2,  proxPC       );
            ps.setString(3,  desc_proxPC  );            
            ps.setString(4,  zonaPC       );
            ps.setString(5,  desc_zonaPC  );
            ps.setString(6,  via          );
            ps.setString(7,  fechaPC      );
            ps.setString(8,  planViaje.getCelular()  );
            ps.setString(9,  fecha        );
            ps.setString(10,  observacion              );
            ps.setString(11,  planViaje.getUsuario()   );
           
            ps.setString(12, planilla     );
            
        // Trafico:
            ps.setString(13,  fecha       );
            ps.setString(14,  zonaPC      );
            ps.setString(15,  proxPC      );
            ps.setString(16,  fechaPC     );
            ps.setString(17,  planViaje.getCelular() );
            ps.setString(18,  fecha       );
            ps.setString(19,  observacion            );
            ps.setString(20,  planViaje.getUsuario() );
            ps.setString(21,  planilla    );
            
            ps.executeUpdate();
            
            
        }catch( SQLException ex ){
            throw new SQLException("ERROR AL ACTUALIZAR DATOS TRAFICOS : "+ex.getMessage());
        }
        finally {
            if ( ps != null ) { ps.close(); }
            this.desconectar(query);
        }
    }
    
    
    
    
    /**
     * Este m�todo nos permite saber si una planilla existe usando un query que
     * solo necesite el numero de planilla como parametro para evaluar. por ejemplo:
     * SELECT * from trafico where planilla = ?.
     * @return devuelve true si se encontr� por lo menos un registro con el numero
     * de planilla dado al ejecutar la consulta dada.
     */
    public boolean existePlanilla(String planilla, String nombreConsulta)throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = this.crearPreparedStatement(nombreConsulta);
            ps.setString(1, planilla);
            rs = ps.executeQuery();
            return rs.next();
        }
        catch( Exception ex ){
            throw new SQLException("ERROR AL BUSCAR PLANILLA CON "+nombreConsulta+": "+ex.getMessage());
        }
        finally {
            if ( rs != null ) { rs.close(); }
            if ( ps != null ) { ps.close(); }
            this.desconectar(nombreConsulta);
        }
    }
    
    /**
     * Permite contar cuantos reportes de trafico existen que sean diferentes al de salida.
     */
    public int contarReportesDeTraficoDiferentesDeSalida(String planilla)throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = this.crearPreparedStatement("SQL_CONTAR_REPORTES_DE_TRAFICO");
            ps.setString(1, planilla);
            rs = ps.executeQuery();
            if ( rs.next() ) {
                return rs.getInt(1);
            }
            return 0;
        }
        catch( Exception ex ){
            throw new SQLException("ERROR AL CONTAR REPORTES DE TRAFICO: "+ex.getMessage());
        }
        finally {
            if ( rs != null ) { rs.close(); }
            if ( ps != null ) { ps.close(); }
            this.desconectar("SQL_CONTAR_REPORTES_DE_TRAFICO");
        }
    }
    
    
      /**
     * Busca si la planilla tiene movimientos en trafico
     * @autor.......igomez          
     * @throws......Exception
     * @version.....1.0.     
     **/  
     public boolean tieneMovimientos( String planilla)throws Exception{
          PreparedStatement st        = null;
          ResultSet         rs        = null;
          String            query     = "SQL_MOVIMIENTO"; 
          boolean           tiene   = false;
          try {
              
              st= this.crearPreparedStatement(query);              
              st.setString(1, planilla);
              rs = st.executeQuery();
              if( rs.next() )
                  tiene = true;
              
          } catch(SQLException e){
              throw new SQLException(" DAO: tieneMovimientos.-->"+ e.getMessage());
          }  
          finally{
             if(st!=null) st.close();
             if(rs!=null) rs.close();
             this.desconectar(query);
          } 
          return tiene;
    }
    
}
