/*
 * RecusosdispDAO.java
 *
 * Created on 16 de junio de 2005, 11:24 PM
 */
package com.tsp.operation.model;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.swing.*;
import com.tsp.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;
import com.tsp.util.connectionpool.*;
import com.tsp.operation.model.RegistroAsignacionServices;

/**
 *
 * @author  Jm
 */
public class RecursosDAO {

    Recursosdisp rd;
    Vector recursosd;

    /** Creates a new instance of RecusosdispDAO */
    public RecursosDAO() {
    }

    /**
     * Getter for property rd.
     * @return Value of property rd.
     */
    public Recursosdisp getRd() {
        return rd;
    }

    /**
     * Setter for property rd.
     * @param rd New value of property rd.
     */
    public void setRd( Recursosdisp rd ) {
        this.rd = rd;
    }

    /**
     * Getter for property recursosd.
     * @return Value of property recursosd.
     */
    public java.util.Vector getRecursosd() {
        return recursosd;
    }

    /**
     * Setter for property recursosd.
     * @param recursosd New value of property recursosd.
     */
    public void setRecursosd( java.util.Vector recursosd ) {
        this.recursosd = recursosd;
    }

    public void list() throws SQLException {
        String consulta = "select * from recursos_disp";
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( "fintra" );
            if ( con != null ) {
                st = con.prepareStatement( consulta );
                rs = st.executeQuery();
                recursosd = new Vector();
                while ( rs.next() ) {
                    rd = new Recursosdisp();
                    rd.setDistrito( rs.getString( "dstrct" ) );///modif-distrito-
                    rd.setPlaca( rs.getString( "placa" ) );
                    rd.setaFecha_disp( rs.getString( "afecha_disp" ) );
                    rd.setReg_status( rs.getString( "reg_status" ) );
                    rd.setNumpla( rs.getString( "numpla" ) );
                    rd.setOrigen( rs.getString( "origen" ) );
                    rd.setDestino( rs.getString( "destino" ) );
                    rd.setClase( rs.getString( "tiporecurso" ) );///////modif -tipo_recurso
                    rd.setTipo( rs.getString( "recurso" ) );
                    rd.setaFecha_asig( rs.getString( "afecha_asig" ) );
                    rd.setTiempo( rs.getFloat( "tiempo" ) );
                    rd.setTipo_asig( rs.getString( "tipo_asig" ) );
                    rd.setCod_cliente( rs.getString( "cod_cliente" ) );
                    rd.setEquipo_aso( rs.getString( "equipo_aso" ) );
                    rd.setTipo_aso( rs.getString( "tipo_aso" ) );
                    rd.setDstrct_code_req( rs.getString( "dstrct_code_req" ) );
                    rd.setNum_sec_req( rs.getInt( "num_sec_req" ) );
                    rd.setStd_job_no_req( rs.getString( "std_job_no_req" ) );
                    rd.setNumpla_req( rs.getString( "numpla_req" ) );
                    rd.setFecha_dispo_req( rs.getString( "fecha_dispo_req" ) );
                    rd.setFecha_creacion( rs.getTimestamp( "creation_date" ) );
                    rd.setFecha_actualizacion( rs.getTimestamp(
                        "last_update" ) );
                    rd.setUsuario_creacion( rs.getString( "creation_user" ) );
                    rd.setUsuario_actualizacion( rs.getString(
                        "user_update" ) );
                    recursosd.add( rd );
                }
            }

        }
        catch ( SQLException e ) {
            throw new SQLException(
                "ERROR DURANTE EL PROCESO DE LISTAR LOS RECURSOS DISPONIBLES " +
                e.getMessage() + " " + e.getErrorCode() );
        }
        finally {
            if ( st != null ) {
                try {
                    st.close();
                }
                catch ( SQLException e ) {
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO" +
                                            e.getMessage() );
                }
            }

            if ( con != null ) {
                poolManager.freeConnection( "fintra", con );
            }
        }

    }

    /********************************** Miembros de clase nuevos ******************************/

    /*
     */

    String nombreConexion = "fintra";///modif
    private static final String consultaTodos = "select * from recursos_disp";
    private static final String consultaTieneRutaFavorita = "select tipo from recursos_disp,ruta_fav where recursos_disp.placa = ruta_fav.placa and ruta_fav.placa = ?";
    private static final String consultaPuedeViajarEnRutaFavorita = "select tipo from recursos_disp,ruta_fav where recursos_disp.placa = ruta_fav.placa and recursos_disp.placa = ? and ruta_fav.origen = ? and ruta_fav.destino = ?";
    private static final String consultaTieneClienteFavorito = "select tipo from recursos_disp,cliente_fav where recursos_disp.placa = cliente_fav.placa and cliente_fav.placa = ?";
    private static final String consultaPuedeTransportarCliente = "select tipo from recursos_disp,cliente_fav where recursos_disp.placa = cliente_fav.placa and recursos_disp.placa = ? and cliente_fav.codcliente = ?";
    private static final String consultaAsignarRecurso = "update recursos_disp set reg_status = 'A', fecha_asig = ?, last_update = now(), dstrct_code_req = ?, num_sec_req = ?, std_job_no_req = ?, numpla_req = ?, fecha_dispo_req = ? where cod_recurso = ? and recurso = ? and dstrct = ? and placa = ? and fecha_disp = ?";//modif-distrito
    private static final String consultaAsignarReq = "update req_cliente set estado = 'A', fecha_asing = ?, fecha_posibleentrega = ?, last_update = now() where dstrct_code = ? and num_sec = ? and std_job_no = ? and fecha_dispo = ? and numpla = ?";
    private static final String consultaObtenerDuracionTramo =
        "select tiempo from tramo where dstrct = ? and origin = ? and destination = ?";
    private static final String sqlCrearNuevoRecursoDisponible =
        "insert into recursos_disp" +
        " (dstrct,placa,fecha_disp,reg_status,numpla,origen,destino,tiporecurso,recurso,tiempo," +
        "equipo_aso,tipo_aso,creation_user,user_update)" +
        " values(?,?,?,?,?,?,?,?,?,?,?,?,?,?)";//modif -distrito-tipo_recurso

    public Vector obtenerRecursosNoAsignados( String origen ) throws SQLException {
        return listarPorWhere( "where tipo_asig = ? and origen = ?", new String[] {"_", origen}
                               , new Class[] {String.class, String.class} );
    }

    private Vector listarPorWhere( String where, Object[] parametros,
                                   Class[] tiposParametros ) throws
        SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        Vector recursos = new Vector();
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( nombreConexion );
            //poolManager.getConnection("fintra");
            ////System.out.println( "con: " + con );
            if ( con != null ) {
                st = con.prepareStatement( consultaTodos + " " + where );
                for ( int i = 0; i < tiposParametros.length; i++ ) {
                    if ( tiposParametros[0] == String.class ) {
                        ////System.out.println( "parametro agregado: " + parametros[i] );
                        st.setString( i + 1, parametros[i].toString() );
                    }
                    else {
                        st.setInt( i + 1,
                                   ( ( Integer ) parametros[i] ).intValue() );
                    }
                }
                rs = st.executeQuery();
                ////System.out.println( "query ejecutado" );
                while ( rs.next() ) {
                    rd = new Recursosdisp();
                    rd.setDistrito( rs.getString( "dstrct" ) );///modif -distrito
                    rd.setPlaca( rs.getString( "placa" ) );
                    rd.setaFecha_disp( rs.getString( "fecha_disp" ) );
                    rd.setReg_status( rs.getString( "reg_status" ) );
                    rd.setNumpla( rs.getString( "numpla" ) );
                    rd.setOrigen( rs.getString( "origen" ) );
                    rd.setDestino( rs.getString( "destino" ) );
                    rd.setClase( rs.getString( "tiporecurso" ) );//modif -tipo_recurso-
                    rd.setTipo( rs.getString( "recurso" ) );
                    rd.setaFecha_asig( rs.getString( "fecha_asig" ) );
                    rd.setTiempo( rs.getFloat( "tiempo" ) );
                    rd.setTipo_asig( rs.getString( "tipo_asig" ) );
                    rd.setCod_cliente( rs.getString( "cod_cliente" ) );
                    rd.setEquipo_aso( rs.getString( "equipo_aso" ) );
                    rd.setTipo_aso( rs.getString( "tipo_aso" ) );
                    rd.setDstrct_code_req( rs.getString( "dstrct_code_req" ) );
                    rd.setNum_sec_req( rs.getInt( "num_sec_req" ) );
                    rd.setStd_job_no_req( rs.getString( "std_job_no_req" ) );
                    rd.setNumpla_req( rs.getString( "numpla_req" ) );
                    rd.setFecha_dispo_req( rs.getString( "fecha_dispo_req" ) );
                    rd.setFecha_creacion( rs.getTimestamp( "creation_date" ) );
                    rd.setFecha_actualizacion( rs.getTimestamp(
                        "last_update" ) );
                    rd.setUsuario_creacion( rs.getString( "creation_user" ) );
                    rd.setUsuario_actualizacion( rs.getString(
                        "user_update" ) );
                    recursos.add( rd );
                    ////System.out.println( "Recurso agregado... recursos = " + recursos );
                }
                return recursos;
            }

        }
        catch ( SQLException e ) {
            throw new SQLException(
                "ERROR DURANTE EL PROCESO DE LISTAR LOS RECURSOS DISPONIBLES " +
                e.getMessage() + " " + e.getErrorCode() );
        }
        finally {
            if ( st != null ) {
                try {
                    st.close();
                }
                catch ( SQLException e ) {
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO" +
                                            e.getMessage() );
                }
            }

            if ( con != null ) {
                poolManager.freeConnection( nombreConexion, con );
                 //"fintra", con );
            }
            return recursos;
        }
    }

    public boolean puedeViajarEnRuta( String placa, String origen, String destino ) throws
        SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( nombreConexion );
            //poolManager.getConnection("fintra");
            ////System.out.println( "con: " + con );
            if ( con != null ) {
                st = con.prepareStatement( consultaTieneRutaFavorita );
                st.setString( 1, placa );
                rs = st.executeQuery();
                if ( !rs.next() ) { // si esta consulta no devuelve nada es porque no hay restriccion de ruta favorita para ese recurso
                    return true;
                }
                st = con.prepareStatement( consultaPuedeViajarEnRutaFavorita );
                st.setString( 1, placa );
                st.setString( 2, origen );
                st.setString( 3, destino );
                rs = st.executeQuery();
                return rs.next(); // si esta consulta devuelve algo es porque si existe esa ruta favorita para ese recurso
            }

        }
        catch ( SQLException e ) {
            throw new SQLException(
                "ERROR DURANTE EL PROCESO DE BUSCAR RUTA FAVORITA " +
                e.getMessage() + " " + e.getErrorCode() );
        }
        finally {
            if ( st != null ) {
                try {
                    st.close();
                }
                catch ( SQLException e ) {
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO" +
                                            e.getMessage() );
                }
            }

            if ( con != null ) {
                poolManager.freeConnection( nombreConexion, con );//"fintra" , con );
            }
            return false;
        }
    }

    public boolean puedeTransportarCliente( String placa, String cliente ) throws
        SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( nombreConexion );
            //poolManager.getConnection("fintra");
            ////System.out.println( "con: " + con );
            if ( con != null ) {
                st = con.prepareStatement( consultaTieneClienteFavorito );
                st.setString( 1, placa );
                rs = st.executeQuery();
                if ( !rs.next() ) { // si esta consulta no devuelve nada es porque no hay restriccion de cliente favorito para ese recurso
                    return true;
                }
                st = con.prepareStatement( consultaPuedeTransportarCliente );
                st.setString( 1, placa );
                st.setString( 2, cliente );
                rs = st.executeQuery();
                return rs.next(); // si esta consulta devuelve algo es porque ese recurso si puede transportar al cliente
            }
            return false;
        }
        catch ( SQLException e ) {
            throw new SQLException(
                "ERROR DURANTE EL PROCESO DE BUSCAR CLIENTE FAVORITO " +
                e.getMessage() + " " + e.getErrorCode() );
        }
        finally {
            if ( st != null ) {
                try {
                    st.close();
                }
                catch ( SQLException e ) {
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO" +
                                            e.getMessage() );
                }
            }

            if ( con != null ) {
                poolManager.freeConnection( nombreConexion, con );//"fintra", con );
            }
            return false;
        }

    }

    public void realizarAsigancion( Recursosdisp cabezote, Recursosdisp trailer,
                                    ReqCliente requerimiento ) throws SQLException {
        if ( cabezote != null ) {
            asignarRecursoARequerimiento( cabezote, requerimiento );
            String tipoAso = trailer == null ? "" : trailer.getTipo();
            String recursoAso = trailer == null ? "" : trailer.getRecurso();
            guardarRecurso( cabezote, tipoAso, recursoAso );
            guardarRegistroAsignacion( cabezote, requerimiento, recursoAso, tipoAso );

        }
        if ( trailer != null ) {
            asignarRecursoARequerimiento( trailer, requerimiento );
            String tipoAso = cabezote == null ? "" : cabezote.getTipo();
            String recursoAso = cabezote == null ? "" : cabezote.getRecurso();
            guardarRecurso( trailer, tipoAso, recursoAso );
            guardarRegistroAsignacion( trailer, requerimiento, recursoAso, tipoAso );
        }

        // ingresar los datos en el registro de asignacion
    }

    /**
     * guardarRegistroAsignacion
     */
    private void guardarRegistroAsignacion( Recursosdisp recurso, ReqCliente requerimiento,
                                            String recursoAso, String tipoAso ) throws SQLException {
        RegistroAsignacion reg = new RegistroAsignacion();
        reg.setClase_req( requerimiento.getclase_req() );
        reg.setCliente_req( requerimiento.getcliente() );
        reg.setCod_cliente_rec( recurso.getCod_cliente() );
        reg.setCreation_user( recurso.getUsuario_creacion() );
        reg.setDestino_rec( recurso.getDestino() );
        reg.setDestino_req( requerimiento.getdestino() );
        reg.setDistri_rec( recurso.getDistrito() );
        reg.setDistri_req( requerimiento.getdestino() );
        reg.setEquipo_aso_rec( recursoAso );
        reg.setEstado_req( "A" );
        reg.setFecha_asig_rec( recurso.getaFecha_asig() );
        reg.setFecha_asing_req( requerimiento.getfecha_asign() );
        reg.setFecha_disp_rec( recurso.getaFecha_disp() );
        reg.setFecha_disp_req( requerimiento.getfecha_dispo() );
        reg.setFecha_posibleentrega_req( requerimiento.getFecha_posibleentrega() );
        reg.setId_rec_cab_req( requerimiento.getId_rec_cab() );
        reg.setId_rec_tra_req( requerimiento.getId_rec_tra() );
        reg.setNuevafecha_dispo_req( recurso.getaFecha_disp() );
        reg.setNuevo_origen( requerimiento.getdestino() );
        reg.setNumpla_rec( recurso.getNumpla() );
        reg.setNumpla_req( requerimiento.getnumpla() );
        reg.setNum_sec_req( requerimiento.getnum_sec() );
        reg.setOrigen_rec( recurso.getOrigen() );
        reg.setOrigen_req( requerimiento.getorigen() );
        reg.setPlaca_rec( recurso.getPlaca() );
        reg.setPrioridad1_req( requerimiento.getprioridad1() );
        reg.setPrioridad2_req( requerimiento.getprioridad2() );
        reg.setPrioridad3_req( requerimiento.getprioridad3() );
        reg.setPrioridad4_req( requerimiento.getprioridad4() );
        reg.setPrioridad5_req( requerimiento.getprioridad5() );
        reg.setRecurso1_req( requerimiento.getrecurso1() );
        reg.setRecurso2_req( requerimiento.getrecurso2() );
        reg.setRecurso3_req( requerimiento.getrecurso3() );
        reg.setRecurso4_req( requerimiento.getrecurso4() );
        reg.setRecurso5_req( requerimiento.getrecurso5() );
        reg.setReg_status_rec( "A" );
        reg.setStd_job_no_req( requerimiento.getstd_job_no() );
        reg.setTiempo_rec( recurso.getaTiempo() );
        reg.setTipo_asig_rec( recurso.getTipo_asig() );
        reg.setTipo_asing_req( requerimiento.gettipo_asign() );
        reg.setTipo_aso_rec( tipoAso );
        reg.setTipo_rec1_req( requerimiento.getTipo_recurso1() );
        reg.setTipo_rec2_req( requerimiento.getTipo_recurso2() );
        reg.setTipo_rec3_req( requerimiento.getTipo_recurso3() );
        reg.setTipo_rec4_req( requerimiento.getTipo_recurso4() );
        reg.setTipo_rec5_req( requerimiento.getTipo_recurso5() );
        reg.setTipo_recurso_rec( recurso.getTipo() );
        reg.setUser_update( recurso.getUsuario_actualizacion() );
        RegistroAsignacionServices serv = new RegistroAsignacionServices();
        serv.guardarRegistroAsignacion(reg);
    }

    /**
     * guardarRecurso
     *
     * @param cabezote Recursosdisp
     * @param fechaPosibleEntrega String
     * @param tipoAso String
     * @param recursoAso String
     */
    private void guardarRecurso( Recursosdisp r, String tipoAso,
                                 String recursoAso ) {
        Connection con = null;
        PreparedStatement ps = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( nombreConexion );
            //poolManager.getConnection("fintra");
            ////System.out.println( "con: " + con );
            if ( con != null ) {

                ps = con.prepareStatement( this.sqlCrearNuevoRecursoDisponible );
                ps.setString( 1, r.getDistrito() );
                ps.setString( 2, r.getPlaca() );
                ps.setString( 3, r.getaFecha_disp() );
                ps.setString( 4, "" );
                ps.setString( 5, r.getNumpla() );
                ps.setString( 6, r.getOrigen() );
                ps.setString( 7, "" );
                ps.setString( 8, r.getTipo() );
                ps.setString( 9, r.getRecurso() );
                ps.setFloat( 10, r.getaTiempo() );
                ps.setString( 11, recursoAso );
                ps.setString( 12, tipoAso );
                ps.setString( 13, r.getUsuario_creacion() );
                ps.setString( 14, r.getUsuario_actualizacion() );

            }
        }
        catch ( Exception ex ) {
            ex.printStackTrace();
        }
        finally{
           
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }

    /**
     * Preguntas pendientes:
     * - En el campo fecha_asig va la hora actual en que se realiza la asignacion o
     * la hora que indica el requerimiento en que debe ser aignado?
     * @param recurso Recursosdisp
     * @param requerimiento ReqCliente
     * @throws SQLException
     */
    private void asignarRecursoARequerimiento( Recursosdisp recurso, ReqCliente requerimiento ) throws
        SQLException {
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( nombreConexion );
            //poolManager.getConnection("fintra");
            ////System.out.println( "con: " + con );
            if ( con != null ) {
// update recursos_disp set reg_status = 'A', fecha_asig = ?, fecha_act = now(), dstrct_code_req = ?, num_sec_req = ?, std_job_no_req = ?, numpla_req = ?, fecha_dispo_req = ? where recurso = ? and distrito = ? and placa = ? and fecha_disp = ?"
                st = con.prepareStatement( consultaAsignarRecurso );
                st.setString( 2, recurso.getDistrito() );
                st.setInt( 3, requerimiento.getnum_sec() );
                st.setString( 4, requerimiento.getstd_job_no() );
                st.setString( 5, requerimiento.getnumpla() );
                st.setString( 6, requerimiento.getfecha_dispo() );
                st.setString( 7, recurso.getRecurso() );
                st.setString( 8, recurso.getDistrito() );
                st.setString( 9, recurso.getPlaca() );
                st.setString( 10, recurso.getaFecha_disp() );

// update req_cliente set estado = 'A', fecha_asing = ? where dstrct_code = ? and num_sec = ? and std_job_no = ?
// and fecha_dispo = ? and numpla = ?
                PreparedStatement st2 = con.prepareStatement( consultaAsignarReq );
                // esta fecha es calculada cuando se valida el rango de recurso respecto a la fecha del requerimiento
                // -> com.slt.AsignarYSincronizar.requerimientoEntraEnRangoDeRecurso
                Calendar cRec = recurso.getFechaInicio();
                Calendar cReq = Util.crearCalendar( requerimiento.getfecha_dispo() );
                Calendar cMayor = cRec.after( cReq ) ? cRec : cReq;
                String horaSalida = Util.crearStringFecha( cMayor );
                st2.setString( 1, horaSalida ); // la mayor de las dos fechas
                st.setString( 1, horaSalida ); // la mayor de las dos fechas
                st2.setString( 3, requerimiento.getdstrct_code() );
                st2.setInt( 4, requerimiento.getnum_sec() );
                st2.setString( 5, requerimiento.getstd_job_no() );
                st2.setString( 6, recurso.getaFecha_disp() );
                st.execute();

                //select tiempo from tramo where dstrct = ? and origin = ? and destination = ?
                PreparedStatement st3 = con.prepareStatement( consultaObtenerDuracionTramo );
                st3.setString( 1, recurso.getDistrito() );
                st3.setString( 2, requerimiento.getorigen() );
                st3.setString( 3, requerimiento.getdestino() );
                rs = st3.executeQuery();
                int duracion = rs.getInt( "tiempo" );
                cMayor.add( Calendar.HOUR, duracion );
                String fechaPosibleEntrega = Util.crearStringFecha( cMayor );
                st2.setString( 2, fechaPosibleEntrega );
                //fecha_posibleentrega
                st2.execute();

                recurso.setDstrct_code_req( requerimiento.getdstrct_code() );
                recurso.setNum_sec_req( requerimiento.getnum_sec() );
                recurso.setStd_job_no_req( requerimiento.getstd_job_no() );
                recurso.setNumpla_req( requerimiento.getnumpla() );
                recurso.setFecha_dispo_req( requerimiento.getfecha_dispo() );
                requerimiento.setfecha_posibleentrega( fechaPosibleEntrega );

                cMayor = Util.crearCalendar( fechaPosibleEntrega );
                ResourceBundle rb = ResourceBundle.getBundle(
                    "com/vcc/util/connectionpool/db" );
                /*ResourceBundle rb = ResourceBundle.getBundle(
                    "com/tsp/util/connectionpool/db" );*/
                int KI = Integer.parseInt( rb.getString( "KI" ) );
                cMayor.add( Calendar.HOUR, KI );
                recurso.setaFecha_disp( Util.crearStringFecha( cMayor ) );
                recurso.setOrigen( requerimiento.getdestino() );
                recurso.setDestino( requerimiento.getdestino() );
                requerimiento.setfecha_asign( horaSalida );
                recurso.setaFecha_asig( requerimiento.getfecha_asign() );
            }

        }
        catch ( SQLException e ) {
            throw new SQLException(
                "ERROR DURANTE EL PROCESO DE BUSCAR CLIENTE FAVORITO " +
                e.getMessage() + " " + e.getErrorCode() );
        }
        finally {
            if ( st != null ) {
                try {
                    st.close();
                }
                catch ( SQLException e ) {
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO" +
                                            e.getMessage() );
                }
            }
            if ( con != null ) {
                poolManager.freeConnection( nombreConexion, con );//"fintra", con );
            }
        }

    }

}
