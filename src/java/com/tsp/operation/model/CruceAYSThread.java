/*
 * CruceAYSThread.java
 *
 * Created on 26 de junio de 2005, 11:51 AM
 */

package com.tsp.operation.model;

import java.io.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import javax.servlet.*;
import javax.servlet.http.*;


/**
 *
 * @author  Sandrameg
 */
public class CruceAYSThread extends Thread {
    private AsignarYSincronizar ays;
    private String usu;
    private Model  model;
    
    /** Creates a new instance of CruceAYSThread */
    public CruceAYSThread() {
    }
    
    public void star (String us) {   
       this.usu = us; 
        super.start();
    }
    
    public synchronized void run(){
        model = new Model();
        try {            
            ays = new AsignarYSincronizar(this.usu);             
        }
        catch(Exception e){
            e.printStackTrace();
        }  
    }    
}
