package com.tsp.operation.model;

import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;


public class StdjobcostoDAO{
    
    private Stdjobcosto standard;
    private List rutas;
    private List costos;
    
    StdjobcostoDAO(){
        
    }
    public List getRutas(){
        return rutas;
    }
    Stdjobcosto getStdJobCosto(){
        return standard;
    }
    
    String isConnected()throws SQLException{
        String Mensaje="No se pudo Conectar a la BD";
        Connection con=null;
        PoolManager poolManager = null;
        try {
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                Mensaje="Si hay conexion";
            }
        }finally{
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return Mensaje;
        
    }
    public void searchStandardJobCosto(String sj )throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        standard=null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("Select  * from stdjobcosto where sj = ? ");
                st.setString(1,sj);
                rs = st.executeQuery();
                
                if(rs.next()){
                    standard = new Stdjobcosto();
                    standard.setSj(rs.getString("sj"));
                    standard.setPeso_lleno_maximo(rs.getFloat("peso_lleno_maximo"));
                    standard.setPorcentaje_maximo_anticipo(rs.getFloat("porcentaje_maximo_anticipo"));
                    standard.setCf_code(rs.getString("cf_code"));
                    standard.setUnit_cost(rs.getFloat("unit_cost"));
                    standard.setCurrency(rs.getString("currency"));
                    standard.setUnit_transp(rs.getString("unit_transp"));
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL STANDARD" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    public void searchStandardJobCosto(String sj, String ft )throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        standard=null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("Select  * from stdjobcosto where sj = ? and ft_code=?");
                st.setString(1,sj);
                st.setString(2,ft);
                rs = st.executeQuery();
                
                if(rs.next()){
                    standard = new Stdjobcosto();
                    standard.setSj(rs.getString("sj"));
                    standard.setPeso_lleno_maximo(rs.getFloat("peso_lleno_maximo"));
                    standard.setPorcentaje_maximo_anticipo(rs.getFloat("porcentaje_maximo_anticipo"));
                    standard.setCf_code(rs.getString("cf_code"));
                    standard.setUnit_cost(rs.getFloat("unit_cost"));
                    standard.setCurrency(rs.getString("currency"));
                    standard.setUnit_transp(rs.getString("unit_transp"));
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL STANDARD" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    public void searchStandardJobCostoFull(String sj, String ft )throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        standard=null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("Select  sc.*, s.porcentaje_maximo_anticipo as pa from stdjobcostfull sc, stdjob s where sc.sj = ? and sc.ft_code=? AND s.dstrct_code='FINV' and s.std_job_no =sc.sj");
                st.setString(1,sj);
                st.setString(2,ft);
                ////System.out.println(""+st.toString());
                rs = st.executeQuery();
                
                if(rs.next()){
                    standard = new Stdjobcosto();
                    standard.setSj(rs.getString("sj"));
                    standard.setPeso_lleno_maximo(rs.getFloat("peso_lleno_maximo"));
                    standard.setPorcentaje_maximo_anticipo(rs.getFloat("pa"));
                    standard.setCf_code(rs.getString("cf_code"));
                    standard.setUnit_cost(rs.getFloat("unit_cost"));
                    if(rs.getFloat("unit_cost")==0){
                        standard.setUnit_cost(rs.getFloat("cost"));
                    }
                    standard.setCurrency(rs.getString("currency"));
                    standard.setUnit_transp(rs.getString("unit_transp"));
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DEL STANDARD" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    public void searchRutas(String sj )throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("select DISTINCT substring(st.ft_code,1,4)as ft, co.nomciu as or, cd.nomciu as des, st.sj  from stdjobcostfull st, ciudad co, ciudad cd where st.sj=? and co.codciu=substring(ft_code,1,2) and cd.codciu=substring(ft_code,3,2) ");
                st.setString(1,sj);
                rs = st.executeQuery();
                rutas = new LinkedList();
                
                while(rs.next()){
                    standard = new Stdjobcosto();
                    standard.setFt_code(rs.getString("ft"));
                    standard.setSj(rs.getString("sj"));
                    standard.setRuta(rs.getString("or")+"-"+rs.getString("des"));
                    rutas.add(standard);
                    
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LAS RUTAS" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    public void searchCostos(String ruta, String sj )throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null, rs1=null;
        PoolManager poolManager = null;
        ruta=ruta+"%";
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("select ft_code, cf_code, sj, unit_cost, cost,unit_transp,currency from  stdjobcostfull where sj = ? and ft_code like ?");
                st.setString(1,sj);
                st.setString(2,ruta);
                rs = st.executeQuery();
                costos = new LinkedList();
                
                while(rs.next()){
                    standard = new Stdjobcosto();
                    standard.setFt_code(rs.getString("ft_code"));
                    standard.setSj(rs.getString("sj"));
                    standard.setCf_code(rs.getString("cf_code"));
                    standard.setUnit_cost(rs.getFloat("unit_cost"));
                    standard.setCurrency(rs.getString("currency"));
                    if(rs.getFloat("unit_cost")==0){
                        standard.setUnit_cost(rs.getFloat("cost"));
                    }
                    standard.setUnit_transp(rs.getString("unit_transp"));
                    costos.add(standard);
                    
                    
                }
                
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS COSTOS" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    public void searchMaxCostos(String ruta, String sj )throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("select max(case when unit_cost=0 then  cost else unit_cost end )as valor ,unit_transp from  stdjobcostfull where ft_code like ? and sj =? group by unit_transp ");
                st.setString(1,ruta+"%");
                st.setString(2,sj);
                rs = st.executeQuery();
                if(rs.next()){
                    standard = new Stdjobcosto();
                    standard.setMax_valor(rs.getFloat("valor"));
                    standard.setMax_unidad(rs.getString("unit_transp"));
                    
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LOS COSTOS" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    
    
    
    
    public boolean estaRuta(String ruta, String sj )throws SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        ruta=ruta+"%";
        boolean sw=false;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            if(con!=null){
                st = con.prepareStatement("select * from stdjobcostfull where sj=? and ft_code like ?");
                st.setString(1,sj);
                st.setString(2,ruta);
                rs = st.executeQuery();
                if(rs.next()){
                    sw=true;
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LAS RUTAS" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
        return sw;
    }
    
    /**
     * Getter for property standard.
     * @return Value of property standard.
     */
    public com.tsp.operation.model.beans.Stdjobcosto getStandard() {
        return standard;
    }
    
    /**
     * Setter for property standard.
     * @param standard New value of property standard.
     */
    public void setStandard(com.tsp.operation.model.beans.Stdjobcosto standard) {
        this.standard = standard;
    }
    
    /**
     * Getter for property costos.
     * @return Value of property costos.
     */
    public java.util.List getCostos() {
        return costos;
    }
    
    /**
     * Setter for property costos.
     * @param costos New value of property costos.
     */
    public void setCostos(java.util.List costos) {
        this.costos = costos;
    }
    
}





