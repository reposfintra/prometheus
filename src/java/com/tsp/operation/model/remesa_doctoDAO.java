/*
 * remesa_doctoDAO.java
 *
 * Created on 12 de septiembre de 2005, 10:57 AM
 */

package com.tsp.operation.model;

import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.MainDAO;
import java.io.*;
import java.sql.*;
import java.util.*;
import com.tsp.util.connectionpool.*;
import com.tsp.util.*;
/**
 *
 * @author  Administrador
 */
public class remesa_doctoDAO extends MainDAO{
    
    public Vector datos;
    public Vector documentos;
    private Vector destinatariosdoc;
    
    /*public static String SQLINSERTREL    =    "   INSERT INTO REMESA_DOCTO ( DSTRCT, NUMREM, TIPO_DOC, DOCUMENTO, TIPO_DOC_REL, DOCUMENTO_REL, DESTINATARIO, CREATION_USER ) "+
    "   VALUES ( ?, ?, ?, ?, ?, ?, ?, ? )    ";*/
    /////////////////////ORDEN DE CARGA
    
    
    public static String SQLDeleteDoc =     " delete from remesa_docto where numrem = ?                     "+
    " and tipo_doc = ?     and documento = ?                        "+
    " and tipo_doc_rel = ? and documento_rel = ?                    "+
    " and destinatario = ? and dstrct = ?                           ";
    
    public static String SQLDOC    =    "   SELECT REG_STATUS, NUMREM, TIPO_DOC, DOCUMENTO, TIPO_DOC_REL, DOCUMENTO_REL, DESTINATARIO   "+
    "   FROM REMESA_DOCTO                                                                           "+
    "   WHERE NUMREM = ?    AND  DESTINATARIO = ?                                                   "+
    "   ORDER BY TIPO_DOC, DOCUMENTO                                                                ";
    
    
    public static String SQLTLBDOC  = " SELECT a.table_code as DOCUMENT_TYPE , b.descripcion as DOCUMENT_NAME, b.dato FROM tablagen_prog a      "+
    " inner JOIN tablagen b on (  b.table_code = a.table_code AND b.table_type = a.table_type )        "+
    " WHERE    a.table_type = 'TDOC'  AND a.program = 'DESPACHO'                                       "+
    " ORDER BY a.table_code ";
    
    public static String SQL_INSERT_IMPO_EXPO    =    "   INSERT INTO impo_expo ( DSTRCT,  TIPO_DOC, DOCUMENTO,CREATION_USER,fecha_sia,fecha_eta,descripcion ,codcli ) "+
    "   VALUES ( ?, ?, ?, ?, ?, ?, ?, ? )    ";
    public static String SQL_INSERT_TBLREL   =    "   INSERT INTO tbl_relacion ( DSTRCT,  TIPO_DOC, DOCUMENTO,tipo_doc_rel,documento_rel,CREATION_USER) "+
    "   VALUES ( ?, ?, ?, ?, ?, ? )    ";
    public static String SQL_BUSCAR_IMPO_EXPO=   " SELECT * FROM IMPO_EXPO WHERE dstrct=? and tipo_doc=? and  documento=?";
    public static String SQL_UPDATE_IMPO_EXPO    =    "UPDATE impo_expo SET user_update=?,fecha_sia=?,fecha_eta=?,descripcion =? WHERE  dstrct=? and tipo_doc=? and  documento=?";
    
    
    public static String SQLSEARCH_OC =     "  SELECT * FROM ocargue_docto WHERE ORDEN = ? AND DESTINATARIO = ?                 "+
    "  AND TIPO_DOC = ? AND DOCUMENTO = ? AND TIPO_DOC_REL = ? AND DOCUMENTO_REL = ?    ";
    public static String SQLINSERTREL_OC    =    "   INSERT INTO ocargue_docto ( DSTRCT, ORDEN, TIPO_DOC, DOCUMENTO, TIPO_DOC_REL, DOCUMENTO_REL, DESTINATARIO, CREATION_USER ) "+
    "   VALUES ( ?, ?, ?, ?, ?, ?, ?, ? )    ";
    
    public static String SQL_VECOC =   "   SELECT REG_STATUS, orden , TIPO_DOC, DOCUMENTO, TIPO_DOC_REL, DOCUMENTO_REL, DESTINATARIO   "+
    "   FROM ocargue_docto                                                                           "+
    "   WHERE orden  = ?                                                    "+
    "   ORDER BY TIPO_DOC, DOCUMENTO                                                                ";
    private static String SQ_DELDOC_OCARGUE = "  DELETE FROM ocargue_docto WHERE orden = ? AND DESTINATARIO = ?          ";
    
    private static final String SQL_DOCUMENTOS_REMESA=  " Select r.documento," +
    "   get_destablagen( r.tipo_doc, 'TDOC' ) AS nombre_documento" +
    "   FROM  remesa_docto r  " +
    "   WHERE r.dstrct = ? " +
    "   AND r.numrem = ?  " +
    "   AND r.destinatario = ''";
    
    
    private static final String SQL_DESTINATARIO_DOC = "SELECT rd.codigo, " +
    "            COALESCE (r.documento,' ') AS documento, " +
    "            COALESCE (r.documento_rel,' ') AS documento_rel, " +
    "            COALESCE (get_nombreciudad(substring(rd.codigo from 5 for 2)),' ') as nomciu, " +
    "            COALESCE (d.nombre,' ')  as nomdes, " +
    "            COALESCE (get_destablagen( r.tipo_doc, 'TDOC' ), ' ') as nombre_documento, " +
    "            COALESCE (get_destablagen( r.tipo_doc_rel, 'TDOC' ) ,' ') as nombre_documento_rel " +
    "           FROM 	remesadest rd  " +
    "            LEFT JOIN remesa_docto r on (r.dstrct = ? and r.numrem =rd.numrem and r.destinatario = rd.codigo) " +
    "            LEFT JOIN remidest d ON ( d.codigo = rd.codigo )  " +
    "           WHERE 	rd.numrem = ?  " +
    "          Order by nomciu, d.nombre";
    
    public static String SQLINSERT    =       "   INSERT INTO REMESA_DOCTO ( DSTRCT, NUMREM, TIPO_DOC, DOCUMENTO, CREATION_USER ) "+
    "   VALUES ( ?, ?, ?, ?, ? )    ";
    
    
    public static String SQLLIST  =     "   SELECT REG_STATUS, NUMREM, TIPO_DOC, DOCUMENTO, TIPO_DOC_REL, DOCUMENTO_REL, DESTINATARIO FROM REMESA_DOCTO WHERE NUMREM = ?  " +
    "   AND DESTINATARIO = ''   ORDER BY TIPO_DOC                           ";
    
    public static String SLQUPDATEREL =     "   UPDATE REMESA_DOCTO SET TIPO_DOC = ? , DOCUMENTO  = ? , TIPO_DOC_REL = ?,  DOCUMENTO_REL = ?, USER_UPDATE = ?   "+
    "   WHERE NUMREM = ? AND TIPO_DOC = ? AND DOCUMENTO = ? AND TIPO_DOC_REL = ? AND DOCUMENTO_REL = ?               ";
    
    
    public static String SLQUPDATE =        "   UPDATE REMESA_DOCTO SET DOCUMENTO  = ? , TIPO_DOC = ?, USER_UPDATE = ?   WHERE NUMREM = ? AND TIPO_DOC = ? AND DOCUMENTO = ? ";
    
    
    /*LISTA DE LOS OBJETOS FORMADOS EN  PAREJA PADRE E HIJOS TIPODODPAPA:DOCPAPA[TIPODOCHIJO:DOCHIJO;]/*/
    
    public static String SQLLISTSTRING = "  SELECT * FROM REMESA_DOCTO WHERE TIPO_DOC_REL <> '' AND DOCUMENTO_REL <> '' AND numrem=? AND destinatario=? ORDER BY TIPO_DOC, DOCUMENTO ";
    
    public static String SQLSEARCH =     "  SELECT * FROM REMESA_DOCTO WHERE NUMREM = ? AND DESTINATARIO = ?                 "+
    "  AND TIPO_DOC = ? AND DOCUMENTO = ? AND TIPO_DOC_REL = ? AND DOCUMENTO_REL = ?    ";
    
    
    public static String SQLSEARCH2 =    "  SELECT * FROM REMESA_DOCTO WHERE NUMREM = ? AND TIPO_DOC = ? AND DOCUMENTO = ? ";
    
    public static String BUSCAR_DOC_GENERALES=    "  SELECT * FROM REMESA_DOCTO WHERE NUMREM = ? AND TIPO_DOC = ? AND DOCUMENTO = ? ";
    
    
    
    
    //JUAN 03.11.05
    public static String SQLINSERTREL    =    "   INSERT INTO REMESA_DOCTO ( DSTRCT, NUMREM, TIPO_DOC, DOCUMENTO, TIPO_DOC_REL, DOCUMENTO_REL, DESTINATARIO, CREATION_USER ) "+
    "   VALUES ( ?, ?, ?, ?, ?, ?, ?, ? )    ";
    private static String SQDEL = "  DELETE FROM REMESA_DOCTO WHERE NUMREM = ? AND DESTINATARIO = ?          ";
    
    
    public static String SQLLISTVEC =   "   SELECT REG_STATUS, NUMREM, TIPO_DOC, DOCUMENTO, TIPO_DOC_REL, DOCUMENTO_REL, DESTINATARIO   "+
    "   FROM REMESA_DOCTO                                                                           "+
    "   WHERE NUMREM = ?                                                   "+
    "   ORDER BY TIPO_DOC, DOCUMENTO                                                                ";
    
    
    /** Creates a new instance of remesa_doctoDAO */
    public remesa_doctoDAO() {
        super("Remesa_DoctoDAO.xml");
    }
    
    /* INSERT DOCUMENTOS RELACIONADOS*/
    public String INSERT_REMDOC(String distrito, String numrem, String tipodoc, String documento, String tipodocrel, String documentorel, String destinatario, String usuario) throws SQLException {
        PreparedStatement st          = null;
        PoolManager       poolManager = PoolManager.getInstance();
        Connection        conPostgres = poolManager.getConnection("fintra");
        String sql="";
        
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        try {
            st = conPostgres.prepareStatement(this.SQLINSERTREL);
            st.setString(1, distrito);
            st.setString(2, numrem);
            st.setString(3, tipodoc);
            st.setString(4, documento);
            st.setString(5, tipodocrel);
            st.setString(6, documentorel);
            st.setString(7, destinatario);
            st.setString(8, usuario);
            //st.executeUpdate();
            sql = st.toString()+";";
        }
        catch(SQLException e) {
            throw new SQLException("Error en rutina INSERT_REMDOC [remesa_doctoDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null)  st.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
        return sql;
    }
    // Juan MOdificado 03.11.05
    public void INSERT_REMDOC2(String distrito, String numrem, String tipodoc, String documento, String tipodocrel, String documentorel, String destinatario, String usuario) throws SQLException {
        PreparedStatement st          = null;
        PoolManager       poolManager = PoolManager.getInstance();
        Connection        conPostgres = poolManager.getConnection("fintra");
        
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        try {
            st = conPostgres.prepareStatement(this.SQLINSERTREL);
            st.setString(1, distrito);
            st.setString(2, numrem);
            st.setString(3, tipodoc);
            st.setString(4, documento);
            st.setString(5, tipodocrel);
            st.setString(6, documentorel);
            st.setString(7, destinatario);
            st.setString(8, usuario);
            ////System.out.println("CONSULTA INSERT REL " + st.toString());
            st.executeUpdate();
            
        }
        catch(SQLException e) {
            throw new SQLException("Error en rutina INSERT_REMDOC2 [remesa_doctoDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null)  st.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
    }
    /*public void INSERT_REMDOC2(String distrito, String numrem, String tipodoc, String documento, String tipodocrel, String documentorel, String destinatario, String usuario) throws SQLException {
        PreparedStatement st          = null;
        PoolManager       poolManager = PoolManager.getInstance();
        Connection        conPostgres = poolManager.getConnection("fintra");
     
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        try {
            st = conPostgres.prepareStatement(this.SQLINSERTREL);
            st.setString(1, distrito);
            st.setString(2, numrem);
            st.setString(3, tipodoc);
            st.setString(4, documento);
            st.setString(5, tipodocrel);
            st.setString(6, documentorel);
            st.setString(7, destinatario);
            st.setString(8, usuario);
            ////System.out.println("CONSULTA INSERT REL " + st.toString());
            st.executeUpdate();
     
        }
        catch(SQLException e) {
            throw new SQLException("Error en rutina INSERT_REMDOC2 [remesa_doctoDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null)  st.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
    }*/
    
    public String INSERT(String distrito, String numrem, String tipodoc, String documento, String usuario) throws SQLException {
        PreparedStatement st          = null;
        PoolManager       poolManager = PoolManager.getInstance();
        Connection        conPostgres = poolManager.getConnection("fintra");
        String sql="";
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        try {
            st = conPostgres.prepareStatement(this.SQLINSERT);
            st.setString(1, distrito);
            st.setString(2, numrem);
            st.setString(3, tipodoc);
            st.setString(4, documento);
            st.setString(5, usuario);
            sql = st.toString()+";";
            //st.executeUpdate();
            
        }
        catch(SQLException e) {
            throw new SQLException("Error en rutina INSERT [remesa_doctoDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null)  st.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
        return sql;
    }
    public void INSERTMAS(String distrito, String numrem, String tipodoc, String documento, String usuario) throws SQLException {
        PreparedStatement st          = null;
        PoolManager       poolManager = PoolManager.getInstance();
        Connection        conPostgres = poolManager.getConnection("fintra");
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        try {
            st = conPostgres.prepareStatement(this.SQLINSERT);
            st.setString(1, distrito);
            st.setString(2, numrem);
            st.setString(3, tipodoc);
            st.setString(4, documento);
            st.setString(5, usuario);
            st.executeUpdate();
            
        }
        catch(SQLException e) {
            throw new SQLException("Error en rutina INSERT [remesa_doctoDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null)  st.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
    }
    
    
    /*LIST */
    public List LIST(String numrem) throws Exception{
        PoolManager poolManager = PoolManager.getInstance();
        Connection con = poolManager.getConnection("fintra");
        if (con == null)
            throw new SQLException("Sin conexion");
        PreparedStatement st = null;
        ResultSet rs         = null;
        List lista           = new LinkedList();
        try{
            st = con.prepareStatement(this.SQLLIST);
            st.setString(1, numrem);
            rs=st.executeQuery();
            if(rs!=null){
                while(rs.next()){
                    lista.add(remesa_docto.load(rs));
                }
            }
        }catch(Exception e){
            throw new SQLException("Error en rutina LIST [remesa_doctoDAO].... \n"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            poolManager.freeConnection("fintra",con);
        }
        return lista;
    }
    
    /*LIST */
    public List LISTTLBDOC() throws Exception{
        PoolManager poolManager = PoolManager.getInstance();
        Connection con = poolManager.getConnection("fintra");
        if (con == null)
            throw new SQLException("Sin conexion");
        PreparedStatement st = null;
        ResultSet rs         = null;
        List lista           = new LinkedList();
        try{
            st = con.prepareStatement(this.SQLTLBDOC);
            rs=st.executeQuery();
            if(rs!=null){
                while(rs.next()){
                    lista.add(remesa_docto.loadTBLDOC(rs));
                }
            }
        }catch(Exception e){
            throw new SQLException("Error en rutina LISTTLBDOC [remesa_doctoDAO].... \n"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            poolManager.freeConnection("fintra",con);
        }
        return lista;
    }
    
    /* MODIFICAR */
    public void UPDATE_REMDOC(String ndocumento, String ndocumentorel, String usuario , String numrem, String documento, String tipodoc , String documentorel, String tipodocrel) throws SQLException {
        PreparedStatement st          = null;
        PoolManager       poolManager = PoolManager.getInstance();
        Connection        conPostgres = poolManager.getConnection("fintra");
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        try {
            st = conPostgres.prepareStatement(this.SLQUPDATE);
            st.setString(1, ndocumento);
            st.setString(2, ndocumentorel);
            st.setString(3, usuario);
            st.setString(4, numrem);
            st.setString(5, tipodoc);
            st.setString(6, documento);
            st.setString(7, tipodocrel);
            st.setString(8, documentorel);
            
            st.executeUpdate();
        }
        catch(SQLException e) {
            throw new SQLException("Error en rutina UPDATE_REMDOC [remesa_doctoDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null)  st.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
    }
    
    
    public void UPDATE(String ndocumento, String ntipodoc, String usuario , String numrem, String documento, String tipodoc ) throws SQLException {
        PreparedStatement st          = null;
        PoolManager       poolManager = PoolManager.getInstance();
        Connection        conPostgres = poolManager.getConnection("fintra");
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        try {
            st = conPostgres.prepareStatement(this.SLQUPDATE);
            st.setString(1, ndocumento);
            st.setString(2, ntipodoc);
            st.setString(3, usuario);
            st.setString(4, numrem);
            st.setString(5, tipodoc);
            st.setString(6, documento);
            st.executeUpdate();
        }
        catch(SQLException e) {
            throw new SQLException("Error en rutina UPDATE [remesa_doctoDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null)  st.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
    }
    
    
    public Vector LISTVector(String numrem) throws Exception{
        PoolManager poolManager = PoolManager.getInstance();
        Connection con = poolManager.getConnection("fintra");
        if (con == null)
            throw new SQLException("Sin conexion");
        PreparedStatement st = null;
        ResultSet rs         = null;
        Vector vec = new Vector();
        try{
            st = con.prepareStatement(this.SQLLIST);
            st.setString(1, numrem);
            rs=st.executeQuery();
            if(rs!=null){
                while(rs.next()){
                    vec.addElement(remesa_docto.load(rs));
                }
            }
        }catch(Exception e){
            throw new SQLException("Error en rutina LISTVector [remesa_doctoDAO].... \n"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            poolManager.freeConnection("fintra",con);
        }
        return vec;
    }
    
    public String LISTSTRING(String numrem, String dest) throws Exception{
        PoolManager poolManager = PoolManager.getInstance();
        Connection con = poolManager.getConnection("fintra");
        if (con == null)
            throw new SQLException("Sin conexion");
        PreparedStatement st = null;
        ResultSet rs         = null;
        List lista           = new LinkedList();
        try{
            st = con.prepareStatement(this.SQLLISTSTRING);
            st.setString(1, numrem);
            st.setString(2, dest);
            rs=st.executeQuery();
            StringBuffer sb = new StringBuffer();
            String padreAnterior = null;
            if(rs!=null){
                while(rs.next()){
                    String aux = rs.getString("tipo_doc")+":"+rs.getString("documento");
                    if ( padreAnterior == null || !padreAnterior.equals(aux)){
                        if ( padreAnterior != null ){
                            sb.append("]");
                            sb.append("/");
                        }
                        sb.append(aux);
                    }
                    else {
                        sb.append(";");
                    }
                    
                    // hijos
                    if ( padreAnterior == null || !padreAnterior.equals(aux) ){
                        sb.append("[");
                    }
                    sb.append(rs.getString("tipo_doc_rel"));
                    sb.append(":");
                    sb.append(rs.getString("documento_rel"));
                    padreAnterior = aux;
                }
                sb.append("]");
            }
            return sb.toString();
        }catch(Exception e){
            throw new SQLException("Error en rutina LISTTLBDOC [remesa_doctoDAO].... \n"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            poolManager.freeConnection("fintra",con);
        }
    }
    
    
    public String LISTSTRINGDOC(String numrem) throws Exception{
        PoolManager poolManager = PoolManager.getInstance();
        Connection con = poolManager.getConnection("fintra");
        if (con == null)
            throw new SQLException("Sin conexion");
        PreparedStatement st = null;
        ResultSet rs         = null;
        List lista           = new LinkedList();
        try{
            st = con.prepareStatement(this.SQLLIST);
            st.setString(1, numrem);
            rs=st.executeQuery();
            StringBuffer sb = new StringBuffer();
            if(rs!=null){
                while(rs.next()){
                    String aux = rs.getString("tipo_doc")+":"+rs.getString("documento")+";";
                    sb.append(aux);
                }
            }
            return sb.toString();
        }catch(Exception e){
            throw new SQLException("Error en rutina LISTTLBDOC [remesa_doctoDAO].... \n"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            poolManager.freeConnection("fintra",con);
        }
    }
    
    public void UPDATEREL(String ntd, String ndoc, String ntdr, String ndr , String user, String numrem, String td, String d, String tdr, String dr ) throws SQLException {
        PreparedStatement st          = null;
        PoolManager       poolManager = PoolManager.getInstance();
        Connection        conPostgres = poolManager.getConnection("fintra");
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        try {
            st = conPostgres.prepareStatement(this.SLQUPDATEREL);
            st.setString(1, ntd);
            st.setString(2, ndoc);
            st.setString(3, ntdr);
            st.setString(4, ndr);
            st.setString(5, user);
            st.setString(6, numrem);
            st.setString(7, td);
            st.setString(8, d);
            st.setString(9, tdr);
            st.setString(10, dr);
            ////System.out.println("CONSULTA UPDATE " + st);
            st.executeUpdate();
        }
        catch(SQLException e) {
            throw new SQLException("Error en rutina UPDATEREL [remesa_doctoDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null)  st.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
    }
    
    public boolean BUSCAR_RD(String numrem, String destinatario, String td, String doc, String tdr, String docr ) throws SQLException {
        PreparedStatement st = null;
        ResultSet rs         = null;
        boolean flag         = false;
        
        PoolManager poolManager = PoolManager.getInstance();
        Connection conPostgres  = poolManager.getConnection("fintra");
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        try {
            st = conPostgres.prepareStatement(this.SQLSEARCH);
            st.setString(1, numrem);
            st.setString(2, destinatario);
            st.setString(3, td);
            st.setString(4, doc);
            st.setString(5, tdr);
            st.setString(6, docr);
            ////System.out.println("BUSCAR  " + st);
            rs = st.executeQuery();
            while(rs.next()){
                flag = true;
                break;
            }
        }
        catch(Exception e) {
            throw new SQLException("Error en rutina BUSCAR_RD [remesa_doctoDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null)  st.close();
            if(rs!=null)  rs.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
        return flag;
    }
    
    public boolean BUSCAR(String numrem, String td, String doc ) throws SQLException {
        PreparedStatement st = null;
        ResultSet rs         = null;
        boolean flag         = false;
        
        PoolManager poolManager = PoolManager.getInstance();
        Connection conPostgres  = poolManager.getConnection("fintra");
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        try {
            st = conPostgres.prepareStatement(this.SQLSEARCH2);
            st.setString(1, numrem);
            st.setString(2, td);
            st.setString(3, doc);
            rs = st.executeQuery();
            while(rs.next()){
                flag = true;
                break;
            }
        }
        catch(Exception e) {
            throw new SQLException("Error en rutina BUSCAR [remesa_doctoDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null)  st.close();
            if(rs!=null)  rs.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
        return flag;
    }
    //Rodrigo
    public List Listar(String numrem) throws Exception{
        ////System.out.println("LISTDAO"+numrem);
        PoolManager poolManager = PoolManager.getInstance();
        Connection con = poolManager.getConnection("fintra");
        remesa_docto rd = new remesa_docto();
        if (con == null)
            throw new SQLException("Sin conexion");
        PreparedStatement st = null;
        ResultSet rs         = null;
        List lista           = new LinkedList();
        try{
            st = con.prepareStatement("SELECT r.tipo_doc,r.documento,t.document_name,r.tipo_doc_rel,r.documento_rel,x.document_name as xrel FROM remesa_docto r left outer join tbldoc t on (t.document_type=r.tipo_doc) left outer join tbldoc x on (x.document_type=r.tipo_doc_rel) where r.numrem=? and r.reg_status != 'A' ");
            st.setString(1, numrem);
            ////System.out.println("CONSULTA="+st);
            rs=st.executeQuery();
            if(rs!=null){
                while(rs.next()){
                    rd = new remesa_docto();
                    rd.setTipo_doc(rs.getString("tipo_doc"));
                    rd.setDocumento(rs.getString("documento"));
                    rd.setTipo_doc_rel(rs.getString("tipo_doc_rel"));
                    rd.setDocumento_rel(rs.getString("documento_rel"));
                    rd.setDocument_type(rs.getString("xrel"));
                    rd.setDocument_name(rs.getString("document_name"));
                    ////System.out.println(rd.getDocument_name());
                    lista.add(rd);
                }
            }
        }catch(Exception e){
            throw new SQLException("Error en rutina LIST [remesa_doctoDAO].... \n"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            poolManager.freeConnection("fintra",con);
        }
        return lista;
    }
    
    /**
     * Getter for property documentos.
     * @return Value of property documentos.
     */
    public java.util.Vector getDocumentos() {
        return documentos;
    }
    
    /**
     * Setter for property documentos.
     * @param documentos New value of property documentos.
     */
    public void setDocumentos(java.util.Vector documentos) {
        this.documentos = documentos;
    }
    
    /**
     *Metodo que llena un vector de documentos relacionados a una remesa para migrarlos
     *@autor: Karen Reales
     *@param: numrem Numero de la remesa
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void MigrarDocumentos(String numrem) throws SQLException{
        PoolManager poolManager = PoolManager.getInstance();
        remesa_docto rd = new remesa_docto();
        PreparedStatement st = null;
        ResultSet rs         = null;
        documentos = null;
        try{
            int i = 0;
            st = crearPreparedStatement("DOCUMENTOS_GENERALES");
            st.setString(1, numrem);
            ////System.out.println("CONSULTA="+st);
            rs=st.executeQuery();
            documentos = new Vector();
            while(rs.next()){
                rd = new remesa_docto();
                rd.setTipo_doc(rs.getString("tipo_doc"));
                rd.setDocumento(rs.getString("documento"));
                rd.setDestinatario("");
                rd.setDocumento_rel("");
                documentos.add(rd);
            }
            
            st = crearPreparedStatement("DOCUMENTOS_DESTINATARIO_NO_REL");
            st.setString(1, numrem);
            ////System.out.println("CONSULTA="+st);
            rs=st.executeQuery();
            while(rs.next()){
                rd = new remesa_docto();
                rd.setTipo_doc(rs.getString("tipo_doc"));
                rd.setDocumento(rs.getString("documento"));
                rd.setDestinatario(rs.getString("destinatario"));
                rd.setDocumento_rel("");
                rd.setDestinatario("");
                documentos.add(rd);
            }
            
            st = crearPreparedStatement("DOCUMENTOS_DESTINATARIO_ELACIONADOS");
            st.setString(1, numrem);
            ////System.out.println("CONSULTA="+st);
            rs=st.executeQuery();
            while(rs.next()){
                rd = new remesa_docto();
                rd.setTipo_doc(rs.getString("tipo_doc"));
                rd.setDocumento(rs.getString("documento"));
                rd.setTipo_doc_rel(rs.getString("tipo_doc_rel"));
                rd.setDocumento_rel(rs.getString("documento_rel"));
                rd.setDestinatario(rs.getString("destinatario"));
                documentos.add(rd);
            }
            
        } catch (SQLException e){
            throw new SQLException("Error en rutina LIST [remesa_doctoDAO].... \n"+ e.getMessage());
        }
        finally {
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar("DOCUMENTOS_GENERALES");
            this.desconectar("DOCUMENTOS_DESTINATARIO_NO_REL");
            this.desconectar("DOCUMENTOS_DESTINATARIO_ELACIONADOS");
        }
    }
    
    //JUAN 03.11.05
    public void DELETEREL(String numrem, String destinatario) throws SQLException {
        PreparedStatement st          = null;
        PoolManager       poolManager = PoolManager.getInstance();
        Connection        conPostgres = poolManager.getConnection("fintra");
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        try {
            st = conPostgres.prepareStatement(this.SQDEL);
            st.setString(1,  numrem);
            st.setString(2,  destinatario);
            ////System.out.println("DELETE " + st);
            st.executeUpdate();
        }
        catch(SQLException e) {
            throw new SQLException("Error en rutina DELETEREL [remesa_doctoDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null)  st.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
    }
    
    public Vector LISTVC(String numrem, String destinatario) throws Exception{
        PoolManager poolManager = PoolManager.getInstance();
        Connection con = poolManager.getConnection("fintra");
        if (con == null)
            throw new SQLException("Sin conexion");
        PreparedStatement st = null;
        ResultSet rs         = null;
        String docpapa = "";
        String tdpapa  = "";
        Vector vec = new Vector();
        Vector hijos = null;
        remesa_docto rem = new remesa_docto();
        remesa_docto remh = new remesa_docto();
        
        try{
            st = con.prepareStatement(this.SQLLISTVEC);
            st.setString(1, numrem);
            //st.setString(2, destinatario);
            ////System.out.println("CONSULTA VECTOR " + st );
            rs=st.executeQuery();
            if(rs!=null){
                String papaAnterior = "";
                while(rs.next()){
                    String doc = rs.getString("DOCUMENTO");
                    String td = rs.getString("TIPO_DOC");
                    if( doc!= null && td != null ){
                        remesa_docto papa = new remesa_docto();
                        remesa_docto hijo = new remesa_docto();
                        papa.setTipo_doc(td);
                        papa.setDocumento(doc);
                        papa.setDestinatario(rs.getString("destinatario"));
                        hijo.setTipo_doc(rs.getString("TIPO_DOC_REL"));
                        hijo.setDocumento(rs.getString("DOCUMENTO_REL"));
                        if ( papaAnterior.equals(doc+td) ){
                            remesa_docto papaVector = (remesa_docto) vec.lastElement();
                            papaVector.agregarHijo(hijo);
                        }
                        else {
                            vec.addElement(papa);
                            papa.agregarHijo(hijo);
                        }
                        papaAnterior = doc+td;
                    }
                }
                
            }
        }catch(Exception e){
            throw new SQLException("Error en rutina LISTVC [remesa_doctoDAO].... \n"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            poolManager.freeConnection("fintra",con);
        }
        
        return vec;
    }
    
    
    public void searchRemesaDoc(Vector planillas) throws SQLException {
        datos = new Vector();
        String pla;
        for (int i=0; i<planillas.size(); i++) {
            pla = (String) planillas.elementAt(i);
            datos.addElement(getDatosReporteRemesaDoc(pla));
        }
    }
    public Vector getVectorReporteRemPla() {
        return datos;
    }
    //Henry
    public Remesa getDatosReporteRemesaDoc(String numpla) throws SQLException {
        int num = 0;
        PreparedStatement st = null;
        ResultSet rs = null;
        String rem = "";
        Remesa r = new Remesa();
        try{
            st = crearPreparedStatement("SQL_REM_DOC");
            st.setString(1, numpla);
            rs = st.executeQuery();
            while(rs.next()) {
                r = new Remesa();
                r.setNumpla(rs.getString("numpla"));
                rem = rs.getString("numrem");
                r.setNumrem(rem);
                r.setReg_status(rs.getString("reg_status"));
                r.setPesoReal(rs.getFloat("pesoreal"));
                r.setPorcentaje(rs.getFloat("porcent"));
                r.setStd_job_no(rs.getString("std_job_no"));
                //obtenemos los documentos internos y las fact. comerciales de una rem
                HExport_MST620 hexport = new HExport_MST620();
                this.MigrarDocumentos(rem);
                Vector  documentos = this.getDocumentos();
                String docs="";
                String facts="";
                int catDocs =0;
                int catFacts =0;
                
                for (int j=0; j<documentos.size();j++){
                    remesa_docto rd = (remesa_docto) documentos.elementAt(j) ;
                    if(rd.getTipo_doc().equals("008")){
                        catDocs++;
                        if(catDocs<=15)
                            docs  = docs + hexport.fill(rd.getDestinatario().equals("")?rd.getDocumento():rd.getDocumento()+":"+rd.getDestinatario(),25,0);
                    }
                    else if(rd.getTipo_doc().equals("009")){
                        catFacts++;
                        if(catFacts<=15)
                            facts  = facts + hexport.fill(rd.getDocumento(),15,0);
                    }
                }
                r.setDocInterno(docs);
                r.setFaccial(facts);
            }
            return r;
        }catch(SQLException e){
            throw new SQLException("ERROR AL BUSCAR REMESAS PLANILLA" + e.getMessage()+"" + e.getErrorCode());
        }finally{
            if(st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR AL CERRAR EL ESTAMENTO" + e.getMessage() );
                }
            }
            this.desconectar("SQL_REM_DOC");
        }
    }
    
    
    
    /**
     * Getter for property destinatariosdoc.
     * @return Value of property destinatariosdoc.
     */
    public java.util.Vector getDestinatariosdoc() {
        return destinatariosdoc;
    }
    /**
     * UpdateTieneDoc Actualiza el campo si tiene documentos la planilla, de acuerdo a el despacho realizado
     * @autor: Ing. Juan Escandon
     * @params String numrem - Numero de la remesa
     * @throws SQLException.
     */
    public void UpdateTieneDoc( String numrem ) throws SQLException {
        PreparedStatement st          = null;
        try {
            st = crearPreparedStatement("UpdateTieneDoc");
            st.setString(1, numrem);
            
            ////System.out.println("UPDATE " + st.toString() );
            st.executeUpdate();
        }
        catch(SQLException e) {
            throw new SQLException("Error en rutina UpdateTieneDoc [remesa_doctoDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null)  st.close();
            this.desconectar("UpdateTieneDoc");
        }
    }
    //////////////////////////////////ORDEN DE CARGUE///////////////////////////////////////////
    
    //////////////////////////////////ORDEN DE CARGUE///////////////////////////////////////////
    /**
     * buscarDocumentOC busca un documento especifico relacionado a una orden de cargue
     * @autor: Ing.Karen Reales
     * @params numrem : Numero de la remesa
     * @params destinatario: Codigo del destinatario
     * @params td: Tipo de documento
     * @params doc: Documento
     * @params tdr: Tipo de documento relacinado
     * @params docr: Documento relacionado
     * @return true o false si existe o no el documento
     * @throws SQLException.
     */
    public boolean buscarDocumentOC(String numrem, String destinatario, String td, String doc, String tdr, String docr ) throws SQLException {
        PreparedStatement st = null;
        ResultSet rs         = null;
        boolean flag         = false;
        
        PoolManager poolManager = PoolManager.getInstance();
        Connection conPostgres  = poolManager.getConnection("fintra");
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        try {
            st = conPostgres.prepareStatement(this.SQLSEARCH_OC);
            st.setString(1, numrem);
            st.setString(2, destinatario);
            st.setString(3, td);
            st.setString(4, doc);
            st.setString(5, tdr);
            st.setString(6, docr);
            ////System.out.println("BUSCAR  " + st);
            rs = st.executeQuery();
            while(rs.next()){
                flag = true;
                break;
            }
        }
        catch(Exception e) {
            throw new SQLException("Error en rutina BUSCAR_RD [remesa_doctoDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null)  st.close();
            if(rs!=null)  rs.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
        return flag;
    }
    /**
     * insertOCargue_docto inserta un vector de documentos relacionados a la orden de cargue
     * @autor: Ing.Karen Reales
     * @params documentos : Vector con objetos de tipo Remesa_docto
     * @params destinatario: Codigo del destinatario
     * @params distrito: Distrito del usuario
     * @params numrem: Numero de la remesa
     * @params usuario: Login del usuario
     * @return String con comando SQL
     * @throws SQLException.
     */
    public String insertOCDocument(String distrito, String numrem, String tipodoc, String documento, String tipodocrel, String documentorel, String destinatario, String usuario) throws SQLException {
        PreparedStatement st          = null;
        PoolManager       poolManager = PoolManager.getInstance();
        Connection        conPostgres = poolManager.getConnection("fintra");
        String sql="";
        
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        try {
            st = conPostgres.prepareStatement(this.SQLINSERTREL_OC);
            st.setString(1, distrito);
            st.setString(2, numrem);
            st.setString(3, tipodoc);
            st.setString(4, documento);
            st.setString(5, tipodocrel);
            st.setString(6, documentorel);
            st.setString(7, destinatario);
            st.setString(8, usuario);
            //st.executeUpdate();
            sql = st.toString()+";";
        }
        catch(SQLException e) {
            throw new SQLException("Error en rutina INSERT_REMDOC [remesa_doctoDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null)  st.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
        return sql;
    }
    /**
     * listarDocOCargue Lista los documentos relacionados a una orden de cargue
     * @autor: Ing.Karen Reales
     * @params ocargue : Numero de la orden de cargue
     * @params destinatario: Codigo del destinatario
     * @throws SQLException.
     */
    public Vector listaDocOCargue(String ocargue, String destinatario) throws Exception{
        PoolManager poolManager = PoolManager.getInstance();
        Connection con = poolManager.getConnection("fintra");
        if (con == null)
            throw new SQLException("Sin conexion");
        PreparedStatement st = null;
        ResultSet rs         = null;
        String docpapa = "";
        String tdpapa  = "";
        Vector vec = new Vector();
        Vector hijos = null;
        remesa_docto rem = new remesa_docto();
        remesa_docto remh = new remesa_docto();
        
        try{
            st = con.prepareStatement(this.SQL_VECOC);
            st.setString(1, ocargue);
            //  st.setString(2, destinatario);
            ////System.out.println("CONSULTA VECTOR " + st );
            rs=st.executeQuery();
            if(rs!=null){
                String papaAnterior = "";
                while(rs.next()){
                    String doc = rs.getString("DOCUMENTO");
                    String td = rs.getString("TIPO_DOC");
                    if( doc!= null && td != null ){
                        remesa_docto papa = new remesa_docto();
                        remesa_docto hijo = new remesa_docto();
                        papa.setTipo_doc(td);
                        papa.setDocumento(doc);
                        papa.setDestinatario(rs.getString("destinatario"));
                        hijo.setTipo_doc(rs.getString("TIPO_DOC_REL"));
                        hijo.setDocumento(rs.getString("DOCUMENTO_REL"));
                        if ( papaAnterior.equals(doc+td) ){
                            remesa_docto papaVector = (remesa_docto) vec.lastElement();
                            papaVector.agregarHijo(hijo);
                        }
                        else {
                            vec.addElement(papa);
                            papa.agregarHijo(hijo);
                        }
                        papaAnterior = doc+td;
                    }
                }
                
            }
        }catch(Exception e){
            throw new SQLException("Error en rutina listaDocOCargue [remesa_doctoDAO].... \n"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            poolManager.freeConnection("fintra",con);
        }
        
        return vec;
    }
    /**
     * DeleteDocOcargue Borra de la base de datos los documentos internos relacionados a una
     * orden de carga y un destinatario
     * @autor: Ing.Karen Reales
     * @params ocargue : Numero de la orden de cargue
     * @params destinatario: Codigo del destinatario
     * @throws SQLException.
     */
    public void DeleteDocsOcargue(String ocargue, String destinatario) throws SQLException {
        PreparedStatement st          = null;
        PoolManager       poolManager = PoolManager.getInstance();
        Connection        conPostgres = poolManager.getConnection("fintra");
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        try {
            st = conPostgres.prepareStatement(this.SQ_DELDOC_OCARGUE);
            st.setString(1,  ocargue);
            st.setString(2,  destinatario);
            ////System.out.println("DELETE " + st);
            st.executeUpdate();
        }
        catch(SQLException e) {
            throw new SQLException("Error en rutina DELETEREL [remesa_doctoDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null)  st.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
    }
    /**
     * insertImpoExpo inserta un vector de documentos relacionados a la orden de cargue
     * @autor: Ing.Karen Reales
     * @params documentos : Vector con objetos de tipo Remesa_docto
     * @params destinatario: Codigo del destinatario
     * @params distrito: Distrito del usuario
     * @params numrem: Numero de la remesa
     * @params usuario: Login del usuario
     * @return String con comando SQL
     * @throws SQLException.
     */
    public String insertImpoExpo(String distrito, String tipodoc, String documento,  String usuario,String fecha_sia,String fecha_eta,String descripcion ,String codcli) throws SQLException {
        
        PreparedStatement st          = null;
        PoolManager       poolManager = PoolManager.getInstance();
        Connection        conPostgres = poolManager.getConnection("fintra");
        String sql="";
        
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        try {
            st = conPostgres.prepareStatement(this.SQL_INSERT_IMPO_EXPO);
            st.setString(1, distrito);
            st.setString(2, tipodoc);
            st.setString(3, documento);
            st.setString(4, usuario);
            st.setString(5, fecha_sia);
            st.setString(6, fecha_eta);
            st.setString(7, descripcion);
            st.setString(8, codcli);
            //st.executeUpdate();
            sql = st.toString();
            
        }
        catch(SQLException e) {
            throw new SQLException("Error en rutina insertImpoExpo [remesa_doctoDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null)  st.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
        return sql;
    }
    /**
     * relacionImpoExpo inserta una relacion entre remesa e impoexpo
     * @autor: Ing.Karen Reales
     * @params documentos : Vector con objetos de tipo Remesa_docto
     * @params destinatario: Codigo del destinatario
     * @params distrito: Distrito del usuario
     * @params numrem: Numero de la remesa
     * @params usuario: Login del usuario
     * @return String con comando SQL
     * @throws SQLException.
     */
    public String relacionImpoExpo(String distrito, String numrem, String tipodoc, String documento,  String usuario) throws SQLException {
        PreparedStatement st          = null;
        PoolManager       poolManager = PoolManager.getInstance();
        Connection        conPostgres = poolManager.getConnection("fintra");
        String sql="";
        
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        try {
            
            st = conPostgres.prepareStatement(this.SQL_INSERT_TBLREL);
            st.setString(1, distrito);
            st.setString(2, tipodoc);
            st.setString(3, documento);
            st.setString(4, "002");
            st.setString(5, numrem);
            st.setString(6, usuario);
            sql =st.toString();
        }
        catch(SQLException e) {
            throw new SQLException("Error en rutina relacionImpoExpo [remesa_doctoDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null)  st.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
        return sql;
    }
    /**
     * buscarImpoExpo Metodo para buscar los datos de una importacion especifica
     * @autor: Ing.Karen Reales
     * @return String con comando SQL
     * @throws SQLException.
     */
    public remesa_docto buscarImpoExpo(String distrito,  String tipodoc, String documento) throws SQLException {
        
        PreparedStatement st          = null;
        PoolManager       poolManager = PoolManager.getInstance();
        Connection        conPostgres = poolManager.getConnection("fintra");
        remesa_docto rem=null;
        ResultSet rs         = null;
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        try {
            st = conPostgres.prepareStatement(this.SQL_BUSCAR_IMPO_EXPO);
            st.setString(1, distrito);
            st.setString(2, tipodoc);
            st.setString(3, documento);
            rs=st.executeQuery();
            //System.out.println("Query impo expo "+st);
            String papaAnterior = "";
            if(rs.next()){
                rem = new remesa_docto();
                rem.setTipo_doc(rs.getString("tipo_doc"));
                rem.setDocumento(rs.getString("documento"));
                rem.setFecha_sia(rs.getString("fecha_sia"));
                rem.setFecha_eta(rs.getString("fecha_eta"));
                rem.setDescripcion_cga(rs.getString("descripcion"));
            }
        }
        catch(SQLException e) {
            throw new SQLException("Error en rutina buscarImpoExpo [remesa_doctoDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null)  st.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
        return rem;
    }
    
    /**
     * borrarDocs Metodo para borrar la lista de documentos relacionados a una remesa que no esten
     * relacionados a un destinatario.
     * @autor: Ing.Karen Reales
     * @return String con comando SQL
     * @throws SQLException.
     */
    public String borrarDocs(String dstrct,String numrem) throws SQLException {
        PreparedStatement st          = null;
        PoolManager       poolManager = PoolManager.getInstance();
        Connection        conPostgres = poolManager.getConnection("fintra");
        String sql = "";
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        try {
            st = conPostgres.prepareStatement("delete from remesa_docto where dstrct=? and numrem=? and destinatario=''");
            st.setString(1,  dstrct);
            st.setString(2,  numrem);
            ////System.out.println("DELETE " + st);
            sql = sql + st.toString();
            
        }
        catch(SQLException e) {
            throw new SQLException("Error en rutina borrarDocs [remesa_doctoDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null)  st.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
        return sql;
    }
    /**
     * borrarRelacion Metodo para borrar la relacion entre remesa e importacion
     * relacionados a un destinatario.
     * @autor: Ing.Karen Reales
     * @return String con comando SQL
     * @throws SQLException.
     */
    public String borrarRelacion(String dstrct,String numrem) throws SQLException {
        PreparedStatement st          = null;
        PoolManager       poolManager = PoolManager.getInstance();
        Connection        conPostgres = poolManager.getConnection("fintra");
        String sql = "";
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        try {
            st = conPostgres.prepareStatement("delete from tbl_relacion where dstrct=?  and  tipo_doc_rel=? and documento_rel=?");
            st.setString(1,  dstrct);
            //st.setString(2,  tipodoc);
            //st.setString(3,  doc);
            st.setString(2,  "002");
            st.setString(3,  numrem);
            ////System.out.println("DELETE " + st);
            sql = sql + st.toString();
            
        }
        catch(SQLException e) {
            throw new SQLException("Error en rutina borrarRelacion [remesa_doctoDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null)  st.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
        return sql;
    }
    /**
     * updateImpoExpo inserta un vector de documentos relacionados a la orden de cargue
     * @autor: Ing.Karen Reales
     * @params documentos : Vector con objetos de tipo Remesa_docto
     * @params destinatario: Codigo del destinatario
     * @params distrito: Distrito del usuario
     * @params numrem: Numero de la remesa
     * @params usuario: Login del usuario
     * @return String con comando SQL
     * @throws SQLException.
     */
    public String updateImpoExpo(String distrito, String tipodoc, String documento,  String usuario,String fecha_sia,String fecha_eta,String descripcion) throws SQLException {
        
        PreparedStatement st          = null;
        PoolManager       poolManager = PoolManager.getInstance();
        Connection        conPostgres = poolManager.getConnection("fintra");
        String sql="";
        
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        try {
            st = conPostgres.prepareStatement(this.SQL_UPDATE_IMPO_EXPO);
            st.setString(1, usuario);
            st.setString(2, fecha_sia);
            st.setString(3, fecha_eta);
            st.setString(4, descripcion);
            st.setString(5, distrito);
            st.setString(6, tipodoc);
            st.setString(7, documento);
            //st.executeUpdate();
            sql = st.toString();
        }
        catch(SQLException e) {
            throw new SQLException("Error en rutina insertImpoExpo [remesa_doctoDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null)  st.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
        return sql;
    }
    
    
    
    
    
    public void deleteDocumentosDestinatarios(String numrem, String tipo, String documento, String tiporel, String docrel, String destinatario , String distrito )  throws SQLException {
        PreparedStatement st          = null;
        PoolManager       poolManager = PoolManager.getInstance();
        Connection        conPostgres = poolManager.getConnection("fintra");
        String sql = "";
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        try {
            st = conPostgres.prepareStatement(SQLDeleteDoc);
            st.setString(1,  numrem);
            st.setString(2,  tipo);
            st.setString(3,  documento);
            st.setString(4,  tiporel);
            st.setString(5,  docrel);
            st.setString(6,  destinatario);
            st.setString(7,  distrito);
            st.executeUpdate();
        }
        catch(SQLException e) {
            throw new SQLException("Error en rutina deleteDocs [remesa_doctoDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null)  st.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
    }
    
    
    
    
    
    
    
    public Vector ListDocumentosDestinatarios(String numrem, String destinatario) throws Exception{
        PoolManager poolManager = PoolManager.getInstance();
        Connection con = poolManager.getConnection("fintra");
        if (con == null)
            throw new SQLException("Sin conexion");
        PreparedStatement st = null;
        ResultSet rs         = null;
        String docpapa = "";
        String tdpapa  = "";
        Vector vec = new Vector();
        Vector hijos = null;
        remesa_docto rem = new remesa_docto();
        remesa_docto remh = new remesa_docto();
        
        try{
            st = con.prepareStatement(this.SQLDOC );
            st.setString(1, numrem );
            st.setString(2, destinatario );
            rs=st.executeQuery();
            if(rs!=null){
                String papaAnterior = "";
                while(rs.next()){
                    
                    String doc = rs.getString("DOCUMENTO");
                    String td = rs.getString("TIPO_DOC");
                    if( doc!= null && td != null ){
                        remesa_docto papa = new remesa_docto();
                        remesa_docto hijo = new remesa_docto();
                        papa.setTipo_doc(td);
                        papa.setDocumento(doc);
                        papa.setDestinatario(rs.getString("destinatario"));
                        hijo.setTipo_doc(rs.getString("TIPO_DOC_REL"));
                        hijo.setDocumento(rs.getString("DOCUMENTO_REL"));
                        if ( papaAnterior.equals(doc+td) ){
                            remesa_docto papaVector = (remesa_docto) vec.lastElement();
                            papaVector.agregarHijo(hijo);
                        }
                        else {
                            vec.addElement(papa);
                            papa.agregarHijo(hijo);
                        }
                        papaAnterior = doc+td;
                    }
                }
                
            }
            
            
        }catch(Exception e){
            throw new SQLException("Error en rutina LISTVC [remesa_doctoDAO].... \n"+ e.getMessage());
        }
        finally{
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            poolManager.freeConnection("fintra",con);
        }
        
        return vec;
    }
    
    public void deleteDocs(String numrem, String tipo, String documento)  throws SQLException {
        PreparedStatement st          = null;
        PoolManager       poolManager = PoolManager.getInstance();
        Connection        conPostgres = poolManager.getConnection("fintra");
        String sql = "";
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        try {
            st = conPostgres.prepareStatement("delete from remesa_docto where numrem = ? and tipo_doc = ? and documento = ? and destinatario=''");
            st.setString(1,  numrem);
            st.setString(2,  tipo);
            st.setString(3,  documento);
            st.executeUpdate();
        }
        catch(SQLException e) {
            throw new SQLException("Error en rutina deleteDocs [remesa_doctoDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null)  st.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
    }
    
    /**
     * listaDocs Metodo para buscar la lista de documentos relacionados a una remesa.
     * @autor: Ing.Karen Reales
     * @return Vector
     * @throws SQLException.
     */
    public Vector listaDocs(String numrem) throws SQLException {
        
        PreparedStatement st          = null;
        PoolManager       poolManager = PoolManager.getInstance();
        Connection        conPostgres = poolManager.getConnection("fintra");
        remesa_docto rem=null;
        ResultSet rs         = null;
        Vector vec = new Vector();
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        try {
            st = conPostgres.prepareStatement("select 	rd.tipo_doc," +
            "	rd.documento," +
            "	ie.fecha_sia," +
            "	ie.fecha_eta," +
            "	ie.descripcion," +
            "	case when ie.fecha_sia is null  then false when ie.fecha_sia='0099-01-01 00:00:00' then false else true end as isImport " +
            " from 	remesa_docto rd" +
            "	left join impo_expo ie on (ie.tipo_doc=rd.tipo_doc and ie.documento=rd.documento)" +
            " where 	numrem=?" +
            "	and rd.destinatario=''");
            st.setString(1, numrem);
            rs=st.executeQuery();
            String papaAnterior = "";
            while(rs.next()){
                rem = new remesa_docto();
                rem.setTipo_doc(rs.getString("tipo_doc"));
                rem.setDocumento(rs.getString("documento"));
                rem.setImportacion("N");
                if(rs.getBoolean("isImport")){
                    rem.setImportacion("S");
                    rem.setFecha_sia(rs.getString("fecha_sia"));
                    rem.setFecha_eta(rs.getString("fecha_eta"));
                    rem.setDescripcion_cga(rs.getString("descripcion"));
                }
                vec.add(rem);
            }
        }
        catch(SQLException e) {
            throw new SQLException("Error en rutina listaDocs [remesa_doctoDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null)  st.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
        return vec;
    }
    
    /*Modificaciones 19 de Enero*/
    // LREALES - MODIFICADO 5 DICIEMBRE 2006
    
    /**
     * Getter for property datos.
     * @return Value of property datos.
     */
    public java.util.Vector getVector() {
        
        return datos;
        
    }
    
    /**
     * Setter for property datos.
     * @param vector New value of property datos.
     */
    public void setVector( java.util.Vector datos ) {
        
        this.datos = datos;
        
    }
    
    /**
     * Funcion publica que obtiene el codigo del cliente
     * de una remesa especifica.
     */
    public String buscarCodCliRemesa( String numrem ) throws Exception {
        
        String codcli = "";
        
        PreparedStatement st = null;
        ResultSet rs = null;
        
        try {
            
            st = crearPreparedStatement( "SQL_SELECT_CODCLI_REMESA" );
            
            st.setString( 1, numrem );
            ////System.out.println( "+++ SQL_SELECT: " + st.toString() );
            rs = st.executeQuery();
            
            if ( rs.next() ) {
                
                codcli = rs.getString( "cliente" )!=null?rs.getString( "cliente" ).toUpperCase():"";
                
            }
            
        } catch( Exception e ) {
            
            e.printStackTrace();
            
        }
        
        finally {
            
            if ( st != null ) {
                
                try{
                    
                    st.close();
                    
                } catch( Exception e ) {
                    
                    e.printStackTrace();
                    throw new Exception( "ERROR DURANTE 'buscarCodCliRemesa()' - [remesa_doctoDAO].. " + e.getMessage() );
                    
                }
                
            }
            
            this.desconectar( "SQL_SELECT_CODCLI_REMESA" );
            
        }
        
        return codcli;
        
    }
    
    /**
     * Funcion publica que obtiene el codigo del cliente
     * de una remesa especifica.
     */
    public void buscarUnImpoExpo( String distrito, String tipo_documento, String documento ) throws Exception {
        
        remesa_docto info = null;
        
        PreparedStatement st = null;
        ResultSet rs = null;
        
        try {
            
            st = crearPreparedStatement( "SQL_SELECT_IMPO_EXPO" );
            
            st.setString( 1, distrito );
            st.setString( 2, tipo_documento );
            st.setString( 3, documento );
            ////System.out.println( "+++ SQL_SELECT: " + st.toString() );
            rs = st.executeQuery();
            
            this.datos = new Vector();
            
            if ( rs.next() ) {
                
                info = new remesa_docto();
                
                info.loadImpoExpo( rs );
                
                datos.add( info );
                
            }
            
        } catch( Exception e ) {
            
            e.printStackTrace();
            
        }
        
        finally {
            
            if ( st != null ) {
                
                try{
                    
                    st.close();
                    
                } catch( Exception e ) {
                    
                    e.printStackTrace();
                    throw new Exception( "ERROR DURANTE 'buscarUnImpoExpo()' - [remesa_doctoDAO].. " + e.getMessage() );
                    
                }
                
            }
            
            this.desconectar( "SQL_SELECT_IMPO_EXPO" );
            
        }
        
    }
    
    /**
     * Funcion publica que actualiza la fecha de posible pago de una factura.
     */
    public void insertarImpoExpo( String distrito, String tipo_documento, String documento, String fecha_sia, String fecha_eta, String descripcion, String usuario, String codcli ) throws Exception {
        
        PreparedStatement st = null;
        
        try {
            
            st = this.crearPreparedStatement( "SQL_INSERT_IMPO_EXPO" );
            
            st.setString( 1, distrito );
            st.setString( 2, tipo_documento );
            st.setString( 3, documento );
            st.setString( 4, fecha_sia );
            st.setString( 5, fecha_eta );
            st.setString( 6, descripcion );
            st.setString( 7, usuario );
            st.setString( 8, usuario );
            st.setString( 9, usuario );
            st.setString( 10, codcli );
            //System.out.println( "--- SQL_INSERT_IMPO_EXPO: " + st.toString() );
            st.executeUpdate();
            
        } catch( Exception e ) {
            
            e.printStackTrace();
            
        }
        
        finally {
            
            if ( st != null ) {
                
                try{
                    
                    st.close();
                    
                } catch( Exception e ) {
                    
                    e.printStackTrace();
                    throw new Exception( "ERROR DURANTE 'insertarImpoExpo()' - [remesa_doctoDAO].. " + e.getMessage() );
                    
                }
                
            }
            
            this.desconectar( "SQL_INSERT_IMPO_EXPO" );
            
        }
        
    }
    
    /**
     * Funcion publica que actualiza la fecha de posible pago de una factura.
     */
    public void actualizarImpoExpo( String fecha_sia, String fecha_eta, String descripcion, String user_update, String distrito, String tipo_documento, String documento ) throws Exception {
        
        PreparedStatement st = null;
        
        try {
            
            st = this.crearPreparedStatement( "SQL_UPDATE_IMPO_EXPO" );
            
            st.setString( 1, fecha_sia );
            st.setString( 2, fecha_eta );
            st.setString( 3, descripcion );
            st.setString( 4, user_update );
            st.setString( 5, distrito );
            st.setString( 6, tipo_documento );
            st.setString( 7, documento );
            //System.out.println( "--- SQL_UPDATE_IMPO_EXPO: " + st.toString() );
            st.executeUpdate();
            
        } catch( Exception e ) {
            
            e.printStackTrace();
            
        }
        
        finally {
            
            if ( st != null ) {
                
                try{
                    
                    st.close();
                    
                } catch( Exception e ) {
                    
                    e.printStackTrace();
                    throw new Exception( "ERROR DURANTE 'actualizarImpoExpo()' - [remesa_doctoDAO].. " + e.getMessage() );
                    
                }
                
            }
            
            this.desconectar( "SQL_UPDATE_IMPO_EXPO" );
            
        }
        
    }
    
    //********************************************************nuevo 12 diciembre 2006
    
    
    
    
    /**
     *Metodo que borra los documento de impoexpo
     *@autor: Diogenes Bastidas
     *@param: distrito,tipo_documento, documento
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void borrarImpoExpo( String distrito, String tipo_documento, String documento ) throws Exception {
        
        PreparedStatement st = null;
        try {
            
            st = this.crearPreparedStatement( "SQL_DELETE_IMPO_EXPO" );
            st.setString( 1, distrito );
            st.setString( 2, tipo_documento );
            st.setString( 3, documento );
            //System.out.println( "SQL_DELETE_IMPO_EXPO: " + st.toString() );
            st.executeUpdate();
            
            
        } catch( Exception e ) {
            
            e.printStackTrace();
            
        }
        
        finally {
            
            if ( st != null ) {
                try{
                    st.close();
                } catch( Exception e ) {
                    e.printStackTrace();
                    throw new Exception( "ERROR DURANTE 'borrarImpoExpo' - [remesa_doctoDAO].. " + e.getMessage() );
                }
            }
            this.desconectar( "SQL_DELETE_IMPO_EXPO" );
        }
    }
    
    /**
     * Procedimiento de busqueda de documentos relacionados con una remesa sin destinatario
     * @autor: Ing. Henry A. Osorio Gonzalez
     * @modificado: Diogenes Bastidas
     * @fecha: 2006-04-11
     * @params Remesa ...... Remesa a Consultar
     * @throws SQLException.
     */
    public void buscarDocumentosRemesa(String Remesa, String dsrtct)throws SQLException{
        PreparedStatement st       = null;
        ResultSet         rs       = null;
        String            doc      = "";
        boolean           liberar = false;
        PoolManager poolManager = PoolManager.getInstance();
        Connection  conPostgres  = poolManager.getConnection("fintra");
        documentos = new Vector();
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        try {
            st = conPostgres.prepareStatement(SQL_DOCUMENTOS_REMESA);
            st.setString(1, dsrtct);
            st.setString(2, Remesa);
            rs = st.executeQuery();
            while(rs.next()){
                remesa_docto rem = new remesa_docto();
                rem.setDocument_name(rs.getString("nombre_documento"));
                rem.setDocumento(rs.getString("documento"));
                documentos.addElement(rem);
            }
        }
        catch(Exception e) {
            throw new SQLException("Error en rutina BuscarDocumentosInternos [PlanillaImpresionDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
    }
    
    /**
     * buscarDestinatariosRemesa busca los destinatario con los documentos
     * @autor: Ing. Diogenes Bastidas
     * @params Remesa ...... Remesa a Consultar
     * @throws SQLException.
     */
    public void buscarDestinatariosRemesa(String Remesa, String dsrtct)throws SQLException{
        PreparedStatement st       = null;
        ResultSet         rs       = null;
        String            doc      = "";
        boolean           liberar = false;
        PoolManager poolManager = PoolManager.getInstance();
        Connection  conPostgres  = poolManager.getConnection("fintra");
        destinatariosdoc = new Vector();
        if (conPostgres == null)
            throw new SQLException("Sin conexion");
        try {
            st = conPostgres.prepareStatement(SQL_DESTINATARIO_DOC);
            st.setString(1, dsrtct);
            st.setString(2, Remesa);
            rs = st.executeQuery();
            while(rs.next()){
                remesa_docto rem = new remesa_docto();
                rem.setDocument_name(rs.getString("nombre_documento"));
                rem.setDocumento(rs.getString("documento"));
                rem.setDocumento_rel(rs.getString("documento_rel"));
                rem.setDocument_rel_name(rs.getString("nombre_documento_rel"));
                rem.setDestinatario(rs.getString("nomdes"));
                rem.setCiudad_destinatario(rs.getString("nomciu"));
                destinatariosdoc.addElement(rem);
            }
        }
        catch(Exception e) {
            throw new SQLException("Error en rutina BuscarDocumentosInternos [PlanillaImpresionDAO]... \n"+e.getMessage());
        }
        finally {
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            poolManager.freeConnection("fintra",conPostgres);
        }
    }
    
    
    /**
     *Metodo que busca los docuemntos de la remesa
     *@autor: Diogenes Bastidas
     *@param: numrem Numero de la remesa
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public void DocumentosRemesa(String numrem) throws SQLException{
        PoolManager poolManager = PoolManager.getInstance();
        remesa_docto rd = new remesa_docto();
        PreparedStatement st = null;
        ResultSet rs         = null;
        documentos = null;
        String SQL = "SQL_DOCUMENTOS_REMESA";
        try{
            int i = 0;
            st = crearPreparedStatement(SQL);
            st.setString(1, numrem);
            
            rs=st.executeQuery();
            documentos = new Vector();
            while(rs.next()){
                rd = new remesa_docto();
                rd.setTipo_doc(rs.getString("tipo_doc"));
                rd.setDocumento(rs.getString("documento"));
                rd.setFecha_sia(rs.getString("fecha_sia"));
                rd.setFecha_eta(rs.getString("fecha_eta"));
                rd.setDescripcion_cga(rs.getString("descripcion"));
                rd.setImportacion(rs.getString("dato").length()>0?rs.getString("dato").substring(0,1):"");
                rd.setExportacion(rs.getString("dato").length()>1?rs.getString("dato").substring(1,2):"");
                documentos.add(rd);
            }
            
            
            
        } catch (SQLException e){
            throw new SQLException("Error en rutina LIST [remesa_doctoDAO].... \n"+ e.getMessage());
        }
        finally {
            if(st!=null) st.close();
            if(rs!=null) rs.close();
            this.desconectar(SQL);
            
        }
    }
    
    /**
     *Metodo que borra los documento de impoexpo
     *@autor: Diogenes Bastidas
     *@param: distrito,tipo_documento, documento
     *@throws: En caso de que un error de base de datos ocurra.
     */
    public String SQL_borrarImpoExpo( String distrito, String tipo_documento, String documento ) throws Exception {
        
        PreparedStatement st = null;
        String sql="";
        try {
            
            st = this.crearPreparedStatement( "SQL_DELETE_IMPO_EXPO" );
            st.setString( 1, distrito );
            st.setString( 2, tipo_documento );
            st.setString( 3, documento );
            /*//System.out.println( "SQL_DELETE_IMPO_EXPO: " + st.toString() );
            st.executeUpdate();*/
            sql = st.toString()+"; ";
            
        } catch( Exception e ) {
            
            e.printStackTrace();
            
        }
        
        finally {
            
            if ( st != null ) {
                try{
                    st.close();
                } catch( Exception e ) {
                    e.printStackTrace();
                    throw new Exception( "ERROR DURANTE 'borrarImpoExpo' - [remesa_doctoDAO].. " + e.getMessage() );
                }
            }
            this.desconectar( "SQL_DELETE_IMPO_EXPO" );
        }
        return sql;
    }
    
    
    
}
