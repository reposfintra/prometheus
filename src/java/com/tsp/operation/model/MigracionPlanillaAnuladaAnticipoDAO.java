/*
 * MigracionRemesasAnuladasDAO.java
 *
 * Created on 21 de julio de 2005, 01:25 PM
 */

package com.tsp.operation.model;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.util.*;
import com.tsp.util.*;
import com.tsp.util.connectionpool.PoolManager;
import java.net.URL;
import org.apache.log4j.Logger;
//import com.tsp.propiedades.Propiedades;
/**
 *
 * @author  Henry
 */
public class MigracionPlanillaAnuladaAnticipoDAO {
    
    static Logger logger = Logger.getLogger(MigracionPlanillaAnuladaAnticipoDAO.class);
    private static String archivo_sincronizado="";
    private static String archivo_recibido="";
    private static Properties dbProps = new Properties();
    
    private Vector planiConAnticipo;
    private Vector planiSinAnticipo;
    
    //private Propiedades propiedades = new Propiedades();;
    
    private static final String SQL_PLANILLAS_ANTICIPO = "SELECT " +
    "     DISTINCT" +
    "     DOCUMENTO AS NUMPLA " +
    "FROM " +
    "    DOCUMENTO_ANULADO D, " +
    "    MOVPLA MP " +
    "WHERE" +
    "    D.TIPODOC = '001' AND" +
    "    D.DOCUMENTO = MP.PLANILLA AND" +
    "    D.DOCUMENTO NOT IN (SELECT PLANILLA FROM MOVPLA WHERE concept_code='01') AND       " +
    "    TO_CHAR(D.CREATION_DATE,'YYYY-MM-DD')= TO_CHAR(MP.CREATION_DATE,'YYYY-MM-DD') AND " +
    "    D.CREATION_DATE  >= ? AND  D.CREATION_DATE <=?";
    
    private static final String SQL_PLANILLAS_CON_ANTICIPO = "SELECT DISTINCT DOCUMENTO AS NUMPLA," +
    "		MP.ITEM," +
    "		U.NIT, " +
    "		TO_CHAR(D.CREATION_DATE,'dd/mm/yy')," +
    "           D.CAUSA_ANULACION," +
    "           D.TIPO_RECUPERACION_ANT " +
    "FROM 		PLANILLA P, " +
    "		MOVPLA MP," +
    "		DOCUMENTO_ANULADO D, " +
    "		USUARIOS U " +
    "WHERE 		P.NUMPLA = MP.PLANILLA " +
    "		AND P.NUMPLA = D.DOCUMENTO " +
    "		AND U.IDUSUARIO = D.CREATION_USER" +
    "		AND TIPODOC = '001'  " +
    "		AND P.NUMPLA IN (SELECT PLANILLA FROM MOVPLA WHERE concept_code='01' AND vlr>0) " +
    "           AND TO_CHAR(D.CREATION_DATE,'YYYY-MM-DD') = TO_CHAR(MP.CREATION_DATE,'YYYY-MM-DD')   " +
    "           AND D.CREATION_DATE  >= ? AND  D.CREATION_DATE <=?";
    
    /*private static final String SQL_PLANILLAS_ANTICIPO  =  "SELECT DISTINCT DOCUMENTO AS NUMPLA FROM PLANILLA P, MOVPLA MP, DOCUMENTO_ANULADO D "+
                                                           "WHERE P.NUMPLA = MP.PLANILLA AND P.NUMPLA = D.DOCUMENTO AND "+
                                                           "TIPODOC = '001' AND P.NUMPLA NOT IN (SELECT PLANILLA FROM MOVPLA WHERE concept_code='01' AND PLANILLA NOT IN (SELECT PLANILLA FROM MOVPLA WHERE concept_code='07')) AND MP.CONCEPT_CODE <> '06' AND  "+
                                                           "TO_CHAR(D.CREATION_DATE,'YYYY-MM-DD')= TO_CHAR(MP.CREATION_DATE,'YYYY-MM-DD') "+
                                                           "AND D.CREATION_DATE BETWEEN ? AND ?";
     
    private static final String SQL_PLANILLAS_CON_ANTICIPO = "SELECT DISTINCT DOCUMENTO AS NUMPLA," +
    "		MP.ITEM," +
    "		U.NIT, " +
    "		TO_CHAR(D.CREATION_DATE,'dd/mm/yy')," +
    "           D.CAUSA_ANULACION," +
    "           D.TIPO_RECUPERACION_ANT " +
    "FROM 		PLANILLA P, " +
    "		MOVPLA MP," +
    "		DOCUMENTO_ANULADO D, " +
    "		USUARIOS U " +
    "WHERE 		P.NUMPLA = MP.PLANILLA " +
    "		AND P.NUMPLA = D.DOCUMENTO " +
    "		AND U.IDUSUARIO = D.CREATION_USER" +
    "		AND TIPODOC = '001'  " +
    "		AND P.NUMPLA IN (SELECT PLANILLA FROM MOVPLA WHERE concept_code='01' AND vlr>0) " +
    "           AND TO_CHAR(D.CREATION_DATE,'YYYY-MM-DD') = TO_CHAR(MP.CREATION_DATE,'YYYY-MM-DD')   " +
    "		AND D.CREATION_DATE BETWEEN ? AND ?";*/
    
    /** Creates a new instance of MigracionRemesasAnuladasDAO */
    public MigracionPlanillaAnuladaAnticipoDAO() {
    }
    /*public void cargarPropiedades() throws IOException{
        InputStream is = propiedades.getClass().getResourceAsStream("general.properties");
        dbProps.load(is);
    }*/
    public String getUltimaFechaProceso(){
        String fecha="";
        fecha = dbProps.getProperty("ultima_fecha_proceso_anticipo");
        return fecha;
    }
    public void setUltimafechaCreacion(String fecha) throws Exception{
        try{
            //SE ACTUALIZAN LOS DATOS EN EL ARCHIVO DE CONFIGURACION..
            //Util u = new Util();
            logger.info("ultima_fecha_proceso_anticipo:"+ fecha);
            dbProps.setProperty("ultima_fecha_proceso_anticipo", fecha);
            URL u = null;//propiedades.getClass().getResource("general.properties");
            logger.info(u.getPath());
            dbProps.store(new FileOutputStream(u.getPath()),"");
            ////System.out.println("Listo escribi en el archivo...");
            
        }catch(IOException e){
            logger.info("ERROR ESCRIBIENDO EN EL ARCHIVO DE PROPIEDADES LA FECHA DE CORTE "+e.getMessage());
            throw new IOException("ERROR ESCRIBIENDO EN EL ARCHIVO DE PROPIEDADES LA FECHA DE CORTE "+e.getMessage());
            
        }
    }
    public void obtenerPlanillasAnuladaSinAnticipo(String fecIni, String fecFin) throws java.sql.SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            //planiAnticipo = new Vector();
            planiSinAnticipo = new Vector();
            if(con!=null){
                st = con.prepareStatement(this.SQL_PLANILLAS_ANTICIPO);
                st.setString(1,fecIni);
                st.setString(2,fecFin);
                rs = st.executeQuery();
                while(rs.next()){
                    planiSinAnticipo.addElement(rs.getString(1));
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LAS REMESAS" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    public void obtenerPlanillasAnuladaConAnticipo(String fecIni, String fecFin) throws java.sql.SQLException{
        
        Connection con=null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection("fintra");
            //planiAnticipo = new Vector();
            planiConAnticipo = new Vector();
            if(con!=null){
                st = con.prepareStatement(this.SQL_PLANILLAS_CON_ANTICIPO);
                st.setString(1,fecIni);
                st.setString(2,fecFin);
                logger.info("Query:"+ st.toString());
                rs = st.executeQuery();
                while(rs.next()){
                    Vector datos = new Vector();
                    datos.addElement(rs.getString(1));
                    datos.addElement("001");
                    datos.addElement(rs.getString(3));
                    datos.addElement(rs.getString(4));
                    datos.addElement("P");
                    datos.addElement("N");
                    datos.addElement("");
                    datos.addElement("1"); //uom
                    datos.addElement("AN");//QI CODE
                    datos.addElement("");
                    datos.addElement("Anulacion OC"); //DR DESC
                    datos.addElement("");
                    datos.addElement("AN");//DR_REF
                    datos.addElement("");
                    datos.addElement(rs.getString("CAUSA_ANULACION")); // causa
                    datos.addElement("");
                    datos.addElement(rs.getString("TIPO_RECUPERACION_ANT")); //anulacion
                    datos.addElement("");
                    datos.addElement("Y");
                    datos.addElement("Y");
                    datos.addElement("");
                    datos.addElement("");
                    datos.addElement("");
                    
                    planiConAnticipo.addElement(datos);
                }
            }
        }catch(SQLException e){
            throw new SQLException("ERROR DURANTE LA BUSQUEDA DE LAS REMESAS" + e.getMessage() + " " + e.getErrorCode());
        }
        finally{
            if (st != null){
                try{
                    st.close();
                }
                catch(SQLException e){
                    throw new SQLException("ERROR CERRANDO EL ESTAMENTO" + e.getMessage());
                }
            }
            
            if (con != null){
                poolManager.freeConnection("fintra", con);
            }
        }
    }
    public Vector getPlanillaAnuladaConAnticipo() {
        return this.planiConAnticipo;
    }
    public Vector getPlanillaAnuladaSinAnticipo() {
        return this.planiSinAnticipo;
    }
}
