/********************************************************************
 *      Nombre Clase.................   ReporteFacturasProveedorTh.java
 *      Descripci�n..................   Hilo para la escritura del xls
 *      Autor........................   Ing. Andr�s Maturana De La Cruz
 *      Fecha........................   04.01.2006
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.model.threads;


import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import java.io.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.operation.model.*;
import java.util.*;
import java.text.*;
//import com.tsp.operation.model.threads.POIWrite;



public class ReporteFacturasProveedorTh extends Thread{
    
    private Vector reporte;
    private String user;   
    private String id_prov;
    private String nom_prov;
    private Model model;
    
    //Log Procesos
    private String procesoName;
    private String des;
    
    public void start(Model modelo, Vector reporte, String user, String id, String nombre){
        this.reporte = reporte;
        this.user = user;
        this.id_prov = id;
        this.nom_prov = nombre;
        this.model   = modelo;
        this.procesoName = "Reporte Facturas Proveedor";
        this.des = "Reporte Facturas Proveedor:  " + id + " - " + nombre;
        
        super.start();
    }
    
    public synchronized void run(){
        try{
            //INSERTO EN EL LOG DE PROCESO
            model.LogProcesosSvc.InsertProceso(this.procesoName, this.hashCode(), this.des, user);
            
            //Fecha del sistema
            Calendar FechaHoy = Calendar.getInstance();
            Date d = FechaHoy.getTime();
            SimpleDateFormat s = new SimpleDateFormat("yyyyMMdd_kkmm");
            SimpleDateFormat s1 = new SimpleDateFormat("yyyy-MM-dd-hh:mm");
            SimpleDateFormat fec = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss");
            SimpleDateFormat time = new SimpleDateFormat("hh:mm");
            String FechaFormated = s.format(d);
            String FechaFormated1 = s1.format(d);
            String hora = time.format(d);
            String Fecha = fec.format(d);
            
            
            //obtener cabecera de ruta
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String path = rb.getString("ruta");
            //armas la ruta
            String Ruta1  = path + "/exportar/migracion/" + user + "/";
            //crear la ruta
            File file = new File(Ruta1);
            file.mkdirs();
            
            POIWrite xls = new POIWrite(Ruta1 +"ReporteFacturasProveedor_" + FechaFormated + ".xls", user, Fecha);
            String nom_archivo = "ReporteFacturasProveedor_" + FechaFormated + ".xls";            
            
            //Definici�n de Estilos
            HSSFCellStyle fecha  = xls.nuevoEstilo("Arial", 9, false , false, "yyyy/mm/dd"  , xls.NONE , xls.NONE , xls.NONE );
            HSSFCellStyle texto  = xls.nuevoEstilo("Arial", 9, false , false, "text"        , xls.NONE , xls.NONE , xls.NONE);
            HSSFCellStyle numero = xls.nuevoEstilo("Arial", 9, false , false, ""            , xls.NONE , xls.NONE , xls.NONE);
            
            HSSFCellStyle header1      = xls.nuevoEstilo("Arial", 12, true , false, "text"        , HSSFColor.WHITE.index , HSSFColor.GREEN.index , HSSFCellStyle.ALIGN_LEFT);
            HSSFCellStyle header2      = xls.nuevoEstilo("Arial", 18, true , false, "text"        , HSSFColor.WHITE.index , HSSFColor.ORANGE.index , HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle header3      = xls.nuevoEstilo("Arial", 12, true , false, "text"        , HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
            HSSFCellStyle texto2      = xls.nuevoEstilo("Arial", 12, false , false, "text"        , HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
            HSSFCellStyle titulo      = xls.nuevoEstilo("Arial", 12, true  , false, "text"        , HSSFColor.BLACK.index , HSSFColor.PINK.index , HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle fechatitle  = xls.nuevoEstilo("Arial", 12, true  , false, "yyyy/mm/dd"  , HSSFColor.RED.index , xls.NONE, HSSFCellStyle.ALIGN_CENTER );
            HSSFCellStyle entero      = xls.nuevoEstilo("Arial", 12, false , false, "text"        , HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle flotante      = xls.nuevoEstilo("Arial", 12, false , false, "text"        , HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_RIGHT);
            HSSFCellStyle moneda  = xls.nuevoEstilo("Arial", 9, false , false, "#,##0.00", xls.NONE , xls.NONE , xls.NONE);                                    
                                    
            xls.obtenerHoja("FACTURAS PROVEEDOR");
            xls.cambiarMagnificacion(3,4);
            
            // cabecera
            
            // cabecera
            
            xls.combinarCeldas(0, 0, 0, 8);
            xls.combinarCeldas(3, 0, 3, 8);
            xls.combinarCeldas(4, 0, 4, 8);
            xls.combinarCeldas(5, 0, 5, 8);
            xls.combinarCeldas(6, 0, 6, 8);
            xls.adicionarCelda(0, 0, "REPORTE DE FACTURAS DE PROVEEDOR", header2);
            xls.adicionarCelda(4, 0, "Fecha del proceso: " + Fecha, header3);            
            xls.adicionarCelda(5, 0, "Proveedor: " + this.nom_prov, header3);
            xls.adicionarCelda(6, 0, "NIT.: " + this.id_prov, header3);
            
                          
            // subtitulos
            
            
            int fila = 8;
            int col  = 0;            
           
            xls.adicionarCelda(fila, col++, "DOCUMENTO", header1);
            xls.adicionarCelda(fila, col++, "BANCO", header1);
            xls.adicionarCelda(fila, col++, "SUCURSAL", header1);
            xls.adicionarCelda(fila, col++, "FECHA FACTURA", header1);
            xls.adicionarCelda(fila, col++, "FECHA VENCIMIENTO", header1);
            xls.adicionarCelda(fila, col++, "ULTIMA FECHA PAGO", header1);
            xls.adicionarCelda(fila, col++, "DESCRIPCION", header1);
            xls.adicionarCelda(fila, col++, "OBSERVACIONES", header1);
            xls.adicionarCelda(fila, col++, "VR NETO", header1);
            
            
            fila++;
            
            double total = 0;
            for (int i = 0; i < reporte.size(); i++){
                col = 0;
                
                CXP_Doc doc = (CXP_Doc) reporte.elementAt(i);
                total += doc.getVlr_neto();
                
                xls.adicionarCelda(fila, col++, doc.getDocumento(), texto);
                xls.adicionarCelda(fila, col++, doc.getBanco(), texto);
                xls.adicionarCelda(fila, col++, doc.getSucursal(), texto);
                xls.adicionarCelda(fila, col++, (doc.getFecha_documento()!=null)? doc.getFecha_documento().substring(0, 10) : " ", texto);
                xls.adicionarCelda(fila, col++, (doc.getFecha_vencimiento()!=null)? doc.getFecha_vencimiento().substring(0, 10) : " ", texto);
                xls.adicionarCelda(fila, col++, (doc.getUltima_fecha_pago()!=null)? doc.getUltima_fecha_pago() : " ", texto);
                xls.adicionarCelda(fila, col++, doc.getDescripcion(), texto);
                xls.adicionarCelda(fila, col++, doc.getObservacion(), texto);
                xls.adicionarCelda(fila, col++, doc.getVlr_neto(), moneda);                
                
                fila++;
            }         
            
            xls.adicionarCelda(fila, 7, "TOTAL", header1);
            xls.adicionarCelda(fila, 8, total, moneda);            
            
            
            xls.cerrarLibro();
            model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(), user, "PROCESO EXITOSO");
        }catch (Exception ex){
            ex.printStackTrace();
            ////System.out.println("ERROR AL GENERAR EL ARCHIVO XLS : " + ex.getMessage());
            try{
                model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(), user, "ERROR :" + ex.getMessage());
            }
            catch(Exception f){
                try{
                    model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(), user, "ERROR :");
                }catch(Exception p){    }
            }
        }
        
    }    
   
    
}