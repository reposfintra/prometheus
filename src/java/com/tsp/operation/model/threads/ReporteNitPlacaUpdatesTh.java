/********************************************************************
 *      Nombre Clase.................   ReporteNitPlacaUpdatesService.java
 *      Descripci�n..................   Hilo para la escritura del xls
 *      Autor........................   Tito Andr�s Maturana
 *      Fecha........................   08.11.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.model.threads;


import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import java.io.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.operation.model.*;
import java.util.*;
import java.text.*;
//import com.tsp.operation.model.threads.POIWrite;



public class ReporteNitPlacaUpdatesTh extends Thread{
    
    private Vector placas;
    private String user;
    private String mensaje;
    private String fechai;
    private String fechaf;
    private Vector cols_placa;
    private Vector nits;
    private Vector cols_nit;
    
    public void start(Vector placas, Vector colspl, Vector nits, Vector colsnit, String user, String fechai, String fechaf){
        this.placas = placas;
        this.user = user;
        this.fechaf = fechaf;
        this.fechai = fechai;
        this.cols_placa = colspl;
        this.nits = nits;
        this.cols_nit = colsnit;
        
        super.start();
    }
    
    public synchronized void run(){
        try{
            //Fecha del sistema
            Calendar FechaHoy = Calendar.getInstance();
            Date d = FechaHoy.getTime();
            SimpleDateFormat s = new SimpleDateFormat("yyyyMMdd");
            SimpleDateFormat s1 = new SimpleDateFormat("yyyy-MM-dd-hh:mm");
            SimpleDateFormat fec = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat time = new SimpleDateFormat("hh:mm");
            String FechaFormated = s.format(d);
            String FechaFormated1 = s1.format(d);
            String hora = time.format(d);
            String Fecha = fec.format(d);
            
            
            //obtener cabecera de ruta
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String path = rb.getString("ruta");
            //armas la ruta
            String Ruta1  = path + "/exportar/migracion/" + user + "/";
            //crear la ruta
            File file = new File(Ruta1);
            file.mkdirs();
            
            POIWrite xls = new POIWrite(Ruta1 +"ReporteActualizacionesNitPlaca_" + FechaFormated + ".xls", user, Fecha);
            String nom_archivo = "ReporteActualizacionesNitPlaca_" + FechaFormated + ".xls";
            this.mensaje = nom_archivo;
            
            //Definici�n de Estilos
            HSSFCellStyle fecha  = xls.nuevoEstilo("Arial, Courier New, Mono", 9, false , false, "yyyy/mm/dd"  , xls.NONE , xls.NONE , xls.NONE );
            HSSFCellStyle texto  = xls.nuevoEstilo("Arial, Courier New, Mono", 9, false , false, "text"        , xls.NONE , xls.NONE , xls.NONE);
            HSSFCellStyle numero = xls.nuevoEstilo("Arial, Courier New, Mono", 9, false , false, ""            , xls.NONE , xls.NONE , xls.NONE);
            
            HSSFCellStyle header1      = xls.nuevoEstilo("Arial, Courier New, Mono", 10, true , false, "text"        , HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
            HSSFCellStyle header2      = xls.nuevoEstilo("Arial, Courier New, Mono", 11, true , false, "text"        , HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
            HSSFCellStyle texto2      = xls.nuevoEstilo("Arial, Courier New, Mono", 10, false , false, "text"        , HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
            HSSFCellStyle titulo      = xls.nuevoEstilo("Arial, Courier New, Mono", 9, true  , false, "text"        , HSSFColor.BLACK.index , HSSFColor.PINK.index , HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle fechatitle  = xls.nuevoEstilo("Arial, Courier New, Mono", 9, true  , false, "yyyy/mm/dd"  , HSSFColor.RED.index , xls.NONE, HSSFCellStyle.ALIGN_CENTER );
            HSSFCellStyle cambio      = xls.nuevoEstilo("Arial, Courier New, Mono", 10, true , false, "text"        ,  HSSFColor.BLACK.index, HSSFColor.CORNFLOWER_BLUE.index, HSSFCellStyle.ALIGN_LEFT);
            
                                    
            xls.obtenerHoja("PLACA");
            xls.cambiarMagnificacion(3,4);
            
            // cabecera
            
            xls.combinarCeldas(2, 0, 2, this.cols_placa.size());
            xls.combinarCeldas(4, 0, 4, this.cols_placa.size());
            xls.combinarCeldas(6, 0, 6, this.cols_placa.size());
            xls.adicionarCelda(2, 0, "TRANSPORTES SANCHEZ POLO S.A.", header2);
            xls.adicionarCelda(4, 0, "Actualizaciones en el archivo de Placas", header2);
            xls.adicionarCelda(6, 0, "Per�odo: " + fechai + " Hasta: " + fechaf, header2);
            
            
            // subtitulos
            
            
            int fila = 8;
            int col  = 1;
            
            xls.adicionarCelda(fila, 0, "BASE", header1);
            for(int i=0; i<this.cols_placa.size(); i++){
                xls.adicionarCelda(fila, col++, (String) this.cols_placa.elementAt(i), header1);
            }
            
            fila++;
            
            for( int i=0; i<placas.size(); i++){
                col = 0;
                
                Vector vec = (Vector) placas.elementAt(i);
                String base = (String) vec.elementAt(0);
                String[] c = (String[]) vec.elementAt(1);
                //////System.out.println("........... placa: " + c[1] + ", base: " + base);
                
                xls.adicionarCelda(fila, 0, base.toUpperCase(), texto);
                
                Vector cambios = new Vector();
                
                for( int j=0; j<c.length; j++){
                    
                    if( i!=0 ){
                        Vector vec1 = (Vector) placas.elementAt(i-1);
                        String[] c1 = (String[]) vec1.elementAt(1);
                        if( c1[1].compareTo(c[1])==0 && c[j].compareTo(c1[j])!=0 ){                            
                            xls.adicionarCelda(fila, (j+1), c[j], cambio);                            
                            cambios.add(String.valueOf(j+1));
                        } else {
                            xls.adicionarCelda(fila, (j+1), c[j], texto);
                        }
                    } else {
                        xls.adicionarCelda(fila, (j+1), c[j], texto);
                    }
                }
                
                
                if( cambios.size()>0 ){                    
                    for( int k=0; k<c.length; k++){
                        for( int j=0; j<cambios.size(); j++){
                            col++;
                            if( String.valueOf((k+1)).compareTo((String) cambios.elementAt(j))!=0 ){
                                xls.adicionarCelda(fila, (k+1), "", texto);
                            }
                        }
                    }
                }                
                
                fila++;
            }
            
            xls.obtenerHoja("NIT");
            xls.cambiarMagnificacion(3,4);
            
            // cabecera
            
            xls.combinarCeldas(2, 0, 2, this.cols_placa.size());
            xls.combinarCeldas(4, 0, 4, this.cols_placa.size());
            xls.combinarCeldas(6, 0, 6, this.cols_placa.size());
            xls.adicionarCelda(2, 0, "TRANSPORTES SANCHEZ POLO S.A.", header2);
            xls.adicionarCelda(4, 0, "Actualizaciones en el archivo de Nits", header2);
            xls.adicionarCelda(6, 0, "Per�odo: " + fechai + " Hasta: " + fechaf, header2);
            
            fila = 8;
            col  = 1;
            
            xls.adicionarCelda(fila, 0, "BASE", header1);
            for(int i=0; i<this.cols_nit.size(); i++){
                xls.adicionarCelda(fila, col++, (String) this.cols_nit.elementAt(i), header1);
            }
            
            fila++;
            
            for( int i=0; i<nits.size(); i++){
                col = 0;
                
                Vector vec = (Vector) nits.elementAt(i);
                String base = (String) vec.elementAt(0);
                String[] c = (String[]) vec.elementAt(1);
                //////System.out.println("........... placa: " + c[1] + ", base: " + base);
                
                xls.adicionarCelda(fila, 0, base.toUpperCase(), texto);
                
                Vector cambios = new Vector();
                
                for( int j=0; j<c.length; j++){
                    
                    if( i!=0 ){
                        Vector vec1 = (Vector) nits.elementAt(i-1);
                        String[] c1 = (String[]) vec1.elementAt(1);
                        if( c1[1].compareTo(c[1])==0 && c[j].compareTo(c1[j])!=0 ){                            
                            xls.adicionarCelda(fila, (j+1), c[j], cambio);                            
                            cambios.add(String.valueOf(j+1));
                        } else {
                            xls.adicionarCelda(fila, (j+1), c[j], texto);
                        }
                    } else {
                        xls.adicionarCelda(fila, (j+1), c[j], texto);
                    }
                }
                
                
                if( cambios.size()>0 ){                    
                    for( int k=0; k<c.length; k++){
                        for( int j=0; j<cambios.size(); j++){
                            col++;
                            if( String.valueOf((k+1)).compareTo((String) cambios.elementAt(j))!=0 ){
                                xls.adicionarCelda(fila, (k+1), "", texto);
                            }
                        }
                    }
                }                
                
                fila++;
            }
            
            xls.cerrarLibro();
            
        }catch (Exception ex){
            ex.printStackTrace();
            ////System.out.println("ERROR AL GENERAR EL ARCHIVO XLS : " + ex.getMessage());
        }
        
    }
    
    /**
     * Getter for property mensaje.
     * @return Value of property mensaje.
     */
    public java.lang.String getMensaje() {
        return mensaje;
    }
    
    /**
     * Setter for property mensaje.
     * @param mensaje New value of property mensaje.
     */
    public void setMensaje(java.lang.String mensaje) {
        this.mensaje = mensaje;
    }
    
}