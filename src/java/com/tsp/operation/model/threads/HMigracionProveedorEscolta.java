
package com.tsp.operation.model.threads;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.text.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;
import com.tsp.util.Util;
import java.util.Vector;
import java.io.*;

public class HMigracionProveedorEscolta extends Thread{
    private Vector car;
    private Vector veh;
    
    private FileWriter fw;
    private BufferedWriter bffw;
    private PrintWriter pntw;    
    private String usuario;
    private String path;
 
    public HMigracionProveedorEscolta() {
    }
    
    public void start(Vector car, Vector veh, String u) {
        this.car = car;
        this.veh = veh;
        this.usuario = u;
        
        try{
            //sandrameg 190905
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            path = rb.getString("ruta");            
            
            String nombre_archivo = "";
            String ruta = path + "/exportar/migracion/" + usuario + "/";
            String fecha_actual = Util.getFechaActual_String(6);            
            
            nombre_archivo = ruta + "PROVEEDOR_ESCOLTA " + fecha_actual.replace(':','_').replace('/','_') + ".csv";            
            File archivo = new File(ruta.trim());
                
            archivo.mkdirs();
            FileOutputStream file = new FileOutputStream(nombre_archivo.trim());
            
            fw = new FileWriter(nombre_archivo);
            bffw = new BufferedWriter(fw);
            pntw = new PrintWriter(bffw);
            
        }
        catch(java.io.IOException e){
            ////System.out.println(e.toString());
        }
        
        super.start();
    }
    
    public synchronized void run(){
       this.writeFile();
    }
    
    protected void writeFile() {    	
    	String linea = "";   
        for(int i=0; i<car.size(); i++){
            Vector datos = (Vector) car.elementAt(i);
            linea = "" + datos.elementAt(0)+ "," +datos.elementAt(1)+","+
                         datos.elementAt(2)+ "," +datos.elementAt(3)+","+
                         datos.elementAt(4)+ "," +datos.elementAt(5)+","+
                         datos.elementAt(6)+ "," +datos.elementAt(7);                        
            pntw.println(linea);
        }    
        for(int i=0; i<veh.size(); i++){
            Vector datos = (Vector) veh.elementAt(i);
            linea = "" + datos.elementAt(0)+ "," +datos.elementAt(1)+","+
                         datos.elementAt(2)+ "," +datos.elementAt(3)+","+
                         datos.elementAt(4)+ "," +datos.elementAt(5)+","+
                         datos.elementAt(6)+ "," +datos.elementAt(7);                        
            pntw.println(linea);
        }    	
        pntw.close();        
    }    
}
