/*
 * Nombre        ReporteFormatoXLS.java
 * Autor         Osvaldo P�rez Ferrer
 * Fecha         28 de octubre de 2006, 01:41 PM
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */

package com.tsp.operation.model.threads;

import java.util.*;
import java.io.*;
import java.lang.*;
import java.text.*;

import com.tsp.operation.model.*;
import com.tsp.operation.model.beans.POIWrite;

import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.FormatoDAO;
import com.tsp.util.*;

public class ReporteFormatoXLS extends Thread{
    
    private Vector datos;
    private String path;
    
    Usuario user;
    
    String   ruta;
    POIWrite xls;
    HSSFCellStyle header  , titulo1, titulo2, titulo3 , titulo4, letra, numero, porcentaje, letraCentrada, numeroNegrita, letraDerecha;
    HSSFColor     cAzul   , cVerde, cAmarillo, cGris ;
    int           fila = 0;
    SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
    
    String[] tipos;
    String[] titulos;
   
    /**
     * Crea una nueva instancia de  ReporteFormatoXLS
     */
    public ReporteFormatoXLS(){
    }
    
    public void generarCuadroAzul(){
        try{
                        
            //f.cuadroAzul( user.getDstrct(), tipodoc, formato , documento);
                        
            
            if(datos.size()>0){
                
                ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
                ruta = rb.getString("ruta") + "/exportar/migracion/"+user.getLogin() ;
                File archivo = new File( ruta );
                if (!archivo.exists()) archivo.mkdirs();
                
                String fecha = Util.getFechaActual_String(6);
                fecha = fecha.replaceAll("/","-");
                fecha = fecha.replaceAll(":","");
                this.crearArchivo("CuadroAzul "+fecha,"Cuadro Azul");
                this.escribirEnArchivo(datos);
                this.cerrarArchivo();
                /************************************************************************************/
                /**** GUARDAR DATOS EN EL ARCHIVO  ***/
                
            }
        }catch(Exception ex){
            //System.out.println("error en ReporteFormatoXLS: "+ex.getMessage());
        }
    }
    
    public void InitArchivo(String nameFile) throws Exception{
        try{
            
            xls          = new com.tsp.operation.model.beans.POIWrite();
            xls.nuevoLibro( ruta + "/" + nameFile + ".xls" );
            
            // colores
            cAzul       = xls.obtenerColor(204,255,255);
            cVerde      = xls.obtenerColor( 51,153,102);
            cAmarillo   = xls.obtenerColor(255,255,153);
            cGris       = xls.obtenerColor(192,192,192);
            
            // estilos
            header       = xls.nuevoEstilo("Tahoma", 14, true  , false, "text"  , HSSFColor.ORANGE.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
            titulo1      = xls.nuevoEstilo("Tahoma", 8 , true  , false, "text"  , xls.NONE  , xls.NONE , xls.NONE);
            titulo2      = xls.nuevoEstilo("Tahoma", 8 , true  , false, "text"  , HSSFColor.BLACK.index, cAmarillo.getIndex() , HSSFCellStyle.ALIGN_CENTER, 1);
            titulo3      = xls.nuevoEstilo("Tahoma", 8 , true  , false, "text"  , HSSFColor.BLACK.index, cAmarillo.getIndex() , HSSFCellStyle.ALIGN_CENTER, 1);
            titulo4      = xls.nuevoEstilo("Tahoma", 8 , true  , false, "text"  , HSSFColor.WHITE.index, cVerde.getIndex() , HSSFCellStyle.ALIGN_CENTER, 2);
            letra        = xls.nuevoEstilo("Tahoma", 8 , false , false, ""      , xls.NONE , xls.NONE , xls.NONE);
            letraCentrada= xls.nuevoEstilo("Tahoma", 8 , false , false, ""      , xls.NONE , cAzul.getIndex() , HSSFCellStyle.ALIGN_CENTER,1);
            letraDerecha = xls.nuevoEstilo("Tahoma", 8 , false , false, ""      , xls.NONE , cAzul.getIndex() , HSSFCellStyle.ALIGN_RIGHT,1);
            numero       = xls.nuevoEstilo("Tahoma", 8 , false , false, "_(#,##0.00_);(@_)"      , xls.NONE , xls.NONE , xls.NONE);
            numeroNegrita= xls.nuevoEstilo("Tahoma", 8 , true  , false, "_($* #,##0.00_);(@_)"      , xls.NONE , xls.NONE , xls.NONE);
            porcentaje   = xls.nuevoEstilo("Tahoma", 8 , false , false, "0.00%" , xls.NONE , xls.NONE , xls.NONE);
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception("Error en InitArchivo .... \n" + ex.getMessage());
        }
    }
    private void crearArchivo(String nameFile, String titulo) throws Exception{
        try{
            InitArchivo(nameFile);
            xls.obtenerHoja("Base");
            xls.combinarCeldas(0, 0, 0, 24);
            xls.combinarCeldas(1, 0, 1, 24);
            xls.combinarCeldas(2, 0, 2, 24);
            xls.adicionarCelda(0, 0, titulo, header);
            //xls.adicionarCelda(1, 0, "Usuario " + usuario.getNombre() , titulo1);
            xls.adicionarCelda(2, 0, "Fecha " + fmt.format( new Date() ) , titulo1);
            
            
            int col = 0;
     
            short [] dimensiones = new short[24];
            
            
            for( int i=0; i<24; i++ ){
                dimensiones[i] = (short)( 4 * 1000 );
            }
            
            fila=4;
                        
            xls.cambiarAnchoColumna(0, 3000);
            
            String[] titulos = {"DESPACHO#","REMISI�N","FECHA DESPACHO","EMPRESA/DPTO","ORIGEN",//5
                                "DESTINO","PO","DO","CONTENIDO","TIPO VEHICULO",//10
                                "COLOR","PLACA","CONDUCTOR","CEDULA","CELULAR",//15
                                "SOLICITANTE DLTD","TEL-EXT-CEL","FECHA ENTREGA","NUMERO FACTURA","FECHA FACTURA",//20
                                "VALOR FLETE","MONEDA","COST CENTER","PO"};
            
            
            for( int i = 0; i<24; i++ ){
             
                xls.adicionarCelda(fila,  i, titulos[i], titulo2);
                xls.cambiarAnchoColumna(i, dimensiones [i]);
                
            }                       
                  
            fila++;
            
            
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception( "Error en crearArchivo ....\n"  + ex.getMessage() );
        }
    }
    
    
    private void escribirEnArchivo(Vector datos) throws Exception{
        
        try{
            
            String[] mon;
            for(int i = 0; i<datos.size(); i++){
                
                int col = 0 ;
                ReporteCuadroAzul r = (ReporteCuadroAzul)datos.get(i);
                
                
                xls.adicionarCelda(fila, col++, r.getDespacho(), letraCentrada );                                
                
                
                xls.adicionarCelda(fila, col++, r.getRemision(), letraCentrada ); 
                xls.adicionarCelda(fila, col++, r.getFecha_despacho(), letraCentrada ); 
                xls.adicionarCelda(fila, col++, r.getEmpresa_dpto() , letraCentrada ); 
                xls.adicionarCelda(fila, col++, r.getOrigen(), letraCentrada );
                xls.adicionarCelda(fila, col++, r.getDestino(), letraCentrada );
                
                xls.adicionarCelda(fila, col++, r.getP_o(), letraCentrada );
                xls.adicionarCelda(fila, col++, r.getD_o(), letraCentrada );
                xls.adicionarCelda(fila, col++, r.getContenido(), letraCentrada );
                xls.adicionarCelda(fila, col++, r.getTipo_vehiculo(), letraCentrada );
                xls.adicionarCelda(fila, col++, r.getColor(), letraCentrada );
                xls.adicionarCelda(fila, col++, r.getPlaca(), letraCentrada );
                xls.adicionarCelda(fila, col++, r.getConductor(), letraCentrada );
                xls.adicionarCelda(fila, col++, r.getCedula(), letraCentrada );
                xls.adicionarCelda(fila, col++, r.getCelular(), letraCentrada );
                xls.adicionarCelda(fila, col++, r.getSolicitante(), letraCentrada );
                xls.adicionarCelda(fila, col++, r.getTel_ext_cel(), letraCentrada );
                xls.adicionarCelda(fila, col++, r.getFecha_entrega(), letraCentrada );
                xls.adicionarCelda(fila, col++, r.getNumero_factura(), letraCentrada );
                xls.adicionarCelda(fila, col++, r.getFecha_factura(), letraCentrada );
                
                mon = r.getValor_flete().split("_-_");
                if( mon.length > 1 ){
                    xls.adicionarCelda(fila, col++, mon[0], letraCentrada );
                    xls.adicionarCelda(fila, col++, mon[1], letraCentrada );
                }else{
                    xls.adicionarCelda(fila, col++, mon[0], letraCentrada );
                    xls.adicionarCelda(fila, col++, "", letraCentrada );
                }
                
                xls.adicionarCelda(fila, col++, r.getCost_center(), letraCentrada );
                xls.adicionarCelda(fila, col++, r.getP_o(), letraCentrada );                                                      
                                       
                fila++;
            }
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
        
    }
    
    private void cerrarArchivo() throws Exception {
        try{
            if (xls!=null)
                xls.cerrarLibro();
        }catch (Exception ex){
            throw new Exception( "Error en crearArchivo ....\n"  + ex.getMessage() );
        }
    }
    
    
    public String formatearDato(String dato, String tipo){
        
        dato = dato.trim();
        
        if( tipo.equals("DO") ){            
            try{
                double d;
                d = Double.parseDouble( dato );                                
                dato = UtilFinanzas.customFormat( "##0.00" ,d ,2 );
                
            }catch (Exception ex){}
        }        
        return dato;
    }
    
    public void start(  Usuario user, Vector datos ) throws Exception{
    
        this.datos = datos;
        this.user    = user;        
        super.start();
    }
    
    public void run(){
        
        this.generarCuadroAzul();
    }    
   
    /*
    public static void main(String[]as) throws Exception{
        
        
        ReporteFormatoXLS r = new ReporteFormatoXLS();
        Usuario u = new Usuario();
        u.setLogin("OPEREZ");
        u.setDstrct("FINV");
        
        FormatoDAO f = new FormatoDAO();
        
        
        f.cuadroAzul( "2006", "Abril", "04", "FINV" );
        Vector v = f.getVector();
        r.start( u, v );
        
        
        //String[] mon = "aaa_-_".split("_-_");
        
        
    }
    */
}
