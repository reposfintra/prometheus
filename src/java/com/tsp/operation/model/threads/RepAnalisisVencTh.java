/********************************************************************
 *      Clase........................   RepAnalisisVencTh.java
 *      Descripci�n..................   Hilo para la escritura del xls
 *      Autor........................   Ing. Andr�s Maturana De La Cruz
 *      Fecha........................   04.01.2006
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.model.threads;


import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import java.io.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.operation.model.*;
import java.util.*;
import java.text.*;
//import com.tsp.operation.model.threads.POIWrite;



public class RepAnalisisVencTh extends Thread{
    
    private Vector reporte;
    private String user;   
    private String _prov;
    private String nom_prov;
    private Model model;
    private int tipo;
    private String[] limit;
    private String fechaRep;
    
    //Log Procesos
    private String procesoName;
    private String des;
    
    public void start(Model modelo, Vector reporte, String user, int tipo, String fecha, String[] limit){
        this.fechaRep = fecha;
        this.reporte = reporte;
        this.user = user;
        this.model   = modelo;
        this.procesoName = "Reporte An�lisis Vencimieno CxP";
        this.des = "Reporte An�lisis Vencimieno CxP en la fecha:  " + fecha;
        this.tipo = tipo;
        this.limit = limit;
        super.start();
    }
    
    public synchronized void run(){
        try{
            //INSERTO EN EL LOG DE PROCESO
            model.LogProcesosSvc.InsertProceso(this.procesoName, this.hashCode(), this.des, user);
            
            //Fecha del sistema
            Calendar FechaHoy = Calendar.getInstance();
            Date d = FechaHoy.getTime();
            SimpleDateFormat s = new SimpleDateFormat("yyyyMMdd_kkmm");
            SimpleDateFormat s1 = new SimpleDateFormat("yyyy-MM-dd-hh:mm");
            SimpleDateFormat fec = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss");
            SimpleDateFormat time = new SimpleDateFormat("hh:mm");
            String FechaFormated = s.format(d);
            String FechaFormated1 = s1.format(d);
            String hora = time.format(d);
            String Fecha = fec.format(d);
            
            
            //obtener cabecera de ruta
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String path = rb.getString("ruta");
            //armas la ruta
            String Ruta1  = path + "/exportar/migracion/" + user + "/";
            //crear la ruta
            File file = new File(Ruta1);
            file.mkdirs();
            
            POIWrite xls = new POIWrite(Ruta1 +"ReporteAnalisisVencCXP_" + FechaFormated + ".xls", user, Fecha);
            String nom_archivo = "ReporteAnalisisVencCXP_" + FechaFormated + ".xls";            
            
            //Definici�n de Estilos
            HSSFCellStyle fecha  = xls.nuevoEstilo("Arial", 9, false , false, "yyyy/mm/dd"  , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_LEFT );
            HSSFCellStyle texto  = xls.nuevoEstilo("Arial", 9, false , false, "text"        , xls.NONE , xls.NONE , xls.NONE);
            HSSFCellStyle numero = xls.nuevoEstilo("Arial", 9, false , false, ""            , xls.NONE , xls.NONE , xls.NONE);
            
            HSSFCellStyle header1      = xls.nuevoEstilo("Arial", 12, true , false, "text"        , HSSFColor.WHITE.index , HSSFColor.GREEN.index , HSSFCellStyle.ALIGN_LEFT);
            HSSFCellStyle header2      = xls.nuevoEstilo("Arial", 18, true , false, "text"        , HSSFColor.WHITE.index , HSSFColor.ORANGE.index , HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle header3      = xls.nuevoEstilo("Arial", 12, true , false, "text"        , HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
            HSSFCellStyle texto2      = xls.nuevoEstilo("Arial", 12, false , false, "text"        , HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
            HSSFCellStyle titulo      = xls.nuevoEstilo("Arial", 12, true  , false, "text"        , HSSFColor.BLACK.index , HSSFColor.PINK.index , HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle fechatitle  = xls.nuevoEstilo("Arial", 12, true  , false, "yyyy/mm/dd"  , HSSFColor.RED.index , xls.NONE, HSSFCellStyle.ALIGN_CENTER );
            HSSFCellStyle entero      = xls.nuevoEstilo("Arial", 12, false , false, "text"        , HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle flotante      = xls.nuevoEstilo("Arial", 12, false , false, "text"        , HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_RIGHT);
            HSSFCellStyle moneda  = xls.nuevoEstilo("Arial", 9, false , false, "#,##0.00", xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_RIGHT);
            HSSFCellStyle total_st  = xls.nuevoEstilo("Arial", 9, true, false, "#,##0.00", xls.NONE , HSSFColor.GREY_25_PERCENT.index , HSSFCellStyle.ALIGN_RIGHT);
                                    
            xls.obtenerHoja("FACTURAS PROVEEDOR");
            xls.cambiarMagnificacion(3,4);
            
            // cabecera
            
            int maxcol = tipo == 0 ? 9 : 6;
            
            // cabecera
            
            xls.combinarCeldas(0, 0, 0, maxcol);
            xls.combinarCeldas(3, 0, 3, maxcol);
            xls.combinarCeldas(4, 0, 4, maxcol);
            xls.adicionarCelda(0, 0, "REPORTE DE ANALISIS DE VENCIMIENTO DE FACTURAS", header2);
            xls.adicionarCelda(4, 0, "Fecha del proceso: " + Fecha, header3); 
            
                          
            // subtitulos
            
            
            int fila = 8;
            int col  = 0;            
           
            xls.adicionarCelda(fila, col++, "DOCUMENTO", header1);
            xls.adicionarCelda(fila, col++, "NOMBRE", header1);
            
            if( tipo == 0 ){
                xls.adicionarCelda(fila, col++, "FACTURA", header1);
                xls.adicionarCelda(fila, col++, "FECHA", header1);
                xls.adicionarCelda(fila, col++, "VENCE", header1);
            }
            
            xls.adicionarCelda(fila, col++, "VENCIDA", header1);
            xls.adicionarCelda(fila, col++, "NO VENCIDA 1  \n" + this.fechaRep, header1);
            xls.adicionarCelda(fila, col++, "NO VENCIDA 2  " + limit[0], header1);
            xls.adicionarCelda(fila, col++, "NO VENCIDA 3  " + limit[1], header1);
            xls.adicionarCelda(fila, col++, "NO VENCIDA 4  " + limit[2], header1);
            
            
            fila++;
            
            double total = 0;
            for (int i = 0; i < reporte.size(); i++){
                col = 0;
                
                Hashtable ht = (Hashtable) reporte.elementAt(i);;
                
                if( tipo == 0 ){
                    if( ht.get("ttl") == null ){
                        xls.adicionarCelda(fila, col++, ht.get("nit").toString(), texto);
                        xls.adicionarCelda(fila, col++, ht.get("nomprov").toString(), texto);
                        xls.adicionarCelda(fila, col++, ht.get("no").toString(), texto);
                        
                        xls.adicionarCelda(fila, col++, com.tsp.util.Util.ConvertiraDate0(ht.get("fec").toString()), fecha);
                        xls.adicionarCelda(fila, col++, com.tsp.util.Util.ConvertiraDate0(ht.get("fecvenc").toString() + " 00:00:00"), fecha);
                        
                        xls.adicionarCelda(fila, col++, Double.parseDouble(ht.get("col01")==null? "0" :  ht.get("col01").toString()), moneda);
                        xls.adicionarCelda(fila, col++, Double.parseDouble(ht.get("col02")==null? "0" :  ht.get("col02").toString()), moneda);
                        xls.adicionarCelda(fila, col++, Double.parseDouble(ht.get("col03")==null? "0" :  ht.get("col03").toString()), moneda);
                        xls.adicionarCelda(fila, col++, Double.parseDouble(ht.get("col04")==null? "0" :  ht.get("col04").toString()), moneda);
                        xls.adicionarCelda(fila, col++, Double.parseDouble(ht.get("col05")==null? "0" :  ht.get("col05").toString()), moneda);
                    } else {
                        col = 5;
                        xls.combinarCeldas(fila, 0, fila, 4);
                        xls.adicionarCelda(fila, 0, "TOTAL", total_st);
                        xls.adicionarCelda(fila, col++, Double.parseDouble(ht.get("col01")==null? "0" :  ht.get("col01").toString()), total_st);
                        xls.adicionarCelda(fila, col++, Double.parseDouble(ht.get("col02")==null? "0" :  ht.get("col02").toString()), total_st);
                        xls.adicionarCelda(fila, col++, Double.parseDouble(ht.get("col03")==null? "0" :  ht.get("col03").toString()), total_st);
                        xls.adicionarCelda(fila, col++, Double.parseDouble(ht.get("col04")==null? "0" :  ht.get("col04").toString()), total_st);
                        xls.adicionarCelda(fila, col++, Double.parseDouble(ht.get("col05")==null? "0" :  ht.get("col05").toString()), total_st);
                    }
                    
                } else {
                    xls.adicionarCelda(fila, col++, ht.get("nit").toString(), texto);
                    xls.adicionarCelda(fila, col++, ht.get("nomprov").toString(), texto);
                    xls.adicionarCelda(fila, col++, Double.parseDouble(ht.get("col01")==null? "0" :  ht.get("col01").toString()), moneda);
                    xls.adicionarCelda(fila, col++, Double.parseDouble(ht.get("col02")==null? "0" :  ht.get("col02").toString()), moneda);
                    xls.adicionarCelda(fila, col++, Double.parseDouble(ht.get("col03")==null? "0" :  ht.get("col03").toString()), moneda);
                    xls.adicionarCelda(fila, col++, Double.parseDouble(ht.get("col04")==null? "0" :  ht.get("col04").toString()), moneda);
                    xls.adicionarCelda(fila, col++, Double.parseDouble(ht.get("col05")==null? "0" :  ht.get("col05").toString()), moneda);
                    
                }
                
                fila++;
            }         
            
            xls.cerrarLibro();
            model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(), user, "PROCESO EXITOSO");
        }catch (Exception ex){
            ex.printStackTrace();
            ////System.out.println("ERROR AL GENERAR EL ARCHIVO XLS : " + ex.getMessage());
            try{
                model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(), user, "ERROR :" + ex.getMessage());
            }
            catch(Exception f){
                try{
                    model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(), user, "ERROR :");
                }catch(Exception p){    }
            }
        }
        
    }    
   
    
}