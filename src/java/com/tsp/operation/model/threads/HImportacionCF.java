/*
 * HImportacionCF.java
 *
 * Created on 7 de septiembre de 2006, 02:42 PM
 */

package com.tsp.operation.model.threads;
import com.tsp.operation.model.Model;
import com.tsp.operation.model.beans.*;
import com.tsp.util.*;
import com.tsp.operation.model.beans.POIWrite;
import java.io.*;
import java.util.*;
/**
 *
 * @author  ALVARO
 */
public class HImportacionCF extends Thread{
     private List        listado;
     private Model       model;
     private Usuario     usuario;
     private File  file;
     private PrintWriter pw;
     private LogWriter logWriter;
    /** Creates a new instance of HImportacionCF */
    public HImportacionCF(Model m, Usuario u, File f) throws Exception{
         model = m;
         usuario = u;
         file =f;
         java.util.Date ahora = new java.util.Date();
        java.text.SimpleDateFormat fechaHora = new java.text.SimpleDateFormat("yyyy-MM-dd hh.mm.ss");
        String fechaTimestamp = fechaHora.format(ahora);
        
        ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
        String ruta = rb.getString("ruta");
        //Creaci�n del archivo log del usuario
        File file = new File( ruta + "/exportar/migracion/" + usuario.getLogin() );
        file.mkdirs();
         
        pw = new PrintWriter( System.err, true );        
        String logFile = ruta +"/exportar/migracion/" + usuario.getLogin() + "/MigracionFacturasClientes_remesa_docto-" + fechaTimestamp + ".log";
        pw = new PrintWriter( new FileWriter( logFile, true ), true );
        logWriter = new LogWriter( "MigracionFacturasClientes_remesa_docto", LogWriter.INFO, pw );
        logWriter.setPrintWriter( pw );
         super.start();
    }
    public synchronized void run() {
        try{
            logWriter.log("**********************************************************",LogWriter.INFO);
            logWriter.log("*  PROCESO DE MIGRACION DE FACTURAS CLI REMESA_DOCTO      ",LogWriter.INFO);
            logWriter.log("*  Usuario  :  "+usuario.getLogin()+"                     ",LogWriter.INFO);
            logWriter.log("*  Fecha    :  "+Util.getFechaActual_String(6)+"          ",LogWriter.INFO);
            logWriter.log("**********************************************************",LogWriter.INFO);
            
            model.LogProcesosSvc.InsertProceso("MigracionFacturasClientes_remesa_docto",this.hashCode(), "Iniciado", usuario.getLogin());
            logWriter.log("INICIO DEL PROCESO ",LogWriter.INFO);
            
            procesarXLS(file);
            
                logWriter.log( "Resultado: "+model.himportacioncfservice.InsertarFacturas(listado,usuario.getLogin()), LogWriter.INFO );
                        
                model.LogProcesosSvc.finallyProceso("MigracionFacturasClientes_remesa_docto", this.hashCode(), usuario.getLogin(), "Exitoso");  
            
            
            logWriter.log("FIN DEL PROCESO ",LogWriter.INFO);
            model.LogProcesosSvc.finallyProceso("MigracionFacturasClientes_remesa_docto", this.hashCode(), usuario.getLogin(), "Proceso Exitoso!");
             
        }catch (Exception ex){
            ex.printStackTrace();
            try{
            logWriter.log("ERROR AL CORRER EL PROCESO ",LogWriter.INFO);
            model.LogProcesosSvc.finallyProceso("MigracionFacturasClientes_remesa_docto", this.hashCode(), usuario.getLogin(),  ex.getMessage());
            } catch (Exception e){}
           
        }
    } 
    
    public void procesarXLS(File f) throws Exception{
        try {
            ////System.out.println("ok");
            listado = new LinkedList();
            ////System.out.println("ruta:"+f.getAbsolutePath());
            JXLRead xls = new JXLRead (f.getAbsolutePath() );
            xls.obtenerHoja("BASE");
            String [] datos = null;
            int i = 1;
            while (  !isEmpty (datos = xls.getValores(i++)) ){
                
                ////System.out.println("datos:");
                for(int h=0; h<datos.length;h++){
                    ////System.out.print("-"+datos[h]);
                }
                listado.add(datos);
            }
            xls.cerrarLibro();
        }catch (Exception ex){
            throw new Exception("Error en la rutina procesarXLS en [HImportacionesCF] ....\n" + ex.getMessage());
        }
    }  
    /**
     * Metodo que indica si elo array contiene o no datos
     * @autor mfontalvo
     * @param datos, Arreglo de String
     * @return boolean , estado del array
     */
    private boolean isEmpty (String []datos ){
        boolean empty = true;
        if (datos!=null){
            for (int i = 0; i < datos.length && empty ; i++){
                if ( datos[i] !=null && !datos[i].trim().equals(""))
                    empty = false;
            }
        }
        return empty;
    }
}
