/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.threads;

/**
 *
 * @author Alvaro
 *
 */


import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.util.HSSFColor;

import com.tsp.operation.model.beans.POIWrite;

import com.tsp.operation.model.LogProcesosService;
import java.sql.SQLException;
import java.util.*;
import java.text.*;
import com.tsp.operation.model.beans.*;

import com.tsp.operation.model.Model;
import com.tsp.util.Util;

import java.io.*;
import com.tsp.util.LogWriter;





public class HIngresoEgresoTSPGenerar extends Thread {

    private Model   model;
    private Usuario usuario;
    private String  distrito;
    private String  processName;

    private SimpleDateFormat fmt;



    // -------------------------------------------------------------------------
    // DEFINICION PARA USAR EXCEL
    // Variables del archivo de excel
    String   rutaInformes;
    String   nombre;
    POIWrite xls;
    HSSFCellStyle header  , titulo1, titulo2, titulo3 , titulo4, titulo5, letra, numero, dinero, numeroCentrado, porcentaje, letraCentrada, numeroNegrita, ftofecha;
    HSSFColor     cAzul   , cVerde, cAmarillo, cGris ;
    int fila = 0;
    // FIN DEFINICION PARA USAR EXCEL
    // -------------------------------------------------------------------------














    /** Creates a new instance of HPrefacturaDetalle */
    public HIngresoEgresoTSPGenerar () {
    }

    public void start(Model model, Usuario usuario, String distrito){

        this.usuario  = usuario;
        this.model    = model;
        this.distrito = distrito;
        this.processName = "GENERACION INGRESOS TSP";

        super.start();
    }




    public synchronized void run(){
        LogProcesosService log = new LogProcesosService(usuario.getBd());
        String comentario="";
        try{

            log.InsertProceso(this.processName, this.hashCode(), "Generacion ingresos a partir de egresos TSP ",usuario.getLogin());
            //model.LogProcesosSvc.InsertProceso(this.processName, this.hashCode(), "Generacion ingresos a partir de egresos TSP " + usuario.getLogin());
            this.generarRUTA();
            this.crearLibro("ReporteIngresos_" ,"REPORTE DE INGRESOS ");

            // encabezado
            String [] cabecera = {
                "DISTRITO","BANCO","SUCURSAL","CHEQUE","FECHA", "VALOR EGRESO",
                "TIPO INGRESO", "INGRESO", "VALOR INGRESO",
                "ITEM NO","TIPO DOCUMENTO","DOCUMENTO","ITEM" ,
                "CONCEPTO","PLANILLA","DESCRIPCION ITEM FACTURA",
                "VLR ITEM EGRESO","VLR ITEM FACTURA",
                "DESCRIPCION ITEM EGRESO",
                "DISTRITO FACTURA",
                "TIPO DOCUMENTO FACTURA","DOCUMENTO FACTURA","ITEM FACTURA",
                "VALOR FACTURA","VALOR_SALDO_FACTURA",
                "CUENTA",
                "DISTRITO INGRESO","TIPO DOCUMENTO INGRESO","NUMERO INGRESO", "ITEM INGRESO"
            };

            short  [] dimensiones = new short [] {
                2000, 5500, 5500, 2000, 6000, 4500, 3000, 3000, 4000, 2000,
                4000, 6000, 2500, 3000, 3000, 6000, 5000, 5000, 6000, 4000,
                6000, 6000, 4000, 5000, 5000, 4000, 5000, 6000, 5000, 5000
            };

            this.generaTitulos(cabecera, dimensiones);
            this.crearIngreso(model,usuario,distrito);
            log.finallyProceso(this.processName, this.hashCode(), usuario.getLogin(),comentario);
        }catch (Exception ex){
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            System.out.println("Error HIngresoEgresoTSPGenerar ...\n" + ex.getMessage());
            try{
                log.finallyProceso(this.processName, this.hashCode(), usuario.getLogin(),comentario);
            }catch (Exception e){
                System.out.println("Error HIngresoEgresoTSPGenerar ...\n" + e.getMessage());
            }
        }
    }








public void crearIngreso(Model model,Usuario usuario,String distrito) throws Exception{

    List     listaEgreso        = null;
    List     listaEgresoItem    = null;
    FacturaFintra facturaFintra = null;
    Vector comandos_sql         = new Vector();
    facturaFintra               = new FacturaFintra();

    boolean       cruzado;
    double        valor_aplicado;




    // -------------------------------------------------------------------------
    // DEFINICION DEL LOG DE ERROR
    // Variables
    PrintWriter pw;
    LogWriter logWriter;
    java.text.SimpleDateFormat formatoFecha;
    String fechaDocumento;
    java.util.Date fecha;

    // Abriendo un log de error
    String user         = usuario.getLogin();
    ResourceBundle rb   = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
    String rutaInformes = rb.getString("ruta")+ "/exportar/migracion/"+user;

    File file = new File(rutaInformes);
    file.mkdirs();
    pw        = new PrintWriter(System.err, true);

    Calendar fechaProceso  = Calendar.getInstance();
    fecha                  = fechaProceso.getTime();
    formatoFecha           = new java.text.SimpleDateFormat("yyyy-MM-dd HHmm");
    fechaDocumento         = formatoFecha.format(fecha);

    String logFile   = rutaInformes + "/Elaboracion ingresos "+fechaDocumento+".txt";

    pw        = new PrintWriter(new FileWriter(logFile, true), true);
    logWriter = new LogWriter("EI-TSP", LogWriter.INFO, pw);
    logWriter.setPrintWriter(pw);

    // FIN DEFINICION DEL LOG DE ERROR
    // -------------------------------------------------------------------------

    try {

        logWriter.log("****************************************************************",LogWriter.INFO);
        logWriter.log("*  PROCESO PARA GENERACION DE INGRESOS A PARTIR DE EGRESOS TSP *",LogWriter.INFO);
        logWriter.log("*  FechaProceso :  "+fechaDocumento+"                             *",LogWriter.INFO);
        logWriter.log("****************************************************************" +"\n",LogWriter.INFO);


        // Crea tabla temporal de facturas realizadas a TSP que no han sido canceladas

        model.prontoPagoService.creaFacturaFintra();
        logWriter.log("CREACION DE TABLA TEMPORAL DE FACTURAS PENDIENTES DE CANCELAR (tem.factura_fintra)  \n",LogWriter.INFO);
        System.out.println("DESPUES DE LA CREACION TERMPORAL DE FACTURAS PENDIENTES DE CANCELAR");
        // lee los egresos
        listaEgreso = model.prontoPagoService.getEgresoTsp(distrito);
        System.out.println("DESPUES DE getEgresoTsp");
        if (listaEgreso.size() > 0) {

            Iterator it = listaEgreso.iterator();
            EgresoCabeceraTsp egreso = new EgresoCabeceraTsp();
            while (it.hasNext()) {
                egreso = (EgresoCabeceraTsp)it.next();
                // lee el detalle de un egreso
                System.out.println("antes de getegreso item"+egreso.getDstrct()+egreso.getBranch_code()+egreso.getBank_account_no()+ egreso.getDocument_no());
               listaEgresoItem = model.prontoPagoService.getEgresoItem(egreso.getDstrct(),egreso.getBranch_code(),
                                                                       egreso.getBank_account_no(), egreso.getDocument_no()) ;
               System.out.println("DESPUES DE getEgresoItem"); 
               if (listaEgresoItem.size() > 0) {
                    Iterator it1 = listaEgresoItem.iterator();
                    EgresoItem egresoItem = new EgresoItem();

                    cruzado = false;
                    valor_aplicado = 0.00;
                    boolean sw_cruce=true;//
                    while (it1.hasNext()){
                        egresoItem = (EgresoItem)it1.next();
                        facturaFintra = localizaFactura(egreso, egresoItem, logWriter);
                        if (facturaFintra != null){
                            egresoItem.setValor_saldo_factura_original(facturaFintra.getValor_saldo());
                            // Al menos un item del egreso cruz� con una factura
                            cruzado = true;

                            valor_aplicado = valor_aplicado + egresoItem.getVlr_item_factura();

                            // Actualiza el registro en factura_fintra con el numero del egreso que ha cruzado
                            model.prontoPagoService.actualizaFacturaFintra(egreso.getDstrct(), egreso.getBranch_code() ,
                                                                           egreso.getBank_account_no(), egreso.getDocument_no(),
                                                                           egresoItem.getItem_no(),
                                                                           "FINV",
                                                                           facturaFintra.getTipo_documento(),
                                                                           facturaFintra.getDocumento(),
                                                                           facturaFintra.getItem() ) ;

                        }else{
                            //sw_cruce=false;
                            System.out.println("no cruzo el egresitem"+egresoItem.getDocumento()+"_"+egresoItem.getItem());
                            logWriter.log(" no cruzo el item de egreso:"+egresoItem.getDocumento()+"_"+egresoItem.getItem()          ,LogWriter.INFO);
                        }
                    } // fin del while de egresoItem
                    /*if (sw_cruce==false){
                        cruzado=false;
                    }*/
                    if (cruzado){
                        // Al menos un item del egreso cruzo con una factura
                        boolean procesoOK = registrarIngreso(comandos_sql,egreso,listaEgresoItem, usuario, logWriter);

                        if (procesoOK) {
                            System.out.println("antes de regingdet");
                            procesoOK  = registrarIngresoDetalle(comandos_sql,egreso,listaEgresoItem, usuario, logWriter);
                        }
                        if (procesoOK) {
                            procesoOK  = registrarEgresoTsp (comandos_sql, egreso, usuario, logWriter);
                        }
                        if (procesoOK) {
                            procesoOK  = registrarEgresoDetalleTSP(comandos_sql, egreso, listaEgresoItem, usuario, logWriter);
                        }
                        if (procesoOK) {
                            procesoOK  = registrarFactura(comandos_sql, egreso, listaEgresoItem , logWriter);
                        }

                        // Reconfirma que todos los procesos terminaron OK para proceder a actualizar en forma integral sin inconsistencia
                        if  (procesoOK) {
                            // Grabando todo a la base de datos.

                            System.out.println("Antes de generar archivo");
                            generarArchivo(egreso,listaEgresoItem);
                            System.out.println("Despues de generar archivo");
                           
                            System.out.println("Antes del ejecutar");
                            model.applusService.ejecutarSQL(comandos_sql);
                            System.out.println("Despues del ejecutar");
                            comandos_sql.removeAllElements();

                            logWriter.log("EGRESO PROCESADO:" + egreso.getBranch_code()     + " , "
                                                               + egreso.getBank_account_no() + " , "
                                                               + egreso.getDocument_no()     ,LogWriter.INFO);


                            




                        }
                    }
                    else {
                        String banco         = egreso.getBranch_code();
                        String sucursal      = egreso.getBank_account_no();
                        String cheque        = egreso.getDocument_no();
                        String fechaCreacion = egreso.getCreation_date();
                        String valor_cheque  = Util.FormatoMiles(egreso.getVlr() );

                        logWriter.log("\n"+"EGRESO NO CRUZADO",LogWriter.INFO);
                        logWriter.log("     Banco    : "+banco          ,LogWriter.INFO);
                        logWriter.log("     Sucursal : "+sucursal       ,LogWriter.INFO);
                        logWriter.log("     Cheque   : "+cheque         ,LogWriter.INFO);
                        logWriter.log("     Fecha    : "+fechaCreacion  ,LogWriter.INFO);
                        logWriter.log("     Valor    : "+valor_cheque   + "\n",LogWriter.INFO);
                    }

                } // fin de la existencia egresoItem

            }  // fin del while de egreso
            System.out.println("antes de cerrar file");

            this.cerrarArchivo();
            logWriter.log("CREACION DE REPORTE INGRESO DE EXCEL \n",LogWriter.INFO);

        }  // fin de la existencia de egreso a procesar



        // generaReporteFacturasNocruzadas();    // No factible por out of memory heap size
        // logWriter.log("CREACION DE REPORTE FACTURAS DE EXCEL \n",LogWriter.INFO);


        java.text.SimpleDateFormat formatoFechaFinal;
        Calendar fechaFinalProceso  = Calendar.getInstance();
        fecha                       = fechaFinalProceso.getTime();
        formatoFechaFinal           = new java.text.SimpleDateFormat("yyyy-MM-dd HH.mm:ss");
        fechaDocumento              = formatoFechaFinal.format(fecha);



        logWriter.log("****************************************************************",LogWriter.INFO);
        logWriter.log("*  FINAL DEL PROCESO " + fechaDocumento + "                       *",LogWriter.INFO);
        logWriter.log("****************************************************************",LogWriter.INFO);

    }catch (Exception e){
        System.out.println("Error ProntoPagoAccederAction, procedimiento crearIngreso  ...\n"  + e.getMessage());
    }



}

public FacturaFintra localizaFactura(EgresoCabeceraTsp egreso, EgresoItem egresoItem, LogWriter logWriter )throws SQLException {
    System.out.println("egresoItem"+egresoItem.getDescripcion_item_factura()+"egresoItem.getConcepto()"+egresoItem.getConcepto());
    FacturaFintra facturaFintra = null;
    facturaFintra = new FacturaFintra();

    try {

        // Localizar una factura que coincida con el egreso

        // CASO EN QUE EL EGRESO ES UN ANTICIPO

         if ( (egresoItem.getConcepto().equalsIgnoreCase("ANTICIPO"))  ||
             (egresoItem.getConcepto().equalsIgnoreCase("FINTRA GASOLINA")) ) {
                 System.out.println("egresoItem.getPlanilla()"+egresoItem.getPlanilla()+"egresoItem.getVlr_item_factura()"+egresoItem.getVlr_item_factura()+"_1");
            facturaFintra = model.prontoPagoService.getFacturaFintra("PL", egresoItem.getPlanilla(),
                                                                     "EXT", "", egresoItem.getVlr_item_factura(),"1" );
        }
        else if (egresoItem.getConcepto().equalsIgnoreCase("PP FINTRA")){
            System.out.println("egresoItem.getPlanilla()"+egresoItem.getPlanilla()+"egresoItem.getVlr_item_factura()"+egresoItem.getVlr_item_factura()+"_2");
            facturaFintra = model.prontoPagoService.getFacturaFintra("PL", egresoItem.getPlanilla(),
                                                                     "EXT", "",  egresoItem.getVlr_item_factura() ,"2");
        }
        else if (egresoItem.getConcepto().equalsIgnoreCase("")){
            System.out.println("egresoItem.getDocumento()"+egresoItem.getDocumento()+"egresoItem.getVlr_item_factura() "+egresoItem.getVlr_item_factura() );
            facturaFintra = model.prontoPagoService.getFacturaFintra("PR", egresoItem.getDocumento(),
                                                                     "EXT",  "", egresoItem.getVlr_item_factura() ,"2");
        }
        else if (egresoItem.getConcepto().equalsIgnoreCase("50")){

            String descripcion_item_egreso = egresoItem.getDescripcion_item_egreso();
            String descripcion_item_factura = egresoItem.getDescripcion_item_factura();
            //int largo = descripcion_item_egreso.length();
            //String numero_operacion = descripcion_item_egreso.substring(largo-6+1);

            String numero_operacion = descripcion_item_egreso.substring(descripcion_item_egreso.lastIndexOf("L")+1);//091228

            String tipo_factura = "";
            String factura = "";   
            String planilla = egresoItem.getPlanilla();
            //if (planilla != null){   
            //tipo_factura = "PR";
            //factura = descripcion_item_factura.substring(13);            
            
            
            if (!planilla.equals("")){ 
                String documento = egresoItem.getDocumento();
                String sigla = documento.substring(0,5);
                if (sigla.equals("PP OP")) {
                    tipo_factura = "PL";
                    factura = egresoItem.getPlanilla();
                }
                else {
                   tipo_factura = "PR";
                   factura = descripcion_item_factura.substring(13);                  
                }
            }
            else {
                tipo_factura = "PR";
                factura = descripcion_item_factura.substring(13);
            }
            
            System.out.println("tipo_factura"+tipo_factura+"factura"+factura+"EXT_numero_operacion"+numero_operacion+"egresoItem.getVlr_item_factura()"+egresoItem.getVlr_item_factura()+"_3");
            facturaFintra = model.prontoPagoService.getFacturaFintra(tipo_factura, factura,
                                                                     "EXT",numero_operacion,
                                                                     egresoItem.getVlr_item_factura() ,"3");
        }
         //.out.println("facturaFintra"+facturaFintra);
        if (facturaFintra != null){
            // Se localizo una factura que cumple con los parametros del egreso item
            facturaFintra.setDstrct_egreso(egreso.getDstrct());
            facturaFintra.setBranch_code_egreso(egreso.getBranch_code());
            facturaFintra.setBank_account_no_egreso(egreso.getBank_account_no());
            facturaFintra.setDocument_no_egreso(egreso.getDocument_no());
            facturaFintra.setItem_no_egreso(egresoItem.getItem());

            egresoItem.setDstrct_factura("FINV");
            egresoItem.setTipo_documento_factura(facturaFintra.getTipo_documento());
            //.out.println("facturaFintra.getTipo_documento()"+facturaFintra.getTipo_documento()+"facturaFintra.getDocumento()"+facturaFintra.getDocumento());
            egresoItem.setDocumento_factura(facturaFintra.getDocumento());
            egresoItem.setItem_factura(facturaFintra.getItem());
            egresoItem.setValor_factura(facturaFintra.getValor_factura());
            egresoItem.setValor_saldo(facturaFintra.getValor_saldo());

            String cuenta = model.prontoPagoService.getCuentaTipoOperacion(facturaFintra.getTipo_operacion() );
            egresoItem.setCuenta(cuenta);

        }

        /* System.out.println(egresoItem.getItem() + "  " +
                           egresoItem.getConcepto() + "  " +
                           egresoItem.getPlanilla() + "  " +
                           egresoItem.getVlr_item_factura() + "  " +
                           egresoItem.getDstrct_factura() + "  " +
                           egresoItem.getTipo_documento_factura() + "  " +
                           egresoItem.getDocumento_factura() + "  " +
                           egresoItem.getItem_factura() );
        */
    }catch(Exception e){


        logWriter.log("ERROR EN METODO localizaFactura " + "\n",LogWriter.INFO);
        logWriter.log("     Banco    : " + egreso.getBranch_code() ,LogWriter.INFO);
        logWriter.log("     Sucursal : " + egreso.getBank_account_no() ,LogWriter.INFO);
        logWriter.log("     Cheque   : " + egreso.getDocument_no() + "\n",LogWriter.INFO);
        System.out.println(e.getMessage());
        e.printStackTrace();

        throw new SQLException("ERROR DURANTE LA INSERCION DE UN REGISTRO EN INGRESO. \n " + e.getMessage());
    }
    System.out.println("facturaFintra"+facturaFintra);
    return facturaFintra;
}


public boolean registrarIngreso(Vector comandos_sql,EgresoCabeceraTsp egreso, List listaEgresoItem, Usuario usuario, LogWriter logWriter) throws SQLException{

    String comandoSQL = "";
    boolean procesoOK = true;

    try {

        SerieGeneral serie = null;
        serie = new SerieGeneral();
        serie = model.serieGeneralService.getSerie("FINV","OP","INGC");
        model.serieGeneralService.setSerie("FINV","OP","INGC");

        String dstrct = "FINV";
        String tipo_documento = "ING";
        String num_ingreso = serie.getUltimo_prefijo_numero();
        String codcli = "CL00749";
        String nitcli = "8901031611";
        String concepto = "SP";
        String tipo_ingreso = "C";
        String fecha_consignacion = egreso.getFecha_cheque();
        String fecha_ingreso = egreso.getFecha_cheque();
        String branch_code = "CAJA TSP";
        String bank_account_no = "TSP";
        String codmoneda = "PES";
        String agencia_ingreso = "OP";
        String descripcion_ingreso = "Ingreso del egreso TSP: "+ " distrito: " + egreso.getDstrct() +
                                                                 " banco: " + egreso.getBranch_code() +
                                                                 " sucursal: " + egreso.getBank_account_no() +
                                                                 " cheque: " + egreso.getDocument_no();
        String periodo = "";
        Double vlr_ingreso = egreso.getVlr();
        double vlr_ingreso_me = egreso.getVlr();
        double vlr_tasa = 1.00;
        String fecha_tasa = egreso.getFecha_cheque();

        Iterator it = listaEgresoItem.iterator();


        int cant_item = 0;

        cant_item = listaEgresoItem.size();

        int transaccion = 0;
        int transaccion_anulacion = 0;

        String fecha_impresion = egreso.getFecha_cheque()+ " 00:00:00";
        String fecha_contabilizacion = "0099-01-01 00:00:00";
        String fecha_anulacion_contabilizacion = "0099-01-01 00:00:00";
        String fecha_anulacion = "0099-01-01 00:00:00";
        String creation_user = usuario.getLogin();

        java.util.Date fechaActual = new Date();
        String creation_date = fechaActual.toString();

        String user_update = usuario.getLogin();
        String last_update = creation_date;
        String base = "COL";
        String nro_consignacion = "";
        String periodo_anulacion = "";

        String cuenta = "11050508";
        String auxiliar = "";
        String abc = "";

        double tasa_dol_bol = 0.00;
        double saldo_ingreso = 0.00;

        String cmc = "";
        String corficolombiana = "";

        comandoSQL = model.prontoPagoService.setIngreso(
                                           "", dstrct, tipo_documento,
                                           num_ingreso, codcli, nitcli, concepto, tipo_ingreso,
                                           fecha_consignacion, fecha_ingreso, branch_code,
                                           bank_account_no, codmoneda, agencia_ingreso,
                                           descripcion_ingreso, periodo, vlr_ingreso,
                                           vlr_ingreso_me, vlr_tasa, fecha_tasa, cant_item,
                                           transaccion, transaccion_anulacion, fecha_impresion,
                                           fecha_contabilizacion, fecha_anulacion_contabilizacion,
                                           fecha_anulacion, creation_user, creation_date,
                                           user_update, last_update, base, nro_consignacion,
                                           periodo_anulacion, cuenta, auxiliar, abc,
                                           tasa_dol_bol, saldo_ingreso, cmc, corficolombiana);


        comandos_sql.add(comandoSQL);


        egreso.setTipo_documento_ingreso(tipo_documento);
        egreso.setNum_ingreso(num_ingreso);
        egreso.setFecha_creacion_ingreso(fecha_ingreso);

    }catch(Exception e){
        procesoOK = false;

        logWriter.log("ERROR EN METODO registrarIngreso " + "\n",LogWriter.INFO);
        logWriter.log("     Banco    : " + egreso.getBranch_code() ,LogWriter.INFO);
        logWriter.log("     Sucursal : " + egreso.getBank_account_no() ,LogWriter.INFO);
        logWriter.log("     Cheque   : " + egreso.getDocument_no() + "\n",LogWriter.INFO);
        System.out.println(e.getMessage());

        e.printStackTrace();

        throw new SQLException("ERROR DURANTE LA INSERCION DE UN REGISTRO EN INGRESO. \n " + e.getMessage());
    }

    return procesoOK;

}





public boolean registrarIngresoDetalle(Vector comandos_sql,EgresoCabeceraTsp egreso, List listaEgresoItem,
                                       Usuario usuario, LogWriter logWriter) throws SQLException{

    String comandoSQL = "";
    boolean procesoOK = true;

    try {


        EgresoItem egresoItem = new EgresoItem();

        int item = 0;

        // Datos comunes a todos los items del ingreso
        String dstrct = "FINV";
        String tipo_documento = "ING";
        String num_ingreso = egreso.getNum_ingreso();
        String nitcli = "8901031611";
        String codigo_retefuente = "";
        double valor_retefuente = 0.00;
        double valor_retefuente_me = 0.00;
        String tipo_doc = "FAC";
        String codigo_reteica = "";
        double valor_reteica = 0.00;
        double valor_reteica_me = 0.00;
        double valor_diferencia_tasa = 0.00;

        String creation_user = usuario.getLogin();
        java.util.Date fechaActual = new Date();

        String creation_date = fechaActual.toString();
        String user_update = usuario.getLogin();
        String last_update = creation_date;
        String base = "COL";
        String auxiliar = "";
        String fecha_contabilizacion = "0099-01-01 00:00:00";
        String fecha_anulacion_contabilizacion = "0099-01-01 00:00:00";
        String periodo = "";
        String fecha_anulacion = "0099-01-01 00:00:00";
        String periodo_anulacion = "";
        int transaccion= 0;
        int transaccion_anulacion = 0;
        double valor_tasa = 1.00;
        double saldo_factura = 0.00;


        Iterator it = listaEgresoItem.iterator();
        while (it.hasNext()){
            
            egresoItem = (EgresoItem)it.next();
            System.out.println("descrip:"+egresoItem.getDescripcion_item_factura()+"__"+egresoItem.getItem());
            if (!egresoItem.getDocumento_factura().equals("")) {

                // ITEM DEL EGRESO SI CRUZO CON UNA FACTURA DE FINTRA

                // Datos especificos para un item de ingreso que si cruzo
                item++;
                double valor_ingreso = egresoItem.getVlr_item_factura();
                double valor_ingreso_me = valor_ingreso;
                String factura = egresoItem.getDocumento_factura();
                String fecha_factura = "0099-01-01";
                String documento = factura ;
                String cuenta = egresoItem.getCuenta();
                saldo_factura = egresoItem.getValor_saldo_factura_original();
                System.out.println("1 factura :  "+factura);


                String descripcion = "Egreso TSP cruzado: "+ " Distrito: " + egreso.getDstrct() +
                                     " Banco: " + egreso.getBranch_code() +
                                     " Sucursal: " + egreso.getBank_account_no() +
                                     " Cheque: " + egreso.getDocument_no() +
                                     " Item cheque: " + egresoItem.getItem() +
                                     " Tipo factura TSP : "+ egresoItem.getTipo_documento() +
                                     " Factura TSP : " + egresoItem.getDocumento() +
                                     " Item factura TSP: " + egresoItem.getItem() +
                                     " Descripcion item factura TSP: " + egresoItem.getDescripcion_item_factura() +
                                     " Concepto: " + egresoItem.getConcepto()+
                                     " Planilla: " + egresoItem.getPlanilla() +
                                     " Descripcion item egreso: " + egresoItem.getDescripcion_item_egreso() +
                                     " Distrito factura fintra: " + egresoItem.getDstrct_factura() +
                                     " Tipo documento factura fintra : " + egresoItem.getTipo_documento_factura() +
                                     " Factura fintra: " + egresoItem.getDocumento_factura() +
                                     " Item factura fintra: " + egresoItem.getItem_factura() ;

                comandoSQL = model.prontoPagoService.setIngresoDetalle( "", dstrct, tipo_documento, num_ingreso, item, nitcli,
                    valor_ingreso, valor_ingreso_me, factura, fecha_factura, codigo_retefuente,
                    valor_retefuente, valor_retefuente_me, tipo_doc, documento, codigo_reteica,
                    valor_reteica, valor_reteica_me, valor_diferencia_tasa, creation_user,
                    creation_date, user_update, last_update, base, cuenta, auxiliar,
                    fecha_contabilizacion, fecha_anulacion_contabilizacion, periodo,
                    fecha_anulacion, periodo_anulacion, transaccion, transaccion_anulacion,
                    descripcion,  valor_tasa, saldo_factura);
                
                System.out.println("1 "+comandoSQL);
                comandos_sql.add(comandoSQL);

            }
            else {
                // ITEM DEL EGRESO NO CRUZO CON NINGUNA FACTURA DE FINTRA

                // Datos especificos para un item de ingreso que noi cruzo
                item++;
                double valor_ingreso = egresoItem.getVlr_item_factura();
                double valor_ingreso_me = valor_ingreso;
                String factura = egresoItem.getDocumento_factura();
                String fecha_factura = "0099-01-01";
                String documento = factura ;
                System.out.println("2 "+factura);

                // Manejo de cuentas cuando el item del egreso no cruza
                String cuenta = "";
                if (egresoItem.getPlanilla().equals("") ) {
                    cuenta = "28050508";
                    egresoItem.setCuenta("28050508");
                }
                else {
                    cuenta = "28050509";
                    egresoItem.setCuenta("28050509");
                }

                String descripcion = "Egreso TSP no cruzado: "+ " Distrito: " + egreso.getDstrct() +
                                     " Banco: " + egreso.getBranch_code() +
                                     " Sucursal: " + egreso.getBank_account_no() +
                                     " Cheque: " + egreso.getDocument_no() +
                                     " Item cheque: " + egresoItem.getItem() +
                                     " Tipo factura TSP : "+ egresoItem.getTipo_documento() +
                                     " Factura TSP : " + egresoItem.getDocumento() +
                                     " Item factura TSP: " + egresoItem.getItem() +
                                     " Descripcion item factura TSP: " + egresoItem.getDescripcion_item_factura() +
                                     " Concepto: " + egresoItem.getConcepto()+
                                     " Planilla: " + egresoItem.getPlanilla() +
                                     " Descripcion item egreso: " + egresoItem.getDescripcion_item_egreso();


                comandoSQL = model.prontoPagoService.setIngresoDetalle( "", dstrct, tipo_documento, num_ingreso, item, nitcli,
                    valor_ingreso, valor_ingreso_me, factura, fecha_factura, codigo_retefuente,
                    valor_retefuente, valor_retefuente_me, tipo_doc, documento, codigo_reteica,
                    valor_reteica, valor_reteica_me, valor_diferencia_tasa, creation_user,
                    creation_date, user_update, last_update, base, cuenta, auxiliar,
                    fecha_contabilizacion, fecha_anulacion_contabilizacion, periodo,
                    fecha_anulacion, periodo_anulacion, transaccion, transaccion_anulacion,
                    descripcion,  valor_tasa, saldo_factura);

                System.out.println("2 factura :  "+comandoSQL);
                comandos_sql.add(comandoSQL);
               

            }

            // Marca el item del egreso con el item del ingreso que le corresponde
            egresoItem.setDstrct_ingreso(dstrct);
            egresoItem.setTipo_documento_ingreso(tipo_documento);
            egresoItem.setNum_ingreso(num_ingreso);
            egresoItem.setItem_ingreso(item);

        }

    }catch(Exception e){
        procesoOK = false;
        
        logWriter.log("ERROR EN METODO registrarIngresoDetalle " + "\n",LogWriter.INFO);
        logWriter.log("     Banco    : " + egreso.getBranch_code() ,LogWriter.INFO);
        logWriter.log("     Sucursal : " + egreso.getBank_account_no() ,LogWriter.INFO);
        logWriter.log("     Cheque   : " + egreso.getDocument_no() + "\n",LogWriter.INFO);

        System.out.println(e.getMessage());
        e.printStackTrace();
        throw new SQLException("ERROR DURANTE LA INSERCION DE UN REGISTRO EN INGRESO DETALLE. \n " + e.getMessage());
    }

    return procesoOK;

}





public boolean registrarEgresoTsp(Vector comandos_sql,EgresoCabeceraTsp egreso, Usuario usuario, LogWriter logWriter) throws SQLException{

    String comandoSQL = "";
    boolean procesoOK = true;

    try {



        comandoSQL = model.prontoPagoService.setEgresoTsp(egreso.getTipo_documento_ingreso(),
                                                          egreso.getNum_ingreso(),
                                                          egreso.getFecha_creacion_ingreso(),
                                                          egreso.getDstrct(),
                                                          egreso.getBranch_code(),
                                                          egreso.getBank_account_no(),
                                                          egreso.getDocument_no()) ;



        comandos_sql.add(comandoSQL);

    }catch(Exception e){
        procesoOK = false;

        logWriter.log("ERROR EN METODO registrarEgresoTsp " + "\n",LogWriter.INFO);
        logWriter.log("     Banco    : " + egreso.getBranch_code() ,LogWriter.INFO);
        logWriter.log("     Sucursal : " + egreso.getBank_account_no() ,LogWriter.INFO);
        logWriter.log("     Cheque   : " + egreso.getDocument_no() + "\n",LogWriter.INFO);
        System.out.println(e.getMessage());
        e.printStackTrace();
        throw new SQLException("ERROR DURANTE LA MARCACION DE UN EGRESO DE TSP CON EL NUMERO DE INGRESO. \n " + e.getMessage());
    }

    return procesoOK;

}


public boolean registrarEgresoDetalleTSP(Vector comandos_sql,EgresoCabeceraTsp egreso, List listaEgresoItem,
                                         Usuario usuario, LogWriter logWriter) throws SQLException{

    String comandoSQL = "";
    boolean procesoOK = true;

    EgresoItem egresoItem = new EgresoItem();

    try {

        Iterator it = listaEgresoItem.iterator();
        while (it.hasNext()){
            egresoItem = (EgresoItem)it.next();

            // Actualiza el registro en egresodet_tsp con numero de factura que cruzo
            comandoSQL = model.prontoPagoService.setEgresodetTsp(egresoItem.getDstrct_factura(),
                                                    egresoItem.getTipo_documento_factura()   ,
                                                    egresoItem.getDocumento_factura(),
                                                    egresoItem.getItem_factura(),

                                                    egresoItem.getDstrct_ingreso(),
                                                    egresoItem.getTipo_documento_ingreso(),
                                                    egresoItem.getNum_ingreso(),
                                                    egresoItem.getItem_ingreso(),

                                                    egreso.getDstrct(),
                                                    egreso.getBranch_code(),
                                                    egreso.getBank_account_no(),
                                                    egreso.getDocument_no(),
                                                    egresoItem.getItem_no() );
            comandos_sql.add(comandoSQL);

        }

    }catch(Exception e){
        procesoOK = false;

        logWriter.log("ERROR EN METODO registrarEgresoDetalleTSP " + "\n",LogWriter.INFO);
        logWriter.log("     Banco    : " + egreso.getBranch_code() ,LogWriter.INFO);
        logWriter.log("     Sucursal : " + egreso.getBank_account_no() ,LogWriter.INFO);
        logWriter.log("     Cheque   : " + egreso.getDocument_no() + "\n",LogWriter.INFO);
        System.out.println(e.getMessage());
        e.printStackTrace();
        throw new SQLException("ERROR DURANTE LA MARCACION DE UN EGRESO DETALLADO DE TSP CON EL NUMERO DE INGRESO. \n " + e.getMessage());
    }

    return procesoOK;

}




public boolean registrarFactura(Vector comandos_sql ,EgresoCabeceraTsp egreso, List listaEgresoItem,
                                LogWriter logWriter ) throws SQLException{

    String comandoSQL = "";
    boolean procesoOK = true;

    EgresoItem egresoItem = new EgresoItem();

    try {

        Iterator it = listaEgresoItem.iterator();
        while (it.hasNext()){
            egresoItem = (EgresoItem)it.next();
            if (!egresoItem.getDocumento_factura().equals("")){
                // Actualiza la factura con los abonos recibidos del egreso de TSP
                comandoSQL = model.prontoPagoService.setFactura(egresoItem.getVlr_item_factura(),
                                                        egresoItem.getDstrct_factura(),
                                                        egresoItem.getTipo_documento_factura()   ,
                                                        egresoItem.getDocumento_factura(),
                                                        egresoItem.getDstrct_ingreso(),
                                                        egresoItem.getTipo_documento_ingreso(),
                                                        egresoItem.getNum_ingreso(),
                                                        egresoItem.getItem_ingreso() );
                comandos_sql.add(comandoSQL);

                // Actualiza el item de la  factura con los abonos recibidos del egreso de TSP
                comandoSQL = model.prontoPagoService.setFacturaDetalle(egresoItem.getVlr_item_factura(),
                                                        egresoItem.getDstrct_factura(),
                                                        egresoItem.getTipo_documento_factura()   ,
                                                        egresoItem.getDocumento_factura(),
                                                        egresoItem.getItem_factura(),
                                                        egresoItem.getDstrct_ingreso(),
                                                        egresoItem.getTipo_documento_ingreso(),
                                                        egresoItem.getNum_ingreso(),
                                                        egresoItem.getItem_ingreso() );
                comandos_sql.add(comandoSQL);



            }

        }

    }catch(Exception e){
        procesoOK = false;

        logWriter.log("ERROR EN METODO registrarFactura " + "\n",LogWriter.INFO);
        logWriter.log("     Banco    : " + egreso.getBranch_code() ,LogWriter.INFO);
        logWriter.log("     Sucursal : " + egreso.getBank_account_no() ,LogWriter.INFO);
        logWriter.log("     Cheque   : " + egreso.getDocument_no() + "\n",LogWriter.INFO);
        System.out.println(e.getMessage());
        e.printStackTrace();
        throw new SQLException("ERROR DURANTE LA ACTUALIZACION DE LA FACTURA CANCELADA POR UN EGRESO DE TSP. \n " + e.getMessage());
    }

    return procesoOK;

}




public void generaReporteFacturasNocruzadas() throws SQLException {

    List listaFacturaFintra;

    try {

    // this.generarRUTA();   // Ya generada en proceso anterior
    this.crearLibro("Reporte facturas_", "REPORTE DE FACTURAS ");

    // encabezado
    String [] cabecera = {
        "DISTRITO","TIPO DOCUMENTO","DOCUMENTO","ITEM","DESCRIPCION", "VALOR FACTURA",
        "VALOR SALDO", "VALOR ITEM", "VALOR INGRESO",
        "TIPO OPERACION","NUMERO OPERACION","TIPO FACTURA","FACTURA" ,
        "CLASE"
    };

    short  [] dimensiones = new short [] {
        2000, 5500, 5500, 2000, 6000, 4500, 3000, 3000, 4000, 2000,
        4000, 6000, 2500, 3000
    };

    this.generaTitulos(cabecera,dimensiones);
    listaFacturaFintra = model.prontoPagoService.getListaFacturaFintra();
    generaArchivoFacturas(listaFacturaFintra);
    this.cerrarArchivo();


    }catch(Exception e){
        System.out.println(e.getMessage());
        e.printStackTrace();
        throw new SQLException("ERROR DURANTE LA CREACION DEL REPORTE DE FACTURAS PENDIENTES. \n " + e.getMessage());
    }


}

















    public void generarRUTA() throws Exception{
        try{

            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            rutaInformes = rb.getString("ruta") + "/exportar/migracion/" + usuario.getLogin();
            File archivo = new File( rutaInformes );
            if (!archivo.exists()) archivo.mkdirs();

        }catch (Exception ex){
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }

    }

    private void crearLibro(String nameFileParcial, String titulo) throws Exception {
        try{
            fmt= new SimpleDateFormat("yyyMMdd_hhmmss");
            this.crearArchivo(nameFileParcial + fmt.format( new Date() ) +".xls", titulo);

        }catch (Exception ex){
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception("Error en generarArchivo ...\n" + ex.getMessage() );
        }
    }





    /**
     * Metodo para crear el  archivo de excel
     * @autor apabon
     * @param nameFile, Nombre del archivo
     * @param titulo, titulo del archivo de excel
     * @throws Exception.
     */
    private void crearArchivo(String nameFile, String titulo) throws Exception{
        try{
            fmt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            InitArchivo(nameFile);
            xls.obtenerHoja("Base");
            xls.combinarCeldas(0, 0, 0, 8);
            xls.adicionarCelda(0,0, titulo, header);
            xls.adicionarCelda(1,0, "FECHA" , titulo1);
            xls.adicionarCelda(1,1, fmt.format( new Date())  , titulo1 );
            //xls.adicionarCelda(2,0, "PERIODO", titulo1);
            //xls.adicionarCelda(2,1, finicial + " - " + ffinal  , titulo1);
            xls.adicionarCelda(2,0, "USUARIO", titulo1);
            xls.adicionarCelda(2,1, usuario.getNombre() , titulo1);


        }catch (Exception ex){
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception( "Error en crearArchivo ....\n"  + ex.getMessage() );
        }
    }




    /**
     * Metodo para Incializar las variables del archivo de excel
     * @autor apabon
     * @param nameFile, nombre del archivo
     * throws Exception.
     */
    private void InitArchivo(String nameFile) throws Exception{
        try{
            xls          = new com.tsp.operation.model.beans.POIWrite();
            nombre       = "/exportar/migracion/" + usuario.getLogin() + "/" + nameFile;
            xls.nuevoLibro( rutaInformes + "/" + nameFile );

            // colores
            /*cAzul       = xls.obtenerColor(  0, 51,102);
            cVerde      = xls.obtenerColor( 51,153,102);
            cAmarillo   = xls.obtenerColor(255,255,153);
            cGris       = xls.obtenerColor(192,192,192);*/


            // estilos

            header         = xls.nuevoEstilo("Tahoma", 14, true  , false, "text"  , HSSFColor.ORANGE.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
            titulo1        = xls.nuevoEstilo("Tahoma", 8 , true  , false, "text"  , xls.NONE  , xls.NONE , xls.NONE);
            titulo2        = xls.nuevoEstilo("Tahoma", 8 , true  , false, "text"  , HSSFColor.WHITE.index, HSSFColor.DARK_BLUE.index , HSSFCellStyle.ALIGN_CENTER, 2);
            //titulo3        = xls.nuevoEstilo("Tahoma", 8 , true  , false, "text"  , HSSFColor.WHITE.index, cAmarillo.getIndex() , HSSFCellStyle.ALIGN_CENTER, 2);
            //titulo4        = xls.nuevoEstilo("Tahoma", 8 , true  , false, "text"  , HSSFColor.WHITE.index, cVerde.getIndex() , HSSFCellStyle.ALIGN_CENTER, 2);
            //titulo5        = xls.nuevoEstilo("Tahoma", 14, true  , false, "text"  , xls.NONE  , xls.NONE , xls.NONE);


            letra          = xls.nuevoEstilo("Tahoma", 8 , false , false, ""      , xls.NONE , xls.NONE , xls.NONE);


            letraCentrada  = xls.nuevoEstilo("Tahoma", 8 , false , false, ""      , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_CENTER);
            numero         = xls.nuevoEstilo("Tahoma", 8 , false , false, "#"     , xls.NONE , xls.NONE , xls.NONE);
            numeroCentrado = xls.nuevoEstilo("Tahoma", 8 , false , false, "#"     , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_CENTER);
            dinero         = xls.nuevoEstilo("Tahoma", 8 , false , false, "#,##0.00" , xls.NONE , xls.NONE , xls.NONE);
            numeroNegrita  = xls.nuevoEstilo("Tahoma", 8 , true  , false, "#"     , xls.NONE , xls.NONE , xls.NONE);
            porcentaje     = xls.nuevoEstilo("Tahoma", 8 , false , false, "0.00%" , xls.NONE , xls.NONE , xls.NONE);
            ftofecha       = xls.nuevoEstilo("Tahoma", 8 , false , false, "yyyy-mm-dd" , xls.NONE , xls.NONE , xls.NONE);

        }catch (Exception ex){
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception("Error en InitArchivo .... \n" + ex.getMessage());
        }

   }






    /**
     * Metodo para cerrar el  archivo de excel
     * @autor apabon
     * @throws Exception.
     */
    private void cerrarArchivo() throws Exception {
        try{
            if (xls!=null)
                xls.cerrarLibro();
        }catch (Exception ex){
            System.out.println(ex.getMessage());
            throw new Exception( "Error en crearArchivo ....\n"  + ex.getMessage() );
        }
    }


    private void generaTitulos(String [] cabecera, short[] dimensiones) throws Exception {
        try{

            fila = 6;

            for ( int i = 0; i<cabecera.length; i++){
                xls.adicionarCelda(fila,  i, cabecera[i], titulo2);
                if ( i < dimensiones.length )
                    xls.cambiarAnchoColumna(i, dimensiones[i] );
            }

        }catch (Exception ex){
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception("Error en generarArchivo ...\n" + ex.getMessage() );
        }
    }





    /**
     * Exportacion de una prefactura detallada
     * @autor apabon
     * @throws Exception.
     */
    private void generarArchivo(EgresoCabeceraTsp egreso, List listaEgresoItem) throws Exception {
        try{

            // Inicia el proceso de listar a excel

           EgresoItem egresoItem = new EgresoItem();
           Iterator it = listaEgresoItem.iterator();
           while (it.hasNext()){

               egresoItem = (EgresoItem)it.next();

               fila++;
               int col = 0;

               xls.adicionarCelda(fila  , col++ , egreso.getDstrct(), letra  );
               xls.adicionarCelda(fila  , col++ , egreso.getBranch_code(), letra  );
               xls.adicionarCelda(fila  , col++ , egreso.getBank_account_no(), letra  );
               xls.adicionarCelda(fila  , col++ , egreso.getDocument_no(), letra  );
               xls.adicionarCelda(fila  , col++ , egreso.getCreation_date(), letra  );
               xls.adicionarCelda(fila  , col++ , egreso.getVlr() , dinero  );

               xls.adicionarCelda(fila  , col++ , egreso.getTipo_documento_ingreso() , letra  );
               xls.adicionarCelda(fila  , col++ , egreso.getNum_ingreso() , letra  );
               xls.adicionarCelda(fila  , col++ , egresoItem.getVlr_item_egreso() , letra  );


               xls.adicionarCelda(fila  , col++ , egresoItem.getItem_no(), letra  );
               xls.adicionarCelda(fila  , col++ , egresoItem.getTipo_documento(), letra  );
               xls.adicionarCelda(fila  , col++ , egresoItem.getDocumento(), letra  );
               xls.adicionarCelda(fila  , col++ , egresoItem.getItem(), letra  );

               xls.adicionarCelda(fila  , col++ , egresoItem.getConcepto(), letra  );
               xls.adicionarCelda(fila  , col++ , egresoItem.getPlanilla(), letra  );
               xls.adicionarCelda(fila  , col++ , egresoItem.getDescripcion_item_factura(), letra  );

               xls.adicionarCelda(fila  , col++ , egresoItem.getVlr_item_egreso(), dinero  );//new
               xls.adicionarCelda(fila  , col++ , egresoItem.getVlr_item_factura(), dinero  );//new


               xls.adicionarCelda(fila  , col++ , egresoItem.getDescripcion_item_egreso(), letra  );//new

               xls.adicionarCelda(fila  , col++ , egresoItem.getDstrct_factura(), letra  );//new
               xls.adicionarCelda(fila  , col++ , egresoItem.getTipo_documento_factura(), letra  );
               xls.adicionarCelda(fila  , col++ , egresoItem.getDocumento_factura(), letra  );
               xls.adicionarCelda(fila  , col++ , egresoItem.getItem_factura(), letra  );//new
               xls.adicionarCelda(fila  , col++ , egresoItem.getValor_factura(), dinero  );
               xls.adicionarCelda(fila  , col++ , egresoItem.getValor_saldo(), dinero  );

               xls.adicionarCelda(fila  , col++ , egresoItem.getCuenta(), letra  );

               xls.adicionarCelda(fila  , col++ , egresoItem.getDstrct_ingreso(), letra  );
               xls.adicionarCelda(fila  , col++ , egresoItem.getTipo_documento_ingreso(), letra  );
               xls.adicionarCelda(fila  , col++ , egresoItem.getNum_ingreso(), letra  );
               xls.adicionarCelda(fila  , col++ , egresoItem.getItem_ingreso(), letra  );

        }

    }catch (Exception ex){
        System.out.println(ex.getMessage());
        ex.printStackTrace();
        throw new Exception("Error en generarArchivo ...\n" + ex.getMessage() );
    }

   }


    private void generaArchivoFacturas(List listFacturaFintra)  throws Exception  {


        try{

           // Inicia el proceso de listar a excel

           FacturaFintra facturaFintra = new FacturaFintra();
           Iterator it = listFacturaFintra.iterator();
           while (it.hasNext()){

               facturaFintra = (FacturaFintra)it.next();

               fila++;
               int col = 0;

               xls.adicionarCelda(fila  , col++ , facturaFintra.getDstrct(), letra  );
               xls.adicionarCelda(fila  , col++ , facturaFintra.getTipo_documento(), letra  );
               xls.adicionarCelda(fila  , col++ , facturaFintra.getDocumento(), letra  );
               xls.adicionarCelda(fila  , col++ , facturaFintra.getItem(), letra  );
               xls.adicionarCelda(fila  , col++ , facturaFintra.getDescripcion(), letra  );
               xls.adicionarCelda(fila  , col++ , facturaFintra.getValor_factura() , dinero  );
               xls.adicionarCelda(fila  , col++ , facturaFintra.getValor_saldo() , dinero  );
               xls.adicionarCelda(fila  , col++ , facturaFintra.getValor_item() , dinero  );
               xls.adicionarCelda(fila  , col++ , facturaFintra.getValor_ingreso() , dinero  );
               xls.adicionarCelda(fila  , col++ , facturaFintra.getTipo_operacion(), letra  );
               xls.adicionarCelda(fila  , col++ , facturaFintra.getNumero_operacion(), letra  );
               xls.adicionarCelda(fila  , col++ , facturaFintra.getTipo_factura(), letra  );
               xls.adicionarCelda(fila  , col++ , facturaFintra.getFactura(), letra  );
               xls.adicionarCelda(fila  , col++ , facturaFintra.getClase(), letra  );
           }

        }catch (Exception ex){
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception("Error en generarArchivo ...\n" + ex.getMessage() );
        }


    }



}
