/*************************************************************
 * Nombre: ReporteMigracionCumplidos.java
 * Descripci�n: Hilo para crear el ReporteMigracionMercancia
 * Autor: Ing. Jose de la rosa
 * Fecha: 24 de octubre de 2005, 11:39 AM
 * Versi�n: Java 1.0
 * Copyright: Fintravalores S.A. S.A.
 **************************************************************/

package com.tsp.operation.model.threads;

import java.text.*;

import java.util.Date;
import java.text.*;
import java.sql.SQLException;

import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;

import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.util.*;
import java.util.*;
import java.io.*;

/**
 *
 * @author  fily
 */
public class ReporteMigracionMercancia extends Thread{
    private String procesoName;
    private String des;
    String id = "";
    String fechaI = "";
    String fechaF = "";
    private PrintWriter pw;
    private LogWriter   logTrans;
    private String path;
    public ReporteMigracionMercancia () {
    }
    
    public void start (String id, String FechaI,String FechaF){
        this.id = id;
        this.fechaI = FechaI;
        this.fechaF = FechaF;
        this.procesoName = "Mercancias Migrado";
        this.des = "Migracion de Mercancias por fechas";
        super.start ();
    }
    
    public synchronized void run (){
        try{
            
            Model model = new Model ();
            
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String ruta = rb.getString("ruta");
            path = ruta + "/exportar/migracion/" + id;

            File archivo = new File( path );
            if (!archivo.exists()) archivo.mkdirs();
            initLog();
            
            model.LogProcesosSvc.InsertProceso (this.procesoName, this.hashCode (), des, this.id);
            model.transitoService.BusquedaMercancias(fechaI, fechaF);
            Vector lista = (Vector) model.transitoService.getVector();
            Date hoy = new Date ();
            SimpleDateFormat s = new SimpleDateFormat ("yyyy-MM-dd hh:mm:ss");
            String fecha_hoy = s.format (hoy);
            String a�o = fecha_hoy.substring(0,4);
            String mes = fecha_hoy.substring(5,7);
            String dia = fecha_hoy.substring(8,10);
            if (lista!=null && lista.size ()>0){
                
                ResourceBundle rbT = ResourceBundle.getBundle ("com/tsp/util/connectionpool/db");
                String pathT = rbT.getString ("ruta");
                File fileT = new File (pathT + "/exportar/migracion/" + id + "/");
                fileT.mkdirs ();
                String NombreArchivo = pathT + "/exportar/migracion/" + id + "/890103161"+mes+a�o+"REMESAS"+".txt";
                PrintStream archivoT = new PrintStream (NombreArchivo);
                
                
                Iterator it = lista.iterator ();
                String datos1;
                String datos2;
                String datos3;
                String datos4;
                String datos5;
                String datos6;
                String datos7;
                String datos8;
                String datos9;
                String datos10;
                String datos11;
                String datos12;
                String decimalG = "";     
                String decimalK = "";      
                        
                while(it.hasNext ()){
                    BeanGeneral Bean = (BeanGeneral) it.next ();
                    if(Bean.getValor_01().equals("")){
                        logTrans.log("La Mercancia de numero de remesa ="+Bean.getValor_02()+"  no tiene numero de Manifiesto ", logTrans.INFO);
                    }
                    datos1= this.TruncarDatos( Bean.getValor_01() , 14);
                   
                    if(Bean.getValor_02().equals("")){
                        logTrans.log("La Mercancia con Numero Manifiesto = "+Bean.getValor_01()+"  no tiene un Numero de Remesa ", logTrans.INFO);
                    }
                    datos2= this.TruncarDatos( Bean.getValor_02() , 10);
                    
                    if(Bean.getValor_03().equals("")){
                        logTrans.log("La Mercancia con Numero Manifiesto = "+Bean.getValor_01()+"  no tiene El codigo de Unidad de Medida  ", logTrans.INFO);
                    }
                    datos3= this.TruncarDatos( Bean.getValor_03() , 1);
                    
                    if(Bean.getValor_04().equals("")){
                        logTrans.log("La Mercancia con Numero Manifiesto = "+Bean.getValor_01()+"  no tiene Unidad de Medida en Galones", logTrans.INFO);
                    }
                    if(Bean.getValor_04().equals("")){
                        datos4= this.TruncarDatos( Bean.getValor_04() , 5);
                    }else{
                      decimalG = this.QuitarDecimales(Bean.getValor_04());
                      datos4= this.TruncarDatos( decimalG , 5);
                    
                   }
                     
                   if(Bean.getValor_05().equals("")){
                        logTrans.log("La Mercancia con Numero Manifiesto = "+Bean.getValor_01()+"  no tiene Peso en Kilogramos", logTrans.INFO);
                    }
                    if(Bean.getValor_05().equals("")){
                        datos5= this.TruncarDatos( Bean.getValor_05() , 6);
                    }else{
                      decimalK = this.QuitarDecimales(Bean.getValor_05());
                      datos5= this.TruncarDatos( decimalK , 6);
                    
                   }
                     
                    if(Bean.getValor_06().equals("")){
                        logTrans.log("La Mercancia con Numero Manifiesto = "+Bean.getValor_01()+"  no tiene Codigo de Unidad de Empaque ", logTrans.INFO);
                    }
                    datos6= this.TruncarDatos( Bean.getValor_06() , 1);
                    
                    if(Bean.getValor_07().equals("")){
                        logTrans.log("La Mercancia con Numero Manifiesto = "+Bean.getValor_01()+"no tiene Codigo de naturaleza de carga ", logTrans.INFO);
                    }
                    datos7= this.TruncarDatos( Bean.getValor_07() , 1);
                    
                    
                    if(Bean.getValor_08().equals("")){
                        logTrans.log("La Mercancia con Numero Manifiesto = "+Bean.getValor_01()+"  no tiene Codigo del producto ", logTrans.INFO);
                    }
                    datos8= this.TruncarDatos( Bean.getValor_08() , 6);
                    
                    
                    if(Bean.getValor_09().equals("")){
                        logTrans.log("La Mercancia con Numero Manifiesto = "+Bean.getValor_01()+"  no tiene Descripcion del producto ", logTrans.INFO);
                    }
                    datos9= this.TruncarDatos( Bean.getValor_09() , 30);
                    
                     if(Bean.getValor_10().equals("")){
                        logTrans.log("La Mercancia con Numero Manifiesto = "+Bean.getValor_01()+"  no tiene Nombre del Remitente ", logTrans.INFO);
                    }
                    datos10= this.TruncarDatos( Bean.getValor_10() , 25);
                    
                    
                    if(Bean.getValor_11().equals("")){
                        logTrans.log("La Mercancia con Numero Manifiesto = "+Bean.getValor_01()+"  no tiene Nombre del Destinatario", logTrans.INFO);
                    }
                    datos11= this.TruncarDatos( Bean.getValor_11() , 25);
                    
                    
                    if(Bean.getValor_12().equals("")){
                        logTrans.log("La Mercancia con Numero Manifiesto = "+Bean.getValor_01()+"  no tiene Codigo de la Ciudad segun el Dane ", logTrans.INFO);
                    }
                    datos12= this.TruncarDatos( Bean.getValor_12() , 8);
                    
                   archivoT.println (datos1 +" \t "+datos2+" \t "+datos3+" \t "+ datos4+ " \t "+datos5 +
                                     " \t "+ datos6+  " \t "+datos7+  " \t "+datos8+" \t "+datos9+" \t "+datos10+" \t "+datos11+" \t "+datos12);
                 
                }
                closeLog();
            }
            
            model.LogProcesosSvc.finallyProceso (this.procesoName, this.hashCode (), this.id, "PROCESO EXITOSO");
        }
        catch(Exception e){
            try{
                Model model = new Model ();
                model.LogProcesosSvc.finallyProceso (this.procesoName, this.hashCode (),this.id,"ERROR :" + e.getMessage ());
            }
            catch(Exception f){
                try{
                    Model model = new Model ();
                    model.LogProcesosSvc.finallyProceso (this.procesoName,this.hashCode (),this.id,"ERROR :");
                }catch(Exception p){    }
            }
        }
    }
    
    /*funciones para llenar */
    public String agregarEspacios(String valor, int longitud ) throws Exception {
       int tama�oValor = valor.length();
       int tama�o = longitud - tama�oValor;
       String vacios = "";
       String relleno = "";
        for(int i= 0;i< tama�o; i++){
            vacios = vacios+" "; 
        }
        relleno =  valor + vacios ;
        
        return relleno;
       
    }  
    
    public String TruncarDatos(String valor, int longitud ) throws Exception {
       String valorFinal = "";
       int tama�oValor = valor.length();
       String valorTruncado = "";
       if(tama�oValor > longitud ){
          valorTruncado = valor.substring(0, longitud);
       }else{
           valorTruncado = valor;
       }
       valorFinal = this.agregarEspacios(valorTruncado, longitud);
       return valorFinal;
       
    }  
    
    public void initLog()throws Exception{
        
        java.text.SimpleDateFormat fmt = new java.text.SimpleDateFormat("yyyyMMdd_hhmmss");
        pw     = new PrintWriter(new BufferedWriter(new FileWriter(path + "/logMigracionRemesas_.txt")));
        logTrans = new LogWriter("Empresas_migrqaciones", LogWriter.INFO , pw );
        logTrans.log( "Proceso Inicializado", logTrans.INFO );
    }
    
    public void closeLog() throws Exception{
        logTrans.log( "Proceso Finalizado", logTrans.INFO );
        pw.close();
    }
    
    public String QuitarDecimales(String valor) throws Exception {
       String valorFinal = "";
       int tama�oValor = valor.length();
       String valorTruncado = "";
       String cadena= "";
       String incremento = "";
       int ini = 0;
       int fin = 1;
       int diferencia = 0;
       if(!valor.equals("")){
           for(int i = 0; i < tama�oValor ; i++ ){
               cadena = valor.substring(ini,fin);
                if (cadena.equals(".")){
                   diferencia = fin - 1;
                   valorFinal= valor.substring(0,diferencia) ;  
               }
               ini = ini + 1;
               fin = fin + 1;
           }
        }    
           return valorFinal;   
              
           
                    
    }  
    
}
