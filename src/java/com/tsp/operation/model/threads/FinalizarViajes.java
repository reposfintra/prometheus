/*
 * FinalizarViajes.java
 *
 * Created on 17 de septiembre de 2006, 02:07 PM
 */

/*
 * Nombre        FinalizarViajes.java
 * Descripci�n   Proceso generacion de finalizacion de viajes en reporte movimiento tr�fico
 * Autor         David Pi�a L�pez
 * Fecha         17 de septiembre de 2006, 02:07 PM
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */
package com.tsp.operation.model.threads;

import com.tsp.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.beans.*;
import java.io.*;
import java.sql.*;
import java.util.*;
import java.lang.*;
import javax.servlet.jsp.*;
import javax.servlet.http.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import javax.servlet.http.*;
import com.tsp.util.LogWriter;
/**
 *
 * @author  David
 */
public class FinalizarViajes extends Thread{
    
    private Usuario usuario;
    private Model model;
    private PrintWriter pw;
    private LogWriter logWriter;
    private File archivo;    
    private EdicionExcel excel;
    private String distrito;
    
    /** Creates a new instance of FinalizarViajes */
    public FinalizarViajes() {
        model = new Model();
    }
    
    public void start( Usuario user, File file, String distrito ) throws Exception{
        archivo = file;
        usuario = user;
        this.distrito = distrito;
        this.start();
    }
    
    public void run(){
        try {        
            int totalFilas = 0;
            int fila = 0;
            int hoja = 0;
            String observacion = "";
            model.LogProcesosSvc.InsertProceso("Finalizacion de Viajes", this.hashCode(), "Proceso de Finalizacion de viajes en reporte movimiento trafico", usuario.getLogin() );            
            
            excel = new EdicionExcel( archivo );
            excel.crearArchivoRW();
            totalFilas = excel.totalFilas( hoja );
            for( fila = 1; fila < totalFilas; fila++ ){
                String planilla = excel.leerArchivoRW( hoja, 0, fila );
                String tempObs = excel.leerArchivoRW( hoja, 1, fila ).trim();
                observacion = ( tempObs.equalsIgnoreCase("") )? observacion : tempObs;                
                if( !planilla.trim().equalsIgnoreCase("") && planilla.indexOf( "." ) != -1 ){
                    planilla = planilla.substring( 0, planilla.indexOf( "." ) );
                }
                
                //se cargan datos de la base de datos
                model.rmtService.BuscarReporteMovTraf( planilla );
                RepMovTrafico brmt = model.rmtService.getRmt();
                RepMovTrafico rmt = new RepMovTrafico();
                if( brmt != null ){
                    rmt.setDstrct( distrito );
                    rmt.setNumpla( planilla );
                    rmt.setObservacion( observacion );
                    rmt.setTipo_procedencia( "CIU" );
                    rmt.setUbicacion_procedencia( brmt.getDestino() );
                    rmt.setTipo_reporte( "ECL" );
                    rmt.setCreation_user( usuario.getLogin() );
                    rmt.setBase( usuario.getBase() );
                    rmt.setFechareporte( "now()" );
                    rmt.setZona( model.rmtService.getZona( brmt.getDestino() ) );
                    rmt.setFec_rep_pla( brmt.getFecha_prox_reporte() );
                    rmt.setCausa( "" );
                    rmt.setClasificacion( "" );
                    rmt.setCreation_date( "now()" );
                    rmt.setLast_update( "now()" );
                    Vector consultas = new Vector();
                    consultas.add( model.rmtService.addRMT( rmt ) );
                    //Se modifica el excel                
                    excel.modificarArchivoRW( hoja, 2, fila, "Entrega Realizada" );
                    model.planillaService.anulaTrafico( planilla );
                    model.rmtService.updateTrafico(rmt);
                    model.despachoService.insertar( consultas );
                }else{
                    ////System.out.println( "No existe la planilla " + planilla + " en ingreso trafico." );
                }
                    
            }
            excel.escribirArchivoRW();
            excel.cerrarArchivoRW();
            model.LogProcesosSvc.finallyProceso("Finalizacion de Viajes", this.hashCode(), usuario.getLogin(), "PROCESO EXITOSO");
        }catch(Exception ex ){
            excel.escribirArchivoRW();
            excel.cerrarArchivoRW();
            try{
                model.LogProcesosSvc.finallyProceso("Finalizacion de Viajes", this.hashCode(),usuario.getLogin(),"ERROR :" + ex.getMessage()); 
            }catch ( SQLException e) {
                ////System.out.println("Error guardando el proceso");
            }            
            ex.printStackTrace();
        }
        
        
    }
}
