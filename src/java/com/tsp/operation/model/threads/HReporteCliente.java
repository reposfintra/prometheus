/*
 * HReporteAnticiposPagados.java
 *
 * Created on 19 octubre de 2006, 03:53 PM
 */

package com.tsp.operation.model.threads;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import java.io.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.controller.*;
import com.tsp.operation.model.services.*;
import com.tsp.operation.model.*;
import java.util.*;
import java.text.*;
import com.tsp.util.*;
/**
 *
 * @author  ffernandez
 */
public class HReporteCliente extends Thread{
    String user;
    String Codigo1;
    String Nit1;
    String Nombre1;
    String agencia1;
    String pais1;
    String procesoName;
    String des;
    //String dstrct;
    Model model = new Model();
    
    public void start(String Codigo, String Nit ,String Nombre, String Agencia,String pais, String usuario){
        this.user = usuario;
        this.procesoName = "Reporte Clientes";
        this.des = "Reporte Documentos Digitalizados del Cliente";
        this.Codigo1 = Codigo;
        this.Nit1 = Nit;
        this.Nombre1 = Nombre;
        this.agencia1= Agencia;
        this.pais1= pais;
            
        //this.dstrct = dstrct;
        super.start();
    }
    
    public synchronized void run(){
        try{
            model.LogProcesosSvc.InsertProceso(this.procesoName, this.hashCode(), des, this.user);
            model.clienteService.BusquedaCLiente(Codigo1, Nit1, Nombre1, agencia1,pais1);
            Vector vec =  model.clienteService.getVector();
            //System.out.println("entro al vector =  "+vec.size());
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String path = rb.getString("ruta");
            //armas la ruta
            String Ruta1  = path + "/exportar/migracion/" + user + "/";
            //crear la ruta
            File file = new File(Ruta1);
            file.mkdirs();
            String hoy = Util.getFechaActual_String(6).trim().replaceAll("/", "").replaceAll(":","_");
            
            POIWrite xls = new POIWrite(Ruta1 +"ReporteClientes" + hoy + ".xls", user, Util.getFechaActual_String(5));
            
            HSSFCellStyle fecha  = xls.nuevoEstilo("Book Antiqua", 9, false , false, "yyyy/mm/dd"  , xls.NONE , xls.NONE , xls.NONE );
            HSSFCellStyle texto  = xls.nuevoEstilo("Book Antiqua", 9, false , false, "text"        , xls.NONE , xls.NONE , xls.NONE);
            HSSFCellStyle numero      = xls.nuevoEstilo("Book Antiqua", 9, false , false, "#.##"         , xls.NONE , xls.NONE , xls.NONE );
            
            HSSFCellStyle header      = xls.nuevoEstilo("Arial Black", 18, true  , false, "text"        , HSSFColor.WHITE.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
            HSSFCellStyle titulo      = xls.nuevoEstilo("Book Antiqua", 9, true  , false, "text"        , HSSFColor.WHITE.index , HSSFColor.BLACK.index , HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle encabezado      = xls.nuevoEstilo("Book Antiqua", 9, true  , false, "text"        , HSSFColor.WHITE.index , HSSFColor.BLACK.index , HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle fechatitle  = xls.nuevoEstilo("Book Antiqua", 9, true  , false, "yyyy/mm/dd"  , HSSFColor.RED.index , xls.NONE, HSSFCellStyle.ALIGN_CENTER );
            
            
            /****************************************
             *    compos de la tabla en agencia     *
             ****************************************/
            
            xls.obtenerHoja("REPORTE");
            xls.cambiarMagnificacion(4,4);
            
            // cabecera
            BeanGeneral prom = (BeanGeneral) vec.lastElement();
             
            xls.adicionarCelda(0,0, "TRANSPORTES SANCHEZ POLO S.A.", encabezado);
            xls.combinarCeldas(0, 0, 0, 2);
            xls.adicionarCelda(1,0, "RESPORTE DE CLIENTES AGREGADOS "+ prom.getValor_16() , encabezado);
            xls.combinarCeldas(1, 0, 0, 2);
           
            if(!Nombre1.equals("")){
                xls.adicionarCelda(2,6, "Nombre :"   , titulo);
                xls.adicionarCelda(2,7, Nombre1, titulo);
            }
            if(!Codigo1.equals("")){
                xls.adicionarCelda(3,6, "Codigo :"   , titulo);
                xls.adicionarCelda(3,7, Codigo1, titulo);
            }
             if(!Nit1.equals("")){
                xls.adicionarCelda(4,6, "Nit :"   , titulo);
                xls.adicionarCelda(4,7, Nit1, titulo);
             }
            
           
            // subtitulos
            
            
            int fila = 7;
            int col  = 0;
            
            xls.adicionarCelda(fila ,col++ , "ITEM"     , titulo ); 
            xls.adicionarCelda(fila ,col++ , "ESTADO"     , titulo );
            xls.adicionarCelda(fila ,col++ , "AGENCIA DUE�A"     , titulo );
            xls.adicionarCelda(fila ,col++ , "CODIGO DEL CLIENTE"     , titulo );
            xls.adicionarCelda(fila ,col++ , "NOMBRE DEL CLIENTE"     , titulo );
            xls.adicionarCelda(fila ,col++ , "DIRECCION DEL CLIENTE"     , titulo );
            xls.adicionarCelda(fila ,col++ , "CIUDAD DEL CLIENTE"     , titulo );
            xls.adicionarCelda(fila ,col++ , "PAIS DEL CLIENTE"     , titulo );
            xls.adicionarCelda(fila ,col++ , "TELEFONO DEL CLIENTE"     , titulo );
            xls.adicionarCelda(fila ,col++ , "NOMBRE DEL CONTACTO"     , titulo );
            xls.adicionarCelda(fila ,col++ , "TELEFONO DEL CONTACTO"     , titulo );
            xls.adicionarCelda(fila ,col++ , "DIRECCION DEL CONTACTO"     , titulo );
            xls.adicionarCelda(fila ,col++ , "E-MAIL DEL CONTACTO"     , titulo );
            xls.adicionarCelda(fila ,col++ , "NOTAS DEL CLIENTE"     , titulo );
           
            xls.adicionarCelda(fila ,col++ , "OBSERVACION DE FACTURACION"     , titulo );
            xls.adicionarCelda(fila ,col++ , "CEDULA DEL CLIENTE"     , titulo );
            xls.adicionarCelda(fila ,col++ , "CEDULA DEL AGENTE"     , titulo );
            xls.adicionarCelda(fila ,col++ , "RENTABILIDAD"     , titulo );
            xls.adicionarCelda(fila ,col++ , "DIA LIMITE DEL MES PARA RECIBIR FACTURAS"     , titulo );
            xls.adicionarCelda(fila ,col++ , "SOPORTE DE FACTURACION"     , titulo );
            xls.adicionarCelda(fila ,col++ , "FIDUCIARIA"     , titulo );
            xls.adicionarCelda(fila ,col++ , "AGENCIA DE COBRO"     , titulo );
            xls.adicionarCelda(fila ,col++ , "DISTRITO CLIENTE"     , titulo );
            xls.adicionarCelda(fila ,col++ , "MONEDA FACTURA DEL CLIENTE"     , titulo );
            xls.adicionarCelda(fila ,col++ , "FORMA DE PAGO"     , titulo );
            xls.adicionarCelda(fila ,col++ , "PLAZO DE PAGO"     , titulo );
            xls.adicionarCelda(fila ,col++ , "BANCO DE CONSIGNACION"     , titulo );
            xls.adicionarCelda(fila ,col++ , "SURCURSAL DEL BANCO"     , titulo );
            xls.adicionarCelda(fila ,col++ , "UNIDAD CONTABLE ASOCIADA"     , titulo );
            xls.adicionarCelda(fila ,col++ , "CODIGO DEL IMPUESTO DEL CLIENTE"     , titulo );
            xls.adicionarCelda(fila ,col++ , "AGENCIA DE FACTURACION"     , titulo );
            xls.adicionarCelda(fila ,col++ , "DIRECCION DE FACTURACION"     , titulo );
            xls.adicionarCelda(fila ,col++ , "PAIS DE FACTURACION"     , titulo );
            xls.adicionarCelda(fila ,col++ , "CIUDAD DE FACTURACION"     , titulo );
            xls.adicionarCelda(fila ,col++ , "TIEMPO DE ELABORACION DE PREFACTURA"     , titulo );
            xls.adicionarCelda(fila ,col++ , "TIEMPO DE LEGALIZACION DE PREFACTURA"     , titulo );
            xls.adicionarCelda(fila ,col++ , "HC"     , titulo );
            xls.adicionarCelda(fila ,col++ , "RIF"     , titulo );
     
           
           int pp = 1; 
           for(int i=0; i<vec.size();i++ ){
                fila++;
                col = 0;
                BeanGeneral rep = (BeanGeneral) vec.get(i);
                xls.adicionarCelda(fila ,col++ ,pp, texto );
                xls.adicionarCelda(fila ,col++ ,rep.getValor_01(), texto );
                xls.adicionarCelda(fila ,col++ ,rep.getValor_05() , texto );
                xls.adicionarCelda(fila ,col++ ,rep.getValor_02() , texto );
                xls.adicionarCelda(fila ,col++ ,rep.getValor_03() , texto );
                xls.adicionarCelda(fila ,col++ ,rep.getValor_26() , texto );
                xls.adicionarCelda(fila ,col++ ,rep.getValor_43() , texto );
                xls.adicionarCelda(fila ,col++ ,rep.getValor_45() , texto );
                xls.adicionarCelda(fila ,col++ ,rep.getValor_27() , texto );
                xls.adicionarCelda(fila ,col++ ,rep.getValor_28(), texto );
                xls.adicionarCelda(fila ,col++ ,rep.getValor_29() , texto );
                xls.adicionarCelda(fila ,col++ ,rep.getValor_39() , texto );
                xls.adicionarCelda(fila ,col++ ,rep.getValor_30() , texto );
                xls.adicionarCelda(fila ,col++ ,rep.getValor_04() , texto );
                xls.adicionarCelda(fila ,col++ ,rep.getValor_07() , texto );
                xls.adicionarCelda(fila ,col++ ,rep.getValor_08(), texto );
                xls.adicionarCelda(fila ,col++ ,rep.getValor_09() , texto );
                xls.adicionarCelda(fila ,col++ ,rep.getValor_10() , texto );
                xls.adicionarCelda(fila ,col++ ,rep.getValor_36() , texto );
                xls.adicionarCelda(fila ,col++ ,rep.getValor_11() , texto );
                xls.adicionarCelda(fila ,col++ ,rep.getValor_12() , texto );
                xls.adicionarCelda(fila ,col++ ,rep.getValor_40() , texto );
                xls.adicionarCelda(fila ,col++ ,rep.getValor_13() , texto );
                xls.adicionarCelda(fila ,col++ ,rep.getValor_14(), texto );
                xls.adicionarCelda(fila ,col++ ,rep.getValor_15(), texto );
                xls.adicionarCelda(fila ,col++ ,rep.getValor_16(), texto );
                xls.adicionarCelda(fila ,col++ ,rep.getValor_18(), texto );
                xls.adicionarCelda(fila ,col++ ,rep.getValor_19(), texto );
                xls.adicionarCelda(fila ,col++ ,rep.getValor_21(), texto );
                xls.adicionarCelda(fila ,col++ ,rep.getValor_22(), texto );
                xls.adicionarCelda(fila ,col++ ,rep.getValor_23(), texto );
                xls.adicionarCelda(fila ,col++ ,rep.getValor_31(), texto );
                xls.adicionarCelda(fila ,col++ ,rep.getValor_46(), texto );
                xls.adicionarCelda(fila ,col++ ,rep.getValor_44(), texto );
                xls.adicionarCelda(fila ,col++ ,rep.getValor_35(), texto );
                xls.adicionarCelda(fila ,col++ ,rep.getValor_34(), texto );
                xls.adicionarCelda(fila ,col++ ,rep.getValor_41(), texto );
                xls.adicionarCelda(fila ,col++ ,rep.getValor_42(), texto );
                
                
                
              pp = pp + 1; 
           }
           			
            
            xls.cerrarLibro();
            //System.out.println("cerro proceso");
            model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(),this.user,"PROCESO EXITOSO");
        }catch (Exception ex){
            ex.printStackTrace();
            try{
                model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(),this.user,"ERROR :" + ex.getMessage());
            }
            catch(Exception f){
                try{
                    model.LogProcesosSvc.finallyProceso(this.procesoName,this.hashCode(),this.user,"ERROR :");
                }catch(Exception p){    }
            }
        }
        
    }
   
    
}
