/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/*
package com.tsp.operation.model.threads;



import com.tsp.operation.model.Model;
import com.tsp.operation.model.beans.*;
import com.tsp.util.Util;

import java.util.*;
import java.text.*;





/**
 *
 * @author Alvaro
 *//*
public class HOrdenServicio extends Thread{*/
//2009-09-02*
/*
    private Model model;
    private Usuario usuario;
    private String distrito;

    public HOrdenServicio () {
    }

    public void start(Model model, Usuario usuario, String distrito){

        this.usuario = usuario;
        this.model = model;
        this.distrito = distrito;

        super.start();
    }



    public synchronized void run(){
        try{
            //model.LogProcesosSvc.InsertProceso(this.processName, this.hashCode(), "Generacion de ordenes de servicio : " , usuario.getLogin());

            Vector comandos_sql =new Vector();

            String banco = "";

            String comandoSQL = "";
            String login = usuario.getLogin();

            java.util.Date fechaActual = new Date();
            String creation_date = fechaActual.toString();

            DateFormat formato_periodo;
            formato_periodo = new SimpleDateFormat("yyyyMM");
            String periodo  = formato_periodo.format(fechaActual);

            DateFormat formato_fecha;
            formato_fecha = new SimpleDateFormat("yyyy-MM-dd");
            String  fecha = formato_fecha.format(fechaActual);

            model.prontoPagoService.buscaProntoPago();
            List listaProntoPago = model.prontoPagoService.getProntoPago();

            if (listaProntoPago.size() != 0){

                ProntoPago prontoPago =  new ProntoPago();
                ProntoPagoDetalle prontoPagoDetalle =  new ProntoPagoDetalle();

                Iterator it = listaProntoPago.iterator();
                 while (it.hasNext()) {


                     // Un registro de orden de servicio para un anticipo pago tercero
                     prontoPago = (ProntoPago)it.next();

                     // Detalle de orden de servicio  para un anticipo pago tercero proveniente de extracto detalle

                     model.prontoPagoService.buscaProntoPagoDetalle(prontoPago.getNumero_operacion());
                     List listaProntoPagoDetalle = model.prontoPagoService.getProntoPagoDetalle();

                     // Totales del extracto

                     prontoPago.setTipo_operacion("EXT");
                     String secuencia = prontoPago.getNumero_operacion();
                     TotalExtracto totalExtracto = (TotalExtracto) model.prontoPagoService.getTotalExtracto(secuencia);
                     prontoPago.setVlr_ext_detalle_calculado(totalExtracto.getVlr_ext_detalle_calculado());
                     prontoPago.setVlr_ext_detalle_registrado(totalExtracto.getVlr_ext_detalle_registrado());
                     prontoPago.setVlr_ext_detalle_diferencia(totalExtracto.getVlr_ext_detalle_diferencia());
                     prontoPago.setVlr_diferencia_ext_ant(totalExtracto.getVlr_ext_detalle_calculado() - prontoPago.getVlr_extracto());

                     // Validar si todo el documento es consistente

                     boolean error = false;
                     String descripcionError = "Extracto "+ secuencia + "\n";
                     // Valida si vlr + retefuente + reteica + impuestos = vlr_ppa_item para cada documento incluido en el extracto
                     if (prontoPago.getVlr_ext_detalle_diferencia() != 0 ) {
                         error = true;
                         descripcionError += "Items de extracto detalle no suman igual a la columna de total por item (vlr_ppa_item) \n";
                     }
                     // Valida si valor total del extracto detalle = a valor del extracto en anticipos pagos terceros
                     if (prontoPago.getVlr_diferencia_ext_ant()!= 0) {
                         error = true;
                         descripcionError += "Valor total del extracto detalle no suma igual al valor del extracto registrado en anticipos pagos terceros \n";
                     }
                     // Valida cual de los documentos en el extracto detalle no suma a su total
                     if (listaProntoPagoDetalle.size() != 0){

                        Iterator it1 = listaProntoPagoDetalle.iterator();
                         while (it1.hasNext()) {
                             prontoPagoDetalle = (ProntoPagoDetalle)it1.next();
                             if (prontoPagoDetalle.getDiferencia_interna() != 0) {
                                 error = true;
                                 descripcionError += "Detalle diferencia \n";
                                 descripcionError += prontoPagoDetalle.getTipo_documento() + " ";
                                 descripcionError += prontoPagoDetalle.getClase() + " ";
                                 descripcionError += prontoPagoDetalle.getDocumento() + " ";
                                 descripcionError += prontoPagoDetalle.getValor_calculado() + " ";
                                 descripcionError += prontoPagoDetalle.getValor_registrado() + " ";
                                 descripcionError += prontoPagoDetalle.getDiferencia_interna() + "\n";
                             }
                         }
                     }
                     else {
                         error = true;
                         descripcionError += "No existe detalle para el extracto \n";
                     }
                     descripcionError += "\n";

                     if (!error) {

                         String cuenta_contable_banco = model.prontoPagoService.getCuentaBanco(prontoPago.getCuenta_banco(), prontoPago.getSucursal());
                         prontoPago.setCuenta_contable_banco(cuenta_contable_banco);

                         banco = model.prontoPagoService.getBanco(periodo, distrito);
                         prontoPago.setBanco(banco);

                         Cmc cmc = model.prontoPagoService.getCmc(distrito, "EXT","EX");
                         prontoPago.setCmc("EX");
                         prontoPago.setCuenta_os(cmc.getCuenta());
                         prontoPago.setDbcr_os(cmc.getDbcr());
                         prontoPago.setSigla_comprobante_os(cmc.getSigla_comprobante());
                         prontoPago.setGrupo_transaccion(model.prontoPagoService.getGrupoTransaccion());

                         prontoPago.setPeriodo_os(periodo);

                         // Procesa transaccion

                         // Grabar un registro en orden de servicio
                         comandoSQL = model.prontoPagoService.setOrdenServicio(prontoPago, distrito, login, creation_date);
                         comandos_sql.add(comandoSQL);

                         comandoSQL = model.prontoPagoService.setAnticipo(prontoPago, "TSP", creation_date);
                         comandos_sql.add(comandoSQL);

                         // Grabar registros en orden de servicio detallado
                         Iterator it1 = listaProntoPagoDetalle.iterator();
                         while (it1.hasNext()) {
                             prontoPagoDetalle = (ProntoPagoDetalle)it1.next();
                             comandoSQL = model.prontoPagoService.setOrdenServicioDetalle(prontoPago, prontoPagoDetalle, distrito, login, creation_date);
                             comandos_sql.add(comandoSQL);
                             }
                         }


                         // Crear la cabecera del comprobante contable de la Orden de servicio

                         Comprobante comprobante = new Comprobante();

                         comprobante.setReg_status("");
                         comprobante.setDstrct(distrito);
                         comprobante.setTipodoc(prontoPago.getSigla_comprobante_os());
                         comprobante.setNumdoc(Integer.toString(prontoPago.getNumero_operacion() ) );
                         comprobante.setGrupo_transaccion(prontoPago.getGrupo_transaccion());
                         comprobante.setSucursal("OP");
                         comprobante.setPeriodo(prontoPago.getPeriodo_os());
                         comprobante.setFechadoc(fecha);
                         comprobante.setDetalle("Contabiliza " + prontoPago.getTipo_operacion() + Integer.toString(prontoPago.getNumero_operacion() ) );
                         comprobante.setTercero(prontoPago.getNit());
                         comprobante.setTotal_debito(prontoPago.getVlr_extracto());
                         comprobante.setTotal_credito(prontoPago.getVlr_extracto()) ;
                         comprobante.setTotal_items(3);
                         comprobante.setMoneda("PES");
                         comprobante.setFecha_aplicacion("0099-01-01 00:00:00");
                         comprobante.setAprobador("ADMIN");
                         comprobante.setLast_update(creation_date);
                         comprobante.setUser_update("ADMIN");
                         comprobante.setCreation_date(creation_date);
                         comprobante.setCreation_user("ADMIN");
                         comprobante.setBase("COL");
                         comprobante.setUsuario_aplicacion("");
                         comprobante.setTipo_operacion("");
                         comprobante.setMoneda_foranea("");
                         comprobante.setValor_for(0.00);
                         comprobante.setRef_1(prontoPago.getTipo_operacion());
                         comprobante.setRef_2(Integer.toString(prontoPago.getNumero_operacion() ) );

                         comandoSQL = model.prontoPagoService.setComprobante(comprobante);
                         comandos_sql.add(comandoSQL);

                         // Crear el detalle del comprobante

                         crearComprobanteDetalle(prontoPago, prontoPagoDetalle, comandos_sql, distrito,creation_date);
                         
                         // Crear la factura CxP al Propietario

                         crearCxPPropietario( prontoPago, prontoPagoDetalle, comandos_sql, distrito);

                         // Grabando todo a la base de datos.
                         model.applusService.ejecutarSQL(comandos_sql);  
                         comandos_sql.removeAllElements();

                 } // final del while

            } // final del if

        }catch (Exception ex){
            ex.printStackTrace();
            try{

                //model.LogProcesosSvc.finallyProceso(this.processName, this.hashCode(), usuario.getLogin(), "Finalizado con error ...\n" + ex.getMessage());
            }catch (Exception e){
                System.out.println("Error HOrdenServicio ...\n"  + e.getMessage());
            }
        }
    }




public void crearCxPPropietario(ProntoPago prontoPago, ProntoPagoDetalle prontoPagoDetalle,
                                Vector comandos_sql, String distrito) {

    String comandoSQL = "";

    // Datos comunes a la cabecera y a los items

    // Cambiar el nit al de PROVINTEGRAL
    String proveedor = prontoPago.getNit();
    String auxiliar  = proveedor;
    String tipo_documento = "FAP";

    String secuencia = Integer.toString(prontoPago.getNumero_operacion());
    String documento = "E"+secuencia;

    String user_update = usuario.getLogin();
    String creation_user = user_update;
    String base = "COL";

    // CREACION DE LA CABECERA EN CXP_DOC

    // Datos de la cabecera

    String tipo_operacion     = prontoPago.getTipo_operacion();

    String descripcion_factura  = "TIPO OPERACION : " + tipo_operacion + "   SECUENCIA EXTRACTO: " + secuencia ;

    String agencia        = "OP";

    String handle_code = prontoPago.getCmc();
    String aprobador   = "JGOMEZ";
    String usuario_aprobacion = "JGOMEZ";

    try{

        String banco = prontoPago.getBanco();
        String sucursal = prontoPago.getSucursal();
        String moneda = "PES";

        double vlr_factura = Util.redondear2(prontoPago.getVlr_neto(), 2);

        double vlr_neto  =  vlr_factura;
        double vlr_total_abonos = 0;
        double vlr_saldo = vlr_neto;
        double vlr_neto_me = vlr_neto;
        double vlr_total_abonos_me = 0;
        double vlr_saldo_me = vlr_neto;
        double tasa = 1;

        String observacion = "DETALLE :";
        observacion = observacion + "  Valor extracto : " + Double.toString(prontoPago.getVlr_extracto()) ;
        observacion = observacion + "  Porcentaje : " + Double.toString(prontoPago.getPorcentaje()) ;
        observacion = observacion + "  Ingreso anticipado : " + Double.toString(prontoPago.getVlr_descuento()) ;

        String clase_documento = "4";
        // String moneda_banco = objeto_proveedor.getC_currency_bank();
        String moneda_banco = "PES";
        String clase_documento_rel = "4";

        // Calculo de la fecha de vencimiento de la factura//


        java.util.Date fechaActual = new Date();
        String creation_date = fechaActual.toString();

        DateFormat formato_fecha_factura;
        formato_fecha_factura = new SimpleDateFormat("yyyy-MM-dd");
        String fecha_factura  = formato_fecha_factura.format(fechaActual);
        String FechaFacturaVencimiento = fecha_factura ;

        comandoSQL = model.applusService.setCxp_doc(distrito, proveedor, tipo_documento, documento, descripcion_factura,
                agencia, handle_code, aprobador, usuario_aprobacion, banco, sucursal,
                moneda, vlr_neto, vlr_total_abonos, vlr_saldo, vlr_neto_me,
                vlr_total_abonos_me, vlr_saldo_me, tasa, observacion, user_update,
                creation_user, base, clase_documento, moneda_banco,
                fecha_factura, FechaFacturaVencimiento, clase_documento_rel,
                creation_date, creation_date);

        comandos_sql.add(comandoSQL);


        // CREACION DE LOS ITEMS EN CXP_ITEMS_DOC

        // Datos comunes a todos los items

        int item = 0;

        // Item de valor del extracto menos el ingreso anticipado

        comandoSQL  = model.applusService.setCxp_items_doc(distrito, proveedor,
                             tipo_documento, documento,Integer.toString(++item),  descripcion_factura,
                             vlr_neto, vlr_neto, "22",
                             user_update, creation_user, base, "098", auxiliar,
                             creation_date, creation_date);
        comandos_sql.add(comandoSQL);





    }catch (Exception ex){
        ex.printStackTrace();
        try{

            //model.LogProcesosSvc.finallyProceso(this.processName, this.hashCode(), usuario.getLogin(), "Finalizado con error ...\n" + ex.getMessage());
        }catch (Exception e){
            System.out.println("Error HOrdenServicio, procedimiento crearCxPPropietario  ...\n"  + e.getMessage());
        }
    }
}



public void crearComprobanteDetalle(ProntoPago prontoPago, ProntoPagoDetalle prontoPagoDetalle, Vector comandos_sql, 
                                    String distrito, String creation_date) {


    try{

         String comandoSQL = "";
         ComprobanteDetalle comprobanteDetalle = new ComprobanteDetalle();


         // Item 1
         comprobanteDetalle.setReg_status("");
         comprobanteDetalle.setDstrct(distrito);
         comprobanteDetalle.setTipodoc(prontoPago.getSigla_comprobante_os());
         comprobanteDetalle.setNumdoc(Integer.toString(prontoPago.getNumero_operacion() ) );
         comprobanteDetalle.setGrupo_transaccion(prontoPago.getGrupo_transaccion());
         comprobanteDetalle.setTransaccion(model.prontoPagoService.getTransaccion());
         comprobanteDetalle.setPeriodo(prontoPago.getPeriodo_os());
         comprobanteDetalle.setCuenta("1380");
         comprobanteDetalle.setAuxiliar(prontoPago.getNit());
         comprobanteDetalle.setDetalle("Contabiliza " + prontoPago.getTipo_operacion() + Integer.toString(prontoPago.getNumero_operacion() ) );
         comprobanteDetalle.setValor_debito(prontoPago.getVlr_extracto());
         comprobanteDetalle.setValor_credito(0.00) ;
         comprobanteDetalle.setTercero(prontoPago.getNit());
         comprobanteDetalle.setDocumento_interno("EXT");
         comprobanteDetalle.setLast_update(creation_date);
         comprobanteDetalle.setUser_update("ADMIN");
         comprobanteDetalle.setCreation_date(creation_date);
         comprobanteDetalle.setCreation_user("ADMIN");
         comprobanteDetalle.setBase("COL");
         comprobanteDetalle.setAbc("");
         comprobanteDetalle.setVlr_for(0.00);
         comprobanteDetalle.setTipodoc_rel(prontoPago.getTipo_operacion());
         comprobanteDetalle.setDocumento_rel(Integer.toString(prontoPago.getNumero_operacion() ) );

         comandoSQL = model.prontoPagoService.setComprobanteDetalle(comprobanteDetalle);
         comandos_sql.add(comandoSQL);

         // Item 2
         comprobanteDetalle.setTransaccion(model.prontoPagoService.getTransaccion());
         comprobanteDetalle.setCuenta("22");
         comprobanteDetalle.setValor_debito(0.00);
         comprobanteDetalle.setValor_credito(prontoPago.getVlr_neto()) ;

         comandoSQL = model.prontoPagoService.setComprobanteDetalle(comprobanteDetalle);
         comandos_sql.add(comandoSQL);


         // Item 3
         comprobanteDetalle.setTransaccion(model.prontoPagoService.getTransaccion());
         comprobanteDetalle.setCuenta("Ixx");
         double ingreso_anticipado = prontoPago.getVlr_extracto() - prontoPago.getVlr_neto();
         comprobanteDetalle.setValor_debito(0.00);
         comprobanteDetalle.setValor_credito(ingreso_anticipado) ;

         comandoSQL = model.prontoPagoService.setComprobanteDetalle(comprobanteDetalle);
         comandos_sql.add(comandoSQL);




    }catch (Exception ex){
        ex.printStackTrace();
        try{

            //model.LogProcesosSvc.finallyProceso(this.processName, this.hashCode(), usuario.getLogin(), "Finalizado con error ...\n" + ex.getMessage());
        }catch (Exception e){
            System.out.println("Error HOrdenServicio, procedimiento crear Comprobante detallado  ...\n"  + e.getMessage());
        }
    }



}









}
*/