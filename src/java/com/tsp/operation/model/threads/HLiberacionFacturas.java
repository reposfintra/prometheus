   /***************************************
    * Nombre Clase ............. HLiberacionFacturas.java
    * Descripci�n  .. . . . . .  Permite liberar las facturas no aprobadas para pago dentro de una corrida
    * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
    * Fecha . . . . . . . . . .  27/03/2006
    * versi�n . . . . . . . . .  1.0
    * Copyright ...Transportes Sanchez Polo S.A.
    *******************************************/




package com.tsp.operation.model.threads;


import java.io.*;
import java.util.*;
import com.tsp.operation.model.Model;
import com.tsp.operation.model.TransaccionService;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.util.Utility;

import org.apache.log4j.*;

public class HLiberacionFacturas extends Thread{
    
    Logger logger = Logger.getLogger(this.getClass());
    
    private  Model         model;
    private  Usuario       usuario ;
    private  String        procesoName;
    private  String[]      corridas; 
    
    
    public HLiberacionFacturas() {}
    
    
    
    /**
     * M�todo inicializa  el proceso
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public void start(Model modelo,  Usuario user, String[] corridas) throws Exception{
         try{
            this.model       = modelo;
            this.usuario     = user;
            this.procesoName = "LIBERACION DE FACTURAS ";
            this.corridas    = corridas;
            super.start();
        }catch(Exception e){
           throw new Exception( e.getMessage() );
       }
    }
    
    
    
    
    
    /**
     * M�todo que ejecuta el proceso de  generaci�n de la Corrida
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
     public synchronized void run(){
       try{
           
           String distrito =   usuario.getDstrct();
           String user     =   usuario.getLogin();
        // Automatico
           if(  corridas == null )
                corridas = model.LiberarFacturasSvc.getCorridas(distrito);
           
           
           
           String comentario="EXITOSO";
           String hoy       = Utility.getHoy("");
           model.LogProcesosSvc.InsertProceso( this.procesoName, this.hashCode(), hoy , user ); 
                 
           String msj      = "";
           TransaccionService  svc = new TransaccionService(usuario.getBd());
           
           for( int i=0; i<this.corridas.length; i++){
               String corrida = this.corridas[i];
               try{
                   List listaFacturas   =  model.LiberarFacturasSvc.getFacturas( distrito, corrida , user );
                   
                   
                   for(int j=0; j<listaFacturas.size();j++){
                        Hashtable factura   = (Hashtable)listaFacturas.get(j);
                        String proveedor      = (String) factura.get("proveedor");
                        String tipo_documento = (String) factura.get("tipo_documento");
                        String documento      = (String) factura.get("documento");                        
                       
                        String  sql           =  model.LiberarFacturasSvc.getSQL( distrito, corrida,  proveedor, tipo_documento, documento );

                      //Ejecutamos SQL:  
                      /*Ejecutamos SQL:
                        svc.crearStatement();
                        svc.getSt().addBatch(sql);
                        svc.execute(); */
                       
                   }
                   
                   msj += " Corrida " + corrida  +" : "+ listaFacturas.size()  + " Facturas Liberadas " ;
                   
                   
               }catch(Exception e){
                   msj += " Corrida " + corrida  + " ERROR : " + e.getMessage();
               }
           }
            
           
           comentario += msj;
           model.LogProcesosSvc.finallyProceso(this.procesoName,this.hashCode(),  this.usuario.getLogin() ,comentario);
           
             
       }catch(Exception e){
           e.printStackTrace();
           try{
                model.LogProcesosSvc.finallyProceso(this.procesoName,this.hashCode(),  this.usuario.getLogin() ,"ERROR Hilo: " + e.getMessage()); 
           }
           catch(Exception f){ }   
       }
       
       
       
    }
     
}
