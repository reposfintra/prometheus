/**
 * Nombre        HImportacionCXP.java
 * Descripci�n   Hilo de control y verificacion de datos de importacion de facturas
 * Autor         Mario Fontalvo Solano
 * Fecha         16 de febrero de 2006, 10:40 AM
 * Version       1.0
 * Coyright      Transportes S�nchez Polo S.A.
 **/

package com.tsp.operation.model.threads;

import com.tsp.operation.model.beans.CXPImportacion;
import com.tsp.operation.model.beans.CXPImpItem;
import com.tsp.operation.model.beans.CXPImpDoc;
import com.tsp.operation.model.beans.CXP_Doc;
import com.tsp.operation.model.beans.CXPItemDoc;
import com.tsp.operation.model.beans.CIA;
import com.tsp.operation.model.beans.Proveedor;
import com.tsp.operation.model.beans.Banco;
import com.tsp.operation.model.beans.Tipo_impuesto;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.Model;
import com.tsp.util.UtilFinanzas;
import org.apache.log4j.Logger;

import java.io.*;
import java.util.*;
import java.text.*;


public class HImportacionCXP extends Evento {
    
    
    
    // varibales generales de la facturas
    Model   model;
    TreeMap cias;
    TreeMap tbldoc;
    Logger  logger = Logger.getLogger(HImportacionCXP.class);
    
    CIA       cia       = null;
    Proveedor proveedor = null;
    Banco     banco     = null;
    TreeMap   monedas   = null;
    
    FileWriter       fw  = null;
    PrintWriter      pw  = null;
    SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss");
    SimpleDateFormat fmt2= new SimpleDateFormat("yyyyMMdd_kkmmss");
    Date             fechaActual = new Date();
        
    
    
    public HImportacionCXP() {
    }
    
    /** Crea una nueva instancia de  HImportacionCXP */
    public HImportacionCXP(Usuario usuario) {
        super(usuario);
    }
    
    
    /** Procedimiento general del hilo */
    public synchronized void run(){
        try{
            
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String ruta = rb.getString("ruta") + "/exportar/migracion/" + usuario.getLogin();
            File archivo = new File(ruta);
            archivo.mkdirs();    
            fw = new FileWriter (ruta + "/MigracionFacturas_"+ fmt2.format(fechaActual) +".txt");
            pw = new PrintWriter(new BufferedWriter(fw));
            
            
            try{
                model = new Model();
                writeLog("*************************************************");
                writeLog("* Inicio del Proceso de Importacion de Facturas *");
                writeLog("*************************************************");
                model = new Model();
                Procesar();
                writeLog("*************************************************");
                writeLog("*               Proceso Finalizado              *");
                writeLog("*************************************************");
            }catch (Exception ex){
                ex.printStackTrace();
                writeLog("*************************************************");
                writeLog("*    Finalizando Proceso pero con anomalias     *");
                writeLog("*************************************************");
                writeLog(ex.getMessage());
            }
            
            
            pw.close();
        }catch (Exception ex){
            ex.printStackTrace();
            //System.out.println(ex.getMessage());
        }
    }
    
    
    
    /**
     * procedimiento general de importacion lectura y agrupacion de de datos
     * @autor mfontalvo
     * @throws Exception.
     * @see ProcesarFactura(List , TreeMap , TreeMap )
     */
    
    public void Procesar()throws Exception{
        
        try {
            int fprocesadas   = 0;
            int fnoprocesadas = 0;
            List lista = model.ImportacionCXPSvc.searchFacturasPendientes(usuario.getLogin());
            if (lista != null && !lista.isEmpty()){
                
                CXPImportacion anterior = null;
                int ranInicial = 0;
                
                /** busqueda parametros de busqueda general */
                cias    = model.ImportacionCXPSvc.searchMonedasCias();
                tbldoc  = initDocumentos();
                monedas = initMonedas(); 
                
                
                /** recorriendo las facturas pendientes por subir */
                for (int ranFinal = 0; ranFinal < lista.size(); ranFinal++){
                    
                    /** obteniendo objeto de  la lista */
                    CXPImportacion item = (CXPImportacion) lista.get(ranFinal);
                    
                    /** procesa una factura al detectar una nueva */
                    if (anterior!=null && !anterior.getKey().equals(item.getKey())){
                        
                        /** intenta procesar una factura con sus items */
                        try{
                            //System.out.println("C:" + ranInicial + " - " + (ranFinal));
                            if (ProcesarFactura(lista.subList(ranInicial, ranFinal)))
                                fprocesadas++;
                            else
                                fnoprocesadas++;
                            
                        }catch (Exception ex){
                            fnoprocesadas++;
                            writeLog2 ( ex.getMessage() );
                        }
                        writeLog2("");
                        ranInicial = ranFinal;
                    }
                    anterior = item;
                }
                
                /** intenta procesar la ultima factura con sus items */
                try{
                    //System.out.println("F:" + ranInicial + " - " + lista.size());
                    if (lista.subList(ranInicial, lista.size())!=null && !lista.subList(ranInicial, lista.size()).isEmpty()){
                        if (ProcesarFactura(lista.subList(ranInicial, lista.size())))
                            fprocesadas++;
                        else
                            fnoprocesadas++;
                    }
                }catch (Exception ex){
                    fnoprocesadas++;
                    writeLog2 ( ex.getMessage() );
                }
                writeLog2("");
                writeLog2("");
                writeLog2("");
                writeLog2("RESUMEN : ");
                writeLog2("Facturas PROCESADAS    : " + fprocesadas   );
                writeLog2("Facturas NO PROCESADAS : " + fnoprocesadas );
                writeLog2("");
            }
            
        }
        catch(Exception e) {
            e.printStackTrace();
            logger.error( e.getMessage() );
            writeLog2   ( e.getMessage() );
            throw new Exception(e.getMessage());
        }
    }
    
    
    /** procedimiento de una factura especifica con sus items */
    
    public boolean ProcesarFactura(List factura)throws Exception{
        String msgError = "";
        boolean estado = false;
        if (factura!=null && !factura.isEmpty()){
            
            CXPImportacion pItem =  (CXPImportacion) factura.get(0);
            
            writeLog2("____________________________________________");
            writeLog2("Distrito   : " + pItem.getDstrct()         );
            writeLog2("Proveedor  : " + pItem.getProveedor()      );
            writeLog2("Tipo Doc   : " + pItem.getTipo_documento() );
            writeLog2("Documento  : " + pItem.getDocumento()      );            
            writeLog2("Nro. Items : " + factura.size() + "\n"     );
            //System.out.println("ITEMS: " + factura.size());
            proveedor = null;
            banco     = null;
            
            try {
                TreeMap impItem = new TreeMap();
                TreeMap impFact = new TreeMap();
                Vector  items   = new Vector ();
                CXP_Doc cab     = verificacionCabecera(pItem);
                double total_bruto = 0;
                double total_fact  = 0;
                /** recorriendo los items de la factura */
                boolean sw = true;
                for (int  i = 0 ; i < factura.size() ; i++) {
                    CXPImportacion item = (CXPImportacion) factura.get(i);
                    // cambio del numero del item a un numero valido
                    item.setItem( this.getItem(item.getItem()));
                    writeLog2("Verificando Item : " + item.getItem());
                    item.setItem( this.getItem( "" + (i+1) ));
                    try{
                        CXPItemDoc it = verificacionItem(cab, item);
                        total_bruto += it.getVlr_me();
                        total_fact  += it.getVlr_me();
                        total_fact  += addImpuestos(impFact, impItem, it, item, cab);
                        items.add(it);
                    } catch (Exception ex){
                        writeLog2(ex.getMessage() + "\n");
                        sw = false;
                    }
                }
                
                if (sw){
                    // validacion de totales
                    if (total_bruto != cab.getVlr_neto_me())
                        throw new Exception("El valor de factura ["+ cab.getVlr_neto_me() +"], no corresponde con la sumatoria de sus items ["+ total_bruto +"], favor verificar");

                    // actualizacion de totales
                    cab.setVlr_neto_me          ( getValor(total_fact                , cab.getMoneda() ) );
                    cab.setVlr_neto             ( getValor(total_fact * cab.getTasa(), cia.getMoneda() ) );
                    cab.setVlr_saldo_me         ( cab.getVlr_neto_me() );
                    cab.setVlr_saldo            ( cab.getVlr_neto   () );                    
                    
                    // grabacion de la factura
                    try{
                        Vector impuestosDoc = new Vector(impFact.values());
                        model.cxpDocService.insertarCXPDoc(cab, items, impuestosDoc, cab.getAgencia());
                    }catch (Exception ex){
                        throw new Exception("Error al momento de grabar factura.... \n" + ex.getMessage());
                    }
                    model.ImportacionCXPSvc.updateFacturasMigrada(cab);
                    writeLog2("Factura procesada con exito.");
                    estado = true;
                }
            }
            catch(Exception e) {
                e.printStackTrace();
                throw new Exception(e.getMessage());
            }
        }
        return estado;
    }
    
    
    private CXP_Doc verificacionCabecera(CXPImportacion pItem) throws Exception {
        try{
            
            String msgError = "";
            
            CXP_Doc cab     = new CXP_Doc();
            CXP_Doc doc_rel = null;

            // compa�ia
            cia = (CIA) cias.get( pItem.getDstrct() );
            if (cia==null){
                msgError += "\nDistrito '"+ pItem.getDstrct() +"' no esta registrado en la tabla de Cias";
            }
            
            if (cia!=null && !cia.getDistrito().equals(usuario.getDstrct())){
                msgError += "\nEl distrito del usuario que ejecuta el proceso es diferente al de la factura. deben ser iguales";
            }            
            
            
            
            // proveedor
            proveedor = model.ImportacionCXPSvc.searchDatosProveedor(  pItem.getDstrct() , pItem.getProveedor() );
            if (proveedor==null){
                msgError += "\nProveedor '"+ pItem.getProveedor() +"', no registrado en la base de datos";
            }
            // tipo de documento
            if (tbldoc.get( pItem.getTipo_documento() )==null ){
                msgError += "\nTipo de Documento  '" + pItem.getTipo_documento() + "', no es valido.";
            }
            // tipo de documento relacionado
            if ( !pItem.getTipo_documento_rel().equals("") && !pItem.getTipo_documento_rel().equals("010") ){
                msgError += "\nEl documento relacionado solo puede ser una Factura (010)";
            }
            // tipo de documento relacionado
            if ( pItem.getTipo_documento_rel().equals("010") && pItem.getTipo_documento().equals("010") ){
                msgError += "\nEste tipo de documento no puede tener documentos relacionados del mismo tipo";
            }            
            
            //  documento
            if (pItem.getDocumento().equals("")){
                msgError += "\nDefina el numero del Documento";
            } else  if (model.ImportacionCXPSvc.searchCXPDoc(pItem.getDstrct(),pItem.getProveedor(),pItem.getTipo_documento(),pItem.getDocumento())!=null ){
                msgError += "\nEl documento ya esta registrado en la base de datos";
            }
            
            // documento relacionado
            if (!pItem.getDocumento_relacionado().equals("") && !pItem.getTipo_documento_rel().equals("")){
                doc_rel = model.ImportacionCXPSvc.searchCXPDoc(pItem.getDstrct(),pItem.getProveedor(),pItem.getTipo_documento_rel(),pItem.getDocumento_relacionado());
                if (  doc_rel == null ){
                    msgError += "\nEl documento relacionado [" + pItem.getDstrct() + "-" + pItem.getProveedor() + "-" +pItem.getTipo_documento_rel() + "-" + pItem.getDocumento_relacionado() +"], no esta registrado en la base de datos";
                } else {
                    if (doc_rel.getVlr_saldo_me() == 0 )
                        msgError += "\nEl documento relacionado [" + pItem.getDstrct() + "-" + pItem.getProveedor() + "-" +pItem.getTipo_documento_rel() + "-" + pItem.getDocumento_relacionado() +"], no se le puede asociar ningun documento, por que ya esta cancelado totalmente.";                        
                }
            }else if  (!pItem.getDocumento_relacionado().equals("") && !pItem.getTipo_documento_rel().equals("")) {
                msgError += "\nDebe definir documento relacionado y tipo documento relacionado al mismo tiempo\n si desea a asociar este documento a otro.";
            }
            
            
            // banco y sucursal
            banco = model.ImportacionCXPSvc.searchBanco(pItem.getDstrct(), pItem.getBanco(), pItem.getSucursal());
            if (banco==null){
                msgError += "\nEl banco y sucursal '"+ pItem.getBanco() + "-" + pItem.getSucursal() +"' no estan registrados en la base de datos " ;
            }
            if (pItem.getPlazo() < 0 ){
                msgError += "\nLa fecha de Vencimiento no puede ser mayor que la fecha de documento  " ;                
            }
            
            
            // handle_code
            String handle_code = "";
            if (pItem.getHc().trim().equals("")){
                handle_code = proveedor.getC_hc();
            } else {
                handle_code = pItem.getHc().trim();
                if ( !model.ImportacionCXPSvc.searchHC( handle_code )) {
                    msgError += "\nEl Handle Code '"+ handle_code +"', no esta registrado en la base de datos." ;
                }
            }
            
            
            // autorizador de la factura
            if (pItem.getAutorizador().equals("") ){
                msgError += "\nRequiere autorizador la factura";
            }else if ( !model.ImportacionCXPSvc.searchAutorizador(pItem.getAutorizador()) ){
                msgError += "\nEl autorizador "+ pItem.getAutorizador() +", no esta registrado en el sistema.";
            }            
            
            // moneda
            double tasa = 0;
            if  ( monedas.get( pItem.getMoneda() )==null ) {
                msgError += "\nLa moneda de la factura no es valida.";
            } else {
                if  (!cia.getMoneda().equals( pItem.getMoneda() ) ) {
                   tasa = model.ImportacionCXPSvc.searchTasa( cia.getDistrito(), pItem.getFecha_documento(), pItem.getMoneda(), cia.getMoneda());
                   if (tasa == 0) msgError += "\nNo se pudo convertir la moneda de [" + pItem.getMoneda() + "-" + cia.getMoneda() + "]";
                } else
                    tasa = 1;
            }
            
            
            if (!usuario.getLogin().equals( pItem.getCreation_user() )){
                msgError += "\nEl usuario creacion debe archivo debe ser el mismo que ejecute el proceso";
            }
            if (!usuario.getLogin().equals( pItem.getUser_update() )){
                msgError += "\nEl usuario actualizacion debe archivo debe ser el mismo que ejecute el proceso";
            }            

            
            if (!msgError.equals(""))
                throw new Exception (msgError.replaceFirst("\n",""));
            cab.setDstrct               ( pItem.getDstrct() );
            cab.setProveedor            ( pItem.getProveedor());
            cab.setTipo_documento       ( pItem.getTipo_documento() );
            cab.setDocumento            ( pItem.getDocumento() );        
            cab.setDescripcion          ( pItem.getDescripcion_doc() );
            cab.setFecha_documento      ( pItem.getFecha_documento() );
            cab.setFecha_vencimiento    ( pItem.getFecha_vencimiento() );
            cab.setPlazo                ( pItem.getPlazo());
            cab.setTipo_documento_rel   ( pItem.getTipo_documento_rel() );
            cab.setDocumento_relacionado( pItem.getDocumento_relacionado() );
            cab.setVlr_neto_me          ( pItem.getVlr_neto_me());
            cab.setTasa                 ( tasa );
            cab.setVlr_total_abonos_me  ( 0 );
            cab.setVlr_total_abonos     ( 0 );
            cab.setBanco                ( pItem.getBanco());
            cab.setSucursal             ( pItem.getSucursal());
            cab.setMoneda               ( pItem.getMoneda());          
            cab.setAgencia              ( banco.getCodigo_Agencia());
            cab.setHandle_code          ( handle_code  );
            cab.setId_mims              ( proveedor.getC_idMims());
            cab.setCreation_date        ( pItem.getCreation_date()   );
            cab.setCreation_user        ( pItem.getCreation_user()   );
            cab.setUser_update          ( pItem.getCreation_user()     );
            cab.setBase                 ( usuario.getBase()          );
            cab.setFecha_aprobacion     ( "0099-01-01" );
            cab.setUsuario_aprobacion   ( "" );
            cab.setUltima_fecha_pago    ( "0099-01-01" );
            cab.setUsuario_contabilizo  ( "" );
            cab.setFecha_contabilizacion( "0099-01-01" );
            cab.setUsuario_anulo        ( "" );
            cab.setFecha_anulacion      ( "0099-01-01" );
            cab.setFecha_contabilizacion_anulacion( "0099-01-01" );
            cab.setObservacion          ( "" );
            cab.setNum_obs_autorizador  ( 0  );
            cab.setNum_obs_pagador      ( 0  );
            cab.setNum_obs_registra     ( 0  );
            cab.setMoneda_banco         ( banco.getMoneda() );
            cab.setClase_documento      ( pItem.getClase_documento() );
            cab.setObservacion          ( pItem.getObservacion() );
            cab.setAprobador            ( pItem.getAutorizador() );
            
            cab.setClase_documento_rel( "4" );
            if ( doc_rel != null )
                cab.setClase_documento_rel( doc_rel.getClase_documento() );
             //aprobacion de facturas
            if ( cab.getCreation_user().equalsIgnoreCase(cab.getAprobador() ) || cab.getVlr_neto_me() < 0){
                cab.setFecha_aprobacion   ( cab.getCreation_date() );
                cab.setUsuario_aprobacion ( cab.getAprobador()     );
            }
            return cab;
            
        } catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
    }

    
    
    private CXPItemDoc verificacionItem(CXP_Doc cab, CXPImportacion item) throws Exception{
        try{
            
            String msgError = "";
            
            CXPItemDoc it = new CXPItemDoc();
            // validar otras cabeceras contra la primera
            if (!cab.getDstrct().equals( item.getDstrct())){
                msgError += "\nDistrito '"+ item.getDstrct() +"', no corresponde con el distrito del item base.";
            }
            if (!cab.getProveedor().equals( item.getProveedor())){
                msgError += "\nProveedor '" + item.getProveedor() + "', no corresponde con el proveedor del item base";
            }
            if (!cab.getTipo_documento().equals( item.getTipo_documento() )){
                msgError += "\nTipo de Documento '"+ item.getTipo_documento() +"', no corresponde con el tipo de documento del item base";
            }
            if (!cab.getDocumento().equals( item.getDocumento()  )){
                msgError += "\nDocumento '"+ item.getDocumento() +"' , no corresponde con el documento del item base" ;
            }
            if (!cab.getTipo_documento_rel().equals( item.getTipo_documento_rel())){
                msgError += "\nDocumento relacionado '" + item.getItem()  +"', no corresponde con el documento relacionado del item base";
            }
            if (!cab.getDocumento_relacionado().equals( item.getDocumento_relacionado()  )){
                msgError += "\nDocumento relacionado '" + item.getDocumento_relacionado()  +"', no corresponde con el documento del item base";
            }
            if (!cab.getFecha_documento().equals( item.getFecha_documento() )){
                msgError += "\nFecha Documento : '" + item.getFecha_documento()  +"', no corresponde con la fecha del documento del item base";
            }
            if (!cab.getFecha_vencimiento().equals( item.getFecha_vencimiento() )){
                msgError += "\nFecha Vencimiento Documento '" + item.getFecha_documento()  +"', no corresponde con la fecha de vencimiento del documento del item base";
            }
            if (!cab.getDescripcion().equals( item.getDescripcion_doc() )){
                msgError += "\nDescripcion del Documento '" + item.getDescripcion_doc()  +"', no corresponde con la descripcion del documento del item base";
            }
            if (cab.getVlr_neto_me() != item.getVlr_neto_me() ){
                msgError += "\nEl valor neto del item '" + item.getVlr_neto_me()  +"', no corresponde con el valor neto del item base";
            }
            if (!cab.getBanco().equals( item.getBanco() ) ){
                msgError += "\nEl banco del item  '" + item.getItem()  +"', no corresponde con el banco del item base";
            }
            if (!cab.getSucursal().equals( item.getSucursal() ) ){
                msgError += "\nLa sucursal del item '" + item.getSucursal()  +"', no corresponde con la sucursal del item base";
            }
            if (!cab.getMoneda().equals( item.getMoneda() ) ){
                msgError += "\nLa moneda del item : '" + item.getMoneda()  +"', no corresponde con la moneda del item base";
            }
            if (!item.getHc().trim().equals("") && !cab.getHandle_code().equals( item.getHc().trim() ) ){
                msgError += "\nEl Handle Code del item : '" + item.getHc()  +"', no corresponde con el Handle Code del item base";
            }            
            //if (!cab.getCreation_date().equals( item.getCreation_date())){
              //  msgError += "\nCreation Date '" + item.getCreation_date()  +"', no corresponde con la fecha de creacion del item base";
                //throw new Exception("Creation Date del Item : [" + item.getItem()  +"], no corresponde con la fecha de creacion del item base");
            //}
            if (!cab.getCreation_user().equals( item.getCreation_user())){
                msgError += "\nCreation User del Item : '" + item.getCreation_user()  +"', no corresponde con el usuario de creacion del item base";
            }  
            if (!cab.getObservacion().equals( item.getObservacion())){
                msgError += "\nObservacion '" + item.getObservacion()  +"', no corresponde con la observacion del item base";
            }
            if (!cab.getAprobador().equals( item.getAutorizador())){
                msgError += "\nAutorizador : '" + item.getAutorizador()  +"', no corresponde con el autorizador del item base";
            }              


            if ( !item.getPlanilla().equals("") && !model.ImportacionCXPSvc.searchPlanilla(item.getPlanilla()) ){
                msgError += "\nLa planilla '"+ item.getPlanilla() +"' asociada al item, no existe.";
                //throw new Exception ("La planilla "+ item.getPlanilla() +" asociada al item, no existe.");
            }
            
            
            
            // validaciones de la cuenta
            if ( item.getCodigo_cuenta().trim().equals("") )
                msgError += "\nDebe indicar el codigo de cuenta";
            else {
                String msg = model.ImportacionCXPSvc.searchCuenta(item.getDstrct(), item.getCodigo_cuenta(), item.getTipo_auxiliar(), item.getAuxiliar() );
                if ( !msg.equals("OK" ) )  msgError += "\n" + msg;
            }

            if ( !item.getCodigo_cuenta().trim().equals("") && "CGI".indexOf(item.getCodigo_cuenta().substring(0,1))!=-1 ){
                if (item.getCodigo_abc().trim().equals("")){
                    msgError += "\nSe requiere codigo ABC";
                } else if ( !model.ImportacionCXPSvc.searchABC(item.getCodigo_abc().trim()) ){
                    msgError += "\nEl codigo ABC '"+ item.getCodigo_abc().trim() +"' no esta registrado.";
                }
            }else{
                if ( !item.getCodigo_abc().trim().equals("") && !model.ImportacionCXPSvc.searchABC(item.getCodigo_abc().trim()) )
                    msgError += "\nEl codigo ABC '"+ item.getCodigo_abc().trim() +"' no esta registrado.";
                    //throw new Exception("El codigo ABC no esta registrado.");        
            }
            
            if (!msgError.equals("")){
                throw new Exception (msgError.replaceFirst("\n",""));
            }


            it.setDstrct        ( cab.getDstrct() );
            it.setProveedor     ( cab.getProveedor() );
            it.setTipo_documento( cab.getTipo_documento() );
            it.setDocumento     ( cab.getDocumento() );
            it.setItem          ( item.getItem() );
            it.setDescripcion   ( item.getDescripcion_item() );
            it.setVlr_me        ( getValor(item.getVlr_me(), cab.getMoneda()) );
            it.setVlr           ( getValor(item.getVlr_me() * cab.getTasa(),  cia.getMoneda() ) );
            it.setCodigo_cuenta ( item.getCodigo_cuenta() );
            it.setTipoSubledger ( item.getTipo_auxiliar());
            it.setAuxiliar      ( item.getAuxiliar());
            it.setCodigo_abc    ( item.getCodigo_abc() );
            it.setPlanilla      ( item.getPlanilla() );
            it.setCreation_date ( cab.getCreation_date());
            it.setCreation_user ( cab.getCreation_user() );
            it.setUser_update   ( cab.getUser_update() );
            it.setBase          ( cab.getBase());
            it.setCodcliarea    ( "" );
            it.setTipcliarea    ( "" );
            it.setConcepto      ( "" );
            return it;
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception (ex.getMessage());
        }
    }
    
    
    
    private double addImpuestos(TreeMap impFact, TreeMap impItem, CXPItemDoc item, CXPImportacion itemImp, CXP_Doc cab) throws Exception{
        try{
            double totalImp = 0;
            
            if (impItem == null)
                throw new Exception("Inicializar lista de impuestos de items");
            
            if (impFact == null)
                throw new Exception("Inicializar lista de impuestos de factura");
            String msgError = "";
            
            if (itemImp!=null){
                Tipo_impuesto i_iva  = null;
                Tipo_impuesto i_riva = null;
                Tipo_impuesto i_rica = null;
                Tipo_impuesto i_rfte = null;
                CXPImpItem impIva  = null;
                CXPImpItem impRiva = null;
                CXPImpItem impRica = null;
                CXPImpItem impRfte = null;
                Vector vimp = new Vector();

                ////////////////////////////////////////////////////////////////
                // IVA
                if (!itemImp.getIva().equals("") ){
                    i_iva = model.ImportacionCXPSvc.searchValorImpuesto(item.getDstrct(), itemImp.getIva(), itemImp.getRiva() , "IVA", cab.getFecha_documento(), "");
                    if (i_iva!=null){
                        impIva = loadImpuesto(item, i_iva, cab);
                        vimp.add(impIva);
                        totalImp += impIva.getVlr_total_impuesto_me();
                        addImpuestosFactura(impFact, impIva);
                    } else
                        msgError += "\nCodigo de IVA '"+ itemImp.getIva() +"', no esta en la base de datos o no existe la relacion con el RIVA definido";
                }
                
                
                ////////////////////////////////////////////////////////////////
                // RIVA
                if (!itemImp.getRiva().equals("") ){
                    if (proveedor.getC_agente_retenedor().equals("N") && proveedor.getC_autoretenedor_iva().equals("S") ){
                        i_riva = model.ImportacionCXPSvc.searchValorImpuesto(item.getDstrct(), itemImp.getRiva() , "", "RIVA", cab.getFecha_documento(), "");
                        if (i_riva!=null){
                            if (i_iva!=null){
                                impRiva = loadImpuesto(item, i_riva, cab);
                                // se calcula diferente
                                impRiva.setVlr_total_impuesto   ( getValor( item.getVlr() * (impIva.getPorcent_impuesto() / 100.0) * ( i_riva.getPorcentaje1() / 100.0 ) * i_riva.getInd_signo()  , cia.getMoneda() ) );
                                impRiva.setVlr_total_impuesto_me( getValor( item.getVlr() * (impIva.getPorcent_impuesto() / 100.0) * ( i_riva.getPorcentaje1() / 100.0 ) * i_riva.getInd_signo()  , cia.getMoneda() ) );
                                vimp.add(impRiva);
                                totalImp += impRiva.getVlr_total_impuesto_me();
                                addImpuestosFactura(impFact, impRiva);                                
                            } else
                                msgError += "\nCodigo de RIVA '"+ itemImp.getRiva() +"', debe estar relacionda al IVA";
                        }
                        else
                            msgError += "\nCodigo de RIVA '"+ itemImp.getRiva() +"', no esta en la base de datos";
                    }
                    else
                        msgError += "\nRIVA, no puede ser aplicado segun la configuracion del proveedor";
                }
                
                
                ////////////////////////////////////////////////////////////////
                // RICA
                if (!itemImp.getRica().equals("") ){
                    String agencia = (banco.getCodigo_Agencia().equals("OP")?"BQ":banco.getCodigo_Agencia());
                    if (proveedor.getC_agente_retenedor().equals("N") && proveedor.getC_autoretenedor_ica().equals("S") ){
                        i_rica = model.ImportacionCXPSvc.searchValorImpuesto(item.getDstrct(), itemImp.getRica() , "", "RICA", cab.getFecha_documento(), agencia);
                        if (i_rica!=null){
                            impRica = loadImpuesto(item, i_rica, cab);
                            vimp.add(impRica);
                            totalImp += impRica.getVlr_total_impuesto_me();
                            addImpuestosFactura(impFact, impRica);
                        }
                        else
                            msgError += "\nCodigo de RICA '"+ itemImp.getRica() +"', no esta en la base de datos";
                    }
                    else
                        msgError += "\nRICA, no puede ser aplicado segun la configuracion del proveedor";
                }                
                
                ////////////////////////////////////////////////////////////////
                // RFTE
                if (!itemImp.getRfte().equals("") ){
                    if (proveedor.getC_agente_retenedor().equals("N") && proveedor.getC_autoretenedor_rfte().equals("S") ){
                        i_rfte = model.ImportacionCXPSvc.searchValorImpuesto(item.getDstrct(), itemImp.getRfte() , "", "RFTE", cab.getFecha_documento(), "");
                        if (i_rfte!=null){
                            impRfte = loadImpuesto(item, i_rfte, cab);
                            vimp.add(impRfte);
                            totalImp += impRfte.getVlr_total_impuesto_me();
                            addImpuestosFactura(impFact, impRfte);
                        }
                        else
                            msgError += "\nCodigo de Retefuente '"+ itemImp.getRfte() +"', no esta en la base de datos";
                    }
                    else
                        msgError += "\nRETEFUENTE, no puede ser aplicado segun la configuracion del proveedor";
                } 
                
                
                ////////////////////////////////////////////////////////////////
                if (!msgError.equals(""))
                    throw new Exception(msgError);
                item.setVItems(vimp);
            }
            return totalImp;
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage()) ;
        }
    }

    
    public CXPImpItem loadImpuesto (CXPItemDoc item, Tipo_impuesto pi,  CXP_Doc cab) throws Exception{
        try{
            CXPImpItem imp = new CXPImpItem();
            imp.setDstrct               (item.getDstrct());
            imp.setProveedor            (item.getProveedor());
            imp.setDocumento            (item.getDocumento());
            imp.setTipo_documento       (item.getTipo_documento());
            imp.setItem                 (item.getItem());
            imp.setBase                 (item.getBase());
            ////////////////////////////////////////////////////////////////
            imp.setCod_impuesto         (pi.getCodigo_impuesto());
            imp.setPorcent_impuesto     (pi.getPorcentaje1() * pi.getInd_signo());
            imp.setVlr_total_impuesto   ( getValor( ((pi.getPorcentaje1() * pi.getInd_signo()) /100.0) * item.getVlr   (), cia.getMoneda() ) );
            imp.setVlr_total_impuesto_me( getValor( ((pi.getPorcentaje1() * pi.getInd_signo()) /100.0) * item.getVlr_me(), cab.getMoneda() ) );
            imp.setCreation_date        (item.getCreation_date());
            imp.setCreation_user        (item.getCreation_user());
            imp.setUser_update          (item.getCreation_user());
            ////////////////////////////////////////////////////////////////
            return imp;
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception (ex.getMessage());
        }
    }
    
    public void addImpuestosFactura (TreeMap impFact, CXPImpItem impi) throws Exception{
        try{
            CXPImpDoc  impf = (CXPImpDoc) impFact.get(impi.getCod_impuesto());
            // adiciona el impuesto si no lo encuentra
            if (impf==null){
                impf = new CXPImpDoc();
                impf.setDstrct               (impi.getDstrct());
                impf.setProveedor            (impi.getProveedor());
                impf.setDocumento            (impi.getDocumento());
                impf.setTipo_documento       (impi.getTipo_documento());
                impf.setCod_impuesto         (impi.getCod_impuesto());
                impf.setPorcent_impuesto     (impi.getPorcent_impuesto());
                impf.setVlr_total_impuesto   (impi.getVlr_total_impuesto());
                impf.setVlr_total_impuesto_me(impi.getVlr_total_impuesto_me());
                impf.setCreation_user        (impi.getCreation_user());
                impf.setCreation_date        (impi.getCreation_date());
                impf.setUser_update          (impi.getUser_update());
                impf.setBase                 (impi.getBase());
                impFact.put(impf.getCod_impuesto() , impf);
            }

            // actualiza si lo encuentra
            else{
                //impf.setPorcent_impuesto     (impf.getPorcent_impuesto() + impi.getPorcent_impuesto() );
                impf.setVlr_total_impuesto   (impf.getVlr_total_impuesto() + impi.getVlr_total_impuesto());
                impf.setVlr_total_impuesto_me(impf.getVlr_total_impuesto_me() + impi.getVlr_total_impuesto_me());
            }
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
    }
    
    public TreeMap initMonedas (){
        TreeMap mon = new TreeMap();
        mon.put("PES","PES");
        mon.put("DOL","DOL");
        mon.put("BOL","BOL");
        mon.put("SUC","SUC");
        return mon;
    }
    
    
    public TreeMap initDocumentos (){
        TreeMap mon = new TreeMap();
        mon.put("010","010");
        mon.put("035","035");
        mon.put("036","036");
        return mon;
    }    
    

    
    public double getValor (double valor, String moneda){
        int decimales = (moneda.equals("DOL")?2:0);
        return com.tsp.util.Util.roundByDecimal(valor, decimales);
    }
    
    public void writeLog(String msg)throws Exception {
        try{
            if (pw!=null ){
                String []msgs = msg.split("\\n");
                for (int i = 0; i< msgs.length;i++)
                    pw.println("[" + getCurrentTime() + "] " +  msgs[i] );
                pw.flush();
            }
        } catch (Exception e){
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }
    public void writeLog2(String msg)throws Exception {
        try{
            if (pw!=null ){
                String []msgs = msg.split("\\n");
                for (int i = 0; i< msgs.length ; i++)
                    pw.println( msgs[i] );
                pw.flush();
            }
        } catch (Exception e){
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }    
    public String getCurrentTime(){
        fechaActual.setTime( System.currentTimeMillis());
        return fmt.format(fechaActual );
    }

    
    public static void main(String [] args) throws Exception{
/*        HImportacionCXP imp = new HImportacionCXP(null);
        */
        Usuario usuario = new Usuario();
        usuario.setLogin("HOSORIO");
        Evento event = (Evento) com.tsp.util.Util.runClass("com.tsp.operation.model.threads.HImportacionCXP");
        event.setUsuario(usuario);
        event.start();
    }
    
    
    /**
     * Metodo para obtener el numero valido del item
     * @autor mfontalvo
     * @param item, cadena a cambiar
     * @throws Exception.
     * @return Numero valido del item
     **/
    public String getItem(String item) throws Exception{
        try{
            String is = "";
            int    in = Integer.parseInt(item);
            if (in < 100)
                is = com.tsp.util.UtilFinanzas.rellenar( String.valueOf(in) , "0", 3);
            else 
                is = String.valueOf(in);
                
            return is;            
        } catch (Exception ex) {
            throw new Exception("Item no valido");
        }
    }
}
