/*
 * HReportePPP.java
 *
 * Created on 9 de marzo de 2007, 10:57 AM
 */

package com.tsp.operation.model.threads;


import com.tsp.operation.model.beans.POIWrite;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.beans.Remesa;
import com.tsp.operation.model.Model;

import java.io.*;
import java.util.*;
import java.text.*;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.util.HSSFColor;
/**
 *
 * @author  equipo
 */
public class HReportePPP extends Thread {
    
    
    
    String finicialRNF;
    String ffinalRNF;
    String finicialRFNP;
    String ffinalRFNP;
    String finicialRP;
    String ffinalRP;    
    Model model;
    Usuario usuario;
    SimpleDateFormat fmt;
    String processName = "ReportePPP";
    private int xPPF = 20;
    
    
    
    // variables del archivo de excel
    String   ruta;
    String   nombre;
    POIWrite xls;
    HSSFCellStyle header  , titulo1, titulo2, titulo3 , titulo4, titulo5, letra, numero, dinero, numeroCentrado, porcentaje, letraCentrada, numeroNegrita, fechaCentrada;
    HSSFColor     cAzul   , cVerde, cAmarillo, cGris ;
    int fila = 0;
    
    
    /** Creates a new instance of HReportePPP */
    public HReportePPP() {
    }
    
    
    public void start(Model model, String finicialRNF, String ffinalRNF, String finicialRFNP, String ffinalRFNP, String finicialRP, String ffinalRP, Usuario usuario){
        this.finicialRNF   = finicialRNF;
        this.ffinalRNF     = ffinalRNF;
        this.finicialRFNP  = finicialRFNP;
        this.ffinalRFNP    = ffinalRFNP;
        this.finicialRP    = finicialRP;
        this.ffinalRP      = ffinalRP;
        this.usuario       = usuario;
        this.model         = model;
        super.start();
    }
    
    public synchronized void run(){
        try{
            
            
            model.LogProcesosSvc.InsertProceso(this.processName, this.hashCode(), "Generacion del Reporte PPP", usuario.getLogin());
            this.generarRUTA();
            this.generarArchivo();
            model.LogProcesosSvc.finallyProceso(this.processName, this.hashCode(), usuario.getLogin(), "PROCESO EXITOSO, Verifique el reporte en su listado de archivos.");
            
            
        }catch (Exception ex){
            ex.printStackTrace();
            try{
                model.LogProcesosSvc.finallyProceso(this.processName, this.hashCode(), usuario.getLogin(), "Finalizado con error ...\n" + ex.getMessage());
            }catch (Exception e){
                System.out.println("Error HReporteOcsConAnticipoSinReporte ...\n" + e.getMessage());
            }
        }
    }
    
    
    public void generarRUTA() throws Exception{
        try{
            
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            ruta = rb.getString("ruta") + "/exportar/migracion/" + usuario.getLogin();
            File archivo = new File( ruta );
            if (!archivo.exists()) archivo.mkdirs();
            
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
    }
    
    
    
    
    
    /**
     * Metodo para Incializar las variables del archivo de excel
     * @autor mfontalvo
     * @param nameFile, nombre del archivo
     * throws Exception.
     */
    private void InitArchivo(String nameFile) throws Exception{
        try{
            xls          = new com.tsp.operation.model.beans.POIWrite();
            nombre       = "/exportar/migracion/" + usuario.getLogin() + "/" + nameFile;
            xls.nuevoLibro( ruta + "/" + nameFile );
            
            // colores
/*            cAzul       = xls.obtenerColor(  0, 51,102);
            cVerde      = xls.obtenerColor( 51,153,102);
            cAmarillo   = xls.obtenerColor(255,255,153);
            cGris       = xls.obtenerColor(192,192,192);*/
            
            
            // estilos
            header         = xls.nuevoEstilo("Tahoma", 14, true  , false, "text"  , HSSFColor.ORANGE.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
            titulo1        = xls.nuevoEstilo("Tahoma", 8 , true  , false, "text"  , xls.NONE  , xls.NONE , xls.NONE);
            titulo2        = xls.nuevoEstilo("Tahoma", 8 , true  , false, "text"  , HSSFColor.WHITE.index, HSSFColor.DARK_BLUE.index, HSSFCellStyle.ALIGN_CENTER, 2);
            titulo3        = xls.nuevoEstilo("Tahoma", 8 , true  , false, "text"  , HSSFColor.WHITE.index, HSSFColor.LIGHT_YELLOW.index , HSSFCellStyle.ALIGN_CENTER, 2);
            titulo4        = xls.nuevoEstilo("Tahoma", 8 , true  , false, "text"  , HSSFColor.WHITE.index, HSSFColor.GREEN.index, HSSFCellStyle.ALIGN_CENTER, 2);
            titulo5        = xls.nuevoEstilo("Tahoma", 14, true  , false, "text"  , xls.NONE  , xls.NONE , xls.NONE);
            letra          = xls.nuevoEstilo("Tahoma", 8 , false , false, ""      , xls.NONE , xls.NONE , xls.NONE);
            letraCentrada  = xls.nuevoEstilo("Tahoma", 8 , false , false, ""      , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_CENTER);
            numero         = xls.nuevoEstilo("Tahoma", 8 , false , false, "#"     , xls.NONE , xls.NONE , xls.NONE);
            numeroCentrado = xls.nuevoEstilo("Tahoma", 8 , false , false, "#"     , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_CENTER);
            dinero         = xls.nuevoEstilo("Tahoma", 8 , false , false, "#,###"  , xls.NONE , xls.NONE , xls.NONE);
            numeroNegrita  = xls.nuevoEstilo("Tahoma", 8 , true  , false, "#"         , xls.NONE , xls.NONE , xls.NONE);
            porcentaje     = xls.nuevoEstilo("Tahoma", 8 , false , false, "0.00%"     , xls.NONE , xls.NONE , xls.NONE);
            fechaCentrada  = xls.nuevoEstilo("Tahoma", 8 , false , false, "YYYY-MM-DD", xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_CENTER);
            
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception("Error en InitArchivo .... \n" + ex.getMessage());
        }
        
    }
    
    
    
    /**
     * Metodo para crear el  archivo de excel
     * @autor mfontalvo
     * @param nameFile, Nombre del archivo
     * @throws Exception.
     */
    private void crearArchivo(String nameFile) throws Exception{
        try{
            fmt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            InitArchivo(nameFile);
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception( "Error en crearArchivo ....\n"  + ex.getMessage() );
        }
    }
    
    
    
    /**
     * Metodo para crear el  archivo de excel
     * @autor mfontalvo
     * @param nameFile, Nombre del archivo
     * @param titulo, titulo del archivo de excel
     * @throws Exception.
     */
    private void crearHoja(String hoja, String titulo, String finicial, String ffinal) throws Exception{
        try{
            xls.obtenerHoja(hoja);
            
            fila = 0;
            xls.combinarCeldas(fila  , 0, fila, 8);
            xls.adicionarCelda(fila++, 0, titulo, header);
            xls.adicionarCelda(fila  , 0, "FECHA" , titulo1);
            xls.adicionarCelda(fila++, 1, fmt.format( new Date())  , titulo1 );
            
            if (!finicial.equals("")){
                xls.adicionarCelda(fila  , 0, "PERIODO", titulo1);
                xls.adicionarCelda(fila++,1, finicial + " - " + ffinal  , titulo1);
            }
            
            xls.adicionarCelda(fila  ,0, "USUARIO", titulo1);
            xls.adicionarCelda(fila++,1, usuario.getNombre() , titulo1);
            
            fila = 6;
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception( "Error en crearArchivo ....\n"  + ex.getMessage() );
        }
    }
    
    
    
    /**
     * Metodo para cerrar el  archivo de excel
     * @autor mfontalvo
     * @throws Exception.
     */
    private void cerrarArchivo() throws Exception {
        try{
            if (xls!=null)
                xls.cerrarLibro();
        }catch (Exception ex){
            throw new Exception( "Error en crearArchivo ....\n"  + ex.getMessage() );
        }
    }
    
    
    /**
     * Exportacion del las amortizaciones transferidas
     * @autor mfontalvo
     * @throws Exception.
     */
    private void generarArchivo() throws Exception {
        try{
            fmt= new SimpleDateFormat("yyyMMdd_hhmmss");
            this.crearArchivo("ReportePPP_" + fmt.format( new Date() ) +".xls");
            TreeMap resumen = new TreeMap();
            
            // inicializacion contante ppf
            try{
                xPPF = Integer.parseInt( model.reportePPPSvc.obtenerDatoTG("XPPF") );
            } catch (Exception e){
                xPPF = 20;
            }
            
            
            
            // remesas no facturadas
            seccionRNF(resumen);
            
            // remesas facturadas no pagadas
            seccionRFNP(resumen);
            
            // remesas facturadas no pagadas
            seccionRFP(resumen);
            
            if (!resumen.isEmpty()){
                seccionResumen(resumen);
            }
            
            this.cerrarArchivo();
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception("Error en generarArchivo ...\n" + ex.getMessage() );
        }
    }
    
    
    
    private void seccionRNF(TreeMap resumen) throws Exception{
        try {
            
            model.reportePPPSvc.obtenerRNF(finicialRNF, ffinalRNF);
            Vector vector = model.reportePPPSvc.getRemesasNF();
            
            this.crearHoja("BASE RNF", "REMESAS POR FACTURAR", finicialRNF, ffinalRNF);
            if (vector!=null && !vector.isEmpty()){
                
                // encabezado
                String [] cabecera = {
                    "Agencia", "Codigo Cliente", "Cliente", "Remesa", "Fecha Remesa", "Valor Remesa", "Factura" , "Fecha Corte", "Dias", "Valor x Dias"
                };
                short  [] dimensiones = new short [] {
                    6000, 4000, 8000, 3500, 4000, 4000, 4000, 4000, 3000, 4000
                };
                for ( int i = 0; i<cabecera.length; i++){
                    xls.adicionarCelda(fila,  i, cabecera[i], titulo2);
                    if ( i < dimensiones.length )
                        xls.cambiarAnchoColumna(i, dimensiones[i] );
                }
                fila++;
                
                
                // detalle
                for ( int i = 0; i< vector.size(); i++, fila++){
                    int col = 0;
                    Remesa r = (Remesa) vector.get(i);
                    r.setValor_facturacion( r.getDias_facturacion() * r.getValorRemesa() );
                    r.setTipo_documento("RNF");
                    
                    
                    // datos de la planilla
                    xls.adicionarCelda(fila  , col++ , r.getNombre_ciu()    , letra          );
                    xls.adicionarCelda(fila  , col++ , r.getCodcli()        , letraCentrada  );
                    xls.adicionarCelda(fila  , col++ , r.getCliente()       , letra          );
                    xls.adicionarCelda(fila  , col++ , r.getNumrem()        , letraCentrada  );
                    xls.adicionarCelda(fila  , col++ , r.getFecrem()        , fechaCentrada  );
                    xls.adicionarCelda(fila  , col++ , r.getValorRemesa()   , dinero         );
                    xls.adicionarCelda(fila  , col++ , ""                   , letraCentrada  );
                    xls.adicionarCelda(fila  , col++ , r.getFecha_corte()   , fechaCentrada  );
                    xls.adicionarCelda(fila  , col++ , r.getDias_facturacion()    , numero   );
                    xls.adicionarCelda(fila  , col++ , r.getValor_facturacion()   , dinero   );
                    
                    
                    String key = "0#" + r.getCliente() + "#" + r.getCodcli();
                    Remesa r1 = (Remesa) resumen.get(key);
                    if ( r1 == null ){
                        resumen.put(key, r);
                    } else {
                        r1.setValorRemesa      ( r1.getValorRemesa()      + r.getValorRemesa()      );
                        r1.setDias_facturacion( r1.getDias_facturacion() + r.getDias_facturacion() );
                        // sumatoria valor_doc * #dias
                        r1.setValor_facturacion( r1.getValor_facturacion()  + r.getValor_facturacion() );
                    }
                    
                }
                
            } else {
                xls.adicionarCelda(fila  ,0, "NO HAY DATOS PARA MOSTRAR", titulo5);
                xls.combinarCeldas(fila , 0, fila, 0+5);
                fila++;
            }
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
    }
    
    private void seccionRFNP(TreeMap resumen) throws Exception{
        try {
            
            model.reportePPPSvc.obtenerRFNP(finicialRFNP, ffinalRFNP);
            Vector vector = model.reportePPPSvc.getRemesasFNP();
            
            this.crearHoja("BASE RFNP", "REMESAS FACTURADAS NO PAGADAS", finicialRFNP, ffinalRFNP);
            if (vector!=null && !vector.isEmpty()){
                
                // encabezado
                String [] cabecera = {
                    "Agencia", "Codigo Cliente", "Cliente", "Remesa", "Fecha Remesa", "Valor Remesa" ,"Factura" , "Fecha Factura", "Valor ItemF", "Fecha Corte", "Dias F", "Dias P",  "Valor x DiasF", "Valor x DiasP"
                };
                short  [] dimensiones = new short [] {
                    6000, 4000, 8000, 3500, 4000, 4000, 4000, 4000, 4000, 4000, 3000, 3000, 4000, 4000
                };
                for ( int i = 0; i<cabecera.length; i++){
                    xls.adicionarCelda(fila,  i, cabecera[i], titulo2);
                    if ( i < dimensiones.length )
                        xls.cambiarAnchoColumna(i, dimensiones[i] );
                }
                fila++;
                
                
                // detalle
                for ( int i = 0; i< vector.size(); i++, fila++){
                    int col = 0;
                    Remesa r = (Remesa) vector.get(i);
                    r.setValor_facturacion( r.getDias_facturacion() * r.getValor_itemf() );
                    r.setValor_pago       ( r.getDias_pago       () * r.getValor_itemf() );
                    r.setTipo_documento("RFNP");
                    
                    
                    // datos de la planilla
                    xls.adicionarCelda(fila  , col++ , r.getNombre_ciu()    , letra          );
                    xls.adicionarCelda(fila  , col++ , r.getCodcli()        , letraCentrada  );
                    xls.adicionarCelda(fila  , col++ , r.getCliente()       , letra          );
                    xls.adicionarCelda(fila  , col++ , r.getNumrem()        , letraCentrada  );
                    xls.adicionarCelda(fila  , col++ , r.getFecrem()        , fechaCentrada  );
                    xls.adicionarCelda(fila  , col++ , r.getValorRemesa()   , dinero         );
                    xls.adicionarCelda(fila  , col++ , r.getFactura()       , letraCentrada  );
                    xls.adicionarCelda(fila  , col++ , r.getFecha_factura() , letraCentrada  );
                    xls.adicionarCelda(fila  , col++ , r.getValor_itemf()   , dinero         );
                    
                    xls.adicionarCelda(fila  , col++ , r.getFecha_corte()         , fechaCentrada  );
                    xls.adicionarCelda(fila  , col++ , r.getDias_facturacion()    , numero   );
                    xls.adicionarCelda(fila  , col++ , r.getDias_pago()           , numero   );
                    xls.adicionarCelda(fila  , col++ , r.getValor_facturacion()   , dinero   );
                    xls.adicionarCelda(fila  , col++ , r.getValor_pago()          , dinero   );
                    
                    
                    
                    String key = "1#" + r.getCliente() + "#" + r.getCodcli() ;
                    Remesa r1 = (Remesa) resumen.get(key);
                    if ( r1 == null ){
                        resumen.put(key, r);
                    } else {
                        r1.setValor_itemf      ( r1.getValor_itemf()        + r.getValor_itemf()      );
                        r1.setDias_facturacion ( r1.getDias_facturacion()   + r.getDias_facturacion() );
                        r1.setDias_pago        ( r1.getDias_pago()          + r.getDias_pago()        );
                        // sumatoria valor_doc * #dias
                        r1.setValor_facturacion( r1.getValor_facturacion()  + r.getValor_facturacion() );
                        r1.setValor_pago       ( r1.getValor_pago()         + r.getValor_pago()        );
                    }
                    
                }
                
            } else {
                xls.adicionarCelda(fila  ,0, "NO HAY DATOS PARA MOSTRAR", titulo5);
                xls.combinarCeldas(fila , 0, fila, 0+5);
                fila++;
            }
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
    }
    
    private void seccionRFP(TreeMap resumen) throws Exception{
        try {
            
            model.reportePPPSvc.obtenerRFP(finicialRP, ffinalRP);
            Vector vector = model.reportePPPSvc.getRemesasFP();
            
            this.crearHoja("BASE RFP", "REMESAS FACTURADAS Y PAGADAS", finicialRP, ffinalRP);
            if (vector!=null && !vector.isEmpty()){
                
                // encabezado
                String [] cabecera = {
                    "Agencia", "Codigo Cliente", "Cliente", "Remesa", "Fecha Remesa", "Valor Remesa" ,"Factura" , "Fecha Factura", "Valor ItemF", "Fecha Pago", "Dias F", "Dias P",  "Valor x DiasF", "Valor x DiasP"
                };
                short  [] dimensiones = new short [] {
                    6000, 4000, 8000, 3500, 4000, 4000, 4000, 4000, 4000, 4000, 3000, 3000, 4000, 4000
                };
                for ( int i = 0; i<cabecera.length; i++){
                    xls.adicionarCelda(fila,  i, cabecera[i], titulo2);
                    if ( i < dimensiones.length )
                        xls.cambiarAnchoColumna(i, dimensiones[i] );
                }
                fila++;
                
                
                // detalle
                for ( int i = 0; i< vector.size(); i++, fila++){
                    int col = 0;
                    Remesa r = (Remesa) vector.get(i);
                    r.setValor_facturacion( r.getDias_facturacion() * r.getValor_itemf() );
                    r.setValor_pago       ( r.getDias_pago       () * r.getValor_itemf() );
                    r.setTipo_documento("RFP");
                    
                    
                    // datos de la planilla
                    xls.adicionarCelda(fila  , col++ , r.getNombre_ciu()    , letra          );
                    xls.adicionarCelda(fila  , col++ , r.getCodcli()        , letraCentrada  );
                    xls.adicionarCelda(fila  , col++ , r.getCliente()       , letra          );
                    xls.adicionarCelda(fila  , col++ , r.getNumrem()        , letraCentrada  );
                    xls.adicionarCelda(fila  , col++ , r.getFecrem()        , fechaCentrada  );
                    xls.adicionarCelda(fila  , col++ , r.getValorRemesa()   , dinero         );
                    xls.adicionarCelda(fila  , col++ , r.getFactura()       , letraCentrada  );
                    xls.adicionarCelda(fila  , col++ , r.getFecha_factura() , letraCentrada  );
                    xls.adicionarCelda(fila  , col++ , r.getValor_itemf()   , dinero         );
                    
                    xls.adicionarCelda(fila  , col++ , r.getFecha_corte()         , fechaCentrada  );
                    xls.adicionarCelda(fila  , col++ , r.getDias_facturacion()    , numero   );
                    xls.adicionarCelda(fila  , col++ , r.getDias_pago()           , numero   );
                    xls.adicionarCelda(fila  , col++ , r.getValor_facturacion()   , dinero   );
                    xls.adicionarCelda(fila  , col++ , r.getValor_pago()          , dinero   );
                    
                    
                    
                    String key = "2#" + r.getCliente() + "#" + r.getCodcli();
                    Remesa r1 = (Remesa) resumen.get(key);
                    if ( r1 == null ){
                        resumen.put(key, r);
                    } else {
                        r1.setValor_itemf      ( r1.getValor_itemf()      + r.getValor_itemf()      );
                        r1.setDias_facturacion( r1.getDias_facturacion() + r.getDias_facturacion() );
                        r1.setDias_pago        ( r1.getDias_pago()        + r.getDias_pago()        );
                        // sumatoria valor_doc * #dias
                        r1.setValor_facturacion( r1.getValor_facturacion()  + r.getValor_facturacion() );
                        r1.setValor_pago       ( r1.getValor_pago()         + r.getValor_pago()        );
                    }
                    
                }
                
            } else {
                xls.adicionarCelda(fila  ,0, "NO HAY DATOS PARA MOSTRAR", titulo5);
                xls.combinarCeldas(fila , 0, fila, 0+5);
                fila++;
            }
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
    }
    
    
    private void seccionResumen(TreeMap clientes) throws Exception{
        try {
            
            
            this.crearHoja("RESUMEN", "RESUMEN REMESAS", "", "");
            if (clientes!=null && !clientes.isEmpty()){
                
                // encabezado
                String [] cabecera = {
                    "Agencia", "Codigo Cliente", "Cliente", "Tipo", "Valor Total R.", "Dias Totales F", "S(VxD) F", "PPF", "Dias Totales P", "S(VxD) P", "PPP",
                };
                short  [] dimensiones = new short [] {
                    6000, 4000, 8000, 3500, 4000, 4000, 4000, 4000, 4000, 4000, 4000
                };
                for ( int i = 0; i<cabecera.length; i++){
                    xls.adicionarCelda(fila,  i, cabecera[i], titulo2);
                    if ( i < dimensiones.length )
                        xls.cambiarAnchoColumna(i, dimensiones[i] );
                }
                fila++;
                
                
                // detalle
                
                Iterator it = clientes.values().iterator();
                while( it.hasNext() ) {
                    int col = 0;
                    Remesa r = (Remesa) it.next();
                    
                    if (r.getTipo_documento().equalsIgnoreCase("RNF")){
                        xls.adicionarCelda(fila  , col++ , r.getNombre_ciu()          , letra          );
                        xls.adicionarCelda(fila  , col++ , r.getCodcli()              , letraCentrada  );
                        xls.adicionarCelda(fila  , col++ , r.getCliente()             , letra          );
                        if ( r.getDias_facturacion() >= xPPF )
                            xls.adicionarCelda(fila  , col++ , r.getTipo_documento()+xPPF  , letraCentrada  );
                        else
                            xls.adicionarCelda(fila  , col++ , r.getTipo_documento()       , letraCentrada  );
                        
                        xls.adicionarCelda(fila  , col++ , r.getValorRemesa()         , dinero         );
                        xls.adicionarCelda(fila  , col++ , r.getDias_facturacion()    , numero         );
                        xls.adicionarCelda(fila  , col++ , r.getValor_facturacion()   , dinero         );
                        xls.adicionarCelda(fila  , col++ , (r.getValorRemesa()==0?0:(r.getValor_facturacion() / r.getValorRemesa() ))  , dinero         );
                    }
                    else {
                        xls.adicionarCelda(fila  , col++ , r.getNombre_ciu()          , letra          );
                        xls.adicionarCelda(fila  , col++ , r.getCodcli()              , letraCentrada  );
                        xls.adicionarCelda(fila  , col++ , r.getCliente()             , letra          );
                        xls.adicionarCelda(fila  , col++ , r.getTipo_documento()      , letraCentrada  );
                        xls.adicionarCelda(fila  , col++ , r.getValor_itemf()         , dinero         );
                        xls.adicionarCelda(fila  , col++ , r.getDias_facturacion()    , numero         );
                        xls.adicionarCelda(fila  , col++ , r.getValor_facturacion()   , dinero         );
                        xls.adicionarCelda(fila  , col++ , (r.getValor_itemf()==0?0:(r.getValor_facturacion() / r.getValor_itemf() ))   , dinero         );
                        
                        xls.adicionarCelda(fila  , col++ , r.getDias_pago()    , numero         );
                        xls.adicionarCelda(fila  , col++ , r.getValor_pago()   , dinero         );
                        xls.adicionarCelda(fila  , col++ , (r.getValor_itemf()==0?0:(r.getValor_pago() / r.getValor_itemf() ))   , dinero         );
                        
                    }
                    fila ++;
                }
                
            } else {
                xls.adicionarCelda(fila  ,0, "NO HAY DATOS PARA MOSTRAR", titulo5);
                xls.combinarCeldas(fila , 0, fila, 0+5);
                fila++;
            }
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
        
        
    }
    
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception{
        Usuario u = new Usuario();
        u.setLogin("MFONTALVO");
        u.setNombre("ADMIN");
        Model model = new Model();
        
        HReportePPP h = new HReportePPP();
        h.start(model, "2007-01-01", "2007-03-12",  "2007-01-02", "2007-03-12",  "2007-01-03", "2007-03-12", u);
        
        
    }
    
}
