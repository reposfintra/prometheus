/*
 * reporteImportacionesMateriaPrima.java
 *
 * Created on 2 de agosto de 2006, 08:26 AM
 */
/******************************************************************************
 * Nombre clase :                   reporteImportacionesMateriaPrima.java     *
 * Descripcion :                    Genera reporte importaciones Materia Prima*
 * Autor :                          David Pi�a Lopez                          *
 * Fecha :                          2 de agosto de 2006, 08:26 AM             *
 * Version :                        1.0                                       *
 * Copyright :                      Fintravalores S.A.                   *
 *****************************************************************************/

package com.tsp.operation.model.threads;

import java.util.*;
import java.io.*;
import java.lang.*;
import java.sql.*;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.beans.POIWrite;
import com.tsp.exceptions.*;
import com.tsp.operation.controller.*;
import java.text.*;
import javax.servlet.http.*;
import com.tsp.util.*;
import com.tsp.operation.model.*;
import com.tsp.util.Util;
import java.sql.SQLException;

/**
 *
 * @author  David
 */
public class reporteImportacionesMateriaPrima extends Thread {
    private Model model;
    private String usuario;
    private String codigo;
    
    /** Creates a new instance of reporteImportacionesMateriaPrima */
    public reporteImportacionesMateriaPrima() {
        //model = new Model();
    }
    
    public void start( String codigo, String usuario, Model model ) {
        this.usuario = usuario;
        this.codigo = codigo;
        this.model = model;
        super.start();
    }
    
    public synchronized void run(){
        try{
            String[] split = this.codigo.split(",");
            String impo;
            boolean sw=false;
            String fecha_actual = Util.getFechaActual_String(6);
            String a�o = fecha_actual.substring(0,4);
            String mes = fecha_actual.substring(5,7);
            String dia = fecha_actual.substring(8,10);
            model.LogProcesosSvc.InsertProceso( "Reporte importaciones materia prima", this.hashCode(), "Reporte importaciones materia prima", usuario );
            POIWrite pw = new POIWrite();
            //obtener cabecera de ruta
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String path = rb.getString("ruta");
            //armas la ruta
            String Ruta  = path + "/exportar/migracion/" + usuario + "/";
            
            //se obtiene el objeto de la consulta inicial
            Vector impoexpo = model.impoExpoService.getVector();
            if ( impoexpo != null && impoexpo.size()>0 && split.length > 0 ){
                for( int i = 0; i < impoexpo.size(); i++ ){
                    Hashtable cab = (Hashtable)impoexpo.get( i );
                    sw=false;
                    for(int k=0; k<split.length; k++){
                        impo = split[k];
                        if( impo.equals( (String)cab.get( "documento" ) ) ){
                            sw=true;
                        }
                    }
                    if( sw ){
                        pw.nuevoLibro( Ruta + cab.get( "nombrecliente" ) + " " + cab.get( "documento" ) + ".xls" );
                        HSSFCellStyle estiloCabecera = pw.nuevoEstilo("Arial", 14, true, false, "", HSSFColor.ORANGE.index, HSSFColor.LIGHT_YELLOW.index, HSSFCellStyle.ALIGN_CENTER, HSSFCellStyle.BORDER_MEDIUM );
                        HSSFCellStyle estiloDetalle1 = pw.nuevoEstilo("Arial", 10, true, false, "", HSSFColor.ORANGE.index, HSSFColor.LIGHT_YELLOW.index, HSSFCellStyle.ALIGN_RIGHT, HSSFCellStyle.BORDER_MEDIUM );
                        HSSFCellStyle estiloDetalle2 = pw.nuevoEstilo("Arial", 10, false, false, "", HSSFColor.BLACK.index, HSSFColor.WHITE.index, HSSFCellStyle.ALIGN_RIGHT, HSSFCellStyle.BORDER_MEDIUM );
                        
                        boolean faltantes = false;
                        model.impoExpoService.obtenerImpoExpoRemesas( (String)cab.get( "dstrct" ), (String)cab.get( "tipo_doc" ), (String)cab.get( "documento" ) );
                        Vector detalles = model.impoExpoService.getVector();
                        for( int j = 0; j < detalles.size(); j++ ){
                            Hashtable det = (Hashtable)detalles.get( j );
                            pw.obtenerHoja( det.get( "documento_rel" ) + "-" + det.get( "planilla" ) );
                            
                            pw.cambiarAnchoColumna( 1, 10000 );
                            pw.cambiarAnchoColumna( 2, 10000 );
                            
                            pw.combinarCeldas( 1, 1, 1, 2 );
                            pw.adicionarCelda( 1, 1, "IMPORTACI�N DE MATERIA PRIMA", estiloCabecera );
                            pw.adicionarCelda( 1, 2, "", estiloCabecera );
                            
                            pw.adicionarCelda( 3, 1, "IMPORTACI�N:", estiloDetalle1 );
                            pw.adicionarCelda( 4, 1, "ORIGEN DE IMPORTACI�N:", estiloDetalle1 );
                            pw.adicionarCelda( 5, 1, "ETA:", estiloDetalle1 );
                            pw.adicionarCelda( 6, 1, "ENTREGA DOC AGECOLDEX:", estiloDetalle1 );
                            pw.adicionarCelda( 9, 1, "CONTENEDOR", estiloDetalle1 );
                            pw.adicionarCelda( 10, 1, "DESCRIPCI�N DE LA CARGA", estiloDetalle1 );
                            pw.adicionarCelda( 11, 1, "PESO (Kilos)", estiloDetalle1 );
                            pw.adicionarCelda( 12, 1, "NOMBRE DE CONDUCTOR", estiloDetalle1 );
                            pw.adicionarCelda( 13, 1, "PLACA DE VEH�CULO", estiloDetalle1 );
                            pw.adicionarCelda( 14, 1, "HORA SALIDA", estiloDetalle1 );
                            pw.adicionarCelda( 15, 1, "FECHA DE CARGUE EN PUERTO", estiloDetalle1 );
                            pw.adicionarCelda( 16, 1, "UTILIZACI�N DE ESCOLTA", estiloDetalle1 );
                            pw.adicionarCelda( 17, 1, "FECHA DE DESCARGUE", estiloDetalle1 );
                            pw.adicionarCelda( 18, 1, "PATIO DE CONTENEDORES", estiloDetalle1 );
                            pw.adicionarCelda( 20, 1, "OT. TRANSPORTE", estiloDetalle1 );
                            pw.adicionarCelda( 21, 1, "No. FACTURA", estiloDetalle1 );
                            pw.adicionarCelda( 22, 1, "FECHA EMISION FACTURA TSP", estiloDetalle1 );
                            pw.adicionarCelda( 23, 1, "VALOR POR TRANSPORTE", estiloDetalle1 );
                            pw.adicionarCelda( 24, 1, "VALOR TOTAL FACTURA", estiloDetalle1 );
                            
                            String documento = ""+cab.get( "documento" );
                            String origenviaje = ""+det.get( "origenviaje" );
                            if( origenviaje.equals("") )faltantes = true;
                            String fecha_eta = ""+cab.get( "fecha_eta" );
                            if( fecha_eta.equals("0099-01-01 00:00:00") ){ fecha_eta = "";faltantes = true; }
                            String fecha_sia = ""+cab.get( "fecha_sia" );
                            if( fecha_sia.equals("0099-01-01 00:00:00") ){ fecha_sia = "";faltantes = true; }
                            String contenedores = ""+det.get( "contenedores" );
                            if( contenedores.equals("") )faltantes = true;
                            String descripcion = ""+cab.get( "descripcion" );
                            if( descripcion.equals("") )faltantes = true;
                            String pesoreal = ""+det.get( "pesoreal" );
                            String conductor = ""+det.get( "conductor" );
                            if( conductor.equals("") )faltantes = true;
                            String placa = ""+det.get( "placa" );
                            if( placa.equals("") )faltantes = true;
                            String horasalida = ""+det.get( "horasalida" );
                            if( horasalida.equals("0099-01-01 00:00:00") ){horasalida="";faltantes = true;}
                            String fechacargue = ""+det.get( "fechacargue" );
                            if( fechacargue.equals("0099-01-01 00:00:00") ){fechacargue="";faltantes = true;}
                            String escolta = ""+det.get( "escolta" );
                            String fechadescargue = ""+det.get( "fechadescargue" );
                            if( fechadescargue.equals("0099-01-01 00:00:00") ){fechadescargue="";faltantes = true;}
                            String patiocontenedores = ""+det.get( "patiocontenedores" );
                            if( patiocontenedores.equals("") )faltantes = true;
                            String documento_rel = ""+det.get( "documento_rel" );
                            
                            String fechafactura = ""+det.get( "fechafactura" );
                            if( fechafactura.equals("0099-01-01") ){ fechafactura="";faltantes = true; }
                            
                            String tipodoctransporte = ""+det.get( "tipodoctransporte" );
                            String doctransporte = ""+det.get( "doctransporte" );
                            
                            String valortransporte = ""+det.get( "valoritem" );
                            String totalFactura    = ""+det.get( "valortransporte" );
                            
                            pw.adicionarCelda( 3, 2, documento, estiloDetalle2 );
                            pw.adicionarCelda( 4, 2, origenviaje, estiloDetalle2 );
                            pw.adicionarCelda( 5, 2, fecha_eta, estiloDetalle2 );
                            pw.adicionarCelda( 6, 2, fecha_sia, estiloDetalle2 );
                            pw.adicionarCelda( 9, 2, contenedores, estiloDetalle2 );
                            pw.adicionarCelda( 10, 2, descripcion, estiloDetalle2 );
                            pw.adicionarCelda( 11, 2, pesoreal, estiloDetalle2 );
                            pw.adicionarCelda( 12, 2, conductor, estiloDetalle2 );
                            pw.adicionarCelda( 13, 2, placa, estiloDetalle2 );
                            pw.adicionarCelda( 14, 2, horasalida, estiloDetalle2 );
                            pw.adicionarCelda( 15, 2, fechacargue, estiloDetalle2 );
                            pw.adicionarCelda( 16, 2, escolta, estiloDetalle2 );
                            pw.adicionarCelda( 17, 2, fechadescargue, estiloDetalle2 );
                            pw.adicionarCelda( 18, 2, patiocontenedores, estiloDetalle2 );
                            pw.adicionarCelda( 20, 2, documento_rel, estiloDetalle2 );
                            pw.adicionarCelda( 21, 2, doctransporte, estiloDetalle2 );
                            pw.adicionarCelda( 22, 2, fechafactura, estiloDetalle2 );
                            pw.adicionarCelda( 23, 2, "$ " + UtilFinanzas.customFormat( valortransporte ), estiloDetalle2 );
                            pw.adicionarCelda( 24, 2, "$ " + UtilFinanzas.customFormat( totalFactura ), estiloDetalle2 );
                        }
                        if( !faltantes ){
                            model.impoExpoService.actualizarGeneracion( (String)cab.get( "dstrct" ), (String)cab.get( "tipo_doc" ), (String)cab.get( "documento" ), usuario );
                        }
                        pw.cerrarLibro();
                    }
                }
                
            }
            else
                model.LogProcesosSvc.finallyProceso( "Reporte importaciones materia prima", this.hashCode(), usuario, "No se encontraron datos en el vector" );
            
            model.LogProcesosSvc.finallyProceso( "Reporte importaciones materia prima", this.hashCode(), usuario, "PROCESO EXITOSO" );
        }catch( Exception ex ){
            ex.printStackTrace();
            //Capturo errores finalizando proceso
            try{
                model.LogProcesosSvc.finallyProceso( "Reporte importaciones materia prima", this.hashCode(), usuario, "ERROR : " + ex.getMessage() );
            } catch( Exception f ){
                f.printStackTrace();
                try{
                    model.LogProcesosSvc.finallyProceso( "Reporte importaciones materia prima",this.hashCode(), usuario, "ERROR : " + f.getMessage() );
                } catch( Exception p ){ p.printStackTrace(); }
            }
        }
    }
}

