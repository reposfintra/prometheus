/********************************************************************
 *      Nombre Clase.................   ReporteExtrafletesTh.java
 *      Descripci�n..................   Hilo para la escritura del xls
 *      Autor........................   Tito Andr�s Maturana
 *      Fecha........................   03.02.2006
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.model.threads;

import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import java.io.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.operation.model.*;
import java.util.*;
import java.text.*;
//import com.tsp.operation.model.threads.POIWrite;



public class ReporteExtrafletesTh extends Thread{
    
    private Vector reporte;
    private String user;    
    private String fechai;
    private String fechaf;
    private boolean ver_costos;
    
    private Model model;
    
    //Log Procesos
    private String procesoName;
    private String des;
    
    public void start(Model modelo, Vector reporte, String user, String fechai, String fechaf, boolean ver_costos){
        this.reporte = reporte;
        this.user = user;
        this.fechaf = fechaf;
        this.fechai = fechai;
        this.ver_costos = ver_costos;
        
        this.model   = modelo;
        this.procesoName = "Reporte de Extrafletes";
        this.des = "Reporte de Extrafletes:  " + fechai + " - " + fechaf;
        
        super.start();
    }
    
    public synchronized void run(){
        try{
            //INSERTO EN EL LOG DE PROCESO
            model.LogProcesosSvc.InsertProceso(this.procesoName, this.hashCode(), this.des, user);
            
            
            //Fecha del sistema
            Calendar FechaHoy = Calendar.getInstance();
            Date d = FechaHoy.getTime();
            SimpleDateFormat s = new SimpleDateFormat("yyyyMMdd_kkmm");
            SimpleDateFormat s1 = new SimpleDateFormat("yyyy-MM-dd-hh:mm");
            SimpleDateFormat fec = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss");
            SimpleDateFormat time = new SimpleDateFormat("hh:mm");
            String FechaFormated = s.format(d);
            String FechaFormated1 = s1.format(d);
            String hora = time.format(d);
            String Fecha = fec.format(d);
            
            
            //obtener cabecera de ruta
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String path = rb.getString("ruta");
            //armas la ruta
            String Ruta1  = path + "/exportar/migracion/" + user + "/";
            //crear la ruta
            File file = new File(Ruta1);
            file.mkdirs();
            
            POIWrite xls = new POIWrite(Ruta1 +"ReporteExtrafletes_" + FechaFormated + ".xls", user, Fecha);
            String nom_archivo = "ReporteExtrafletes_" + FechaFormated + ".xls";            
            
            //Definici�n de Estilos
            HSSFCellStyle fecha  = xls.nuevoEstilo("Arial", 9, false , false, "yyyy/mm/dd hh:mm:ss"  , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
            HSSFCellStyle texto  = xls.nuevoEstilo("Arial", 9, false , false, "text"        , xls.NONE , xls.NONE , xls.NONE);
            HSSFCellStyle numero = xls.nuevoEstilo("Arial", 9, false , false, ""            , xls.NONE , xls.NONE , xls.NONE);
            
            HSSFCellStyle header1      = xls.nuevoEstilo("Arial", 12, true , false, "text"        , HSSFColor.WHITE.index , HSSFColor.GREEN.index , HSSFCellStyle.ALIGN_LEFT);
            HSSFCellStyle header2      = xls.nuevoEstilo("Arial", 18, true , false, "text"        , HSSFColor.WHITE.index , HSSFColor.ORANGE.index , HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle header3      = xls.nuevoEstilo("Arial", 12, true , false, "text"        , HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
            HSSFCellStyle texto2      = xls.nuevoEstilo("Arial", 12, false , false, "text"        , HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
            HSSFCellStyle titulo      = xls.nuevoEstilo("Arial", 12, true  , false, "text"        , HSSFColor.BLACK.index , HSSFColor.PINK.index , HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle fechatitle  = xls.nuevoEstilo("Arial", 12, true  , false, "yyyy/mm/dd"  , HSSFColor.RED.index , xls.NONE, HSSFCellStyle.ALIGN_CENTER );
            HSSFCellStyle entero      = xls.nuevoEstilo("Arial", 12, false , false, "text"        , HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle flotante      = xls.nuevoEstilo("Arial", 12, false , false, "text"        , HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_RIGHT);
            HSSFCellStyle moneda  = xls.nuevoEstilo("Arial", 9, false , false, "#,##0.00", xls.NONE , xls.NONE , xls.NONE);
            //\"$ \"
                                                
            xls.obtenerHoja("Reporte de Extrafletes");
            xls.cambiarMagnificacion(3,4);
            
            // cabecera
            int nceldas = this.ver_costos? 13 : 12;
            
            xls.combinarCeldas(0, 0, 0, nceldas);
            xls.combinarCeldas(3, 0, 3, nceldas);
            xls.combinarCeldas(4, 0, 4, nceldas);
            xls.adicionarCelda(0, 0, "REPORTE DE EXTRAFLETES Y COSTOS REEMBOLSABLES", header2);
            xls.adicionarCelda(3, 0, "Fecha desde: " + fechai + " Hasta: " + fechaf, header3);
            xls.adicionarCelda(4, 0, "Fecha del proceso: " + Fecha, header3);
            
                            
            // subtitulos
            
            
            int fila = 6;
            int col  = 0;
            
            xls.adicionarCelda(fila, col++, "TIPO DE DOCUMENTOS", header1);
            xls.adicionarCelda(fila, col++, "DOCUMENTOS", header1);
            xls.adicionarCelda(fila, col++, "PLANILLA", header1);
            xls.adicionarCelda(fila, col++, "REMESA", header1);
            xls.adicionarCelda(fila, col++, "FECHA DESPACHO", header1);
            xls.adicionarCelda(fila, col++, "CLIENTE", header1);
            xls.adicionarCelda(fila, col++, "RUTA", header1);
            
            if( this.ver_costos )
                xls.adicionarCelda(fila, col++, "VALOR COSTO", header1);
            
            xls.adicionarCelda(fila, col++, "VALOR INGRESO", header1);
            xls.adicionarCelda(fila, col++, "TIPO", header1);
            xls.adicionarCelda(fila, col++, "CODIGO", header1);
            xls.adicionarCelda(fila, col++, "DESCRIPCION", header1);
            xls.adicionarCelda(fila, col++, "AUTORIZADOR", header1);
            xls.adicionarCelda(fila, col++, "OBSERVACION", header1);
            
            fila++;
            
            for( int i=0; i<reporte.size(); i++){
                col = 0;
                
                Hashtable ht = (Hashtable) reporte.elementAt(i);
                
                xls.adicionarCelda(fila, col++, ht.get("tdocumentos").toString(), texto);
                xls.adicionarCelda(fila, col++, ht.get("documentos").toString(), texto);
                xls.adicionarCelda(fila, col++, ht.get("numpla").toString(), texto);
                xls.adicionarCelda(fila, col++, ht.get("numrem").toString(), texto);
                java.util.Date date = com.tsp.util.Util.ConvertiraDate0(ht.get("fecdsp").toString());
                xls.adicionarCelda(fila, col++, date, fecha); 
                xls.adicionarCelda(fila, col++, ht.get("cliente").toString(), texto);                
                xls.adicionarCelda(fila, col++, ht.get("ruta").toString(), texto);
                
                if( this.ver_costos )
                    xls.adicionarCelda(fila, col++, com.tsp.util.Util.redondear(Double.valueOf(ht.get("vlr").toString()).doubleValue(), 2) , moneda);
                
                xls.adicionarCelda(fila, col++, com.tsp.util.Util.redondear(Double.valueOf(ht.get("vlr_ingreso").toString()).doubleValue(), 2) , moneda);
                xls.adicionarCelda(fila, col++, ht.get("tipo").toString(), texto);
                xls.adicionarCelda(fila, col++, ht.get("codigo").toString(), texto);
                xls.adicionarCelda(fila, col++, ht.get("desc").toString(), texto);
                xls.adicionarCelda(fila, col++, ht.get("autorizador").toString(), texto);
                xls.adicionarCelda(fila, col++, ht.get("observacion").toString(), texto);
                
                fila++;
            }         
            
            xls.cerrarLibro();
            model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(), user, "PROCESO EXITOSO");
            
        }catch (Exception ex){
            ex.printStackTrace();
            ////System.out.println("ERROR AL GENERAR EL ARCHIVO XLS : " + ex.getMessage());
            try{
                model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(), user, "ERROR :" + ex.getMessage());
            }
            catch(Exception f){
                try{
                    model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(), user, "ERROR :");
                }catch(Exception p){    }
            }
        }
        
    }    
   
    
}