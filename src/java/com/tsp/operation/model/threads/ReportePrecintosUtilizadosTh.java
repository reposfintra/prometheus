/********************************************************************
 *      Nombre Clase.................   ReportePrecintosNoUtilizados.java
 *      Descripci�n..................   Hilo para la escritura del xls
 *      Autor........................   Tito Andr�s Maturana
 *      Fecha........................   08.11.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.model.threads;


import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import java.io.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.operation.model.*;
import java.util.*;
import java.text.*;
//import com.tsp.operation.model.threads.POIWrite;



public class ReportePrecintosUtilizadosTh extends Thread{
    
    private Vector reporte;
    private String user;    
    private String fechai;
    private String fechaf;
    private String agencia;
    
    private Model model;
    
    //Log Procesos
    private String procesoName;
    private String des;
    
    public void start(Model modelo, Vector reporte, String user, String fechai, String fechaf, String agencia){
        this.reporte = reporte;
        this.user = user;
        this.fechaf = fechaf;
        this.fechai = fechai;
        this.agencia = agencia;
        
        this.model   = modelo;
        this.procesoName = "Reporte Precintos Utilizados";
        this.des = "Reporte Precintos Utilizados:  " + fechai + " - " + fechaf;
        
        super.start();
    }
    
    public synchronized void run(){
        try{
            //INSERTO EN EL LOG DE PROCESO
            model.LogProcesosSvc.InsertProceso(this.procesoName, this.hashCode(), this.des, user);
            
            //Fecha del sistema
            Calendar FechaHoy = Calendar.getInstance();
            Date d = FechaHoy.getTime();
            SimpleDateFormat s = new SimpleDateFormat("yyyyMMdd_kkmm");
            SimpleDateFormat s1 = new SimpleDateFormat("yyyy-MM-dd-hh:mm");
            SimpleDateFormat fec = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat time = new SimpleDateFormat("hh:mm");
            String FechaFormated = s.format(d);
            String FechaFormated1 = s1.format(d);
            String hora = time.format(d);
            String Fecha = fec.format(d);
            
            
            //obtener cabecera de ruta
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String path = rb.getString("ruta");
            //armas la ruta
            String Ruta1  = path + "/exportar/migracion/" + user + "/";
            //crear la ruta
            File file = new File(Ruta1);
            file.mkdirs();
            
            POIWrite xls = new POIWrite(Ruta1 +"ReportePrecintosUtilizados_" + FechaFormated + ".xls", user, Fecha);
            String nom_archivo = "ReportePrecintosUtilizados_" + FechaFormated + ".xls";            
            
            //Definici�n de Estilos
            HSSFCellStyle fecha  = xls.nuevoEstilo("Arial, Courier New, Mono", 9, false , false, "yyyy/mm/dd"  , xls.NONE , xls.NONE , xls.NONE );
            HSSFCellStyle texto  = xls.nuevoEstilo("Arial, Courier New, Mono", 9, false , false, "text"        , xls.NONE , xls.NONE , xls.NONE);
            HSSFCellStyle numero = xls.nuevoEstilo("Arial, Courier New, Mono", 9, false , false, ""            , xls.NONE , xls.NONE , xls.NONE);
            
            HSSFCellStyle header1      = xls.nuevoEstilo("Arial, Courier New, Mono", 10, true , false, "text"        , HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
            HSSFCellStyle header2      = xls.nuevoEstilo("Arial, Courier New, Mono", 11, true , false, "text"        , HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
            HSSFCellStyle texto2      = xls.nuevoEstilo("Arial, Courier New, Mono", 10, false , false, "text"        , HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
            HSSFCellStyle titulo      = xls.nuevoEstilo("Arial, Courier New, Mono", 9, true  , false, "text"        , HSSFColor.BLACK.index , HSSFColor.PINK.index , HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle fechatitle  = xls.nuevoEstilo("Arial, Courier New, Mono", 9, true  , false, "yyyy/mm/dd"  , HSSFColor.RED.index , xls.NONE, HSSFCellStyle.ALIGN_CENTER );
            HSSFCellStyle cambio      = xls.nuevoEstilo("Arial, Courier New, Mono", 10, true , false, "text"        ,  HSSFColor.BLACK.index, HSSFColor.CORNFLOWER_BLUE.index, HSSFCellStyle.ALIGN_LEFT);
            
            header2.setAlignment(header2.ALIGN_CENTER);
                                    
            xls.obtenerHoja("PRECINTOS UTILIZADOS");
            xls.cambiarMagnificacion(3,4);
            
            // cabecera
            
            xls.combinarCeldas(2, 0, 2, 7);
            xls.combinarCeldas(4, 0, 4, 7);
            xls.combinarCeldas(6, 0, 6, 7);
            xls.adicionarCelda(2, 0, "TRANSPORTES SANCHEZ POLO S.A.", header2);
            xls.adicionarCelda(4, 0, "Reporte de Precintos Utilizados", header2);
            xls.adicionarCelda(6, 0, "Per�odo: " + fechai + " Hasta: " + fechaf, header2);
            
            if( this.agencia.length()!=0){
                xls.combinarCeldas(8, 0, 8, 7);
                xls.adicionarCelda(8, 0, "Agencia: " + agencia, header2);
            }                 
            // subtitulos
            
            
            int fila = 10;
            int col  = 0;
            
            if( this.agencia.length()==0) 
                xls.adicionarCelda(fila, col++, "AGENCIA", header1);
            xls.adicionarCelda(fila, col++, "PRECINTO", header1);
            xls.adicionarCelda(fila, col++, "FECHA UTILIZACION", header1);
            xls.adicionarCelda(fila, col++, "USUARIO", header1);
            xls.adicionarCelda(fila, col++, "PLANILLA", header1);
            xls.adicionarCelda(fila, col++, "REMESA", header1);
            xls.adicionarCelda(fila, col++, "RUTA", header1);
            xls.adicionarCelda(fila, col++, "FECHA", header1);
            xls.adicionarCelda(fila, col++, "CLIENTE", header1);
            
            
            fila++;
            
            for( int i=0; i<reporte.size(); i++){
                col = 0;
                
                Vector obj = (Vector) reporte.elementAt(i);
                Precinto prec = (Precinto) obj.elementAt(0);
                InfoPlanilla info = (InfoPlanilla) obj.elementAt(1);
                                
                if( this.agencia.length()==0 )
                    xls.adicionarCelda(fila, col++, prec.getAgencia(), texto);
                xls.adicionarCelda(fila, col++, prec.getPrecinto(), texto);
                xls.adicionarCelda(fila, col++, prec.getFec_utilizacion(), texto);
                xls.adicionarCelda(fila, col++, prec.getUser_update(), texto);
                xls.adicionarCelda(fila, col++, prec.getNumpla(), texto);
                xls.adicionarCelda(fila, col++, info.getRemesa(), texto);
                xls.adicionarCelda(fila, col++, info.getOrigen() + " - " + info.getDestino(), texto);
                xls.adicionarCelda(fila, col++, info.getFecha(), texto);
                xls.adicionarCelda(fila, col++, info.getCliente(), texto);
                
                fila++;
            }         
            
            
            
            xls.cerrarLibro();
            model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(), user, "PROCESO EXITOSO");
        }catch (Exception ex){
            ex.printStackTrace();
            ////System.out.println("ERROR AL GENERAR EL ARCHIVO XLS : " + ex.getMessage());
            try{
                model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(), user, "ERROR :" + ex.getMessage());
            }
            catch(Exception f){
                try{
                    model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(), user, "ERROR :");
                }catch(Exception p){    }
            }
        }
        
    }    
   
    
}