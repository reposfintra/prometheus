/********************************************************************
 *      Nombre Clase.................   ReporteAnticiposTh.java
 *      Descripci�n..................   Hilo para la escritura del xls
 *      Autor........................   Tito Andr�s Maturana
 *      Fecha........................   03.11.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.model.threads;


import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import java.io.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.controller.ReporteCumplidoAction;
import com.tsp.operation.model.services.*;
import com.tsp.operation.model.*;
import java.util.*;
import java.text.*;
import com.tsp.util.*;
//import com.tsp.operation.model.threads.POIWrite;



public class ReporteAnticiposTh extends Thread{
    
    private Vector informe;
    private Vector resumen;
    private String user;
    private String mensaje;
    private String fechai;
    private String fechaf;
    
    private Model model;
    
    //Log Procesos
    private String procesoName;
    private String des;
    
    public void start(Model modelo, Vector informe, Vector resumen, String user, String fechai, String fechaf){
        this.informe = informe;
        this.user = user;
        this.fechaf = fechaf;
        this.fechai = fechai;
        this.resumen = resumen;
        
        this.model   = modelo;
        this.procesoName = "Reporte Anticipos";
        this.des = "Reporte Anticipos:  " + fechai + " - " + fechaf;
        
        super.start();
    }
    
    public synchronized void run(){
        try{
            //INSERTO EN EL LOG DE PROCESO
            model.LogProcesosSvc.InsertProceso(this.procesoName, this.hashCode(), this.des, user);
            
            //Fecha del sistema
            Calendar FechaHoy = Calendar.getInstance();
            Date d = FechaHoy.getTime();
            SimpleDateFormat s = new SimpleDateFormat("yyyyMMdd");
            SimpleDateFormat s1 = new SimpleDateFormat("yyyy-MM-dd-hh:mm");
            SimpleDateFormat fec = new SimpleDateFormat("yyyy-MM-dd");
            String FechaFormated = s.format(d);
            String FechaFormated1 = s1.format(d);
            String Fecha = fec.format(d);
            
            //Hora del sistema
            String horaActual = Util.getFechaActual_String(6);
            String hora = horaActual.substring(10,19);
            
            
            //obtener cabecera de ruta
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String path = rb.getString("ruta");
            //armas la ruta
            String Ruta1  = path + "/exportar/migracion/" + user + "/";
            //crear la ruta
            File file = new File(Ruta1);
            file.mkdirs();
            
            POIWrite xls = new POIWrite(Ruta1 +"ReporteAnticipos_" + FechaFormated + ".xls", user, Fecha);
            String nom_archivo = "ReporteAnticipos_" + FechaFormated + ".xls";
            this.mensaje = nom_archivo;
            
            //Definici�n de Estilos
            HSSFCellStyle fecha  = xls.nuevoEstilo("Book Antiqua", 9, false , false, "yyyy-mm-dd hh:mm:ss", xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_LEFT );
            HSSFCellStyle fecha2  = xls.nuevoEstilo("Book Antiqua", 9, false , false, "AAAA-MM-DD", xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_LEFT );
            HSSFCellStyle texto  = xls.nuevoEstilo("Book Antiqua", 9, false , false, "text", xls.NONE , xls.NONE , xls.NONE);
            HSSFCellStyle numero = xls.nuevoEstilo("Book Antiqua", 9, false , false, "#,##0.00", xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_RIGHT);
            HSSFCellStyle numero2 = xls.nuevoEstilo("Book Antiqua", 9, false , false, "#,##0.00", xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_RIGHT);
            
            HSSFCellStyle header1      = xls.nuevoEstilo("Arial", 10, true , false, "text"        , HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
            HSSFCellStyle header2      = xls.nuevoEstilo("Arial", 12, true , false, "text"        , HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
            HSSFCellStyle texto2      = xls.nuevoEstilo("Arial", 10, false , false, "text"        , HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
            HSSFCellStyle titulo      = xls.nuevoEstilo("Book Antiqua", 9, true  , false, "text"        , HSSFColor.BLACK.index , HSSFColor.PINK.index , HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle fechatitle  = xls.nuevoEstilo("Book Antiqua", 9, true  , false, "yyyy/mm/dd"  , HSSFColor.RED.index , xls.NONE, HSSFCellStyle.ALIGN_CENTER );
            HSSFCellStyle acpm_hd = xls.nuevoEstilo("Arial", 10, true , false, "text", HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
            HSSFCellStyle texto3  = xls.nuevoEstilo("Book Antiqua", 9, false , false, "text", xls.NONE , xls.NONE , xls.NONE);
            HSSFCellStyle celda = xls.nuevoEstilo("Book Antiqua", 9, false , false, "text", xls.NONE , xls.NONE , xls.NONE);
            
            header1.setBorderTop(HSSFCellStyle.BORDER_MEDIUM);
            header1.setBorderBottom(HSSFCellStyle.BORDER_THIN);
            header1.setAlignment(HSSFCellStyle.ALIGN_CENTER);
            header2.setAlignment(HSSFCellStyle.ALIGN_CENTER);
            
            acpm_hd.setAlignment(HSSFCellStyle.ALIGN_CENTER);
            acpm_hd.setBorderTop(HSSFCellStyle.BORDER_MEDIUM);
            acpm_hd.setBorderBottom(HSSFCellStyle.BORDER_MEDIUM);
            acpm_hd.setBorderLeft(HSSFCellStyle.BORDER_MEDIUM);
            acpm_hd.setBorderRight(HSSFCellStyle.BORDER_MEDIUM);
            
            texto3.setBorderTop(HSSFCellStyle.BORDER_MEDIUM);
            texto3.setBorderBottom(HSSFCellStyle.BORDER_MEDIUM);
            texto3.setBorderLeft(HSSFCellStyle.BORDER_MEDIUM);
            texto3.setBorderRight(HSSFCellStyle.BORDER_MEDIUM);
            texto3.setAlignment(HSSFCellStyle.ALIGN_RIGHT);
            celda.setBorderLeft(HSSFCellStyle.BORDER_MEDIUM);
            
            numero2.setBorderTop(HSSFCellStyle.BORDER_MEDIUM);
            numero2.setBorderBottom(HSSFCellStyle.BORDER_MEDIUM);
            numero2.setBorderLeft(HSSFCellStyle.BORDER_MEDIUM);
            numero2.setBorderRight(HSSFCellStyle.BORDER_MEDIUM);
            
            
            xls.obtenerHoja("Informe Anticipos");
            xls.cambiarMagnificacion(3,4);
            
            // cabecera
            
            xls.combinarCeldas(2, 0, 2, 10);
            xls.combinarCeldas(4, 0, 4, 10);
            xls.combinarCeldas(6, 0, 6, 10);
            xls.adicionarCelda(2, 0, "TRANSPORTES SANCHEZ POLO S.A.", header2);
            xls.adicionarCelda(4, 0, "Reporte de Anticipos Entregados", header2);
            xls.adicionarCelda(5, 0, "Generado:" + Fecha + " Hora: " + hora , texto2);
            xls.adicionarCelda(6, 0, "Fecha del Reporte: " + fechai + " Hasta: " + fechaf, texto2);
            
            //resumen
            
            xls.adicionarCelda(0, 13, "RESUMEN", acpm_hd);
            xls.combinarCeldas(0, 13, 0, 15);
            xls.adicionarCelda(0, 16, "", celda);
            
            
            
            // subtitulos
            
            
            int fila = 8;
            int col  = 0;
            
            xls.adicionarCelda(fila, col++, "REMISION", header1);
            xls.adicionarCelda(fila, col++, "PLACA", header1);
            xls.adicionarCelda(fila, col++, "FECHA", header1);
            xls.adicionarCelda(fila, col++, "FECHA DESPACHO", header1);
            
            // ricardo
            xls.adicionarCelda(fila, col++, "PUERTO", header1);
            
            xls.adicionarCelda(fila, col++, "NIT-PROPIETARIO", header1);
            xls.adicionarCelda(fila, col++, "PROPIETARIO", header1);
            xls.adicionarCelda(fila, col++, "PROVEEDOR-ACPM", header1);
            xls.adicionarCelda(fila, col++, "GALONES", header1);
            xls.adicionarCelda(fila, col++, "VALOR-ACPM", header1);
            
            // ricardo
            xls.adicionarCelda(fila, col++, "PROVEEDOR-ANTICIPO", header1);
            
            xls.adicionarCelda(fila, col++, "PEAJES", header1);
            xls.adicionarCelda(fila, col++, "EFECTIVO", header1);
            xls.adicionarCelda(fila, col++, "TOTAL", header1);
            
            fila = 9;
            for(int i=0; i<informe.size(); i++){
                ReporteAnticipos anticip = (ReporteAnticipos) informe.elementAt(i);
                col = 0;
                if( anticip.getTotal().doubleValue()!=0 ){
                    xls.adicionarCelda(fila, col++, anticip.getRemision(), texto);
                    xls.adicionarCelda(fila, col++, anticip.getPlaca(), texto);
                    
                    //xls.adicionarCelda(fila, col++, anticip.getFecha(), fecha);                    
                    Date date = Util.ConvertiraDate0(anticip.getFecha());
                    
                    xls.adicionarCelda(fila, col++, fec.format(date), fecha2);
                    xls.adicionarCelda(fila, col++, date, fecha);
                    // ricardo
                    xls.adicionarCelda(fila, col++, anticip.getPuerto(), texto);
                    
                    xls.adicionarCelda(fila, col++, anticip.getNitprop(), texto);
                    xls.adicionarCelda(fila, col++, anticip.getNomprop(), texto);
                    xls.adicionarCelda(fila, col++, anticip.getProveedor(), texto);
                    xls.adicionarCelda(fila, col++, Double.valueOf(anticip.getGalon()).doubleValue(), numero);
                    xls.adicionarCelda(fila, col++, anticip.getVr_galon().doubleValue(), numero);
                    //ricardo
                    xls.adicionarCelda(fila, col++, anticip.getProvAnticipo(), texto);
                    
                    xls.adicionarCelda(fila, col++, anticip.getPeajes().doubleValue(), numero);
                    xls.adicionarCelda(fila, col++, anticip.getEfectivo().doubleValue(), numero);
                    xls.adicionarCelda(fila, col++, anticip.getTotal().doubleValue(), numero);
                    fila++;
                }
            }
            
            fila = 1;
            for(int i=0; i<resumen.size(); i++){
                Vector ln = (Vector) resumen.get(i);
                String prov = (String) ln.get(0);
                String gal = (String) ln.get(1);
                String val = (String) ln.get(2);
                col = 13;
                xls.adicionarCelda(fila, col++, prov, acpm_hd);
                xls.adicionarCelda(fila, col++, Double.valueOf(gal).doubleValue(), numero2);
                xls.adicionarCelda(fila, col++, Double.valueOf(val).doubleValue(), numero2);
                fila++;
            }
            
            // datos
            
            
            xls.cerrarLibro();
            model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(), user, "PROCESO EXITOSO");
        }catch (Exception ex){
            ex.printStackTrace();
            ////System.out.println("ERROR AL GENERAR EL ARCHIVO XLS : " + ex.getMessage());
            try{
                model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(), user, "ERROR :" + ex.getMessage());
            }
            catch(Exception f){
                try{
                    model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(), user, "ERROR :");
                }catch(Exception p){    }
            }
        }
        
    }
    
    /**
     * Getter for property mensaje.
     * @return Value of property mensaje.
     */
    public java.lang.String getMensaje() {
        return mensaje;
    }
    
    /**
     * Setter for property mensaje.
     * @param mensaje New value of property mensaje.
     */
    public void setMensaje(java.lang.String mensaje) {
        this.mensaje = mensaje;
    }
    
}