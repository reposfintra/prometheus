/*
 * Nombre        CuadroAzul.java
 * Autor         Osvaldo P�rez Ferrer
 * Fecha         4 de noviembre de 2006, 01:40 PM
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */

package com.tsp.operation.model.threads;


import java.util.*;
import java.io.*;
import java.lang.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.*;
import com.tsp.operation.model.*;

public class HCuadroAzul extends Thread{
    
    private String usuario;
    private String procesoName;
    private String anio;
    private String nummes;
    private String nombremes;
    private String dstrct;
    private String opc;
    private Usuario u;
    
    private Model model;
    /**
     * Crea una nueva instancia de  CuadroAzul
     */
    public HCuadroAzul() {
    }
    
    public void start( Usuario u , String nummes, String nombremes, String anio, String opc, Model model) {
        
        this.procesoName = "Reporte Cuadro Azul";
        this.usuario = u.getLogin();
        this.dstrct = u.getDstrct();
        this.anio = anio;
        this.nummes = nummes;
        this.nombremes = nombremes;
        this.opc = opc;
        this.u = u;
        this.model = model;
        
        super.start();
        
    }
    
    
    public synchronized void run(){
        
        boolean h = false;
        
        try{
            
            String comentario="EXITOSO";                       
            
            if( opc.equals("web") ){
                
                h = true;
                model.LogProcesosSvc.InsertProceso( this.procesoName, this.hashCode()," Reporte Cuadro Azul", this.usuario );
                
                model.FormatoSvc.cuadroAzul( anio, nombremes, nummes, dstrct );
                
                model.LogProcesosSvc.finallyProceso(this.procesoName,this.hashCode(),  this.usuario ,comentario);
                
            }else if( opc.equals("export") ){
                
                ReporteFormatoXLS r = new ReporteFormatoXLS();
                r.start( u, model.FormatoSvc.getVector() );
                
                
            }else if( opc.equals("excel") ){
                
                h = true;
                model.LogProcesosSvc.InsertProceso( this.procesoName, this.hashCode()," Reporte Cuadro Azul", this.usuario );
                
                model.FormatoSvc.cuadroAzul( anio, nombremes, nummes, dstrct );
                ReporteFormatoXLS r = new ReporteFormatoXLS();
                r.start( u, model.FormatoSvc.getVector() );
                
                model.LogProcesosSvc.finallyProceso(this.procesoName,this.hashCode(),  this.usuario ,comentario);
                
            }
            
            
        }catch (Exception ex){
            if ( h ){
                try{
                    model.LogProcesosSvc.finallyProceso(this.procesoName,this.hashCode(),  this.usuario ,"ERROR Hilo: " + ex.getMessage());
                }catch(Exception e){}
            }
        }
        
    }
}
