/*
 * reportePlanDeViaje.java
 *
 * Created on 24 de agosto de 2006, 08:49 AM
 */
/******************************************************************************
 * Nombre clase :                   reportePlanDeViaje.java                   *
 * Descripcion :                    Genera reporte Plan de Viaje              *
 * Autor :                          David Pi�a Lopez                          *
 * Fecha :                          24 de agosto de 2006, 08:49 AM            *
 * Version :                        1.0                                       *
 * Copyright :                      Fintravalores S.A.                   *
 *****************************************************************************/
package com.tsp.operation.model.threads;
import java.util.*;
import java.io.*;
import java.lang.*;
import java.sql.*;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.beans.POIWrite;
import com.tsp.exceptions.*;
import com.tsp.operation.controller.*;
import java.text.*;
import javax.servlet.http.*;
import com.tsp.util.*;
import com.tsp.operation.model.*;
import com.tsp.util.Util;
import java.sql.SQLException;

/**
 *
 * @author  David
 */
public class reportePlanDeViaje extends Thread {
    
    private Model model;
    private String fechainicio;
    private String fechafin;
    private String usuario;
    
    /** Creates a new instance of reportePlanDeViaje */
    public reportePlanDeViaje() {
        model = new Model();        
    }
    /*Inicia el hilo para la generacion del reporte*/
    public void start( String fechainicio, String fechafin, String usuario ) {
        this.fechainicio = fechainicio;
        this.fechafin = fechafin;
        this.usuario = usuario;
        super.start();        
    }
     public synchronized void run(){
        try{
            model.LogProcesosSvc.InsertProceso( "Reporte Plan de Viaje", this.hashCode(), "Reporte Plan de Viaje", usuario );
            POIWrite pw = new POIWrite();
            //obtener cabecera de ruta
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String path = rb.getString("ruta");
            //armas la ruta
            String Ruta  = path + "/exportar/migracion/" + usuario + "/";
            String fechaActual = Util.getFechaActual_String( 6 ).replaceAll( "/", "-" ).replaceAll( ":", "" );
            String fechaHoy = Util.getFechaActual_String( 8 );
            
            pw.nuevoLibro( Ruta + "ReportePlanDeViaje " + fechaActual + ".xls" );
            pw.obtenerHoja( "REPORTE" );
            for( int i = 0; i < 20; i++ ){
                pw.cambiarAnchoColumna( i, 5300 );
            }            
            pw.cambiarAnchoColumna( 5, 8500 );
            pw.cambiarAnchoColumna( 7, 8000 );
            pw.cambiarAnchoColumna( 11, 6000 );
            pw.cambiarAnchoColumna( 12, 7800 );
            pw.cambiarAnchoColumna( 13, 7300 );
            pw.cambiarAnchoColumna( 14, 6300 );
            HSSFCellStyle estiloCabecera = pw.nuevoEstilo("Tahoma", 8, true, false, "", HSSFColor.BLACK.index, pw.NONE, HSSFCellStyle.ALIGN_LEFT );
            HSSFCellStyle estiloDetalle1 = pw.nuevoEstilo("Tahoma", 8, true, false, "", HSSFColor.BLACK.index, HSSFColor.PALE_BLUE.index, HSSFCellStyle.ALIGN_CENTER, HSSFCellStyle.BORDER_THIN );
            HSSFCellStyle estiloDetalle2 = pw.nuevoEstilo("Tahoma", 8, false, false, "", HSSFColor.BLACK.index, pw.NONE, HSSFCellStyle.ALIGN_RIGHT );
            
            pw.adicionarCelda( 0, 0, "TRANSPORTES SANCHEZ POLO S.A.", estiloCabecera );
            pw.adicionarCelda( 1, 0, "REPORTE PLAN DE VIAJE / HOJA DE REPORTE", estiloCabecera );
            pw.adicionarCelda( 2, 0, "DESDE : " + fechainicio.replaceAll( "-", "" ) + " HASTA: " + fechafin.replaceAll( "/", "" ), estiloCabecera );
            pw.adicionarCelda( 3, 0, "FECHA DEL REPORTE: " + fechaHoy, estiloCabecera );
            
            pw.adicionarCelda( 5, 0, "# OC", estiloDetalle1 );
            pw.adicionarCelda( 5, 1, "PLANILLA MANUAL", estiloDetalle1 );            
            pw.adicionarCelda( 5, 2, "USUARIO GENERADOR", estiloDetalle1 );
            pw.adicionarCelda( 5, 3, "PLACA", estiloDetalle1 );
            pw.adicionarCelda( 5, 4, "TIPO DE VIAJE", estiloDetalle1 );
            pw.adicionarCelda( 5, 5, "RUTA", estiloDetalle1 );
            pw.adicionarCelda( 5, 6, "AGENCIA", estiloDetalle1 );
            pw.adicionarCelda( 5, 7, "CLIENTE", estiloDetalle1 );
            pw.adicionarCelda( 5, 8, "TIPO REGISTRO", estiloDetalle1 );
            pw.adicionarCelda( 5, 9, "EXISTENCIA", estiloDetalle1 );
            pw.adicionarCelda( 5, 10, "CELULAR", estiloDetalle1 );
            pw.adicionarCelda( 5, 11, "DESPACHADOR PLAN VIAJE", estiloDetalle1 );
            pw.adicionarCelda( 5, 12, "FECHA Y HORA MODIFICACI�N P.V.", estiloDetalle1 );
            pw.adicionarCelda( 5, 13, "TIEMPO DE VIAJE PROYECTADO", estiloDetalle1 );
            pw.adicionarCelda( 5, 14, "DESPACHADOR HOJA REPORTE", estiloDetalle1 );
            pw.adicionarCelda( 5, 15, "FECHA Y HORA DE SALIDA", estiloDetalle1 );
            pw.adicionarCelda( 5, 16, "LUGAR PERNOCTACION", estiloDetalle1 );
            pw.adicionarCelda( 5, 17, "FECHA DE CREACION", estiloDetalle1 );
            pw.adicionarCelda( 5, 18, "FECHA DE IMPRESION", estiloDetalle1 );
            pw.adicionarCelda( 5, 19, "IMPRESO POR", estiloDetalle1 );
            
            int fila = 6;
            model.planillaService.getHojadeReporte( fechainicio, fechafin );
            Vector planillas = model.planillaService.getPlanillas2();
            for( int i = 0; i < planillas.size(); i++ ){
                Planilla pla = (Planilla)planillas.get( i );
                
                pw.adicionarCelda( fila, 0, pla.getNumpla(), estiloDetalle2 );
                pw.adicionarCelda( fila, 1, "", estiloDetalle2 );
                pw.adicionarCelda( fila, 2, pla.getObservacion(), estiloDetalle2 );
                pw.adicionarCelda( fila, 3, pla.getPlaveh(), estiloDetalle2 );
                pw.adicionarCelda( fila, 4, pla.getTipoviaje(), estiloDetalle2 );
                pw.adicionarCelda( fila, 5, pla.getOrinom(), estiloDetalle2 );
                pw.adicionarCelda( fila, 6, pla.getNomagc(), estiloDetalle2 );
                pw.adicionarCelda( fila, 7, pla.getNomcliente(), estiloDetalle2 );
                pw.adicionarCelda( fila, 8, "HOJA DE REPORTE", estiloDetalle2 );
                pw.adicionarCelda( fila, 9, (pla.getNumrem().equalsIgnoreCase(""))? "NO":"SI", estiloDetalle2 );
                pw.adicionarCelda( fila, 10, "", estiloDetalle2 );
                pw.adicionarCelda( fila, 11, "", estiloDetalle2 );
                pw.adicionarCelda( fila, 12, "", estiloDetalle2 );
                pw.adicionarCelda( fila, 13, "", estiloDetalle2 );
                pw.adicionarCelda( fila, 14, pla.getDespachador(), estiloDetalle2 );
                pw.adicionarCelda( fila, 15, "", estiloDetalle2 );
                pw.adicionarCelda( fila, 16, "", estiloDetalle2 );
                pw.adicionarCelda( fila, 17, pla.getFeccum(), estiloDetalle2 );
                pw.adicionarCelda( fila, 18, pla.getPrinter_date2(), estiloDetalle2 );
                pw.adicionarCelda( fila, 19, pla.getNomprop(), estiloDetalle2 );
                
                fila++;                
            }
            model.planillaService.getPlanDeViaje( fechainicio, fechafin );
            planillas = model.planillaService.getPlanillas2();
            for( int i = 0; i < planillas.size(); i++ ){
                Planilla pla = (Planilla)planillas.get( i );
                
                float duracion = model.remesaService.calcularDuracion( pla.getRuta_pla() );
                
                pw.adicionarCelda( fila, 0, pla.getNumpla(), estiloDetalle2 );
                pw.adicionarCelda( fila, 1, "", estiloDetalle2 );
                pw.adicionarCelda( fila, 2, pla.getObservacion(), estiloDetalle2 );
                pw.adicionarCelda( fila, 3, pla.getPlaveh(), estiloDetalle2 );
                pw.adicionarCelda( fila, 4, pla.getTipoviaje(), estiloDetalle2 );
                pw.adicionarCelda( fila, 5, pla.getOrinom(), estiloDetalle2 );
                pw.adicionarCelda( fila, 6, pla.getNomagc(), estiloDetalle2 );
                pw.adicionarCelda( fila, 7, pla.getNomcliente(), estiloDetalle2 );
                pw.adicionarCelda( fila, 8, "PLAN VIAJE", estiloDetalle2 );
                pw.adicionarCelda( fila, 9, (pla.getNumrem().equalsIgnoreCase(""))? "NO":"SI", estiloDetalle2 );
                pw.adicionarCelda( fila, 10, pla.getCelularcon(), estiloDetalle2 );
                pw.adicionarCelda( fila, 11, pla.getDespachador(), estiloDetalle2 );
                pw.adicionarCelda( fila, 12, pla.getFecdsp(), estiloDetalle2 );
                pw.adicionarCelda( fila, 13, "" + duracion, estiloDetalle2 );
                pw.adicionarCelda( fila, 14, "", estiloDetalle2 );
                pw.adicionarCelda( fila, 15, pla.getFecha_salida(), estiloDetalle2 );
                pw.adicionarCelda( fila, 16, "", estiloDetalle2 );
                pw.adicionarCelda( fila, 17, pla.getFeccum(), estiloDetalle2 );
                pw.adicionarCelda( fila, 18, "", estiloDetalle2 );
                pw.adicionarCelda( fila, 19, "", estiloDetalle2 );
                
                fila++;                
            }
            for( int i = 0; i < planillas.size(); i++ ){
                Planilla pla = (Planilla)planillas.get( i );
                
                float duracion = model.remesaService.calcularDuracion( pla.getRuta_pla() );
                
                pw.adicionarCelda( fila, 0, pla.getNumpla(), estiloDetalle2 );
                pw.adicionarCelda( fila, 1, "", estiloDetalle2 );
                pw.adicionarCelda( fila, 2, pla.getObservacion(), estiloDetalle2 );
                pw.adicionarCelda( fila, 3, pla.getPlaveh(), estiloDetalle2 );
                pw.adicionarCelda( fila, 4, pla.getTipoviaje(), estiloDetalle2 );
                pw.adicionarCelda( fila, 5, pla.getOrinom(), estiloDetalle2 );
                pw.adicionarCelda( fila, 6, pla.getNomagc(), estiloDetalle2 );
                pw.adicionarCelda( fila, 7, pla.getNomcliente(), estiloDetalle2 );
                pw.adicionarCelda( fila, 8, "PERNOCTACION", estiloDetalle2 );
                pw.adicionarCelda( fila, 9, (pla.getNomdesrel().equalsIgnoreCase("")&&pla.getNomdest().equalsIgnoreCase("")&&pla.getNomori().equalsIgnoreCase(""))? "NO":"SI", estiloDetalle2 );
                pw.adicionarCelda( fila, 10, "", estiloDetalle2 );
                pw.adicionarCelda( fila, 11, pla.getDespachador(), estiloDetalle2 );
                pw.adicionarCelda( fila, 12, pla.getFecdsp(), estiloDetalle2 );
                pw.adicionarCelda( fila, 13, "" + duracion, estiloDetalle2 );
                pw.adicionarCelda( fila, 14, "", estiloDetalle2 );
                pw.adicionarCelda( fila, 15, pla.getFecha_salida(), estiloDetalle2 );
                pw.adicionarCelda( fila, 16, pla.getNomdesrel()+"-"+pla.getNomdest()+"-"+pla.getNomori(), estiloDetalle2 );
                pw.adicionarCelda( fila, 17, pla.getFeccum(), estiloDetalle2 );
                pw.adicionarCelda( fila, 18, "", estiloDetalle2 );
                pw.adicionarCelda( fila, 19, "", estiloDetalle2 );
                
                fila++;                
            }
            for( int i = 0; i < planillas.size(); i++ ){
                Planilla pla = (Planilla)planillas.get( i );
                
                float duracion = model.remesaService.calcularDuracion( pla.getRuta_pla() );
                
                pw.adicionarCelda( fila, 0, pla.getNumpla(), estiloDetalle2 );
                pw.adicionarCelda( fila, 1, "", estiloDetalle2 );
                pw.adicionarCelda( fila, 2, pla.getObservacion(), estiloDetalle2 );
                pw.adicionarCelda( fila, 3, pla.getPlaveh(), estiloDetalle2 );
                pw.adicionarCelda( fila, 4, pla.getTipoviaje(), estiloDetalle2 );
                pw.adicionarCelda( fila, 5, pla.getOrinom(), estiloDetalle2 );
                pw.adicionarCelda( fila, 6, pla.getNomagc(), estiloDetalle2 );
                pw.adicionarCelda( fila, 7, pla.getNomcliente(), estiloDetalle2 );
                pw.adicionarCelda( fila, 8, "CELULAR", estiloDetalle2 );
                pw.adicionarCelda( fila, 9, (pla.getCelularcon().equalsIgnoreCase(""))? "NO":"SI", estiloDetalle2 );
                pw.adicionarCelda( fila, 10, pla.getCelularcon(), estiloDetalle2 );
                pw.adicionarCelda( fila, 11, pla.getDespachador(), estiloDetalle2 );
                pw.adicionarCelda( fila, 12, pla.getFecdsp(), estiloDetalle2 );
                pw.adicionarCelda( fila, 13, "" + duracion, estiloDetalle2 );
                pw.adicionarCelda( fila, 14, "", estiloDetalle2 );
                pw.adicionarCelda( fila, 15, pla.getFecha_salida(), estiloDetalle2 );
                pw.adicionarCelda( fila, 16, "", estiloDetalle2 );
                pw.adicionarCelda( fila, 17, pla.getFeccum(), estiloDetalle2 );
                pw.adicionarCelda( fila, 18, "", estiloDetalle2 );
                pw.adicionarCelda( fila, 19, "", estiloDetalle2 );
                
                fila++;                
            }            
            pw.cerrarLibro();            
            model.LogProcesosSvc.finallyProceso( "Reporte Plan de Viaje", this.hashCode(), usuario, "PROCESO EXITOSO" );
        }catch(Exception ex ){
            ex.printStackTrace();            
            //Capturo errores finalizando proceso
            try{                
                model.LogProcesosSvc.finallyProceso( "Reporte Plan de Viaje", this.hashCode(), usuario, "ERROR : " + ex.getMessage() );            
            } catch( Exception f ){                
                f.printStackTrace();
                try{                    
                    model.LogProcesosSvc.finallyProceso( "Reporte Plan de Viaje",this.hashCode(), usuario, "ERROR : " + f.getMessage() );
                } catch( Exception p ){ p.printStackTrace(); }                
            }
        }
     }
     /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        reportePlanDeViaje hilo = new reportePlanDeViaje();
        hilo.start("2006-08-01", "2006-08-03", "GPINA" );
    }
    
}
