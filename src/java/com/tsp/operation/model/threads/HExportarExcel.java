/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.threads;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.Model;

import java.io.*;
import java.util.*;
import java.text.*;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.hssf.util.Region;

/**
 *
 * @author maltamiranda
 */
public class HExportarExcel extends Thread{

    private ArrayList info;
    private Model model;
    private Usuario usuario;
    private String processName,ruta;
    HSSFCellStyle header  , titulo1, titulo2, titulo5, letra, numero, dinero, numeroCentrado, porcentaje, letraCentrada, numeroNegrita, ftofecha;
    
    public void start(Model model,ArrayList info, Usuario usuario,String processName ){
        this.info = info;
        this.usuario = usuario;
        this.model = model;
        this.processName=processName;
        super.start();
    }

    public synchronized void run(){
        try{
            model.LogProcesosSvc.InsertProceso(this.processName, this.hashCode(), "Exportacion a EXCEL", usuario.getLogin());
            this.generarRUTA();
            this.generarArchivo();
            model.LogProcesosSvc.finallyProceso(this.processName, this.hashCode(), usuario.getLogin(), "PROCESO EXITOSO");
        }catch (Exception ex){
            ex.printStackTrace();
            try{
                model.LogProcesosSvc.finallyProceso(this.processName, this.hashCode(), usuario.getLogin(), "Finalizado con error ...\n" + ex.getMessage());
            }catch (Exception e){
                System.out.println("Error "+this.processName +"...\n" + e.getMessage());
            }
        }
    }

    public HSSFCellStyle nuevoEstilo (HSSFWorkbook wb, String name, int size, boolean bold, boolean italic , String formato ,int color, int fondo, int align, int border)throws Exception{
        if (wb==null)
            throw new Exception("No se pudo crear el estilo, primero deber� crear el libro");
        
        HSSFCellStyle style   = wb.createCellStyle();
        HSSFFont      font    = wb.createFont();
        
        font.setFontHeightInPoints((short) size);
        font.setFontName(name);                                 
        if (bold)   font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        font.setItalic(italic);
        if (color!=-1) font.setColor((short) color);
        
        
        style.setFont(font);
        if (!formato.equals(""))
            style.setDataFormat(wb.createDataFormat().getFormat(formato)); 
        
        if (fondo!=-1) {
            style.setFillPattern((short) HSSFCellStyle.SOLID_FOREGROUND);
            style.setFillForegroundColor((short)fondo);
        }
        
        if (align!=-1) style.setAlignment((short)align);
        
        if (border>0){
            style.setBorderBottom((short)border);
            style.setBottomBorderColor(HSSFColor.BLACK.index);
            
            style.setBorderLeft((short)border);
            style.setLeftBorderColor(HSSFColor.BLACK.index);
            
            style.setBorderRight((short)border);
            style.setRightBorderColor(HSSFColor.BLACK.index);
            
            style.setBorderTop((short)border);
            style.setTopBorderColor(HSSFColor.BLACK.index);
        }      
        return style;
    }

    public void crear_estilos (HSSFWorkbook wb)
    {   try {
            header         = nuevoEstilo(wb,"Tahoma", 14, true  , false, "text"  , HSSFColor.ORANGE.index , -1 , HSSFCellStyle.ALIGN_LEFT,-1);
            titulo1        = nuevoEstilo(wb,"Tahoma", 8 , true  , false, "text"  , -1  , -1 , -1,-1);
            titulo2        = nuevoEstilo(wb,"Tahoma", 8 , true  , false, "text"  , HSSFColor.WHITE.index, HSSFColor.DARK_BLUE.index , HSSFCellStyle.ALIGN_CENTER, 2);
            letra          = nuevoEstilo(wb,"Tahoma", 8 , false , false, ""      , -1 , -1 , -1,-1);
            letraCentrada  = nuevoEstilo(wb,"Tahoma", 8 , false , false, ""      , -1 , -1 , HSSFCellStyle.ALIGN_CENTER,-1);
            numero         = nuevoEstilo(wb,"Tahoma", 8 , false , false, "#"     , -1 , -1 , -1,-1);
            numeroCentrado = nuevoEstilo(wb,"Tahoma", 8 , false , false, "#"     , -1 , -1 , HSSFCellStyle.ALIGN_CENTER,-1);
            dinero         = nuevoEstilo(wb,"Tahoma", 8 , false , false, "#,##0.00" , -1 , -1 , -1,-1);
            numeroNegrita  = nuevoEstilo(wb,"Tahoma", 8 , true  , false, "#"     , -1 , -1 , -1,-1);
            porcentaje     = nuevoEstilo(wb,"Tahoma", 8 , false , false, "0.00%" , -1 , -1 , -1,-1);
            ftofecha       = nuevoEstilo(wb,"Tahoma", 8 , false , false, "yyyy-mm-dd" , -1 , -1 , -1,-1);
            titulo5        = nuevoEstilo(wb,"Tahoma", 14, true  , false, "text"  , -1  , -1 , -1, -1);
        }catch (Exception ex){
            ex.printStackTrace();
            ex.getMessage();
        }
    }

    public void generarRUTA() throws Exception{
        try{

            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            ruta = rb.getString("ruta") + "/exportar/migracion/" + usuario.getLogin();
            File archivo = new File( ruta );
            if (!archivo.exists()) archivo.mkdirs();

        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }

    }

    private void generarArchivo() throws Exception {
        HSSFWorkbook wb = new HSSFWorkbook();
        HSSFSheet    sheet;
        sheet = wb.createSheet("BASE");
        this.crear_estilos(wb);

/////////////////inicio escritura cabecera archivo//////////////////////////////
        
        sheet.addMergedRegion(new Region(0,(short)0 ,0,(short)8));


        HSSFRow row   = sheet.createRow( (short) 0);
        HSSFCell cell = row.createCell ( (short) 0);
        cell.setCellValue(processName);
        cell.setCellStyle(header);

        row   = sheet.createRow( (short) 1);
        cell = row.createCell ( (short) 0);
        cell.setCellValue("FECHA");
        cell.setCellStyle(titulo1);

        row   = sheet.createRow( (short) 1);
        cell = row.createCell ( (short) 1);
        cell.setCellValue( (new SimpleDateFormat("yyyy-MM-dd hh:mm a")).format( new Date()));
        cell.setCellStyle(titulo1);

        row   = sheet.createRow( (short) 2);
        cell = row.createCell ( (short) 0);
        cell.setCellValue("USUARIO");
        cell.setCellStyle(titulo1);

        row   = sheet.createRow( (short) 2);
        cell = row.createCell ( (short) 1);
        cell.setCellValue(usuario.getNombre());
        cell.setCellStyle(titulo1);

/////////////////final escritura cabecera archivo///////////////////////////////
///////////////////////inicio escritura datos///////////////////////////////////

        for(int i=0;i<info.size()&&info.size()>1;i++){
            ArrayList info2=(ArrayList)info.get(i);
            for(int j=0;j<info2.size();j++){
                row   = sheet.createRow( (short) i+4);
                cell = row.createCell ( (short) j);
                cell.setCellValue((String)info2.get(j));
                cell.setCellStyle((i==0)?titulo2:letra);
            }
        }

        if(info.size()<=1)
        {   row   = sheet.createRow( (short) 4);
            cell = row.createCell ( (short) 0);
            cell.setCellValue("NO HAY DATOS PARA MOSTRAR");
            cell.setCellStyle(titulo5);
        }

        String archivo=ruta + "/" +processName+ (new SimpleDateFormat("yyyMMdd_hhmmss")).format( new Date()) +".xls";
        FileOutputStream fileOut = new FileOutputStream(archivo);
        wb.write(fileOut);
        fileOut.close();
        wb = null;
//////////////////////////final escritura datos/////////////////////////////////
    }
}
