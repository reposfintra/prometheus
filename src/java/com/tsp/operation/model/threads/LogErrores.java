/*
 * ReporteBalancePrueba.java
 *
 * Created on 20 de octubre de 2005, 05:07 PM
 */

package com.tsp.operation.model.threads;

import java.io.*;
import java.util.*;

import com.tsp.exceptions.*;
import java.io.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.pdf.*;
import java.util.*;
import org.dom4j.Element;
import javax.mail.*;
import com.tsp.operation.model.Model;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import com.tsp.util.*;
/**
 *
 * @author  JuanM
 */
public class LogErrores extends Thread{
    private  FileWriter        fw;
    private  BufferedWriter    bf;
    private  PrintWriter       linea;
    private String usuario;
    private List listaD;
    private String procesoName;
    private String des;
    private com.tsp.operation.model.Model model;
    /** Creates a new instance of MigracionCambioCheque */
    public LogErrores() {
    }
    
    public void start(String id, List lista ){
        this.usuario = id;
        this.listaD = lista;
        this.procesoName = "Reporte Diario Despacho";
        this.des = "Proceso de creacion del Log del Reporte Diario de Despacho";
        super.start();
    }
    
    public synchronized void run(){
        try{
            /*Archivo excel*/
            model = new Model();
            Util util   = new Util();
            
            model.LogProcesosSvc.InsertProceso( this.procesoName, this.hashCode(),this.des, this.usuario );
            
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String url = rb.getString("ruta");
            String fecha = model.movplaService.obtenerFechaActualEnFormato("YYYYMMDD_HHMIss");
            url += "/exportar/migracion/"+usuario+"/ReporteDiarioDespachoLogErrores"+fecha+".txt";
            
            //AMATURANA 22.03.2007
            File ruta = new File( "/exportar/migracion/"+usuario+"/" );
            if (!ruta.exists()) ruta.mkdirs();
            File file = new File(url);
            
            this.fw         = new FileWriter    ( url );
            this.bf         = new BufferedWriter( this.fw  );
            this.linea      = new PrintWriter   ( this.bf  );
            
            linea.println("ERRORES ENCONTRADOS                  ");
            
            Iterator it = listaD.iterator();
            while( it.hasNext() ){
                Informe inf = (Informe)it.next();
                //Validacion de Valor de la Remesa
                if( inf.getVlr_rem_base() == 0 )
                    linea.println("La OT # " + inf.getRemision() + " tiene valor base 0 y no se puede calcular el porcentaje prorrateado de la remesa, se muestra el campo % Prorrateado Remesa en la reporte con valor 0" );
                
                if( inf.getVlr_pla_base() == 0 )
                    linea.println("La OC # " + inf.getNumpla() + " tiene valor base 0 y no se puede calcular el porcentaje prorrateado de la planilla, se muestra el campo % Prorrateado planilla en la reporte con valor 0" );   
                
                if( inf.getVlr_rem_pro() == 0 )
                    linea.println("La OC # " + inf.getNumpla() + " tiene valor remesa prorrateado 0 y no se puede calcular el porcentaje utilidad bruta, se muestra el campo % Utilidad Bruta en la reporte con valor 0" );                
                //Validacion moneda de la Planilla
                String monedapla = ( inf.getMonedapla()!=null )?inf.getMonedapla():"";
                monedapla = monedapla.trim();
                if( monedapla.equals("") )
                    linea.println("La Planilla # " + inf.getNumpla() + " tiene la moneda vacia" );
                //Validacion moneda de la Remesa
                String monedaremesa = ( inf.getMonedarem()!=null )?inf.getMonedarem():"";
                monedaremesa = monedaremesa.trim();
                if( monedaremesa.equals("") )
                    linea.println("La Remesa # " + inf.getRemision() + " tiene la moneda vacia" );
            }
            // Fin de recorrido de lista
            
            String msg = "";
            if( listaD.size() > 0 ){
                msg = " Hay " + listaD.size() + " registros  que marcaron  \n"+
                " con rojo en el archivo en excel, verificar  \n"+
                " el log de errores.";
            }
            else
                msg = "Se genero el reporte exitosamente";
            
            linea.println("FIN DE ARCHIVO" );
            
            this.linea.close(); //Fin de la archivo
            
            model.LogProcesosSvc.finallyProceso(this.procesoName,this.hashCode(),  this.usuario ,msg);
            
        }catch(Exception e){
            e.printStackTrace();
            try{
                model.LogProcesosSvc.finallyProceso(this.procesoName,this.hashCode(),  this.usuario ,"ERROR HILO : " + e.getMessage());
            }catch(Exception ex){}
        }
    }
    
    public static void main(String a []){
        SendMailManager hilo = new SendMailManager();
        // hilo.start("jescandon@mail.tsp.com", "jescandon@mail.tsp.com", "mail.tsp.com", "c:/ReportePC.xls", "true", "HOSORIO");
    }
    
}
//22 Marzo de 2007
