/*************************************************************
 * Nombre: ReporteC620XLS.java
 * Descripci�n: Hilo para crear el reporte de migraci�n C620.
 * Autor: Ing. Jose de la rosa
 * Fecha: 3 de diciembre de 2005, 11:03 AM
 * Versi�n: Java 1.0
 * Copyright: Fintravalores S.A. S.A.
 **************************************************************/

package com.tsp.operation.model.threads;

import java.text.*;
import java.util.Date;
import java.text.*;
import java.sql.SQLException;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.util.*;
import java.util.*;
import java.io.*;

public class ReporteC620XLS extends Thread{
    private String procesoName;
    private String des;
    String id="";
    Vector vec = new Vector ();
    
    /** Creates a new instance of ReporteC620XLS */
    public void start (String id, Vector vec) {
        this.id= id;
        this.vec=vec;
        this.procesoName = "Reporte C620";
        this.des = "Reporte migraci�n c620 :";
        super.start ();
    }

    public synchronized void run (){
        try{
            Model model = new Model ();
            model.LogProcesosSvc.InsertProceso(this.procesoName, this.hashCode(), des, this.id);
            String fecha_actual = Util.getFechaActual_String (6);
            String a�o = fecha_actual.substring (0,4);
            String mes = fecha_actual.substring (5,7);
            String dia = fecha_actual.substring (8,10);
            ResourceBundle rb = ResourceBundle.getBundle ("com/tsp/util/connectionpool/db");
            String path = rb.getString ("ruta");
            File file = new File (path + "/exportar/migracion/" + id + "/");
            file.mkdirs ();
            String NombreArchivo = path + "/exportar/migracion/" + id + "/C620_"+a�o+mes+dia+".csv";
            PrintStream archivo = new PrintStream (NombreArchivo);
            if (vec!=null && vec.size () >0 ){
                for (int i = 0; i < vec.size (); i++){
                    model.remesaService.buscaRemesas ( (String)vec.get (i) );
                    Vector vecrem = model.remesaService.getRemesas ();
                    for (int j = 0; j < vecrem.size (); j++){
                        Remesa r = (Remesa) vecrem.elementAt (j);
                        //BUSCO LOS DOCUMENTOS RELACIONADOS CON LA REMESA
                        remesa_doctoDAO remDoc = new remesa_doctoDAO ();
                        remDoc.MigrarDocumentos ((String)vec.get (i));
                        Vector  documentos = remDoc.getDocumentos ();
                        String docs="";
                        String facts="";
                        int catDocs =0;
                        int catFacts =0;
                        for (int k=0; k<documentos.size ();k++){
                            remesa_docto rd = (remesa_docto) documentos.elementAt (k) ;
                            if(rd.getTipo_doc ().equals ("008")){
                                catDocs++;
                                if(catDocs<=15)
                                    docs=","+( rd.getDestinatario().equals("")?rd.getDocumento():rd.getDocumento()+":"+rd.getDestinatario() );
                            }
                            else if(rd.getTipo_doc ().equals ("009")){
                                catFacts++;
                                if(catFacts<=15)
                                    facts=","+rd.getDocumento();
                            }
                        }
                        if(catFacts<15)
                            for(int m=0; m<(15-catDocs); m++)
                                docs+=",";
                        if(catFacts<15)
                            for(int m=0; m<(15-catFacts); m++)
                                facts+=",";
                        archivo.println ((String)vec.get (i)+","+r.getStdJobNo ()+","+r.getFecRem ()+","+r.getPesoReal ()+",,,,,,,"+docs+facts );
                    }
                }
                
            }
            model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(), this.id, "PROCESO EXITOSO");
        }
        catch(Exception e){
            try{
                Model model = new Model();
                model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(),this.id,"ERROR :" + e.getMessage());
            }
            catch(Exception f){
                try{
                    Model model = new Model();
                    model.LogProcesosSvc.finallyProceso(this.procesoName,this.hashCode(),this.id,"ERROR :");
                }catch(Exception p){    }
            }
        }
    }
}
