
/*
* Nombre        SerieDocAnuladosTh.java
* Descripci�n   Exportaci�n a MS Excel de la consulta
* Autor         Ing. Andr�s Maturana D.
* Fecha         22 de marzo de 2007
* Versi�n       1.0
* Coyright      Transportes Sanchez Polo S.A.
*/

package com.tsp.operation.model.threads;

import com.tsp.operation.model.beans.POIWrite;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.beans.RepGral;
import com.tsp.operation.model.Model;

import java.io.*;
import java.util.*;
import java.text.*;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.util.HSSFColor;


/**
 *
 * @author  equipo
 */
public class ListadoCarteraTh extends Thread{
    

    private Vector info;
    private Model model;
    private Usuario usuario;
    private String fecha;
    
    private SimpleDateFormat fmt;
    private String processName = "Reporte de Cartera";
    
    
    
    // variables del archivo de excel
    String   ruta;
    String   nombre;
    POIWrite xls;
    HSSFCellStyle header  , titulo1, titulo2, titulo3 , titulo4, titulo5, letra, numero, dinero, numeroCentrado, porcentaje, letraCentrada, numeroNegrita, ftofecha;
    HSSFColor     cAzul   , cVerde, cAmarillo, cGris ;
    int fila = 0;
    
    
    /** Creates a new instance of HReporteOcsConAnticipoSinReporte */
    public ListadoCarteraTh() {
    }
    
    public void start(Model model, Vector info, Usuario usuario, String fecha){
        this.info = info;
        this.usuario = usuario;
        this.model = model;
        this.fecha = fecha;
        super.start();
    }
    
    public synchronized void run(){
        try{
            model.LogProcesosSvc.InsertProceso(this.processName, this.hashCode(), "Generacion del Reporte de Cartera. Fecha de corte: " + fecha, usuario.getLogin());
            this.generarRUTA();
            this.generarArchivo();
            model.LogProcesosSvc.finallyProceso(this.processName, this.hashCode(), usuario.getLogin(), "PROCESO EXITOSO");
        }catch (Exception ex){
            ex.printStackTrace();
            try{
                model.LogProcesosSvc.finallyProceso(this.processName, this.hashCode(), usuario.getLogin(), "Finalizado con error ...\n" + ex.getMessage());
            }catch (Exception e){
                //System.out.println("Error HReporteOcsConAnticipoSinReporte ...\n" + e.getMessage());
            }
        }
    }
    
    
    public void generarRUTA() throws Exception{
        try{
            
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            ruta = rb.getString("ruta") + "/exportar/migracion/" + usuario.getLogin();
            File archivo = new File( ruta );
            if (!archivo.exists()) archivo.mkdirs();
            
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
        
    }
    
    
    /**
     * Metodo para Incializar las variables del archivo de excel
     * @autor mfontalvo
     * @param nameFile, nombre del archivo
     * throws Exception.
     */
    private void InitArchivo(String nameFile) throws Exception{
        try{
            xls          = new com.tsp.operation.model.beans.POIWrite();
            nombre       = "/exportar/migracion/" + usuario.getLogin() + "/" + nameFile;
            xls.nuevoLibro( ruta + "/" + nameFile );
            
            // colores
            /*cAzul       = xls.obtenerColor(  0, 51,102);
            cVerde      = xls.obtenerColor( 51,153,102);
            cAmarillo   = xls.obtenerColor(255,255,153);
            cGris       = xls.obtenerColor(192,192,192);*/
            
            
            // estilos
            header         = xls.nuevoEstilo("Tahoma", 14, true  , false, "text"  , HSSFColor.ORANGE.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
            titulo1        = xls.nuevoEstilo("Tahoma", 8 , true  , false, "text"  , xls.NONE  , xls.NONE , xls.NONE);
            titulo2        = xls.nuevoEstilo("Tahoma", 8 , true  , false, "text"  , HSSFColor.WHITE.index, HSSFColor.DARK_BLUE.index , HSSFCellStyle.ALIGN_CENTER, 2);
            //titulo3        = xls.nuevoEstilo("Tahoma", 8 , true  , false, "text"  , HSSFColor.WHITE.index, cAmarillo.getIndex() , HSSFCellStyle.ALIGN_CENTER, 2);
            //titulo4        = xls.nuevoEstilo("Tahoma", 8 , true  , false, "text"  , HSSFColor.WHITE.index, cVerde.getIndex() , HSSFCellStyle.ALIGN_CENTER, 2);
            //titulo5        = xls.nuevoEstilo("Tahoma", 14, true  , false, "text"  , xls.NONE  , xls.NONE , xls.NONE);
            letra          = xls.nuevoEstilo("Tahoma", 8 , false , false, ""      , xls.NONE , xls.NONE , xls.NONE);
            letraCentrada  = xls.nuevoEstilo("Tahoma", 8 , false , false, ""      , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_CENTER);
            numero         = xls.nuevoEstilo("Tahoma", 8 , false , false, "#"     , xls.NONE , xls.NONE , xls.NONE);
            numeroCentrado = xls.nuevoEstilo("Tahoma", 8 , false , false, "#"     , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_CENTER);
            dinero         = xls.nuevoEstilo("Tahoma", 8 , false , false, "#,##0.00" , xls.NONE , xls.NONE , xls.NONE);
            numeroNegrita  = xls.nuevoEstilo("Tahoma", 8 , true  , false, "#"     , xls.NONE , xls.NONE , xls.NONE);
            porcentaje     = xls.nuevoEstilo("Tahoma", 8 , false , false, "0.00%" , xls.NONE , xls.NONE , xls.NONE);
            ftofecha       = xls.nuevoEstilo("Tahoma", 8 , false , false, "yyyy-mm-dd" , xls.NONE , xls.NONE , xls.NONE);
            
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception("Error en InitArchivo .... \n" + ex.getMessage());
        }
        
    }
    
    
    
    /**
     * Metodo para crear el  archivo de excel
     * @autor mfontalvo
     * @param nameFile, Nombre del archivo
     * @param titulo, titulo del archivo de excel
     * @throws Exception.
     */
    private void crearArchivo(String nameFile, String titulo) throws Exception{
        try{
            fmt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            InitArchivo(nameFile);
            xls.obtenerHoja("Base");
            xls.combinarCeldas(0, 0, 0, 8);
            xls.adicionarCelda(0,0, titulo, header);
            xls.adicionarCelda(1,0, "FECHA" , titulo1);
            xls.adicionarCelda(1,1, fmt.format( new Date())  , titulo1 );
            //xls.adicionarCelda(2,0, "PERIODO", titulo1);
            //xls.adicionarCelda(2,1, finicial + " - " + ffinal  , titulo1);
            xls.adicionarCelda(2,0, "USUARIO", titulo1);
            xls.adicionarCelda(2,1, usuario.getNombre() , titulo1);
            
            
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception( "Error en crearArchivo ....\n"  + ex.getMessage() );
        }
    }
    
    
    /**
     * Exportacion del las amortizaciones transferidas
     * @autor mfontalvo
     * @throws Exception.
     */
    private void generarArchivo() throws Exception {
        try{
            fmt= new SimpleDateFormat("yyyMMdd_hhmmss");
            this.crearArchivo("ReporteCartera_" + fmt.format( new Date() ) +".xls", "REPORTE DE CARTERA ");
            fila = 6;
            boolean sw =this.usuario.getPerfil().equals("FID_COLPATR"); //Validacion de negocio
            Vector vector = this.info;
            if (vector!=null && !vector.isEmpty()){
                


                    // encabezado
                    String [] cabecera = {
                    "GESTION",    
                    "PROXIMA ACCION","FECHA PROXIMA ACCION","ESTADO DEL CLIENTE","CODIGO","ID","CLIENTE","NIT AFILIADO","AFILIADO",
                    "FECHA DESEM","DIRECCION","BARRIO","TELEFONO","CIUDAD","CED ESTUDIANTE","NOMBRE ESTUDIANTE","TELEFONO ESTUDIANTE","DIRECCION ESTUDIANTE",
                    "CED CODEUDOR","NOMBRE CODEUDOR","TELEFONO CODEUDOR","DIRECCION CODEUDOR","TASA FENALCO","FACTURA","ASESOR COMERCIAL","FECHA ULTIMO PAGO","VLR ULTIMO PAGO",
                    "CONSECUTIVO", "FECHA FACTURA", "FECHA VENCIMIENTO", "SOLICITUD","CICLO","FECHA FACTURACION","No AVAL","BANCO","REFERENCIA",
                    "CUENTA","DIAS","RANGO DE VENCIMIENTO","VALOR FACTURA","VALOR SALDO FACTURA","VALOR CAPITAL","VALOR CUOTA ADMIN","VALOR INTERESES", "VALOR CAT","VALOR SEGURO","CAPITAL AVAL","INTERES AVAL","VALOR AVAL","NEGOCIO","NUMERO DE PAGARE","NEGOCIO REL","CONVENIO",
                    "PLAZO","TASA","PLAN AL DIA",
                    "AGENCIA"
                     };
                  
              
                short  [] dimensiones = new short [] {
                    
                    5000,5000,5000,5000,5000,5000,5000,5000,5000, 5000,
                    5000,5000,5000,5000,5000,5000,5000,5000,5000, 5000,
                    5000,5000,5000,5000,5000,5000,5000,5000,5000, 5000,
                    5000,5000,5000,5000,5000,5000,5000,5000,5000, 5000,
                    5000,5000,5000,5000,5000,5000,5000,5000,5000, 5000,
                    5000,5000, 5000,5000,5000,5000, 5000,5000 
                };
                for ( int i = 0; i<cabecera.length; i++){
                    xls.adicionarCelda(fila,  i, cabecera[i], titulo2);
                    if ( i < dimensiones.length )
                        xls.cambiarAnchoColumna(i, dimensiones[i] );
                }                
                fila++;
                
                
                int cont=0;
                for ( int i = 0; i< vector.size(); i++){
                    int col = 0;
                    RepGral rp = (RepGral) vector.elementAt(i);
                    if(fila>65535)
                    {   fila=0;cont++;
                        xls.newSheet("BASE"+cont);
                    }
                    
                    xls.adicionarCelda(fila  , col++ , rp.getGestion(), letra  );//GESTION
                    //xls.adicionarCelda(fila  , col++ , rp.getConcepto_desc(), letra  );//comentarios
                    xls.adicionarCelda(fila  , col++ , rp.getProxima_accion(), letra  );//PROXIMA ACCION
                    xls.adicionarCelda(fila  , col++ , rp.getFecha_proxima_accion(), letra  );//FECHA PROXIMA ACCION
                    xls.adicionarCelda(fila  , col++ , rp.getEstado_cliente(), letra  );//ESTADO DEL CLIENTE
                    xls.adicionarCelda(fila  , col++ , rp.getCodcli(), letra  );//CODIGO
                    xls.adicionarCelda(fila  , col++ , rp.getNitpro(), letra  );//ID 
                    xls.adicionarCelda(fila  , col++ , rp.getNomcli(), letra  );//CLIENTE
                    xls.adicionarCelda(fila  , col++ , rp.getProveedor(), letra  );//NIT AFILIADO
                    xls.adicionarCelda(fila  , col++ , rp.getNitter(), letra  );//AFILIADO
                    xls.adicionarCelda(fila  , col++ , rp.getDespla(), ftofecha  );//FECHA DESEMBOLSO
                    xls.adicionarCelda(fila  , col++ , rp.getDireccion(), letra  );//DIRECCION
                    xls.adicionarCelda(fila  , col++ , rp.getBarrio(), letra  );//BARRIO
                    xls.adicionarCelda(fila  , col++ , rp.getNom_costo_2(), letra  );//Telefono de contacto
                    xls.adicionarCelda(fila  , col++ , rp.getCiudad(), letra  );//CIUDAD
                    xls.adicionarCelda(fila  , col++ , rp.getCedula_estudiante(), letra  );//CED ESTUANTE
                    xls.adicionarCelda(fila  , col++ , rp.getNombre_estudiante(), letra  );//NOMBRE ESTUANTE
                    xls.adicionarCelda(fila  , col++ , rp.getTelefono_estudiante(), letra  );//TELEFONO ESTUDIANTE
                    xls.adicionarCelda(fila  , col++ , rp.getDireccion_estudiante(), letra  );//DIRECCION ESTUDIANTE
                    xls.adicionarCelda(fila  , col++ , rp.getCedula_codeudor(), letra  );//CED CODEUDOR
                    xls.adicionarCelda(fila  , col++ , rp.getNombre_codeudor(), letra  );//NOMBRE CODEUDOR
                    xls.adicionarCelda(fila  , col++ , rp.getTelefono_codeudor(), letra  );//TELEFONO CODEUDOR
                    xls.adicionarCelda(fila  , col++ , rp.getDireccion_codeudor(), letra  );//DIRECCION CODEUDOR
                    xls.adicionarCelda(fila  , col++ , rp.getRegistros(), numero  );//TASA FENALCO
                    xls.adicionarCelda(fila  , col++ , rp.getFactura(), ftofecha  );//FACTURA
                    xls.adicionarCelda(fila  , col++ , rp.getAsesor(), letra  );//ASESOR COMERCIAL
                    xls.adicionarCelda(fila  , col++ , rp.getFecha_ultimo_pago(), ftofecha  );//FECHA ULTIMO PAGO
                    xls.adicionarCelda(fila  , col++ , rp.getVlr_ultimo_pago(), numero  );//VLR ULTIMO PAGO
                    xls.adicionarCelda(fila  , col++ , rp.getDocu(), letra  );//CONSECUTIVO
                    xls.adicionarCelda(fila  , col++ , rp.getFecha_fra(), ftofecha  );//FECHA FACTURA
                    xls.adicionarCelda(fila  , col++ , rp.getFecha_venc_fra(), ftofecha  );//FECHA VENCIMIENTO
                    xls.adicionarCelda(fila  , col++ , rp.getNo_solicitud()==0?"":String.valueOf(rp.getNo_solicitud()), letra  );//SOLICITUD
                    xls.adicionarCelda(fila  , col++ , rp.getCiclo()==0?"":String.valueOf(rp.getCiclo()), letra  );//CICLO
                    xls.adicionarCelda(fila  , col++ , rp.getFechaFacturacion(),letra);//FECHA FACTURACION
                    xls.adicionarCelda(fila  , col++ , rp.getNumaval(), letra  );//AVAL
                    xls.adicionarCelda(fila  , col++ , rp.getCodbco(), letra  );//BANCO
                    xls.adicionarCelda(fila  , col++ , rp.getRefcl(), letra  );//REFERENCIA CL
                    xls.adicionarCelda(fila  , col++ , rp.getCod_cuenta(), numero  );//CUENTA
                    xls.adicionarCelda(fila  , col++ , String.valueOf(rp.getNdias()), numero );//DIAS
                    xls.adicionarCelda(fila  , col++ , rp.getRango(), letra  );//RANGO DE VENCIMIENTO
                    xls.adicionarCelda(fila  , col++ , rp.getValor_factura(), dinero  );//VALOR FACTURA
                    xls.adicionarCelda(fila  , col++ , rp.getVlr_saldo(), dinero  );//VALOR SALDO FACTURA
                    xls.adicionarCelda(fila  , col++ , rp.getVlr_capital(), dinero  );//VALOR CAPITAL
                    xls.adicionarCelda(fila  , col++ , sw ? 0 : rp.getVlr_cuota_manejo(), dinero  );//VALOR CUOTA ADMIN
                    xls.adicionarCelda(fila  , col++ , sw ? 0 : rp.getValor_interes(), dinero  );//VALOR INTERESES
                    xls.adicionarCelda(fila  , col++ , sw ? 0 : rp.getValor_cat(), dinero  );//VALOR CAT
                    xls.adicionarCelda(fila  , col++ , sw ? 0 : rp.getValor_seguro(), dinero  );//VALOR SEGURO
                    xls.adicionarCelda(fila  , col++ , sw ? 0 : rp.getCapital_aval(), dinero  );//VALOR SEGURO
                    xls.adicionarCelda(fila  , col++ , sw ? 0 : rp.getInteres_aval(), dinero  );//VALOR SEGURO
                    xls.adicionarCelda(fila  , col++ , sw ? 0 : rp.getValor_aval(), dinero  );//VALOR SEGURO
                    xls.adicionarCelda(fila  , col++ , sw ? "" : rp.getNegocio(), letra  );//NEGOCIO
                    xls.adicionarCelda(fila  , col++ , rp.getNumero_pagare(), letra  );//NUMERO DE PAGARE                     
                    xls.adicionarCelda(fila  , col++ , rp.getNegocio_rel(), letra  );//NEGOCIO REL
                    xls.adicionarCelda(fila  , col++ , rp.getId_convenio(), letra  );//
                    xls.adicionarCelda(fila  , col++ , rp.getPlazo(), letra  ); //plazo
                    xls.adicionarCelda(fila  , col++ , rp.getTasa(), letra  );//tasa 
                    xls.adicionarCelda(fila  , col++ , rp.getPlan_al_Dia(), letra  );//plan al dia                    
                    xls.adicionarCelda(fila  , col++ , rp.getAgencia(), letra  );// agencia
                    
                                       
                    
//                    xls.adicionarCelda(fila  , col++ , rp.getAgc_duenia(), letra  );
//                    xls.adicionarCelda(fila  , col++ , rp.getAgc_cobro(), letra  );
//                    xls.adicionarCelda(fila  , col++ , rp.getAgcfacturacion(), letra  );
//                    xls.adicionarCelda(fila  , col++ , rp.getCmc(), letra  );
//                    xls.adicionarCelda(fila  , col++ , rp.getMoneda(), letra  );
//                    xls.adicionarCelda(fila  , col++ , rp.getFecha_corte(), letra  );
//                    xls.adicionarCelda(fila  , col++ , rp.getVencimiento(), letraCentrada  );
//                    xls.adicionarCelda(fila  , col++ , rp.getNom_costo_3(), letra  );//VENCIMIENTO FENALCO
//                    xls.adicionarCelda(fila  , col++ , rp.getTipoRef1(), letraCentrada  );
//                    xls.adicionarCelda(fila  , col++ , rp.getRef1(), letra  );
//                    xls.adicionarCelda(fila  , col++ , rp.getTipoRef2(), letraCentrada  );
//                    xls.adicionarCelda(fila  , col++ , rp.getRef2(), letra  );                                      
                    
                    
                    
                    fila++;
                }                        
                
            } else {
                xls.adicionarCelda(fila  ,0, "NO HAY DATOS PARA MOSTRAR", titulo5);
                xls.combinarCeldas(fila , 0, fila, 0+5);
                fila++;                
            }
            
            
            this.cerrarArchivo();
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception("Error en generarArchivo ...\n" + ex.getMessage() );
        }
    }
    
    
    /**
     * Metodo para cerrar el  archivo de excel
     * @autor mfontalvo
     * @throws Exception.
     */
    private void cerrarArchivo() throws Exception {
        try{
            if (xls!=null)
                xls.cerrarLibro();
        }catch (Exception ex){
            throw new Exception( "Error en crearArchivo ....\n"  + ex.getMessage() );
        }
    }
    
    
}

//22 Marzo de 2007

