/********************************************************************
 *  Nombre Clase.................   ReporteRetroactivosTh.java
 *  Descripci�n..................   Hilo que genera el archivo .xls
 *                                  del reporte de retroactivos
 *  Autor........................   Ing. Tito Andr�s Maturana
 *                                  Ing. Leonardo Parodi
 *  Fecha........................   14.12.2005
 *  Versi�n......................   1.0
 *  Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.model.threads;

import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import java.io.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.controller.*;
import com.tsp.operation.model.services.*;
import com.tsp.operation.model.*;
import java.util.*;
import java.text.*;

/**
 *
 * @author  EQUIPO12
 */
public class ReportePlacaNitXLS extends Thread{
    
    private String des;
    private String procesoName;
    Model model = null;
    String agencia;    
    String fechaI;
    String fechaF;
    Usuario usuario;
    String Fecha;
    ReportePlacaFotos placa = new ReportePlacaFotos();
    ReporteConductorFotos conductor = new ReporteConductorFotos();
    
    public void start(String FechaI, String FechaF ,Usuario user , Model m, String Agencia){
        this.procesoName = "Reporte de Relacion de Placa y Conductor con sus fotos";
        this.des = "Realiza Reportes de las fotos subidas";
        this.fechaI = FechaI;
        this.fechaF = FechaF;
        this.model = m;
        this.usuario = user;
        this.agencia = Agencia;
        super.start();
    }
    
     public synchronized void run(){        
        try{            
            model.LogProcesosSvc.InsertProceso(this.procesoName, this.hashCode(), des, usuario.getLogin() );
            Calendar FechaHoy = Calendar.getInstance();
            Date d = FechaHoy.getTime();
            SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd-hh-mm");
            SimpleDateFormat s1 = new SimpleDateFormat("yyyyMMdd_kkmm");
            SimpleDateFormat fec = new SimpleDateFormat("yyyy-MM-dd");
            String FechaFormated = s.format(d);
            String FechaFormated1 = s1.format(d);
            Fecha = fec.format(d);
            //Busca la informacion
            List placas = model.reporteplacanitfotoService.ReportePlacaFoto(fechaI, fechaF, agencia,usuario.getDstrct());
            
            List nits = model.reporteplacanitfotoService.ReporteConductorFoto(fechaI, fechaF, agencia,usuario.getDstrct());
            
            
            
            //obtener cabeera de ruta
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String path = rb.getString("ruta");
            //armas la ruta
            String Ruta1  = path + "/exportar/migracion/" + usuario.getLogin() + "/";
            //crear la ruta
            File file = new File(Ruta1);
            file.mkdirs();
            
            POIWrite xls = new POIWrite(Ruta1 +"ReportePlacaNitFotos_" + FechaFormated1 + ".xls", usuario.getLogin(), Fecha);
            
            HSSFCellStyle fecha  = xls.nuevoEstilo("Book Antiqua", 9, false , false, "yyyy/mm/dd"  , xls.NONE , xls.NONE , xls.NONE );
            HSSFCellStyle texto  = xls.nuevoEstilo("Book Antiqua", 9, false , false, "text"        , xls.NONE , xls.NONE , xls.NONE);
            HSSFCellStyle numero = xls.nuevoEstilo("Book Antiqua", 9, false , false, ""            , xls.NONE , xls.NONE , xls.NONE);
            
            HSSFCellStyle header      = xls.nuevoEstilo("Arial Black", 18, true  , false, "text"        , HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
            HSSFCellStyle titulo      = xls.nuevoEstilo("Book Antiqua", 9, true  , false, "text"        , HSSFColor.BLACK.index , HSSFColor.PINK.index , HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle fechatitle  = xls.nuevoEstilo("Book Antiqua", 9, true  , false, "yyyy/mm/dd"  , HSSFColor.RED.index , xls.NONE, HSSFCellStyle.ALIGN_CENTER );
            
            /************************************
             *      Estilos de Hojas 3 y 4      *
             ************************************/
            
            HSSFCellStyle header2 = xls.nuevoEstilo("Arial", 12, true, false, "text", HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
            HSSFCellStyle header3 = xls.nuevoEstilo("Arial", 8, true, false, "text", HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
            HSSFCellStyle texto2  = xls.nuevoEstilo("Arial", 8, false , false, "text", xls.NONE , xls.NONE , xls.NONE);
            HSSFCellStyle moneda  = xls.nuevoEstilo("Arial", 8, false , false, "\"$ \"#,##0.00", xls.NONE , xls.NONE , xls.NONE);
            HSSFCellStyle headerfecha  = xls.nuevoEstilo("Arial", 9, true, false, "text", HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
            HSSFCellStyle celda = xls.nuevoEstilo("Arial", 9, true, false, "text", HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
            
            celda.setBorderLeft(celda.BORDER_MEDIUM);
            
            header2.setAlignment(HSSFCellStyle.ALIGN_CENTER);
            header3.setAlignment(header3.ALIGN_CENTER);
            texto2.setAlignment(texto2.ALIGN_CENTER);
            moneda.setAlignment(moneda.ALIGN_CENTER);
            
            texto2.setBorderBottom(texto2.BORDER_THIN);
            texto2.setBorderTop(texto2.BORDER_THIN);
            texto2.setBorderRight(texto2.BORDER_THIN);
            texto2.setBorderLeft(texto2.BORDER_THIN);
            
            moneda.setBorderBottom(texto2.BORDER_THIN);
            moneda.setBorderTop(texto2.BORDER_THIN);
            moneda.setBorderRight(texto2.BORDER_THIN);
            moneda.setBorderLeft(texto2.BORDER_THIN);
            
            header3.setBorderBottom(header3.BORDER_MEDIUM);
            header3.setBorderTop(header3.BORDER_MEDIUM);
            header3.setBorderLeft(header3.BORDER_MEDIUM);
            header3.setBorderRight(header3.BORDER_MEDIUM);
            
            /****************************************
             *    FOTOS DE PLACAS Y CONDUCTORES     *
             ****************************************/
            ////System.out.println("Tama�o de la lista placa = "+placas.size());
            xls.obtenerHoja("PLACAS");
            xls.cambiarMagnificacion(4,4);
            
            // cabecera
            
            xls.adicionarCelda(0,0, "TRANSPORTES SANCHEZ POLO S.A.", header2);
            xls.combinarCeldas(0, 0, 0, 2);
            xls.adicionarCelda(1,0, "PLACAS Y FOTOS ASIGNADAS", header2);
            xls.combinarCeldas(1, 0, 0, 2);
            xls.adicionarCelda(2,0, "Fecha Inicial :"   , headerfecha);
            xls.adicionarCelda(2,1, fechaI , fecha);
            xls.adicionarCelda(2,2, "Fecha Final :"   , headerfecha);
            xls.adicionarCelda(2,3, fechaF, fecha);
            xls.adicionarCelda(3,0, "Fecha y hora Proceso :"   , headerfecha);
            xls.adicionarCelda(3,1, FechaFormated1, fecha);
            
            // subtitulos
            
            
            int fila = 6;
            int col  = 0;
            
            xls.adicionarCelda(fila ,col++ , "Agencia"     , header3 );
            xls.adicionarCelda(fila ,col++ , "Placa"     , header3 );
            xls.adicionarCelda(fila ,col++ , "UsuarioCreacion"     , header3 );
            xls.adicionarCelda(fila ,col++ , "marca"      , header3 );
            xls.adicionarCelda(fila ,col++ , "capacidad"     , header3 );
            xls.adicionarCelda(fila ,col++ , "modelo"      , header3 );
            xls.adicionarCelda(fila ,col++ , "FechaCreacion"     , header3 );
            xls.adicionarCelda(fila ,col++ , "Dif_FechaPlaca_Fecha_Foto"     , header3 );
            xls.adicionarCelda(fila ,col++ , "Existe Foto"     , header3 );
            xls.adicionarCelda(fila ,col++ , "filename"     , header3 );
            xls.adicionarCelda(fila ,col++ , "Creation_Date_Foto"     , header3 );
            xls.adicionarCelda(fila ,col++ , "Creation_User_Foto"     , header3 );
            xls.adicionarCelda(fila ,col++ , "Agencia Foto"     , header3 );
            xls.adicionarCelda(fila ,col++ , "propietario"     , header3 );
            xls.adicionarCelda(fila ,col++ , "Agencia_Propietario"     , header3 );
            
            
            // datos
            
            int it = 0;
            while(it < placas.size()){
                fila++;
                col = 0;
                placa = (ReportePlacaFotos)placas.get(it);
                xls.adicionarCelda(fila ,col++ , placa.getAgencia(), texto2 );
                xls.adicionarCelda(fila ,col++ , placa.getPlaca(), texto2);
                xls.adicionarCelda(fila ,col++ , placa.getUsuariocrea(), texto2 );
                xls.adicionarCelda(fila ,col++ , placa.getMarca(), texto2 );
                xls.adicionarCelda(fila ,col++ , placa.getCapacidad(), texto2 );
                xls.adicionarCelda(fila ,col++ , placa.getModelo(), texto2 );
                if (placa.getFechacrea()!= null) {
                        String fecha1 = placa.getFechacrea().toString().replaceAll("-","/");
                        xls.adicionarCelda(fila ,col++ , fecha1.substring(0,10), texto2 );
                }else{
                        xls.adicionarCelda(fila ,col++ , "", texto2 );
                }
                if (placa.getDiferencia()!=null) {
                        String dia = placa.getDiferencia().substring(0, ( placa.getDiferencia().indexOf("days")-1 > 0)? placa.getDiferencia().indexOf("days")-1: 0  ); 
                        xls.adicionarCelda(fila ,col++ , dia, texto2 );
                }else{
                        xls.adicionarCelda(fila ,col++ , "", texto2 );
                }
                xls.adicionarCelda(fila ,col++ , placa.getExiste(), texto2 );
                xls.adicionarCelda(fila ,col++ , placa.getFilename(), texto2 );
                if (placa.getCreation_date()!=null) {
                        String fecha2 = placa.getCreation_date().toString().replaceAll("-","/");
                        xls.adicionarCelda(fila ,col++ , fecha2.substring(0,10), texto2 );
                }else{
                        xls.adicionarCelda(fila ,col++ , "", texto2 );
                }
                xls.adicionarCelda(fila ,col++ , placa.getCreation_user(), texto2 );
                xls.adicionarCelda(fila ,col++ , placa.getAgenciafoto(), texto2 );
                xls.adicionarCelda(fila ,col++ , placa.getPropietario(), texto2 );
                xls.adicionarCelda(fila ,col++ , placa.getAgenciaPropietario(), texto2 );
                
                it++;
                
            }
            
            xls.obtenerHoja("CONDUCTOR");
            xls.cambiarMagnificacion(4,4);
            
            // cabecera
            
            xls.adicionarCelda(0,0, "TRANSPORTES SANCHEZ POLO S.A.", header2);
            xls.combinarCeldas(0, 0, 0, 2);
            xls.adicionarCelda(1,0, "PLACAS Y FOTOS ASIGNADAS", header2);
            xls.combinarCeldas(1, 0, 0, 2);
            xls.adicionarCelda(2,0, "Fecha Inicial :"   , headerfecha);
            xls.adicionarCelda(2,1, fechaI , fecha);
            xls.adicionarCelda(2,2, "Fecha Final :"   , headerfecha);
            xls.adicionarCelda(2,3, fechaF, fecha);
            xls.adicionarCelda(3,0, "Fecha y hora Proceso :"   , headerfecha);
            xls.adicionarCelda(3,1, FechaFormated1, fecha);
            
            // subtitulos
            
            
            fila = 6;
            col  = 0;
            
            xls.adicionarCelda(fila ,col++ , "Agencia"     , header3 );
            xls.adicionarCelda(fila ,col++ , "Cedula"     , header3 );
            xls.adicionarCelda(fila ,col++ , "nombre"      , header3 );
            xls.adicionarCelda(fila ,col++ , "UsuarioCreacion"     , header3 );
            xls.adicionarCelda(fila ,col++ , "FechaCreacion"     , header3 );
            xls.adicionarCelda(fila ,col++ , "Dif_FechaConductor_Fecha_Foto"     , header3 );
            xls.adicionarCelda(fila ,col++ , "Existe Foto"     , header3 );
            xls.adicionarCelda(fila ,col++ , "filename"     , header3 );
            xls.adicionarCelda(fila ,col++ , "Creation_Date_Foto"     , header3 );
            xls.adicionarCelda(fila ,col++ , "Creation_User_Foto"     , header3 );
            xls.adicionarCelda(fila ,col++ , "agencia foto"      , header3 );
            
            
            
            // datos
            
            it = 0;
            while(it < nits.size()){
                fila++;
                col = 0;
                 conductor = (ReporteConductorFotos)nits.get(it);
                xls.adicionarCelda(fila ,col++ , conductor.getAgencia() , texto2 );
                xls.adicionarCelda(fila ,col++ , conductor.getCedula(), texto2);
                xls.adicionarCelda(fila ,col++ , conductor.getNombre(), texto2 );
                xls.adicionarCelda(fila ,col++ , conductor.getUsuariocrea(), texto2 );
                if (conductor.getFechacrea()!= null) {
                        String fecha1 = conductor.getFechacrea().toString().replaceAll("-","/");
                        xls.adicionarCelda(fila ,col++ , fecha1.substring(0,10), texto2 );
                }else{
                        xls.adicionarCelda(fila ,col++ , "", texto2 );
                }
                if (conductor.getDiferencia()!=null) {
                        String dia1 = conductor.getDiferencia().substring(0, ( conductor.getDiferencia().indexOf("days")-1 > 0 )? conductor.getDiferencia().indexOf("days")-1 : 0 ); 
                        xls.adicionarCelda(fila ,col++ , dia1, texto2 );
                }else{
                        xls.adicionarCelda(fila ,col++ , "", texto2 );
                }
                xls.adicionarCelda(fila ,col++ , conductor.getExiste(), texto2 );
                xls.adicionarCelda(fila ,col++ , placa.getFilename(), texto2 );
                if (conductor.getCreation_date()!= null) {
                        String fecha2 = conductor.getCreation_date().toString().replaceAll("-","/");
                        xls.adicionarCelda(fila ,col++ , fecha2.substring(0,10), texto2 );
                }else{
                        xls.adicionarCelda(fila ,col++ , "", texto2 );
                }
                xls.adicionarCelda(fila ,col++ , conductor.getCreation_user(), texto2 );
                xls.adicionarCelda(fila ,col++ , conductor.getAgenciafoto(), texto2 );
                it++;
                
            }
            
            
            
            xls.cerrarLibro();
            model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(),usuario.getLogin(),"PROCESO EXITOSO");
        }catch (Exception ex){
                ex.printStackTrace();
                ////System.out.println("Error : " + ex.getMessage());
                try{
                        model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(),usuario.getLogin(),"ERROR :" + ex.getMessage());
                }
                catch(Exception f){
                        try{                    
                                model.LogProcesosSvc.finallyProceso(this.procesoName,this.hashCode(),usuario.getLogin(),"ERROR :");
                        }catch(Exception p){    }
                }
        }
        
    }
        
}
