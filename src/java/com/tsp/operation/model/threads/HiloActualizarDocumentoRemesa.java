/****************************************************************************
 *  Nombre Clase.................   HiloActualizarDocumentoRemesa.java      *
 *  Descripci�n..................   se encarga de consultar de mims         *
 *                                  ( msf900 y msf900_V ) para actualizar   *
 *                                  el campo documento de la tabla remesa.  *
 *  Autor........................   LREALES                                 *
 *  Fecha........................   21.07.2006                              *
 *  Versi�n......................   1.0                                     *
 *  Copyright....................   Transportes Sanchez Polo S.A.           *
 ****************************************************************************/

package com.tsp.operation.model.threads;

import com.tsp.util.connectionpool.PoolManager;
import java.sql.*;
import java.util.*;
import com.tsp.util.*;
import com.tsp.operation.model.DAOS.OpDAO;
import com.tsp.operation.model.beans.*;
import java.io.*;
import java.lang.*;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import com.tsp.operation.model.beans.POIWrite;
import javax.servlet.*;
import com.tsp.exceptions.*;
import com.tsp.operation.controller.*;
import java.text.*;
import javax.servlet.http.*;
import com.tsp.operation.model.*;
import com.tsp.util.Util;
import java.sql.SQLException;

public class HiloActualizarDocumentoRemesa extends Thread {
    
    private Remesa info;
    
    private Vector vector;
    
    private static final String SQL_SELECT_FECPROCESO = 
        "SELECT " +
            "referencia " +
        "FROM " +
            "tablagen " +
        "WHERE " +
            "table_type = 'FECPROCESO' " +
            "AND table_code = 'ACT_DOC_REM' " ;
       
    private static final String SQL_UPDATE_FECPROCESO = 
        "UPDATE " +
            "tablagen " +
        "SET " +
            "reg_status = '', referencia = ?, last_update = 'now()', user_update = 'ADMIN' " +
        "WHERE " +
            "table_type = 'FECPROCESO' " +
            "AND table_code = 'ACT_DOC_REM' " ;
        
    private static final String SQL_SELECT_MIMS = 
        "SELECT " +
           "B.WORK_ORDER_V REMESA, B.AR_INV_NO_V DOCUMENTO, B.PROCESS_DATE DIAS, A.TRAN_AMOUNT*-1 VALOR_FACT " +
        "FROM " +
            "PRDMOE43.MSF900 A, " +
            "(   SELECT  " +
                    "B.DSTRCT_CODE, B.PROCESS_DATE, B.TRANSACTION_NO, B.REC900_TYPE, B.WORK_ORDER_V, " +
                    "B.AR_INV_NO_V, B.AR_INV_TYPE_V " +
                "FROM " +
                    "PRDMOE43.MSF900_V B " +
                "WHERE " +
                    "B.WORK_ORDER_V <> ' ' " +
                    "AND B.AR_INV_NO_V LIKE 'R%' " +
                    "AND B.AR_INV_TYPE_V = '3' " +
            ") B " +
        "WHERE " +
            "B.DSTRCT_CODE = A.DSTRCT_CODE " +
            "AND B.PROCESS_DATE = A.PROCESS_DATE " +
            "AND B.TRANSACTION_NO = A.TRANSACTION_NO " +
            "AND B.REC900_TYPE = A.REC900_TYPE " +
            "AND SUBSTR ( A.ACCOUNT_CODE, 1, 1 ) = 'I' " +
            "AND SUBSTR ( A.ACCOUNT_CODE, 10, 4 ) = '8005' " +

            "AND ( A.PROCESS_DATE >= ? AND A.PROCESS_DATE <= ? ) " +

            //"AND ROWNUM < 11 " +
            
        "ORDER BY " +
            "B.WORK_ORDER_V " ;
    
    private static final String SQL_EXISTE_REMESA = 
        "SELECT " +
            "* " +
        "FROM " +
            "remesa " +
        "WHERE " +
            "numrem = ? " ;
    
    private static final String SQL_UPDATE_REMESA = 
        "UPDATE " +
            "remesa " +
        "SET " +
            "lastupdate = 'now()', documento = ? " +
        "WHERE " +
            "numrem = ? " ;
    
    /** Creates a new instance of HiloActualizarDocumentoRemesa */
    public HiloActualizarDocumentoRemesa() { 
        
    }
    
    public static void main ( String[] args ) throws Exception {
        
        try{
                       
            HiloActualizarDocumentoRemesa hadr = new HiloActualizarDocumentoRemesa();
            
            hadr.run();  
            
            System.gc();
            
        } catch ( Exception e ){ 
            
            e.printStackTrace();
            throw new Exception( e.getMessage() );
            
        }
        
    }
    
    public void run() {
        
        try{
            
            // Traigo la fecha exacta de la ULTIMA EJECUCION
            String referencia = buscarFecProcesoActDocRem ();
            // Traigo la fecha ACTUAL
            String fecha_actual = Util.fechaActualTIMESTAMP ();            
            // Actualizo la FECHA DEL PROCESO
            actualizarFecProcesoActDocRem ( fecha_actual );
            
            if ( referencia.equals("0099-01-01 00:00:00") ) {
                
                System.gc();
                infoMims ( "1", "" + ( Util.diasTranscurridos( "1980-01-02 00:00:00", "1999-12-31 23:59:59" ) + 1 ) );
                System.gc();
                infoMims ( "" + ( Util.diasTranscurridos( "1980-01-02 00:00:00", "2000-01-01 00:00:00" ) + 1 ), "" + ( Util.diasTranscurridos( "1980-01-02 00:00:00", "2000-12-31 23:59:59" ) + 1 ) );
                System.gc();
                infoMims ( "" + ( Util.diasTranscurridos( "1980-01-02 00:00:00", "2001-01-01 00:00:00" ) + 1 ), "" + ( Util.diasTranscurridos( "1980-01-02 00:00:00", "2001-12-31 23:59:59" ) + 1 ) );
                System.gc();
                infoMims ( "" + ( Util.diasTranscurridos( "1980-01-02 00:00:00", "2002-01-01 00:00:00" ) + 1 ), "" + ( Util.diasTranscurridos( "1980-01-02 00:00:00", "2002-12-31 23:59:59" ) + 1 ) );
                System.gc();
                infoMims ( "" + ( Util.diasTranscurridos( "1980-01-02 00:00:00", "2003-01-01 00:00:00" ) + 1 ), "" + ( Util.diasTranscurridos( "1980-01-02 00:00:00", "2003-12-31 23:59:59" ) + 1 ) );
                System.gc();
                infoMims ( "" + ( Util.diasTranscurridos( "1980-01-02 00:00:00", "2004-01-01 00:00:00" ) + 1 ), "" + ( Util.diasTranscurridos( "1980-01-02 00:00:00", "2004-12-31 23:59:59" ) + 1 ) );
                System.gc();
                infoMims ( "" + ( Util.diasTranscurridos( "1980-01-02 00:00:00", "2005-01-01 00:00:00" ) + 1 ), "" + ( Util.diasTranscurridos( "1980-01-02 00:00:00", "2005-12-31 23:59:59" ) + 1 ) );
                System.gc();
                infoMims ( "" + ( Util.diasTranscurridos( "1980-01-02 00:00:00", "2006-01-01 00:00:00" ) + 1 ), "" + ( Util.diasTranscurridos( "1980-01-02 00:00:00" ) + 1 ) );
                System.gc();
                
            } else {
                
                infoMims ( "" + ( Util.diasTranscurridos( "1980-01-02 00:00:00", referencia ) + 1 ), "" + ( Util.diasTranscurridos( "1980-01-02 00:00:00" ) + 1 ) );
                
            }
                        
        } catch ( Exception e ){
            
            e.printStackTrace ();
            
        }
        
    }
    
    /**
     * Getter for property vector.
     * @return Value of property vector.
     */
    public java.util.Vector getVector() {
        
        return vector;
        
    }
    
    /**
     * Setter for property vector.
     * @param vector New value of property vector.
     */
    public void setVector( java.util.Vector vector ) {
        
        this.vector = vector;
        
    }    
    
    /**
     * Se encarga de buscar la referencia de la fecha del proceso de la 
     * actualizacion de los documentos de la remesa.
     * @autor LREALES
     * @param -
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public String buscarFecProcesoActDocRem () throws Exception {
        
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        String referencia = "";
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( "fintra" );
            
            if( con != null ){
                
                st = con.prepareStatement( this.SQL_SELECT_FECPROCESO );
                
                rs = st.executeQuery();
                
                if ( rs.next() ){
                    
                    referencia = rs.getString( "referencia" ).toUpperCase();
                    
                }
                
            }
                        
        } catch( Exception e ){
            
            e.printStackTrace();
            throw new Exception( "ERROR EN 'buscarFecProcesoActDocRem' - [HiloActualizarDocumentoRemesa].. " + e.getMessage() );
        
        }
        
        finally{
            
            if ( st != null ){
                try{
                    st.close();
                } catch( Exception e ){
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO " + e.getMessage() );
                }
            }            
            if ( con != null ){
                poolManager.freeConnection( "fintra", con );
            }
            
        }
        
        return referencia;
        
    }
    
    /**
     * Se encarga de actualizar la fecha de proceso de la actualizacion
     * de los documentos de la remesa.
     * @autor LREALES
     * @param la fecha_actual.
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void actualizarFecProcesoActDocRem ( String fecha_actual ) throws Exception {
        
        Connection con = null;
        PreparedStatement st = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( "fintra" );
            
            if( con != null ){
                
                st = con.prepareStatement( this.SQL_UPDATE_FECPROCESO );
                
                st.setString ( 1, fecha_actual );
                
                st.executeUpdate();
                                
            }
                        
        } catch( Exception e ){
            
            e.printStackTrace();
            throw new Exception( "ERROR EN 'actualizarFecProcesoActDocRem' - [HiloActualizarDocumentoRemesa].. " + e.getMessage() );
        
        }
        
        finally{
            
            if ( st != null ){
                try{
                    st.close();
                } catch( Exception e ){
                    throw new Exception( "ERROR CERRANDO EL ESTAMENTO " + e.getMessage() );
                }
            }            
            if ( con != null ){
                poolManager.freeConnection( "fintra", con );
            }
            
        }
        
    }
    
    /**
     * Obtiene una lista con los documentos de las remesas de mims.
     * @autor LREALES
     * @param el process_date_inicial y el process_date_actual.
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public void infoMims ( String process_date_inicial, String process_date_actual ) throws Exception{
        
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        String temp_remesa = null;
        String temp_documento = "";
        int temp_dias = 0;
        double temp_valor_fact = 0;
        
        com.tsp.operation.model.Model modelOperation = new com.tsp.operation.model.Model();
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( "oracle" );
            
            if( con != null ){
                
                modelOperation.LogProcesosSvc.InsertProceso("Actualizar Documentos Remesas", this.hashCode(), "Proceso de Actualizacion de los Documentos de las Remesas", "ADMIN");
                
                st = con.prepareStatement( this.SQL_SELECT_MIMS );
                
                st.setString ( 1, process_date_inicial );
                st.setString ( 2, process_date_actual );
                
                rs = st.executeQuery();
                
                this.vector = new Vector();
                
                while ( rs.next() ){
                    
                    String remesa = rs.getString(1)!=null?rs.getString(1).toUpperCase():"";
                    String documento = rs.getString(2)!=null?rs.getString(2).toUpperCase():"";
                    String dias_str = rs.getString(3)!=null?rs.getString(3).toUpperCase():"0";
                    int dias = dias_str.equals( "" )?0:Integer.parseInt( dias_str );
                    String valor_fact_str = rs.getString(4)!=null?rs.getString(4).toUpperCase():"0";
                    double valor_fact = valor_fact_str.equals( "" )?0:Double.parseDouble( valor_fact_str );
                                      
                    info = new Remesa ();
                    
                    info.setNumRem( remesa );
                    info.setDocumento( documento );
                    
                    vector.add( info ); 
                    
                    if ( temp_remesa != null && existeRemesa ( remesa ) ) {                          
                        if ( temp_remesa.equals( remesa ) ){                            
                            valor_fact = valor_fact + temp_valor_fact;
                            if ( temp_dias > dias ){                                
                                dias = temp_dias;
                                documento = temp_documento;
                            }
                        }                 
                    }
                    
                    if ( temp_remesa != null && existeRemesa ( temp_remesa ) ) {                          
                        if ( !temp_remesa.equals( remesa ) ){                            
                            if ( temp_valor_fact > 0 ){                                
                                actualizarRemesa ( info );
                                vector = new Vector();                                
                            }                        
                        }                    
                    }
                    
                    temp_remesa = remesa;
                    temp_documento = documento;
                    temp_dias = dias;
                    temp_valor_fact = valor_fact;
        
                }
                
            }
            
        } catch( Exception e ){
            
            modelOperation.LogProcesosSvc.finallyProceso("Actualizar Documentos Remesas", this.hashCode(),"ADMIN","ERROR : " + e.getMessage() );
            
            e.printStackTrace();
            throw new Exception( "ERROR EN 'infoMims' - [HiloActualizarDocumentoRemesa] " + e.getMessage() );
        
        }
        
        finally{
            
            if ( st != null ){
                try{
                    st.close();
                } catch( Exception e ){
                    throw new Exception( "ERROR CERRANDO EL ESTAMENTO " + e.getMessage() );
                }
            }            
            if ( con != null ){
                poolManager.freeConnection( "oracle", con );
            }
            
            modelOperation.LogProcesosSvc.finallyProceso("Actualizar Documentos Remesas", this.hashCode(), "ADMIN", "PROCESO EXITOSO");
            
        }
        
    }
    
    /**
     * Se encarga de verificar si existe o no la Remesa en la WEB.
     * @autor LREALES
     * @param el numero de la remesa.
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    public boolean existeRemesa ( String remesa ) throws SQLException{
        
        Connection con = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        PoolManager poolManager = null;
        
        boolean sw = false;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( "fintra" );
            
            if( con != null ){
                
                st = con.prepareStatement( this.SQL_EXISTE_REMESA );
                
                st.setString( 1, remesa );         
                
                rs = st.executeQuery();
                
                if ( rs.next() ){
                    
                    sw = true;
                    
                }
                
            }
                        
        } catch( SQLException e ){
            
            e.printStackTrace();
            throw new SQLException( "ERROR EN 'existeRemesa' - [HiloActualizarDocumentoRemesa].. " + e.getMessage() + " " + e.getErrorCode() );
        
        }
        
        finally{
            
            if ( st != null ){
                try{
                    st.close();
                } catch( SQLException e ){
                    throw new SQLException( "ERROR CERRANDO EL ESTAMENTO " + e.getMessage() );
                }
            }            
            if ( con != null ){
                poolManager.freeConnection( "fintra", con );
            }
            
        }
        
        return sw;
        
    }
    
    /**
     * Se encarga de actualizar el campo documento de la tabla remesa.
     * @autor LREALES
     * @param la remesa y el documento.
     * @throws SQLException En caso de que un error de base de datos ocurra.
     * @version 1.0
     */
    //public void actualizarRemesa ( String documento, String remesa ) throws Exception {
     public void actualizarRemesa ( Remesa info ) throws Exception {
        
        Connection con = null;
        PreparedStatement st = null;
        PoolManager poolManager = null;
        
        try {
            
            poolManager = PoolManager.getInstance();
            con = poolManager.getConnection( "fintra" );
            
            if( con != null ){
                
                st = con.prepareStatement( this.SQL_UPDATE_REMESA );
                
                st.setString ( 1, info.getDocumento() );
                st.setString ( 2, info.getNumrem() );                
                                
                st.executeUpdate();
                                
            }
                        
        } catch( Exception e ){
            
            e.printStackTrace();
            throw new Exception( "ERROR EN 'actualizarRemesa' - [HiloActualizarDocumentoRemesa].. " + e.getMessage() );
        
        }
        
        finally{
            
            if ( st != null ){
                try{
                    st.close();
                } catch( Exception e ){
                    throw new Exception( "ERROR CERRANDO EL ESTAMENTO " + e.getMessage() );
                }
            }            
            if ( con != null ){
                poolManager.freeConnection( "fintra", con );
            }
            
        }
        
    }
    
}