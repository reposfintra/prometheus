/**
 * Nombre        HReporteUtilidadPlanillas.java
 * Descripci�n
 * Autor         Mario Fontalvo Solano
 * Fecha         10 de mayo de 2006, 06:00 PM
 * Version       1.0
 * Coyright      Transportes S�nchez Polo S.A.
 **/

package com.tsp.operation.model.threads;


import com.tsp.operation.model.Model;
import com.tsp.operation.model.beans.POIWrite;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.beans.Planilla;
import com.tsp.operation.model.beans.Remesa;
import com.tsp.util.*;


import java.io.*;
import java.util.*;
import java.text.*;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.util.HSSFColor;



public class HReporteUtilidadPlanillas extends Thread {
    
    private Model   model   = null;
    private Usuario usuario = null;
    private String  ano     = "";
    private String  mes     = "";
    
    /* LREALES - CREADO 01 OCTUBRE 2006 */
    private String nombreArchivo = "";
    private String procesoName = "Reporte Utilidad@";
    
    // variables del archivo de excel
    String   ruta;
    POIWrite xls;
    HSSFCellStyle header  , titulo1, titulo2, titulo3 , titulo4, letra, numero, porcentaje, letraCentrada, numeroNegrita;
    HSSFColor     cAzul   , cVerde, cAmarillo, cGris ;
    
    int              fila = 0;
    SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
    public Date      fechaActual = new Date();
    
    
    /** Crea una nueva instancia de  HReporteUtilidadPlanillas */
    public HReporteUtilidadPlanillas() {
    }
    
    public void start(Model model, Usuario usuario, String ano, String mes ){
        this.model   = model;
        this.usuario = usuario;
        this.ano     = ano;
        this.mes     = mes;
        super.start();
    }
    
    public synchronized void run(){
        try{
            
            String fechaInicial = ano + "-" + mes + "-01";
            String fechaFinal   = ano + "-" + mes + "-" + com.tsp.util.UtilFinanzas.diaFinal(ano,mes)  ;   
            
            ////////////////////////////////////////////////////////////////////
            // para generar el nombre del archivo
            Date   fechaActual     = new Date();
            int    periodoActual   = Integer.parseInt(UtilFinanzas.customFormatDate(fechaActual, "yyyyMM"));
            int    periodoReporte  = Integer.parseInt(ano+mes);            
            String diaActual       = UtilFinanzas.customFormatDate(fechaActual, "dd");
            String periodoFinal    = ano+mes;
            if (periodoActual == periodoReporte){
                periodoFinal += diaActual;
            }else{
                periodoFinal += UtilFinanzas.diaFinal(ano,mes);
            }
            
            
            ////////////////////////////////////////////////////////////////////
            nombreArchivo = "ReporteUtilidad_"+ fechaInicial.replaceAll("-","") + "_" + periodoFinal.replaceAll("-","");
           
            
            
            //INSERTO EN EL LOG DE PROCESO
            model.LogProcesosSvc.InsertProceso(this.procesoName, this.hashCode(), "Generando : " + nombreArchivo, usuario.getLogin());            
            // creacion del directorio del usuario
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            ruta = rb.getString("ruta") + "/exportar/migracion/" + usuario.getLogin();
            File archivo = new File( ruta );
            if (!archivo.exists()) archivo.mkdirs();        
            
            
            
            model.PlanillaCostosSvc.obtenerRemesasPorPeriodosDePlanilla(fechaInicial, fechaFinal);
            Vector listado = model.PlanillaCostosSvc.getListaRemesas();
            System.gc();
            
            
            
            if (listado!=null){
                crearArchivo(nombreArchivo, "REPORTE UTILIDAD");
                int total = 0, sec = 0;
                
                int c = 1;
                for (int i=0; i<listado.size(); i++){c++;
                    Remesa r = (Remesa) listado.get(i);
                    
                    model.PlanillaCostosSvc.generarCostos( r );
                    total += r.getPlanillas().size();
                    escribirEnArchivo(r);
                    
                    if (total>40000){
                        sec++;
                        total = 0;
                        cerrarArchivo();
                        System.gc();
                        crearArchivo( nombreArchivo + "_SEC" + sec , "REPORTE UTILIDAD");
                    }
                    //System.out.println("[" + c + "]");
                }
                
                System.gc();
                cerrarArchivo();
            }
            
            model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(), usuario.getLogin(), "Exitoso");
        }catch (Exception ex){
            ex.printStackTrace();
            try{
            model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(), usuario.getLogin(), "Error : " + ex.getMessage());
            } catch (Exception e) {}
        }/* LREALES - 24 AGOSTO 2006 */
        finally{
            
            validar_finalizo = true;
            
            /* LREALES - CREADO 01 OCTUBRE 2006 */
            try {
                
                String login_usuario_rep_control = usuario.getLogin();
                login_usuario_rep_control = login_usuario_rep_control.toUpperCase();
                boolean existe_rep_control = false;
                existe_rep_control = model.PlanillaCostosSvc.existeReporteControl ( login_usuario_rep_control );
                
                if ( existe_rep_control ) {

                    model.PlanillaCostosSvc.actualizarReporteControl( nombreArchivo, login_usuario_rep_control );

                } else {

                    model.PlanillaCostosSvc.insertarReporteControl( login_usuario_rep_control, nombreArchivo );

                }
                
            } catch ( Exception e ) {
            
                e.printStackTrace();
            
            }

        }
    }
    
    
    
    public void InitArchivo(String nameFile) throws Exception{
        try{
                        
            xls          = new com.tsp.operation.model.beans.POIWrite();
            xls.nuevoLibro( ruta + "/" + nameFile + ".xls" );
            
            // colores
            cAzul       = xls.obtenerColor(204,255,255);
            cVerde      = xls.obtenerColor( 51,153,102);
            cAmarillo   = xls.obtenerColor(255,255,153);
            cGris       = xls.obtenerColor(192,192,192);
            
            // estilos
            header       = xls.nuevoEstilo("Tahoma", 14, true  , false, "text"  , HSSFColor.ORANGE.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
            titulo1      = xls.nuevoEstilo("Tahoma", 8 , true  , false, "text"  , xls.NONE  , xls.NONE , xls.NONE);
            titulo2      = xls.nuevoEstilo("Tahoma", 8 , true  , false, "text"  , HSSFColor.BLACK.index, cAzul.getIndex() , HSSFCellStyle.ALIGN_CENTER, 1);
            titulo3      = xls.nuevoEstilo("Tahoma", 8 , true  , false, "text"  , HSSFColor.BLACK.index, cAmarillo.getIndex() , HSSFCellStyle.ALIGN_CENTER, 1);
            titulo4      = xls.nuevoEstilo("Tahoma", 8 , true  , false, "text"  , HSSFColor.WHITE.index, cVerde.getIndex() , HSSFCellStyle.ALIGN_CENTER, 2);
            letra        = xls.nuevoEstilo("Tahoma", 8 , false , false, ""      , xls.NONE , xls.NONE , xls.NONE);
            letraCentrada= xls.nuevoEstilo("Tahoma", 8 , false , false, ""      , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_CENTER);
            numero       = xls.nuevoEstilo("Tahoma", 8 , false , false, "_(#,##0.00_);(@_)"      , xls.NONE , xls.NONE , xls.NONE);
            numeroNegrita= xls.nuevoEstilo("Tahoma", 8 , true  , false, "_($* #,##0.00_);(@_)"      , xls.NONE , xls.NONE , xls.NONE);
            porcentaje   = xls.nuevoEstilo("Tahoma", 8 , false , false, "0.00%" , xls.NONE , xls.NONE , xls.NONE);
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception("Error en InitArchivo .... \n" + ex.getMessage());
        }
    }
    
    
    /**
     * Metodo para crear el  archivo de excel
     * @autor mfontalvo
     * @param nameFile, Nombre del archivo
     * @param titulo, titulo del archivo de excel
     * @throws Exception.
     */
    private void crearArchivo(String nameFile, String titulo) throws Exception{
        try{

            
            InitArchivo(nameFile);
            xls.obtenerHoja("Base");
            xls.combinarCeldas(0, 0, 0, 8);
            xls.combinarCeldas(1, 0, 1, 8);
            xls.combinarCeldas(2, 0, 2, 8);
            xls.combinarCeldas(3, 0, 3, 8);
            xls.adicionarCelda(0, 0, titulo, header);
            xls.adicionarCelda(1, 0, "Usuario " + usuario.getNombre() , titulo1);
            xls.adicionarCelda(2, 0, "Fecha " + fmt.format( new Date() ) , titulo1);
            xls.adicionarCelda(3, 0, "Periodo reporte " + ano+mes , titulo1);
            
            
            int col = 0;
            String [] cabecera = { "REMESA", "ESTANDAR", "DESCRIPCION",  "CODIGO_RUTA_REMESA", "RUTA_REMESA",  
                                   "CODIGO_CLIENTE", "CLIENTE", "CODIGO_AGENCIA", "AGENCIA" ,"ESTADO", "UW", "OT_PADRE", "OT_RELACIONADA",
                                   "TIPO_VIAJE_R", "FECHA_REMESA", "A�O_VENTA", "MES_VENTA", "DIA_VENTA" ,"VENTAS", "VLR_VENTA", "CODIGO_CUENTA_I", "UNIDAD_I" , "CODIGO_CUENTA_C", "UNIDAD_C" ,                                   
                                   
                                   "PLANILLA", "FECHA_PLANILLA", "DESPACHO" ,"PLACA", "TIPO_VIAJE_P" ,"CODIGO_RUTA_PLANILLA", "RUTA_PLANILLA", 
                                   
                                   "VALOR_REMESA", "MONEDAR", "TASAR", "VLR_REMESA" , "ACUM_PLANILLAS" ,
                                   "VALOR_PLANILLA", "MONEDAP", "TASAP", "VLR_PLANILLA", 
                                   "PPP", "VLR_PLANILLA_P", "VLR_VACIO", "PPR", "VLR_REMESA_P", "UTILIDAD"  };
            short [] dimensiones = {
                4000, 4000, 9000, 6000, 10000, 
                4000, 6000, 4000, 6000, 3100, 3100, 4000, 4000, 
                3100, 4000, 4000, 4000, 4000, 4000, 4800, 4800, 4000, 4800, 4000, 
                
                4000, 4000, 4000, 4000, 4000, 6000, 10000 , 
                
                4800, 4000, 4800, 4800, 4800, 
                4800, 4000, 4800, 4800, 
                4800, 4800, 4800, 4800, 4800, 4800
            };            
              
            fila=5;
            for ( int i = 0; i<cabecera.length; i++){
                //if (i == 37 || i == 39)
                if (i == 41 || i == 42 || i == 44)
                    xls.adicionarCelda(fila,  i, cabecera[i], titulo3);
                else
                    xls.adicionarCelda(fila,  i, cabecera[i], titulo2);
                    
                xls.cambiarAnchoColumna(i, dimensiones [i]);
            }
            
            fila++;
            
            
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception( "Error en crearArchivo ....\n"  + ex.getMessage() );
        }
    }
    
    /**
     * Metodo para cerrar el  archivo de excel
     * @autor mfontalvo
     * @throws Exception.
     */
    private void cerrarArchivo() throws Exception {
        try{
            if (xls!=null)
                xls.cerrarLibro();
        }catch (Exception ex){
            throw new Exception( "Error en crearArchivo ....\n"  + ex.getMessage() );
        }
    }
    
    private void escribirEnArchivo(Remesa r) throws Exception{
        System.gc();
        try{
            if (r!=null){
                Vector planillas = r.getPlanillas();
                if (planillas!=null && !planillas.isEmpty()){
                    for (int i = 0; i < planillas.size(); i++){
                        int col = 0;
                        xls.adicionarCelda(fila, col++, r.getNumrem()             , letraCentrada );
                        xls.adicionarCelda(fila, col++, r.getStd_job_no()         , letraCentrada );
                        xls.adicionarCelda(fila, col++, r.getStd_job_desc()       , letra         );
                        xls.adicionarCelda(fila, col++, r.getOrirem() + " - " + r.getDesrem()  , letraCentrada );
                        xls.adicionarCelda(fila, col++, r.getNombreOrigen() + " - " + r.getNombreDestino(), letra );
                        xls.adicionarCelda(fila, col++, r.getCliente()            , letraCentrada );
                        xls.adicionarCelda(fila, col++, r.getNombre_cli()         , letra         );
                        xls.adicionarCelda(fila, col++, r.getPadre()              , letraCentrada );
                        xls.adicionarCelda(fila, col++, r.getNombre_ciu()         , letra         );                        
                        xls.adicionarCelda(fila, col++, r.getEstado()             , letraCentrada );
                        xls.adicionarCelda(fila, col++, r.getUnitOfWork()         , letraCentrada );
                        xls.adicionarCelda(fila, col++, r.getOt_padre()           , letraCentrada );
                        xls.adicionarCelda(fila, col++, r.getOt_rela()            , letraCentrada );
                        xls.adicionarCelda(fila, col++, r.getTipoViaje()          , letraCentrada );
                        xls.adicionarCelda(fila, col++, fmt.format(r.getFecRem()) , letraCentrada );
                        xls.adicionarCelda(fila, col++, UtilFinanzas.customFormatDate(r.getFecRem(), "yyyy") , letraCentrada );
                        xls.adicionarCelda(fila, col++, UtilFinanzas.customFormatDate(r.getFecRem(), "MM")   , letraCentrada );
                        xls.adicionarCelda(fila, col++, UtilFinanzas.customFormatDate(r.getFecRem(), "dd")   , letraCentrada );
                        xls.adicionarCelda(fila, col++, getDescripcionFecha(r.getFecRem()) , letraCentrada );
                        xls.adicionarCelda(fila, col++, (i==0?(r.getValorRemesa() * r.getTasa()):0), numero);
                        
                        
                        // datos de la planilla
                        Planilla p = (Planilla) planillas.get(i);
                        xls.adicionarCelda(fila, col++, p.getAccount_code_i()     , letraCentrada );
                        xls.adicionarCelda(fila, col++, p.getUnidad_i()           , letraCentrada );
                        xls.adicionarCelda(fila, col++, p.getAccount_code_c()     , letraCentrada );
                        xls.adicionarCelda(fila, col++, p.getUnidad_c()           , letraCentrada );  
                        
                        xls.adicionarCelda(fila, col++, p.getNumpla()              , letraCentrada );
                        xls.adicionarCelda(fila, col++, (p.getFechadespacho()!=null?fmt.format( p.getFechadespacho()):"") , letraCentrada );
                        xls.adicionarCelda(fila, col++, getDescripcionFecha( p.getFechadespacho()) , letraCentrada );
                        xls.adicionarCelda(fila, col++, p.getPlaveh()              , letraCentrada );
                        xls.adicionarCelda(fila, col++, p.getTipoviaje()           , letraCentrada );
                        xls.adicionarCelda(fila, col++, p.getOripla() + " - " + p.getDespla()   , letraCentrada );
                        xls.adicionarCelda(fila, col++, p.getNomori() + " - " + p.getNomdest()  , letra         );

                        
                        // VALORES REMESA
                        xls.adicionarCelda(fila, col++, r.getValorRemesa()        , numero        );
                        xls.adicionarCelda(fila, col++, r.getCurrency()           , letraCentrada );
                        xls.adicionarCelda(fila, col++, r.getTasa()               , numero        );
                        xls.adicionarCelda(fila, col++, (r.getValorRemesa() * r.getTasa()), numero);
                        xls.adicionarCelda(fila, col++, r.getValorTotalPlanillas(), numero        );
                        // VALORES PLANILLA
                        xls.adicionarCelda(fila, col++, p.getVlrPlanilla()         , numero        );
                        xls.adicionarCelda(fila, col++, p.getCurrency()            , letraCentrada );
                        xls.adicionarCelda(fila, col++, p.getTasa()                , numero        );
                        xls.adicionarCelda(fila, col++, (p.getVlrPlanilla()*p.getTasa()), numero   );
                        
                        
                        // VALORES UTILIDAD
                        xls.adicionarCelda(fila, col++, (p.getPorcentaje()/100)    , porcentaje    );
                        
                        // planilla
                        //xls.adicionarCelda(fila, col++, p.getVlrParcial()          , numero        );
                        xls.adicionarCelda(fila, col++, (p.getTipoviaje().equals("VAC")?0:p.getVlrParcial()) , numero        );                        
                        xls.adicionarCelda(fila, col++, (p.getTipoviaje().equals("VAC")?  (p.getVlrPlanilla()*p.getTasa() * (p.getPorcentaje()/100) ) :0) , numero        );
                        
                        xls.adicionarCelda(fila, col++, (p.getPorcentajeProrrateo()/100) , porcentaje    );
                        xls.adicionarCelda(fila, col++, p.getVlrProrrateado()      , numero        );
                        
                        // total
                        //xls.adicionarFormula(fila, col++, "AN#r#-AL#r#".replaceAll("#r#",String.valueOf(fila+1)) , numero);
                        xls.adicionarFormula(fila, col++, "AS#r#-AP#r#-AQ#r#".replaceAll("#r#",String.valueOf(fila+1)) , numero);
                        //xls.adicionarCelda(fila, col++, p.getVlrProrrateado() - (p.getVlrParcial()) , numero );
                        fila++;
                         // actualizacion de los campos de utilidad en plarem
                        if (!p.getNumpla().equalsIgnoreCase("ESTIMADA")) {
                            double u_vlrpla     = p.getVlrPlanilla() * p.getTasa();
                            double u_vlrpla_pro = (p.getTipoviaje().equals("VAC")?0:p.getVlrParcial());
                            double u_vlrrem     = r.getValorRemesa() * r.getTasa();
                            double u_vlrrem_pro = p.getVlrProrrateado();
                            model.PlanillaCostosSvc.updatePlarem(r.getNumrem(), p.getNumpla(), u_vlrpla, u_vlrpla_pro, u_vlrrem, u_vlrrem_pro);
                        }

                    }
                }
                else{
                    //System.out.println("REMESA NO PROCESADA : " + r.getNumrem() + ", sin planillas.");
                }
                    
            }
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
        
    }
    
    public String getDescripcionFecha(Date fecha) throws Exception {
        String descripcion = "";
        try{
            if (fecha!=null){
                int f  = Integer.parseInt(UtilFinanzas.customFormatDate(fecha      , "yyyyMM"));
                int periodo = Integer.parseInt(ano+mes);
                if ( f < periodo )
                    descripcion = "ANTERIOR";
                else if ( f > periodo )
                    descripcion = "POSTERIOR";
                else if ( f == periodo )
                    descripcion = "ACTUAL";
            }
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
        return descripcion;
    }
        /**
     * Variable tipo boolean 'validar_finalizo'
     * Descripcion :    Variable que permite verificar si finalizo o no el proceso
     *                  para llevar la informacion a ReporteControlAction.
     * @autor :         LREALES - 24 AGOSTO 2006
     */
    private boolean validar_finalizo = false;





    /**
     * Getter for property validar_finalizo.
     * @return Value of property validar_finalizo.
     * LREALES - 24 AGOSTO 2006
     */
    public boolean isValidar_finalizo() {
        
        return validar_finalizo;
        
    }
    
    /**
     * Setter for property validar_finalizo.
     * @param validar_finalizo New value of property validar_finalizo.
     * LREALES - 24 AGOSTO 2006
     */
    public void setValidar_finalizo( boolean validar_finalizo ) {
        
        this.validar_finalizo = validar_finalizo;
        
    }
    
    public static void main (String []args)throws Exception {
        
        if (args.length==1 && args[0]!=null && args[0].trim().length()>0){
            HReporteUtilidadPlanillas h = new HReporteUtilidadPlanillas();
            String anoActual = UtilFinanzas.customFormatDate(h.fechaActual, "yyyy");
            String mesActual = UtilFinanzas.customFormatDate(h.fechaActual, "MM");
            Usuario u = new Usuario();
            u.setLogin(args[0].toUpperCase());
            u.setNombre("ADMINISTRADOR");
            h.start(new Model(), u, anoActual, mesActual );
        }
        else{
            //System.out.println("Usuario no definido para la generacion del Reporte Utilidad, por favor especifique el valor de este parametro.");
        }
            
            
        
        
    }
    
}
