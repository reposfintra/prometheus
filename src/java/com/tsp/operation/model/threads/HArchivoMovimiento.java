   /***************************************
    * Nombre Clase ............. HArchivoMovimiento.java
    * Descripci�n  .. . . . . .  Permite Generar el archivo txt de movimiento
    * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
    * Fecha . . . . . . . . . .  15/03/2006
    * versi�n . . . . . . . . .  1.0
    * Copyright ...Transportes Sanchez Polo S.A.
    *******************************************/




package com.tsp.operation.model.threads;


import java.io.*;
import java.util.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.util.*;




public class HArchivoMovimiento  extends Thread{
    
    
    private Model         model;
    private String        procesoName;
    private String        usuario;
    
    private  FileWriter      fw;
    private  BufferedWriter  bf;
    private  PrintWriter     linea;
    
    
    
    public HArchivoMovimiento() {}
    
    
    
     public void start(Model modelo, String user) throws Exception{
         try{
            this.model       = modelo;
            this.usuario     = user;
            this.procesoName = "GENERACION ARCHIVO MOVIMIENTO";
            super.start();
        }catch(Exception e){
           throw new Exception( e.getMessage() );
       }
    }
     
     
     
    public void initTxt()throws Exception{
        try{
             model.DirectorioSvc.create(this.usuario );
             
             String filename = "TRANSF " + model.ArchivoMovimientoSvc.getBanco() +"_"+ model.ArchivoMovimientoSvc.getSucursal() +"_"+ model.ArchivoMovimientoSvc.getCorrida()+".txt";
             
             String ruta     = model.DirectorioSvc.getUrl() + this.usuario + "/"+ filename;
         
             this.fw     = new FileWriter    (ruta   );
             this.bf     = new BufferedWriter(this.fw);
             this.linea  = new PrintWriter   (this.bf);
        }catch(Exception e){
            throw new Exception (e.getMessage() );
        }
    }
     
    
    
    public void save(){
        this.linea.close();
    }
    
    
    
    
    
    public synchronized void run(){
       try{
           String comentario="EXITOSO";
           model.LogProcesosSvc.InsertProceso(this.procesoName, this.hashCode(),  model.ArchivoMovimientoSvc.getBanco() +" "+ model.ArchivoMovimientoSvc.getCorrida() ,this.usuario); 
           
           model.ArchivoMovimientoSvc.setIsProceso(true);
           initTxt();
              
               String spaceJsp = "&nbsp";
               String spaceTxt = " ";
              
               List lista = model.ArchivoMovimientoSvc.getListCheques();
               for(int i=0;i<lista.size();i++){
                   ArchivoMovimiento  am = (ArchivoMovimiento) lista.get(i);
                   String  dato = am.getColumnas().replaceAll(spaceJsp,spaceTxt);
                   linea.println( dato );
               }
           
           model.ArchivoMovimientoSvc.insertTransferencia(this.usuario);
           save();
           model.ArchivoMovimientoSvc.setIsProceso(false);
           
           model.LogProcesosSvc.finallyProceso(this.procesoName,this.hashCode(),this.usuario,comentario);
             
       }catch(Exception e){
           try{
                  model.ArchivoMovimientoSvc.setIsProceso(false);
                  model.LogProcesosSvc.finallyProceso(this.procesoName,this.hashCode(),this.usuario,"ERROR Hilo: " + e.getMessage()); }
           catch(Exception f){}   
       }
    }
    
     
     
     
    
}
