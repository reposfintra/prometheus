   /***************************************
    * Nombre Clase ............. HExportarExel_Reporte_de_cumplidos.java
    * Descripci�n  .. . . . . .  Permite Generar archivo con los reportes cumplidos entre las fechas
    * Autor  . . . . . . . . . . JULIO ERNESTO BARROS RUEDA
    * Fecha . . . . . . . . . .  18/12/2006
    * versi�n . . . . . . . . .  1.0
    * Copyright ...Transportes Sanchez Polo S.A.
    *******************************************/




package com.tsp.operation.model.threads;




import com.tsp.util.*;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.Model;
import com.tsp.operation.model.LogProcesosService.*;
import java.util.*;
import java.lang.*;
import java.io.*;



public class HExportarExel_Reporte_de_cumplidos extends Thread {
    
    Model    model;
    Usuario  usuario;
    
    //String   NumPrefactura;
    //Log Procesos
    private String procesoName;
    private String des;
    private String fecha1;
    private String fecha2;
    private Date fec = new Date();
    
    public HExportarExel_Reporte_de_cumplidos() {        
    }
    
    
    public void init(Model model,Usuario usu, String fec1,String fec2){
        
        this.usuario       = usu;
        this.fecha1        = fec1;
        this.fecha2        = fec2;
        this.model         = model;
        this.procesoName = "Reporte Cumplidos";
        this.des = "Reporte Cumplidos";
        super.start();
    }
    
    
    public synchronized void run(){
        try{
            //INSERTO EN EL LOG DE PROCESO
            model.LogProcesosSvc.InsertProceso(this.procesoName, this.hashCode(), this.des, usuario.getLogin());
            
            List lista = model.AnticiposPagosTercerosSvc.getReporte_CumplidosFF(fecha1,fecha2);
            
                ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
                String path = rb.getString("ruta") + "/exportar/migracion";

                String ruta = path + "/" + usuario.getLogin() + "/";

                File f = new File(ruta); 
                if ( !f.exists() ){
                    f.mkdirs();
                }
                String lafecha = Util.getFechaActual_String(6);
                lafecha = lafecha.replaceAll("/", "").replaceAll(":","").replaceAll(" ","_");
                
                com.tsp.operation.model.beans.POIWrite xls = new com.tsp.operation.model.beans.POIWrite( ruta + "/Reporte_Cumplidos"+lafecha+".xls");
                // Definicion de Estilos para la hoja de excel
               
                HSSFCellStyle fecha   = xls.nuevoEstilo("Book Antiqua", 9  , false   , false, "yyyy/mm/dd" , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_CENTER );
                HSSFCellStyle texto   = xls.nuevoEstilo("Book Antiqua", 14 , true    , false, "text"       , xls.NONE , xls.NONE , xls.NONE);
                HSSFCellStyle texto3  = xls.nuevoEstilo("Book Antiqua", 9  , false   , false, "text"       , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_CENTER);
                HSSFCellStyle texto2  = xls.nuevoEstilo("Book Antiqua", 10 , true    , false, "text"       , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_RIGHT);
                HSSFCellStyle total   = xls.nuevoEstilo("Book Antiqua", 10 , true    , false, "text"       , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_LEFT );
                HSSFCellStyle titulo2 = xls.nuevoEstilo("Book Antiqua", 12 , true    , false, "text"       , HSSFColor.WHITE.index , HSSFColor.BLACK.index, HSSFCellStyle.ALIGN_CENTER);
                HSSFCellStyle numero  = xls.nuevoEstilo("Book Antiqua", 9  , false   , false, ""           , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_RIGHT);
                HSSFCellStyle titulo  = xls.nuevoEstilo("Book Antiqua", 9  , true    , false, "text"       , HSSFColor.BLACK.index , HSSFColor.GREY_25_PERCENT.index, HSSFCellStyle.ALIGN_CENTER);

                xls.obtenerHoja("Cumplidos");
                
                int fila = 1;
                int col  = 0;
                
                xls.adicionarCelda(fila++ ,col  , "FINTRA" , texto );
                xls.combinarCeldas(fila, col, fila, col+5); 
                
                xls.adicionarCelda(fila++ ,col   , "Reporte de Cumplidos"   , titulo2 );
                
                xls.adicionarCelda(fila   ,col++ , "Documento"                   , titulo );
                xls.cambiarAnchoColumna(col, 4500);
                xls.adicionarCelda(fila   ,col++ , "Valor Planilla"              , titulo );
                xls.cambiarAnchoColumna(col, 4500);
                xls.adicionarCelda(fila   ,col++ , "Nit Propietario"             , titulo );
                xls.cambiarAnchoColumna(col, 4500);
                xls.adicionarCelda(fila   ,col++ , "nombre"                      , titulo );
                xls.cambiarAnchoColumna(col, 4500);
                xls.adicionarCelda(fila   ,col++ , "direccion"                   , titulo );
                xls.cambiarAnchoColumna(col, 4500);
                xls.adicionarCelda(fila   ,col++ , "Cod Ciudad"                  , titulo );
                xls.cambiarAnchoColumna(col, 4500);
                xls.adicionarCelda(fila   ,col++ , "telefono"                   , titulo );
                xls.cambiarAnchoColumna(col, 4500);
                xls.adicionarCelda(fila   ,col++ , "celular"                    , titulo );
                xls.cambiarAnchoColumna(col, 4500);
                xls.adicionarCelda(fila   ,col++ , "e_mail"                     , titulo );
                xls.cambiarAnchoColumna(col, 4500);
                xls.adicionarCelda(fila   ,col++ , "Usuario Creador"            , titulo );
                xls.cambiarAnchoColumna(col, 4500);
                xls.adicionarCelda(fila   ,col++ , "Fecha de creacion"          , titulo );
                xls.cambiarAnchoColumna(col, 4500);
                xls.adicionarCelda(fila   ,col++ , "id_agencia"                 , titulo );
                xls.cambiarAnchoColumna(col, 4500);
                xls.adicionarCelda(fila   ,col++ , "migrada_a_mims"             , titulo );
                xls.cambiarAnchoColumna(col, 4500);
                
                
                
                Iterator it = lista.iterator();

                //System.out.println("lista "+lista.size());
                
                while(it.hasNext()){
                    fila++;
                    col = 0;
                    Hashtable       Rep  = (Hashtable)      it.next();
                    
                    //System.out.println("fila "+fila+"  col "+col);
                    
                    xls.adicionarCelda  (fila ,col++ , ""+Rep.get("Documento"        ) , texto3 );
                    xls.adicionarCelda  (fila ,col++ , ""+Rep.get("Valor Planilla"   ), texto3 );
                    xls.adicionarCelda  (fila ,col++ , ""+Rep.get("Nit Propietario"  ), texto3 );
                    xls.adicionarCelda  (fila ,col++ , ""+Rep.get("nombre"           ), texto3 );
                    xls.adicionarCelda  (fila ,col++ , ""+Rep.get("direccion"        ), texto3 );
                    xls.adicionarCelda  (fila ,col++ , ""+Rep.get("Cod Ciudad"       ), texto3 );
                    xls.adicionarCelda  (fila ,col++ , ""+Rep.get("telefono"         ), texto3 );
                    xls.adicionarCelda  (fila ,col++ , ""+Rep.get("celular"          ), texto3 );
                    xls.adicionarCelda  (fila ,col++ , ""+Rep.get("e_mail"           ), texto3 );
                    xls.adicionarCelda  (fila ,col++ , ""+Rep.get("Usuario Creador"  ), texto3 );
                    xls.adicionarCelda  (fila ,col++ , ""+Rep.get("Fecha de creacion")   , fecha );
                    xls.adicionarCelda  (fila ,col++ , ""+Rep.get("id_agencia"       ), texto3 );
                    xls.adicionarCelda  (fila ,col++ , ""+Rep.get("migrada_a_mims"   ), texto3 );
                }
                xls.cerrarLibro();
                
             //System.out.println("Listo..");
             model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(), usuario.getLogin(), "PROCESO EXITOSO");
        }catch (Exception ex){            
            //System.out.println("run() Error : " + ex.getMessage());
            ex.printStackTrace();
           try{            
                model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(),usuario.getLogin() ,"ERROR :" + ex.getMessage());
            }
            catch(Exception f){
                try{               
                    model.LogProcesosSvc.finallyProceso(this.procesoName,this.hashCode(),usuario.getLogin(),"ERROR :");
                }catch(Exception p){    }
              } 
           }
        
    }
    
}
