/*
 * QueryToExcelThread.java
 *
 * Created on 6 de agosto de 2005, 02:57 PM
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package com.tsp.operation.model.threads;

import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.util.*;
import java.util.*;

/**
 *
 * @author amendez
 */
public class QueryToExcelThread implements Runnable{
    private Thread thread;
    private Model model;
    private ExcelApplication excel;
    private String path;
    private Hashtable<String, String[]> parameters;
    private List<ConsultaSQL> resultList;
    /**
     * Creates a new instance of QueryToExcelThread 
     */
    public QueryToExcelThread() {
        thread = new Thread(this, "QueryToExcelFile");
    }
    
    public void start(Model model, String path, Map parameters){
        this.model = model;
        this.path = path;
        this.parameters = new Hashtable<String, String[]>(parameters);
        thread.start();
    }
    
    public void join(){
        try{
            thread.join();
        }
        catch(Exception e){
            //System.out.println("ERROR: INTERRUPCION DEL HILO");
        }
    }
    
    public void run () {
        ConsultaSQL sql = null;
        String nombreCampo[] = null;
        String titulo[] = null;
        String descripcion = null;
        String s=null;
        try{
            try{
                model.consultaService.consultasManageSqlExecute(parameters);
                resultList = new LinkedList<ConsultaSQL>(model.consultaService.getActionResultsList());
            } 
            catch(Exception e){
                //System.out.println("OCURRIO EL SIGUIENTE ERROR MIENTRAS SE EJECUTAVA LA CONSULTA EN EL HILO DE EXPORTACION A EXCEL " + e.getMessage());
                e.printStackTrace();
            }
            
            titulo = parameters.get("descripcion");
            descripcion = titulo[0];
            if (descripcion.length() > 20)
                descripcion = descripcion.substring(0, 20).toUpperCase().trim();
            else
                descripcion.toUpperCase().trim();
            
            excel = new ExcelApplication();
            excel.createSheet(descripcion);
            excel.createFont("Titulo", "Arial", (short)1, true, (short)12);
            excel.createFont("Subtitulo", "Arial", (short)0, true, (short)8);
//            excel.createFont("Contenido", "Trebuchet MS", (short)0, false, (short)7);
            excel.createFont("Contenido", "Lucida Sans Typewriter", (short)0, false, (short)8);


            //AMARILLO
            excel.createColor((short)11, (byte)255, (byte)255, (byte)204);  
            //AZUL
            excel.createColor((short)9, (byte)153, (byte)204, (byte)255);
            //NARANJA
            excel.createColor((short)10, (byte)255, (byte)153, (byte)0);

            excel.createStyle("estilo1", excel.getFont("Titulo"), (short)10, true, "@");
            excel.createStyle("estilo2", excel.getFont("Subtitulo"), (short)9, true, "@");
            excel.createStyle("estilo3", excel.getFont("Contenido"), (short)11, true, "@");
            excel.setNumericByDefault(0,0);//new
            excel.setDataCell(0, 0, titulo[0]);
            excel.setCellStyle(0, 0, excel.getStyle("estilo1"));

            Iterator<ConsultaSQL> it = resultList.iterator();
            sql = it.next();
            if (sql != null){
                nombreCampo = sql.getNombresCampos();
                for (int i = 0; i < nombreCampo.length; i++){
                    excel.setNumericByDefault(1,i);//new
                    excel.setDataCell(1, i, nombreCampo[i]);
                    excel.setCellStyle(1, i, excel.getStyle("estilo2"));
                    excel.setNumericByDefault(2,i);//new
                    
                    if(isNumeric(sql.getValorCampo(nombreCampo[i]))){
                        excel.setDataCell(2, i, Integer.parseInt(sql.getValorCampo(nombreCampo[i])));
                        //System.out.println("Es int");
                    }
                    else if(isDouble(sql.getValorCampo(nombreCampo[i]))){
                        excel.setDataCell(2, i, Double.parseDouble(sql.getValorCampo(nombreCampo[i])));
                        //System.out.println("Es double");
                    }
                    else if(isDate(sql.getValorCampo(nombreCampo[i]))){
                        excel.setDataCell(2, i, this.formatoFecha(sql.getValorCampo(nombreCampo[i])));
                        //System.out.println("Es date");
                    }
                    else {
                        excel.setDataCell(2, i, sql.getValorCampo(nombreCampo[i]));
                        //System.out.println("Es string");
                    }
                    //excel.setDataCell(2, i, sql.getValorCampo(nombreCampo[i]));
                    excel.setCellStyle(2, i, excel.getStyle("estilo3"));
                }
            }
            int i = 3;
            while (it.hasNext()){
                sql = it.next();
                if (i > 65530){
                    excel.createSheet("HOJA NUMERO "+(excel.getSheetCount()+1));
                    i=0;
                }
                for (int j = 0; j < sql.getNombresCampos().length; j++){
                    excel.setNumericByDefault(i,j);//new
                    
                    if(isNumeric(sql.getValorCampo(nombreCampo[j]))){
                        excel.setDataCell(i, j, Integer.parseInt(sql.getValorCampo(nombreCampo[j])));
                        //System.out.println("Es int");
                    }
                    else if(isDouble(sql.getValorCampo(nombreCampo[j]))){
                        excel.setDataCell(i, j, Double.parseDouble(sql.getValorCampo(nombreCampo[j])));
                        //System.out.println("Es double");
                    }
                    else if(isDate(sql.getValorCampo(nombreCampo[j]))){
                        excel.setDataCell(i, j, this.formatoFecha(sql.getValorCampo(nombreCampo[j])));
                        //System.out.println("Es date");
                    }
                    else{
                       excel.setDataCell(i, j, sql.getValorCampo(nombreCampo[j]));
                    }
                    //excel.setDataCell(i, j, sql.getValorCampo(nombreCampo[j]));
                    excel.setCellStyle(i, j, excel.getStyle("estilo3"));
                }
                i++;
            }
            //excel.setNumeric();
            excel.saveToFile(path);
        }
        catch(Exception e){
            System.out.println("ERROR DURANTE LA EXPORTACION A EXCEL "+e.toString());
            e.printStackTrace();
        }
    }
    
    private boolean isNumeric(String cad){
     try {
        int i = Integer.parseInt(cad);
        return true;
     }
     catch(Exception ex){
        return false;
     }
   }
   
   private boolean isDouble(String cad){
     try {
        double d = Double.parseDouble(cad);
        return true;
     }
     catch(Exception ex){
        return false;
     }
   }
   
   private boolean isDate(String cad){
       int ind = cad.indexOf(":");
       int r=cad.indexOf("-");
       if(ind==-1 || r==-1) return false;
       else return true;
   }
   
   public String formatoFecha(String fecha){
      String[] s = new String[2];
      String aux="";
      int ind = fecha.indexOf(":");
      int r=fecha.indexOf("-");
      try{  
        
        if(ind==-1 || r==-1) return fecha;
        else{
            s=fecha.split(" ");
            aux=s[0];
            aux=aux.replaceAll("-", "/");
            //System.out.println("cadena "+aux);
            return aux;
        }
      }
      catch(Exception e){
          System.out.println("Error formateando... "+e.toString());
      }
      return aux;
    }
}
