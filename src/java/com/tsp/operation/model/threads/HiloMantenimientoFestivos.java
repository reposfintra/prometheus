
package com.tsp.operation.model.threads;

import java.util.*;
import java.io.*;
import java.lang.*;
import java.sql.*;
import java.text.*;
import java.sql.SQLException;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import com.tsp.operation.model.beans.Festivos;
import com.tsp.operation.model.beans.POIWrite;
import com.tsp.exceptions.*;
import com.tsp.operation.controller.*;
import com.tsp.util.*;
import com.tsp.operation.model.*;
import com.tsp.util.Util;
import javax.servlet.*;
import javax.servlet.http.*;

import com.tsp.util.UtilFinanzas.*;

public class HiloMantenimientoFestivos extends Thread {
        
    private String to;
    private Vector datos;
    private String path;
    private String usuario;
    private String m = "";//
    private String procesoName;
    private String des;
    private Festivos fes;
    private Model model = new Model();
    
    public HiloMantenimientoFestivos() { }
    
    public void start( Vector datos, String usu) {
      
        this.to = "";
        
        this.datos = datos;
        this.procesoName = "Listado Festivos";
        this.des = "Listado Festivos";
        this.usuario = usu;  
        
        super.start();
        
    }
    
    public synchronized void run(){
        
        try{
                        
            //Inicia el proceso
            model.LogProcesosSvc.InsertProceso( procesoName, this.hashCode(), des, usuario );
            
            //FORMATO DATE
            SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            
            Util u = new Util();
            
            ResourceBundle rb = ResourceBundle.getBundle( "com/tsp/util/connectionpool/db" );
            String path = rb.getString( "ruta" );
                        
            File file = new File( path + "/exportar/migracion/" + usuario );
            file.mkdirs();
               
            String fecha = Util.getFechaActual_String(6);            
            fecha=fecha.replaceAll( "/", "-" );
            fecha=fecha.replaceAll( ":", "_" );
            
            String nombreArch  = "ListadoFestivos[" + fecha + "].xls";
            String       Hoja  = "ListadoFestivos";
            String       Ruta  = path + "/exportar/migracion/" + usuario + "/" +nombreArch; 
            
            HSSFWorkbook wb    = new HSSFWorkbook();
            HSSFSheet    sheet = wb.createSheet( Hoja );
            HSSFRow      row   = null;
            HSSFRow      row2  = null;
            HSSFCell     cell  = null;
            
            for ( int col = 0; col < 3 ; col++ ){ //COLUMNAS
                
                sheet.setColumnWidth( (short) col, (short) ( ( 50 * 8 ) / ( (double) 1 / 20 ) ) ); 
                
            }
            
            /****  ENCABEZADO Y DEFINICION DE ESTILOS ************************************************/
            
            /** ENCABEZADO GENERAL *******************************/
            HSSFFont  fuente1 = wb.createFont();
            fuente1.setFontName("verdana");
            fuente1.setFontHeightInPoints((short)(16)) ;
            fuente1.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            fuente1.setColor(HSSFColor.DARK_RED.index);
            
            HSSFCellStyle estilo1 = wb.createCellStyle();
            estilo1.setFont(fuente1);
            estilo1.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo1.setBottomBorderColor(HSSFColor.WHITE.index);
            estilo1.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo1.setLeftBorderColor  (HSSFColor.WHITE.index);
            estilo1.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo1.setRightBorderColor(HSSFColor.WHITE.index);
            estilo1.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo1.setTopBorderColor   (HSSFColor.WHITE.index);
            
            
            /** SUBTITULO *******************************/
            HSSFFont  fuenteX = wb.createFont();
            fuenteX.setFontName("verdana");
            fuenteX.setFontHeightInPoints((short)(11)) ;
            fuenteX.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            fuenteX.setColor(HSSFColor.DARK_RED.index);
            
            HSSFCellStyle estiloX = wb.createCellStyle();
            estiloX.setFont(fuenteX);
            estiloX.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estiloX.setBottomBorderColor(HSSFColor.WHITE.index);
            estiloX.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estiloX.setLeftBorderColor  (HSSFColor.WHITE.index);
            estiloX.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estiloX.setRightBorderColor(HSSFColor.WHITE.index);
            estiloX.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estiloX.setTopBorderColor   (HSSFColor.WHITE.index);
            
            /** TEXTO EN EL ENCABEAZADO *************************/
            HSSFFont  fuente2 = wb.createFont();
            fuente2.setFontName("verdana");
            fuente2.setFontHeightInPoints((short)(11)) ;
            fuente2.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            fuente2.setColor((short)(0x0));
            
            HSSFCellStyle estilo2 = wb.createCellStyle();
            estilo2.setFont(fuente2);
            estilo2.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo2.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            
            /** ENCABEZADO DE LAS COLUMNAS***********************/
            HSSFFont  fuente3 = wb.createFont();
            fuente3.setFontName("verdana");
            fuente3.setFontHeightInPoints((short)(9)) ;
            fuente3.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            fuente3.setColor(HSSFColor.BLACK.index);
            
            HSSFCellStyle estilo3 = wb.createCellStyle();
            estilo3.setFont(fuente3);
            estilo3.setFillForegroundColor(HSSFColor.GREY_25_PERCENT.index);
            estilo3.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo3.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo3.setBottomBorderColor(HSSFColor.BLACK.index);
            estilo3.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo3.setLeftBorderColor  (HSSFColor.BLACK.index);
            estilo3.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo3.setRightBorderColor(HSSFColor.BLACK.index);
            estilo3.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo3.setTopBorderColor   (HSSFColor.BLACK.index);
            estilo3.setAlignment(HSSFCellStyle.ALIGN_CENTER);            
                        
            /** TEXTO NORMAL ************************************/
            HSSFFont  fuente4 = wb.createFont();
            fuente4.setFontName("verdana");
            fuente4.setFontHeightInPoints((short)(9)) ;
            fuente4.setBoldweight(HSSFFont.BOLDWEIGHT_NORMAL);
            fuente4.setColor((short)(0x0));
            
            HSSFCellStyle estilo4 = wb.createCellStyle();
            estilo4.setFont(fuente4);
            estilo4.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo4.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo4.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo4.setBottomBorderColor(HSSFColor.BLACK.index);
            estilo4.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo4.setLeftBorderColor  (HSSFColor.BLACK.index);
            estilo4.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo4.setRightBorderColor(HSSFColor.BLACK.index);
            estilo4.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo4.setTopBorderColor   (HSSFColor.BLACK.index);
            
            /** NUMEROS ************************************/
            HSSFCellStyle estilo5 = wb.createCellStyle();
            estilo5.setFont(fuente4);
            estilo5.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo5.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo5.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo5.setBottomBorderColor(HSSFColor.BLACK.index);
            estilo5.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo5.setLeftBorderColor  (HSSFColor.BLACK.index);
            estilo5.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo5.setRightBorderColor(HSSFColor.BLACK.index);
            estilo5.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo5.setTopBorderColor   (HSSFColor.BLACK.index);
            estilo5.setAlignment(HSSFCellStyle.ALIGN_RIGHT);
            /****************************************************/
            
            HSSFFont fuente6 = wb.createFont();
            fuente6.setColor((short)0x0);  
            fuente6.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            
            HSSFCellStyle estilo6 = wb.createCellStyle();
            estilo6.setFont(fuente6);
            estilo6.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo6.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo6.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo6.setBottomBorderColor(HSSFColor.WHITE.index);
            estilo6.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo6.setLeftBorderColor  (HSSFColor.WHITE.index);
            estilo6.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo6.setRightBorderColor(HSSFColor.WHITE.index);
            estilo6.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo6.setTopBorderColor   (HSSFColor.WHITE.index);
            
            /****************************************************/  
            /* MONEDA */
            HSSFCellStyle estilo7 = wb.createCellStyle();
            estilo7.setFont(fuente4);
            estilo7.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo7.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo7.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo7.setBottomBorderColor(HSSFColor.BLACK.index);
            estilo7.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo7.setLeftBorderColor  (HSSFColor.BLACK.index);
            estilo7.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo7.setRightBorderColor(HSSFColor.BLACK.index);
            estilo7.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo7.setTopBorderColor   (HSSFColor.BLACK.index);
            estilo7.setAlignment(HSSFCellStyle.ALIGN_RIGHT);
            estilo7.setDataFormat(wb.createDataFormat().getFormat("$#,##0.00"));
            
            /* TEXTO NORMAL CENTRADO */
            HSSFCellStyle estilo9 = wb.createCellStyle();
            estilo9.setFont(fuente4);
            estilo9.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo9.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo9.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo9.setBottomBorderColor(HSSFColor.BLACK.index);
            estilo9.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo9.setLeftBorderColor  (HSSFColor.BLACK.index);
            estilo9.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo9.setRightBorderColor(HSSFColor.BLACK.index);
            estilo9.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo9.setTopBorderColor   (HSSFColor.BLACK.index);
            estilo9.setAlignment(HSSFCellStyle.ALIGN_CENTER);
            
            row  = sheet.createRow((short)(0));
            row  = sheet.createRow((short)(1));
            row  = sheet.createRow((short)(2));
            row  = sheet.createRow((short)(3));
            row  = sheet.createRow((short)(4));
            row  = sheet.createRow((short)(5));
            row  = sheet.createRow((short)(6));
            row  = sheet.createRow((short)(7));
            row  = sheet.createRow((short)(8));
            row  = sheet.createRow((short)(9));
            
            for ( int j = 0; j < 2; j++ ) { //COLUMNAS
                
                row  = sheet.getRow((short)(0));
                cell = row.createCell((short)(j));
                cell.setCellStyle(estilo1);
                row  = sheet.getRow((short)(1));
                cell = row.createCell((short)(j)); 
                cell.setCellStyle(estiloX);
                
            }
            
            row  = sheet.getRow((short)(0));
            cell = row.getCell((short)(0));
            cell.setCellValue("TRANSPORTES SANCHEZ POLO");
                        
            row  = sheet.getRow((short)(1));            
            cell = row.getCell((short)(0));            
            cell.setCellValue("Festivos");
            
            for( int i = 2; i < 3; i++ ){ //FILAS
                 
                for ( int j = 0; j < 2; j++ ) { //COLUMNAS
                    
                    row  = sheet.getRow((short)(i));
                    cell = row.createCell((short)(j)); 
                    cell.setCellStyle(estilo6);
                    
                }      
                
            }
            
            
            /*************************************************************************************/
            /***** RECORRER LOS DATOS ******/
            
            int Fila = 3; 
            for ( int t = 0; t < datos.size(); t++ ) {
                
                fes = ( Festivos ) datos.elementAt( t );
                
                Fila++;
                row  = sheet.createRow( ( short )( Fila ) );
                                                
                cell = row.createCell( ( short )( 0 ) );
                cell.setCellStyle( estilo4 );
                cell.setCellValue( fes.getFecha() );
                                
                cell = row.createCell( ( short )( 1 ) );
                cell.setCellStyle( estilo4 );
                cell.setCellValue( fes.getDescripcion() );
                
                cell = row.createCell( ( short )( 2 ) );
                cell.setCellStyle( estilo4 );
                cell.setCellValue( (fes.getPais().equals("VE")?"VENEZUELA":"COLOMBIA") );
                
                
            }
                        
            row = sheet.createRow((short)(3));            
            row = sheet.getRow( (short)(3) );     
            
            cell = row.createCell((short)(0));
            cell.setCellStyle(estilo3);
            cell.setCellValue("FECHA");
            
            cell = row.createCell((short)(1));
            cell.setCellStyle(estilo3);
            cell.setCellValue("DESCRIPCION");
            
            cell = row.createCell((short)(2));
            cell.setCellStyle(estilo3);
            cell.setCellValue("PAIS");
            
            
            
            /******************************************************************/
            /***** GUARDAR DATOS EN EL ARCHIVO *****/
            FileOutputStream fo = new FileOutputStream ( Ruta );
            wb.write ( fo );
            fo.close ();
                    
            //Finalizo el proceso exitosamente
            model.LogProcesosSvc.finallyProceso( procesoName, this.hashCode(), usuario, "PROCESO EXITOSO" );
            
        } catch ( Exception e ) {    
            
            e.printStackTrace();
            
            //Capturo errores finalizando proceso
            try {
                
                model.LogProcesosSvc.finallyProceso( procesoName, this.hashCode(), usuario, "ERROR : " + e.getMessage() );
            
            } catch ( Exception f ) {
                
                f.printStackTrace();
                
                try {
                    
                    model.LogProcesosSvc.finallyProceso( procesoName,this.hashCode(), usuario, "ERROR : " + f.getMessage() );
                
                } catch ( Exception p ) { p.printStackTrace(); }
                
            }

        }
        
    }
    
}