/*
 * POIExcel.java
 *
 * Created on 21 de septiembre de 2005, 04:22 PM
 */


package com.tsp.operation.model.threads;

import java.io.*;
import java.util.*;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;


/**
 *
 * @author  mfontalvo
 */
public class POIWrite {
    
    private String       fileName;
    private HSSFWorkbook wb;
    private HSSFSheet    sheet;
    
    public static final int NONE = -1;

    
    /** Creates a new instance of POIExcel */
    
    
    public POIWrite(String file,String user,String Fecha) throws Exception{
        nuevoLibro(file, user, Fecha);
    }
    
    public POIWrite() {
    }

    
    public void nuevoLibro (String file, String user, String Fecha) throws Exception{
        wb = new HSSFWorkbook();
        this.fileName = file;
        //obtener cabeera de ruta
        ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
        String path = rb.getString("ruta");
        //armas la ruta
        String Ruta  = path + "/exportar/migracion/" + user + "/"+fileName;
        //crear la ruta
        File file1 = new File(Ruta);
        file1.mkdirs();
        
    }
    
    public void cerrarLibro () throws Exception{
        if (wb==null)
            throw new Exception("No se pudo cerrar el libro, primero deber� crear el libro");
        
        FileOutputStream fileOut = new FileOutputStream(fileName);
        wb.write(fileOut);
        fileOut.close(); 
        wb = null;
    }
    
    public void obtenerHoja(String hoja)throws Exception{
        if (wb==null)
            throw new Exception("No se pudo crear la hoja, primero deber� crear el libro");
        
        sheet = wb.getSheet(hoja); 
        if (sheet == null) sheet = wb.createSheet(hoja); 
       
    }    
    
    public HSSFCell obtenerCelda(int fila, int columna)throws Exception{
        if (sheet==null)
            throw new Exception("No se pudo crear la celda, primero deber� crear la hoja");
         
        HSSFRow row  = sheet.getRow(fila);
        if (row==null) row = sheet.createRow( (short) fila);
        
        HSSFCell cell   = row.getCell((short) columna );
        if (cell == null) cell = row.createCell( (short) columna);
        
        return cell;
    }
    
    public void adicionarCelda(int fila, int columna, String value, HSSFCellStyle style)throws Exception{
        if (sheet==null)
            throw new Exception("No se pudo crear la celda, primero deber� crear la hoja");
         
        HSSFRow row   = sheet.createRow( (short) fila);
        HSSFCell cell = row.createCell ( (short) columna);
        
        cell.setCellValue(value);
        
        if (style!=null) cell.setCellStyle(style);
    }
    
    public void adicionarCelda(int fila, int columna, double value, HSSFCellStyle style)throws Exception{
        if (sheet==null)
            throw new Exception("No se pudo crear la celda, primero deber� crear la hoja");
         
        HSSFRow row   = sheet.createRow( (short) fila);
        HSSFCell cell = row.createCell ( (short) columna);
        
        cell.setCellValue(value);
        
        if (style!=null) cell.setCellStyle(style);
    }    
    
    public void adicionarCelda(int fila, int columna, Date value, HSSFCellStyle style)throws Exception{
        if (sheet==null)
            throw new Exception("No se pudo crear la celda, primero deber� crear la hoja");
         
        HSSFRow row   = sheet.createRow( (short) fila);
        HSSFCell cell = row.createCell ( (short) columna);
        
        cell.setCellValue(value);
        
        if (style!=null) cell.setCellStyle(style);
    } 
    
    public void combinarCeldas(int filaInicial, int columnaInicial, int filaFinal , int columnaFinal)throws Exception{
        if (sheet==null)
            throw new Exception("No se pudo crear la celda, primero deber� crear la hoja");
                 
        sheet.addMergedRegion(new Region(filaInicial,(short)columnaInicial ,filaFinal,(short)columnaFinal));
     
    }
    
    public void cambiarMagnificacion (int x, int y)throws Exception{
        if (sheet==null)
            throw new Exception("No se pudo modificar la magnificacion, primero deber� crear la hoja");
                 
        sheet.setZoom(x,y);   
    }
    
    public void cambiarAnchoColumna (int columna, int ancho) throws Exception{
        if (sheet==null)
            throw new Exception("No se pudo cambiar el ancho de la columna, primero deber� crear la hoja");
        sheet.setColumnWidth((short) columna, (short) ancho);
        
    }
    
//    public void cambiarAltoFila (int fila, int alto) throws Exception{
//        if (sheet==null)
//            throw new Exception("No se pudo cambiar alto de la fila, primero deber� crear la hoja");
//        sheet.setRowWidth((short) fila, (short) alto);
//        
//    }     
    
    public void crearPanel (int colSplit, int rowSplit, int leftmostColumn, int topRow)throws Exception{
        if (sheet==null)
            throw new Exception("No se pudo crear el panel, primero deber� crear la hoja");
        sheet.createFreezePane(colSplit, rowSplit , leftmostColumn ,topRow );
    }

    
    public HSSFCellStyle nuevoEstilo (String name, int size, boolean bold, boolean italic , String formato ,int color, int fondo, int align)throws Exception{
        if (wb==null)
            throw new Exception("No se pudo crear la hoja, primero deber� crear el libro");
        
        HSSFCellStyle style   = wb.createCellStyle();
        HSSFFont      font    = wb.createFont();
        
        font.setFontHeightInPoints((short) size);
        font.setFontName(name);                                 
        if (bold)   font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        font.setItalic(italic);
        if (color!=NONE) font.setColor((short) color);
        
        
        style.setFont(font);
        if (!formato.equals(""))
            style.setDataFormat(wb.createDataFormat().getFormat(formato)); 
        
        if (fondo!=NONE) {
            style.setFillPattern((short) style.SOLID_FOREGROUND);
            style.setFillForegroundColor((short)fondo);
        }
        
        if (align!=NONE) style.setAlignment((short)align);
        return style;
        
    }
    
}
