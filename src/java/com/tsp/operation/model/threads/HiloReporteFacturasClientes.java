/******************************************************************************
 * Nombre clase :                   HiloReporteFacturasClientes.java          *
 * Descripcion :                    Clase que maneja los eventos relacionados *
 *                                  con el programa que busca el reporte de   *
 *                                  facturas de un cliente en la BD.          *
 * Autor :                          LREALES                                   *
 * Fecha :                          29 de noviembre de 2006, 08:00 PM         *
 * Version :                        1.0                                       *
 * Copyright :                      Fintravalores S.A.                   *
 *****************************************************************************/

package com.tsp.operation.model.threads;

import java.util.*;
import java.io.*;
import java.lang.*;
import java.sql.*;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.beans.POIWrite;
import javax.servlet.*;
import com.tsp.exceptions.*;
import com.tsp.operation.controller.*;
import java.text.*;
import javax.servlet.http.*;
import com.tsp.util.*;
import com.tsp.operation.model.*;
import com.tsp.util.Util;
import java.sql.SQLException;
import com.tsp.util.UtilFinanzas.*;

public class HiloReporteFacturasClientes extends Thread {
        
    private BeanGeneral bean;
    private Vector datos;
    private String path;
    private String usuario;
    private String m = "";//
    private String procesoName;
    private String des;
    private Model model;
    
    public HiloReporteFacturasClientes() {
        model = new Model();
    }
    public HiloReporteFacturasClientes(Model model) {
        this.model = model;
    }
    
    public void start( Vector datos, String usu ) {
      
        this.datos = datos;
        this.procesoName = "Reporte Facturas Cliente";
        this.des = "Reporte de Facturas de un Cliente";
        this.usuario = usu;  
        
        super.start();
        
    }
    
    public synchronized void run(){
        
        try{
                        
            //Inicia el proceso
            model.LogProcesosSvc.InsertProceso( procesoName, this.hashCode(), des, usuario );
            
            Util u = new Util();
            
            ResourceBundle rb = ResourceBundle.getBundle( "com/tsp/util/connectionpool/db" );
            String path = rb.getString( "ruta" );
                        
            File file = new File( path + "/exportar/migracion/" + usuario );
            file.mkdirs();
               
            String fecha = Util.getFechaActual_String(6);            
            fecha=fecha.replaceAll( "/", "-" );
            fecha=fecha.replaceAll( ":", "_" );
            
            String nombreArch  = "ReporteFacturasClientes[" + fecha + "].xls";
            String       Hoja  = "ReporteFacturasClientes";
            String       Ruta  = path + "/exportar/migracion/" + usuario + "/" +nombreArch; 
            
            HSSFWorkbook wb    = new HSSFWorkbook();
            HSSFSheet    sheet = wb.createSheet( Hoja );
            HSSFRow      row   = null;
            HSSFRow      row2  = null;
            HSSFCell     cell  = null;
            
            for ( int col = 0; col < 13 ; col++ ){ //COLUMNAS
                
                sheet.setColumnWidth( (short) col, (short) ( ( 50 * 8 ) / ( (double) 1 / 20 ) ) ); 
                
            }            
            
            sheet.setColumnWidth( (short)3, (short) ( ( 50 * 8 ) / ( (double) 1 / 10 ) ) );
            sheet.setColumnWidth( (short)4, (short) ( ( 50 * 8 ) / ( (double) 1 / 10 ) ) );
            sheet.setColumnWidth( (short)8, (short) ( ( 50 * 8 ) / ( (double) 1 / 10 ) ) );
            
            sheet.setColumnWidth( (short)0, (short) ( ( 50 * 8 ) / ( (double) 1 / 15 ) ) );
            sheet.setColumnWidth( (short)2, (short) ( ( 50 * 8 ) / ( (double) 1 / 15 ) ) );
            sheet.setColumnWidth( (short)5, (short) ( ( 50 * 8 ) / ( (double) 1 / 15 ) ) );
            sheet.setColumnWidth( (short)6, (short) ( ( 50 * 8 ) / ( (double) 1 / 15 ) ) );
            sheet.setColumnWidth( (short)7, (short) ( ( 50 * 8 ) / ( (double) 1 / 15 ) ) );
            
            sheet.setColumnWidth( (short)12, (short) ( ( 50 * 8 ) / ( (double) 1 / 50 ) ) );
            
            /****  ENCABEZADO Y DEFINICION DE ESTILOS ************************************************/
            
            /** ENCABEZADO GENERAL *******************************/
            HSSFFont  fuente1 = wb.createFont();
            fuente1.setFontName("verdana");
            fuente1.setFontHeightInPoints((short)(16)) ;
            fuente1.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            fuente1.setColor(HSSFColor.DARK_RED.index);
            
            HSSFCellStyle estilo1 = wb.createCellStyle();
            estilo1.setFont(fuente1);
            estilo1.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo1.setBottomBorderColor(HSSFColor.WHITE.index);
            estilo1.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo1.setLeftBorderColor  (HSSFColor.WHITE.index);
            estilo1.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo1.setRightBorderColor(HSSFColor.WHITE.index);
            estilo1.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo1.setTopBorderColor   (HSSFColor.WHITE.index);
            
            
            /** SUBTITULO *******************************/
            HSSFFont  fuenteX = wb.createFont();
            fuenteX.setFontName("verdana");
            fuenteX.setFontHeightInPoints((short)(11)) ;
            fuenteX.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            fuenteX.setColor(HSSFColor.DARK_RED.index);
            
            HSSFCellStyle estiloX = wb.createCellStyle();
            estiloX.setFont(fuenteX);
            estiloX.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estiloX.setBottomBorderColor(HSSFColor.WHITE.index);
            estiloX.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estiloX.setLeftBorderColor  (HSSFColor.WHITE.index);
            estiloX.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estiloX.setRightBorderColor(HSSFColor.WHITE.index);
            estiloX.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estiloX.setTopBorderColor   (HSSFColor.WHITE.index);
            
            /** TEXTO EN EL ENCABEAZADO *************************/
            HSSFFont  fuente2 = wb.createFont();
            fuente2.setFontName("verdana");
            fuente2.setFontHeightInPoints((short)(11)) ;
            fuente2.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            fuente2.setColor((short)(0x0));
            
            HSSFCellStyle estilo2 = wb.createCellStyle();
            estilo2.setFont(fuente2);
            estilo2.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo2.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            
            /** ENCABEZADO DE LAS COLUMNAS***********************/
            HSSFFont  fuente3 = wb.createFont();
            fuente3.setFontName("verdana");
            fuente3.setFontHeightInPoints((short)(9)) ;
            fuente3.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            fuente3.setColor(HSSFColor.BLACK.index);
            
            HSSFCellStyle estilo3 = wb.createCellStyle();
            estilo3.setFont(fuente3);
            estilo3.setFillForegroundColor(HSSFColor.GREY_25_PERCENT.index);
            estilo3.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo3.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo3.setBottomBorderColor(HSSFColor.BLACK.index);
            estilo3.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo3.setLeftBorderColor  (HSSFColor.BLACK.index);
            estilo3.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo3.setRightBorderColor(HSSFColor.BLACK.index);
            estilo3.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo3.setTopBorderColor   (HSSFColor.BLACK.index);
            estilo3.setAlignment(HSSFCellStyle.ALIGN_CENTER);            
                        
            /** TEXTO NORMAL ************************************/
            HSSFFont  fuente4 = wb.createFont();
            fuente4.setFontName("verdana");
            fuente4.setFontHeightInPoints((short)(9)) ;
            fuente4.setBoldweight(HSSFFont.BOLDWEIGHT_NORMAL);
            fuente4.setColor((short)(0x0));
            
            HSSFCellStyle estilo4 = wb.createCellStyle();
            estilo4.setFont(fuente4);
            estilo4.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo4.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo4.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo4.setBottomBorderColor(HSSFColor.BLACK.index);
            estilo4.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo4.setLeftBorderColor  (HSSFColor.BLACK.index);
            estilo4.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo4.setRightBorderColor(HSSFColor.BLACK.index);
            estilo4.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo4.setTopBorderColor   (HSSFColor.BLACK.index);
            
            /** NUMEROS ************************************/
            HSSFCellStyle estilo5 = wb.createCellStyle();
            estilo5.setFont(fuente4);
            estilo5.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo5.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo5.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo5.setBottomBorderColor(HSSFColor.BLACK.index);
            estilo5.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo5.setLeftBorderColor  (HSSFColor.BLACK.index);
            estilo5.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo5.setRightBorderColor(HSSFColor.BLACK.index);
            estilo5.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo5.setTopBorderColor   (HSSFColor.BLACK.index);
            estilo5.setAlignment(HSSFCellStyle.ALIGN_RIGHT);
            /****************************************************/
            
            HSSFFont fuente6 = wb.createFont();
            fuente6.setColor((short)0x0);  
            fuente6.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            
            HSSFCellStyle estilo6 = wb.createCellStyle();
            estilo6.setFont(fuente6);
            estilo6.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo6.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo6.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo6.setBottomBorderColor(HSSFColor.WHITE.index);
            estilo6.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo6.setLeftBorderColor  (HSSFColor.WHITE.index);
            estilo6.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo6.setRightBorderColor(HSSFColor.WHITE.index);
            estilo6.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo6.setTopBorderColor   (HSSFColor.WHITE.index);
            
            /****************************************************/  
            /* MONEDA */
            HSSFCellStyle estilo7 = wb.createCellStyle();
            estilo7.setFont(fuente4);
            estilo7.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo7.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo7.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo7.setBottomBorderColor(HSSFColor.BLACK.index);
            estilo7.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo7.setLeftBorderColor  (HSSFColor.BLACK.index);
            estilo7.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo7.setRightBorderColor(HSSFColor.BLACK.index);
            estilo7.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo7.setTopBorderColor   (HSSFColor.BLACK.index);
            estilo7.setAlignment(HSSFCellStyle.ALIGN_RIGHT);
            estilo7.setDataFormat(wb.createDataFormat().getFormat("$#,##0.00"));
            
            /* TEXTO NORMAL CENTRADO */
            HSSFCellStyle estilo9 = wb.createCellStyle();
            estilo9.setFont(fuente4);
            estilo9.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo9.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo9.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo9.setBottomBorderColor(HSSFColor.BLACK.index);
            estilo9.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo9.setLeftBorderColor  (HSSFColor.BLACK.index);
            estilo9.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo9.setRightBorderColor(HSSFColor.BLACK.index);
            estilo9.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo9.setTopBorderColor   (HSSFColor.BLACK.index);
            estilo9.setAlignment(HSSFCellStyle.ALIGN_CENTER);
            
            row  = sheet.createRow((short)(0));
            row  = sheet.createRow((short)(1));
            row  = sheet.createRow((short)(2));
            row  = sheet.createRow((short)(3));
            row  = sheet.createRow((short)(4));
            row  = sheet.createRow((short)(5));
            row  = sheet.createRow((short)(6));
            row  = sheet.createRow((short)(7));
            row  = sheet.createRow((short)(8));
            row  = sheet.createRow((short)(9));
            
            for ( int j = 0; j < 20; j++ ) { //COLUMNAS
                
                row  = sheet.getRow((short)(0));
                cell = row.createCell((short)(j));
                cell.setCellStyle(estilo1);
                row  = sheet.getRow((short)(1));
                cell = row.createCell((short)(j)); 
                cell.setCellStyle(estiloX);
                
            }
            
            row  = sheet.getRow((short)(0));
            cell = row.getCell((short)(0));
            cell.setCellValue("FINTRAVALORES SA");
                        
            row  = sheet.getRow((short)(1));            
            cell = row.getCell((short)(0));            
            cell.setCellValue("Reporte de Facturas de Clientes");
            
            for( int i = 2; i < 5; i++ ){ //FILAS
                 
                for ( int j = 0; j < 20; j++ ) { //COLUMNAS
                    
                    row  = sheet.getRow((short)(i));
                    cell = row.createCell((short)(j)); 
                    cell.setCellStyle(estilo6);
                    
                }      
                
            }
            
            
            BeanGeneral encabezado = ( BeanGeneral ) datos.elementAt( 0 );
            
            String opcion = encabezado.getValor_18();
            String factura = encabezado.getValor_16();
            String remesa = encabezado.getValor_17();
            String fec_ini = encabezado.getValor_02();
            String fec_fin = encabezado.getValor_03();
            
            if ( opcion.equals("1") ) {
                
                //FECHA
                row = sheet.getRow((short)(3));              
                cell = row.createCell((short)(0));            
                cell = row.getCell((short)(0));            
                cell.setCellStyle(estilo6);
                cell.setCellValue( "Desde: " + fec_ini + " - Hasta: " + fec_fin );
                
            } else if ( opcion.equals("2") ) {
                
                //FACTURA
                row = sheet.getRow((short)(3));              
                cell = row.createCell((short)(0));            
                cell = row.getCell((short)(0));            
                cell.setCellStyle(estilo6);
                cell.setCellValue( "N�mero de la Factura: " + factura );
                
            } else if ( opcion.equals("3") ) {
                
                //REMESA
                row = sheet.getRow((short)(3));              
                cell = row.createCell((short)(0));            
                cell = row.getCell((short)(0));            
                cell.setCellStyle(estilo6);
                cell.setCellValue("N�mero de la Remesa: " + remesa );
                
            }
            
            /*************************************************************************************/
            /***** RECORRER LOS DATOS ******/
            
            int Fila = 5; 
            for ( int t = 0; t < datos.size(); t++ ){
                
                BeanGeneral info = ( BeanGeneral ) datos.elementAt( t );
                
                Fila++;
                row  = sheet.createRow( ( short )( Fila ) );
                                
                cell = row.createCell( ( short )( 0 ) );// CODIGO CLIENTE
                cell.setCellStyle( estilo4 );
                cell.setCellValue( info.getValor_01() );
                                
                cell = row.createCell( ( short )( 1 ) );// NOMBRE CLIENTE
                cell.setCellStyle( estilo4 );
                cell.setCellValue( info.getValor_05() );
                                
                cell = row.createCell( ( short )( 2 ) );// NIT CLIENTE
                cell.setCellStyle( estilo4 );
                cell.setCellValue( info.getValor_04() );
                
                cell = row.createCell( ( short )( 3 ) );// TEL CLIENTE
                cell.setCellStyle( estilo4 );
                cell.setCellValue( info.getValor_27() );
                                
                cell = row.createCell( ( short )( 4 ) );// ESTADO
                cell.setCellStyle( estilo4 );
                cell.setCellValue( info.getValor_06() );
                
                cell = row.createCell( ( short )( 5 ) );// N� FACTURA
                cell.setCellStyle( estilo4 );
                cell.setCellValue( info.getValor_07() );
                
                double vlr_08 = Double.parseDouble( info.getValor_08().equals("")?"0":info.getValor_08() );
                cell = row.createCell( ( short )( 6 ) );// VALOR FACTURA
                cell.setCellStyle( estilo7 );
                cell.setCellValue( vlr_08 );
                
                double vlr_09 = Double.parseDouble( info.getValor_09().equals("")?"0":info.getValor_09() );
                cell = row.createCell( ( short )( 7 ) );// VALOR ABONO
                cell.setCellStyle( estilo7 );
                cell.setCellValue( vlr_09 );
                
                double vlr_10 = Double.parseDouble( info.getValor_10().equals("")?"0":info.getValor_10() );
                cell = row.createCell( ( short )( 8 ) );// VALOR SALDO
                cell.setCellStyle( estilo7 );
                cell.setCellValue( vlr_10 );
                
                cell = row.createCell( ( short )( 9 ) );// MONEDA
                cell.setCellStyle( estilo9 );
                cell.setCellValue( info.getValor_11() );
                
                cell = row.createCell( ( short )( 10 ) );// FECHA DE LA FACTURA
                cell.setCellStyle( estilo9 );
                cell.setCellValue( info.getValor_12() );
                
                cell = row.createCell( ( short )( 11 ) );// FECHA DE VENCIMIENTO
                cell.setCellStyle( estilo9 );
                cell.setCellValue( info.getValor_13() );
                
                cell = row.createCell( ( short )( 12 ) );// FECHA DE ULTIMO PAGO
                cell.setCellStyle( estilo9 );
                cell.setCellValue( info.getValor_14() );
                
                cell = row.createCell( ( short )( 13 ) );// DESCRIPCION
                cell.setCellStyle( estilo4 );
                cell.setCellValue( info.getValor_15() );
                
                
                
                cell = row.createCell( ( short )( 14 ) );// FECHA DE IMPRESION
                cell.setCellStyle( estilo4 );
                cell.setCellValue( info.getValor_21() );
                
                cell = row.createCell( ( short )( 15 ) );// FECHA DE ANULACION
                cell.setCellStyle( estilo4 );
                cell.setCellValue( info.getFecha_anulacion() );
                
                cell = row.createCell( ( short )( 16 ) );// USUARIO ANULO
                cell.setCellStyle( estilo4 );
                cell.setCellValue( info.getUsuario_anulo() );
                
                cell = row.createCell( ( short )( 17 ) );// FECHA DE CONTABILIZACION
                cell.setCellStyle( estilo4 );
                cell.setCellValue( info.getValor_23() );
                
                cell = row.createCell( ( short )( 18 ) );// TRASACCION
                cell.setCellStyle( estilo4 );
                cell.setCellValue( info.getValor_24() );
                
                cell = row.createCell( ( short )( 19 ) );// REMESAS
                cell.setCellStyle( estilo4 );
                cell.setCellValue( info.getOts() );
                
                cell = row.createCell( ( short )( 20 ) );// USUARIO DE CREACION
                cell.setCellStyle( estilo4 );
                cell.setCellValue( info.getCreation_user() );
                
            }
                        
            row = sheet.createRow((short)(5));            
            row = sheet.getRow( (short)(5) );     
            
            cell = row.createCell((short)(0));// CODIGO DEL CLIENTE
            cell.setCellStyle(estilo3);
            cell.setCellValue("CODIGO DEL CLIENTE");
            
            cell = row.createCell((short)(1));// NOMBRE DEL CLIENTE
            cell.setCellStyle(estilo3);
            cell.setCellValue("NOMBRE DEL CLIENTE");
            
            cell = row.createCell((short)(2));// NIT DEL CLIENTE
            cell.setCellStyle(estilo3);
            cell.setCellValue("IDENTIFICACI�N DEL CLIENTE");
            
            cell = row.createCell((short)(3));// TEL DEL CLIENTE
            cell.setCellStyle(estilo3);
            cell.setCellValue("TELEFONO");
            
            cell = row.createCell((short)(4));// ESTADO
            cell.setCellStyle(estilo3);
            cell.setCellValue("ESTADO");
            
            cell = row.createCell((short)(5));// N� FACTURA
            cell.setCellStyle(estilo3);
            cell.setCellValue("N� FACTURA");
            
            cell = row.createCell((short)(6));// VALOR FACTURA
            cell.setCellStyle(estilo3);
            cell.setCellValue("VALOR FACTURA");
            
            cell = row.createCell((short)(7));// VALOR ABONO
            cell.setCellStyle(estilo3);
            cell.setCellValue("VALOR ABONO");
            
            cell = row.createCell((short)(8));// VALOR SALDO
            cell.setCellStyle(estilo3);
            cell.setCellValue("VALOR SALDO");
            
            cell = row.createCell((short)(9));// MONEDA
            cell.setCellStyle(estilo3);
            cell.setCellValue("MONEDA");
            
            cell = row.createCell((short)(10));// FECHA DE LA FACTURA
            cell.setCellStyle(estilo3);
            cell.setCellValue("FECHA DE LA FACTURA");
            
            cell = row.createCell((short)(11));// FECHA DE VENCIMIENTO
            cell.setCellStyle(estilo3);
            cell.setCellValue("FECHA DE VENCIMIENTO");
            
            cell = row.createCell((short)(12));// FECHA DE ULTIMO PAGO
            cell.setCellStyle(estilo3);
            cell.setCellValue("FECHA DE ULTIMO PAGO");
            
            cell = row.createCell((short)(13));// DESCRIPCION
            cell.setCellStyle(estilo3);
            cell.setCellValue("DESCRIPCION");
            
            
            cell = row.createCell((short)(14));// FECHA DE IMPRESION
            cell.setCellStyle(estilo3);
            cell.setCellValue("FECHA DE IMPRESION");
            
            cell = row.createCell((short)(15));// FECHA DE ANULACION
            cell.setCellStyle(estilo3);
            cell.setCellValue("FECHA DE ANULACION");
            
            cell = row.createCell((short)(16));// FECHA DE ANULACION
            cell.setCellStyle(estilo3);
            cell.setCellValue("USUARIO ANULO");
            
            cell = row.createCell((short)(17));// FECHA DE CONTABILIZACION
            cell.setCellStyle(estilo3);
            cell.setCellValue("FECHA DE CONTABILIZACION");
            
            cell = row.createCell((short)(18));// TRASACCION
            cell.setCellStyle(estilo3);
            cell.setCellValue("TRASACCION");
            
            cell = row.createCell((short)(19));// REMESAS
            cell.setCellStyle(estilo3);
            cell.setCellValue("DOCUMENTO RELACIONADO");
            
            cell = row.createCell((short)(20));// REMESAS
            cell.setCellStyle(estilo3);
            cell.setCellValue("USUARIO CREACION");//USUARIO DE CREACION
            
            sheet.setColumnWidth((short) 13, (short) 7000);
            sheet.setColumnWidth((short) 14, (short) 7000);
            sheet.setColumnWidth((short) 15, (short) 5500);
            sheet.setColumnWidth((short) 16, (short) 7500);
            sheet.setColumnWidth((short) 17, (short) 4000);
            sheet.setColumnWidth((short) 18, (short) 8000);
            sheet.setColumnWidth((short) 19, (short) 5500);
            /******************************************************************/
            /***** GUARDAR DATOS EN EL ARCHIVO *****/
            FileOutputStream fo = new FileOutputStream ( Ruta );
            wb.write ( fo );
            fo.close ();
        
            //Finalizo el proceso exitosamente
            model.LogProcesosSvc.finallyProceso( procesoName, this.hashCode(), usuario, "PROCESO EXITOSO" );
            
        } catch( Exception e ){    
            
            e.printStackTrace();
            
            //Capturo errores finalizando proceso
            try{
                
                model.LogProcesosSvc.finallyProceso( procesoName, this.hashCode(), usuario, "ERROR : " + e.getMessage() );
            
            } catch( Exception f ){
                
                f.printStackTrace();
                
                try{
                    
                    model.LogProcesosSvc.finallyProceso( procesoName,this.hashCode(), usuario, "ERROR : " + f.getMessage() );
                
                } catch( Exception p ){ p.printStackTrace(); }
                
            }

        }
        
    }

}
/*************************************
Entregado a Tito 12 febrero de 2007
**************************************/
