/*************************************************************
 * Nombre: ReporteStandarMalosXLS.java
 * Descripci�n: hilo para generar el reporte de standares malos.
 * Autor: Ing. Jose de la rosa
 * Fecha: 5 de abril de 2006, 10:18 AM
 * Versi�n: Java 1.0
 * Copyright: Fintravalores S.A. S.A.
 **************************************************************/

package com.tsp.operation.model.threads;

import java.text.*;
import java.util.Date;
import java.text.*;
import java.sql.SQLException;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.util.*;
import java.util.*;
import java.io.*;

public class ReporteStandarMalosXLS extends Thread{
    private String procesoName;
    private String des;
    private String id="";
    Model model = new Model ();
    
    /** Creates a new instance of ReporteStandarMalosXLS */
    public ReporteStandarMalosXLS () {
    }
    
    public boolean standarMalo (Vector trec, Vector vprio){
        boolean sw = false;
        int T [] = new int[5];
        int C [] = new int[5];
        int O [] = new int[5];
        int cT=0,cC=0,cO=0;
        for ( int l = 0; l < 5; l++ ){
            T[l] = 0;
            C[l] = 0;
            O[l] = 0;
        }
        for ( int i = 0; i < 5; i++ ){
            if( trec.elementAt (i).equals ("T") ){
                T[cT] = Integer.parseInt ( vprio.elementAt (i).toString () );
                cT++;
            }
            else if(trec.elementAt (i).equals ("C")){
                C[cC]= Integer.parseInt ( vprio.elementAt (i).toString () );
                cC++;
            }
            else if(trec.elementAt (i).equals ("O")){
                O[cO] = Integer.parseInt ( vprio.elementAt (i).toString () );
                cO++;
            }
        }
        if ( !verificarPrioridad (T,cT) ){
            sw = true;
        }
        if( !verificarPrioridad (C,cC) ){
            sw = true;
        }
        if( !verificarPrioridad (O,cO) ){
            sw = true;
        }
        return sw;
    }
    
    public boolean verificarPrioridad (int [] Vec, int cant ){
        int i;
        int a [] = new int[5];
        a[0] = 0;
        a[1] = 0;
        a[2] = 0;
        a[3] = 0;
        a[4] = 0;
        boolean sw = true;
        for (i=0;i<5;i++){
            if( Vec[i] >0 ){
                if(Vec[i]<=cant){
                    if (a[Vec[i]]==0){
                        a[Vec[i]] = Vec[i];
                        sw = true;
                    }
                    else{
                        sw = false;
                        break;
                    }
                }
                else{
                    sw = false;
                    break;
                }
            }
            else{
                if(i == 0 && cant > 0)
                    sw = false;
                break;
            }
        }
        return sw;
    }
    
    public void start (String id){
        this.id= id;
        this.procesoName = "Standares Malos";
        this.des = "Reporte para generar los standares malos";
        super.start ();
    }
    
    public synchronized void run (){
        try{           
            this.model.LogProcesosSvc.InsertProceso (this.procesoName, this.hashCode (), des, this.id);
            List lista = (List) model.stdjobService.obtenerStandaresVacios ();
            List lista1 = (List) model.stdjobService.obtenerStandaresLLenos ();
            String fecha_actual = Util.getFechaActual_String (6);
            if (lista!=null && lista.size ()>0 && lista1!=null && lista1.size ()>0){
                ResourceBundle rb = ResourceBundle.getBundle ("com/tsp/util/connectionpool/db");
                String path = rb.getString ("ruta") + "/exportar/migracion/"+id;
                //String path = "c:/Tomcat5/webapps/fintravalores/exportar/migracion/"+id;
                ////System.out.println (" path -> "+path);
                String a�o = fecha_actual.substring (0,4);
                String mes = fecha_actual.substring (5,7);
                String dia = fecha_actual.substring (8,10);
                String hora = fecha_actual.substring (11,13);
                String min = fecha_actual.substring (14,16);
                String seg = fecha_actual.substring (17,19);
                File file = new File (path);
                file.mkdirs ();
                POIWrite xls = new POIWrite (path+"/Standares Inconcistentes "+a�o+mes+dia+".xls","","");
                // fuente, tama�o, negrita, cursiva, formato, color, fondo, alineado
                HSSFCellStyle fecha  = xls.nuevoEstilo ("verdana", 12, false , false, "yyyy/mm/dd"  , xls.NONE , xls.NONE , xls.NONE );
                HSSFCellStyle texto  = xls.nuevoEstilo ("verdana", 12, false , false, "text"        , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_CENTER);
                HSSFCellStyle texto_fondo  = xls.nuevoEstilo ("verdana", 12, false , false, "text"        , xls.NONE , HSSFColor.GREY_25_PERCENT.index , HSSFCellStyle.ALIGN_CENTER);
                HSSFCellStyle numero = xls.nuevoEstilo ("verdana", 12, false , false, ""            , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
                HSSFCellStyle negrita      = xls.nuevoEstilo ("verdana", 12, true  , false, "text"        , HSSFColor.BLACK.index , xls.NONE , xls.NONE);
                HSSFCellStyle header      = xls.nuevoEstilo ("verdana", 18, true  , false, "text"        , HSSFColor.WHITE.index , HSSFColor.ORANGE.index , HSSFCellStyle.ALIGN_CENTER);
                HSSFCellStyle titulo      = xls.nuevoEstilo ("verdana", 12, true  , false, "text"        , HSSFColor.WHITE.index , HSSFColor.LIGHT_BLUE.index , HSSFCellStyle.ALIGN_CENTER);
                HSSFCellStyle fechatitle  = xls.nuevoEstilo ("verdana", 12, true  , false, "yyyy/mm/dd"  , HSSFColor.WHITE.index , HSSFColor.DARK_GREEN.index , HSSFCellStyle.ALIGN_CENTER );
                
                
                xls.obtenerHoja ("BASE");
                xls.cambiarMagnificacion (3,4);
                
                // subtitulos
                
                xls.adicionarCelda (0,0, "Fecha Proceso: "   , negrita);
                xls.adicionarCelda (0,1, fecha_actual , fecha);
                xls.adicionarCelda (1,0, "Elaborado Por: "   , negrita);
                xls.adicionarCelda (1,1, id , fecha);
                
                
                int fila = 3;
                int col  = 0;
                xls.cambiarAnchoColumna (col,5000);
                xls.adicionarCelda (fila ,col++ , "STANDAR JOB"                          , titulo );
                xls.cambiarAnchoColumna (col,20000);
                xls.adicionarCelda (fila ,col++ , "DESCRIPCI�N"                          , titulo );
                xls.cambiarAnchoColumna (col,4000);
                xls.adicionarCelda (fila ,col++ , "RECURSO1"                             , titulo );
                xls.cambiarAnchoColumna (col,4000);
                xls.adicionarCelda (fila ,col++ , "RECURSO2"                             , titulo );
                xls.cambiarAnchoColumna (col,4000);
                xls.adicionarCelda (fila ,col++ , "RECURSO3"                             , titulo );
                xls.cambiarAnchoColumna (col,4000);
                xls.adicionarCelda (fila ,col++ , "RECURSO4"                             , titulo );
                xls.cambiarAnchoColumna (col,4000);
                xls.adicionarCelda (fila ,col++ , "RECURSO5"                             , titulo );
                int j=0;
                //malos con validacion SQL
                Iterator it = lista.iterator ();
                while(it.hasNext ()){
                    fila++;
                    col = 0;
                    StdJob stdjob = (StdJob) it.next ();
                    xls.adicionarCelda (fila ,col++ , stdjob.getstd_job_no ()                                                           , texto );//numero del standar job
                    xls.adicionarCelda (fila ,col++ , stdjob.getstd_job_desc ()                                                         , texto );//descripcion del standar job
                    xls.adicionarCelda (fila ,col++ , stdjob.getTipo_recurso1 ()+"-"+stdjob.getrecurso1 ()+"-"+stdjob.getprioridad1 ()  , texto );//recurso1
                    xls.adicionarCelda (fila ,col++ , stdjob.getTipo_recurso2 ()+"-"+stdjob.getrecurso2 ()+"-"+stdjob.getprioridad2 ()  , texto );//recurso2
                    xls.adicionarCelda (fila ,col++ , stdjob.getTipo_recurso3 ()+"-"+stdjob.getrecurso3 ()+"-"+stdjob.getprioridad3 ()  , texto );//recurso3
                    xls.adicionarCelda (fila ,col++ , stdjob.getTipo_recurso4 ()+"-"+stdjob.getrecurso4 ()+"-"+stdjob.getprioridad4 ()  , texto );//recurso4
                    xls.adicionarCelda (fila ,col++ , stdjob.getTipo_recurso5 ()+"-"+stdjob.getrecurso5 ()+"-"+stdjob.getprioridad5 ()  , texto );//recurso5
                    
                }
                // malos con validacion java
                Iterator it1 = lista1.iterator ();
                while(it1.hasNext ()){
                    boolean sw = false;
                    
                    col = 0;
                    StdJob stdjob = (StdJob) it1.next ();
                    Vector vec1 = new Vector ();
                    vec1.add (0, stdjob.getTipo_recurso1 () );
                    vec1.add (1, stdjob.getTipo_recurso2 () );
                    vec1.add (2, stdjob.getTipo_recurso3 () );
                    vec1.add (3, stdjob.getTipo_recurso4 () );
                    vec1.add (4, stdjob.getTipo_recurso5 () );
                    Vector vec2 = new Vector ();
                    vec2.add (0, stdjob.getprioridad1 () );
                    vec2.add (1, stdjob.getprioridad2 () );
                    vec2.add (2, stdjob.getprioridad3 () );
                    vec2.add (3, stdjob.getprioridad4 () );
                    vec2.add (4, stdjob.getprioridad5 () );
                    if( standarMalo ( vec1, vec2 ) ){
                        sw=true;
                    }
                    if(sw){
                        fila++;
                        xls.adicionarCelda (fila ,col++ , stdjob.getstd_job_no ()                                                           , texto );//numero del standar job
                        xls.adicionarCelda (fila ,col++ , stdjob.getstd_job_desc ()                                                         , texto );//descripcion del standar job
                        xls.adicionarCelda (fila ,col++ , stdjob.getTipo_recurso1 ()+"-"+stdjob.getrecurso1 ()+"-"+stdjob.getprioridad1 ()  , texto );//recurso1
                        xls.adicionarCelda (fila ,col++ , stdjob.getTipo_recurso2 ()+"-"+stdjob.getrecurso2 ()+"-"+stdjob.getprioridad2 ()  , texto );//recurso2
                        xls.adicionarCelda (fila ,col++ , stdjob.getTipo_recurso3 ()+"-"+stdjob.getrecurso3 ()+"-"+stdjob.getprioridad3 ()  , texto );//recurso3
                        xls.adicionarCelda (fila ,col++ , stdjob.getTipo_recurso4 ()+"-"+stdjob.getrecurso4 ()+"-"+stdjob.getprioridad4 ()  , texto );//recurso4
                        xls.adicionarCelda (fila ,col++ , stdjob.getTipo_recurso5 ()+"-"+stdjob.getrecurso5 ()+"-"+stdjob.getprioridad5 ()  , texto );//recurso5
                    }
                }
                ////System.out.println (" cantidad "+ j);
                ////System.out.println ("termino");
                xls.cerrarLibro ();
            }
            model.LogProcesosSvc.finallyProceso (this.procesoName, this.hashCode (), this.id, "PROCESO EXITOSO");
        }
        catch(Exception e){
            try{
                model.LogProcesosSvc.finallyProceso (this.procesoName, this.hashCode (),this.id,"ERROR :" + e.getMessage ());
            }
            catch(Exception f){
                try{             
                    model.LogProcesosSvc.finallyProceso (this.procesoName,this.hashCode (),this.id,"ERROR :");
                }catch(Exception p){    }
            }
        }
    }
    
}
