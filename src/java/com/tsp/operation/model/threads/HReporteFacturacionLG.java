/***************************************************************************
 * Nombre clase : ............... HReporteFacturacionLG.java               *
 * Descripcion :................. Hilo que permite generar un reporte de   *
 *                               facturacion de LG                         *
 * Autor :....................... Ing. Karen Reales                        *
 * Fecha :........................ 29 de Enero de 2007, 08:38   AM         *
 * Version :...................... 1.0                                     *
 * Copyright :.................... Fintravalores S.A.                 *
 ***************************************************************************/

package com.tsp.operation.model.threads;
import java.util.*;
import java.io.*;
import java.text.*;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.*;
import com.tsp.operation.model.*;
import java.sql.*;

public class HReporteFacturacionLG extends Thread{
    
    private Vector reporte;
    private String user;
    Model model;
    
    /** Creates a new instance of ReporteRegistroTiempoXLS */
    public HReporteFacturacionLG() {
    }
    
    public void start( Vector reporte, String user) {
        this.user = user;
        this.reporte = reporte;
        model= new Model();
        super.start();
    }
    
    public synchronized void run(){
        
        try{
            
            model.LogProcesosSvc.InsertProceso("Generacion Reporte Facturacion LG", this.hashCode(), "Inicio de reporte facturacion LG" ,this.user);
            
            //  //System.out.println("Inicio...");
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String path = rb.getString("ruta") + "/exportar/migracion";
            
            String ruta = path + "/" + user + "/";
            
            String nombreArch= "ReporteFacturacionLG.xls";
            String       Hoja  = "Reporte Facturacion";
            String       Ruta  =ruta+ nombreArch;
            
            
            // Definicion de Estilos para la hoja de excel
            com.tsp.operation.model.beans.POIWrite xls = new com.tsp.operation.model.beans.POIWrite( Ruta);
            
            HSSFCellStyle fecha   = xls.nuevoEstilo("Verdana", 9  , false   , false, "yyyy/mm/dd" , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_CENTER );
            HSSFCellStyle texto   = xls.nuevoEstilo("Verdana", 10 , true    , false, "text"       , xls.NONE , xls.NONE , xls.NONE);
            HSSFCellStyle texto3  = xls.nuevoEstilo("Verdana", 9  , false   , false, "text"       , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_CENTER,1);
            HSSFCellStyle texto2  = xls.nuevoEstilo("Verdana", 9 ,  true    , false, "text"       , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_CENTER,1);
            HSSFCellStyle total   = xls.nuevoEstilo("Verdana", 10 , true    , false, "text"       , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_LEFT );
            HSSFCellStyle titulo2 = xls.nuevoEstilo("Verdana", 9  , true    , false, "text"       , HSSFColor.BLACK.index, HSSFColor.YELLOW.index, HSSFCellStyle.ALIGN_CENTER,1);
            HSSFCellStyle numero  = xls.nuevoEstilo("Verdana", 9  , false   , false, ""           , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_RIGHT,1);
            HSSFCellStyle titulo  = xls.nuevoEstilo("Verdana", 9  , true    , false, "text"       , HSSFColor.BLACK.index , HSSFColor.GREY_25_PERCENT.index, HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle textoIzq  = xls.nuevoEstilo("Verdana", 9 ,  false    , false, "text"       , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
            xls.obtenerHoja("Facturacion");
            
            int fila = 1;
            int col  = 0;
            xls.combinarCeldas(fila, col, fila, col+3);
            xls.adicionarCelda(fila++ ,col   , "TRANSPORTE SANCHEZ POLO S.A"             , texto  );
            xls.combinarCeldas(2, col, fila, col+3);
            xls.adicionarCelda(fila++ ,col   , "LG Electronics Colombia Ltda."             , texto  );
            xls.combinarCeldas(3, col, fila, col+3);
            xls.adicionarCelda(fila++ ,col   , "Reporte de facturacion", texto  );
            
            
            
            
            xls.cambiarAnchoColumna(0, 4000);
            xls.cambiarAnchoColumna(1, 4000);
            xls.cambiarAnchoColumna(2, 13000);
            for(int i =3; i<=13;i++){
                xls.cambiarAnchoColumna(i, 6000);
            }
            xls.cambiarAnchoColumna(8, 10000);
            fila++;
            
            xls.adicionarCelda(fila,0 , "DESP" , titulo2 );
            xls.adicionarCelda(fila,1 , "FACTURA LG" , titulo2 );
            xls.adicionarCelda(fila,2 , "CLIENTE" , titulo2 );
            xls.adicionarCelda(fila,3 , "QTY" , titulo2 );
            xls.adicionarCelda(fila,4 , "CUB UNIT" , titulo2 );
            xls.adicionarCelda(fila,5 , "CUB TOTAL" , titulo2 );
            xls.adicionarCelda(fila,6 , "V UNIT" , titulo2 );
            xls.adicionarCelda(fila,7 , "VAL TOTAL" , titulo2 );
            xls.adicionarCelda(fila,8 , "DIRECCION" , titulo2 );
            xls.adicionarCelda(fila,9 , "CIUDAD DESTINO" , titulo2 );
            xls.adicionarCelda(fila,10 , "INST ESPECIAL" , titulo2 );
            xls.adicionarCelda(fila,11 , "REMESA TRANSPORTE" , titulo2 );
            xls.adicionarCelda(fila,12 , "VLR FLETE" , titulo2 );
            xls.adicionarCelda(fila,13 , "FACTURA EXPRESO" , titulo2 );
            
            for(int i =0; i<reporte.size();i++){
                
                BeanGeneral inst = (BeanGeneral) reporte.elementAt(i);
                if(!inst.getValor_27().equals("")){
                    fila++;
                    xls.adicionarCelda(fila,0 ,inst.getValor_29() , numero );
                    xls.adicionarCelda(fila,1 , inst.getValor_37(), texto3 );
                    xls.adicionarCelda(fila,2 , inst.getValor_30() , texto3 );
                    xls.adicionarCelda(fila,3 , inst.getValor_31(), numero );
                    xls.adicionarCelda(fila,4 , inst.getValor_32() , numero );
                    xls.adicionarCelda(fila,5 , inst.getValor_33() , numero );
                    xls.adicionarCelda(fila,6 , "" , texto3 );
                    xls.adicionarCelda(fila,7 , "", texto3 );
                    xls.adicionarCelda(fila,8 , inst.getValor_34(), texto3 );
                    xls.adicionarCelda(fila,9 , inst.getValor_35() , texto3 );
                    xls.adicionarCelda(fila,10 , inst.getValor_36() , numero );
                    xls.adicionarCelda(fila,11 , inst.getValor_14() , texto3 );
                    xls.adicionarCelda(fila,12 ,com.tsp.util.Util.customFormat(Double.parseDouble(inst.getValor_18().equals("")?"0":inst.getValor_18())) , numero );
                    xls.adicionarCelda(fila,13 , inst.getValor_26(), texto3 );
                }
            }
            
            
            
            xls.cerrarLibro();
            model.LogProcesosSvc.finallyProceso( "Generacion Reporte Facturacion LG", this.hashCode(), user, "PROCESO FINALIZADO CON EXITO!" );
        }catch(Exception e){
            //System.out.println("Hay un error: "+e.getMessage());
            
            try{
                
                model.LogProcesosSvc.finallyProceso( "Generacion Reporte Facturacion LG", this.hashCode(), user, "ERROR : " + e.getMessage() );
                
            } catch( Exception f ){
                
                f.printStackTrace();
                
                try{
                    
                    model.LogProcesosSvc.finallyProceso( "Generacion Reporte Facturacion LG",this.hashCode(), user, "ERROR : " + f.getMessage() );
                    
                } catch( Exception p ){ p.printStackTrace(); }
                
            }
        }
    }
    
    public static void main(String a [])throws SQLException{
        HReporteFacturacionLG hilo = new HReporteFacturacionLG();
        Model model = new Model();
        model.instrucciones_despachoService.buscarFactura("FINV", "FAC", "F0000451");
        hilo.start(model.instrucciones_despachoService.getLista() ,"KREALES");
    }
}
