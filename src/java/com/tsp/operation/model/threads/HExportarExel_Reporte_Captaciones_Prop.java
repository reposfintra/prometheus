
   /***************************************
    * Nombre Clase ............. HExportarExel_Reporte_Captaciones_Prop.java
    * Descripci�n  .. . . . . .  Permite Generar archivo con los reportes de las captacioners
    * Autor  . . . . . . . . . . JULIO ERNESTO BARROS RUEDA
    * Fecha . . . . . . . . . .  28/06/2007
    * versi�n . . . . . . . . .  1.0
    * Copyright ... . . . . . .  Fintravalores S.A.
    *******************************************/



package com.tsp.operation.model.threads; 


import com.tsp.util.*;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.Model;
import com.tsp.operation.model.LogProcesosService.*;
import java.util.*;
import java.lang.*;
import java.io.*;



public class HExportarExel_Reporte_Captaciones_Prop extends Thread {
    
    Model    model;
    Usuario  usuario;
    
    //String   NumPrefactura;
    //Log Procesos
    private String procesoName;
    private String des;
    private String fecha1;
    private String fecha2;
    private Date fec = new Date();
    private String nit;
    private String us;
    
    public HExportarExel_Reporte_Captaciones_Prop() {        
    }
    
    
    public void init(Model model,Usuario usu,String us){
        
        this.usuario       = usu;
        
        this.model         = model;
        this.procesoName = "Reporte Captaciones";
        this.des = "Reporte Captaciones";
        this.us=us;
        super.start();
    }
    
    
    public synchronized void run(){
        try{
            //INSERTO EN EL LOG DE PROCESO
                model.LogProcesosSvc.InsertProceso(this.procesoName, this.hashCode(), this.des, usuario.getLogin());
            
                if(us.equals("S"))
                {
                    this.genera_excel_us();
                }
                else
                {
                     this.genera_excel();
                }

               
                
  
   

                
             //System.out.println("Listo..");
             model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(), usuario.getLogin(), "PROCESO EXITOSO");
        }catch (Exception ex){            
            System.out.println("run() Error : " + ex.getMessage());
            ex.printStackTrace();
           try{            
                model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(),usuario.getLogin() ,"ERROR :" + ex.getMessage());
            }
            catch(Exception f){
                f.printStackTrace();
                try{               
                    model.LogProcesosSvc.finallyProceso(this.procesoName,this.hashCode(),usuario.getLogin(),"ERROR :");
                }catch(Exception p){  p.printStackTrace();  }
              } 
           }
        
    }







    public void genera_excel() throws Exception
    {



                ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
                String path = rb.getString("ruta") + "/exportar/migracion";

                String ruta = path + "/" + usuario.getLogin() + "/";

                File f = new File(ruta);
                if ( !f.exists() ){
                    f.mkdirs();
                }
                String lafecha = Util.getFechaActual_String(6);
                lafecha = lafecha.replaceAll("/", "").replaceAll(":","").replaceAll(" ","_");

                com.tsp.operation.model.beans.POIWrite xls = new com.tsp.operation.model.beans.POIWrite( ruta + "/Reporte_Captaciones"+lafecha+".xls");
                // Definicion de Estilos para la hoja de excel

                HSSFCellStyle fecha   = xls.nuevoEstilo("Book Antiqua", 9  , false   , false, "yyyy/mm/dd" , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_CENTER );
                HSSFCellStyle texto   = xls.nuevoEstilo("Book Antiqua", 14 , true    , false, "text"       , xls.NONE , HSSFColor.GREEN.index , HSSFCellStyle.ALIGN_CENTER);
                HSSFCellStyle texto3  = xls.nuevoEstilo("Book Antiqua", 9  , false   , false, "text"       , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_CENTER);
                HSSFCellStyle texto2  = xls.nuevoEstilo("Book Antiqua", 10 , true    , false, "text"       , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_RIGHT);
                HSSFCellStyle total   = xls.nuevoEstilo("Book Antiqua", 10 , true    , false, "text"       , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_LEFT );
                HSSFCellStyle titulo2 = xls.nuevoEstilo("Book Antiqua", 12 , true    , false, "text"       , HSSFColor.WHITE.index , HSSFColor.GREEN.index, HSSFCellStyle.ALIGN_CENTER);
                HSSFCellStyle numero  = xls.nuevoEstilo("Book Antiqua", 9  , false   , false, ""           , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_RIGHT);
                HSSFCellStyle titulo  = xls.nuevoEstilo("Book Antiqua", 9  , true    , false, "text"       , HSSFColor.BLACK.index , HSSFColor.GREY_25_PERCENT.index, HSSFCellStyle.ALIGN_CENTER);



                xls.obtenerHoja("Captaciones ");


                Propietario  Propietario  = new Propietario();

                Vector  lista   = model.CaptacionesFintraSvc.getCaptaciones();
                Propietario     = model.CaptacionesFintraSvc.getPropietario();

                double vlr_captacion   = 0;
                double vlr_descuento  = 0;
                double vlr_neto       = 0;
                double vlr_comision   = 0;
                double vlr_consignar  = 0;
                String fechaAnterior  = "";
                double Naporte   = 0;
                double Reinteg = 0;
                String fechaCorte="";
                double TotAporte = 0;
                double TotReintg = 0;
                double TotalRTFT = 0;
                double TotalRTICA = 0;
                double TotalIntr = 0;
                double Intereses = 0;
                double Retefuente= 0;
                double Reteica= 0;
                double saldoInicial=0;
                int dias=0;



                int fila = 1;
                int col  = 0;

                xls.adicionarCelda(fila++ ,col  , "FINTRA S.A." , texto );
                xls.combinarCeldas(fila, col, fila, col+5);

                xls.adicionarCelda(fila++ ,col   , "Reporte de Captaciones"   , titulo2 );
                xls.combinarCeldas(fila, col, fila, col+5);

                xls.adicionarCelda(fila++ ,col   , Propietario.getP_nombre()   , titulo2 );

                xls.adicionarCelda(fila   ,col++ , "Fecha Inicial"                   , titulo );
                xls.cambiarAnchoColumna(col, 4500);
                xls.adicionarCelda(fila   ,col++ , "Saldo Inicial"              , titulo );
                xls.cambiarAnchoColumna(col, 4500);
                xls.adicionarCelda(fila   ,col++ , "Dias"             , titulo );
                xls.cambiarAnchoColumna(col, 4500);
                xls.adicionarCelda(fila   ,col++ , "%"                      , titulo );
                xls.cambiarAnchoColumna(col, 4500);
                xls.adicionarCelda(fila   ,col++ , "Nuevo Aporte"                   , titulo );
                xls.cambiarAnchoColumna(col, 4500);
                xls.adicionarCelda(fila   ,col++ , "Reintegro"                  , titulo );
                xls.cambiarAnchoColumna(col, 4500);
                xls.adicionarCelda(fila   ,col++ , "Subtotal"                   , titulo );
                xls.cambiarAnchoColumna(col, 4500);
                xls.adicionarCelda(fila   ,col++ , "Valor Intereses"                    , titulo );
                xls.cambiarAnchoColumna(col, 4500);
                xls.adicionarCelda(fila   ,col++ , "Valor Retefuente"                     , titulo );
                xls.cambiarAnchoColumna(col, 4500);

                xls.adicionarCelda(fila   ,col++ , "Valor Reteica"                     , titulo );
                xls.cambiarAnchoColumna(col, 4500);

                xls.adicionarCelda(fila   ,col++ , "Capitalizado"            , titulo );
                xls.cambiarAnchoColumna(col, 4500);
                xls.adicionarCelda(fila   ,col++ , "Saldo Final"          , titulo );
                xls.cambiarAnchoColumna(col, 4500);
                xls.adicionarCelda(fila   ,col++ , "Fecha Final"                 , titulo );
                xls.cambiarAnchoColumna(col, 4500);


                double IntAcum = 0; double ReteAcumula = 0;double ReteicaAcumula = 0; boolean print = true;
                double AporAcum =0; double ReinAcum =0; int dia2 = 0;
                //Iterator it = lista.iterator();

                //System.out.println("lista "+lista.size());

                for(int i=0;i<lista.size();i++){
                    if(print){
                        IntAcum = 0; ReteAcumula = 0;
                        AporAcum =0; ReinAcum =0;
                    }
                    print = true;
                    Captacion  captacion   = (Captacion)lista.get(i);
                    Captacion  captacion2  = new Captacion();
                    saldoInicial      = captacion.getVlr_total_capital() - captacion.getVlr_retefuente() - captacion.getVlr_reteica() ;
                    if(captacion.getTipo_documento().equals("01")||captacion.getTipo_documento().equals("04")){Naporte = captacion.getVlr_capital();} else {Naporte = 0;}
                    if(captacion.getTipo_documento().equals("03")){Reinteg = captacion.getVlr_capital();} else {Reinteg = 0;}
                    if( i < lista.size()-1 ){
                        captacion2  = (Captacion)lista.get(i+1);
                        fechaCorte = com.tsp.util.Util.fechaMenosUnDia(captacion2.getFecha_inicio().substring(0,10)) ;
                        Intereses  = captacion2.getVlr_intereses() ;
                        Retefuente = captacion2.getVlr_retefuente();
                        Reteica = captacion.getVlr_reteica();
                    }else {
                        fechaCorte = com.tsp.util.Util.AnoActual()+"-"+com.tsp.util.Util.getFechaActual_String(3)+"-"+com.tsp.util.Util.getFechaActual_String(5);
                    }
                    dias = com.tsp.util.Util.diasTranscurridos( captacion.getFecha_inicio().substring(0,10) ,fechaCorte.substring(0,10)) + 1 ;
                    if( i == lista.size() -1 ){
                        Intereses  = Math.round(((double)(dias+dia2)/30.0)*((double)captacion.getInteres()/100.0)*(double)(saldoInicial+ Naporte - Reinteg ));
                        Retefuente = Math.round((7.0/100.0)*Intereses);
                        //Reteica = Math.round((0.5/100.0)*Intereses);
                    }
                    
                    
                        if(Propietario.getPerfil().equals("INVERSIONISTA US"))
                          Retefuente  = 0;


                        if(!Propietario.getPerfil().equals("INVERSIONISTA US"))                       
                            Reteica = 0;
                    
                    
                    
                    
                    
                    
                    
                    if( captacion.getFecha_inicio().equals(captacion2.getFecha_inicio())){
                        print = false;
                        IntAcum += Intereses;
                        ReteAcumula += Retefuente;
                        ReteicaAcumula += Reteica;
                        AporAcum += Naporte;
                        ReinAcum += Reinteg;
                        dia2 += dias;
                    }

                    TotalRTFT += Retefuente;
                    TotalRTICA += Reteica;
                    TotalIntr += Intereses;
                    TotAporte += Naporte ;
                    TotReintg += Reinteg ;

                    if(print){
                        Intereses  += IntAcum;
                        Retefuente += ReteAcumula;
                        Reteica    += ReteicaAcumula ;
                        Naporte    += AporAcum;
                        Reinteg    += ReinAcum;
                        //*/

                        fila++;
                        col = 0;


                        xls.adicionarCelda  (fila ,col++ , captacion.getFecha_inicio().substring(0,10) , texto3 );
                        xls.adicionarCelda  (fila ,col++ , Util.customFormat( saldoInicial ), texto3 );
                        xls.adicionarCelda  (fila ,col++ , dias, texto3 );
                        xls.adicionarCelda  (fila ,col++ , captacion.getInteres(), texto3 );
                        xls.adicionarCelda  (fila ,col++ , Util.customFormat( Naporte ), texto3 );
                        xls.adicionarCelda  (fila ,col++ , Util.customFormat( Reinteg ), texto3 );
                        xls.adicionarCelda  (fila ,col++ , Util.customFormat( saldoInicial + Naporte - Reinteg ), texto3 );
                        xls.adicionarCelda  (fila ,col++ , Util.customFormat( Intereses ), texto3 );
                        xls.adicionarCelda  (fila ,col++ , Util.customFormat( Retefuente ), texto3 );
                        xls.adicionarCelda  (fila ,col++ , Util.customFormat( Reteica ), texto3 );
                        xls.adicionarCelda  (fila ,col++ , Util.customFormat( Intereses - Retefuente - Reteica), texto3 );
                        xls.adicionarCelda  (fila ,col++ , Util.customFormat( captacion.getVlr_nuevo_capital() +Intereses - Retefuente -Reteica), fecha );
                        xls.adicionarCelda  (fila ,col++ , fechaCorte.substring(0,10), texto3 );//*/
                    }
                }
                fila+=3;
                col = 0;
                xls.adicionarCelda(fila   ,col++ , "TOTALES"  , titulo );
                xls.cambiarAnchoColumna(col, 4500);
                xls.adicionarCelda(fila   ,col++ , "" , titulo );
                xls.cambiarAnchoColumna(col, 4500);
                xls.adicionarCelda(fila   ,col++ , "" , titulo );
                xls.cambiarAnchoColumna(col, 4500);
                xls.adicionarCelda(fila   ,col++ , "" , titulo );
                xls.cambiarAnchoColumna(col, 4500);
                xls.adicionarCelda(fila   ,col++ , Util.customFormat(TotAporte) , titulo );
                xls.cambiarAnchoColumna(col, 4500);
                xls.adicionarCelda(fila   ,col++ , Util.customFormat(TotReintg) , titulo );
                xls.cambiarAnchoColumna(col, 4500);
                xls.adicionarCelda(fila   ,col++ , ""  , titulo );
                xls.cambiarAnchoColumna(col, 4500);
                xls.adicionarCelda(fila   ,col++ , Util.customFormat(TotalIntr)  , titulo );
                xls.cambiarAnchoColumna(col, 4500);
                xls.adicionarCelda(fila   ,col++ , Util.customFormat(TotalRTFT) , titulo );
                xls.cambiarAnchoColumna(col, 4500);
                 xls.adicionarCelda(fila   ,col++ , Util.customFormat(TotalRTICA) , titulo );
                xls.cambiarAnchoColumna(col, 4500);
                xls.adicionarCelda(fila   ,col++ , Util.customFormat(TotalIntr-TotalRTFT-TotalRTICA) , titulo );
                xls.cambiarAnchoColumna(col, 4500);
                xls.adicionarCelda(fila   ,col++ , Util.customFormat(TotAporte-TotReintg+TotalIntr-TotalRTFT-TotalRTICA) , titulo );
                xls.cambiarAnchoColumna(col, 4500);
                xls.adicionarCelda(fila   ,col++ , "" , titulo );
                xls.cambiarAnchoColumna(col, 4500);


                xls.cerrarLibro();
    }










    /*********************************************excel us*************************************************/

    public void genera_excel_us() throws Exception
    {



         ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
                String path = rb.getString("ruta") + "/exportar/migracion";

                String ruta = path + "/" + usuario.getLogin() + "/";

                File f = new File(ruta);
                if ( !f.exists() ){
                    f.mkdirs();
                }
                String lafecha = Util.getFechaActual_String(6);
                lafecha = lafecha.replaceAll("/", "").replaceAll(":","").replaceAll(" ","_");
                com.tsp.operation.model.beans.POIWrite xls = new com.tsp.operation.model.beans.POIWrite( ruta + "/Reporte_Captaciones"+lafecha+".xls");
                // Definicion de Estilos para la hoja de excel

                HSSFCellStyle fecha   = xls.nuevoEstilo("Book Antiqua", 9  , false   , false, "yyyy/mm/dd" , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_CENTER );
                HSSFCellStyle texto   = xls.nuevoEstilo("Book Antiqua", 14 , true    , false, "text"       , xls.NONE , HSSFColor.GREEN.index , HSSFCellStyle.ALIGN_CENTER);
                HSSFCellStyle texto3  = xls.nuevoEstilo("Book Antiqua", 9  , false   , false, "text"       , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_CENTER);
                HSSFCellStyle texto2  = xls.nuevoEstilo("Book Antiqua", 10 , true    , false, "text"       , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_RIGHT);
                HSSFCellStyle total   = xls.nuevoEstilo("Book Antiqua", 10 , true    , false, "text"       , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_LEFT );
                HSSFCellStyle titulo2 = xls.nuevoEstilo("Book Antiqua", 12 , true    , false, "text"       , HSSFColor.WHITE.index , HSSFColor.GREEN.index, HSSFCellStyle.ALIGN_CENTER);
                HSSFCellStyle numero  = xls.nuevoEstilo("Book Antiqua", 9  , false   , false, ""           , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_RIGHT);
                HSSFCellStyle titulo  = xls.nuevoEstilo("Book Antiqua", 9  , true    , false, "text"       , HSSFColor.BLACK.index , HSSFColor.GREY_25_PERCENT.index, HSSFCellStyle.ALIGN_CENTER);



                xls.obtenerHoja("Captaciones ");


                Propietario  Propietario  = new Propietario();

                        Vector  lista   = model.CaptacionesFintraSvc.getCaptaciones();
                Propietario     = model.CaptacionesFintraSvc.getPropietario();



                /*-----------------------------------------------------------------------*/



                double vlr_captacion   = 0;
                double vlr_descuento  = 0;
                double vlr_neto       = 0;
                double vlr_comision   = 0;
                double vlr_consignar  = 0;
                String fechaAnterior  = "";
                double Naporte   = 0;
                double Reinteg = 0;
                String fechaCorte="";
                double TotAporte = 0;
                double TotReintg = 0;
                double TotalRTFT = 0;
                double TotalIntr = 0;
                double Intereses = 0;
                double Retefuente= 0;
                double saldoInicial=0;
                int dias=0;



                int fila = 1;
                int col  = 0;

                xls.adicionarCelda(fila++ ,col  , "FINTRA S.A." , texto );
                xls.combinarCeldas(fila, col, fila, col+5);

                xls.adicionarCelda(fila++ ,col   , "Reporte de Captaciones"   , titulo2 );
                xls.combinarCeldas(fila, col, fila, col+5);

                xls.adicionarCelda(fila++ ,col   , Propietario.getP_nombre()   , titulo2 );

                xls.adicionarCelda(fila   ,col++ , "Fecha Inicial"                   , titulo );
                xls.cambiarAnchoColumna(col, 4500);
                xls.adicionarCelda(fila   ,col++ , "Saldo Inicial"              , titulo );
                xls.cambiarAnchoColumna(col, 4500);
                xls.adicionarCelda(fila   ,col++ , "Dias"             , titulo );
                xls.cambiarAnchoColumna(col, 4500);                       
                xls.adicionarCelda(fila   ,col++ , "Nuevo Aporte"                   , titulo );
                xls.cambiarAnchoColumna(col, 4500);
                xls.adicionarCelda(fila   ,col++ , "Retiro"                  , titulo );
                xls.cambiarAnchoColumna(col, 4500);
                xls.adicionarCelda(fila   ,col++ , "Subtotal"                   , titulo );
                xls.cambiarAnchoColumna(col, 4500);
                xls.adicionarCelda(fila   ,col++ , "Valor Intereses"                    , titulo );
                xls.cambiarAnchoColumna(col, 4500);
                xls.adicionarCelda(fila   ,col++ , "Capitalizado"            , titulo );
                xls.cambiarAnchoColumna(col, 4500);
                xls.adicionarCelda(fila   ,col++ , "Saldo Final"          , titulo );
                xls.cambiarAnchoColumna(col, 4500);
                xls.adicionarCelda(fila   ,col++ , "Fecha Final"                 , titulo );
                xls.cambiarAnchoColumna(col, 4500);


                double IntAcum = 0; double ReteAcumula = 0; boolean print = true;
                double AporAcum =0; double ReinAcum =0; int dia2 = 0;
                //Iterator it = lista.iterator();

                //System.out.println("lista "+lista.size());

                for(int i=0;i<lista.size();i++){
                    if(print){
                        IntAcum = 0; ReteAcumula = 0;
                        AporAcum =0; ReinAcum =0;
                    }
                    print = true;
                    Captacion  captacion   = (Captacion)lista.get(i);
                    Captacion  captacion2  = new Captacion();
                    saldoInicial      = captacion.getVlr_total_capital() - captacion.getVlr_retefuente() ;
                    if(captacion.getTipo_documento().equals("01")||captacion.getTipo_documento().equals("04")){Naporte = captacion.getVlr_capital();} else {Naporte = 0;}
                    if(captacion.getTipo_documento().equals("03")){Reinteg = captacion.getVlr_capital();} else {Reinteg = 0;}
                    if( i < lista.size()-1 ){
                        captacion2  = (Captacion)lista.get(i+1);
                        fechaCorte = com.tsp.util.Util.fechaMenosUnDia(captacion2.getFecha_inicio().substring(0,10)) ;
                        Intereses  = captacion2.getVlr_intereses() ;
                        Retefuente = captacion2.getVlr_retefuente();
                    }else {
                        fechaCorte = com.tsp.util.Util.AnoActual()+"-"+com.tsp.util.Util.getFechaActual_String(3)+"-"+com.tsp.util.Util.getFechaActual_String(5);
                    }
                    dias = com.tsp.util.Util.diasTranscurridos( captacion.getFecha_inicio().substring(0,10) ,fechaCorte.substring(0,10)) + 1 ;
                    if( i == lista.size() -1 ){
                        Intereses  = Math.round(((double)(dias+dia2)/30.0)*((double)captacion.getInteres()/100.0)*(double)(saldoInicial+ Naporte - Reinteg ));
                        if(!this.us.equals("S"))
                        {
                             Retefuente = Math.round((7.0/100.0)*Intereses);
                        }

                    }
                    if( captacion.getFecha_inicio().equals(captacion2.getFecha_inicio())){
                        print = false;
                        IntAcum += Intereses;
                        ReteAcumula += Retefuente;
                        AporAcum += Naporte;
                        ReinAcum += Reinteg;
                        dia2 += dias;
                    }

                    TotalRTFT += Retefuente;
                    TotalIntr += Intereses;
                    TotAporte += Naporte ;
                    TotReintg += Reinteg ;

                    if(print){

                        Intereses  += IntAcum;
                        Retefuente += ReteAcumula;
                        Naporte    += AporAcum;
                        Reinteg    += ReinAcum;
                        //*/

                        fila++;
                        col = 0;


                        xls.adicionarCelda  (fila ,col++ , captacion.getFecha_inicio().substring(0,10) , texto3 );
                        xls.adicionarCelda  (fila ,col++ , "US$"+Util.customFormat( saldoInicial ), texto3 );
                        xls.adicionarCelda  (fila ,col++ , dias, texto3 );                       
                        xls.adicionarCelda  (fila ,col++ , "US$"+Util.customFormat( Naporte ), texto3 );
                        xls.adicionarCelda  (fila ,col++ , "US$"+Util.customFormat( Reinteg ), texto3 );
                        xls.adicionarCelda  (fila ,col++ , "US$"+Util.customFormat( saldoInicial + Naporte - Reinteg ), texto3 );
                        xls.adicionarCelda  (fila ,col++ , Util.customFormat( Intereses ), texto3 );
                        xls.adicionarCelda  (fila ,col++ , Util.customFormat( Intereses), texto3 );

                        xls.adicionarCelda  (fila ,col++ , "US$"+Util.customFormat( captacion.getVlr_nuevo_capital() +Intereses), fecha );
                        xls.adicionarCelda  (fila ,col++ , fechaCorte.substring(0,10), texto3 );//*/
                    }
                }
                fila+=3;
                col = 0;
                xls.adicionarCelda(fila   ,col++ , "TOTALES"  , titulo );
                xls.cambiarAnchoColumna(col, 4500);
                xls.adicionarCelda(fila   ,col++ , "" , titulo );
                xls.cambiarAnchoColumna(col, 4500);
                xls.adicionarCelda(fila   ,col++ , "" , titulo );
                xls.cambiarAnchoColumna(col, 4500);
                xls.adicionarCelda(fila   ,col++ , "" , titulo );
                xls.cambiarAnchoColumna(col, 4500);
                xls.adicionarCelda(fila   ,col++ ,"US$"+ Util.customFormat(TotAporte) , titulo );
                xls.cambiarAnchoColumna(col, 4500);
                xls.adicionarCelda(fila   ,col++ , "US$"+Util.customFormat(TotReintg) , titulo );
                xls.cambiarAnchoColumna(col, 4500);
                xls.adicionarCelda(fila   ,col++ , ""  , titulo );
                xls.cambiarAnchoColumna(col, 4500);
                xls.adicionarCelda(fila   ,col++ , "US$"+Util.customFormat(TotalIntr)  , titulo );
                xls.cambiarAnchoColumna(col, 4500);
                if(!this.us.equals("S"))
                {
                xls.adicionarCelda(fila   ,col++ ,"US$"+ Util.customFormat(TotalRTFT) , titulo );
                xls.cambiarAnchoColumna(col, 4500);
                }

                xls.adicionarCelda(fila   ,col++ ,"US$"+ Util.customFormat(TotalIntr) , titulo );
                
                xls.cambiarAnchoColumna(col, 4500);

                 xls.adicionarCelda(fila   ,col++ ,"US$"+ Util.customFormat(TotAporte-TotReintg+TotalIntr) , titulo );
                

                xls.cambiarAnchoColumna(col, 4500);
                xls.adicionarCelda(fila   ,col++ , "" , titulo );
                xls.cambiarAnchoColumna(col, 4500);

                xls.cerrarLibro();

    }









    
}
