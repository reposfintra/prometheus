/********************************************************************
 *      Nombre Clase.................   InformeFacturacionTh.java
 *      Descripci�n..................   Hilo para la escritura del xls
 *      Autor........................   Tito Andr�s Maturana
 *      Fecha........................   08.11.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.model.threads;


import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import java.io.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.controller.ReporteCumplidoAction;
import com.tsp.operation.model.services.*;
import com.tsp.operation.model.*;
import java.util.*;
import java.text.*;
import com.tsp.operation.model.beans.POIWrite;



public class InformeFacturacionTh extends Thread{
    
    private Vector informe;
    private String user;
    private String mensaje;
    private String fechai;
    private String fechaf;
    private Model model;
    
    //Log Procesos
    private String procesoName;
    private String des;
    
    public void start(Model model, Vector informe, String user, String fechai, String fechaf){
        this.informe = informe;
        this.user = user;
        this.fechaf = fechaf;
        this.fechai = fechai;
        this.model = model;
        
        this.procesoName = "Informe para Facturaci�n";
        this.des = "Per�odo: " + fechai + " - " + fechaf;
        
        super.start();
    }
    
    public synchronized void run(){
        try{
            
            //INSERTO EN EL LOG DE PROCESO
            model.LogProcesosSvc.InsertProceso(this.procesoName, this.hashCode(), this.des, user);
            
            //Fecha del sistema
            Calendar FechaHoy = Calendar.getInstance();
            Date d = FechaHoy.getTime();
            SimpleDateFormat s = new SimpleDateFormat("yyyyMMdd");
            SimpleDateFormat s1 = new SimpleDateFormat("yyyy-MM-dd-hh:mm");
            SimpleDateFormat fec = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat time = new SimpleDateFormat("hh:mm");
            String FechaFormated = s.format(d);
            String FechaFormated1 = s1.format(d);
            String hora = time.format(d);
            String Fecha = fec.format(d);
            
            
            //obtener cabecera de ruta
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String path = rb.getString("ruta");
            //armas la ruta
            String Ruta1  = path + "/exportar/migracion/" + user + "/";
            //crear la ruta
            File file = new File(Ruta1);
            file.mkdirs();
            
            POIWrite xls = new POIWrite(Ruta1 +"InformeFacturacion_" + FechaFormated + ".xls");//, user, Fecha);
            String nom_archivo = "InformeFacturacion_" + FechaFormated + ".xls";
            this.mensaje = nom_archivo;
            
            //Definici�n de Estilos
            HSSFCellStyle fecha  = xls.nuevoEstilo("Courier, Courier New, Mono", 9, false , false, "yyyy/mm/dd"  , xls.NONE , xls.NONE , xls.NONE );
            HSSFCellStyle texto  = xls.nuevoEstilo("Courier, Courier New, Mono", 9, false , false, "text"        , xls.NONE , xls.NONE , xls.NONE);
            HSSFCellStyle numero = xls.nuevoEstilo("Courier, Courier New, Mono", 9, false , false, ""            , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_RIGHT);
            
            HSSFCellStyle header1      = xls.nuevoEstilo("Courier, Courier New, Mono", 10, true , false, "text"        , HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
            HSSFCellStyle header2      = xls.nuevoEstilo("Courier, Courier New, Mono", 10, true , false, "text"        , HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
            HSSFCellStyle texto2      = xls.nuevoEstilo("Courier, Courier New, Mono", 10, false , false, "text"        , HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
            HSSFCellStyle titulo      = xls.nuevoEstilo("Courier, Courier New, Mono", 9, true  , false, "text"        , HSSFColor.BLACK.index , HSSFColor.PINK.index , HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle fechatitle  = xls.nuevoEstilo("Courier, Courier New, Mono", 9, true  , false, "yyyy/mm/dd"  , HSSFColor.RED.index , xls.NONE, HSSFCellStyle.ALIGN_CENTER );
            
            
            header1.setBorderTop(HSSFCellStyle.BORDER_MEDIUM);
            header1.setBorderBottom(HSSFCellStyle.BORDER_THIN);
            header1.setAlignment(HSSFCellStyle.ALIGN_CENTER);
            
            header2.setAlignment(HSSFCellStyle.ALIGN_CENTER);
            
            xls.obtenerHoja("Informe para Facturaci�n");
            xls.cambiarMagnificacion(3,4);
            
            // cabecera
            
            xls.combinarCeldas(2, 0, 2, 2);
            xls.combinarCeldas(4, 0, 4, 2);
            xls.combinarCeldas(6, 0, 6, 2);
            xls.adicionarCelda(2, 0, "TRANSPORTES SANCHEZ POLO S.A.", header2);
            xls.adicionarCelda(4, 0, "Reporte para Facturaci�n", header2);
            xls.adicionarCelda(6, 0, "Reporte de fecha: " + fechai + " Hasta: " + fechaf, header2);
            xls.adicionarCelda(2, 3, "Fecha: ", header2);
            xls.adicionarCelda(3, 3, "Hora: ", header2);
            xls.adicionarCelda(2, 4, Fecha, header2);
            xls.adicionarCelda(3, 4, hora, header2);
            
            // subtitulos
            
            
            int fila = 8;
            int col  = 0;
            
            xls.adicionarCelda(fila, col++, "REMISION", header1);
            xls.adicionarCelda(fila, col++, "PLACA", header1);
            xls.adicionarCelda(fila, col++, "CONDUCTOR", header1);
            xls.adicionarCelda(fila, col++, "TONELAJE", header1);
            xls.adicionarCelda(fila, col++, "FECHA Y HORA", header1);
            
            fila = 9;
            
            double tonelaje = 0;
            double tonelaje_ttl = 0;
            long viajes_ttl = 0;
            long viajes_d = 0;
            long viajes = 0;
            for( int i= 0; i< informe.size(); i++){
                InformeFact info = (InformeFact) informe.elementAt(i);
                Vector linea = info.getRemisiones();
                
                tonelaje = 0;
                viajes = 0;
                
                String std_job = info.getStd_job();
                model.stdjobdetselService.buscaStandard(std_job);
                Stdjobdetsel stdj = model.stdjobdetselService.getStandardDetSel();
                String desc = stdj.getSj_desc();
                
                xls.combinarCeldas(fila, 0, fila, 4);
                xls.adicionarCelda(fila, 0, "RUTA: " + desc + " - Std. Job. No. " + std_job, texto);
                fila++;
                
                for( int j=0; j<linea.size(); j++){
                    Vector obj = (Vector) linea.elementAt(j);
                    Vector rems = (Vector) obj.elementAt(0);
                    String tonelaje_d = (String) obj.elementAt(1);
                    
                    viajes += rems.size();
                    viajes_d = rems.size();
                    
                    for( int k=0; k<rems.size(); k++){
                        
                        RemisionIFact remision = new RemisionIFact();
                        remision = (RemisionIFact) rems.elementAt(k);
                        
                        tonelaje += remision.getTonelaje().doubleValue();
                        
                        String fecha_rem = remision.getFecha();
                        
                        col = 0;
                        
                        xls.adicionarCelda(fila, col++, remision.getRemision(), texto);
                        xls.adicionarCelda(fila, col++, remision.getPlaca(), texto);
                        xls.adicionarCelda(fila, col++, remision.getConductor(), texto);
                        xls.adicionarCelda(fila, col++, remision.getTonelaje().doubleValue() + " T", texto);
                        xls.adicionarCelda(fila, col++, remision.getFecha(), texto);
                        
                        fila++;
                    }
                    xls.adicionarCelda(fila, 0, "Viajes Diarios >>", texto);
                    xls.adicionarCelda(fila, 1, viajes_d, texto);
                    xls.adicionarCelda(fila, 2, "Tonelaje Diario >>", texto);
                    xls.adicionarCelda(fila, 3, com.tsp.util.Util.redondear( Double.valueOf(tonelaje_d).doubleValue() , 2) + " T", texto);
                    
                    fila += 2;
                    
                    tonelaje_ttl += tonelaje;
                    viajes_ttl += viajes;
                }
                
                xls.adicionarCelda(fila, 0, "RUTA : Viajes >>", texto);
                xls.adicionarCelda(fila, 1, viajes, texto);
                xls.adicionarCelda(fila, 2, "Tonelaje >>", texto);
                xls.adicionarCelda(fila, 3, com.tsp.util.Util.redondear( tonelaje , 2) + " T", texto);
            }
            
            fila += 2;
            
            xls.adicionarCelda(fila, 0, "CLIENTE : Ttl Viajes >>", texto);
            xls.adicionarCelda(fila, 1, viajes_ttl, texto);
            xls.adicionarCelda(fila, 2, "Ttl Tonelaje >>", texto);
            xls.adicionarCelda(fila, 3, com.tsp.util.Util.redondear( tonelaje_ttl , 2) + " T", texto);
            
            
            // datos
            
            /*******************************************************************************************
             *                                      RESUMEN                                            *
             *******************************************************************************************/
            Vector res = this.generarResumen(informe);
            
            if( res.size()>0 ){
                
                xls.obtenerHoja("RESUMEN");
                xls.cambiarMagnificacion(3,4);
                
                // cabecera
                
                xls.combinarCeldas(2, 0, 2, res.size()-1);
                xls.combinarCeldas(4, 0, 4, res.size()-1);
                xls.combinarCeldas(6, 0, 6, res.size()-1);
                xls.adicionarCelda(2, 0, "TRANSPORTES SANCHEZ POLO S.A.", header2);
                xls.adicionarCelda(4, 0, "Reporte para Facturaci�n", header2);
                xls.adicionarCelda(6, 0, "Reporte de fecha: " + fechai + " Hasta: " + fechaf, header2);
                xls.adicionarCelda(2, res.size(), "Fecha: " + Fecha, header2);
                xls.adicionarCelda(3,res.size(), "Hora: " + hora, header2);
                
                // subtitulos
                
                
                fila = 8;
                col  = 0;                
            
                fila = 9;
                
                tonelaje = 0;
                tonelaje_ttl = 0;
                viajes_ttl = 0;
                viajes_d = 0;
                viajes = 0;
                //com.tsp.util.UtilFinanzas.addFecha(Fecha
                
                
                
                String fecha0 = fechai;
                String fecha1 = com.tsp.util.UtilFinanzas.addFecha(fechaf, 1, 2);
                col = 1;
                for( int i=0; i<res.size(); i++){
                    Hashtable ht = (Hashtable) res.elementAt(i);
                    String desc = ht.get("sj_desc").toString();
                    xls.adicionarCelda(fila, col++, ht.get("sj_no").toString() + " " + desc, header1);
                }
                xls.adicionarCelda(fila, col++, "TOTAL", header1);
                fila++;
                
                col = 0;
                fecha0 = fechai;
                fecha1 = com.tsp.util.UtilFinanzas.addFecha(fechaf, 1, 2);
                double ton_ttl = 0;
                do{
                    col = 0;
                    xls.adicionarCelda(fila, col++, fecha0, numero);
                    double ton = 0;
                    for( int i=0; i<res.size(); i++){
                        Hashtable ht = (Hashtable) res.elementAt(i);
                        double t = ht.get(fecha0)!=null? Double.parseDouble(ht.get(fecha0).toString()) : 0;
                        ton += t;
                        xls.adicionarCelda(fila, col++, com.tsp.util.Util.redondear( t, 2), numero);                        
                    }
                    ton_ttl += ton;
                    xls.adicionarCelda(fila, col++, ton, numero);
                    fecha0= com.tsp.util.UtilFinanzas.addFecha(fecha0, 1, 2);
                    fila++;
                    
                }while( Integer.parseInt(fecha0.replaceAll("-",""))<Integer.parseInt(fecha1.replaceAll("-","")) );
                
                col = 0;
                xls.adicionarCelda(fila, col++, "TOTAL", texto);
                for( int i=0; i<res.size(); i++){
                    Hashtable ht = (Hashtable) res.elementAt(i);
                    double ton = 0;
                    fecha0 = fechai;
                    fecha1 = com.tsp.util.UtilFinanzas.addFecha(fechaf, 1, 2);
                    do{
                        double t = ht.get(fecha0)!=null? Double.parseDouble(ht.get(fecha0).toString()) : 0;
                        ton += t;
                        fecha0= com.tsp.util.UtilFinanzas.addFecha(fecha0, 1, 2);
                    }while( Integer.parseInt(fecha0.replaceAll("-",""))<Integer.parseInt(fecha1.replaceAll("-","")) );
                    xls.adicionarCelda(fila, col++, com.tsp.util.Util.redondear( ton, 2 ), numero);
                }
                xls.adicionarCelda(fila, col++, com.tsp.util.Util.redondear( ton_ttl, 2 ), numero);
                
            }
            xls.cerrarLibro();
            
            model.rep_utilidadSvc.dropTable();
            model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(), user, "PROCESO FINALIZADO CON EXITO.");
            
        }catch (Exception ex){
            ex.printStackTrace();
            //////System.out.println("ERROR AL GENERAR EL ARCHIVO XLS : " + ex.getMessage());
            try{
                model.rep_utilidadSvc.dropTable();
                model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(), user, "ERROR :" + ex.getMessage());
            } catch(Exception f){
                try{
                    model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(), user, "ERROR :");
                }catch(Exception p){    }
            }
        }
        
    }
    
    /**
     * Getter for property mensaje.
     * @return Value of property mensaje.
     */
    public java.lang.String getMensaje() {
        return mensaje;
    }
    
    /**
     * Setter for property mensaje.
     * @param mensaje New value of property mensaje.
     */
    public void setMensaje(java.lang.String mensaje) {
        this.mensaje = mensaje;
    }
    
    private Vector generarResumen(Vector informe) throws Exception{
        Vector resum = new Vector();
        
        double tonelaje = 0;
        double tonelaje_ttl = 0;
        long viajes_ttl = 0;
        long viajes_d = 0;
        long viajes = 0;
        
        try{
            for( int i= 0; i< informe.size(); i++){
                InformeFact info = (InformeFact) informe.elementAt(i);
                Vector linea = info.getRemisiones();
                
                tonelaje = 0;
                viajes = 0;
                Hashtable ht = new Hashtable();
                
                String std_job = info.getStd_job();
                model.stdjobdetselService.buscaStandard(std_job);
                Stdjobdetsel stdj = model.stdjobdetselService.getStandardDetSel();
                String desc = stdj.getSj_desc();
                model.tablaGenService.setTblgen(null);
                model.tablaGenService.buscarDatos("ALIASSJ", std_job);
                desc = model.tablaGenService.getTblgen()!=null ?
                model.tablaGenService.getTblgen().getDescripcion() : "NR";
                desc = desc.toLowerCase().indexOf("carbon ")!=-1 ? desc.substring(7) : desc; 
                
                int col = 0;
                
                ht.put("sj_no", std_job);
                ht.put("sj_desc", desc);
                
                for( int j=0; j<linea.size(); j++){
                    Vector obj = (Vector) linea.elementAt(j);
                    Vector rems = (Vector) obj.elementAt(0);
                    String tonelaje_d = (String) obj.elementAt(1);
                    
                    viajes += rems.size();
                    viajes_d = rems.size();
                    
                    col = 0;
                    String[] fechar = {"",""};
                    for( int k=0; k<rems.size(); k++){
                        RemisionIFact remision = new RemisionIFact();
                        remision = (RemisionIFact) rems.elementAt(k);
                        tonelaje += remision.getTonelaje().doubleValue();
                        fechar = remision.getFecha().split(" ");
                        ht.put(fechar[0], "" + com.tsp.util.Util.redondear( Double.valueOf(tonelaje_d).doubleValue() , 2));
                        break;
                    }
                    
                    //ht.put("ton", "" + com.tsp.util.Util.redondear( Double.valueOf(tonelaje_d).doubleValue() , 2));
                    
                    tonelaje_ttl += Double.valueOf(tonelaje_d).doubleValue();//tonelaje;
                    viajes_ttl += viajes;
                }
                
                resum.add(ht);
                
            }
        } catch ( java.sql.SQLException exc){
            throw new Exception(exc.getMessage());
        }
        
        return resum;
    }
    
}