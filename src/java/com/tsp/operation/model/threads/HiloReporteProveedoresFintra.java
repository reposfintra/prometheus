/******************************************************************************
 * Nombre clase :                   HiloReporteProveedoresFintra.java         *
 * Descripcion :                    Clase que maneja los eventos relacionados *
 *                                  con el programa que busca el reporte de   *
 *                                  los movimientos de fintra en la BD.       *
 * Autor :                          LREALES                                   *
 * Fecha :                          12 de septiembre de 2006, 11:00 AM        *
 * Version :                        1.0                                       *
 * Copyright :                      Fintravalores S.A.                   *
 *****************************************************************************/

package com.tsp.operation.model.threads;

import java.util.*;
import java.io.*;
import java.lang.*;
import java.sql.*;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.beans.POIWrite;
import javax.servlet.*;
import com.tsp.exceptions.*;
import com.tsp.operation.controller.*;
import java.text.*;
import javax.servlet.http.*;
import com.tsp.util.*;
import com.tsp.operation.model.*;
import com.tsp.util.Util;
import java.sql.SQLException;

public class HiloReporteProveedoresFintra extends Thread {
        
    private Movpla bean;
    private Vector datos;
    private String path;
    private String usuario;
    private String fi;
    private String ff;
    private String m = "";//
    private String procesoName;
    private String des;
    private Model model = new Model();
    
    public HiloReporteProveedoresFintra() { }
    
    public void start( Vector datos, String usu, String fecha_inicial, String fecha_final ) {
      
        this.datos = datos;
        this.procesoName = "Reporte Proveedores Fintra";
        this.des = "Reporte De Proveedores De Terceros De Fintra";
        this.usuario = usu;  
        this.fi = fecha_inicial;
        this.ff = fecha_final;
        
        super.start();
        
    }
    
    public synchronized void run(){
        
        try{
            
            //Inicia el proceso
            model.LogProcesosSvc.InsertProceso( procesoName, this.hashCode(), des, usuario );
            
            Util u = new Util();
            
            ResourceBundle rb = ResourceBundle.getBundle( "com/tsp/util/connectionpool/db" );
            String path = rb.getString( "ruta" );
                        
            File file = new File( path + "/exportar/migracion/" + usuario );
            file.mkdirs();
               
            String fecha = Util.getFechaActual_String(6);            
            fecha=fecha.replaceAll( "/", "-" );
            fecha=fecha.replaceAll( ":", "_" );
            
            String nombreArch  = "ReporteProveedoresFintra[" + fecha + "].xls";
            String       Hoja  = "ReporteProveedoresFintra";
            String       Ruta  = path + "/exportar/migracion/" + usuario + "/" +nombreArch; 
            
            HSSFWorkbook wb    = new HSSFWorkbook();
            HSSFSheet    sheet = wb.createSheet( Hoja );
            HSSFRow      row   = null;
            HSSFRow      row2  = null;
            HSSFCell     cell  = null;
            
            for ( int col = 0; col < 19 ; col++ ){ //COLUMNAS
                
                sheet.setColumnWidth( (short) col, (short) ( ( 50 * 8 ) / ( (double) 1 / 15 ) ) ); 
                
            }
            
            /****  ENCABEZADO Y DEFINICION DE ESTILOS ************************************************/
            
            /** ENCABEZADO GENERAL *******************************/
            HSSFFont  fuente1 = wb.createFont();
            fuente1.setFontName("verdana");
            fuente1.setFontHeightInPoints((short)(16)) ;
            fuente1.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            fuente1.setColor(HSSFColor.DARK_RED.index);
            
            HSSFCellStyle estilo1 = wb.createCellStyle();
            estilo1.setFont(fuente1);
            estilo1.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo1.setBottomBorderColor(HSSFColor.WHITE.index);
            estilo1.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo1.setLeftBorderColor  (HSSFColor.WHITE.index);
            estilo1.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo1.setRightBorderColor(HSSFColor.WHITE.index);
            estilo1.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo1.setTopBorderColor   (HSSFColor.WHITE.index);
            
            
            /** SUBTITULO *******************************/
            HSSFFont  fuenteX = wb.createFont();
            fuenteX.setFontName("verdana");
            fuenteX.setFontHeightInPoints((short)(11)) ;
            fuenteX.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            fuenteX.setColor(HSSFColor.DARK_RED.index);
            
            HSSFCellStyle estiloX = wb.createCellStyle();
            estiloX.setFont(fuenteX);
            estiloX.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estiloX.setBottomBorderColor(HSSFColor.WHITE.index);
            estiloX.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estiloX.setLeftBorderColor  (HSSFColor.WHITE.index);
            estiloX.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estiloX.setRightBorderColor(HSSFColor.WHITE.index);
            estiloX.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estiloX.setTopBorderColor   (HSSFColor.WHITE.index);
            
            /** TEXTO EN EL ENCABEAZADO *************************/
            HSSFFont  fuente2 = wb.createFont();
            fuente2.setFontName("verdana");
            fuente2.setFontHeightInPoints((short)(11)) ;
            fuente2.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            fuente2.setColor((short)(0x0));
            
            HSSFCellStyle estilo2 = wb.createCellStyle();
            estilo2.setFont(fuente2);
            estilo2.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo2.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            
            /** ENCABEZADO DE LAS COLUMNAS***********************/
            HSSFFont  fuente3 = wb.createFont();
            fuente3.setFontName("verdana");
            fuente3.setFontHeightInPoints((short)(9)) ;
            fuente3.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            fuente3.setColor(HSSFColor.BLACK.index);
            
            HSSFCellStyle estilo3 = wb.createCellStyle();
            estilo3.setFont(fuente3);
            estilo3.setFillForegroundColor(HSSFColor.GREY_25_PERCENT.index);
            estilo3.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo3.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo3.setBottomBorderColor(HSSFColor.BLACK.index);
            estilo3.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo3.setLeftBorderColor  (HSSFColor.BLACK.index);
            estilo3.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo3.setRightBorderColor(HSSFColor.BLACK.index);
            estilo3.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo3.setTopBorderColor   (HSSFColor.BLACK.index);
            estilo3.setAlignment(HSSFCellStyle.ALIGN_CENTER);            
                        
            /** TEXTO NORMAL ************************************/
            HSSFFont  fuente4 = wb.createFont();
            fuente4.setFontName("verdana");
            fuente4.setFontHeightInPoints((short)(9)) ;
            fuente4.setBoldweight(HSSFFont.BOLDWEIGHT_NORMAL);
            fuente4.setColor((short)(0x0));
            
            HSSFCellStyle estilo4 = wb.createCellStyle();
            estilo4.setFont(fuente4);
            estilo4.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo4.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo4.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo4.setBottomBorderColor(HSSFColor.BLACK.index);
            estilo4.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo4.setLeftBorderColor  (HSSFColor.BLACK.index);
            estilo4.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo4.setRightBorderColor(HSSFColor.BLACK.index);
            estilo4.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo4.setTopBorderColor   (HSSFColor.BLACK.index);
            
            /** NUMEROS ************************************/
            HSSFCellStyle estilo5 = wb.createCellStyle();
            estilo5.setFont(fuente4);
            estilo5.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo5.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo5.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo5.setBottomBorderColor(HSSFColor.BLACK.index);
            estilo5.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo5.setLeftBorderColor  (HSSFColor.BLACK.index);
            estilo5.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo5.setRightBorderColor(HSSFColor.BLACK.index);
            estilo5.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo5.setTopBorderColor   (HSSFColor.BLACK.index);
            estilo5.setAlignment(HSSFCellStyle.ALIGN_RIGHT);
            /****************************************************/
            
            HSSFFont fuente6 = wb.createFont();
            fuente6.setColor((short)0x0);  
            fuente6.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            
            HSSFCellStyle estilo6 = wb.createCellStyle();
            estilo6.setFont(fuente6);
            estilo6.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo6.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo6.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo6.setBottomBorderColor(HSSFColor.WHITE.index);
            estilo6.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo6.setLeftBorderColor  (HSSFColor.WHITE.index);
            estilo6.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo6.setRightBorderColor(HSSFColor.WHITE.index);
            estilo6.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo6.setTopBorderColor   (HSSFColor.WHITE.index);
            
            /****************************************************/  
            /* MONEDA */
            HSSFCellStyle estilo7 = wb.createCellStyle();
            estilo7.setFont(fuente4);
            estilo7.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo7.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo7.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo7.setBottomBorderColor(HSSFColor.BLACK.index);
            estilo7.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo7.setLeftBorderColor  (HSSFColor.BLACK.index);
            estilo7.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo7.setRightBorderColor(HSSFColor.BLACK.index);
            estilo7.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo7.setTopBorderColor   (HSSFColor.BLACK.index);
            estilo7.setAlignment(HSSFCellStyle.ALIGN_RIGHT);
            estilo7.setDataFormat(wb.createDataFormat().getFormat("$#,##0.00"));
            
            /* TEXTO NORMAL CENTRADO */
            HSSFCellStyle estilo9 = wb.createCellStyle();
            estilo9.setFont(fuente4);
            estilo9.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo9.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo9.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo9.setBottomBorderColor(HSSFColor.BLACK.index);
            estilo9.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo9.setLeftBorderColor  (HSSFColor.BLACK.index);
            estilo9.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo9.setRightBorderColor(HSSFColor.BLACK.index);
            estilo9.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo9.setTopBorderColor   (HSSFColor.BLACK.index);
            estilo9.setAlignment(HSSFCellStyle.ALIGN_CENTER);
            
            row  = sheet.createRow((short)(0));
            row  = sheet.createRow((short)(1));
            row  = sheet.createRow((short)(2));
            row  = sheet.createRow((short)(3));
            row  = sheet.createRow((short)(4));
            row  = sheet.createRow((short)(5));
            row  = sheet.createRow((short)(6));
            row  = sheet.createRow((short)(7));
            row  = sheet.createRow((short)(8));
            row  = sheet.createRow((short)(9));
            
            for ( int j = 0; j < 19; j++ ) { //COLUMNAS
                
                row  = sheet.getRow((short)(0));
                cell = row.createCell((short)(j));
                cell.setCellStyle(estilo1);
                row  = sheet.getRow((short)(1));
                cell = row.createCell((short)(j)); 
                cell.setCellStyle(estiloX);
                
            }
            
            row  = sheet.getRow((short)(0));
            cell = row.getCell((short)(0));
            cell.setCellValue("TRANSPORTES SANCHEZ POLO");
                        
            row  = sheet.getRow((short)(1));            
            cell = row.getCell((short)(0));            
            cell.setCellValue("Reporte de Proveedores de Terceros de Fintra");
            
            for( int i = 2; i < 5; i++ ){ //FILAS
                 
                for ( int j = 0; j < 19; j++ ) { //COLUMNAS
                    
                    row  = sheet.getRow((short)(i));
                    cell = row.createCell((short)(j)); 
                    cell.setCellStyle(estilo6);
                    
                }      
                
            }
            
            //FECHA
            row = sheet.getRow((short)(3));              
            cell = row.createCell((short)(0));            
            cell = row.getCell((short)(0));            
            cell.setCellStyle(estilo6);
            cell.setCellValue("Desde: " + fi + " - Hasta: " + ff);
            
            /*************************************************************************************/
            /***** RECORRER LOS DATOS ******/
            
            int Fila = 5; 
            for ( int t = 0; t < datos.size(); t++ ){
                
                Movpla info = ( Movpla ) datos.elementAt( t );
                
                Fila++;
                row  = sheet.createRow( ( short )( Fila ) );
                  
                cell = row.createCell( ( short )( 0 ) );// NIT
                cell.setCellStyle( estilo4 );
                cell.setCellValue( info.getProveedor_anticipo() );
                
                cell = row.createCell( ( short )( 1 ) );// PROVEEDOR
                cell.setCellStyle( estilo4 );
                cell.setCellValue( info.getProveedor() );
                
                cell = row.createCell( ( short )( 2 ) );// FECHA DOCUMENTO
                cell.setCellStyle( estilo4 );
                cell.setCellValue( info.getDate_doc() );
                
                cell = row.createCell( ( short )( 3 ) );// PLANILLA
                cell.setCellStyle( estilo4 );
                cell.setCellValue( info.getPlanilla() );
                
                cell = row.createCell( ( short )( 4 ) );// AGENCIA
                cell.setCellStyle( estilo4 );
                cell.setCellValue( info.getAgency_id() );
                
                cell = row.createCell( ( short )( 5 ) );// ORIGEN
                cell.setCellStyle( estilo4 );
                cell.setCellValue( info.getObservacion() );
                
                cell = row.createCell( ( short )( 6 ) );// DESTINO
                cell.setCellStyle( estilo4 );
                cell.setCellValue( info.getDocument_type() );
                
                cell = row.createCell( ( short )( 7 ) );// PLACA
                cell.setCellStyle( estilo4 );
                cell.setCellValue( info.getSupplier() );
                
                cell = row.createCell( ( short )( 8 ) );// PROPIETARIO
                cell.setCellStyle( estilo4 );
                cell.setCellValue( info.getPla_owner() );
                
                cell = row.createCell( ( short )( 9 ) );// DESCUENTO
                cell.setCellStyle( estilo7 );
                cell.setCellValue( info.getVlr_disc() );
                
                cell = row.createCell( ( short )( 10 ) );// VALOR
                cell.setCellStyle( estilo7 );
                cell.setCellValue( info.getVlr() );
                
                cell = row.createCell( ( short )( 11 ) );// VALOR ME
                cell.setCellStyle( estilo7 );
                cell.setCellValue( info.getVlr_for() );
                
                cell = row.createCell( ( short )( 12 ) );// TASA
                cell.setCellStyle( estilo4 );
                cell.setCellValue( info.getTipo_rec() );
                
                cell = row.createCell( ( short )( 13 ) );// MONEDA
                cell.setCellStyle( estilo4 );
                cell.setCellValue( info.getCurrency() );
                
                cell = row.createCell( ( short )( 14 ) );// CHEQUE
                cell.setCellStyle( estilo4 );
                cell.setCellValue( info.getDocument() );
                
                cell = row.createCell( ( short )( 15 ) );// BANCO
                cell.setCellStyle( estilo4 );
                cell.setCellValue( info.getBranch_code() );
                
                cell = row.createCell( ( short )( 16 ) );// SUCURSAL
                cell.setCellStyle( estilo4 );
                cell.setCellValue( info.getBank_account_no() );
                
                cell = row.createCell( ( short )( 17 ) );// CREATION_DATE
                cell.setCellStyle( estilo4 );
                cell.setCellValue( info.getCreation_date() );
                
                cell = row.createCell( ( short )( 18 ) );// CREATION_USER
                cell.setCellStyle( estilo4 );
                cell.setCellValue( info.getCreation_user() );
                
            }
                        
            row = sheet.createRow((short)(5));            
            row = sheet.getRow( (short)(5) );     
            
            cell = row.createCell((short)(0));// NIT
            cell.setCellStyle(estilo3);
            cell.setCellValue("NIT");
            
            cell = row.createCell((short)(1));// PROVEEDOR
            cell.setCellStyle(estilo3);
            cell.setCellValue("PROVEEDOR");
            
            cell = row.createCell((short)(2));// FECHA DOCUMENTO
            cell.setCellStyle(estilo3);
            cell.setCellValue("FECHA DOCUMENTO");
            
            cell = row.createCell((short)(3));// PLANILLA
            cell.setCellStyle(estilo3);
            cell.setCellValue("PLANILLA");
            
            cell = row.createCell((short)(4));// AGENCIA
            cell.setCellStyle(estilo3);
            cell.setCellValue("AGENCIA");
            
            cell = row.createCell((short)(5));// ORIGEN
            cell.setCellStyle(estilo3);
            cell.setCellValue("ORIGEN");
            
            cell = row.createCell((short)(6));// DESTINO
            cell.setCellStyle(estilo3);
            cell.setCellValue("DESTINO");
            
            cell = row.createCell((short)(7));// PLACA
            cell.setCellStyle(estilo3);
            cell.setCellValue("PLACA");
            
            cell = row.createCell((short)(8));// PROPIETARIO
            cell.setCellStyle(estilo3);
            cell.setCellValue("PROPIETARIO");
            
            cell = row.createCell((short)(9));// DESCUENTO
            cell.setCellStyle(estilo3);
            cell.setCellValue("DESCUENTO");
            
            cell = row.createCell((short)(10));// VALOR
            cell.setCellStyle(estilo3);
            cell.setCellValue("VALOR");
            
            cell = row.createCell((short)(11));// VALOR ME
            cell.setCellStyle(estilo3);
            cell.setCellValue("VALOR ME");
            
            cell = row.createCell((short)(12));// TASA
            cell.setCellStyle(estilo3);
            cell.setCellValue("TASA");
            
            cell = row.createCell((short)(13));// MONEDA
            cell.setCellStyle(estilo3);
            cell.setCellValue("MONEDA");
            
            cell = row.createCell((short)(14));// CHEQUE
            cell.setCellStyle(estilo3);
            cell.setCellValue("CHEQUE");
            
            cell = row.createCell((short)(15));// BANCO
            cell.setCellStyle(estilo3);
            cell.setCellValue("BANCO");
            
            cell = row.createCell((short)(16));// SUCURSAL
            cell.setCellStyle(estilo3);
            cell.setCellValue("SUCURSAL");
            
            cell = row.createCell((short)(17));// CREATION_DATE
            cell.setCellStyle(estilo3);
            cell.setCellValue("CREATION_DATE");
            
            cell = row.createCell((short)(18));// CREATION_USER
            cell.setCellStyle(estilo3);
            cell.setCellValue("CREATION_USER");
            
            /******************************************************************/
            /***** GUARDAR DATOS EN EL ARCHIVO *****/
            FileOutputStream fo = new FileOutputStream ( Ruta );
            wb.write ( fo );
            fo.close ();
        
            //Finalizo el proceso exitosamente
            model.LogProcesosSvc.finallyProceso( procesoName, this.hashCode(), usuario, "PROCESO EXITOSO" );
            
        } catch( Exception e ){    
            
            e.printStackTrace();
            
            //Capturo errores finalizando proceso
            try{
                
                model.LogProcesosSvc.finallyProceso( procesoName, this.hashCode(), usuario, "ERROR : " + e.getMessage() );
            
            } catch( Exception f ){
                
                f.printStackTrace();
                
                try{
                    
                    model.LogProcesosSvc.finallyProceso( procesoName,this.hashCode(), usuario, "ERROR : " + f.getMessage() );
                
                } catch( Exception p ){ p.printStackTrace(); }
                
            }

        }
        
    }

    // Funcion para formatear el texto de los decimales
    public String customFormat( double value ) {
        
        DecimalFormat df = ( DecimalFormat ) NumberFormat.getNumberInstance( new Locale( "en", "US" ) );
        
        df.applyPattern( "$#,###.##" );
        df.setMaximumFractionDigits( 2 );
        df.setMinimumFractionDigits( 2 );
        
        return df.format( value );
        
    }
    
}