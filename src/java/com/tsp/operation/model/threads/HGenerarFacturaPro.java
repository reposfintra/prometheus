/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.threads;

import com.tsp.operation.model.Model;
import com.tsp.operation.model.beans.*;

import java.util.*;
import java.text.*;

/**
 *
 * @author Alvaro
 */



public class HGenerarFacturaPro extends Thread{


    private Model   model;
    private Usuario usuario;
    private String  dstrct;


    /** Creates a new instance of HGenerarFacturaPro */
    public HGenerarFacturaPro () {
    }

    public void start(Model model, Usuario usuario, String distrito){

        this.usuario = usuario;
        this.model = model;
        this.dstrct = distrito;

        super.start();
    }


    public synchronized void run(){
        try{
            //model.LogProcesosSvc.InsertProceso(this.processName, this.hashCode(), "Generacion del reporte de prefactura : " + prefactura, usuario.getLogin());
            

            Vector comandos_sql =new Vector();
            String comandoSQL = "";

            java.util.Date fechaActual = new Date();
            String creation_date = fechaActual.toString();

            List listaPrefacturaEca =  model.applusService.buscaPrefacturaPro();
            
            if (listaPrefacturaEca.size() != 0){

                SerieGeneral serie = null;
                serie = new SerieGeneral();
                PrefacturaEca prefacturaEca =  new PrefacturaEca();


                Iterator it = listaPrefacturaEca.iterator();
                while (it.hasNext()) {

                    prefacturaEca = (PrefacturaEca)it.next();

                    // Datos comunes a la cabecera y a los items

                    // Cambiar el nit al de PROVINTEGRAL
                    String proveedor = "9000742958";
                    String auxiliar  = proveedor;
                    String tipo_documento = "FAP";

                    // Buscar un consecutivo
                    serie = model.serieGeneralService.getSerie("FINV","OP","FAPPR");
                    model.serieGeneralService.setSerie("FINV","OP","FAPPR");
                    String documento = serie.getUltimo_prefijo_numero();

                    String user_update = usuario.getLogin();
                    String creation_user = user_update;
                    String base = "COL";

                    // CREACION DE LA CABECERA EN CXP_DOC

                    // Datos de la cabecera

                    int  id_orden     = prefacturaEca.getId_orden() ;
                    
                    String esquema_comision=model.negociosApplusService.getEsquemaComision(""+id_orden);//090511
                    
                    String num_os     = prefacturaEca.getNum_os();

                    String simbolo_variable     = prefacturaEca.getSimbolo_variable();
                    String descripcion_factura  = "NUMERO OS: " + num_os + "   ORDEN: " + id_orden +
                                                  "   SIMBOLO VARIABLE: " + simbolo_variable;

                    String facturaEca     = prefacturaEca.getFactura_eca();
                    String descripcion    = "Factura por comision en factura : " + facturaEca+ "   " + descripcion_factura;
                    String agencia        = "BQ";
                    Proveedor objeto_proveedor = model.proveedorService.obtenerProveedorPorNit(proveedor);
                    //String handle_code = objeto_proveedor.getC_hc();
                    
                    String handle_code = "MP";
                    
                    String aprobador   = "JGOMEZ";
                    String usuario_aprobacion = "JGOMEZ";
                    String banco = objeto_proveedor.getC_branch_code();
                    String sucursal = objeto_proveedor.getC_bank_account();
                    String moneda = "PES";


                    double e_comision_provintegral = prefacturaEca.getE_comision_provintegral();
                    
                    double e_iva_comision_provintegral = prefacturaEca.getE_iva_comision_provintegral();//090401

                    double vlr_factura = e_comision_provintegral+e_iva_comision_provintegral;
                                        
                    /*if (esquema_comision.equals("MODELO_ANTERIOR")){//090511
                        vlr_factura=vlr_factura+e_comision_provintegral//nuevo 1;//090402            +e_iva_comision_provintegral//090511
                    }*/
                    
                    double vlr_neto  =  vlr_factura;
                    double vlr_total_abonos = 0;
                    double vlr_saldo = vlr_neto;
                    double vlr_neto_me = vlr_neto;
                    double vlr_total_abonos_me = 0;
                    double vlr_saldo_me = vlr_neto;
                    double tasa = 1;


                    String observacion = "DETALLE:  Comision provintegral: ";
                    observacion = observacion + Double.toString(e_comision_provintegral) ;

                    String clase_documento = "4";
                    // String moneda_banco = objeto_proveedor.getC_currency_bank();
                    String moneda_banco = "PES";
                    String clase_documento_rel = "4";
                    
                    // Calculo de la fecha de vencimiento de la factura// 
                    
                    String fecha_factura_eca = prefacturaEca.getFecha_factura_eca();

                    Calendar calendarFechaVencimiento = Calendar.getInstance();
                    String ano = fecha_factura_eca.substring(0,4);
                    String mes = fecha_factura_eca.substring(5,7);
                    String dia = fecha_factura_eca.substring(8,10);

                    calendarFechaVencimiento.set(Integer.parseInt(ano),Integer.parseInt(mes),Integer.parseInt(mes),8,0,0);
                    calendarFechaVencimiento.add(Calendar.DATE, 60);

                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    String sFechaFacturaVencimiento = sdf.format(calendarFechaVencimiento.getTime()) ;

                    comandoSQL = model.applusService.setCxp_doc(dstrct, proveedor, tipo_documento, documento, descripcion,
                            agencia, handle_code, aprobador, usuario_aprobacion, banco, sucursal,
                            moneda, vlr_neto, vlr_total_abonos, vlr_saldo, vlr_neto_me,
                            vlr_total_abonos_me, vlr_saldo_me, tasa, observacion, user_update,
                            creation_user, base, clase_documento, moneda_banco,
                            fecha_factura_eca, sFechaFacturaVencimiento, clase_documento_rel,
                            creation_date, creation_date);

                    comandos_sql.add(comandoSQL);


                    // CREACION DE LOS ITEMS EN CXP_ITEMS_DOC

                    // Datos comunes a todos los items

                    int item = 0;

                    // Item de valor comision
                    descripcion = "Comision provintegral: " + descripcion_factura;
                    comandoSQL  = model.applusService.setCxp_items_doc(dstrct, proveedor,
                                         tipo_documento, documento,Integer.toString(++item),  descripcion,
                                         e_comision_provintegral, e_comision_provintegral, "28150502",
                                         user_update, creation_user, base, "083", auxiliar,
                                         creation_date, creation_date);
                    comandos_sql.add(comandoSQL);
                    
//inicio de cambio 090401
                    // Item de valor Iva de  e_comision_provintegral
                    descripcion = "Iva Comision provintegral: " + descripcion_factura + "   Base: " +
                                   Double.toString(e_comision_provintegral) + "   ";
                    comandoSQL  = model.applusService.setCxp_items_doc(dstrct, proveedor,
                                         tipo_documento, documento,Integer.toString(++item),  descripcion,
                                         e_iva_comision_provintegral, e_iva_comision_provintegral, "28150502",
                                         user_update, creation_user, base, "085", auxiliar,
                                         creation_date, creation_date);
                    comandos_sql.add(comandoSQL);
//fin de cambio 090401       
                    
                    //inicio de 090511 
                    /*if (esquema_comision.equals("MODELO_ANTERIOR")){
                        descripcion = "nuevo 1 de provintegral: " + descripcion_factura + "   Base: " +
                                       Double.toString(e_comision_provintegral) + "   ";
                        comandoSQL  = model.applusService.setCxp_items_doc(dstrct, proveedor,
                                             tipo_documento, documento,Integer.toString(++item),  descripcion,
                                             e_comision_provintegral, e_comision_provintegral, "28150501",
                                             user_update, creation_user, base, "087", auxiliar,
                                             creation_date, creation_date);                    
                        comandos_sql.add(comandoSQL);
                    } */   

                    //fin de 090511

                    // ACTUALIZA EL NUMERO DE FACTURA APPLUS EN OFERTA

                    comandoSQL = model.applusService.setFacturaPro(id_orden,documento,fecha_factura_eca);
                    comandos_sql.add(comandoSQL);

                }

                // Grabando todo a la base de datos.
                model.applusService.ejecutarSQL(comandos_sql);
            }


            //model.LogProcesosSvc.finallyProceso(this.processName, this.hashCode(), usuario.getLogin(), "PROCESO EXITOSO");
        }catch (Exception ex){
            ex.printStackTrace();
            try{
                //model.LogProcesosSvc.finallyProceso(this.processName, this.hashCode(), usuario.getLogin(), "Finalizado con error ...\n" + ex.getMessage());
            }catch (Exception e){
                System.out.println("Error HGenerarFacturaPro ...\n" + e.getMessage());
            }
        }
    }


}
