/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.threads;

/**
 *
 * @author geotech
 */
import com.tsp.operation.model.DAOS.*;
import com.tsp.operation.model.beans.factura;
import com.tsp.operation.model.services.GestionCarteraService;


import java.util.*;

/**
 *
 * @author Ing. Iris Vargas
 */
public class HEnviarEstadoCuenta extends Thread {

    private String tox, copytox, hiddencopytox;
    private String fromx, asuntox, msgx;
    private String ruta, nombre_archivo;
    String correo_salida = "fintravalores@geotechsa.com";
    String pass_salida = "fintra21";
    String dataBaseName;

    public HEnviarEstadoCuenta(String dataBaseName) {
        this.dataBaseName = dataBaseName;
    }

    public synchronized void run() {

        try {
            GestionCarteraService cserv = null;
            cserv = new GestionCarteraService(dataBaseName);
            ArrayList<String> lista = null;
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            ruta = rb.getString("ruta") + "/exportar/migracion/EstadoCuenta/";
            fromx = "servicioalcliente@fintra.co";
            asuntox = "Su Estado De Cuenta - Fintra S.A.";
            copytox = "";
            hiddencopytox = "";
            nombre_archivo = "estado_cuenta.pdf";
            msgx = "<p><b>Estimado cliente</b>,</p><p>&nbsp; </p><p>Reciba un cordial saludo.  </p><p> &nbsp; </p><p>Adjunto "
                    + "a la presente, enviamos su estado de cuentas para que proceda con el "
                    + "pago de la pr&oacute;xima cuota a vencer en la fecha indicada y correspondiente "
                    + "a este periodo. Si el pago ya fue realizado le solicitamos hacer caso "
                    + "omiso a este e-mail.  </p><p> &nbsp; &nbsp; </p><p><b>Nota: </b>Este es un "
                    + " correo autom&aacute;tico, no responder directamente. Para mayor informaci&oacute;n "
                    + "puede comunicarse a los tel&eacute;fonos 57+(5) 3679900 - 3679902 - 3679906 &oacute; "
                    + "escribirnos a la direcci&oacute;n de correo electr&oacute;nico <i><a>director.cartera@fintra.co</a> "
                    + "</i></p><p> &nbsp; </p><p>Atentamente,  </p><p> &nbsp; </p><p> &nbsp; </p><p><b><i><span>"
                    + "Departamento De Cartera  </span></i></b></p>";

            lista = cserv.facturasPorVencer(5, 3);

            long tiempoInicio = System.currentTimeMillis();
            int envios = 0;

            for (int i = 0; i < lista.size(); i++) {
                long totaltiempo = System.currentTimeMillis() - tiempoInicio;
                if (envios < 3 && (totaltiempo < 60000)) {

                    String[] dat = lista.get(i).split(";");
                    ArrayList<factura> listafactscli = null;
                    ArrayList<String> listahead = null;
                    try {
                        listafactscli = cserv.facturasNegocioClie(dat[0]);
                        listahead = cserv.cabeceraTabla(dat[0]);

                        if (cserv.generarPdf4("EstadoCuenta", listafactscli, listahead)) {

                            tox =dat[1];//correo modificar para subir en productivo
                            if (cserv.envia_correo(fromx, tox, copytox, correo_salida, pass_salida, nombre_archivo, ruta, hiddencopytox, asuntox, msgx)) {
                                cserv.insObservacion(dat[2], "Env�o estado de cuenta autom�tico por correo", "ADMIN", "35", dat[3] + " 08:00:00", "37","","");
                            }
                        }
                    } catch (Exception e) {
                        System.out.print("ERROR Hilo HEnviarEstadoCuenta: " + e.getMessage());
                    }
                    envios++;
                } else {
                    if ((totaltiempo > 60000)) {
                        tiempoInicio = System.currentTimeMillis();
                        envios = 0;
                        i--;
                    }
                    if (envios >= 3 && totaltiempo < 60000) {
                        this.sleep(60000 - totaltiempo);
                        tiempoInicio = System.currentTimeMillis();
                        envios = 0;
                        i--;
                    }
                }
            }

        } catch (Exception e) {

            System.out.print("ERROR Hilo: " + e.getMessage());

        }


    }
}
