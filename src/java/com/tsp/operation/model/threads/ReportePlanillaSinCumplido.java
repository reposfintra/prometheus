/******************************************************************************
 * Nombre clase : ............... ReportePlanillaSinCumplido.java             *
 * Descripcion :................. Hilo que permite la creacion del reporte    *
 *                                de Planillas Sin Cumplido                   *
 *                                 en un archivo en excel                     *
 * Autor :....................... Ing. David Velasquez Gonzalez  
 * Modified:......................Ing. Enrique De Lavalle
 * Fecha :........................ 27 de noviembre de 2006, 03:41 PM          *
 * Version :...................... 1.0                                        *
 * Copyright :.................... Fintravalores S.A.                    *
 ******************************************************************************/
package com.tsp.operation.model.threads;

import java.util.*;
import java.io.*;
import java.lang.*;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.util.*;
import com.tsp.operation.model.*;

public class ReportePlanillaSinCumplido extends Thread{
    private List Lista;
    private String fechainicio;
    private String fechafin;
    private String cliente;
    private String id;
    private String usuario;
    
    Model model = new Model ();
    
    /** Creates a new instance of ReporteAnulacionPlanilla */
    public ReportePlanillaSinCumplido() {
    }
    
    public void start(List Lista, String fechai, String fechaf, String cliente, String id, String usuario) {
        this.Lista = Lista;
        this.fechainicio = fechai;
        this.fechafin = fechaf;
        this.cliente = cliente;
        this.id = id;
        this.usuario = usuario;
        super.start();
    }
    
    public synchronized void run(){
        try{
                    
            //INSERTO EN EL LOG DE PROCESO
            model.LogProcesosSvc.InsertProceso("Reporte Planillas sin Cumplido", this.hashCode(),  "Reporte Planillas sin Cumplido", usuario );
                      
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String path = rb.getString("ruta");
            
            File file = new File(path + "/exportar/migracion/"+id);
            file.mkdirs();
            
            String nombreArch= "PlanillaSinCumplido["+fechainicio+"]["+fechafin+"].xls";
            String       Hoja  = "PlanillasSinCumplido";
            String       Ruta  = path + "/exportar/migracion/"+ id +"/" + nombreArch;
            HSSFWorkbook wb    = new HSSFWorkbook();
            HSSFSheet    sheet = wb.createSheet(Hoja);
            HSSFRow      row   = null;
            HSSFRow      row2  = null;
            HSSFCell     cell  = null;
            
            
            
            /****  ENCABEZADO Y DEFINICION DE ESTILOS ************************************************/
            
            /** ENCABEZADO GENERAL *******************************/
            HSSFFont  fuente1 = wb.createFont();
            fuente1.setFontName("verdana");
            fuente1.setFontHeightInPoints((short)(16)) ;
            fuente1.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            fuente1.setColor((short)(0x1));
            
            HSSFCellStyle estilo1 = wb.createCellStyle();
            estilo1.setFont(fuente1);
            estilo1.setFillForegroundColor(HSSFColor.BLUE.index);
            estilo1.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            
            /** TEXTO EN EL ENCABEAZADO *************************/
            HSSFFont  fuente2 = wb.createFont();
            fuente2.setFontName("verdana");
            fuente2.setFontHeightInPoints((short)(11)) ;
            fuente2.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            fuente2.setColor((short)(0x1));
            
            HSSFCellStyle estilo2 = wb.createCellStyle();
            estilo2.setFont(fuente2);
            estilo2.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo2.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            
            /** ENCABEZADO DE LAS COLUMNAS***********************/
            HSSFFont  fuente3 = wb.createFont();
            fuente3.setFontName("verdana");
            fuente3.setFontHeightInPoints((short)(11)) ;
            fuente3.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            fuente3.setColor((short)(0x1));
            
            HSSFCellStyle estilo3 = wb.createCellStyle();
            estilo3.setFont(fuente3);
            estilo3.setFillForegroundColor(HSSFColor.BLUE.index);
            estilo3.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo3.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo3.setBottomBorderColor(HSSFColor.BLACK.index);
            estilo3.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo3.setLeftBorderColor  (HSSFColor.BLACK.index);
            estilo3.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo3.setRightBorderColor(HSSFColor.BLACK.index);
            estilo3.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo3.setTopBorderColor   (HSSFColor.BLACK.index);
            
            /** TEXTO NORMAL ************************************/
            HSSFFont  fuente4 = wb.createFont();
            fuente4.setFontName("verdana");
            fuente4.setFontHeightInPoints((short)(9)) ;
            fuente4.setBoldweight(HSSFFont.BOLDWEIGHT_NORMAL);
            fuente4.setColor((short)(0x0));
            
            HSSFCellStyle estilo4 = wb.createCellStyle();
            estilo4.setFont(fuente4);
            estilo4.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo4.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo4.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo4.setBottomBorderColor(HSSFColor.BLACK.index);
            estilo4.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo4.setLeftBorderColor  (HSSFColor.BLACK.index);
            estilo4.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo4.setRightBorderColor(HSSFColor.BLACK.index);
            estilo4.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo4.setTopBorderColor   (HSSFColor.BLACK.index);
            
            
            
            HSSFCellStyle numerico = wb.createCellStyle();
            numerico.setFont(fuente4);
            numerico.setFillForegroundColor(HSSFColor.WHITE.index);
            numerico.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            numerico.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            numerico.setBottomBorderColor(HSSFColor.BLACK.index);
            numerico.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            numerico.setLeftBorderColor  (HSSFColor.BLACK.index);
            numerico.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            numerico.setRightBorderColor(HSSFColor.BLACK.index);
            numerico.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            numerico.setTopBorderColor   (HSSFColor.BLACK.index);
            numerico.setDataFormat(wb.createDataFormat().getFormat("###,##"));
            
            
            
            HSSFCellStyle numerico2 = wb.createCellStyle();
            numerico2.setFont(fuente4);
            numerico2.setFillForegroundColor(HSSFColor.WHITE.index);
            numerico2.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            numerico2.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            numerico2.setBottomBorderColor(HSSFColor.BLACK.index);
            numerico2.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            numerico2.setLeftBorderColor  (HSSFColor.BLACK.index);
            numerico2.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            numerico2.setRightBorderColor(HSSFColor.BLACK.index);
            numerico2.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            numerico2.setTopBorderColor   (HSSFColor.BLACK.index);
            numerico2.setDataFormat(wb.createDataFormat().getFormat("#0.0"));
            
             HSSFCellStyle moneda = wb.createCellStyle();
            moneda.setFont(fuente4);
            moneda.setFillForegroundColor(HSSFColor.WHITE.index);
            moneda.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            moneda.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            moneda.setBottomBorderColor(HSSFColor.BLACK.index);
            moneda.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            moneda.setLeftBorderColor  (HSSFColor.BLACK.index);
            moneda.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            moneda.setRightBorderColor(HSSFColor.BLACK.index);
            moneda.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            moneda.setTopBorderColor   (HSSFColor.BLACK.index);
            moneda.setDataFormat(wb.createDataFormat().getFormat("###,##"));
            
            
            
            HSSFCellStyle estilo5 = wb.createCellStyle();
            estilo5.setFont(fuente4);
            estilo5.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo5.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo5.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo5.setBottomBorderColor(HSSFColor.BLACK.index);
            estilo5.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo5.setLeftBorderColor  (HSSFColor.BLACK.index);
            estilo5.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo5.setRightBorderColor(HSSFColor.BLACK.index);
            estilo5.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo5.setTopBorderColor   (HSSFColor.BLACK.index);
           
            
            HSSFCellStyle estilo6 = wb.createCellStyle();
            estilo6.setFont(fuente4);
            estilo6.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo6.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo6.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo6.setBottomBorderColor(HSSFColor.BLACK.index);
            estilo6.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo6.setLeftBorderColor  (HSSFColor.BLACK.index);
            estilo6.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo6.setRightBorderColor(HSSFColor.BLACK.index);
            estilo6.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo6.setTopBorderColor   (HSSFColor.BLACK.index);
            
            
            
            /****************************************************/
            row  = sheet.createRow((short)(0));
            
            for (int j=0;j<10;j++) {
                cell = row.createCell((short)(j)); cell.setCellStyle(estilo1);
            }
            
            row  = sheet.createRow((short)(1));
            row  = sheet.createRow((short)(2));
            row  = sheet.createRow((short)(3));
            row  = sheet.createRow((short)(4));
            
            sheet.createFreezePane(0,5);
            
            row  = sheet.getRow((short)(0));
            cell = row.getCell((short)(0));
            cell.setCellValue("TRANSPORTE SANCHEZ POLO");
            
            
            row  = sheet.getRow((short)(1));
            cell = row.createCell((short)(0));
            cell.setCellStyle(estilo2);
            cell.setCellValue("Reporte de Planillas Sin Cumplido");
            
            /*************************************************************************************/
            int Fila = 4;
            
            row  = sheet.createRow((short)(Fila));
            
            sheet.setColumnWidth((short)0, (short)5000 );
            cell = row.createCell((short)(0));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Planilla");
            
            sheet.setColumnWidth((short)1, (short)5000 );
            cell = row.createCell((short)(1));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Generador OC");
            
            sheet.setColumnWidth((short)2, (short)5000 );
            cell = row.createCell((short)(2));
            cell.setCellStyle(estilo3);
            cell.setCellValue("OT");
            
            sheet.setColumnWidth((short)3, (short)4500 );
            cell = row.createCell((short)(3));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Valor OT");
            
            sheet.setColumnWidth((short)4, (short)5500 );
            cell = row.createCell((short)(4));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Fecha Generaci�n");            
            
            sheet.setColumnWidth((short)5, (short)5500 );
            cell = row.createCell((short)(5));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Hora Generaci�n");            
            
            sheet.setColumnWidth((short)6, (short)5500 );
            cell = row.createCell((short)(6));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Fecha Entrega");
            
            sheet.setColumnWidth((short)7, (short)5500 );
            cell = row.createCell((short)(7));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Hora Entrega");  
            
            sheet.setColumnWidth((short)8, (short)3500 );
            cell = row.createCell((short)(8));
            cell.setCellStyle(estilo3);
            cell.setCellValue("D�as De Generada");    
            
            sheet.setColumnWidth((short)9, (short)3500 );
            cell = row.createCell((short)(9));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Placa");
            
            sheet.setColumnWidth((short)10, (short)6000 );
            cell = row.createCell((short)(10));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Sede Pago");
            
            sheet.setColumnWidth((short)11, (short)7000 );
            cell = row.createCell((short)(11));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Origen");
            
            sheet.setColumnWidth((short)12, (short)7000 );
            cell = row.createCell((short)(12));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Destino");
            
            sheet.setColumnWidth((short)13, (short)5000 );
            cell = row.createCell((short)(13));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Agencia De Cumplido");
            
            sheet.setColumnWidth((short)14, (short)5000 );
            cell = row.createCell((short)(14));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Tipo De Viaje");
            
            sheet.setColumnWidth((short)15, (short)7000 );
            cell = row.createCell((short)(15));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Cliente");
            
            sheet.setColumnWidth((short)16, (short)4000 );
            cell = row.createCell((short)(16));
            cell.setCellStyle(estilo3);
            cell.setCellValue("C�digo Cliente");
            
            sheet.setColumnWidth((short)17, (short)5000 );
            cell = row.createCell((short)(17));
            cell.setCellStyle(estilo3);
            cell.setCellValue("No Factura");
            
            sheet.setColumnWidth((short)18, (short)5000 );
            cell = row.createCell((short)(18));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Valor Factura");
            
            sheet.setColumnWidth((short)19, (short)5000 );
            cell = row.createCell((short)(19));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Anticipo");
            
            sheet.setColumnWidth((short)20, (short)5500 );
            cell = row.createCell((short)(20));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Fecha Anticipo");
            
            sheet.setColumnWidth((short)21, (short)5000 );
            cell = row.createCell((short)(21));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Valor OC");
            
            sheet.setColumnWidth((short)22, (short)4000 );
            cell = row.createCell((short)(22));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Estado");
            
            sheet.setColumnWidth((short)23, (short)2500 );
            cell = row.createCell((short)(23));
            cell.setCellStyle(estilo3);
            cell.setCellValue("D�as");
            
            sheet.setColumnWidth((short)24, (short)2500 );
            cell = row.createCell((short)(24));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Cont. OC");
            
            sheet.setColumnWidth((short)25, (short)5000 );
            cell = row.createCell((short)(25));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Clasificaci�n");
            
            sheet.setColumnWidth((short)26, (short)2500 );
            cell = row.createCell((short)(26));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Calificaci�n De OC");
        
            Iterator It = this.Lista.iterator();
            while(It.hasNext()) {
                
                PlanillaSinCumplido pa = (PlanillaSinCumplido) It.next();
             
                Fila++;
                row  = sheet.createRow((short)(Fila));
                
                cell = row.createCell((short)(0));
                cell.setCellStyle(estilo4);
                cell.setCellValue((pa.getNumpla()!=null)?pa.getNumpla():"");
                
                cell = row.createCell((short)(1));
                cell.setCellStyle(estilo4);
                cell.setCellValue((pa.getGenerador_oc()!=null)?pa.getGenerador_oc():"");
                
                cell = row.createCell((short)(2));
                cell.setCellStyle(estilo4);
                cell.setCellValue((pa.getOt()!=null)?pa.getOt():"");
                
                cell = row.createCell((short)(3));
                cell.setCellStyle(moneda);               
                cell.setCellValue(pa.getValor_ot());
                
                cell = row.createCell((short)(4));
                cell.setCellStyle(estilo4);                
                String fechaGen = (pa.getFecha_gen()!=null)?pa.getFecha_gen():"" ;                              
                if (pa.getFecha_gen()!=null){                    
                    cell.setCellValue(fechaGen.substring(0,10));
                }else
                    cell.setCellValue("");
                
                
               cell = row.createCell((short)(5));
               cell.setCellStyle(estilo4);                
               String diasGen = (pa.getFecha_gen()!=null)?pa.getFecha_gen():"" ;                              
               if (pa.getFecha_gen()!=null){                   
                   cell.setCellValue(diasGen.substring(11,19));
               }else
                   cell.setCellValue("");
                
                cell = row.createCell((short)(6));
                cell.setCellStyle(estilo5);
                String fechaEnt = (pa.getFecha_ent()!=null)?pa.getFecha_ent():"";
                if (pa.getFecha_ent()!=null){                    
                    cell.setCellValue(fechaEnt.substring(0,10));
                }else 
                    cell.setCellValue("");
                
                cell = row.createCell((short)(7));
                cell.setCellStyle(estilo5);
                String diasEnt = (pa.getFecha_ent()!=null)?pa.getFecha_ent():"";
                if (pa.getFecha_ent()!=null){                    
                    cell.setCellValue(diasEnt.substring(11,19));
                }else 
                    cell.setCellValue("");
                
                
                cell = row.createCell((short)(8));
                cell.setCellStyle(numerico);
                cell.setCellValue(pa.getDiasDeGen()); 
                
                cell = row.createCell((short)(9));
                cell.setCellStyle(estilo5);
                cell.setCellValue((pa.getPlaca()!=null)?pa.getPlaca():"");         
                
                cell = row.createCell((short)(10));
                cell.setCellStyle(estilo4);
                cell.setCellValue((pa.getSede()!=null)?pa.getSede():"");
                
                cell = row.createCell((short)(11));
                cell.setCellStyle(estilo4);
                cell.setCellValue((pa.getOrigen()!=null)?pa.getOrigen():"");
                
                cell = row.createCell((short)(12));
                cell.setCellStyle(estilo6);
                cell.setCellValue((pa.getDestino()!=null)?pa.getDestino():"");
                
                cell = row.createCell((short)(13));
                cell.setCellStyle(estilo4);
                cell.setCellValue((pa.getAgencia_dest()!=null)?pa.getAgencia_dest() : "" );
                
                cell = row.createCell((short)(14));
                cell.setCellStyle(estilo4);
                cell.setCellValue((pa.getTipoviaje()!=null)?pa.getTipoviaje():"");
                             
                cell = row.createCell((short)(15));
                cell.setCellStyle(estilo4);
                cell.setCellValue((pa.getCliente()!=null)?pa.getCliente():"");
                
                cell = row.createCell((short)(16));
                cell.setCellStyle(estilo4);
                cell.setCellValue((pa.getCod_cliente()!=null)?pa.getCod_cliente():"");
                
                cell = row.createCell((short)(17));
                cell.setCellStyle(estilo4);
                cell.setCellValue((pa.getNo_factura()!=null)?pa.getNo_factura(): "");
                
                cell = row.createCell((short)(18));
                cell.setCellStyle(moneda);
                cell.setCellValue(pa.getValor_fact());               
                
                cell = row.createCell((short)(19));
                cell.setCellStyle(numerico);
                cell.setCellValue(pa.getAnticipo());
                
                cell = row.createCell((short)(20));
                cell.setCellStyle(estilo4);
                String fechaAnt = (pa.getFecha_anticipo()!=null)?pa.getFecha_anticipo():"";
                if (!pa.getFecha_anticipo().equals("")){                    
                    cell.setCellValue(fechaAnt.substring(0,10));                
                }else{                    
                    cell.setCellValue("");
                }
                
                cell = row.createCell((short)(21));
                cell.setCellStyle(moneda);
                cell.setCellValue(pa.getValor_oc());
                
                cell = row.createCell((short)(22));
                cell.setCellStyle(estilo6);
                cell.setCellValue((pa.getVacio()!=null)?pa.getVacio():"");
                
                cell = row.createCell((short)(23));
                cell.setCellStyle(estilo6);
                cell.setCellValue(pa.getDias());
                
                cell = row.createCell((short)(24));
                cell.setCellStyle(numerico);
                cell.setCellValue(pa.getContaroc());
                
                cell = row.createCell((short)(25));
                cell.setCellStyle(estilo6);
                cell.setCellValue((pa.getClasificacion()!=null)?pa.getClasificacion():"");
                
                cell = row.createCell((short)(26));
                cell.setCellStyle(numerico2);
                cell.setCellValue(pa.getCalificacion());
                          
                
            }
            model.LogProcesosSvc.finallyProceso( "Reporte Planillas sin Cumplido", this.hashCode(), usuario, "Proceso Exitoso" );
            /************************************************************************************/
            /**** GUARDAR DATOS EN EL ARCHIVO  ***/
            FileOutputStream fo = new FileOutputStream(Ruta);
            wb.write(fo);
            fo.close();
        }catch(Exception e){
            e.printStackTrace();
            try{
                model.LogProcesosSvc.finallyProceso( "Reporte palnillas sin Cumplido",this.hashCode(), usuario, "ERROR : " + e.getMessage() );
            }catch(Exception ex){
                ex.printStackTrace();
            }            
            ////System.out.println(e.getMessage());
        }finally{
            super.destroy();
          
        }     
       
    } 
  
}

