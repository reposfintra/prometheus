/*
 * Nombre        HMigrarFacturaMigracion.java
 * Autor         Osvaldo P�rez Ferrer
 * Fecha         4 de diciembre de 2006, 09:46 AM
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */

package com.tsp.operation.model.threads;


import java.util.*;
import java.io.*; 
import java.lang.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.*;
import com.tsp.operation.model.*;

public class HMigrarFacturaMigracion extends Thread{
    
    private String ruta;                                  
    //private String usuario;
    private String procesoName;   
    private String des;
    private Date d;
    
    private PrintWriter pw;
    private String path;
    private String filename;
    
    private Vector v;
    private String formato;
    private Usuario usuario;
    private String moneda;
    private String nombre_formato;
    //v, formato, usuario, moneda
    
    private LogWriter   logTrans;
    Model model;
    
    /**
     * Crea una nueva instancia de  HMigrarFacturaMigracion
     */
    public HMigrarFacturaMigracion() {
    }
    
    public void start( Usuario usuario, String formato, String moneda, Vector datos, Model m, String nombre_formato ) {
        
        this.procesoName = "Migracion de Facturas";
        this.des = "Genera facturas a partir de la tabla factura_migracion";
        this.usuario = usuario;        
        this.d = new Date();
        this.v = datos;
        this.formato = formato;
        this.moneda = moneda;
        this.model = m;        
        this.nombre_formato = nombre_formato;
        
        super.start();
    }
    
    public synchronized void run(){
        
        try{
            String fecha = "";
            
            Util u = new Util();
            String comentario="EXITOSO";
            model.LogProcesosSvc.InsertProceso( this.procesoName, this.hashCode(),"Migracion de Facturas", this.usuario.getLogin() );
            
            
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String ruta = rb.getString("ruta");
            path = ruta + "/exportar/migracion/" + this.usuario.getLogin();
            
            File archivo = new File( path );
            if (!archivo.exists()) archivo.mkdirs();
            
            initLog();
            
            String mens = model.formato_tablaService.insert( v, formato, usuario, moneda );
                                    
            logTrans.log("Formato: "+nombre_formato, logTrans.INFO);     
            logTrans.log("Registros a migrar  "+v.size(), logTrans.INFO);     
            
            String[] lineas = mens.split("\n");
            
            int cont = 0;
            
            for( int i=0; i<lineas.length; i++ ){
                ////System.out.println(lineas[i]);
                cont += lineas[i].indexOf("Insertado") != -1? 1 : 0;            
            }
            
            logTrans.log("Registros NO migrados  "+( (lineas.length-1)-cont ), logTrans.INFO);     
            
            for( int i=0; i<lineas.length; i++ ){
                logTrans.log(lineas[i], logTrans.INFO);     
            }
            
            
            closeLog(); // cerrando archivo de log
            model.LogProcesosSvc.finallyProceso(this.procesoName,this.hashCode(),  this.usuario.getLogin() ,comentario);
            
        }catch(Exception e){
            //System.out.println("Error en com.tsp.operation.model.threads.HMigrarFacturaMigracion" + e.getMessage());
            try{
                
                model.LogProcesosSvc.finallyProceso(this.procesoName,this.hashCode(),  this.usuario.getLogin() ,"ERROR Hilo: " + e.getMessage());
            }catch(Exception ex){}
            
        }
    }
    
    public void initLog()throws Exception{
        java.text.SimpleDateFormat fmt = new java.text.SimpleDateFormat("yyyyMMdd_hhmmss");
        pw     = new PrintWriter(new BufferedWriter(new FileWriter(path + "/logMigracionFacturas_"+ fmt.format(new Date()) +".txt")));
        logTrans = new LogWriter("Migracion_Facturas", LogWriter.INFO , pw );
        logTrans.log( "Proceso Inicializado", logTrans.INFO );
    }
    
    public void closeLog() throws Exception{
        logTrans.log( "Proceso Finalizado", logTrans.INFO );
        pw.close();
    }
    
}
