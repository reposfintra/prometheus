/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.threads;


import com.tsp.operation.model.Model;
import com.tsp.operation.model.beans.*;
import com.tsp.util.Util;

import java.util.*;
import java.text.*;




/**
 *
 * @author Alvaro
 */
public class HGenerarFacturaEca extends Thread {

    private Model model;
    private Usuario usuario;


    /** Creates a new instance of HPrefacturaDetalle */
    public HGenerarFacturaEca () {
    }

    public void start(Model model, Usuario usuario){

        this.usuario = usuario;
        this.model = model;

        super.start();
    }


    public synchronized void run(){
        try{
            //model.LogProcesosSvc.InsertProceso(this.processName, this.hashCode(), "Generacion del reporte de prefactura : " + prefactura, usuario.getLogin());


            Vector comandos_sql =new Vector();
            java.util.Date fechaActual = new Date();
            String creation_date = fechaActual.toString();
            String comandoSQL = "";
            String login = usuario.getLogin();

            Date fecha_hoy = new Date();
            DateFormat formato_fecha;
            formato_fecha = new SimpleDateFormat("yyyy-MM-dd");
            String fecha_factura = formato_fecha.format(fecha_hoy);
            Date fecha_pago = fecha_hoy ;
            String fecha_vencimiento = fecha_factura;
            String fecha_impresion   = fecha_factura;

            List listaPrefacturaEca =  model.applusService.buscaPrefacturaEca();

            if (listaPrefacturaEca.size() != 0){


                SerieGeneral serie = null;
                serie = new SerieGeneral();
                PrefacturaEca prefacturaEca =  new PrefacturaEca();

                Iterator it = listaPrefacturaEca.iterator();
                while (it.hasNext()) {

                  prefacturaEca = (PrefacturaEca)it.next();

                  serie = model.serieGeneralService.getSerie("FINV","OP","FACMS");
                  model.serieGeneralService.setSerie("FINV","OP","FACMS");

                  String documento_inicial = serie.getUltimo_prefijo_numero();

                  String nit = prefacturaEca.getNit() ;
                  String id_cliente = prefacturaEca.getId_cliente();
                  int  plazo = prefacturaEca.getPlazo() ;


                  // Calculo de la fecha de vencimiento de la factura
                  String fecha_factura_eca = prefacturaEca.getFecha_factura_eca();

                  Calendar calendarFechaVencimiento = Calendar.getInstance();
                  String ano = fecha_factura_eca.substring(0,4);
                  String mes = fecha_factura_eca.substring(5,7);
                  String dia = fecha_factura_eca.substring(8,10);

                  calendarFechaVencimiento.set(Integer.parseInt(ano),Integer.parseInt(mes)-1,Integer.parseInt(dia),8,0,0);

                  
                  String sFechaFacturaVencimientoInicial=formato_fecha.format(calendarFechaVencimiento.getTime()) ;
                  calendarFechaVencimiento.add(Calendar.DATE, -30);

                  String cmc        = prefacturaEca.getCmc() ;
                  int  id_orden     = prefacturaEca.getId_orden() ;
                  String num_os     = prefacturaEca.getNum_os();
                  int cuotas_reales = prefacturaEca.getCuotas_reales();
                  String simbolo_variable     = prefacturaEca.getSimbolo_variable();
                  String descripcion_factura  = "Factura Eca   NUMERO OS: " + num_os + "   ORDEN: " + id_orden +
                                                "   SIMBOLO VARIABLE: " + simbolo_variable;
                  
                  // Busqueda de la lista de valores correspondientes al total prev1 por contratista
                  
                  List listaPrefacturaEcaContratista = model.applusService.buscaPrefacturaEcaContratista(id_orden);
                  
                  
                  

                  double e_total_prev1 = prefacturaEca.getE_total_prev1();
                  double e_iva_total_prev1 = prefacturaEca.getE_iva_total_prev1();
                                    
                  double e_comision_applus = prefacturaEca.getE_comision_applus();
                  double e_comision_provintegral = prefacturaEca.getE_comision_provintegral();
                  double e_comision_factoring_fintra = prefacturaEca.getE_comision_factoring_fintra();
                  double e_comision_fintra = prefacturaEca.getE_comision_fintra();
                  double e_iva_comision_applus_total = prefacturaEca.getE_iva_comision_applus_total();
                  double e_comision_eca = prefacturaEca.getE_comision_eca();
                  double e_iva_comision_eca = prefacturaEca.getE_iva_comision_eca();
                  double e_cuota_pago = prefacturaEca.getE_cuota_pago();
                  double e_iva_comision_provintegral = prefacturaEca.getE_iva_comision_provintegral();
                  double e_iva_bonificacion = prefacturaEca.getE_iva_bonificacion();//visto en 20100105
                  System.out.println("prefacturaeca_e_iva_bonificacion:"+e_iva_bonificacion);
                  // VALORES TOTALES A COBRAR AL CLIENTE

                  // valor a cobrar al cliente sin los intereses de financiacion

                  double total_liquidacion_original = e_total_prev1 + e_iva_total_prev1 + e_comision_applus +
                                                      e_comision_provintegral + e_comision_factoring_fintra +
                                                      e_comision_fintra +
                                                      e_iva_comision_applus_total + e_comision_eca +
                                                      e_iva_comision_eca +e_iva_comision_provintegral 
                                                        + e_iva_bonificacion ;  // apabon 20091229 y 20100105




                  double total_liquidacion = total_liquidacion_original;
                  // valor a cobrar al cliente con los intereses de finaciacion
                  double e_total_financiacion_original = prefacturaEca.getE_total_financiacion();
                  double e_total_financiacion          = e_total_financiacion_original;
                  // valor de los intereses
                  double financiacion_original = e_total_financiacion - total_liquidacion;       // financiacion = intereses
                  double financiacion          = financiacion_original;

                  double valor_factura = 0;

                  // REVALORIZACION DE LOS VALORES A COBRAR SI esquema financiacion es = VIEJO
                  double diferencia      = 0;

                  double ec_cuota_pago          = prefacturaEca.getEc_financiacion_fintra();      // Cuota a pagar con el sistema  VIEJO
                  double ec_total_financiacion  = prefacturaEca.getEc_total_financiacion();       // Valor a cobrar al cliente con intereses con el sistema VIEJO
                  double ec_financiacion_fintra = prefacturaEca.getEc_financiacion_fintra();      // Valor a cobrar al cliente sin intereses sin el sistema VIEJO
                  double ec_intereses           = ec_total_financiacion - ec_financiacion_fintra; // Intereses

                  boolean generar_factura_diferencia = false;

                  // Recalculando valores si existen valores en la liquidacion con el sistema VIEJO
                  if( (ec_financiacion_fintra != 0) && ( (total_liquidacion - ec_financiacion_fintra)!= 0 ) ) {
                      generar_factura_diferencia = true;
                      diferencia           = total_liquidacion - ec_financiacion_fintra;          // Diferencia entre ambas liquidacions en el valor sin intereses
                      total_liquidacion    = total_liquidacion - diferencia;                      // Asignando nuevo valor ya que se introduce un nuevo item a la factura
                      e_total_financiacion = ec_total_financiacion;                               // Al cambiar el valor de la liquidacion a la del sistema VIEJO, el valor con lo intereses es igual a la del sistema VIEJO
                      financiacion         = ec_intereses;                                        // Los intereses son iguales a los del sistema VIEJO
                  }


                  // Divide los valores segun el numero de cuotas

                  double item_total_prev1               = Util.redondear2(e_total_prev1/cuotas_reales ,0) ;
                  double item_iva_total_prev1           = Util.redondear2(e_iva_total_prev1/cuotas_reales , 0) ;

                  double item_iva_bonificacion = Util.redondear2(e_iva_bonificacion/cuotas_reales , 0) ;//20100105

                  double item_comision_applus           = Util.redondear2(e_comision_applus/cuotas_reales ,0) ;
                  double item_comision_provintegral     = Util.redondear2(e_comision_provintegral/cuotas_reales ,0) ;
                  double item_comision_factoring_fintra = Util.redondear2(e_comision_factoring_fintra/cuotas_reales ,0) ;
                  double item_comision_fintra           = Util.redondear2(e_comision_fintra/cuotas_reales ,0) ;
                  double item_iva_comision_applus_total = Util.redondear2(e_iva_comision_applus_total/cuotas_reales ,0) ;
                  double item_iva_comision_provintegral = Util.redondear2(e_iva_comision_provintegral/cuotas_reales ,0) ;
                  

                  double item_comision_eca              = Util.redondear2(e_comision_eca/cuotas_reales ,0) ;
                  double item_iva_comision_eca          = Util.redondear2(e_iva_comision_eca/cuotas_reales ,0) ;


                  double item_financiacion              = Util.redondear2(financiacion/cuotas_reales, 0);

                  double item_diferencia                = -Util.redondear2(diferencia/cuotas_reales, 0);



                  // Inicializando variables que totalizan los items de todas las facturas

                  double total_item_total_prev1               = 0.0 ;
                  double total_item_iva_total_prev1           = 0.0 ;

                  double total_item_iva_bonificacion          = 0.0 ;//20100105

                  double total_item_comision_applus           = 0.0 ;
                  double total_item_comision_provintegral     = 0.0 ;
                  double total_item_comision_factoring_fintra = 0.0 ;
                  double total_item_comision_fintra           = 0.0 ;
                  double total_item_iva_comision_applus_total = 0.0 ;
                  double total_item_comision_eca              = 0.0 ;
                  double total_item_iva_comision_eca          = 0.0 ;
                  double total_item_financiacion              = 0.0 ;
                  double total_item_diferencia                = 0.0 ;
                  double total_item_iva_comision_provintegral = 0.0 ;
                  

                  String descripcion = "";


                  // Actualiza la oferta con el numero de la factura eca
                  comandoSQL = model.applusService.setOferta(id_orden, documento_inicial, fecha_factura+" 00:00:00");
                  comandos_sql.add(comandoSQL);

                  // Generar un numero de facturas igual al numero de cuotas reales

                  for(int i = 1 ;i < cuotas_reales ; i++) {

                      String documento = documento_inicial + "_" + Integer.toString(i);


                      valor_factura = item_total_prev1  + item_iva_total_prev1  + item_comision_applus +
                                      item_comision_provintegral  +  item_comision_factoring_fintra +
                                      item_comision_fintra   +  item_iva_comision_applus_total  +
                                      item_comision_eca  +  item_iva_comision_eca  +  item_financiacion +
                                      /*item_diferencia +090706*/ item_iva_comision_provintegral
                                      +item_iva_bonificacion;//20100105
                      /*ystem.out.println("valor_factura"+valor_factura+"_"+item_total_prev1  +"_"+ item_iva_total_prev1  +"_"+ item_comision_applus +"_"+
                                      item_comision_provintegral  +"_"+  item_comision_factoring_fintra +"_"+
                                      item_comision_fintra   +"_"+  item_iva_comision_applus_total  +"_"+
                                      item_comision_eca  +"_"+  item_iva_comision_eca  +"_"+  item_financiacion +"_"+
                                      item_diferencia +"_"+ item_iva_comision_provintegral);*/
                      

                      int k = 0;

                      
                      
                      
                      
                      
                      // Items de la factura
                      if (item_total_prev1 != 0) {
                          descripcion = "Material, mano obra, otros y aiu";
                          
                          double item_total_prev1_teorico=0;
                          int contador=0;
                          
                          PrefacturaEcaContratista prefacturaEcaContratista =  new PrefacturaEcaContratista();
                          
                          Iterator it1 = listaPrefacturaEcaContratista.iterator();
                          while (it1.hasNext()) {
                              
                              prefacturaEcaContratista = (PrefacturaEcaContratista)it1.next();                          

                              double valorContratista = prefacturaEcaContratista.getE_total_prev1();
                              String nombre_contratista = prefacturaEcaContratista.getNombre_contratista();
                              String nit_contratista    = prefacturaEcaContratista.getNit();
                              
                              double itemValorContratista = Util.redondear2(valorContratista/cuotas_reales ,0) ;
                              
                              contador++;
                              if (contador==listaPrefacturaEcaContratista.size()){
                                itemValorContratista=item_total_prev1-item_total_prev1_teorico;
                              }
                              item_total_prev1_teorico=item_total_prev1_teorico+itemValorContratista;
                              
                              k++;
                              comandoSQL = model.applusService.setFacturaDetalle( "FINV", "FAC",  documento,
                                                             k,  nit,  "080", descripcion + "  " + descripcion_factura + "   " + nombre_contratista,
                                                             "28050601", 1.0,
                                                             itemValorContratista , itemValorContratista , itemValorContratista,
                                                             itemValorContratista , 1.0, "PES",
                                                             creation_date, login, creation_date,
                                                             login, "COL", nit_contratista);
                              comandos_sql.add(comandoSQL);
                          }
                      }

                      
                      System.out.println("item_iva_total_prev1"+item_iva_total_prev1);
                      if (item_iva_total_prev1 != 0) {
                          descripcion = "Iva del material, mano obra, otros y aiu";

                          PrefacturaEcaContratista prefacturaEcaContratista =  new PrefacturaEcaContratista();

                          Iterator it2 = listaPrefacturaEcaContratista.iterator();
                          
                          int tamañolistaPrefacturaEcaContratista=listaPrefacturaEcaContratista.size();
                          int contador=0;  
                          double teorico_item_iva_total_prev1=0;
                          System.out.println("it2");
                          while (it2.hasNext()) {
                              contador=contador+1;
                              System.out.println("contador"+contador);
                              prefacturaEcaContratista = (PrefacturaEcaContratista)it2.next();                          

                              double ivaValorContratista = prefacturaEcaContratista.getE_iva_total_prev1();
                              String nombre_contratista  = prefacturaEcaContratista.getNombre_contratista();
                              String nit_contratista     = prefacturaEcaContratista.getNit();
                              double bonificacion        = prefacturaEcaContratista.getBonificacion();
                              double iva                 = prefacturaEca.getE_porcentaje_iva();

                              double iva_bonificacion    = prefacturaEcaContratista.getE_iva_bonificacion();
                              System.out.println("iva_bonificacion"+iva_bonificacion);
                              
                              double itemIvaValorContratista = Util.redondear2(ivaValorContratista/cuotas_reales ,0) ;


                              // Adiciona un item por el valor del iva asociado a la bonificacion y se le descuenta en primera cuota
                              // al item del iva asociado a materiales etc.
                              if ((i==1)&& ( iva_bonificacion != 0) ){
                                  double itemIvaBonificacion = iva_bonificacion;
                                  //itemIvaValorContratista = itemIvaValorContratista /* -  itemIvaBonificacion */;//20100105

                                  //teorico_item_iva_total_prev1=teorico_item_iva_total_prev1 /*+itemIvaBonificacion*/;//20100105

                                  k++;
                                  comandoSQL = model.applusService.setFacturaDetalle( "FINV", "FAC",  documento,
                                                             k,  nit,  "097",
                                                             "Iva bonificacion, Base :"+ Util.FormatoMiles(bonificacion) +"  "+  descripcion_factura + "   " + nombre_contratista,
                                                             "28050601", 1.0,
                                                             itemIvaBonificacion , itemIvaBonificacion , itemIvaBonificacion,
                                                             itemIvaBonificacion , 1.0, "PES",
                                                             creation_date, login, creation_date,
                                                             login, "COL", nit_contratista);//20100104
                                  comandos_sql.add(comandoSQL);
                              }

                              

                              /*  Fuentes originales

                              // Adiciona un item por el valor del iva asociado a la bonificacion y se le descuenta en primera cuota
                              // al item del iva asociado a materiales etc.
                              if ((i==1)&& ( bonificacion * iva != 0) ){
                                  double itemIvaBonificacion = bonificacion * iva;
                                  itemIvaValorContratista = itemIvaValorContratista -  itemIvaBonificacion ;
                                  
                                  teorico_item_iva_total_prev1=teorico_item_iva_total_prev1+itemIvaBonificacion;
                                  
                                  k++;
                                  comandoSQL = model.applusService.setFacturaDetalle( "FINV", "FAC",  documento,
                                                             k,  nit,  "097",  
                                                             "Iva bonificacion, Base :"+ Util.FormatoMiles(bonificacion) +"  "+  descripcion_factura + "   " + nombre_contratista,
                                                             "27050555", 1.0,
                                                             itemIvaBonificacion , itemIvaBonificacion , itemIvaBonificacion,
                                                             itemIvaBonificacion , 1.0, "PES",
                                                             creation_date, login, creation_date,
                                                             login, "COL", nit_contratista);
                                  comandos_sql.add(comandoSQL);
                              }

                              */




                              k++;
                              
                              if (contador==tamañolistaPrefacturaEcaContratista){
                                  itemIvaValorContratista=item_iva_total_prev1-teorico_item_iva_total_prev1;
                              }
                              
                              teorico_item_iva_total_prev1=teorico_item_iva_total_prev1+itemIvaValorContratista;
                              comandoSQL = model.applusService.setFacturaDetalle( "FINV", "FAC",  documento,
                                                             k,  nit,  "081", descripcion + "  " + descripcion_factura + "   " + nombre_contratista,
                                                             "28050601", 1.0,
                                                             itemIvaValorContratista , itemIvaValorContratista , itemIvaValorContratista,
                                                             itemIvaValorContratista , 1.0, "PES",
                                                             creation_date, login, creation_date,
                                                             login, "COL", nit_contratista);
                              comandos_sql.add(comandoSQL);




                          }
                      }
                      
                      
                      
                      
                      
                      
                      
                      
                      
                      if (item_comision_applus != 0){
                          descripcion = "Applus";
                          k++;
                          comandoSQL = model.applusService.setFacturaDetalle( "FINV", "FAC",  documento,
                                                             k,  nit,  "082", descripcion + "  " + descripcion_factura,
                                                             "23050703", 1.0,
                                                             item_comision_applus , item_comision_applus , item_comision_applus,
                                                             item_comision_applus , 1.0, "PES",
                                                             creation_date, login, creation_date,
                                                             login, "COL", nit);
                          comandos_sql.add(comandoSQL);
                      }

                      /*090701
                      if (generar_factura_diferencia){
                          descripcion = "Diferencia en el valor a financiar";
                          k++;
                          comandoSQL = model.applusService.setFacturaDetalle( "FINV", "FAC",  documento,
                                                             k,  nit,  "091", descripcion + "  " + descripcion_factura,
                                                             "13109702", 1.0,
                                                             item_diferencia , item_diferencia , item_diferencia,
                                                             item_diferencia , 1.0, "PES",
                                                             creation_date, login, creation_date,
                                                             login, "COL", nit);
                          comandos_sql.add(comandoSQL);
                      }
                      */



                      if (item_comision_provintegral != 0){
                          descripcion = "Provintegral";
                          k++;
                          comandoSQL = model.applusService.setFacturaDetalle( "FINV", "FAC",  documento,
                                                             k,  nit,  "083", descripcion + "  " + descripcion_factura,
                                                             "28150502", 1.0,
                                                             item_comision_provintegral , item_comision_provintegral , item_comision_provintegral,
                                                             item_comision_provintegral , 1.0, "PES",
                                                             creation_date, login, creation_date,
                                                             login, "COL", nit);
                          comandos_sql.add(comandoSQL);
                      }

                      
                      if (item_iva_comision_provintegral != 0){
                          descripcion = "Iva Provintegral";
                          k++;
                          comandoSQL = model.applusService.setFacturaDetalle( "FINV", "FAC",  documento,
                                                             k,  nit,  "096", descripcion + "  " + descripcion_factura,
                                                             "28150502", 1.0,
                                                             item_iva_comision_provintegral , item_iva_comision_provintegral , item_iva_comision_provintegral,
                                                             item_iva_comision_provintegral , 1.0, "PES",
                                                             creation_date, login, creation_date,
                                                             login, "COL", nit);
                          comandos_sql.add(comandoSQL);
                      }                      
                      
                      
                      
                      
                      if (item_iva_comision_applus_total != 0){
                          descripcion = "Iva Applus";
                          k++;
                          comandoSQL = model.applusService.setFacturaDetalle( "FINV", "FAC",  documento,
                                                             k,  nit,  "084", descripcion + "  " + descripcion_factura,
                                                             "23050703", 1.0,
                                                             item_iva_comision_applus_total , item_iva_comision_applus_total , item_iva_comision_applus_total,
                                                             item_iva_comision_applus_total , 1.0, "PES",
                                                             creation_date, login, creation_date,
                                                             login, "COL", nit);
                          comandos_sql.add(comandoSQL);
                      }


                      if (item_comision_eca != 0) {
                          descripcion = "Eca";
                          k++;
                          comandoSQL = model.applusService.setFacturaDetalle( "FINV", "FAC",  documento,
                                                             k,  nit,  "086", descripcion + "  " + descripcion_factura,
                                                             "23050705", 1.0,
                                                             item_comision_eca , item_comision_eca , item_comision_eca,
                                                             item_comision_eca , 1.0, "PES",
                                                             creation_date, login, creation_date,
                                                             login, "COL", nit);
                          comandos_sql.add(comandoSQL);
                      }

                      if (item_iva_comision_eca != 0) {
                          descripcion = "Iva Eca";
                          k++;
                          comandoSQL = model.applusService.setFacturaDetalle( "FINV", "FAC",  documento,
                                                             k,  nit,  "087", descripcion + "  " + descripcion_factura,
                                                             "23050705", 1.0,
                                                             item_iva_comision_eca , item_iva_comision_eca , item_iva_comision_eca,
                                                             item_iva_comision_eca , 1.0, "PES",
                                                             creation_date, login, creation_date,
                                                             login, "COL", nit);
                          comandos_sql.add(comandoSQL);
                      }


                      if (item_comision_factoring_fintra != 0) {
                          descripcion = "comision_factoring_fintra";
                          k++;
                          comandoSQL = model.applusService.setFacturaDetalle( "FINV", "FAC",  documento,
                                                             k,  nit,  "089", descripcion + "  " + descripcion_factura,
                                                             "27050553", 1.0,
                                                             item_comision_factoring_fintra , item_comision_factoring_fintra , item_comision_factoring_fintra,
                                                             item_comision_factoring_fintra , 1.0, "PES",
                                                             creation_date, login, creation_date,
                                                             login, "COL", nit);
                          comandos_sql.add(comandoSQL);
                      }

                      if (item_comision_fintra != 0){
                          descripcion = "comision_fintra";
                          k++;
                          comandoSQL = model.applusService.setFacturaDetalle( "FINV", "FAC",  documento,
                                                             k,  nit,  "090", descripcion + "  " + descripcion_factura,
                                                             "27050554", 1.0,
                                                             item_comision_fintra , item_comision_fintra , item_comision_fintra,
                                                             item_comision_fintra , 1.0, "PES",
                                                             creation_date, login, creation_date,
                                                             login, "COL", nit);
                          comandos_sql.add(comandoSQL);
                      }


                      if (item_financiacion != 0){
                          descripcion = "Financiacion Fintra";
                          k++;
                          comandoSQL = model.applusService.setFacturaDetalle( "FINV", "FAC",  documento,
                                                             k,  nit,  "088", descripcion + "  " + descripcion_factura,
                                                             "27050552", 1.0,
                                                             item_financiacion , item_financiacion , item_financiacion,
                                                             item_financiacion , 1.0, "PES",
                                                             creation_date, login, creation_date,
                                                             login, "COL", nit);
                          comandos_sql.add(comandoSQL);
                      }
                      

                      

                      // Cabecera de la factura
                      SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                      calendarFechaVencimiento.add(Calendar.DATE, 30);
                      String sFechaFacturaVencimiento = sdf.format(calendarFechaVencimiento.getTime()) ;


                      comandoSQL = model.applusService.setFactura("FINV", "FAC", documento,
                                   nit , id_cliente,"MU", fecha_factura, sFechaFacturaVencimiento, fecha_impresion,
                                   "Cuota " + Integer.toString(i) + ":  " + descripcion_factura,
                                   valor_factura, 0.0, valor_factura, valor_factura,
                                   0.0, valor_factura, 1.0, "PES", k,
                                   "CREDITO", "OP", "OP",
                                   "", "COL", creation_date, login, creation_date,
                                   login, cmc, "", "OP"
                                   ,"MS",num_os,"SV",simbolo_variable);

                      comandos_sql.add(comandoSQL);




                      // Suma de los items de todas las facturas

                      total_item_total_prev1               +=  item_total_prev1 ;
                      total_item_iva_total_prev1           +=  item_iva_total_prev1 ;

                      total_item_iva_bonificacion          +=  item_iva_bonificacion ;//20100105

                      total_item_comision_applus           +=  item_comision_applus;
                      total_item_comision_provintegral     +=  item_comision_provintegral;
                      total_item_iva_comision_provintegral +=  item_iva_comision_provintegral;
                      total_item_comision_factoring_fintra +=  item_comision_factoring_fintra;
                      total_item_comision_fintra           +=  item_comision_fintra;
                      total_item_iva_comision_applus_total +=  item_iva_comision_applus_total;

                      total_item_comision_eca              +=  item_comision_eca;
                      total_item_iva_comision_eca          +=  item_iva_comision_eca;
                      total_item_financiacion              +=  item_financiacion ;
                      total_item_diferencia                +=  item_diferencia;
                      

                  }


                  // Calculo de los items de la ultima factura


                  double ultimo_item_total_prev1               =  e_total_prev1 - total_item_total_prev1 ;
                  double ultimo_item_iva_total_prev1           =  e_iva_total_prev1 - total_item_iva_total_prev1 ;

                  double ultimo_item_iva_bonificacion           =  e_iva_bonificacion - total_item_iva_bonificacion ;//20100105

                  double ultimo_item_comision_applus           =  e_comision_applus -  total_item_comision_applus;
                  double ultimo_item_comision_provintegral     =  e_comision_provintegral -  total_item_comision_provintegral;
                  double ultimo_item_iva_comision_provintegral =  e_iva_comision_provintegral -  total_item_iva_comision_provintegral;
                  double ultimo_item_comision_factoring_fintra =  e_comision_factoring_fintra -  total_item_comision_factoring_fintra;
                  double ultimo_item_comision_fintra           =  e_comision_fintra -  total_item_comision_fintra;
                  double ultimo_item_iva_comision_applus_total =  e_iva_comision_applus_total - total_item_iva_comision_applus_total;

                  double ultimo_item_comision_eca              =  e_comision_eca - total_item_comision_eca;
                  double ultimo_item_iva_comision_eca          =  e_iva_comision_eca -  total_item_iva_comision_eca;
                  double ultimo_item_financiacion              =  financiacion - total_item_financiacion ;
                  double ultimo_item_diferencia                =  -(diferencia + total_item_diferencia);


                  // Calculo los valores de la cabecera de la ultima factura

                  valor_factura = ultimo_item_total_prev1  + ultimo_item_iva_total_prev1  + ultimo_item_comision_applus +
                                  ultimo_item_comision_provintegral  +  ultimo_item_comision_factoring_fintra +
                                  ultimo_item_comision_fintra   +  ultimo_item_iva_comision_applus_total  +
                                  ultimo_item_comision_eca  +  ultimo_item_iva_comision_eca  +  ultimo_item_financiacion +
                                  /*ultimo_item_diferencia  +090706*/ ultimo_item_iva_comision_provintegral
                                  +ultimo_item_iva_bonificacion;//20100105

                  /*ystem.out.println("valor_factura"+valor_factura+"_"+ultimo_item_total_prev1  +"_"+ ultimo_item_iva_total_prev1  +"_"+ ultimo_item_comision_applus 
                                  +"_"+ultimo_item_comision_provintegral  +"_"+  ultimo_item_comision_factoring_fintra +"_"+
                                  ultimo_item_comision_fintra   +"_"+  ultimo_item_iva_comision_applus_total  +"_"+
                                  ultimo_item_comision_eca  +"_"+  ultimo_item_iva_comision_eca  +"_"+  u*timo_item_financiacion +"_"+
                                  ultimo_item_diferencia  +"_"+ ultimo_item_iva_comision_provintegral);*/
                  // Cabecera de la ultima factura

                  String leyenda = "";
                  if (cuotas_reales == 1) {
                      leyenda = "Cuota unica:" ;
                  }
                  else {
                      leyenda = "Cuota " + Integer.toString(cuotas_reales);
                  }


                  String documento = documento_inicial + "_" + Integer.toString(cuotas_reales);

                   // Items de la ultima factura

                   int k = 0;
                   
                   
                      


                  // Ultimo Items de la factura
                  if (item_total_prev1 != 0) {
                      descripcion = "Material, mano obra, otros y aiu";

                      double item_total_prev1_teorico=0;
                      int contador=0;
                          
                      PrefacturaEcaContratista prefacturaEcaContratista =  new PrefacturaEcaContratista();

                      Iterator it3 = listaPrefacturaEcaContratista.iterator();
                      
                      while (it3.hasNext()) {

                          prefacturaEcaContratista = (PrefacturaEcaContratista)it3.next();                          

                          double valorContratista = prefacturaEcaContratista.getE_total_prev1();
                          String nombre_contratista = prefacturaEcaContratista.getNombre_contratista();
                          String nit_contratista    = prefacturaEcaContratista.getNit();

                          double itemValorContratista = Util.redondear2(valorContratista/cuotas_reales ,0)*(cuotas_reales-1) ;
                          double ultimoItemValorContratista = valorContratista - itemValorContratista;

                          contador++;   
                          if (contador==listaPrefacturaEcaContratista.size()){
                            ultimoItemValorContratista=ultimo_item_total_prev1-item_total_prev1_teorico;
                          }
                          item_total_prev1_teorico=item_total_prev1_teorico+ultimoItemValorContratista;
                          
                          k++;
                          comandoSQL = model.applusService.setFacturaDetalle( "FINV", "FAC",  documento,
                                                         k,  nit,  "080", descripcion + "  " + descripcion_factura + "   " + nombre_contratista,
                                                         "28050601", 1.0,
                                                         ultimoItemValorContratista , ultimoItemValorContratista , ultimoItemValorContratista,
                                                         ultimoItemValorContratista , 1.0, "PES",
                                                         creation_date, login, creation_date,
                                                         login, "COL", nit_contratista);
                          comandos_sql.add(comandoSQL);
                      }
                  }



                  if (item_iva_total_prev1 != 0) {
                      descripcion = "Iva del material, mano obra, otros y aiu";

                      PrefacturaEcaContratista prefacturaEcaContratista =  new PrefacturaEcaContratista();

                      Iterator it4 = listaPrefacturaEcaContratista.iterator();
                      
                      int tamañolistaPrefacturaEcaContratista=listaPrefacturaEcaContratista.size();
                      int contador=0;  
                      double teorico_item_iva_total_prev1=0;
                          
                      while (it4.hasNext()) {

                          contador=contador+1;
                          
                          prefacturaEcaContratista = (PrefacturaEcaContratista)it4.next();                          

                          double ivaValorContratista = prefacturaEcaContratista.getE_iva_total_prev1();
                          String nombre_contratista = prefacturaEcaContratista.getNombre_contratista();
                          String nit_contratista    = prefacturaEcaContratista.getNit();
                          
                          double itemIvaValorContratista = Util.redondear2(ivaValorContratista/cuotas_reales ,0)*(cuotas_reales-1) ;
                          double ultimoItemIvaValorContratista = ivaValorContratista - itemIvaValorContratista;                          

                          double bonificacion        = prefacturaEcaContratista.getBonificacion();
                          double iva                 = prefacturaEca.getE_porcentaje_iva();
                          double iva_bonificacion    = prefacturaEcaContratista.getE_iva_bonificacion();

                          //ystem.out.println("cuotasreales:"+cuotas_reales+"__bonificacion"+bonificacion+"__iva"+iva);
                          // Adiciona un item por el valor del iva asociado a la bonificacion y se le descuenta en primera cuota
                          // al item del iva asociado a materiales etc.
                          if ((cuotas_reales == 1)&& ( iva_bonificacion != 0) ){
                              double ultimoItemIvaBonificacion = iva_bonificacion;
                              //ultimoItemIvaValorContratista = ultimoItemIvaValorContratista -  ultimoItemIvaBonificacion ;//20100105
                                                            
                              //teorico_item_iva_total_prev1=teorico_item_iva_total_prev1+ultimoItemIvaBonificacion;//20100105
                              
                              k++;
                              comandoSQL = model.applusService.setFacturaDetalle( "FINV", "FAC",  documento,
                                                         k,  nit,  "097",
                                                         "Iva bonificacion, Base :"+ Util.FormatoMiles(bonificacion) + "  " +
                                                         descripcion_factura + "   " + nombre_contratista,
                                                         "28050601", 1.0,
                                                         ultimoItemIvaBonificacion , ultimoItemIvaBonificacion , ultimoItemIvaBonificacion,
                                                         ultimoItemIvaBonificacion , 1.0, "PES",
                                                         creation_date, login, creation_date,
                                                         login, "COL", nit_contratista);//20100104
                              comandos_sql.add(comandoSQL);
                          }
                          

                          k++;
                          if (contador==tamañolistaPrefacturaEcaContratista){
                                ultimoItemIvaValorContratista=ultimo_item_iva_total_prev1-teorico_item_iva_total_prev1;
                          }
                          
                          teorico_item_iva_total_prev1=teorico_item_iva_total_prev1+ultimoItemIvaValorContratista;
                          comandoSQL = model.applusService.setFacturaDetalle( "FINV", "FAC",  documento,
                                                         k,  nit,  "081", descripcion + "  " + descripcion_factura + "   " + nombre_contratista,
                                                         "28050601", 1.0,
                                                         ultimoItemIvaValorContratista , ultimoItemIvaValorContratista , ultimoItemIvaValorContratista,
                                                         ultimoItemIvaValorContratista , 1.0, "PES",
                                                         creation_date, login, creation_date,
                                                         login, "COL", nit_contratista);
                          comandos_sql.add(comandoSQL);
                      }
                  }


                   if (ultimo_item_comision_applus != 0){
                      descripcion = "Applus";
                      k++;
                      comandoSQL = model.applusService.setFacturaDetalle( "FINV", "FAC",  documento,
                                                         k,  nit,  "082", descripcion + "  " + descripcion_factura,
                                                         "23050703", 1.0,
                                                         ultimo_item_comision_applus , ultimo_item_comision_applus , ultimo_item_comision_applus,
                                                         ultimo_item_comision_applus , 1.0, "PES",
                                                         creation_date, login, creation_date,
                                                         login, "COL", "8305137738");
                      comandos_sql.add(comandoSQL);
                   }
                      /*090701
                      if (generar_factura_diferencia){
                          descripcion = "Diferencia en el valor a financiar";
                          k++;
                          comandoSQL = model.applusService.setFacturaDetalle( "FINV", "FAC",  documento,
                                                             k,  nit,  "091", descripcion + "  " + descripcion_factura,
                                                             "13109702", 1.0,
                                                             ultimo_item_diferencia , ultimo_item_diferencia , ultimo_item_diferencia,
                                                             ultimo_item_diferencia , 1.0, "PES",
                                                             creation_date, login, creation_date,
                                                             login, "COL", "8305137738");
                          comandos_sql.add(comandoSQL);
                      }
                      */

                   if (ultimo_item_comision_provintegral != 0) {
                      descripcion = "Provintegral";
                      k++;
                      comandoSQL = model.applusService.setFacturaDetalle( "FINV", "FAC",  documento,
                                                         k,  nit,  "083", descripcion + "  " + descripcion_factura,
                                                         "28150502", 1.0,
                                                         ultimo_item_comision_provintegral , ultimo_item_comision_provintegral , ultimo_item_comision_provintegral,
                                                         ultimo_item_comision_provintegral , 1.0, "PES",
                                                         creation_date, login, creation_date,
                                                         login, "COL", "9000742958");
                      comandos_sql.add(comandoSQL);
                   }

                   
                   if (ultimo_item_iva_comision_provintegral != 0) {
                      descripcion = "Iva Provintegral";
                      k++;
                      comandoSQL = model.applusService.setFacturaDetalle( "FINV", "FAC",  documento,
                                                         k,  nit,  "096", descripcion + "  " + descripcion_factura,
                                                         "28150502", 1.0,
                                                         ultimo_item_iva_comision_provintegral , ultimo_item_iva_comision_provintegral , ultimo_item_iva_comision_provintegral,
                                                         ultimo_item_iva_comision_provintegral , 1.0, "PES",
                                                         creation_date, login, creation_date,
                                                         login, "COL", "9000742958");
                      comandos_sql.add(comandoSQL);
                   }                   
                   
                   
                   

                    if (ultimo_item_iva_comision_applus_total != 0) {
                      descripcion = "Iva Applus";
                      k++;
                      comandoSQL = model.applusService.setFacturaDetalle( "FINV", "FAC",  documento,
                                                         k,  nit,  "084", descripcion + "  " + descripcion_factura,
                                                         "23050703", 1.0,
                                                         ultimo_item_iva_comision_applus_total , ultimo_item_iva_comision_applus_total , ultimo_item_iva_comision_applus_total,
                                                         ultimo_item_iva_comision_applus_total , 1.0, "PES",
                                                         creation_date, login, creation_date,
                                                         login, "COL", "8305137738");
                      comandos_sql.add(comandoSQL);
                    }


                    if (ultimo_item_comision_eca != 0) {
                      descripcion = "Eca";
                      k++;
                      comandoSQL = model.applusService.setFacturaDetalle( "FINV", "FAC",  documento,
                                                         k,  nit,  "086", descripcion + "  " + descripcion_factura,
                                                         "23050705", 1.0,
                                                         ultimo_item_comision_eca , ultimo_item_comision_eca , ultimo_item_comision_eca,
                                                         ultimo_item_comision_eca , 1.0, "PES",
                                                         creation_date, login, creation_date,
                                                         login, "COL", "8020076706");
                      comandos_sql.add(comandoSQL);
                    }


                    if(ultimo_item_iva_comision_eca != 0) {
                      descripcion = "Iva Eca";
                      k++;
                      comandoSQL = model.applusService.setFacturaDetalle( "FINV", "FAC",  documento,
                                                         k,  nit,  "087", descripcion + "  " + descripcion_factura,
                                                         "23050705", 1.0,
                                                         ultimo_item_iva_comision_eca , ultimo_item_iva_comision_eca , ultimo_item_iva_comision_eca,
                                                         ultimo_item_iva_comision_eca , 1.0, "PES",
                                                         creation_date, login, creation_date,
                                                         login, "COL", "8020076706");
                      comandos_sql.add(comandoSQL);
                    }

                   if (ultimo_item_comision_factoring_fintra != 0){
                      descripcion = "comision_factoring_fintra";
                      k++;
                      comandoSQL = model.applusService.setFacturaDetalle( "FINV", "FAC",  documento,
                                                         k,  nit,  "089", descripcion + "  " + descripcion_factura,
                                                         "27050553", 1.0,
                                                         ultimo_item_comision_factoring_fintra , ultimo_item_comision_factoring_fintra , ultimo_item_comision_factoring_fintra,
                                                         ultimo_item_comision_factoring_fintra , 1.0, "PES",
                                                         creation_date, login, creation_date,
                                                         login, "COL", "8305137738");
                      comandos_sql.add(comandoSQL);
                   }

                   if (ultimo_item_comision_fintra != 0) {
                      descripcion = "comision_fintra";
                      k++;
                      comandoSQL = model.applusService.setFacturaDetalle( "FINV", "FAC",  documento,
                                                         k,  nit,  "090", descripcion + "  " + descripcion_factura,
                                                         "27050554", 1.0,
                                                         ultimo_item_comision_fintra , ultimo_item_comision_fintra , ultimo_item_comision_fintra,
                                                         ultimo_item_comision_fintra , 1.0, "PES",
                                                         creation_date, login, creation_date,
                                                         login, "COL", "8305137738");
                      comandos_sql.add(comandoSQL);
                   }


                   if (ultimo_item_financiacion != 0) {
                      descripcion = "Financiacion Fintra"; 
                      String cuenta = "27050552";
                      String concepto = "088";
                      if  ((ultimo_item_financiacion  >= -6.0 ) && (ultimo_item_financiacion <= 6.0) ) {
                          descripcion =  "Diferencia por redondeo decena" ;
                          cuenta = "I010100054211";
                          concepto = "095";
                      }
                      
                      k++;
                      comandoSQL = model.applusService.setFacturaDetalle( "FINV", "FAC",  documento,
                                                         k,  nit,  concepto, descripcion + "  " + descripcion_factura,
                                                         cuenta, 1.0,
                                                         ultimo_item_financiacion , ultimo_item_financiacion , ultimo_item_financiacion,
                                                         ultimo_item_financiacion , 1.0, "PES",
                                                         creation_date, login, creation_date,
                                                         login, "COL", "");
                      comandos_sql.add(comandoSQL);
                   }

   
                   
                   
                   
                  // Cabecera de la ultima factura



                  calendarFechaVencimiento.add(Calendar.DATE, 30);
                  SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                  String sFechaFacturaVencimiento = sdf.format(calendarFechaVencimiento.getTime()) ;
 
                  comandoSQL = model.applusService.setFactura("FINV", "FAC", documento,
                               nit , id_cliente,"MU", fecha_factura, sFechaFacturaVencimiento, fecha_impresion,
                               leyenda + ":  " + descripcion_factura,
                               valor_factura, 0.0, valor_factura, valor_factura,
                               0.0, valor_factura, 1.0, "PES", k,
                               "CREDITO", "OP", "OP",
                               "", "COL", creation_date, login, creation_date,
                               login, cmc, "", "OP"
                               ,"MS",num_os,"SV",simbolo_variable);

                   comandos_sql.add(comandoSQL);
                   
                   
                   // Generando una factura adicional de diferencia en los valores a financiar
                   /*090701
                   if ((generar_factura_diferencia) && ( diferencia > 0) ) {
                       
                       
                       serie = model.serieGeneralService.getSerie("FINV","OP","FACDF");
                       model.serieGeneralService.setSerie("FINV","OP","FACDF");

                       documento= serie.getUltimo_prefijo_numero();   
                       
                       //nit e id_cliente se cambian para saber a quien cobrarle
                       // Cabecera de la factura de diferencia
                       comandoSQL = model.applusService.setFactura("FINV", "FAC", documento,
                                     "8305137738", "CL03223","MU", fecha_factura, sFechaFacturaVencimientoInicial, fecha_impresion,
                                    "Diferencia en el valor financiacion de : " + descripcion_factura,
                                    diferencia, 0.0, diferencia, diferencia,
                                    0.0, diferencia, 1.0, "PES", 1,
                                    "CREDITO", "OP", "OP",
                                    "", "COL", creation_date, login, creation_date,
                                    login, cmc, "", "OP"
                                    ,"DA",num_os,"SV",simbolo_variable);

                        comandos_sql.add(comandoSQL);  


                        // Item de la factura
                        
                        comandoSQL = model.applusService.setFacturaDetalle( "FINV", "FAC",  documento,
                                                         k,  "8305137738",  "091", "Diferencia en el valor financiacion de : " + descripcion_factura,
                                                         "13109702", 1.0,
                                                         diferencia , diferencia , diferencia,
                                                         diferencia , 1.0, "PES",
                                                         creation_date, login, creation_date,
                                                         login, "COL", nit);
                        comandos_sql.add(comandoSQL);                        
                       
                       
                       
                   }
                   */





                } // Final del while

                // Grabando todo a la base de datos.
                model.applusService.ejecutarSQL(comandos_sql);

            }

            //model.LogProcesosSvc.finallyProceso(this.processName, this.hashCode(), usuario.getLogin(), "PROCESO EXITOSO");
        }catch (Exception ex){
            ex.printStackTrace();
            try{
                //model.LogProcesosSvc.finallyProceso(this.processName, this.hashCode(), usuario.getLogin(), "Finalizado con error ...\n" + ex.getMessage());
            }catch (Exception e){
                System.out.println("Error HGenerarFacturaEca ...\n"  + e.getMessage());
            }
        }
    }
    
    
    public void expandirTotalPrev1(){
        
        
        
        
    }


    
    
    
    
    
}
