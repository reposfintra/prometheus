/*
 * ReporteDXPXSL.java
 *
 * Created on 3 de noviembre de 2005, 04:42 PM
 */

package com.tsp.operation.model.threads;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import java.io.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.controller.ReporteCumplidoAction;
import com.tsp.operation.model.services.*;
import com.tsp.operation.model.*;
import java.util.*;
import java.text.*;
/**
 *
 * @author  dlamadrid
 */
public class ReporteDXPXSL extends Thread{
    
        /** Creates a new instance of ReporteDXPXSL */
        public ReporteDXPXSL() {
        }
    
        private String mensaje="";
        String Agencia;
        String user;
        String Fecha;
        int rango_i;
        int rango_f;
        //ReportePlaRemPorFecha datos = new ReportePlaRemPorFecha();
        Model model;
        Vector reporte;
        
        public void start(Vector reporte,int rango_i,int rango_f,String Agencia,String user,String Fecha){
                ////System.out.println("Aqui toy en el start");
                this.reporte=reporte;
                this.rango_i=rango_i;
                this.rango_f=rango_f;
                this.Agencia = Agencia;
                this.user = user;
                this.Fecha = Fecha;
                super.start();
        }
    
        public synchronized void run(){
                ////System.out.println("Aqui toy en el run");
        
                try{
                        Model model = new Model();
                        ////System.out.println(" va a setear el vector");
                        if (reporte!=null && reporte.size()>0){
                                ////System.out.println("seteo el vector");
                                Calendar FechaHoy = Calendar.getInstance();
                                Date d = FechaHoy.getTime();
                                SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd-hh-mm");
                                SimpleDateFormat s1 = new SimpleDateFormat("yyyy-MM-dd-hh:mm");
                                String FechaFormated = s.format(d);
                                String FechaFormated1 = s1.format(d);


                                //obtener cabeera de ruta
                                ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
                                String path = rb.getString("ruta");
                                //armas la ruta
                                String Ruta1  = path + "/exportar/migracion/" + user + "/";
                                //  String Ruta1  ="c:exportar/";
                                //crear la ruta
                                File file = new File(Ruta1);
                                file.mkdirs();

                                POIWrite xls = new POIWrite(Ruta1+"ReporteDXP-"+user+Agencia+".xls",user,Fecha);

                                ////System.out.println("Ruta del Archivo xls " + Ruta1+"ReporteDXP-"+user+Agencia+".xls");
                                HSSFCellStyle fecha  = xls.nuevoEstilo("Book Antiqua", 9, false , false, "yyyy/mm/dd"  , xls.NONE , xls.NONE , xls.NONE );
                                HSSFCellStyle texto  = xls.nuevoEstilo("Book Antiqua", 9, false , false, "text"        , xls.NONE , xls.NONE , xls.NONE);
                                HSSFCellStyle numero = xls.nuevoEstilo("Book Antiqua", 9, false , false, ""            , xls.NONE , xls.NONE , xls.NONE);

                                HSSFCellStyle header      = xls.nuevoEstilo("Arial Black", 18, true  , false, "text"        , HSSFColor.ORANGE.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
                                HSSFCellStyle titulo      = xls.nuevoEstilo("Book Antiqua", 9, true  , false, "text"        , HSSFColor.BLACK.index , HSSFColor.PINK.index , HSSFCellStyle.ALIGN_CENTER);
                                HSSFCellStyle fechatitle  = xls.nuevoEstilo("Book Antiqua", 9, true  , false, "yyyy/mm/dd"  , HSSFColor.RED.index , xls.NONE, HSSFCellStyle.ALIGN_CENTER );


                                xls.obtenerHoja("BASE");
                                xls.cambiarMagnificacion(3,4);

                                // cabecera

                                xls.adicionarCelda(0,0, "REPORTE DOCUMENTOS POR PAGAR ", header);
                                xls.combinarCeldas(0, 0, 0, 2);

                                xls.adicionarCelda(1,0, "Rango Inicial :"   , fechatitle);
                                xls.adicionarCelda(1,1, ""+rango_i , fecha);
                                xls.adicionarCelda(1,2, "Rango Final Final :"   , fechatitle);
                                String r_f=""+rango_f;
                                if(rango_f==32){r_f="+30";}

                                xls.adicionarCelda(1,3, ""+r_f , fecha);
                                xls.adicionarCelda(2,0, "Agencia Despacho :"   , fechatitle);
                                xls.adicionarCelda(2,1, Agencia, texto);
                                xls.adicionarCelda(3,0, "Fecha y hora Proceso :"   , fechatitle);
                                xls.adicionarCelda(3,1, FechaFormated1, fecha);

                                // subtitulos


                                int fila = 6;
                                int col  = 0;

                                xls.adicionarCelda(fila ,col++ , "Proveedor"     , titulo );
                                xls.adicionarCelda(fila ,col++ , "Tipo de Documento"     , titulo );
                                xls.adicionarCelda(fila ,col++ , "Documento"     , titulo );
                                xls.adicionarCelda(fila ,col++ , "Banco"     , titulo );
                                xls.adicionarCelda(fila ,col++ , "Sucursal"     , titulo ); 
                                xls.adicionarCelda(fila ,col++ , "Fecha Documento"     , titulo );
                                xls.adicionarCelda(fila ,col++ , "Fecha vencimiento "     , titulo );
                                xls.adicionarCelda(fila ,col++ , "Dias "     , titulo );
                                xls.adicionarCelda(fila ,col++ , "No Vencido "     , titulo );
                                if((rango_i <=7)&&(rango_f>=1)){
                                        xls.adicionarCelda(fila ,col++ , "1-7 "     , titulo );
                                }
                                if((rango_i <=15)&&(rango_f>=8)){
                                        xls.adicionarCelda(fila ,col++ , "8-15 "     , titulo );
                                }
                                if((rango_i <=23)&&(rango_f>=16)){
                                        xls.adicionarCelda(fila ,col++ , "16-23 "     , titulo );
                                }
                                if((rango_i <=30)&&(rango_f>=24)){
                                        xls.adicionarCelda(fila ,col++ , "24-30 "     , titulo );
                                }
                                if(rango_f==32){
                                        xls.adicionarCelda(fila ,col++ , " +30"     , titulo );
                                }
                                // datos

                                int it = 0;
                                int i=0;

                                java.util.Date utilDate = new java.util.Date(); //fecha actual
                                long lnMilisegundos = utilDate.getTime();
                                java.sql.Timestamp sqlTimestamp = new java.sql.Timestamp(lnMilisegundos);
                                double vt0=0;
                                double vt1=0;
                                double vt2=0;
                                double vt3=0;
                                double vt4=0;
                                double vt5=0;
                                while(it < reporte.size()){
                                        double v0=0;
                                        double v1=0;
                                        double v2=0;
                                        double v3=0;
                                        double v4=0;
                                        double v5=0;
                                        long  dias=0;
                                        Vector vProveedor = (Vector)reporte.elementAt(it);
                                        for(int x=0;x <vProveedor.size();x++){
                                                col = 0;
                                                fila++;
                                                CXP_Doc doc1=(CXP_Doc)vProveedor.elementAt(x);

                                                ////System.out.println("Proveedor [" + x +"] "+ doc1.getProveedor() );
                                                xls.adicionarCelda(fila ,col++ , doc1.getProveedor() , texto );
                                                xls.adicionarCelda(fila ,col++ , model.cxpDocService.nombreDocumento(""+doc1.getTipo_documento()) , texto );
                                                xls.adicionarCelda(fila ,col++ , doc1.getDocumento(), texto );
                                                xls.adicionarCelda(fila ,col++ , doc1.getBanco(), texto );
                                                xls.adicionarCelda(fila ,col++ , doc1.getSucursal(), texto );
                                                xls.adicionarCelda(fila ,col++ , doc1.getFecha_documento() , fecha );
                                                xls.adicionarCelda(fila ,col++ , doc1.getFecha_vencimiento() ,fecha );
                                                java.sql.Timestamp fecha_v =java.sql.Timestamp.valueOf(doc1.getFecha_vencimiento());
                                                dias = model.cxpDocService.getDiasDiferencia(sqlTimestamp,fecha_v);
                                                xls.adicionarCelda(fila ,col++ ,""+dias,fecha );
                                                String d0="";
                                                String d1_7="";
                                                String d8_15="";
                                                String d16_23="";
                                                String d24_30="";
                                                String m_30="";
                                                if(dias <=0 ){
                                                        d0=""+doc1.getVlr_neto();
                                                        v0= v0+doc1.getVlr_neto();
                                                }
                                                if((dias >=1) && (dias <7)){
                                                        d1_7=""+doc1.getVlr_neto();
                                                        v1= v1+doc1.getVlr_neto();
                                                }
                                                if((dias >=8) && (dias <15)){
                                                        d8_15=""+doc1.getVlr_neto();
                                                        v2= v2+doc1.getVlr_neto();
                                                }
                                                if((dias >=16) && (dias <23)){
                                                        d16_23=""+doc1.getVlr_neto();
                                                        v3= v3+doc1.getVlr_neto();
                                                }
                                                if((dias >=24) && (dias <=30)){
                                                        d24_30=""+doc1.getVlr_neto();
                                                        v4= v4+doc1.getVlr_neto();
                                                }
                                                if(dias >30){
                                                        m_30 =""+doc1.getVlr_neto();
                                                        v5= v5+doc1.getVlr_neto();
                                                }
                                                xls.adicionarCelda(fila ,col++ , d0    , numero );
                                                if((rango_i <=7)&&(rango_f>=1)){
                                                        xls.adicionarCelda(fila ,col++ , d1_7    , numero );
                                                }
                                                if((rango_i <=15)&&(rango_f>=8)){
                                                        xls.adicionarCelda(fila ,col++ , d8_15     , numero );
                                                }
                                                if((rango_i <=23)&&(rango_f>=16)){
                                                        xls.adicionarCelda(fila ,col++ , d16_23     , numero );
                                                }
                                                if((rango_i <=30)&&(rango_f>=24)){
                                                        xls.adicionarCelda(fila ,col++ , d24_30     , numero );
                                                }
                                                if(rango_f==32){
                                                        xls.adicionarCelda(fila ,col++ ,  m_30      , numero );
                                                }
                                        }
                                        vt0=vt0+v0;
                                        vt1=vt1+v1;
                                        vt2=vt2+v2;
                                        vt3=vt3+v3;
                                        vt4=vt4+v4;
                                        vt5=vt5+v5;  
                                        it++;
                                }//Fin del While
                                col=0;
                                fila++;
                                xls.adicionarCelda(fila ,col++ , ""     , texto );
                                xls.adicionarCelda(fila ,col++ , ""     , texto );
                                xls.adicionarCelda(fila ,col++ , ""     , texto );
                                xls.adicionarCelda(fila ,col++ , ""     , texto );
                                xls.adicionarCelda(fila ,col++ , ""     , texto ); 
                                xls.adicionarCelda(fila ,col++ , ""     , texto );
                                xls.adicionarCelda(fila ,col++ , ""     , texto );
                                xls.adicionarCelda(fila ,col++ , "TOTAL "     , texto );
                                xls.adicionarCelda(fila ,col++ , ""+vt0     ,numero );
                                if((rango_i <=7)&&(rango_f>=1)){
                                        xls.adicionarCelda(fila ,col++ , ""+vt1     , numero );
                                }
                                if((rango_i <=15)&&(rango_f>=8)){
                                        xls.adicionarCelda(fila ,col++ , ""+vt2     , numero );
                                }
                                if((rango_i <=23)&&(rango_f>=16)){
                                        xls.adicionarCelda(fila ,col++ ,""+ vt3     , numero );
                                }
                                if((rango_i <=30)&&(rango_f>=24)){
                                        xls.adicionarCelda(fila ,col++ , ""+vt4    ,numero);
                                }
                                if(rango_f==32){
                                        xls.adicionarCelda(fila ,col++ , ""+vt5    , numero );
                                }
                                xls.cerrarLibro();
                                ////System.out.println("cerro el libro");
                        }
                        else{
                                //ReportePlaRemPorFechaXLS.setMensaje("El reporte no Arroj� datos");
                                this.setMensaje("El reporte no Arroj� datos");
                        }

                }//Fin del try
                catch (Exception ex){
                        ////System.out.println("Error : " + ex.getMessage());
                }
        }
        
        public void setMensaje(String Mensaje){

        this.mensaje = Mensaje;

        }

        public String getMensaje(){
        return mensaje;
        }
        /*public static void main(String []args)throws Exception{
        ReportePlaRemPorFechaXLS hilo = new ReportePlaRemPorFechaXLS() ;
        hilo.start("2005-05-21","2005-05-21","CG");

        }*/  
}
