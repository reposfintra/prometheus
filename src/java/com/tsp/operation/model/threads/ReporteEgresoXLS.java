/*
 * ReporteEgreso.java
 *
 * Created on 13 de septiembre de 2005, 03:42 PM
 */

package com.tsp.operation.model.threads;

import java.util.*;
import java.io.*;
import java.lang.*;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import com.tsp.operation.model.beans.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.util.*;
import com.tsp.operation.model.*;
/**
 *
 * @author  Administrador
 */
public class ReporteEgresoXLS extends Thread{
    
    private List ListaRE;
    private List ListaBancos;
    private String fechainicial;
    private String fechafinal;
    private String id;
    private String path;
    private com.tsp.operation.model.Model model;
    private String procesoName;
    private String des;
    private String user;
    /** Creates a new instance of ReporteEgreso */
    public ReporteEgresoXLS() {
    }
    
    public void start(List ListaRE, List ListaBancos ,String fechai, String fechaf, String id) {
        this.ListaRE = ListaRE;
        this.ListaBancos = ListaBancos;
        this.fechainicial = fechai;
        this.fechafinal = fechaf;
        this.id = id;
        this.procesoName = "Reporte Egreso";
        this.des = "Exportacion de Reporte de Egreso a Excel";
        this.user = id;
        super.start();
    }
    
    public synchronized void run(){
        try{
            
            Util u = new Util();
            model = new Model();
            
            model.LogProcesosSvc.InsertProceso( this.procesoName, this.hashCode(),this.des, this.user );
            
            //sandrameg 190905
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            path = rb.getString("ruta");
            
            File file = new File(path + "/exportar/migracion/"+id);
            file.mkdirs();
            
            
            String nombreArch= "ReporteEgreso["+this.fechainicial+"]["+this.fechafinal+"].xls";
            String       Hoja  = "ReporteEgreso";
            String       Ruta  = path + "/exportar/migracion/"+ id +"/" + nombreArch; //System.out.println("RUTA " + Ruta);
            HSSFWorkbook wb    = new HSSFWorkbook();
            HSSFSheet    sheet = wb.createSheet(Hoja);
            HSSFRow      row   = null;
            HSSFRow      row2  = null;
            HSSFCell     cell  = null;
            
            for (int col=0; col<40 ; col++)
                sheet.setColumnWidth( (short) col, (short) ( ( 50 * 8 ) / ( (double) 1 / 20 ) ) );
            
            /****  ENCABEZADO Y DEFINICION DE ESTILOS ************************************************/
            
            /** ENCABEZADO GENERAL *******************************/
            HSSFFont  fuente1 = wb.createFont();
            fuente1.setFontName("verdana");
            fuente1.setFontHeightInPoints((short)(16)) ;
            fuente1.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            fuente1.setColor((short)(0x1));
            
            HSSFCellStyle estilo1 = wb.createCellStyle();
            estilo1.setFont(fuente1);
            estilo1.setFillForegroundColor(HSSFColor.ORANGE.index);
            estilo1.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            
            /** TEXTO EN EL ENCABEAZADO *************************/
            HSSFFont  fuente2 = wb.createFont();
            fuente2.setFontName("verdana");
            fuente2.setFontHeightInPoints((short)(11)) ;
            fuente2.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            fuente2.setColor((short)(0x0));
            
            HSSFCellStyle estilo2 = wb.createCellStyle();
            estilo2.setFont(fuente2);
            estilo2.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo2.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            
            /** ENCABEZADO DE LAS COLUMNAS***********************/
            HSSFFont  fuente3 = wb.createFont();
            fuente3.setFontName("verdana");
            fuente3.setFontHeightInPoints((short)(11)) ;
            fuente3.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            fuente3.setColor((short)(0x1));
            
            HSSFCellStyle estilo3 = wb.createCellStyle();
            estilo3.setFont(fuente3);
            estilo3.setFillForegroundColor(HSSFColor.SEA_GREEN.index);
            estilo3.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo3.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo3.setBottomBorderColor(HSSFColor.BLACK.index);
            estilo3.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo3.setLeftBorderColor  (HSSFColor.BLACK.index);
            estilo3.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo3.setRightBorderColor(HSSFColor.BLACK.index);
            estilo3.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo3.setTopBorderColor   (HSSFColor.BLACK.index);
            
            /** TEXTO NORMAL ************************************/
            HSSFFont  fuente4 = wb.createFont();
            fuente4.setFontName("verdana");
            fuente4.setFontHeightInPoints((short)(9)) ;
            fuente4.setBoldweight(HSSFFont.BOLDWEIGHT_NORMAL);
            fuente4.setColor((short)(0x0));
            
            HSSFCellStyle estilo4 = wb.createCellStyle();
            estilo4.setFont(fuente4);
            estilo4.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo4.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo4.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo4.setBottomBorderColor(HSSFColor.BLACK.index);
            estilo4.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo4.setLeftBorderColor  (HSSFColor.BLACK.index);
            estilo4.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo4.setRightBorderColor(HSSFColor.BLACK.index);
            estilo4.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo4.setTopBorderColor   (HSSFColor.BLACK.index);
            
            
            HSSFCellStyle estilo5 = wb.createCellStyle();
            estilo5.setFont(fuente4);
            estilo5.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo5.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo5.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo5.setBottomBorderColor(HSSFColor.BLACK.index);
            estilo5.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo5.setLeftBorderColor  (HSSFColor.BLACK.index);
            estilo5.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo5.setRightBorderColor(HSSFColor.BLACK.index);
            estilo5.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo5.setTopBorderColor   (HSSFColor.BLACK.index);
            estilo5.setDataFormat(wb.createDataFormat().getFormat("###,##"));
            
            
            
            HSSFCellStyle estilo6 = wb.createCellStyle();
            estilo6.setFont(fuente3);
            estilo6.setFillForegroundColor(HSSFColor.SEA_GREEN.index);
            estilo6.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo6.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo6.setBottomBorderColor(HSSFColor.BLACK.index);
            estilo6.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo6.setLeftBorderColor  (HSSFColor.BLACK.index);
            estilo6.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo6.setRightBorderColor(HSSFColor.BLACK.index);
            estilo6.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo6.setTopBorderColor   (HSSFColor.BLACK.index);
            estilo6.setAlignment(HSSFCellStyle.ALIGN_LEFT);
            //   style.setDataFormat(wb.createDataFormat().getFormat(formato));
            
            /****************************************************/
            
            //sheet.createFreezePane(0,5);
            
            
            row  = sheet.createRow((short)(0));
            
            for (int j=0;j<7;j++) {
                cell = row.createCell((short)(j)); cell.setCellStyle(estilo1);
            }
            
            row  = sheet.createRow((short)(1));
            row  = sheet.createRow((short)(2));
            row  = sheet.createRow((short)(3));
            row  = sheet.createRow((short)(4));
            
            
            
            row  = sheet.getRow((short)(0));
            cell = row.getCell((short)(0));
            cell.setCellValue("TRANSPORTE SANCHEZ POLO");
            
            
            row  = sheet.getRow((short)(1));
            cell = row.createCell((short)(0));
            cell.setCellStyle(estilo2);
            cell.setCellValue("Relacion de Egreso");
            
            /*************************************************************************************/
            
            /***** RECORRER LAS PLANILLAS ******/
            int Fila = 4;
            
            Iterator It = ListaBancos.iterator();
            while(It.hasNext()) {
                Banco banco = (Banco) It.next();
                /* BANCO */
                
                sheet.addMergedRegion(new Region(Fila-1,(short)(0) ,Fila-1,(short)(10)));
                
                row2  = sheet.createRow((short)(Fila-1));
                cell = row2.createCell((short)(0));
                cell.setCellStyle(estilo6);
                cell.setCellValue("BANCO" + " " + banco.getBanco() + " " + banco.getBank_account_no());
                
                row  = sheet.createRow((short)(Fila));
                
                //sheet.setColumnWidth((short)0, (short)12000 );
                cell = row.createCell((short)(0));
                cell.setCellStyle(estilo3);
                cell.setCellValue("BANCO");
                
                //sheet.setColumnWidth((short)1, (short)12000 );
                cell = row.createCell((short)(1));
                cell.setCellStyle(estilo3);
                cell.setCellValue("SUCURSAL");
                
                //sheet.setColumnWidth((short)2, (short)12000 );
                //cell = row.createCell((short)(2));
                //cell.setCellStyle(estilo3);
                //cell.setCellValue("PLANILLA");
                
                cell = row.createCell((short)(2));
                cell.setCellStyle(estilo3);
                cell.setCellValue("CHEQUE");
                
                cell = row.createCell((short)(3));
                cell.setCellStyle(estilo3);
                cell.setCellValue("VALOR CHEQUE EN PESOS");
                
                cell = row.createCell((short)(4));
                cell.setCellStyle(estilo3);
                cell.setCellValue("VALOR CHEQUE");
                
                cell = row.createCell((short)(5));
                cell.setCellStyle(estilo3);
                cell.setCellValue("MONEDA CHEQUE");
                
                cell = row.createCell((short)(6));
                cell.setCellStyle(estilo3);
                cell.setCellValue("BENEFICIARIO");
                
                cell = row.createCell((short)(7));
                cell.setCellStyle(estilo3);
                cell.setCellValue("FECHA CHEQUE");
                
                cell = row.createCell((short)(8));
                cell.setCellStyle(estilo3);
                cell.setCellValue("DESPACHADOR");
                
                cell = row.createCell((short)(9));
                cell.setCellStyle(estilo3);
                cell.setCellValue("PROVEEDOR");
                
                cell = row.createCell((short)(10));
                cell.setCellStyle(estilo3);
                cell.setCellValue("ESTADO");
                
                int sw = 0;
                Iterator It2 = ListaRE.iterator();
                while(It2.hasNext()) {
                    ReporteEgreso re = (ReporteEgreso) It2.next();
                    if(re.getBanco().equals(banco.getBanco()) && re.getBancoCuenta().equals(banco.getBank_account_no())){
                        
                        if( model.tblgensvc.existeRegistro("USUEGRESOS", this.user )){
                            
                            if( re.getFecha_reporte().equals("0099-01-01 00:00:00") ){
                                
                                sw = 1;
                                Fila++;
                                
                                row  = sheet.createRow((short)(Fila));
                                
                                cell = row.createCell((short)(0));
                                cell.setCellStyle(estilo4);
                                cell.setCellValue((re.getBanco()!=null)?re.getBanco():"");
                                
                                cell = row.createCell((short)(1));
                                cell.setCellStyle(estilo4);
                                cell.setCellValue((re.getBancoCuenta()!=null)?re.getBancoCuenta():"");
                                
                                //cell = row.createCell((short)(2));
                                //cell.setCellStyle(estilo4);
                                //cell.setCellValue((re.getNumeroPlanilla()!=null)?re.getNumeroPlanilla():"");
                                
                                cell = row.createCell((short)(2));
                                cell.setCellStyle(estilo4);
                                cell.setCellValue((re.getNumeroChk()!=null)?re.getNumeroChk():"");
                                
                                cell = row.createCell((short)(3));
                                cell.setCellStyle(estilo5);
                                cell.setCellValue(re.getValorChk());
                                
                                cell = row.createCell((short)(4));
                                cell.setCellStyle(estilo5);
                                cell.setCellValue(re.getVlr_for());
                                
                                cell = row.createCell((short)(5));
                                cell.setCellStyle(estilo5);
                                cell.setCellValue((re.getMoneda()!=null)?re.getMoneda():"");
                                
                                cell = row.createCell((short)(6));
                                cell.setCellStyle(estilo4);
                                cell.setCellValue((re.getBeneficiario()!=null)?re.getBeneficiario():"");
                                
                                cell = row.createCell((short)(7));
                                cell.setCellStyle(estilo4);
                                cell.setCellValue((re.getFechaChk()!=null)?re.getFechaChk():"");
                                
                                cell = row.createCell((short)(8));
                                cell.setCellStyle(estilo4);
                                cell.setCellValue((re.getDespachador()!=null)?re.getDespachador():"");
                                
                                cell = row.createCell((short)(9));
                                cell.setCellStyle(estilo4);
                                cell.setCellValue((re.getProveedor()!=null)?re.getProveedor():"");
                                
                                cell = row.createCell((short)(10));
                                cell.setCellStyle(estilo4);
                                cell.setCellValue((re.getReg_status()!=null)?re.getReg_status().equals("A")?"ANULADO":"" :"");
                                
                                model.ReporteEgresoSvc.updateEgreso(re.getNumeroChk(),re.getBanco(),re.getBancoCuenta(), this.user); //Actualiazion fechaReporte
                                
                            }//Fin de validacion de Fecha_reporte
                           /* else{
                                row  = sheet.createRow((short)(Fila));
                                cell = row.createCell((short)(0));
                                cell.setCellStyle(estilo4);
                                cell.setCellValue("NO SE ENCONTRARON DATOS PARA ESTE BANCO O LOS REGISTROS YA FUERON GENERADOS ANTERIORMENTE");
                            }*/
                            
                        }//Fin de validacion de Usuario Tabla_gen
                        else{
                            
                            sw = 1;
                            Fila++;
                            
                            row  = sheet.createRow((short)(Fila));
                            
                            cell = row.createCell((short)(0));
                            cell.setCellStyle(estilo4);
                            cell.setCellValue((re.getBanco()!=null)?re.getBanco():"");
                            
                            cell = row.createCell((short)(1));
                            cell.setCellStyle(estilo4);
                            cell.setCellValue((re.getBancoCuenta()!=null)?re.getBancoCuenta():"");
                            
                            //cell = row.createCell((short)(2));
                            //cell.setCellStyle(estilo4);
                            //cell.setCellValue((re.getNumeroPlanilla()!=null)?re.getNumeroPlanilla():"");
                            
                            cell = row.createCell((short)(2));
                            cell.setCellStyle(estilo4);
                            cell.setCellValue((re.getNumeroChk()!=null)?re.getNumeroChk():"");
                            
                            cell = row.createCell((short)(3));
                            cell.setCellStyle(estilo5);
                            cell.setCellValue(re.getValorChk());
                            
                            cell = row.createCell((short)(4));
                            cell.setCellStyle(estilo5);
                            cell.setCellValue(re.getVlr_for());
                            
                            cell = row.createCell((short)(5));
                            cell.setCellStyle(estilo5);
                            cell.setCellValue((re.getMoneda()!=null)?re.getMoneda():"");
                            
                            cell = row.createCell((short)(6));
                            cell.setCellStyle(estilo4);
                            cell.setCellValue((re.getBeneficiario()!=null)?re.getBeneficiario():"");
                            
                            cell = row.createCell((short)(7));
                            cell.setCellStyle(estilo4);
                            cell.setCellValue((re.getFechaChk()!=null)?re.getFechaChk():"");
                            
                            cell = row.createCell((short)(8));
                            cell.setCellStyle(estilo4);
                            cell.setCellValue((re.getDespachador()!=null)?re.getDespachador():"");
                            
                            cell = row.createCell((short)(9));
                            cell.setCellStyle(estilo4);
                            cell.setCellValue((re.getProveedor()!=null)?re.getProveedor():"");
                            
                            cell = row.createCell((short)(10));
                            cell.setCellStyle(estilo4);
                            cell.setCellValue((re.getReg_status()!=null)?re.getReg_status().equals("A")?"ANULADO":"" :"");
                            
                            
                        }//Fin de Else de no Usuarios Tabla_gen
                        
                    }// If de Bancos
                    
                }//Fin del Reporte
                
                if( sw == 0 ){
                    
                    Fila++;
                    row  = sheet.createRow((short)(Fila));
                    cell = row.createCell((short)(0));
                    cell.setCellStyle(estilo4);
                    cell.setCellValue("NO SE ENCONTRARON DATOS PARA ESTE BANCO O LOS REGISTROS YA FUERON GENERADOS ANTERIORMENTE");
                    
                }
                Fila+=2;
                
            }// Fin de las Bancos
            
            
            System.out.println("Generacion de Archivo");
            
            /************************************************************************************/
            /**** GUARDAR DATOS EN EL ARCHIVO  ***/
            FileOutputStream fo = new FileOutputStream(Ruta);
            wb.write(fo);
            fo.close();
            
            
            model.LogProcesosSvc.finallyProceso(this.procesoName,this.hashCode(),  this.user ,"Proceso Exitoso");
            
        }catch(Exception e){
            e.printStackTrace();
            try{
                model.LogProcesosSvc.finallyProceso(this.procesoName,this.hashCode(),  this.user ,"ERROR HILO : " + e.getMessage());
            }catch(Exception ex){}
        }finally{
            super.destroy();
        }
    }
    
}
