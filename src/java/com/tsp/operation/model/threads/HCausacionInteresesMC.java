/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.threads;

import com.tsp.operation.model.Model;
import com.tsp.operation.model.TransaccionService;
import com.tsp.operation.model.beans.BeanGeneral;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.util.Util;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

/**
 *
 * @author geotech
 */
/**
 *
 * @author Ing. Iris Vargas
 */
public class HCausacionInteresesMC extends Thread {

    private int caso = 0;
    private String ciclo="";
    private String periodo="";
    Model model;
    Usuario usuario;

    public HCausacionInteresesMC(Usuario usuario) {
        this.usuario = usuario;
    }

    public void start(int caso) throws Exception {
        try {
            this.caso = caso;
            super.start();
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }
    
    public void start(int caso,String ciclo,String periodo) throws Exception {
        try {
            this.caso = caso;
            this.ciclo=ciclo;
            this.periodo=periodo;
            super.start();
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    public synchronized void run() {

        try {

            model = new Model(usuario.getBd());


            switch (caso) {
                
                case 1:

                    try {
                        ArrayList<BeanGeneral> list = model.Negociossvc.datosNegInteresMC();
                        TransaccionService tService = new TransaccionService(usuario.getBd());

                        for (int i = 0; i < list.size(); i++) {
                            double valor = Double.parseDouble(list.get(i).getValor_05()) - Double.parseDouble(list.get(i).getValor_06());
                            tService.crearStatement();
                            String documento=model.Negociossvc.UpCP(list.get(i).getValor_01());
                            
                            list.get(i).setValor_19(usuario.getLogin());
                            
                            //Creacion de las Facturas    
                            tService.getSt().addBatch(model.Negociossvc.ingresarCXCMicrocreditoPost(list.get(i), valor, documento));
                            tService.getSt().addBatch(model.Negociossvc.ingresarDetalleCXCMicrocreditoPost(list.get(i), valor, documento,"1"));
                            
                            valor = Double.parseDouble(list.get(i).getValor_06()) + valor;
                            
                            //Actualiza la tabla documentos_neg_aceptados -> Interes causado.
                            tService.getSt().addBatch(model.Negociossvc.updateInteresCausado(valor, list.get(i).getValor_12(), list.get(i).getValor_13(), list.get(i).getValor_07()));
                            
                            tService.execute();
                            
                            System.out.println("DIA VENCIMIENTO - CXC INTERES MICROCREDITO DEL NEGOCIO " + list.get(i).getValor_13() + " INSERTADA.");
                        }
                    } catch (Exception e) {
                        System.out.print("ERROR Hilo CASO1: " + e.getMessage());
                    }
                    break;
                    
                case 2: generarMIMicrocreditoMes(); 
                    break;                    
                //opcion nueva para generar intereses microcredito
                case 3: 
                    generarMIMicrocredito();
                    break;
            }

        } catch (Exception e) {

            System.out.print("ERROR Hilo: " + e.getMessage());

        }


    }
    
    private void generarMIMicrocredito() {
        try {
            ArrayList<BeanGeneral> list = model.Negociossvc.datosNegInteresMIMC(ciclo, periodo);
            TransaccionService tService = new TransaccionService(usuario.getBd());

            for (int i = 0; i < list.size(); i++) {
                double valor = Double.parseDouble(list.get(i).getValor_05()) - Double.parseDouble(list.get(i).getValor_06());
                tService.crearStatement();
                String documento = model.Negociossvc.UpCP(list.get(i).getValor_01());

                list.get(i).setValor_19(usuario.getLogin());

                //Creacion de las Facturas    
                tService.getSt().addBatch(model.Negociossvc.ingresarCXCMicrocreditoPost(list.get(i), valor, documento));
                tService.getSt().addBatch(model.Negociossvc.ingresarDetalleCXCMicrocreditoPost(list.get(i), valor, documento, "1"));

                valor = Double.parseDouble(list.get(i).getValor_06()) + valor;

                //Actualiza la tabla documentos_neg_aceptados -> Interes causado.
                tService.getSt().addBatch(model.Negociossvc.updateInteresCausado(valor, list.get(i).getValor_12(), list.get(i).getValor_13(), list.get(i).getValor_07()));

                tService.execute();

                System.out.println("DIA VENCIMIENTO - CXC INTERES MICROCREDITO DEL NEGOCIO " + list.get(i).getValor_13() + " INSERTADA.");
            }
        } catch (Exception e) {
            System.out.print("ERROR Hilo CASO3: " + e.getMessage());
        }

    }
    
    private void generarMIMicrocreditoMes() {
        try {
            ArrayList<BeanGeneral> list = model.Negociossvc.datosNegInteresMCFMesPC();
            TransaccionService tService = new TransaccionService(usuario.getBd());

            for (int i = 0; i < list.size(); i++) {
                double valor = Double.parseDouble(list.get(i).getValor_05()) - Double.parseDouble(list.get(i).getValor_06());
                tService.crearStatement();
                String documento = model.Negociossvc.UpCP(list.get(i).getValor_01());

                list.get(i).setValor_19(usuario.getLogin());

                //Creacion de las Facturas    
                tService.getSt().addBatch(model.Negociossvc.ingresarCXCMicrocreditoPost(list.get(i), valor, documento));
                tService.getSt().addBatch(model.Negociossvc.ingresarDetalleCXCMicrocreditoPost(list.get(i), valor, documento, "1"));

                valor = Double.parseDouble(list.get(i).getValor_06()) + valor;

                //Actualiza la tabla documentos_neg_aceptados -> Interes causado.
                tService.getSt().addBatch(model.Negociossvc.updateInteresCausado(valor, list.get(i).getValor_12(), list.get(i).getValor_13(), list.get(i).getValor_07()));

                tService.execute();

                System.out.println("DIA VENCIMIENTO - CXC INTERES MICROCREDITO DEL NEGOCIO " + list.get(i).getValor_13() + " INSERTADA.");
            }
        } catch (Exception e) {
            System.out.print("ERROR Hilo CASO3: " + e.getMessage());
        }

    }
}
