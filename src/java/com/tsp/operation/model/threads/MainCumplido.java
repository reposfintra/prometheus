/*
 * MigracionCambioCheque.java
 *
 * Created on 20 de octubre de 2005, 05:07 PM
 */

package com.tsp.operation.model.threads;

import java.text.*;

import java.util.Date;
import java.text.*;
import java.sql.SQLException;

import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;

import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.util.*;
import java.util.*;
import java.io.*;
/**
 *
 * @author  JuanM
 */
public class MainCumplido extends Thread{
    private String user;
    /** Creates a new instance of MigracionCambioCheque */
    public MainCumplido() {
    }
    
    public void start(){
        super.start();
    }
    
    public synchronized void run(){
        try{
            Model model = new Model();
            model.cumplidoService.ReporteCumplidos();
            
        }catch(Exception e){
            ////System.out.println(e.getMessage());
        }finally{
            super.destroy();
        }
    }
    
    public static void main(String a []){
        MainCumplido hilo = new MainCumplido();
        hilo.start();
    }
    
}
