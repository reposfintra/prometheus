package com.tsp.operation.model.threads;

import com.itextpdf.text.*;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.WSHistCreditoService;
import com.tsp.util.Util;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.ResourceBundle;
import java.util.TreeMap;

/**
 * Hilo para generar el PDF con la historia de credito para una persona
 * 14/12/2011
 * @author darrieta
 */
public class HHistoriaDeCredito extends Thread {
    
    private WSHistCreditoService srv;
    private Usuario usuario;
    private String tipoIdentificacion;
    private String identificacion;
    private String ruta;
    private Font fTitulo;
    private Font fSubTitulo;
    private Font fEncabezado;
    private Font fNormal;
    private Font fmonospace;
    private BaseColor cTitulo;//Verde oscuro
    private BaseColor cEncabezado;//Gris claro
    private String nitEmpresa;
    private String vista;
         
    public void start(Usuario usuario, String tipoIdentificacion, String identificacion, String vista) {
        this.usuario = usuario;
        this.tipoIdentificacion = tipoIdentificacion.trim();
        this.identificacion = identificacion;
        this.vista=vista;
        this.srv = new WSHistCreditoService(usuario.getBd());
        super.start();
    }
    
    @Override
    public synchronized void run() {
        try {
            generarPDF();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Metodo para obtener la ruta donde se va a generar el Archivo
     * @author Ing. Fabian Diaz A - GEOTECH
     * @date 04/03/2010
     * @version 1.0
     * @throws Exception
     */
    public void generarRUTA() throws Exception {
        try {
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            this.ruta = rb.getString("ruta") + "/exportar/migracion/" + usuario.getLogin();
            File archivo = new File(this.ruta);
            if (!archivo.exists()) {
                archivo.mkdirs();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
    }
    
    
    public void generarPDF() throws Exception{
        generarRUTA();
        Document document=new Document(PageSize.LETTER, 30, 30, 25, 25);
        PdfWriter.getInstance(document, new FileOutputStream(ruta+"/HC_"+identificacion+".pdf"));
        document.open();
        document.addTitle("Historia de credito");
        document.addAuthor("FINTRA");
        
        
        //Fuentes
        fTitulo = new Font(FontFamily.HELVETICA, 12, Font.BOLD, BaseColor.WHITE);
        fSubTitulo = new Font(FontFamily.HELVETICA, 9, Font.NORMAL, BaseColor.WHITE);
        fEncabezado = new Font(FontFamily.HELVETICA, 7, Font.BOLD, new BaseColor(0, 108, 58));
        fNormal = new Font(FontFamily.HELVETICA, 7);
        fmonospace = new Font(FontFamily.COURIER, 7);
        
        //Colores
        cTitulo = new BaseColor(0, 108, 58); //Verde oscuro
        cEncabezado = new BaseColor(0xE8, 0xE8, 0xE8); //Gris claro
        
        //Se consulta la informaci\u00f3n basica de la persona
        Persona persona = null;
        if (vista.equals("7")) {//fenalco atlantico o fintra

            persona = srv.consultarPersonaDecisor(identificacion, tipoIdentificacion, "8904800244");


        } else {//fenalco bolivar

            persona = srv.consultarPersona(identificacion, tipoIdentificacion, vista.equals("2") ? true : false);

        }
        
       
        nitEmpresa=persona.getNitEmpresa();
        
        Paragraph nombre = new Paragraph("\nHISTORIA DE CR\u00c9DITO\n");
        nombre.setAlignment(Element.ALIGN_CENTER);
        nombre.getFont().setStyle(Font.BOLD);
        document.add(nombre);
        
        if(persona!=null){
            Phrase msg = new Phrase("\nFecha consulta: "+Util.formatoTimestamp(persona.getUltima_hc(), 1));
            document.add(msg);
            
            agregarInformacionBasica(document, persona);
            agregarAlertas(document);
            agregarComentarios(document);
            agregarScores(document);
            agregarTotales(document);
            agregarObligacionesAbiertas(document);
            agregarObligacionesCerradas(document);
            agregarEndeudamientoGlobal(document);
            agregarReclamos(document);
            agregarConsultas(document);
        }else{
            Paragraph msg = new Paragraph("\nNO SE ENCONTRO INFORMACION PARA LA IDENTIFICACION INGRESADA "+identificacion);
            msg.setAlignment(Element.ALIGN_CENTER);
            document.add(msg);
        }
        
        document.close();
    }

    
    private void agregarInformacionBasica(Document document, Persona persona) throws DocumentException, Exception{

        PdfPTable table = new PdfPTable(10);
        table.setWidthPercentage(100);
        table.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);

        PdfPCell cell = new PdfPCell();
        cell.setPhrase(new Phrase("INFORMACI\u00d3N B\u00c1SICA", fTitulo));
        cell.setBackgroundColor(cTitulo);
        cell.setPadding(3);
        cell.setBorderColor(cTitulo);
        cell.setColspan(10);
        table.addCell(cell);

        cell = new PdfPCell();
        cell.setPhrase(new Phrase("Tipo documento", fEncabezado));
        cell.setBorderColor(BaseColor.GRAY);
        table.addCell(cell);

        cell = new PdfPCell();
        cell.setPhrase(new Phrase(persona.getTipoIdentificacion(), fNormal));
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setBorderColor(BaseColor.GRAY);
        table.addCell(cell);

        cell = new PdfPCell();
        cell.setPhrase(new Phrase("Num documento", fEncabezado));
        cell.setBorderColor(BaseColor.GRAY);
        table.addCell(cell);

        cell = new PdfPCell();
        cell.setPhrase(new Phrase(identificacion, fNormal));
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setBorderColor(BaseColor.GRAY);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase("Estado documento", fEncabezado));
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setBorderColor(BaseColor.GRAY);
        table.addCell(cell);

        cell = new PdfPCell();
        cell.setPhrase(new Phrase(persona.getEstadoId(), fNormal));
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setBorderColor(BaseColor.GRAY);
        table.addCell(cell);

        cell = new PdfPCell();
        cell.setPhrase(new Phrase("Lugar expedici\u00f3n", fEncabezado));
        cell.setBorderColor(BaseColor.GRAY);
        table.addCell(cell);

        cell = new PdfPCell();
        cell.setPhrase(new Phrase(persona.getCiudadId()+" ["+persona.getDepartamentoId()+"]", fNormal));
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setBorderColor(BaseColor.GRAY);
        table.addCell(cell);

        cell = new PdfPCell(new Phrase("Fecha expedici\u00f3n", fEncabezado));
        cell.setBorderColor(BaseColor.GRAY);
        table.addCell(cell);

        cell = new PdfPCell();
        cell.setPhrase(new Phrase(Util.formatoTimestamp(persona.getFechaExpedicionId(), 1), fNormal));
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setBorderColor(BaseColor.GRAY);
        table.addCell(cell);

        cell = new PdfPCell();
        cell.setPhrase(new Phrase("Nombre", fEncabezado));
        cell.setBorderColor(BaseColor.GRAY);
        table.addCell(cell);

        cell = new PdfPCell();
        cell.setPhrase(new Phrase(persona.getNombreCompleto(), fNormal));
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setColspan(3);
        cell.setBorderColor(BaseColor.GRAY);
        table.addCell(cell);

        cell = new PdfPCell();
        cell.setPhrase(new Phrase("Rango edad", fEncabezado));
        cell.setBorderColor(BaseColor.GRAY);
        table.addCell(cell);

        cell = new PdfPCell();
        cell.setPhrase(new Phrase(persona.getEdadMin()+"-"+persona.getEdadMax(), fNormal));
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setBorderColor(BaseColor.GRAY);
        table.addCell(cell);

        cell = new PdfPCell();
        cell.setPhrase(new Phrase("Genero", fEncabezado));
        cell.setBorderColor(BaseColor.GRAY);
        table.addCell(cell);

        cell = new PdfPCell();
        cell.setPhrase(new Phrase(persona.getGenero(), fNormal));
        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cell.setBorderColor(BaseColor.GRAY);
        table.addCell(cell);

        cell = new PdfPCell();
        cell.setPhrase(new Phrase("", fNormal));
        cell.setBorderColor(BaseColor.GRAY);
        table.addCell(cell);

        cell = new PdfPCell();
        cell.setPhrase(new Phrase("", fNormal));
        cell.setBorderColor(BaseColor.GRAY);
        table.addCell(cell);

        table.setSpacingAfter(10);
        document.add(table);
    }
    
    private void agregarComentarios(Document document) throws DocumentException, Exception{
        ArrayList<ComentarioInforme> comentarios = srv.consultarComentarios(identificacion, tipoIdentificacion, nitEmpresa);
        if(comentarios.size()>0){
            PdfPTable table = new PdfPTable(3);
            table.setWidthPercentage(100);
            table.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);

            PdfPCell cell = new PdfPCell();
            cell.setPhrase(new Phrase("COMENTARIOS", fTitulo));
            cell.setBackgroundColor(cTitulo);
            cell.setPadding(3);
            cell.setBorderColor(cTitulo);
            cell.setColspan(3);
            table.addCell(cell);
            
            cell = new PdfPCell(new Phrase("Tipo", fEncabezado));
            cell.setBorderColor(BaseColor.GRAY);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("Fecha vencimiento", fEncabezado));
            cell.setBorderColor(BaseColor.GRAY);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("Texto", fEncabezado));
            cell.setBorderColor(BaseColor.GRAY);
            table.addCell(cell);

            for (int i = 0; i < comentarios.size(); i++) {
                ComentarioInforme comentario = comentarios.get(i);

                cell = new PdfPCell(new Phrase(comentario.getTipo(), fNormal));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setBorderColor(BaseColor.GRAY);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase(Util.formatoTimestamp(comentario.getFechaVencimiento(),1), fNormal));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setBorderColor(BaseColor.GRAY);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase(comentario.getTexto(), fNormal));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setBorderColor(BaseColor.GRAY);
                table.addCell(cell);
            }
            
            table.setSpacingAfter(10);
            document.add(table);
        }
    }
    
    private void agregarAlertas(Document document) throws DocumentException, Exception{
        ArrayList<Alerta> alertas = srv.consultarAlertas(identificacion, tipoIdentificacion, nitEmpresa);
        if(alertas.size()>0){
            PdfPTable table = new PdfPTable(3);
            table.setWidthPercentage(100);
            table.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);

            PdfPCell cell = new PdfPCell();
            cell.setPhrase(new Phrase("ALERTAS", fTitulo));
            cell.setBackgroundColor(cTitulo);
            cell.setPadding(3);
            cell.setBorderColor(cTitulo);
            cell.setColspan(3);
            table.addCell(cell);
            
            cell = new PdfPCell(new Phrase("Fecha", fEncabezado));
            cell.setBorderColor(BaseColor.GRAY);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("Descripci\u00f3n", fEncabezado));
            cell.setBorderColor(BaseColor.GRAY);
            table.addCell(cell);
            
            cell = new PdfPCell(new Phrase("Fuente", fEncabezado));
            cell.setBorderColor(BaseColor.GRAY);
            table.addCell(cell);

            for (int i = 0; i < alertas.size(); i++) {
                Alerta alerta = alertas.get(i);

                cell = new PdfPCell(new Phrase(Util.formatoTimestamp(alerta.getColocacion(),1), fNormal));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setBorderColor(BaseColor.GRAY);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase(alerta.getTexto(), fNormal));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setBorderColor(BaseColor.GRAY);
                table.addCell(cell);
                
                cell = new PdfPCell(new Phrase(alerta.getFuente(), fNormal));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setBorderColor(BaseColor.GRAY);
                table.addCell(cell);
            }
            
            table.setSpacingAfter(10);
            document.add(table);
        }
    }
    
    private void agregarConsultas(Document document) throws DocumentException, Exception{
        ArrayList<Consulta> consultas = srv.consultarConsultas(identificacion, tipoIdentificacion, nitEmpresa);
        if(consultas.size()>0){
            float[] widths = {0.1f, 0.15f, 0.30f, 0.20f, 0.15f, 0.10f};
            PdfPTable table = new PdfPTable(widths);
            table.setWidthPercentage(100);
            table.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
            table.setKeepTogether(true);

            PdfPCell cell = new PdfPCell();
            cell.setPhrase(new Phrase("HIST\u00d3RICO DE CONSULTAS", fTitulo));
            cell.setBackgroundColor(cTitulo);
            cell.setPadding(3);
            cell.setBorderColor(cTitulo);
            cell.setColspan(6);
            table.addCell(cell);
            
            cell = new PdfPCell(new Phrase("Fecha", fEncabezado));
            cell.setBorderColor(BaseColor.GRAY);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("Tipo de cuenta", fEncabezado));
            cell.setBorderColor(BaseColor.GRAY);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("Consultante", fEncabezado));
            cell.setBorderColor(BaseColor.GRAY);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("Oficina", fEncabezado));
            cell.setBorderColor(BaseColor.GRAY);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("Ciudad", fEncabezado));
            cell.setBorderColor(BaseColor.GRAY);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("# consultas", fEncabezado));
            cell.setBorderColor(BaseColor.GRAY);
            table.addCell(cell);

            for (int i = 0; i < consultas.size(); i++) {
                Consulta consulta = consultas.get(i);

                cell = new PdfPCell(new Phrase(Util.formatoTimestamp(consulta.getFecha(),1), fNormal));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setBorderColor(BaseColor.GRAY);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase(consulta.getTipoCuenta(), fNormal));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setBorderColor(BaseColor.GRAY);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase(consulta.getEntidad(), fNormal));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setBorderColor(BaseColor.GRAY);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase(consulta.getOficina(), fNormal));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setBorderColor(BaseColor.GRAY);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase(consulta.getCiudad(), fNormal));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setBorderColor(BaseColor.GRAY);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase(String.valueOf(consulta.getNumConsultas()), fNormal));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setBorderColor(BaseColor.GRAY);
                table.addCell(cell);
            }
            
            table.setSpacingAfter(10);
            document.add(table);
        }
    }
    
    private void agregarReclamos(Document document) throws DocumentException, Exception{
        ArrayList<Reclamo> reclamos = srv.consultarReclamos(identificacion, tipoIdentificacion, nitEmpresa);
        if(reclamos.size()>0){
            float[] widths = {0.2f, 0.3f, 0.1f, 0.1f, 0.2f, 0.1f};
            PdfPTable table = new PdfPTable(widths);
            table.setWidthPercentage(100);
            table.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
            table.setKeepTogether(true);

            PdfPCell cell = new PdfPCell();
            cell.setPhrase(new Phrase("DESACUERDO DE INFORMACI\u00d3N", fTitulo));
            cell.setBackgroundColor(cTitulo);
            cell.setPadding(3);
            cell.setBorderColor(cTitulo);
            cell.setColspan(6);
            table.addCell(cell);
            
            cell = new PdfPCell(new Phrase("Tipo", fEncabezado));
            cell.setBorderColor(BaseColor.GRAY);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("Argumentos", fEncabezado));
            cell.setBorderColor(BaseColor.GRAY);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("Estado", fEncabezado));
            cell.setBorderColor(BaseColor.GRAY);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("Fecha", fEncabezado));
            cell.setBorderColor(BaseColor.GRAY);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("Entidad", fEncabezado));
            cell.setBorderColor(BaseColor.GRAY);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("Num. cuenta", fEncabezado));
            cell.setBorderColor(BaseColor.GRAY);
            table.addCell(cell);

            for (int i = 0; i < reclamos.size(); i++) {
                Reclamo reclamo = reclamos.get(i);

                cell = new PdfPCell(new Phrase(reclamo.getTipo(), fNormal));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setBorderColor(BaseColor.GRAY);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase(reclamo.getTexto(), fNormal));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setBorderColor(BaseColor.GRAY);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase(reclamo.getTipoLeyenda(), fNormal));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setBorderColor(BaseColor.GRAY);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase(Util.formatoTimestamp(reclamo.getFecha(),1), fNormal));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setBorderColor(BaseColor.GRAY);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase(reclamo.getEntidad(), fNormal));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setBorderColor(BaseColor.GRAY);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase(reclamo.getNumeroCuenta(), fNormal));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setBorderColor(BaseColor.GRAY);
                table.addCell(cell);
            }
            
            table.setSpacingAfter(10);
            document.add(table);
        }
    }
    
    private void agregarScores(Document document) throws DocumentException, Exception{
        ArrayList<Score> scores = srv.consultarScores(identificacion, tipoIdentificacion, nitEmpresa);
        if(scores.size()>0){
            PdfPTable table = new PdfPTable(3);
            table.setWidthPercentage(100);
            table.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);

            PdfPCell cell = new PdfPCell();
            cell.setPhrase(new Phrase("SCORES", fTitulo));
            cell.setBackgroundColor(cTitulo);
            cell.setPadding(3);
            cell.setBorderColor(cTitulo);
            cell.setColspan(3);
            table.addCell(cell);
            
            cell = new PdfPCell(new Phrase("Evaluaci\u00f3n", fEncabezado));
            cell.setBorderColor(BaseColor.GRAY);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("Puntaje", fEncabezado));
            cell.setBorderColor(BaseColor.GRAY);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("C\u00f3digos/Razones", fEncabezado));
            cell.setBorderColor(BaseColor.GRAY);
            table.addCell(cell);

            for (int i = 0; i < scores.size(); i++) {
                Score score = scores.get(i);

                cell = new PdfPCell(new Phrase(score.getClasificacion(), fNormal));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setBorderColor(BaseColor.GRAY);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase(String.valueOf(score.getPuntaje()), fNormal));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setBorderColor(BaseColor.GRAY);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase(score.getRazon(), fNormal));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setBorderColor(BaseColor.GRAY);
                table.addCell(cell);
            }
            
            ArrayList<RespuestaPersonalizada> respuestas = srv.consultarRespuestaPersonalizada(identificacion, tipoIdentificacion, nitEmpresa);
            if(respuestas.size()>0){
                cell = new PdfPCell(new Phrase("Respuesta Personalizada", fEncabezado));
                cell.setColspan(3);
                cell.setBorderColor(BaseColor.GRAY);
                table.addCell(cell);
                for (int i = 0; i < respuestas.size(); i++) {
                    RespuestaPersonalizada respuesta = respuestas.get(i);
                    
                    cell = new PdfPCell(new Phrase(respuesta.getLinea(), fNormal));
                    cell.setColspan(3);
                    cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                    cell.setBorderColor(BaseColor.GRAY);
                    table.addCell(cell);
                }
            }
            
            table.setSpacingAfter(10);
            document.add(table);
        }
    }
  
    private void agregarTotales(Document document) throws DocumentException, Exception{
        ArrayList<Valor> totales = srv.consultarTotales(identificacion, tipoIdentificacion, nitEmpresa);
        if(totales.size()>0){
            float[] widths = {0.35f, 0.15f, 0.14f, 0.12f, 0.12f, 0.12f};
            PdfPTable table = new PdfPTable(widths);
            table.setWidthPercentage(100);
            table.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);

            PdfPCell cell = new PdfPCell();
            cell.setPhrase(new Phrase("TOTALES, SALDOS, CUPOS Y MORAS (MILES DE $)", fTitulo));
            cell.setBackgroundColor(cTitulo);
            cell.setPadding(3);
            cell.setBorderColor(cTitulo);
            cell.setColspan(6);
            table.addCell(cell);
            
            cell = new PdfPCell(new Phrase("Tipo de cuenta", fEncabezado));
            cell.setBorderColor(BaseColor.GRAY);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("Calidad", fEncabezado));
            cell.setBorderColor(BaseColor.GRAY);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("Cupo o Vr inicial", fEncabezado));
            cell.setBorderColor(BaseColor.GRAY);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("Saldo actual", fEncabezado));
            cell.setBorderColor(BaseColor.GRAY);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("Saldo en mora", fEncabezado));
            cell.setBorderColor(BaseColor.GRAY);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("Valor cuota", fEncabezado));
            cell.setBorderColor(BaseColor.GRAY);
            table.addCell(cell);
            
            TreeMap<String, ArrayList<Valor>> calidades = new TreeMap<String, ArrayList<Valor>>();
            double totalVlrInicial = 0;
            double totalSaldoActual = 0;
            double totalSaldoMora = 0;
            double totalCuota = 0;
            
            for (int i = 0; i < totales.size(); i++) {
                Valor valor = totales.get(i);
                if(valor.getValorInicial().intValue()!=0 || valor.getSaldoActual().intValue()!=0 || valor.getSaldoMora().intValue()!=0 || valor.getCuota().intValue()!=0){
                    //Se suman los valores a los totales
                    totalVlrInicial += Util.convertirMiles(valor.getValorInicial());
                    totalSaldoActual += Util.convertirMiles(valor.getSaldoActual());
                    totalSaldoMora += Util.convertirMiles(valor.getSaldoMora());
                    totalCuota += Util.convertirMiles(valor.getCuota());

                    //Se clasifican los valores por calidad(Tipo de garante) para mas adelante poder calcular los subtotales
                    if(calidades.containsKey(valor.getCalidad())){
                        calidades.get(valor.getCalidad()).add(valor);
                    }else{
                        ArrayList<Valor> valores = new ArrayList<Valor>();
                        valores.add(valor);
                        calidades.put(valor.getCalidad(), valores);
                    }

                    cell = new PdfPCell(new Phrase(valor.getTipoPadre(), fNormal));
                    cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                    cell.setBorderColor(BaseColor.GRAY);
                    table.addCell(cell);

                    cell = new PdfPCell(new Phrase(valor.getCalidad(), fNormal));
                    cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                    cell.setBorderColor(BaseColor.GRAY);
                    table.addCell(cell);

                    cell = new PdfPCell(new Phrase("$"+Util.convertirMilesString(valor.getValorInicial()), fNormal));
                    cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                    cell.setBorderColor(BaseColor.GRAY);
                    table.addCell(cell);

                    cell = new PdfPCell(new Phrase("$"+Util.convertirMilesString(valor.getSaldoActual()), fNormal));
                    cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                    cell.setBorderColor(BaseColor.GRAY);
                    table.addCell(cell);

                    cell = new PdfPCell(new Phrase("$"+Util.convertirMilesString(valor.getSaldoMora()), fNormal));
                    cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                    cell.setBorderColor(BaseColor.GRAY);
                    table.addCell(cell);

                    cell = new PdfPCell(new Phrase("$"+Util.convertirMilesString(valor.getCuota()), fNormal));
                    cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                    cell.setBorderColor(BaseColor.GRAY);
                    table.addCell(cell);
                }
            }
            
            cell = new PdfPCell();
            cell.setBackgroundColor(cEncabezado);
            cell.setBorderColor(BaseColor.GRAY);
            table.addCell(cell);
            
            cell = new PdfPCell();
            cell.setPhrase(new Phrase("% deuda", fEncabezado));
            cell.setBackgroundColor(cEncabezado);
            cell.setBorderColor(BaseColor.GRAY);
            table.addCell(cell);
            
            cell = new PdfPCell();
            cell.setBackgroundColor(cEncabezado);
            cell.setBorderColor(BaseColor.GRAY);
            cell.setColspan(4);
            table.addCell(cell);
            
            //Calcular subtotales
            Iterator<String> it = calidades.keySet().iterator();
            while(it.hasNext()){
                double subtotalVlrInicial = 0;
                double subtotalSaldoActual = 0;
                double subtotalSaldoMora = 0;
                double subtotalCuota = 0;
                String calidad = it.next();
                ArrayList<Valor> valores = calidades.get(calidad);
                for (int i = 0; i < valores.size(); i++) {
                    Valor valor = valores.get(i);
                    
                    subtotalVlrInicial += Util.convertirMiles(valor.getValorInicial());
                    subtotalSaldoActual += Util.convertirMiles(valor.getSaldoActual());
                    subtotalSaldoMora += Util.convertirMiles(valor.getSaldoMora());
                    subtotalCuota += Util.convertirMiles(valor.getCuota());
                }
                
                double porcDeuda = (subtotalVlrInicial*100) / totalVlrInicial;
                
                cell = new PdfPCell(new Phrase("Subtotal "+calidad, fNormal));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setBorderColor(BaseColor.GRAY);
                table.addCell(cell);
                
                cell = new PdfPCell(new Phrase(Math.round(porcDeuda)+"%", fNormal));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setBorderColor(BaseColor.GRAY);
                table.addCell(cell);
                
                cell = new PdfPCell(new Phrase("$"+Util.customFormat(subtotalVlrInicial), fNormal));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setBorderColor(BaseColor.GRAY);
                table.addCell(cell);
                
                cell = new PdfPCell(new Phrase("$"+Util.customFormat(subtotalSaldoActual), fNormal));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setBorderColor(BaseColor.GRAY);
                table.addCell(cell);
                
                cell = new PdfPCell(new Phrase("$"+Util.customFormat(subtotalSaldoMora), fNormal));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setBorderColor(BaseColor.GRAY);
                table.addCell(cell);
                
                cell = new PdfPCell(new Phrase("$"+Util.customFormat(subtotalCuota), fNormal));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setBorderColor(BaseColor.GRAY);
                table.addCell(cell);
            }
            
            //Totales
            cell = new PdfPCell(new Phrase("Total ", fNormal));
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setBorderColor(BaseColor.GRAY);
            table.addCell(cell);

            cell = new PdfPCell();
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setBorderColor(BaseColor.GRAY);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("$"+Util.customFormat(totalVlrInicial), fNormal));
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setBorderColor(BaseColor.GRAY);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("$"+Util.customFormat(totalSaldoActual), fNormal));
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setBorderColor(BaseColor.GRAY);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("$"+Util.customFormat(totalSaldoMora), fNormal));
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setBorderColor(BaseColor.GRAY);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase("$"+Util.customFormat(totalCuota), fNormal));
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setBorderColor(BaseColor.GRAY);
            table.addCell(cell);
            
            table.setSpacingAfter(10);
            document.add(table);
        }
    }
    
    
    private void agregarObligacionesAbiertas(Document document) throws DocumentException, Exception{
        ArrayList<CuentaCartera> obligaciones = srv.consultarObligacionesAbiertas(identificacion, tipoIdentificacion, nitEmpresa);
        if(obligaciones.size()>0){
            
            PdfPTable tableTitulo = new PdfPTable(1);
            tableTitulo.setWidthPercentage(100);
            tableTitulo.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);

            PdfPCell cell = new PdfPCell();
            cell.setPhrase(new Phrase("HABITO DE PAGO DE OBLIGACIONES ABIERTAS / VIGENTES", fTitulo));
            cell.setBackgroundColor(cTitulo);
            cell.setPadding(3);
            cell.setBorderColor(cTitulo);
            tableTitulo.addCell(cell);
            tableTitulo.setSpacingAfter(6);
            document.add(tableTitulo);
            String sectorAnterior = "";


            for (int i = 0; i < obligaciones.size(); i++) {
                CuentaCartera obligacion = obligaciones.get(i);
                Valor valor = obligacion.getValor();

                float[] widths = {0.12f, 0.08f, 0.09f, 0.09f, 0.09f, 0.09f, 0.08f, 0.08f, 0.28f};
                PdfPTable table = new PdfPTable(widths);
                table.setWidthPercentage(100);
                table.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
                
                //Se coloca el titulo con el sector
                if(!obligacion.getSector().equals(sectorAnterior)){                
                    cell = new PdfPCell(new Phrase(obligacion.getSector(), fEncabezado));
                    cell.setBackgroundColor(cEncabezado);
                    cell.setPadding(3);
                    cell.setBorderColor(cEncabezado);
                    cell.setColspan(9);
                    table.addCell(cell);
                    sectorAnterior = obligacion.getSector();
                }
                
                cell = new PdfPCell(new Phrase("Entidad informante", fEncabezado));
                cell.setBorderColor(BaseColor.GRAY);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase("Tipo cuenta", fEncabezado));
                cell.setBorderColor(BaseColor.GRAY);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase("# cuenta", fEncabezado));
                cell.setBorderColor(BaseColor.GRAY);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase("Estado obligaci\u00f3n", fEncabezado));
                cell.setBorderColor(BaseColor.GRAY);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase("Fecha actuali", fEncabezado));
                cell.setBorderColor(BaseColor.GRAY);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase("Fecha apertura", fEncabezado));
                cell.setBorderColor(BaseColor.GRAY);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase("Fecha venc", fEncabezado));
                cell.setBorderColor(BaseColor.GRAY);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase("Mora m\u00e1x", fEncabezado));
                cell.setBorderColor(BaseColor.GRAY);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase("47 meses", fEncabezado));
                cell.setBorderColor(BaseColor.GRAY);
                table.addCell(cell);
            
                cell = new PdfPCell(new Phrase(obligacion.getEntidad(), fNormal));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setBorderColor(BaseColor.GRAY);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase(String.valueOf(obligacion.getTipoCuenta()), fNormal));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setBorderColor(BaseColor.GRAY);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase(obligacion.getNumeroObligacion(), fNormal));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setBorderColor(BaseColor.GRAY);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase(obligacion.getEstado(), fNormal));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setBorderColor(BaseColor.GRAY);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase(Util.formatoTimestamp(obligacion.getUltimaActualizacion(),2), fNormal));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setBorderColor(BaseColor.GRAY);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase(Util.formatoTimestamp(obligacion.getFechaApertura(),2), fNormal));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setBorderColor(BaseColor.GRAY);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase(Util.formatoTimestamp(obligacion.getFechaVencimiento(),2), fNormal));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setBorderColor(BaseColor.GRAY);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase(String.valueOf(valor.getMaximaMora()), fNormal));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setBorderColor(BaseColor.GRAY);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase(formatearComportamiento(obligacion.getComportamiento()), fmonospace));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setBorderColor(BaseColor.GRAY);
                table.addCell(cell);
                
                //agregar informacion adicional                    
                cell = new PdfPCell(new Phrase("Reclamo", fEncabezado));
                cell.setBorderColor(BaseColor.GRAY);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase("Estado del titular", fEncabezado));
                cell.setBorderColor(BaseColor.GRAY);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase("Cupo total", fEncabezado));
                cell.setBorderColor(BaseColor.GRAY);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase("Saldo actual", fEncabezado));
                cell.setBorderColor(BaseColor.GRAY);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase("Saldo en mora", fEncabezado));
                cell.setBorderColor(BaseColor.GRAY);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase("Valor cuota", fEncabezado));
                cell.setBorderColor(BaseColor.GRAY);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase("Permanen", fEncabezado));
                cell.setBorderColor(BaseColor.GRAY);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase("Cuotas/ M/ Vigencia", fEncabezado));
                cell.setBorderColor(BaseColor.GRAY);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase("Oficina / deudor", fEncabezado));
                cell.setBorderColor(BaseColor.GRAY);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase(obligacion.getReclamo(), fNormal));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setBorderColor(BaseColor.GRAY);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase(obligacion.getSituacionTitular(), fNormal));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setBorderColor(BaseColor.GRAY);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase(Util.convertirMilesString(valor.getCupo()), fNormal));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setBorderColor(BaseColor.GRAY);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase(Util.convertirMilesString(valor.getSaldoActual()), fNormal));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setBorderColor(BaseColor.GRAY);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase(Util.convertirMilesString(valor.getSaldoMora()), fNormal));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setBorderColor(BaseColor.GRAY);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase(Util.convertirMilesString(valor.getCuota()), fNormal));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setBorderColor(BaseColor.GRAY);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase(String.valueOf(obligacion.getMesesPermanencia()), fNormal));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setBorderColor(BaseColor.GRAY);
                table.addCell(cell);

                //Se concatenan los valores
                String cuotas="-";
                if(valor.getCuotasCanceladas()!=0 || valor.getTotalCuotas()!=0){
                    cuotas = valor.getCuotasCanceladas().intValue()==0?"-":valor.getCuotasCanceladas().intValue() + " de " + (valor.getTotalCuotas().intValue()==0?"-":valor.getTotalCuotas().intValue());
                }
                String tipoContrato="-";
                if(obligacion.getTipoContrato()!=null && !obligacion.getTipoContrato().isEmpty()){
                    tipoContrato=obligacion.getTipoContrato().substring(0, 1);
                }
                String periodicidad = (obligacion.getPeriodicidad()==null || obligacion.getPeriodicidad().isEmpty())?"-":obligacion.getPeriodicidad();
                cell = new PdfPCell(new Phrase(cuotas+"/"+periodicidad+"/"+tipoContrato, fNormal));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setBorderColor(BaseColor.GRAY);
                table.addCell(cell);

                String oficina = "-";
                if(obligacion.getOficina()!=null && !obligacion.getOficina().isEmpty() && !obligacion.getOficina().equals("NO INFORMO")){
                    oficina = obligacion.getOficina();
                }
                String deudor = "-";
                if(obligacion.getGarante()!=null && !obligacion.getGarante().isEmpty()){
                    if(obligacion.getGarante().equals("f")){
                        deudor = "Deudor";
                    }else if(obligacion.getGarante().equals("t")){
                        deudor = "Amparada";
                    }else{
                        deudor = obligacion.getGarante();
                    }
                }
                cell = new PdfPCell(new Phrase(oficina+"/"+deudor, fNormal));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setBorderColor(BaseColor.GRAY);
                table.addCell(cell);                
                
                table.setSpacingAfter(4);
                document.add(table);
            }
        }
    }
    
    private void agregarObligacionesCerradas(Document document) throws DocumentException, Exception{
        ArrayList<CuentaCartera> obligaciones = srv.consultarObligacionesCerradas(identificacion, tipoIdentificacion, nitEmpresa);
        if(obligaciones.size()>0){
            
            float[] widths = {0.12f, 0.1f, 0.1f, 0.09f, 0.08f, 0.07f, 0.07f, 0.07f, 0.09f, 0.28f};
            PdfPTable table = new PdfPTable(widths);
            table.setWidthPercentage(100);
            table.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
            table.setKeepTogether(true);

            PdfPCell cell = new PdfPCell();
            cell.setPhrase(new Phrase("HABITO DE PAGO DE OBLIGACIONES CERRADAS / INACTIVAS", fTitulo));
            cell.setBackgroundColor(cTitulo);
            cell.setPadding(3);
            cell.setBorderColor(cTitulo);
            cell.setColspan(10);
            table.addCell(cell);
            table.setSpacingBefore(6);
            String sectorAnterior = "";
            
            for (int i = 0; i < obligaciones.size(); i++) {
                CuentaCartera obligacion = obligaciones.get(i);
                
                //Se coloca el titulo con el sector
                if(!obligacion.getSector().equals(sectorAnterior)){                
                    cell = new PdfPCell(new Phrase(obligacion.getSector(), fEncabezado));
                    cell.setBackgroundColor(cEncabezado);
                    cell.setPadding(3);
                    cell.setBorderColor(cEncabezado);
                    cell.setColspan(10);
                    table.addCell(cell);
                    sectorAnterior = obligacion.getSector();
                    
                    cell = new PdfPCell(new Phrase("Entidad informante", fEncabezado));
                    cell.setBorderColor(BaseColor.GRAY);
                    table.addCell(cell);

                    cell = new PdfPCell(new Phrase("Tipo cuenta", fEncabezado));
                    cell.setBorderColor(BaseColor.GRAY);
                    table.addCell(cell);

                    cell = new PdfPCell(new Phrase("N\u00famero cuenta", fEncabezado));
                    cell.setBorderColor(BaseColor.GRAY);
                    table.addCell(cell);

                    cell = new PdfPCell(new Phrase("Estado de la obligaci\u00f3n", fEncabezado));
                    cell.setBorderColor(BaseColor.GRAY);
                    table.addCell(cell);

                    cell = new PdfPCell(new Phrase("Fecha apertura", fEncabezado));
                    cell.setBorderColor(BaseColor.GRAY);
                    table.addCell(cell);

                    cell = new PdfPCell(new Phrase("Fecha cierre", fEncabezado));
                    cell.setBorderColor(BaseColor.GRAY);
                    table.addCell(cell);
                    
                    cell = new PdfPCell(new Phrase("Valor inicial", fEncabezado));
                    cell.setBorderColor(BaseColor.GRAY);
                    table.addCell(cell);

                    cell = new PdfPCell(new Phrase("Fecha venc", fEncabezado));
                    cell.setBorderColor(BaseColor.GRAY);
                    table.addCell(cell);

                    cell = new PdfPCell(new Phrase("Oficina / deudor", fEncabezado));
                    cell.setBorderColor(BaseColor.GRAY);
                    table.addCell(cell);

                    cell = new PdfPCell(new Phrase("47 meses", fEncabezado));
                    cell.setBorderColor(BaseColor.GRAY);
                    table.addCell(cell);
                }
                                
            
                cell = new PdfPCell(new Phrase(obligacion.getEntidad(), fNormal));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setBorderColor(BaseColor.GRAY);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase(String.valueOf(obligacion.getTipoCuenta()), fNormal));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setBorderColor(BaseColor.GRAY);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase(obligacion.getNumeroObligacion(), fNormal));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setBorderColor(BaseColor.GRAY);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase(obligacion.getEstado(), fNormal));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setBorderColor(BaseColor.GRAY);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase(Util.formatoTimestamp(obligacion.getFechaApertura(),2), fNormal));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setBorderColor(BaseColor.GRAY);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase(Util.formatoTimestamp(obligacion.getUltimaActualizacion(),2), fNormal));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setBorderColor(BaseColor.GRAY);
                table.addCell(cell);              
                
                cell = new PdfPCell(new Phrase(Util.convertirMilesString(obligacion.getValor_Inicial()), fNormal));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setBorderColor(BaseColor.GRAY);
                table.addCell(cell);
                
                cell = new PdfPCell(new Phrase(Util.formatoTimestamp(obligacion.getFechaVencimiento(),2), fNormal));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setBorderColor(BaseColor.GRAY);
                table.addCell(cell);
                
                String oficina = "-";
                if(obligacion.getOficina()!=null && !obligacion.getOficina().isEmpty() && !obligacion.getOficina().equals("NO INFORMO")){
                    oficina = obligacion.getOficina();
                }
                String deudor = "-";
                if(obligacion.getGarante()!=null && !obligacion.getGarante().isEmpty()){
                    if(obligacion.getGarante().equals("f")){
                        deudor = "Deudor";
                    }else if(obligacion.getGarante().equals("t")){
                        deudor = "Amparada";
                    }else{
                        deudor = obligacion.getGarante();
                    }
                }
                cell = new PdfPCell(new Phrase(oficina+"/"+deudor, fNormal));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setBorderColor(BaseColor.GRAY);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase(formatearComportamiento(obligacion.getComportamiento()), fmonospace));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setBorderColor(BaseColor.GRAY);
                table.addCell(cell);
              
                
            }
            table.setSpacingAfter(10);
            document.add(table);
        }
    }
    
    private String formatearComportamiento(String comportamiento){
        String cadena = "";
        if(comportamiento!=null){
            StringBuilder str = new StringBuilder(comportamiento);

            for (int i = 12; i < str.length(); i=i+14) {
                    str.insert(i, "][");
                }

            if(str.length()>=48){
                str.insert(27, "\n25 a 47");
            }
            str.insert(0, "ult 24 [");
            str.insert(str.length(), " ]");            

            cadena = str.toString();
        }
        return cadena;
    }
    
    /**
     * Metodo para colocar las 2 tablas de endeudamiento globa: endeudamiento global clasificado y resumen endeudamiento
     * @param document
     * @throws DocumentException
     * @throws Exception 
     */
    private void agregarEndeudamientoGlobal(Document document) throws DocumentException, Exception{
        ArrayList<EndeudamientoGlobal> endeudamientos = srv.consultarEndeudamientoGlobal(identificacion, tipoIdentificacion, nitEmpresa);
        if(endeudamientos.size()>0){
            float[] widths = {0.15f, 0.05f, 0.05f, 0.08f, 0.05f, 0.08f, 0.05f, 0.08f, 0.05f, 0.08f, 0.05f, 0.08f, 0.08f, 0.07f};
            PdfPTable table = new PdfPTable(widths);
            table.setWidthPercentage(100);
            table.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
            table.setKeepTogether(true);
            
            PdfPTable tableResumen = new PdfPTable(11);
            tableResumen.setWidthPercentage(100);
            tableResumen.getDefaultCell().setVerticalAlignment(Element.ALIGN_MIDDLE);
            tableResumen.setKeepTogether(true);

            PdfPCell cell = new PdfPCell();
            cell.setPhrase(new Phrase("ENDEUDAMIENTO GLOBAL CLASIFICADO", fTitulo));
            cell.setBackgroundColor(cTitulo);
            cell.setPadding(3);
            cell.setBorderColor(cTitulo);
            cell.setColspan(14);
            table.addCell(cell);

            cell = new PdfPCell();
            cell.setPhrase(new Phrase("RESUMEN ENDEUDAMIENTO", fTitulo));
            cell.setBackgroundColor(cTitulo);
            cell.setPadding(3);
            cell.setBorderColor(cTitulo);
            cell.setColspan(11);
            tableResumen.addCell(cell);
                        
            double saldoTotal = 0;
            double saldoComercial = 0;
            double saldoHipotecario = 0;
            double saldoConsumo = 0;
            double saldoMicrocredito = 0;
            int numTotal = 0;
            int numComercial = 0;
            int numHipotecario = 0;
            int numConsumo = 0;
            int numMicrocredito = 0;
            int numAdmisible = 0;
            int numOtro = 0;
            String trimestreAnterior = "";
            
            //Titulos tabla de resumen endeudamiento
            cell = new PdfPCell(new Phrase("Fecha corte", fEncabezado));
            cell.setBorderColor(BaseColor.GRAY);
            tableResumen.addCell(cell);
            
            cell = new PdfPCell(new Phrase("Comercial", fEncabezado));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorderColor(BaseColor.GRAY);
            cell.setColspan(2);
            tableResumen.addCell(cell);

            cell = new PdfPCell(new Phrase("Hipotecario", fEncabezado));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorderColor(BaseColor.GRAY);
            cell.setColspan(2);
            tableResumen.addCell(cell);

            cell = new PdfPCell(new Phrase("Consumo", fEncabezado));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorderColor(BaseColor.GRAY);
            cell.setColspan(2);
            tableResumen.addCell(cell);

            cell = new PdfPCell(new Phrase("Microcredito", fEncabezado));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorderColor(BaseColor.GRAY);
            cell.setColspan(2);
            tableResumen.addCell(cell);

            cell = new PdfPCell(new Phrase("Tipo garant\u00eda", fEncabezado));
            cell.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell.setBorderColor(BaseColor.GRAY);
            cell.setColspan(2);
            tableResumen.addCell(cell);

            cell = new PdfPCell();
            cell.setBorderColor(BaseColor.GRAY);
            tableResumen.addCell(cell);
            
            cell = new PdfPCell(new Phrase("Nro", fEncabezado));
            cell.setBorderColor(BaseColor.GRAY);
            tableResumen.addCell(cell);

            cell = new PdfPCell(new Phrase("Miles $", fEncabezado));
            cell.setBorderColor(BaseColor.GRAY);
            tableResumen.addCell(cell);
            
            cell = new PdfPCell(new Phrase("Nro", fEncabezado));
            cell.setBorderColor(BaseColor.GRAY);
            tableResumen.addCell(cell);

            cell = new PdfPCell(new Phrase("Miles $", fEncabezado));
            cell.setBorderColor(BaseColor.GRAY);
            tableResumen.addCell(cell);
            
            cell = new PdfPCell(new Phrase("Nro", fEncabezado));
            cell.setBorderColor(BaseColor.GRAY);
            tableResumen.addCell(cell);

            cell = new PdfPCell(new Phrase("Miles $", fEncabezado));
            cell.setBorderColor(BaseColor.GRAY);
            tableResumen.addCell(cell);
            
            cell = new PdfPCell(new Phrase("Nro", fEncabezado));
            cell.setBorderColor(BaseColor.GRAY);
            tableResumen.addCell(cell);

            cell = new PdfPCell(new Phrase("Miles $", fEncabezado));
            cell.setBorderColor(BaseColor.GRAY);
            tableResumen.addCell(cell);
            
            cell = new PdfPCell(new Phrase("Admisible", fEncabezado));
            cell.setBorderColor(BaseColor.GRAY);
            tableResumen.addCell(cell);

            cell = new PdfPCell(new Phrase("Otro", fEncabezado));
            cell.setBorderColor(BaseColor.GRAY);
            tableResumen.addCell(cell);
            
            for (int i = 0; i < endeudamientos.size(); i++) {
                EndeudamientoGlobal endeudamiento = endeudamientos.get(i);
                
                //Se coloca el titulo con el trimestre
                String trimestreActual = Util.formatoTimestamp(endeudamiento.getFechaReporte(),3);
                if(!trimestreActual.equals(trimestreAnterior)){
                    
                    if(!trimestreAnterior.isEmpty()){
                        //Colocar los totales del trimestre anterior en tabla de endeudamiento global clasificado
                        cell = new PdfPCell(new Phrase("TOTAL", fNormal));
                        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                        cell.setBorderColor(BaseColor.GRAY);
                        cell.setColspan(2);
                        table.addCell(cell);

                        cell = new PdfPCell(new Phrase(String.valueOf(numTotal), fNormal));
                        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                        cell.setBorderColor(BaseColor.GRAY);
                        table.addCell(cell);

                        cell = new PdfPCell(new Phrase(Util.customFormat(saldoTotal), fNormal));
                        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                        cell.setBorderColor(BaseColor.GRAY);
                        table.addCell(cell);

                        cell = new PdfPCell(new Phrase(String.valueOf(numComercial), fNormal));
                        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                        cell.setBorderColor(BaseColor.GRAY);
                        table.addCell(cell);

                        cell = new PdfPCell(new Phrase(Util.customFormat(saldoComercial), fNormal));
                        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                        cell.setBorderColor(BaseColor.GRAY);
                        table.addCell(cell);

                        cell = new PdfPCell(new Phrase(String.valueOf(numHipotecario), fNormal));
                        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                        cell.setBorderColor(BaseColor.GRAY);
                        table.addCell(cell);

                        cell = new PdfPCell(new Phrase(Util.customFormat(saldoHipotecario), fNormal));
                        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                        cell.setBorderColor(BaseColor.GRAY);
                        table.addCell(cell);

                        cell = new PdfPCell(new Phrase(String.valueOf(numConsumo), fNormal));
                        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                        cell.setBorderColor(BaseColor.GRAY);
                        table.addCell(cell);

                        cell = new PdfPCell(new Phrase(Util.customFormat(saldoConsumo), fNormal));
                        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                        cell.setBorderColor(BaseColor.GRAY);
                        table.addCell(cell);

                        cell = new PdfPCell(new Phrase(String.valueOf(numMicrocredito), fNormal));
                        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                        cell.setBorderColor(BaseColor.GRAY);
                        table.addCell(cell);

                        cell = new PdfPCell(new Phrase(Util.customFormat(saldoMicrocredito), fNormal));
                        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                        cell.setBorderColor(BaseColor.GRAY);
                        table.addCell(cell);
                        
                        cell = new PdfPCell();
                        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                        cell.setBorderColor(BaseColor.GRAY);
                        cell.setColspan(2);
                        table.addCell(cell);
                        
                        //Colocar los totales del trimestre anterior en tabla de resumen de endeudamiento
                        cell = new PdfPCell(new Phrase(trimestreAnterior, fNormal));
                        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                        cell.setBorderColor(BaseColor.GRAY);
                        tableResumen.addCell(cell);
                        
                        cell = new PdfPCell(new Phrase(String.valueOf(numComercial), fNormal));
                        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                        cell.setBorderColor(BaseColor.GRAY);
                        tableResumen.addCell(cell);
                        
                        cell = new PdfPCell(new Phrase(Util.customFormat(saldoComercial), fNormal));
                        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                        cell.setBorderColor(BaseColor.GRAY);
                        tableResumen.addCell(cell);

                        cell = new PdfPCell(new Phrase(String.valueOf(numHipotecario), fNormal));
                        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                        cell.setBorderColor(BaseColor.GRAY);
                        tableResumen.addCell(cell);

                        cell = new PdfPCell(new Phrase(Util.customFormat(saldoHipotecario), fNormal));
                        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                        cell.setBorderColor(BaseColor.GRAY);
                        tableResumen.addCell(cell);

                        cell = new PdfPCell(new Phrase(String.valueOf(numConsumo), fNormal));
                        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                        cell.setBorderColor(BaseColor.GRAY);
                        tableResumen.addCell(cell);

                        cell = new PdfPCell(new Phrase(Util.customFormat(saldoConsumo), fNormal));
                        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                        cell.setBorderColor(BaseColor.GRAY);
                        tableResumen.addCell(cell);

                        cell = new PdfPCell(new Phrase(String.valueOf(numMicrocredito), fNormal));
                        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                        cell.setBorderColor(BaseColor.GRAY);
                        tableResumen.addCell(cell);

                        cell = new PdfPCell(new Phrase(Util.customFormat(saldoMicrocredito), fNormal));
                        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                        cell.setBorderColor(BaseColor.GRAY);
                        tableResumen.addCell(cell);                        

                        cell = new PdfPCell(new Phrase(numAdmisible==0?"-":String.valueOf(numAdmisible), fNormal));
                        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                        cell.setBorderColor(BaseColor.GRAY);
                        tableResumen.addCell(cell);                        

                        cell = new PdfPCell(new Phrase(numOtro==0?"-":String.valueOf(numOtro), fNormal));
                        cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                        cell.setBorderColor(BaseColor.GRAY);
                        tableResumen.addCell(cell);
                    }
                    
                    cell = new PdfPCell(new Phrase("TRIMESTRE "+trimestreActual, fEncabezado));
                    cell.setBackgroundColor(cEncabezado);
                    cell.setPadding(3);
                    cell.setBorderColor(cEncabezado);
                    cell.setColspan(14);
                    table.addCell(cell);
                    trimestreAnterior = trimestreActual;
                    
                    cell = new PdfPCell();
                    cell.setBorderColor(BaseColor.GRAY);
                    cell.setColspan(4);
                    table.addCell(cell);
                    
                    cell = new PdfPCell(new Phrase("Comercial", fEncabezado));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setBorderColor(BaseColor.GRAY);
                    cell.setColspan(2);
                    table.addCell(cell);
                    
                    cell = new PdfPCell(new Phrase("Hipotecario", fEncabezado));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setBorderColor(BaseColor.GRAY);
                    cell.setColspan(2);
                    table.addCell(cell);
                    
                    cell = new PdfPCell(new Phrase("Consumo", fEncabezado));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setBorderColor(BaseColor.GRAY);
                    cell.setColspan(2);
                    table.addCell(cell);
                    
                    cell = new PdfPCell(new Phrase("Microcredito", fEncabezado));
                    cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                    cell.setBorderColor(BaseColor.GRAY);
                    cell.setColspan(2);
                    table.addCell(cell);
                    
                    cell = new PdfPCell();
                    cell.setBorderColor(BaseColor.GRAY);
                    cell.setColspan(2);
                    table.addCell(cell);
                    
                    cell = new PdfPCell(new Phrase("Entidad", fEncabezado));
                    cell.setBorderColor(BaseColor.GRAY);
                    table.addCell(cell);

                    cell = new PdfPCell(new Phrase("Calif.", fEncabezado));
                    cell.setBorderColor(BaseColor.GRAY);
                    table.addCell(cell);

                    cell = new PdfPCell(new Phrase("Num", fEncabezado));
                    cell.setBorderColor(BaseColor.GRAY);
                    table.addCell(cell);

                    cell = new PdfPCell(new Phrase("Saldo total", fEncabezado));
                    cell.setBorderColor(BaseColor.GRAY);
                    table.addCell(cell);

                    cell = new PdfPCell(new Phrase("Nro", fEncabezado));
                    cell.setBorderColor(BaseColor.GRAY);
                    table.addCell(cell);

                    cell = new PdfPCell(new Phrase("Miles $", fEncabezado));
                    cell.setBorderColor(BaseColor.GRAY);
                    table.addCell(cell);

                    cell = new PdfPCell(new Phrase("Nro", fEncabezado));
                    cell.setBorderColor(BaseColor.GRAY);
                    table.addCell(cell);

                    cell = new PdfPCell(new Phrase("Miles $", fEncabezado));
                    cell.setBorderColor(BaseColor.GRAY);
                    table.addCell(cell);

                    cell = new PdfPCell(new Phrase("Nro", fEncabezado));
                    cell.setBorderColor(BaseColor.GRAY);
                    table.addCell(cell);

                    cell = new PdfPCell(new Phrase("Miles $", fEncabezado));
                    cell.setBorderColor(BaseColor.GRAY);
                    table.addCell(cell);

                    cell = new PdfPCell(new Phrase("Nro", fEncabezado));
                    cell.setBorderColor(BaseColor.GRAY);
                    table.addCell(cell);

                    cell = new PdfPCell(new Phrase("Miles $", fEncabezado));
                    cell.setBorderColor(BaseColor.GRAY);
                    table.addCell(cell);

                    cell = new PdfPCell(new Phrase("Garant\u00eda", fEncabezado));
                    cell.setBorderColor(BaseColor.GRAY);
                    table.addCell(cell);

                    cell = new PdfPCell(new Phrase("Moneda", fEncabezado));
                    cell.setBorderColor(BaseColor.GRAY);
                    table.addCell(cell);
                    
                    saldoTotal = 0;
                    saldoComercial = 0;
                    saldoHipotecario = 0;
                    saldoConsumo = 0;
                    saldoMicrocredito = 0;
                    numTotal = 0;
                    numComercial = 0;
                    numHipotecario = 0;
                    numConsumo = 0;
                    numMicrocredito = 0;
                    numAdmisible = 0;
                    numOtro = 0;
                }
                
                //Se suman los valores a los totales
                saldoTotal += Util.convertirMiles(endeudamiento.getSaldoPendiente());
                saldoComercial += Util.convertirMiles(endeudamiento.getTotalComercial());
                saldoHipotecario += Util.convertirMiles(endeudamiento.getTotalHipotecario());
                saldoMicrocredito += Util.convertirMiles(endeudamiento.getTotalMicrocredito());
                saldoConsumo += Util.convertirMiles(endeudamiento.getTotalConsumo());
                numTotal += endeudamiento.getNumeroCreditos();
                numComercial += endeudamiento.getNumComercial();
                numHipotecario += endeudamiento.getNumHipotecario();
                numMicrocredito += endeudamiento.getNumMicrocredito();
                numConsumo += endeudamiento.getNumConsumo();
                if(endeudamiento.getGarantia()!=null){
                    if(endeudamiento.getGarantia().equals("OTRAS")){
                        numOtro += numTotal;
                    }else if(endeudamiento.getGarantia().equals("ADMIS")){
                        numAdmisible += numTotal;
                    }
                }

                cell = new PdfPCell(new Phrase(endeudamiento.getEntidad(), fNormal));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setBorderColor(BaseColor.GRAY);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase(endeudamiento.getCalificacion(), fNormal));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setBorderColor(BaseColor.GRAY);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase(String.valueOf(endeudamiento.getNumeroCreditos()), fNormal));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setBorderColor(BaseColor.GRAY);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase(Util.convertirMilesString(endeudamiento.getSaldoPendiente()), fNormal));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setBorderColor(BaseColor.GRAY);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase(String.valueOf(endeudamiento.getNumComercial()), fNormal));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setBorderColor(BaseColor.GRAY);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase(Util.convertirMilesString(endeudamiento.getTotalComercial()), fNormal));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setBorderColor(BaseColor.GRAY);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase(String.valueOf(endeudamiento.getNumHipotecario()), fNormal));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setBorderColor(BaseColor.GRAY);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase(Util.convertirMilesString(endeudamiento.getTotalHipotecario()), fNormal));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setBorderColor(BaseColor.GRAY);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase(String.valueOf(endeudamiento.getNumConsumo()), fNormal));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setBorderColor(BaseColor.GRAY);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase(Util.convertirMilesString(endeudamiento.getTotalConsumo()), fNormal));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setBorderColor(BaseColor.GRAY);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase(String.valueOf(endeudamiento.getNumMicrocredito()), fNormal));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setBorderColor(BaseColor.GRAY);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase(Util.convertirMilesString(endeudamiento.getTotalMicrocredito()), fNormal));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setBorderColor(BaseColor.GRAY);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase(endeudamiento.getGarantia(), fNormal));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setBorderColor(BaseColor.GRAY);
                table.addCell(cell);

                cell = new PdfPCell(new Phrase(endeudamiento.getMoneda(), fNormal));
                cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                cell.setBorderColor(BaseColor.GRAY);
                table.addCell(cell);
            }
            
            //Colocar los totales ultimo trimestre . endeudamiento global clasificado
            cell = new PdfPCell(new Phrase("TOTAL", fNormal));
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setBorderColor(BaseColor.GRAY);
            cell.setColspan(2);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase(String.valueOf(numTotal), fNormal));
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setBorderColor(BaseColor.GRAY);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase(Util.customFormat(saldoTotal), fNormal));
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setBorderColor(BaseColor.GRAY);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase(String.valueOf(numComercial), fNormal));
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setBorderColor(BaseColor.GRAY);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase(Util.customFormat(saldoComercial), fNormal));
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setBorderColor(BaseColor.GRAY);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase(String.valueOf(numHipotecario), fNormal));
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setBorderColor(BaseColor.GRAY);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase(Util.customFormat(saldoHipotecario), fNormal));
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setBorderColor(BaseColor.GRAY);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase(String.valueOf(numConsumo), fNormal));
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setBorderColor(BaseColor.GRAY);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase(Util.customFormat(saldoConsumo), fNormal));
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setBorderColor(BaseColor.GRAY);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase(String.valueOf(numMicrocredito), fNormal));
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setBorderColor(BaseColor.GRAY);
            table.addCell(cell);

            cell = new PdfPCell(new Phrase(Util.customFormat(saldoMicrocredito), fNormal));
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setBorderColor(BaseColor.GRAY);
            table.addCell(cell);

            cell = new PdfPCell();
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setBorderColor(BaseColor.GRAY);
            cell.setColspan(2);
            table.addCell(cell);
            
            //Colocar los totales ultimo trimestre en tabla de resumen de endeudamiento
            cell = new PdfPCell(new Phrase(trimestreAnterior, fNormal));
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setBorderColor(BaseColor.GRAY);
            tableResumen.addCell(cell);

            cell = new PdfPCell(new Phrase(String.valueOf(numComercial), fNormal));
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setBorderColor(BaseColor.GRAY);
            tableResumen.addCell(cell);

            cell = new PdfPCell(new Phrase(Util.customFormat(saldoComercial), fNormal));
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setBorderColor(BaseColor.GRAY);
            tableResumen.addCell(cell);

            cell = new PdfPCell(new Phrase(String.valueOf(numHipotecario), fNormal));
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setBorderColor(BaseColor.GRAY);
            tableResumen.addCell(cell);

            cell = new PdfPCell(new Phrase(Util.customFormat(saldoHipotecario), fNormal));
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setBorderColor(BaseColor.GRAY);
            tableResumen.addCell(cell);

            cell = new PdfPCell(new Phrase(String.valueOf(numConsumo), fNormal));
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setBorderColor(BaseColor.GRAY);
            tableResumen.addCell(cell);

            cell = new PdfPCell(new Phrase(Util.customFormat(saldoConsumo), fNormal));
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setBorderColor(BaseColor.GRAY);
            tableResumen.addCell(cell);

            cell = new PdfPCell(new Phrase(String.valueOf(numMicrocredito), fNormal));
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setBorderColor(BaseColor.GRAY);
            tableResumen.addCell(cell);

            cell = new PdfPCell(new Phrase(Util.customFormat(saldoMicrocredito), fNormal));
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setBorderColor(BaseColor.GRAY);
            tableResumen.addCell(cell);                        

            cell = new PdfPCell(new Phrase(numAdmisible==0?"-":String.valueOf(numAdmisible), fNormal));
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setBorderColor(BaseColor.GRAY);
            tableResumen.addCell(cell);                        

            cell = new PdfPCell(new Phrase(numOtro==0?"-":String.valueOf(numOtro), fNormal));
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setBorderColor(BaseColor.GRAY);
            tableResumen.addCell(cell);
            
            table.setSpacingAfter(10);
            document.add(table);
            
            tableResumen.setSpacingAfter(10);
            document.add(tableResumen);
        }
    }
    
     public String getNitEmpresa() {
        return nitEmpresa;
    }

    public void setNitEmpresa(String nitEmpresa) {
        this.nitEmpresa = nitEmpresa;
    }

}
