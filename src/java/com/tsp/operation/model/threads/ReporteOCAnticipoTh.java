/***************************************************************************
 * Nombre clase : ............... ReporteOCAnticipoTh.java                 *
 * Descripcion :................. Hilo que permite la creacion del reporte *
 *                                de OC con Anticipoen un archivo en excel *
 * Autor :....................... Ing. Iv�n Dar�o Devia Acosta             *
 * Fecha :........................ 25 de noviembre de 2006, 12:40 AM       *
 * Version :...................... 1.0                                     *
 * Copyright :.................... Fintravalores S.A.                 *
 ***************************************************************************/
package com.tsp.operation.model.threads;

import java.util.*;
import java.io.*;
import java.lang.*;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import com.tsp.operation.model.beans.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.util .*;
import com.tsp.operation.model.*;
import java.sql.*;
import com.tsp.operation.model.beans.POIWrite;
import com.tsp.exceptions.*;
import com.tsp.operation.controller.*;
import java.text.*;

import java.sql.SQLException;
import org.apache.log4j.*;

public class ReporteOCAnticipoTh extends Thread{
    private List Lista;
    private String fechaini;
    private String fechafin;
    private String distrito;
    private String usuario;
    private Model model;
    
    /** Creates a new instance of ReporteAnulacionPlanilla */
    public ReporteOCAnticipoTh() {
        model = new Model();
    }
    
    public void start(List Lista, String fechaini, String fechafin, Usuario u) {
        this.Lista = Lista;
        this.fechaini = fechaini;
        this.fechafin = fechafin;
        this.distrito = u.getDstrct();
        this.usuario = u.getLogin();
        super.start();
    }
    
    public synchronized void run(){
        try{
            
            model.LogProcesosSvc.InsertProceso( "Reporte OC Anticipo sin Tr�fico", this.hashCode(), "Reporte OC Anticipo sin Tr�fico", usuario );
            //obtener cabecera de ruta
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String path = rb.getString("ruta");
            
            File file = new File(path + "/exportar/migracion/"+usuario);
            file.mkdirs();
            
            String nombreArch= "ReporteOCAnticipo["+fechaini+"]["+fechafin+"].xls";
            String       Hoja  = "ReporteOCAnticipo";
            String       Hoja2 = "Otras OC Asociadas";
            String       Ruta  = path + "/exportar/migracion/"+ usuario +"/" + nombreArch;
            HSSFWorkbook wb    = new HSSFWorkbook();
            HSSFSheet    sheet = wb.createSheet(Hoja);
            HSSFSheet    sheet2 = wb.createSheet(Hoja2);
            HSSFRow      row   = null;
            HSSFRow      row2  = null;
            HSSFCell     cell  = null;
            
            
            
            /****  ENCABEZADO Y DEFINICION DE ESTILOS ************************************************/
            
            /** ENCABEZADO GENERAL *******************************/
            HSSFFont  fuente1 = wb.createFont();
            fuente1.setFontName("verdana");
            fuente1.setFontHeightInPoints((short)(16)) ;
            fuente1.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            fuente1.setColor((short)(0x1));
            
            HSSFCellStyle estilo1 = wb.createCellStyle();
            estilo1.setFont(fuente1);
            estilo1.setFillForegroundColor(HSSFColor.BLUE.index);
            estilo1.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            
            /** TEXTO EN EL ENCABEAZADO *************************/
            HSSFFont  fuente2 = wb.createFont();
            fuente2.setFontName("verdana");
            fuente2.setFontHeightInPoints((short)(11)) ;
            fuente2.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            fuente2.setColor((short)(0x1));
            
            HSSFCellStyle estilo2 = wb.createCellStyle();
            estilo2.setFont(fuente2);
            estilo2.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo2.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            
            /** ENCABEZADO DE LAS COLUMNAS***********************/
            HSSFFont  fuente3 = wb.createFont();
            fuente3.setFontName("verdana");
            fuente3.setFontHeightInPoints((short)(11)) ;
            fuente3.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            fuente3.setColor((short)(0x1));
            
            HSSFCellStyle estilo3 = wb.createCellStyle();
            estilo3.setFont(fuente3);
            estilo3.setFillForegroundColor(HSSFColor.BLUE.index);
            estilo3.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo3.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo3.setBottomBorderColor(HSSFColor.BLACK.index);
            estilo3.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo3.setLeftBorderColor  (HSSFColor.BLACK.index);
            estilo3.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo3.setRightBorderColor(HSSFColor.BLACK.index);
            estilo3.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo3.setTopBorderColor   (HSSFColor.BLACK.index);
            
            /** TEXTO NORMAL ************************************/
            HSSFFont  fuente4 = wb.createFont();
            fuente4.setFontName("verdana");
            fuente4.setFontHeightInPoints((short)(9)) ;
            fuente4.setBoldweight(HSSFFont.BOLDWEIGHT_NORMAL);
            fuente4.setColor((short)(0x0));
            
            HSSFCellStyle estilo4 = wb.createCellStyle();
            estilo4.setFont(fuente4);
            estilo4.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo4.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo4.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo4.setBottomBorderColor(HSSFColor.BLACK.index);
            estilo4.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo4.setLeftBorderColor  (HSSFColor.BLACK.index);
            estilo4.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo4.setRightBorderColor(HSSFColor.BLACK.index);
            estilo4.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo4.setTopBorderColor   (HSSFColor.BLACK.index);
            
            
            HSSFCellStyle estilo5 = wb.createCellStyle();
            estilo5.setFont(fuente4);
            estilo5.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo5.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo5.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo5.setBottomBorderColor(HSSFColor.BLACK.index);
            estilo5.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo5.setLeftBorderColor  (HSSFColor.BLACK.index);
            estilo5.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo5.setRightBorderColor(HSSFColor.BLACK.index);
            estilo5.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo5.setTopBorderColor   (HSSFColor.BLACK.index);
            //estilo5.setDataFormat(wb.createDataFormat().getFormat("####-##-## ##:##:##"));
            
            HSSFCellStyle estilo6 = wb.createCellStyle();
            estilo6.setFont(fuente4);
            estilo6.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo6.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo6.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo6.setBottomBorderColor(HSSFColor.BLACK.index);
            estilo6.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo6.setLeftBorderColor  (HSSFColor.BLACK.index);
            estilo6.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo6.setRightBorderColor(HSSFColor.BLACK.index);
            estilo6.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo6.setTopBorderColor   (HSSFColor.BLACK.index);
            //estilo6.setDataFormat(wb.createDataFormat().getFormat("###,##"));
            
            
            /**************************
             * HOJA 1
             **************************/
            row  = sheet.createRow((short)(0));
            
            for (int j=0;j<10;j++) {
                cell = row.createCell((short)(j)); cell.setCellStyle(estilo1);
            }
            
            row  = sheet.createRow((short)(1));
            row  = sheet.createRow((short)(2));
            row  = sheet.createRow((short)(3));
            row  = sheet.createRow((short)(4));
            
            sheet.createFreezePane(0,5);
            
            row  = sheet.getRow((short)(0));
            cell = row.getCell((short)(0));
            cell.setCellValue("TRANSPORTE SANCHEZ POLO");
            
            
            row  = sheet.getRow((short)(1));
            cell = row.createCell((short)(0));
            cell.setCellStyle(estilo2);
            cell.setCellValue("Reporte de OC con  Anticipo sin Tr�fico");
            
            /*************************************************************************************/
            
            /************************
             * HOJA2
             ****************************/
            row  = sheet2.createRow((short)(0));
            
            for (int j=0;j<10;j++) {
                cell = row.createCell((short)(j)); cell.setCellStyle(estilo1);
            }
            
            row  = sheet2.createRow((short)(1));
            row  = sheet2.createRow((short)(2));
            row  = sheet2.createRow((short)(3));
            row  = sheet2.createRow((short)(4));
            
            sheet.createFreezePane(0,5);
            
            row  = sheet.getRow((short)(0));
            cell = row.getCell((short)(0));
            cell.setCellValue("TRANSPORTE SANCHEZ POLO");
            
            
            row  = sheet.getRow((short)(1));
            cell = row.createCell((short)(0));
            cell.setCellStyle(estilo2);
            cell.setCellValue("Reporte de OC con  Anticipo sin Tr�fico");
            
            /*************************************************************************************/
            
            
            
            /*
             HOJA1
             */
            int Fila = 4;
            
            row  = sheet.createRow((short)(Fila));
            
            sheet.setColumnWidth((short)0, (short)5000 );
            cell = row.createCell((short)(0));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Agencia OC");
            
            sheet.setColumnWidth((short)1, (short)5000 );
            cell = row.createCell((short)(1));
            cell.setCellStyle(estilo3);
            cell.setCellValue("User generador OC");
            
            sheet.setColumnWidth((short)2, (short)5000 );
            cell = row.createCell((short)(2));
            cell.setCellStyle(estilo3);
            cell.setCellValue("OC");
            
            sheet.setColumnWidth((short)3, (short)9000 );
            cell = row.createCell((short)(3));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Contador planillas");
            
            sheet.setColumnWidth((short)4, (short)10000 );
            cell = row.createCell((short)(4));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Valor OC");
            
            sheet.setColumnWidth((short)5, (short)7000 );
            cell = row.createCell((short)(5));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Placa Vehiculo");
            
            sheet.setColumnWidth((short)6, (short)7000 );
            cell = row.createCell((short)(6));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Fecha gen OC");
            
            sheet.setColumnWidth((short)7, (short)6000 );
            cell = row.createCell((short)(7));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Dias gen OC");
            
            sheet.setColumnWidth((short)8, (short)7000 );
            cell = row.createCell((short)(8));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Fecha print OC");
            
            sheet.setColumnWidth((short)9, (short)7000 );
            cell = row.createCell((short)(9));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Fecha Anticipo");
            
            sheet.setColumnWidth((short)10, (short)5000 );
            cell = row.createCell((short)(10));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Valor Anticipo");
            
            sheet.setColumnWidth((short)11, (short)5000 );
            cell = row.createCell((short)(11));
            cell.setCellStyle(estilo3);
            cell.setCellValue("No Cheque");
            
            sheet.setColumnWidth((short)12, (short)5000 );
            cell = row.createCell((short)(12));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Fecha print Chq");
            
            sheet.setColumnWidth((short)13, (short)7000 );
            cell = row.createCell((short)(13));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Ruta");
            
            sheet.setColumnWidth((short)14, (short)8000 );
            cell = row.createCell((short)(14));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Agencia origen");
            
            sheet.setColumnWidth((short)15, (short)8000 );
            cell = row.createCell((short)(15));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Agencia Destino");
            
            sheet.setColumnWidth((short)16, (short)8000 );
            cell = row.createCell((short)(16));
            cell.setCellStyle(estilo3);
            cell.setCellValue("No Puestos Control");
            
            sheet.setColumnWidth((short)17, (short)7000 );
            cell = row.createCell((short)(17));
            cell.setCellStyle(estilo3);
            cell.setCellValue("OT");
            
            sheet.setColumnWidth((short)18, (short)5000 );
            cell = row.createCell((short)(18));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Valor OT");
            
            sheet.setColumnWidth((short)19, (short)140000 );
            cell = row.createCell((short)(19));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Descripcion OT");
            
            sheet.setColumnWidth((short)20, (short)12000 );
            cell = row.createCell((short)(20));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Cliente");
            
            sheet.setColumnWidth((short)21, (short)5000 );
            cell = row.createCell((short)(21));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Factura Cliente");
            
            sheet.setColumnWidth((short)22, (short)7000 );
            cell = row.createCell((short)(22));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Agencia Anticipo");
            
            sheet.setColumnWidth((short)23, (short)7000 );
            cell = row.createCell((short)(23));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Ced gen Anticipo");
            
            sheet.setColumnWidth((short)24, (short)7000 );
            cell = row.createCell((short)(24));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Nombre gen Anticipo");
            
            sheet.setColumnWidth((short)25, (short)7000 );
            cell = row.createCell((short)(25));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Valor Flete");
            
            sheet.setColumnWidth((short)26, (short)7000 );
            cell = row.createCell((short)(26));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Fecha Cumplido OC");
            
            sheet.setColumnWidth((short)27, (short)7000 );
            cell = row.createCell((short)(27));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Ced User Cumplido");
            
            sheet.setColumnWidth((short)28, (short)7000 );
            cell = row.createCell((short)(28));
            cell.setCellStyle(estilo3);
            cell.setCellValue("User Cumplido");
            
            sheet.setColumnWidth((short)29, (short)7000 );
            cell = row.createCell((short)(29));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Tipo Viaje");
            
            sheet.setColumnWidth((short)30, (short)7000 );
            cell = row.createCell((short)(30));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Discrepancia");
            
            sheet.setColumnWidth((short)31, (short)7000 );
            cell = row.createCell((short)(31));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Clasificacion");
            
            sheet.setColumnWidth((short)32, (short)7000 );
            cell = row.createCell((short)(32));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Rango Demora");
            
            sheet.setColumnWidth((short)33, (short)7000 );
            cell = row.createCell((short)(33));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Calificacion");
            /*FIN HOJA1*/
            
            
            /*
             HOJA2
             */
            
            int Fila1 = 4;
            
            row  = sheet2.createRow((short)(Fila1));
            
            sheet2.setColumnWidth((short)0, (short)5000 );
            cell = row.createCell((short)(0));
            cell.setCellStyle(estilo3);
            cell.setCellValue("Agencia OC");
            
            sheet.setColumnWidth((short)1, (short)5000 );
            cell = row.createCell((short)(1));
            cell.setCellStyle(estilo3);
            cell.setCellValue("OC");
            
            sheet2.setColumnWidth((short)2, (short)50000 );
            cell = row.createCell((short)(2));
            cell.setCellStyle(estilo3);
            cell.setCellValue("OC Asociadas");
            
            
            
            
            
            
            
            /*
             HOJA1
             */
            Iterator It = this.Lista.iterator();
            while(It.hasNext()) {
                
                ReporteOCAnticipo rp = (ReporteOCAnticipo) It.next();
                Fila++;
                row  = sheet.createRow((short)(Fila));
                
                cell = row.createCell((short)(0));
                cell.setCellStyle(estilo4);
                cell.setCellValue((rp.getCiudad()!=null)?rp.getCiudad():"");
                
                cell = row.createCell((short)(1));
                cell.setCellStyle(estilo4);
                cell.setCellValue((rp.getDespachador()!=null)?rp.getDespachador():"");
                
                cell = row.createCell((short)(2));
                cell.setCellStyle(estilo4);
                cell.setCellValue((rp.getNumpla()!=null)?rp.getNumpla():"");
                
                cell = row.createCell((short)(3));
                cell.setCellStyle(estilo4);
                cell.setCellValue((rp.getContaroc()!=null)?rp.getContaroc():"");
                
                cell = row.createCell((short)(4));
                cell.setCellStyle(estilo4);
                cell.setCellValue((rp.getVlrpla2()!=null)?rp.getVlrpla2():"");
                
                cell = row.createCell((short)(5));
                cell.setCellStyle(estilo4);
                cell.setCellValue((rp.getPlatlr()!=null)?rp.getPlatlr():"");
                
                cell = row.createCell((short)(6));
                cell.setCellStyle(estilo5);
                cell.setCellValue(( rp.getCreation_date() != null )?rp.getCreation_date():"");
                
                cell = row.createCell((short)(7));
                cell.setCellStyle(estilo4);
                cell.setCellValue(( rp.getDif() != null )?rp.getDif():"");
                
                cell = row.createCell((short)(8));
                cell.setCellStyle(estilo4);
                cell.setCellValue(( rp.getPrinter_date() != null )?rp.getPrinter_date():"");
                
                cell = row.createCell((short)(9));
                cell.setCellStyle(estilo6);
                cell.setCellValue(( rp.getFechaanticipo() != null )?rp.getFechaanticipo():"");
                
                cell = row.createCell((short)(10));
                cell.setCellStyle(estilo4);
                cell.setCellValue(( rp.getVlr() != null )?rp.getVlr():"");
                
                cell = row.createCell((short)(11));
                cell.setCellStyle(estilo4);
                cell.setCellValue(( rp.getDocument() != null )?rp.getDocument():"");
                
                cell = row.createCell((short)(12));
                cell.setCellStyle(estilo4);
                cell.setCellValue(( rp.getFecha_cheque() != null )?rp.getFecha_cheque():"");
                
                cell = row.createCell((short)(13));
                cell.setCellStyle(estilo4);
                cell.setCellValue(( rp.getRuta_pla() != null )?rp.getRuta_pla():"");
                
                cell = row.createCell((short)(14));
                cell.setCellStyle(estilo4);
                cell.setCellValue(( rp.getOrigenoc() != null )?rp.getOrigenoc():"");
                
                cell = row.createCell((short)(15));
                cell.setCellStyle(estilo4);
                cell.setCellValue(( rp.getDestinooc() != null )?rp.getDestinooc():"");
                
                cell = row.createCell((short)(16));
                cell.setCellStyle(estilo4);
                cell.setCellValue(( rp.getPuestocontrol() != null )?rp.getPuestocontrol():"");
                
                cell = row.createCell((short)(17));
                cell.setCellStyle(estilo4);
                cell.setCellValue(( rp.getNumrem() != null )?rp.getNumrem():"");
                
                cell = row.createCell((short)(18));
                cell.setCellStyle(estilo4);
                cell.setCellValue(( rp.getVlrrem2() != null )?rp.getVlrrem2():"");
                
                cell = row.createCell((short)(19));
                cell.setCellStyle(estilo6);
                String cliente = ( rp.getCliente() != null )?rp.getCliente():"";
                String descrip = ( rp.getDescripcion_via() != null )?rp.getDescripcion_via():"";
                cell.setCellValue( cliente + " - " + descrip );
                
                cell = row.createCell((short)(20));
                cell.setCellStyle(estilo4);
                cell.setCellValue(( rp.getCliente() != null )?rp.getCliente():"");
                
                cell = row.createCell((short)(21));
                cell.setCellStyle(estilo4);
                cell.setCellValue(( rp.getFacturacliente() != null )?rp.getFacturacliente():"");
                
                cell = row.createCell((short)(22));
                cell.setCellStyle(estilo4);
                cell.setCellValue(( rp.getAgenciaanticipo() != null )?rp.getAgenciaanticipo():"");
                
                cell = row.createCell((short)(23));
                cell.setCellStyle(estilo4);
                cell.setCellValue(( rp.getCedulaanticipo() != null )?rp.getCedulaanticipo():"");
                
                cell = row.createCell((short)(24));
                cell.setCellStyle(estilo4);
                cell.setCellValue(( rp.getNombreanticipo() != null )?rp.getNombreanticipo():"");
                
                cell = row.createCell((short)(25));
                cell.setCellStyle(estilo4);
                cell.setCellValue(( rp.getValorflete() != null )?rp.getValorflete():"");
                
                cell = row.createCell((short)(26));
                cell.setCellStyle(estilo4);
                cell.setCellValue(( rp.getFechacumplido() != null )?rp.getFechacumplido():"");
                
                cell = row.createCell((short)(27));
                cell.setCellStyle(estilo4);
                cell.setCellValue(( rp.getCedulacumplido() != null )?rp.getCedulacumplido():"");
                
                cell = row.createCell((short)(28));
                cell.setCellStyle(estilo4);
                cell.setCellValue(( rp.getNombrecumplido() != null )?rp.getNombrecumplido():"");
                
                cell = row.createCell((short)(29));
                cell.setCellStyle(estilo4);
                cell.setCellValue(( rp.getTipo_viaje() != null )?rp.getTipo_viaje():"");
                
                cell = row.createCell((short)(30));
                cell.setCellStyle(estilo4);
                cell.setCellValue(( rp.getDiscrepancia() != null )?rp.getDiscrepancia():"");
                
                cell = row.createCell((short)(31));
                cell.setCellStyle(estilo4);
                cell.setCellValue(( rp.getClasificacion() != null )?rp.getClasificacion():"");
                
                cell = row.createCell((short)(32));
                cell.setCellStyle(estilo4);
                cell.setCellValue(( rp.getRango() != null)?rp.getRango():"");
                
                cell = row.createCell((short)(33));
                cell.setCellStyle(estilo4);
                cell.setCellValue(( rp.getCalificacion()!= null)?rp.getCalificacion():"");
                
            }
            
            
            /*
             HOJA2
             */
            Iterator It1 = this.Lista.iterator();
            while(It1.hasNext()) {
                
                ReporteOCAnticipo rp = (ReporteOCAnticipo) It1.next();
                Fila1++;
                row  = sheet2.createRow((short)(Fila1));
                
                cell = row.createCell((short)(0));
                cell.setCellStyle(estilo4);
                cell.setCellValue((rp.getCiudad()!=null)?rp.getCiudad():"");
                
                cell = row.createCell((short)(1));
                cell.setCellStyle(estilo4);
                cell.setCellValue((rp.getNumpla()!=null)?rp.getNumpla():"");
                
                cell = row.createCell((short)(2));
                cell.setCellStyle(estilo4);
                cell.setCellValue(( rp.getOcasociadas() != null )?rp.getOcasociadas():"");
                
            }
            
            /************************************************************************************/
            /**** GUARDAR DATOS EN EL ARCHIVO  ***/
            FileOutputStream fo = new FileOutputStream(Ruta);
            wb.write(fo);
            fo.close();
            model.LogProcesosSvc.finallyProceso( "Reporte OC Anticipo sin Tr�fico", this.hashCode(), usuario, "PROCESO EXITOSO" );
        }catch( Exception ex ){
            ex.printStackTrace();
            //Capturo errores finalizando proceso
            try{
                model.LogProcesosSvc.finallyProceso( "Reporte OC Anticipo sin Tr�fico", this.hashCode(), usuario, "ERROR : " + ex.getMessage() );
            } catch( Exception f ){
                f.printStackTrace();
                try{
                    model.LogProcesosSvc.finallyProceso( "Reporte OC Anticipo sin Tr�fico",this.hashCode(), usuario, "ERROR : " + f.getMessage() );
                } catch( Exception p ){ p.printStackTrace(); }
            }
            
        }finally{
            super.destroy();
        }
    }
    
}
