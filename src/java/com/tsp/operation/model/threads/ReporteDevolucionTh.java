/******************************************************************
* Nombre                        ReporteDevolucionTh.java
* Descripci�n                   Clase Threads para el reporte de devoluciones
* Autor                         ricardo rosero
* Fecha                         20/01/2006
* Versi�n                       1.0
* Coyright                      Transportes Sanchez Polo S.A.
*******************************************************************/

package com.tsp.operation.model.threads;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import java.io.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.controller.ReporteCumplidoAction;
import com.tsp.operation.model.services.*;
import com.tsp.operation.model.*;
import java.util.*;
import java.text.*;

/**
 *
 * @author  rrosero
 */
public class ReporteDevolucionTh extends Thread{
    //atributos
    private Vector informe;
    private Vector resumen;
    private String user;
    private String mensaje;
    private String fechai;
    private String fechaf;
    private Usuario usu;
    
    /** Creates a new instance of ReporteDevolucionTh */
    public ReporteDevolucionTh() {
    }
    
    /**
     * funcion que inicializa el hilo
     * @autor                       ricardo rosero
     * @param                       Vector informe, usuario, fecha inicial, fecha final
     * @throws                      SQLException
     * @version                     1.0
     */
    public void start(Vector informe, String user, String fechai, String fechaf){
        this.informe = informe;            
        this.user = user;
        this.fechaf = fechaf;
        this.fechai = fechai;

        super.start();
    }

    /**
     * funcion que ejecuta el hilo
     * @autor                       ricardo rosero
     * @throws                      SQLException
     * @version                     1.0
     */
    public synchronized void run(){
        Model model = new Model();
        try{
            model.LogProcesosSvc.InsertProceso(
            "ReporteDevolucion", 
            this.hashCode(), 
            "Genera un reporte de devoluciones entre dos fechas", 
            this.user );  

            //Fecha del sistema             
            Calendar FechaHoy = Calendar.getInstance();
            Date d = FechaHoy.getTime();
            SimpleDateFormat s = new SimpleDateFormat("yyyyMMdd");
            SimpleDateFormat s1 = new SimpleDateFormat("yyyy-MM-dd-hh:mm");
            SimpleDateFormat fec = new SimpleDateFormat("yyyy-MM-dd");            
            String FechaFormated = s.format(d);
            String FechaFormated1 = s1.format(d);
            String Fecha = fec.format(d);


            //obtener cabecera de ruta
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String path = rb.getString("ruta");
            //armas la ruta
            String Ruta1  = path + "/exportar/migracion/" + user + "/";
            //crear la ruta
            File file = new File(Ruta1);
            file.mkdirs();

            POIWrite xls = new POIWrite(Ruta1 +"ReporteDevoluciones_" + FechaFormated + ".xls", user, Fecha);
            String nom_archivo = "ReporteDevoluciones_" + FechaFormated + ".xls";
            this.mensaje = nom_archivo;

            //Definici�n de Estilos
            HSSFCellStyle fecha  = xls.nuevoEstilo("Arial", 9, false , false, "yyyy/mm/dd"  , xls.NONE , xls.NONE , xls.NONE );
            HSSFCellStyle texto  = xls.nuevoEstilo("Arial", 9, false , false, "text"        , xls.NONE , xls.NONE , xls.NONE);
            HSSFCellStyle numero = xls.nuevoEstilo("Arial", 9, false , false, ""            , xls.NONE , xls.NONE , xls.NONE);
            
            HSSFCellStyle header1      = xls.nuevoEstilo("Arial", 12, true , false, "text"        , HSSFColor.WHITE.index , HSSFColor.GREEN.index , HSSFCellStyle.ALIGN_LEFT);
            HSSFCellStyle header2      = xls.nuevoEstilo("Arial", 18, true , false, "text"        , HSSFColor.WHITE.index , HSSFColor.ORANGE.index , HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle header3      = xls.nuevoEstilo("Arial", 12, true , false, "text"        , HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
            HSSFCellStyle texto2      = xls.nuevoEstilo("Arial", 12, false , false, "text"        , HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
            HSSFCellStyle titulo      = xls.nuevoEstilo("Arial", 12, true  , false, "text"        , HSSFColor.BLACK.index , HSSFColor.PINK.index , HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle fechatitle  = xls.nuevoEstilo("Arial", 12, true  , false, "yyyy/mm/dd"  , HSSFColor.RED.index , xls.NONE, HSSFCellStyle.ALIGN_CENTER );
            HSSFCellStyle entero      = xls.nuevoEstilo("Arial", 12, false , false, "text"        , HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle flotante      = xls.nuevoEstilo("Arial", 12, false , false, "text"        , HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_RIGHT);
            
            header1.setBorderTop(HSSFCellStyle.BORDER_MEDIUM);
            header1.setBorderBottom(HSSFCellStyle.BORDER_THIN);
            header1.setAlignment(HSSFCellStyle.ALIGN_CENTER);
            header2.setAlignment(HSSFCellStyle.ALIGN_CENTER);

            xls.obtenerHoja("Reporte Devoluciones");
            xls.cambiarMagnificacion(3,4);

            // cabecera

            xls.combinarCeldas(4, 0, 4, 23);
            xls.combinarCeldas(6, 0, 6, 23);
            xls.adicionarCelda(4, 0, "Reporte de Devoluciones", header2);
            xls.adicionarCelda(6, 0, "Reporte de fecha: " + Fecha , texto2);

            int fila = 8;
            int col  = 0;

            xls.adicionarCelda(fila, col++, "EQUIPO", header1);
            xls.adicionarCelda(fila, col++, "PLANILLA", header1);
            xls.adicionarCelda(fila, col++, "REMESA", header1);
            xls.adicionarCelda(fila, col++, "ORIGEN", header1);
            xls.adicionarCelda(fila, col++, "DESTINO", header1);
            xls.adicionarCelda(fila, col++, "FECHA DE ENTREGA", header1);
            xls.adicionarCelda(fila, col++, "FECHA DE DEVOLUCION", header1);
            xls.adicionarCelda(fila, col++, "CLIENTE", header1);
            xls.adicionarCelda(fila, col++, "DOCUMENTO", header1);
            xls.adicionarCelda(fila, col++, "TIPO DOCUMENTO", header1);
            xls.adicionarCelda(fila, col++, "CODIGO PRODUCTO", header1);
            xls.adicionarCelda(fila, col++, "PRODUCTO", header1);
            xls.adicionarCelda(fila, col++, "TIPO DE EMPAQUE", header1);
            xls.adicionarCelda(fila, col++, "CANT. DEVUELTAS", header1);
            xls.adicionarCelda(fila, col++, "NOMBRE DEL CONTACTO", header1);
            xls.adicionarCelda(fila, col++, "MOTIVO DEVOLUCION", header1);
            xls.adicionarCelda(fila, col++, "RESP. DEVOLUCION", header1);
            xls.adicionarCelda(fila, col++, "NOTA DEBITO O CREDITO", header1);
            xls.adicionarCelda(fila, col++, "FECHA DEV. BODEGA", header1);
            xls.adicionarCelda(fila, col++, "UBICACION", header1);
            xls.adicionarCelda(fila, col++, "FECHA REP. RECHAZO", header1);
            xls.adicionarCelda(fila, col++, "RECHAZADO POR", header1);
            xls.adicionarCelda(fila, col++, "No RECHAZO O DEVOLUCION", header1);
            xls.adicionarCelda(fila, col++, "FECHA CUMPLIDO", header1);


            fila = 9;
            for(int i=0; i<informe.size(); i++){
                ReporteDevolucion fit = (ReporteDevolucion) informe.elementAt(i);
                col = 0;
                xls.adicionarCelda(fila, col++, fit.getPlaveh(), texto);
                xls.adicionarCelda(fila, col++, fit.getNumpla(), texto);
                xls.adicionarCelda(fila, col++, fit.getNumrem(), texto);
                xls.adicionarCelda(fila, col++, fit.getOrirem(), texto);
                xls.adicionarCelda(fila, col++, fit.getDesrem(), texto);
                xls.adicionarCelda(fila, col++, fit.getFechareporte(), texto);
                xls.adicionarCelda(fila, col++, fit.getFec_devolucion(), texto);
                xls.adicionarCelda(fila, col++, fit.getCliente(), texto);
                xls.adicionarCelda(fila, col++, fit.getDocumento(), texto);
                xls.adicionarCelda(fila, col++, fit.getTipo_doc(), texto);
                xls.adicionarCelda(fila, col++, fit.getCodprod(), texto);
                xls.adicionarCelda(fila, col++, fit.getProducto(), texto);
                xls.adicionarCelda(fila, col++, fit.getTipoemp(), texto);
                xls.adicionarCelda(fila, col++, fit.getCantdev(), texto);
                xls.adicionarCelda(fila, col++, fit.getNombredev(), texto);
                xls.adicionarCelda(fila, col++, fit.getMotivodev(), texto);
                xls.adicionarCelda(fila, col++, fit.getRespondev(), texto);
                xls.adicionarCelda(fila, col++, fit.getNotasxom(), texto);
                xls.adicionarCelda(fila, col++, fit.getFechdevbod(), texto);
                xls.adicionarCelda(fila, col++, fit.getUbicacion(), texto);
                xls.adicionarCelda(fila, col++, fit.getFecrechazo(), texto);
                xls.adicionarCelda(fila, col++, fit.getNombrerep(), texto);
                xls.adicionarCelda(fila, col++, fit.getNumrechazo(), texto);
                xls.adicionarCelda(fila, col++, fit.getFec_cumplido(), texto);
                fila++;                                
            }
            xls.cerrarLibro();

            model.LogProcesosSvc.finallyProceso(
            "ReporteDevolucion",
            this.hashCode(), 
            this.user, 
            "PROCESO EXITOSO"
            );

        }
        catch (Exception ex){
            ex.printStackTrace();
            ////System.out.println("ERROR AL GENERAR EL ARCHIVO XLS : " + ex.getMessage());
            try{            
                model.LogProcesosSvc.finallyProceso(
                "ReporteDevolucion",
                this.hashCode(), 
                this.user,
                "ERROR :" + ex.getMessage()
                );
            }
            catch(Exception f){
                try{               
                    model.LogProcesosSvc.finallyProceso(
                    "ReporteDevolucion",
                    this.hashCode(), 
                    this.user,
                    "ERROR :"
                    );
                }catch(Exception p){  }
            }
        }

    }

    /**
     * Getter for property mensaje.
     * @return Value of property mensaje.
     */
    public java.lang.String getMensaje() {
        return mensaje;
    }

    /**
     * Setter for property mensaje.
     * @param mensaje New value of property mensaje.
     */
    public void setMensaje(java.lang.String mensaje) {
        this.mensaje = mensaje;
    }
    
    /**
     * Getter for property usu.
     * @return Value of property usu.
     */
    public com.tsp.operation.model.beans.Usuario getUsu() {
        return usu;
    }
    
    /**
     * Setter for property usu.
     * @param usu New value of property usu.
     */
    public void setUsu(com.tsp.operation.model.beans.Usuario usu) {
        this.usu = usu;
    }
    
}

