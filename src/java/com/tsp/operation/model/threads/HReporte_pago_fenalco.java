/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.threads;

/**
 *
 * @author jpinedo
 */


import java.io.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.util.Util;

//librerias pdf

import com.lowagie.text.PageSize;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import com.lowagie.text.Image;
import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Font;
import com.lowagie.text.HeaderFooter;
import com.lowagie.text.Rectangle;
import com.tsp.operation.model.Model;
import com.tsp.operation.model.NitDAO;
import com.tsp.util.Utility;
import java.sql.Timestamp;

import java.text.SimpleDateFormat;
public class HReporte_pago_fenalco extends Thread {

     private Model     model= new Model();
      Proveedor prov = new Proveedor();
      ProveedorService ps= new ProveedorService();
      NitSot objnit = new NitSot();
      NitDAO nitDAO = new NitDAO();
      String  url_logo="fintrapdf.gif";
      Vector negs= new Vector();
      List <String> negs_MC= new ArrayList <String> ();
      List <String> negs_TSP= new ArrayList <String> ();
      Vector nits =new Vector();
      Usuario usuario=null;
     

  public HReporte_pago_fenalco(Vector negsx, Usuario usuario)
  {
        this.negs=negsx;
        this.usuario=usuario;
  }



       public void start() {
         try{         
        super.start();
        }catch(Exception e){

       }
    }



     public synchronized void run()
     {
         try
         {
             GestionSolicitudAvalService solserv = new GestionSolicitudAvalService(this.usuario.getBd());
            String mensajeMC="";
             for(int j=0;j<this.negs.size();j++){
                 SolicitudAval sol=solserv.buscarSolicitud((String)this.negs.get(j));
                 if(sol.getTipoConv().equals("Microcredito")){
                     if(sol.isCat()){
                         BeanGeneral mail =model.Negociossvc.mailinfo((String)this.negs.get(j));
                         mensajeMC+="<br/>"+this.negs.get(j)+" "+mail.getValor_02()+" "+mail.getValor_01()+" "+mail.getValor_05()+" "+sol.getAsesor();
                         negs_MC.add((String)this.negs.get(j));
                     }else{
                         negs_TSP.add((String)this.negs.get(j));
                     }
                     this.negs.remove(this.negs.get(j));
                     j--;
                 }
             }
             if (this.negs_MC.size() > 0) {
                 String correosMC = solserv.nombreTablagen("CORREODES", "MC");
                mensajeMC="Les Informamos que los siguientes negocios fueron desembolsados satisfatoreamente:"+mensajeMC;
                     this.envia_correo("reportedepago@fintra.co", correosMC, "", "ReportePago", mensajeMC, "", "");
             }
             if (this.negs_TSP.size() > 0) {
                 String correosTSP = solserv.nombreTablagen("CORREODES", "TSP");
                
                 for (int j = 0; j < this.negs_TSP.size(); j++) {
                      BeanGeneral mail =model.Negociossvc.mailinfo(this.negs_TSP.get(j));
                     String mensajeTSP="Le informamos, que el negocio No. "+this.negs_TSP.get(j)+" correspondiente al Sr(a). "+mail.getValor_01()+"  con cedula No. "+mail.getValor_02()+
                             "  se le desembolso el valor de $"+mail.getValor_05()+" satisfactoreamente.";

                     this.envia_correo("reportedepago@fintra.co", correosTSP, "", "ReportePago", mensajeTSP, "", "");

                 }
             }
            if (this.negs.size() > 0) {
                 this.nits = model.Negociossvc.VerNits(this.negs);

                   for (int i=0;i<nits.size();i++)
                   {
                   BeanGeneral obj=(BeanGeneral)nits.get(i);
                   if(this.exportarReportePdf(obj,negs))
                   {

                       String  directorio = this.directorioArchivo("DOCUMENTOS");
                       String ruta=directorio+"/";
                       String  archivo="ReportePago"+prov.getC_payment_name()+".pdf";
                   if(!objnit.getE_mail().equals(""))
                   {

                   this.envia_correo("reportedepago@fintra.co","jpinedo@fintra.co", "asesor.comercial3@fintra.co;asistente.administrativo@fintra.co", "ReportePago", this.getMensaje(), ruta, archivo);
                   }
                   else
                   {
                     String mensaje="Error enviado soporte de pago a "+prov.getC_payment_name()+" Email invalido";
                     this.envia_correo("reportedepago@fintra.co","jpinedo@fintra.co", "asesor.comercial3@fintra.co;asistente.administrativo@fintra.co", "ReportePago", mensaje,"","");
                   }

                 }

                }
             }
         }
         catch (Exception ex) {
            Logger.getLogger(HReporte_pago_fenalco.class.getName()).log(Level.SEVERE, null, ex);
        }

     }






/***********************************************soporte pdf transferencia negocios ************************************************************/
protected boolean exportarReportePdf(BeanGeneral obj,Vector negs) throws Exception {
    
    prov = ps.obtenerProveedor(obj.getValor_01(), "");
    objnit = nitDAO.searchNit(obj.getValor_01());
    Vector vn = model.Negociossvc.VerInfoPago(negs, obj.getValor_01());

    double vlr = model.Negociossvc.Total_pagado(vn);
    Negocios neg = (Negocios) vn.get(0);

    boolean generado = true;
    String directorio = "";
    String documento_detalle = "";
    ResourceBundle rb = null;
    try {
        rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
        directorio = this.directorioArchivo("DOCUMENTOS", "ReportePago" + prov.getC_payment_name(), "pdf");
        Font fuente = new Font(Font.HELVETICA, 9);
        Font fuenteB = new Font(Font.HELVETICA, 9, Font.BOLD);
        Document documento = null;
        documento = this.createDoc();
        PdfWriter.getInstance(documento, new FileOutputStream(directorio));

        documento.setFooter(this.getMyFooter());
        documento.open();
        documento.newPage();
        Image img = Image.getInstance(rb.getString("ruta") + "/images/" + url_logo);
        img.scaleToFit(200, 200);
        documento.add(img);
        documento.add(Chunk.NEWLINE);
        documento.add(new Paragraph("802.022.016-1", fuenteB));
        documento.add(Chunk.NEWLINE);
        Paragraph p = new Paragraph("REPORTE DE PAGOS", fuenteB);
        p.setAlignment(Element.ALIGN_CENTER);
        documento.add(p);
        documento.add(Chunk.NEWLINE);

        /*----------------------------------datos_afiliado--------------------------------------*/
        PdfPTable tabla_datos_pro = this.DatosAfiliado(prov, vn);
        tabla_datos_pro.setWidthPercentage(100);
        documento.add(tabla_datos_pro);
        documento.add(Chunk.NEWLINE);
        documento.add(Chunk.NEWLINE);

        /*----------------------------------lista_negocios--------------------------------------*/
        p = new Paragraph("DETALLE DE PAGOS", fuenteB);
        p.setAlignment(Element.ALIGN_LEFT);
        documento.add(p);
        documento.add(Chunk.NEWLINE);
        if (obj.getValor_02().equals("N")) {
            PdfPTable tabla_negocios = this.TrasferenciasNegocios(vn);
            tabla_negocios.setWidthPercentage(100);
            documento.add(tabla_negocios);
        } else {
            PdfPTable tabla_negocios = this.TrasferenciasNegociosEdu(vn);
            tabla_negocios.setWidthPercentage(100);
            documento.add(tabla_negocios);
        }

        documento.add(Chunk.NEWLINE);

        if (obj.getValor_02().equals("N")) {
            PdfPTable tabla_total = this.Total(vlr);
            tabla_total.setWidthPercentage(100);
            documento.add(tabla_total);
        } else {
            PdfPTable tabla_total = this.TotalEdu(vlr);
            tabla_total.setWidthPercentage(100);
            documento.add(tabla_total);
        }
        documento.add(Chunk.NEWLINE);
        documento.add(Chunk.NEWLINE);
        documento.close();


    } catch (Exception e) {
        generado = false;
        System.out.println("error al generar pdf : " + e.toString());
        e.printStackTrace();
    }
    //Envio de Correo

    return generado;
    }



    private String directorioArchivo(String user, String cons, String extension) throws Exception {
        String ruta = "";
        try {
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            ruta = rb.getString("ruta") + "/exportar/migracion/" + user.toUpperCase();
            SimpleDateFormat fmt = new SimpleDateFormat("yyyMMdd_hhmmss");
            File archivo = new File(ruta);
            if (!archivo.exists()) {
                archivo.mkdirs();
            }
            ruta = ruta + "/" + cons +/* "_" + fmt.format(new Date()) +*/ "." + extension;
        } catch (Exception e) {
            throw new Exception("Error al generar el directorio: " + e.toString());
        }
        return ruta;
    }



       private Document createDoc() {
        Document doc = new Document(PageSize.LETTER, 60, 40, 40, 40);//
        doc.addAuthor ("Fintra S.A");
        doc.addSubject ("Trasferencias");
        return doc;
    }


       public HeaderFooter getMyFooter() throws ServletException
        {
        try {
             Phrase p = new Phrase();
            Font fuente = new Font(Font.HELVETICA, 9);
            Font fuenteB = new Font(Font.HELVETICA, 9, Font.BOLD);
             p.clear();


             p.add(new Paragraph("Carrera 53 # 79 - 01 Local 205 Barranquilla, Colombia\n",fuenteB));
             p.add(new Paragraph("PBX: 57 5 3679900 FAX: 57 5 3679906\n",fuenteB));
             p.add(new Paragraph("www.fintra.co\n",fuenteB));
             HeaderFooter header = new HeaderFooter(p, false);
             header.setBorder(Rectangle.NO_BORDER);
             header.setAlignment(Paragraph.ALIGN_CENTER);

             return header;
         } catch (Exception ex) {
            throw new ServletException("Header Error");
         }
     }



        /************************************** DATOS AFILIADO***************************************************/
  protected PdfPTable DatosAfiliado(Proveedor prv,Vector  vn) throws DocumentException {
        Negocios ng= (Negocios)vn.get(0);
        String fecha=ng.getFechatran().substring(0, 18);
        PdfPTable tabla_temp = new PdfPTable(4);

        //double vlr=model.Negociossvc.Total_pagado(vn);
      float[] medidaCeldas = {0.170f, 0.500f,0.170f,0.160f};
        tabla_temp.setWidths(medidaCeldas);

        Font fuente = new Font(Font.HELVETICA, 9);
        Font fuenteB = new Font(Font.HELVETICA, 9, Font.BOLD);
        PdfPCell celda_temp = new PdfPCell();

        // fila 1
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("AFILIADO", fuenteB));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        celda_temp.setBorder(0);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setBorder(0);
        celda_temp.setPhrase(new Phrase(prv.getC_payment_name(), fuente));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);


        celda_temp = new PdfPCell();
        celda_temp.setBorder(0);
        celda_temp.setPhrase(new Phrase("NIT", fuenteB));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setBorder(0);
        celda_temp.setPhrase(new Phrase(prv.getC_nit(), fuente));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);




        // fila 1
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("TITULAR", fuenteB));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        celda_temp.setBorder(0);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setBorder(0);
        celda_temp.setPhrase(new Phrase(prv.getNombre_cuenta(), fuente));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);


        celda_temp = new PdfPCell();
        celda_temp.setBorder(0);
        celda_temp.setPhrase(new Phrase("NIT TITULAR", fuenteB));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setBorder(0);
        celda_temp.setPhrase(new Phrase(prv.getCedula_cuenta(), fuente));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);


        celda_temp = new PdfPCell();
        celda_temp.setBorder(0);
        celda_temp.setPhrase(new Phrase("BANCO", fuenteB));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setBorder(0);
        celda_temp.setColspan(3);
        celda_temp.setPhrase(new Phrase(prv.getC_banco_transfer(), fuente));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);

        celda_temp = new PdfPCell();
        celda_temp.setBorder(0);
        celda_temp.setPhrase(new Phrase("TIPO CTA", fuenteB));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setBorder(0);
        celda_temp.setColspan(3);
        celda_temp.setPhrase(new Phrase(prv.getC_tipo_cuenta(), fuente));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);

        celda_temp = new PdfPCell();
        celda_temp.setBorder(0);
        celda_temp.setPhrase(new Phrase("CUENTA BANCARIA", fuenteB));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setBorder(0);
        celda_temp.setColspan(3);
        celda_temp.setPhrase(new Phrase(prv.getC_numero_cuenta(), fuente));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);

        celda_temp = new PdfPCell();
        celda_temp.setBorder(0);
        celda_temp.setPhrase(new Phrase("FECHA DE PAGO", fuenteB));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setBorder(0);
        celda_temp.setColspan(3);
        celda_temp.setPhrase(new Phrase(fecha.substring(8, 10)+" de "+Utility.NombreMes(Integer.parseInt(fecha.substring(5, 7)))+" de "+fecha.substring(0, 4), fuente));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);


        celda_temp = new PdfPCell();
        celda_temp.setBorder(0);
        celda_temp.setPhrase(new Phrase("TOTAL PAGADO", fuenteB));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setBorder(0);
        celda_temp.setColspan(3);
        celda_temp.setPhrase(new Phrase(Utility.customFormat(this.Total_pagado(vn)), fuenteB));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        tabla_temp.addCell(celda_temp);

          return tabla_temp;
    }



  /************************************** trasferencias***************************************************/
  protected PdfPTable TrasferenciasNegocios( Vector   vn) throws DocumentException {

        double vlr=0,ip=0;
         PdfPTable tabla_temp = new PdfPTable(5);
        try
        {

        Negocios neg = new Negocios();


      float[] medidaCeldas = {0.05f, 0.135f, 0.160f, 0.500f, 0.200f};


      tabla_temp.setWidths(medidaCeldas);

        Font fuente = new Font(Font.TIMES_ROMAN, 9);
        Font fuenteB = new Font(Font.TIMES_ROMAN, 9, Font.BOLD);
        PdfPCell celda_temp = new PdfPCell();
        celda_temp = new PdfPCell();

        // columna 1
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("N�", fuenteB));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);

        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Negocio", fuenteB));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);

        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Id Cliente", fuenteB));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);

        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Cliente", fuenteB));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);

        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("Valor", fuenteB));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        tabla_temp.addCell(celda_temp);

       for (int i=0;i<vn.size();i++)
       {
            neg = (Negocios)vn.get(i);
            neg.setVr_desem(model.egresoService.getVlr(neg.getCod_negocio()));


            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase(""+(i+1), fuenteB));
            celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            tabla_temp.addCell(celda_temp);

            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase(neg.getCod_negocio(), fuente));
            celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            tabla_temp.addCell(celda_temp);

            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase(neg.getCod_cli(), fuente));
            celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            tabla_temp.addCell(celda_temp);

            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase(neg.getNom_cli(), fuente));
            celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            tabla_temp.addCell(celda_temp);

            celda_temp = new PdfPCell();
            celda_temp.setPhrase(new Phrase(Util.customFormat(neg.getVr_desem()), fuente));
            celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            tabla_temp.addCell(celda_temp);



        }
      }
        catch (Exception e)
        {

            System.out.println("error al generar pdf solicitud de aval: " + e.toString());
            e.printStackTrace();
        }
        return tabla_temp;

    }







    /************************************** trasferencias***************************************************/
  protected PdfPTable TrasferenciasNegociosEdu( Vector   vn) throws DocumentException {

        double vlr = 0, ip = 0;
      PdfPTable tabla_temp = new PdfPTable(6);
      try {

          Negocios neg = new Negocios();


          float[] medidaCeldas = {0.05f, 0.130f, 0.130f, 0.160f, 0.450f, 0.125f};


          tabla_temp.setWidths(medidaCeldas);

          Font fuente = new Font(Font.TIMES_ROMAN, 9);
          Font fuenteB = new Font(Font.TIMES_ROMAN, 9, Font.BOLD);
          PdfPCell celda_temp = new PdfPCell();
          celda_temp = new PdfPCell();

          // columna 1
          celda_temp = new PdfPCell();
          celda_temp.setPhrase(new Phrase("N�", fuenteB));
          celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
          tabla_temp.addCell(celda_temp);

          celda_temp = new PdfPCell();
          celda_temp.setPhrase(new Phrase("Negocio", fuenteB));
          celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
          tabla_temp.addCell(celda_temp);


          celda_temp = new PdfPCell();
          celda_temp.setPhrase(new Phrase("Codigo Est.", fuenteB));
          celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
          tabla_temp.addCell(celda_temp);

          celda_temp = new PdfPCell();
          celda_temp.setPhrase(new Phrase("Identificacion", fuenteB));
          celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
          tabla_temp.addCell(celda_temp);

          celda_temp = new PdfPCell();
          celda_temp.setPhrase(new Phrase("Estudiante", fuenteB));
          celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
          tabla_temp.addCell(celda_temp);

          celda_temp = new PdfPCell();
          celda_temp.setPhrase(new Phrase("Valor", fuenteB));
          celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
          tabla_temp.addCell(celda_temp);

          for (int i = 0; i < vn.size(); i++) {
              neg = (Negocios) vn.get(i);
              neg.setVr_desem(model.egresoService.getVlr(neg.getCod_negocio()));


              celda_temp = new PdfPCell();
              celda_temp.setPhrase(new Phrase("" + (i + 1), fuenteB));
              celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
              tabla_temp.addCell(celda_temp);

              celda_temp = new PdfPCell();
              celda_temp.setPhrase(new Phrase(neg.getCod_negocio(), fuente));
              celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
              tabla_temp.addCell(celda_temp);

              celda_temp = new PdfPCell();
              celda_temp.setPhrase(new Phrase(neg.getCodigou(), fuente));
              celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
              tabla_temp.addCell(celda_temp);


              celda_temp = new PdfPCell();
              celda_temp.setPhrase(new Phrase(neg.getCod_cli(), fuente));
              celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
              tabla_temp.addCell(celda_temp);

              celda_temp = new PdfPCell();
              celda_temp.setPhrase(new Phrase(neg.getNom_cli(), fuente));
              celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
              tabla_temp.addCell(celda_temp);

              celda_temp = new PdfPCell();
              celda_temp.setPhrase(new Phrase(Util.customFormat(neg.getVr_desem()), fuente));
              celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
              tabla_temp.addCell(celda_temp);



        }
      }
        catch (Exception e)
        {

            System.out.println("error al generar pdf solicitud de aval: " + e.toString());
            e.printStackTrace();
        }
        return tabla_temp;

    }


   /************************************** tabla total***************************************************/
  protected PdfPTable Total( double vlr  ) throws DocumentException {


        PdfPTable tabla_temp = new PdfPTable(5);
        float[] medidaCeldas = {0.05f, 0.135f, 0.160f, 0.500f, 0.200f};
        tabla_temp.setWidths(medidaCeldas);

        Font fuente = new Font(Font.HELVETICA, 9);
        Font fuenteB = new Font(Font.HELVETICA, 9, Font.BOLD);
        PdfPCell celda_temp = new PdfPCell();
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase(" ", fuenteB));
        celda_temp.setColspan(3);
        celda_temp.setBorder(0);
        tabla_temp.addCell(celda_temp);


        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("TOTAL PAGADO", fuenteB));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        celda_temp.setBorder(0);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase(Utility.customFormat(vlr), fuenteB));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        celda_temp.setBorder(0);
        tabla_temp.addCell(celda_temp);

        return tabla_temp;
    }




   protected PdfPTable TotalEdu( double vlr  ) throws DocumentException {


        PdfPTable tabla_temp = new PdfPTable(6);
        float[] medidaCeldas = {0.05f, 0.130f,0.130f, 0.160f, 0.450f, 0.125f};
        tabla_temp.setWidths(medidaCeldas);

        Font fuente = new Font(Font.HELVETICA, 9);
        Font fuenteB = new Font(Font.HELVETICA, 9, Font.BOLD);
        PdfPCell celda_temp = new PdfPCell();
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase(" ", fuenteB));
        celda_temp.setColspan(4);
        celda_temp.setBorder(0);
        tabla_temp.addCell(celda_temp);


        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase("TOTAL PAGADO", fuenteB));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        celda_temp.setBorder(0);
        tabla_temp.addCell(celda_temp);
        celda_temp = new PdfPCell();
        celda_temp.setPhrase(new Phrase(Utility.customFormat(vlr), fuenteB));
        celda_temp.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        celda_temp.setBorder(0);
        tabla_temp.addCell(celda_temp);

        return tabla_temp;
    }




 public double Total_pagado( Vector vn  ) throws DocumentException {

      double vlr=0;
       Negocios neg = new Negocios();

        for (int i=0;i<vn.size();i++)
        {
            neg = (Negocios)vn.get(i);
             vlr=vlr+neg.getVr_desem();
        }
       return vlr;
    }





   public boolean envia_correo(String from,String to,String copia,String asunto,String msg,String ruta,String archivo)
    {
     boolean enviado=false;
     try
       {


         /************************* Envio de Correo*************************/
           Email2 emailData = null;
           String ahora = new java.util.Date().toString();
            emailData=new Email2();
            emailData.setEmailId(12345);
            emailData.setRecstatus("A");//A
            emailData.setEmailcode( "emailcode");
            emailData.setEmailfrom(from);
            emailData.setEmailto(to );
            emailData.setEmailcopyto(copia);
            emailData.setEmailHiddencopyto("");
            emailData.setEmailsubject(asunto);//"WebServiceMultiple_Fintra2" );
            emailData.setEmailbody(msg);
            emailData.setLastupdat(new Timestamp(System.currentTimeMillis()) );
            emailData.setSenderName( "sender name" );
            emailData.setRemarks("remark2");
            emailData.setTipo("E");
            emailData.setRutaArchivo(ruta);
            emailData.setNombreArchivo(archivo);


           // EmailSendingEngineService emailSendingEngineService=new EmailSendingEngineService("smtpbar.une.net.co","fintravalores@geotechsa.com","fintra21");
              EmailSendingEngineService emailSendingEngineService=new EmailSendingEngineService();
           enviado= emailSendingEngineService.send(emailData);
            emailSendingEngineService=null;//091206
            /*************************Fin Envio de Correo*************************/
           }
           catch(Exception e)
           {

               System.out.println("Error  "+e.getMessage());
           }

     return enviado;
    }



    public String getMensaje()
    {
        String mensaje="<span style='font-family: ; font-size:14px; line-height:25px '; >"
                + "Estimado Cliente. </br>Adjunto le estamos enviando el soporte de pago de los negocios desembolsados correspondienten a los clientes aqu&iacute; relacionados.<br />"
                + "Favor no responder este mensaje. Este se genera de manera autom&aacute;tica. En caso de requerir comunicarse con nosotros, favor conta&aacute;ctenos en los tel&eacute;fonos (5) 3679901 Ext, 1131 -1112."
                + "</span>";

        return mensaje;
    }


     private String directorioArchivo(String user) throws Exception {
        String ruta = "";
        try {
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            ruta = rb.getString("ruta") + "/exportar/migracion/" + user.toUpperCase();
            SimpleDateFormat fmt = new SimpleDateFormat("yyyMMdd_hhmmss");
            File archivo = new File(ruta);
            if (!archivo.exists()) {
                archivo.mkdirs();
            }

        } catch (Exception e) {
            throw new Exception("Error al generar el directorio: " + e.toString());
        }
        return ruta;

    }

}
