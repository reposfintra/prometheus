/*************************************************************
 * Nombre: ReporteExportarFlotaXLS.java
 * Descripci�n: Hilo para crear el reporte de producci�n
 *              de flota.
 * Autor: Ing. Jose de la rosa
 * Fecha: 26 de noviembre de 2005, 12:04 PM
 * Versi�n: Java 1.0
 * Copyright: Fintravalores S.A. S.A.
 **************************************************************/

package com.tsp.operation.model.threads;

import java.text.*;
import java.util.Date;
import java.text.*;
import java.sql.SQLException;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.util.*;
import java.util.*;
import java.io.*;
import java.util.zip.*;

/**
 *
 * @author  EQUIPO13
 */
public class ReporteExportarFlotaXLS extends Thread{
    private String procesoName;
    private String des;
    String id="";
    String fechaini="";
    String fechafin="";
    String ffin="";
    String Nmes="";
    String diac="";
    int vec_dia[]=new int[31];
    int vec_actual[]=new int[31];
    float vec_peso[]=new float[31];
    public ReporteExportarFlotaXLS () {
    }
    
    public void start (String id, String Nmes,String fechaini, String fechafin,String ffin,String diac){
        this.id = id;
        this.fechaini = fechaini;
        this.fechafin = fechafin;
        this.Nmes = Nmes;
        this.ffin = ffin;
        this.diac = diac;
        this.procesoName = "Prod Flota "+Nmes;
        this.des = "La producci�n de la flota en el mes de :"+Nmes;
        super.start ();
    }
    public synchronized void run (){
        String path = "";
        String pathzip = "";
        ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
        path =  rb.getString("ruta") + "/exportar/migracion/"+id+"/ReporteFlota";
        File file = new File(path);
        try{
            Model model = new Model ();
            model.LogProcesosSvc.InsertProceso (this.procesoName, this.hashCode (), des, this.id);
            String fecha_actual = Util.getFechaActual_String (6);
            
            //Declaraci�n de variables
            int fila = 5;
            int col  = 0;
            int j = 1;
            int m = 0;
            int d = 0;
            int temp_dia=0;
            float temp_peso=0;
            int viajes = 0;
            String tryler="";
            float peso=0;
            int cont_d=0;
            float cont_p=0;
            int n = Integer.parseInt (ffin);
            //comienzo conexion
            //ResourceBundle rb = ResourceBundle.getBundle ("com/tsp/util/connectionpool/db");
            //String path = rb.getString ("ruta") + "/exportar/migracion/"+id;
            String dia = fecha_actual.substring (8,10);
            //File file = new File (path);
            pathzip = rb.getString("ruta") + "/exportar/migracion/"+id;
            file.mkdirs ();
            com.tsp.operation.model.beans.POIWrite xls = new com.tsp.operation.model.beans.POIWrite (path+"/PROD FLOTA "+Nmes+" "+diac+".xls");
            // fuente, tama�o, negrita, cursiva, formato, color, fondo, alineado
            HSSFWorkbook wb = new HSSFWorkbook ();
            HSSFCellStyle fecha  = xls.nuevoEstilo ("verdana", 12, false , false, "yyyy/mm/dd"  , xls.NONE , xls.NONE , xls.NONE );
            HSSFCellStyle texto  = xls.nuevoEstilo ("verdana", 12, false , false, "text"        , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle numero = xls.nuevoEstilo ("verdana", 12, false , false, ""            , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle numero_negrita = xls.nuevoEstilo ("verdana", 12, true , false, "#,##0.00"            , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle numero_negrita_sin = xls.nuevoEstilo ("verdana", 12, true , false, ""            , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle negrita      = xls.nuevoEstilo ("verdana", 12, true  , false, "text"        , HSSFColor.BLACK.index , xls.NONE , xls.NONE);
            HSSFCellStyle header      = xls.nuevoEstilo ("verdana", 18, true  , false, "text"        , HSSFColor.WHITE.index , HSSFColor.ORANGE.index , HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle titulo      = xls.nuevoEstilo ("verdana", 12, true  , false, "text"        , HSSFColor.WHITE.index , HSSFColor.GREEN.index , HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle fechatitle  = xls.nuevoEstilo ("verdana", 12, true  , false, "yyyy/mm/dd"  , HSSFColor.WHITE.index , HSSFColor.DARK_GREEN.index , HSSFCellStyle.ALIGN_CENTER );
            
            
            xls.obtenerHoja ("GENERAL");
            xls.cambiarMagnificacion (3,4);
            // cabecera
            
            xls.adicionarCelda (0,0, "PRODUCCI�N ACUMULADA GENERAL, MES DE: "+Nmes  , header);
            xls.combinarCeldas (0, 0, 0, 7);
            
            // subtitulos
            xls.adicionarCelda (2,0, "Fecha Proceso: "   , negrita);
            xls.adicionarCelda (2,1, fecha_actual , fecha);
            xls.adicionarCelda (3,0, "Elaborado Por: "   , negrita);
            xls.adicionarCelda (3,1, id , fecha);
            
            xls.adicionarCelda (fila ,col++ , "Nit"                                  , titulo );
            xls.adicionarCelda (fila ,col++ , "Propietario"                          , titulo );
            xls.adicionarCelda (fila ,col++ , "Placa"                                , titulo );
            xls.adicionarCelda (fila ,col++ , "Trayler"                              , titulo );
            
            for(int i = 1; i <= n ; i++){
                xls.adicionarCelda (fila ,col++ , i                                  , titulo );
                vec_dia[i-1]=0;
                vec_peso[i-1]=0;
                vec_actual[i-1]=0;
            }
            xls.adicionarCelda (fila ,col++ , "Viajes"                               , titulo );
            xls.adicionarCelda (fila ,col++ , "Toneadas"                             , titulo );
            // datos
            List lista = (List) model.planillaService.reporteProdFlotaPlacasXLS (fechaini,fechafin);
            Iterator it = lista.iterator ();
            while(it.hasNext ()){
                viajes = 0;
                for(int i = 1; i <= n ; i++){
                    vec_actual[i-1]=0;
                }
                peso=0;
                fila++;
                col = 0;
                Planilla planilla = (Planilla) it.next ();
                xls.adicionarCelda (fila ,col++ , planilla.getNitpro ()               , texto );
                xls.adicionarCelda (fila ,col++ , planilla.getNomCond ()              , texto );
                xls.adicionarCelda (fila ,col++ , planilla.getPlaveh ()               , texto );
                xls.adicionarCelda (fila ,col++ , planilla.getPlatlr ()               , texto );
                List    lis = (List)    model.planillaService.reporteProdFlotaPlacasDiasXLS (fechaini,fechafin,planilla.getPlaveh (),planilla.getNitpro ());
                Iterator ite = lis.iterator ();
                while(ite.hasNext ()){
                    Planilla plan = (Planilla) ite.next ();
                    m = Integer.parseInt (plan.getFeccum ());
                    vec_actual[m-1] = plan.getDias ();
                    vec_dia[m-1] += plan.getDias ();
                    vec_peso[m-1]+=plan.getPesoreal ();
                    viajes += plan.getDias ();
                    peso += plan.getPesoreal ();
                }
                for(j = 1; j <= n ; j++){
                    xls.adicionarCelda (fila ,col++ , vec_actual[j-1]               , numero );
                }
                xls.adicionarCelda (fila ,col++ , viajes                            , numero_negrita_sin );
                xls.adicionarCelda (fila ,col++ ,peso                               , numero_negrita );
            }
            col=4;
            fila+=2;
            cont_d=0;
            cont_p=0;
            xls.adicionarCelda (fila ,0 ,"Total Dias"                                           , negrita );
            for(int s = 0; s<n; s++){
                xls.adicionarCelda (fila ,col++ ,vec_dia[s]                                     , numero_negrita_sin );
                cont_d+=vec_dia[s];
            }
            xls.adicionarCelda (fila ,col++ ,cont_d                                             , numero_negrita_sin );
            col=4;
            fila++;
            xls.adicionarCelda (fila ,0 ,"Total Toneladas"                                       , negrita );
            for(int s = 0; s<n; s++){
                xls.adicionarCelda (fila ,col++ , vec_peso[s]                                    , numero_negrita );
                cont_p+=vec_peso[s];
            }
            col++;
            xls.adicionarCelda (fila ,col++ ,  cont_p                                            , numero_negrita );
            
            //segunda hoja
            List lista_lufkin = (List) model.planillaService.reporteLufkinXLS (fechaini,fechafin);
            xls.obtenerHoja ("LUFKIN");
            xls.cambiarMagnificacion (3,4);
            
            // cabecera
            xls.adicionarCelda (0,0, "PRODUCCI�N ACUMULADA LUFKIN, MES DE: "+Nmes  , header);
            xls.combinarCeldas (0, 0, 0, 7);
            
            // subtitulos
            xls.adicionarCelda (2,0, "Fecha Proceso: "   , negrita);
            xls.adicionarCelda (2,1, fecha_actual , fecha);
            xls.adicionarCelda (3,0, "Elaborado Por: "   , negrita);
            xls.adicionarCelda (3,1, id , fecha);
            fila = 5;
            col  = 0;
            xls.adicionarCelda (fila ,col++ , "Nit"                                  , titulo );
            xls.adicionarCelda (fila ,col++ , "Propietario"                          , titulo );
            xls.adicionarCelda (fila ,col++ , "Placa"                                , titulo );
            xls.adicionarCelda (fila ,col++ , "Trayler"                              , titulo );
            n = Integer.parseInt (ffin);
            for(int i = 1; i <= n ; i++){
                xls.adicionarCelda (fila ,col++ , i                                  , titulo );
                vec_dia[i-1]=0;
                vec_peso[i-1]=0;
                vec_actual[i-1]=0;
            }
            xls.adicionarCelda (fila ,col++ , "Viajes"                               , titulo );
            xls.adicionarCelda (fila ,col++ , "Toneadas"                             , titulo );
            // datos
            m = 0;
            viajes = 0;
            peso=0;
            Iterator it_lufkin = lista_lufkin.iterator ();
            while(it_lufkin.hasNext ()){
                viajes = 0;
                for(int i = 1; i <= n ; i++){
                    vec_actual[i-1]=0;
                }
                peso=0;
                fila++;
                col = 0;
                Planilla planilla_lufkin = (Planilla) it_lufkin.next ();
                xls.adicionarCelda (fila ,col++ , planilla_lufkin.getNitpro ()               , texto );
                xls.adicionarCelda (fila ,col++ , planilla_lufkin.getNomCond ()              , texto );
                xls.adicionarCelda (fila ,col++ , planilla_lufkin.getPlaveh ()               , texto );
                xls.adicionarCelda (fila ,col++ , planilla_lufkin.getPlatlr ()               , texto );
                List    lis_lufkin = (List)    model.planillaService.reporteProdFlotaPlacasDiasXLS (fechaini,fechafin,planilla_lufkin.getPlaveh (),planilla_lufkin.getNitpro ());                
                Iterator ite_lufkin = lis_lufkin.iterator ();
                while(ite_lufkin.hasNext ()){
                    Planilla plan_lufkin = (Planilla) ite_lufkin.next ();
                    m = Integer.parseInt (plan_lufkin.getFeccum ());
                    vec_actual[m-1] = plan_lufkin.getDias ();
                    vec_dia[m-1] += plan_lufkin.getDias ();
                    vec_peso[m-1]+=plan_lufkin.getPesoreal ();
                    viajes += plan_lufkin.getDias ();
                    peso += plan_lufkin.getPesoreal ();
                }
                
                
                for(j = 1; j <= n ; j++){
                    xls.adicionarCelda (fila ,col++ , vec_actual[j-1]               , numero );
                }
                xls.adicionarCelda (fila ,col++ , viajes                            , numero_negrita_sin );
                xls.adicionarCelda (fila ,col++ ,peso                               , numero_negrita );
                
            }
            col=4;
            fila+=2;
            cont_d=0;
            cont_p=0;
            xls.adicionarCelda (fila ,0 ,"Total Dias"                                           , negrita );
            for(int s = 0; s<n; s++){
                xls.adicionarCelda (fila ,col++ ,vec_dia[s]                                     , numero_negrita_sin );
                cont_d+=vec_dia[s];
            }
            xls.adicionarCelda (fila ,col++ ,cont_d                                             , numero_negrita_sin );
            col=4;
            fila++;
            xls.adicionarCelda (fila ,0 ,"Total Toneladas"                                       , negrita );
            for(int s = 0; s<n; s++){
                xls.adicionarCelda (fila ,col++ , vec_peso[s]                                    , numero_negrita );
                cont_p+=vec_peso[s];
            }
            col++;
            xls.adicionarCelda (fila ,col++ ,  cont_p                                            , numero_negrita );
            
            
            //tercera hoja
            List lista_standar = (List)    model.planillaService.reporteProdStandarJobXLS ();
            Iterator it_standar = lista_standar.iterator ();
            while(it_standar.hasNext ()){
                Planilla planilla_standar = (Planilla) it_standar.next ();
                List lista_reporte_st = (List) model.planillaService.reporteProdFlotaPlacasStandarXLS (fechaini,fechafin,planilla_standar.getSj ());
                if(lista_reporte_st.size ()>0){
                    xls.obtenerHoja (planilla_standar.getStatus_220 ().substring (8,22));
                    xls.cambiarMagnificacion (3,4);
                    // cabecera
                    
                    xls.adicionarCelda (0,0, "PRODUCCI�N ACUMULADA "+planilla_standar.getStatus_220 ()+", MES DE: "+Nmes  , header);
                    xls.combinarCeldas (0, 0, 0, 7);
                    
                    // subtitulos
                    xls.adicionarCelda (2,0, "Fecha Proceso: "   , negrita);
                    xls.adicionarCelda (2,1, fecha_actual , fecha);
                    xls.adicionarCelda (3,0, "Elaborado Por: "   , negrita);
                    xls.adicionarCelda (3,1, id , fecha);
                    
                    fila = 5;
                    col  = 0;
                    xls.adicionarCelda (fila ,col++ , "Nit"                                  , titulo );
                    xls.adicionarCelda (fila ,col++ , "Propietario"                          , titulo );
                    xls.adicionarCelda (fila ,col++ , "Placa"                                , titulo );
                    xls.adicionarCelda (fila ,col++ , "Trayler"                              , titulo );
                    n = Integer.parseInt (ffin);
                    for(int i = 1; i <= n ; i++){
                        xls.adicionarCelda (fila ,col++ , i                                  , titulo );
                        vec_dia[i-1]=0;
                        vec_peso[i-1]=0;
                        vec_actual[i-1]=0;
                    }
                    xls.adicionarCelda (fila ,col++ , "Viajes"                               , titulo );
                    xls.adicionarCelda (fila ,col++ , "Toneadas"                             , titulo );
                    // datos
                    viajes = 0;
                    tryler="";
                    peso=0;
                    
                    Iterator it_lst = lista_reporte_st.iterator ();
                    while(it_lst.hasNext ()){
                        Planilla planilla_reporte_st = (Planilla) it_lst.next ();
                        viajes = 0;
                        for(int i = 1; i <= n ; i++){
                            vec_actual[i-1]=0;
                        }
                        peso=0;
                        fila++;
                        col = 0;
                        xls.adicionarCelda (fila ,col++ , planilla_reporte_st.getNitpro ()               , texto );
                        xls.adicionarCelda (fila ,col++ , planilla_reporte_st.getNomCond ()              , texto );
                        xls.adicionarCelda (fila ,col++ , planilla_reporte_st.getPlaveh ()               , texto );
                        xls.adicionarCelda (fila ,col++ , planilla_reporte_st.getPlatlr ()               , texto );
                        List lista_dias = (List) model.planillaService.reporteProdFlotaPlacasDiasSTDXLS (fechaini,fechafin,planilla_reporte_st.getPlaveh (),planilla_reporte_st.getNitpro (),planilla_standar.getSj ());
                        Iterator ite_dias = lista_dias.iterator ();
                        while(ite_dias.hasNext ()){
                            Planilla planilla_dias = (Planilla) ite_dias.next ();
                            m = Integer.parseInt (planilla_dias.getFeccum ());
                            vec_actual[m-1] = planilla_dias.getDias ();
                            vec_dia[m-1] += planilla_dias.getDias ();
                            vec_peso[m-1]+=planilla_dias.getPesoreal ();
                            viajes += planilla_dias.getDias ();
                            peso += planilla_dias.getPesoreal ();
                        }
                        
                        
                        for(j = 1; j <= n ; j++){
                            xls.adicionarCelda (fila ,col++ , vec_actual[j-1]               , numero );
                        }
                        xls.adicionarCelda (fila ,col++ , viajes                            , numero_negrita_sin );
                        xls.adicionarCelda (fila ,col++ ,peso                               , numero_negrita );
                        
                    }
                    col=4;
                    fila+=2;
                    cont_d=0;
                    cont_p=0;
                    xls.adicionarCelda (fila ,0 ,"Total Dias"                                           , negrita );
                    for(int s = 0; s<n; s++){
                        xls.adicionarCelda (fila ,col++ ,vec_dia[s]                                     , numero_negrita_sin );
                        cont_d+=vec_dia[s];
                    }
                    xls.adicionarCelda (fila ,col++ ,cont_d                                             , numero_negrita_sin );
                    col=4;
                    fila++;
                    xls.adicionarCelda (fila ,0 ,"Total Toneladas"                                       , negrita );
                    for(int s = 0; s<n; s++){
                        xls.adicionarCelda (fila ,col++ , vec_peso[s]                                    , numero_negrita );
                        cont_p+=vec_peso[s];
                    }
                    col++;
                    xls.adicionarCelda (fila ,col++ ,  cont_p                                            , numero_negrita );
                }
            }
            
            
            //cuarta hoja
            xls.obtenerHoja ("FLOTA DIRECTA");
            xls.cambiarMagnificacion (3,4);
            // cabecera
            
            xls.adicionarCelda (0,0, "PRODUCCI�N ACUMULADA FLOTA DIRECTA, MES DE: "+Nmes  , header);
            xls.combinarCeldas (0, 0, 0, 7);
            
            // subtitulos
            xls.adicionarCelda (2,0, "Fecha Proceso: "   , negrita);
            xls.adicionarCelda (2,1, fecha_actual , fecha);
            xls.adicionarCelda (3,0, "Elaborado Por: "   , negrita);
            xls.adicionarCelda (3,1, id , fecha);
            
            fila = 5;
            col  = 0;
            xls.adicionarCelda (fila ,col++ , "Nit"                                  , titulo );
            xls.adicionarCelda (fila ,col++ , "Propietario"                          , titulo );
            xls.adicionarCelda (fila ,col++ , "Placa"                                , titulo );
            xls.adicionarCelda (fila ,col++ , "Trayler"                              , titulo );
            n = Integer.parseInt (ffin);
            for(int i = 1; i <= n ; i++){
                xls.adicionarCelda (fila ,col++ , i                                  , titulo );
                vec_dia[i-1]=0;
                vec_peso[i-1]=0;
                vec_actual[i-1]=0;
            }
            xls.adicionarCelda (fila ,col++ , "Viajes"                               , titulo );
            xls.adicionarCelda (fila ,col++ , "Toneadas"                             , titulo );
            // datos
            viajes = 0;
            tryler="";
            peso=0;
            List lista_reporte_directa = (List) model.planillaService.reporteFlotaDirectaXLS (fechaini,fechafin);
            Iterator it_lst_directa = lista_reporte_directa.iterator ();
            while(it_lst_directa.hasNext ()){
                Planilla planilla_reporte_directa = (Planilla) it_lst_directa.next ();
                viajes = 0;
                for(int i = 1; i <= n ; i++){
                    vec_actual[i-1]=0;
                }
                peso=0;
                fila++;
                col = 0;
                temp_dia=0;
                temp_peso=0;
                xls.adicionarCelda (fila ,col++ , planilla_reporte_directa.getNitpro ()               , texto );
                xls.adicionarCelda (fila ,col++ , planilla_reporte_directa.getNomCond ()              , texto );
                xls.adicionarCelda (fila ,col++ , planilla_reporte_directa.getPlaveh ()               , texto );
                xls.adicionarCelda (fila ,col++ , planilla_reporte_directa.getPlatlr ()               , texto );
                List lista_dias_directa = (List) model.planillaService.reporteProdFlotaPlacasDiasXLS (fechaini,fechafin,planilla_reporte_directa.getPlaveh (),planilla_reporte_directa.getNitpro ());
                Iterator ite_dias_directa = lista_dias_directa.iterator ();
                while(ite_dias_directa.hasNext ()){
                    Planilla planilla_dias_directa = (Planilla) ite_dias_directa.next ();
                    m = Integer.parseInt (planilla_dias_directa.getFeccum ());
                    vec_actual[m-1] = planilla_dias_directa.getDias ();
                    vec_dia[m-1] += planilla_dias_directa.getDias ();
                    vec_peso[m-1]+=planilla_dias_directa.getPesoreal ();
                    viajes += planilla_dias_directa.getDias ();
                    peso += planilla_dias_directa.getPesoreal ();
                }
                
                
                for(j = 1; j <= n ; j++){
                    xls.adicionarCelda (fila ,col++ , vec_actual[j-1]               , numero );
                }
                xls.adicionarCelda (fila ,col++ , viajes                            , numero_negrita_sin );
                xls.adicionarCelda (fila ,col++ ,peso                               , numero_negrita );
            }
            col=4;
            fila+=2;
            cont_d=0;
            cont_p=0;
            xls.adicionarCelda (fila ,0 ,"Total Dias"                                           , negrita );
            for(int s = 0; s<n; s++){
                xls.adicionarCelda (fila ,col++ ,vec_dia[s]                                     , numero_negrita_sin );
                cont_d+=vec_dia[s];
            }
            xls.adicionarCelda (fila ,col++ ,cont_d                                             , numero_negrita_sin );
            col=4;
            fila++;
            xls.adicionarCelda (fila ,0 ,"Total Toneladas"                                       , negrita );
            for(int s = 0; s<n; s++){
                xls.adicionarCelda (fila ,col++ , vec_peso[s]                                    , numero_negrita );
                cont_p+=vec_peso[s];
            }
            col++;
            xls.adicionarCelda (fila ,col++ ,  cont_p                                            , numero_negrita );
            
            //quinta hoja
            xls.obtenerHoja ("FLOTA INTERMEDIA");
            xls.cambiarMagnificacion (3,4);
            // cabecera
            
            xls.adicionarCelda (0,0, "PRODUCCI�N ACUMULADA FLOTA INTERMEDIA, MES DE: "+Nmes  , header);
            xls.combinarCeldas (0, 0, 0, 7);
            
            // subtitulos
            xls.adicionarCelda (2,0, "Fecha Proceso: "   , negrita);
            xls.adicionarCelda (2,1, fecha_actual , fecha);
            xls.adicionarCelda (3,0, "Elaborado Por: "   , negrita);
            xls.adicionarCelda (3,1, id , fecha);
            
            fila = 5;
            col  = 0;
            xls.adicionarCelda (fila ,col++ , "Nit"                                  , titulo );
            xls.adicionarCelda (fila ,col++ , "Propietario"                          , titulo );
            xls.adicionarCelda (fila ,col++ , "Placa"                                , titulo );
            xls.adicionarCelda (fila ,col++ , "Trayler"                              , titulo );
            n = Integer.parseInt (ffin);
            for(int i = 1; i <= n ; i++){
                xls.adicionarCelda (fila ,col++ , i                                  , titulo );
                vec_dia[i-1]=0;
                vec_peso[i-1]=0;
            }
            xls.adicionarCelda (fila ,col++ , "Viajes"                               , titulo );
            xls.adicionarCelda (fila ,col++ , "Toneadas"                             , titulo );
            // datos
            viajes = 0;
            tryler="";
            peso=0;
            List lista_reporte_intermedia = (List) model.planillaService.reporteFlotaIntermediaXLS (fechaini,fechafin);
            Iterator it_lst_intermedia = lista_reporte_intermedia.iterator ();
            while(it_lst_intermedia.hasNext ()){
                Planilla planilla_reporte_intermedia = (Planilla) it_lst_intermedia.next ();
                viajes = 0;
                peso=0;
                fila++;
                col = 0;
                temp_dia=0;
                temp_peso=0;
                List lista_dias_intermedia = (List) model.planillaService.reporteProdFlotaPlacasDiasXLS (fechaini,fechafin,planilla_reporte_intermedia.getPlaveh (),planilla_reporte_intermedia.getNitpro ());
                xls.adicionarCelda (fila ,col++ , planilla_reporte_intermedia.getNitpro ()               , texto );
                xls.adicionarCelda (fila ,col++ , planilla_reporte_intermedia.getNomCond ()              , texto );
                xls.adicionarCelda (fila ,col++ , planilla_reporte_intermedia.getPlaveh ()               , texto );
                xls.adicionarCelda (fila ,col++ , planilla_reporte_intermedia.getPlatlr ()               , texto );
                
                Iterator ite_dias_intermedia = lista_dias_intermedia.iterator ();
                while(ite_dias_intermedia.hasNext ()){
                    Planilla planilla_dias_intermedia = (Planilla) ite_dias_intermedia.next ();
                    m = Integer.parseInt (planilla_dias_intermedia.getFeccum ());
                    m = Integer.parseInt (planilla_dias_intermedia.getFeccum ());
                    vec_actual[m-1] = planilla_dias_intermedia.getDias ();
                    vec_dia[m-1] += planilla_dias_intermedia.getDias ();
                    vec_peso[m-1]+=planilla_dias_intermedia.getPesoreal ();
                    viajes += planilla_dias_intermedia.getDias ();
                    peso += planilla_dias_intermedia.getPesoreal ();
                }
                for(j = 1; j <= n ; j++){
                    xls.adicionarCelda (fila ,col++ , vec_actual[j-1]               , numero );
                }
                xls.adicionarCelda (fila ,col++ , viajes                            , numero_negrita_sin );
                xls.adicionarCelda (fila ,col++ ,peso                               , numero_negrita );
                
            }
            col=4;
            fila+=2;
            cont_d=0;
            cont_p=0;
            xls.adicionarCelda (fila ,0 ,"Total Dias"                                           , negrita );
            for(int s = 0; s<n; s++){
                xls.adicionarCelda (fila ,col++ ,vec_dia[s]                                     , numero_negrita_sin );
                cont_d+=vec_dia[s];
            }
            xls.adicionarCelda (fila ,col++ ,cont_d                                             , numero_negrita_sin );
            col=4;
            fila++;
            xls.adicionarCelda (fila ,0 ,"Total Toneladas"                                       , negrita );
            for(int s = 0; s<n; s++){
                xls.adicionarCelda (fila ,col++ , vec_peso[s]                                    , numero_negrita );
                cont_p+=vec_peso[s];
            }
            col++;
            xls.adicionarCelda (fila ,col++ ,  cont_p                                            , numero_negrita );
            
            xls.cerrarLibro ();
            model.LogProcesosSvc.finallyProceso (this.procesoName, this.hashCode (), this.id, "PROCESO EXITOSO");
            compressZipFile(path+"/", pathzip+"/ReporteFlota.zip", rb.getString("ruta") + "/exportar/migracion/"+id+"/");
        }
        catch(Exception e){
            try{
                Model model = new Model ();
                model.LogProcesosSvc.finallyProceso (this.procesoName, this.hashCode (),this.id,"ERROR :" + e.getMessage ());
            }
            catch(Exception f){
                try{
                    Model model = new Model ();
                    model.LogProcesosSvc.finallyProceso (this.procesoName,this.hashCode (),this.id,"ERROR :");
                }catch(Exception p){    }
            }
        }
    }
    
    
    // metodo para comprimir un archivo.
    public static final void compressZipFile(String carpeName, String destino, String ruta) throws IOException {
        try {
            //System.out.println("Se va a comprimir todo lo de la carpeta "+carpeName);
            //System.out.println("Quedara en la ruta "+destino);
            int BUFFER_SIZE=10485760;
            // Reference to the file we will be adding to the zipfile
            BufferedInputStream origin = null;
            File file = new File(ruta);
            file.mkdirs();
            // Reference to our zip file
            FileOutputStream dest = new FileOutputStream( destino );
            
            // Wrap our destination zipfile with a ZipOutputStream
            ZipOutputStream out = new ZipOutputStream(new BufferedOutputStream( dest ) );
            
            // Create a byte[] buffer that we will read data
            // from the source
            // files into and then transfer it to the zip file
            byte[] data = new byte[ BUFFER_SIZE ];
            File carpeta = new File (carpeName);
            File[] files = carpeta.listFiles();
            
            for( int i =0; i<files.length;i++ ) {
                // Get a BufferedInputStream that we can use to read the
                // source file
                String filename = ( String ) files[i].getName();
                //System.out.println( "Adding: " + carpeta+File.separator+filename );
                
                FileInputStream fi = new FileInputStream( carpeta+File.separator+filename );
                origin = new BufferedInputStream( fi, BUFFER_SIZE );
                
                // Setup the entry in the zip file
                ZipEntry entry = new ZipEntry( filename );
                out.putNextEntry( entry );
                
                // Read data from the source file and write it out to the zip file
                int count;
                while( ( count = origin.read(data, 0, BUFFER_SIZE ) ) != -1 ) {
                    out.write(data, 0, count);
                }
                
                // Close the source file
                origin.close();
            }
            // Close the zip file
            out.close();
        }
        catch( Exception e ) {
            e.printStackTrace();
        }
    }
}
