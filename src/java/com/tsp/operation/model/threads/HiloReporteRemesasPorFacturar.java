/*
 * HiloReporteRemesasPorFacturar2.java
 *
 * Created on 10 de marzo de 2007, 10:32 AM
 */

package com.tsp.operation.model.threads;

/**
 *
 * @author  Osvaldo P�rez Ferrer
 */

import java.util.*;
import java.io.*;
import java.lang.*;
import java.text.*;

import com.tsp.operation.model.*;
import com.tsp.operation.model.beans.POIWrite;

import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.DAOS.FormatoDAO;
import com.tsp.util.*;

public class HiloReporteRemesasPorFacturar extends Thread{
    
    String   ruta;
    String  hoja;
    POIWrite xls;
    HSSFCellStyle header  , titulo1, titulo2, titulo3 , titulo4, letra, numero, porcentaje, letraCentrada, numeroNegrita, letraDerecha;
    //HSSFColor     cAzul   , cVerde, cAmarillo, cGris ;
    int           fila = 0;
    SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
    
    String[] tipos;
    String[] titulos;
    
    private String dist;
    private String fini;
    private String ffin;
    private String aged;
    private String agef;
    private String anio;
    private String anual;
    
    private ReporteRemesasPorFacturar r;
    
    private String path;
    private String usuario;
    private String procesoName;
    private String des;
    
    private Model model;
    
    /** Creates a new instance of HiloReporteRemesasPorFacturar2 */
    public HiloReporteRemesasPorFacturar( String usu, String distrito, String fechaini, String fechafin, String agencia_duenia, String agencia_facturadora, String anual, String anio, Model model ) {
        
        this.procesoName = "Reporte De Remesas Por Facturar";
        this.des = "Reporte De Remesas Por Facturar";
        this.usuario = usu;
        
        this.dist = distrito;
        this.fini = fechaini;
        this.ffin = fechafin;
        this.aged = agencia_duenia;
        this.agef = agencia_facturadora;
        this.anio = anio;
        this.anual = anual;
        this.model = model;
        
        super.start();
    }
    
    
    public synchronized void run(){
        
        try{
                                    
            //Inicia el proceso
            model.LogProcesosSvc.InsertProceso( procesoName, this.hashCode(), "Iniciado: " + des, usuario );
            
            
            model.remesasPorFacturarService.RemesasPorFacturar( dist, fini, ffin, aged, agef, anual, anio );
            Vector datos = model.remesasPorFacturarService.getVectorRemesa();
            
            
            Util u = new Util();
            
            ResourceBundle rb = ResourceBundle.getBundle( "com/tsp/util/connectionpool/db" );
            path = rb.getString( "ruta" );
            
            File file = new File( path + "/exportar/migracion/" + usuario );
            file.mkdirs();
            
            String fecha = Util.getFechaActual_String(6);
            fecha=fecha.replaceAll( "/", "-" );
            fecha=fecha.replaceAll( ":", "_" );
            
            String nombreArch= "ReporteDeRemesasPorFacturar[" + fecha + "]";
            hoja  = "ReporteDeRemesasPorFacturar";
            ruta  = path + "/exportar/migracion/" + usuario + "/";
            
            String comentario = "PROCESO EXITOSO";
            if( datos.size() > 0 ){
                this.crearArchivo( nombreArch,"Remesas por Facturar");
                this.escribirEnArchivo(datos);
                this.cerrarArchivo();
            }else{
                comentario = "No se encontraron remesas pendientes por facturar";
            }
            
            //System.out.println("REPORTE GENERADO");
            
            model.LogProcesosSvc.finallyProceso( procesoName, this.hashCode(), usuario, comentario );
            System.gc();
        }catch ( java.lang.OutOfMemoryError me ){
            try{
                model.LogProcesosSvc.finallyProceso( procesoName,this.hashCode(), usuario, "ERROR : " + me.getMessage() );
                System.gc();
            } catch( Exception p ){ p.printStackTrace(); }
            me.printStackTrace();
        }
        catch( Exception e ){
                        
            e.printStackTrace();
            try{
                System.gc();
                model.LogProcesosSvc.finallyProceso( procesoName, this.hashCode(), usuario, "ERROR : " + e.getMessage() );
                
            } catch( Exception f ){
                
                f.printStackTrace();
                
                try{
                    
                    model.LogProcesosSvc.finallyProceso( procesoName,this.hashCode(), usuario, "ERROR : " + f.getMessage() );
                    
                } catch( Exception p ){ p.printStackTrace(); }
                
            }
            
        }
        
    }
    
    private void crearArchivo(String nameFile, String titulo) throws Exception{
        try{
            
            InitArchivo(nameFile);
            xls.obtenerHoja( hoja );
            xls.combinarCeldas(0, 0, 0, 46);
            xls.combinarCeldas(1, 0, 1, 46);
            xls.combinarCeldas(2, 0, 2, 46);
            xls.adicionarCelda(0, 0, "Transportes Sanchez polo S.A.", header);
            xls.adicionarCelda(1, 0, titulo, header);
            
            xls.adicionarCelda(2, 0, "Fecha " + fmt.format( new Date() ) , titulo1);
            
            
            int col = 0;
            
            short [] dimensiones = new short[46];
            
            
            for( int i=0; i<46; i++ ){
                dimensiones[i] = (short)( 4 * 1000 );
            }
            
            fila=4;
            
            xls.cambiarAnchoColumna(0, 3000);
            
            String[] titulos = {"IND","OT","VALOR OT","ESTADO OT","AGENCIA ORIGEN","FECHA DESPACHO",//6
            "AGENCIA DESTINO","FECHA ENTREGA","ENTREGADA","TIPO ENTREGA","AGENCIA CUMPLIDO","FECHA CUMPLIDO",//12
            "CUMPLIDA","OC","CLIENTE","CODIGO CLIENTE", "FIDUCIARIA", "RUTA",//18 AMATURANA 31.03.2007
            "AGENCIA DUE�A","DOCUMENTO INTERNO","STANDARD JOB","DESCRIPCION OT","AUXILIAR DE FACTURACION",//22
            "FECHA ENVIO LOGICO","FECHA ENVIO FISICO","FECHA RECIBO LOGICO","FECHA RECIBO FISICO","AGENCIA FACTURACION",//27
            "FECHA CIERRE OT","DIAS","A�O CUMPLIDO","MES CUMPLIDO","DIA CUMPLIDO",//32
            "A�O REPORTE","MES REPORTE","DIA REPORTE","VALOR OC","CANTIDAD OC",//37
            "PLACA","WO TYPE","A�O DESPACHO","MES DESPACHO","CUMPLIDOR",//42
            "ES VACIO","OT PADRE","OT�S ASOCIADAS"};//46
            
            for( int i = 0; i<titulos.length; i++ ){
                
                xls.adicionarCelda(fila,  i, titulos[i], titulo2);
                xls.cambiarAnchoColumna(i, dimensiones [i]);
                
            }
            
            fila++;
            
            
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception( "Error en crearArchivo ....\n"  + ex.getMessage() );
        }
    }
    
    public void InitArchivo(String nameFile) throws Exception{
        try{
            
            xls          = new com.tsp.operation.model.beans.POIWrite();
            xls.nuevoLibro( ruta + "/" + nameFile + ".xls" );
            
            // colores
            //cAzul       = xls.obtenerColor(204,255,255);
            //cVerde      = xls.obtenerColor( 51,153,102);
            //cAmarillo   = xls.obtenerColor(255,255,153);
            //cGris       = xls.obtenerColor(192,192,192);
            
            // estilos
            header       = xls.nuevoEstilo("Tahoma", 14, true  , false, "text"  , HSSFColor.DARK_RED.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
            titulo1      = xls.nuevoEstilo("Tahoma", 10 , true  , false, "text"  , xls.NONE  , xls.NONE , xls.NONE);
            titulo2      = xls.nuevoEstilo("Tahoma", 10 , true  , false, "text"  , HSSFColor.BLACK.index, HSSFColor.GREY_25_PERCENT.index , HSSFCellStyle.ALIGN_CENTER, 1);
            titulo3      = xls.nuevoEstilo("Tahoma", 10 , true  , false, "text"  , HSSFColor.BLACK.index, HSSFColor.YELLOW.index , HSSFCellStyle.ALIGN_CENTER, 1);
            titulo4      = xls.nuevoEstilo("Tahoma", 10 , true  , false, "text"  , HSSFColor.WHITE.index, HSSFColor.GREEN.index , HSSFCellStyle.ALIGN_CENTER, 2);
            letra        = xls.nuevoEstilo("Tahoma", 10 , false , false, ""      , xls.NONE , xls.NONE , xls.NONE);
            letraCentrada= xls.nuevoEstilo("Tahoma", 10 , false , false, ""      , xls.NONE , HSSFColor.WHITE.index , HSSFCellStyle.ALIGN_CENTER,1);
            letraDerecha = xls.nuevoEstilo("Tahoma", 10 , false , false, ""      , xls.NONE , HSSFColor.LIGHT_BLUE.index , HSSFCellStyle.ALIGN_RIGHT,1);
            numero       = xls.nuevoEstilo("Tahoma", 10 , false , false, "#,##0.00", xls.NONE , HSSFColor.WHITE.index , HSSFCellStyle.ALIGN_RIGHT,1);
            numeroNegrita= xls.nuevoEstilo("Tahoma", 10 , true  , false, "_($* #,##0.00_);(@_)"      , xls.NONE , xls.NONE , xls.NONE);
            porcentaje   = xls.nuevoEstilo("Tahoma", 10 , false , false, "0.00%" , xls.NONE , xls.NONE , xls.NONE);
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception("Error en InitArchivo .... \n" + ex.getMessage());
        }
    }
    
    private void escribirEnArchivo(Vector datos) throws Exception{
        
        try{
            
            String[] mon;
            for(int i = 0; i<datos.size(); i++){
                
                int col = 0 ;
                
                r = (ReporteRemesasPorFacturar)datos.get(i);
                
                
                
                xls.adicionarCelda(fila, col++, i+1, letraCentrada );
                xls.adicionarCelda(fila, col++, r.getOt(), letraCentrada );
                xls.adicionarCelda(fila, col++, Double.parseDouble( r.getValor_ot() ), numero );
                xls.adicionarCelda(fila, col++, r.getEstado_ot() , letraCentrada );
                xls.adicionarCelda(fila, col++, r.getAge_ori(), letraCentrada );
                xls.adicionarCelda(fila, col++, r.getFec_desp(), letraCentrada );
                
                xls.adicionarCelda(fila, col++, r.getAge_dest(), letraCentrada );
                xls.adicionarCelda(fila, col++, r.getFec_trafico(), letraCentrada );
                xls.adicionarCelda(fila, col++, r.getEntregada(), letraCentrada );
                xls.adicionarCelda(fila, col++, r.getTipo_entrega(), letraCentrada );
                xls.adicionarCelda(fila, col++, r.getAge_cump(), letraCentrada );
                xls.adicionarCelda(fila, col++, r.getFec_cump(), letraCentrada );
                xls.adicionarCelda(fila, col++, r.getCumplida(), letraCentrada );
                xls.adicionarCelda(fila, col++, r.getOc(), letraCentrada );
                xls.adicionarCelda(fila, col++, r.getCliente(), letraCentrada );
                xls.adicionarCelda(fila, col++, r.getCod_cliente(), letraCentrada );
                xls.adicionarCelda(fila, col++, r.getFiduciaria(), letraCentrada );//AMATURANA 31.03.2007
                xls.adicionarCelda(fila, col++, r.getRuta(), letraCentrada );
                
                
                xls.adicionarCelda(fila, col++, r.getAge_duenia(), letraCentrada );
                xls.adicionarCelda(fila, col++, r.getDoc_int(), letraCentrada );
                xls.adicionarCelda(fila, col++, r.getStd_job(), letraCentrada );
                
                xls.adicionarCelda(fila, col++, r.getDescripcion_ot(), letraCentrada );
                xls.adicionarCelda(fila, col++, r.getAux_integral(), letraCentrada );
                
                xls.adicionarCelda(fila, col++, r.getFec_envio_log(), letraCentrada );
                xls.adicionarCelda(fila, col++, r.getFec_envio_fis(), letraCentrada );
                xls.adicionarCelda(fila, col++, r.getFec_recibo_log(), letraCentrada );
                xls.adicionarCelda(fila, col++, r.getFec_recibo_fis(), letraCentrada );
                xls.adicionarCelda(fila, col++, r.getAge_facturacion(), letraCentrada );
                xls.adicionarCelda(fila, col++, "", letraCentrada );//      OJO fecha de cierre
                xls.adicionarCelda(fila, col++, r.getDias(), letraCentrada );
                xls.adicionarCelda(fila, col++, r.getAnio_cump(), letraCentrada );
                xls.adicionarCelda(fila, col++, r.getMes_cump(), letraCentrada );
                xls.adicionarCelda(fila, col++, r.getDia_cump(), letraCentrada );
                xls.adicionarCelda(fila, col++, r.getAnio_reporte(), letraCentrada );
                xls.adicionarCelda(fila, col++, r.getMes_reporte(), letraCentrada );
                xls.adicionarCelda(fila, col++, r.getDia_reporte(), letraCentrada );
                xls.adicionarCelda(fila, col++, Double.parseDouble( r.getValor_oc() ), numero );
                xls.adicionarCelda(fila, col++, Double.parseDouble( r.getCant_oc() ), numero );
                xls.adicionarCelda(fila, col++, r.getPlaca(), letraCentrada );                                              
                xls.adicionarCelda(fila, col++, r.getWo_type(), letraCentrada );
                xls.adicionarCelda(fila, col++, r.getAnio_desp(), letraCentrada );
                xls.adicionarCelda(fila, col++, r.getMes_desp(), letraCentrada );
                xls.adicionarCelda(fila, col++, r.getCumplidor(), letraCentrada );
                xls.adicionarCelda(fila, col++, r.getEsVacio(), letraCentrada );
                xls.adicionarCelda(fila, col++, r.getOt_padre(), letraCentrada );
                xls.adicionarCelda(fila, col++, r.getAsociadas_ot_padre(), letraCentrada );
                
                fila++;
            }
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
        
    }
    
    private void cerrarArchivo() throws Exception {
        try{
            if (xls!=null)
                xls.cerrarLibro();
        }catch (Exception ex){
            throw new Exception( "Error en crearArchivo ....\n"  + ex.getMessage() );
        }
    }
 
    public static void main( String[]jkd )throws Exception{
        Model model = new Model();
        HiloReporteRemesasPorFacturar r = new HiloReporteRemesasPorFacturar("OPEREZ","FINV","2007-01-01","2007-01-07","","","NO","",model);
        
    }
    
}
