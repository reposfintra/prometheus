/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.model.threads;

import com.tsp.operation.model.Model;
import com.tsp.operation.model.beans.Usuario;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;

/**
 *
 * @author maltamiranda
 */
public class HGenerarArchivoCorficolombiana extends Thread{

    private ArrayList info;
    private Model model;
    private Usuario usuario;
    private String processName,ruta;

    public void start(Model model,ArrayList info, Usuario usuario,String processName ){
        this.info = info;
        this.usuario = usuario;
        this.model = model;
        this.processName=processName;
        super.start();
    }

    public synchronized void run(){
        try{
            model.LogProcesosSvc.InsertProceso(this.processName, this.hashCode(), "Exportacion a EXCEL", usuario.getLogin());
            this.generarRUTA();
            this.generarArchivo();
            model.LogProcesosSvc.finallyProceso(this.processName, this.hashCode(), usuario.getLogin(), "PROCESO EXITOSO");
        }catch (Exception ex){
            ex.printStackTrace();
            try{
                model.LogProcesosSvc.finallyProceso(this.processName, this.hashCode(), usuario.getLogin(), "Finalizado con error ...\n" + ex.getMessage());
            }catch (Exception e){
                System.out.println("Error "+this.processName +"...\n" + e.getMessage());
            }
        }
    }


    public void generarRUTA() throws Exception{
        try{

            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            ruta = rb.getString("ruta") + "/exportar/migracion/" + usuario.getLogin();
            File archivo = new File( ruta );
            if (!archivo.exists()) archivo.mkdirs();

        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }

    }

    private void generarArchivo() throws Exception {
        String  url   =  ruta + "/" + processName + (new SimpleDateFormat("yyyMMdd_hhmmss")).format( new Date())+".txt";
      //  PrintWriter pw = new PrintWriter( new BufferedWriter( new FileWriter(url) )  );
        PrintWriter pw = new PrintWriter( new BufferedWriter( new OutputStreamWriter(new FileOutputStream(url),"ISO-8859-1" ) ));
        for(int i=0;i<info.size()&&info.size()>1;i++){
            ArrayList info2=(ArrayList)info.get(i);
            for(int j=0;j<info2.size();j++){
                pw.print((String)info2.get(j));
            }
            pw.println("");
        }
        pw.close();
    }

}

