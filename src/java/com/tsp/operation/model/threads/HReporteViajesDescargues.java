/*
 * HReporteAnticiposPagados.java
 *
 * Created on 19 octubre de 2006, 03:53 PM
 */

package com.tsp.operation.model.threads;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import java.io.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.controller.*;
import com.tsp.operation.model.services.*;
import com.tsp.operation.model.*;
import java.util.*;
import java.text.*;
import com.tsp.util.*;
/**
 *
 * @author  ffernandez
 */
public class HReporteViajesDescargues extends Thread{
    String user;
    String fechaI1;
    String Nit1;
    String fechaF1;
    String procesoName;
    String des;
     //String dstrct;
    Model model = new Model();
    
    public void start(String fechai, String fechaf,String usuario){
        this.user = usuario;
        this.procesoName = "Reporte de Viaje x Descargues";
        this.des = "Documento excel Reporte de Viaje x Descargues";
        this.fechaI1 = fechai;
        this.fechaF1 = fechaf;
               
        //this.dstrct = dstrct;
        super.start();
    }
    
    public synchronized void run(){
        try{
           model.LogProcesosSvc.InsertProceso(this.procesoName, this.hashCode(), des, this.user);
           Vector vec = model.planllegadacargue.getvectorValores();
           double valorTemp = 0;
           double convertir = 0;
           model.planllegadacargue.ReporteViajesDescargues(fechaI1,fechaF1);
           vec = model.planllegadacargue.getvectorValores();
            
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String path = rb.getString("ruta");
            //armas la ruta
            String Ruta1  = path + "/exportar/migracion/" + user + "/";
            //crear la ruta
            File file = new File(Ruta1);
            file.mkdirs();
            String hoy = Util.getFechaActual_String(6).trim().replaceAll("/", "").replaceAll(":","_");
            
            POIWrite xls = new POIWrite(Ruta1 +"ReporteViajexDescargues" + hoy + ".xls", user, Util.getFechaActual_String(5));
            
            HSSFCellStyle fecha  = xls.nuevoEstilo("Book Antiqua", 9, false , false, "yyyy/mm/dd"  , xls.NONE , xls.NONE , xls.NONE );
            HSSFCellStyle texto  = xls.nuevoEstilo("Book Antiqua", 9, false , false, "text"        , xls.NONE , xls.NONE , xls.NONE);
            HSSFCellStyle numero      = xls.nuevoEstilo("Book Antiqua", 9, false , false, "#.##"         , xls.NONE , xls.NONE , xls.NONE );
            
            HSSFCellStyle header      = xls.nuevoEstilo("Arial Black", 18, true  , false, "text"        , HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
            HSSFCellStyle titulo      = xls.nuevoEstilo("Book Antiqua", 9, true  , false, "text"        , HSSFColor.BLACK.index , HSSFColor.LIGHT_BLUE.index , HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle encabezado      = xls.nuevoEstilo("Book Antiqua", 9, true  , false, "text"        , HSSFColor.BLACK.index , HSSFColor.WHITE.index , HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle fechatitle  = xls.nuevoEstilo("Book Antiqua", 9, true  , false, "yyyy/mm/dd"  , HSSFColor.RED.index , xls.NONE, HSSFCellStyle.ALIGN_CENTER );
            
            
            /****************************************
             *    compos de la tabla en agencia     *
             ****************************************/
            
            xls.obtenerHoja("REPORTE");
            xls.cambiarMagnificacion(4,4);
            
            // cabecera
            BeanGeneral prom = (BeanGeneral) vec.lastElement();
            
            xls.adicionarCelda(0,0, "TRANSPORTES SANCHEZ POLO S.A.", encabezado);
            xls.combinarCeldas(0, 0, 0, 2);
            xls.adicionarCelda(1,0, "REPORTE VIAJES POR DESCARGUES " , encabezado);
            xls.adicionarCelda(2,0, "FECHA INICIAL "+ fechaI1 + "   FECHA FINAL "+fechaF1, encabezado);
            xls.combinarCeldas(2, 0, 0, 3);
             
             
            int fila = 6;
            int col  = 0;
            
            xls.adicionarCelda(fila ,col++ , "ITEM"     , titulo ); 
            xls.adicionarCelda(fila ,col++ , "REMISION"     , titulo );
            xls.adicionarCelda(fila ,col++ , "PLACA"     , titulo );
            xls.adicionarCelda(fila ,col++ , "FECHA DE CUMPLIDO"     , titulo );
            xls.adicionarCelda(fila ,col++ , "PESO REAL"     , titulo );
            xls.adicionarCelda(fila ,col++ , "NIT DEL PROPIETARIO"     , titulo );
            xls.adicionarCelda(fila ,col++ , "NOMBRE DEL PROPIETARIO"     , titulo );
            xls.adicionarCelda(fila ,col++ , "CEDULA DE CONDUCTOR"     , titulo );
            xls.adicionarCelda(fila ,col++ , "NOMBRE DEL CONDUCTOR"     , titulo );
            xls.adicionarCelda(fila ,col++ , "PESO MINA LLENO"     , titulo );
            xls.adicionarCelda(fila ,col++ , "PESO MINA VACIO"     , titulo );
            xls.adicionarCelda(fila ,col++ , "PESO NETO"     , titulo );
            xls.adicionarCelda(fila ,col++ , "RUTA"     , titulo );
            xls.adicionarCelda(fila ,col++ , "PUERTO DE DESCARGUE"     , titulo );
           
           double lleno = 0;
           double vacio = 0;
           double neto = 0;
           for(int i=0; i<vec.size();i++ ){
                fila++;
                col = 0;
               BeanGeneral rep = (BeanGeneral) vec.get(i);
                
                if (rep.getValor_09().equals("")){
                    lleno = 0;
                }else {
                    lleno = Double.parseDouble(rep.getValor_09());
                }
                if (rep.getValor_10().equals("")){
                    vacio = 0;
                }else {
                    vacio = Double.parseDouble(rep.getValor_10());
                }
                neto = lleno - vacio;
                xls.adicionarCelda(fila ,col++ ,rep.getValor_01(), texto );
                xls.adicionarCelda(fila ,col++ ,rep.getValor_02(), texto );
                xls.adicionarCelda(fila ,col++ ,rep.getValor_14() , texto );
                xls.adicionarCelda(fila ,col++ ,rep.getValor_03() , texto );
                xls.adicionarCelda(fila ,col++ ,rep.getValor_04() , texto );
                xls.adicionarCelda(fila ,col++ ,rep.getValor_05() , texto );
                xls.adicionarCelda(fila ,col++ ,rep.getValor_06() , texto );
                xls.adicionarCelda(fila ,col++ ,rep.getValor_07(), texto );
                xls.adicionarCelda(fila ,col++ ,rep.getValor_08() , texto );
                xls.adicionarCelda(fila ,col++ ,rep.getValor_09() , texto );
                xls.adicionarCelda(fila ,col++ ,rep.getValor_10()  , texto );
                xls.adicionarCelda(fila ,col++ ,neto , texto );
                xls.adicionarCelda(fila ,col++ ,rep.getValor_12()+ " - "+ rep.getValor_13() , texto );
                xls.adicionarCelda(fila ,col++ ,rep.getValor_15() , texto );
                lleno = 0;
                vacio = 0;
                neto = 0;
               
           }
           			
            
            xls.cerrarLibro();
            model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(),this.user,"PROCESO EXITOSO");
        }catch (Exception ex){
            ex.printStackTrace();
            try{
                model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(),this.user,"ERROR :" + ex.getMessage());
            }
            catch(Exception f){
                f.printStackTrace();
                try{
                    model.LogProcesosSvc.finallyProceso(this.procesoName,this.hashCode(),this.user,"ERROR :");
                }catch(Exception p){  
                    p.printStackTrace();
                }
            }
        }
        
    }
    
    
}
