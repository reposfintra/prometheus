
/*
* Nombre        ListadoRxPporConceptoTh.java
* Descripci�n   Exportaci�n a MS Excel de la consulta
* Autor         Ing. Andr�s Maturana D.
* Fecha         22 de marzo de 2007
* Versi�n       1.0
* Coyright      Transportes Sanchez Polo S.A.
*/

package com.tsp.operation.model.threads;

import com.tsp.operation.model.beans.POIWrite;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.beans.RepGral;
import com.tsp.operation.model.Model;

import java.io.*;
import java.util.*;
import java.text.*;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.util.HSSFColor;


/**
 *
 * @author  equipo
 */
public class ListadoRxPporConceptoTh extends Thread{
    

    private Vector info;
    private Model model;
    private Usuario usuario;
    private String fechai;
    private String fechaf;
    
    private SimpleDateFormat fmt;
    private String processName = "Reporte de Recurrentes por Concepto";
    
    
    
    // variables del archivo de excel
    String   ruta;
    String   nombre;
    POIWrite xls;
    HSSFCellStyle header  , titulo1, titulo2, titulo3 , titulo4, titulo5, letra, numero, dinero, numeroCentrado, porcentaje, letraCentrada, numeroNegrita, ftofecha;
    HSSFColor     cAzul   , cVerde, cAmarillo, cGris ;
    int fila = 0;
    
    
    /** Creates a new instance of HReporteOcsConAnticipoSinReporte */
    public ListadoRxPporConceptoTh() {
    }
    
    public void start(Model model, Vector info, Usuario usuario, String fechai, String fechaf){
        this.info = info;
        this.usuario = usuario;
        this.model = model;
        this.fechai = fechai;
        this.fechaf = fechaf;
        super.start();
    }
    
    public synchronized void run(){
        try{
            model.LogProcesosSvc.InsertProceso(this.processName, this.hashCode(), "Generacion del Reporte de Recurrentes por Concepto. Periodo: " + fechai + " a " + fechaf, usuario.getLogin());
            this.generarRUTA();
            this.generarArchivo();
            model.LogProcesosSvc.finallyProceso(this.processName, this.hashCode(), usuario.getLogin(), "PROCESO EXITOSO");
        }catch (Exception ex){
            ex.printStackTrace();
            try{
                model.LogProcesosSvc.finallyProceso(this.processName, this.hashCode(), usuario.getLogin(), "Finalizado con error ...\n" + ex.getMessage());
            }catch (Exception e){
                //System.out.println("Error HReporteOcsConAnticipoSinReporte ...\n" + e.getMessage());
            }
        }
    }
    
    
    public void generarRUTA() throws Exception{
        try{
            
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            ruta = rb.getString("ruta") + "/exportar/migracion/" + usuario.getLogin();
            File archivo = new File( ruta );
            if (!archivo.exists()) archivo.mkdirs();
            
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
        
    }
    
    
    /**
     * Metodo para Incializar las variables del archivo de excel
     * @autor mfontalvo
     * @param nameFile, nombre del archivo
     * throws Exception.
     */
    private void InitArchivo(String nameFile) throws Exception{
        try{
            xls          = new com.tsp.operation.model.beans.POIWrite();
            nombre       = "/exportar/migracion/" + usuario.getLogin() + "/" + nameFile;
            xls.nuevoLibro( ruta + "/" + nameFile );
            
            // colores
            /*cAzul       = xls.obtenerColor(  0, 51,102);
            cVerde      = xls.obtenerColor( 51,153,102);
            cAmarillo   = xls.obtenerColor(255,255,153);
            cGris       = xls.obtenerColor(192,192,192);*/
            
            
            // estilos
            header         = xls.nuevoEstilo("Tahoma", 14, true  , false, "text"  , HSSFColor.ORANGE.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
            titulo1        = xls.nuevoEstilo("Tahoma", 8 , true  , false, "text"  , xls.NONE  , xls.NONE , xls.NONE);
            titulo2        = xls.nuevoEstilo("Tahoma", 8 , true  , false, "text"  , HSSFColor.WHITE.index, HSSFColor.DARK_BLUE.index , HSSFCellStyle.ALIGN_CENTER, 2);
            //titulo3        = xls.nuevoEstilo("Tahoma", 8 , true  , false, "text"  , HSSFColor.WHITE.index, cAmarillo.getIndex() , HSSFCellStyle.ALIGN_CENTER, 2);
            //titulo4        = xls.nuevoEstilo("Tahoma", 8 , true  , false, "text"  , HSSFColor.WHITE.index, cVerde.getIndex() , HSSFCellStyle.ALIGN_CENTER, 2);
            //titulo5        = xls.nuevoEstilo("Tahoma", 14, true  , false, "text"  , xls.NONE  , xls.NONE , xls.NONE);
            letra          = xls.nuevoEstilo("Tahoma", 8 , false , false, ""      , xls.NONE , xls.NONE , xls.NONE);
            letraCentrada  = xls.nuevoEstilo("Tahoma", 8 , false , false, ""      , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_CENTER);
            numero         = xls.nuevoEstilo("Tahoma", 8 , false , false, "#"     , xls.NONE , xls.NONE , xls.NONE);
            numeroCentrado = xls.nuevoEstilo("Tahoma", 8 , false , false, "#"     , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_CENTER);
            dinero         = xls.nuevoEstilo("Tahoma", 8 , false , false, "#,##0.00" , xls.NONE , xls.NONE , xls.NONE);
            numeroNegrita  = xls.nuevoEstilo("Tahoma", 8 , true  , false, "#"     , xls.NONE , xls.NONE , xls.NONE);
            porcentaje     = xls.nuevoEstilo("Tahoma", 8 , false , false, "0.00%" , xls.NONE , xls.NONE , xls.NONE);
            ftofecha       = xls.nuevoEstilo("Tahoma", 8 , false , false, "yyyy-mm-dd" , xls.NONE , xls.NONE , xls.NONE);
            
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception("Error en InitArchivo .... \n" + ex.getMessage());
        }
        
    }
    
    
    
    /**
     * Metodo para crear el  archivo de excel
     * @autor mfontalvo
     * @param nameFile, Nombre del archivo
     * @param titulo, titulo del archivo de excel
     * @throws Exception.
     */
    private void crearArchivo(String nameFile, String titulo) throws Exception{
        try{
            fmt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            InitArchivo(nameFile);
            xls.obtenerHoja("Base");
            xls.combinarCeldas(0, 0, 0, 8);
            xls.adicionarCelda(0,0, titulo, header);
            xls.adicionarCelda(1,0, "FECHA" , titulo1);
            xls.adicionarCelda(1,1, fmt.format( new Date())  , titulo1 );
            xls.adicionarCelda(2,0, "PERIODO", titulo1);
            xls.adicionarCelda(2,1, fechai + " a " + fechaf  , titulo1);
            xls.adicionarCelda(3,0, "USUARIO", titulo1);
            xls.adicionarCelda(3,1, usuario.getNombre() , titulo1);
            
            
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception( "Error en crearArchivo ....\n"  + ex.getMessage() );
        }
    }
    
    
    /**
     * Exportacion del las amortizaciones transferidas
     * @autor mfontalvo
     * @throws Exception.
     */
    private void generarArchivo() throws Exception {
        try{
            fmt= new SimpleDateFormat("yyyMMdd_hhmmss");
            this.crearArchivo("ReporteRecurrentes_" + fmt.format( new Date() ) +".xls", "REPORTE DE RECURRENTES ");
            fila = 6;
            
            Vector vector = this.info;
            if (vector!=null && !vector.isEmpty()){
                


                    // encabezado
                String [] cabecera = { 
                    "Recurrente", "Fecha", "Descripci�n", "C�dula o Nit", "Nombre", "Valor", "Cuotas a Descontar", "Fecha Inicio Descuento",
                    "Cuota a Descontar", "Cuotas Generadas", "Fecha Proxima Cuota", "Fecha Ultima Cuota", "Vr. Transferido", "Saldo", "Item", "Descripci�n Item", "Concepto", "Descripci�n Concepto", "Cuenta"
                };
                short  [] dimensiones = new short [] { 
                    4500, 4000, 7000, 3500, 10000, 5500, 5000, 5500,
                    5500, 5500, 6000, 6000, 5500, 5500, 1500, 7000, 2500, 7000, 5000
                };
                for ( int i = 0; i<cabecera.length; i++){
                    xls.adicionarCelda(fila,  i, cabecera[i], titulo2);
                    if ( i < dimensiones.length )
                        xls.cambiarAnchoColumna(i, dimensiones[i] );
                }                
                fila++;
                
                
                for ( int i = 0; i< vector.size(); i++, fila++){
                    int col = 0;
                    RepGral rp = (RepGral) vector.elementAt(i);

                    xls.adicionarCelda(fila  , col++ , rp.getNo_recurrente(), letra  );
                    xls.adicionarCelda(fila  , col++ , rp.getFecha_rxp(), ftofecha  );
                    xls.adicionarCelda(fila  , col++ , rp.getDescripcion_rxp(), letra  );
                    xls.adicionarCelda(fila  , col++ , rp.getProveedor_rxp(), letra  );
                    xls.adicionarCelda(fila  , col++ , rp.getNom_proveedor_rxp(), letra  );
                    xls.adicionarCelda(fila  , col++ , rp.getVlr_rxp(), dinero  );
                    xls.adicionarCelda(fila  , col++ , rp.getNo_cuotas_rxp(), numeroCentrado  );
                    xls.adicionarCelda(fila  , col++ , rp.getFecha_ini_dscto(), ftofecha  );
                    xls.adicionarCelda(fila  , col++ , rp.getVlr_prox_cuota(), dinero  );
                    xls.adicionarCelda(fila  , col++ , rp.getNo_cuotas_transfer(), letra  );
                    xls.adicionarCelda(fila  , col++ , rp.getFecha_prox_cuota(), ftofecha  );
                    xls.adicionarCelda(fila  , col++ , rp.getFecha_fin_dcto(), ftofecha  );
                    xls.adicionarCelda(fila  , col++ , rp.getVlr_transfer_ml(), dinero  );
                    xls.adicionarCelda(fila  , col++ , rp.getSaldo(), dinero  );
                    xls.adicionarCelda(fila  , col++ , rp.getNo_item(), letra  );
                    xls.adicionarCelda(fila  , col++ , rp.getDescripcion_rxp_item(), letra  );
                    xls.adicionarCelda(fila  , col++ , rp.getConcepto_code(), letraCentrada  );
                    xls.adicionarCelda(fila  , col++ , rp.getConcepto_desc(), letra  );
                    xls.adicionarCelda(fila  , col++ , rp.getCod_cuenta(), letra  );
                    //xls.adicionarCelda(fila  , col++ , rp, letra  );
                }            

                
                
            } else {
                xls.adicionarCelda(fila  ,0, "NO HAY DATOS PARA MOSTRAR", titulo5);
                xls.combinarCeldas(fila , 0, fila, 0+5);
                fila++;                
            }
            
            
            this.cerrarArchivo();
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception("Error en generarArchivo ...\n" + ex.getMessage() );
        }
    }
    
    
    /**
     * Metodo para cerrar el  archivo de excel
     * @autor mfontalvo
     * @throws Exception.
     */
    private void cerrarArchivo() throws Exception {
        try{
            if (xls!=null)
                xls.cerrarLibro();
        }catch (Exception ex){
            throw new Exception( "Error en crearArchivo ....\n"  + ex.getMessage() );
        }
    }
    
    
}

//22 Marzo de 2007
