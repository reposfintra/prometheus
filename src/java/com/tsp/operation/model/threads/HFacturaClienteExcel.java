

package com.tsp.operation.model.threads;

import com.tsp.operation.model.beans.POIWrite;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.Model;
import com.tsp.util.*;

import java.io.*;
import java.util.*;
import java.text.*;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.util.HSSFColor;


/**
 *
 * @author  equipo
 */
public class HFacturaClienteExcel extends Thread{

    
    Model model;
    Usuario usuario;
    SimpleDateFormat fmt;
    String processName = "Factura Cliente";
    
    
    // variables del archivo de excel
    String   ruta;
    String   nombre;
    POIWrite xls;
    HSSFCellStyle header  , titulo1, titulo2, titulo5, letra, numero, dinero, numeroCentrado, porcentaje, letraCentrada, numeroNegrita;
    HSSFColor     cAzul   , cVerde, cAmarillo, cGris ;
    int fila = 0;
    
    
    /** Creates a new instance of HReporteOcsConAnticipoSinReporte */
    public HFacturaClienteExcel() {
    }
    
    public void start(Model model, Usuario usuario){
        this.usuario  = usuario;
        this.model    = model;
        super.start();
    }
    
    public synchronized void run(){
        try{
            model.LogProcesosSvc.InsertProceso(this.processName, this.hashCode(), "Generacion del archivo de factura cliente ", usuario.getLogin());
            this.generarRUTA();
            this.generarArchivo();
            model.LogProcesosSvc.finallyProceso(this.processName, this.hashCode(), usuario.getLogin(), "PROCESO EXITOSO");
        }catch (Exception ex){
            ex.printStackTrace();
            try{
                model.LogProcesosSvc.finallyProceso(this.processName, this.hashCode(), usuario.getLogin(), "Finalizado con error ...\n" + ex.getMessage());
            }catch (Exception e){
                //System.out.println("Error HReporteOcsConAnticipoSinReporte ...\n" + e.getMessage());
            }
        }
    }
    
    
    public void generarRUTA() throws Exception{
        try{
            
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            ruta = rb.getString("ruta") + "/exportar/migracion/" + usuario.getLogin();
            File archivo = new File( ruta );
            if (!archivo.exists()) archivo.mkdirs();
            
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
        
    }
    
    
    /**
     * Metodo para Incializar las variables del archivo de excel
     * @autor mfontalvo
     * @param nameFile, nombre del archivo
     * throws Exception.
     */
    private void InitArchivo(String nameFile) throws Exception{
        try{
            xls          = new com.tsp.operation.model.beans.POIWrite();
            nombre       = "/exportar/migracion/" + usuario.getLogin() + "/" + nameFile;
            xls.nuevoLibro( ruta + "/" + nameFile );
            
            
            // estilos
            header         = xls.nuevoEstilo("Tahoma", 14, true  , false, "text"  , HSSFColor.ORANGE.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
            titulo1        = xls.nuevoEstilo("Tahoma", 8 , true  , false, "text"  , HSSFColor.WHITE.index  , HSSFColor.DARK_YELLOW.index , HSSFCellStyle.ALIGN_CENTER, 2);
            titulo2        = xls.nuevoEstilo("Tahoma", 8 , true  , false, "text"  , HSSFColor.WHITE.index, HSSFColor.DARK_BLUE.index , HSSFCellStyle.ALIGN_CENTER, 2);
            titulo5        = xls.nuevoEstilo("Tahoma", 14, true  , false, "text"  , xls.NONE  , xls.NONE , xls.NONE);
            letra          = xls.nuevoEstilo("Tahoma", 8 , false , false, ""      , xls.NONE , xls.NONE , xls.NONE);
            letraCentrada  = xls.nuevoEstilo("Tahoma", 8 , false , false, ""      , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_CENTER);
            numero         = xls.nuevoEstilo("Tahoma", 8 , false , false, "#"     , xls.NONE , xls.NONE , xls.NONE);
            numeroCentrado = xls.nuevoEstilo("Tahoma", 8 , false , false, "#"     , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_CENTER);
            dinero         = xls.nuevoEstilo("Tahoma", 8 , false , false, "#0.00" , xls.NONE , xls.NONE , xls.NONE);
            numeroNegrita  = xls.nuevoEstilo("Tahoma", 8 , true  , false, "#"     , xls.NONE , xls.NONE , xls.NONE);
            porcentaje     = xls.nuevoEstilo("Tahoma", 8 , false , false, "0.00%" , xls.NONE , xls.NONE , xls.NONE);
            
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception("Error en InitArchivo .... \n" + ex.getMessage());
        }
        
    }
    
    
    
    /**
     * Metodo para crear el  archivo de excel
     * @autor mfontalvo
     * @param nameFile, Nombre del archivo
     * @param titulo, titulo del archivo de excel
     * @throws Exception.
     */
    private void crearArchivo(String nameFile, String titulo) throws Exception{
        try{
            fmt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            InitArchivo(nameFile);
            xls.obtenerHoja("BASE");
           /* xls.combinarCeldas(0, 0, 0, 8);
            xls.adicionarCelda(0,0, titulo, header);
            xls.adicionarCelda(1,0, "FECHA" , titulo1);
            xls.adicionarCelda(1,1, fmt.format( new Date())  , titulo1 );
            xls.adicionarCelda(3,0, "USUARIO", titulo1);
            xls.adicionarCelda(3,1, usuario.getNombre() , titulo1);*/
            
            
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception( "Error en crearArchivo ....\n"  + ex.getMessage() );
        }
    }
    
    
    /**
     * Exportacion del las amortizaciones transferidas
     * @autor mfontalvo
     * @throws Exception.
     */
    private void generarArchivo() throws Exception {
        try{
            fmt= new SimpleDateFormat("yyyMMdd_hhmmss");
            this.crearArchivo("CXC01" + fmt.format( new Date() ) +".xls", "REPORTE DE OCS CON ANTICIPOS SIN REPORTE EN TRAFICO ");
            fila = 0;
            
            factura fac = model.facturaService.getFactu();
            Vector vector = model.facturaService.getRemesasFacturar();
            
            
            
            if (vector!=null && !vector.isEmpty()){
                
                vector = model.facturaService.buscarDatosRemesa(vector);
                
              
                // encabezado
                String [] cabecera = {
                    "Cliente", "Nit", "Agencia Facturacion", "Fecha Factura", "Forma de Pago", "Plazo",
                    
                    "Descripcion","Observacion",
                    
                    "Moneda","Tasa", "Valor", "Item", "Remesa","Fecha","Origen","Destino",
                    
                    "Peso real cargado","Unit packed","Peso cobrar",
                    
                    "Planilla", "Placa", "Descripcion item", "Cantidad",
                    
                    "Unidad", "Valor Unitario", "Valor Item",
                    
                    "Codigo Cuenta","Tipo","Auxiliar"
                };
                short  [] dimensiones = new short [] {
                    3000, 3000, 4000, 3500, 3500, 3000,
                    
                    5000,5000,
                    
                    3500, 3000, 6000, 6000,  4000, 4000, 6000, 6000,
                    
                    2000, 2000, 2000,
                    
                    4000, 4000, 7000, 1500,
                    
                    4000, 4000, 5500,
                    
                    3500, 1000, 4000
                };
                for ( int i = 0; i<cabecera.length; i++){
                    HSSFCellStyle cab ;
                    cab = (i>=11)?titulo1:titulo2;
                    xls.adicionarCelda(fila,  i, cabecera[i], cab);
                    if ( i < dimensiones.length )
                        xls.cambiarAnchoColumna(i, dimensiones[i] );
                }
                fila++;
                
                
                for ( int i = 0; i< vector.size(); i++, fila++){
                    int col = 0;
                    factura_detalle factu_deta = (factura_detalle) vector.get(i);
                    
                    xls.adicionarCelda(fila  , col++ , fac.getCodcli()               , letra  );
                    xls.adicionarCelda(fila  , col++ , fac.getNit()                  , letra  );
                    xls.adicionarCelda(fila  , col++ , fac.getAgencia_facturacion()  , letra );
                    xls.adicionarCelda(fila  , col++ , fac.getFecha_factura()        , letra );
                    xls.adicionarCelda(fila  , col++ , fac.getForma_pago()           , letra  );
                    xls.adicionarCelda(fila  , col++ , fac.getPlazo()                , letra  );
                    xls.adicionarCelda(fila  , col++ , fac.getDescripcion()          , letra  );
                    xls.adicionarCelda(fila  , col++ , fac.getObservacion()          , letra  );
                    xls.adicionarCelda(fila  , col++ , fac.getMoneda()               , letra );
                    xls.adicionarCelda(fila  , col++ , fac.getValor_tasa().doubleValue()     , dinero  );
                    xls.adicionarCelda(fila  , col++ , fac.getValor_factura()                , dinero  );
                    xls.adicionarCelda(fila  , col++ , Util.llenarConCerosALaIzquierda(i+1,3), letra  );
                    xls.adicionarCelda(fila  , col++ , factu_deta.getNumero_remesa()   , letra  );
                    xls.adicionarCelda(fila  , col++ , factu_deta.getFecrem()    , letra  );
                    xls.adicionarCelda(fila  , col++ , factu_deta.getOrigen()    , letra  );
                    xls.adicionarCelda(fila  , col++ , factu_deta.getDestino()   , letra  );
                    
                    xls.adicionarCelda(fila  , col++ , factu_deta.getPeso_real_cargado()       , dinero  );
                    xls.adicionarCelda(fila  , col++ , factu_deta.getUnit_packed()   , letra  );
                    xls.adicionarCelda(fila  , col++ , factu_deta.getPeso_cobrar()       , dinero  );
                    
                    
                    xls.adicionarCelda(fila  , col++ , factu_deta.getPlanilla()  , letra  );
                    xls.adicionarCelda(fila  , col++ , factu_deta.getPlaveh()    , letra  );
                    
                    xls.adicionarCelda(fila  , col++ , factu_deta.getDescripcion()           , letra  );
                    xls.adicionarCelda(fila  , col++ , factu_deta.getCantidad().doubleValue(), dinero  );
                    
                    
                    xls.adicionarCelda(fila  , col++ , factu_deta.getUnidad()           , letra  );
                    xls.adicionarCelda(fila  , col++ , factu_deta.getValor_unitario()   , dinero );
                    xls.adicionarCelda(fila  , col++ , factu_deta.getValor_item()       , dinero  );
                    
                    xls.adicionarCelda(fila  , col++ , factu_deta.getCodigo_cuenta_contable()        , letra  );
                    xls.adicionarCelda(fila  , col++ , factu_deta.getTiposubledger()                 , letra  );
                    //String auxiliar = (!factu_deta.getTiposubledger().equals(""))?factu_deta.getTiposubledger()+"-"+factu_deta.getAuxliliar():"";
                    xls.adicionarCelda(fila  , col++ , factu_deta.getAuxliliar()                      , letra  );
                    
                }
                
                
                
            } else {
                xls.adicionarCelda(fila  ,0, "NO HAY DATOS PARA MOSTRAR", titulo5);
                xls.combinarCeldas(fila , 0, fila, 0+5);
                fila++;
            }
            
            
            this.cerrarArchivo();
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception("Error en generarArchivo ...\n" + ex.getMessage() );
        }
    }
    
    
    /**
     * Metodo para cerrar el  archivo de excel
     * @autor mfontalvo
     * @throws Exception.
     */
    private void cerrarArchivo() throws Exception {
        try{
            if (xls!=null)
                xls.cerrarLibro();
        }catch (Exception ex){
            throw new Exception( "Error en crearArchivo ....\n"  + ex.getMessage() );
        }
    }
    
    
}

