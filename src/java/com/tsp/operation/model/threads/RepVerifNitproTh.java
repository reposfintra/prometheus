/********************************************************************
 *      Nombre Clase.................   RepVerifNitproTh.java
 *      Descripción..................   Hilo para la escritura del xls
 *      Autor........................   Ing. Andrés Maturana D.
 *      Fecha........................   20.09.2006
 *      Versión......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.model.threads;


import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import java.io.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.operation.model.*;
import java.util.*;
import java.text.*;
import org.apache.log4j.*;
//import com.tsp.operation.model.threads.POIWrite;



public class RepVerifNitproTh extends Thread{
    
    Logger logger = Logger.getLogger(this.getClass());
    
    private List reporte;
    private String user;
    private String fechai;
    private String fechaf;
    private Model model;
    private String cia;
    
    public void start(Model modelo, String user, String fechai, String fechaf, String cia){
        this.user = user;
        this.fechaf = fechaf;
        this.fechai = fechai;
        this.model = modelo;
        this.cia = cia;
        super.start();
    }
    
    public synchronized void run(){
        try{
            
            model.proveedorService.reporteVerificacionNitpro(fechai, fechaf, cia);
            //model.proveedorService.reporteVerificacionNitpro();
            this.reporte = model.proveedorService.getProveedores_list();
            logger.info("REGISTROS ENCONTRADOS: " + this.reporte.size());
            
            //INSERTO EN EL LOG DE PROCESO
            model.LogProcesosSvc.InsertProceso("Generación Reporte Verificación Propietario", this.hashCode(), "Generación Reporte Verificación Propietario " + fechai + " - " + fechaf, user);
            
            //Fecha del sistema
            Calendar FechaHoy = Calendar.getInstance();
            Date d = FechaHoy.getTime();
            SimpleDateFormat s = new SimpleDateFormat("yyyyMMdd_kkmm");
            SimpleDateFormat s1 = new SimpleDateFormat("yyyy-MM-dd-hh:mm");
            SimpleDateFormat fec = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss");
            SimpleDateFormat time = new SimpleDateFormat("hh:mm");
            String FechaFormated = s.format(d);
            String FechaFormated1 = s1.format(d);
            String hora = time.format(d);
            String Fecha = fec.format(d);
            
            
            //obtener cabecera de ruta
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String path = rb.getString("ruta");
            //armas la ruta
            String Ruta1  = path + "/exportar/migracion/" + user + "/";
            //crear la ruta
            File file = new File(Ruta1);
            file.mkdirs();
            
            POIWrite xls = new POIWrite(Ruta1 +"ReportePropietario_" + FechaFormated + ".xls", user, Fecha);
            String nom_archivo = "ReportePropietario_" + FechaFormated + ".xls";
            
            //Definición de Estilos
            HSSFCellStyle fecha  = xls.nuevoEstilo("Arial", 9, false , false, "yyyy/mm/dd"  , xls.NONE , xls.NONE , xls.NONE );
            HSSFCellStyle texto  = xls.nuevoEstilo("Arial", 9, false , false, "text"        , xls.NONE , xls.NONE , xls.NONE);
            HSSFCellStyle numero = xls.nuevoEstilo("Arial", 9, false , false, ""            , xls.NONE , xls.NONE , xls.NONE);
            
            HSSFCellStyle header1      = xls.nuevoEstilo("Arial", 12, true , false, "text"        , HSSFColor.WHITE.index , HSSFColor.GREEN.index , HSSFCellStyle.ALIGN_LEFT);
            HSSFCellStyle header2      = xls.nuevoEstilo("Arial", 18, true , false, "text"        , HSSFColor.WHITE.index , HSSFColor.ORANGE.index , HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle header3      = xls.nuevoEstilo("Arial", 12, true , false, "text"        , HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
            HSSFCellStyle texto2      = xls.nuevoEstilo("Arial", 12, false , false, "text"        , HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
            HSSFCellStyle titulo      = xls.nuevoEstilo("Arial", 12, true  , false, "text"        , HSSFColor.BLACK.index , HSSFColor.PINK.index , HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle fechatitle  = xls.nuevoEstilo("Arial", 12, true  , false, "yyyy/mm/dd"  , HSSFColor.RED.index , xls.NONE, HSSFCellStyle.ALIGN_CENTER );
            HSSFCellStyle entero      = xls.nuevoEstilo("Arial", 12, false , false, "text"        , HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle flotante      = xls.nuevoEstilo("Arial", 12, false , false, "text"        , HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_RIGHT);
            
            
            xls.obtenerHoja("REPORTE");
            xls.cambiarMagnificacion(3,4);
            
            // cabecera
            
            xls.combinarCeldas(0, 0, 0, 10);
            xls.combinarCeldas(3, 0, 3, 10);
            xls.combinarCeldas(4, 0, 4, 10);
            xls.adicionarCelda(0, 0, "REPORTE DE VERIFICACION DE PROPIETARIO", header2);
            xls.adicionarCelda(3, 0, "Fecha desde: " + fechai + " Hasta: " + fechaf, header3);
            xls.adicionarCelda(4, 0, "Fecha del proceso: " + Fecha, header3);
            
            // subtitulos
            
            
            int fila = 6;
            int col  = 0;
            
            xls.adicionarCelda(fila, col++, "PLANILLA", header1);
            xls.adicionarCelda(fila, col++, "FECHA", header1);
            xls.adicionarCelda(fila, col++, "ORIGEN", header1);
            xls.adicionarCelda(fila, col++, "DESTINO", header1);
            xls.adicionarCelda(fila, col++, "AGENCIA", header1);
            xls.adicionarCelda(fila, col++, "FECHA CREACION", header1);
            xls.adicionarCelda(fila, col++, "USUARIO CREO", header1);
            xls.adicionarCelda(fila, col++, "NIT PROPIETARIO", header1);
            xls.adicionarCelda(fila, col++, "EXISTE NIT", header1);
            xls.adicionarCelda(fila, col++, "EXISTE PROVEEDOR", header1);
            xls.adicionarCelda(fila, col++, "EXISTE CMC", header1);
            
            fila++;
            
            for( int i=0; i<this.reporte.size(); i++){
                col = 0;
                
                RepGral obj = (RepGral) this.reporte.get(i);
                
                xls.adicionarCelda(fila, col++, obj.getNumpla(), texto2);
                xls.adicionarCelda(fila, col++, obj.getFecpla(), texto2);
                xls.adicionarCelda(fila, col++, obj.getOripla(), texto2);
                xls.adicionarCelda(fila, col++, obj.getDespla(), texto2);
                xls.adicionarCelda(fila, col++, obj.getAgcpla(), texto2);
                xls.adicionarCelda(fila, col++, obj.getCreation_date(), texto2);
                xls.adicionarCelda(fila, col++, obj.getCreation_user(), texto2);
                xls.adicionarCelda(fila, col++, obj.getNitpro(), texto2);
                xls.adicionarCelda(fila, col++, obj.getExiste_nit(), texto2);
                xls.adicionarCelda(fila, col++, obj.getExiste_prov(), texto2);
                xls.adicionarCelda(fila, col++, obj.getExiste_cmc(), texto2);
                
                fila++;
            }
            
            
            
            xls.cerrarLibro();
            
            model.LogProcesosSvc.finallyProceso("Generación Reporte Verificación Propietario", this.hashCode(), user, "PROCESO EXITOSO");
        }catch (Exception ex){
            ex.printStackTrace();
            //System.out.println("ERROR AL GENERAR EL ARCHIVO XLS : " + ex.getMessage());
            try{
                model.LogProcesosSvc.finallyProceso("Generación Reporte Verificación Propietario", this.hashCode(), user , "ERROR :" + ex.getMessage());
            }
            catch(Exception f){
                try{
                    model.LogProcesosSvc.finallyProceso("Generación Reporte Verificación Propietario", this.hashCode(), user,"ERROR :");
                }catch(Exception p){    }
            }
        }
        
    }
    
    
}