/*************************************************************************
 * Nombre:        HMigracionRemesaNF.java            *
 * Descripci�n:   Hilos que escribe el archivo csv    *
 * Autor:         Ing. Diogenes Antonio Bastidas Morales   *
 * Fecha:         20 de enero de 2006, 01:36 PM                               * 
 * Versi�n        1.0                                      * 
 * Coyright:      Transportes Sanchez Polo S.A.            *
 *************************************************************************/

package com.tsp.operation.model.threads;
import java.io.*;
import java.sql.*;
import java.util.*;
import java.text.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.connectionpool.PoolManager;
import com.tsp.util.Util;
import java.util.Vector;
import java.io.*;  


public class HMigracionRemesaNF extends Thread {
    private Vector remesasnf;
    private FileWriter fw;
    private BufferedWriter bffw;
    private PrintWriter pntw;
    private String usuario;
    private String path;
    
    /** Creates a new instance of HMigracionRemesaNF */
    public HMigracionRemesaNF() {
    }
    
    public void start(Vector remesasnf, String usu) {
        this.remesasnf = remesasnf;
        this.usuario = usu;
        
        try{
            String nombre_archivo = "";
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            path = rb.getString("ruta");
            String ruta = path + "/exportar/migracion/" + usuario + "/";
            String fecha_actual = Util.getFechaActual_String(6);
            
            
            nombre_archivo = ruta + "/RemesaNF" + fecha_actual.replace(':','_').replace('/','_') + ".csv";
            
            File archivo = new File(ruta.trim());
                
            archivo.mkdirs();
            FileOutputStream file = new FileOutputStream(nombre_archivo.trim());
            
            fw = new FileWriter(nombre_archivo);
            bffw = new BufferedWriter(fw);
            pntw = new PrintWriter(bffw);
        }
        catch(java.io.IOException e){
            ////System.out.println(e.toString());
        }
        
        super.start();
    }
    
    public synchronized void run(){
       this.writeFile();
    }
    
    protected void writeFile() {    	
    	String linea = "";          
        for(int i=0; i<remesasnf.size(); i++){
            RemesaAnulada remnf = (RemesaAnulada) remesasnf.elementAt(i);
            linea = remnf.getNumero_remesa() + "," + remnf.getFecha_creacion() + "," +
                remnf.getUsuario_creacion() + ",NF";
            ////System.out.println(linea);
            pntw.println(linea);
        }

        pntw.close();
        ////System.out.println("Listo Remesas NF"); 	    	    	
    }
    
    protected String getFormatDate(String fecha){
        String formato="";
        if(!fecha.equals("")){
            String  ano = fecha.substring(2,4);
            String  mes = fecha.substring(5,7);
            String  dia = fecha.substring(8,10);
            formato     = dia+"/"+mes+"/"+ano;
        }
        return formato;
    }
    
}
