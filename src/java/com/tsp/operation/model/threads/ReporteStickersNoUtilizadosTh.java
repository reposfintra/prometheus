/********************************************************************
 *      Nombre Clase.................   ReporteStickersNoUtilizados.java
 *      Descripci�n..................   Hilo para la escritura del xls
 *      Autor........................   Tito Andr�s Maturana
 *      Fecha........................   04.01.2006
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.model.threads;


import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import java.io.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.operation.model.*;
import java.util.*;
import java.text.*;
//import com.tsp.operation.model.threads.POIWrite;



public class ReporteStickersNoUtilizadosTh extends Thread{
    
    private Vector reporte;
    private String user;    
    private String fechai;
    private String fechaf;
    private String agencia;
    
    private Model model;
    
    //Log Procesos
    private String procesoName;
    private String des;
    
    public void start(Model modelo, Vector reporte, String user, String fechai, String fechaf, String agencia){
        this.reporte = reporte;
        this.user = user;
        this.fechaf = fechaf;
        this.fechai = fechai;
        this.agencia = agencia;
        
        this.model   = modelo;
        this.procesoName = "Reporte Stickers No Utilizados";
        this.des = "Reporte Stickers No Utilizados:  " + fechai + " - " + fechaf;
        
        super.start();
    }
    
    public synchronized void run(){
        try{
            //INSERTO EN EL LOG DE PROCESO
            model.LogProcesosSvc.InsertProceso(this.procesoName, this.hashCode(), this.des, user);
            
            //Fecha del sistema
            Calendar FechaHoy = Calendar.getInstance();
            Date d = FechaHoy.getTime();
            SimpleDateFormat s = new SimpleDateFormat("yyyyMMdd_kkmm");
            SimpleDateFormat s1 = new SimpleDateFormat("yyyy-MM-dd-hh:mm");
            SimpleDateFormat fec = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat time = new SimpleDateFormat("hh:mm");
            String FechaFormated = s.format(d);
            String FechaFormated1 = s1.format(d);
            String hora = time.format(d);
            String Fecha = fec.format(d);
            
            
            //obtener cabecera de ruta
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String path = rb.getString("ruta");
            //armas la ruta
            String Ruta1  = path + "/exportar/migracion/" + user + "/";
            //crear la ruta
            File file = new File(Ruta1);
            file.mkdirs();
            
            POIWrite xls = new POIWrite(Ruta1 +"ReporteStickersNoUtilizados_" + FechaFormated + ".xls", user, Fecha);
            String nom_archivo = "ReporteStickersNoUtilizados_" + FechaFormated + ".xls";            
            
            //Definici�n de Estilos
            HSSFCellStyle fecha  = xls.nuevoEstilo("Arial", 9, false , false, "yyyy/mm/dd"  , xls.NONE , xls.NONE , xls.NONE );
            HSSFCellStyle texto  = xls.nuevoEstilo("Arial", 9, false , false, "text"        , xls.NONE , xls.NONE , xls.NONE);
            HSSFCellStyle numero = xls.nuevoEstilo("Arial", 9, false , false, ""            , xls.NONE , xls.NONE , xls.NONE);
            
            HSSFCellStyle header1      = xls.nuevoEstilo("Arial", 12, true , false, "text"        , HSSFColor.WHITE.index , HSSFColor.GREEN.index , HSSFCellStyle.ALIGN_LEFT);
            HSSFCellStyle header2      = xls.nuevoEstilo("Arial", 18, true , false, "text"        , HSSFColor.WHITE.index , HSSFColor.ORANGE.index , HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle header3      = xls.nuevoEstilo("Arial", 12, true , false, "text"        , HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
            HSSFCellStyle texto2      = xls.nuevoEstilo("Arial", 12, false , false, "text"        , HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
            HSSFCellStyle titulo      = xls.nuevoEstilo("Arial", 12, true  , false, "text"        , HSSFColor.BLACK.index , HSSFColor.PINK.index , HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle fechatitle  = xls.nuevoEstilo("Arial", 12, true  , false, "yyyy/mm/dd"  , HSSFColor.RED.index , xls.NONE, HSSFCellStyle.ALIGN_CENTER );
            HSSFCellStyle entero      = xls.nuevoEstilo("Arial", 12, false , false, "text"        , HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle flotante      = xls.nuevoEstilo("Arial", 12, false , false, "text"        , HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_RIGHT);
                                                
                                    
            xls.obtenerHoja("STICKERS NO UTILIZADOS");
            xls.cambiarMagnificacion(3,4);
            
            // cabecera
            
            xls.combinarCeldas(0, 0, 0, 3);
            xls.combinarCeldas(3, 0, 3, 3);
            xls.combinarCeldas(4, 0, 4, 3);
            xls.adicionarCelda(0, 0, "REPORTE DE STICKERS NO UTILIZADOS" + 
                    (( this.agencia.length()!=0)? "Agencia: " + agencia : ""), header2);
            xls.adicionarCelda(3, 0, "Fecha desde: " + fechai + " Hasta: " + fechaf, header3);
            xls.adicionarCelda(4, 0, "Fecha del proceso: " + Fecha, header3);
                        
            // subtitulos
            
            
            int fila = 6;
            int col  = 0;
            
            if( this.agencia.length()==0) 
                xls.adicionarCelda(fila, col++, "AGENCIA", header1);
            xls.adicionarCelda(fila, col++, "STICKER", header1);
            xls.adicionarCelda(fila, col++, "FECHA CREACION", header1);
            xls.adicionarCelda(fila, col++, "DIAS TRANSCURRIDOS", header1);
            
            
            fila++;
            
            for( int i=0; i<reporte.size(); i++){
                col = 0;
                
                Precinto prec = (Precinto) reporte.elementAt(i);
                                
                if( this.agencia.length()==0)
                    xls.adicionarCelda(fila, col++, prec.getAgencia(), texto);
                xls.adicionarCelda(fila, col++, prec.getPrecinto(), texto);
                xls.adicionarCelda(fila, col++, prec.getCreation_date().substring(0,19), texto);
                xls.adicionarCelda(fila, col++, com.tsp.util.Util.diasTranscurridos(prec.getCreation_date()), texto);
                
                fila++;
            }         
            
            
            
            xls.cerrarLibro();
            model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(), user, "PROCESO EXITOSO");
        }catch (Exception ex){
            ex.printStackTrace();
            ////System.out.println("ERROR AL GENERAR EL ARCHIVO XLS : " + ex.getMessage());
            try{
                model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(), user, "ERROR :" + ex.getMessage());
            }
            catch(Exception f){
                try{
                    model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(), user, "ERROR :");
                }catch(Exception p){    }
            }
        }
        
    }    
   
    
}