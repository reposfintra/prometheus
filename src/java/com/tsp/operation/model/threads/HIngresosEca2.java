/*
 * HIngresosEca.java
 * Created on 20 de mayo de 2009, 8:58
 */
package com.tsp.operation.model.threads;
import com.tsp.operation.model.Model;
import com.tsp.operation.model.beans.*;
import com.tsp.util.Util;
import com.tsp.util.Utility;
import java.util.*;
import java.text.*;
import com.tsp.operation.model.LogProcesosService;

/**
 * @author  Fintra
 */
public class HIngresosEca2 extends Thread{
    private Model model;
    private Usuario usuario;
    private String dstrct;
    
    public HIngresosEca2() {    }    
    
    public void start(Model model, Usuario usuario, String distrito){
        this.usuario = usuario;
        this.model = model;
        this.dstrct = distrito;
        super.start();
    }
    public synchronized void run(){
        ///
        LogProcesosService log = new LogProcesosService(usuario.getBd());

        ///
        try{
            
            String hoy  =  Utility.getHoy("-");
            String ayer =  Utility.convertirFecha( hoy, -1);

            String comentario="EXITOSO";
            log.InsertProceso("CruceEca", this.hashCode(), hoy ,usuario.getLogin()); 

            //.out.println("antes");
                       
            
            Vector comandos_sql =new Vector();            
            String comandoSQL = "";
            
            model.datosEcaService.obtainRegistrosArchivo2();            
            List registrosArchivo =  model.datosEcaService.getRegistrosArchivo();    
            //.out.println("registrosArchivo"+registrosArchivo.size());
            //.out.println("registrosArchivo.size"+registrosArchivo.size());
            for (int i=0;i<registrosArchivo.size();i++ ){
                String num_ingreso = "";
                List facturasEca=model.datosEcaService.obtainFacturasEca2((DatosEca)registrosArchivo.get(i));
                //.out.println("facturasEca"+facturasEca.size());
                /*if (facturasEca.size()>0){
                    num_ingreso =model.ingresoService.buscarSerie( "ING"+"C" );
                    //.out.println("num_ingreso"+num_ingreso+"i"+i);
                    Vector cruceFacIng=model.datosEcaService.cruzarFacIng((DatosEca)registrosArchivo.get(i),facturasEca,num_ingreso);                 
                    ////.out.println("antes de ejecutar sql");
                    model.applusService.ejecutarSQL(cruceFacIng);
                    ////.out.println("despues de sql");
                }*/
                
                for (int j=0;j<facturasEca.size();j++){
                    num_ingreso =model.ingresoService.buscarSerie( "ING"+"C" );
                    //.out.println("casi"+Double.parseDouble(((DatosEca)registrosArchivo.get(i)).getSaldo()));
                    if (  Double.parseDouble(((DatosEca)registrosArchivo.get(i)).getSaldo())>0){
                        Vector cruceFacIng=model.datosEcaService.cruzarFacIng2((DatosEca)registrosArchivo.get(i),facturasEca,num_ingreso,(facturasEca.size()-j-1),j);//penultimo parametro es 0 cuando es la ultima cxc de ese sv                                      
                        model.applusService.ejecutarSQL(cruceFacIng);
                    }
                }
                //comandos_sql.add(cruceFacIng);
                
            }           
            
            //comandoSQL =model.applusService.setFacturaContratista(null,null, null, null);
            //.out.println("fin de run");
               
            log.finallyProceso("CruceEca", this.hashCode(), usuario.getLogin(),comentario);
            
            // Grabando todo a la base de datos.
            //model.applusService.ejecutarSQL(comandos_sql);
        }catch (Exception ex){
            System.out.println("error en h:"+ex.toString()+"__"+ex.getMessage());            
            try{
                log.finallyProceso("CruceEca",this.hashCode(), usuario.getLogin(),"ERROR Hilo: " + ex.getMessage()); 
            }catch(Exception w){
                System.out.println("errorrinho"+w.toString());
            }
        }
    }
}

