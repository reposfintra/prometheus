//20100602
/* * HIngresosEcaPms3.java * 201006 */
package com.tsp.operation.model.threads;
import com.tsp.operation.model.Model;
import com.tsp.operation.model.beans.*;
import com.tsp.util.Util;
import com.tsp.util.Utility;
import java.util.*;
import java.text.*;
import com.tsp.operation.model.LogProcesosService;
import com.tsp.operation.model.services.DirectorioService;//20100608

import java.io.PrintWriter;//20100608
import java.io.BufferedWriter;//20100608
import java.io.FileWriter;//20100608

import com.tsp.util.LogWriter;

/** * @author  Fintra */
public class HIngresosEcaPms3 extends Thread{
    private Model model;
    private Usuario usuario;
    private String dstrct;

    public HIngresosEcaPms3() {    }

    public void start(Model model, Usuario usuario, String distrito){
        this.usuario = usuario;
        this.model = model;
        this.dstrct = distrito;
        super.start();
    }
    public synchronized void run(){
        LogProcesosService log = new LogProcesosService(usuario.getBd());

       try{
       ////////////////////inicio de log

            String       fecha = Util.getFechaActual_String(6).replaceAll("/|:| ","");
            System.out.println("fechita:"+fecha);
            DirectorioService  svc   =  new DirectorioService();
            svc.create( usuario.getLogin() );
            String             url   =  svc.getUrl() + usuario.getLogin() + "/Recaudo" +  fecha +".txt";
            System.out.println("url:"+url);

            PrintWriter pw = new PrintWriter( new BufferedWriter( new FileWriter(url) )  );

            try{
                    String hoy  =  Utility.getHoy("-");
                    String ayer =  Utility.convertirFecha( hoy, -1);
                    String comentario="EXITOSO";

                    LogWriter logM = new LogWriter("Recaudo de Eca",LogWriter.INFO, pw);
                    System.out.println("antes del log..");
                    logM.log("Inicio del recaudo....",logM.INFO);
                     ////////fin de log
                    System.out.println("despues del log..");
                    log.InsertProceso("CruceEcaPms3", this.hashCode(), "Cruces de los Recaudos de Eca con las CxCs de Clientes con prefijo PM. "+hoy ,usuario.getLogin());

                    Vector comandos_sql =new Vector();
                    String comandoSQL = "";

                    model.datosEcaService.obtainRegistrosArchivo3();
                    List registrosArchivo =  model.datosEcaService.getRegistrosArchivo3();
                    //.out.println("registrosArchivo"+registrosArchivo.size());
                    //.out.println("registrosArchivo.size"+registrosArchivo.size());
                    logM.log("cantidad de registros del archivo : "+registrosArchivo.size(),logM.INFO);
                    for (int i=0;i<registrosArchivo.size();i++ ){
                        String num_ingreso = "";
                        List facturasEca=model.datosEcaService.obtainFacturasEca3((DatosEca)registrosArchivo.get(i));
                        //.out.println("facturasEca"+facturasEca.size());
                        /*if (facturasEca.size()>0){
                            num_ingreso =model.ingresoService.buscarSerie( "ING"+"C" );
                            //.out.println("num_ingreso"+num_ingreso+"i"+i);
                            Vector cruceFacIng=model.datosEcaService.cruzarFacIng((DatosEca)registrosArchivo.get(i),facturasEca,num_ingreso);
                            ////.out.println("antes de ejecutar sql");
                            model.applusService.ejecutarSQL(cruceFacIng);
                            ////.out.println("despues de sql");
                        }*/
                        logM.log("cantidad de facturas para la fila "+(i+1)+" con cxc "+ ((DatosEca)registrosArchivo.get(i)).getCxcExcel()+" del archivo : "+facturasEca.size(),logM.INFO);
                        for (int j=0;j<facturasEca.size();j++){
                            num_ingreso =model.ingresoService.buscarSerie( "ICA"+"C" );
                            //.out.println("casi"+Double.parseDouble(((DatosEca)registrosArchivo.get(i)).getSaldo()));
                            if (  Double.parseDouble(((DatosEca)registrosArchivo.get(i)).getSaldo())>0){
                                logM.log("se va a cruzar "+Double.parseDouble(((DatosEca)registrosArchivo.get(i)).getSaldo()),logM.INFO);
                                Vector cruceFacIng=model.datosEcaService.cruzarFacIngPms3((DatosEca)registrosArchivo.get(i),facturasEca,num_ingreso,(facturasEca.size()-j-1),j,usuario.getLogin());//penultimo parametro es 0 cuando es la ultima cxc de ese sv
                                logM.log("se va a ejecutar el codigo en la base de datos. ",logM.INFO);
                                model.applusService.ejecutarSQL(cruceFacIng);
                                logM.log("se ejecuto el codigo en la base de datos. ",logM.INFO);
                            }
                        }
                        //comandos_sql.add(cruceFacIng);

                    }
                    if (registrosArchivo.size()>0){
                        logM.log("se van a generar REs. ",logM.INFO);//20100610
                        String generar_re=model.datosEcaService.generarRe();
                        logM.log("result : "+generar_re,logM.INFO);//20100610
                    }

                //comandoSQL =model.applusService.setFacturaContratista(null,null, null, null);
                //.out.println("fin de run");

                log.finallyProceso("CruceEcaPms3", this.hashCode(), usuario.getLogin(),comentario);

                // Grabando todo a la base de datos.
                //model.applusService.ejecutarSQL(comandos_sql);
            }catch (Exception ex){
                System.out.println("error en h_:"+ex.toString()+"__"+ex.getMessage());
                ex.printStackTrace();
                try{
                    log.finallyProceso("CruceEcaPms3",this.hashCode(), usuario.getLogin(),"ERROR Hilo_: " + ex.getMessage());
                }catch(Exception w){
                    System.out.println("errorrinho_"+w.toString());
                }
            }finally{
                pw.close();
            }
       }catch(Exception ee){
           System.out.println("error en el hilo del recaudo.."+ee.toString());
           ee.printStackTrace();
       }
    }
}
