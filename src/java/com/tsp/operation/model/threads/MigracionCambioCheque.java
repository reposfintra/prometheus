/*
 * MigracionCambioCheque.java
 *
 * Created on 20 de octubre de 2005, 05:07 PM
 */

package com.tsp.operation.model.threads;

import java.io.*;
import java.util.*;

import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.*;
import java.sql.SQLException;
/**
 *
 * @author  JuanM
 */
public class MigracionCambioCheque extends Thread{
    private String user;
    private Model model = new Model();
    private String path;
    private PrintStream archivoPlano;
    private String fecha;
    /** Creates a new instance of MigracionCambioCheque */
    public MigracionCambioCheque() {
    }
    
    public void start(String user){
        this.user = user;
        super.start();
    }
    
    public synchronized void run(){
        try{
            //Model model = new Model();
            model.LogProcesosSvc.InsertProceso("Migraci�n de Cambio de Cheques", this.hashCode(),"Migraci�n de Cambio de Cheques", this.user );
            String fecha = model.movplaService.obtenerFechaActualEnFormato("YYYYMMDD");
            List lista = model.ReporteEgresoSvc.LISTCHKNOMIGRADOS();/*obtenemos lista de chk no migrados*/
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            path = rb.getString("ruta") + "/exportar/migracion/"+user;
            //path = rb.getString("ruta") + "/exportar/migracion/JM";
            /*File file = new File(path);
            file.mkdirs();
             
            String nombreArch= "MIGRACIONCAMBIOCHEQUE["+fecha+"].xls";
            com.tsp.operation.model.beans.POIWrite xls = new com.tsp.operation.model.beans.POIWrite(path+"/"+nombreArch);
             
            HSSFCellStyle negrita      = xls.nuevoEstilo("arial", 10, true  , false, "text"        , HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle texto  = xls.nuevoEstilo("arial", 10, false , false, "text"        , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle numero = xls.nuevoEstilo("arial", 10, false , false, "###,##"            , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_RIGHT);
             
            xls.obtenerHoja("Chequesmigrados");*/
            
            /*Creamos titulos*/
           /* int fila = 0;
            int col  = 0;
            
            /*LLENADO DE CABECERAS*/
           /* xls.adicionarCelda(fila ,col++ , "NO. CORRIDA" , negrita );
            xls.cambiarAnchoColumna(0, 3500);
            xls.adicionarCelda(fila ,col++ , "SECU" , negrita );
            xls.adicionarCelda(fila ,col++ , "FIJO" , negrita );
            xls.adicionarCelda(fila ,col++ , "FIJO" , negrita );
            xls.adicionarCelda(fila ,col++ , "CODIGO CUENTA" , negrita );
            xls.cambiarAnchoColumna(4, 4500);
            xls.adicionarCelda(fila ,col++ , "TABLA" , negrita );
            xls.adicionarCelda(fila ,col++ , "SUBLEDGER" , negrita );
            xls.adicionarCelda(fila ,col++ , "PERIO" , negrita );
            xls.adicionarCelda(fila ,col++ , "VALOR" , negrita );
            xls.cambiarAnchoColumna(8, 4500);
            xls.adicionarCelda(fila ,col++ , "FIJO" , negrita );
            xls.adicionarCelda(fila ,col++ , "FIJO" , negrita );
            xls.adicionarCelda(fila ,col++ , "FIJO" , negrita );
            xls.adicionarCelda(fila ,col++ , "FECHA" , negrita );
            xls.adicionarCelda(fila ,col++ , "No.OT" , negrita );
            xls.adicionarCelda(fila ,col++ , "No. Proy" , negrita );
            xls.adicionarCelda(fila ,col++ , "No. Equipo" , negrita );
            xls.adicionarCelda(fila ,col++ , "NO.COMPRO" , negrita );
            xls.cambiarAnchoColumna(16, 4500);
            xls.adicionarCelda(fila ,col++ , "DESCRIPCION" , negrita );
            xls.cambiarAnchoColumna(17, 6000);
            xls.adicionarCelda(fila ,col++ , "Referencia" , negrita );
            xls.cambiarAnchoColumna(18, 4000);
            xls.adicionarCelda(fila ,col++ , "F" , negrita );
            xls.adicionarCelda(fila ,col++ , "FIJO" , negrita );
            xls.adicionarCelda(fila ,col++ , "FIJO" , negrita );
            xls.adicionarCelda(fila ,col++ , "FIJO" , negrita );
            xls.adicionarCelda(fila ,col++ , "F" , negrita );
            xls.adicionarCelda(fila ,col++ , "F" , negrita );
            xls.adicionarCelda(fila ,col++ , "F" , negrita );
            xls.adicionarCelda(fila ,col++ , "FIJO" , negrita );
            xls.adicionarCelda(fila ,col++ , "FI" , negrita );
            xls.adicionarCelda(fila ,col++ , "FI" , negrita );*/
            
            /*recorremos la lista*/
            
            String mesActual = model.movplaService.obtenerFechaActualEnFormato("MM");
            String a�oActual = model.movplaService.obtenerFechaActualEnFormato("YY");
            Serie serieCHAux = null;
            String agenciaAnterior = null;
            
            Serie serieCH = model.servicioSerie.obtenerSerie( "006" );
            
            String codigoContableAux = null;
            int contadorCH = 0;
            int agencias = 0;
            int c = 0;
            Iterator it = lista.iterator();
            //fila++;
            while(it.hasNext()){
                
                
                Cambio_cheque chk = (Cambio_cheque) it.next();
                Planilla p = model.planillaService.obtenerPlanillaPorNumero( chk.getPlanilla() );
                
                if ( p == null ){
                    continue;
                }
                
                if ( agenciaAnterior == null || !agenciaAnterior.equals(chk.getAgencia() ) ){
                    if ( agenciaAnterior != null ){
                        actualizarSerie(serieCH);
                        contadorCH = 0;
                    }
                    
                    // mfontalvo
                    // para que solo genere un solo archivo
                    if (agenciaAnterior == null)
                        crearArchivosParaAgencia(chk.getAgencia());
                    
                    agenciaAnterior = chk.getAgencia();
                    agencias++;
                    serieCHAux = serieCH;
                }
                
                String consecutivoCH =
                serieCH.getPrefix() +
                Util.llenarConCerosALaIzquierda( serieCH.getLast_number(), 3 ) +
                mesActual +
                a�oActual.charAt(1);
                
                
                String comprobanteCH =
                serieCH.getPrefix() +
                a�oActual +
                mesActual +
                Util.llenarConCerosALaIzquierda( serieCH.getLast_number(), 3 );
                
                
                codigoContableAux = model.servicioBanco.obtenerBanco( chk.getDstrct(),chk.getBranch_code(),chk.getBank_account() ).getCodigo_cuenta();
                String periodo = a�oActual + mesActual;
                /*cheque viejo signo + */
                double valorChk = chk.getValorchk();
                
                Hashtable filas = new Hashtable();
                
                filas.put( new Integer( 1 ), new CadenaConLongitud( consecutivoCH, 8 ) );
                filas.put( new Integer( 5 ), new CadenaConLongitud( "22010101", 24 ) );
                
                
                
                // busqueda del codigo contable
                
                codigoContableAux =
                model.servicioBanco.obtenerBanco(
                chk.getDstrct(),
                chk.getBranch_code(),
                chk.getBank_account()
                ).getCodigo_cuenta();
                
                
                filas.put( new Integer( 17 ), new CadenaConLongitud( comprobanteCH, 10 ) );
                
                filas.put( new Integer( 18 ),
                new CadenaConLongitud(
                chk.getPlanilla() +
                p.getPlaveh() +
                p.getCedcon()+"REEMPLAZO CHEQUE", 40 )
                );
                
                contadorCH++;
                c = contadorCH;
                
                if ( !filas.isEmpty() ) {
                    
                    filas.put( new Integer( 2 ),
                    new CadenaConLongitud( Util.llenarConCerosALaIzquierda( c, 4 ), 4 ) );
                    filas.put( new Integer( 3 ), new CadenaConLongitud( chk.getDstrct(), 4 ) );
                    filas.put( new Integer( 4 ), new CadenaConLongitud( "MJV", 3 ) );
                    // el valor 5 lo cambiamos al final
                    filas.put( new Integer( 6 ), new CadenaConLongitud( "", 2 ) );
                    filas.put( new Integer( 7 ), new CadenaConLongitud( "", 10 ) );
                    filas.put( new Integer( 8 ),
                    new CadenaConLongitud( a�oActual + mesActual, 4 ) ); // MMYY
                    // el valor 9 lo cambiamos al final
                    filas.put( new Integer( 9 ),
                    new CadenaConLongitud( String.valueOf( chk.getValorchk() ), 17 ) );
                    filas.put( new Integer( 10 ), new CadenaConLongitud( "", 17 ) );
                    filas.put( new Integer( 11 ), new CadenaConLongitud( "", 17 ) );
                    filas.put( new Integer( 12 ), new CadenaConLongitud( "", 4 ) );
                    filas.put( new Integer( 13 ), new CadenaConLongitud( fecha, 8 ) );
                    filas.put( new Integer( 14 ), new CadenaConLongitud( "", 8 ) );
                    // el valor 15 lo cambiamos al final
                    filas.put( new Integer( 15 ), new CadenaConLongitud( "", 8 ) );
                    filas.put( new Integer( 16 ), new CadenaConLongitud( "", 12 ) );
                    // el valor 17 y 18 fue calculado arriba
                    filas.put( new Integer( 19 ), new CadenaConLongitud( chk.getDocument_no(), 8 ) );
                    filas.put( new Integer( 20 ), new CadenaConLongitud( "Y", 1 ) );
                    filas.put( new Integer( 21 ), new CadenaConLongitud( "", 17 ) );
                    filas.put( new Integer( 22 ), new CadenaConLongitud( "7777777", 10 ) );
                    filas.put( new Integer( 23 ), new CadenaConLongitud( "", 8 ) );
                    filas.put( new Integer( 24 ), new CadenaConLongitud( "N", 1 ) );
                    filas.put( new Integer( 25 ), new CadenaConLongitud( "N", 1 ) );
                    filas.put( new Integer( 26 ), new CadenaConLongitud( "N", 1 ) );
                    filas.put( new Integer( 27 ), new CadenaConLongitud( "", 64 ) );
                    filas.put( new Integer( 28 ), new CadenaConLongitud( "", 2 ) );
                    filas.put( new Integer( 29 ), new CadenaConLongitud( "", 3 ) );
                    
                    guardarFilaEnArchivoPlano( archivoPlano, filas, "" );
                    
                    // hacemos los cambios para la fila credito
                    filas.put( new Integer( 2 ), new CadenaConLongitud( Util.llenarConCerosALaIzquierda( ++c, 4 ), 4 ) );
                    filas.put( new Integer( 5 ), new CadenaConLongitud( codigoContableAux, 24 ) );
                    filas.put( new Integer( 9 ), new CadenaConLongitud( String.valueOf( -chk.getValorchk() ), 17 ) );
                    filas.put( new Integer( 15 ),new CadenaConLongitud( "", 8 ) );
                    filas.put( new Integer( 18 ),
                    new CadenaConLongitud(
                    chk.getPlanilla() +
                    p.getPlaveh() +
                    p.getCedcon()+"REEMPLAZO CHEQUE", 40 )
                    );
                    filas.put( new Integer( 19 ), new CadenaConLongitud( chk.getDocument_no(), 8 ) );
                    
                    guardarFilaEnArchivoPlano( archivoPlano, filas, "" );
                    
                    
                    filas.put( new Integer( 2 ), new CadenaConLongitud( Util.llenarConCerosALaIzquierda( ++c, 4 ), 4 ) );
                    filas.put( new Integer( 5 ), new CadenaConLongitud( "22010101", 24 ) );
                    filas.put( new Integer( 9 ), new CadenaConLongitud( String.valueOf( -chk.getValorchk() ), 17 ) );
                    filas.put( new Integer( 15 ),new CadenaConLongitud( "", 8 ) );
                    filas.put( new Integer( 18 ),
                    new CadenaConLongitud(
                    chk.getPlanilla() +
                    p.getPlaveh() +
                    p.getCedcon()+"CAMBIO CHEQUE", 40 )
                    );
                    filas.put( new Integer( 19 ), new CadenaConLongitud( chk.getNew_document_no(), 8 ) );
                    
                    guardarFilaEnArchivoPlano( archivoPlano, filas, "" );
                    
                    
                    filas.put( new Integer( 2 ), new CadenaConLongitud( Util.llenarConCerosALaIzquierda( ++c, 4 ), 4 ) );
                    filas.put( new Integer( 5 ), new CadenaConLongitud( codigoContableAux, 24 ) );
                    filas.put( new Integer( 9 ), new CadenaConLongitud( String.valueOf( chk.getValorchk() ), 17 ) );
                    filas.put( new Integer( 15 ),new CadenaConLongitud( "", 8 ) );
                    filas.put( new Integer( 18 ),
                    new CadenaConLongitud(
                    chk.getPlanilla() +
                    p.getPlaveh() +
                    p.getCedcon()+"CAMBIO CHEQUE", 40 )
                    );
                    filas.put( new Integer( 19 ), new CadenaConLongitud( chk.getNew_document_no(), 8 ) );
                    
                    
                    guardarFilaEnArchivoPlano( archivoPlano, filas, "" );
                    
                    //todos.add(mp);
                    contadorCH++;
                    
                    //model.movplaService.actualizarMovplaMigrado(mp);
                    
                }
                
                //  CrearFilaXLS(fila++,0,xls,chk.getDocument_no(),valorChk,"CAMBIO DE CHEQUE",comprobanteCH,codigoContableAux,chk.getDstrct(),texto,numero,periodo,fecha);
                
                /*cheque viejo signo -*/
              /*  valorChk = valorChk *-1;
                CrearFilaXLS(fila++,0,xls,chk.getDocument_no(),valorChk,"CAMBIO DE CHEQUE",comprobanteCH,codigoContableAux,chk.getDstrct(),texto,numero,periodo,fecha);
               
                /*cheque nuevo signo +*/
                //  CrearFilaXLS(fila++,0,xls,chk.getDocument_no(),valorChk,"REEMPLAZO",comprobanteCH,codigoContableAux,chk.getDstrct(),texto,numero,periodo,fecha);
                
                /*cheque nuevo signo -*/
             /*   valorChk = valorChk *-1;
                CrearFilaXLS(fila++,0,xls,chk.getDocument_no(),valorChk,"REEMPLAZO",comprobanteCH,codigoContableAux,chk.getDstrct(),texto,numero,periodo,fecha);
              
                //Actualiza la fecha de migracion y el usuario de migracion*/
                model.ReporteEgresoSvc.UPDATEMIGRACIONCHK(user, chk.getDocument_no());
                
            } //FIN ITERATOR
            
            if ( agencias != 0 ){
                archivoPlano.flush();
                archivoPlano.close();
              /*  wb.write( fo );
                fo.close();*/
                actualizarSerie(serieCHAux);
            }
            
            //xls.cerrarLibro();
            ////System.out.println("Termina proceso");
            try{
                model.LogProcesosSvc.finallyProceso("Migraci�n de Cambio de Cheques", this.hashCode(), this.user, "Proceso Finalizado");
            }catch(SQLException ex3){
                ////System.out.println(ex3.getMessage());
            }
            
        }catch(Exception e){
            try{
                model.LogProcesosSvc.finallyProceso("Migraci�n de Cambio de Cheques", this.hashCode(), this.user, "Error : " + e.getMessage() );
            }catch(SQLException ex){
                ////System.out.println(ex.getMessage());
            }
            ////System.out.println(e.getMessage());
        }finally{
            ////System.out.println("Entra Finally");
            try{
                model.LogProcesosSvc.finallyProceso("Migraci�n de Cambio de Cheques", this.hashCode(), this.user, "Proceso Finalizado");
            }catch(SQLException ex2){
                ////System.out.println(ex2.getMessage());
            }
            super.destroy();
        }
    }
    
    /*Llena una nueva filas en con lo datos correspondientes excel*/
    public void CrearFilaXLS(int fila, int col, com.tsp.operation.model.beans.POIWrite xls, String chk, double valor, String descripcion, String comprobanteCH, String codigoContableAux, String dstrct, HSSFCellStyle texto, HSSFCellStyle numero, String periodo, String fecha){
        try{
            xls.adicionarCelda(fila ,col++ , comprobanteCH , texto ); /*No corrida*/
            xls.adicionarCelda(fila ,col++ , "XX" , texto ); /*Secu*/
            xls.adicionarCelda(fila ,col++ , dstrct , texto ); /*Distrito*/
            xls.adicionarCelda(fila ,col++ , "MJV" , texto ); /*MJV*/
            
            xls.adicionarCelda(fila ,col++ , codigoContableAux , texto ); /*CODIGO CUENTA*/
            xls.adicionarCelda(fila ,col++ , "" , texto ); /*TABLA*/
            xls.adicionarCelda(fila ,col++ , "" , texto ); /*SUBLEDGER*/
            xls.adicionarCelda(fila ,col++ , periodo , texto ); /*PERIO*/
            
            /*VALOR CHK*/
            xls.adicionarCelda(fila ,col++ , valor , numero ); /*VALOR*/
            
            xls.adicionarCelda(fila ,col++ , "" , texto ); /*FIJO*/
            xls.adicionarCelda(fila ,col++ , "" , texto ); /*FIJO*/
            xls.adicionarCelda(fila ,col++ , "" , texto ); /*FIJO*/
            
            xls.adicionarCelda(fila ,col++ , fecha , texto ); /*FECHA*/
            xls.adicionarCelda(fila ,col++ , "" , texto ); /*No OT*/
            xls.adicionarCelda(fila ,col++ , "" , texto ); /*No proy*/
            xls.adicionarCelda(fila ,col++ , "" , texto ); /*No Equipo*/
            xls.adicionarCelda(fila ,col++ , comprobanteCH , texto ); /*No Comprobante*/
            
            /*DESCRIPCION*/
            xls.adicionarCelda(fila ,col++ , descripcion , texto ); /*Descripcion*/
            
            xls.adicionarCelda(fila ,col++ , chk , texto ); /*referencia o chk*/
            xls.adicionarCelda(fila ,col++ , "Y" , texto ); /*referencia o chk*/
            xls.adicionarCelda(fila ,col++ , "" , texto ); /*FIJO*/
            xls.adicionarCelda(fila ,col++ , "7777777" , texto ); /*FIJO*/
            xls.adicionarCelda(fila ,col++ , "" , texto ); /*FIJO*/
            xls.adicionarCelda(fila ,col++ , "N" , texto ); /*F*/
            xls.adicionarCelda(fila ,col++ , "N" , texto ); /*F*/
            xls.adicionarCelda(fila ,col++ , "N" , texto ); /*F*/
            xls.adicionarCelda(fila ,col++ , "" , texto ); /*FIJO*/
            xls.adicionarCelda(fila ,col++ , "" , texto ); /*FI*/
            xls.adicionarCelda(fila ,col++ , "" , texto ); /*FI*/
        }catch(Exception e){
            ////System.out.println(e.getMessage());
        }
    }
    
    private void actualizarSerie( Serie s ) throws SQLException {
        int num = Integer.parseInt(s.getLast_number())+1;
        s.setLast_number(String.valueOf(num));
        model.servicioSerie.actualizarSerie(s);
    }
    
    public void crearArchivosParaAgencia(String agencia) throws Exception {
        File f = new File(path);
        if ( !f.exists() ){
            f.mkdirs();
            ////System.out.println("directorios creados en: "+f.getAbsolutePath());
        }
        
        fecha = model.movplaService.obtenerFechaActualEnFormato("YYYYMMDD_HHMIss");
        
        /*
        // este bloque se reemplazo por el bloque de abajo
        // mfontalvo
         
        archivoPlano = new PrintStream( new FileOutputStream( ruta + "/Cheques" + agencia + fecha + ".txt", true ), true );
         
        wb = new HSSFWorkbook();
        sheet = wb.createSheet( "Cheques migrados agencia: "+agencia );
        fo = new FileOutputStream( ruta + "/Cheques" + agencia + fecha + ".xls" );
         */
        
        
        
        ////////////////////////////////////////////////////////////////////////
        // mfontalvo
        // Noviembre 28 2005
        // modificacion para  generar solo un archivo de agencias
        archivoPlano = new PrintStream( new FileOutputStream( path + "/Cheques" + fecha + ".txt", true ), true );
        
        /*wb = new HSSFWorkbook();
        sheet = wb.createSheet( "Cheques migrados agencia: " + agencia );
        fo = new FileOutputStream( path + "/Cheques" + fecha + ".xls" );
         
        fueInicializadoElArchivoExcel = false;
        fecha = model.movplaService.obtenerFechaActualEnFormato("YYYYMMDD");*/
        /////////////////////////////////////////////////////////////////////////
        
        
    }
    
    private void guardarFilaEnArchivoPlano( PrintStream archivo, Hashtable fila, String separador ) {
        StringBuffer linea = new StringBuffer();
        int i;
        for ( i = 1; i <= fila.size(); i++ ) {
            CadenaConLongitud str = ( CadenaConLongitud ) fila.get( new Integer( i ) );
            if ( str == null ){
                break;
            }
            //////System.out.println("I " + i + " CONTENIDO " + str.getStr());
            linea.append( Util.llenarConEspaciosAlFinal( str.getStr(), str.getLongitud() ) );
            linea.append(separador);
        }
        if ( separador.length() > 0 ){
            linea.deleteCharAt(linea.length()-separador.length());
        }
        archivo.println( linea.toString() );
    }
    
    public static void main(String a []){
        MigracionCambioCheque hilo = new MigracionCambioCheque();
        hilo.start("juan");
    }
    
}
