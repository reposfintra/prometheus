/************************************************************************
 *      Nombre Clase.................   ReporteFacturasProveedorTh.java
 *      Descripci�n..................   Hilo para la escritura del xls
 *      Autor........................   Ing. Andr�s Maturana De La Cruz
 *      Fecha........................   25.10.2006
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 ************************************************************************/

package com.tsp.operation.model.threads;


import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import java.io.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.operation.model.*;
import java.util.*;
import java.text.*;
//import com.tsp.operation.model.threads.POIWrite;

import org.apache.log4j.*;



public class ReporteCarteraTh extends Thread{
    
    Logger logger = Logger.getLogger(this.getClass());
    
    private Vector reporte;
    private String user;
    private Model model;
    private String fecharep;
    
    //Log Procesos
    private String procesoName;
    private String des;
    
    public void start(Model modelo, Vector reporte, String user,  String fecharep){
        this.reporte = reporte;
        this.user = user;
        this.model = modelo;
        this.fecharep = fecharep;
        
        this.procesoName = "Reporte Cartera";
        this.des = "An�lisis de Vencimiento de Cartera";
        
        super.start();
    }
    
    public synchronized void run(){
        try{
            //INSERTO EN EL LOG DE PROCESO
            model.LogProcesosSvc.InsertProceso(this.procesoName, this.hashCode(), this.des, user);
            
            logger.info("FILAS DEL REPORTE: " + this.reporte.size());
            
            //Fecha del sistema
            Calendar FechaHoy = Calendar.getInstance();
            Date d = FechaHoy.getTime();
            SimpleDateFormat s = new SimpleDateFormat("yyyyMMdd_kkmm");
            SimpleDateFormat s1 = new SimpleDateFormat("yyyy-MM-dd-hh:mm");
            SimpleDateFormat fec = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss");
            SimpleDateFormat time = new SimpleDateFormat("hh:mm");
            String FechaFormated = s.format(d);
            String FechaFormated1 = s1.format(d);
            String hora = time.format(d);
            String Fecha = fec.format(d);
            
            
            //obtener cabecera de ruta
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String path = rb.getString("ruta");
            //armas la ruta
            String Ruta1  = path + "/exportar/migracion/" + user + "/";
            //crear la ruta
            File file = new File(Ruta1);
            file.mkdirs();
            
            POIWrite xls = new POIWrite(Ruta1 +"ReporteCartera[" + FechaFormated + "].xls", user, Fecha);
            String nom_archivo = "ReporteCarteraGral_" + FechaFormated + ".xls";
            
            //Definici�n de Estilos
            HSSFCellStyle fecha  = xls.nuevoEstilo("Arial", 9, false , false, "yyyy/mm/dd"  , xls.NONE , xls.NONE , xls.NONE );
            HSSFCellStyle texto  = xls.nuevoEstilo("Arial", 9, false , false, "text"        , xls.NONE , xls.NONE , xls.NONE);
            HSSFCellStyle numero = xls.nuevoEstilo("Arial", 9, false , false, ""            , xls.NONE , xls.NONE , xls.NONE);
            
            HSSFCellStyle header1      = xls.nuevoEstilo("Arial", 12, true , false, "text"        , HSSFColor.WHITE.index , HSSFColor.GREEN.index , HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle header2      = xls.nuevoEstilo("Arial", 18, true , false, "text"        , HSSFColor.WHITE.index , HSSFColor.ORANGE.index , HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle header3      = xls.nuevoEstilo("Arial", 12, true , false, "text"        , HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
            HSSFCellStyle texto2      = xls.nuevoEstilo("Arial", 12, false , false, "text"        , HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
            HSSFCellStyle titulo      = xls.nuevoEstilo("Arial", 12, true  , false, "text"        , HSSFColor.BLACK.index , HSSFColor.PINK.index , HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle fechatitle  = xls.nuevoEstilo("Arial", 12, true  , false, "yyyy/mm/dd"  , HSSFColor.RED.index , xls.NONE, HSSFCellStyle.ALIGN_CENTER );
            HSSFCellStyle entero      = xls.nuevoEstilo("Arial", 12, false , false, "text"        , HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle flotante      = xls.nuevoEstilo("Arial", 12, false , false, "text"        , HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_RIGHT);
            HSSFCellStyle moneda  = xls.nuevoEstilo("Arial", 9, false , false, "#,##0.00", xls.NONE , xls.NONE , xls.NONE);
            
            xls.obtenerHoja("REPORTE DE CARTERA");
            xls.cambiarMagnificacion(3,4);
            
            // cabecera
            
            // cabecera
            
            xls.combinarCeldas(0, 0, 0, 15);
            xls.combinarCeldas(3, 0, 3, 15);
            xls.combinarCeldas(4, 0, 4, 15);
            xls.adicionarCelda(0, 0, "REPORTE DE CARTERA", header2);
            xls.adicionarCelda(3, 0, "Fecha del proceso: " + Fecha, header3);
            
            
            // subtitulos
            
            
            int fila = 5;
            int col  = 0;
            
            xls.adicionarCelda(fila, col++, "AGENCIA DUE�A", header1);
            xls.adicionarCelda(fila, col++, "AGENCIA COBRO", header1);
            xls.adicionarCelda(fila, col++, "AGENCIA FACT.", header1);
            xls.adicionarCelda(fila, col++, "CODIGO", header1);
            xls.adicionarCelda(fila, col++, "CLIENTE", header1);
            xls.adicionarCelda(fila, col++, "FACTURA", header1);
            xls.adicionarCelda(fila, col++, "FECHA", header1);
            xls.adicionarCelda(fila, col++, "VENCE", header1);
            xls.adicionarCelda(fila, col++, "MONEDA", header1);
            xls.adicionarCelda(fila, col++, "NO VENCIDA", header1);            
            xls.adicionarCelda(fila, col++, "VENCIDA 1 " + fecharep + "", header1);
            xls.adicionarCelda(fila, col++, "VENCIDA 2 " + com.tsp.util.Util.addFecha(fecharep, -15, 2) + " ", header1);
            xls.adicionarCelda(fila, col++, "VENCIDA 3 " + com.tsp.util.Util.addFecha(fecharep, -30, 2) + "", header1);
            xls.adicionarCelda(fila, col++, "VENCIDA 4 " + com.tsp.util.Util.addFecha(fecharep, -45, 2) + "", header1);
            xls.adicionarCelda(fila, col++, "VENCIDA 5 " + com.tsp.util.Util.addFecha(fecharep, -60, 2) + "", header1);
            xls.adicionarCelda(fila, col++, "VENCIDA 6 " + com.tsp.util.Util.addFecha(fecharep, -90, 2) + "", header1);
            
            
            fila++;
            col = 0;
            
            xls.adicionarCelda(fila, col++, "", header1);
            xls.adicionarCelda(fila, col++, "", header1);
            xls.adicionarCelda(fila, col++, "", header1);
            xls.adicionarCelda(fila, col++, "", header1);
            xls.adicionarCelda(fila, col++, "", header1);
            xls.adicionarCelda(fila, col++, "", header1);
            xls.adicionarCelda(fila, col++, "", header1);
            xls.adicionarCelda(fila, col++, "", header1);
            xls.adicionarCelda(fila, col++, "", header1);
            xls.adicionarCelda(fila, col++, "", header1);    
            xls.adicionarCelda(fila, col++, "1 - 15 D�as", header1);
            xls.adicionarCelda(fila, col++, "15 - 30 D�as", header1);
            xls.adicionarCelda(fila, col++, "30 - 45 D�as", header1);
            xls.adicionarCelda(fila, col++, "45 - 60 D�as", header1);
            xls.adicionarCelda(fila, col++, "60 - 90 D�as", header1);
            xls.adicionarCelda(fila, col++, "M�s de 90 D�as", header1);    
            
            
            fila++;
            
            double total = 0;
            
            Vector vec = this.reporte;
            
            for (int i = 0; i < vec.size(); i++){
                col = 0;
                
                RepGral obj = (RepGral) vec.elementAt(i);
                
                xls.adicionarCelda(fila, col++, obj.getAgc_duenia(), texto);
                xls.adicionarCelda(fila, col++, obj.getAgc_cobro(), texto);
                xls.adicionarCelda(fila, col++, obj.getAgcfacturacion(), texto);
                xls.adicionarCelda(fila, col++, obj.getCodcli(), texto);
                xls.adicionarCelda(fila, col++, obj.getNomcli(), texto);
                xls.adicionarCelda(fila, col++, obj.getFactura(), texto);
                xls.adicionarCelda(fila, col++, obj.getFecha_fra(), texto);
                xls.adicionarCelda(fila, col++, obj.getFecha_venc_fra(), texto);
                xls.adicionarCelda(fila, col++, obj.getMoneda(), texto);
                xls.adicionarCelda(fila, col++, Double.valueOf(obj.getNo_vencida()).doubleValue(), moneda);
                xls.adicionarCelda(fila, col++, Double.valueOf(obj.getVencida_1()).doubleValue(), moneda);
                xls.adicionarCelda(fila, col++, Double.valueOf(obj.getVencida_2()).doubleValue(), moneda);
                xls.adicionarCelda(fila, col++, Double.valueOf(obj.getVencida_3()).doubleValue(), moneda);
                xls.adicionarCelda(fila, col++, Double.valueOf(obj.getVencida_4()).doubleValue(), moneda);
                xls.adicionarCelda(fila, col++, Double.valueOf(obj.getVencida_5()).doubleValue(), moneda);
                xls.adicionarCelda(fila, col++, Double.valueOf(obj.getVencida_6()).doubleValue(), moneda);
                
                fila++;
            }
            
            xls.cerrarLibro();
            model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(), user, "PROCESO EXITOSO");
        }catch (Exception ex){
            ex.printStackTrace();
            ////System.out.println("ERROR AL GENERAR EL ARCHIVO XLS : " + ex.getMessage());
            try{
                model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(), user, "ERROR :" + ex.getMessage());
            }
            catch(Exception f){
                try{
                    model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(), user, "ERROR :");
                }catch(Exception p){    }
            }
        }
        
    }
    
    
}