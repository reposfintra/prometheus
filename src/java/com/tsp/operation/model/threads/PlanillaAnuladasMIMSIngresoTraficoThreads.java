/*
 * PlanillaAnuladasMIMSIngresoTraficoThreads.java
 *
 * Created on 9 de agosto de 2006, 09:48 AM
 */

package com.tsp.operation.model.threads;


import java.text.*;
import java.util.Date;
import java.text.*;
import java.sql.SQLException;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.util.*;
import java.util.*;
import java.io.*;

public class PlanillaAnuladasMIMSIngresoTraficoThreads extends Thread{
    private String procesoName;
    private String des;
    String id = "";
    String dstrct = "";
    Model model;
    
    /** Creates a new instance of PlanillaAnuladasMIMSIngresoTraficoThreads */
    public PlanillaAnuladasMIMSIngresoTraficoThreads ( Model model ) {
        this.model = model;
    }
    
    public void start(String id, String dstrct){        
        this.procesoName = "Reporte de Planillas Anuladas en MIMS que se encuentren en Ingreso Trafico";
        this.des = "Reporte de Planillas Anuladas en MIMS que se encuentren en Ingreso Trafico";
        this.id = id;
        this.dstrct = dstrct;
        super.start();
    }
    
    public synchronized void run(){
        try{
            String fecha_actual = Util.getFechaActual_String(6);
            
            //registramos el proeso en el log de procesos
            model.LogProcesosSvc.InsertProceso(this.procesoName, this.hashCode(), des, this.id);
            
            //obtenemos la lista de facturas creadas segun parametros definidos
            Vector lista = model.rmtService.planillaAnuladasMimsIngresoTrafico ();
            if (lista!=null && lista.size()>0){
                Usuario us = (Usuario) model.usuarioService.obtenerUsuario(id);
                ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
                String path = rb.getString("ruta") + "/exportar/migracion/"+id;
                
                File file = new File(path);
                file.mkdirs();
                
                String a�o = fecha_actual.substring(0,4);
                String mes = fecha_actual.substring(5,7);
                String dia = fecha_actual.substring(8,10);
                String hora = fecha_actual.substring(11,13);
                String min = fecha_actual.substring(14,16);
                String seg = fecha_actual.substring(17,19);
                
                com.tsp.operation.model.beans.POIWrite xls = new com.tsp.operation.model.beans.POIWrite(path+"/PLANILLAS_ANULADAS_MIMS_TRAFICO" +a�o + "-" + mes + "-" + dia + " " + hora + ":" + min +".xls");
                
                // Definicion de estilos: fuente, tama�o, negrita, cursiva, formato, color, fondo, alineado                                           
                HSSFCellStyle fecha = xls.nuevoEstilo(      "verdana", 12, false , false, "yyyy/mm/dd", HSSFColor.BLACK.index, xls.NONE, HSSFCellStyle.ALIGN_RIGHT );
                HSSFCellStyle texto = xls.nuevoEstilo(      "verdana", 12, false, false, "text", HSSFColor.BLACK.index, xls.NONE, HSSFCellStyle.ALIGN_LEFT);
                HSSFCellStyle numero = xls.nuevoEstilo(     "verdana", 12, false, false, "", xls.NONE, xls.NONE, HSSFCellStyle.ALIGN_RIGHT);
                HSSFCellStyle negrita = xls.nuevoEstilo(    "verdana", 12, true, false, "text", HSSFColor.BLACK.index, xls.NONE, xls.NONE);
                HSSFCellStyle header = xls.nuevoEstilo(     "verdana", 18, true, false, "text", HSSFColor.LIME.index,xls.NONE, HSSFCellStyle.ALIGN_LEFT);
                HSSFCellStyle titulo = xls.nuevoEstilo(     "verdana", 16, true, false, "text", HSSFColor.INDIGO.index, xls.NONE, HSSFCellStyle.ALIGN_LEFT);
                HSSFCellStyle subtitulo = xls.nuevoEstilo(  "verdana", 14, true, false, "text", HSSFColor.INDIGO.index, xls.NONE, HSSFCellStyle.ALIGN_LEFT);
                HSSFCellStyle cabecera = xls.nuevoEstilo(   "verdana", 12, true, false, "text", HSSFColor.WHITE.index, HSSFColor.INDIGO.index, HSSFCellStyle.ALIGN_CENTER );
                
                
                xls.obtenerHoja("BASE");
                //formateo al abrir hoja (tamano)
                xls.cambiarMagnificacion(3,4);
                
                //Titulo (cabecera)
                xls.adicionarCelda(0, 0, "TRANSPORTES SANCHEZ POLO S.A", header);
                xls.combinarCeldas(0, 0, 0, 5);  
                xls.adicionarCelda(2, 0, "REPORTE DE PLANILLAS ANULADAS EN MIMS QUE SE ENCUENTRA EN TRAFICO", titulo);
                xls.combinarCeldas(2, 0, 0, 10);
                
                xls.adicionarCelda(3, 0, "Elaborado Por:", subtitulo);
                xls.adicionarCelda(3, 1, id, negrita);
                
                int fila = 5;
                int col  = 0;
                //definicion de titulos de la tabla
                xls.adicionarCelda(fila ,col++ , "Planilla"                                , cabecera );
                //definicion de la tabla con los datos obtenidos de la consulta
                Iterator it = lista.iterator();
                while(it.hasNext()){
                    fila++;
                    col = 0;
                    String planilla = (String) it.next();                    
                    xls.adicionarCelda(fila ,col++ , planilla             , texto );
                }
                
                xls.cerrarLibro();
                model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(), this.id, "Proceso Exitoso!");
            }else {
                model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(), this.id, "No se encontraron Planillas En MIMS...");
            }
            model.cxpDocService.setEnproceso();
        }
        catch(Exception e){
            try{
                model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(),this.id,"ERROR :" + e.getMessage());
                model.cxpDocService.setEnproceso();
            }
            catch(Exception f){
                try{
                    model.LogProcesosSvc.finallyProceso(this.procesoName,this.hashCode(),this.id,"ERROR :");
                }catch(Exception p){    }
            }
        }
    }
}
