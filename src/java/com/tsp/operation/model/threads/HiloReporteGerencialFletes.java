
package com.tsp.operation.model.threads;

import java.util.*;
import java.io.*;
import java.lang.*;
import java.sql.*;
import java.text.*;
import java.sql.SQLException;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.beans.POIWrite;
import com.tsp.exceptions.*;
import com.tsp.operation.controller.*;
import com.tsp.util.*;
import com.tsp.operation.model.*;
import com.tsp.util.Util;
import javax.servlet.*;
import javax.servlet.http.*;

import com.tsp.util.UtilFinanzas.*;

public class HiloReporteGerencialFletes extends Thread {
        
    private String to;
    private Vector datos;
    private String path;
    private Usuario usuario;
    private String m = "";//
    private String procesoName;
    private String des;
    private AjusteFlete aj;
    private String fechai;
    private String fechaf;
    private Model model = new Model();
    private String val = "";
    private String dstrc = "";
    private double  valor_tasa = 0;
    private double  valor_cambio = 0;
    private String moneda_usuario;
    private double var = 0;
     
    
    public HiloReporteGerencialFletes() { }
    
    public void start( String fechai, String fechaf, Usuario usu, String moneda_usuario, String dstrc) {
      
        this.to = "";
        this.fechai = fechai;
        this.fechaf = fechaf;
        this.procesoName = "Reporte Gerencia Fletes";
        this.des = "Reporte Gerencia Fletes";
        this.usuario = usu;  
        this.moneda_usuario = moneda_usuario;
        this.dstrc = dstrc;
        super.start();
        
    }
    
    public synchronized void run(){
        
        try{
                        
            
            //Inicia el proceso
            model.LogProcesosSvc.InsertProceso( procesoName, this.hashCode(), des, usuario.getLogin() );
            
            //FORMATO DATE
            SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            
            Util u = new Util();
            
            ResourceBundle rb = ResourceBundle.getBundle( "com/tsp/util/connectionpool/db" );
            String path = rb.getString( "ruta" );
                        
            File file = new File( path + "/exportar/migracion/" + usuario.getLogin() );
            file.mkdirs();
               
            String fecha = Util.getFechaActual_String(6);            
            fecha=fecha.replaceAll( "/", "-" );
            fecha=fecha.replaceAll( ":", "_" );
            
            datos = model.reporteGeneralService.ajustesFletes(fechai, fechaf, dstrc);
            
            String nombreArch  = "ReporteFletes[" + fecha + "].xls";
            String       Hoja  = "ReporteFletes";
            String       Ruta  = path + "/exportar/migracion/" + usuario.getLogin() + "/" +nombreArch; 
            
            HSSFWorkbook wb    = new HSSFWorkbook();
            HSSFSheet    sheet = wb.createSheet( Hoja );
            HSSFRow      row   = null;
            HSSFRow      row2  = null;
            HSSFCell     cell  = null;
            
            for ( int col = 0; col < 37 ; col++ ){ //COLUMNAS
                
                if(col == 5 || col == 15){
                     sheet.setColumnWidth( (short) col, (short) ( ( 50 * 8 ) / ( (double) 1 / 35 ) ) ); 
                }else{
                    sheet.setColumnWidth( (short) col, (short) ( ( 50 * 8 ) / ( (double) 1 / 350 ) ) ); 
                }
                
                
            }
            
            /****  ENCABEZADO Y DEFINICION DE ESTILOS ************************************************/
            
            /** ENCABEZADO GENERAL *******************************/
            HSSFFont  fuente1 = wb.createFont();
            fuente1.setFontName("verdana");
            fuente1.setFontHeightInPoints((short)(16)) ;
            fuente1.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            fuente1.setColor(HSSFColor.DARK_RED.index);
            
            HSSFCellStyle estilo1 = wb.createCellStyle();
            estilo1.setFont(fuente1);
            estilo1.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo1.setBottomBorderColor(HSSFColor.WHITE.index);
            estilo1.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo1.setLeftBorderColor  (HSSFColor.WHITE.index);
            estilo1.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo1.setRightBorderColor(HSSFColor.WHITE.index);
            estilo1.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo1.setTopBorderColor   (HSSFColor.WHITE.index);
            
            
            /** SUBTITULO *******************************/
            HSSFFont  fuenteX = wb.createFont();
            fuenteX.setFontName("verdana");
            fuenteX.setFontHeightInPoints((short)(11)) ;
            fuenteX.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            fuenteX.setColor(HSSFColor.DARK_RED.index);
            
            HSSFCellStyle estiloX = wb.createCellStyle();
            estiloX.setFont(fuenteX);
            estiloX.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estiloX.setBottomBorderColor(HSSFColor.WHITE.index);
            estiloX.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estiloX.setLeftBorderColor  (HSSFColor.WHITE.index);
            estiloX.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estiloX.setRightBorderColor(HSSFColor.WHITE.index);
            estiloX.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estiloX.setTopBorderColor   (HSSFColor.WHITE.index);
            
            /** TEXTO EN EL ENCABEAZADO *************************/
            HSSFFont  fuente2 = wb.createFont();
            fuente2.setFontName("verdana");
            fuente2.setFontHeightInPoints((short)(11)) ;
            fuente2.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            fuente2.setColor((short)(0x0));
            
            HSSFCellStyle estilo2 = wb.createCellStyle();
            estilo2.setFont(fuente2);
            estilo2.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo2.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            
            /** ENCABEZADO DE LAS COLUMNAS***********************/
            HSSFFont  fuente3 = wb.createFont();
            fuente3.setFontName("verdana");
            fuente3.setFontHeightInPoints((short)(9)) ;
            fuente3.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            fuente3.setColor(HSSFColor.BLACK.index);
            
            HSSFCellStyle estilo3 = wb.createCellStyle();
            estilo3.setFont(fuente3);
            estilo3.setFillForegroundColor(HSSFColor.GREY_25_PERCENT.index);
            estilo3.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo3.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo3.setBottomBorderColor(HSSFColor.BLACK.index);
            estilo3.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo3.setLeftBorderColor  (HSSFColor.BLACK.index);
            estilo3.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo3.setRightBorderColor(HSSFColor.BLACK.index);
            estilo3.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo3.setTopBorderColor   (HSSFColor.BLACK.index);
            estilo3.setAlignment(HSSFCellStyle.ALIGN_CENTER);            
                        
            /** TEXTO NORMAL ************************************/
            HSSFFont  fuente4 = wb.createFont();
            fuente4.setFontName("verdana");
            fuente4.setFontHeightInPoints((short)(9)) ;
            fuente4.setBoldweight(HSSFFont.BOLDWEIGHT_NORMAL);
            fuente4.setColor((short)(0x0));
            
            HSSFCellStyle estilo4 = wb.createCellStyle();
            estilo4.setFont(fuente4);
            estilo4.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo4.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo4.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo4.setBottomBorderColor(HSSFColor.BLACK.index);
            estilo4.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo4.setLeftBorderColor  (HSSFColor.BLACK.index);
            estilo4.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo4.setRightBorderColor(HSSFColor.BLACK.index);
            estilo4.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo4.setTopBorderColor   (HSSFColor.BLACK.index);
            estilo4.setAlignment(HSSFCellStyle.ALIGN_CENTER);
            
            /** NUMEROS ************************************/
            HSSFCellStyle estilo5 = wb.createCellStyle();
            estilo5.setFont(fuente4);
            estilo5.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo5.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo5.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo5.setBottomBorderColor(HSSFColor.BLACK.index);
            estilo5.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo5.setLeftBorderColor  (HSSFColor.BLACK.index);
            estilo5.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo5.setRightBorderColor(HSSFColor.BLACK.index);
            estilo5.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo5.setTopBorderColor   (HSSFColor.BLACK.index);
            estilo5.setAlignment(HSSFCellStyle.ALIGN_RIGHT);
            estilo5.setDataFormat(wb.createDataFormat().getFormat("#,##0.00")); 
            /****************************************************/
            HSSFCellStyle estilo10 = wb.createCellStyle();
            estilo10.setFont(fuente4);
            estilo10.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo10.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo10.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo10.setBottomBorderColor(HSSFColor.BLACK.index);
            estilo10.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo10.setLeftBorderColor  (HSSFColor.BLACK.index);
            estilo10.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo10.setRightBorderColor(HSSFColor.BLACK.index);
            estilo10.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo10.setTopBorderColor   (HSSFColor.BLACK.index);
            estilo10.setAlignment(HSSFCellStyle.ALIGN_RIGHT);
            estilo10.setDataFormat(wb.createDataFormat().getFormat("#"));
             /****************************************************/
            
            HSSFFont fuente6 = wb.createFont();
            fuente6.setColor((short)0x0);  
            fuente6.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
            
            HSSFCellStyle estilo6 = wb.createCellStyle();
            estilo6.setFont(fuente6);
            estilo6.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo6.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo6.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo6.setBottomBorderColor(HSSFColor.WHITE.index);
            estilo6.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo6.setLeftBorderColor  (HSSFColor.WHITE.index);
            estilo6.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo6.setRightBorderColor(HSSFColor.WHITE.index);
            estilo6.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo6.setTopBorderColor   (HSSFColor.WHITE.index);
            
            /****************************************************/  
            /* MONEDA */
            HSSFCellStyle estilo7 = wb.createCellStyle();
            estilo7.setFont(fuente4);
            estilo7.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo7.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo7.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo7.setBottomBorderColor(HSSFColor.BLACK.index);
            estilo7.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo7.setLeftBorderColor  (HSSFColor.BLACK.index);
            estilo7.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo7.setRightBorderColor(HSSFColor.BLACK.index);
            estilo7.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo7.setTopBorderColor   (HSSFColor.BLACK.index);
            estilo7.setAlignment(HSSFCellStyle.ALIGN_RIGHT);
            estilo7.setDataFormat(wb.createDataFormat().getFormat("$#,##0.00"));
            
            /* TEXTO NORMAL CENTRADO */
            HSSFCellStyle estilo9 = wb.createCellStyle();
            estilo9.setFont(fuente4);
            estilo9.setFillForegroundColor(HSSFColor.WHITE.index);
            estilo9.setFillPattern((short)(estilo1.SOLID_FOREGROUND));
            estilo9.setBorderBottom     (HSSFCellStyle.BORDER_THIN);
            estilo9.setBottomBorderColor(HSSFColor.BLACK.index);
            estilo9.setBorderLeft       (HSSFCellStyle.BORDER_THIN);
            estilo9.setLeftBorderColor  (HSSFColor.BLACK.index);
            estilo9.setBorderRight      (HSSFCellStyle.BORDER_THIN);
            estilo9.setRightBorderColor(HSSFColor.BLACK.index);
            estilo9.setBorderTop        (HSSFCellStyle.BORDER_THIN);
            estilo9.setTopBorderColor   (HSSFColor.BLACK.index);
            estilo9.setAlignment(HSSFCellStyle.ALIGN_CENTER);
            
            row  = sheet.createRow((short)(0));
            row  = sheet.createRow((short)(1));
            row  = sheet.createRow((short)(2));
            row  = sheet.createRow((short)(3));
            row  = sheet.createRow((short)(4));
            row  = sheet.createRow((short)(5));
            row  = sheet.createRow((short)(6));
            row  = sheet.createRow((short)(7));
            row  = sheet.createRow((short)(8));
            row  = sheet.createRow((short)(9));
            
            for ( int j = 0; j < 37; j++ ) { //COLUMNAS
                
                row  = sheet.getRow((short)(0));
                cell = row.createCell((short)(j));
                cell.setCellStyle(estilo1);
                row  = sheet.getRow((short)(1));
                cell = row.createCell((short)(j)); 
                cell.setCellStyle(estiloX);
                
            }
            
            row  = sheet.getRow((short)(0));
            cell = row.getCell((short)(0));
            cell.setCellValue("TRANSPORTES SANCHEZ POLO");
                        
            row  = sheet.getRow((short)(1));            
            cell = row.getCell((short)(0));            
            cell.setCellValue("Reporte Gerencial Ajustes Fletes");
            
            for( int i = 2; i < 3; i++ ){ //FILAS
                 
                for ( int j = 0; j < 2; j++ ) { //COLUMNAS
                    
                    row  = sheet.getRow((short)(i));
                    cell = row.createCell((short)(j)); 
                    cell.setCellStyle(estilo6);
                    
                }      
                
            }
            
            row = sheet.createRow((short)(3));            
            row = sheet.getRow( (short)(3) );     
            
            cell = row.createCell((short)(0));
            cell.setCellStyle(estilo3);
            cell.setCellValue("AG ORIGINA OC");
            
            cell = row.createCell((short)(1));
            cell.setCellStyle(estilo3);
            cell.setCellValue("CREACION OC");
            
            cell = row.createCell((short)(2));
            cell.setCellStyle(estilo3);
            cell.setCellValue("CREA OC");
            
            cell = row.createCell((short)(3));
            cell.setCellStyle(estilo3);
            cell.setCellValue("AG USUARIO");
            
            cell = row.createCell((short)(4));
            cell.setCellStyle(estilo3);
            cell.setCellValue("OC");
            
            cell = row.createCell((short)(5));
            cell.setCellStyle(estilo3);
            cell.setCellValue("RUTA OC");
            
            cell = row.createCell((short)(6));
            cell.setCellStyle(estilo3);
            cell.setCellValue("PLACA");
            
            cell = row.createCell((short)(7));
            cell.setCellStyle(estilo3);
            cell.setCellValue("TIPO  DE RECURSO");
            
            cell = row.createCell((short)(8));
            cell.setCellStyle(estilo3);
            cell.setCellValue("CANTIDAD");
            
            cell = row.createCell((short)(9));
            cell.setCellStyle(estilo3);
            cell.setCellValue("UNIDADES");
            
            cell = row.createCell((short)(10));
            cell.setCellStyle(estilo3);
            cell.setCellValue("ESTANDAR");
            
            cell = row.createCell((short)(11));
            cell.setCellStyle(estilo3);
            cell.setCellValue("CLIENTE");
            
            cell = row.createCell((short)(12));
            cell.setCellStyle(estilo3);
            cell.setCellValue("AG DUE�A");
            
            cell = row.createCell((short)(13));
            cell.setCellStyle(estilo3);
            cell.setCellValue("OT");
            
            cell = row.createCell((short)(14));
            cell.setCellStyle(estilo3);
            cell.setCellValue("TIPO");
            
            cell = row.createCell((short)(15));
            cell.setCellStyle(estilo3);
            cell.setCellValue("DESCRIPCION OT");
            
            cell = row.createCell((short)(16));
            cell.setCellStyle(estilo3);
            cell.setCellValue("CANTIDAD OT");
            
            cell = row.createCell((short)(17));
            cell.setCellStyle(estilo3);
            cell.setCellValue("COD TARIFA");
            
            cell = row.createCell((short)(18));
            cell.setCellStyle(estilo3);
            cell.setCellValue("COD CF");
            
            cell = row.createCell((short)(19));
            cell.setCellStyle(estilo3);
            cell.setCellValue("FLETE STD");
            
            cell = row.createCell((short)(20));
            cell.setCellStyle(estilo3);
            cell.setCellValue("UND OC FLETE STD");
            
            cell = row.createCell((short)(21));
            cell.setCellStyle(estilo3);
            cell.setCellValue("MON FLETE");
            
            cell = row.createCell((short)(22));
            cell.setCellStyle(estilo3);
            cell.setCellValue("FLETE DESPACHO PESOS");
            
            cell = row.createCell((short)(23));
            cell.setCellStyle(estilo3);
            cell.setCellValue("FLETE DES MON EST");
            
            cell = row.createCell((short)(24));
            cell.setCellStyle(estilo3);
            cell.setCellValue("FLETE STD PES");
            
            cell = row.createCell((short)(25));
            cell.setCellStyle(estilo3);
            cell.setCellValue("AJUSTE FLETE");
            
            cell = row.createCell((short)(26));
            cell.setCellStyle(estilo3);
            cell.setCellValue("TARIFA PESOS");
            
            cell = row.createCell((short)(27));
            cell.setCellStyle(estilo3);
            cell.setCellValue("VAR PORC FLETES");
            
            cell = row.createCell((short)(28));
            cell.setCellStyle(estilo3);
            cell.setCellValue("CALIFICACION");
            
            cell = row.createCell((short)(29));
            cell.setCellStyle(estilo3);
            cell.setCellValue("AJUSTE NEGATIVO");
            
            cell = row.createCell((short)(30));
            cell.setCellStyle(estilo3);
            cell.setCellValue("AJUSTE POSITIVO");
            
            cell = row.createCell((short)(31));
            cell.setCellStyle(estilo3);
            cell.setCellValue("SIN AJUSTE");
            
            cell = row.createCell((short)(32));
            cell.setCellStyle(estilo3);
            cell.setCellValue("VALOR NEGATIVO");
            
            cell = row.createCell((short)(33));
            cell.setCellStyle(estilo3);
            cell.setCellValue("VALOR POSITIVO");
            
            cell = row.createCell((short)(34));
            cell.setCellStyle(estilo3);
            cell.setCellValue("BALANCE");
            
            cell = row.createCell((short)(35));
            cell.setCellStyle(estilo3);
            cell.setCellValue("RENT STD");
            
            cell = row.createCell((short)(36));
            cell.setCellStyle(estilo3);
            cell.setCellValue("RENT DESP");
            
            
            /*************************************************************************************/
            /***** RECORRER LOS DATOS ******/
            
            int Fila = 3; 
            for ( int t = 0; t < datos.size(); t++ ) {
                
                aj = ( AjusteFlete ) datos.elementAt( t );
                if((aj.getBase().equals("")) || (aj.getBase().equals("COL"))){
                
                    Fila++;
                    row  = sheet.createRow( ( short )( Fila ) );

                    cell = row.createCell( ( short )( 0 ) );
                    cell.setCellStyle( estilo4 );
                    cell.setCellValue( aj.getAgoc() );

                    cell = row.createCell( ( short )( 1 ) );
                    cell.setCellStyle( estilo4 );
                    cell.setCellValue( aj.getFecoc() );

                    cell = row.createCell( ( short )( 2 ) );
                    cell.setCellStyle( estilo4 );
                    cell.setCellValue( aj.getCreaoc() );

                    cell = row.createCell( ( short )( 3 ) );
                    cell.setCellStyle( estilo4 );
                    cell.setCellValue( aj.getAgoc() );

                    cell = row.createCell( ( short )( 4 ) );
                    cell.setCellStyle( estilo4 );
                    cell.setCellValue( aj.getNumoc() );

                    cell = row.createCell( ( short )( 5 ) );
                    cell.setCellStyle( estilo4 );
                    cell.setCellValue( aj.getRutaoc() );

                    cell = row.createCell( ( short )( 6 ) );
                    cell.setCellStyle( estilo4 );
                    cell.setCellValue( aj.getPlacaoc() );

                    cell = row.createCell( ( short )( 7 ) );
                    cell.setCellStyle( estilo4 );
                    cell.setCellValue( aj.getRecoc() );

                    cell = row.createCell( ( short )( 8 ) );
                    cell.setCellStyle( estilo5 );
                    cell.setCellValue( Double.parseDouble(aj.getCantoc()) );

                    cell = row.createCell( ( short )( 9 ) );
                    cell.setCellStyle( estilo4 );
                    cell.setCellValue( aj.getUnioc() );

                    cell = row.createCell( ( short )( 10 ) );
                    cell.setCellStyle( estilo4 );
                    cell.setCellValue( aj.getStgoc() );

                    cell = row.createCell( ( short )( 11 ) );
                    cell.setCellStyle( estilo4 );
                    cell.setCellValue( aj.getClioc() );

                    cell = row.createCell( ( short )( 12 ) );
                    cell.setCellStyle( estilo4 );
                    cell.setCellValue( aj.getAgduenaoc() );

                    cell = row.createCell( ( short )(13 ) );
                    cell.setCellStyle( estilo4 );
                    cell.setCellValue( aj.getOtoc() );

                    cell = row.createCell( ( short )( 14 ) );
                    cell.setCellStyle( estilo4 );
                    cell.setCellValue( aj.getTipooc() );

                    cell = row.createCell( ( short )( 15 ) );
                    cell.setCellStyle( estilo4 );
                    cell.setCellValue( aj.getDecot() );

                    cell = row.createCell( ( short )( 16 ) );
                    cell.setCellStyle( estilo5 );
                    cell.setCellValue( Double.parseDouble(aj.getCantot()) );

                    cell = row.createCell( ( short )( 17 ) );
                    cell.setCellStyle( estilo4 );
                    cell.setCellValue( aj.getTarifaot() );

                    cell = row.createCell( ( short )( 18 ) );
                    cell.setCellStyle( estilo4 );
                    cell.setCellValue( aj.getCodcf() );

                    cell = row.createCell( ( short )( 19 ) );
                    cell.setCellStyle( estilo5 );
                    cell.setCellValue( Double.parseDouble(aj.getValor_unidad()) * Double.parseDouble(aj.getCantoc()) );

                    cell = row.createCell( ( short )( 20 ) );
                    cell.setCellStyle( estilo4 );
                    cell.setCellValue( aj.getUnioc() );

                    cell = row.createCell( ( short )( 21 ) );
                    cell.setCellStyle( estilo4 );
                    cell.setCellValue( aj.getMonflete() );

                    cell = row.createCell( ( short )( 22 ) );
                    cell.setCellStyle( estilo5 );
                    cell.setCellValue( Double.parseDouble(aj.getFletedespes()) );

                    cell = row.createCell( ( short )( 23 ) );
                    cell.setCellStyle( estilo5 );
                    cell.setCellValue( Double.parseDouble(aj.getFletedepmonest()) );

                    cell = row.createCell( ( short )( 24 ) );
                    cell.setCellStyle( estilo5 );
                    
                    valor_cambio = Double.parseDouble(aj.getValor_unidad()) * Double.parseDouble(aj.getCantoc());
                    if(aj.getMonflete().equals("PES")){
                        cell.setCellValue( valor_cambio );
                    }else{
                        Tasa tasa = model.tasaService.buscarValorTasa(moneda_usuario, aj.getMonflete(), "PES", aj.getFecoc().substring(0,10) );
                        if (tasa != null){
                            valor_tasa = tasa.getValor_tasa();
                            valor_cambio = valor_cambio * valor_tasa;
                            cell.setCellValue( valor_cambio );
                        }
                    }

                    cell = row.createCell( ( short )( 25 ) );
                    cell.setCellStyle( estilo5 );
                    cell.setCellValue( Double.parseDouble(aj.getFletedespes()) - valor_cambio );

                    cell = row.createCell( ( short )( 26 ) );
                    cell.setCellStyle( estilo5 );
                    cell.setCellValue( Double.parseDouble(aj.getTarifapes()) );
                    double porcfletes = 0;
                    if(valor_cambio == 0){
                       porcfletes = 0; 
                    }else{
                       porcfletes = ((Double.parseDouble(aj.getFletedespes()) / valor_cambio) -1 ); 
                    }
                    
                    cell = row.createCell( ( short )( 27 ) );
                    cell.setCellStyle( estilo5 );
                    cell.setCellValue( porcfletes );

                    if((porcfletes * 100) <= 0){
                         var = 100;
                    }else{
                          if((porcfletes * 100) > 30){
                              var = 0;
                          }else{
                              var = (-10/3) *  (porcfletes * 100) + 100 / 100;
                          }
                    }

                    cell = row.createCell( ( short )( 28 ) );
                    cell.setCellStyle( estilo5 );
                    cell.setCellValue( var );




                    cell = row.createCell( ( short )( 29 ) );
                    cell.setCellStyle( estilo4 );
                    cell.setCellValue( (aj.getAnegativo().equals("0"))?"0":"1");

                    cell = row.createCell( ( short )( 30 ) );
                    cell.setCellStyle( estilo4 );
                    cell.setCellValue( (aj.getApositivo().equals("0"))?"0":"1");

                    cell = row.createCell( ( short )( 31 ) );
                    cell.setCellStyle( estilo4 );
                    cell.setCellValue( (!aj.getAnegativo().equals("0") || !aj.getApositivo().equals("0")  )?"0":"1");

                    cell = row.createCell( ( short )( 32 ) );
                    cell.setCellStyle( estilo5 );
                    cell.setCellValue( Double.parseDouble(aj.getValneg()) );

                    cell = row.createCell( ( short )( 33 ) );
                    cell.setCellStyle( estilo5 );
                    cell.setCellValue( Double.parseDouble(aj.getValpos()) );

                    cell = row.createCell( ( short )( 34 ) );
                    cell.setCellStyle( estilo5 );
                    cell.setCellValue( Double.parseDouble(aj.getFletedespes()) - valor_cambio );

                    cell = row.createCell( ( short )( 35 ) );
                    cell.setCellStyle( estilo5 );
                    if(Double.parseDouble(aj.getTarifapes()) == 0){
                        cell.setCellValue( 0 );
                    }else{
                        cell.setCellValue((100 - ( valor_cambio / Double.parseDouble(aj.getTarifapes()))  * 100) );
                    }


                    cell = row.createCell( ( short )( 36 ) );
                    cell.setCellStyle( estilo5 );
                    if(Double.parseDouble(aj.getTarifapes()) == 0){
                        cell.setCellValue( 0 );
                    }else{
                        cell.setCellValue( ((100 - ( Double.parseDouble(aj.getFletedespes()) / Double.parseDouble(aj.getTarifapes()))  * 100) ) );
                    }    
                }
            }
            
            
            /******************************************************************/
            /***** GUARDAR DATOS EN EL ARCHIVO *****/
            FileOutputStream fo = new FileOutputStream ( Ruta );
            wb.write ( fo );
            fo.close ();
                    
            //Finalizo el proceso exitosamente
            model.LogProcesosSvc.finallyProceso( procesoName, this.hashCode(), usuario.getLogin(), "PROCESO EXITOSO" );
            
        } catch ( Exception e ) {    
            
            e.printStackTrace();
            
            //Capturo errores finalizando proceso
            try {
                
                model.LogProcesosSvc.finallyProceso( procesoName, this.hashCode(), usuario.getLogin(), "ERROR : " + e.getMessage() );
            
            } catch ( Exception f ) {
                
                f.printStackTrace();
                
                try {
                    
                    model.LogProcesosSvc.finallyProceso( procesoName,this.hashCode(), usuario.getLogin(), "ERROR : " + f.getMessage() );
                
                } catch ( Exception p ) { p.printStackTrace(); }
                
            }

        }
        
    }
    
}