/********************************************************************
 *      Nombre Clase.................   ReporteFacturasTh.java
 *      Descripci�n..................   Hilo para la escritura del xls
 *      Autor........................   Ing. Andr�s Maturana De La Cruz
 *      Fecha........................   25.10.2006
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.model.threads;


import java.util.*;
import java.io.*;
import java.lang.*;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.beans.POIWrite;
import com.tsp.exceptions.*;
import com.tsp.operation.controller.*;
import java.text.*;
import javax.servlet.http.*;
import com.tsp.util.*;
import com.tsp.operation.model.*;
import com.tsp.util.Util;
import java.sql.SQLException;
import java.text.*;
import org.apache.log4j.*;
//import com.tsp.operation.model.threads.POIWrite;



public class ReporteFacturasTh extends Thread{
    
    Logger logger = Logger.getLogger(this.getClass());
    
    private Vector reporte;
    private String user;
    private Model model;
    private String empresa;
    
    //Log Procesos
    private String procesoName;
    private String des;
   
    public void start(Model modelo, Vector reporte, String user,String empresa){
        this.reporte = reporte;
        this.user = user;
        this.model   = modelo;
        this.procesoName = "Reporte Facturas";
        this.des = "Reporte Facturas";
        this.empresa = empresa;
        
        super.start();
    }
    
    public synchronized void run(){
        try{
            //INSERTO EN EL LOG DE PROCESO
            model.LogProcesosSvc.InsertProceso(this.procesoName, this.hashCode(), this.des, user);
            
            //Fecha del sistema
            Calendar FechaHoy = Calendar.getInstance();
            Date d = FechaHoy.getTime();
            SimpleDateFormat s = new SimpleDateFormat("yyyyMMdd_kkmm");
            SimpleDateFormat s1 = new SimpleDateFormat("yyyy-MM-dd-hh:mm");
            SimpleDateFormat fec = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss");
            SimpleDateFormat time = new SimpleDateFormat("hh:mm");
            String FechaFormated = s.format(d);
            String FechaFormated1 = s1.format(d);
            String hora = time.format(d);
            String Fecha = fec.format(d);
            
            
            //obtener cabecera de ruta
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String path = rb.getString("ruta");
            //armas la ruta
            String Ruta1  = path + "/exportar/migracion/" + user + "/";
            //crear la ruta
            //File file = new File(Ruta1);
            //file.mkdirs();
            
            //POIWrite xls = new POIWrite(Ruta1 +"ReporteFacturas_" + FechaFormated + ".xls", user, Fecha);
            String nom_archivo = "ReporteFacturas_" + FechaFormated + ".xls";
            POIWrite xls = new POIWrite();
            xls.nuevoLibro(Ruta1 + nom_archivo);
            logger.info("?Ruta del archivo xls: " + Ruta1 + nom_archivo);
            
            //Definici�n de Estilos
            HSSFCellStyle fecha  = xls.nuevoEstilo("Arial", 9, false , false, "yyyy/mm/dd"  , xls.NONE , xls.NONE , xls.NONE );
            HSSFCellStyle texto  = xls.nuevoEstilo("Arial", 9, false , false, "text"        , xls.NONE , xls.NONE , xls.NONE);
            HSSFCellStyle numero = xls.nuevoEstilo("Arial", 9, false , false, ""            , xls.NONE , xls.NONE , xls.NONE);
            
            HSSFCellStyle header1      = xls.nuevoEstilo("Arial", 12, true , false, "text"        , HSSFColor.WHITE.index , HSSFColor.GREEN.index , HSSFCellStyle.ALIGN_LEFT);
            HSSFCellStyle header2      = xls.nuevoEstilo("Arial", 18, true , false, "text"        , HSSFColor.WHITE.index , HSSFColor.ORANGE.index , HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle header3      = xls.nuevoEstilo("Arial", 12, true , false, "text"        , HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
            HSSFCellStyle texto2      = xls.nuevoEstilo("Arial", 12, false , false, "text"        , HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
            HSSFCellStyle titulo      = xls.nuevoEstilo("Arial", 12, true  , false, "text"        , HSSFColor.BLACK.index , HSSFColor.PINK.index , HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle fechatitle  = xls.nuevoEstilo("Arial", 12, true  , false, "yyyy/mm/dd"  , HSSFColor.RED.index , xls.NONE, HSSFCellStyle.ALIGN_CENTER );
            HSSFCellStyle entero      = xls.nuevoEstilo("Arial", 12, false , false, "text"        , HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_CENTER);
            HSSFCellStyle flotante      = xls.nuevoEstilo("Arial", 12, false , false, "text"        , HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_RIGHT);
            HSSFCellStyle moneda  = xls.nuevoEstilo("Arial", 9, false , false, "#,##0.00", xls.NONE , xls.NONE , xls.NONE);
            
            xls.obtenerHoja("FACTURAS PROVEEDOR");
            xls.cambiarMagnificacion(3,4);
            
            // cabecera
            
            // cabecera
            
            xls.combinarCeldas(0, 0, 0, 34);
            xls.combinarCeldas(3, 0, 3, 34);
            xls.combinarCeldas(4, 0, 4, 34);
            xls.adicionarCelda(0, 0, "REPORTE DE FACTURAS", header2);
            xls.adicionarCelda(4, 0, "Fecha del proceso: " + Fecha, header3);
            
            
            // subtitulos
            
            
            int fila = 5;
            int col  = 0;
            
            
            xls.adicionarCelda(fila, col++, "NIT", header1);
            xls.adicionarCelda(fila, col++, "DOCUMENTO", header1);
            xls.adicionarCelda(fila, col++, "FACTURA", header1);
            xls.adicionarCelda(fila, col++, "TIPO DOCUMENTO", header1);
            xls.adicionarCelda(fila, col++, "DESCRIPCION", header1);
            xls.adicionarCelda(fila, col++, "ESTADO FACTURA", header1);
            xls.adicionarCelda(fila, col++, "RETENCION DE PAGO", header1);
            xls.adicionarCelda(fila, col++, "CLASE DOCUMENTO", header1);
            xls.adicionarCelda(fila, col++, "VR DOCUMENTO", header1);
            xls.adicionarCelda(fila, col++, "VR EN PESOS", header1);
            xls.adicionarCelda(fila, col++, "VR A PAGAR", header1);
            xls.adicionarCelda(fila, col++, "VR A PAGAR EN PESOS", header1);
            xls.adicionarCelda(fila, col++, "SALDO", header1);
            xls.adicionarCelda(fila, col++, "MONEDA", header1);
            xls.adicionarCelda(fila, col++, "PAGADA", header1);
            xls.adicionarCelda(fila, col++, "AGENCIA", header1);
            xls.adicionarCelda(fila, col++, "BANCO", header1);
            xls.adicionarCelda(fila, col++, "SUCURSAL", header1);
            xls.adicionarCelda(fila, col++, "CORRIDA", header1);
            xls.adicionarCelda(fila, col++, "CHEQUE", header1);
            xls.adicionarCelda(fila, col++, "DOC. RELACIONADO", header1);
            xls.adicionarCelda(fila, col++, "TIPO DOC. REL.", header1);
            xls.adicionarCelda(fila, col++, "PLANILLA", header1);
            xls.adicionarCelda(fila, col++, "USUARIO CREO", header1);
            xls.adicionarCelda(fila, col++, "FECHA CREACION", header1);
            xls.adicionarCelda(fila, col++, "FECHA DOC.", header1);
            xls.adicionarCelda(fila, col++, "FECHA VENCIMIENTO", header1);
            xls.adicionarCelda(fila, col++, "ULTIMA FECHA PAGO", header1);
            xls.adicionarCelda(fila, col++, "APROBADOR", header1);
            xls.adicionarCelda(fila, col++, "FECHA APROBACION", header1);
            xls.adicionarCelda(fila, col++, "USUARIO APROBACION", header1);
            xls.adicionarCelda(fila, col++, "USUARIO ANULO", header1);
            xls.adicionarCelda(fila, col++, "FECHA ANULACION", header1);
            xls.adicionarCelda(fila, col++, "TIPO DE PAGO", header1);
            xls.adicionarCelda(fila, col++, "BANCO TRANFER.", header1);
            xls.adicionarCelda(fila, col++, "SUCURSAL TRANSFER.", header1);
            xls.adicionarCelda(fila, col++, "CUENTA NRO.", header1);
            xls.adicionarCelda(fila, col++, "TIPO CTA.", header1);
            xls.adicionarCelda(fila, col++, "NOMBRE", header1);
            xls.adicionarCelda(fila, col++, "CEDULA/NIT", header1);
            xls.adicionarCelda(fila, col++, "FECHA CONTABILIZACION", header1);
            xls.adicionarCelda(fila, col++, "USUARIO CONTABILIZO", header1);
            xls.adicionarCelda(fila, col++, "PERIODO", header1);
            xls.adicionarCelda(fila, col++, "TRANSACCION", header1);
            xls.adicionarCelda(fila, col++, "TRANSACCION ANULACION", header1);
            xls.adicionarCelda(fila, col++, "FECHA CONT. ANULACION", header1);
            if( empresa.equals("INYM")){
                xls.adicionarCelda(fila, col++, "# MS", header1);
            }
            
            
            
            
            
            
            fila++;
            
            double total = 0;
            for (int i = 0; i < reporte.size(); i++){
                col = 0;
                
                CXP_Doc doc = (CXP_Doc) reporte.elementAt(i);
                total += doc.getVlr_neto();
                String tdoc = "";
                String tdoc_rel = "";
                
                if( doc.getTipo_documento().equals("FAP") ){
                    tdoc = "FACTURA";
                } else if( doc.getTipo_documento().equals("NC") ){
                    tdoc = "NOTA CREDITO";
                } else if( doc.getTipo_documento().equals("ND") ){
                    tdoc = "NOTA DEBITO";
                }
                
                if( doc.getTipo_documento_rel().equals("FAP") ){
                    tdoc_rel = "FACTURA";
                } else if( doc.getTipo_documento_rel().equals("NC") ){
                    tdoc_rel = "NOTA CREDITO";
                } else if( doc.getTipo_documento_rel().equals("ND") ){
                    tdoc_rel = "NOTA DEBITO";
                }
                
                xls.adicionarCelda(fila, col++, doc.getProveedor(), texto);
                xls.adicionarCelda(fila, col++, doc.getNomProveedor(), texto);
                xls.adicionarCelda(fila, col++, doc.getDocumento(), texto);
                xls.adicionarCelda(fila, col++, tdoc, texto);
                xls.adicionarCelda(fila, col++, doc.getDescripcion(), texto);
                xls.adicionarCelda(fila, col++, "" + ( !doc.getReg_status().equals("A") ? doc.getReg_status() : "ANULADO" ), texto);
                xls.adicionarCelda(fila, col++, doc.getRetencion_pago()!=null && doc.getRetencion_pago().equals("S") ? "SI" : "NO", texto);
                xls.adicionarCelda(fila, col++, "" + ( !doc.getClase_documento().equals("") ? doc.getClase_documento() : "NO REGISTRA" ), texto);
                xls.adicionarCelda(fila, col++, doc.getVlr_bruto_me(), moneda);
                xls.adicionarCelda(fila, col++, doc.getVlr_bruto_ml(), moneda);
                xls.adicionarCelda(fila, col++, doc.getVlr_neto_me(), moneda);
                xls.adicionarCelda(fila, col++, doc.getVlr_neto(), moneda);
                xls.adicionarCelda(fila, col++, doc.getVlr_saldo_me(), moneda);
                xls.adicionarCelda(fila, col++, doc.getMoneda(), texto);
                xls.adicionarCelda(fila, col++, ( doc.getVlr_saldo()==0 ? "SI" : "NO" ), texto);
                xls.adicionarCelda(fila, col++, doc.getAgencia(), texto);
                xls.adicionarCelda(fila, col++, doc.getBanco(), texto);
                xls.adicionarCelda(fila, col++, doc.getSucursal(), texto);
                xls.adicionarCelda(fila, col++, doc.getCorrida(), texto);
                xls.adicionarCelda(fila, col++, doc.getCheque(), texto);
                xls.adicionarCelda(fila, col++, doc.getDocumento_relacionado(), texto);
                xls.adicionarCelda(fila, col++, tdoc_rel, texto);
                xls.adicionarCelda(fila, col++, doc.getNumpla(), texto);
                xls.adicionarCelda(fila, col++, doc.getCreation_user(), texto);
                xls.adicionarCelda(fila, col++, doc.getCreation_date(), texto);
                xls.adicionarCelda(fila, col++, doc.getFecha_documento(), texto);
                xls.adicionarCelda(fila, col++, ( !doc.getFecha_vencimiento().equals("0099-01-01") )? doc.getFecha_vencimiento() : "NO REGISTRA", texto);
                xls.adicionarCelda(fila, col++, ( !doc.getUltima_fecha_pago().equals("0099-01-01") )? doc.getUltima_fecha_pago() : "NO REGISTRA", texto);
                xls.adicionarCelda(fila, col++, ( !doc.getAprobador().equals("") )? doc.getAprobador() : "NO REGISTRA", texto);
                xls.adicionarCelda(fila, col++, ( !doc.getFecha_aprobacion().equals("0099-01-01") )? doc.getFecha_aprobacion() : "NO REGISTRA", texto);
                xls.adicionarCelda(fila, col++, ( !doc.getUsuario_aprobacion().equals("") )? doc.getUsuario_aprobacion() : "NO REGISTRA" , texto);
                xls.adicionarCelda(fila, col++, "" + ( !doc.getUsuario_anulo().equals("") ? doc.getUsuario_anulo() : "NO REGISTRA" ), texto);
                xls.adicionarCelda(fila, col++, "" + ( !doc.getFecha_anulacion().equals("0099-01-01 00:00") ? doc.getFecha_anulacion() : "NO REGISTRA" ), texto);
                xls.adicionarCelda(fila, col++, doc.getProv_tipo_pago(), texto);
                xls.adicionarCelda(fila, col++, doc.getProv_banco_transfer(), texto);
                xls.adicionarCelda(fila, col++, doc.getProv_suc_transfer(), texto);
                xls.adicionarCelda(fila, col++, doc.getProv_no_cuenta(), texto);
                xls.adicionarCelda(fila, col++, doc.getProv_tipo_cuenta(), texto);
                xls.adicionarCelda(fila, col++, doc.getProv_no_cuenta(), texto);
                xls.adicionarCelda(fila, col++, doc.getProv_cedula_cuenta(), texto);
                xls.adicionarCelda(fila, col++, ( !doc.getFecha_contabilizacion().equals("0099-01-01") )? doc.getFecha_contabilizacion() : "NO REGISTRA", texto);
                xls.adicionarCelda(fila, col++, ( !doc.getUsuario_contabilizo().equals("") )? doc.getUsuario_contabilizo() : "NO REGISTRA", texto);
                xls.adicionarCelda(fila, col++, ( !doc.getPeriodo().equals("") )? doc.getPeriodo() : "NO REGISTRA", texto);
                xls.adicionarCelda(fila, col++, "" + ( doc.getTransaccion() != 0 ? doc.getTransaccion() : 0 ), texto);
                xls.adicionarCelda(fila, col++, "" + ( doc.getTransaccion_anulacion() != 0 ? doc.getTransaccion_anulacion() : 0 ), texto);
                xls.adicionarCelda(fila, col++, "" + ( !doc.getFecha_contabilizacion_anulacion().equals("0099-01-01 00:00") ? doc.getFecha_contabilizacion_anulacion() : "NO REGISTRA" ), texto);
                 if( empresa.equals("INYM")){
                 xls.adicionarCelda(fila, col++, doc.getMultiservicio(), texto);
                 }
                fila++;
            }
            
            xls.cerrarLibro();
            model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(), user, "PROCESO EXITOSO");
        }catch (Exception ex){
            ex.printStackTrace();
            ////System.out.println("ERROR AL GENERAR EL ARCHIVO XLS : " + ex.getMessage());
            try{
                model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(), user, "ERROR :" + ex.getMessage());
            }
            catch(Exception f){
                try{
                    model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(), user, "ERROR :");
                }catch(Exception p){    }
            }
        }
        
    }
    
    
}
//Entregado a tito 1 Marzo de 2007
