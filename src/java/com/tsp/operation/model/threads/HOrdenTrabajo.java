/*
 * HOrdenTrabajo.java
 * Created on 21 de abril de 2009, 17:39
 */
package com.tsp.operation.model.threads;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import org.apache.poi.poifs.filesystem.DirectoryEntry;
import org.apache.poi.poifs.filesystem.DocumentEntry;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;

public class HOrdenTrabajo  extends Thread{
        
    public HOrdenTrabajo() {        
    }
    
    public void start(String user ) {
        try{ 
            escribirWordArchivo("C://orden_de_trabajo.doc", "esto es una peque�a prueba que sirve para tener varias palabras aunque realmente la prueba no es tan grande como va a ser cuando llegue el momento de hacer la prueba con ");
        }catch(Exception e){ 
            System.out.println("error en hordentrabajo:"+e.toString()+"__"+e.getMessage());            
        } 
    }
    
    public boolean escribirWordArchivo(String path, String content) { 
        boolean w = false; 
        try { 
            byte b[] = content.getBytes("ISO-8859-1"); 
            // byte b[] = content.getBytes(); 
            ByteArrayInputStream bais = new ByteArrayInputStream(b); 
            POIFSFileSystem fs = new POIFSFileSystem(); 
            DirectoryEntry directory = fs.getRoot(); 
            DocumentEntry de = directory.createDocument("WordDocument", bais);

            FileOutputStream ostream = new FileOutputStream(path); 
            fs.writeFilesystem(ostream); 
            bais.close(); 
            ostream.close(); 
        } catch (IOException e) { 
            System.out.println("errorcito en hordentrabajo:"+e.toString()+"__"+e.getMessage());            
        } 
        return w; 
    } 
    
}



