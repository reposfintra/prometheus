/*Created on 23 de julio de 2005, 04:28 PM*/

package com.tsp.operation.model.threads;



import com.tsp.util.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.beans.*;
import java.io.*;
import java.sql.*;
import java.util.*;
import java.lang.*;
import javax.servlet.jsp.*;
import javax.servlet.http.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;
import javax.servlet.http.*;


// EXCEL
 import org.apache.poi.hssf.usermodel.*;
 import org.apache.poi.hssf.util.*;
 
 
 
public class HExportarExcel_RepDetalladoTrafico  extends Thread {
    
    private   Model       model;
    private   String      unidad;
    private   String      path;
    private   String      fileName;  ;
    private   Usuario     usuario;
    private   String      planilla;
    
    // Excel:
    private   HSSFWorkbook   libroExcel;
private   HSSFSheet      hojaExcel;
    private   HSSFRow        row;
    private   HSSFCell       cell;
    private   HSSFFont       font;
    private   HSSFFont       font1;
    private   HSSFCellStyle  style;
    private   HSSFCellStyle  style2;
    private   HSSFCellStyle  style3;
    private   HSSFCellStyle  style4;
    
    
    public HExportarExcel_RepDetalladoTrafico() {
    }
    
    
     public void start(  Usuario user, String numpla) throws Exception{
        try{
            this.model       = new Model();
            this.usuario     = user;
            this.planilla    = numpla;
            String dir       = user.getLogin();            
            String hoy       = Utility.getDate(6).replaceAll("/","");
            hoy = hoy.replaceAll(":", "");
            this.fileName    = "Trafico_"+hoy;
            this.unidad      = model.DirectorioSvc.getUrl() + dir; 
            model.DirectorioSvc.create(dir);
            this.path        = unidad +"/"+ fileName + ".xls"; 
            initExcel();
            super.start();
        }catch(Exception e){ throw new Exception ( " Hilo: "+ e.getMessage());}
    }
     
     public void initExcel(){
        this.libroExcel = new HSSFWorkbook();
        //this.hojaExcel  = libroExcel.createSheet(fileName);
        this.row        = null;
        this.cell       = null;
        createStyle();
    }
     
      public void createStyle(){         
          font = libroExcel.createFont();
          font.setFontHeightInPoints((short)10);        //tamano letra
          font.setFontName("Arial");                    //Tipo letra
          font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD); //letra negrita
          font.setColor((short)0x1);                    //color letra blanca  
          
          font1 = libroExcel.createFont();
          font1.setFontHeightInPoints((short)10);        //tamano letra
          font1.setFontName("Arial");                    //Tipo letra
          font1.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD); //letra negrita
          font1.setColor( HSSFColor.BLACK.index );                    
         
          
          style = libroExcel.createCellStyle();                        
          style.setFont(font);                       
          style.setFillForegroundColor(HSSFColor.DARK_BLUE.index);
          
          style.setFillPattern((short) style.SOLID_FOREGROUND);                                                
          style.setDataFormat(HSSFDataFormat.getBuiltinFormat("text"));
          style.setAlignment(style.ALIGN_CENTER);
          
          style2 = libroExcel.createCellStyle();                        
          style2.setFont(font1); 
          
          style3 = libroExcel.createCellStyle();                        
          style3.setFont(font);                       
          style3.setFillForegroundColor(HSSFColor.GREEN.index);
          style3.setFillPattern((short) style.SOLID_FOREGROUND);                                                
          style3.setDataFormat(HSSFDataFormat.getBuiltinFormat("text"));
          style3.setAlignment(style.ALIGN_CENTER);
          
          style4 = libroExcel.createCellStyle();                        
          style4.setFont(font1);                       
          style4.setFillForegroundColor(HSSFColor.YELLOW.index);
          style4.setFillPattern((short) style.SOLID_FOREGROUND);                                                
          style4.setDataFormat(HSSFDataFormat.getBuiltinFormat("text"));
          style4.setAlignment(style.ALIGN_CENTER);
   }
     
    public void prepared(int fila, int columnas) throws Exception{
       try{          
           row = hojaExcel.createRow((short)fila);               
           for(int j=0;j<=columnas;j++)
               cell = row.createCell((short)j);  
           row = hojaExcel.getRow((short)fila);
      }catch(Exception e){ throw new Exception (e.getMessage());}
    } 
     
    
     
    public void write(String dato, int columna) throws Exception{
       try{            
           cell = row.getCell((short)columna) ; cell.setCellValue(dato);
      }catch(Exception e){ throw new Exception (e.getMessage());}
    } 
     
    
    
   public void line1() throws Exception{
      try{
        prepared(0,4);
        cell = row.getCell((short)0) ;  cell.setCellStyle(style); cell.setCellValue(" OC/Planilla".toUpperCase());
        cell = row.getCell((short)1) ;  cell.setCellStyle(style); cell.setCellValue("Equipo".toUpperCase());
        cell = row.getCell((short)2) ;  cell.setCellStyle(style); cell.setCellValue("Origen".toUpperCase());
        cell = row.getCell((short)3) ;  cell.setCellStyle(style); cell.setCellValue("Destino".toUpperCase());   
        cell = row.getCell((short)4) ;  cell.setCellStyle(style); cell.setCellValue("ESTADO".toUpperCase());   
      }catch(Exception e){ throw new Exception (e.getMessage());}
    }
    
    
    public void line2( TraficoDetallado trafico ) throws Exception{
      try{
        prepared(1,4);
        cell = row.getCell((short)0) ;  cell.setCellStyle(style2); cell.setCellValue( trafico.getNumpla() );
        cell = row.getCell((short)1) ;  cell.setCellStyle(style2); cell.setCellValue( trafico.getPlaca()  );
        cell = row.getCell((short)2) ;  cell.setCellStyle(style2); cell.setCellValue( trafico.getOripla() );
        cell = row.getCell((short)3) ;  cell.setCellStyle(style2); cell.setCellValue( trafico.getDespla() );   
        cell = row.getCell((short)4) ;  cell.setCellStyle(style2); cell.setCellValue( trafico.getEstado() );   
      }catch(Exception e){ throw new Exception (e.getMessage());}
    }
     
   
     public void line3() throws Exception{
      try{
        prepared(3,3);
        cell = row.getCell((short)0) ;  cell.setCellStyle(style4); cell.setCellValue("Tipo".toUpperCase());
        cell = row.getCell((short)1) ;  cell.setCellStyle(style4); cell.setCellValue("Fecha".toUpperCase());
        cell = row.getCell((short)2) ;  cell.setCellStyle(style4); cell.setCellValue("Hora".toUpperCase());
        cell = row.getCell((short)3) ;  cell.setCellStyle(style4); cell.setCellValue("Ciudad".toUpperCase());   
      }catch(Exception e){ throw new Exception (e.getMessage());}
    }
     
     
     
    public void line4( int fila, ReporteTrafico rptTra ) throws Exception{
      try{
        prepared(fila,3);
        cell = row.getCell((short)0) ;   cell.setCellValue( rptTra.getTipo() );
        cell = row.getCell((short)1) ;   cell.setCellValue( rptTra.getFechaReporte()  );
        cell = row.getCell((short)2) ;   cell.setCellValue( rptTra.getHoraReporte() );
        cell = row.getCell((short)3) ;   cell.setCellValue( rptTra.getNomCiu() );   
      }catch(Exception e){ throw new Exception (e.getMessage());}
    }
     
     
    public void line5(int fila) throws Exception{
      try{
        prepared(fila,3);
        cell = row.getCell((short)0) ;  cell.setCellStyle(style4); cell.setCellValue("Observacion".toUpperCase());
        cell = row.getCell((short)1) ;  cell.setCellStyle(style4); cell.setCellValue("Fecha".toUpperCase());
        cell = row.getCell((short)2) ;  cell.setCellStyle(style4); cell.setCellValue("Hora".toUpperCase());
        cell = row.getCell((short)3) ;  cell.setCellStyle(style4); cell.setCellValue("Ciudad".toUpperCase());   
      }catch(Exception e){ throw new Exception (e.getMessage());}
    }
     
     
    
   public void line6( int fila, ReporteTraficoOb  rptTraOb   ) throws Exception{
      try{
        prepared(fila,3);
        cell = row.getCell((short)0) ;   cell.setCellValue( rptTraOb.getObservacion() );
        cell = row.getCell((short)1) ;   cell.setCellValue( rptTraOb.getFechaReporte()  );
        cell = row.getCell((short)2) ;   cell.setCellValue( rptTraOb.getHoraReporte() );
        cell = row.getCell((short)3) ;   cell.setCellValue( rptTraOb.getNomCiu() );   
      }catch(Exception e){ throw new Exception (e.getMessage());}
    }
     
     
             
    public void run(){
      try{
          model.traficoService.getReporteDetalladoTraf(planilla);        
          TraficoDetallado trafico;
          Vector reporte;
          if (model.traficoService.getTraficoDetallado().size() > 0)  reporte = new Vector(model.traficoService.getTraficoDetallado());
          else reporte = new Vector();
          for (int i = 0; i < reporte.size(); i++){
              
              trafico = (TraficoDetallado)reporte.get(i);
              this.hojaExcel  = libroExcel.createSheet(trafico.getNumpla());
              this.hojaExcel.setColumnWidth( (short)0,  (short)7000);
              this.hojaExcel.setColumnWidth( (short)1,  (short)2500);
              this.hojaExcel.setColumnWidth( (short)2,  (short)7000);
              this.hojaExcel.setColumnWidth( (short)3,  (short)7000);
              line1();
              line2(trafico);
              prepared(2,0); 
              cell.setCellStyle(style3);
              write("INFORMES DE TRAFICO",0);
              ReporteTrafico rptTra = null;
              List traficoList      = trafico.getTraficoList();
              Iterator traficoIt    = traficoList.iterator();
              line3();
              int fila = 4;
              while( traficoIt.hasNext() ){
                  rptTra = (ReporteTrafico)traficoIt.next(); 
                  line4(fila,rptTra);
                  fila++;
              }
              traficoList = null;
              traficoIt   = null;
              prepared(fila,0);   
              cell.setCellStyle(style3);
              write("OBSERVACIONES",0);
              ReporteTraficoOb rptTraOb = null;
              List traficoObList   = trafico.getTraficoObList();
              Iterator traficoObIt = traficoObList.iterator();
              fila++;
              line5(fila);
              fila++;
              while( traficoObIt.hasNext() ){
                rptTraOb = (ReporteTraficoOb)traficoObIt.next(); 
                line6(fila,rptTraOb);
                fila++;
              }
              traficoObList = null;
              traficoObIt   = null;
          }
          save();       
      }catch(Exception e){} 
    }
     
     
    public void save() throws Exception{
        try{
          FileOutputStream fo = new FileOutputStream(path);
          libroExcel.write(fo);
          fo.close();
         }
        catch(Exception e){
            throw new Exception (e.getMessage());
        }
   }
    public static void main(String[] afsfd){
        
        try{
            Usuario op = new Usuario();
            op.setLogin("OPEREZ");
            HExportarExcel_RepDetalladoTrafico h = new HExportarExcel_RepDetalladoTrafico();
            h.start(op, "596028,596141");
        }catch (Exception ex){
            ////System.out.println(ex.getMessage());
        }
    }
    
}
