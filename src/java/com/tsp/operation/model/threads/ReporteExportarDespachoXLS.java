/*************************************************************
 * Nombre: ReporteExportarDespachoXLS.java
 * Descripci�n: hilo para generar el reporte de indice de despacho.
 * Autor: Ing. Jose de la rosa
 * Fecha: 15 de diciembre de 2005, 05:02 AM
 * Versi�n: Java 1.0
 * Copyright: Fintravalores S.A. S.A.
 **************************************************************/

package com.tsp.operation.model.threads;


import java.text.*;
import java.util.Date;
import java.text.*;
import java.sql.SQLException;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.util.*;
import java.util.*;
import java.io.*;

public class ReporteExportarDespachoXLS extends Thread{
    private String procesoName;
    private String des;
    String id="";
    String fechaini = "";
    String fechafin = "";
    /** Creates a new instance of ReporteExportarDespachoXLS */
    public ReporteExportarDespachoXLS () {
    }
    
    public void start(String id, String fechaini, String fechafin){
        this.id= id;
        this.fechaini=fechaini;
        this.fechafin=fechafin;
        this.procesoName = "Indice despacho";
        this.des = "Reporte para generar el inidice de despacho";
        super.start();
    }    
    
    public synchronized void run(){
        try{
            Model model = new Model();
            model.LogProcesosSvc.InsertProceso(this.procesoName, this.hashCode(), des, this.id);
            List    lista = (List)    model.planillaService.obtenerDespacho (fechaini, fechafin);
            String fecha_actual = Util.getFechaActual_String(6);
            if (lista!=null && lista.size()>0){
                ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
                String path = rb.getString("ruta") + "/exportar/migracion/"+id;
                String a�o = fecha_actual.substring(0,4);
                String mes = fecha_actual.substring(5,7);
                String dia = fecha_actual.substring(8,10);
                String hora = fecha_actual.substring(11,13);
                String min = fecha_actual.substring(14,16);
                String seg = fecha_actual.substring(17,19);
                File file = new File(path);
                file.mkdirs();
                POIWrite xls = new POIWrite(path+"/DESPACHOS "+a�o+mes+dia+".xls","","");
                // fuente, tama�o, negrita, cursiva, formato, color, fondo, alineado
                HSSFCellStyle fecha  = xls.nuevoEstilo("verdana", 12, false , false, "yyyy/mm/dd"  , xls.NONE , xls.NONE , xls.NONE );
                HSSFCellStyle texto  = xls.nuevoEstilo("verdana", 12, false , false, "text"        , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_CENTER);
                HSSFCellStyle texto_fondo  = xls.nuevoEstilo("verdana", 12, false , false, "text"        , xls.NONE , HSSFColor.GREY_25_PERCENT.index , HSSFCellStyle.ALIGN_CENTER);
                HSSFCellStyle numero = xls.nuevoEstilo("verdana", 12, false , false, ""            , xls.NONE , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
                HSSFCellStyle negrita      = xls.nuevoEstilo("verdana", 12, true  , false, "text"        , HSSFColor.BLACK.index , xls.NONE , xls.NONE);
                HSSFCellStyle header      = xls.nuevoEstilo("verdana", 18, true  , false, "text"        , HSSFColor.WHITE.index , HSSFColor.ORANGE.index , HSSFCellStyle.ALIGN_CENTER);
                HSSFCellStyle titulo      = xls.nuevoEstilo("verdana", 12, true  , false, "text"        , HSSFColor.WHITE.index , HSSFColor.LIGHT_BLUE.index , HSSFCellStyle.ALIGN_CENTER);
                HSSFCellStyle fechatitle  = xls.nuevoEstilo("verdana", 12, true  , false, "yyyy/mm/dd"  , HSSFColor.WHITE.index , HSSFColor.DARK_GREEN.index , HSSFCellStyle.ALIGN_CENTER );
                
                
                xls.obtenerHoja("BASE");
                xls.cambiarMagnificacion(3,4);
                
                // subtitulos
                
                xls.adicionarCelda(0,0, "Fecha Proceso: "   , negrita);
                xls.adicionarCelda(0,1, fecha_actual , fecha);
                xls.adicionarCelda(1,0, "Elaborado Por: "   , negrita);
                xls.adicionarCelda(1,1, id , fecha);
                
                
                int fila = 3;
                int col  = 0;
                
                xls.adicionarCelda(fila ,col++ , "FECHA IMPRESION"                      , titulo );
                xls.adicionarCelda(fila ,col++ , "OC"                                   , titulo );
                xls.adicionarCelda(fila ,col++ , "CLIENTE"                              , titulo );
                xls.adicionarCelda(fila ,col++ , "TIPO DE VIAJE"                        , titulo );
                xls.adicionarCelda(fila ,col++ , "PLACA"                                , titulo );
                xls.adicionarCelda(fila ,col++ , "CONDUCTOR"                            , titulo );
                xls.adicionarCelda(fila ,col++ , "FECHA CREACION"                       , titulo );
                xls.adicionarCelda(fila ,col++ , "HORA CREACION"                        , titulo );
                xls.adicionarCelda(fila ,col++ , "USUARIO"                              , titulo );
                xls.adicionarCelda(fila ,col++ , "AGENCIA USUARIO"                      , titulo );
                xls.adicionarCelda(fila ,col++ , "AGENCIA ORIGEN"                       , titulo );
                xls.adicionarCelda(fila ,col++ , "FECHA FOTO PLACA"                     , titulo );
                xls.adicionarCelda(fila ,col++ , "HORA FOTO PLACA"                      , titulo );
                xls.adicionarCelda(fila ,col++ , "FECHA FOTO CONDUCTOR"                 , titulo );
                xls.adicionarCelda(fila ,col++ , "HORA FOTO CONDUCTOR"                  , titulo );                
                xls.adicionarCelda(fila ,col++ , "VEHICULO SIN FOTO"                    , titulo );
                xls.adicionarCelda(fila ,col++ , "CONDUCTOR SIN FOTO"                   , titulo );
                
                // datos
                String vehiculo="", conductor ="",fecha_placa="",hora_placa="",col_placa="",col_cond="";
                String fecha_conductor="", hora_conductor="", ced_conductor="", tipo_viaje="";
                Iterator it = lista.iterator();
                while(it.hasNext()){
                    
                    fila++;
                    col = 0;
                    
                    Planilla planilla = (Planilla) it.next();  
                    
                    vehiculo = (planilla.getFecha_placa().equals(""))?"VEH SIN FOTO":"VEH CON FOTO";
                    conductor = (planilla.getFecha_conductor().equals(""))?"COND SIN FOTO":"COND CON FOTO";
                    fecha_placa = (!planilla.getFecha_placa ().equals (""))?planilla.getFecha_placa ().substring(0,10):"";
                    hora_placa = (!planilla.getFecha_placa ().equals (""))?planilla.getFecha_placa ().substring(11,16):"";
                    fecha_conductor = (!planilla.getFecha_conductor ().equals (""))?planilla.getFecha_conductor ().substring(0,10):"";                    
                    hora_conductor = (!planilla.getFecha_conductor ().equals (""))?planilla.getFecha_conductor ().substring(11,16):"";                    
                    xls.adicionarCelda(fila ,col++ , planilla.getPrinter_date2().substring(0,10)            , texto );//fecha impresion
                    xls.adicionarCelda(fila ,col++ , planilla.getNumpla()                                   , texto );//numero de la planilla
                    xls.adicionarCelda(fila ,col++ , planilla.getNitpro()                                   , texto );//cliente
                    xls.adicionarCelda(fila ,col++ , model.planillaService.tipoViaje(planilla.getNumpla())  , texto );//tipo de viaje
                    xls.adicionarCelda(fila ,col++ , planilla.getPlaveh()                                   , (planilla.getFecha_placa().equals(""))?texto_fondo:texto );//placa
                    xls.adicionarCelda(fila ,col++ , planilla.getNomCond()                                  , (planilla.getFecha_conductor().equals(""))?texto_fondo:texto );//cedula conductor
                    xls.adicionarCelda(fila ,col++ , planilla.getFecdsp().substring(0,10)                   , texto );//fecha creacion
                    xls.adicionarCelda(fila ,col++ , planilla.getFecdsp().substring(11,16)                  , texto );//hora creacion
                    xls.adicionarCelda(fila ,col++ , planilla.getDespachador()                              , texto );//usuario
                    xls.adicionarCelda(fila ,col++ , planilla.getAgcpla()                                   , texto );//agencia usuario
                    xls.adicionarCelda(fila ,col++ , planilla.getNomori()                                   , texto );//agencia origen
                    xls.adicionarCelda(fila ,col++ , fecha_placa                                            , texto );//fecha foto placa
                    xls.adicionarCelda(fila ,col++ , hora_placa                                             , texto );//hora foto placa
                    xls.adicionarCelda(fila ,col++ , fecha_conductor                                        , texto );//fecha foto conductor
                    xls.adicionarCelda(fila ,col++ , hora_conductor                                         , texto );//hora foto conductor
                    xls.adicionarCelda(fila ,col++ , vehiculo                                               , texto );//vehiculo sin foto
                    xls.adicionarCelda(fila ,col++ , conductor                                              , texto );//conductor sin foto

                }
                
                xls.cerrarLibro();
            }
            model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(), this.id, "PROCESO EXITOSO");
        }
        catch(Exception e){
            try{
                Model model = new Model();
                model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(),this.id,"ERROR :" + e.getMessage());
            }
            catch(Exception f){
                try{
                    Model model = new Model();
                    model.LogProcesosSvc.finallyProceso(this.procesoName,this.hashCode(),this.id,"ERROR :");
                }catch(Exception p){    }
            }
        }
    }
    
}
