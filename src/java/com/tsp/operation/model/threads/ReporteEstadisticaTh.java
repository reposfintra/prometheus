/******************************************************************
* Nombre                        ReporteEstadisticaTh.java
* Descripci�n                   Clase Threads para la tabla est_carga_placa
* Autor                         ricardo rosero
* Fecha                         13/01/2006
* Versi�n                       1.0
* Coyright                      Transportes Sanchez Polo S.A.
*******************************************************************/

package com.tsp.operation.model.threads;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import java.io.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.controller.ReporteCumplidoAction;
import com.tsp.operation.model.services.*;
import com.tsp.operation.model.*;
import java.util.*;
import java.text.*;

/**
 *
 * @author  rrosero
 */
public class ReporteEstadisticaTh extends Thread{
    //atributos
    private Vector informe;
    private Vector resumen;
    private String user;
    private String mensaje;
    private String fechai;
    private String fechaf;

    /** Creates a new instance of ReporteEstadisticaTh */
    public ReporteEstadisticaTh() { 
    }
    
    /**
     * funcion que inicializa el hilo
     * @autor                       ricardo rosero
     * @param                       none
     * @throws                      SQLException
     * @version                     1.0
     */
    public void start(Vector informe, String user, String fechai, String fechaf){
            this.informe = informe;            
            this.user = user;
            this.fechaf = fechaf;
            this.fechai = fechai;
            
            super.start();
    }

    /**
     * funcion que ejecuta el hilo
     * @autor                       ricardo rosero
     * @param                       none
     * @throws                      SQLException
     * @version                     1.0
     */
    public synchronized void run(){
            try{
                    //Fecha del sistema             
                    Calendar FechaHoy = Calendar.getInstance();
                    Date d = FechaHoy.getTime();
                    SimpleDateFormat s = new SimpleDateFormat("yyyyMMdd");
                    SimpleDateFormat s1 = new SimpleDateFormat("yyyy-MM-dd-hh:mm");
                    SimpleDateFormat fec = new SimpleDateFormat("yyyy-MM-dd");            
                    String FechaFormated = s.format(d);
                    String FechaFormated1 = s1.format(d);
                    String Fecha = fec.format(d);


                    //obtener cabecera de ruta
                    ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
                    String path = rb.getString("ruta");
                    //armas la ruta
                    String Ruta1  = path + "/exportar/migracion/" + user + "/";
                    //crear la ruta
                    File file = new File(Ruta1);
                    file.mkdirs();

                    POIWrite xls = new POIWrite(Ruta1 +"ReporteEstadisticas_" + FechaFormated + ".xls", user, Fecha);
                    String nom_archivo = "ReporteEstadisticas_" + FechaFormated + ".xls";
                    this.mensaje = nom_archivo;

                    //Definici�n de Estilos
                    HSSFCellStyle fecha  = xls.nuevoEstilo("Book Antiqua", 9, false , false, "yyyy/mm/dd"  , xls.NONE , xls.NONE , xls.NONE );
                    HSSFCellStyle texto  = xls.nuevoEstilo("Book Antiqua", 9, false , false, "text"        , xls.NONE , xls.NONE , xls.NONE);
                    HSSFCellStyle numero = xls.nuevoEstilo("Book Antiqua", 9, false , false, ""            , xls.NONE , xls.NONE , xls.NONE);

                    HSSFCellStyle header1      = xls.nuevoEstilo("Arial", 10, true , false, "text"        , HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
                    HSSFCellStyle header2      = xls.nuevoEstilo("Arial", 12, true , false, "text"        , HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
                    HSSFCellStyle texto2      = xls.nuevoEstilo("Arial", 10, false , false, "text"        , HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
                    HSSFCellStyle titulo      = xls.nuevoEstilo("Book Antiqua", 9, true  , false, "text"        , HSSFColor.BLACK.index , HSSFColor.PINK.index , HSSFCellStyle.ALIGN_CENTER);
                    HSSFCellStyle fechatitle  = xls.nuevoEstilo("Book Antiqua", 9, true  , false, "yyyy/mm/dd"  , HSSFColor.RED.index , xls.NONE, HSSFCellStyle.ALIGN_CENTER );
                    HSSFCellStyle acpm_hd = xls.nuevoEstilo("Arial", 10, true , false, "text", HSSFColor.BLACK.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
                    HSSFCellStyle texto3  = xls.nuevoEstilo("Book Antiqua", 9, false , false, "text", xls.NONE , xls.NONE , xls.NONE);
                    HSSFCellStyle celda = xls.nuevoEstilo("Book Antiqua", 9, false , false, "text", xls.NONE , xls.NONE , xls.NONE);

                    header1.setBorderTop(HSSFCellStyle.BORDER_MEDIUM);
                    header1.setBorderBottom(HSSFCellStyle.BORDER_THIN);
                    header1.setAlignment(HSSFCellStyle.ALIGN_CENTER);
                    header2.setAlignment(HSSFCellStyle.ALIGN_CENTER);

                    acpm_hd.setAlignment(HSSFCellStyle.ALIGN_CENTER);
                    acpm_hd.setBorderTop(HSSFCellStyle.BORDER_MEDIUM);
                    acpm_hd.setBorderBottom(HSSFCellStyle.BORDER_MEDIUM);
                    acpm_hd.setBorderLeft(HSSFCellStyle.BORDER_MEDIUM);
                    acpm_hd.setBorderRight(HSSFCellStyle.BORDER_MEDIUM);

                    texto3.setBorderTop(HSSFCellStyle.BORDER_MEDIUM);
                    texto3.setBorderBottom(HSSFCellStyle.BORDER_MEDIUM);
                    texto3.setBorderLeft(HSSFCellStyle.BORDER_MEDIUM);
                    texto3.setBorderRight(HSSFCellStyle.BORDER_MEDIUM);
                    texto3.setAlignment(HSSFCellStyle.ALIGN_RIGHT);
                    celda.setBorderLeft(HSSFCellStyle.BORDER_MEDIUM);


                    xls.obtenerHoja("Reporte Estadisticas Carga Placa");
                    xls.cambiarMagnificacion(3,4);

                    // cabecera

                    xls.combinarCeldas(2, 0, 2, 10);
                    xls.combinarCeldas(4, 0, 4, 10);
                    xls.combinarCeldas(6, 0, 6, 10);
                    xls.adicionarCelda(2, 0, "TRANSPORTES SANCHEZ POLO S.A.", header2);
                    xls.adicionarCelda(4, 0, "Reporte de Estadisticas", header2);
                    xls.adicionarCelda(6, 0, "Reporte de fecha: " + Fecha , texto2);

                    // subtitulos
                    
                    int fila = 8;
                    int col  = 0;
                    
                    xls.adicionarCelda(fila, col++, "PLACA", header1);
                    xls.adicionarCelda(fila, col++, "CARGA", header1);
                    xls.adicionarCelda(fila, col++, "VIAJES", header1);
                    xls.adicionarCelda(fila, col++, "PERIODO", header1);
                    xls.adicionarCelda(fila, col++, "FECHA", header1);
                    xls.adicionarCelda(fila, col++, "LAST-UPDATE", header1);
                    xls.adicionarCelda(fila, col++, "USER-UPDATE", header1);
                    xls.adicionarCelda(fila, col++, "CREATION-DATE", header1);
                    xls.adicionarCelda(fila, col++, "CREATION-USER", header1);
                    xls.adicionarCelda(fila, col++, "DISTRITO", header1);
                    xls.adicionarCelda(fila, col++, "BASE", header1);
                    
                    fila = 9;
                    for(int i=0; i<informe.size(); i++){
                        EstCargaPlaca ecp = (EstCargaPlaca) informe.elementAt(i);
                        col = 0;
                        xls.adicionarCelda(fila, col++, ecp.getPlaca(), texto);
                        xls.adicionarCelda(fila, col++, ecp.getCarga(), texto);
                        xls.adicionarCelda(fila, col++, ecp.getViajes(), texto);
                        xls.adicionarCelda(fila, col++, ecp.getPeriodo(), texto);
                        xls.adicionarCelda(fila, col++, ecp.getFecha(), texto);
                        xls.adicionarCelda(fila, col++, ecp.getFM(), texto);
                        xls.adicionarCelda(fila, col++, ecp.getUM(), texto);
                        xls.adicionarCelda(fila, col++, ecp.getFC(), texto);
                        xls.adicionarCelda(fila, col++, ecp.getUC(), texto);
                        xls.adicionarCelda(fila, col++, ecp.getDistrito(), texto);
                        xls.adicionarCelda(fila, col++, ecp.getBase(), texto);
                        fila++;                                
                    }
                    
                    xls.cerrarLibro();

            }catch (Exception ex){
                            ////System.out.println("ERROR AL GENERAR EL ARCHIVO XLS : " + ex.getMessage());
            }

    }

    /**
     * Getter for property mensaje.
     * @return Value of property mensaje.
     */
    public java.lang.String getMensaje() {
        return mensaje;
    }

    /**
     * Setter for property mensaje.
     * @param mensaje New value of property mensaje.
     */
    public void setMensaje(java.lang.String mensaje) {
        this.mensaje = mensaje;
    }


}
