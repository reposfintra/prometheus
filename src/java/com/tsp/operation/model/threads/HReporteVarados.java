/*
 * Nombre        HReporteVarados.java
 * Autor         Osvaldo P�rez Ferrer
 * Fecha         9 de noviembre de 2006, 09:55 AM
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */

package com.tsp.operation.model.threads;

import java.util.*;
import java.io.*;
import java.lang.*;
import java.text.*;

import com.tsp.operation.model.*;
import com.tsp.operation.model.beans.POIWrite;

import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.*;

public class HReporteVarados extends Thread{
    
    /**
     * Crea una nueva instancia de  HReporteVarados
     */
    public HReporteVarados() {
    }
    
    private Vector datos;
    private String path;
    
    Usuario user;
    String inicio;
    String fin;
    String   ruta;
    POIWrite xls;
    HSSFCellStyle header  , titulo1, titulo2, titulo3 , titulo4, letra, numero, porcentaje, letraCentrada, numeroNegrita, letraDerecha;
    HSSFColor     cAzul   , cVerde, cAmarillo, cGris ;
    int           fila = 0;
    SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
    
    String[] tipos;
    String[] titulos;
    
    private Model model = new Model();
    private String procesoName;
    private  PrintWriter pw;
    private LogWriter   logTrans;
    
    public void generarReporteVarados(){
        try{
            
            //f.cuadroAzul( user.getDstrct(), tipodoc, formato , documento);
            
            String comentario="EXITOSO";
            model.LogProcesosSvc.InsertProceso( this.procesoName, this.hashCode(),"Reporte de Varados", this.user.getLogin() );
            
            if(datos.size()>0){
                
                ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
                ruta = rb.getString("ruta") + "/exportar/migracion/"+user.getLogin() ;
                File archivo = new File( ruta );
                if (!archivo.exists()) archivo.mkdirs();
                
                String fecha = Util.getFechaActual_String(6);
                fecha = fecha.replaceAll("/","-");
                fecha = fecha.replaceAll(":","");
                this.crearArchivo("ReporteVarados "+fecha,"REPORTE DE VEHICULOS VARADOS");
                this.escribirEnArchivo(datos);
                this.cerrarArchivo();
                /************************************************************************************/
                /**** GUARDAR DATOS EN EL ARCHIVO  ***/
                
                model.LogProcesosSvc.finallyProceso(this.procesoName,this.hashCode(),  this.user.getLogin() ,comentario);
                
            }
        }catch(Exception ex){
            ex.printStackTrace();
            //System.out.println("error en ReporteFormatoXLS: "+ex.getMessage());
            try{
                model.LogProcesosSvc.finallyProceso(this.procesoName,this.hashCode(),  this.user.getLogin() ,"ERROR Hilo: " + ex.getMessage());
            }catch (Exception e){}
        }
    }
    
    public void InitArchivo(String nameFile) throws Exception{
        try{
            
            xls          = new com.tsp.operation.model.beans.POIWrite();
            xls.nuevoLibro( ruta + "/" + nameFile + ".xls" );
            
            // colores
            cAzul       = xls.obtenerColor(204,255,255);
            cVerde      = xls.obtenerColor( 51,153,102);
            cAmarillo   = xls.obtenerColor(255,255,153);
            cGris       = xls.obtenerColor(192,192,192);
            
            // estilos
            header       = xls.nuevoEstilo("Tahoma", 14, true  , false, "text"  , HSSFColor.ORANGE.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
            titulo1      = xls.nuevoEstilo("Tahoma", 8 , true  , false, "text"  , xls.NONE  , xls.NONE , xls.NONE);
            titulo2      = xls.nuevoEstilo("Tahoma", 8 , true  , false, "text"  , HSSFColor.BLACK.index, cAzul.getIndex() , HSSFCellStyle.ALIGN_CENTER, 1);
            titulo3      = xls.nuevoEstilo("Tahoma", 8 , true  , false, "text"  , HSSFColor.BLACK.index, cAmarillo.getIndex() , HSSFCellStyle.ALIGN_CENTER, 1);
            titulo4      = xls.nuevoEstilo("Tahoma", 8 , true  , false, "text"  , HSSFColor.WHITE.index, cVerde.getIndex() , HSSFCellStyle.ALIGN_CENTER, 2);
            letra        = xls.nuevoEstilo("Tahoma", 8 , false , false, ""      , xls.NONE , xls.NONE , xls.NONE);
            letraCentrada= xls.nuevoEstilo("Tahoma", 8 , false , false, ""      , xls.NONE , HSSFColor.WHITE.index , HSSFCellStyle.ALIGN_CENTER,1);
            letraDerecha = xls.nuevoEstilo("Tahoma", 8 , false , false, ""      , xls.NONE , cAzul.getIndex() , HSSFCellStyle.ALIGN_RIGHT,1);
            numero       = xls.nuevoEstilo("Tahoma", 8 , false , false, "_(#,##0.00_);(@_)"      , xls.NONE , xls.NONE , xls.NONE);
            numeroNegrita= xls.nuevoEstilo("Tahoma", 8 , true  , false, "_($* #,##0.00_);(@_)"      , xls.NONE , xls.NONE , xls.NONE);
            porcentaje   = xls.nuevoEstilo("Tahoma", 8 , false , false, "0.00%" , xls.NONE , xls.NONE , xls.NONE);
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception("Error en InitArchivo .... \n" + ex.getMessage());
        }
    }
    private void crearArchivo(String nameFile, String titulo) throws Exception{
        try{
            
            InitArchivo(nameFile);
            xls.obtenerHoja("Base");
            //xls.combinarCeldas(0, 0, 0, 14);
            //xls.combinarCeldas(1, 0, 1, 14);
            xls.combinarCeldas(2, 0, 2, 14);
            xls.adicionarCelda(0, 0, "TRANSPORTES SANCHEZ POLO S.A.", header);
            xls.adicionarCelda(1, 0, titulo, header);
            
            xls.adicionarCelda(0, 4, "Fecha Inicial: "+this.inicio, titulo1);
            xls.adicionarCelda(1, 4, "Fecha Final: "+this.fin, titulo1);
            
            //xls.adicionarCelda(1, 0, "Usuario " + usuario.getNombre() , titulo1);
            xls.adicionarCelda(2, 0, "Generado: " + fmt.format( new Date() ) , titulo1);
            
            
            int col = 0;
            
            short [] dimensiones = new short[20];
            
            
            for( int i=0; i<20; i++ ){
                dimensiones[i] = (short)( 4 * 1000 );
            }
            
            fila=4;
            
            xls.cambiarAnchoColumna(0, 3000);
            
            String[] titulos = {"CLIENTE","OT'S","DESCRIPCION OT","RUTA OT","AGC DESP",//5
            "OC","RUTA OC","PLACA","FECHA REP","HORA REP",//10
            "PUESTO CONTROL","SIG FEC","SIG HOR","SIG PTO","OBSERVACION",//15
            "TIEMPO VARADO", "FEC DESPACHO", "HORA DESPACHO","FEC ENTREGA", "HORA ENTREGA" };//20
            
            
            
            for( int i = 0; i<20; i++ ){
                
                xls.adicionarCelda(fila,  i, titulos[i], titulo2);
                xls.cambiarAnchoColumna(i, dimensiones [i]);
                
            }
            
            fila++;
            
            
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception( "Error en crearArchivo ....\n"  + ex.getMessage() );
        }
    }
    
    
    private void escribirEnArchivo(Vector datos) throws Exception{
        
        try{
            
            String[] mon;
            for(int i = 0; i<datos.size(); i++){
                
                int col = 0 ;
                Hashtable h = (Hashtable)datos.get(i);
                
                xls.adicionarCelda(fila, col++,  nvl( (String)h.get("cliente") ), letraCentrada ) ;
                xls.adicionarCelda(fila, col++,  nvl( (String)h.get("ot") ), letraCentrada );
                xls.adicionarCelda(fila, col++,  nvl( (String)h.get("desc_ot") ), letraCentrada );
                xls.adicionarCelda(fila, col++,  nvl( (String)h.get("ruta_ot") ), letraCentrada );
                xls.adicionarCelda(fila, col++,  nvl( (String)h.get("ag_desp")  ), letraCentrada );
                xls.adicionarCelda(fila, col++,  nvl( (String)h.get("oc") ), letraCentrada );
                xls.adicionarCelda(fila, col++,  nvl( (String)h.get("ruta_oc") ), letraCentrada );
                xls.adicionarCelda(fila, col++,  nvl( (String)h.get("placa") ), letraCentrada );
                xls.adicionarCelda(fila, col++,  nvl( (String)h.get("fecha_reporte") ), letraCentrada );
                xls.adicionarCelda(fila, col++,  nvl( (String)h.get("hora_reporte") ), letraCentrada );
                xls.adicionarCelda(fila, col++,  nvl( (String)h.get("puesto_control") ), letraCentrada );
                xls.adicionarCelda(fila, col++,  nvl( (String)h.get("sig_fec") ), letraCentrada );
                xls.adicionarCelda(fila, col++,  nvl( (String)h.get("sig_hor") ), letraCentrada );
                xls.adicionarCelda(fila, col++,  nvl( (String)h.get("sig_pto") ), letraCentrada );
                xls.adicionarCelda(fila, col++,  nvl( (String)h.get("observacion") ), letraCentrada );
                
                String tiempo = nvl( (String)h.get("tiempo") );
                String horas = !(tiempo).equals("")? UtilFinanzas.customFormat( "#,##0.00", Util.toHours(tiempo), 2 ) : "";
                xls.adicionarCelda(fila, col++,  horas , letraCentrada );
                xls.adicionarCelda(fila, col++,  nvl( (String)h.get("fecha_despacho") ), letraCentrada );
                xls.adicionarCelda(fila, col++,  nvl( (String)h.get("hora_despacho") ), letraCentrada );
                xls.adicionarCelda(fila, col++,  nvl( (String)h.get("fecha_entrega") ), letraCentrada );
                xls.adicionarCelda(fila, col++,  nvl( (String)h.get("hora_entrega") ), letraCentrada );
                
                fila++;
            }
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
        
    }
    
    private void cerrarArchivo() throws Exception {
        try{
            if (xls!=null)
                xls.cerrarLibro();
        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception( "Error en crearArchivo ....\n"  + ex.getMessage() );
        }
    }
    
    public String nvl(String value){
        return value!=null? value : "";
    }
    
    
    
    public void start(  Usuario user, Vector datos, String inicio, String fin ) throws Exception{
        
        this.datos = datos;
        this.user    = user;
        this.inicio = inicio;
        this.fin = fin;
        this.procesoName = "Reporte de Varados";
        super.start();
    }
    
    public void run(){
        
        this.generarReporteVarados();
    }
    
}
