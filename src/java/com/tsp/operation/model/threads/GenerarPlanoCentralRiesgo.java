/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.model.threads;

import com.tsp.operation.model.Model;
import com.tsp.operation.model.beans.Usuario;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;

/**
 *
 * @author desarrollo
 */
public class GenerarPlanoCentralRiesgo extends Thread {
    
    private String info;
    private Model model;
    private Usuario usuario;
    private String processName,ruta,codCentralRiesgo;
    private int cantReg;
    private String fecha;

    public void start(Model model,String info, Usuario usuario,String processName, String codCentralRiesgo, int cantReg, String fecha ){
        this.info = info;
        this.usuario = usuario;
        this.model = model;
        this.processName=processName;
        this.codCentralRiesgo=codCentralRiesgo;
        this.cantReg=cantReg;
        this.fecha=fecha;
        super.start();
    }

    public synchronized void run(){
        try{
            model.LogProcesosSvc.InsertProceso(this.processName, this.hashCode(), "Exportacion TXT", usuario.getLogin());
            this.generarRUTA();
            this.generarArchivo();
            model.LogProcesosSvc.finallyProceso(this.processName, this.hashCode(), usuario.getLogin(), "PROCESO EXITOSO");
       
       
        
        }catch (Exception ex){
            ex.printStackTrace();
            try{
                model.LogProcesosSvc.finallyProceso(this.processName, this.hashCode(), usuario.getLogin(), "Finalizado con error ...\n" + ex.getMessage());
            }catch (Exception e){
                System.out.println("Error "+this.processName +"...\n" + e.getMessage());
            }
        }
    }


    public void generarRUTA() throws Exception{
        try{

            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            ruta = rb.getString("ruta") + "/exportar/migracion/" + usuario.getLogin();
            File archivo = new File( ruta );
            if (!archivo.exists()) archivo.mkdirs();

        }catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }

    }

    private void generarArchivo() throws Exception {
        String url = ruta + "/" + processName +"T.txt";
        PrintWriter pw = new PrintWriter(new BufferedWriter(new OutputStreamWriter(new FileOutputStream(url), "ISO-8859-1")));
        pw.print("HHHHHHHHHHHHHHHHHH" + this.codCentralRiesgo +this.codCentralRiesgo.substring(0, 2)+ fecha + "M T0000000000000000N");
        pw.println();
        pw.print(info);
        String cant = String.format("%08d", this.cantReg+2);
        pw.print("ZZZZZZZZZZZZZZZZZZ" + fecha + cant + "00000000");
        pw.close();
    }
    
}
