   /***************************************
    * Nombre Clase ............. HReporteConsultaAnticipos.java
    * Descripci�n  .. . . . . .  Permite Generar Reporte de la consulta
    * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
    * Fecha . . . . . . . . . .  22/08/2006
    * versi�n . . . . . . . . .  1.0
    * Copyright ...Transportes Sanchez Polo S.A.
    *******************************************/



package com.tsp.operation.model.threads;





import com.tsp.operation.model.beans.POIWrite;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;

import java.io.*;
import java.util.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.util.*;



public class HReporteConsultaAnticipos  extends Thread{

     private  Model        model;
    private  Usuario      usuario ;
    private  String       procesoName;
    private String opcion="";


    private String          url;
    private POIWrite        Excel;
    private String          hoja;
    private int             fila;
    private int             columna;

    private HSSFCellStyle   texto, negrilla, numero, titulo, porcentaje;
    private HSSFCellStyle   grupoA, grupoB, grupoC, grupoD;





    /***********************datos filtro consultas******************************/
private String distrito, proveedor, ckAgencia, Agencia, ckPropietario, Propietario, ckPlanilla, Planilla, ckPlaca, Placa, ckConductor, Conductor,  ckReanticipo, reanticipo,  ckFechas, fechaIni, fechaFin;
   private String     ckLiquidacion, Liquidacion, ckTransferencia, Transferencia, ckFactura, Factura;
    public HReporteConsultaAnticipos() {
    }



    /**
     * M�todo inicializa  el proceso
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public void start(Model modelo,  Usuario user,String op,String distritox,
            String proveedorx, String ckAgenciax, String Agenciax, String ckPropietariox, String Propietariox, String ckPlanillax,
            String Planillax, String ckPlacax, String Placax, String ckConductorx,
            String Conductorx,  String ckReanticipox, String reanticipox,  String ckFechasx, String fechaInix, String fechaFinx,
            String ckLiquidacionx, String Liquidacionx,  String ckTransferenciax, String Transferenciax, String ckFacturax, String  Facturax) throws Exception{
         try{
            this.model       = modelo;
            this.usuario     = user;
            this.procesoName = "GENERACION DE REPORTE CONSULTA ANTICIPOS ";
            this.opcion=op;





           this.distrito=distritox;
          this.proveedor = proveedorx;
           this.ckAgencia =ckAgenciax;
           this.Agencia = Agenciax;
           this.ckPropietario =ckPropietariox;
           this.Propietario= Propietariox;
            this.ckPlanilla=ckPlanillax;
            this.Planilla=Planillax;
           this.ckPlaca=ckPlacax;
           this.Placa= Placax;
           this.ckConductor=ckConductorx;
           this.Conductor= Conductorx;
           this.ckReanticipo=ckReanticipox;
           this.reanticipo=reanticipox;
           this.ckFechas=ckFechasx;
           this.fechaIni=fechaInix;
           this.fechaFin=fechaFinx;



            super.start();

        }catch(Exception e){
           throw new Exception( e.getMessage() );
       }
    }


     
    public void start(Model modelo,  Usuario user) throws Exception{
         try{
            this.model       = modelo;
            this.usuario     = user;
            this.procesoName = "GENERACION DE REPORTE CONSULTA ANTICIPOS ";

            super.start();

        }catch(Exception e){
           throw new Exception( e.getMessage() );
       }
    }




    /**
     * M�todo que ejecuta el proceso
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
     public synchronized void run(){
       try{


           if(opcion.equals("consulta"))
           {
            model.ConsultaAnticiposTercerosSvc.searchAnticipos(distrito, proveedor, ckAgencia, Agencia, ckPropietario, Propietario, ckPlanilla, Planilla, ckPlaca, Placa, ckConductor, Conductor,  ckReanticipo, reanticipo,  ckFechas, fechaIni, fechaFin,  ckLiquidacion, Liquidacion, ckTransferencia, Transferencia, ckFactura, Factura );
            List lista = model.ConsultaAnticiposTercerosSvc.getLista();
           }

           String comentario="EXITOSO";
           model.LogProcesosSvc.InsertProceso( this.procesoName, this.hashCode()," Reporte Produccion ", this.usuario.getLogin() );


           configXLS();
           createHoja("Consulta");
           titulo();


           List  lista  = model.ConsultaAnticiposTercerosSvc.getLista();
           for(int i=0;i<lista.size();i++){
                AnticiposTerceros anticipo = (AnticiposTerceros) lista.get(i);
                addAnticipo(anticipo);
           }



           Excel.cerrarLibro();

           model.LogProcesosSvc.finallyProceso(this.procesoName,this.hashCode(),  this.usuario.getLogin() ,comentario);


       }catch(Exception e){
           try{
               model.LogProcesosSvc.finallyProceso(this.procesoName,this.hashCode(),  this.usuario.getLogin() ,"ERROR Hilo: " + e.getMessage());
           }
           catch(Exception f){ }
       }

    }





      /**
     * M�todo que establece titulos xls
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public void titulo()throws Exception{
        try{


            Excel.combinarCeldas( fila, 0, fila, 3);  Excel.adicionarCelda( this.fila, this.columna, "FINTRA S.A.",           this.negrilla ); incFila();
            Excel.combinarCeldas( fila, 0, fila, 3);  Excel.adicionarCelda( this.fila, this.columna, "REPORTE DE CONSULTA",   this.negrilla ); incFila();

            incFila();
            incFila();

            Excel.combinarCeldas( fila, 0,  fila, 11);   Excel.adicionarCelda( this.fila, 0,  "DATOS ANTICIPO",          this.grupoA );
            Excel.combinarCeldas( fila, 10, fila, 11);  Excel.adicionarCelda( this.fila, 10, "ESTADO ANTICIPO",         this.grupoB );
            Excel.combinarCeldas( fila, 12, fila, 22);  Excel.adicionarCelda( this.fila, 12, "DATOS TRANSFERENCIA",     this.grupoC );
            Excel.combinarCeldas( fila, 23, fila, 34);  Excel.adicionarCelda( this.fila, 23, "DATOS MIGRACION A MIMS",  this.grupoD );

            incFila();
            setearColumna();

            Excel.cambiarAnchoColumna( columna, 3000 );  Excel.adicionarCelda( this.fila, this.columna, "ESTADO",       this.titulo); incColumna();
            Excel.cambiarAnchoColumna( columna, 3000 );  Excel.adicionarCelda( this.fila, this.columna, "OBSERVACION ANULACION",this.titulo); incColumna();
            Excel.cambiarAnchoColumna( columna, 3000 );  Excel.adicionarCelda( this.fila, this.columna, "TIPO REGISTRO",       this.titulo); incColumna();
            Excel.cambiarAnchoColumna( columna, 3000 );  Excel.adicionarCelda( this.fila, this.columna, "AGENCIA",             this.titulo); incColumna();
            Excel.cambiarAnchoColumna( columna, 3000 );  Excel.adicionarCelda( this.fila, this.columna, "CONDUCTOR",           this.titulo); incColumna();
            Excel.cambiarAnchoColumna( columna, 6000 );  Excel.adicionarCelda( this.fila, this.columna, "NOMBRE CONDUCTOR",    this.titulo); incColumna();
            Excel.cambiarAnchoColumna( columna, 2000 );  Excel.adicionarCelda( this.fila, this.columna, "PROPIETARIO",         this.titulo); incColumna();
            Excel.cambiarAnchoColumna( columna, 4000 );  Excel.adicionarCelda( this.fila, this.columna, "NOMBRE PROPIETARIO",  this.titulo); incColumna();
            Excel.cambiarAnchoColumna( columna, 4000 );  Excel.adicionarCelda( this.fila, this.columna, "PLACA",               this.titulo); incColumna();
            Excel.cambiarAnchoColumna( columna, 4000 );  Excel.adicionarCelda( this.fila, this.columna, "PLANILLA",            this.titulo); incColumna();
            
            Excel.cambiarAnchoColumna( columna, 4000 );  Excel.adicionarCelda( this.fila, this.columna, "FECHA ANTICIPO",      this.titulo); incColumna();
            Excel.cambiarAnchoColumna( columna, 3000 );  Excel.adicionarCelda( this.fila, this.columna, "CALCULO DE DIFERENCIAS",      this.titulo); incColumna();
            Excel.cambiarAnchoColumna( columna, 3000 );  Excel.adicionarCelda( this.fila, this.columna, "RANGO DIFERENCIA",      this.titulo); incColumna();
            Excel.cambiarAnchoColumna( columna, 4000 );  Excel.adicionarCelda( this.fila, this.columna, "FECHA ENVIO A FINTRA",this.titulo); incColumna();
            Excel.cambiarAnchoColumna( columna, 3000 );  Excel.adicionarCelda( this.fila, this.columna, "VALOR ANTICIPO",      this.titulo); incColumna();
            Excel.cambiarAnchoColumna( columna, 3000 );  Excel.adicionarCelda( this.fila, this.columna, "REANTICIPO",          this.titulo); incColumna();

            Excel.cambiarAnchoColumna(columna, 3000);            Excel.adicionarCelda(this.fila, this.columna, "ASESOR", this.titulo);            incColumna();//20100728
            Excel.cambiarAnchoColumna(columna, 3000);            Excel.adicionarCelda(this.fila, this.columna, "REFERENCIADO", this.titulo);            incColumna();//20100728
            Excel.cambiarAnchoColumna(columna, 3000);            Excel.adicionarCelda(this.fila, this.columna, "USER_CREACION", this.titulo);            incColumna();//20100728


            Excel.cambiarAnchoColumna( columna, 3000 );  Excel.adicionarCelda( this.fila, this.columna, "APROBADO",            this.titulo); incColumna();
            Excel.cambiarAnchoColumna( columna, 3000 );  Excel.adicionarCelda( this.fila, this.columna, "TRANSFERIDO",         this.titulo); incColumna();
            Excel.cambiarAnchoColumna( columna, 3000 );  Excel.adicionarCelda( this.fila, this.columna, "BANCO TRANSFERENCIA", this.titulo); incColumna();
            Excel.cambiarAnchoColumna( columna, 3000 );  Excel.adicionarCelda( this.fila, this.columna, "CUENTA TRANSFERENCA", this.titulo); incColumna();
            Excel.cambiarAnchoColumna( columna, 3000 );  Excel.adicionarCelda( this.fila, this.columna, "TIPO CUENTA TRANSFERENCIA", this.titulo); incColumna();


            Excel.cambiarAnchoColumna( columna, 3000 );  Excel.adicionarCelda( this.fila, this.columna, "BANCO",               this.titulo); incColumna();
            Excel.cambiarAnchoColumna( columna, 3000 );  Excel.adicionarCelda( this.fila, this.columna, "CUENTA",              this.titulo); incColumna();
            Excel.cambiarAnchoColumna( columna, 3000 );  Excel.adicionarCelda( this.fila, this.columna, "TIPO CTA",            this.titulo); incColumna();
            Excel.cambiarAnchoColumna( columna, 3000 );  Excel.adicionarCelda( this.fila, this.columna, "NOMBRE CTA",          this.titulo); incColumna();
            Excel.cambiarAnchoColumna( columna, 3000 );  Excel.adicionarCelda( this.fila, this.columna, "NIT CTA",             this.titulo); incColumna();
            Excel.cambiarAnchoColumna( columna, 3000 );  Excel.adicionarCelda( this.fila, this.columna, "% DESCUENTO",         this.titulo); incColumna();
            Excel.cambiarAnchoColumna( columna, 3000 );  Excel.adicionarCelda( this.fila, this.columna, "VALOR DESCUENTO",     this.titulo); incColumna();
            Excel.cambiarAnchoColumna( columna, 3000 );  Excel.adicionarCelda( this.fila, this.columna, "VALOR NETO",          this.titulo); incColumna();
            Excel.cambiarAnchoColumna( columna, 3000 );  Excel.adicionarCelda( this.fila, this.columna, "VALOR COMISION",      this.titulo); incColumna();
            Excel.cambiarAnchoColumna( columna, 3000 );  Excel.adicionarCelda( this.fila, this.columna, "VALOR CONSIGNADO",    this.titulo); incColumna();
            Excel.cambiarAnchoColumna( columna, 3000 );  Excel.adicionarCelda( this.fila, this.columna, "FECHA TRANSFERENCIA", this.titulo); incColumna();
            
            Excel.cambiarAnchoColumna( columna, 3000 );  Excel.adicionarCelda( this.fila, this.columna, "PERIODO CONTABILIZADO", this.titulo); incColumna();
            Excel.cambiarAnchoColumna( columna, 3000 );  Excel.adicionarCelda( this.fila, this.columna, "DOCUMENTO CONTABLE", this.titulo); incColumna();


            Excel.cambiarAnchoColumna( columna, 3000 );  Excel.adicionarCelda( this.fila, this.columna, "FECHA MIGRACION",      this.titulo); incColumna();
            Excel.cambiarAnchoColumna( columna, 3000 );  Excel.adicionarCelda( this.fila, this.columna, "FACTURA MIMS",         this.titulo); incColumna();

            Excel.cambiarAnchoColumna( columna, 3000 );  Excel.adicionarCelda( this.fila, this.columna, "VLR TERCERO",          this.titulo); incColumna();
            Excel.cambiarAnchoColumna( columna, 3000 );  Excel.adicionarCelda( this.fila, this.columna, "ESTADO FACTURA TER",   this.titulo); incColumna();
            Excel.cambiarAnchoColumna( columna, 3000 );  Excel.adicionarCelda( this.fila, this.columna, "FECHA PAGO TER",       this.titulo); incColumna();
            Excel.cambiarAnchoColumna( columna, 3000 );  Excel.adicionarCelda( this.fila, this.columna, "CHEQUE FAC TER",       this.titulo); incColumna();
            Excel.cambiarAnchoColumna( columna, 3000 );  Excel.adicionarCelda( this.fila, this.columna, "CORRIDA FAC TER",      this.titulo); incColumna();

            Excel.cambiarAnchoColumna( columna, 3000 );  Excel.adicionarCelda( this.fila, this.columna, "VLR DESC PROPIETARIO", this.titulo); incColumna();
            Excel.cambiarAnchoColumna( columna, 3000 );  Excel.adicionarCelda( this.fila, this.columna, "ESTADO FACTURA PRO",   this.titulo); incColumna();
            Excel.cambiarAnchoColumna( columna, 3000 );  Excel.adicionarCelda( this.fila, this.columna, "FECHA DESC PRO",       this.titulo); incColumna();
            Excel.cambiarAnchoColumna( columna, 3000 );  Excel.adicionarCelda( this.fila, this.columna, "CHEQUE FAC PRO",       this.titulo); incColumna();
            Excel.cambiarAnchoColumna( columna, 3000 );  Excel.adicionarCelda( this.fila, this.columna, "CORRIDA FAC PRO",      this.titulo); incColumna();
            Excel.cambiarAnchoColumna( columna, 3000 );  Excel.adicionarCelda( this.fila, this.columna, "FECHA TRANSFERENCIA GASOLINA",       this.titulo); incColumna();
            Excel.cambiarAnchoColumna( columna, 3000 );  Excel.adicionarCelda( this.fila, this.columna, "USUARIO TRANSFERENCIA GASOLINA",      this.titulo); incColumna();

            incFila();
            setearColumna();

        }catch(Exception e){
           throw new Exception( e.getMessage() );
       }
    }





    /**
     * M�todo que adiciona informacion del anticipo al reporte
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public void addAnticipo(AnticiposTerceros anticipo)throws Exception{
        try{

            Excel.adicionarCelda( this.fila, this.columna, anticipo.getDes_concept()             ,this.texto);  incColumna();
            Excel.adicionarCelda( this.fila, this.columna, anticipo.getReg_status()           ,this.texto);  incColumna();
            Excel.adicionarCelda( this.fila, this.columna, anticipo.getObs_anulacion()           ,this.texto);  incColumna();
            Excel.adicionarCelda( this.fila, this.columna, anticipo.getNombreAgencia()           ,this.texto);  incColumna();
            Excel.adicionarCelda( this.fila, this.columna, anticipo.getConductor()               ,this.texto);  incColumna();
            Excel.adicionarCelda( this.fila, this.columna, anticipo.getNombreConductor()         ,this.texto);  incColumna();
            Excel.adicionarCelda( this.fila, this.columna, anticipo.getPla_owner()               ,this.texto);  incColumna();
            Excel.adicionarCelda( this.fila, this.columna, anticipo.getNombrePropietario()       ,this.texto);  incColumna();
            Excel.adicionarCelda( this.fila, this.columna, anticipo.getSupplier()                ,this.texto);  incColumna();
            Excel.adicionarCelda( this.fila, this.columna, anticipo.getPlanilla()                ,this.texto);  incColumna();
            
            Excel.adicionarCelda( this.fila, this.columna, anticipo.getFecha_anticipo()         ,this.texto);  incColumna();
            Excel.adicionarCelda( this.fila, this.columna, anticipo.getDiferencia() ,this.texto); incColumna();
            Excel.adicionarCelda( this.fila, this.columna, anticipo.getRango_diferencia(),this.texto); incColumna();

            Excel.adicionarCelda( this.fila, this.columna, anticipo.getFecha_envio()             ,this.texto);  incColumna();
            Excel.adicionarCelda( this.fila, this.columna, anticipo.getVlr()                     ,this.numero); incColumna();
            Excel.adicionarCelda( this.fila, this.columna, anticipo.getReanticipo()              ,this.texto);  incColumna();

            //inicio de 20100728
            Excel.adicionarCelda(this.fila, this.columna, anticipo.getAsesor(), this.texto);
            incColumna();
            Excel.adicionarCelda(this.fila, this.columna, anticipo.getReferenciado(), this.texto);
            incColumna();
            Excel.adicionarCelda(this.fila, this.columna, anticipo.getUsuario_creacion(), this.texto);
            incColumna();
            //fin de 20100728

            String banco_transfer = "";
            if (anticipo.getBanco_transferencia().equals("07")){
                banco_transfer = "BANCOLOMBIA";
            }else if(anticipo.getBanco_transferencia().equals("23")){
                 banco_transfer = "OCCIDENTE";
            }else{
                banco_transfer = anticipo.getBanco_transferencia();
            }

            Excel.adicionarCelda( this.fila, this.columna, anticipo.getAprobado()                ,this.texto);  incColumna();
            Excel.adicionarCelda( this.fila, this.columna, anticipo.getTransferido()             ,this.texto);  incColumna();
            Excel.adicionarCelda( this.fila, this.columna, banco_transfer                       ,this.texto);  incColumna();
            Excel.adicionarCelda( this.fila, this.columna, anticipo.getCuenta_transferencia()     ,this.texto);  incColumna();
            Excel.adicionarCelda( this.fila, this.columna, anticipo.getTcta_transferencia()        ,this.texto);  incColumna();

            Excel.adicionarCelda( this.fila, this.columna, anticipo.getBanco()                   ,this.texto);  incColumna();
            Excel.adicionarCelda( this.fila, this.columna, anticipo.getCuenta()                  ,this.texto);  incColumna();
            Excel.adicionarCelda( this.fila, this.columna, anticipo.getTipo_cuenta()             ,this.texto);  incColumna();
            Excel.adicionarCelda( this.fila, this.columna, anticipo.getNombre_cuenta()           ,this.texto);  incColumna();
            Excel.adicionarCelda( this.fila, this.columna, anticipo.getNit_cuenta()              ,this.texto);  incColumna();
            Excel.adicionarCelda( this.fila, this.columna, anticipo.getPorcentaje()              ,this.texto); incColumna();
            Excel.adicionarCelda( this.fila, this.columna, anticipo.getVlrDescuento()            ,this.numero); incColumna();
            Excel.adicionarCelda( this.fila, this.columna, anticipo.getVlrNeto()                 ,this.numero); incColumna();
            Excel.adicionarCelda( this.fila, this.columna, anticipo.getVlrComision()             ,this.numero); incColumna();
            Excel.adicionarCelda( this.fila, this.columna, anticipo.getVlrConsignar()            ,this.numero); incColumna();
            Excel.adicionarCelda( this.fila, this.columna, anticipo.getFecha_transferencia()     ,this.texto); incColumna();

            Excel.adicionarCelda( this.fila, this.columna, anticipo.getPeriodo_Contabilizacion() ,this.texto); incColumna();
            Excel.adicionarCelda( this.fila, this.columna, anticipo.getDocum_Contable()          ,this.texto); incColumna();
            
            Excel.adicionarCelda( this.fila, this.columna, anticipo.getFecha_migracion()         ,this.texto); incColumna();
            Excel.adicionarCelda( this.fila, this.columna, anticipo.getFactura_mims()            ,this.texto); incColumna();


            Excel.adicionarCelda( this.fila, this.columna, anticipo.getVlr_mims_tercero()        ,this.numero); incColumna();
            Excel.adicionarCelda( this.fila, this.columna, anticipo.getEstado_pago_tercero()     ,this.texto); incColumna();
            Excel.adicionarCelda( this.fila, this.columna, anticipo.getFecha_pago_tercero()      ,this.texto); incColumna();
            Excel.adicionarCelda( this.fila, this.columna, anticipo.getCheque_pago_tercero()     ,this.texto); incColumna();
            Excel.adicionarCelda( this.fila, this.columna, anticipo.getCorrida_pago_tercero()    ,this.texto); incColumna();

            Excel.adicionarCelda( this.fila, this.columna, anticipo.getVlr_mims_propietario()    ,this.numero); incColumna();
            Excel.adicionarCelda( this.fila, this.columna, anticipo.getEstado_desc_propietario() ,this.texto); incColumna();
            Excel.adicionarCelda( this.fila, this.columna, anticipo.getFecha_desc_propietario()  ,this.texto); incColumna();
            Excel.adicionarCelda( this.fila, this.columna, anticipo.getCheque_desc_propietario() ,this.texto); incColumna();
            Excel.adicionarCelda( this.fila, this.columna, anticipo.getCheque_desc_propietario() ,this.texto); incColumna();
            Excel.adicionarCelda( this.fila, this.columna, anticipo.getFecha_trans_gasolina() ,this.texto); incColumna();
            Excel.adicionarCelda( this.fila, this.columna, anticipo.getUser_trans_gasolina() ,this.texto); incColumna();

            incFila();
            setearColumna();

        }catch(Exception e){
           throw new Exception( e.getMessage() );
       }
    }




    /**
     * M�todo que configura parametros de Excel
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
     public void configXLS() throws Exception{
       try{
            DirectorioService dd = new DirectorioService();
            dd.create( this.usuario.getLogin()  );
            url = dd.getUrl() +  this.usuario.getLogin() + "/CONSULTAANT"+ Util.getFechaActual_String(6).replaceAll("/|:| ","") + ".xls";

            this.Excel  = new POIWrite(url);
            createStyle();

       }catch(Exception e){
           throw new Exception( e.getMessage() );
       }
    }




    /**
     * M�todo que Define estilo del archivo xls
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public void createStyle()throws Exception{
       try{

         // colores
           /* cAzul       = xls.obtenerColor(  0, 51,102);
            cVerde      = xls.obtenerColor( 51,153,102);
            cAmarillo   = xls.obtenerColor(255,255,153);*/
             HSSFColor cGris       = Excel.obtenerColor(192,192,192);


          this.texto        = Excel.nuevoEstilo("Tahoma", 7,   false , false, "text"        , Excel.NONE , Excel.NONE , Excel.NONE);
          this.numero       = Excel.nuevoEstilo("Tahoma", 8 ,  false , false, "_($* #,##0.00_);(@_)"      , Excel.NONE , Excel.NONE , Excel.NONE);
          this.titulo       = Excel.nuevoEstilo("Tahoma", 8,   true  , false, "text"        , HSSFColor.WHITE.index , HSSFColor.BLACK.index  , HSSFCellStyle.ALIGN_CENTER);
          this.negrilla     = Excel.nuevoEstilo("Arial", 11,   true  , false, "text"        , Excel.NONE , Excel.NONE , Excel.NONE);

          this.porcentaje   = Excel.nuevoEstilo("Tahoma", 8 , false , false, "0.00%" , Excel.NONE , Excel.NONE , Excel.NONE);

          this.grupoA       = Excel.nuevoEstilo("Tahoma",10,   true  , false, "text"        , HSSFColor.WHITE.index , HSSFColor.TEAL.index        , HSSFCellStyle.ALIGN_CENTER);
          this.grupoB       = Excel.nuevoEstilo("Tahoma",10,   true  , false, "text"        , HSSFColor.WHITE.index , cGris.getIndex()            , HSSFCellStyle.ALIGN_CENTER);
          this.grupoC       = Excel.nuevoEstilo("Tahoma",10,   true  , false, "text"        , HSSFColor.WHITE.index , HSSFColor.LIGHT_BLUE.index  , HSSFCellStyle.ALIGN_CENTER);
          this.grupoD       = Excel.nuevoEstilo("Tahoma",10,   true  , false, "text"        , HSSFColor.WHITE.index , HSSFColor.ORANGE.index      , HSSFCellStyle.ALIGN_CENTER);

       }catch(Exception e){
           throw new Exception( e.getMessage() );
       }
    }








     /**
     * M�todo que crea la hoja
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public void createHoja(String nombreHoja)throws Exception{
        try{

            hoja = nombreHoja;
            this.Excel.obtenerHoja(hoja);
            setearFila();
            setearColumna();

        }catch(Exception e){
           throw new Exception( e.getMessage() );
       }
    }


    public void incFila()      {  this.fila++;      }
    public void incColumna()   {  this.columna++;   }
    public void setearFila()   {  this.fila=0;      }
    public void setearColumna(){  this.columna=0;   }



}