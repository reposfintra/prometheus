/*************************************************************
 * Nombre: ReporteTiemposViajesRemesaThreads.java
 * Descripci�n: Hilo para crear el reporte de control de vacios
 * Autor: Ing. Jose de la rosa
 * Fecha: 23 de agosto de 2006, 10:54 AM
 * Versi�n: Java 1.0
 * Copyright: Fintravalores S.A. S.A.
 **************************************************************/

package com.tsp.operation.model.threads;


import java.text.*;
import java.util.Date;
import java.text.*;
import java.sql.SQLException;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.util.*;
import java.util.*;
import java.io.*;

public class ReporteTiemposViajesRemesaThreads extends Thread{
    private String procesoName;
    private String des;
    String id = "";
    String fecini = "";
    String fecfin = "";
    String dstrct = "";
    String cliente = "";
    String viaje = "";
    Model model;
    /** Creates a new instance of ReporteTiemposViajesRemesaThreads */
    public ReporteTiemposViajesRemesaThreads (Model model) {
        this.model = model;
    }
    
    public void start ( String fecini, String fecfin, String id, String dstrct, String cliente, String viaje){
        this.procesoName = "Reporte Tiempos de Viaje";
        this.des = "Reporte Tiempos de Viajes en un periodo de tiempo";
        this.id = id;
        this.fecini = fecini;
        this.fecfin = fecfin;
        this.dstrct = dstrct;
        this.cliente = cliente;
        this.viaje = viaje;
        super.start ();
    }
    public synchronized void run(){
        try{
            String fecha_actual = Util.getFechaActual_String(6);
            
            //registramos el proeso en el log de procesos
            model.LogProcesosSvc.InsertProceso(this.procesoName, this.hashCode(), des, this.id);
            
            //obtenemos la lista de facturas creadas segun parametros definidos
            Vector lista = model.remesaService.obtenerTiemposRemesa (fecini, fecfin, cliente, viaje);
            if (lista!=null && lista.size()>0){
                Usuario us = (Usuario) model.usuarioService.obtenerUsuario(id);
                ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
                String path = rb.getString("ruta") + "/exportar/migracion/"+id;
                
                File file = new File(path);
                file.mkdirs();
                
                String a�o = fecha_actual.substring(0,4);
                String mes = fecha_actual.substring(5,7);
                String dia = fecha_actual.substring(8,10);
                String hora = fecha_actual.substring(11,13);
                String min = fecha_actual.substring(14,16);
                String seg = fecha_actual.substring(17,19);
                
                com.tsp.operation.model.beans.POIWrite xls = new com.tsp.operation.model.beans.POIWrite(path+"/TIEMPOS_VIAJES_REM " +a�o + "-" + mes + "-" + dia +".xls");
                
                // Definicion de estilos: fuente, tama�o, negrita, cursiva, formato, color, fondo, alineado                                           
                HSSFCellStyle fecha = xls.nuevoEstilo(      "verdana", 12, false , false, "yyyy/mm/dd", HSSFColor.BLACK.index, xls.NONE, HSSFCellStyle.ALIGN_RIGHT );
                HSSFCellStyle texto = xls.nuevoEstilo(      "verdana", 12, false, false, "text", HSSFColor.BLACK.index, xls.NONE, HSSFCellStyle.ALIGN_LEFT);
                HSSFCellStyle numero = xls.nuevoEstilo(     "verdana", 12, false, false, "#", xls.NONE, xls.NONE, HSSFCellStyle.ALIGN_RIGHT);
                HSSFCellStyle negrita = xls.nuevoEstilo(    "verdana", 12, true, false, "text", HSSFColor.BLACK.index, xls.NONE, xls.NONE);
                HSSFCellStyle header = xls.nuevoEstilo(     "verdana", 18, true, false, "text", HSSFColor.LIME.index,xls.NONE, HSSFCellStyle.ALIGN_LEFT);
                HSSFCellStyle titulo = xls.nuevoEstilo(     "verdana", 16, true, false, "text", HSSFColor.INDIGO.index, xls.NONE, HSSFCellStyle.ALIGN_LEFT);
                HSSFCellStyle subtitulo = xls.nuevoEstilo(  "verdana", 14, true, false, "text", HSSFColor.INDIGO.index, xls.NONE, HSSFCellStyle.ALIGN_LEFT);
                HSSFCellStyle cabecera = xls.nuevoEstilo(   "verdana", 12, true, false, "text", HSSFColor.WHITE.index, HSSFColor.INDIGO.index, HSSFCellStyle.ALIGN_CENTER );
                
                
                xls.obtenerHoja("BASE");
                //formateo al abrir hoja (tamano)
                xls.cambiarMagnificacion(3,4);
                
                //Titulo (cabecera)
                xls.adicionarCelda(0, 0, "TRANSPORTES SANCHEZ POLO S.A", header);
                xls.combinarCeldas(0, 0, 0, 5);  
                xls.adicionarCelda(2, 0, "TIEMPOS DE VIAJE", titulo);
                xls.combinarCeldas(2, 0, 0, 5);
                xls.adicionarCelda(3, 0, "Fecha Inicial:", subtitulo);
                xls.adicionarCelda(3, 1, fecini, negrita);
                xls.adicionarCelda(3, 2, "Fecha Final:", subtitulo);
                xls.adicionarCelda(3, 3, fecfin, negrita);
                xls.adicionarCelda(4, 0, "Elaborado Por: ", subtitulo);
                xls.adicionarCelda(4, 1, us.getNombre(), negrita);
                xls.adicionarCelda(4, 2, "Agencia:", subtitulo);
                xls.adicionarCelda(4, 3, model.ciudadservice.obtenerNombreCiudad(us.getId_agencia()), negrita);
                xls.adicionarCelda(4, 4, "Distrito:", subtitulo);
                xls.adicionarCelda(4, 5, dstrct, negrita);
                xls.adicionarCelda(5, 0, "Fecha Reporte", subtitulo);
                xls.adicionarCelda(5, 1, fecha_actual, negrita);
                
                int fila = 7;
                int col  = 0;
                
                
                //definicion de titulos de la tabla
                xls.adicionarCelda(fila ,col++ , "ORIGEN"                       , cabecera );//1
                xls.adicionarCelda(fila ,col++ , "DESTINO"                      , cabecera );//2
                xls.adicionarCelda(fila ,col++ , "PAIS ORIGEN"                  , cabecera );//3
                xls.adicionarCelda(fila ,col++ , "OT"                           , cabecera );//4
                xls.adicionarCelda(fila ,col++ , "CLIENTE"                      , cabecera );//5
                xls.adicionarCelda(fila ,col++ , "AGENCIA DUE�A"                , cabecera );//6
                xls.adicionarCelda(fila ,col++ , "NRO DE FACTURA"               , cabecera );//7
                xls.adicionarCelda(fila ,col++ , "TIPO DE VIAJE"                , cabecera );//8
                xls.adicionarCelda(fila ,col++ , "FECHA DESPACHO"               , cabecera );//9
                xls.adicionarCelda(fila ,col++ , "LLEGADA_FRONT_EXP"            , cabecera );//10
                xls.adicionarCelda(fila ,col++ , "CARGA_LISTA_EXP"              , cabecera );//11
                xls.adicionarCelda(fila ,col++ , "SALIDA FRONT EXP"             , cabecera );//12
                xls.adicionarCelda(fila ,col++ , "LLEGADA_FRONT_IMP"            , cabecera );//13
                xls.adicionarCelda(fila ,col++ , "TRAM ADUANAS OK"              , cabecera );//14
                xls.adicionarCelda(fila ,col++ , "LLEGADA FACTURA COM"          , cabecera );//15
                xls.adicionarCelda(fila ,col++ , "LLEGADA CERT ORIGEN"          , cabecera );//16
                xls.adicionarCelda(fila ,col++ , "COLOCACION DE FONDOS"         , cabecera );//17
                xls.adicionarCelda(fila ,col++ , "DEPOSITO PREVIO"              , cabecera );//18
                xls.adicionarCelda(fila ,col++ , "CARGA_LISTA_IMP"              , cabecera );//19
                xls.adicionarCelda(fila ,col++ , "LIBERACION DE ADUANAS"        , cabecera );//20
                xls.adicionarCelda(fila ,col++ , "SALIDA_FRONT_IMP"             , cabecera );//21
                xls.adicionarCelda(fila ,col++ , "FECHA_ENTREGA"                , cabecera );//22
                xls.adicionarCelda(fila ,col++ , "ORIGEN-FRONTERA"              , cabecera );//23
                xls.adicionarCelda(fila ,col++ , "DEMORA EN TRAMITES EXP"       , cabecera );//24
                xls.adicionarCelda(fila ,col++ , "DEMORA FACTURA COM"           , cabecera );//25
                xls.adicionarCelda(fila ,col++ , "DEMORA CERT ORIGEN"           , cabecera );//26
                xls.adicionarCelda(fila ,col++ , "DEMORA COLOCACION FONDOS"     , cabecera );//27
                xls.adicionarCelda(fila ,col++ , "DEMORA DEPOSITO PREVIO"       , cabecera );//28
                xls.adicionarCelda(fila ,col++ , "DEMORA TIMP"                  , cabecera );//29
                xls.adicionarCelda(fila ,col++ , "DEMORA REEXP EXP"             , cabecera );//30
                xls.adicionarCelda(fila ,col++ , "TFRONT"                       , cabecera );//31
                xls.adicionarCelda(fila ,col++ , "FR-DT"                        , cabecera );//32
                xls.adicionarCelda(fila ,col++ , "TIEMPO DE VIAJE"              , cabecera );//33
                //definicion de la tabla con los datos obtenidos de la consulta
                Iterator it = lista.iterator();
                while(it.hasNext()){
                    fila++;
                    col = 0;
                    Remesa rem = (Remesa) it.next(); 
                    
                    xls.adicionarCelda(fila ,col++ , rem.getOrirem ()                                                                   , texto );//ORIGEN 1
                    xls.adicionarCelda(fila ,col++ , rem.getDesrem ()                                                                   , texto );//DESTINO 2
                    xls.adicionarCelda(fila ,col++ , rem.getNombreOrigen ()                                                             , texto );//PAIS ORIGEN 3
                    xls.adicionarCelda(fila ,col++ , rem.getNumrem ()                                                                   , texto );//OT 4
                    xls.adicionarCelda(fila ,col++ , rem.getNombre_cli()                                                                , texto );//CLIENTE 5
                    xls.adicionarCelda(fila ,col++ , rem.getAgcrem ()                                                                   , texto );//AGENCIA DUE�A 6
                    xls.adicionarCelda(fila ,col++ , rem.getDocumento ()                                                                , texto );//NRO DE FACTURA 7
                    xls.adicionarCelda(fila ,col++ , rem.getTipoviaje ()                                                                , texto );//TIPO DE VIAJE 8
                    xls.adicionarCelda(fila ,col++ , rem.getFeccum ()                                                                   , texto );//FECHA DESPACHO 9
                    xls.adicionarCelda(fila ,col++ , rem.getFec_fron ()                                                                 , texto );//LLEGADA_FRONT_EXP 10
                    xls.adicionarCelda(fila ,col++ , ""                                                                                 , texto );//CARGA_LISTA_EXP 11
                    xls.adicionarCelda(fila ,col++ , rem.getFec_sal_fron()                                                              , texto );//SALIDA FRONT EXP 12
                    xls.adicionarCelda(fila ,col++ , rem.getFec_lle_imp()                                                               , texto );//LLEGADA_FRONT_IMP 13
                    xls.adicionarCelda(fila ,col++ , ""                                                                                 , texto );//TRAM ADUANAS OK 14
                    xls.adicionarCelda(fila ,col++ , ""                                                                                 , texto );//LLEGADA FACTURA COM 15
                    xls.adicionarCelda(fila ,col++ , ""                                                                                 , texto );//LLEGADA CERT ORIGEN 16
                    xls.adicionarCelda(fila ,col++ , ""                                                                                 , texto );//COLOCACION DE FONDOS 17
                    xls.adicionarCelda(fila ,col++ , ""                                                                                 , texto );//DEPOSITO PREVIO 18
                    xls.adicionarCelda(fila ,col++ , ""                                                                                 , texto );//CARGA_LISTA_IMP 19
                    xls.adicionarCelda(fila ,col++ , ""                                                                                 , texto );//LIBERACION DE ADUANAS 20
                    xls.adicionarCelda(fila ,col++ , rem.getFec_sal_imp()                                                               , texto );//SALIDA_FRONT_IMP 21
                    xls.adicionarCelda(fila ,col++ , rem.getFec_fin ()                                                                  , texto );//FECHA_ENTREGA 22
                    xls.adicionarCelda(fila ,col++ , Double.parseDouble( rem.getFec_ori_fron ().equals("")?"0":rem.getFec_ori_fron () ) , numero );//ORIGEN-FRONTERA 23
                    xls.adicionarCelda(fila ,col++ , 0                                                                                  , numero );//DEMORA EN TRAMITES EXP 24
                    xls.adicionarCelda(fila ,col++ , 0                                                                                  , numero );//DEMORA FACTURA COM 25
                    xls.adicionarCelda(fila ,col++ , 0                                                                                  , numero );//DEMORA CERT ORIGEN 26
                    xls.adicionarCelda(fila ,col++ , 0                                                                                  , numero );//DEMORA COLOCACION FONDOS 27
                    xls.adicionarCelda(fila ,col++ , 0                                                                                  , numero );//DEMORA DEPOSITO PREVIO 28
                    xls.adicionarCelda(fila ,col++ , 0                                                                                  , numero );//DEMORA TIMP 29
                    xls.adicionarCelda(fila ,col++ , 0                                                                                  , numero );//DEMORA REEXP EXP 30
                    xls.adicionarCelda(fila ,col++ , Double.parseDouble( rem.getTiem_fron ().equals("")?"0":rem.getTiem_fron () )       , numero );//TFRONT 31
                    xls.adicionarCelda(fila ,col++ , Double.parseDouble( rem.getFec_fron_des ().equals("")?"0":rem.getFec_fron_des () ) , numero );//FR-DT 32
                    xls.adicionarCelda(fila ,col++ , Double.parseDouble( rem.getTiem_viaje ().equals("")?"0":rem.getTiem_viaje () )     , numero );//TIEMPO DE VIAJE 33
                }
                xls.cerrarLibro();
                model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(), this.id, "Proceso Exitoso!");
            }else {
                model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(), this.id, "No se encontraron OC en las tablas...");
            }
            model.cxpDocService.setEnproceso();
        }
        catch(Exception e){
            try{
                model.LogProcesosSvc.finallyProceso(this.procesoName, this.hashCode(),this.id,"ERROR :" + e.getMessage());
                model.cxpDocService.setEnproceso();
            }
            catch(Exception f){
                try{
                    model.LogProcesosSvc.finallyProceso(this.procesoName,this.hashCode(),this.id,"ERROR :");
                }catch(Exception p){    }
            }
        }
    }    
}
